#clean cache of npm and bower
sudo npm cache clean
sudo bower cache clean --allow-root
#install bower with ember 1.10
sudo rm -rf bower_components
sudo bower install --save ember#1.10.1 --allow-root
sudo bower install --allow-root
#install openlayers tmp file
sudo bower install --save openlayers3@3.0.0 --allow-root
#reload npm
sudo rm -rf node_modules
sudo npm install


