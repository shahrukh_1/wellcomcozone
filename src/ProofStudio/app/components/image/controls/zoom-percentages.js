import Ember from "ember";

export default Ember.Component.extend({
  tagName: 'span',
  classNames: ['percentages', 'hidden-sm', 'hidden-xs'],

  minZoomResolution: 1,
  maxZoomResolution: 0,// probably 16
  currentResolution: 8,
  zoomLevels: Ember.A([]),

  highResolution: true,
  enableHighResolution: true,
  restrictedZoom: false,

  onInit: function() {
    this.get('viewerChangedCommunicationService').RegisterOnViewerChagned(this, 'refreshHighResolution');
    this.get('softProofingParamCommunicationService').RegisterOnUHRTriggeredChanged(this,'disableUHR');
    this.setHighRes();
    var self = this;
    Ember.run.later(function(){
      
       if( window.AppkitENV.IsApprovalZoomLevelChangesEnabled > 0 &&  window.AppkitENV.ApprovalZoomLevel > 0)
       {
        self.send('changeZoomTo', window.AppkitENV.ApprovalZoomLevel);
       }
      },850);
    
  }.on('init'),

  onCleanup: function() {
    this.get('viewerChangedCommunicationService').UnRegisterOnViewerChagned(this, 'refreshHighResolution');
  this.get('softProofingParamCommunicationService').UnRegisterOnUHRTriggeredChanged(this,'disableUHR');
  }.on('willDestroyElement'),

  refreshHighResolution: function(approvalId){
    this.setHighRes(approvalId);
  },

  disableUHR(triggeredBy) {
    this.set('highResolution', true);
  },

  zoomInPercentages: function(key, value, previousValue) {
    // setter
    if (arguments.length > 1) {
      if (isNaN(value)) {
        return;
      }
    }
    var self = this;
    Ember.run.later(function(){
    var session = self.get('session');
    var previousPageZoom = session.get('previousPageZoomLevel');
     if(previousPageZoom != null && previousPageZoom > 0)
     {
      self.setCurrentResolutionFromZoomInPercentages(previousPageZoom);
     }
    });
    // getter
    return this.getZoomInPercentages();
  }.property('currentResolution'),

  onCurrentResolutionChanged: function() {
    var currentZoom = this.getZoomInPercentages();
    if (currentZoom == 0) {
      return;
    }
    var zoomLevels = this.get('zoomLevels');// zoomLevels[0] is 100
    if (!zoomLevels || zoomLevels.get('length') == 0) {
      return;
    }

    for (var i = 0; i < zoomLevels.get('length'); i++) {
      var zoomLevel = zoomLevels.objectAt(i);
      if (currentZoom / 2.5 == zoomLevel.zoom.toFixed(2)) {
        Ember.set(zoomLevel, "isChecked", true);
      }
      else {
        Ember.set(zoomLevel, "isChecked", false);
      }
    }
  }.observes('currentResolution'),

  GetNextZoomIn() {// higher zoom
    var currentZoom = this.getZoomInPercentages();
    currentZoom = currentZoom / 2.5;
    var zoomLevels = this.get('zoomLevels');// zoomLevels[0] is 100
    if (zoomLevels.get('length') == 0) {
      return 100;
    }

    for (var i = 1; i < zoomLevels.get('length'); i++) {
      var zoomLevel = zoomLevels.objectAt(i);
      var zoom = zoomLevel.zoom;
      if (currentZoom >= zoom) {
        var nextZoomLevel = zoomLevels.objectAt(i-1);
        return nextZoomLevel.zoom;
      }
    }

    return zoomLevels.get('lastObject.zoom');
  },
  GetNextZoomOut() {// lower zoom
    var currentZoom = this.getZoomInPercentages();
    currentZoom = currentZoom / 2.5;
    var zoomLevels = this.get('zoomLevels');// zoomLevels[0] is 100
    if (zoomLevels.get('length') == 0) {
      return 100;
    }

    for (var i = 1; i < zoomLevels.get('length'); i++) {
      var zoomLevel = zoomLevels.objectAt(i);
      var zoom = zoomLevel.zoom;
      if (currentZoom > zoom) {
        return zoom;
      }
    }
    return zoomLevels.get('lastObject.zoom');
  },

  setCurrentResolutionFromZoomInPercentages(zoomInPercentages) {
    if(Ember.$('#modalAnnotationsReportFilter').length === 0 || Ember.$('#modalAnnotationsReportFilter')[0].style.cssText === "display: none;" || Ember.$('#modalAnnotationsReportFilter')[0].style.cssText === "")
    {
    var resolution = 100 / zoomInPercentages;

    this.sendAction('zoomToResolution', resolution);
    }
  },
  getZoomInPercentages() { 
    var res = this.get('currentResolution');
    if (typeof res !== 'number')
      return 0;

    var zoomInPercentages = 100 / res;
    zoomInPercentages = zoomInPercentages * 2.5;
    if (zoomInPercentages < 10)
      return Math.round(zoomInPercentages * 100) / 100;
    else
      return Math.round(zoomInPercentages * 10) / 10;
  },

  isTileSize: function() {
    var res = this.get('currentResolution');
    if (typeof res !== 'number')
      return false;

    if (res % 1 !== 0)
      return false;
    return res && (res & (res - 1)) === 0;
  }.property('currentResolution'),

  setHighRes: function(versionId){
    var softProofingParam = this.get('session.softProofingParams').filter(function(spp){
      return spp.DefaultSession === true && (versionId == undefined || spp.versionId === versionId);
    }).get('firstObject');
    if(softProofingParam) {
      this.set('highResolution', softProofingParam.HighResolution);
      this.set('enableHighResolution', softProofingParam.EnableHighResolution);
    }
  },

  setPossibleZoomsInPercentages: function() {
    var tmpR = this.get('maxZoomResolution');
    if (!tmpR || tmpR == 0)
      return;

    var zoomLevels = this.get('zoomLevels');
    zoomLevels.clear();

    var IsrestrictedZoom = this.get("restrictedZoom");

   if(!IsrestrictedZoom){
      zoomLevels.pushObject({zoom:200, displayedZoom:500, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:160, displayedZoom:400, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:120, displayedZoom:300, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:100, displayedZoom:250, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:80, displayedZoom:200, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:70, displayedZoom:175, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:60, displayedZoom:150, isFractional:false, isChecked:false, visible:true});
      zoomLevels.pushObject({zoom:50, displayedZoom:125, isFractional:false, isChecked:false, visible:true});
    };

    zoomLevels.pushObject({zoom:40, displayedZoom:100, isFractional:false, isChecked:false, visible:true});
    
    var maxResolution = this.get('maxResolution');
    if (!maxResolution)
      return;

    var resolution = maxResolution / 2;
    // on higher zoom we have the possibility to zoom in more steps

    if (resolution < 1) {
      return;
    }
    zoomLevels.pushObject({zoom:35, displayedZoom:87.5, isFractional:true, isChecked:false, visible:false});
    zoomLevels.pushObject({zoom:30, displayedZoom:75, isFractional:true, isChecked:false, visible:true});
    zoomLevels.pushObject({zoom:25, displayedZoom:62.5, isFractional:true, isChecked:false, visible:false});
    zoomLevels.pushObject({zoom:20, displayedZoom:50, isFractional:false, isChecked:false, visible:true});

    resolution = resolution / 2;
    if (resolution < 1) {
      return;
    }

    zoomLevels.pushObject({zoom:15, displayedZoom:37.5, isFractional:true, isChecked:false, visible:false});
    zoomLevels.pushObject({zoom:10, displayedZoom:25, isFractional:false, isChecked:false, visible:true});
    var zoom = 25;
    resolution = resolution / 2;
    // while (resolution >= 1) {
    //   zoom = zoom / 2;
    //   zoomLevels.pushObject({zoom:zoom, displayedZoom:zoom.toFixed(2), isFractional:false, isChecked:false, visible:true});

    //   resolution = resolution / 2;
    // }

  }.observes('maxResolution', 'maxZoomResolution'),

  minZoomInPercentages: function() {
    return 100 * (1/ this.get('maxResolution'));
  }.property('maxResolution'),

  maxResolution: function() {
    return this.get('maxZoomResolution') - this.get('minZoomResolution') + 1;
  }.property('minZoomResolution', 'maxZoomResolution'),

  actions: {
    changeZoomTo:function(zoom) {
      this.setCurrentResolutionFromZoomInPercentages(zoom);
      if(window.AppkitENV.IsApprovalZoomLevelChangesEnabled > 0){
      this.store.adapterFor('annotation').updateApprovalZoomLevel(zoom, window.AppkitENV.approval_id).then(function(result){
      }
      );
    }
    },
    zoomIn: function() {
      var nextZoom = this.GetNextZoomIn();
      this.setCurrentResolutionFromZoomInPercentages(nextZoom);
      if(window.AppkitENV.IsApprovalZoomLevelChangesEnabled > 0){
      this.store.adapterFor('annotation').updateApprovalZoomLevel(nextZoom, window.AppkitENV.approval_id).then(function(result){
      }
      );
    }
    },
    zoomOut: function() {
      var prevZoom = this.GetNextZoomOut();
      this.setCurrentResolutionFromZoomInPercentages(prevZoom);
      if(window.AppkitENV.IsApprovalZoomLevelChangesEnabled > 0){
      this.store.adapterFor('annotation').updateApprovalZoomLevel(prevZoom, window.AppkitENV.approval_id).then(function(result){
      }
      );
    }
    },

    changeToHigherResolution() {
      if (this.get('highResolution') == false) {
        this.set('highResolution', true);
        this.sendAction('changeToHighResolution');
      }
    }

  }
});
