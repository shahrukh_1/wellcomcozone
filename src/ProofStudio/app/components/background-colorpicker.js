import Ember from "ember";
import ColorPicker from 'appkit/components/color-picker';

var BackgroundColorPicker = ColorPicker.extend({
    layoutName:'components/color-picker',
    classNames: ['czn-background-color-picker'],

    componentInserted: function(){
        this._super();
        this.sendAction('changeBackgroundColorPicker');
    },

    save: function(hexColor){
        this._super();
        this.sendAction('changeBackgroundColorPicker', hexColor);

        //send call to services to save the new color
        this.sendAction('saveBackgroundColor', hexColor);
        this.set('lastColorSaved', hexColor);
    },

    colorChanged: function(hexColor){
        this._super();
        Ember.$(".czn-image-viewer, .video-view, .zip-view").css('background', hexColor);
    },

    cancel: function(){
        this.sendAction('changeBackgroundColorPicker');
    }
});
export default BackgroundColorPicker;