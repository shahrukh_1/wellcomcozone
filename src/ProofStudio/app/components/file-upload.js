import Ember from "ember";

var FileUploadComponent = Ember.Component.extend(Ember.Evented,{
    url: '',
    type: 'POST',
    canAdd: true,
    canDelete: true,
    layoutName: 'file-upload',
    multipleUpload: null,

    getAttachmentsCount: function(){
        var annotationId = this.get('annotationId');
        return new Ember.RSVP.Promise(function (resolve, reject) {
            var hash = {
                url: window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/attachments-count',
                type: 'GET',
                data: {
                    key: window.AppkitENV.key,
                    user_key: window.AppkitENV.user_key,
                    is_external_user: window.AppkitENV.is_external_user,
                    annotationId: annotationId
                },
                success: function(response){
                    hash.success(response);
                },
                error: function(){
                    hash.error();
                }
            };

            hash.success = function (data) {
                Ember.run(null, resolve, data);
            };

            hash.error = function () {
                Ember.run(null, reject);
            };

            Ember.$.ajax(hash);
          });
    },

    didInsertElement: function(){
        var uploader = this.$().find('.czn-uploader');
        var files = this.get('multipleUpload.files');
        this.set('uploader', uploader);
        this.set('url', window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/annotation-attachment?key=' + window.AppkitENV.key + '&user_key=' + window.AppkitENV.user_key + '&is_external_user=' + window.AppkitENV.is_external_user);

        var self = this;
        uploader.change(function(){
            if (!(window.ActiveXObject || "ActiveXObject" in window)) { // if is not IE
                self.get('multipleUpload').set('errorFilesCountTooLong', false);
                self.get('multipleUpload').set('errorFileSizeTooLong', false);
            }

            files = self.get('multipleUpload.model.firstObject.AnnotationAttachments') || self.get('multipleUpload.newFiles');
            var filenames = uploader.get(0).files;
            var filename = '';
            for (var i = 0; i < filenames.length; ++i) {
                filename = filenames.item(i).name;
                var size = filenames.item(i).size;

                // max 5 files, size of each file <= 250MB
                if (files && size <= 262144000 && files.get('length') < 5){
                    var attachment = self.store.createRecord('annotation-attachment', { Name: filename, DisplayName: filename, Filename: filename, isSent: false, Guid: '', inProgress: true});
                    files.pushObject(attachment);

                    self.get('multipleUpload').set('errorFilesCountTooLong', false);
                    self.get('multipleUpload').set('errorFileSizeTooLong', false);
                }
                else if (size > 262144000){
                    self.get('multipleUpload').set('errorFileSizeTooLong', true);
                }
                else if (files.get('length') >= 5){
                    self.get('multipleUpload').set('errorFilesCountTooLong', true);
                }
            }


            var fileForUpload = uploader.get(0).files;
            self.getAttachmentsCount().then(function(attachmentsCount){
                for(i = 0; i < Math.min(fileForUpload.length, 5 - attachmentsCount); i++){
                    filename = fileForUpload[i].name;
                    self.upload(null, [fileForUpload[i]]).then(function(data){
                        var files = self.get('multipleUpload.model.firstObject.AnnotationAttachments') || self.get('multipleUpload.newFiles');
                        if (files && data.files.length > 0){
                            files.forEach(function(file){
                                if (file.get('Filename') === data.files[0].filename && file.get('inProgress') === true){
                                    file.set('Guid', data.files[0].guid);
                                    file.set('inProgress', false);
                                    file.set('id', data.files[0].id);
                                }
                            });
                        }
                    });
                }
                uploader.val(null);
            });
        });

        this.sendAction('fileUploadInit', this);
    },

    setupFormData: function(files, extra) {
        var formData = new FormData();

        for (var prop in extra) {
            if (extra.hasOwnProperty(prop)) {
                formData.append(this.toNamespacedParam(prop), extra[prop]);
            }
        }

        // if is a array of files ...
        if (Ember.isArray(files)) {
            var paramName;

            for (var i = files.length - 1; i >= 0; i--) {
                paramName = this.toNamespacedParam(this.paramName) + '[' + i + ']';
                formData.append(paramName , files[i]);
            }
        } else {
            // if has only one file object ...
            formData.append(this.toNamespacedParam(this.paramName), files);
        }

        return formData;
    },

    toNamespacedParam: function(name) {
        if (this.paramNamespace) {
            return this.paramNamespace + '[' + name + ']';
        }

        return name;
    },

    didUpload: function(data) {
        this.set('isUploading', false);
        this.trigger('uploadCompleted', data);
    },

    didProgress: function(e, fileName) {
        e.percent = e.loaded / e.total * 100;
        var scrollBars = Ember.$('.progress-bar');
        scrollBars.each(function(i,v){
            if(v.id === fileName)
            {
            Ember.$(v).css('width', e.percent + '%');
            }
        });
        this.trigger('uploadInProgress', e);
    },

    abort: function() {
        this.set('isUploading', false);
        this.trigger('isAborting');
    },

    ajax: function(url, params, method, fileName) {
        var self = this;
        var settings = {
            url: url,
            type: method || 'POST',
            contentType: false,
            processData: false,
            xhr: function() {
                var xhr = Ember.$.ajaxSettings.xhr();
                xhr.upload.onprogress = function(e) {
                    self.didProgress(e,fileName);
                };
                self.one('isAborting', function() { xhr.abort(); });
                return xhr;
            },
            data: params
        };

      return this._ajax(settings);
    },

    _ajax: function(settings) {
        return new Ember.RSVP.Promise(function(resolve, reject) {
            settings.success = function(data) {
                Ember.run(null, resolve, data);
            };

            settings.error = function(jqXHR, textStatus, errorThrown) {
                Ember.run(null, reject, jqXHR);
                console.log('errorThrown: ', errorThrown);
                console.log('textStatus: ', textStatus);
            };

            Ember.$.ajax(settings);
        });
    },

    upload: function(extra, files, url){
        extra = extra || {};
        if (typeof(files) === 'undefined') {
            files = this.get('uploader').get(0).files;
        }
        var data = this.setupFormData(files, extra);
        if (typeof (url) === 'undefined'){
            url = this.get('url');
        }
        var annotationId = this.get('annotationId');
        if (typeof(annotationId) !== 'undefined' && annotationId > 0){
            url += '&annotationid=' + annotationId;
        }
        var type = this.get('type');
        var self = this;

        if (!Ember.isEmpty(files)) {

            this.set('isUploading', true);

            return this.ajax(url, data, type, files[0].name).then(function (respData) {
                self.didUpload(respData);
                return respData;
            });
        }
        else{
            // return empty promise
            return new Ember.RSVP.Promise(function() {} );
        }
    },
    delete: function(extra, files, url){
        extra = extra || {};
        if (typeof(files) === 'undefined') {
            files = this.get('uploader').get(0).files;
        }
        var data = this.setupFormData(files, extra);
        if (typeof (url) === 'undefined'){
            url = this.get('url');
        }
        var type = 'DELETE';
        var self = this;

        this.set('isUploading', true);

        return this.ajax(url, data, type).then(function (respData) {
            self.didUpload(respData);
            return respData;
        });
    },

    deleteFile: function(filename, guid, annotationId){
        var uploader = this.get('uploader');
        var self = this;
        if (uploader) {
            var url = this.get('url') + '&filename=' + window.escape(filename) + '&guid=' + guid;
            if (typeof(annotationId) !== 'undefined'){
                url += "&annotationId=" + annotationId;
            }
            return this.delete(null, [], url).then(function(dataResult){
                var files = self.get('multipleUpload.model.firstObject.AnnotationAttachments') || self.get('multipleUpload.newFiles');
                if (files){
                    files.forEach(function(file){
                        if (file && file.get('Guid') === dataResult.guid){
                            files.removeObject(file);

                            var attachments = self.store.all('annotation-attachment').filter(function(attachment){
                                return attachment.get('Guid') === dataResult.guid;
                            });
                            if (attachments && attachments.get('length') > 0){
                                attachments.forEach(function(attachment){
                                    attachment.transitionTo('deleted.saved');
                                });
                            }
                        }
                    });
                }
            });
        }
        else{
            // return empty promise
            return new Ember.RSVP.Promise(function() {} );
        }
    }
});
export default FileUploadComponent;
