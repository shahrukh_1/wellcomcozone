import Ember from "ember";

export default Ember.Component.extend({
  showAlertDialog: false,
  alertDialogMessage: '',
  versionId:'',
  actions: {
    closeAlert() {
      this.set('showAlertDialog', false);
      this.get('modalDialogsCommunicationService').CloseAlertUHRDialog(this.get('versionId'));
    }
  },

  showMessageInAlertDialog(message) {
    this.set('alertDialogMessage', message);
    this.set('versionId', message.byVersionId);
    this.set('showAlertDialog', true);
  },

  onInit: function () {
    this.set('showAlertDialog', false);
    this.get('modalDialogsCommunicationService').RegisterOnOpenAlertDialog(this, 'showMessageInAlertDialog');
  }.on('init'),

  cleanup: function() {
    this.get('modalDialogsCommunicationService').UnRegisterOnOpenAlertDialog(this, 'showMessageInAlertDialog');
  }.on('willDestroyElement')
});
