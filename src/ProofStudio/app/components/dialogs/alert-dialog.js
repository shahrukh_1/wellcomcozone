import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var ConfirmationModalController = Ember.Component.extend({
  messageTitle: function() {
    return this.get('message.title') || '';
  }.property('message.title'),

  messageHeading: function() {
    return this.get('message.heading') || '';
  }.property('message.heading'),

  messageBody: function() {
    return this.get('message.body') || '';
  }.property('message.body'),

  messageNote: function() {
    return this.get('message.note') || '';
  }.property('message.note'),

  okButton: function() {
    return this.get('message.ok') || 'OK';
  }.property('message.ok'),

  onInit: function () {
    var self = this;
    Ember.$(document).on('keydown.alertDialog', function (e) {
      if (e.keyCode == 27) {
        self.send('cancel');
      }
    });
  }.on('init'),

  actions: {
    cancel: function () {
      Ember.$(document).off('keydown.alertDialog');
      this.sendAction('closeAction');
    }
  }
});
export default ConfirmationModalController;
