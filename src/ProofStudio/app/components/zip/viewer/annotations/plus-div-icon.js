import Ember from "ember";

var PlusDivIcon = Ember.Object.extend({
  className: 'leaflet-marker-icon czn-plus-icon',
  element: null,
  pinslayer : null,
  width: 0,
  height: 0,
  htmlMinMaxValues: 0,

  createIcon: function () {
    var div = document.createElement('div');
    div.setAttribute('class', this.className);
    var numdiv = document.createElement('span');
    numdiv.setAttribute ( "class", "fa fa-plus-square-o" );
    numdiv.innerHTML = '';
    div.appendChild ( numdiv );

    return div;
  },

  addTo: function(parentContainer){
    var self = this;
    var position = this.get('position');
    var element = this.createIcon();

    Ember.$(element).css({
      left: position.x,
      top: position.y
    });

    Ember.$(element).on('click', function(ev){
      var pinsLayer = self.get('pinslayer');
      if (pinsLayer){
        pinsLayer.clickMarker(ev);
      }
    });

    parentContainer.append(element);
    this.set('element', element);
  },

  remove: function(){
    var element = this.get('element');
    if (element){
      element.remove();
    }
  },

  syncPosition: function(width, height, isNewAnnotationAdding, newAnnotationOrgWidht, newAnnotationOrgHeight ){
    var position = null;
    var element = this.get('element');
    if (element){
      var originalPosition = null;
      var newLeft;
      var newTop;
      if(isNewAnnotationAdding){
        originalPosition = {offsetX: newAnnotationOrgWidht, offsetY: newAnnotationOrgHeight};
        position = {left: this.position.x, top: this.position.y};
        newLeft = originalPosition.offsetX > 0 ? parseInt(position.left * width/originalPosition.offsetX) : position.left;
        newTop = originalPosition.offsetY > 0 ? parseInt(position.top * height/originalPosition.offsetY) : position.top;
      }
      else{
        position = {left: this.get('pinslayer.annotation.CrosshairXCoord'), top: this.get('pinslayer.annotation.CrosshairYCoord')};
        originalPosition = {offsetX: this.get('pinslayer.annotation.OriginalVideoWidth'), offsetY: this.get('pinslayer.annotation.OriginalVideoHeight')};
        if(window.AppkitENV.selectedHtmlPageId === "0"){
          newLeft = this.get('htmlMinMaxValues')[1] + position.left;
          newTop = this.get('htmlMinMaxValues')[0] + position.top;
        } else {
          newLeft = position.left;
          newTop = position.top;
        }
    }

      Ember.$(element).css({
        left:  newLeft,
        top: newTop
      });
    }
  }
});
export default PlusDivIcon;
