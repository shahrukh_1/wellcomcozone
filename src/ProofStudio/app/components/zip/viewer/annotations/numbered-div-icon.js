import Ember from "ember";

var NumberedDivIcon = Ember.Object.extend({
  className: 'leaflet-div-icon leaflet-marker-icon',
  annotation: null,
  element: null,
  pinslayer : null,
  width: 0,
  height: 0,
  htmlMinMaxValues: 0,

  createIcon: function () {
    var number = this.get('annotation.Nr');

    var div = document.createElement('div');
    div.setAttribute('class', this.className);
    div.setAttribute('annotationId', this.get('annotation.id'));
    var numdiv = document.createElement('div');
    numdiv.setAttribute ( "class", "number" );
    numdiv.innerHTML = number || '';
    div.appendChild ( numdiv );

    return div;
  },

  addTo: function(parentContainer){
    var self = this;
    var element = this.createIcon();
    var position;
    if(window.AppkitENV.selectedHtmlPageId === "0") {
     position = {left: this.get('annotation.CrosshairXCoord' + this.get('htmlMinMaxValues')[1]), top: this.get('annotation.CrosshairYCoord') + this.get('htmlMinMaxValues')[0]};
    } else {
     position = {left: this.get('annotation.CrosshairXCoord'), top: this.get('annotation.CrosshairYCoord')};
    }
    Ember.$(element).css({
      left: position.left,
      top: position.top
    });

    Ember.$(element).on('click', function(ev){
      var pinsLayer = self.get('pinslayer');
      if (pinsLayer){
        pinsLayer.clickMarker(ev);
      }
    });

    parentContainer.append(element);
    this.set('element', element);
  },

  syncPosition: function(width, height){
    var position = {left: this.get('annotation.CrosshairXCoord'), top: this.get('annotation.CrosshairYCoord')};
    var originalPosition = {offsetX: this.get('annotation.OriginalVideoWidth'), offsetY: this.get('annotation.OriginalVideoHeight')};
    var element = this.get('element');
    var newLeft;
    var newTop;
    if (element){
      if(window.AppkitENV.selectedHtmlPageId === "0"){
        newLeft = this.get('htmlMinMaxValues')[1] + position.left;
        newTop = this.get('htmlMinMaxValues')[0] + position.top;
      } else {
        newLeft = position.left;
        newTop = position.top;
      }
      Ember.$(element).css({
        left:  newLeft,
        top: newTop
      });
    }
  }
});

export default NumberedDivIcon;
