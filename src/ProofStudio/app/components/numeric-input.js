import Ember from "ember";

var NumericInput = Ember.TextField.extend({
    didInsertElement: function(){
        var self = this;
        var input = this.$();
        input.keypress(function (e) {
            if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
              return false;
            }
        });
        input.change(function(){
            self.sendAction('valueChanged');
        });
    }
});
export default NumericInput;
