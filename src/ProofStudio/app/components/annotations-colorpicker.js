import Ember from "ember";
import ColorPicker from 'appkit/components/color-picker';

var AnnotationsColorPicker = ColorPicker.extend({
    layoutName:'components/color-picker',

    save: function(hexColor){
        this._super();
        this.sendAction('changeDrawingColor', hexColor);
    }
});
export default AnnotationsColorPicker;
