import DS from "ember-data";

/* global moment */
var DateTransform = DS.DateTransform.extend({
    serialize: function(date) {
        if (date instanceof Date) {

            var pad = function(num) {
                return num < 10 ? "0"+num : ""+num;
            };

            var utcYear = date.getUTCFullYear(),
                utcMonth = date.getUTCMonth(),
                utcDayOfMonth = date.getUTCDate(),
                utcHours = date.getUTCHours(),
                utcMinutes = date.getUTCMinutes(),
                utcSeconds = date.getUTCSeconds();


            var dayOfMonth = pad(utcDayOfMonth);

//            return dayOfWeek + ", " + dayOfMonth + " " + month + " " + utcYear + " " +
//                pad(utcHours) + ":" + pad(utcMinutes) + ":" + pad(utcSeconds) + " GMT";
            return utcYear + '-' + pad(utcMonth) + '-' + dayOfMonth + " " + 
                pad(utcHours) + ":" + pad(utcMinutes) + ":" + pad(utcSeconds);
        } else {
            return null;
        }
    },
    
    deserialize: function(dateString) {
        return moment(dateString, 'YYYY-MM-DD HH:mm:ss');
    }
});
export default DateTransform;