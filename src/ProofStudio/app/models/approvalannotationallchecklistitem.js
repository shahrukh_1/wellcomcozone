import DS from "ember-data";
var attr = DS.attr;

var ApprovalannotationAllChecklistitem = DS.Model.extend({
    ID: attr('number'),
    ChecklistItemID: attr('number'),
    ChecklistItemName: attr('string'),
    ChecklistItemValue: attr('string'),
    Creator: attr('string'),
    CreatedDate: attr('string')
   });
export default ApprovalannotationAllChecklistitem;