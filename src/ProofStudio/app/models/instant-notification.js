import DS from "ember-data";
var attr = DS.attr;

var InstantNotification = DS.Model.extend({
	Creator: DS.belongsTo('internal-collaborator'),
	ExternalCreator: DS.belongsTo('external-collaborator'),
	Version: DS.belongsTo('version'),
	EntityType: attr('number')
});
export default InstantNotification;
