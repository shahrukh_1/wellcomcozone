import DS from "ember-data";
var attr = DS.attr;

var AccountSetting = DS.Model.extend({
   ProofStudioBackgroundColor: attr('string'),
   AccountId: attr("number")
});
export default AccountSetting;
