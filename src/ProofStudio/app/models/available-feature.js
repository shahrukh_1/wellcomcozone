import DS from "ember-data";
var attr = DS.attr;

var AvailableFeature = DS.Model.extend({
    Title: attr('string')
});
export default AvailableFeature;
