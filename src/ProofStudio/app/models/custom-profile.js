import DS from "ember-data";
var attr = DS.attr;

var CustomProfile = DS.Model.extend({
    SimulationProfileId: attr('number'),
    EmbeddedProfileId: attr('number'),
    CustomProfileId: attr('number'),

    //FileName: attr('string'),
    //Url: attr('string', {defaultValue: ''}),
    //Size: attr('number', {defaultValue: 0}),
    //IsEmbedded: attr('boolean', {defaultValue: false}),
    //DefaultFileName: attr('string'),
    DisplayName: attr('string'),//k
    IsRGB: attr('boolean', {defaultValue: false}),
    //DefaultProfile: attr('boolean', {defaultValue: false}),
    temp: attr('boolean', {defaultValue: false}),

    VersionId: attr('number'),
    MediaCategory: attr('string'),//k
    MeasurementCondition: attr('string'),//k
    L: attr('number'),
    a: attr('number'),
    b: attr('number')
});
export default CustomProfile;
