import DS from "ember-data";
var attr = DS.attr;

var Page = DS.Model.extend({
    PageNumber: attr('number'),
    Approval: DS.belongsTo('version'),
    PageFolder: attr('string'),
    SmallThumbnailPath: attr('string'),
    LargeThumbnailPath: attr('string'),
    SmallThumbnailHeight: attr('number'),
    LargeThumbnailHeight: attr('number'),
    SmallThumbnailWidth: attr('number'),
    LargeThumbnailWidth: attr('number'),
    OriginalImageWidth: attr('number'),
    OriginalImageHeight: attr('number'),
    OutputImageWidth: attr('number'),
    OutputImageHeight: attr('number'),
    DPI: attr('number'),
    ApprovalSeparationPlates: DS.hasMany('approval-separation-plate', {async: true}),
    Annotations: DS.hasMany('annotation', {inverse: 'Page', async: true}),
    ApprovalChecklists: DS.hasMany('approval-checklist',{async: true}),
    TranscodingJobId: attr('string'),
    PrevPage: attr('number'),
    NextPage: attr('number'),
    EmbeddedProfile: attr('string'),
    HTMLFilePath: attr('string')
});
export default Page;
