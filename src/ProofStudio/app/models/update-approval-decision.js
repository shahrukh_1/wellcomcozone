import DS from "ember-data";
var attr = DS.attr;

var UpdateApprovalDecision = DS.Model.extend({
  Decision: attr('number')
});
export default UpdateApprovalDecision;
