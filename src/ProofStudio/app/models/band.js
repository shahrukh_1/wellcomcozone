import DS from "ember-data";
var attr = DS.attr;

var Band = DS.Model.extend({
    Page: DS.belongsTo('page'),
    FileName: attr('string'),
    Url : attr('string'),
    Size: attr('number')
});
export default Band;
