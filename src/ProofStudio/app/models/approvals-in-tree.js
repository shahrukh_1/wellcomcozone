import DS from "ember-data";
var attr = DS.attr;

var Version = DS.Model.extend({
    TreeFolderId: attr('number'),
    Name: attr('string'),
    Thumbnail: attr('string'),
    Owner: attr('string')
});
export default Version;
