import DS from "ember-data";
var attr = DS.attr;

var ApprovalChecklist = DS.Model.extend({
  ID: attr('number'),
  Id: attr('number'),
  Item: attr('string'),
  IsItemChecked: attr('boolean'),
  ItemValue: attr('string')
   });
export default ApprovalChecklist;