import DS from "ember-data";
var attr = DS.attr;

var ApprovalAnnotationMarkup = DS.Model.extend({
    ApprovalAnnotation: DS.belongsTo('annotation'),
    BackgroundColor: attr('string'),
    Color: attr('string'),
    Rotation: attr('number'),
    ScaleX: attr('number'),
    ScaleY: attr('number'),
    Shapes: DS.hasMany('shape', {embedded: 'always'}),
    Size: attr('number'),
    X: attr('number'),
    Y: attr('number')
});
export default ApprovalAnnotationMarkup;
