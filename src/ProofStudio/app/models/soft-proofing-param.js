import DS from "ember-data";
var attr = DS.attr;

var softProofingParams = DS.Model.extend({
    SimulationProfileId: attr('number'),
    EmbeddedProfileId: attr('number'),
    CustomProfileId: attr('number'),
    PaperTintId: attr('number'),

    ActiveChannels: attr('string'),


    DefaultSession: attr('boolean'),
    //OutputProfileName: attr('string'),
    OutputRgbProfileName: attr('string'),
    SessionGuid: attr('string'),

    //SimulatePaperTint: attr('boolean'),

    Version: DS.belongsTo('version'),
    HighResolution: attr('boolean', {defaultValue: false}),
    EnableHighResolution: attr('boolean', {defaultValue: false}),

    isClone: attr('boolean', {defaultValue: false}),
    HighResolutionInProgress: attr('boolean', {defaultValue: false})
});
export default softProofingParams;
