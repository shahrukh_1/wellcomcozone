import DS from "ember-data";
var attr = DS.attr;

var TextHighlightingWord = DS.Model.extend({
  x: attr('number'),
  y: attr('number'),
  width: attr('number'),
  height: attr('number'),
  letters: attr('string'),
  alpha: attr('number'),
  startIndex: attr('number'),
  endIndex: attr('number'),
  pageWidth: attr('number'),
  pageHeight: attr('number'),
  pageId: attr('number')
});
export default TextHighlightingWord;
