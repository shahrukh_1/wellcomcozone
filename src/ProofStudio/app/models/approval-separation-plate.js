import DS from "ember-data";
var attr = DS.attr;

var ApprovalSeparationPlate = DS.Model.extend({
    Page: DS.belongsTo('page'),
    Name: attr('string'),
    ChannelIndex: attr('number'),
    RGBHex: attr('string')
});
export default ApprovalSeparationPlate;
