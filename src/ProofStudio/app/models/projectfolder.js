import DS from "ember-data";
var attr = DS.attr;

var ProjectFolder = DS.Model.extend({
    ID: attr('number'),
    ProjectFolderName: attr('string'),
    ProjectFolderApprovals: DS.hasMany('projectfolderapproval', {async: true}),
   });
export default ProjectFolder;