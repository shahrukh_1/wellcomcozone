import DS from "ember-data";
var attr = DS.attr;

var Shape = DS.Model.extend({
    FXG: attr('string'),
    IsCustom: attr('boolean'),
    SVG: attr('string'),

    ApprovalAnnotation: DS.belongsTo('annotation', {async: true, inverse: 'shapes'}),
    BackgroundColor: attr('string'),
    Color: attr('string'),
    Rotation: attr('number'),
    ScaleX: attr('number'),
    ScaleY: attr('number'),
    Size: attr('number'),
    X: attr('number'),
    Y: attr('number'),
    HighlighType: attr('number')
});
export default Shape;
