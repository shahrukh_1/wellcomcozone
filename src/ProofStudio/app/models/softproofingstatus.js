import DS from "ember-data";
var attr = DS.attr;

var SoftProofingStatus = DS.Model.extend({
    Enabled: attr('boolean'),
    Level: attr('number'),
    MonitorName: attr('string'),
    DisplayWidth: attr('number'),
    DisplayHeight: attr('number')
});
export default SoftProofingStatus;
