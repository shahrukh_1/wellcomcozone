import DS from "ember-data";
var attr = DS.attr;

var Collaborator = DS.Model.extend({
    GivenName: attr('string'),
    FamilyName: attr('string'),
    AssignedDate: attr('date'),
    CollaboratorAvatar: attr('string'),
    CollaboratorRole: attr('number'),
    UserColor: attr('string'),
    IsGroup: attr('boolean'),
    Decision: DS.belongsTo('decision'),
    IsAcessRevoked: attr('boolean'),
    SubstituteUserName: attr('string'),
    UserStatus: attr('boolean'),
});
export default Collaborator;
