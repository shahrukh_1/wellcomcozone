import DS from "ember-data";
var attr = DS.attr;

var ApprovalannotationChecklistitem = DS.Model.extend({
  ID: attr('number'),
  Id: attr('number'),
  Item: attr('string'),
  IsItemChecked: attr('boolean'),
  ItemValue: attr('string'),
  TotalChanges: attr('number'),
  ShowNoOfChanges: attr('boolean')
   });
export default ApprovalannotationChecklistitem;