import DS from "ember-data";
var attr = DS.attr;

var Annotation = DS.Model.extend({
    Status: DS.belongsTo('annotation-status'),
    Parent: DS.belongsTo('annotation', {inverse: null}),
    Page: DS.belongsTo('page', { inverse: 'Annotations', async: true }),
    OrderNumber: attr('number'),
    PageNumber: attr('number'),
    Creator: DS.belongsTo('internal-collaborator'),
    Modifier: DS.belongsTo('internal-collaborator'),
    ExternalCreator: DS.belongsTo('external-collaborator'),
    ExternalModifier: DS.belongsTo('external-collaborator'),
    ModifiedDate: attr('date'),
    Comment: attr('string'),
    base64: attr('string'),
    ReferenceFilePath: attr('string'),
    AnnotatedDate: attr('date'),
    CommentType: attr('number'),
    StartIndex: attr('number'),
    EndIndex: attr('number'),
    HighlightType: attr('number'), // TODO implement HighlightType
    CrosshairXCoord: attr('number'),
    CrosshairYCoord: attr('number'),

    OriginalVideoWidth: attr('number'),
    OriginalVideoHeight: attr('number'),

    CrosshairXCoordScaledForHighRes: Ember.computed('CrosshairXCoord', function() {
      return this.get('CrosshairXCoord') * this.get('ScaledImage');
    }),
    CrosshairYCoordScaledForHighRes: Ember.computed('CrosshairYCoord', function() {
      return this.get('CrosshairYCoord') * this.get('ScaledImage');
    }),
    SelectedUserDetails: attr('string'),
    HighlightData: attr('string'),
    IsPrivate: attr('boolean'),
    TimeFrame: attr('number'),
    Checklist: attr('string'),
    ApprovalAnnotationPrivateCollaborators: attr(), // TODO implement private collaborators
    shapes: DS.hasMany('custom-shape', {embedded: 'always', async: true }),
    AnnotationNr: attr('number'),
    AnnotationAttachments: DS.hasMany('annotation-attachment', {embedded: 'always'}),
    AnnotationAllchecklistitems:DS.hasMany('annotation-allchecklistitem', {embedded: 'always'}),
    ApprovalannotationChecklistitems: DS.hasMany('approvalannotation-checklistitem', {embedded: 'always'}),
    Version: attr('string'),
    SoftProofingLevel: attr('number'),
    PhaseName: attr('string'),
    Phase: attr('number'),
    isShown: attr('boolean'),
    ChecklistItemUpdate: attr('boolean'),
    IsDifferentPhase: attr('boolean'),
    IsPrivateAnnotation: attr('boolean'),
    IsVideo: attr('boolean'),
    ShowVideoReport: attr('boolean'),
    IsSamePage: attr('boolean'),

    SPPsessionGUID: attr('string'),// used on save
    CalibrationStatus: attr('number'),
    ViewingCondition: DS.belongsTo('viewing-condition'),// loaded annotation load annotation

    AnnotatedOnImageWidth: attr('number'),
    //AnnotatedOnImageHeight: attr('number'),

    ScaledImage: Ember.computed('AnnotatedOnImageWidth', 'Page', function() {
      return this.get('Page.OutputImageWidth') / this.get('AnnotatedOnImageWidth');
    }),
});
export default Annotation;
