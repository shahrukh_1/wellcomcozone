import DS from "ember-data";
var attr = DS.attr;

var Approval = DS.Model.extend({
    //CreatedDate: attr('date'),
    Creator: attr('number'),
    //Deadline: attr('date'),
    //Job: attr('number'),
    ErrorMessage: attr('string', {defaultValue: null}),
    //Guid: attr('string'),
    //IsDeleted: attr('boolean'),
    //IsPageSpreads: attr('boolean'),
    //ModifiedDate: attr('date'),
    //Modifier: attr('number'),
    //Parent: attr('number'),
    //SelectedAccessMode: attr('number'),
    Title: attr('string'),
    //VersionNumber: attr('number'),
    //LockProofWhenAllDecisionsMade: attr('boolean'),
    //IsLocked: attr('boolean'),
    //OnlyOneDecisionRequired: attr('boolean'),
    //PrimaryDecisionMaker: attr('number'),
    //AllowDownloadOriginal: attr('boolean'),
    //ApprovalFolder: attr('string'),
    //OriginalFileType: attr('string'),
    //DownloadOriginalFileLink: attr('string'),
    //Owner: attr('number'),
    //Decision: DS.belongsTo('decision'),
    ApprovalType: attr('string'),
    //MovieFilePath: attr('string'),
    Versions: DS.hasMany('version', {async: true}),
    //JobStatus: attr('string'),
    //IsRGBColorSpace: attr('boolean')
});
export default Approval;
