import DS from "ember-data";
var attr = DS.attr;

var softProofingPaperTintMeasurement = DS.Model.extend({
    PaperTint: DS.belongsTo('soft-proofing-papertint'),
    Measurement: attr('string'),
    L: attr('number'),
    a: attr('number'),
    b: attr('number')
});
export default softProofingPaperTintMeasurement;
