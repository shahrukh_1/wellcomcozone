import DS from "ember-data";
var attr = DS.attr;

var InternalCollaboratorLink = DS.Model.extend({
    collaborator_id: DS.belongsTo('internal-collaborator'),
    approval_version: DS.belongsTo('version'),
    approval_role: DS.belongsTo('collaborator-role', {async: true}),
    IsExpired: attr('boolean'),
    ExpireDate: attr('date'),
    Decision: DS.belongsTo('decision'),
    UserGroup: DS.belongsTo('user-group'),
    CanAnnotate: attr('boolean'),
    CanApprove: attr('boolean'),
    IsCompleted: attr('boolean'),
    IsRejected: attr('boolean')
});
export default InternalCollaboratorLink;
