import DS from "ember-data";
var attr = DS.attr;

var ApprovalBasic = DS.Model.extend({
  ApprovalId: attr('number'),
  JobId: attr('number'),
  CurrentPhaseId: attr('number'),
  FirstPageId: attr('number'),
  PDM: attr('number')
});

export default ApprovalBasic;
