import DS from "ember-data";
var attr = DS.attr;

var Generic = DS.Model.extend({
    Key: attr('string'),
    Name: attr('string'),
    Priority: attr('number'),
    JobStatus: attr('string')
});
export default Generic;