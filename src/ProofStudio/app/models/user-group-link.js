import DS from "ember-data";

var UserGroupLink = DS.Model.extend({
  user_group: DS.belongsTo('user-group'),
  decision: DS.belongsTo('decision'),
  approval_version: DS.belongsTo('version')
});
export default UserGroupLink;
