import DS from "ember-data";
var attr = DS.attr;

var ChecklistItemName = DS.Model.extend({
  Id: attr('number'),
  Item: attr('string'),
 });
export default ChecklistItemName;
