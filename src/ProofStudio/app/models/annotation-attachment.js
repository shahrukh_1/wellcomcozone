import DS from "ember-data";
var attr = DS.attr;

var AnnotationAttachment = DS.Model.extend({
    Guid: attr('string'),
    Filename: attr('string'),
    DisplayName: attr('string'),
    Name: attr('string'),
    //isSent: attr('boolean'),
    //inProgress: attr('boolean'),
    ApprovalAnnotation: attr('number')
});
export default AnnotationAttachment;
