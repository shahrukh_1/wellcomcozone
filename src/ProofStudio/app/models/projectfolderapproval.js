import DS from "ember-data";
var attr = DS.attr;

var ProjectFolderApproval = DS.Model.extend({
    ID: attr('number'),
    ApprovalName: attr('string'),
    ApprovalThumbnailPath : attr('string'),
    ApprovalType : attr('string')
   });
export default ProjectFolderApproval;