import DS from "ember-data";
var attr = DS.attr;

var Account = DS.Model.extend({
    //AccountName: attr('string'),
    //AccountTypeName: attr('string'),
    //AccountStatusName: attr('string'),
    //AccountContactEmailAddress: attr('string'),
    //AccountCurrencyName: attr('string'),
    //AccountCurrencyCode: attr('string'),
    AccountDateFormat: attr('string'),
    AccountTimeFormat: attr('string'),
    //AccountTimeZone: attr('string'),
    //AccountThemeName: attr('string'),
    //AccountCustomColor: attr('string'),
    //AccountIsNeedPDFsToBeColorManaged: attr('boolean'),
    //AccountIsNeedProfileManagement: attr('boolean'),
    //AccountIsRemoveAllGMGCollaborateBranding: attr('boolean'),
    //AccountLocaleName: attr('string'),
    ProofStudioBackgroundColor: attr('string'),
    //AccountCYMK_RGBIndicator: attr('boolean'),
    ProofStudioPenWidth: attr('string'),
    AvailableFeatures: DS.hasMany('available-feature'),
});
export default Account;
