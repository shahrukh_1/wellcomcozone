import DS from "ember-data";
var attr = DS.attr;

var CustomProfile = DS.Model.extend({
  SimulationProfileId: attr('number'),
  EmbeddedProfileId: attr('number'),
  CustomProfileId: attr('number'),
  PaperTintId: attr('number')

});
export default CustomProfile;
