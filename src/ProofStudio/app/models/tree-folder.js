import DS from "ember-data";
var attr = DS.attr;

var Version = DS.Model.extend({
    Parent: attr('number'),
    Name: attr('string'),

    Creator: attr('number'),
    Level: attr('number'),
    HasApprovals : attr('number')
});
export default Version;
