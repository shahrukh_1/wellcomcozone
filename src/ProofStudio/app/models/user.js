import DS from "ember-data";
var attr = DS.attr;

var User = DS.Model.extend({
    //UserGivenName: attr('string'),
    //UserFamilyName: attr('string'),
    //UserIsActive: attr('boolean'),
    //UserIsDeleted: attr('boolean'),
    UserPhotoPath: attr('string'),// not used, it might be used as imporovement
    //UserStatusName: attr('string'),
    Username: attr('string'),
    //UserHomeTelephoneNumber: attr('string'),
    //UserEmailAddress: attr('string'),
    //UserMobileTelephoneNumber: attr('string'),
    UserAvatar: attr('string'),
    UserRole: DS.belongsTo('role', {async: true}),
    ApprovalRole: DS.belongsTo('collaborator-role', {async: true}),
    UserLocaleName: attr('string'),
    UserIsExternal: attr('boolean'),
    UserColor: attr('string'),
    //Domain: attr('string'),
    BrandingHeaderLogo: attr('string'),
    CanSeeAllAnnotations: attr('boolean'),
    ShowAnnotationsOfAllPhases: attr('boolean'),
    PrivateAnnotations: attr('boolean')
});
export default User;
