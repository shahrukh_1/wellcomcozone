import DS from "ember-data";
var attr = DS.attr;

var UserGroup = DS.Model.extend({
  Name: attr('string'),
  Decision: DS.belongsTo('decision')
});
export default UserGroup;
