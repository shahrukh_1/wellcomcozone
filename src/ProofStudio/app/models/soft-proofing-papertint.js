import DS from "ember-data";
var attr = DS.attr;

var softProofingPaperTint = DS.Model.extend({
    PaperTintName: attr('string'),
    MediaCategory: attr('string'),
    PaperTintMeasurements: DS.hasMany('soft-proofing-papertint-measurement', {embedded: 'always'})
});
export default softProofingPaperTint;
