import Ember from "ember";
import SliderView from 'appkit/views/utils/sliderview';

var BackgroundView = SliderView.extend({

  minValue: 0,
  maxValue: 100,
  value: 0,
  step: 1,
  firstValChanged: false,
  templateName: 'utils/background-slider',
  classNames: "czn-slider czn-slider-image unselectable",


  didInsertElement: function(){
    this.initialize();
    this.setHandle(this.get('value'));

    Ember.$('#backgroundSliderValue').html(0);

    Ember.$('#mybg').css('opacity', '0');

  },

  change: function (value) {
    this.set('value',value);
    var opacity = value / 100;


    Ember.$('#backgroundSliderValue').html(value);

    Ember.$('#mybg').css('opacity', opacity);

  }
});
export default BackgroundView;
