import Ember from "ember";
import SliderView from 'appkit/views/utils/sliderview';

var AlphaView = SliderView.extend({
    minValue: 0,
    maxValue: 100,
    value: 100,
    step: 1,
    firstValChanged: false,
    templateName: 'utils/alpha',
    classNames: "czn-slider czn-slider-image unselectable",

    didInsertElement: function(){
        this.initialize();
        this.setHandle(this.get('value'));

        var self = this;
        Ember.run.later(function() {
            if (self.get('renderer')==null) return;
            self.get('renderer').send('syncOverlappedVersions');
            self.get('controller').send('setAlphaViewData', self);
            Ember.$('.czn-overlay-view-opacity').css('opacity', '1');
        });
    },

    change: function (value) {
        this.set('value',value);
        var opacity = value / 100;

        Ember.run.later(function() {
            Ember.$('.czn-overlay-view-opacity').css('opacity', opacity);
        });
    }
});
export default AlphaView;
