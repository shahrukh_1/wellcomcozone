import Ember from "ember";

var SliderView = Ember.View.extend({
    classNames: "czn-slider unselectable",
    tagName: "div",
    minValue: 0,
    maxValue:100,
    step: 1,
    templateName: 'utils/sliderview',
    handle: null,
    isElementInserted: false,

    didInsertElement: function(){
        this.initialize();
    },

    setHandle: function(value){
        var maxValue = this.get('maxValue');
        var minValue = this.get('minValue');

        if (value > maxValue) {
          value = maxValue;
        }

        if (value < minValue) {
          value = minValue;
        }

        var handle = this.get('handle') || this.$().find('.czn-slider-handle');
        var badWidth = this.$().width() - handle.width();

        var left = value * badWidth / maxValue;

        handle.css('left', left + 'px');
    },

    initialize: function(){
        var self = this;
        this.set('isElementInserted', true);

        var handle =this.$().find('.czn-slider-handle');
        handle.draggable({
            axis: "x",
            containment: "parent",
            stop: function(ev){
                var left = parseInt(Ember.$(ev.target).css('left').replace('px',''));
                if (left < 10) {
                  left = left - parseInt(handle.width()) - 2;
                }
                var procent = (left + parseInt(handle.width()) + 2) * 100 / parseInt(self.$().width());
                var value = self.get('minValue') + ((self.get('maxValue') * procent)/100);

                var roundedValue = self.round(value);
                self.change(roundedValue);
                self.setHandle(roundedValue);
            }
        });

        this.$().click(function(ev){
            self.markerClick(ev);
        });
    },

    markerClick: function(ev){
        var self = this;
        var handle = this.$().find('.czn-slider-handle');
        if (!Ember.$(ev.target).hasClass('czn-marker-handle') && !Ember.$(ev.target).hasClass('markerClass')) {
            var offset = ev.hasOwnProperty('offsetX') && ev.offsetX !== undefined ? ev.offsetX : (ev.clientX - Ember.$(ev.target).offset().left);
            var procent = (offset + parseInt(handle.width() / 2)) * 100 / parseInt(self.$().width());
            var value = self.get('minValue') + ((self.get('maxValue') * procent) / 100);

            if(!this.get('isOrderAsc')) {
              value = self.get('minValue') - (((self.get('minValue') - self.get('maxValue')) * procent) / 100);
            }

            var roundedValue = self.round(value);
            self.setHandle(roundedValue);
            self.change(roundedValue);
        }
    },

    round: function(number) {
        if (this.get('step') >= 1)
        {
            var found = false;
            var returnNumber = 100;

            var currentValue = this.get('minValue'),
              maximumValue = this.get('maxValue');

            while (currentValue <= maximumValue && !found) {
                if (currentValue - this.get('step') / 2 <= number && number < currentValue + this.get('step') / 2) {
                    returnNumber = currentValue;
                    found = true;
                }
                currentValue += this.get('step');
            }
            return returnNumber;
        }
        return number;
    },

    resetCurrentTime: function(){
        var handle = this.get('handle') || this.$().find('.czn-slider-handle');
        handle.css('left', 0 + 'px');
    }
});
export default SliderView;
