import Ember from "ember";
import SliderView from 'appkit/views/utils/sliderview';

var ThresholdView = SliderView.extend({
  minValue: 0,
  maxValue: 100,
  value: 13,
  step: 1,
  firstValChanged: false,
  templateName: 'utils/threshold-slider',
  classNames: "czn-slider czn-slider-image unselectable",


  didInsertElement: function(){
    this.initialize();

    var val = 13 / 100;
    myThreshold=val;

    var handle = this.get('handle') || this.$().find('.czn-slider-handle');

    handle.css('left', 13 + 'px');

    Ember.$('#thresholdSliderValue').html(13);

    var self = this;
    self.get('controller').send('changeDiffColor');

  },

  change: function (value) {
    this.set('value',value);
    var val = value / 100;

    Ember.$('#thresholdSliderValue').html(value);

    var self = this;
    myThreshold=val;
    self.get('controller').send('changeDiffColor');

  }
});
export default ThresholdView;
