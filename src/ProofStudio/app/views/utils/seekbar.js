import Ember from "ember";
import SliderView from 'appkit/views/utils/sliderview';

var SeekBarView = SliderView.extend({
    attributeBindings: ["value"],
    templateName: 'utils/seekbar',
    minValue: 0,
    maxValue: Ember.computed.alias("controller.maxDuration"),
    value: Ember.computed.alias("controller.currentTime"),
    step: 0.5,
    classNames: "czn-slider czn-slider-video unselectable",
    annotations: Ember.computed.alias("controller.annotations"),
    currentSecond: null,


    initialize: function(){
        this._super();
        this.seekbarResetObservable();
    },

    change: function (value) {
        this.get('controller').send('synchronizeWithVideo', value);
    },

    currentSecondChanged: function(){
        var value = this.round(this.get('controller.currentTime'));
        this.setHandle(value);
    }.observes('currentSecond'),

    selectedMarkerChanged: function(){
        var timeFrame = this.get('controller.timeFrame');

        this.setHandle(timeFrame);
        this.change(timeFrame);
        this.get('controller').send('pause');
    }.observes('controller.timeFrame'),

    selectedAnnotation: function(){
        var annotations = this.get('annotations');
        var index = this.get('controller.currentAnnotation');

        if (annotations && annotations.length > 0 && index > 0 && index <= annotations.length) {
            var time = (annotations[index - 1].get('TimeFrame') / 1000000);

            this.setHandle(time);
            this.change(time);
            this.get('controller').send('pause');
        }
    }.observes('annotations.@each.selected'),

    currentTimeChanged: function() {
        var value = this.get('controller.currentTime');
        var second = this.preciseRound(value, 1);
        if (this.get('currentSecond') !== second)
        {
            this.set('currentSecond', this.preciseRound(value, 1));
        }
    }.observes('controller.currentTime'),

    setMarker: function(value) {
        var seconds = (value/1000000);
        var handle = this.$().find('.czn-marker-handle');
        var badWidth = this.$().width() - handle.width();
        var left = seconds * badWidth / this.get('maxValue');
        return 'left:' + left + 'px';
    },

    preciseRound: function(value, decimal){
        return Math.round(value * Math.pow(10, decimal)) / Math.pow(10, decimal);
    },

    annotationMarkers: function(){
        var annotations = this.get('annotations'),
            markers = [],
            self = this;
           if(this.get('isElementInserted')) {
               annotations.forEach(function (annotation) {
                 if (annotation.get('isShown') == true) {
                   var timeFrame = annotation.get('TimeFrame');
                   var position = self.setMarker(timeFrame);
                   var exist = false;

                   for (var i = 0; i < markers.length; i++) {
                     if (markers[i].left === position) {
                       exist = true;
                       var nrOfMarkers = markers[i].nr;
                       markers[i].nr = nrOfMarkers + 1;
                       markers[i].timeFrame = timeFrame;
                       markers[i].twoDecimals = markers[i].nr >= 10;
                     }
                   }

                   if (!exist) {
                     var Marker = Ember.Object.create({
                       nr: 1,
                       left: position,
                       timeFrame: timeFrame,
                       twoDecimals: false
                     });
                     markers.pushObject(Marker);
                   }
                 }
               });
           }
        return markers;
    }.property('controller.currentTime','annotations','isElementInserted','maxValue'),

    markerClick: function(ev){
        var self = this;
        var handle =this.$().find('.czn-slider-handle');
        if (!Ember.$(ev.target).hasClass('czn-marker-handle') && !Ember.$(ev.target).hasClass('markerClass')) {
            var offset = ev.hasOwnProperty('offsetX') && ev.offsetX !== undefined ? ev.offsetX : (ev.clientX - Ember.$(ev.target).offset().left);
            var procent = (offset + parseInt(handle.width() / 2)) * 100 / parseInt(self.$().width());
            var value = self.get('minValue') + ((self.get('maxValue') * procent) / 100);
            var roundedValue = self.round(value);
            self.setHandle(roundedValue);
            self.change(roundedValue);
        }
        else if (Ember.$(ev.target).hasClass('markerClass') || Ember.$(ev.target).hasClass('czn-marker-handle')){
            var timeFrame = parseInt(Ember.$(ev.target).attr('timeFrame'));
            this.setHandle(timeFrame);
        }
    },

    seekbarResetObservable: function(){
        this.resetCurrentTime();
    }.observes('controller.model.pages.currentPage.id')

});
export default SeekBarView;
