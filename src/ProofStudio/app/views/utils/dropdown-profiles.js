import Ember from "ember";

var DropdownProfilesView = Ember.View.extend({
    classNameBindings: [':dropdown', ':dropdown-profiles'],
	  profiles: Ember.computed.alias('controller.controllers.navigation/custom-profiles/index.profiles'),
    currentSPP : Ember.computed.alias('controller.controllers.navigation/custom-profiles/index.currentSPP'),
    isClosing: true,
    didInsertElement:function () {
      var self = this;
      Ember.$('.dropdown-profiles').on('hide.bs.dropdown', function (e) {
        if (self.get('isClosing') == false) {
          e.stopPropagation();
          e.preventDefault();
        }
      });
    },
    actions: {
        dropdownToggle: function(){
            this.toggleProperty('isClosing');
        }
    },
    OpenChanged: function () {
      if ((this.get('isClosing') == false) && (Ember.$("#dropdown-for-spp").hasClass('active'))){
        Ember.$("#dropdown-for-spp").removeClass('active');
      }
      if (this.get('isClosing') == false) {
        this.get('controller.controllers.navigation/custom-profiles/index').send('resetTmpSelected');
      }
    }.observes('isClosing')
});
export default DropdownProfilesView;
