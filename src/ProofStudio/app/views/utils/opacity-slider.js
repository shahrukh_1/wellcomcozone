import Ember from "ember";
import SliderView from 'appkit/views/utils/sliderview';

var OpacityView = SliderView.extend({
  minValue: 0,
  maxValue: 100,
  value:100,
  step: 1,
  firstValChanged: false,
  templateName: 'utils/opacity-slider',
  classNames: "czn-slider czn-slider-image unselectable",


  didInsertElement: function(){
    this.initialize();
    var handle = this.get('handle') || this.$().find('.czn-slider-handle');

    handle.css('left', 100 + 'px');

    Ember.$('#opacitySliderValue').html(100);

    Ember.$('#map3').css('opacity', '1');

  },

  change: function (value) {
    this.set('value',value);
    var opacity = value / 100;

    Ember.$('#opacitySliderValue').html(value);

    Ember.$('#map3').css('opacity', opacity);

  }
});
export default OpacityView;
