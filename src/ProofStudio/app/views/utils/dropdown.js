import Ember from "ember";

var DropdownView = Ember.View.extend({
    needs:['navigation/main'],
    versions: Ember.computed.alias('controller.versions'),
    classNameBindings: [':dropdown'],
    isClosing: true,
    didInsertElement:function () {
      var self = this;
      Ember.$('.dropdown').on('hide.bs.dropdown', function (e) {
        if (self.get('isClosing') == false) {
          e.stopPropagation();
          e.preventDefault();
        }
      });
    },

    actions: {
        dropdownToggle: function() {
            this.toggleProperty('isClosing');

            this.get('versions').filterBy('selected', true).forEach(function(version) {
                version.set('selected', false);
            });
        }
    },
});
export default DropdownView;
