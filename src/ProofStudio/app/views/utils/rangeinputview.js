import Ember from "ember";
import SliderView from 'appkit/views/utils/sliderview';

var RangeInputView = SliderView.extend({
    minValue: Ember.computed.alias("renderer.minZoom"),
    maxValue: Ember.computed.alias("renderer.maxZoom"),
    step: 0.1,
    value: null,
    classNames: "czn-slider czn-slider-image czn-slider-zoom unselectable",

    change: function (value) {
        this.get('renderer').send('zoomTo', value);
        this.set('value',value);
    },

    currentZoomChanged: function() {
      var renderer = this.get('renderer');
      if(renderer && renderer.get('isDestroyed') === false) {
        if(this.get('value') !== renderer.get('currentZoom')) {
          var value = renderer.get('currentZoom');
          this.set('value', value);
          this.setHandle(value);
        }
      }
    }.observes('renderer.currentZoom', 'renderer.url', 'renderer.hash'),
});
export default RangeInputView;
