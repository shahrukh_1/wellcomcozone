import Ember from "ember";
import AppConstants from "appkit/config/constants";

var InstantNotificationItemView = Ember.View.extend({
  onInit: function() {
    this.get('instantNotificationsCommunicationService').RegisterOnConfirmedToShowNewTiles(this,'closeOnConfirmedToShowTiles');
  }.on('init'),

  onCleanup: function() {
    this.get('instantNotificationsCommunicationService').UnRegisterOnConfirmedToShowNewTiles(this,'closeOnConfirmedToShowTiles');
  }.on('willDestroyElement'),
  closeOnConfirmedToShowTiles() {
    if (this.get('controller.model.EntityType') == AppConstants.InstantNotificationEntityType.TilesAreReady) {
      this.send('close');
    }
  },

  actions: {
      close: function(item){
        var element = this.$();
        element.toggleClass('zoomOut animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e){
          Ember.$(e.target).removeClass('zoomOut').removeClass('animated').hide();
        });
      }
  },


	onItemShown: function(){
		var element = this.$();
		element.toggleClass('zoomIn animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e){
      Ember.$(e.target).removeClass('zoomIn').removeClass('animated').show();
		});
	}.on('didInsertElement')
});
export default InstantNotificationItemView;
