import Ember from "ember";

var ErrorView = Ember.View.extend({
    classNames: ['czn-error-wrapper'],
    tagName: 'div',
    
    didInsertElement: function() {
        Ember.$('.czn-error').modal();
    }
});
export default ErrorView;
