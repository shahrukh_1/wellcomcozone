import Ember from "ember";

var ApplicationView = Ember.View.extend({
    tagName: 'div',
    classNames: ['czn-application']
});

export default ApplicationView;