import Ember from "ember";

var SeparationsIndexView = Ember.View.extend({
    didInsertElement : function(){
        this._super();
        Ember.run.scheduleOnce('afterRender', this, function(){
            this.$("#czn-separations").draggable();
        });
    }

});
export default SeparationsIndexView;
