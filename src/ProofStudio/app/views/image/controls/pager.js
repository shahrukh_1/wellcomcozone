import Ember from "ember";

var PagerView = Ember.View.extend({
    classNames: ['czn-pager'],
    tagName: 'span'
});
export default PagerView;
