import Ember from "ember";

var AnnotationItem = Ember.View.extend({
    classNameBindings: ['visible:hidden'],
    attributeBindings: ['annotation', 'page'],
    selected: Ember.computed.alias('controller.model.selected'),
    annotation: Ember.computed.alias('controller.model.id'),
    page: Ember.computed.alias('controller.model.Page.id'),
    markerCreated: false,
    visible: true,

    onDidInsertElement: function(){
        //this.createMarker();
    }.on('didInsertElement'),


    beforeDestroyingElement: function(){
        this.deleteMarker();
        this.$().off();
    }.on('willDestroyElement'),

    deleteMarker: function(){
        var marker = this.get('marker');
        if (marker){
            this.get('controller').send('removeMarker', marker);
        }
    }

});
export default AnnotationItem;
