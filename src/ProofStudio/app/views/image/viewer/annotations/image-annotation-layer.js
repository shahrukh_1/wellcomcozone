import Ember from "ember";

var ImageIndexView = Ember.View.extend({

    onDidInsertElement: function(){
       this.initImageIndex();
    }.on('didInsertElement'),

    initImageIndex: function(){
        var controller = this.get('controller'),
            currentElement = this.$();
        if (currentElement) {
            controller.send('initializeImageIndex', controller);
            var plusMarker = currentElement.find(".plus-marker").get(0);
            controller.set('plusMarker', plusMarker);
        }
    },

    initImageIndexObservable: function(){
        this.initImageIndex();
    }.observes('controller.model.approval.currentVersion')
});
export default ImageIndexView;
