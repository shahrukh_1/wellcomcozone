import Ember from "ember";
import AppConstants from "appkit/config/constants";
import CanvasView from 'appkit/controllers/viewer/annotations/canvas-view';
import UserPermissions from 'appkit/routes/mixins/user-permissions';
import DrawMarqueeShape from 'appkit/views/viewer/annotations/draw-shape-marquee';

var ImageCanvasView = Ember.View.extend(UserPermissions, CanvasView, DrawMarqueeShape, {
  jqueryUtilsService: Ember.inject.service('jquery-utils'),
  classNames: ['czn-canvas-viewer-control'],
  tagName: 'div',
  mouseUp: true,
  isDrawRectangleSelected: Ember.computed.alias('controller.controllers.navigation/annotation.isDrawRectangleSelected'),
  drawColor: Ember.computed.alias('controller.controllers.navigation/annotation.drawColor'),
  selectedDrawWidth:  Ember.computed.alias('controller.controllers.navigation/annotation.selectedWidth'),
  isSelectedObjectChanging: false,

  createFabricCanvas: function() {
    var self = this,
      canvasElem = this.$().find('canvas:first').get(0),
      canvas = new fabric.Canvas(canvasElem, { stateful: false, selection: false });

    canvas.isDrawingMode = false;
    this.set('imageFabricCanvas', canvas);
    this.set('marqueeFabricCanvas', canvas);

    canvas.on('touch:drag', function(ev) {// event accepted on tablet and desktop as well

      // mouseUp is used to run addLineMouseDown only when the mouse is pressed or on the first touche
      if ((self.get("mouseUp") == true) && ev && ev.e && (ev.e.clientX || (ev.e.touches && ev.e.touches.length > 0 && ev.e.touches[0].clientX))) {

        self.set("mouseUp", false);

        if (!(ev.e.clientX)) {// it is on tablet
          ev.e.clientX = ev.e.touches[0].clientX;
          ev.e.clientY = ev.e.touches[0].clientY;
          ev.e.button = 0;
        }
        self.addLineMouseDown(ev);

        if (self.get('isDrawRectangleSelected')) {
          self.startDrawMarqueeRectangle(ev.e);
        }

      }

      })
      canvas.on('mouse:down', function(ev) {
        self.addLineMouseDown(ev);
    }).on('mouse:up', function(ev){

      if (!(ev.e.clientX)) {// it is on tablet
        ev.e.clientX = ev.e.changedTouches[0].clientX;
        ev.e.clientY = ev.e.changedTouches[0].clientY;
      }
      if (self.get('isDrawRectangleSelected')) {
        self.startDrawMarqueeRectangle(ev.e);
      }
      if(self.get('mouseUp') == true) {
        self.addShapeOnMouseDown(ev);
        if (self.get('isDrawRectangleSelected')) {
          self.storeMarqueeRectangleOnMouseUp(ev.e);
        }
      }

      self.set("mouseUp",true);
      self.addLineMouseUp(ev);
    }).on('mouse:move', function(ev){
      if (!(ev.e.clientX)) {// it is on tablet
        ev.e.clientX = ev.e.touches[0].clientX;
        ev.e.clientY = ev.e.touches[0].clientY;
      }

      self.addLineMouseMove(ev);

      if (self.get('isDrawRectangleSelected') && (self.get('mouseUp') == false)) {
        self.adjustMarqueeRectangleOnMouseMove(ev.e);
      }
    }).on('path:created', function(e){
      self.set('commentType', AppConstants.CommentTypes.DRAW_COMMENT);
      self.canvasPathCreatedEvent(e);
    }).on('object:selected', function(){
      self.updateDrawingSvg();
      self.canvasUpdate();
    }).on('group:selected', function(){
      self.canvasUpdate();
    }).on('selection:cleared', function() {
      self.canvasUpdate();
    }).on('object:scaling', function(){
      self.set('isSelectedObjectChanging', true);
      self.updateDrawingSvg();
    }).on('object:rotating', function(){
      self.set('isSelectedObjectChanging', true);
      self.updateDrawingSvg();
    }).on('object:modified', function() {
      self.updateDrawingSvg();
    }).on('object:moving', function(e) {
      self.set('isSelectedObjectChanging', true);
      self.updateDrawingSvg();
    });
  },

  onDidInsertElement: function(){
    this.createFabricCanvas();

    this.hide();
    var self = this;

    Ember.$(document).on('keydown', function(e){
      self.onKeyDownHandler(e);
    });
  }.on('didInsertElement'),

  beforeDestroyingElement: function(){
    var canvas = this.get('imageFabricCanvas');
    if(canvas) {
      canvas.dispose();
      canvas = null;
    }
    this.$().off();
  }.on('willDestroyElement'),

  isCompareMode: Ember.computed.alias('controller.controllers.image/controls/image.isCompareMode'),

  initializeView: function(){
    this.get('controller').send('initializeCanvas', this);
  }.observes('controller.model.model.currentPage.id'),

  onKeyDownHandler: function(e){
    var canvas = this.get('imageFabricCanvas'),
      controller = this.get('controller');

    switch (e.keyCode) {
      case 46: // delete
        var activeObject = canvas.getActiveObject();
        if (activeObject) {
          canvas.remove(activeObject);

          //disable "add comment" text area if no shapes were added on the canvas
          var objects = canvas.getObjects();
          if(objects.length === 0){
            Ember.$('#commentArea').attr('disabled', true);
          }
        }
        return false;
      case 27:  //escape
        if (controller) {
          var commentsController = controller.controllerFor('comments/index');
          // if there are comments and the confirmation dialog is not visible than open confirmation dialog
          if (commentsController.get('isUnSavedAnnotation') == true) {
              var closeDialogController = controller.controllerFor('confirm-dialog');
              if (closeDialogController.get('visible') == false) {
                controller.send('showConfirmDialogModal');
              }
          } else {
            controller.send('doneAnnotating');
            this.closeColorPicker();
            this.closeAddLine();
          }
        }

        return false;
    }
  },

  canvasPathCreatedEvent: function(e){
    this.canvasUpdate();
    var path = e.path;

    path.perPixelTargetFind = true;
    path.targetFindTolerance = 0;

    var point = [0, 0];
    var pageId = this.get('controller').get('model.currentPage.id');
    this.get('controller').send('newComment', {Point: point, PageId: pageId, CommentType: AppConstants.CommentTypes.DRAW_COMMENT});
    this.set('commentType', AppConstants.CommentTypes.DRAW_COMMENT);
    this.set('pathMinX', e.path.minX);
    this.set('pathMinY', e.path.minY);
    var currentPointPos = this.canvasToMap(new fabric.Point(e.path.left, e.path.top));
    this.get('controller').send('addMarkerOverlay', [currentPointPos.x, -currentPointPos.y]);
    this.sendMarkupsAndShapesToCommentPopup();
  },

  activateRotateAndScaling: function(path){
    path.hasControls = true;
    path.hasBorders = true;
    path.cornerColor = 'white';
    path.cornerSize =  10;
    path.transparentCorners = false;
    path.lockMovementX = false;
    path.lockMovementY = false;
    path.hasRotatingPoint = true;
  },

  canvasUpdate: function(){
    var canvas = this.get('imageFabricCanvas');
    if (canvas){
      canvas.renderAll();
    }
  },

  updateDrawingSvg: function() {
    var timer = this.get('timer'),
        self = this;
    if(timer)
    {
      Ember.run.cancel(timer);
      timer = null;
    }

    timer = Ember.run.later(function() {
      self.set('commentType', AppConstants.CommentTypes.DRAW_COMMENT);
      self.sendMarkupsAndShapesToCommentPopup();
      self.set('timer', null);
    }, 300);

    this.set('timer', timer);
  },

  syncCanvasPosition: function(newSize){
    if(typeof newSize != 'undefined') {
      var canvas = this.get('imageFabricCanvas');

      if (canvas) { // for open-layers
        var newCanvasWidth = newSize[0];//$('.ol-unselectable').width();
        var newCanvasHeight = newSize[1];//$('.ol-unselectable').height();
        var mapMinZoomSize = this.get('controller.model.mapMinZoomSize');
        var offset = this.get('canvasOffset');

        //update fabric
        canvas.setWidth(newCanvasWidth);
        canvas.setHeight(newCanvasHeight);
        canvas.calcOffset();

        var newOffsetX = -(newCanvasWidth - mapMinZoomSize.width) / 2;
        var newOffsetY = -(newCanvasHeight - mapMinZoomSize.height) / 2;

        var diffX = newOffsetX - offset[0];
        var diffY = newOffsetY - offset[1];

        this.set('canvasOffset', [newOffsetX, newOffsetY]);

        // Move canvas viewport to match map movement
        var moveTransform = [1, 0, 0, 1, -diffX, -diffY];

        var resultTransform = fabric.util.multiplyTransformMatrices(moveTransform, canvas.viewportTransform);

        canvas.setViewportTransform(resultTransform);

        this.set('globalOffsetX', this.get('globalOffsetX') - diffX);
        this.set('globalOffsetY', this.get('globalOffsetY') - diffY);
      }
      else if (canvas) { // for video
        var mainWindow = Ember.$("#main");
        canvas.setWidth(mainWindow.width());
        canvas.setHeight(mainWindow.height());
      }
    }
  },

  initCanvasPosition: function(mapInfo){
    var canvas = this.get('imageFabricCanvas');

    if (canvas){ // for open-layers
      //reset viewport transform
      canvas.setViewportTransform([1, 0, 0, 1, 0, 0]);
      canvas.setWidth(mapInfo.width);//1050
      canvas.setHeight(mapInfo.height); //815
      this.set('previousCenter', mapInfo.center);
      this.set('canvasOffset', mapInfo.offset);
      this.set('initialCanvasOffset', new fabric.Point(mapInfo.offset[0], mapInfo.offset[1]));
      this.set('initialScale', mapInfo.scale);
      this.set('globalOffsetX', 0);
      this.set('globalOffsetY', 0);
      this.set('globalScaleFactor', 1);
      this.set('previousResolution', 1 / mapInfo.scale);
      this.hide();
    }
    else if (canvas) { // for video
      var mainWindow = Ember.$("#main");
      canvas.setWidth(mainWindow.width());
      canvas.setHeight(mainWindow.height());
    }
  },

  show: function(){
    this.$().show();
    Ember.$(this.getPathForSelectElement(".ol-overlay-container .canvas-container")).show();
  },

  hide: function(){
    var elem = this.$();
    if(elem) {
      elem.hide();
      this.clearCanvas();
      Ember.$(this.getPathForSelectElement(".ol-overlay-container .canvas-container")).hide();
    }
  },

  disableMapEvents: function(map, leaveScrollEnabled){
    var annotationId = this.get('annotation.id');
    // if annotationId is not null then return from this method
    if (annotationId){
      return;
    }

    if (map) {
      map.dragging.disable();
      if (leaveScrollEnabled) {
        map.scrollWheelZoom.enable();
      }
      else{
        map.scrollWheelZoom.disable();
      }
      map.boxZoom.disable();
      map.keyboard.disable();
      map.touchZoom.disable();
      map.doubleClickZoom.disable();
    }
  },

  enableMapEvents: function(map){
    var annotationId = this.get('annotation.id');
    // if annotationId is not null then return from this method
    if (annotationId){
      return;
    }

    if (map) {
      map.dragging.enable();
      map.scrollWheelZoom.enable();
      map.boxZoom.enable();
      map.keyboard.enable();
      map.touchZoom.enable();
      map.doubleClickZoom.enable();
    }
  },

  onDrawColorChange: function(){
    var canvas = this.get('imageFabricCanvas');
    this.drawColorChanged(canvas);
  }.observes('controller.controllers.navigation/annotation.drawColor'),

  getScale: function(){
    return this.get('controller.model.model.mapScale');
  },

  updateRotation: function(rotation) {
    var canvas = this.get('imageFabricCanvas'),
      globalScaleFactor = this.get('globalScaleFactor');

    // Rotate canvas content with map rotation
    rotation = -rotation;
    var moveToCenterTransform = [1, 0, 0, 1, canvas.getCenter().left, canvas.getCenter().top];
    var rotateTransform = [Math.cos(rotation), -Math.sin(rotation), Math.sin(rotation), Math.cos(rotation), 0, 0];
    var scaleTransform = [globalScaleFactor, 0, 0, globalScaleFactor, 0, 0];
    var moveBackTransform = [1, 0, 0, 1, -canvas.getCenter().left, -canvas.getCenter().top];
    var offsetTransform = [1, 0, 0, 1, this.get('globalOffsetX'), this.get('globalOffsetY')];

    var r1 = fabric.util.multiplyTransformMatrices(moveToCenterTransform, rotateTransform);
    var r2 = fabric.util.multiplyTransformMatrices(r1, scaleTransform);
    var r3 = fabric.util.multiplyTransformMatrices(r2, moveBackTransform);
    var r4 = fabric.util.multiplyTransformMatrices(r3, offsetTransform);

    canvas.setViewportTransform(r4);
  },

  updateScale: function(resolution) {
    var canvas = this.get('imageFabricCanvas'),
      previousResolution = this.get('previousResolution');
    var factor = previousResolution / resolution;
    // Scale canvas viewport transform
    var moveToCenterTransform = [1, 0, 0, 1, canvas.getCenter().left, canvas.getCenter().top];
    var scaleTransform = [factor, 0, 0, factor, 0, 0];
    var moveBackTransform = [1, 0, 0, 1, -canvas.getCenter().left, -canvas.getCenter().top];

    var r1 = fabric.util.multiplyTransformMatrices(moveToCenterTransform, scaleTransform);
    var r2 =  fabric.util.multiplyTransformMatrices(r1, moveBackTransform);
    var r3 = fabric.util.multiplyTransformMatrices(r2, canvas.viewportTransform);
    canvas.setViewportTransform(r3);

    this.set('previousResolution', resolution);
    this.set('globalScaleFactor', this.get('globalScaleFactor') * factor);
  },

  updateCenter: function(currentCenter) {
    var mapMinZoomSize = this.get('controller.model.mapMinZoomSize'),
      imgSize =  this.get('controller.size'),
      canvas = this.get('imageFabricCanvas');

    var previousCenter = this.get('previousCenter');
    var scaleFactorX = imgSize.width / mapMinZoomSize.width;
    var scaleFactorY = imgSize.height / mapMinZoomSize.height;

    // Move canvas viewport with map
    var offsetX = (previousCenter[0] - currentCenter[0])/scaleFactorX;
    var offsetY = -(previousCenter[1] - currentCenter[1])/scaleFactorY;
    this.set('previousCenter', currentCenter);

    var moveTransform = [1, 0, 0, 1, offsetX, offsetY];
    var resultTransform = fabric.util.multiplyTransformMatrices(canvas.viewportTransform, moveTransform);

    canvas.setViewportTransform(resultTransform);

    this.set('globalOffsetX', this.get('globalOffsetX') + offsetX);
    this.set('globalOffsetY', this.get('globalOffsetY') + offsetY);

    this.get('controller.setMaskFromPixelFromCoordinates')(this);
  },

  enableFabric: function() {
    this.show();
    this.stopOverlayToMove();
  },

  stopOverlayToMove: function() {
    Ember.$(".ol-overlay-container")[0].attributes.style.nodeValue = "position: absolute; left: 0px; top: 0px;";
    Ember.$(this.getPathForSelectElement(".ol-overlay-container .canvas-container")).parent().appendTo(this.getPathForSelectElement(".ol-overlaycontainer-stopevent"));
  },

  disableFabric: function() {
    var canvas = this.get('imageFabricCanvas');
    this.stopFabricEvents();
    this.hide();
  },

  stopFabricEvents: function() {
    var canvas = this.get('imageFabricCanvas');

    if(canvas){
      canvas.isDrawingMode = false;
      canvas.selection = false;
      canvas.forEachObject(function(o) {
        o.selectable = false;
      });

      Ember.$(this.getPathForSelectElement(".ol-overlay-container .canvas-container")).parent().appendTo(this.getPathForSelectElement(".ol-overlaycontainer"));
    }
  },

  redraw: function (annotationId) {
    this.show();

    var canvas = this.get('imageFabricCanvas');
    var annotationId = annotationId || this.get('annotation.id');
    var canvasOffset = this.get('initialCanvasOffset');
    var angle = this.get('controller.model.model.mapPosition.angle');
    var singleAnnotationView = true;
    if (canvas){
      canvas.clear();
      canvas.isDrawingMode = false;
    }
    if (annotationId && (singleAnnotationView === true)) {
      var scale = this.get('initialScale');
      var currentAnn = this.get('controller').store.all('annotation').filter(function(ann){
        return ann.get('id') === annotationId;
      });
      var opacity = currentAnn.get('firstObject.CommentType') == AppConstants.CommentTypes.TEXT_COMMENT ? 0.3 : 1;

      var shapes = this.get('controller').store.all('custom-shape').filter(function(custom_shape){
        return custom_shape.get('ApprovalAnnotation.id') === annotationId;
      });

      var scaledImage = currentAnn.get('firstObject.ScaledImage');
      shapes.forEach(function (shape, markerIndex) {
        var svg = shape.get('SVG');

        fabric.loadSVGFromString(svg, function (objects, options) {
          var loadedObject = fabric.util.groupSVGElements(objects, options);

          if (loadedObject) {
            loadedObject.set({
              lockMovementX: true,
              lockMovementY: true,
              hasRotatingPoint: false,
              hasControls: false,
              hasBorders: false,
              selectable: false,
              scaleX: scale * scaledImage,
              scaleY: scale * scaledImage,
              originX: 'left',
              originY: 'top',
              left: (-canvasOffset.x) ,
              top: (-canvasOffset.y),
              opacity: opacity
              //angle: 0
            }).setCoords();
            canvas.add(loadedObject).renderAll();
          }
        });
      });
    }
  },

  addShapeOnMouseDown: function (ev) {
    var canvas = this.get('imageFabricCanvas');
    var activeObject = canvas.getActiveObject();
    var annotationController = this.get('controller.controllers.navigation/annotation');
    if (annotationController.get('isDrawShapeSelected') == true &&
      !activeObject) {
        var shape = annotationController.get('shape');
        var map = this.get('controller.approval.map');
        this.addShapeToCanvas(shape, map, ev.e);
    }
  },

  onDrawingLineChanged: function(){
    var canvas = this.get('imageFabricCanvas');
    this.drawingLineChanged(canvas);
  }.observes('controller.controllers.navigation/annotation.isDrawLineSelected', 'controller.controllers.navigation/annotation.isMoveShapeSelected'),

  addShapeToCanvas: function(shape, map, mouseEventDetails){
      var self = this,
      mapPosition = this.get("controller.model.model.mapPosition"),
      canvas = this.get('imageFabricCanvas');

      if (map){
        this.set('map', map);
      }

      this.set('annotation', null);

      if (canvas){
        var clickedPoint = canvas.getPointer(mouseEventDetails);
        var svg = shape.get('SVG');
        var shapeColor = this.get("controller.controllers.navigation/annotation").get('drawColor');

        Ember.run.later(function() {
          //extract path from svg and create fabric path object
          var paths = [];
          //in case there are more than 2 paths in the svg group them
          var group = [];
          var svgElem = Ember.$(svg);
          var svgOptions = {
            width: parseInt(svgElem.attr('width')),
            height: parseInt(svgElem.attr('height'))
          };
          //get viewbox attribute from svg element(strange jquery selector is not working)
          for(var i = 0; i < svgElem[0].attributes.length; i++) {
            if(svgElem[0].attributes[i].nodeName == 'viewBox')
            {
              svgOptions.viewBox = svgElem[0].attributes[i].value.split(" ");
              break;
            }
          }

          svgElem.find('path').each(function(index, path) {
            path = Ember.$(path);
            var pathString = path.attr('d'),
              fabricPath = new fabric.Path(pathString);
            paths.push(fabricPath);
          });

            paths.forEach(function(loadedObject) {
              loadedObject = self.setSVGObject(loadedObject, shapeColor, svgOptions);

              canvas.draggable = true;
              //check if single path or should create group
              if (paths.length > 1) {
                group.push(loadedObject);
              }
              else {
                loadedObject.set({
                  left: clickedPoint.x - loadedObject.getWidth() / 2,
                  top: clickedPoint.y - loadedObject.getHeight() / 2,
                  angle: 0
                }).setCoords();

                canvas.add(loadedObject);
                canvas.setActiveObject(loadedObject);
              }
            });

          //in case group should be created
          if(group.length > 0) {
            var offsetY = 15;
            for(var i=1; i < group.length; i++){
              group[i].set('top', group[i].get('top') - offsetY);
              offsetY += 15;
            }
            var fabricGroup = new fabric.Group(group, {left: clickedPoint.x, top: clickedPoint.y, angle: -1 * mapPosition.angle, active: true});
            canvas.add(fabricGroup);
            self.sendMarkupsAndShapesToCommentPopup();
          }
        });
      }

      this.get('controller').send('newComment', {Point: clickedPoint, PageId: self.get('controller').get('model.currentPage.id'), CommentType: AppConstants.CommentTypes.DRAW_COMMENT});
  },

  setSVGObject: function(loadedObject, shapeColor, svgOptions){
    loadedObject.set({
      cornerColor: 'white',
      cornerSize: 10,
      transparentCorners: false,
      lockMovementX: false,
      lockMovementY: false,
      hasRotatingPoint: true,
      fill: shapeColor,
      isCustomShape: true,
      scaleX: svgOptions.width / parseInt(svgOptions.viewBox[2]),
      scaleY: svgOptions.height / parseInt(svgOptions.viewBox[3]),
      originX: "left",
      originY: "top"
    }).setCoords();

    return loadedObject;
  },

  clearCanvas: function(){
    var canvas = this.get('imageFabricCanvas');
    if (canvas){
        canvas.getContext().clearRect(0,0,canvas.getWidth(), canvas.getHeight());
        canvas.clear().renderAll();
        this.set('startX', null);
        this.set('startY', null);
    }
  },

  clearSelection: function(){
    var canvas = this.get('imageFabricCanvas');
    if (canvas){
      canvas.discardActiveObject();
      canvas.discardActiveGroup();
    }
  },

  sendMarkupsAndShapesToCommentPopup: function(){

      var annotationId = this.get('annotation.id');
      // if annotationId is not null then return from this method
      if (annotationId){
          return;
      }
      var self = this,
          documentScale = this.getScale(),
          mapPosition = this.get('controller.model.model.mapPosition');
      if (!mapPosition) {
        return;
      }
      var mapLeft = -1 * mapPosition.left,
          mapTop = -1 * mapPosition.top,
          angle = -1 * mapPosition.angle,
          previousScaleFactorX = this.get('globalScaleFactor'),
          previousScaleFactorY = this.get('globalScaleFactor');

      Ember.run.later(function(){
          Ember.$('#commentArea').attr('disabled', false);
          var fabricCanvas = self.get('imageFabricCanvas');

        var width = mapPosition.width, //self.$("canvas").width(), // current video width
              height = mapPosition.height, //self.$("canvas").height(), //current video height
              fullWidth = width / documentScale, //original video width
              fullHeight = height / documentScale;//original video height

          if(fullWidth === 0 || fullHeight === 0){
              throw new Error("Original video width or height is 0!");
          }

          var objects = [],
              scale = [],
              positions =[];
          fabricCanvas.forEachObject(function(obj){
              //calculate top-left corner of the svg relative to the original video size
              var center = obj.getCenterPoint();
              positions.pushObject({x:center.x,y:center.y});

              //calculate scale of the svg
              var x = obj.scaleX * fullWidth / width;
              var y = obj.scaleY * fullHeight / height;
              scale.pushObject({scaleX: x, scaleY: y});

              previousScaleFactorX *= obj.scaleX;
              previousScaleFactorY *= obj.scaleY;

              var objectSvg = obj.toSVG();
              var svg = AppConstants.SVGFormat.replace(/{width}/g, fullWidth)
                                              .replace(/{height}/g, fullHeight)
                                              .replace(/{translate}/g, "scale(" + (1/documentScale) + " " + (1/documentScale) + ") rotate(" + angle  + ") translate(" + mapLeft + "," + mapTop +  ")")
                                              .replace(/{svg}/g, objectSvg)
                                              .replace(/stroke-dasharray: ;/g, '').replace("scale(0 0)", "rotate(" + mapPosition.angle  + ") scale(" + previousScaleFactorX + " " + previousScaleFactorY + ")");
              objects.pushObject(svg);
          });
          scale.reverse();

          var poligon = new fabric.Polygon(positions);
          var centerRelativeToImage = self.canvasToMap(poligon.getCenterPoint());

          var point = {x: centerRelativeToImage.x.toFixed(1),y: centerRelativeToImage.y.toFixed(1)};
          var pinPoint = [point.x, point.y];

          var pageId = self.get('controller').get('model.currentPage.id');
          var data = {Point: pinPoint, PageId: pageId, TimeFrame: 0, Svg: objects, Scale: scale, commentType: self.get('commentType')}; //svgAnnotations
          self.get('controller').send('newSVGAnnotation', data);
      });
  }.observes('drawColor', 'selectedDrawWidth'),

  //show/hide drawing related buttons from navigation menu
  showSpecificButtons: function(){
    var canvas = this.get('imageFabricCanvas');
    var selectedShape = canvas.getActiveObject();
    if(selectedShape && selectedShape.isCustomShape){
      this.get('controller').send('showSpecificButtons', true);
      return true;
    }
    this.get('controller').send('showSpecificButtons', false);
  },

  closeAddLine: function(){
      var map = this.get('controller.approval.map');

      this.set('addMeasurementLine', false);
      this.get('controller').set('lineStarted', false);

    this.closeSoftProofingPopup(map);

    Ember.$(this.getPathForSelectElement('div.canvas-container')).removeClass('czn-canvas-color-picker');
  },

  initAddLine: function(){
    var canvas = this.get('imageFabricCanvas'),
      map = this.get('controller.approval.map'),
      annotationLayer = this.get('annotation-layer');

    this.set('controller.addColorPicker', false);
    this.set('addMeasurementLine', true);

    if (map) {
      this.set('annotation', null);
    }

    if (annotationLayer){
      annotationLayer.set('newAnnotationAddingState', false);
    }

    this.closeSoftProofingPopup(map);

    if (canvas) {
      canvas.isDrawingMode = false;
      canvas.selection = false;
    }

    Ember.$(this.getPathForSelectElement('div.canvas-container')).addClass('czn-canvas-color-picker');

    var controller = this.get('controller');
    controller.send('clearPinsLayers', this.get('controller.approval.map'), this.get('controller.approval.select'));
  },

  updateMask: function() {
    var currentView = this.$();
    if (currentView && currentView.is(":visible")) {
      var mapPos = this.get('controller.model.mapPosition');
      if (mapPos) {
        this.get('controller.setMaskFromMapPosition')(this);
      }
    }
  }.observes('controller.model.mapPosition'),
  addLineMouseDown: function(ev){

    var canvas = this.get('imageFabricCanvas'),
      startPoint = canvas.getPointer(ev.e), mapPosition = this.get('controller.model.mapPosition'),
      mapOffset = this.get('controller.model.mapOffset');
    if (this.get('controller').isInMap(mapPosition, { x: ev.e.clientX - mapOffset.left, y: ev.e.clientY - mapOffset.top})) {
      this.get('controller.setMaskFromMapPosition')(this);// make sure the clip is updated correctly
      if (this.get('addMeasurementLine') && canvas) {
        canvas.clear();
        if (ev.e.button === 0) {
          //var startPoint = canvas.getPointer(ev.e);
          this.set('startX', startPoint.x);
          this.set('startY', startPoint.y);
          this.get('controller').set('lineStarted', true);
          var currentViewerController = this.GetCurrentViewerController();
          var currentResolution = currentViewerController.get('renderer.currentResolution');

          var inversOfMaxResolution = 1 / currentViewerController.get("renderer.maxRezolution");
          var strokeWidth = inversOfMaxResolution * currentResolution;

          var line = new fabric.Line([startPoint.x, startPoint.y, startPoint.x, startPoint.y], {
            fill: 'black',
            stroke: 'black',
            strokeWidth: strokeWidth,
            selectable: false
          });
          canvas.add(line);
          this.set('line', line);
        }
      }
    }
  },

  addLineMouseMove: function(ev){

      var canvas = this.get('imageFabricCanvas'),
        stopPoint = canvas.getPointer(ev.e),
        map = this.get('controller.approval.map'),
        line = this.get('line'), mapPosition = this.get('controller.model.mapPosition'),
        mapOffset = this.get('controller.model.mapOffset');
      if (this.get('addMeasurementLine') && this.get('controller.lineStarted') && line && this.get('controller').isInMap(mapPosition, {
          x: ev.e.clientX - mapOffset.left,
          y: ev.e.clientY - mapOffset.top
        })) {
        line.set({x2: stopPoint.x, y2: stopPoint.y});
        canvas.renderAll();
      }

  },

  addLineMouseUp: function(ev){
      var canvas = this.get('imageFabricCanvas'),
        line = this.get('line'),
        startX = this.get('startX') || 0,
        startY = this.get('startY') || 0,
        stopPoint = canvas.getPointer(ev.e), mapPosition = this.get('controller.model.mapPosition'),
        mapOffset = this.get('controller.model.mapOffset');

      var isValidPoint = this.get('controller').isInMap(mapPosition, { x: ev.e.clientX - mapOffset.left, y: ev.e.clientY - mapOffset.top});//stopPoint.x <= canvas.getWidth() && stopPoint.x >= 0 && stopPoint.y <= canvas.getHeight() && stopPoint.y >= 0;
      if (this.get('addMeasurementLine') && stopPoint && (startX !== stopPoint.x || startY !== stopPoint.y) && isValidPoint) {
        var absoluteStopPoint = this.canvasToMap(new fabric.Point(stopPoint.x, stopPoint.y));
        var absoluteStartPoint = this.canvasToMap(new fabric.Point(startX, startY));
        this.calculateLineMeasurement(absoluteStartPoint, absoluteStopPoint);
      }
      this.get('controller').set('lineStarted', false);

      if (!isValidPoint && line) {
        line.remove();
      }
  },

  calculateLineMeasurement: function (startPoint, stopPoint) {
    var boundingWidth = Math.abs(startPoint.x - stopPoint.x),
      boundingHeight = Math.abs(startPoint.y - stopPoint.y);

    var dpi = this.get('controller.model.model.currentPage.DPI'),
      distanceInPixels = Math.sqrt(Math.pow(boundingWidth, 2) + Math.pow(boundingHeight, 2)),
      distanceInInch = distanceInPixels/dpi,

      map = this.get('controller.approval.map');

    var showMeasurementsInInches = this.get('controller.showMeasurementsInInches'),
      annotationHtml = '<table>content</table>',
      annotationText = '',
      content = '';

    if (showMeasurementsInInches){
      content = '<tr><td>' +  ' '  + this.roundNum(distanceInInch) + '  inch' + '</td></tr>' ;

      annotationText =  this.roundNum(distanceInInch) + '  inch' + '\n' ;
    }
    else
    {
    var distanceInMM = Math.round(distanceInInch * AppConstants.Inch2Milimeters);
        if( Math.abs((startPoint.y - stopPoint.y)) > Math.abs((startPoint.x - stopPoint.x)) )
      {
        var valueToAdd = Math.round(distanceInMM / 100);
        distanceInMM = distanceInMM + valueToAdd;
      }
       content = '<tr><td>' +  ' ' +  (distanceInMM + '  mm') + '</td></tr>' ;
       
      annotationText =  ' ' + distanceInMM + '  mm' + '\n' ;
    }

    var popup = this.getSoftProofingPopup(map);

    annotationHtml = annotationHtml.replace(/content/g, content);

    if (popup && map) {
      var self = this;
      var element = popup.getElement();
      Ember.$(element).popover('destroy');
      popup.setPosition([stopPoint.x, -stopPoint.y]);
      Ember.$(element).popover({
        'placement': this.get('jqueryUtilsService').popOverPlacement,
        'animation': false,
        'html': true,
        'content': annotationHtml
      }).data('bs.popover')
        .tip()
        .addClass('czn-colorDensitometer-panel czn-canvas-panel-selectable')
        .on('click', function() { self.softProofingPopupClick(); });

      Ember.$(element).attr('annotationText', annotationText);
      Ember.$(element).attr('coords', "" + startPoint.x + "|" + startPoint.y  + "|" + stopPoint.x + "|" + stopPoint.y);
      Ember.$(element).popover('show');
    }
  },

  roundNum: function(num){
    var showMeasurementsInInches = this.get('controller.showMeasurementsInInches');
    if (showMeasurementsInInches){
      return parseFloat(Math.round(num * 100) / 100).toFixed(2);
    }
    else{
      return Math.round(num);
    }
  },

  softProofingPopupClick: function(){
    if (this.get('userCanAnnotate')) {
      var map = this.get('controller.approval.map'),
        mapPosition,
        mapOffset,
        layerPins = this.get('controller.pins'),
        pageId = this.get('controller').get('model.currentPage.id'),
        coords,
        scale = this.getScale(),
        commentsController = this.get('controller.controllers.comments/index'),
        self = this;

      if (this.get('isCompareMode') === true){
        Ember.$('ul.czn-versions-ul>li.czn-versions-li.active .czn-lock-button').click();
      }
      else{
        Ember.$('.czn-lock-button').click();
      }

      var popup = this.getSoftProofingPopup(map);
      var element = popup.getElement();
      coords =  (Ember.$(element).attr('coords') || '').split('|');

      if (commentsController) {
        commentsController.send('initAnnotating');
      }

      if (this.get('color-picker-point')) {
        layerPins.removeLayer(this.get('color-picker-point'));
      }

      if (map && coords && coords.length === 4) {
         mapPosition = this.get('controller.model.mapPosition');
         mapOffset = this.get('controller.model.mapOffset');
      }

      if (this.get('addMeasurementLine')) {

        this.closeAddLine();
        var lineMiddlePoint = [(parseFloat(coords[0]) + parseFloat(coords[2])) / 2, (parseFloat(coords[1]) + parseFloat(coords[3])) / 2];

        if (commentsController) {
          commentsController.set('model', []);
          commentsController.send('show');
          commentsController.set('pageId', pageId);
          commentsController.set('commentType', AppConstants.CommentTypes.LINE_COMMENT);
          commentsController.send('setMarkerPosition', mapPosition.left + lineMiddlePoint[0] * scale , mapPosition.top + lineMiddlePoint[1]*scale + mapOffset.top);
        }
        this.get('controller').send('addMarkerOverlay', [lineMiddlePoint[0]  , -lineMiddlePoint[1] ]);
        this.set('commentType', AppConstants.CommentTypes.LINE_COMMENT);
        this.sendMarkupsAndShapesToCommentPopup();
        this.stopFabricEvents();
      }
      if (this.get('controller.addColorPicker')) {

        this.closeColorPicker();

        if (commentsController) {
          var point = [Math.floor(coords[0]),  Math.floor(-coords[1])];
          commentsController.set('model', []);
          commentsController.send('show');
          commentsController.set('pageId', pageId);
          commentsController.set('commentType', AppConstants.CommentTypes.COLOR_PICKER_COMMENT);
          commentsController.set('model', []);
          commentsController.set('point', point);
          commentsController.set('svg', []);
          this.get('controller').send('addMarkerOverlay', [coords[0], coords[1]]);
          this.get('controller').send('updateMarkerPosition');
        }
        this.set('commentType', AppConstants.CommentTypes.COLOR_PICKER_COMMENT);
      }
      Ember.run.later(function () {
          self.get('controller.controllers.comments/index').set('newComment', Ember.$(element).attr('annotationText'));
      });
    }
  },

  initColorPicker: function(){
    var annotationLayer = this.get('annotation-layer'),
      canvas = this.get('imageFabricCanvas'),
      line = this.get('line'),
      self = this;

    this.set('addMeasurementLine', false);
    this.get('controller').set('lineStarted', false);
    this.set('addColorPicker', true);

    if (line){
      line.remove();
    }

    if (annotationLayer){
      annotationLayer.set('newAnnotationAddingState', false);
    }
    if (canvas) {
      canvas.isDrawingMode = false;
      canvas.selection = false;
      this.$().addClass('czn-canvas-color-picker');
    }
  },

  closeColorPicker: function(){
    var map = this.get('controller.approval.map'),
      annotationLayer = this.get('annotation-layer'),
      layerPins = annotationLayer ? annotationLayer.get('layerPins') : null;

    this.set('controller.addColorPicker', false);
    this.$().removeClass('czn-canvas-color-picker');

    if (layerPins && this.get('color-picker-point')) {
      layerPins.removeLayer(this.get('color-picker-point'));
    }

    this.closeSoftProofingPopup(map);
  },

  getSoftProofingPopup: function(map) {
    if(map) {
      var popup;
      for (var i = 0; i < map.getOverlays().getLength(); i++) {
        popup = map.getOverlays().item(i);
        if (popup.get('softProofingToolsPopup')) {
          return popup;
        }
      }
    }
    return null;
  },
  closeSoftProofingPopup: function(map){
    var popup = this.getSoftProofingPopup(map);

    if (popup){
      var element = popup.getElement();
      Ember.$(element).popover('hide');
      Ember.$(element).popover('destroy');
    }
  },

  getPathForSelectElement: function(elem){
    var isOverlayMode = this.get('controller.controllers.image/controls/image.isDifferenceSelected');

    if (isOverlayMode) {
      return ".czn-overlay-view-opacity .czn-image-viewer " + elem;
    }

    if(this.get('isCompareMode') && !isOverlayMode) {
        return "ul.czn-versions-ul li.czn-versions-li.active .czn-image-viewer "+ elem;
    }

    return ".czn-image-viewer " + elem;
  },

  canvasToMap: function(p){
    var pRes = p,
      mapMinZoomSize = this.get('controller.model.mapMinZoomSize');
    if (this.get('initialCanvasOffset')) {
      pRes = p.add(this.get('initialCanvasOffset'));
    }

    pRes.x *= this.get('controller.model.model.currentPage.OutputImageWidth') / mapMinZoomSize.width;
    pRes.y *= this.get('controller.model.model.currentPage.OutputImageHeight') / mapMinZoomSize.height;
    return pRes;
  },

//--------------------- Marquee drawing tool -----------------------------

  onMouseUpChange:function() {
    if (this.get('mouseUp')) {
      this.set('isSelectedObjectChanging', false);
    }
  }.observes('mouseUp'),

  onSelectedDrawWidthChange: function() {
    this.changeSelectedMarqueRectangleWidth(this.get('selectedDrawWidth'));
    this.drawWidthChanged(this.get('imageFabricCanvas'), this.get('selectedDrawWidth'));
  }.observes('selectedDrawWidth')

  //-----------------------------------------------------------------------
});
export default ImageCanvasView;
