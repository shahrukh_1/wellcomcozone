import Ember from "ember";

var CompareItemView = Ember.View.extend({
  tagName: 'li',
  classNameBindings: [':czn-versions-li', ':selectable', 'select:active'],
  select: Ember.computed.alias('controller.select'),
  version: Ember.computed.alias('controller.model'),

  click: function(e){
      if (!this.get('select')){
          var version = this.get('version');
          this.get('controller').send('changeSelectedVersion', version);
      }
  }
});
export default CompareItemView;
