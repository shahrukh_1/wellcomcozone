import Ember from "ember";

var MiniMapViewer = Ember.View.extend({
    didInsertElement: function(){
        var self = this;
        var selection = this.$("div.czn-minimap-selection");
        this.get('controller').send('initializeView', this);

        selection.draggable({
            containment: ".czn-minimap-img",
            start: function() {
            },
            drag: function() {
            },
            stop: function() {
            }
        });
    },

    getData: function(){
        var selection = this.$("div.czn-minimap-selection");
        return {
            selection: {
                left: parseInt(selection.css('left'),10),
                top: parseInt(selection.css('top'), 10),
                width: parseInt(selection.width(), 10),
                height: parseInt(selection.height(), 10)
            },
            miniMap: {
                width: parseInt(this.$('.czn-minimap-img').width(), 10),
                height: parseInt(this.$('.czn-minimap-img').height(), 10)
            }
        };
    }
});
export default MiniMapViewer;
