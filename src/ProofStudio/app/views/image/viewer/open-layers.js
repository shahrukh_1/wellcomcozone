import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var OpenLayersViewer = Ember.View.extend({
  jqueryUtilsService: Ember.inject.service('jquery-utils'),
    classNames: ['czn-image-viewer'],
    classNameBindings: ['uniqueClassWithinVersions'],
    tagName: 'div',
    pageWasRefreshedAfterUHRFinished: true,
    uniqueClassWithinVersions: function () {
        return "czn-image-viewer-v" + this.get('controller.id');
    }.property('controller.id'),




  startCompare: function(){
    this.compareCanvases();
  },


  onInit: function() {
    this.get('instantNotificationsCommunicationService').RegisterOnShowNewTilesInstantNotification(this,'disableColorPickerUntilPageIsRefreshed');
    this.get('modalDialogsCommunicationService').RegisterCompareCanvasesEvent(this,'startCompare');
  }.on('init'),

  onCleanup: function() {
    this.get('instantNotificationsCommunicationService').UnRegisterOnShowNewTilesInstantNotification(this,'disableColorPickerUntilPageIsRefreshed');
   // this.get('modalDialogsCommunicationService').UnRegisteCompareCanvasesEvent(this,'startCompare');
  }.on('willDestroyElement'),





  disableColorPickerUntilPageIsRefreshed(message){

      if (this.get('controller.model.id') != message.Version || message.EntityType != AppConstants.InstantNotificationEntityType.VersionSwitchedToHighRes)
        return;

      this.set('pageWasRefreshedAfterUHRFinished', false);
    },

  afterViewRender: function(){
        this.refreshView();
    }.on('didInsertElement'),

    initializeView: function(){
        this.refreshView();
    }.observes('controller.versionIsChanged', 'controller.currentPage'),

    onOverlayMode: function() {
      if (this.get('controller.isOverlayMode')) {
        if (this.get("controller.model.approvalMap")) {
          this.get('controller.model.approvalMap').render();
        }
      }
    }.observes('controller.percentagesSwipe'),

    refreshView: function(){
        var self = this;

        Ember.run.later(function(){
            var controller = self.get('controller');
            if (controller && !controller.get('isDestroyed')){
                controller.send('initializeView', self);
            }
        });
    },

    getSizeOfTheSmallestTile: function() {
      var width = this.get('controller.size.width'),
        height = this.get('controller.size.height');

      while (width > 256 || height > 256){
        width = width / 2;
        height = height / 2;
      }

      return (width > height) ? width : height;// the while stopped because of the max edge
    },

    getMinZoom: function () {
      var smallestTileSize = this.getSizeOfTheSmallestTile();
      var ratioBetweenSamllestTileAndTileSize = smallestTileSize/AppConstants.TileSize;
      return Math.log(ratioBetweenSamllestTileAndTileSize) / Math.log(2); // As Math.Log2 is not found in IE11
    },
    getMaxZoom: function(){
    var maxZoom = 0,
        width = this.get('controller.size.width'),
        height = this.get('controller.size.height');

      while (width > 256 || height > 256){
        width = width / 2;
        height = height / 2;
        maxZoom ++;
      }

    return maxZoom;
  },

  getMapSource: function(mapData) {
    var tileLoadFunction = function(image, src) {
      (function checkImage(image, src) {
        var hash = {
          type: "HEAD",
          data: {},
          url: src,
          cache: true,
          error: function(xhr, textStatus, errorThrown) {
            if(xhr.status == 404 || xhr.status == 403) {
              setTimeout(function() {
                checkImage(image, src);
              }, 1000);
              return true;
            }
          },
          success: function() {
            image.getImage().src = src;
          }
        };
        Ember.$.ajax(hash);
      })(image, src);
    };

    var crossOrigin = 'anonymous';

    var source = new ol.source.Zoomify({
      url: mapData.url,
      size: [mapData.imgWidth, mapData.imgHeight],
      crossOrigin: crossOrigin,
    });
    source.setTileLoadFunction(tileLoadFunction);

    //In case a tile fails, try to reload it ( 0 = State IDLE)
    source.on('tileloaderror', function(ev) {
      setTimeout(function() {
        ev.tile.state = 0;
        ev.tile.load();
      }, 5000);
    });

    return source;
  },
  buildTile: function(source) {
    return new ol.layer.Tile({
      source: source
    });
  },
  buildMap: function(mapData) {
    var self = this;

    var controller = this.get('controller');

    var proj = mapData.projection,
      select = new ol.interaction.Select(),
      overviewMapControl = new ol.control.OverviewMap({
        className: 'ol-overviewmap ol-custom-overviewmap ol-map-' + self.get('controller.id') + self.get('controller.isClone'),
        layers: [
          new ol.layer.Tile({
            source: mapData.source
          })
        ],
        view: new ol.View({
          projection: proj,
          center: mapData.imgCenter
        }),
        collapsed: true,
        collapsible: false,
        target: document.getElementById("minimap")
      }),
      map = new ol.Map({
        layers: [
          mapData.olLayerTile
        ],
        target: this.$().attr('id'),
        view: new ol.View({
          projection: proj,
          center: mapData.imgCenter,
          zoom: mapData.minZoom,
          minZoom: mapData.minZoom,
          maxZoom: mapData.maxZoom,
          //zoomFactor:1.5,
          //tileSize:188
        }),
        controls: [overviewMapControl],
        interactions: ol.interaction.defaults({
          shiftDragZoom: false,
          altShiftDragRotate: false,
          pinchRotate: false,
          dragPan: false,
          mouseWheelZoom: false
        }).extend([
          new ol.interaction.DragPan({kinetic: false}),
          new ol.interaction.MouseWheelZoom({duration: 0}),
          select
        ])
      });
    return {map:map, select: select};
  },

  getCustomProjection: function(imgWidth, imgHeight) {
    return new ol.proj.Projection({
      code: 'ZOOMIFY',
      units: 'pixels',
      extent: [0, 0, imgWidth, imgHeight]
    });
  },

  getSetupMapData: function() {
    var url = this.get('controller.model.tilesUrl');
    var imgWidth = this.get('controller.size.width'),
      imgHeight = this.get('controller.size.height');
    var imgCenter = [imgWidth/2, -imgHeight/2];
    var minZoom = this.getMinZoom();
    var maxZoom = this.getMaxZoom() + minZoom; // minZoom is between -1/2 and 0

    return {
      url: url,
      imgWidth: imgWidth,
      imgHeight: imgHeight,
      imgCenter: imgCenter,
      minZoom: minZoom,
      maxZoom: maxZoom
    }
  },

    createMap: function() {
      var setupMapData = this.getSetupMapData();
      if (!setupMapData.url || !setupMapData.imgWidth || !setupMapData.imgHeight){
        return;
      }

      setupMapData.projection = this.getCustomProjection(setupMapData.imgWidth, setupMapData.imgHeight);

      setupMapData.source = this.getMapSource(setupMapData);

      if (typeof this.$() === 'undefined') {
        return;
      }

      var olLayerTile = this.buildTile(setupMapData.source);
      setupMapData.olLayerTile = olLayerTile;

      var self=this;
      olLayerTile.on('postcompose', function (event) {

        if (event!=null && typeof self.compareCanvases != 'undefined') {
          var currentCanvas = event.context.canvas;
          var currentClasses = currentCanvas.parentElement.parentElement.classList;
          var currentClass = currentClasses[2];
          var currentImg = currentCanvas.getContext('2d').getImageData(0, 0, currentCanvas.width, currentCanvas.height);
          var currentVersion = currentClass.replace('czn-image-viewer-v', '');
          var sdf =  window.Appkit.ProjectFolderID
          if (myV1Version == '' || myV1Version == currentVersion) {
            myV1Version = currentVersion;
            myV1Img = currentImg;
            var myDiffDivPositionLeft = currentCanvas.getBoundingClientRect().left;
            if (myDiffDivPositionLeft>2 && sdf === "0")
            {
              document.getElementById('map3').style.left = "" + myDiffDivPositionLeft + "px";
            }
          }
          else {
            myV2Version = currentVersion;
            myV2Img = currentImg;
            var myDiffDivPositionLeft = currentCanvas.getBoundingClientRect().left;
            if (myDiffDivPositionLeft>2 && sdf === "0")
            {
              document.getElementById('map3').style.left = "" + myDiffDivPositionLeft + "px";
            }
          }
          self.compareCanvases();
        }
      });

      var mapResult = this.buildMap(setupMapData);

      this.bindMapEvents(mapResult.map);
      this.addSelectEvent(mapResult.select, mapResult.map);

      setupMapData.map = mapResult.map;
      setupMapData.select = mapResult.select;

      this.loadSwipe(olLayerTile);

      this.setDataToController(setupMapData);

      this.createMarkers();

      this.fitLater();
    },

  setDataToController: function (mapData) {
    var controller = this.get('controller');
    var map = mapData.map;
    controller.set('approval', {map: map, select: mapData.select});
    this.get('controller.model').set('approvalMap', map);

    controller.set('imgCenter', mapData.imgCenter);
    controller.set('imgWidth', mapData.imgWidth);
    controller.set('imgHeight', mapData.imgHeight);
    controller.set('minZoom', mapData.minZoom);
    controller.set('maxZoom', mapData.maxZoom);
    controller.set('widthAtZeroZoom', this.get('controller').getWidthAtZoomZero());
    controller.set('minRezolution', 1);
    controller.set('maxRezolution', Math.pow(2,mapData.maxZoom - mapData.minZoom));

    var currentpage = this.get("controller.controllers.approval.currentVersion");
    var approvalRestrictedZoom = currentpage.get('RestrictedZoom');
    controller.set('restrictedZoom', approvalRestrictedZoom);

    var scale = 1 / map.getView().getResolution(),
      minZoomWidth = Math.ceil(mapData.imgWidth * scale),
      minZoomHeight = Math.ceil(mapData.imgHeight * scale);

    var mapMinZoomnSize = { width: minZoomWidth, height:  minZoomHeight},
      mapSize = map.getSize();

    var offsetX = -(mapSize[0] - minZoomWidth)/2,
      offsetY = -(mapSize[1] - minZoomHeight)/2;

    controller.set('mapMinZoomSize', mapMinZoomnSize);
    this.addCanvasAsOverlay(map, [offsetX, offsetY]);
    controller.initCanvasPosition( { width: mapSize[0], height: mapSize[1], center: map.getView().getCenter(), offset: [offsetX, offsetY], scale: scale });

  },

  loadSwipe: function (olLayerTile) {
    if (this.get('controller.isOverlayMode')) {
      if (this.get('controller.currentPage').get('Approval').get('id') == this.get('controller.currentVersion').get('id') &&
        this.get('controller.view.controller').get('model.isClone') == this.get('controller.currentVersion').get('isClone')) {
        this.loadSwipeForGivenLayer(olLayerTile);
      }
    }
  },
  fitLater: function() {
    var self = this;
    Ember.run.later(function(){
      if (self.get('controller')) {
        self.get('controller').send('fit');
      }
    }, 500);
  },

  addSelectEvent: function(select, map){
    var controller = this.get('controller');
    select.on('select', function(e) {
      if(e.deselected[0]) {
        var desName = e.deselected[0].get('name');
        e.deselected[0].setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 3
            }),
            fill: new ol.style.Fill({
              color: '#000'
            })
          }),
          text: new ol.style.Text({
            text: desName,
            fill: new ol.style.Fill({
              color: '#fff'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 1
            }),
          })
        }));
      }
      if(e.selected[0]) {
        var name = e.selected[0].get('name'),
          annotationId = e.selected[0].get('annotationId'),
          annotation = controller.getSelectedAnnotation(name);

        e.selected[0].setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 10,
            stroke: new ol.style.Stroke({
              color: 'red',
              width: 3
            }),
            fill: new ol.style.Fill({
              color: '#000'
            })
          }),
          text: new ol.style.Text({
            text: name,
            fill: new ol.style.Fill({
              color: '#fff'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 1
            }),
          })
        }));

        var balloonsView = Ember.$(".czn-balloons-view");
        var balloonsViewIsVisible = balloonsView && balloonsView.length > 0 && balloonsView.css('display') !== 'none';
        if(!balloonsViewIsVisible) {
          controller.send('clearPinsLayers', map, select);
          if(annotation) {
            controller.send('displaySingleAnnotationPopup', annotation);
          }
        }
        else {
          controller.showCanvasForAnnotation(map, annotationId);
        }
      }
    });
  },

  bindMapEvents: function(map) {
    var controller = this.get('controller');
    var olView = map.getView(),
      self = this;

    if (olView) {
      map.on('postrender', function(){
        if (typeof self.compareCanvases != 'undefined')
        {
          //self.compareCanvases();
        }
      });
      map.on('moveend', function(){
        self.updateMapPosition();
        controller.set('movestart', false);
      });
      map.on('change:size', function(){
        var mapSize =  map.getSize();
        if(typeof  mapSize !== 'undefined') {
          self.updateMapPosition();
          controller.send('dispatchResizeEvent', mapSize);
        }
      });
      map.on('click', function(ev) {
        var canvas = self.get('controller').get('image-canvas-view');
        if (canvas) {
          canvas.get('controller.setMaskFromPixelFromCoordinates')(canvas);
        }
        self.addColorPickerClick(ev);
        self.set("controller.controllers.comments/index.closeUsersList", true);

        self.addMarkerClick(ev);
      });
      //make sure popup dom element is present when map is recreated
      if( this.$('.colorDensitometer').length == 0) {
        this.$('#spCnt').append('<div class="colorDensitometer"></div>');
      }
      var colorDensiometerPopup = new ol.Overlay({
        element: this.$('.colorDensitometer')[0]
      });
      colorDensiometerPopup.set('softProofingToolsPopup', true);
      map.addOverlay(colorDensiometerPopup);
      this.set('colorDensitometerPopup', colorDensiometerPopup);

      olView.on('change:rotation', function(){
        self.updateMapPosition();
        if(controller.get('isDestroyed') == false) {
          controller.send('updateRotation', olView.getRotation());
          controller.send('updateMarkerPosition');
          controller.set('movestart', true);
          controller.syncOverlayMaps();
          controller.syncZoomInCompareMaps();
        }
      });
      olView.on('change:resolution', function(){
        if(controller.get('isDestroyed') == false) {
          controller.set('isFitToPage', false);
          controller.send('updateScale', olView.getResolution());
          controller.send('updateMarkerPosition', 150);
          controller.set('movestart', true);
          controller.syncOverlayMaps();
          controller.syncZoomInCompareMaps();
        }
      });
      olView.on('change:center', function(){
        if(controller.get('isDestroyed') == false) {
	        controller.send('updateCenter', olView.getCenter());
          controller.send('updateMarkerPosition');
          controller.set('movestart', true);
          controller.set('isFitToPage', false);
          controller.syncOverlayMaps();
          controller.syncZoomInCompareMaps();
        }
      });

      Ember.$(".ol-overviewmap-box").draggable({drag: function (event, ui) {  }});
      Ember.$(".ol-overviewmap-box").on("dragstop", function (event, ui) {
        self.syncMinimap(ui, map);
        /* After drag the box left and top are off */
        Ember.$(".ol-overviewmap-box").css("left", "auto");
        Ember.$(".ol-overviewmap-box").css("top", "auto");
      });
    }
  },

  updateMapPosition: function(){
    var map = this.get('controller.approval.map'),
      mapOffset = this.$().offset(),
      startingPointPosition = map ? map.getPixelFromCoordinate([0,0]) : [0,0],
      view = map ? map.getView() : null,
      resolution = view ? view.getResolution() : 1,
      rotation = view ? view.getRotation() : 0, // in radians
      angle = rotation * 180 / Math.PI, // in degrees
      scale = 1 / resolution,
      width = this.get('controller.size.width') * scale ,
      height = this.get('controller.size.height') * scale,
      center = view ? [view.getCenter()[0] / resolution, view.getCenter()[1] / resolution] : [0,0];

      if (startingPointPosition) {
        var position = {
          width: width,
          height: height,
          angle: angle,
          left: startingPointPosition[0],
          top: startingPointPosition[1]
        };

        this.get('controller').set('currentResolution', resolution);

        this.get('controller').set('currentZoom', (Math.log((this.get('controller.size.width') / (resolution * this.get('controller.widthAtZeroZoom')))) / Math.log(2)));
        //this.get('controller').set('currentZoom', (Math.log((this.get('controller.size.width') / (resolution * 256))) / Math.log(2)));

        // update mapPosition property on the open-layers controller
        this.get('controller').set('mapPosition', position);

        // udpate mapCenter property on the open-layers controller
        this.get('controller').set('mapCenter', center);

        this.get('controller').set('mapScale', scale);

        this.get('controller').set('mapOffset', mapOffset);

        var self = this;
        Ember.run.later(function() {
          var balloonsView = self.get('controller.balloons-view');
          if (balloonsView) {
            balloonsView.syncPosition();
            balloonsView.arrangeBalloons();
          }
        });
      }
      else {
        console.log('no starting point');
      }
  },

  createMarkers: function(){
    if(this.get("controller.controllers.navigation/main.isShowPins")){
      if(this.get('controller.controllers.comments/index.visible')) return;

      var map = this.get('controller.approval.map'),
      select = this.get('controller.approval.select'),
      markers = this.get('controller').get('markers');

      if (map && markers ){
        if(markers.get('length') > 0)
        {
          this.get('controller').send("clearPinsLayers", map, select);

          this.get('controller').send('hideCanvas');
          map.addLayer(this.getMarkerLayer(markers));
          this.get('controller').send('removeMarkerOverlay');
        }
      }
    }
  },

  getMarkerLayer: function(markers){
    var features = new Array();
    markers.forEach(function(marker) {

      var feature = new ol.Feature({
        geometry: new ol.geom.Point([marker.pos[0],marker.pos[1]]),
        name: marker.element,
        annotationId: marker.annotationId
      });
      features.push(feature);
    });

    var sourceMarkers = new ol.source.Vector({
      features: features
    });

    var vectorLayer = new ol.layer.Vector({
      source: sourceMarkers,
      style: function(feature, resolution) {
     var name = feature.get('name');
           return [new ol.style.Style({
            image: new ol.style.Circle({
              radius: 10,
              stroke: new ol.style.Stroke({
                color: '#fff',
                width:3
              }),
              fill: new ol.style.Fill({
                color: '#000'
              })
            }),
            text: new ol.style.Text({
              text: name,
              fill: new ol.style.Fill({
                color: '#fff'
              }),
              stroke: new ol.style.Stroke({
                color: '#fff',
                width:1
              }),
            })
          })];
      }
    });

    return vectorLayer;
  },

  addCanvasAsOverlay: function(map, offset) {
    var controller = this.get('controller');

    if(!controller.getOverlayByProperty('isFabricOverlay')) {
      var overlayContent = controller.get('image-canvas-view').$('.canvas-container')[0];
      var overlay = new ol.Overlay({
        element: overlayContent,
        position: [0, 0],
        stopEvent: false,
        offset: offset
      });

      overlay.set('isFabricOverlay', true);
      map.addOverlay(overlay);
      Ember.run.later(function() {
        var p = new fabric.Point(0,0);
        overlay.setOffset([p[0], p[1]]);
      });
    }
  },

  isInMap: function(ev, mapSize) {
    return ev.coordinate && ev.coordinate[0] > 0 && ev.coordinate[0] <= mapSize.width && ev.coordinate[1] < 0 && ev.coordinate[1] > -mapSize.height;
  },

  addColorPickerClick: function(ev){
    var self = this;

    if(self.get('pageWasRefreshedAfterUHRFinished') == false) {
      self.get('modalDialogsCommunicationService').openUHRFinishedPageRefreshMustBeDone(this.get('controller.model.id'));
      return;
    }
            self.getColorPickerData(ev);
  },

  getColorPickerData: function(ev){
    var mapSize = this.get('controller.size');

    if (this.get('controller.addColorPicker') && this.get('isInMap')(ev, mapSize)){
        var self = this,
            pageId = self.get('controller.currentPage.id');

        var store = self.get('controller.store'),
            bands = store.all('band').filter(function(band){
                return band.get('Page.id') === pageId;
        });

        if(bands.get('length') == 0){
          store.find('band', {
              pageId: pageId
          }).then(function(resultedBands){
              self.getColorPickerFromServer(ev,resultedBands, mapSize);
          }).catch(function(err){
              console.log('getPageBands', err);
          });
        }else{
            self.getColorPickerFromServer(ev, bands, mapSize);
        }
    }
  },

  getColorPickerFromServer: function(ev, bandFiles, mapSize){
    var map = this.get('controller.approval.map'),
      self = this,
      channelsNr = this.get('controller.model.model.currentPage.ApprovalSeparationPlates.length'),
      pageId = this.get('controller.model.model.currentPage.id'),
      clickPointFull = {x: Math.floor(ev.coordinate[0]), y: Math.floor(-ev.coordinate[1])},
      bandHeight = Math.floor(bandFiles.get('firstObject.Size') / (mapSize.width * channelsNr)),
      bandIndex = Math.floor(clickPointFull.y / bandHeight),
      linesNr = clickPointFull.y % bandHeight,
      startByte = Math.floor(channelsNr * ((linesNr - 1) * mapSize.width + clickPointFull.x)),
      bandUrl = bandFiles.filter(function(band){
        return band.get('FileName') === ('Band' + bandIndex + '.bin') && band.get('Page.id') === pageId;
      }).get('firstObject.Url');

    Ember.$.ajax({
      type: "GET",
      url: bandUrl,
      headers: { "Range" : 'bytes=' + startByte + '-' + (startByte + channelsNr - 1) },
      contentType: 'application/octet-stream; charset=UTF-8',
      mimeType: 'text/plain; charset=x-user-defined',
      cache: false,
      success: function(str){
        var channels = [];
        self.get('controller.model.model.currentPage.ApprovalSeparationPlates').forEach(function(chanel){
          channels[chanel.get('ChannelIndex')] = chanel.get('Name');
        });
        self.showColorPickerMeasurement(str, ev.coordinate, channels);
      },
      error: function(){
        console.log('error on byte range');
      }
    });
  },

  addMarkerClick: function(ev){
    var controller = this.get('controller'),
      point = ev.coordinate, mapSize = this.get('controller.size');

    if(controller.get('canClickMap') && this.get('isInMap')(ev, mapSize)) {
      controller.send("clickMap", {ev: ev, point: point});
      controller.set('canClickMap', false);
    }
  },

  toUTF8Array: function(str) {
    var utf8 = [];
    for(var i = 0; i < str.length; i ++){
      utf8.push(str.charCodeAt(i) & 0xFF);
    }
    return utf8;
  },

  showColorPickerMeasurement: function(str, point, channels){
    var map = this.get('controller.approval.map'),
      popup = this.get('controller').getOverlayByProperty('softProofingToolsPopup'),
      annotationHtml = Ember.I18n.t("lblMeasurement") + '<br/>',
      annotationText = Ember.I18n.t("lblMeasurement") + '\n',
      self = this;

    if (channels && str){
      var data = this.toUTF8Array(str);
      var total = 0;
      for (var i = 0; i < data.length; i++){

        if (i >= channels.length){
          break;
        }

        var percent = Math.round((data[i]) * 100/255);
        annotationHtml += channels[i] + ' ' + percent.toString() + '% <br/>';
        annotationText += channels[i] + ' ' + percent.toString() + '% \n';
        total = total + percent;
             }
             if(channels.length === 4){
               annotationHtml += 'Total' + ' ' + '=' + ' ' + total.toString() + '% <br/>';
               annotationText += 'Total' + ' ' + '=' + ' ' + total.toString() + '% \n';

      }
    }
    if (popup && map) {
      var element = popup.getElement();
      Ember.$(element).popover('destroy');
      //if(!popup.getMap())
      //{
      //  popup.setMap(map);
      //}

      popup.setPosition(point);
      Ember.$(element).popover({
        'placement': this.get('jqueryUtilsService').popOverPlacement,
        'animation': false,
        'html': true,
        'content': annotationHtml
      }).data('bs.popover')
        .tip()
        .addClass('czn-colorDensitometer-panel czn-canvas-panel-selectable')
        .on('click', function() { self.get('controller.image-canvas-view').softProofingPopupClick(); });;

      Ember.$(element).attr('annotationText', annotationText);
      Ember.$(element).attr('coords', [point[0], point[1], point[0], point[1]].join('|'));
      Ember.$(element).popover('show');
    }
  },

  getPathForElement: function(elem) {
    return "." + this.get('uniqueClassWithinVersions') + " " + elem;
  },

  createMarkerLayerObservable: function(){
    //console.log("createMarkerLayerObservable");
    this.createMarkers();
  }.observes('controller.markers.length', 'controller.model.id', 'controller.approval', 'controller.model.currentPage', 'controller.model.versionIsChanged',
      'controller.controllers.approval.doneCount', 'controller.controllers.comments/index.visible'),

  destroyMap: function() {
    var map = this.get('controller.approval.map');
    if (map) {
      if (Ember.$(this.getPathForElement(".canvas-container")).length == 1) {
        Ember.$(this.getPathForElement(".canvas-container")).appendTo(this.getPathForElement(".czn-canvas-viewer-control"));
      }
      // Ember.$(this.getPathForElement(".annMarker")).appendTo(this.getPathForElement(""));
      map.getLayers().getArray().forEach(function (layer) {
        map.removeLayer(layer);
      });
      map.getOverlays().forEach(function (overlay) {
        map.removeOverlay(overlay);
      });
      map.getControls().forEach(function (control) {
        map.removeControl(control);
      });
      this.$().off();
      map.setTarget(null);
      map = null;
      this.get('controller').set('approval', null);
      this.get('controller.model').set('approvalMap', null);
    }
      
  },

  createMapObservable: function(){
    Ember.run.once(this, 'processcreateMapObservable');
  }.observes('controller.model.tilesUrl'),

  processcreateMapObservable: function () {
    this.destroyMap();
    if (this.get('controller.model.tilesUrl')) {
      this.createMap();
      var self = this;
      var map = this.get('controller.approval.map');
      this.get('controller.model').set('generateTilesInProgress', false);
      Ember.$(".czn-navbar-main-controls").show();
      Ember.$("#soft-proofing-menu").show(); // show soft proofing menu
      Ember.$("li.dropdown-profiles").show();
      Ember.run.later(function () {
        var zoomLevel = (self.get('controller.session.zoomLevels') || []).filter(function (zl) {
          return zl.get('pageId') === self.get('controller.model.currentPage.id');
        }).get('firstObject');
        if (zoomLevel) {
          var view = map.getView();
          view.setZoom(zoomLevel.get('zoom'));
          self.set('controller.currentZoom', 0);
          self.set('controller.currentZoom', zoomLevel.get('zoom'));
          view.setCenter(zoomLevel.get('center'));
          var angle = zoomLevel.get('angle');
          view.setRotation(angle);
          self.get('controller.session.zoomLevels').removeObject(zoomLevel);
          self.get('controller').set('isFitToPage', zoomLevel.get('isFitToPage'));
        }
      }, 500);
    }
  },

    beforeDestroyingElement: function(){
        this.destroyMap();
    }.on('willDestroyElement'),

  syncMinimap: function(overviewDiv, map) {
      var offset = overviewDiv.helper.position();
      var divSize = [overviewDiv.helper.width(), overviewDiv.helper.height()];
      var mapSize = map.getSize();
      var c = map.getView().getResolution();
      var xMove = offset.left * (Math.abs(mapSize[0] / divSize[0]));
      var yMove = offset.top * (Math.abs(mapSize[1] / divSize[1]));
      var bottomLeft = [0 + xMove, mapSize[1] + yMove];
      var topRight = [mapSize[0] + xMove, 0 + yMove];
      var left = map.getCoordinateFromPixel(bottomLeft);
      var top = map.getCoordinateFromPixel(topRight);
      //ol.coordinate.rotate(left, -angle);
      //ol.coordinate.rotate(top, -angle);
      var extent = [left[0], left[1], top[0], top[1]];
      map.getView().fit(extent);
      map.getView().setResolution(c);
    },

    loadSwipeForGivenLayer:function(olLayerTile) {
      var self = this;
      olLayerTile.on('precompose', function (event) {
        var ctx = event.context;
        var width = ctx.canvas.width * (self.get('controller.percentagesSwipe') / 100);

        ctx.save();
        ctx.beginPath();
        ctx.rect(width, 0, ctx.canvas.width - width, ctx.canvas.height);
        ctx.clip();
      });

      olLayerTile.on('postcompose', function (event) {
        var ctx = event.context;
        ctx.restore();
     });
    },

  compareCanvases: function() {
    if (myV1Img==null || myV2Img==null)
      return;
    if (typeof document.getElementById("map3")=='undefined')
      return;
    if (document.getElementById("map3")==null)
      return;
    if (document.getElementById("map3").style.display=='none')
      return;

    //console.log('myDiffExecutionsCount: ' + myDiffExecutionsCount++);

    //var img1 = img1Ctx.getImageData(0, 0, width, height);
    //var img2 = img2Ctx.getImageData(0, 0, width, height);
    var img1 = myV1Img;
    var img2 = myV2Img;
    var width = img1.width;
    var height = img1.height;
    if (height!=img2.height || width!=img2.width) return;
    var diffCanvas = document.getElementById("map3").querySelector('canvas');
    var diffCanvasForZoom = document.getElementById("map4").querySelector('canvas');
    var diffCtx = diffCanvas.getContext('2d');
    var diffCtxForZoom = diffCanvasForZoom.getContext('2d');
    diffCanvas.width = 2*width/window.devicePixelRatio;
    diffCanvas.height = 2*height/window.devicePixelRatio;
    diffCanvasForZoom.width = width;
    diffCanvasForZoom.height = height;

    var diff = diffCtx.createImageData(width, height);

    //console.log('start:');
    var start = Date.now();
    var diffCount = pixelmatch(img2.data, img1.data, diff.data, width, height, {threshold: myThreshold, includeAA: true});
    var millis = Date.now() - start;
    //console.log(millis);

    diffCtxForZoom.putImageData(diff, 0, 0);

    diffCtx.scale(1.0/window.devicePixelRatio, 1.0/window.devicePixelRatio);
    diffCtx.drawImage(diffCanvasForZoom, 0, 0);

    this.myChangeColorParameters();
  },

  myColorizeImageData: function(data, r, g, b) {
    for (var i = 0; i < data.length; i+= 4) {
      if(data[i+3]==255) // only the not-transparent pixels
      {
        data[i] = r;
        data[i+1] = g;
        data[i+2] = b;
      }
    }
  },

  myColorizeCanvas: function(r, g, b) {
    var diffCanvas = document.getElementById("map3").querySelector('canvas');
    var width = diffCanvas.width;
    var height = diffCanvas.height;
    var diffCtx = diffCanvas.getContext('2d');
    var img2 = diffCtx.getImageData(0, 0, width, height);
    this.myColorizeImageData(img2.data, r, g, b);
    diffCtx.putImageData(img2, 0, 0);
  },

  myParseColor: function(input) {
    return input.split("(")[1].split(")")[0].split(",");
  },

  myChangeColorParameters: function() {
    var mySelectedColorList = document.getElementsByClassName('diffColorPickerSelected');
    if (mySelectedColorList.length==0)
    {
      return;
    }
    var mySelectedColor=mySelectedColorList[0].style.backgroundColor;

    var mypar_DiffColor_r = parseInt(this.myParseColor(mySelectedColor)[0]);
    var mypar_DiffColor_g = parseInt(this.myParseColor(mySelectedColor)[1]);
    var mypar_DiffColor_b = parseInt(this.myParseColor(mySelectedColor)[2]);

    this.myColorizeCanvas(mypar_DiffColor_r, mypar_DiffColor_g, mypar_DiffColor_b);
  },
});
export default OpenLayersViewer;
