import Ember from "ember";

export default Ember.View.extend({
  classNames: ['czn-image-view-container'],
  onInit: function() {
    var self = this;

    Ember.run.later(function(){
      var controller = self.get('controller');
      if (controller && !controller.get('isDestroyed')){
        controller.registerEvents();
      }
    });

  }.on('init'),

  onCleanup: function() {
    var controller = this.get('controller');
    if (controller && !controller.get('isDestroyed')){
      controller.unRegisterEvents();
    }
  }.on('willDestroyElement'),
});
