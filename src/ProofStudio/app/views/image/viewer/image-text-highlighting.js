import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var ImageTextHighLightingLayer = Ember.View.extend({
    classNames: ['image-text-highlighting-view'],
    layerTextHighlighting: null,
    firstSelectedText: null,
    url: null,
    data: null,
    canSelectText: true,
    isStrike: false,
    templateName: 'image/viewer/image-text-highlighting',

    didInsertElement: function(){
        this.initializeView();
    },

    initializeView: function(){
        this.get('controller').send('initializeTextHighlighting', this);

        var self = this;
        this.$().find('ul').find('li').on('click', function(e){
            self.$().find('ul').find('li').removeClass('selected');
            Ember.$(this).addClass('selected');
            self.sendDataToCommentPopup();
            var highlighttype = parseInt(Ember.$(this).attr('highlighttype'));
            var isStrike = (highlighttype === AppConstants.HighlightType.Delete) || (highlighttype === AppConstants.HighlightType.Replace);
            self.set('isStrike', isStrike);
            if (isStrike) {
                self.$().find('.selected').addClass('strike');
            }
            else{
                self.$().find('.strike').removeClass('strike');
            }
            e.preventDefault();
            return false;
        });
    }.observes('controller.model.model.currentPage.id'),

    actions: {
        initializeTextHighlighting: function (size, map, controller, view, pageId) {
            var self = this;
            this.set('size', size);
            this.set('map', map);
            this.set('view', view);
            this.set('pageId', pageId);
            this.set('controller', controller);
            this.$().find('.image-text-highlighting').empty();
            var approvalId = controller.get('controllers.approval').get('id');

            self.set('gettingDataInProgress', true);
            var controller = this.get('controller');
            controller.send('getTextHighlightData', this.get('pageId'), approvalId);

            this.syncPosition();
        }
    },

    mapMoveObservable: function(){
      if(this.get('controller.controllers.navigation/annotation.isTextSelected')) {
        var view = this.$(),
            self = this,
            hide = this.get('controller.movestart');
        Ember.run.later(function () {
          if (view) {
            if (hide) {
              view.hide();
            }
            else {
              view.show();
              self.syncPosition();
            }
          }
        });
      }
    }.observes('controller.movestart'),

    syncPosition: function(){
        var layerIsShown = this.$().find('.image-text-highlighting').is(':visible');
        if (!layerIsShown){
            return;
        }

        var maxSize = this.get('size'),
            layerTextHighlighting = this.$().find('.image-text-highlighting'),
            mapScale = this.get('controller.mapScale'),
            mapPosition = this.get('controller.mapPosition');

        if (layerTextHighlighting && maxSize){
            layerTextHighlighting.css({
                width: maxSize.width,
                height: maxSize.height,
                left: mapPosition.left,
                top: mapPosition.top,
                transform: 'scale(' + mapScale + ',' + mapScale + ') rotate(' + mapPosition.angle + 'deg)',
                '-webkit-transform': 'scale(' + mapScale + ',' + mapScale + ') rotate(' + mapPosition.angle + 'deg)',
                'transform-origin': '0 0',
                '-webkit-transform-origin': '0 0'
            });
        }
    },

    sendDataToCommentPopup: function(){
        var self = this,
            startIndex = null,
            endIndex = 0,
            selectedText = "",
            layerTextHighlighting = this.$().find('.image-text-highlighting');

        Ember.$.each(layerTextHighlighting.find('.box-text-highlighting.selected'), function(i,v){
            selectedText = Ember.$(v).attr('letters') + selectedText;
            var currentStartIndex = parseInt(Ember.$(v).attr('startindex'));
            var currentEndIndex = parseInt(Ember.$(v).attr('endindex'));
            if (currentStartIndex < startIndex || startIndex === null){
                startIndex = currentStartIndex;
            }
            if (currentEndIndex > endIndex){
                endIndex = currentEndIndex;
            }
        });

        var relX = this.get('relX');
        var relY = this.get('relY');

        var optionsTextHighlighting = this.$().find('.options-text-highlighting');
        if (optionsTextHighlighting && layerTextHighlighting.find('.box-text-highlighting.selected').length > 0){
            if (relX && relY) {
                optionsTextHighlighting.css({'left': relX + 'px', 'top': relY + 'px'});
            }
            optionsTextHighlighting.show();
        }

        var highlightType = 1;
        if (optionsTextHighlighting){
            highlightType = parseInt(optionsTextHighlighting.find('ul li.selected').attr('highlighttype'));
            if (highlightType === AppConstants.HighlightType.Replace){
                selectedText = "[REPLACE WITH]\n" + selectedText;
            }
            else if (highlightType === AppConstants.HighlightType.Insert){
                selectedText = "[INSERT]\n";
            }
            else if (highlightType === AppConstants.HighlightType.Delete){
                selectedText = "[DELETE]\n" + selectedText;
            }
        }

        var controller = self.get('controller');
        if (controller){

          var mapScale = this.get('controller.mapScale');
          var angle = this.get("controller.mapPosition.angle");
          //reset transform angle to save svg in correlation with original image
          layerTextHighlighting.css({
            transform: 'scale(' + mapScale + ',' + mapScale + ') rotate(' + 0 + 'deg)',
            '-webkit-transform': 'scale(' + mapScale + ',' + mapScale + ') rotate(' + 0 + 'deg)'
          });

            var parentOffset = layerTextHighlighting.offset();
            var position = layerTextHighlighting.find('.box-text-highlighting.selected:last').offset();

            if (position && parentOffset) {
                controller.send('newComment', {
                    Point: [
                        (position.left - parentOffset.left) / mapScale,
                        (position.top - parentOffset.top) / mapScale
                    ],
                    PageId: controller.get('currentPage.id'),
                    CommentType: AppConstants.CommentTypes.TEXT_COMMENT,
                    CommentText: selectedText,
                    HighlightType: highlightType,
                    StartIndex: startIndex,
                    EndIndex: endIndex,
                    HighlightData: self.getSVG()
                });

              controller.send('addMarkerOverlay', [(position.left - parentOffset.left) / mapScale , -(position.top - parentOffset.top) / mapScale]);
              controller.send('updateMarkerPosition');

              //reapply current angle after svg is saved
              layerTextHighlighting.css({
                transform: 'scale(' + mapScale + ',' + mapScale + ') rotate(' + angle + 'deg)',
                '-webkit-transform': 'scale(' + mapScale + ',' + mapScale + ') rotate(' + angle + 'deg)'
              });

              Ember.run.later(function(){
                  var quill = Ember.$('.ql-editor');
                  quill[0].childNodes[0].innerHTML = selectedText;
              });
            }
        }
    },

    showText: function(){
        var data = this.get('controller').textHighlightingWords;
        var maxSize = this.get('size');
        var self = this;
        var size = {width: maxSize.width , height: maxSize.height};

        var layerTextHighlighting = this.$().find('.image-text-highlighting');

        if (data && layerTextHighlighting) {

            var scale = {x: 1, y: 1};
            if (data && data.length > 0){
                var word = Ember.$(data).get(0);
                scale = {x: size.width/word.get('pageWidth'), y: size.height/word.get('pageHeight') };
            }

            layerTextHighlighting.find('.box-text-highlighting').remove();
            Ember.$.each(data, function (i, v) {
                var word = Ember.$(v).get(0);
                if(word.get('alpha') === 180){
                    if(word.get('letters') === " "){
                        layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable hide' style='transform: translate(" +
                        (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                        "deg); -webkit-transform: translate(" + ((word.get('x') -Math.abs(word.get('width'))) * scale.x) + "px," + ((word.get('y') * scale.y) - 10) +
                        "px) rotate(" + word.get('alpha') + "deg); width: " + 0 + "px; height: " + (word.get('height') *
                        scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                        word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                        word.get('alpha') + "' clientX='" + 0 + "' clientY='" + 0 + "'>&nbsp;</div>");
                      }
                    else{
                    layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable' style='transform: translate(" +
                    (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                    "deg); -webkit-transform: translate(" + ((word.get('x') - word.get('width')) * scale.x) + "px," + ((word.get('y') * scale.y) - 10) +
                    "px) rotate(" + word.get('alpha') + "deg); width: " + (word.get('width') * scale.x) + "px; height: " + (word.get('height') *
                    scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                    word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                    word.get('alpha') + "' clientX='" + ((word.get('x') - word.get('width')) * scale.x) + "' clientY='" + ((word.get('y') * scale.y) - 10) + "'>&nbsp;</div>");
                    }
                }
                if(word.get('alpha') === 0)
                {
                    if(word.get('letters') === " "){
                        layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable hide' style='transform: translate(" +
                        (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                        "deg); -webkit-transform: translate(" + (word.get('x') * scale.x) + "px," + ((word.get('y') -Math.abs(word.get('height'))) * scale.y) +
                        "px) rotate(" + word.get('alpha') + "deg); width: " + 0 + "px; height: " + (word.get('height') *
                        scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                        word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                        word.get('alpha') + "' clientX='" + 0 + "' clientY='" + 0  + "'>&nbsp;</div>");
                    }else{
                 layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable' style='transform: translate(" +
                  (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                  "deg); -webkit-transform: translate(" + (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) +
                  "px) rotate(" + word.get('alpha') + "deg); width: " + (word.get('width') * scale.x) + "px; height: " + (word.get('height') *
                  scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                  word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                  word.get('alpha') + "' clientX='" + (word.get('x') * scale.x) + "' clientY='" + ((word.get('y') - word.get('height')) * scale.y) + "'>&nbsp;</div>");
                    }
                }
                if(word.get('alpha') === 90){
                    if(word.get('letters') === " "){
                        layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable hide' style='transform: translate(" +
                        (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                        "deg); -webkit-transform: translate(" + ((word.get('x') * scale.x) - (((Math.abs(word.get('width')) * scale.x) + (word.get('height') * scale.y) - 10) / 2) - (((Math.abs(word.get('width')) * scale.x) + 20)/2 )) + "px," + ((word.get('y') * scale.y) - (((Math.abs(word.get('width')) * scale.x) + (word.get('height') * scale.y) + 10) / 2) - (((Math.abs(word.get('width')) * scale.x) + 20)/2 ))+
                        "px) rotate(" + word.get('alpha') + "deg); width: " + 0 + "px; height: " + (word.get('height') *
                        scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                        word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                        word.get('alpha') + "' clientX='" + 0 + "' clientY='" + 0  + "'>&nbsp;</div>");  
                    }else{
                    layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable' style='transform: translate(" +
                    (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                    "deg); -webkit-transform: translate(" + ((word.get('x') * scale.x) - (((word.get('width') * scale.x) + (word.get('height') * scale.y) - 10) / 2)) + "px," + ((word.get('y') * scale.y) - (((word.get('width') * scale.x) + (word.get('height') * scale.y) + 10) / 2))+
                    "px) rotate(" + word.get('alpha') + "deg); width: " + (word.get('width') * scale.x) + "px; height: " + (word.get('height') *
                    scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                    word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                    word.get('alpha') + "' clientX='" + ((word.get('x') * scale.x) - (((word.get('width') * scale.x) + (word.get('height') * scale.y) - 10) / 2)) +"' clientY='" + ((word.get('y') * scale.y) - (((word.get('width') * scale.x) + (word.get('height') * scale.y) + 10) / 2)) + "'>&nbsp;</div>");  
                    }
                }
                if(word.get('alpha') === -90)
                {
                    if(word.get('letters') === " "){
                        layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable hide' style='transform: translate(" +
                        (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                        "deg); -webkit-transform: translate(" + ((word.get('x') * scale.x)-((Math.abs(word.get('width')) * scale.x) -(word.get('height') * scale.y) + 10) /2 - (((Math.abs(word.get('width')) * scale.x) + 20)/2 )) + "px," + ((word.get('y')  * scale.y) + (((Math.abs(word.get('width')) * scale.x) -(word.get('height') * scale.y) - 10)) / 2 + (((Math.abs(word.get('width')) * scale.x) + 20)/2 )) +
                        "px) rotate(" + word.get('alpha') + "deg); width: " + 0 + "px; height: " + (word.get('height') *
                        scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                        word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                        word.get('alpha') + "' clientX='" + 0 + "' clientY='" + 0  + "'>&nbsp;</div>");
                       }
                else{
                  layerTextHighlighting.prepend("<div box='" + i + "' class='box-text-highlighting unselectable' style='transform: translate(" +
                  (word.get('x') * scale.x) + "px," + ((word.get('y') - word.get('height')) * scale.y) + "px) rotate(" + word.get('alpha') +
                  "deg); -webkit-transform: translate(" + ((word.get('x') * scale.x)-((word.get('width') * scale.x) -(word.get('height') * scale.y) + 10) /2 ) + "px," + ((word.get('y')  * scale.y) + (((word.get('width') * scale.x) -(word.get('height') * scale.y) - 10)) / 2) +
                  "px) rotate(" + word.get('alpha') + "deg); width: " + (word.get('width') * scale.x) + "px; height: " + (word.get('height') *
                  scale.y) + "px; font-size: " + (word.get('height') * scale.y) + "px; line-height: " + (word.get('height')) + "px;' letters='" +
                  word.get('letters') + "' startIndex='" + word.get('startIndex') + "' endIndex='" + word.get('endIndex') + "' alpha='" +
                  word.get('alpha') + "' clientX='" + ((word.get('x') * scale.x)-((word.get('width') * scale.x) -(word.get('height') * scale.y) + 10) /2 ) + "' clientY='" + ((word.get('y')  * scale.y) + (((word.get('width') * scale.x) -(word.get('height') * scale.y) - 10)) / 2) + "'>&nbsp;</div>");
                      }
                }
            });

            layerTextHighlighting.mousedown(function(e){
                var canSelectText = self.get('canSelectText');
                if (!canSelectText){
                    return;
                }
                layerTextHighlighting.find('.box-text-highlighting.selected').removeClass('selected');
                if(e.which === 1) {
                    self.set('leftButtonDown', true);
                    self.disableMapEvents();
                }

                var optionsTextHighlighting = self.get('optionsTextHighlighting');
                if (optionsTextHighlighting){
                    optionsTextHighlighting.hide();
                    optionsTextHighlighting.find('ul').find('li').removeClass('selected');
                    optionsTextHighlighting.find('ul').find('li:first').addClass('selected');
                    self.set('isStrike', false);
                }

                layerTextHighlighting.find('.strike').removeClass('strike');
            });
            layerTextHighlighting.mousemove(function(e){
                var canSelectText = self.get('canSelectText');
                if (!canSelectText){
                    return;
                }
                if (self.$().find(".options-text-highlighting").is(":visible")){
                    return;
                }
                //var parentOffset = $(this).offset();
                var relX = e.pageX;// - parentOffset.left;
                var relY = e.pageY;// - parentOffset.top;

                self.set('relX', relX);
                self.set('relY', relY);
            });
            layerTextHighlighting.mouseup(function(e){
                var canSelectText = self.get('canSelectText');
                self.set('firstSelectedText', null);
                if (!canSelectText){
                    return;
                }
                if(e.which === 1) {
                    self.set('leftButtonDown', false);
                    self.enableMapEvents();
                }
                self.sendDataToCommentPopup();
            });

            layerTextHighlighting.find('.box-text-highlighting').mousemove(function(){

                if (self.get('leftButtonDown') === true && self.get('canSelectText')) {
                    if (Ember.$(this).hasClass('selected')){
                        self.deselectWords(this);
                        // deselect text - need to be investigated
                    }
                    else {
                        Ember.$(this).addClass('selected');
                        if(self.get('firstSelectedText') === null){
                            self.set('firstSelectedText', this);
                        } else {
                           self.deselectWords(this);
                        }
                        var timer = Ember.run.next(self, function(){
                            self.selectWords();
                        }, 300);
                        self.set('timerMouseMove', timer);
                    }
                }
            });
        }
    },

    selectWords: function(){
        // select words that were not selected when the user select a word
        // from a row and move the mouse down to select another word from the next row

        var layerTextHighlighting = this.$().find('.image-text-highlighting'),
            selectedItems = layerTextHighlighting.find('.box-text-highlighting.selected'),
            arr           = [];

        selectedItems.each(function (i, v) {
            arr.push(parseInt(Ember.$(v).attr('box')));
        });

        var min = Math.min.apply(Math, arr),
            max = Math.max.apply(Math, arr);

        for (var j = min; j < max; j++) {
            layerTextHighlighting.find('[box=' + j.toString() + ']').addClass('selected');
        }
        this.set('timerMouseMove', null);
    },

    deselectWords: function(lastSelectedText) {
        var self = this,
        layerTextHighlighting = this.$().find('.image-text-highlighting'),
        selectedItems = layerTextHighlighting.find('.box-text-highlighting.selected'),
        firstSelectedText = self.get('firstSelectedText'),
        firstSelectedTextBoxId = parseInt(Ember.$(firstSelectedText).attr('box')),
        lastSelectedTextBoxId = parseInt(Ember.$(lastSelectedText).attr('box')),
        arr = [];

          if(firstSelectedTextBoxId < lastSelectedTextBoxId){
            selectedItems.each(function(i, v){
              if(parseInt(Ember.$(v).attr('box')) < firstSelectedTextBoxId || parseInt(Ember.$(v).attr('box')) > lastSelectedTextBoxId)
              {
                  arr.push(parseInt(Ember.$(v).attr('box')));
              }
           });
        } else {
            selectedItems.each(function(i, v){
              if(parseInt(Ember.$(v).attr('box')) > firstSelectedTextBoxId || parseInt(Ember.$(v).attr('box')) < lastSelectedTextBoxId)
              {
                  arr.push(parseInt(Ember.$(v).attr('box')));
              }
            });
        }
           
        arr.forEach(function(boxId){
          layerTextHighlighting.find('[box=' + boxId.toString() + ']').removeClass('selected');
        });
     
    },
   
    show: function(){
        var layerTextHighlighting = this.$().find('.image-text-highlighting');
        if (layerTextHighlighting && !layerTextHighlighting.is(':visible')){
            layerTextHighlighting.show();
            this.syncPosition();
        }
    },

    hide: function(){
        this.deselectAll();
        var view = this.$();
        if (view) {
            var layerTextHighlighting = view.find('.image-text-highlighting');
            if (layerTextHighlighting && layerTextHighlighting.is(':visible')) {
                layerTextHighlighting.hide();
            }
            layerTextHighlighting.find('.box-text-highlighting.selected').removeClass('selected');
            Ember.$('#commentArea').val("");
            var optionsTextHighlighting = view.find('.options-text-highlighting');
            if (optionsTextHighlighting) {
                optionsTextHighlighting.hide();
                optionsTextHighlighting.find('ul').find('li.selected').removeClass('selected');
                optionsTextHighlighting.find('ul').find('li:first').addClass('selected');
            }
        }
    },

    disableMapEvents: function(){
        var map = this.get('map');
    },

    enableMapEvents: function(){
        var map = this.get('map');
    },

    deselectAll: function(){
        var view = this.$();
        if (view) {
            var layerTextHighlighting = view.find('.image-text-highlighting');
            if (layerTextHighlighting) {
                layerTextHighlighting.find('.selected').removeClass('selected');
                layerTextHighlighting.find('.strike').removeClass('strike');
            }
        }
    },

    selectAnnotation: function(annotation){
        var gettingDataInProgress = this.get('gettingDataInProgress');
        if (gettingDataInProgress === false) {
            var startIndex = parseInt(annotation.get('StartIndex'));
            var endIndex = parseInt(annotation.get('EndIndex'));
            var commentType = annotation.get('CommentType');
            var layerTextHighlighting = this.$().find('.image-text-highlighting');

            this.hide();

            if (layerTextHighlighting && (endIndex > 0 || startIndex > 0) && (commentType === AppConstants.CommentTypes.TEXT_COMMENT)) {
                this.show();

                layerTextHighlighting.find('div[box].selected').removeClass('selected');

                layerTextHighlighting.find('div[box]').filter(function(){
                    var localStartIndex = parseInt(Ember.$(this).attr('startindex'));
                    return startIndex <= localStartIndex && endIndex > localStartIndex;
                }).addClass('selected');
            }
            this.set('canSelectText', false);
            this.set('annotation', null);
        }
        else{
            this.set('annotation', annotation);
        }
    },

    selectAnnotationObservable: function(){
        var annotation = this.get('annotation');
        var gettingDataInProgress = this.get('controller.gettingDataInProgress');
        if(gettingDataInProgress == false) {
          this.showText();
        }

        if (annotation && gettingDataInProgress === false){
            this.selectAnnotation(annotation);
        }
    }.observes('controller.gettingDataInProgress', 'annotation'),

    getSVG: function(){
        var self = this;
        var size = this.get('size');
        var result = "<svg version=\"1.1\" width=\"" + size.width + "px\" height=\"" + size.height + "px\" viewBox=\"0 0 " + size.width + " " + size.height + "\" xmlns=\"http://www.w3.org/2000/svg\"><style type=\"text/css\"><![CDATA[ rect.bls { fill: #00CCFF; stroke: #00CCFF; stroke-width: 0;  }  ]]></style><g>";
        var scale = this.get('controller.mapScale');
        var isStrike = this.get('isStrike');
        var layerTextHighlighting = this.$().find('.image-text-highlighting');
        if (layerTextHighlighting){
            var selectedWords = layerTextHighlighting.find('.box-text-highlighting.selected');
                Ember.$.each(selectedWords, function(index, word){
                var alpha = parseFloat(Ember.$(word).attr('alpha'));
                if(alpha === 180 || alpha === 0){ alpha = 0;}
                if(alpha === -90){ alpha = -1;}
                if(alpha === 90){ alpha = 1;}
                var position = Ember.$(word).offset();
                var parentPosition = layerTextHighlighting.offset();
                var offset = {left: (position.left - parentPosition.left), top: (position.top - parentPosition.top) };
                var width = Ember.$(word).width();
                var height = Ember.$(word).height();
                var c0 = {left: word.attributes.clientx.nodeValue, top: word.attributes.clienty.nodeValue};
                var remainder = size.height / size.width;
                if(remainder > 1){
                    if(alpha === 0 ){
                    result += "<rect x=\"" + (self.roundWithDecimals(c0.left)) + "\" y=\"" + (self.roundWithDecimals(c0.top) + (remainder * 10)) + "\" height=\"" + self.roundWithDecimals(height) +  "\" class=\"bls\" width=\"" + self.roundWithDecimals(width) + "\" transform=\"rotate(" + self.roundWithDecimals(alpha) + " " + self.roundWithDecimals(c0.left) + " " + self.roundWithDecimals(c0.top) + ")\" />";
                    }
                    if(alpha === 1){
                    result += "<rect x=\"" + (self.roundWithDecimals(c0.left) + ((width - height +10)/2) - 3)   + "\" y=\"" + (self.roundWithDecimals(c0.top) - ((width - height -10)/2) + 7)  + "\" height=\"" + self.roundWithDecimals(width) +  "\" class=\"bls\" width=\"" + self.roundWithDecimals(height) + "\" transform=\"rotate(" + self.roundWithDecimals(0) + " " + self.roundWithDecimals(c0.left) + " " + self.roundWithDecimals(c0.top) + ")\" />";
                    }
                    if(alpha === -1){
                    result += "<rect x=\"" + (self.roundWithDecimals(c0.left) + ((width - height +10)/2) - 3) + "\" y=\"" + (self.roundWithDecimals(c0.top) - ((width - height -10)/2) + 3) + "\" height=\"" + self.roundWithDecimals(width) +  "\" class=\"bls\" width=\"" + self.roundWithDecimals(height) + "\" transform=\"rotate(" + self.roundWithDecimals(0) + " " + self.roundWithDecimals(c0.left) + " " + self.roundWithDecimals(c0.top) + ")\" />";
                    }  
                 }else{
                     if(alpha === 0){
                     result += "<rect x=\"" + (self.roundWithDecimals(c0.left)) + "\" y=\"" + (self.roundWithDecimals(c0.top) + 3) + "\" height=\"" + self.roundWithDecimals(height) +  "\" class=\"bls\" width=\"" + self.roundWithDecimals(width) + "\" transform=\"rotate(" + self.roundWithDecimals(alpha) + " " + self.roundWithDecimals(c0.left) + " " + self.roundWithDecimals(c0.top) + ")\" />";
                     }
                     if(alpha === 1){
                     result += "<rect x=\"" + (self.roundWithDecimals(c0.left) + ((width - height +10)/2) - 5)   + "\" y=\"" + (self.roundWithDecimals(c0.top) - ((width - height -10)/2))  + "\" height=\"" + self.roundWithDecimals(width) +  "\" class=\"bls\" width=\"" + self.roundWithDecimals(height) + "\" transform=\"rotate(" + self.roundWithDecimals(0) + " " + self.roundWithDecimals(c0.left) + " " + self.roundWithDecimals(c0.top) + ")\" />";
                     }
                     if(alpha === -1){
                     result += "<rect x=\"" + (self.roundWithDecimals(c0.left) + ((width - height +10)/2) - 5) + "\" y=\"" + (self.roundWithDecimals(c0.top) - ((width - height -10)/2)) + "\" height=\"" + self.roundWithDecimals(width) +  "\" class=\"bls\" width=\"" + self.roundWithDecimals(height) + "\" transform=\"rotate(" + self.roundWithDecimals(0) + " " + self.roundWithDecimals(c0.left) + " " + self.roundWithDecimals(c0.top) + ")\" />";
                     } 
                 }
            });
        }
        result += "</g></svg>";
         return result;
    },
	roundWithDecimals: function(val, decimals){
		if (!decimals){
			decimals = 3;
		}
		return Math.round(val * Math.pow(10, decimals))/ Math.pow(10, decimals);
	},

       rotatePoint: function(point, alpha, originPoint, scale){
        return {
            left: (originPoint.left + Math.abs(point.left - originPoint.left) * Math.cos(alpha) + Math.abs(point.top - originPoint.top) * Math.sin(alpha)) / scale + 5 ,
            top: (originPoint.top - Math.abs(point.left - originPoint.left) * Math.sin(alpha) + Math.abs(point.top - originPoint.top) * Math.cos(alpha)) / scale + 5
        };
    }
});
export default ImageTextHighLightingLayer;
