import Ember from "ember";

var VideoView = Ember.View.extend({
    controls: false,
    tagName: 'div',
    classNames: ['video-view'],

    didInsertElement: function(){
        this.get('controller').send('changeBackgroundColorPicker');
    }
});
export default VideoView;
