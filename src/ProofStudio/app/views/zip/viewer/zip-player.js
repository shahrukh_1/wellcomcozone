import Ember from "ember";

 var zipPlayerView = Ember.View.extend({
    // srcBinding: 'controller.src',
    // controls: false,
    // tagName: 'video',
    // attributeBindings: 'src controls width height'.w(),
    // versionChanged: Ember.computed.alias('controller.versionIsChanged'),
    // classNames: ['webpage-player'],

    // onInsertElement: function() {
    //     var self = this;
    //     this.refreshView();
    //     this.$().on('canplaythrough', function(ev) {
    //         self.setVideoDuration(ev);
    //     });
    //     this.$().on("ended", function () {
    //         self.get('controller').set('playing', false);
    //     });
    //     this.$().on("timeupdate", function (event) {
    //         var controller = self.get('controller');
    //         if (Math.abs(event.target.currentTime - self.get('controller.currentTime')) >= 0.5 && controller) {
    //             controller.set('currentTime', event.target.currentTime);
    //             controller.set('duration', event.target.duration);
    //             controller.send('timeUpdate', event.target.currentTime, event.target.duration);
    //         }
    //     });
    //     this.$().get(0).oncontextmenu = function() {return false;};
    // }.on('didInsertElement'),

    refreshView: function(){
        var self = this;
        Ember.run.later(function(){
            var controller = self.get('controller');
            if (controller) {
                controller.send('initializeView', self);
                controller.set('view', self);
                //controller.set('currentTime', 0);
                //controller.set('playing', false);
            }
        });
    }.observes('controller.versionIsChanged'),

    // setVideoDuration: function(event){
    //     var video = this.$().get(0);
    //     if (video.readyState > 0) {
    //         var vid_duration = Math.round(video.duration);
    //         var controller = this.get('controller');
    //         if (controller) {
    //             controller.set('duration', vid_duration);
    //             if (controller.get('currentTime') === 0) {
    //                 controller.send('timeUpdate', 0, event.target.duration);
    //             }
    //         }
    //     }
    // }
 });
export default zipPlayerView;
