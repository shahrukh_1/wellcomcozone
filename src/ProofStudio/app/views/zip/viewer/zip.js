import Ember from "ember";

var ZipView = Ember.View.extend({
    controls: false,
    tagName: 'div',
    classNames: ['zip-view'],

    didInsertElement: function(){
        this.get('controller').send('changeBackgroundColorPicker');
    }
});
export default ZipView;
