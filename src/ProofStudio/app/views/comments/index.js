import Ember from "ember";

var CommentsIndexView = Ember.View.extend({
    keyUp: function(ev) { 
      
        if(Ember.$('#editCommentArea').length > 0){
            if (ev.keyCode === 13 && !ev.shiftKey) {
                this.get("controller.controllers.comments/item").send("addLineInEditComment");
              } else {
                  this.get("controller.controllers.comments/item").send("getEditCommentContent");
              }
        } else {
            if (ev.keyCode === 13 && !ev.shiftKey) {
               this.get("controller").send("addLineInComment");
              } else {
                  this.get("controller").send("getCommentContent");
              }
    }
      },
    didInsertElement : function(){
        this._super();
        var self = this;
        this.get('controller').reDrawCommentToMarkerLine();
        Ember.run.scheduleOnce('afterRender', this, function(){
            this.$("#comments-modal").draggable({ handle: ".modal-header",
                drag: function() {
                   self.get('controller').reDrawCommentToMarkerLine();
                }
            });
            this.$("#commentArea").resizable({
                handles: "s"
            });
            this.$(".ui-resizable-handle").addClass("fa fa-angle-double-down");
        });
    }
});
export default CommentsIndexView;
