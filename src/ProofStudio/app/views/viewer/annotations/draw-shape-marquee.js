import Ember from "ember";
import AppConstants from "appkit/config/constants";

var DrawShapeMarquee = Ember.Mixin.create({
  // marqueeshape: - the shape to be drawn
  // marqueeFabricCanvas - image or video fabric canvas
  isDrawSelectedAgain: false,
  marqueeX0:0,
  marqueeY0:0,
  marqueeStarted: false,
  marqueeMousedown(ev) {
    var shape = this.get('marqueeShape');
    var canvas = this.get('marqueeFabricCanvas');
    if (!shape || !canvas)
      return;

    this.set('marqueeStarted', true);
    this.set('marqueeX0', ev.x);
    this.set('marqueeY0', ev.y);
    canvas.add(shape);
  },

  marqueeMousemove(ev) {
    var shape = this.get('marqueeShape');
    var canvas = this.get('marqueeFabricCanvas');
    if (!shape || !canvas)
      return;

    if (this.get('marqueeStarted') == false) {
      return;
    }

    var x0 = this.get('marqueeX0'),
      y0 = this.get('marqueeY0');
    var x = Math.min(ev.x, x0),
      y = Math.min(ev.y, y0),
      w = Math.abs(ev.x - x0),
      h = Math.abs(ev.y - y0);

    if (!w || !h) {
      return;
    }

    shape.left = x;
    shape.top = y;
    shape.width = w;
    shape.height = h;

    shape.setCoords();

    canvas.renderAll();
  },

  marqueeMouseup(ev) {
    if (!this.get('marqueeStarted'))
      return;

    var shape = this.get('marqueeShape');
    var canvas = this.get('marqueeFabricCanvas');

    if (!shape || !canvas) {
      return;
    }

    this.marqueeMousemove(ev);
    this.set('marqueeStarted', false);
    canvas.setActiveObject(shape);
  },

  //----------------- marque rectangle draw -------------------------------
  startDrawMarqueeRectangle(mouseEventDetails) {
    var canvas = this.get('marqueeFabricCanvas');
    if (!canvas)
      return;

    if (this.get('isSelectedObjectChanging')) {
      return;
    }

    var clickedPoint = canvas.getPointer(mouseEventDetails);

    var drawColor = this.get('drawColor');
    var lineWidth = this.get('selectedDrawWidth');

    var rect = new fabric.Rect({
      top: clickedPoint.y,
      left: clickedPoint.x,
      width: 1,
      height: 1,
      fill: 'rgba(0,0,0,0)',
      selectable: true,
      stroke: drawColor,
      strokeWidth: lineWidth,
      cornerSize: 6,
    });

    var self = this;

    rect.on({'scaling': function() {
      self.doNotChangeWidthOnScaling(this);
    }
    });

    this.set('marqueeShape', rect);
    this.marqueeMousedown(clickedPoint);
    this.set('isDrawSelectedAgain', true);
  },

  doNotChangeWidthOnScaling(obj) {
    var w = obj.width * obj.scaleX,
      h = obj.height * obj.scaleY;

    obj.set({
      'height'     : h,
      'width'      : w,
      'scaleX'     : 1,
      'scaleY'     : 1
    });
  },

  adjustMarqueeRectangleOnMouseMove(mouseEventDetails) {
    var canvas = this.get('marqueeFabricCanvas');
    if (!canvas)
      return;
    if (this.get('isSelectedObjectChanging')) {
      return;
    }

    var clickedPoint = canvas.getPointer(mouseEventDetails);

    this.marqueeMousemove(clickedPoint);

  },
  storeMarqueeRectangleOnMouseUp(mouseEventDetails) {
    var canvas = this.get('marqueeFabricCanvas');
    if (!canvas)
      return;

    if (this.get('isSelectedObjectChanging')) {
      if('isDrawSelectedAgain'){ 
        var clickedPoint = canvas.getPointer(mouseEventDetails);
       this.marqueeMouseup(clickedPoint);
       this.canvasMarqueCreatedEvent();
       }
       else{
         return;
       }
      }
      else{
          
    var clickedPoint = canvas.getPointer(mouseEventDetails);

    this.marqueeMouseup(clickedPoint);

    this.canvasMarqueCreatedEvent();
      }
  },

  canvasMarqueCreatedEvent: function() {
    var point = [0, 0];
    var pageId = this.get('controller').get('model.currentPage.id');
    this.set('commentType', AppConstants.CommentTypes.MARQUEE_RECTANGLE);
    this.get('controller').send('newComment', {Point: point, PageId: pageId, CommentType: AppConstants.CommentTypes.MARQUEE_RECTANGLE});

    this.sendMarkupsAndShapesToCommentPopup();

    this.set('isSelectedObjectChanging', false);

    //var shape = self.get('marqueeShape');
    //var canvas = self.get('marqueeFabricCanvas');
    //canvas.setActiveObject(shape);
  },

  changeSelectedMarqueRectangleWidth: function(width) {
    var canvas = this.get('marqueeFabricCanvas');
    if (!canvas)
      return;

    var activeObject = canvas.getActiveObject();
    if (!activeObject) {
      return;
    }

    if (activeObject.isType('rect')) {
      activeObject.setStrokeWidth(width);
      canvas.renderAll();
    }
  }
});

export default DrawShapeMarquee;
