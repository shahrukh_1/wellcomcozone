import Ember from "ember";

var NotificationsView = Ember.View.extend({

    initializeView: function(){
        this.get('controller').send('initializeNotificationsView', this.get('controller'));
    }.observes('controller.model.model.currentPage.id'),

    onAfterRender: function() {
        var self = this;
        Ember.run.later(function(){
            self.initializeView();
        });
    }.on('afterRender')

});
export default NotificationsView;
