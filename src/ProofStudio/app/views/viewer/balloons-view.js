import Ember from "ember";
import AppConstants from "appkit/config/constants";

var BalloonsView = Ember.View.extend({
    shapeColor: '',
    lineWeight: '',
    classNames: ['czn-balloons-view'],
    autoArrange: true,

    initializeView: function(){
        this.get('controller').send('initializeBalloonsView', this);
    }.observes('controller.model.model.currentPage.id'),

    initializeAnnotations: function() {
        var annotations = this.get('controller.annotations');
        this.set('annotations', annotations || []);
        this.readCookie();
        this.arrangeBalloons();
    }.observes('controller.annotations'),

    onAfterRender: function() {
        var self = this;
        Ember.run.later(function(){
            self.initializeView();
            //TODO the following lines should be refactor asap
            // workaround to display the balloons when the user enter the page and click the 'all tickets' button and did not (de)select any user before this
            Ember.run.later(function(){
                var user = (self.get('controller.controllers.sidebar/index.users') || []).filterBy('selected', true).get('firstObject');
                if (user){
                    user.set('selected', false);
                    Ember.run.later(function(){
                        user.set('selected', true);
                    });
                }
            });
            self.readCookie();
        });

        Ember.$(window).resize(function(){
            self.arrangeBalloons();
            self.syncPosition();
        });
    }.on('afterRender'),

    readCookie: function(){
        var currentView = this.$(),
            currentPageId = parseInt(this.get('controller.model.model.currentPage.id'));

        if (this.get('isDeleted') || this.get('isDestroyed')){
            return;
        }

        this.set('autoArrange', true);

        var session = this.get('controller.session');
        if (currentView) {
            var cookieValue = (typeof(session) !== 'undefined' && typeof(session.cookie) !== 'undefined') ? session.cookie : { pages: [] },
                pages = cookieValue.pages.filter(function(page){
                return page.id === currentPageId;
            });
            var currentPage = pages.length > 0 ? pages[0]: null;
            if (currentPage) {
                this.set('autoArrange', currentPage.autoArrange);
                this.set('previousBoxPositions', currentPage.annotations);
            }
        }
    },

    saveCookie: function(){
        var currentView = this.$(),
            currentPageId = parseInt(this.get('controller.model.model.currentPage.id'));

        if (currentView && currentView.is(":visible")) {

            var cookieValue = this.get('controller.session.cookie') || { pages: [] };

            var pages = cookieValue.pages.filter(function(page){
                return page.id === currentPageId;
            });

            var currentPage = (pages.length > 0) ? pages[0] : {
                autoArrange: this.get('autoArrange'),
                id: currentPageId,
                annotations: []
            };
            currentPage.annotations = [];

            var annotations = currentView.find('.czn-balloon-annotation');
            Ember.$.each(annotations, function (i, v) {
                var annotation = Ember.$(v);
                currentPage.annotations[currentPage.annotations.length] = { id: annotation.attr('annotation-id'), left: annotation.css('left'), top: annotation.css('top') };
            });

            if (pages.length === 0){
                cookieValue.pages[cookieValue.pages.length] = currentPage;
            }
            this.get('controller.session').set('cookie', cookieValue);
        }
    },

    showLayer: function(){
        if (this.get('isDeleted') || this.get('isDestroyed')){
            return;
        }

        var viewItem = this.$();
        if (viewItem) {
            viewItem.show();
        }
        this.readCookie();
        this.arrangeBalloons();
        this.syncPosition();
        this.showExpandCollapse();
        this.get('controller').send('doneAnnotating');
        this.syncPosition();

        Ember.run.later(function(){
            Ember.$(".czn-pager").hide();
            Ember.$(".btnShowPagination").hide();
            Ember.$(".czn-page-viewer").hide();
            Ember.$(".btnAnnotate").hide();
            Ember.$(".czn-decision-list").hide();
            Ember.$(".czn-title-version").hide();
            Ember.$("section.czn-annotation-type").hide();
            Ember.$(".btnGoToStart,.btnGoBackward,.btnPause,.btnPlay,.btnForward,.btnGoToEnd").css('visibility', 'hidden');
            Ember.$(".czn-seekbar-time,.czn-slider-video").hide();
            Ember.$("div.leaflet-marker-icon").hide();

            Ember.$(".btnResetBalloonsPositions").show();
        });
    },

    hideLayer: function(){
        var viewItem = this.$();
        if (viewItem) {
            viewItem.hide();
        }
        Ember.$(".czn-pager").show();
        Ember.$(".btnShowPagination").show();
        Ember.$(".czn-page-viewer").show();
        Ember.$(".btnAnnotate").show();
        Ember.$(".czn-decision-list").show();
        Ember.$(".czn-title-version").show();
        Ember.$("section.czn-annotation-type").show();
        Ember.$("li.czn-list-annotation.selected").removeClass('selected');
        Ember.$(".btnGoToStart,.btnGoBackward,.btnPause,.btnPlay,.btnForward,.btnGoToEnd").css('visibility', 'visible');
        Ember.$(".czn-seekbar-time,.czn-slider-video").show();
        Ember.$("div.leaflet-marker-icon").show();

        Ember.$(".btnResetBalloonsPositions").hide();
        Ember.$("i.collapse").hide();

        var canvas = this.get('imageFabricCanvas');
        if (canvas) {
            canvas.getObjects().forEach(function (loadedObject) {
                if (loadedObject.annotationId > 0 && loadedObject.isPin === false) {
                    loadedObject.visible = false;
                }
                else if (loadedObject.annotationId > 0 && loadedObject.isPin === true && loadedObject.isPinSelected === true) {
                    loadedObject.visible = false;
                }
                else if (loadedObject.annotationId > 0 && loadedObject.isPin === true && loadedObject.isPinSelected === false) {
                    loadedObject.visible = true;
                }
            });
            canvas.renderAll();
        }
    },

    showExpandCollapse: function(){
        var currentView = this.$();
        var self = this;
        if (currentView && currentView.is(":visible")) {
            var annotations = currentView.find('.czn-balloon-annotation');
            Ember.$.each(annotations, function (i, v) {
                var annotation = Ember.$(v);
                if (annotation.height() > 60) {
                    annotation.css({height: '56px'});
                    annotation.find('i.expand').show();
                }
                self.drawLine(annotation.attr('annotation-id'));
            });
        }
    },

    syncCanvasPositionObservable: function(){
        this.syncPosition();
    }.observes('controller.model.mapPosition'),

  initializeData: function(map, videoPlayer){
    var self = this;

    if (this.get('isDestroyed')) {
      return;
    }
    this.set('map', map);
    this.set('video', videoPlayer);

    //if (map) {
    //  map.on('dragstart', function () {
    //
    //  });
    //  map.on('dragend', function () {
    //    self.syncPosition();
    //  });
    //  map.on('movestart', function () {
    //
    //  });
    //  map.on('moveend', function () {
    //    self.syncPosition();
    //  });
    //  map.on('zoomanim', function () {
    //
    //  });
    //  map.on('zoomend', function () {
    //    self.syncPosition();
    //  });
    //}
  },

    getBrightness: function(hex){
        //calculate the perceived brightness
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex),
            r = parseInt(result[1], 16),
            g = parseInt(result[2], 16),
            b = parseInt(result[3], 16);
            return 0.299*r + 0.587*g + 0.114*b;
    },

    displayAnnotations: function(){
        var annotations = this.get('annotations');
        var self = this;
        var currentView = this.$();
        //remove all previous balloons
        if (currentView) {
            currentView.find(".czn-balloon-annotations").remove();
            currentView.find(".czn-balloon-annotation-lines").remove();
        }
        var fullSize = this.get('controller.model.size'),
            fabricCanvas = null;
        if (currentView && annotations && fullSize){
            var linesParent = Ember.$("<div class=\"czn-balloon-annotation-lines\"></div>").appendTo(self.$());
            var annotationsParent = Ember.$("<div class=\"czn-balloon-annotations\"></div>").appendTo(self.$());

            var currentPageId = this.get('controller.model.model.currentPage.id') || [];
            var parentAnnotations = annotations.filter(function(annotation){
                return !annotation.get('Parent') && annotation.get('Page.id') === currentPageId && (annotation.get('Creator.selected') === true || annotation.get('ExternalCreator.selected') === true);
            });
            var scale = this.getScale();
            parentAnnotations.forEach(function(annotation) {
                var annotationType = annotation.get('CommentType'),
                    annotationIconClass = 'fa-pencil';
                if(AppConstants.Annotation.AnnotationType.hasOwnProperty(annotationType)) {
                    annotationIconClass =  AppConstants.Annotation.AnnotationType[annotationType].icon;
                }

                var annotationId = annotation.get('id');
                var annotationComment = "<div class=\"czn-balloon-annotation-comment\">" + annotation.get('Comment') + "</div>";
                var childAnnotations = annotations.filter(function(localAnnotation){
                    return localAnnotation.get('Parent') === annotation;
                });
                if (childAnnotations){
                    childAnnotations.forEach(function(localAnnotation){
                        annotationComment += "<div class=\"czn-balloon-annotation-comment-child\">" + localAnnotation.get('Comment') + "</div>";
                    });
                }
                var creator = annotation.get('Creator') || annotation.get('ExternalCreator');
                var userColor = creator.get('UserColor');
                var brightness = self.getBrightness(userColor);

                var element = Ember.$("<div class=\"czn-balloon-annotation\"><div class=\"czn-balloon-annotation-icon2 " + (brightness <= 128 ? "czn-balloons-color-black" : "czn-balloons-color-white") + "\" style=\"background-color: " + userColor + ";\"><div class=\"czn-icon2 fa " + annotationIconClass + " fa-2x\"></div></div><i class=\"expand fa fa-angle-double-down\"></i><i class=\"collapse fa fa-angle-double-up\"></i>" + annotationComment + "</div>").appendTo(annotationsParent);
                element.attr('annotation-id', annotationId).attr('annotation-x', annotation.get('CrosshairXCoordScaledForHighRes')).attr('annotation-y', annotation.get('CrosshairYCoordScaledForHighRes'));

                var annotationIndex = annotation.get('AnnotationNumber'),
                    svg = AppConstants.PinMarker;
                if (annotationIndex && annotationIndex > 9){
                    svg = svg.replace('<g transform="translate(50 100)"', '<g transform="translate(30 100)"');
                }

                var normalSVG = svg.replace(/#color/g,"#ffffff").replace(/#index/g, annotationIndex);
                var selectedSVG = svg.replace(/#color/g,"#ff0000").replace(/#index/g, annotationIndex);

                element.find('i.expand').click(function(){
                    element.css({height: 'auto'});
                    element.find('i.expand').hide();
                    element.find('i.collapse').show();
                    self.drawLine(annotationId);
                });
                element.find('i.collapse').click(function(){
                    element.css({height: '56px'});
                    element.find('i.collapse').hide();
                    element.find('i.expand').show();
                    self.drawLine(annotationId);
                });

                var currentLine = Ember.$("<div class=\"czn-balloon-annotation-line\"></div>").appendTo(linesParent);
                currentLine.attr('annotation-id', annotation.get('id')).attr('annotation-x', annotation.get('CrosshairXCoordScaledForHighRes')).attr('annotation-y', annotation.get('CrosshairYCoordScaledForHighRes'));
            });
        }
        this.syncPosition();

        if (currentView) {
            currentView.find(".czn-balloon-annotation").draggable({
                drag: function (e) {
                    var element = Ember.$(e.target);
                    var annotationId = element.attr('annotation-id');
                    self.drawLine(annotationId);
                },
                stop: function () {
                    self.set('autoArrange', false);
                    self.saveCookie();
                }
            });
        }
        this.showExpandCollapse();
      }.observes('annotations', 'controller.model.mapScale'),

    fabricObjectClicked: function(e){
        if (e.target !== null && typeof(e.target) !== 'undefined' ) {
            var annotationId = e.target.annotationId;
            var fabricCanvas = e.target.canvas;
            var currentElementIsVisible = e.target.isPinSelected === true;
            if (e.target.isPin === true) {
                // hide all elements from canvas
                fabricCanvas.getObjects().forEach(function (element) {
                    if (element.isPin === false && element.annotationId > 0) {
                        element.visible = false;
                    }
                    if (element.annotationId > 0 && element.isPin === true && element.isPinSelected === true) {
                        element.visible = false;
                    }
                });
                // show only the paths that coresponds with the clicked annotation id
                if (!currentElementIsVisible) {
                    fabricCanvas.getObjects().forEach(function (element) {
                        if (element.annotationId === annotationId && element.isPin === false) {
                            element.visible = true;
                        }
                        if (element.annotationId === annotationId && element.isPin === true && element.isPinSelected === true) {
                            element.visible = true;
                        }
                    });
                }
            }
        }
    },

    getScale: function(){
        var mapScale = this.get('controller.model.mapScale') || 1;
        return {x: mapScale, y: mapScale};
    },

    compareTwoValues: function(a,b){
        if (a===b) {
            return 0;
        }
        else
            if (a > b) {
                return 1;
            }
            else {
                return -1;
            }
    },

    sortjQueryElements: function( arr, field, asc){
        var self = this;
        return arr.sort(function(x, y){
            var a = parseFloat(Ember.$(x).attr(field));
            var b = parseFloat(Ember.$(y).attr(field));
            if (!asc){
                return self.compareTwoValues(b, a);
            }
            else{
                return self.compareTwoValues(a, b);
            }
        });
    },

    getGroupNr: function(x,y){
        var width = this.get('fullSize.width'),
            height = this.get('fullSize.height'),
            coords = {
                A: {x: 0, y: 0},
                B: {x: width, y: 0},
                C: {x: width, y: height},
                D: {x: 0, y: height}
            };
        // diagonals AC and DB divides the screen in four regious
        if (this.compareTwoValues((coords.A.y - coords.C.y)/(coords.A.x - coords.C.x),(y - coords.A.y)/(x - coords.A.x)) >= 0 && this.compareTwoValues((coords.B.y - coords.D.y)/(coords.B.x - coords.D.x),(y - coords.D.y)/(x - coords.D.x)) > 0){
            return 1; // above the AC and above the DB, including the points from AO where O is the center of the rectangle
        }
        else if (this.compareTwoValues((coords.A.y - coords.C.y)/(coords.A.x - coords.C.x),(y - coords.A.y)/(x - coords.A.x)) > 0 && this.compareTwoValues((coords.B.y - coords.D.y)/(coords.B.x - coords.D.x),(y - coords.D.y)/(x - coords.D.x)) <= 0){
            return 2; // above the AC and under the DB, including the points from BO where O is the center of the rectangle
        }
        else if (this.compareTwoValues((coords.A.y - coords.C.y)/(coords.A.x - coords.C.x),(y - coords.A.y)/(x - coords.A.x)) <= 0 && this.compareTwoValues((coords.B.y - coords.D.y)/(coords.B.x - coords.D.x),(y - coords.D.y)/(x - coords.D.x)) < 0){
            return 3; // under the AC and under the DB, including the points from CO where O is the center of the rectangle
        }
        else if (this.compareTwoValues((coords.A.y - coords.C.y)/(coords.A.x - coords.C.x),(y - coords.A.y)/(x - coords.A.x)) < 0 && this.compareTwoValues((coords.B.y - coords.D.y)/(coords.B.x - coords.D.x),(y - coords.D.y)/(x - coords.D.x)) >= 0){
            return 4; // under the AC and above the DB, including the points from DO where O is the center of the rectangle
        }
        return 0;
    },

    arrangeBalloons: function(){
        var autoArrange = this.get('autoArrange');
        if (autoArrange === true){
            this._autoArrangeBalloons();
        }
        else{
            this._manualArrangeBalloons();
        }
    },

    _manualArrangeBalloons: function(){
        var margin = 20,
            annotationBoxWidth = 185,
            annotationBoxHeight = 60,
            currentView = this.$(),
            positions = this.get('previousBoxPositions');

      var map = this.get('controller').get('approval.map');

      if(map) {
        var mapSize = map.getSize();
        var windowWidth = mapSize[0],
          windowHeight = mapSize[1];
        if (positions && currentView && !this.get('autoArrange')) {
          positions.forEach(function (position) {
            var annotationBox = currentView.find(".czn-balloon-annotation[annotation-id='" + position.id + "']");
            annotationBox.css({
              left: position.left,
              top: position.top
            });
            annotationBox.addClass('manualArrange');
          });

          var orphanAnnotations = currentView.find(".czn-balloon-annotation");
          Ember.$.each(orphanAnnotations, function (ii, vv) {
            var annotation = Ember.$(vv);
            var annotationId = annotation.attr('annotation-id');
            if (!annotation.hasClass('manualArrange')) {

              var left = (Math.random() * (windowWidth - margin - annotationBoxWidth)) + margin;
              var top = (Math.random() * (windowHeight - margin - annotationBoxHeight)) + margin;

              annotation.css({
                left: left + 'px',
                top: top + 'px'
              });

              positions[positions.length] = {id: annotationId, left: left + 'px', top: top + 'px'};
            }
          });
          currentView.find('.manualArrange').removeClass('manualArrange');
          this.set('previousBoxPositions', positions);
        }
        this.syncPosition();
      }
    },

    _autoArrangeBalloons: function(){
        var self = this;
        var margin = 20;
        var annotationBoxWidth = 185;
        var annotationBoxHeight = 60;
        var currentView = this.$();
        if (!currentView || currentView.css('display') == 'none' || !this.get('autoArrange')){
            return;
        }

        var map = this.get('controller').get('approval.map');

        if(map) {
          var mapSize = map.getSize();
          var windowWidth = mapSize[0],
            windowHeight = mapSize[1],
            lines = [];
          lines[0] = {}; // there should not be a groupNr = 0
          lines[1] = {
            startX: margin + (annotationBoxWidth + margin),
            startY: margin,
            annotationsNr: 0,
            maxAnnotationsNr: parseInt((windowWidth - 3 * margin - 2 * annotationBoxWidth) / (annotationBoxWidth + margin))
          };
          lines[2] = {
            startX: lines[1].startX + lines[1].maxAnnotationsNr * (annotationBoxWidth + margin),
            startY: margin,
            annotationsNr: 0,
            maxAnnotationsNr: parseInt((windowHeight - 2 * margin) / (annotationBoxHeight + margin))
          };
          lines[3] = {
            startX: 2 * margin + annotationBoxWidth,
            startY: lines[2].startY + (lines[2].maxAnnotationsNr - 1) * (annotationBoxHeight + margin),
            annotationsNr: 0,
            maxAnnotationsNr: lines[1].maxAnnotationsNr
          };
          lines[4] = {
            startX: margin,
            startY: margin,
            annotationsNr: 0,
            maxAnnotationsNr: lines[2].maxAnnotationsNr
          };
          if (lines[1].maxAnnotationsNr >= 1) {
            for (var i = 1; i <= lines[1].maxAnnotationsNr; i++) {
              lines[4 + i] = {
                startX: lines[1].startX + (i - 1) * (annotationBoxWidth + margin),
                startY: lines[1].startY + annotationBoxHeight + margin,
                annotationsNr: 0,
                maxAnnotationsNr: lines[2].maxAnnotationsNr - 2
              };
            }
          }

          var annotations = currentView.find(".czn-balloon-annotation");

          currentView.find("div.czn-balloon-annotation-line").show();
          currentView.find("div.czn-balloon-annotation").show();

          if (annotations) {
            Ember.$.each(annotations, function (i, v) {
              var annotation = Ember.$(v);
              var x = parseFloat(annotation.attr('annotation-x'));
              var y = parseFloat(annotation.attr('annotation-y'));
              var groupNr = self.getGroupNr(x, y);
              annotation.attr('group', groupNr);
            });

            for (var groupNr = 1; groupNr <= 4; groupNr++) {
              annotations = currentView.find(".czn-balloon-annotation[group='" + groupNr + "']");
              switch (groupNr) {
                case 1:
                  annotations = self.sortjQueryElements(annotations, 'annotation-x', true);
                  break;
                case 2:
                  annotations = self.sortjQueryElements(annotations, 'annotation-y', true);
                  break;
                case 3:
                  annotations = self.sortjQueryElements(annotations, 'annotation-x', true);
                  break;
                case 4:
                  annotations = self.sortjQueryElements(annotations, 'annotation-y', true);
                  break;
              }

              annotations.removeClass('orphan');

              Ember.$.each(annotations, function (i, v) {
                var annotation = Ember.$(v);
                switch (groupNr) {
                  case 1:
                  case 3:
                    if (lines[groupNr].annotationsNr < lines[groupNr].maxAnnotationsNr) {
                      annotation.css({
                        left: (lines[groupNr].startX + lines[groupNr].annotationsNr * (annotationBoxWidth + margin)) + 'px',
                        top: (lines[groupNr].startY) + 'px'
                      });
                      lines[groupNr].annotationsNr += 1;
                    }
                    else {
                      annotation.addClass('orphan');
                    }
                    break;
                  case 2:
                  case 4:
                    if (lines[groupNr].annotationsNr < lines[groupNr].maxAnnotationsNr) {
                      annotation.css({
                        left: (lines[groupNr].startX) + 'px',
                        top: lines[groupNr].startY + lines[groupNr].annotationsNr * (margin + annotationBoxHeight) + 'px'
                      });
                      lines[groupNr].annotationsNr += 1;
                    }
                    else {
                      annotation.addClass('orphan');
                    }
                    break;
                }
              });
            }
            var orphanAnnotations = currentView.find(".czn-balloon-annotation.orphan");
            Ember.$.each(orphanAnnotations, function (j, v) {
              var annotation = Ember.$(v);
              var i = 1;
              while (i < lines.length) {
                if (lines[i].annotationsNr < lines[i].maxAnnotationsNr) {
                  if ((i === 1 || i === 3)) {
                    annotation.css({
                      left: (lines[i].startX + lines[i].annotationsNr * (annotationBoxWidth + margin)) + 'px',
                      top: (lines[i].startY) + 'px'
                    });
                    lines[i].annotationsNr += 1;
                    break;
                  }
                  else {
                    // this is for lines 2 and 4 and also for all supplementary lines
                    annotation.css({
                      left: (lines[i].startX) + 'px',
                      top: lines[i].startY + lines[i].annotationsNr * (margin + annotationBoxHeight) + 'px'
                    });
                    lines[i].annotationsNr += 1;
                    break;
                  }
                }
                if (i === lines.length - 1) {
                  annotation.css({
                    left: ((Math.random() * (windowWidth - margin - annotationBoxWidth)) + margin) + 'px',
                    top: ((Math.random() * (windowHeight - margin - annotationBoxHeight)) + margin) + 'px'
                  });
                }
                i++;
              }
            });
          }
          this.syncPosition();
        }
    },

    syncPosition: function(){
        var self = this,
            fullSize = this.get('controller.model.model.size'),
            mapPosition = this.get('controller.model.mapPosition'),
            canvas = this.get('imageFabricCanvas'),
            currentView = this.$(),
            scale = this.getScale();

        if (currentView && currentView.css('display') != 'none') {
            currentView.find("[annotation-id]").each(function (i, v) {
                var annotationId = Ember.$(v).attr('annotation-id');
                self.drawLine(annotationId);
            });
        }
    },

    getAngle: function(A,B,C){
        // get angle ABC in degree
        var AB = Math.sqrt(Math.pow(B.x-A.x,2)+ Math.pow(B.y-A.y,2));
        var BC = Math.sqrt(Math.pow(B.x-C.x,2)+ Math.pow(B.y-C.y,2));
        var AC = Math.sqrt(Math.pow(C.x-A.x,2)+ Math.pow(C.y-A.y,2));
        return Math.acos((BC*BC+AB*AB-AC*AC)/(2*BC*AB)) * 180 / Math.PI;
    },

    drawLine: function(annotationId){
        var mapPosition = this.get('controller.model.mapPosition'),
            mapCenter = this.get('controller.model.mapCenter'),
            mapScale = this.get('controller.model.mapScale') || 1,
            video = this.get('video'),
            dbPoint = {x: 0, y: 0},
            vPoint,
            currentView = this.$(),
            annotation = currentView.find("div.czn-balloon-annotation[annotation-id='" + annotationId + "']"),
            line = currentView.find("div.czn-balloon-annotation-line[annotation-id='" + annotationId + "']"),
            xPoint = line.attr('annotation-x'),
            yPoint = line.attr('annotation-y'),
            pointZeroPixels = {x: 0, y: 0},
            pointPixels = {x: 0, y: 0},
            controller = this.get('controller');

        if (mapPosition){
            pointPixels = {x: parseInt(xPoint), y: parseInt(yPoint)};

            // apply map.getPixelFromCoordinate to get the actual pixel for pin
            controller.send('getMapPixelFromCoordinate', [pointPixels.x, - pointPixels.y]);
            var newPixelPoint = controller.get('currentPixelFromCoordinate');
            dbPoint = {x: (newPixelPoint[0]) , y: newPixelPoint[1]};
        }
        else if (video){

            var padding = 70,
                videoOffset = video.offset(),
                scale = this.getScale();

            pointZeroPixels = { x: parseFloat(videoOffset.left) + padding, y: parseFloat(videoOffset.top) + padding };
            pointPixels = { x: parseFloat(xPoint) * scale.x , y: parseFloat(yPoint) * scale.y };
            dbPoint = {x: (pointPixels.x + pointZeroPixels.x) ,y: (pointPixels.y + pointZeroPixels.y - 46) };
        }

        var offset = annotation.position(),
            angle = this.getAngle(dbPoint, {x: offset.left, y: offset.top + annotation.outerHeight()/2}, {x: offset.left + annotation.outerWidth(), y: offset.top + annotation.outerHeight()/2});

        vPoint = {
            top: offset.top + annotation.outerHeight()/2,
            left: angle <= 90 ? (offset.left + annotation.outerWidth()) : (offset.left)
        };

        var x1 = 0,x2 = 0,y1 = 0,y2 = 0;
        if (dbPoint.x < vPoint.left){
            x1 = dbPoint.x;
            y1 = dbPoint.y;
            x2 = vPoint.left;
            y2 = vPoint.top;
        }
        else{
            x1 = vPoint.left;
            y1 = vPoint.top;
            x2 = dbPoint.x;
            y2 = dbPoint.y;
        }

        var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
        //angle  = Math.atan((y2 - y1)/(x2 - x1)) * 180 / Math.PI;

        angle = Math.atan2(y2 - y1, x2 - x1);
        if (angle < 0) {
            angle += 2 * Math.PI;
        }
        //convert to degrees and rotate so 0 degrees = 3 o'clock
        angle = (angle * 180 / Math.PI) % 360;
        angle = (angle > 89 && angle < 220) ? angle : (angle + 0.5);

        line.css({
            '-ms-transform': 'rotate(' + angle.toFixed(6) + 'deg)',
            '-webkit-transform': 'rotate(' + angle.toFixed(6) + 'deg)',
            transform: 'rotate(' + angle.toFixed(6) + 'deg)',
            left: x1.toFixed(6) + 'px',
            top: y1.toFixed(6) + 'px',
            width: length.toFixed(6) + 'px'
        }).width();
    }
});
export default BalloonsView;
