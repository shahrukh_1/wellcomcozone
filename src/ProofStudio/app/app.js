import Ember from 'ember';
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';

Ember.MODEL_FACTORY_INJECTIONS = true;

var App = Ember.Application.extend({
  modulePrefix: 'appkit', // TODO: loaded via config
  Resolver: Resolver
});
//application error handling references - https://speakerdeck.com/elucid/ember-errors-and-you

loadInitializers(App, 'appkit');

export default App;
