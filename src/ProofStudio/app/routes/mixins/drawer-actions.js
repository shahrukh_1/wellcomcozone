import Ember from "ember";

var DrawerActions = Ember.Mixin.create({
    actions:{
        initAddMarker: function(annotationType, editMode){
            editMode = editMode || false;

            this.sendActionToViewer('initAddMarkerInViewerController', annotationType, editMode);
        },

        initAddMarkerInDrawerActions: function(annotationType, editMode){
          this.send('initAddMarker',annotationType, editMode);
        },
        closeAddMarker: function(){
            this.sendActionToViewer('closeAddMarker');

            this.commentController.send('hide');
        },
        initColorDensitometer: function(){
          this.sendActionToViewer('initColorDensitometer');
        },

        drawingModeInDrawerActions:function(enter, lineWidth, lineColor){
          this.sendActionToViewer('drawingModeInViewerController', enter, lineWidth, lineColor);
        },
        clearCanvas: function(){
            this.sendActionToViewer('clearCanvas');
        },
        hideCanvas: function(){
            this.sendActionToViewer('hideCanvas');
        },
        removeMarkerOverlay: function(){
            this.sendActionToViewer('removeMarkerOverlay');
        },
        addShapeToCanvas:function(shape, color, mouseEventDetails){
                viewerController.send('addShapeToCanvas', shape, color, mouseEventDetails);
        },
        //drawingSelectionCreated: function(){
        //    Ember.$("input[name='draw-type']").first().trigger('click');
        //},
        clearSelection: function(){
            this.sendActionToViewer('clearSelection');
        },
        showSpecificButtons: function(isCustomShape){
            this.controllerFor('navigation/annotation').send('showSpecificButtons', isCustomShape);
        }
    }
});
export default DrawerActions;
