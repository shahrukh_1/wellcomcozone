import Ember from "ember";
import CurrentViewer from 'appkit/routes/mixins/current-viewer';

var AnnotationActions = Ember.Mixin.create(CurrentViewer, {
    actions: {
        selectAnnotation: function(annotation) {
          this.controllerFor('comments/index').set('annotationID', annotation.id);
          this.controllerFor('comments/index').set('isSideAnnotation', true);
          this.controllerFor('comments/index').set('selectedUserToWhomMailShouldBeSent',[]);
          this.controllerFor('comments/index').set('indexes',[]);
          this.controllerFor('comments/index').set('savedAnnotationChecklist',[]);
          var tempAnnotationChecklists = [];
          var checklistItemdetails = annotation.get('ApprovalannotationChecklistitems').content;
          checklistItemdetails.forEach(function(item){
            var temp = {ItemValue : item._data.ItemValue, ItemID : item._data.id};
            tempAnnotationChecklists.push(temp);
          });
          this.controllerFor('comments/index').set('tempAnnotationChecklist', tempAnnotationChecklists);
          this.controllerFor('comments/index').set('checklistHistoryCount', annotation.get('AnnotationAllchecklistitems').content.length);
            var viewerController = this.GetCurrentViewerController();

            if(viewerController && !viewerController.get('isDestroyed')) {

              var balloonsView = Ember.$(".czn-balloons-view");
              var balloonsViewIsVisible = balloonsView && balloonsView.length > 0 && balloonsView.css('display') !== 'none';

                if (balloonsViewIsVisible){
                    viewerController.deselectAnnotations();
                    annotation.set('selected', true);
                    return false;
                }

                if(annotation.get('selected') === true) {
                    viewerController.deselectAnnotations(annotation);
                    this.commentController.send('hide');
                }
                else {
                  var approvalController = this.controllerFor('approval');
                  var currentVersion = approvalController.get('currentVersion');

                  if(annotation.get("Page.Approval.id") === viewerController.get("currentPage.Approval.id") && viewerController.get('isClone') == currentVersion.get('isClone')) {
                    if (annotation.get('Page.id') !== viewerController.get('currentPage.id') && window.AppkitENV.approvalType != "Zip") {
                      viewerController.set('currentPage', annotation.get('Page'));
                      viewerController.setCurrentPageActive();
                      viewerController.deselectAnnotations(annotation);
                      viewerController.send('selectAnnotationAsync', annotation);
                    }
                    else {
                      viewerController.deselectAnnotations();

                      if (!annotation.get('isDisplayed')) {
                        viewerController.send('displaySingleAnnotation', annotation);
                      }
                      annotation.set('selected', true);

                      var annotations = [annotation];
                      this.store.all('annotation').forEach(function (localAnnotation) {
                        if (localAnnotation.get('Parent.id') === annotation.get('id')) {
                          annotations.push(localAnnotation);
                        }
                      });

                       
          var listOfCheckListItems = [];
          var arrayOfCheckList = annotation._data.Page.store.all("approval-checklist").content
          var annotationChecklistItems = annotations[annotations.length - 1].get('ApprovalannotationChecklistitems').content;
          if(annotationChecklistItems.length == 0){
            annotationChecklistItems = annotations[0].get('ApprovalannotationChecklistitems').content;
          }
          arrayOfCheckList.forEach(function(content){
            if(content._data.ChecklistItem != undefined){
                if(content._data.ChecklistItem.length > 0){
                  listOfCheckListItems.pushObjects(content._data.ChecklistItem);
                }
             }  
            });
        var listOfSortedCheckListItems=[];
        listOfSortedCheckListItems.pushObjects( listOfCheckListItems);
            if(annotationChecklistItems.length > 0){
                  listOfCheckListItems.forEach(function(listitem, index){
                    Ember.set(listOfSortedCheckListItems.objectAt(index),'ItemValue', null)
                    Ember.set(listOfSortedCheckListItems.objectAt(index),'IsItemChecked', false)
                    annotationChecklistItems.forEach(function(itemvalcontent){
                    if(listitem.Id == itemvalcontent._data.Id){
                      listOfSortedCheckListItems.removeAt(index);
                      listOfSortedCheckListItems.insertAt(index,itemvalcontent._data);
                      }
                    })
                  })
                }

                      this.controllerFor('comments/index').set('savedAnnotationChecklist', listOfSortedCheckListItems);
                      this.controllerFor('comments/index').set('approvalChecklistItems',[]);

                      this.commentController.set('model', annotations);
                      this.commentController.send('show');
                      viewerController.send('selectAnnotationAsync', null);

                      this.controllerFor('navigation/annotation').set('isDrawSelected', false);

                      viewerController.send('showCanvasForAnnotation', annotation.get('id'));

                      this.send("refreshMarkerPosition", annotation, viewerController);

                      Ember.run.later(function () {
                        Ember.$('#commentArea').attr('disabled', false);
                      });
                    }
                  }
                  else {
                      viewerController.send('changeSelectedVersion', currentVersion);
                      Ember.run.later(function() {
                        viewerController.send('displaySingleAnnotation', annotation);
                      }, 500);
                  }
                }
            }
        },

        hide: function(){
            this.commentController.send('hide');
        },
        hideComment: function(){
            var viewerController = this.get('currentViewerController');
            if (viewerController && !viewerController.get('isDestroyed')){
                viewerController.deselectAnnotations();
            }
            this.disconnectAnnotationsModal();
        },
        showComment: function() {
            this.renderAnnotationsModal();
        },
        setMarkerPosition: function(x, y){
            this.commentController.send('setMarkerPosition', x, y);
        },
        removeComment: function(comment) {
           this.controllerFor(this.sidebarController).send('removeComment', comment);
        },
        refreshMarkerPosition:function(annotation, viewerController){
          if(window.AppkitENV.selectedHtmlPageId === "0" && window.AppkitENV.approvalType === "Zip"){
            viewerController.send('addMarkerOverlay', [annotation.get("CrosshairXCoord"), - annotation.get("CrosshairYCoord")], (annotation._data.PageNumber - 1));
          } else{
          viewerController.send('addMarkerOverlay', [annotation.get("CrosshairXCoord"), - annotation.get("CrosshairYCoord")],[annotation.get("OriginalVideoWidth"), annotation.get("OriginalVideoHeight")]);
          }
          Ember.run.later(function() {
            viewerController.send('updateMarkerPosition');
          });
		    }
    },
});
export default AnnotationActions;
