import Ember from "ember";
import CurrentViewer from 'appkit/routes/mixins/current-viewer';

var CustomProfilesActions = Ember.Mixin.create(CurrentViewer, {

    needs: ['approval'],

    actions: {
        hideCustomProfiles: function() {
            Ember.$('.czn-custom-profiles.modal').modal('hide');
            this.disconnectOutlet({
                outlet: 'custom-profiles',
                parentView: 'application'
            });
        },

        changeCustomProfiles: function(selectedItems){
            var viewerControllers = this.get('controllers.approval.viewerControllers') || this.controllerFor('approval').get('viewerControllers');

            selectedItems.forEach(function(selectedItem){

                var viewerRenderer = viewerControllers.filter(function(viewer){
                    return (viewer.get('model.id') === selectedItem.versionId) && (viewer.get('model.isClone') == selectedItem.isClone);
                }).get('firstObject.renderer');

                var newSoftProofingConditions = {
                  pageId:viewerRenderer.get('currentPage.id')
                };

                //var profileName, defaultProfile;
                if (selectedItem.profile) {
                    //profileName = selectedItem.profile.get('FileName');
                    //defaultProfile = selectedItem.profile.get('defaultProfile');

                    newSoftProofingConditions.SimulationProfileId = selectedItem.profile.get('SimulationProfileId');
                    newSoftProofingConditions.EmbeddedProfileId = selectedItem.profile.get('EmbeddedProfileId');
                    newSoftProofingConditions.CustomProfileId = selectedItem.profile.get('CustomProfileId');
                }
                if (selectedItem.paperTint) {
                  newSoftProofingConditions.PaperTintId = selectedItem.paperTint.get('id');
                }

                if (viewerRenderer){
                  viewerRenderer.send('updateSoftProofingConditionsHavingRenderer', newSoftProofingConditions, viewerRenderer);
                }
            });
        },
    }
});
export default CustomProfilesActions;
