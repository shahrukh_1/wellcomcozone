import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var CurrentViewer = Ember.Mixin.create({
    needs: ['approval', 'comments/index', 'sidebar/index', 'navigation/main'],

    tilesActionsService: Ember.inject.service('tiles/tiles-actions-service'),

    PhaseNamesAllControllersViews: Ember.A(),

    GetCurrentViewerController: function () {
      var vControllers = this.get('controllers.approval.viewerControllers') || this.get('controller.controllers.approval.viewerControllers');

      var currentVersionId = this.get('controllers.approval.currentVersion.id');
      if (currentVersionId == undefined) {
        currentVersionId = this.get('controller.controllers.approval.currentVersion.id');
      }

      var currentVersionIsClone = this.get('controllers.approval.currentVersion.isClone');
      if (currentVersionIsClone == undefined) {
        currentVersionIsClone = this.get('controller.controllers.approval.currentVersion.isClone');
      }

      var currentViewerC = vControllers.filter(function(vController) {
        return vController.get('id') == currentVersionId && vController.get('isClone') == currentVersionIsClone;
      }).get('firstObject');

      if (!currentViewerC) {
        var testPattern = new RegExp(/@route:/);
        if (testPattern.test(Ember.inspect(this))) {
          currentViewerC = this.controllerFor(this.viewerControllerName);
        }
        else {
          return null;
        }
      }

      return currentViewerC;
    },
    GetNonCurrentViewerControllers: function() {
      var vControllers = this.get('controllers.approval.viewerControllers') || this.get('controller.controllers.approval.viewerControllers');

      var currentVersionId = this.get('controllers.approval.currentVersion.id');
      if (currentVersionId == undefined) {
        currentVersionId = this.get('controller.controllers.approval.currentVersion.id');
      }

      var currentVersionIsClone = this.get('controllers.approval.currentVersion.isClone');
      if (currentVersionIsClone == undefined) {
        currentVersionIsClone = this.get('controller.controllers.approval.currentVersion.isClone');
      }

      var nonCurrentViewerC = vControllers.filter(function(vController) {
        return vController.get('id') != currentVersionId || vController.get('isClone') != currentVersionIsClone;
      });

      return nonCurrentViewerC;
    },

    currentViewerController: function() {
      return this.GetCurrentViewerController();
    }.property('controllers.approval.viewerControllers.@each.select', 'controllers.approval.currentVersion.id'),

  annotationsUnsorted: function() {
    var currentVersionId = this.get('currentViewerController.model.id') || "0";
    if (currentVersionId == 0) {
      return [];
    }

    var annotations = this.store.all('annotation').filter(function(annotation) {
      return annotation.get('Version') === currentVersionId && !annotation.get('isDestroyed') && !annotation.get('ChecklistItemUpdate');
    });

    var selectedUsers = this.get('controllers.sidebar/index').get('selectedUsers');

    // load phases names if phases are used, if show annotations of all phases is clicked and do it only once
    if (this.get('controllers.navigation/main.isPhase') == true && this.get('session.user.data.ShowAnnotationsOfAllPhases') == true &&
      (annotations.length > 0) && this.get('PhaseNamesAllControllersViews').filterBy('controllerId', currentVersionId).get('length') == 0) {
      this.loadPhasesNames(annotations, currentVersionId);// load phases for current version
    }

    var selectedPhases = this.get('controllers.sidebar/index').get('selectedPhases');

    var self = this;

    var anns = annotations.filter(function(annotation) {
      return self.isVisibleAnnotation(annotation, selectedUsers, selectedPhases)
    });

    return anns;
  }.property('currentViewerController', 'currentViewerController.annotationsChanged',
      'controllers.approval.viewerControllers.@each.annotationsChanged',
      'session.user.data.ShowAnnotationsOfAllPhases'),

  /* phases names are used in sidebar when show all phases is selected */
  loadPhasesNames: function(annotations, currentVersionId) {
    var self = this;

      var phaseNames = [];
      var phaseIds = [];

      annotations.forEach(function (annotation) {
        if (annotation.get("Phase") != undefined && annotation.get("Phase") != null) {
          if ((annotation.get('Version') == currentVersionId) && (!phaseIds.contains(annotation.get("Phase")))) {
            phaseNames.push(Ember.Object.create({
              phaseId: annotation.get("Phase"),
              phaseName: annotation.get("PhaseName"),
              selected: true
            }));
            phaseIds.push(annotation.get("Phase"));
          }
        }
      });

      var version = self.get("controllers.approval.Versions").filterBy('id',currentVersionId).get('firstObject');
      var currentPhase = version.get("CurrentPhase");
      if (currentPhase != undefined && currentPhase != null) {
        if (!phaseIds.contains(currentPhase)) {
          phaseNames.push(Ember.Object.create({
            phaseId: currentPhase,
            phaseName: version.get('CurrentPhaseName'), selected: true
          }));
        }
      }

      self.get('PhaseNamesAllControllersViews').pushObject(Ember.Object.create({
        controllerId: currentVersionId,
        phasesNames: phaseNames}));

      if (annotations.length > 0) {// makes sure it loads fisrt annotation
        this.get('controllers.sidebar/index').set('PhasesNamesFlag', !this.get('controllers.sidebar/index').get('PhasesNamesFlag'));
      }
  },

  annotationsSortedByIndex: function() {
    var annotations = this.get('annotationsUnsorted');

    annotations = annotations.filter(function(annotation){
      return !annotation.get('Parent');
    });

    return annotations.sort(function(a,b){
      if (a.get('AnnotationNumber') > b.get('AnnotationNumber')){
        return 1;
      }
      else if (a.get('AnnotationNumber') < b.get('AnnotationNumber')){
        return -1;
      }
      else {
        return 0;
      }
    });
  }.property('currentViewerController', 'currentViewerController.annotationsChanged', 'controllers.approval.viewerControllers.@each.annotationsChanged',
      'session.user.data.ShowAnnotationsOfAllPhases'),
  annotationsSortedByIndexWithFilterFlag: function() {

    var annotations = this.get('annotationsSortedByIndex');
    var selectedUsers = this.get('controllers.sidebar/index').get('selectedUsers');
    var selectedPhases = this.get('controllers.sidebar/index').get('selectedPhases');
    var self = this;

    if (annotations && annotations.length > 0) {
      annotations.forEach(function(annotation) {
        if (!annotation.get('isDeleted') && !annotation.get('isDestroyed')) {
          if (self.isVisibleAnnotation(annotation, selectedUsers, selectedPhases)) {
            annotation.set('isShown', true);
          }
          else {
            annotation.set('isShown', false);
          }
        }
      });
    }

    return !annotations ? [] : annotations;
  }.property('currentViewerController', 'currentViewerController.annotationsChanged', 'controllers.approval.viewerControllers.@each.annotationsChanged',
    'session.user.data.ShowAnnotationsOfAllPhases','controllers.sidebar/index.selectedUsers', 'controllers.sidebar/index.selectedPhases','annotationsSortedByIndex'),

  isVisibleAnnotation: function(annotation, selectedUsers, selectedPhases) {
    if (annotation.get('isDeleted') || annotation.get('isDestroyed')) {
      return false;
    }
    // is visible based on phases
    if (this.get('controllers.navigation/main.isPhase') && this.get('session.user.data.ShowAnnotationsOfAllPhases') == true) { // consider the filter by phases only if it is show annotations of all phases selected
      if (selectedPhases.filter(function (phase) {
          return phase.get('phaseId') == annotation.get('Phase')
        }).length == 0) {
        return false;
      }
    }

    // is visible based on users
    selectedUsers = this.get('collaborators');
    return !annotation.get('Parent') && (
        selectedUsers.contains(annotation.get('Creator')) ||
        selectedUsers.contains(annotation.get('Modifier')) ||
        selectedUsers.contains(annotation.get('ExternalCreator')) ||
        selectedUsers.contains(annotation.get('ExternalModifier')));
  },

  annotationsWithComments: function() {
    var annotations = this.get('annotationsUnsorted');

    return annotations.sort(function(a,b){
      if (a.get('id') > b.get('id')){
        return 1;
      }
      else if (a.get('id') < b.get('id')){
        return -1;
      }
      else {
        return 0;
      }
    });
  }.property('currentViewerController', 'annotationsUnsorted','currentViewerController.annotationsChanged', 'controllers.approval.viewerControllers.@each.annotationsChanged', 'session.user.data.ShowAnnotationsOfAllPhases'),


    parentAnnotations: function(){
        var index = 0,
            annotations = this.get('annotationsWithComments').filter(function(annotation){
                return !annotation.get('Parent') && !annotation.get('isDestroyed');
            });

        if (annotations){
            annotations = annotations.sort(function(a,b){
                if (a.get('AnnotatedDate') > b.get('AnnotatedDate')){
                    return 1;
                }
                else if (a.get('AnnotatedDate') < b.get('AnnotatedDate')){
                    return -1;
                }
                else{
                    return 0;
                }
            });
            annotations.forEach(function(annotation){
                if (!annotation.get('isDestroyed') && !annotation.get('isDeleted')) {
                    annotation.set('AnnotationNumber', ++index);
                }
            });
        }

        return annotations;
    }.property('currentViewerController', 'annotationsWithComments', 'annotationsWithComments.@each.isDestroyed',
      'annotationsWithComments.@each.isDeleted', 'currentViewerController.annotationsChanged',
      'currentViewerController.currentPage', 'session.user.data.ShowAnnotationsOfAllPhases'),

    collaborators: function(){
        var collaborators = [],
            currentVersionId = this.get('currentViewerController.id') || "0";

        this.store.all('external-collaborators-link').forEach(function(externalCollaboratorLink){
            if (externalCollaboratorLink.get('approval_version.id') === currentVersionId){
                var collaborator = externalCollaboratorLink.get('collaborator_id');
                collaborator.set('Decision', externalCollaboratorLink.get('Decision'));
                collaborators.push(collaborator);
            }
        });
        this.store.all('internal-collaborators-link').forEach(function(internalCollaboratorLink){
            if (internalCollaboratorLink.get('approval_version.id') === currentVersionId){
                var collaborator = internalCollaboratorLink.get('collaborator_id');
                collaborator.set('UserGroup', internalCollaboratorLink.get('UserGroup'));
                collaborator.set('Decision', internalCollaboratorLink.get('Decision'));
                collaborators.push(collaborator);
            }
        });

        return collaborators;
    }.property('controllers.approval.viewerControllers.@each.select', 'currentViewerController.usersChanged'),

    initializeSoftProofingParams: function(){
      this.get('tilesActionsService').initializeDefaultSoftProofingParams();
    },

  getPaperTintName: function(softProofingParam) {
    var selectedPaperTint = this.store.all('soft-proofing-papertint').filter(function (paperTint) {
      return paperTint.get('id') == softProofingParam.PaperTintId;
    }).get('firstObject');
    if (selectedPaperTint)
    {
      return selectedPaperTint.get('PaperTintName');
    }
    else
    {
      return "";
    }
  },

    cloneJSON: function (obj) {
      var clonedObj = {};
      Ember.keys(obj).forEach(function(prop) {
        clonedObj[prop] = obj[prop];
      });
      return clonedObj;
    },

    formatDate: function (date) {
      var format;

      var dateFormat = this.get('session.account.AccountDateFormat'),
        timeFormat = this.get('session.account.AccountTimeFormat');

      if(AppConstants.Date.DateFormat.hasOwnProperty(dateFormat)) {
        dateFormat = AppConstants.Date.DateFormat[dateFormat];
      }
      else {
        dateFormat = dateFormat.toUpperCase();
      }
      if(AppConstants.Time.TimeFormat.hasOwnProperty(timeFormat)) {
        timeFormat = AppConstants.Time.TimeFormat[timeFormat];
      }

      format = dateFormat + ' ' + timeFormat;

      return window.moment(date).format(format);
    }
});
export default CurrentViewer;
