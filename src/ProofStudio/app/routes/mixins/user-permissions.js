import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var UserPermissions = Ember.Mixin.create(CurrentViewerMixin, {

  userCanAnnotate: function() {
      var userId = this.get('session.user.id') || this.get('controller.session.user.id'),
          versionId = this.get('currentViewerController.model.id') || this.get('controller.currentViewerController.model.id') || this.get('controllers.approval.currentVersion.id'),
          userIsExternal = this.get('session.user.UserIsExternal') || this.get('controller.session.user.UserIsExternal'),
          store = this.get('store') || this.get('controller.store');

      if (userIsExternal){
          return store.all('external-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId && link.get('CanAnnotate') == true;
          }).get('length') > 0;
      }
      else{
          return store.all('internal-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId && link.get('CanAnnotate') === true;
          }).get('length') > 0;
      }
  }.property('session.user', 'controller.session.user', 'currentViewerController.model', 'controller.currentViewerController.model'),

  userCanApprove: function() {
      var userId = this.get('session.user.id') || this.get('controller.session.user.id'),
          versionId = this.get('currentViewerController.model.id') || this.get('controller.currentViewerController.model.id') || this.get('controllers.approval.currentVersion.id'),
          userIsExternal = this.get('session.user.UserIsExternal') || this.get('controller.session.user.UserIsExternal'),
          store = this.get('store') || this.get('controller.store');

          if(window.AppkitENV.approvalType === "Zip"){
          if(window.AppkitENV.selectedHtmlPageId != '0' && this.store.all('page').content.length > 1 ){
              return false;
          } else {
              return true;
          }
        }
      if (this.get('controllers.approval.currentVersion.isClone'))
          return false;

      if (userIsExternal){
          return store.all('external-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId && link.get('CanApprove') === true;
          }).get('length') > 0;
      }
      else{
          return store.all('internal-collaborators-link').filter(function(link){
             return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId && link.get('CanApprove') === true;
          }).get('length') > 0;
      }

  }.property('session.user', 'controller.session.user', 'currentViewerController.model.id', 'controller.currentViewerController.model.id', 'controllers.approval.currentVersion', 'controllers.approval.currentVersion.isClone'),

  userCanUnlockVersion: function(){
      return this.get('userCanAnnotate') || this.get('userCanApprove');
  },

  userIsCollaborator: function(userId, versionId, is_external_user){
      if (!userId){
          userId = this.get('session.user.id');
      }
      if (is_external_user === null || is_external_user === undefined){
          is_external_user = this.get('session.user.UserIsExternal');
      }

      var store = this.get('store') || this.get('controller.store');

      if (is_external_user === true){
          return store.all('external-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId;
          }).get('length') > 0;
      }
      else{
          return store.all('internal-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId;
          }).get('length') > 0;
      }
  },

  getUserRole: function(versionId, userId, is_external_user){

      if (!userId){
          userId = this.get('session.user.id');
      }

      if (is_external_user === null || is_external_user === undefined){
          is_external_user = this.get('session.user.UserIsExternal');
      }

      if (!versionId){
          versionId = this.get('currentViewerController.model.id') || this.get('controllers.approval.currentVersion.id');
      }

      var store = this.get('store') || this.get('controller.store');
      if (is_external_user === true){
          return store.all('external-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId;
          }).get('firstObject.approval_role');
      }
      else{
          return store.all('internal-collaborators-link').filter(function(link){
              return link.get('collaborator_id.id') === userId && link.get('approval_version.id') === versionId;
          }).get('firstObject.approval_role');
      }
  },

  getUserRoleKey: function (versionId, userId, is_external_user){
      var role = this.getUserRole(versionId, userId, is_external_user);
      if (role){
          return role.get('Key');
      }
      return null;
  }
});
export default UserPermissions;
