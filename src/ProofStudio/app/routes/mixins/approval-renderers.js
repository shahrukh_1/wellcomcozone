import AppConstants from "appkit/config/constants";
import Ember from "ember";

var ApprovalRenderers = Ember.Mixin.create({
    renderDecision: function() {
        this.render(this.decisionController, {
            into: 'application',
            outlet: 'sidebar'
        });
    },

    renderSidebar: function() {
        this.render(this.sidebarController, {
            into: 'application',
            outlet: 'sidebar'
        });
    },

    renderProjectfolder: function() {
        this.render(this.projectfolderController, {
            into: 'application',
            outlet: 'projectfolder'
        });
    }, 

    disconnectSidebar: function() {
        this.disconnectOutlet({
            outlet: 'sidebar',
            parentView: 'application'
        });
    },

    renderControlsContainer: function(){
        this.render('controls/index', {
            into: 'approval',
            outlet: 'controls-container'
        });
    },
    renderImageControls: function(){
        this.render('image/controls/index', {
            into: 'controls/index',
            outlet: 'controls',
            controller: 'image/controls/image'
        });
        this.render('image/viewer/minimap', {
           into: 'application',
            outlet: 'miniMap',
            controller: 'image/viewer/minimap'
        });
    },

    renderZipControls: function(){
        this.render('zip/controls/index', {
            into: 'controls/index',
            outlet: 'controls',
            controller: 'zip/controls/zip'
        });
    },

    renderVideoControls: function(){
        this.render('video/controls/index', {
            into: 'controls/index',
            outlet: 'controls',
            controller: 'video/controls/video'
        });
    },

    renderAnnotationsModal: function(){
        this.render('comments/index',{
            into: 'application',
            outlet: 'comments',
            controller: 'comments/index'
        });
    },

    disconnectAnnotationsModal: function(){
        this.disconnectOutlet({
            outlet: "comments",
            parentView: "application"
        });
    },

    renderControls: function() {
        switch(this.approvalType) {
            case AppConstants.ApprovalTypes.TYPE_IMAGE:
            case AppConstants.ApprovalTypes.TYPE_WEBPAGE:
                this.renderImageControls();
                break;
                case AppConstants.ApprovalTypes.TYPE_ZIP:
                this.renderZipControls();
                break;
            case AppConstants.ApprovalTypes.TYPE_VIDEO:
                this.renderVideoControls();
                break;
            default:
                throw "Invalid approval type";
        }
    },

    renderSeparationsModal: function(){
        this.render('image/separations/index',{
            into: 'application',
            outlet: 'separations',
            controller: 'image/separations/index'
        });
    },

    disconnectSeparationsModal: function(){
        this.disconnectOutlet({
            outlet: "separations",
            parentView: "application"
        });
    }
});
export default ApprovalRenderers;
