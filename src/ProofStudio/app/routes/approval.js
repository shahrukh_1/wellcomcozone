import Ember from "ember";
import AppConstants from "appkit/config/constants";
import ApprovalRenderers from "appkit/routes/mixins/approval-renderers";
import VideoActions from "appkit/routes/video/mixins/video-actions";
import ImageActions from "appkit/routes/image/mixins/image-actions";
import AnnotationActions from "appkit/routes/mixins/annotation-actions";
import SeparationActions from "appkit/routes/image/mixins/separation-actions";
import DrawerActions from "appkit/routes/mixins/drawer-actions";
import TilesActions from "appkit/routes/image/mixins/tiles-actions";
import CustomProfilesActions from "appkit/routes/mixins/custom-profiles-actions";
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var ApprovalRoute = Ember.Route.extend(ApprovalRenderers, VideoActions, ImageActions, AnnotationActions, SeparationActions, DrawerActions, TilesActions, CustomProfilesActions, UserPermissions, {
    loadInitialData: Ember.inject.service('load-initial-data'),

    viewerControllerName: 'viewer/',
    controlsControllerName: 'controls/',
    commentControllerName : 'comments/index',
    viewerController: null,
    commentController: null,
    sidebarController: 'sidebar/index',
    projectfolderController: 'projectfolder/index',
    decisionController: 'sidebar/decisions',
    annotationStatusController: 'sidebar/annotation-statuses',
    navigationController: 'navigation/index',
    navigationMainController: 'navigation/main',
    separationsControllerName: 'image/separations/index',
    separationsController: null,
    sidebarState: 'annotations',
    compareControllerName: 'image/viewer/compare',
    compareController: 'image/viewer/compare-item',
    overlayControllerName: 'image/viewer/overlay',
    isCompareMode: false,

    actions: {
        initializeViewerController: function(viewerController){
            var viewerControllers = this.controllerFor('approval').get('viewerControllers');
            if (!viewerControllers.contains(viewerController)) { // Warning: this check adds duplicate controllers
              viewerControllers.pushObject(viewerController);
            }
        },
        showDecision: function() {
            var applicationController = this.controllerFor('application');
            if(!applicationController.get('sidebarShown')){
                applicationController.send('toggleSidebar');
            }
            if(this.sidebarState !== 'decision') {
                this.disconnectSidebar();
                this.renderDecision();
                this.sidebarState = 'decision';
            }
        },
        unloadDecision: function() {
            this.disconnectSidebar();
            this.renderSidebar();
            this.sidebarState = 'annotations';
        },
        saveDecision: function(decision) {
            this.controllerFor('approval').send('saveDecision', decision);
            if(window.AppkitENV.tablet) {
                this.send('unloadDecision');
            }
        },
        prevPage: function() {
            this.sendActionToViewer('prevPage');
        },
        nextPage: function() {
            this.sendActionToViewer('nextPage');
        },

        // show/hide the pages of each view depending on the selected mode(single/compare)
        togglePagination: function(){
            if(this.get('isCompareMode')){
                var self = this;
                this.controllerFor('approval').get('selectedVersions').forEach(function(localVersion){
                    self.showOrHidePages(localVersion);
                });
            }
            else
            {
                var currentVersion = this.controllerFor('approval').get('currentVersion');
                this.showOrHidePages(currentVersion);
            }
        },
        toggleAllTickets: function(show){
            if(!this.get('isCompareMode')) {
                this.sendActionToViewer('toggleAllTickets', show);
            }
        },
        gotoPage: function(pageId)
        {
            this.sendActionToViewer('gotoPage', pageId);
        },
        changeCurrentVersion: function() {
            var currentVersion = this.controllerFor('approval').get('currentVersion');
            var controller = this.controllerFor(this.navigationMainController);

            if(currentVersion.approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO) {
                this.send('initializeVideoRenderer', currentVersion.get('videoRenderer'));
            }else{
            }
            this.checkVersionIsLocked(currentVersion);

            //show/hide lock button for selected version
            var approvalRole = this.getUserRoleKey();
            if(currentVersion.get('IsLocked')){
                if(currentVersion.get('isCompareMode'))
                {
                    //checks if approval role is 'read only' or 'review' and hides the annotation/decision buttons
                    if(approvalRole === AppConstants.Permissions.READ_ONLY || approvalRole === AppConstants.Permissions.REVIEW){
                        controller.send('showAnnotationDecision', false);
                    }
                    //show/hide the annotation/decision buttons based on the user role
                    else if(currentVersion.get('userRole') === AppConstants.UserRoles.MODERATOR ||
                        currentVersion.get('userRole') === AppConstants.UserRoles.CONTRIBUTOR ||
                        currentVersion.get('userRole') === AppConstants.UserRoles.VIEWER)
                    {
                        controller.send('showAnnotationDecision', false);
                    }
                    else {
                        controller.send('showAnnotationDecision', !currentVersion.get('isLocked'));
                    }
                }else{
                    if(approvalRole === AppConstants.Permissions.READ_ONLY || approvalRole === AppConstants.Permissions.REVIEW){
                        controller.send('changeLockType', currentVersion);
                        controller.send('showAnnotationDecision', false);
                    }
                    else if(currentVersion.get('userRole') === AppConstants.UserRoles.MODERATOR ||
                        currentVersion.get('userRole') === AppConstants.UserRoles.CONTRIBUTOR ||
                        currentVersion.get('userRole') === AppConstants.UserRoles.VIEWER)
                    {
                        controller.send('showAnnotationDecision', false);
                    }
                    else{
                        controller.send('showAnnotationDecision', !currentVersion.get('isLocked'));
                    }
                }
            }else {
                controller.send('showAnnotationDecision', true);
            }

            //render the template based on selected mode
            if(currentVersion.get('isCompareMode') || currentVersion.approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO)
            {
                var self = this;
                Ember.run.later(function(){
                    self.initCurrentVersion(self.controllerFor('approval'));
                });
            }
            else
            {
                this.initSingleVersion(this.controllerFor('approval'));
                currentVersion.set('paginationVisible', false);
                var currentViewerController = this.get('currentViewerController');
                currentViewerController.send('initVersion', currentViewerController);
            }
        },
        displayMultipleVersions: function() {
            this.initCompareVersions(this.controllerFor('approval'));
        },
        //viewSingleVersion: function(){
        //    this.initSingleVersion(this.controllerFor('approval'));
        //},
        changeSelectedVersion: function(version){
          var self = this;
          var approvalController = this.controllerFor('approval');
            var currentVersion = approvalController.get('currentVersion');

            if ((currentVersion.get('id') !== version.get('id')) || (approvalController.get('selectedVersions').filterBy('isClone',true).length > 0)) {
              // unselect all versions and than select only the selected version. This is to avoid duplicate selects
              approvalController.get('selectedVersions').forEach(function (localVersion) {
                localVersion.set('select', false);
                self.checkUserAccessRole(localVersion);
              });
              Ember.run.later(function() {
                var versionSelected = false;
                approvalController.get('selectedVersions').forEach(function (localVersion) {
                  if (!versionSelected && localVersion.get('id') === version.get('id') && localVersion.get('isClone') === version.get('isClone') ) {
                    versionSelected = true;
                    localVersion.set('select', true);
                    self.checkUserAccessRole(localVersion);
                  }
                });
                approvalController.send('changeCurrentVersion', version);
                self.get('session').set('zoomLevels', []);
              });
            }
        },
        changeToOverlayMode: function(){
            this.initOverlayVersion(this.controllerFor('approval'));
        },
        newComment: function(data){
            this.commentController.set('model', []);
            this.commentController.set('point', data.Point);
            this.commentController.set('timeFrame', data.TimeFrame);
            this.commentController.set('pageId', data.PageId);
            this.commentController.send('show');
            this.commentController.set('svg', null);
            if(data.OrderNumberOfHtmlPage >= 0){
                this.commentController.set('orderNumberOfHtmlPage', data.OrderNumberOfHtmlPage);
            }
            if (data.CommentText) {
                this.commentController.set('newComment', data.CommentText);
            }
            if (data.CommentType){
                this.commentController.set('commentType', data.CommentType);
            }
            if (data.HighlightType){
                this.commentController.set('HighlightType', data.HighlightType);
            }
            if (data.StartIndex){
                this.commentController.set('StartIndex', data.StartIndex);
            }
            if (data.EndIndex){
                this.commentController.set('EndIndex', data.EndIndex);
            }
            if (data.HighlightData){
                this.commentController.set('HighlightData', data.HighlightData);
            }
        },
        doneAnnotating: function(){
            this.sendActionToViewer('closeAddMarker');
            this.sendActionToViewer('hideCanvas');
            this.sendActionToViewer('removeMarkerOverlay');
            this.commentController.send('hide');

            var annotationCtrl = this.controllerFor('navigation/annotation');
            if (annotationCtrl)
            {
                annotationCtrl.set('isDrawSelected', false);
                annotationCtrl.set('isMarkerSelected', false);
            }
            //hack for display markers when exiting color picker
            var approvalCtrl = this.controllerFor('approval');
            if (approvalCtrl){
                approvalCtrl.set('doneCount', (approvalCtrl.get('doneCount') || 0) + 1);
              }
            return true;
        },
        newSVGAnnotation: function(data){
            this.commentController.set('model', []);
            this.commentController.set('point', data.Point);
            this.commentController.set('timeFrame', data.TimeFrame);
            this.commentController.set('pageId', data.PageId);
            this.commentController.set('svg', data.Svg);
            this.commentController.set('scales', data.Scale);
            this.commentController.set('commentType', data.commentType);
        },
        textHighlighting: function(enable){
            this.controllerFor('navigation/annotation').send('textHighlighting', enable);
        },
        changeBackgroundColorPicker: function(color){
            if (!color){
                color = this.get('session.account.ProofStudioBackgroundColor') || "#eeeeee";
            }
            Ember.$('.czn-background-color-picker:visible .color-swatch').spectrum('set', color);
            //Ember.$(".czn-image-viewer, .video-view").css('background', color);
            return false;
        },
        tilesReady: function(){
            if (this.get('isCompareMode') && !this.get('isTilesReady')){
                this.setFirstVersion();
                this.set('isTilesReady', true);
            }
        }
    },

    setFirstVersion: function(selectedVersions){

        selectedVersions = selectedVersions || this.controllerFor('approval').get('selectedVersions');

        // if any version is selected then the user comes from overlay view mode
        // else the user comes from single view mode
        var anyVersionIsSelected = selectedVersions.filterBy('select', true).get('length') > 0,
            firstVersion = anyVersionIsSelected ? selectedVersions.filterBy('select', true).get('firstObject') : selectedVersions.get('lastObject'),
            lastVersion = anyVersionIsSelected ? selectedVersions.filterBy('select', false).get('firstObject') : selectedVersions.get('firstObject'),
            self = this;

        if (lastVersion){
            this.send('changeSelectedVersion', lastVersion);
        }

        if (firstVersion) {
            Ember.run.later(this, function () {
                //self.controllerFor('viewer/compare').send('changeSelectedVersion', firstVersion);
                self.checkUserAccessRole(firstVersion);
                self.checkVersionIsLocked(firstVersion);
                self.send('changeSelectedVersion', firstVersion);
                self.controllerFor(self.navigationMainController).send('showAnnotationDecision', !firstVersion.get('IsLocked'));
            }, 750);
        }
    },

    setSessionData(values) {
      var session = this.get('session');
      session.set('selectedAnnotationsToMakePublic', []);
      session.set('showOnlyPrivateAnnotations', false);
      session.set('user', values.user);
      session.set('account', values.account);
      session.set('softproofingstatus', values.softproofingstatus);
      session.set('shapes', values.shapes.filterBy('IsCustom', false));
      session.set('allowMultiplePanelsOpen', true);
      session.set('isUsersPanelOpen', true);
      session.set('isPhasesPanelOpen', true);
      session.set('isAnnotationsPanelOpen', true);

      session.set('custom-shapes-per-page', this.get("loadInitialData.pageId"));
    },
    updateLocaleData(values) {
      var userLocaleName = values.user.get('UserLocaleName');
      //this.get('locale').change(userLocaleName);
      window.AppkitENV.default_locale = userLocaleName;
    },

    setStatusesAndDecisionOptions(values) {
      this.set('annotationStatuses', values.annotationStatuses);
      this.set('decisions', values.decisions);
    },

    setLoadedValues(values) {
      this.setSessionData(values);
      this.updateLocaleData(values);
      this.setStatusesAndDecisionOptions(values);
    },


    model: function() {
      var loadInitialDataService = this.get('loadInitialData');
      var self = this;
      loadInitialDataService.set('singleRecord', window.AppkitENV.singleRecord);
      loadInitialDataService.set('approvalsIds', window.AppkitENV.approvals);
      loadInitialDataService.set('versionsIds', window.AppkitENV.versionsIds);
      loadInitialDataService.set('pageId', window.AppkitENV.approvalFirstPageId);
      loadInitialDataService.generateApprovalIdFromApprovalsIds();

      if (window.AppkitENV.approvalType == AppConstants.ApprovalTypes.TYPE_IMAGE ||
            window.AppkitENV.approvalType == AppConstants.ApprovalTypes.TYPE_WEBPAGE) {
        var session = self.get('session');
        session.set('selectedApprovals', window.AppkitENV.approvals);

        loadInitialDataService.set('machineId', window.AppkitENV.machineId);
        loadInitialDataService.set('versionsIds', window.AppkitENV.versionsIds);

        return loadInitialDataService.loadImageStartUpData().then(function (values) {
          self.setLoadedValues(values);
          return values.approvals;
        }).catch(loadInitialDataService.showErrorOnLoadingData);
      } else {
        return loadInitialDataService.loadVideoStartUpData().then(function (values) {
          self.setLoadedValues(values);
          return values.approvals;
        }).catch(loadInitialDataService.showErrorOnLoadingData);
      }
    },
    setupController: function(controller, model) {
        controller.set('model', model);

        this.approvalType = model.get('ApprovalType');

        // choosing the viewer depending on the approval type
        switch(this.approvalType) {
            case AppConstants.ApprovalTypes.TYPE_IMAGE:
            case AppConstants.ApprovalTypes.TYPE_WEBPAGE:
                this.viewerControllerName = 'image/' + this.viewerControllerName + 'image';
                this.controlsControllerName = 'image/' + this.controlsControllerName + 'image';
                break;
            case AppConstants.ApprovalTypes.TYPE_ZIP:
                this.viewerControllerName = 'zip/' + this.viewerControllerName + 'zip';
                this.controlsControllerName = 'zip/' + this.controlsControllerName + 'zip';
                break;
            case AppConstants.ApprovalTypes.TYPE_VIDEO:
                this.viewerControllerName = 'video/' + this.viewerControllerName + 'video';
                this.controlsControllerName = 'video/' + this.controlsControllerName + 'video';
                break;
            default:
                throw "Invalid approval type";
        }

        this.initCurrentVersion(controller);
        var currentViewerController = this.get('currentViewerController');
        currentViewerController.send('initVersion', currentViewerController);

        this.controllerFor(this.decisionController).set('model', this.get('decisions'));
        this.controllerFor(this.decisionController).set('approval', model);
        this.controllerFor(this.annotationStatusController).set('model', this.get('annotationStatuses'));
        this.checkUserAccessRole(controller.get('currentVersion'));
        var currentVersion = controller.get('currentVersion');
        currentVersion.set('isLocked', currentVersion.get('IsLocked'));
        this.controllerFor(this.navigationMainController).send('showAnnotationDecision', !currentVersion.get('isLocked'));

        this.commentController = this.controllerFor(this.commentControllerName);
        this.send('loadingComplete');

        this.initializeSoftProofingParams();

        if (window.AppkitENV.approvals) {
            var approvalsIds = window.AppkitENV.approvals.split(',');
            var explicitVersions = this.explicitVersionsToCompare();

            if (approvalsIds.length > 1 || explicitVersions.length > 0) {// enter in compare mode
              var self = this;

              Ember.run.later(function () {
                var mainController = self.controllerFor(self.navigationMainController);
                self.decideCompareVersions(mainController, explicitVersions);

                Ember.run.later(function () {
                  mainController.send('compareVersion');
                });
              });
            }
        }
      this.get('loadInitialData').triggerUltraHighResolutionIfItIsTheCase(this.get('store'));
    },

    checkUserAccessRole: function(version){
        var userAccessRole = this.getUserRoleKey(version.get('id'));
        if (version.get('IsLocked') && (userAccessRole === AppConstants.Permissions.READ_ONLY || userAccessRole === AppConstants.Permissions.REVIEW)) {
            version.set('isApprovalReviewer', false);
            this.sendActionToViewer('changeLockType', version);
        }
        else {
            version.set('isApprovalReviewer', true);
        }
    },
    checkVersionIsLocked:function(version){
        if(version.get('IsLocked')){
            version.set('isLocked', true);
        }
        else{
            version.set('isLocked', false);
        }
    },
    renderTemplate: function(/*controller, model*/) {
       this.renderSingleView();
    },

    initCompareVersions: function(controller){
        var approvalCtrl = this.controllerFor('approval');

        var selectedVersions = controller.get('selectedVersions');
        selectedVersions = Ember.A(selectedVersions.toArray().reverse());
        controller.set('selectedVersions', selectedVersions);

        this.set('isCompareMode', true);
        (approvalCtrl.get('versionsForOverlay') || []).forEach(function (version) {
            version.set('isOverlayMode', false);
        });

        this.controllerFor(this.compareControllerName).set('model', selectedVersions);
        this.renderViewers();
        var viewerControllers = approvalCtrl.get('viewerControllers');
        viewerControllers.clear();
        this.get('session').set('zoomLevels', []);
        this.set('isTilesReady', false);

      this.selectEachVersionInCompareToMakeMarkersAvailable(selectedVersions);
    },

    selectEachVersionInCompareToMakeMarkersAvailable(selectedVersions) {
      var self = this;
      Ember.run.later(function() {
        var i = 0;
        selectedVersions.forEach(function (version) {
          Ember.run.later(function() {
            self.send('changeSelectedVersion', version);
          }, i* 500);
          i++;
        });
      }, 1500);

    },

    initCurrentVersion: function(controller) {
        //Initialize compare mode

        var currentViewerController = this.get('currentViewerController');
        if (!currentViewerController.get('id')) {
          currentViewerController.set('model', controller.get('currentVersion'));

          this.constrolsController = this.controllerFor(this.controlsControllerName);
        }

        // initializing the viewer depending on the approval type
        if(this.approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO){
            this.sendActionToViewer('initializeViewer');
        }
    },

    initSingleVersion: function(controller){
        //Initialize single mode
        this.set('isCompareMode', false);
        var currentVersion = controller.get('currentVersion');

        var currentViewerController = this.get('currentViewerController');
        currentViewerController.set('model', currentVersion);
        this.constrolsController = this.controllerFor(this.controlsControllerName);

        // initializing the viewer depending on the approval type
        if(this.approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO){
            this.sendActionToViewer('initializeViewer');
        }else{
            if(currentVersion.get('canInitializeView')){
                this.sendActionToViewer('initializeViewer');
            }
            this.renderSingleView();
        }
        var viewerControllers = this.controllerFor('approval').get('viewerControllers');
        viewerControllers.clear();
        this.get('session').set('zoomLevels', []);
    },

    initOverlayVersion: function(controller){
        this.controllerFor(this.overlayControllerName).set('model', controller.get('versionsForOverlay'));

        this.constrolsController = this.controllerFor(this.controlsControllerName);

        var approvalCtrl = this.controllerFor('approval');
        var viewerControllers = approvalCtrl.get('viewerControllers');
        viewerControllers.clear();

        this.renderOverlayView();
    },

    renderSingleView: function(){
        this.render();
        this.render(this.viewerControllerName, {
            into: 'approval',
            outlet: 'viewer',
            controller: this.viewerControllerName
        });
        this.renderControlsContainer();
        this.renderControls();
        this.renderSidebar();
        this.renderProjectfolder();
    },

    renderViewers: function(){
        this.render();
        this.render(this.compareControllerName, {
            into: 'approval',
            outlet: 'viewer',
            controller: this.compareControllerName
        });

        this.renderControlsContainer();
        this.renderControls();
        this.renderSidebar();
    },

    renderOverlayView: function(){
        this.render();
        this.render(this.overlayControllerName, {
            into: 'approval',
            outlet: 'viewer',
            controller: this.overlayControllerName
        });
        this.renderControlsContainer();
        this.renderControls();
        this.renderSidebar();
    },

    showOrHidePages: function(version){
        var visible = !version.get('paginationVisible');
        this.controllerFor('approval').set('paginationVisible', visible);
        version.set('paginationVisible', visible);
        this.sendActionToViewer('showBottomNavigation', visible);
    },

    decideCompareVersions(mainController, explicitVersions) {
      if (explicitVersions.length > 0) {
        mainController.get('versions').forEach(function (version) {
          if (explicitVersions.indexOf(version.get('id')) >= 0) {
            version.set('selected', true);
          }
          else {
            version.set('selected', false);
          }
        });
      }
      else {
        var processedVersions = [];

        // the versions are already sorted from latest versions to lowest
        mainController.get('versions').forEach(function (version) {
          if (processedVersions.indexOf(version.get('Parent')) < 0) {

            version.set('selected', true);// latest version per approval is selected
            processedVersions.push(version.get('Parent'));
          }
          else {
            version.set('selected', false);
          }
        });
      }

    },

    explicitVersionsToCompare() {
      var versions = [];
      if (window.AppkitENV.explicitVersionsToCompare) {
        versions = window.AppkitENV.explicitVersionsToCompare.split(",");
      }
      return versions;
    },

    sendActionToViewer(actionName, param1, param2, param3){
        //var currentViewerController = this.get('currentViewerController');
        var currentViewerController = this.GetCurrentViewerController();
        if (currentViewerController) {
            currentViewerController.send(actionName, param1, param2, param3);
        }
    },

});
export default ApprovalRoute;
