import Ember from "ember";
import CurrentViewer from 'appkit/routes/mixins/current-viewer';

var ImageActions = Ember.Mixin.create(CurrentViewer, {
    actions: {
        initializeRenderer: function(renderer) {
            var viewerController = this.get('currentViewerController');

            if (viewerController && !viewerController.get('isDestroyed')) {
              if (viewerController.get('id') == renderer.get('id')) {
                this.constrolsController.set('renderer', renderer);

                viewerController.set('renderer', renderer);
              }
            }
        },
        zoomTo: function(value) {
            this.sendActionToViewer('zoomTo', value);
        },
        zoomToResolution: function(value) {
          this.sendActionToViewer('zoomToResolution', value);
        },

      changeToHighResolution() {
        var currentViewerController = this.GetCurrentViewerController();
        var currentPage = currentViewerController.get('currentPage');

        this.send('updateSoftProofingConditionsHavingRenderer', {
            HighResolution: true,
            pageId: currentPage.get('id')
          }, currentViewerController.get('renderer'));
      },

        zoomOut: function(){
            this.sendActionToViewer('zoomOut');
        },
        zoomIn: function(){
            this.sendActionToViewer('zoomIn');
        },
        fit: function(){
            this.sendActionToViewer('fit');
        },
        rotate: function() {
            this.sendActionToViewer('rotate');
        },

        displayCompareControls: function(isVisible){
            this.constrolsController.send('displayCompareControls', isVisible);
        },
        showAnnotationDecision: function(permision){
            this.controllerFor(this.navigationMainController).send('showAnnotationDecision', permision);
        },
        syncOverlappedVersions: function(){
          this.sendActionToViewer('syncOverlappedVersions');
        },
        resizeVersionsViews: function(){
            this.controllerFor('navigation/main').send('resizeVersionsViews');
        },
        changeLockType: function(localVersion){
            this.sendActionToViewer('changeLockType', localVersion);
        },
        displaySingleAnnotation: function(annotation){
            this.sendActionToViewer('displaySingleAnnotation', annotation);
        },
        setMarkersCount: function(markersCount){
            this.sendActionToViewer('setMarkersCount', markersCount);
        },
        clearAnnotationsLayer: function(){
            this.sendActionToViewer('clearAnnotationsLayer');
        }
    }
});
export default ImageActions;
