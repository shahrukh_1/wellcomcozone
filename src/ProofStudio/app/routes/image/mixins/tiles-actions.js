import Ember from "ember";
import CurrentViewer from "appkit/routes/mixins/current-viewer";

var TilesActions = Ember.Mixin.create(CurrentViewer, {
    viewAreaService: Ember.inject.service('tiles/view-area'),

    arrGenerateTiles: Ember.A([]),

    checkIfTilesExist: function(url){
        var hash = {
            type: "HEAD",
            data: {},
            url: url,
            cache: true
        };
        return Ember.$.ajax(hash);
    },

    sendGenerateTiles: function(softProofingConditions){//pageId, outputProfileName, channels, defaultProfile, simulatePaperTint){
        var url = window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/generate-tiles';
        return new Ember.RSVP.Promise(function (resolve, reject) {
            var hash = {
                type: "POST",
                data: {
                    key: window.AppkitENV.key,
                    user_key: window.AppkitENV.user_key,
                    is_external_user: window.AppkitENV.is_external_user,

                    pageId: softProofingConditions.pageId,
                    SessionGuid: softProofingConditions.SessionGuid
                },
                url: url,
                cache: false,
                success: function (data, statusText, xhr) {
                    if (xhr.status === 200){
                       hash.success();
                    }
                    else{
                        hash.error();
                    }
                },
                error: function () {
                    hash.error();
                }
            };
            hash.success = function (data) {
                Ember.run(null, resolve, data);
            };
            hash.error = function () {
                Ember.run(null, reject);
            };
            Ember.$.ajax(hash);
        });
    },

    getCurrentZoomLevel: function(currentZoom) {
      return Math.ceil(currentZoom);
    },

    extractCurrentZoomAndViewTiles(viewerRenderer) {
      // update zoom level and center in session
      viewerRenderer.updateZoomLevel();

      var currentZoomLevel = this.getCurrentZoomLevel(viewerRenderer.get("currentZoom"));
      var map = viewerRenderer.get("approval.map");

      var currentViewTiles = this.get('viewAreaService').getCurrentViewTiles(map, {width:viewerRenderer.get('size.width'), height:viewerRenderer.get('size.height')});

      return {
        zoomLevel:currentZoomLevel,
        viewTiles:currentViewTiles
      }
    },


    actions: {
        addNotification: function(msg, viewerRenderer){
            viewerRenderer.send('addNotification', msg, viewerRenderer);
        },

        clearNotifications: function(viewerRenderer){
            viewerRenderer.send('clearNotifications');
        },

        updateSoftProofingConditionsHavingRenderer: function(newSoftProofing, viewerRenderer){
          var currentZoomAndViewTiles = this.extractCurrentZoomAndViewTiles(viewerRenderer);
          newSoftProofing.VersionId = viewerRenderer.get('model.id');
          newSoftProofing.VersionIsClone = viewerRenderer.get('model.isClone');

          this.send('doneAnnotating');
          this.get('tilesActionsService').updateSoftProofingConditions(newSoftProofing, currentZoomAndViewTiles);
        },
        generateTiles: function (softProofingConditions) {

          var found = this.get('arrGenerateTiles').filter(function(generatedTiles){
                return generatedTiles.pageId === softProofingConditions.pageId && generatedTiles.SessionGuid == softProofingConditions.SessionGuid;
              }).get('length') > 0;

          if (found) { // the request to generate tiles was sent
            Ember.run.later(function() {
              softProofingConditions.viewerRenderer.updateTilesUrl();// try to update tiles
            }, 500);
          }
          else {
            var self = this;
            var pageNumber = this.store.all('page').filter(function(p){
                return p.get('id') === softProofingConditions.pageId;
              }).get('firstObject.PageNumber');
            var otherSessionIsDefault = false;
            var currentSessionIsHighRes = false;
            var spp = this.store.all('soft-proofing-param').forEach(function(spp) {
              if (spp.get('DefaultSession') === true && spp.get('SessionGuid') !== softProofingConditions.SessionGuid && spp.get('HighResolution') === false) {
                otherSessionIsDefault = true;
              } else if (spp.get('DefaultSession') === false && spp.get('SessionGuid') === softProofingConditions.SessionGuid && spp.get('HighResolution') === true) {
                currentSessionIsHighRes = true;
              }
            });

              //.filter(function(spp){
              //return spp.get('SessionGuid') == softProofingConditions.SessionGuid;
            //}).get('firstObject');
            var message = Ember.I18n.t("lblGenerateTiles", {pageNumber: pageNumber});
            if(spp && otherSessionIsDefault && currentSessionIsHighRes){
              message = Ember.I18n.t("lblHighResInProgress");
            }
            var msg = Ember.Object.create({
              msg: message,
              showSpinner: true,
              inError: false,

              pageId: softProofingConditions.pageId,
              SoftProofingSessionGUID: softProofingConditions.SessionGuid,
            });

            softProofingConditions.viewerRenderer.send('addNotification', msg, softProofingConditions.viewerRenderer);

            this.sendGenerateTiles({
              pageId:softProofingConditions.pageId,
              SessionGuid:softProofingConditions.SessionGuid
            }).then(function() {
              self.get('arrGenerateTiles').pushObject({
                pageId:softProofingConditions.pageId,
                SessionGuid:softProofingConditions.SessionGuid
              });

              softProofingConditions.viewerRenderer.updateTilesUrl();// try to update tiles
            });

          }
        }
    },

});
export default TilesActions;
