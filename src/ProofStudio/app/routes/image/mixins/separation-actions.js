import Ember from "ember";
import CurrentViewer from 'appkit/routes/mixins/current-viewer';
import TilesActions from "appkit/routes/image/mixins/tiles-actions";

var SeparationActions = Ember.Mixin.create(CurrentViewer, TilesActions, {
    actions: {
        hideSeparations: function() {
            this.disconnectSeparationsModal();
        },
        showSeparations: function() {
            this.renderSeparationsModal();
        },
        selectSeparations: function(channels){
          var currentViewerController = this.GetCurrentViewerController();

          if(!currentViewerController.get('model.IsRGBColorSpace')) {
            var currentPage = currentViewerController.get('currentPage');
            this.send('updateSoftProofingConditionsHavingRenderer',
              {
                pageId: currentPage.get('id'),
                ActiveChannels: channels
              }, currentViewerController.get('renderer'));
          }
          else {
            this.send('hideSeparations');
          }
        }
    }
});
export default SeparationActions;
