import Ember from "ember";
import CurrentViewer from 'appkit/routes/mixins/current-viewer';

var ZipActions = Ember.Mixin.create(CurrentViewer, {
    actions: {
        // play: function(){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('play');
        //     }
        // },
        // pause: function(){
        //     var viewerController = this.get('currentViewerController');
        //     //redirect the pause action only for video approvals
        //     if (viewerController && !viewerController.get('isDestroyed') && (viewerController.get('model.ApprovalType')).toLowerCase() === 'movie') {
        //         viewerController.send('pause');
        //     }
        // },
        // forward: function(){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('forward');
        //     }
        // },
        // backward: function(){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('backward');
        //     }
        // },
        // goToEnd: function(){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('goToEnd');
        //     }
        // },
        // goToStart: function(){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('goToStart');
        //     }
        // },
        // setDuration: function(duration){
        //     this.constrolsController.send('setDuration', duration);
        // },
        // synchronizeWithVideo: function(currentTime){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('synchronizeWithVideo', currentTime);
        //     }
        // },
        // setProgressBar: function(percent){
        //     var viewerController = this.get('currentViewerController');
        //     if (viewerController && !viewerController.get('isDestroyed')) {
        //         viewerController.send('setProgressBar', percent);
        //     }
        // },
        initializeZipRenderer: function(renderer){
            var viewerController = this.get('currentViewerController');
            if (viewerController && !viewerController.get('isDestroyed')) {
                viewerController.send('initializeZipRenderer', renderer);
            }
        }
    }
});
export default ZipActions;
