import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var ApplicationRoute = Ember.Route.extend({
    actions: {
        goToUrl: function(url) {
          var controller = this.controllerFor('comments/index');
          if (controller.get('isUnSavedAnnotation') == true) {
            this.send('showConfirmDialogModal', url);
          } else {
            this.markSessionAsComplete(url);
          }
        },
        loading: function() {
            Ember.$('#czn-loading-screen').modal();
        },

        loadingComplete: function() {
            Ember.$('#czn-loading-screen').modal('hide');
        },

        error: function(reason) {
            console.log(reason, reason.stack);
            //this.send('loadingComplete');
            Ember.$('#czn-loading-screen').modal('hide');
            this.controllerFor('error').set('reason', reason);
            this.render();
            this.render('error', {
                into: 'application',
                outlet: 'modals',
                controller: 'error'
            });
        },

        showChangesCompleteModal: function(pageId) {
          var controller = this.controllerFor('confirm-dialog')
          controller.set('model', pageId);
          controller.set('yes', Ember.I18n.t('lblYes'));
          controller.set('no', Ember.I18n.t('lblNo'));
          controller.set('mode', AppConstants.ConfirmDialogMode.ChangesComplete);
          controller.set('title', Ember.I18n.t("lblApprovalChangesComplete"));
          controller.set('message', Ember.I18n.t("lblChangesCompleteConfirmation"));
          controller.set('headingMessage', "");

          controller.init();
          this.send('showConfirmDialog');
        },

        showPhaseCompleteModal: function(lockApprovalAfterFirstDecision, isRejected, isCompleted, versionPhaseShouldBeViewedByAll) {
          var controller = this.controllerFor('confirm-dialog');
          controller.set('yes', Ember.I18n.t('lblYes'));
          controller.set('no', Ember.I18n.t('lblNo'));
          controller.set('mode', AppConstants.ConfirmDialogMode.PhaseComplete);

          if(lockApprovalAfterFirstDecision == undefined){
            if(isRejected){
              controller.set('title', Ember.I18n.t("lblRejectApproval"));
              controller.set('message', Ember.I18n.t("lblRejectApprovalConfirmation"));
              controller.set('headingMessage', Ember.I18n.t("lblRejectApprovalMessageHeading"));
            }

            if(isCompleted){
              if(versionPhaseShouldBeViewedByAll)
              {
                controller.set('viewedByAllMessage', Ember.I18n.t("lblCurrentPhaseShouldBeViewedByAll"));
              }
              controller.set('title', Ember.I18n.t("lblPhaseComplete"));
              controller.set('message', Ember.I18n.t("lblPhaseCompleteConfirmation"));
              controller.set('headingMessage', Ember.I18n.t("lblPhaseCompleteMessageHeading"));
            }

            controller.set('noteMessage', Ember.I18n.t("lblDeleteMessagePleaseNote"));
          }
          else{
            if(versionPhaseShouldBeViewedByAll)
            {
              controller.set('viewedByAllMessage',  Ember.I18n.t("lblCurrentPhaseShouldBeViewedByAll"));
            }
            controller.set('title', Ember.I18n.t("lblLockApproval"));
            controller.set('message', Ember.I18n.t("lblLockApprovalConfirmation"));
            controller.set('headingMessage', Ember.I18n.t("lblLockApprovalMessageHeading"));
          }

          controller.init();
          this.send('showConfirmDialog');
      },

      showConfirmDialogModal: function(url) {
        var annotationPopUp = this.controllerFor('comments/index');
        var controller = this.controllerFor('confirm-dialog');
        controller.set('title', Ember.I18n.t("lblAnnotationChangesTitle"));
        controller.set('message', Ember.I18n.t("lblAnnotationChangesMessage"));
        controller.set('yes', Ember.I18n.t("lblLeaveAnnotation"));
        controller.set('no', Ember.I18n.t("lblStayHere"));
        controller.set('headingMessage', "");
        controller.set('annotationPopUp', annotationPopUp);
        controller.set('url', url);
        controller.set('visible', true);
        controller.set('mode', AppConstants.ConfirmDialogMode.AnnotationChanges)
        controller.init();

        this.send('showConfirmDialog');
      },

      closeConfirmDialog: function() {
        var controller = this.controllerFor('confirm-dialog');
        controller.set('visible', false);

        this.disconnectOutlet({
          outlet: 'modals',
          parentView: 'application'
        });

        Ember.$(document).off('keydown.confirmDialog');
      },

      showAddApprovalsDialogModal: function() {
        this.render('image/addapprovals/addapprovals', {
          into: 'application',
          outlet: 'modals'
        });
        var self = this;
        Ember.$(document).on('keydown.addApprovals', function(e) {
          if (e.keyCode == 27) {
            self.send('closeAddApprovalsDialogModal');
          }
        });
      },
      showFilterApprovalsDialogModal: function() {
        this.render('projectfolder/filterapprovals', {
          into: 'application',
          outlet: 'modals'
        });
        var self = this;
        Ember.$(document).on('keydown.filterApprovals', function(e) {
          if (e.keyCode == 27) {
            self.send('closeFilterApprovalsDialogModal');
          }
        });
      },
      closeFilterApprovalsDialogModal: function() {
        this.disconnectOutlet({
          outlet: 'modals',
          parentView: 'application'
        });
        Ember.$(document).off('keydown.filterApprovals');
      },

      closeAddApprovalsDialogModal: function() {
        this.store.unloadAll('tree-folder');
        this.store.unloadAll('approvals-in-tree');
        this.disconnectOutlet({
          outlet: 'modals',
          parentView: 'application'
        });
        Ember.$(document).off('keydown.addApprovals');
      },

      showChecklistItemsHistoryDialog:function(){
       this.render('checklisthistory-dialog', {
        into: 'application',
        outlet: 'modals'
       });
      },

      showPhaseIsDifferentDialog:function(){
        this.render('phasehaschanged-dialog', {
         into: 'application',
         outlet: 'modals'
        });
       },

      showConfirmDialog: function(){
        this.render('confirm-dialog', {
          into: 'application',
          outlet: 'modals'
        });
      },

        message: function(reason, title){
            this.disconnectOutlet({
              outlet: 'message',
              parentView: 'application'
            });

            this.controllerFor('message').set('reason', reason);
            this.controllerFor('message').set('title', title);

            this.render('message', {
              into: 'application',
              outlet: 'message',
              controller: 'message'
            });

            Ember.run.later(function(){
                Ember.$('#czn-message-screen').modal('show');
            });
        },

        printPopup: function(currentVersionId){
            Ember.$("#modalAnnotationsReportFilter").show();
            window.showAnnotationsReportFilters(currentVersionId);
        },

        sendToDeliverPopUp: function(currentVersionId){
          Ember.$("#modalDialogSubmitToDeliverFromPS").show();
          window.showSendToDeliverPopup(currentVersionId);
        },

        openPopup: function(/*currentVersionId*/){
            //var reportType = Ember.$("input[name='reportType']:checked").val();
            //var groupBy = Ember.$("input[name='groupBy']:checked").val();
            //var url = window.AppkitENV.PrintVersionUrl + "?reportType=" + reportType + "&groupBy=" + groupBy + "&SelectedApproval=" + currentVersionId;
            //window.open(url, 'print');
            //Ember.$('#czn-print-popup').modal('hide');
        },

        closeError: function() {
            this.disconnectOutlet({
                outlet: 'modals',
                parentView: 'application'
            });
        },

        closeMessage: function() {
            this.disconnectOutlet({
                outlet: 'message',
                parentView: 'application'
            });
        },

        reload: function() {
            location.reload();
        },

        initAnnotating: function(){
            this.disconnectNavigation();
            this.renderAnnotationNavigation();
        },

        annotate: function() {
            var controller = this.controllerFor('comments/index');
            controller.set('isSideAnnotation', false);
            this.send('playPauseVideo', "pause");
            this.disconnectNavigation();
            this.renderAnnotationNavigation();

            var navigationController = this.controllerFor('navigation/annotation');
            if (navigationController) {
                navigationController.set('isSoftProofingSelected', false);
                navigationController.set('isDrawSelected', false);
                navigationController.set('isDrawLineSelected', false);
                navigationController.set('isDrawShapeSelected', false);
            }
        },

        doneAnnotating: function() {
            this.disconnectNavigation();
            this.renderMainNavigation();
        },

        renderImageControls: function(){
            this.disconnectControls();
            this.renderImageControls();
        },
        changeBackgroundColorPicker: function(){
            return false;
        },

      playPauseVideo: function(state) {
        var videoApprovalType = AppConstants.ApprovalTypes.TYPE_VIDEO;
        var zipApprovalType = AppConstants.ApprovalTypes.TYPE_ZIP;
        var currentApprovalType = this.controllerFor('approval').get('model').get('ApprovalType');

        if(currentApprovalType == videoApprovalType) {
          var controller = this.controllerFor('video/controls/video');
          if(controller) {
            controller.send(state);
          }
        }
        if(currentApprovalType == zipApprovalType) {
          var controller = this.controllerFor('zip/controls/zip');
          if(controller) {
            controller.send(state);
          }
        }

      }
    },

    setupController: function () {
      var _this = this;

      Ember.$(window).on('beforeunload', function () {
        if(window.AppkitENV.is_external_user === true){
          _this.blockAccessAfterExit();
        }
        else{
        _this.markSessionAsComplete();
        }
      });
    },

    renderTemplate: function(/*controller, model*/) {
        this.render();
        this.renderNavigation();
        this.renderMainNavigation();
    },

    renderNavigation: function() {
        this.render('navigation/index', {
            into: 'application',
            outlet: 'navigation'
        });
    },

    renderMainNavigation: function() {
        this.render('navigation/main', {
            into: 'navigation/index',
            outlet: 'navigation'
        });
    },

    disconnectNavigation: function() {
        this.disconnectOutlet({
            outlet: 'navigation',
            parentView: 'navigation/index'
        });
    },

    renderAnnotationNavigation: function () {
        this.render('navigation/annotation', {
            into: 'navigation/index',
            outlet: 'navigation'
        });
    },

    disconnectControls: function() {
        this.disconnectOutlet({
            outlet: 'controls',
            parentView: 'controls/index'
        });
    },

    markSessionAsComplete: function(url) {
      var completeSessUrl = window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/complete-session?key=' + window.AppkitENV.key + '&session_key=' +
        window.AppkitENV.session_key;
        Ember.$.ajax({
          type: "POST",
          url: completeSessUrl,
          contentType: "application/json; charset=utf-8",
          cache: false,
          complete: function(){
          if(typeof(url) !== 'undefined') {
          location.href = url;
          }
       }
     });
    },

    blockAccessAfterExit: function(url) {
          var completeSessUrl = window.AppkitENV.hostname + '/' + 
          window.AppkitENV.namespace + '/block-externalurl?approval_id=' + 
          window.AppkitENV.approvals + '&user_guid=' +
          window.AppkitENV.user_key ;

      Ember.$.ajax({
        type: "POST",
        url: completeSessUrl,
        contentType: "application/json; charset=utf-8",
        cache: false,
        complete: function(){
          if(typeof(url) !== 'undefined') {
            location.href = url;
          }
        }
      });
    }
});
export default ApplicationRoute;
