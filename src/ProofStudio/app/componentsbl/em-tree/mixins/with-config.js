import Em from 'ember';

export default Em.Mixin.create({
  //configName: Em.computed(function() {
  //  var config;
  //  config = this.nearestWithProperty('configName');
  //  if (config) {
  //    return config.get('configName');
  //  } else {
  //    return 'default';
  //  }
  //}),
  //config: Em.computed('configName', function() {
  //  return Em.IdxConfig.getConfig(this.get('configName'));
  //})
  config: {
    tree: {
      classes: ['em-tree-branch', 'em-tree', 'fa-ul'],
      branchClasses: ['em-tree-branch', 'fa-ul'],
      nodeClasses: ['em-tree-node'],
      nodeOpenClasses: [],
      nodeCloseClasses: [],
      nodeOpenIconClasses: ['fa-li', 'fa', 'fa-minus-square-o'],
      nodeCloseIconClasses: ['fa-li', 'fa', 'fa-plus-square-o'],
      nodeLeafClasses: ['leaf'],
      nodeLeafIconClasses: ['fa-li', 'fa','fa-square-o'],
      nodeLoadingIconClasses: ['fa-li', 'fa', 'fa-spinner', 'fa-spin'],
      nodeSelectedClasses: ['em-tree-node-active']
    }
  }
});
