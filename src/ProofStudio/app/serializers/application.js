import Ember from "ember";
import DS from "ember-data";

var get = Ember.get;
var ApplicationSerializer = DS.RESTSerializer.extend({
    primaryKey: 'ID',
    
    keyForAttribute: function(attr) {
        return attr.replace('-', '_');
    },
    /**
     Serialize has-may relationship when it is configured as embedded objects.

     @method serializeHasMany
     */
    serializeHasMany: function(record, json, relationship) {
        var key   = relationship.key,
            embed = relationship.options.embedded === 'always';
        if (embed) {
            json[this.keyForAttribute(key)] = get(record, key).map(function(relation) {
                var data = relation.serialize(),
                    primaryKey = get(this, 'primaryKey');

                data[primaryKey] = get(relation, primaryKey);

                return data;
            }, this);
        }
        else {
            this._super(record, json, relationship);
        }
    }
});
export default ApplicationSerializer;