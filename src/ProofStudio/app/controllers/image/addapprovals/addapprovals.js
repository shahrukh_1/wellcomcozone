import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var AddApprovals = Ember.ObjectController.extend({
  needs: ['image/addapprovals/tree','approval','navigation/main'],
  title: Ember.I18n.t('lblChooseFileToCompare'),
  yes: Ember.I18n.t('lblApply'),
  no: Ember.I18n.t('btnCancel'),
  loadAllApprovals: Ember.I18n.t('lblLoadApprovalsInFolder'),
  loadMore: Ember.I18n.t('lblMoreApprovals'),
  posibileMoreApprovals: true,
  currentPageNumber: 1,
  loadTree: Ember.I18n.t('lblLoadTree'),
  folderNextPageNumber: 1,
  allApprovalsNextPageNumber: 1,
  allApprovalsLoaded:false,
  visible: false,
  annotationPopUp: null,
  url: null,
  selectedFolderId: -1,
  treeFolders: [], // example: [{id:1, Parent:0, Name:"First"}],
  selectedApprovals: Ember.A([]), // example: [{id:11, TreeFolderId:0, Name: "A1", Thumbnail: url, Owner: "Admin"],
  showLoadingApprovals: true,
  showLoadingTreeData: false,
  noApprovalIsSelected: true,
  searchParameter: null,
  searchText:Ember.I18n.t('lblSearchApprovalsInTree'),
  loadInitialDataService: Ember.inject.service('load-initial-data'),

  init: function() {
    this.loadTreeFolders();
  },
  onChangeSelectedFolder: function() {
    if (this.get('selectedFolderId') < 0) {
      return;
    }

    this.reloadSelectedApprovals();

    this.set('noApprovalIsSelected', true);

    this.set('posibileMoreApprovals', true);
    if (!this.get('allApprovalsLoaded') && this.get('selectedFolderId') > 0) {

      if (this.get('selectedApprovals').get('length') >= AppConstants.NumberOfApprovalsPerPageInProofStudio) {
        this.set('folderNextPageNumber', Math.floor(this.get('selectedApprovals').get('length') / AppConstants.NumberOfApprovalsPerPageInProofStudio) + 1);
      }
      else if (this.get('selectedApprovals').get('length') == 0) {
        this.set('folderNextPageNumber', 1);
      }
      this.loadFromServerApprovals();
    }
  }.observes('selectedFolderId','loadedApprovals'),

  reloadSelectedApprovals: function() {
    if (this.get('selectedFolderId') == 0) {
        this.set('selectedApprovals', this.processApprovals(this.store.all('approvals-in-tree')));
    } else {
        this.set('selectedApprovals', this.processApprovals(this.store.all('approvals-in-tree').filterBy('TreeFolderId', parseInt(this.get('selectedFolderId')))));
    }
  },
  loadedApprovals: function() {
    return this.get('session').get('selectedApprovals').split(',');
  }.property('session.selectedApprovals.length'),

  noOfLoadedApprovals: function() {
    return this.get('filteredApprovals.length');
  }.property('filteredApprovals.length'),

  loadMoreCase: function() {
    return this.get('posibileMoreApprovals') && !this.get('allApprovalsLoaded');
  }.property('posibileMoreApprovals','allApprovalsLoaded'),

  processApprovals: function(approvals) {
    var addedApprovals = [];
    var loadedApprovals = this.get('loadedApprovals');
    var self = this;

    approvals.forEach(function(approval) {
      if (!loadedApprovals.contains(approval.get('id'))) {
        addedApprovals.pushObject(self.approvalToLocalApproval(approval));
      }
    });
    return addedApprovals;
  },

  page: function () {
    if (this.get('selectedFolderId') > 0) {
      return this.get('folderNextPageNumber');
    }
    else
    {
      return this.get('allApprovalsNextPageNumber');
    }
  }.property('folderNextPageNumber', 'allApprovalsNextPageNumber', 'selectedFolderId'),

  filteredApprovals: function() {
    var searchParameter = this.get('searchParameter');
    var selectedApprovals = this.get('selectedApprovals');
    if(selectedApprovals != null && searchParameter != null)
    {
      return selectedApprovals.filter(function(approval){
        return approval.get('Name').toLowerCase().indexOf(searchParameter.toLowerCase()) !== -1;
      });
    }else{
      return selectedApprovals;
    }

  }.property('searchParameter','selectedApprovals'),

  incrementPage: function() {
    if (this.get('selectedFolderId') > 0) {
      this.set('folderNextPageNumber', this.get('folderNextPageNumber') + 1);
    } else {
      this.set('allApprovalsNextPageNumber', this.get('allApprovalsNextPageNumber') + 1);
    }
  },
  resetPageToZero: function() {
    if (this.get('selectedFolderId') > 0) {
      this.set('folderNextPageNumber', 0);
    } else {
      this.set('allApprovalsNextPageNumber', 0);
    }
  },

  loadFromServerApprovals: function () {
    this.set('showLoadingApprovals', true);
    var self = this;

    this.store.find('approvals-in-tree', {TreeFolderId: this.get('selectedFolderId'), Page: this.get('page')}).then(function(approvalsInTreeResult) {

      self.reloadSelectedApprovals();

      if (self.get('page') == 0 && self.get('selectedFolderId') == 0) {// all data are loaded
        self.set('allApprovalsLoaded', true);
      }
      else
      {
        if (approvalsInTreeResult.get('length') >= AppConstants.NumberOfApprovalsPerPageInProofStudio) {
          self.incrementPage();
        }
        else
        {
          self.set('posibileMoreApprovals', false);
          if (self.get('selectedFolderId') == 0) {
            self.set('allApprovalsLoaded', true);
          }
        }
      }
      self.set('showLoadingApprovals', false);
    });
  },

  approvalToLocalApproval:function (approval) {
    return Ember.Object.create({
        id: approval.get('id'),
        TreeFolderId: approval.get('TreeFolderId'),
        Name: approval.get('Name'),
        Thumbnail: approval.get('Thumbnail'),
        Owner: approval.get('Owner'),
        selected: false
    });
  },

  loadTreeFolders: function() {
    var self = this;
    self.set('showLoadingTreeData', true);
    self.set('selectedApprovals',[]);
    self.set('showLoadingApprovals', true);

    self.set('selectedFolderId', -1);
    self.store.unloadAll('tree-folder');
    self.store.unloadAll('approvals-in-tree');

    var treeFolders = [];

    this.store.find('tree-folder').then(function(treeFoldersResult) {
      treeFoldersResult.forEach(function(folder) {
        treeFolders.pushObject({
          id: folder.get('id'),
          Parent: folder.get('Parent'),
          Name: folder.get('Name')
        });
      });

      self.set('treeFolders', treeFolders);
      self.get('controllers.image/addapprovals/tree').send('reloadTree');

      self.set('showLoadingTreeData', false);

      self.set('posibileMoreApprovals', true);
      self.set('allApprovalsNextPageNumber', 1);

      self.set('selectedFolderId', 0);


      self.loadFromServerApprovals();

    });
  },

  isAnyApprovalSelected: function(){
    this.set('noApprovalIsSelected', !this.get('selectedApprovals').any(function(item){
      return item.get('selected');
    }));
  },

  approvalsToBeAdded: function() {
    var toBeAddedApprovals = this.get('selectedApprovals').filterBy('selected', true);
    var toBeAddedApprovalsIds = [];
    var alreadySelectedApprovals = this.get('session').get('selectedApprovals').split(',');
    toBeAddedApprovals.forEach(function (approval) {
      if (!alreadySelectedApprovals.contains(approval.get('id'))) {
        toBeAddedApprovalsIds.push(approval.get('id'));
      }
    });
    return toBeAddedApprovalsIds;
  },

  addedNewApprovals: function(approvalsIds) {
    this.get('session').set('selectedApprovals', this.get('session').get('selectedApprovals') + "," + approvalsIds.join());
  },
  processApprovalsFromServer(loadedVersions) {
    var versions = this.get('controllers.approval.Versions');
    var newVersions = Ember.A([]);
    newVersions.pushObjects(versions.toArray());

    this.store.all('version').forEach(function(version) {
      var addedNewVersionsIds = newVersions.map(function(version) { return version.get('id'); })
      if (!addedNewVersionsIds.contains(version.get('id')) && loadedVersions === version.get('id')) {
        version.set('isClone', false);

        newVersions.pushObject(version);
      }
    });

    newVersions = newVersions.sortBy('id');
    var k = newVersions.get('length');
    newVersions.forEach(function(version) {
      version.set('OrderInDropDownMenu', k--);
    });

    versions.clear();
    versions.pushObjects(newVersions.toArray().reverse());

    this.processLabelsAfterAddedVersions();
  },
  processLabelsAfterAddedVersions: function() {
    var versions = this.get('controllers.approval.model.Versions');
    var k = 1;

    var sortedVersions = versions.sortBy('OrderInDropDownMenu');
    var lastParent = sortedVersions.get('firstObject').get('Parent');
    sortedVersions.forEach(function(version) {
      if (lastParent != version.get('Parent')) {
        lastParent = version.get('Parent');
        k += 1;
      }

      var versionlabel = version.get('VersionLabel');
      var newVersionLabel = versionlabel.substring(versionlabel.indexOf(":") + 1);
      version.set('VersionLabel', newVersionLabel);
    });

  },

  loadedVersions: function(addedApprovals) {
    var versions = "";
    var self = this;
    addedApprovals.forEach(function(addedApproval) {
      self.store.all('version').filterBy('Parent',parseInt(addedApproval)).forEach(function(version) {
        if (versions == "") {
          versions = version.get('id');
        }
        else {
          versions = versions + "," + version.get('id');
        }
      });
    });
    return versions;
  },

  getNextProfileId: function() {
    var ids = this.store.all('custom-profile').mapBy('id');
    return Math.max(...ids) + 1;
  },

  loadApprovalsVersionsFromServer: function (toBeAddedApprovalsIds) {
    var self = this;
    Ember.$("#czn-loading-screen").modal();
    this.get('loadInitialDataService').loadApprovalsAndTriggerUltraHighResNotification(toBeAddedApprovalsIds.join())
      .then(function(approvals) {
        var loadedVersions = self.loadedVersions(toBeAddedApprovalsIds);
        customProfiles: self.store.find('custom-profile', {
          versionsIds: loadedVersions, nextProfileId: self.getNextProfileId()
        }).then(function(profiles) {
          self.processApprovalsFromServer(loadedVersions);

          self.addedNewApprovals(toBeAddedApprovalsIds);
          self.openCompareModeWithSelectedVersions();

          Ember.$("#czn-loading-screen").modal('hide');
          self.send('closeAddApprovalsDialogModal');
        });
      })
      .catch(function(e){

      });
  },
  openCompareModeWithSelectedVersions: function() {
    var self = this;
    var versions = this.get('controllers.approval.model.Versions');
    var alreadySelectedApprovals = this.get('session').get('selectedApprovals').split(',');

    versions.forEach(function(version) {
      if (alreadySelectedApprovals.contains(version.get('id'))) {
        version.set('selected', true);
      }
    });

    Ember.run.later(function () {
      self.get('controllers.navigation/main').send('compareVersion');
    });
  },

  actions: {
    selectApproval: function (approvalId) {
      this.get('selectedApprovals').forEach(function (approval) {
        if (approval.get('id') == approvalId) {
          approval.toggleProperty('selected');
        }
      });
      this.isAnyApprovalSelected();
    }
  ,

    loadApproval: function () {
      var toBeAddedApprovalsIds = this.approvalsToBeAdded();
      if (toBeAddedApprovalsIds.length > 0) {
        this.loadApprovalsVersionsFromServer(toBeAddedApprovalsIds);
      }
      this.set('searchParameter', '');
    }
  ,
    cancel: function () {
      this.send('closeAddApprovalsDialogModal');
      this.set('searchParameter', '');
    }
  ,
    refreshTree: function () {
      this.loadTreeFolders();
    }
  ,
    refreshApprovals: function () {
      this.resetPageToZero();
      this.set('posibileMoreApprovals', false);
      this.loadFromServerApprovals();
    },

    loadMoreApprovals: function () {
      this.loadFromServerApprovals();
    },
  }
});
export default AddApprovals;
