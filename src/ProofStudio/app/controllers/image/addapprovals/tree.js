import Ember from "ember";
import TreeNode from 'appkit/componentsbl/em-tree/node';

export default Ember.ObjectController.extend({
  needs: ['image/addapprovals/addapprovals'],
  treeFoldersInTree: Ember.computed.alias('controllers.image/addapprovals/addapprovals.treeFolders'),
  root: TreeNode.create({title: Ember.I18n.t('lblAllApprovels'), treeId:0}),

  init: function() {
    if (!this.get("root.children")) {
      this.populateTree(this.get('treeFoldersInTree'), this.get('root'), 0);
    }

    return this.set('model', this.get('root'));
  },

  onSelectedChange:function() {
    this.set('controllers.image/addapprovals/addapprovals.selectedFolderId',this.get('selected.treeId'));
  }.observes('selected.treeId'),

  populateTree: function(folders, parentNode, parent) {
    var self = this;

    var children = folders.filterBy("Parent", parseInt(parent));
    if (children.get('length') > 0) {
      var newFolders = parent > 0 ? folders.removeObject(parentNode) : folders;

      children.forEach(function(child) {
        var newParent = parentNode.createChild({
          title: child.Name,
          treeId: child.id
        });
        self.populateTree(newFolders, newParent, child.id)
      });
    }
  },

  actions: {
    reloadTree() {
      if (this.get("root.children")) {
        this.get("root").set('children', null);
      }
      this.populateTree(this.get('treeFoldersInTree'), this.get('root'), 0);
    }
  }
});
