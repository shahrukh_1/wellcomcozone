import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var CompareItemVersionsIndexController = Ember.ObjectController.extend(CurrentViewerMixin, {
    needs: ['sidebar/index'],

    select: function(){
        if(this.get('model.select') === true){
          this.get('viewerChangedCommunicationService').ViewerChagned(this.get('model.id'));
          return true;
        }
        return false;
    }.property('model.select')
});
export default CompareItemVersionsIndexController;
