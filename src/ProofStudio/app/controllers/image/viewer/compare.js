import Ember from "ember";

var CompareVersionsIndexController = Ember.ArrayController.extend({
    needs: ['approval'],
    versions: Ember.computed.alias('controllers.approval.selectedVersions'),
    itemController: 'image/viewer/compare-item'
});
export default CompareVersionsIndexController;
