import Ember from "ember";
import ViewerController from 'appkit/controllers/viewer/viewer';
import AppConstants from 'appkit/config/constants';
import TilesActions from "appkit/routes/image/mixins/tiles-actions";
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var ImageViewerController = ViewerController.extend(TilesActions, UserPermissions, {
    needs:['image/controls/image', 'image/viewer/compare', 'approval', 'navigation/annotation', 'navigation/main'],
    renderer: null,
    isPageSyncSelected: Ember.computed.alias('controllers.image/controls/image.isPageSync'),
    isVertical: Ember.computed.alias('controllers.image/controls/image.isVertical'),
    isHorizontal: Ember.computed.alias('controllers.image/controls/image.isHorizontal'),
    isOverlayMode: Ember.computed.alias('controllers.image/controls/image.isDifferenceSelected'),
    isCompareMode: Ember.computed.alias('controllers.image/controls/image.isCompareMode'),
    currentVersion: Ember.computed.alias('controllers.approval.currentVersion'),
    compareVersions: Ember.computed.alias('controllers.approval.selectedVersions'),
    previousPageNr: 1,
    tilesUrl: null,
	  isEnableSoftProofing: Ember.computed.alias('controllers.navigation/main.isEnableSoftProofing'),
    pagesChanged: Ember.computed.alias('controllers.approval.pagesChanged'),
    percentagesSwipe: Ember.computed.alias('controllers.image/controls/image.percentagesSwipe'),
    isHighResolution: Ember.computed.alias('controllers.navigation/custom-profiles/index.currentSPP.HighResolution'),
    uhrTriggeredBy: AppConstants.UltraHighResolutionTriggerStatus.UHRNotTriggered,
    updateTilesClicked: false,
    updateTilesTriggeredGenerateTiles: false,

    softProofingGuidChanged: 1,

    registerEvents() {
      this.get('instantNotificationsCommunicationService').RegisterOnConfirmedToShowNewTiles(this,'reloadFromUrl');
      this.get('softProofingParamCommunicationService').RegisterOnSoftProofingChanged(this,'increaseSoftProofingGuidChanged');
      this.get('softProofingParamCommunicationService').RegisterOnUHRTriggeredChanged(this,'uhrTriggered');
    },

    unRegisterEvents() {
      this.get('instantNotificationsCommunicationService').UnRegisterOnConfirmedToShowNewTiles(this,'reloadFromUrl');
      this.get('softProofingParamCommunicationService').UnRegisterOnSoftProofingChanged(this,'increaseSoftProofingGuidChanged');
      this.get('softProofingParamCommunicationService').UnRegisterOnUHRTriggeredChanged(this,'uhrTriggered');
    },

    increaseSoftProofingGuidChanged(versionId, isForClone) {
      if (this.get('model.id') != versionId || this.get('model.isClone') != isForClone)
        return;

      this.set('softProofingGuidChanged', this.get('softProofingGuidChanged') + 1);
    },

    uhrTriggered(triggeredBy) {
      if (this.get('model.id') != triggeredBy.version) {
        return;
      }

      this.set('uhrTriggeredBy', triggeredBy.triggeredType)
    },

    actions: {
        initializeRenderer: function(renderer){
          if (this.get('id') != renderer.get('id')) {
            return;
          }

          this.set('renderer', renderer);

          if (!this.get('currentPage'))
          {
              var currentPage = this.getCurrentPage();
              this.set('currentPage', currentPage);
              this.setCurrentPageActive();
              this.set('tilesUrl', null);
          }

          this.send('initializeViewerController', this);

          Ember.run.scheduleOnce('afterRender', this, this.getData);
          this.updateTilesUrl();
          return true;
        },
        prevPage: function() {
            var prevPage = this.get('currentPage.PrevPage');
            if(window.AppkitENV.IsApprovalZoomLevelChangesEnabled > 0){
            var session = this.get('session');
            var zoomLevel = Ember.$('.ember-view.ember-text-field')[0].value / 2.5;
            session.set('previousPageZoomLevel', zoomLevel);
          }
            this.changeCurrentPage(prevPage);
        },
        nextPage: function() {
            var nextPage = this.get('currentPage.NextPage');
            if(window.AppkitENV.IsApprovalZoomLevelChangesEnabled > 0){
            var session = this.get('session');
            var zoomLevel = Ember.$('.ember-view.ember-text-field')[0].value / 2.5;
            session.set('previousPageZoomLevel', zoomLevel);
            }
            this.changeCurrentPage(nextPage);
        },
        gotoPage: function(pageId){
          this.changeCurrentPage(pageId);
        },
        zoomTo: function(zoomLevel) {
            this.get('renderer').send('zoomTo', zoomLevel);
        },

        zoomToResolution: function(resolution) {
          this.get('renderer').send('zoomToResolution', resolution);
        },

        //changeToHighResolution() {
        //  alert('ch to high 1');
        //},

        zoomIn: function() {
            this.get('renderer').send('zoomIn');
        },
        zoomOut: function() {
            this.get('renderer').send('zoomOut');
        },
        fit: function(){
            this.get('renderer').send('fit');
        },
        rotate: function() {
            this.get('renderer').send('rotate');
        },
        showBottomNavigation: function(show){
            this.get('renderer').send('showBottomNavigation', show);
        },
        showSeparation: function(colorName){
            this.get('renderer').send('showSeparation', colorName);
        },
        renderAnnotation: function(markups){
            this.get('renderer').send('renderAnnotation', markups);
        },
        toggleVersionLock: function(model){
            var userCanAnnotate = this.get('userCanAnnotate'),
                userCanApprove = this.get('userCanApprove'),
                isLocked = model.get('isLocked');

            if (this.get('model.IsLocked') === true  && !this.userCanUnlockVersion()) {
                return false;
            }
            else if(model.get('userRole') === AppConstants.UserRoles.ADMIN || model.get('userRole')=== AppConstants.UserRoles.MANAGER)
            {
                this.get('model').set('isLocked', !isLocked);
                this.send('showAnnotationDecision', isLocked);
            }
            else if(model.get('userRole') === AppConstants.UserRoles.MODERATOR ||
                model.get('userRole') === AppConstants.UserRoles.CONTRIBUTOR ||
                model.get('userRole') === AppConstants.UserRoles.VIEWER)
            {
                this.get('model').set('isLocked', !isLocked);
            }
            else{
                this.get('model').set('isLocked', !isLocked);
                this.send('showAnnotationDecision', isLocked);
            }
        },
        initAddMarkerInViewerController: function(annotationType, editMode){
            this.get('renderer').send('initAddMarkerInRederer', annotationType, editMode);
        },
        closeAddMarker: function(){
            var renderer = this.get('renderer');
            if (renderer) {
                renderer.send('closeAddMarker');
            }
        },
        initColorDensitometer: function(){
          var renderer = this.get('renderer');
          if (renderer) {
            renderer.send('initColorDensitometer');
          }
        },

      hideCanvas: function(){
          var renderer = this.get('renderer');
          if (renderer) {
            renderer.send('hideCanvas');
          }
        },
      removeMarkerOverlay: function(){
        var renderer = this.get('renderer');
        if (renderer) {
          renderer.send('removeMarkerOverlay');
        }
      },
        changeLockType: function(/*localVersion*/){
            //TODO check if this is stil needed
            //this.set('unlockButtonClass', 'czn-lock-button-gray');
        },
        displaySingleAnnotation: function(annotation){
            this.get('renderer').send('displaySingleAnnotation', annotation);
        },
        setMarkersCount: function(markersCount){
            //change the total number of annotations displayed on the thumbnail page of the current page
            if(this.get('model.pages')){
                var page = this.get('model').get('pages').findBy('PageNumber', this.get('currentPage.PageNumber'));
                if(page){
                    this.get('currentPage').set('markersCount', markersCount);

                    //hide the icon if current page has no annotations
                    if(markersCount > 0){
                        page.set('hasAnnotations', true);
                    }else{
                        page.set('hasAnnotations', false);
                    }
                }
            }
        },
        clearAnnotationsLayer: function(){
            this.get('renderer').send('clearAnnotationsLayer');
        },
        drawingModeInViewerController: function(enter, lineWidth, lineColor){
           this.get('renderer').send('drawingModeInRederer', enter, lineWidth, lineColor);
        },
        clearSelection: function(){
            this.get('renderer').send('clearSelection');
        },
        clearCanvas: function(){
            this.get('renderer').send('clearCanvas');
        },
        addShapeToCanvas: function(shape, color, mouseEventDetails){
            this.get('renderer').send('addShapeToCanvas', shape, color, mouseEventDetails);
        },
        initializeCanvas: function(view){
            this.get('renderer').send('initializeCanvas', view);
        },
        toggleAllTickets: function(show){
            this.get('renderer').send('toggleAllTickets', show);
        },
        initializeBalloons: function(view){
            this.get('renderer').send('initializeBalloons', view);
        },
        enableTextHighlighting: function(visible){
            var renderer = this.get('renderer');
            if (renderer) {
                renderer.send('enableTextHighlighting', visible);
            }
        },
        resetSeparations: function(){
            this.get('renderer').send('resetSeparations');
        },
        addNotification: function(msg){
            this.get('renderer').send('addNotification', msg);
        },
        clearNotifications: function(msg){
          this.get('renderer').send('clearNotifications');
        },
        selectAnnotationAsync: function(annotation){
            this.get('renderer').send('selectAnnotationAsync', annotation);
        },

        showCanvasForAnnotation:function(annotationId){
          this.get('renderer').send('showCanvasForAnnotation', annotationId);
        },

        addMarkerOverlay:function(point){
          this.get('renderer').send('addMarkerOverlay', point);
        },
        updateMarkerPosition: function(waitTime) {
          this.get('renderer').send('updateMarkerPosition', waitTime);
        }
    },

  changeCurrentPage: function(pageId) {
      if (pageId) {
        var page = this.store.all('page').filterBy('id', pageId.toString()).get('firstObject');
        if (page) {

          this.set('currentPage', page);// runs this.syncVersionsPages(); by observable
          this.setCurrentPageActive();

          var self = this;
          Ember.run.later(function () {
            Ember.run.later(function () {
              self.get('controllers.navigation/annotation').send('reloadSelected');
            }, 500);
          });
        }
      }
    },

    syncCanvasPosition: function(){
      this.get('renderer').syncCanvasPosition();
    },

    versionLocked: function(){
      return this.get('model.isLocked');
    }.property('model.isLocked'),

    unlockButtonClass: function(){
        var isLocked = this.get('model.isLocked'),
            userCanAnnotate = this.get('userCanAnnotate'),
            userCanApprove = this.get('userCanApprove'),
            isPhaseCompleteOrRejected = this.get('model.IsCompletedOrRejected');

        if (isLocked && !userCanAnnotate && !userCanApprove){
            return 'czn-lock-button-gray';
        }

        if (userCanAnnotate || userCanApprove) {

            if (isLocked === true) {
                return 'czn-lock-button-red';
            }
            else {
                return 'czn-lock-button-green';
            }
        }
        else{
            return 'czn-lock-button-gray'
        }
    }.property('model.isLocked', 'currentViewerController.model.id'),

    checkAnnotationsCount: function(){
        //check the total number of annotations on each page
        //and display it on the thumbnail page
        var self = this;
        var annotations = this.store.all('annotation');
        var pages = this.store.all('page');

        var currentVersionPages = pages.filter(function(page){
            return page.get('approval.id') === self.get('model.id');
        });
        if (currentVersionPages) {
            currentVersionPages.forEach(function(page){
                var annotationsForThisPage = annotations.filter(function(annotation){
                    return annotation.get('Page.id') === page.get('id') && !annotation.get('Parent');
                });
                page.set('hasAnnotations', annotationsForThisPage && annotationsForThisPage.get('length') > 0);
                if (annotationsForThisPage) {
                    page.set('markersCount', annotationsForThisPage.get('length'));
                }
            });
        }
    }.observes('model'),

    tileUrlChanged: function(){
        // this observable should not be executed when current page is changed,
        // else the zoom and the center of the map will be changed in compare mode when clicking from a version to another
        if (this.get('softProofingGuidChanged') && this.get('currentPage.id')) {
            this.set('updateTilesClicked', true);
            this.updateTilesUrl();
        }
    }.observes('softProofingGuidChanged'),

    getSoftProofing() {
      var self = this,
        sessionSoftProofingParams = this.get('session.softProofingParams'),
        softProofingParam = sessionSoftProofingParams ? sessionSoftProofingParams.filter(function(spp){
          return spp.versionId === self.get('model.id') && spp.isClone == self.get('model.isClone');
        }).get('firstObject') : null;

      return softProofingParam;
    },
    getUrl(softProofingParam) {
      if (!(this.get('currentPage') && softProofingParam)) {
        return null;
      }

      var url = this.get('currentPage.PageFolder') || '';
      url += softProofingParam.SessionGuid ? (softProofingParam.SessionGuid + '/') : '';
      url += this.get('currentPage.PageNumber') + '/';

      return url;
    },

    reloadFromUrl(versionId) {
      if (versionId != this.get('model.id')) {
        return;
      }

      var url = this.getUrl(this.getSoftProofing());

      this.set('tilesUrl', url);
      var openlayers = this.get('renderer');
      if (openlayers) {
        openlayers.refreshOpenLayers(url);
        openlayers.send('tilesReady');
      }
      this.send('clearNotifications');
      this.set('updateTilesClicked', false);
      this.set('updateTilesTriggeredGenerateTiles', false);
    },

    notifyUHRTriggered() {
      if (this.get('uhrTriggeredBy') == AppConstants.UltraHighResolutionTriggerStatus.UHRTriggeredFirstTimeByButton) {
        this.store.adapterFor('approval').SendNotificationThatVersionSwitchedToHighRes(this.get('model.id')).then(() => {
          //window.location.reload();
          this.get('instantNotificationsCommunicationService').ShowSwitchedToHighResInstantNotification(this.get('model.id'));
        });
      }
      else {
        this.get('instantNotificationsCommunicationService').ShowSwitchedToHighResInstantNotification(this.get('model.id'));
      }
      this.set('uhrTriggeredBy', AppConstants.UltraHighResolutionTriggerStatus.UHRNotTriggered);
    },

    updateTilesUrl: function(){
        var self = this,
          softProofingParam = this.getSoftProofing(),
          url = this.getUrl(softProofingParam);

        if (url) {
          function TilesReady() {
            console.log('TilesReady', self.get('updateTilesTriggeredGenerateTiles'), self.get('uhrTriggeredBy'), self.get('updateTilesClicked'));

            if (!self.get('updateTilesTriggeredGenerateTiles') && self.get('uhrTriggeredBy') == AppConstants.UltraHighResolutionTriggerStatus.UHRNotTriggered) {
              self.reloadFromUrl(self.get('model.id'));
              return;
            }

            if (self.get('uhrTriggeredBy') != AppConstants.UltraHighResolutionTriggerStatus.UHRNotTriggered) {
              self.notifyUHRTriggered();
            }
            else {
              if (self.get('updateTilesClicked')) {
                self.get('instantNotificationsCommunicationService').ShowNewTilesInstantNotification(self.get('model.id'));
              }
              else {
                self.reloadFromUrl(self.get('model.id'));
              }
            }
            self.set('updateTilesClicked', false);
          };

          // if partial tiles or completed tiles are available than render the tiles
          if(!this.get('model.isClone'))
          {
          this.checkIfTilesExist(url + AppConstants.PartialTileUrl)
            .done(TilesReady)
            .fail(function() {// partial tiles are not available
              self.checkIfTilesExist(url + AppConstants.SmallestTileUrl)
                .done(TilesReady) // make it compatible with previous versions
                .fail(function () {
                  self.set('updateTilesTriggeredGenerateTiles', true);
                  if (!self.get('isDestroyed')) {
                      //self.set('tilesUrl', null);
                      //self.clearLayers();
                      self.send('generateTiles', {
                        pageId: self.get('currentPage.id'),
                        SessionGuid: softProofingParam.SessionGuid,
                        viewerRenderer: self
                      });
                  }
              })
            });
          }
          else {
            TilesReady();
          }
        }
        else{
            self.set('tilesUrl', url);
        }
    },

    clearLayers: function(){
        var renderer = this.get('renderer');

        if (renderer){
            renderer.send('clearMarkers');
            renderer.get('model').set('generateTilesInProgress', true);
        }

        this.send('doneAnnotating');
        Ember.$(".czn-navbar-main-controls").hide(); // hide add annotation top menu
        Ember.$("#soft-proofing-menu").hide(); // hide soft proofing menu
        Ember.$("li.dropdown-profiles").hide();
    },

    originalImageSize: function() {
        return {
            width: this.get('currentPage.OutputImageWidth') || this.get('currentPage.OriginalImageWidth'),
            height: this.get('currentPage.OutputImageHeight') || this.get('currentPage.OriginalImageHeight')
        };
    }.property('currentPage'),

    //this function will be executed when currentPage is changed
    //or PageSync button is pressed
    syncVersionsPages: function(){
      var self = this,
            map = this.get('renderer.approval.map');
        if (map && this.get('model.isCompareMode') === true) {
            var versions = this.get('compareVersions'),
                currentVersion = this.get('currentVersion'),
                pageNumber = currentVersion.get('currentPage.PageNumber');

            //check if the PageSync button is selected
            if (this.get('isPageSyncSelected') && pageNumber && this.get('previousPageNr') !== pageNumber) {
                versions.forEach(function (version) {
                    var page = self.store.all('page').filter(function (page) {
                        return page.get('Approval.id') === version.get('id') && page.get('PageNumber') === pageNumber;
                    }).get('firstObject');

                    //if versions have similar number of pages
                    //then the current page of each version will be the same as in selected version
                    if (page) {
                        self.set('previousPageNr', pageNumber);

                        self.store.all('page').forEach(function (pp) {
                            if (pp.get('Approval.id') === version.get('id') && pp.get('PageNumber') === pageNumber) {
                                version.set('currentPage', page);
                                self.setCurrentPageActive();
                            }
                        });
                    }
                    else {
                        var lastPage = null;
                        self.store.all('page').forEach(function (pp) {
                            if (pp.get('Approval.id') === version.get('id')) {
                                pp.set('active', false);
                                if (!lastPage) {
                                    lastPage = pp;
                                }
                                if (lastPage.get('PageNumber') < pp.get('PageNumber')) {
                                    lastPage = pp;
                                }
                            }
                        });
                        if (lastPage) {
                            version.set('currentPage', lastPage);
                            self.setCurrentPageActive();
                        }
                    }
                });
            }
        }
    }.observes('isPageSyncSelected', 'compareVersions.@each.currentPage'),

    getCurrentPage: function(pages){
        var self = this;
        pages = pages || this.store.all('page');
        if (this.get('isOverlayMode') === true || this.get('isCompareMode') === true){
            var overlayPages = this.get('overlayPageSelection');

            var page = (overlayPages || []).filter(function(op){
                return op.version === self.get('model.id');
            }).get('firstObject');

            if (page){
                var pageId = page.page;

                return pages.filter(function(p){
                    return p.get('id') === pageId && self.get('model.id') === p.get('Approval.id');
                }).get('firstObject');
            }
            else{
                return pages.filter(function(p){
                    return p.get('Approval.id') === self.get('model.id') && p.get('PageNumber') === 1;
                }).get('firstObject');
            }
        }
        else{
            return pages.filter(function(p){
                return p.get('Approval.id') === self.get('model.id') && p.get('PageNumber') === 1;
            }).get('firstObject');
        }
    },


    pagerClass: function(){
        if(!this.get('model.isCompareMode')){
            if(this.get('model.paginationVisible')){
                return "czn-page-navigation-up";
            }else{
                return "czn-page-navigation-down";
            }
        }else{
            if(this.get('isVertical')){
                if(this.get('model.paginationVisible')){
                    return "czn-page-navigation-up czn-horizontal-page-navigation-up";
                }else{
                    return "czn-page-navigation-down czn-horizontal-page-navigation-down";
                }
            }else{
                if(this.get('model.paginationVisible')){
                    return "czn-page-navigation-up czn-horizontal-page-navigation-up";
                }else{
                    return "czn-page-navigation-down czn-horizontal-page-navigation-down";
                }
            }
        }
    }.property('controllers.image/controls/image.isVertical', 'controllers.image/controls/image.isHorizontal','model.paginationVisible'),

    pageClass: function(){
        if(!this.get('model.isCompareMode')){
            return "czn-page-viewer list-inline list-unstyled unselectable";
        }else{
            return "czn-page-viewer czn-horizontal-page-viewer list-inline list-unstyled unselectable";
        }
    }.property('controllers.image/controls/image.isVertical', 'controllers.image/controls/image.isHorizontal','model.paginationVisible'),

    scrollToAll: function(){
        var self = this;

        //move thumbnails scroll bar for all versions from compare mode when active page is changed
        //and also for single mode
        if(this.get('isPageSyncSelected') || !this.get('model.isCompareMode')){
            Ember.$('.czn-page-viewer').each(function(){
                var left = (self.get('currentPage.PageNumber') - 2) * 100;
                Ember.$(this).scrollLeft(left);
            });
            Ember.$('.czn-pager-btn').off('click', this.changePageButton(self)).on('click', this.changePageButton(self));
        }else{
            this.scrollTo();
        }
    }.observes('model','currentPage','model.paginationVisible','isPageSyncSelected'),

    changePageButton: function(self){
        var pageNumber = self.get('currentPage.PageNumber');
        Ember.$('.czn-page-viewer').each(function () {
            var left = (pageNumber - 2) * 100;
            Ember.$(this).scrollLeft(left);
        });
    },

    scrollTo: function(){
        var self = this;
        //move thumbnails scroll bar only for the selected version from compare mode
        Ember.run.later(function(){
            var child = Ember.$('.czn-versions-ul .czn-versions-li.selectable.active section > ul');
            child.each(function(){
                var left = (self.get('currentPage.PageNumber') - 2) * 100;
                Ember.$(this).scrollLeft(left);
            });

            var pagerBtn = Ember.$('.czn-versions-ul .czn-versions-li.selectable.active span.czn-pager-btn');
            pagerBtn.click(function (){
                Ember.run.later(function() {
                    child.each(function () {
                        var left = (self.get('currentPage.PageNumber') - 2) * 100;
                        Ember.$(this).scrollLeft(left);
                    });
                });
            });
        });
    },

    pages: function(){
        var pages = [], self = this;
        var annotations = this.store.all('annotation');
        var pps = this.store.all('page').filter(function(page){
            return page.get('Approval.id') === self.get('model.id');
        });
        pps.forEach(function(page){
            page.set('markersCount', annotations.filter(function(annotation){
                return annotation.get('Page.id') === page.get('id') && !annotation.get('Parent');
            }).get('length'));
            page.set('hasAnnotations', page.get('markersCount') > 0);
            pages.push(page);
        });
        pages.sort(function(page1,page2) {
            if (page1.get('PageNumber') < page2.get('PageNumber')) {
                return -1;
            }
            else if (page1.get('PageNumber') > page2.get('PageNumber')) {
                return 1;
            }
            else{
                return 0;
            }
        });
        return pages;
    }.property('currentPage', 'annotationsChanged', 'controllers.approval.pagesChanged'),

    getData: function(forceReload){
        var self = this,
            approvalId = this.get('model.id'),
            currentVersionHasPages = this.store.all('page').filter(function(page){
                return page.get('Approval.id') === approvalId;
            }).get('length') > 0;

        if (!currentVersionHasPages && !self.get('isDestroyed')) {
            if(!this.get('loadingPages')) {
              this.set('loadingPages', true);
              self.store.find('page', {approvalId: approvalId, reload: true}).then(function (pages) {
                if (self.get('isDestroyed')) {
                  return false;
                }
                self.set('pagesChanged', new Date());

                self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', true);
                self.store.find('annotation', {approvalId: approvalId, isPreviousVersion: true}).then(function () {
                  self.set('annotationsChanged', new Date());
                  self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', false);
                }).catch(function (/*e*/) {
                  try {
                    if (self.get('isDestroyed')) {
                      return false;
                    }
                    self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', false);
                  }
                  catch (e) {

                  }
                });

                var currentPage = Ember.isArray(pages) ? pages.get('firstObject') : pages;
                if (forceReload == true || (currentPage && currentPage.get('id') !== self.get('currentPage.id'))) {
                  self.set('currentPage', currentPage);
                  self.setCurrentPageActive();
                }

                self.set('loadingPages', false);
                self.updatePages();
              }).catch(function() {
                self.set('loadingPages', false);
              });
            }
        } else {
          self.updatePages();
        }
    },

    updatePages: function(){
      var self = this;

      if (this.get('timer')){
        Ember.run.cancel(this.get('timer'));
      }

      var timer = Ember.run.later(function(){
        var pagesCount = self.store.all('page').filter(function(page){
            return page.get('Approval.id') === self.get('model.id');
          }).get('length'),
          approvalId = self.get('model.id');

        if (self && !self.get('isDestroyed') && pagesCount < self.get('model.PagesCount') && approvalId ) {
          self.store.find('page', {approvalId: approvalId}).then(function () {
            self.set('pagesChanged', new Date());
            self.updatePages();
          });
        }
        else if (!approvalId){
          self.updatePages();
        }
      }, 1000);

      if (!this.get('isDestroyed') && timer) {
        this.set('timer', timer);
      }
    },

    overlayPageSelection: function(){
        return eval('(' + window.sessionStorage.getItem('overlay-pages') + ')');
    }.property('model'),

    showIsLockedIcon: function() {
      if (this.get('isOverlayMode') == true) {
        // only the top version controls if is locked button is shown
        var isFirstVersionOnTop = this.get('controllers.approval.isFirstOverlayVersionOnTop');
        var versions = this.get('controllers.approval.selectedVersions') || [];
        var topVersion = versions.filterBy('select', isFirstVersionOnTop).get('firstObject');

        if (topVersion) {
          if (topVersion.get('id') == this.get('model.id') && topVersion.get('isClone') == this.get('model.isClone'))
            return topVersion.get('IsLocked');
          else
            return false;
        }
        else {
          return this.get('model.IsLocked');
        }
      }
      return this.get('model.IsLocked');

    }.property('model.IsLocked', 'isOverlaySelected', 'controllers.approval.isFirstOverlayVersionOnTop')
});
export default ImageViewerController;
