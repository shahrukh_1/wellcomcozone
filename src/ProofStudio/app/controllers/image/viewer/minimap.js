import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var MiniMapController = Ember.ObjectController.extend(CurrentViewerMixin, {
    needs: ['approval'],
    selection: { top: 10, left: 10, width: 10, height: 10 },
    mapSize: { width: 0, height: 0 },
    selectionElement: null,
    view: null,
    miniMapIsUp: false,
    isHidden: true,
    actions:
    {
        initializeView: function(view){
            this.set('view', view);
            this.set('selectionElement', view.$("div.czn-minimap-selection"));

            var thumbnail = view.$('img.czn-minimap-img');
            this.set('mapSize', {
                width: thumbnail.width(),
                height: thumbnail.height()
            });
        },

        showMiniMap: function(show){
            this.set('isHidden', show);
        }
    }
});
export default MiniMapController;
