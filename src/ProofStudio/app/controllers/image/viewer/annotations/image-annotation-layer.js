import Ember from "ember";
import AnnotationLayer from 'appkit/controllers/viewer/annotations/annotation-layer';
import AppConstants from 'appkit/config/constants';
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var ImageAnnotationLayer = AnnotationLayer.extend(CurrentViewerMixin, {
    needs: ['navigation/annotation'],
    actions: {
    },
    addNewAnnotation: function () {
    }.observes('newAnnotationAddingState'),

    newAnnotationClicked: function (param) {
        var pageId = this.get('pageId');
        this.send('newComment', {Point: param.point, PageId: pageId, CommentType: AppConstants.CommentTypes.MARKER_COMMENT});
        this.set('newAnnotationAddingState', false);
    },

    refreshSingleAnnotationLayer: function () {
        var layerPins = this.get('layerPins');

        var annotation = this.get('annotation');
        var singleAnnotationView = this.get('singleAnnotationView');

    }.observes('layerIsVisible', 'singleAnnotationView'),

    addShapeToCanvas: function(shape, color, mouseEventDetails){
        var layerCanvas = this.get('model.image-canvas-view');
        if (layerCanvas){
            var map = this.get("model.approval.map");
            layerCanvas.addShapeToCanvas(shape, color, map, mouseEventDetails);
        }
    },

    closeAddMarker: function(){
        this._super();
        var layerCanvas = this.get('model.image-canvas-view');
        var map = this.get('model.approval.map');
        if (layerCanvas && map){
            //layerCanvas.enableMapEvents(map);
            layerCanvas.hide();
        }
    },

    initAddMarkerInImageAnnotationLayer: function (annotationType, editMode) {
        this._super(annotationType, editMode);
        if (annotationType === AppConstants.CommentTypes.LINE_COMMENT || annotationType === AppConstants.CommentTypes.COLOR_PICKER_COMMENT){
          var layerCanvas = this.get('model.image-canvas-view');
          var map = this.get('model.approval.map');

          if (layerCanvas) {
            layerCanvas.enableFabric();
            layerCanvas.closeAddLine();
            layerCanvas.closeColorPicker();
          }
          if (annotationType === AppConstants.CommentTypes.LINE_COMMENT && editMode === true) {
              if (layerCanvas && map) {
                  layerCanvas.clearCanvas();
                  //layerCanvas.disableMapEvents(map, true);
                  layerCanvas.initAddLine();
              }
          }
          else if (annotationType === AppConstants.CommentTypes.COLOR_PICKER_COMMENT && editMode === true){
              if (layerCanvas && map) {
                  layerCanvas.clearCanvas();
                  //layerCanvas.disableMapEvents(map, true);
                  layerCanvas.initColorPicker();
              }
          }
        }
    },

  drawingModeInAnnotationLayer: function(enter, lineWidth, lineColor){
    lineWidth = lineWidth * 0.25;
    var isMarkerSelected = this.get('navigationAnnotation.isMarkerSelected');
    if(!isMarkerSelected) {
      var layerCanvas = this.get('model.image-canvas-view'),
        map = this.get("model.approval.map"),
        select  = this.get('model.approval.select'),
        self = this;

      var freeDrawingMode = layerCanvas.get('freeDrawingMode');

      var canvas = layerCanvas.imageFabricCanvas;
      if (canvas) {
        var selectedObject = canvas.getActiveObject();
        if (selectedObject) {
          if(selectedObject.get('type') === "group") {
            selectedObject.forEachObject(function(obj) {
              self.setObjectStyle(obj, lineColor, lineWidth, layerCanvas);
            });
          }
          else {
            this.setObjectStyle(selectedObject, lineColor, lineWidth, layerCanvas);
          }
          canvas.renderAll();
        } else {
          if(lineWidth) {
            if (enter) {
              layerCanvas.enableFabric();
              this.send("clearPinsLayers", map, select);
            }

            layerCanvas.isDrawingMode = true;
            canvas.selection = false;
            canvas.draggable = false;
            canvas.isDrawingMode = enter;

            if (enter === true) {
              layerCanvas.set('freeDrawingMode', true);
              canvas.freeDrawingBrush.color = lineColor;
              canvas.freeDrawingBrush.width = lineWidth;
              canvas.freeDrawingBrush.shadowBlur = 0;
              layerCanvas.set('annotation', null);

              layerCanvas.get('controller.setMaskFromPixelFromCoordinates')(layerCanvas);
            }
          }
        }
      }
    }
  },

  selectAnnotation: function (annotation) {
    this.send('selectAnnotation', annotation);
  }
});
export default ImageAnnotationLayer;
