import Ember from "ember";

var CanvasViewController = Ember.ObjectController.extend({
    needs: ['comments/index', 'image/controls/image', 'approval', 'navigation/annotation', 'image/viewer/image'],
    annotation: Ember.computed.alias('model.selectedAnnotation'),
    actions: {
        initializeView: function (view) {
            this.set('view', view);
            this.send('initializeCanvas', view, this);
        }
    },
    getMapCoordinates: function(mapPosition) {
        if (mapPosition.angle == 0) {
          return {left: mapPosition.left, top: mapPosition.top, width: mapPosition.width, height:mapPosition.height};
        }
        if (mapPosition.angle == 90) {
          return {left: mapPosition.left - mapPosition.height, top: mapPosition.top, width: mapPosition.height, height:mapPosition.width};
        }
        if (mapPosition.angle == 180) {
          return {left: mapPosition.left - mapPosition.width, top: mapPosition.top - mapPosition.height, width: mapPosition.width, height:mapPosition.height};
        }
        if (mapPosition.angle == 270) {
          return {left: mapPosition.left, top: mapPosition.top - mapPosition.width, width: mapPosition.height, height:mapPosition.width};
        }
    },
    isInMap: function(mapPosition, point) {
        if (mapPosition.angle == 0) {
          return point.x <= mapPosition.left + mapPosition.width && point.x >= mapPosition.left && point.y <= mapPosition.top + mapPosition.height && point.y >= mapPosition.top;
        }
        if (mapPosition.angle == 90) {
          return point.x <= mapPosition.left && point.x >= mapPosition.left - mapPosition.height && point.y <= mapPosition.top + mapPosition.width && point.y >= mapPosition.top;
        }
        if (mapPosition.angle == 180) {
          return point.x <= mapPosition.left && point.x >= mapPosition.left - mapPosition.width && point.y <= mapPosition.top && point.y >= mapPosition.top - mapPosition.height;
        }
        if (mapPosition.angle == 270) {
          return point.x <= mapPosition.left + mapPosition.height && point.x >= mapPosition.left && point.y <= mapPosition.top && point.y >= mapPosition.top - mapPosition.width;
        }
    },
    setMaskFromMapPosition: function(layerCanvas) {
        if (layerCanvas) {
          var mapPosition = layerCanvas.get('controller.getMapCoordinates')(layerCanvas.get('controller.model.mapPosition'));
          var rect = new fabric.Rect({
            left: mapPosition.left,
            top: mapPosition.top,

            width: mapPosition.width,
            height: mapPosition.height,
            stroke: '#000',
            strokeWidth: 0,
            fill: "transparent",
          });
          var canvas = layerCanvas.imageFabricCanvas;
          if (canvas) {
            canvas.clipTo = function (ctx) {
              rect.render(ctx);
            };
            canvas.renderAll();
          }
        }
    },
    // this method should be called when mapPosition is not updated
    setMaskFromPixelFromCoordinates: function(layerCanvas) {
        var currentView = layerCanvas.$();
        if (currentView && currentView.is(":visible")) {
          if (layerCanvas) {
            var map = layerCanvas.get('controller.approval.map'),
              startingPointPosition = map ? map.getPixelFromCoordinate([0, 0]) : [0, 0],
              view = map ? map.getView() : null,
              resolution = view ? view.getResolution() : 1,
              rotation = view ? view.getRotation() : 0, // in radians
              angle = rotation * 180 / Math.PI, // in degrees
              scale = 1 / resolution,
              width = layerCanvas.get('controller.size.width') * scale,
              height = layerCanvas.get('controller.size.height') * scale;

            if (startingPointPosition) {
              var mapPosition = layerCanvas.get('controller.getMapCoordinates')({
                left: startingPointPosition[0],
                top: startingPointPosition[1],
                width: width,
                height: height,
                angle: angle
              });
              var rect = new fabric.Rect({
                left: mapPosition.left,
                top: mapPosition.top,

                width: mapPosition.width,
                height: mapPosition.height,
                stroke: '#000',
                strokeWidth: 0,
                fill: "transparent",
              });

              var canvas = layerCanvas.imageFabricCanvas;
              if (canvas) {
                canvas.clipTo = function (ctx) {
                  rect.render(ctx);
                };
                canvas.renderAll();
              }
            }
          }
        }
    }
});
export default CanvasViewController;
