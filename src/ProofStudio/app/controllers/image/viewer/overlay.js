import Ember from "ember";

var OverlayVersionsIndexController = Ember.ArrayController.extend({
    needs: ['approval', 'image/controls/image'],
    versions: Ember.computed.alias('controllers.approval.versionsForOverlay'),
    paginationVisible: Ember.computed.alias('controllers.approval.paginationVisible'),
});
export default OverlayVersionsIndexController;
