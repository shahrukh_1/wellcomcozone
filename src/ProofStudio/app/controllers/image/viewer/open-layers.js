import Ember from "ember";
import AppConstants from 'appkit/config/constants';
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';


var OpenLayersViewer = Ember.ObjectController.extend(CurrentViewerMixin, {
  needs: ['application', 'image/viewer/minimap', 'image/controls/image', 'navigation/main', 'approval', 'comments/index', 'sidebar/index', 'image/separations/index', 'navigation/annotation'],
  approval: null,
  url: Ember.computed.alias('model.tilesUrl'),
  size: Ember.computed.alias('model.originalImageSize'),
  thumbnail: Ember.computed.alias('model.currentPage.LargeThumbnailPath'),
  miniMapController: Ember.computed.alias('controllers.image/viewer/minimap'),
  isSyncZoomSelected: Ember.computed.alias('controllers.image/controls/image.isZoomSync'),
  versions: Ember.computed.alias('controllers.navigation/main.selectedVersions'),
  currentVersion: Ember.computed.alias('controllers.approval.currentVersion'),
  versionsForOverlay: Ember.computed.alias('controllers.approval.versionsForOverlay'),
  currentZoom: 0,
  miniMapControl: null,
  separationsUrl: null,
  separationsOptions: null,
  miniMapIsUp: Ember.computed.alias('controllers.image/viewer/minimap.miniMapIsUp'),
  backgroundColor:'',
  approvalMap: null,
  isSyncSelected:  Ember.computed.alias('controllers.image/controls/image.isSyncSelected'),
  isOverlaySelected: Ember.computed.alias('controllers.image/controls/image.isDifferenceSelected'),
  canSyncVersions: false,
  previousVersion: null,
  currentCenterPoint: [0,0],
  commentsDialogIsVisible: Ember.computed.alias('controllers.comments/index.visible'),
  compareVersions: Ember.computed.alias('controllers.approval.selectedVersions'),
  pinsAreShown: Ember.computed.alias('controllers.navigation/main.isShowPins'),
  annotations: Ember.computed.alias('model.annotations'),
  notifications: Ember.A(),
  markers: Ember.A(),
  textHighlightingWords: null,
  gettingDataInProgress: false,
  restrictedZoom:  false,

  onInit: function() {
    this.get('modalDialogsCommunicationService').RegisterOnCloseAlertUHRDialog(this,'removeColorPicker');
    var self = this;
    Ember.run.later(function(){
    if(window.AppkitENV.SelectedAnnotationID != "0" && self.store.all("annotation").filterBy('id', window.AppkitENV.SelectedAnnotationID).length > 0){
      var ann = self.store.all("annotation").filterBy('id', window.AppkitENV.SelectedAnnotationID)[0];
      self.send('selectAnnotation', ann);
    }
  },500);
  }.on('init'),

  onCleanup: function() {
    this.get('modalDialogsCommunicationService').UnRegisterOnCloseAlertUHRDialog(this,'removeColorPicker');
  }.on('willDestroyElement'),

  removeColorPicker(versionId) {
    if (this.get('model.id') != versionId)
      return;

    this.set('addColorPicker', false);
  },

  actions: {
    initializeView: function(view) {
      this.set('view', view);

      this.send('initializeRenderer', this);

      this.send('changeBackgroundColorPicker');
    },
    initializeImageIndex: function(pinsLayer){
      // pins is the image-index controller
      this.set('image-annotation-layer', pinsLayer);
    },
    addMarkerOverlay: function(point) {
      var map = this.get('approval.map');
      if(map) {
        //if(Ember.$(this.getPathForSelectElement(".annMarker")).length > 1){
          while(Ember.$(this.getPathForSelectElement(".annMarker")).length > 1){
            Ember.$(this.getPathForSelectElement(".annMarker"))[0].remove();
          }
      //  }
        Ember.$(this.getPathForSelectElement(".annMarker")).show();
        var markerOverlay = this.getOverlayByProperty('isMarkerOverlay');
        if(!markerOverlay) {
           markerOverlay = new ol.Overlay({
            position: point,
            element: Ember.$(this.getPathForSelectElement(".annMarker"))[0]
          });
          markerOverlay.set("isMarkerOverlay", true);
          map.addOverlay(markerOverlay);
        }
        else {
          markerOverlay.setPosition(point);
        }
      }
    },

    removeMarkerOverlay: function(){
      var self = this;
      var map = this.get('approval.map');
      if(map) {
        var markerOverlay = this.getOverlayByProperty('isMarkerOverlay');

        if(markerOverlay) {
          Ember.$(self.getPathForSelectElement(".annMarker")).appendTo(self.getPathForSelectElement(''));

          map.removeOverlay(markerOverlay);
        }
        Ember.$(self.getPathForSelectElement(".annMarker")).hide();
      }
    },

    updateMarkerPosition: function(waitTime) {
      var markerOverlay = this.getOverlayByProperty('isMarkerOverlay');
      if(markerOverlay) {
        var element = markerOverlay.getElement();
        var commentsController = this.get('controllers.comments/index');
        if(commentsController)
        {
          if(!waitTime) {
            waitTime = 5;
          }
          Ember.run.later(function(){
            var offset = Ember.$(element).offset();
            if(offset) {
              commentsController.send('setMarkerPosition', offset.left, offset.top);
            }
          }, waitTime);
        }
      }
    },

    getMapPixelFromCoordinate: function(point){
      var map = this.get('approval.map');

      if(map){
        this.set('currentPixelFromCoordinate', map.getPixelFromCoordinate(point));
      }
    },

    zoomTo: function(zoom) {
      var map = this.get('approval.map'),
        view = map ? map.getView() : null;
      if(view) {
        view.setZoom(zoom);
      }
      //this.set('currentZoom', zoom);
    },

    zoomToResolution: function(resolution) {
      //alert("x: " + resolution);
      var map = this.get('approval.map'),
        view = map ? map.getView() : null;
      if (!view) {
        return;
      }

      if (resolution > this.get('maxRezolution')) {
        view.setResolution(this.get('maxRezolution'));
        return;
      }
      if (resolution < this.get('minRezolution')) {
        view.setResolution(this.get('minRezolution'));
      }
      view.setResolution(resolution);
    },

    //changeToHighResolution() {
    //  alert('ch to high 2');
    //},

    fit: function(){
      var approval = this.get('approval');
      if(approval && approval.map) {
        var map = approval.map;
        map.updateSize();

        var mapSize = map.getSize();

        var bottomOffset = this.get('model.isCompareMode') && this.get('model.isOverlayMode') === false ? 0 : AppConstants.BottomBarHeight;

        this.changeZoomToFit(approval.map);

        map.getView().centerOn([this.get('size.width') / 2, -this.get('size.height') / 2], [mapSize[0], mapSize[1] - bottomOffset], [mapSize[0] / 2, mapSize[1] / 2 - bottomOffset]);

        this.set('isFitToPage', true);
        var self = this;
        Ember.run.later(function() {
          var session = self.get('session');
          session.set('previousPageZoomLevel', 0);
        }, 500);
      }
      else {
        console.log('approval or approval map not defined. Try to re fit');
        var self = this;
        Ember.run.later(function() {
          self.fit();
        }, 500);
      }
    },

    rotate: function() {
      var map = this.get('approval.map'),
        view = map ? map.getView() : null;
      if(view) {
        var angle = view.getRotation() + Math.PI / 2;
        if(angle == Math.PI * 2){
          angle = 0;
        }
        view.setRotation(angle);
      }

      if(this.get('isFitToPage')){
        this.send('fit');
      }
    },

    showSeparation: function(separation){
      if (separation === null) {
        this.set('separationsUrl', null);
      }
      else
      {
        this.set('separationsUrl', 'separations/' + separation + '/');
      }
      var approval = this.get('approval');
      this.set('separationsOptions', { center: approval.map.getCenter(), zoom: approval.map.getZoom() });
      this.set('lastSeparationColorName', separation);
    },
    showBottomNavigation: function(show){
      this.set('miniMapIsUp', show);
    },

    syncOverlappedVersions: function(){
      if (this.get('isDestroyed') === false) {
        this.set('canSyncVersions', !this.get('canSyncVersions'));
      }
    },

    initAddMarkerInRederer: function(annotationType, editMode){
      if(annotationType == AppConstants.CommentTypes.MARKER_COMMENT) {
        this.set('canClickMap', true);
        this.send('hideCanvas');
      }

      this._super();
      this.send("clearMarkers");
      this.send('clearPinsLayers', this.get('approval.map'), this.get('approval.select'));
      this.set('annotation', null);

      var pinsImage = this.get('image-annotation-layer');
      if (pinsImage){
        pinsImage.initAddMarkerInImageAnnotationLayer(annotationType, editMode);
        var map = pinsImage.get('map');
        if (map){
          map.fullSize = this.get('size');
          map.pageId = this.get('model.currentPage.id');
        }

        if (annotationType === AppConstants.CommentTypes.TEXT_COMMENT) {
          pinsImage.set('newAnnotationAddingState', false);
        }
      }

      if (annotationType === AppConstants.CommentTypes.TEXT_COMMENT) {
        var textHighlighting = this.get('textHighlighting');
        if (textHighlighting) {
          textHighlighting.show();
        }
      }

      if (annotationType !== AppConstants.CommentTypes.LINE_COMMENT) {
        var canvas = this.get('image-canvas-view');
        if (canvas) {
          canvas.closeAddLine();
        }
      }

      if (annotationType !== AppConstants.CommentTypes.COLOR_PICKER_COMMENT) {
        var canvas = this.get('image-canvas-view');
        if (canvas) {
          canvas.closeColorPicker();
        }
      }
    },
    closeAddMarker: function(){
      var pinsImage = this.get('image-annotation-layer');
      if (pinsImage){
        pinsImage.closeAddMarker();
      }
      var textHighlighting = this.get('textHighlighting');
      if (textHighlighting){
        textHighlighting.hide();
      }
    },

    initColorDensitometer: function(){
      var map = this.get('approval.map');
      this.set('addColorPicker', true);

      var self = this,
        canvasView = this.get('image-canvas-view');

      if(canvasView) {
        canvasView.set('addMeasurementLine', false);
        var line = canvasView.get('line');
        if (line){
          line.remove();
        }
        canvasView.closeSoftProofingPopup(map);
      }

      this.set('lineStarted', false);
      this.send('clearPinsLayers', map, this.get('approval.select'));
      this.send('hideCanvas');
    },

    displaySingleAnnotation: function(annotation){
      var pinsImage = this.get('image-annotation-layer');
      if (pinsImage){
        pinsImage.initAddMarkerInImageAnnotationLayer(annotation.get('CommentType'));
        pinsImage.displaySingleAnnotation(annotation);
      }
    },

    displaySingleAnnotationPopup: function(annotation){
      var map = this.get('approval.map');
      var pinsImage = this.get('image-annotation-layer');
      if (pinsImage){
        pinsImage.displaySingleAnnotation(annotation);
      }
      var self = this;
      Ember.run.later(function(){
        self.send('clearPinsLayers', map, self.get('approval.select'));
      });
    },

    drawingModeInRederer: function(enter, lineWidth, lineColor){
      var pinsImage = this.get('image-annotation-layer');
      if (pinsImage){
        var map = pinsImage.get('map');
        if (map){
          map.fullSize = this.get('size');
          map.pageId = this.get('model.currentPage.id');
        }
        pinsImage.drawingModeInAnnotationLayer(enter, lineWidth, lineColor);
      }
    },

    clearCanvas: function(){
    },
    clearSelection: function(){
    },
    clearAnnotationsLayer: function(){
    },

    initializeCanvas: function(view, controller){
      this.set('image-canvas-view', view);
      this.set('canvas-controller', controller);
    },

    initializeBalloons: function(view, controller){
      this.set('balloons-view', view);
      this.set('balloons-controller', controller);
    },

    initializeNotificationsView: function(view){
      this.set('notifications-view', view);
    },
    addShapeToCanvas: function(shape, color, mouseEventDetails){
      var pinsImage = this.get('image-annotation-layer');
      if (pinsImage){
        var map = pinsImage.get('map');
        if (map){
          map.fullSize = this.get('size');
          map.pageId = this.get('model.currentPage.id');
        }
        pinsImage.addShapeToCanvas(shape, color, mouseEventDetails);
      }
    },
    enableTextHighlighting: function(visible){
      var textHighlighting = this.get('textHighlighting');
      if (textHighlighting){
        if (visible === true) {
          textHighlighting.show();
          textHighlighting.set('canSelectText', true);
        }
        else{
          textHighlighting.hide();
          textHighlighting.set('canSelectText', false);
        }
      }
    },
    initializeTextHighlighting: function(textHighlightingView){
      if (textHighlightingView && this.get('isDestroyed') === false && textHighlightingView.get('isDestroyed') === false ) {
        this.set('textHighlighting', textHighlightingView);
      }
    },
    toggleAllTickets: function(show){
      var balloonsView = this.get('balloons-view');
      if (balloonsView){
        if (show){
          balloonsView.showLayer();
        }
        else{
          balloonsView.hideLayer();
        }
      }
    },
    resetSeparations: function(){
      this.set('separationsUrl', null);
    },

    addNotification: function(msg){
      var notificationsView = this.get('notifications-view');
      if (notificationsView) {
        notificationsView.send('addNotification', msg);
      }
    },
    clearNotifications: function(){
      var notificationsView = this.get('notifications-view');
      if (notificationsView) {
        notificationsView.send('clearNotifications');
      }
    },
    selectAnnotationAsync: function(annotation){
      this.set('selectAnnotationAsync', annotation);
    },
    removeMarker: function(marker){
      this.get('markers').removeObject(marker);
    },
    clearMarkers: function(){
      //this.set('markers', Ember.A([]));

      //Ember.$('div.leaflet-marker-icon').remove();
    },
    clickMap: function(ev){
        var point = ev.point,
          pixel = ev.ev.pixel;
        var pins = this.get('image-annotation-layer');
      pins.newAnnotationClicked(ev);
      if(pixel) {
        var offset = this.get('mapOffset');
        this.send('addMarkerOverlay', [point[0] - 5, point[1] + 5]);
        this.send('updateMarkerPosition');
        // SVG
      }
    },

    showCanvas: function(annotationId){
      var canvas = this.get('image-canvas-view');
      if(canvas) {
        canvas.redraw(annotationId);
        canvas.get('controller.setMaskFromPixelFromCoordinates')(canvas);
      }
    },

    hideCanvas: function(){
        var canvas = this.get('image-canvas-view');

        if(canvas){
          canvas.disableFabric();
      }
    },

    updateRotation: function(rotation){
      var canvas = this.get('image-canvas-view');
      canvas.updateRotation(rotation);
    },

      updateScale: function(factor){
        var canvas = this.get('image-canvas-view');
        if(canvas) {
          canvas.updateScale(factor);
        }
      },

      updateCenter: function(center, rotation){
        var canvas = this.get('image-canvas-view');
        if(canvas) {
          canvas.updateCenter(center);
        }
    },

    clearPinsLayers: function(map, select){
      if(select!= null){
      select.getFeatures().clear();
      }
      if(map && map.getLayers().getArray().length > 1) {
        map.removeLayer(map.getLayers().getArray()[1]);
      }
    },

    showCanvasForAnnotation: function(annotationId){
      var map = this.get('approval.map');
      if(map != null){
        this.send('clearPinsLayers', this.get('approval.map'), this.get('approval.select'));
        this.centerMapToAnnotation(annotationId);
        this.showCanvasForAnnotation(map, annotationId);
      }
    },

      dispatchResizeEvent: function(newSize) {
      var textHighlighting = this.get('textHighlighting');
      if(textHighlighting) {
        textHighlighting.syncPosition();
      }

        var canvas = this.get('image-canvas-view');
        if(canvas) {
          canvas.syncCanvasPosition(newSize);
        }

      this.send('updateMarkerPosition');
    },

    getTextHighlightData: function(pageId, approvalId){
        this.set('gettingDataInProgress', true);
        this.set('textHighlightingWords', []);

        this.getTextHighlightingWords(pageId, approvalId);
    },
  },

  getTextHighlightingWords: function(pageId, approvalId) {
    var self = this;

    // Check if words were already loaded in store
    var words = self.store.all('text-highlighting-word');

    if(words != null && words.content.length > 0) {
      var filteredWordsList = words.content.filterBy('pageId', parseInt(pageId));

      if(filteredWordsList.length > 0) {
        self.setTextHighlightingWordsProperties(filteredWordsList);
      } else {
        this.getTextHighlightingFromServerSide(pageId, approvalId);
      }
    } else {
      this.getTextHighlightingFromServerSide(pageId, approvalId);
    }
  },

  getTextHighlightingFromServerSide: function(pageId, approvalId){
    var self = this;

    this.store.find('text-highlighting-word', { pageId: pageId, approvalId: approvalId}).then(function(words) {
        if(words != null && words.content.length > 0) {
          self.setTextHighlightingWordsProperties(words.content);
        } else {
          self.get('controllers.navigation/annotation').send('textHighlighting', false);
          self.set('gettingDataInProgress', false);
        }
    });
  },

  setTextHighlightingWordsProperties: function(words) {
    if (!this.get('controllers.navigation/annotation')) {
      return;
    }
    if(words != null && words.length > 0) {
      this.get('controllers.navigation/annotation').send('textHighlighting', true);
      this.set('textHighlightingWords', words);
    } else {
      this.get('controllers.navigation/annotation').send('textHighlighting', false);
    }
    this.set('gettingDataInProgress', false);
  },

  getWidthAtZoomZero: function(){
    var map = this.get('approval.map'),
      imgWidth = this.get('size.width');

    var width = Math.ceil(imgWidth * (1 / map.getView().getResolution()));

    return width;
  },

  changeZoomToFit: function(map) {
    var resolution = this.computeResolutionToFit(map);
    var oldResolution = map.getView().getResolution();
    map.getView().setResolution(resolution);
    var newResolution  = map.getView().getResolution();

    // sometimes zoom does not change because not all data are loaded. For this case try to change the zoom after 500 ms
    if (oldResolution == newResolution && newResolution != resolution) {
      var self = this;
      Ember.run.later(function () {
        self.changeZoomToFit(map);
      }, 500);
    }
  },

  computeResolutionToFit: function(map){
    var mapSize = map.getSize(),
      width = this.get('size.width'),
      height = this.get('size.height'),
      resolution = 1;
    if (!mapSize)
      return resolution;

    var bottomOffset = this.get('model.isCompareMode') && this.get('model.isOverlayMode') === false ? 0 : AppConstants.BottomBarHeight;
    var fitWidth = mapSize[0];
    var fitHeight = mapSize[1] - bottomOffset;

    var rotation = map.getView().getRotation();
    if(rotation % (Math.PI) === 0) {
      resolution = ((width / fitWidth) > (height / fitHeight)) ? (width / fitWidth) : (height / fitHeight);
    }
    else {
      resolution = ((height / fitWidth) > (width / fitHeight)) ? (height / fitWidth) : (width / fitHeight);
    }

    return resolution;
  },

  getPathForSelectElement: function(elem){
    var canvas = this.get('image-canvas-view');
    if(canvas){
      return canvas.getPathForSelectElement(elem);
    }
    return elem;
  },

  showCanvasForAnnotation: function(map, annotationId){
    var self = this;
    Ember.run.later(function() {
      self.send('showCanvas', annotationId);
    });
  },

  initCanvasPosition: function(size) {
    var canvas = this.get('image-canvas-view');
    if(canvas) {
      canvas.initCanvasPosition(size);
    }
  },

  syncMapsZoomObservable: function() {
    if (this.get('model.isCompareMode') === true) {
      this.syncTheOtherVersions();
    }
  }.observes('isSyncSelected'),

  syncTheOtherVersions: function() {
    var versions = this.get('compareVersions'),
      currentVersionMap = this.get('currentVersion.approvalMap');
    if (currentVersionMap) {
      versions.forEach(function (localVersion) {
        var approvalMap = localVersion.get('approvalMap');
        if(approvalMap && currentVersionMap != approvalMap) {
            approvalMap.getView().setResolution(currentVersionMap.getView().getResolution());
            approvalMap.getView().setRotation(currentVersionMap.getView().getRotation());
            approvalMap.getView().setCenter(currentVersionMap.getView().getCenter());
        }
      });
    }
  },

  syncOverlayMaps: function() {
    if (this.get('model.isOverlayMode') === true) {
      var versions = this.get('versionsForOverlay');
      if (versions) {
        var firstVersion = versions.get('lastObject');
        var secondVersion = versions[versions.indexOf(firstVersion) - 1];

        if (firstVersion && firstVersion.get('isDestroyed') === false && secondVersion && secondVersion.get('isDestroyed') === false) {
          var map1 = firstVersion.get('approvalMap'),
            map2 = secondVersion.get('approvalMap');

          if (map1 && map2) {
            var firstMapView = map1.getView(),
              secondMapView = map2.getView();

            if(map1 == this.get('approval.map')) {
              if(secondMapView && !secondMapView.get('isDestroyed') && secondMapView.getCenter() !== firstMapView.getCenter()) {
                secondMapView.setCenter(firstMapView.getCenter());
              }
              if(secondMapView && !secondMapView.get('isDestroyed') && secondMapView.getResolution() !== firstMapView.getResolution()) {
                secondMapView.setResolution(firstMapView.getResolution());
              }
              if(secondMapView && !secondMapView.get('isDestroyed') && secondMapView.getRotation() !== firstMapView.getRotation()) {
                secondMapView.setRotation(firstMapView.getRotation());
              }
            }
            else if (map2 == this.get('approval.map')) {
              if(firstMapView && !firstMapView.get('isDestroyed') && secondMapView.getCenter() !== firstMapView.getCenter()) {
                firstMapView.setCenter(secondMapView.getCenter());
              }
              if(firstMapView && !firstMapView.get('isDestroyed') && secondMapView.getResolution() !== firstMapView.getResolution()) {
                firstMapView.setResolution(secondMapView.getResolution());
              }
              if(firstMapView && !firstMapView.get('isDestroyed') && secondMapView.getRotation() !== firstMapView.getRotation()) {
                firstMapView.setRotation(secondMapView.getRotation());
              }
            }
          }
        }
      }
    }
  },

  syncZoomInCompareMaps: function() {
    if ((this.get('model.isCompareMode') === true && this.get('isSyncZoomSelected') == true) && (this.get('model.isOverlayMode') === false) ) {
      this.syncTheOtherVersions();
    }
  },

  initializeTextHighlightingObservable: function(){
    var textHighlighting = this.get('textHighlighting'),
      view = this.get('view'),
      map = this.get('approval.map'),
      pageId = this.get('model.currentPage.id');
    if (this.get('model.currentPage.PageFolder') && textHighlighting && view && map && textHighlighting.get('isDestroyed') === false) {
      textHighlighting.send('initializeTextHighlighting', this.get('size'), map, this, view.$(), pageId);
    }
  }.observes('approval.map'),

  changeSelectedVersionObservable: function(){
    var select = this.get('model.select');
    if (select === false) {
      var pins = this.get('image-annotation-layer');
      if (pins){
        this.send('doneAnnotating');
      }
      var canvasView = this.get('image-canvas-view');
      if(canvasView){
        canvasView.closeAddLine();
        canvasView.closeColorPicker();
      }
    }
    if (Ember.$(".czn-separations").is(":visible")){
      this.get('controllers.image/separations/index').set('activeColor', this.get('lastSeparationColorName'));
    }
  }.observes('model.select'),

  createPinsLayer: function(){
    var map = this.get('approval.map');
    var pageId = this.get('model.currentPage.id');
    var canvas = this.get('image-canvas-view');
    var self = this;
    var pins = this.get('image-annotation-layer');
    if (pins && !pins.get('isDestroyed')){
      pins.set('pageId', pageId);
      pins.showLayer();
    }

    if(map) {
      map.fullSize = this.get('size');
    }
  },

  showPinsOptionChanges: function(){
    if(this.get('pinsAreShown')) {
      var view = this.get('view');
      if(view) {
        view.createMarkers();
      }
    }
    else {
      this.send('clearPinsLayers', this.get('approval.map'), this.get('approval.select'));
    }
  }.observes('pinsAreShown'),

  getMappedVersions: function(){
    var versionsToMap = [];
    var currVersion = this.get('currentVersion');

    var selectedVersions = this.get('versions');
    selectedVersions.forEach(function(version){
      if(version.get('id') !== currVersion.get('id')){
        versionsToMap.push(version.get('approvalMap'));
      }
    });
    return versionsToMap;
  },

  getPreviousSyncVersions: function(){
    var versionsToMap = [];
    var prevVersion =  this.get('previousVersion');

    var selectedVersions = this.get('versions');
    selectedVersions.forEach(function(version){
      if(version.get('id') !== prevVersion.get('id')){
        versionsToMap.push(version.get('approvalMap'));
      }
    });
    return versionsToMap;
  },
  initializeOpenlayers: function() {
    if (this.get('url') && this.get('currentPage.id') && this.get('model.tilesUrl')) {
      this.refreshOpenLayers();
    }
  }.observes('url', 'currentPage', 'model.tilesUrl'),

  updateZoomLevel: function(){
    var zoomLevels = this.get('session.zoomLevels') || [],
      pageId = this.get('model.currentPage.id'),
      map = this.get('approval.map'),
      currentZoomLevel = zoomLevels.filter(function(zl){
        return zl.get('pageId') === pageId;
      }).get('firstObject'),
      isFitToPage = this.get('isFitToPage');

    if (map) {
      if (!currentZoomLevel) {
        currentZoomLevel = Ember.Object.create({
          pageId: 0,
          center: [0,0],
          zoom: 0,
          angle: 0
        });
      }
      var zoom = map.getView().getZoom();
      if(typeof zoom === 'undefined')
      {
        zoom = this.get('currentZoom');
      }

      currentZoomLevel.set('pageId', pageId);
      currentZoomLevel.set('center', map.getView().getCenter());
      currentZoomLevel.set('zoom', zoom);
      currentZoomLevel.set('angle', map.getView().getRotation());
      currentZoomLevel.set('isFitToPage', isFitToPage);

      zoomLevels.pushObject(currentZoomLevel);
      this.get('session').set('zoomLevels', zoomLevels);
    }
  },

  refreshOpenLayers: function(url, size){
    this.createPinsLayer();

    var self= this;
    if (url) {
      Ember.run.later(function () {
        var annotation = self.get('selectAnnotationAsync');
        if (annotation) {
          self.send('selectAnnotation', annotation);
        }
      }, 500);
    }
  },

  getOverlayByProperty: function(property){
    var overlay,
      map = this.get('approval.map');
    if(map) {
      for (var i = 0; i < map.getOverlays().getLength(); i++) {
        overlay = map.getOverlays().item(i);
        if (overlay.get(property)) {
          return overlay;
        }
      }
    }
    return null;
  },

  isTimeToChangeMinimap: function() {
    if (this.get('isOverlaySelected') == true) {
      var isFirstVersionOnTop = this.get('controllers.approval.isFirstOverlayVersionOnTop');
      return this.get('model.select') == isFirstVersionOnTop;
    }
    return this.get('model.select');
  },

  changedSelectedViewer: function(){
    if (this.isTimeToChangeMinimap()) {
      var currentVersionMap = this.get('approval.map');
      if (currentVersionMap) {
        //show overviewmap for current selected version and hide others
        var selectedId = this.get('id') + this.get('isClone');
        Ember.run.later(function() {
          var currentOverviewMap = 'ol-map-' + selectedId;

          Ember.$('.' + currentOverviewMap).show();
          Ember.$('.ol-overviewmap:not(.' + currentOverviewMap + ')').hide();
        }, 500);
      }
      if (this.get('controllers.image/separations/index.showingSeparations')){
        this.get('controllers.image/separations/index').send('selectSeparation', this.get('lastSeparationColorName') || null);
      }
    }
  }.observes('model.select', 'controllers.approval.viewerControllers.@each.select', 'model.currentPage'),

  minZoom: function(){
    return 0;
  }.property('model'),

  maxZoom: function(){
    var view = this.get('view');
    return view ? view.getMaxZoom() : 0;
  }.property('approval', 'model.currentPage', 'model.versionIsChanged'),

  initialZoom: function(){
    var map = this.get('approval.map'),
      view = map ? map.getView() : null;
    if(view) {
      return view.getZoom();
    }
    return 0;
  }.property('approval'),

  markersObservable: function() {
    var self = this;

    var markers = Ember.A();

    this.get('model.annotations').forEach(function(annotation){
      var posX = annotation.get('CrosshairXCoordScaledForHighRes') || 0,
        posY    = annotation.get('CrosshairYCoordScaledForHighRes') || 0,
        pos     = [posX, -posY],
        annotationId = annotation.get('id'),
        element = annotation.get('AnnotationNumber').toString(); //this.$().get(0);

      if (element && annotationId) {
        var marker = {pos: pos, element: element, annotationId: annotationId};
        markers.pushObject(marker);
      }
    });

    self.set('markers', markers);
  }.observes('model.annotations','model.annotations.[]'),

  syncData: function(map){
    var view = this.get('view');
    if(view){
      //var zoom = this.get('currentZoom');
      var maxZoom = this.get('maxZoom');
      var center = this.get('mapCenter');

      if(view.get('_state') !== 'inDOM'){
        return false;
      }
      var viewSize = {
        width: view.$().width(),
        height: view.$().height()
      };

      var imageSize = this.get('size');
      //var scale = 1/ Math.pow(2, (maxZoom - zoom));
      var scale = this.get('mapScale');
      var size = { width: imageSize.width * scale , height: imageSize.height * scale };

      return {
        size: size,
        center: center,
        view: viewSize,
        startpoint: map.getPixelFromCoordinate([0,0])
      };
    }

    return {
      size: 1,
      center: {},
      view: {},
      startpoint: map.getPixelFromCoordinate([0,0])
    }
  },

  //Resize map
  invalidateMapSize: function() {
        var map = this.get('approval.map'),
          self = this;
    if(map) {
      Ember.run.later(function() {
        map.updateSize();
      });
    }
    var self = this;
    Ember.run.later(function() {
      var balloonsView = self.get('balloons-view');
      if (balloonsView) {
        balloonsView.syncPosition();
        balloonsView.arrangeBalloons();
      }
    });
  }.observes('controllers.application.sidebarShown'),

  //Resize maps
  invalidateAllMaps: function(){
    var self = this;
    (this.get('versions') || Ember.A([])).forEach(function(version) {
      var map = version.get('approvalMap');
      if (map) {
        Ember.run.later(function () {
          map.updateSize();
          self.send('fit');
        }, 50);
      }
    });
  }.observes('controllers.application.sidebarShown', 'controllers.image/controls/image.isVertical', 'controllers.image/controls/image.isHorizontal'),

  getSelectedAnnotation: function(annotationNumber){
    var annotations =  this.get('annotations');
    for(var i=0; i< annotations.length; i++){
      if(annotations[i].get('AnnotationNumber') == annotationNumber){
        return annotations[i];
      }
    }
    return null;
  },

  getViewportCoordFromImageCoord: function(relativeCoord) {
    var mapPosition = this.get('mapPosition'),
        mapOffset = this.get('mapOffset');

      return [relativeCoord[0] + mapPosition.left + mapOffset.left, relativeCoord[1] + mapPosition.top + mapOffset.top ];
  },

  centerMapToAnnotation: function(annotationId){
    if(!this.get('isFitToPage')) {
      if(this.get('approval.map') != null){
      var map = this.get('approval.map'),
        mapSize = map.getSize();

      var bottomOffset = this.get('model.isCompareMode') && this.get('model.isOverlayMode') === false ? 0 : AppConstants.BottomBarHeight;
      var fitWidth = mapSize[0];
      var fitHeight = mapSize[1] - bottomOffset;
      var currentResolution = map.getView().getResolution();
      var fitResolution = this.computeResolutionToFit(map, fitWidth, fitHeight);

      // Set annotation as center only if ZOOM IN is used
      if(currentResolution < fitResolution) {
        var markers = this.get("markers");
        var annotationMarker = markers.findBy('annotationId', annotationId);
        var point = annotationMarker.pos;
        map.getView().setCenter(point);
      }
    }
  }
  },

  getZoomChangerResolutionForZoomIn: function(resolution) {
    if (resolution <= 2 ) {
      return AppConstants.ZoomStep;
    }
    var roundedRezolution = Math.pow(2, Math.floor(Math.log(resolution - 0.01) / Math.log(2))); // As Math.Log2 is not found in IE11
    return AppConstants.ZoomStep * roundedRezolution;
  },
  getZoomChangerResolutionForZoomOut: function(resolution) {
    if (resolution < 2 ) {
      return AppConstants.ZoomStep;
    }
    var roundedRezolution = Math.pow(2, Math.floor(Math.log(resolution) / Math.log(2))); // As Math.Log2 is not found in IE11
    return AppConstants.ZoomStep * roundedRezolution;
  },

  zoomInPercentages: function() {
    var res = this.get('currentResolution');
    if (typeof res !== 'number')
      return 0;

    return (100 * (1 / res)).toFixed(2);;
  }.property('currentResolution'),

  isTileSize: function() {
    var res = this.get('currentResolution');
    if (typeof res !== 'number')
      return false;

    if (res % 1 !== 0)
      return false;

    return res && (res & (res - 1)) === 0;
  }.property('currentResolution')


});
export default OpenLayersViewer;
