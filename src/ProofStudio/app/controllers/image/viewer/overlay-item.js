import Ember from "ember";

var OverlayItemVersionsIndexController = Ember.ObjectController.extend({
    needs: ['approval', 'image/viewer/overlay'],
    viewers: Ember.computed.alias('controllers.approval.viewerControllers'),
    overlayVersions: Ember.computed.alias('controllers.image/viewer/overlay.model'),

    pages: function(){
        var pages = [], self = this;
        this.get('viewers').forEach(function(viewer){
            if (!viewer.get('isDestroyed') && viewer.get('model.id') === self.get('model.id')){
                pages = viewer.get('renderer.pages');
            }
        });
        return pages;
    }.property('model', 'controllers.approval.pagesChanged'),

    actions:{
        gotoPage: function(pageId){
            var self = this;
            this.get('viewers').forEach(function(viewer){
                if (!viewer.get('isDestroyed') && viewer.get('model.id') === self.get('model.id')){
                    viewer.send('gotoPage', pageId);
                }
            });
        }
    }
});
export default OverlayItemVersionsIndexController;
