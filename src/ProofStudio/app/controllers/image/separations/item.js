import Ember from "ember";

var SeparationsItemController = Ember.ObjectController.extend({

    cubeClassName: function(){
        if(this.get('RGBHex') == null) {
          return "czn-color-" + this.get('Name').toLowerCase();
        }
    }.property('Name'),

    style: function() {
      if(this.get('RGBHex') != null) {
        return 'background-color:%@%@'.fmt('#', this.get('RGBHex'));
      }
    }.property('RGBHex'),

    separationName: function(){
        return this.get('Name').toUpperCase();
    }.property('Name'),

    actions:{
        selectChannel: function(){
            this.set('selected', !this.get('selected'));
            this.set('changed', true);
        }
    }
});
export default SeparationsItemController;
