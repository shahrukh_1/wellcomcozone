import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var SeparationsIndexController = Ember.ArrayController.extend(CurrentViewerMixin, {
	  needs: ['navigation/main', 'image/controls/image', 'navigation/custom-profiles/index'],
    showingSeparations: false,
	  isEnableSoftProofing: Ember.computed.alias('controllers.navigation/main.isEnableSoftProofing'),
    currentSPP : Ember.computed.alias('controllers.navigation/custom-profiles/index.currentSPP'),

    separations: function(){
      if(!this.get('currentViewerController.model.IsRGBColorSpace')) {
        var currentVersion = this.get('controllers.approval.currentVersion'),
          items = currentVersion.get('currentPage.ApprovalSeparationPlates'),

          currentSoftProofingParam = this.get('currentSPP'),
          allChannelsAreSelected = currentSoftProofingParam && (currentSoftProofingParam.ActiveChannels === null || currentSoftProofingParam.ActiveChannels === ''),
          selectedChannels = currentSoftProofingParam && currentSoftProofingParam.ActiveChannels ? currentSoftProofingParam.ActiveChannels.split(',') : '';

        if (items) {
          items.forEach(function (item) {
            if (allChannelsAreSelected) {
              item.set('selected', true);
            }
            else
            if (selectedChannels) {
              var found = (Ember.$.inArray(item.get('ChannelIndex').toString(), selectedChannels) > -1);
              item.set('selected', found);
            }
            item.set('changed', false);
          });
        }

        if (items) {
          items = items.sortBy('ChannelIndex');
        }

        return items || [];
      }
      else {
        this.send('hideSeparations');
      }
    }.property('showingSeparations', 'session.softProofingParams.@each.ActiveChannels','currentSPP'),

    isEnableButton: function(){
        var enabled = false;

        this.get('separations').forEach(function(item){
            if (item.get('selected')){
                enabled = true;
            }
        });

        return enabled;
    }.property('separations.@each.selected'),

    actions:{
        showSeparations: function(){
            this.set('model', this.get('separations'));
            return true;
        },
        selectSeparation: function(){
            var selectedSeparations = [], allChannels = true;
            this.get('separations').forEach(function(sp){
                if (sp.get('selected')){
                    selectedSeparations.pushObject(sp.get('ChannelIndex'));
                }
                else{
                    allChannels = false;
                }
            });
            this.send('selectSeparations', allChannels ? '' : selectedSeparations.join(','));
        }
    }
});
export default SeparationsIndexController;
