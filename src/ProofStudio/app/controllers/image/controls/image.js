import Ember from "ember";
import UserPermissions from 'appkit/routes/mixins/user-permissions';
import AppConstants from 'appkit/config/constants';

var ImageControlsController = Ember.Controller.extend(UserPermissions, {
    needs: ['image/separations/index', 'image/viewer/minimap', 'image/viewer/compare', 'approval', 'sidebar/index', 'sidebar/annotation', 'comments/index', 'navigation/annotation', 'navigation/main', 'navigation/version'],
    showingSeparations: Ember.computed.alias('controllers.image/separations/index.showingSeparations'),
    separations: Ember.computed.alias('model.currentPage.ApprovalSeparationPlates'),
    separationsController: Ember.computed.alias('controllers.image/separations/index'),
    miniMapController: Ember.computed.alias('controllers.image/viewer/minimap'),
    selectedVersions: Ember.computed.alias('controllers.approval.selectedVersions'),
    approval: Ember.computed.alias('controllers.approval'),
    renderer: null,
    hideMiniMap: false,
    isCompareSelected: false,
    isDifferenceSelected: false,
    isFeaturePixelCompareSelected: false,
    active: false,
    isCompareMode: false,
    isHorizontal: false,
    isVertical: true,
    isSyncSelected: false,
    isPageSync: true,
    isZoomSync: true,
    backgroundColorPageSync: '',
    backgroundColorFeaturePixelCompare: '',
    showAllTickets: false,
    miniMapIsUp: Ember.computed.alias('controllers.image/viewer/minimap.miniMapIsUp'),
    showAnnotationButtons: Ember.computed.alias('controllers.navigation/annotation.showAnnotationButtons'),
    hasPrePressTools: Ember.computed.alias('controllers.navigation/main.hasPrePressTools'),
    isSwipe: false,
    percentagesSwipe: 0,
    isFirstOverlayVersionOnTop: true,
    showPagination: Ember.computed.alias('controllers.approval.paginationVisible'),

    onOverlaySwipe: function() {
      if (this.get('isDifferenceSelected')) {
        var self = this;
        Ember.$('#main').on('mousemove.swipeInOverlay', function(ev) {
          if (self.get('isSwipe')) {
            if (ev.clientY < Ember.$('#main').height()) {
              self.set('percentagesSwipe', ev.clientX * 100 / Ember.$('#main').width());
            }
          }
        });
        this.set('isSwipe',false);
        self.set('percentagesSwipe',0);
      }
      else {
        Ember.$('#main').off('mousemove.swipeInOverlay');
      }

    }.observes('isDifferenceSelected'),

    onDifferenceOrSwipe: function() {
      if (this.get('isDifferenceSelected') && this.get('isSwipe')) {
        this.addSwipeCursor();
      }
      else {
        Ember.$('#main').removeClass('czn-col-resize');
        Ember.$('.czn-canvas-viewer').removeClass('czn-col-resize');
      }
    }.observes('isDifferenceSelected','isSwipe'),

    addSwipeCursor: function() {
      Ember.$('#main').addClass('czn-col-resize');
      Ember.$('.czn-canvas-viewer').addClass('czn-col-resize');
    },

    actions:{
        changeDiffColor: function(){
          this.get('modalDialogsCommunicationService').TriggerCompareCanvases();
        },

        showOrHideMiniMap: function(){
            this.get('miniMapController').send('showMiniMap', this.get('hideMiniMap'));
            this.set('hideMiniMap', !this.get('hideMiniMap'));
        },
        showSeparations: function(){
            var showingSeparations = this.get('showingSeparations');
            if (showingSeparations === true){
                this.send('hideSeparations');
            }

            this.set('showingSeparations', !showingSeparations);

            if (this.get('showingSeparations') === true){
                return true;
            }
        },
        hideSeparations: function(){
            this.set('showingSeparations', false);
            return true;
        },
        toggleCompare: function(){
            if (this.get('isCompareSelected') !== true) {
                this.set('isFeaturePixelCompareSelected', false);
                this.set('backgroundColorFeaturePixelCompare', 'background-image: linear-gradient(to bottom, #ffffff 0%, #f2f2f2 100%); color: #000;');
                this.set('active', false);
                this.set('isCompareSelected', true);
                this.set('isDifferenceSelected', false);
	              this.get('controllers.image/viewer/minimap').send('showMiniMap', true);
                this.set('hideMiniMap', false);

                var versions = this.get('selectedVersions');
                versions.forEach(function (version) {
                    version.set('isCompareMode', true);
                });
                this.get('approval').send('displayMultipleVersions', versions);
                this.send('resizeVersionsViews');

                var self = this;
                Ember.run.later(function(){
                    self.set('isPageSync', self.get('session.sync-pages-and-zoom.pageSync') == true);
                    self.set('isZoomSync', self.get('session.sync-pages-and-zoom.zoomSync') == true);
                });
            }
        },
      toggleFeaturePixelCompare: function(){

        if (this.get('isFeaturePixelCompareSelected') !== true) {
          this.set('isFeaturePixelCompareSelected', true);
          this.set('backgroundColorFeaturePixelCompare', 'background-color: #9E9E9E; color: #fff;');
          document.getElementById('map3').style.display = 'block';
          document.getElementById('mybg').style.display = 'block';
        }
        else {
          this.set('isFeaturePixelCompareSelected', false);
          this.set('backgroundColorFeaturePixelCompare', 'background-image: linear-gradient(to bottom, #ffffff 0%, #f2f2f2 100%); color: #000;');
          document.getElementById('map3').style.display = 'none';
          document.getElementById('mybg').style.display = 'none';
        }
      },
        toggleDifference: function(){
            if (this.get('isDifferenceSelected') !== true) {
                this.get('session').set('sync-pages-and-zoom', {zoomSync: this.get('isZoomSync'), pageSync: this.get('isPageSync')});

                this.set('isFeaturePixelCompareSelected', false);
                this.set('backgroundColorFeaturePixelCompare', 'background-image: linear-gradient(to bottom, #ffffff 0%, #f2f2f2 100%); color: #000;');
                this.set('active', true);
                this.set('isCompareSelected', false);
                this.set('isDifferenceSelected', true);
                this.set('isPageSync', false);
                this.set('isZoomSync', false);
	              this.get('controllers.image/viewer/minimap').send('showMiniMap', true);
                this.set('hideMiniMap', false);

                var currentPages = [];
                this.get('selectedVersions').forEach(function (version) {
                    currentPages.pushObject({version: version.get('id'), page: version.get('currentPage.id')});
                });
                window.sessionStorage.setItem('overlay-pages', JSON.stringify(currentPages));

                this.changeToOverlay();
            }
        },
        toggleSyncPageZoom: function(){
            this.toggleProperty('isZoomSync');
            this.toggleProperty('isSyncSelected');
        },
        toggleSync: function(){
            this.toggleProperty('isSyncSelected');
        },
        togglePageSync: function(){
            this.toggleProperty('isPageSync');
        },
        displayCompareControls: function(isVisible){
            this.set('isCompareMode', isVisible);
        },
        setHorizontal: function(){
            var self = this;
            this.set('isVertical', false);
            this.set('isHorizontal', true);

            var versions = self.get('selectedVersions');
            var versionCount = versions.length;

            var mainWidth = Ember.$('.czn-main').width();
            var mainHeight = Ember.$('.czn-main').height();
            var bottomBarHeight = Ember.$('.czn-controls-toolbar').height();

            var ulHeight = mainHeight - bottomBarHeight - 8;
            var ulWidth = mainWidth -  Ember.$('.czn-sidebar-handle').width();
            var liHeight = parseInt(ulHeight / versionCount);

            var leafletContainer = Ember.$('.leaflet-container');
            leafletContainer.css('width', ulWidth - 6);
            leafletContainer.css('height', liHeight - 4);

            Ember.run.later(function(){
                Ember.$('.czn-versions-ul').css('height', ulHeight);
                Ember.$('.czn-versions-ul').css('width', ulWidth);

                Ember.$('li.czn-versions-li').each(function(i,v){
                    Ember.$(v).css('height', liHeight);
                    Ember.$(v).css('width', ulWidth - 5);
                });
                var top = Ember.$('.czn-versions-ul')[0].offsetHeight / 2;
                document.getElementById('map3').style.top = top + 2 +'px';
                document.getElementById('map3').style.left = '0px';
            });
        },
        setVertical: function(){
            var self = this;
            this.set('isVertical', true);
            this.set('isHorizontal', false);

            var versions = self.get('selectedVersions');
            var versionCount = versions.length;

            var mainWidth = Ember.$('.czn-main').width();
            var mainHeight = Ember.$('.czn-main').height();
            var bottomBarHeight = Ember.$('.czn-controls-toolbar').height();

            document.getElementById('map3').style.top = '2px';
            var ulHeight = mainHeight - bottomBarHeight - 8;
            var ulWidth = mainWidth -  Ember.$('.czn-sidebar-handle').width();
            var liWidth = parseInt(ulWidth / versionCount);

            var leafletContainer = Ember.$('.leaflet-container');
            leafletContainer.css('width', liWidth - 6);
            leafletContainer.css('height', ulHeight - 4);

            Ember.run.later(function(){
                Ember.$('.czn-versions-ul').css('height', ulHeight);
                Ember.$('.czn-versions-ul').css('width', ulWidth);

                Ember.$('li.czn-versions-li').each(function(i,v){
                    Ember.$(v).css('width', liWidth - 2);
                    Ember.$(v).css('height', ulHeight);
                });
            });
        },
        viewLatestVersion: function(){
          this.set('active', false);
          this.set('isPageSync', false);
          this.set('isLocked', false);

          var maxVersionId = 0;
          this.get('approval.Versions').forEach(function(version) {
            if (version.get('id') > maxVersionId) {
              maxVersionId = version.get('id');
            }
          });

          var version = this.get('approval.Versions').filterBy('id', maxVersionId).get('firstObject');
          var self = this;
          this.get('approval.Versions').forEach(function(version1) {
            if (version1.get('id') != version.get('id')) {
              self.get('approval.Versions').removeObject(version1);
            }
          });

          this.get('approval.selectedVersions').forEach(function(versionItem){
              versionItem.set('select', false);
          });
          this.get('approval.selectedVersions').forEach(function(version1) {
            if (version1.get('id') != version.get('id')) {
              self.get('approval.selectedVersions').removeObject(version1);
            }
          });
          this.get('session').set('selectedApprovals',version.get('id'));
          Ember.run.later(function(){
            self.get('controllers.navigation/version').send('selectVersion', version);
            self.get('controllers.navigation/main').send('viewVersion');
            window.sessionStorage.removeItem('overlay-pages');
          });
        },
        overlayFlip: function(){
          var alphaView = this.get('alphaView');
          var value;
          if (alphaView) {
            value = alphaView.get('value');
          }

          this.toggleProperty('isFirstOverlayVersionOnTop');

          this.changeToOverlay();
          var self = this;

          Ember.run.later(function() {
            if (value < 100) {
              var alphaView = self.get('alphaView');
              if (alphaView) {
                alphaView.change(value);
                alphaView.setHandle(value);
              }
            }
          },500);
        },
        swipeOnOff: function() {
          this.toggleProperty('isSwipe');
          if (this.get('isSwipe') == false) {
            this.set('percentagesSwipe',0);
          }
        },
        setAlphaViewData: function(alphaView){
            this.set('alphaView', alphaView);
        },
        showHideAllTickets: function(){
            this.set('showAllTickets', !this.get('showAllTickets'));
            this.send('toggleAllTickets', this.get('showAllTickets'));
            this.set('miniMapIsUp', false);
        },

        copyAnnotation: function(){
          var self = this,
            destinationPage = this.get('selectedVersions').filterBy('selected', false).get('firstObject.currentPage'),
            sourcePage = this.get('selectedVersions').filterBy('select', true).get('firstObject.currentPage'),
            annotation = this.store.all('annotation').filterBy('selected', true).get('firstObject');

          var destinationPageHeight = destinationPage.get('OriginalImageHeight');
          var destinationPageWidth = destinationPage.get('OriginalImageWidth');
          var destinationOutputPageHeight = destinationPage.get('OutputImageHeight');
          var destinationOutputPageWidth = destinationPage.get('OutputImageWidth');

          var sourcePageHeight = sourcePage.get('OriginalImageHeight');
          var sourcePageWidth =sourcePage.get('OriginalImageWidth');
          var sourceOutputPageHeight = sourcePage.get('OutputImageHeight');
          var sourceOutputPageWidth =sourcePage.get('OutputImageWidth');

          var crosshairXCoord = annotation.get('CrosshairXCoordScaledForHighRes');
          var crosshairYCoord = annotation.get('CrosshairYCoordScaledForHighRes');

          // Check if the copied annotation position fits in the new version document dimensions
          if((destinationPageHeight < sourcePageHeight || destinationPageWidth < sourcePageWidth) ||
              // Take into consideration DPI factor
            (destinationOutputPageHeight < sourceOutputPageHeight || destinationOutputPageWidth < sourceOutputPageWidth)) {
            if((crosshairXCoord > destinationPageWidth || crosshairYCoord > destinationPageHeight) ||
                // Take into consideration DPI factor
              (crosshairXCoord > destinationOutputPageWidth || crosshairYCoord > destinationOutputPageHeight)){
                var controller = this.controllerFor('confirm-dialog');
                controller.set('title', Ember.I18n.t("lblAnnotationCopyTitle"));
                controller.set('headingMessage', Ember.I18n.t("lblAnnotationCopyHeader"));
                controller.set('message', Ember.I18n.t("lblAnnotationCopyMessage"));
                controller.set('yes', Ember.I18n.t("btnContinue"));
                controller.set('no', Ember.I18n.t("btnCancel"));
                controller.set('viewedByAllMessage', "");
                controller.set('noteMessage', "");
                controller.set('mode', AppConstants.ConfirmDialogMode.AnnotationDoesNotFit);
                controller.set('visible', true);
                controller.init();

                this.send('showConfirmDialog');
            }
            else {
              this.processAnnotationCopy();
            }
          }
          else {
            this.processAnnotationCopy();
          }
        }
    },

    processAnnotationCopy: function(){
    var self = this,
      destinationPage = this.get('selectedVersions').filterBy('selected', false).get('lastObject.currentPage'),
      annotation = this.store.all('annotation').filterBy('selected', true).get('firstObject');

    try {
      if (annotation && destinationPage) {
        var annotationId = annotation.get('id');
        annotation.set('selected', false);

        Ember.$("#czn-loading-screen").modal();
        self.store.adapterFor('annotation').copyAnnotation({
          id: annotationId,
          DestinationPageId: destinationPage.get('id')
        }).then(function (savedAnnotations) {
          if ((savedAnnotations.ErrorMessage === null || savedAnnotations.ErrorMessage === "") && savedAnnotations.annotation && savedAnnotations.annotation.length > 0) {
            var copiedAnnotation = savedAnnotations.annotation.get('firstObject');

            savedAnnotations.annotation.forEach(function(item, index, annotations) {
              this.store.pushPayload('annotation', item)
            }, self);

            Ember.$("#czn-loading-screen").modal('hide');
            Ember.$(".box-text-highlighting.selected").removeClass('selected');

            var destinationPageOfAnnotation = self.get('store').all('page').filterBy('id', copiedAnnotation.annotation.Page.id).get('firstObject'),
              destinationVersion = destinationPageOfAnnotation ? destinationPageOfAnnotation.get('Approval'): null;

            if (destinationVersion) {
              self.send('changeSelectedVersion', destinationVersion)
            }
            Ember.run.later(function(){
              self.get('controllers.comments/index').set('annotationsChanged', new Date());
            });
          }
          else if (savedAnnotations.ErrorMessage != null && savedAnnotations.ErrorMessage.length > 0) {
            Ember.$("#czn-loading-screen").modal('hide');
            self.send('message', savedAnnotations.ErrorMessage, Ember.I18n.t("lblCopyAnnotationFailed"));
          }
        });
      }
    }
    catch(e){
      Ember.$("#czn-loading-screen").modal('hide');
      throw e;
    }
  },

    //this property changes the background of the SyncZoom button
    isSyncZoomSelected: function(){
        var isZoomSync = this.get('isZoomSync');

        if(isZoomSync) {
            this.set('backgroundColor', 'background-color: #9E9E9E; color: #fff;');
        }
        else {
            this.set('backgroundColor', 'background-image: linear-gradient(to bottom, #ffffff 0%, #f2f2f2 100%); color: #000;');
        }

        return isZoomSync;
    }.property('isZoomSync'),

    hasNoTickets: function(){
        var self = this;
        return this.get('controllers.sidebar/index.annotations').filter(function(annotation){
            return annotation.get('Page.id') == self.get('renderer.model.currentPage.id');
        }).get('length') < 1;
    }.property('controllers.sidebar/index.annotations', 'renderer.model.currentPage.id'),


  //this property changes the background of the SyncPages button
    isPageSyncSelected: function(){
        var isPageSync = this.get('isPageSync');

        if(isPageSync) {
            this.set('backgroundColorPageSync', 'background-color: #9E9E9E; color: #fff;');
        }
        else {
            this.set('backgroundColorPageSync', 'background-image: linear-gradient(to bottom, #ffffff 0%, #f2f2f2 100%); color: #000;');
        }

        return isPageSync;
    }.property('isPageSync'),

    hasSeparations: function(){
        return this.get('separations.length') > 0;
    }.property('separations'),

    overlayDisabled: function(){
        return (this.get('selectedVersions').length !== 2);
    }.property('selectedVersions'),

    isCopyAnnotationsHidden: function(){
        var hidden = true,
            currentVersionId = this.get('currentViewerController.model.id'),
            selectedVersions = this.get('selectedVersions') || [],
            destinationVersion = selectedVersions.filterBy('selected', false).get('firstObject'),
            self = this;

        if (selectedVersions.get('length') > 2){
            return true;
        }

        if (!this.get('currentViewerController.currentPage')){
            return true;
        }

        if (!this.get('userCanAnnotate')){
            return true;
        }

        if (!destinationVersion || (destinationVersion && !this.userIsCollaborator(null, destinationVersion.get('id')))){
            return true;
        }

        var annotations = this.get('currentViewerController.currentPage.Annotations').filter(function(annotation){
            return annotation.get('selected') === true;
        });
        if (!annotations || (annotations && annotations.length === 0)){
            hidden = true;
        }
        else {
            // check if the selected version has the minimum version number
            var selectedVersionNumber = this.get('controllers.approval.currentVersion.VersionNumber');
            this.get('selectedVersions').forEach(function (version) {
                if (version.get('VersionNumber') > selectedVersionNumber) {
                    hidden = false;
                }
            });
        }
        return hidden;
    }.property('currentViewerController.currentPage.Annotations.@each.selected', 'currentViewerController.currentPage.Annotations.@each.Status', 'controllers.approval.currentVersion.VersionNumber', 'controllers.approval.viewerControllers.@each.select'),

    overlayLabels: function() {
      var overlayVersions = this.get('approval.selectedVersions'),
        versionsLabels = {
          firstPageVersionLabel:"",
          secondPageVersionLabel:""
        };
      if (overlayVersions && overlayVersions.length == 2) {
        var emberVers = Ember.A(overlayVersions);
        var firstVersion = emberVers.filterBy('isFirstVersion',true).get('firstObject');
        versionsLabels.firstPageVersionLabel = firstVersion.get('PageVersionLabel');

        var secondVersion = emberVers.filterBy('isFirstVersion',false).get('firstObject');
        versionsLabels.secondPageVersionLabel = secondVersion.get('PageVersionLabel');
      }
      return versionsLabels;
    }.property('approval.versionsForOverlay'),

    changeToOverlay:function() {
      this.get('approval').send('changeToOverlayMode');
      this.send('doneAnnotating');
    }
});
export default ImageControlsController;
