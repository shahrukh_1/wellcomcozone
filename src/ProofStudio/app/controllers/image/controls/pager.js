import Ember from "ember";

var PagerController = Ember.Controller.extend({

    prevDisabled: function() {
        return this.get('currentPageNumber') === 1;
    }.property('model.currentPage', 'totalPages'),

    nextDisabled: function() {
        return this.get('currentPageNumber') === this.get('totalPages');
    }.property('model.currentPage', 'totalPages'),

    currentPageNumber: function() {
        var pageNr = this.get('model.currentPage.PageNumber');
        return pageNr;
    }.property('model.currentPage', 'model.totalPages', 'model'),

    totalPages: function() {
        var versionId = this.get('model.currentPage.Approval.id') || 0;
        return this.store.all('page').filter(function(page){
            return page.get('Approval.id') === versionId;
        }).get('length');
    }.property('model.currentPage', 'model.totalPages', 'model', 'controllers.approval.pagesChanged'),

    totalPagesUnProcessed: function(){
	    var totalUnProcessedPages = this.get('model.PagesCount');
	    if (totalUnProcessedPages > this.get('totalPages')){
		    return totalUnProcessedPages;
	    }
	    return null;
    }.property('totalPages', 'model.PagesCount')
});
export default PagerController;
