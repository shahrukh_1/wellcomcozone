import Ember from "ember";

var PageViewerController = Ember.Controller.extend({
    unProcessedPages: function(){
        var pageDiff = this.get('model.model.PagesCount') - this.get('model.pages.length'),
            pages = [],
            maxPageNr = 0;
        if (pageDiff > 0){

            this.get('model.pages').forEach(function(page){
                if (maxPageNr < page.get('PageNumber')){
                    maxPageNr = page.get('PageNumber');
                }
            });

            for(var i = 0; i< pageDiff; i++){
                pages.pushObject({PageNumber: ++maxPageNr});
            }
        }
        return pages;
    }.property('model.pages.length')
});
export default PageViewerController;
