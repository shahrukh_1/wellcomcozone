import Ember from "ember";

var MultipleUploadController = Ember.ObjectController.extend({
    needs: ['comments/index'],
    errorFileSizeTooLong: false,
    errorFilesCountTooLong: false,
    newFiles: [],

    files: function(){
        var annotations = this.get('model.firstObject.AnnotationAttachments');
        return annotations;
    }.property('model.firstObject.AnnotationAttachments'),

    isAttachmentsCopied: function(){
        var annotations = this.get('controllers.comments/index.attachmentsToPaste');
        if(annotations.length == 0){
            return false;
        }
        else{
             this.set('newFiles', annotations);
             return true;
        }
       }.property('controllers.comments/index.attachmentsToPaste'),
       
    fileUploadChanged: function(){
        var files = this.get('model.firstObject.AnnotationAttachments') || this.get('newFiles');
        var commentsController = this.get('controllers.comments/index');
        if (files && commentsController) {
            commentsController.set('attachments', files);
        }
    }.observes('model.firstObject.AnnotationAttachments', 'model.firstObject.AnnotationAttachments.@each', 'newFiles', 'newFiles.@each'),

    annotationIdObservable: function(){
        var instance = this.get('component');
        if (instance){
            instance.set('annotationId', this.get('annotationId'));
        }
    }.observes('model.firstObject'),

    actions: {
        fileUploadInit: function(instance){
            this.set('component', instance);
            instance.set('annotationId', this.get('annotationId'));
            instance.set('multipleUpload', this);
        },
        selectFiles: function(){
            var component = this.get('component');
            if (component){
                component.get('uploader').click();
            }
        },
        removeFile: function(file){
            this.deleteFile(file);
            this.set('errorFileSizeTooLong', false);
            this.set('errorFilesCountTooLong', false);
        },
        removeAllFiles: function(){
            var files = this.get('model.files');
            var self = this;
            if (files){
                for(var i=0;i<files.length; i++){
                    self.deleteFile(files[i]);
                }
            }
        },
        downloadFile: function(file){
            var url = window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/download-attachment?key=' + window.AppkitENV.key + '&user_key=' + window.AppkitENV.user_key + '&is_external_user=' + window.AppkitENV.is_external_user + '&guid=' + file.get('Guid');
            window.open(url);
        },
        closeErrFileSizeTooLong: function(){
            this.set('errorFileSizeTooLong', false);
        },
        closeErrorFilesCountTooLong: function(){
            this.set('errorFilesCountTooLong', false);
        }
    },
    deleteFile: function(file){
        var instance = this.get('component');
        if (instance){
            instance.deleteFile(file.get('Name'), file.get('Guid'), this.get('annotationId'));
        }
    }
});
export default MultipleUploadController;
