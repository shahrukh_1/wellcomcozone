import Ember from "ember";

var ConfirmationModalController = Ember.ObjectController.extend({
  needs: ['comments/index'],
  title: "CANNOT COMPLETE YOUR ACTION",
  message: "Press ok to refresh the page",
  headingMessage: "The present phase has been completed by other user, you cannot perform any action without refreshing the page",
  yes: "OK",
     
  actions: {
    reloadPage: function(){
        window.location.reload();
    },
   
  },

});
export default ConfirmationModalController;