import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var ApplicationController = Ember.Controller.extend({
    sidebarShown: true,
    needs: ['approval'],
    approval: Ember.computed.alias('controllers.approval'),
    version: window.AppkitENV.version,
    IsfromProjectFolder: window.AppkitENV.IsfromProjectFolder,
    init: function () {
      var windowWidth = Ember.$(window).width();
      if (windowWidth < AppConstants.ExtraSmallDevicesWidth) {
        var self = this;
        Ember.run.later(function() {
          self.set("sidebarShown", false);
        });
      }
    },
    actions: {
        toggleSidebar: function(){
            this.set('sidebarShown', !this.get('sidebarShown'));
        },
        makeDecision: function() {
            this.get('approval').send('showDecision');
        }
    }

});
export default ApplicationController;
