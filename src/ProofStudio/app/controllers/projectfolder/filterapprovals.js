import Ember from "ember";

var FilterApprovals = Ember.ObjectController.extend({
   title: "Filter Approvals",
  yes: Ember.I18n.t('lblApply'),
  no: Ember.I18n.t('btnCancel'),
  tagwords: [],
  imageChecked: '',
  videoChecked: '',
  documentChecked: '',
  htmlChecked: '',
  checkedApprovalTypes: function(){
    var selectedApprovalTypes = window.AppkitENV.SelectedApprovalTypes.split(',');
    var self = this;
    selectedApprovalTypes.forEach(function(approvalType){
    if(approvalType === "Image"){
      self.set('imageChecked', true);
    }
    if(approvalType === "Video"){
      self.set('videoChecked', true);
    }
    if(approvalType === "Document"){
      self.set('documentChecked', true);
    }
    if(approvalType === "HTML"){
      self.set('htmlChecked', true);
    }
    });
  },
  isApprovalTypeImageChecked: function() {
    if(this.get('imageChecked') === ''){
      this.checkedApprovalTypes();
      if(this.get('imageChecked') === ''){
        this.set('imageChecked', false);
      }
    }
   return this.get('imageChecked');
  }.property('imageChecked'),
  isApprovalTypeVideoChecked: function() {
    if(this.get('videoChecked') === ''){
      this.checkedApprovalTypes();
      if(this.get('videoChecked') === ''){
        this.set('videoChecked', false);
      }
    }
    return this.get('videoChecked');
   }.property('videoChecked'),
   isApprovalTypeDocumentChecked: function() {
    if(this.get('documentChecked') === ''){
      this.checkedApprovalTypes();
      if(this.get('documentChecked') === ''){
        this.set('documentChecked', false);
      }
    }
    return this.get('documentChecked');
   }.property('documentChecked'),
   isApprovalTypeHTMLChecked: function() {
    if(this.get('htmlChecked') === ''){
      this.checkedApprovalTypes();
      if(this.get('htmlChecked') === ''){
        this.set('htmlChecked', false);
      }
    }
    return this.get('htmlChecked');
   }.property('htmlChecked'),
  changeApprovalsCountOnSelection: function() {
    var self = this;
    var selectedApprovalTypes = self.allApprovalTypesSelected();
    var selectedProjectName = Ember.$('.czn-project-name')[0].innerText;
    var selectedProjectID = this.store.all("projectfolder").filterBy("ProjectFolderName", selectedProjectName)[0].id;
    var selectedTagwords = self.allSelectedTagwords();
    this.store.adapterFor('annotation').loadTagwordsandApprovalCount(selectedProjectID, selectedApprovalTypes, selectedTagwords ).then(function(result){
      self.send('showFilterApprovalsDialogModal');
      Ember.$('.czn-approvalTypeImage-results')[0].innerText = result.NoOfImageApprovals + " " + "results";
      Ember.$('.czn-approvalTypeDocument-results')[0].innerText = result.NoOfDocumentApprovals + " " + "results";
      Ember.$('.czn-approvalTypeVideo-results')[0].innerText = result.NoOfVideoApprovals + " " + "results";
      Ember.$('.czn-approvalTypeHTML-results')[0].innerText = result.NoOfHTMLApprovals + " " + "results";

      if(Ember.$('.czn-unselectedtagword').length > 0){
        Ember.$('.czn-unselectedtagword').remove();
      }
      self.set('tagwords', result.tagwords);
    }); 
  },
  allSelectedTagwords: function(){
    var selectedTagwords = "";
    if(Ember.$('.czn-selectedtagword').length > 0){
       for(var i = 0; i < Ember.$('.czn-selectedtagword').length ; i++){
        if(selectedTagwords === ""){
          selectedTagwords = Ember.$('.czn-selectedtagword')[i].id;
        } else {
          selectedTagwords = selectedTagwords + ',' + Ember.$('.czn-selectedtagword')[i].id;
        }
      }
    }
    return selectedTagwords;

  },
  allApprovalTypesSelected: function () {
    var selectedApprovalTypes = "";
    if(Ember["$"]('.czn-approvalType-image')[0].classList[1] === "fa-check-square-o"){
      selectedApprovalTypes ="Image";
      this.set('imageChecked', true);
    }
    else {
      this.set('imageChecked', false);
    }
    if(Ember["$"]('.czn-approvalType-document')[0].classList[1] === "fa-check-square-o"){
      if(selectedApprovalTypes === ""){
      selectedApprovalTypes = "Document";
      } else {
        selectedApprovalTypes = selectedApprovalTypes + ',' + "Document";
      }
      this.set('documentChecked', true);
    }
    else {
      this.set('documentChecked', false);
    }
    if(Ember["$"]('.czn-approvalType-video')[0].classList[1] === "fa-check-square-o"){
      if(selectedApprovalTypes === ""){
      selectedApprovalTypes ="Video";
      } else {
        selectedApprovalTypes = selectedApprovalTypes + ',' + "Video";
      }
      this.set('videoChecked', true);
    } else {
      this.set('videoChecked', false);
    }
    if(Ember["$"]('.czn-approvalType-html')[0].classList[1] === "fa-check-square-o"){
      if(selectedApprovalTypes === ""){
      selectedApprovalTypes ="HTML";
      } else{
        selectedApprovalTypes = selectedApprovalTypes + ',' + "HTML";
      }
      this.set('htmlChecked', true);
    } else {
      this.set('htmlChecked', false);
    }
      return selectedApprovalTypes;
  },
  actions: {
    selectTagword: function(tagword) {
      Ember.$('.czn-tagwordstitle').append('<span class="czn-selectedtagword" id='+ tagword.Id + '><span class="czn-tagwordText">'+ tagword.tagword+'</span><span class="czn-tagwordCount">'+tagword.count+'</span></span>');
      Ember.$('#clearAllSelectedtagwords').show();
      this.changeApprovalsCountOnSelection();
    },
    clearAll: function() {
      Ember.$('#clearAllSelectedtagwords').hide();
      Ember.$('.czn-selectedtagword').remove();
      this.changeApprovalsCountOnSelection();
    },
    filterApproval: function(){
      this.send('cancel');
     var selectedApprovalTypes = this.allApprovalTypesSelected();
     if(selectedApprovalTypes != ""){
     var approvalTypes = selectedApprovalTypes.split(',');
     Ember.$('.czn-projectApproval-list').hide();
     approvalTypes.forEach(function(type){
      Ember.$('.czn-projectApproval-list.'+type).show();
     });
    } else {
      Ember.$('.czn-projectApproval-list').show();
    }
    },
    cancel: function () {
      this.send('closeFilterApprovalsDialogModal');
    },
    selectedApprovalType: function (approvalType) {
      var self = this;
      if(Ember["$"]('.czn-approvalType-' + approvalType)[0].classList[1] === "fa-square-o")
      {
      Ember["$"]('.czn-approvalType-' + approvalType)[0].className = "fa fa-check-square-o czn-approvalType-" + approvalType;
      } else {
      Ember["$"]('.czn-approvalType-' + approvalType)[0].className = "fa fa-square-o czn-approvalType-" + approvalType;
      }
      self.changeApprovalsCountOnSelection();
   },
  }
});
export default FilterApprovals;
