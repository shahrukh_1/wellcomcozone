import Ember from "ember";
import AppConstants from "appkit/config/constants";
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var ProjectFolderIndexController = Ember.ArrayController.extend(UserPermissions, {
    needs: ['projectfolder/filterapprovals'],
    selectedProjectName: '',
    selectedProjectID: '',
    projectFolderNames: [],

    setScrollTop: function() {
      var s = parseInt(window.AppkitENV.ScrollTop);
      if(s > 0){
      Ember.$('#projectfolder').scrollTop(s);
      }
    },
    hideUnselectedApprovalType: function() {
      if(window.AppkitENV.SelectedApprovalTypes != "")
      {
      Ember.$('.czn-projectApproval-list').hide();
      window.AppkitENV.SelectedApprovalTypes.split(',').forEach(function(type){
        Ember.$('.czn-projectApproval-list.'+type).show();
      });
    } else {
      var approvaltypes = this.SelectedApprovalTypes();
      if(approvaltypes.length > 0 && approvaltypes[0] != ""){
        Ember.$('.czn-projectApproval-list').hide();
        approvaltypes.forEach(function(type){
        Ember.$('.czn-projectApproval-list.'+type).show();
      });
      }
    }
    },
    SelectedApprovalTypes: function() {
      var result = [];
      if(this.get('controllers.projectfolder/filterapprovals.imageChecked') === ''){
        window.AppkitENV.SelectedApprovalTypes.split(',').forEach(function(approvaltype){
         result.push(approvaltype);
        });
    } else {
      if(this.get('controllers.projectfolder/filterapprovals.imageChecked') === true){
        result.push("Image");
      } 
      if(this.get('controllers.projectfolder/filterapprovals.videoChecked') === true){
        result.push("Video");
      }
      if(this.get('controllers.projectfolder/filterapprovals.documentChecked') ===  true){
        result.push("Document");
      }
      if(this.get('controllers.projectfolder/filterapprovals.htmlChecked') === true){
        result.push("HTML");
      }
    }
     return result;
    },
    initProjectFolders: function(){
      var self = this;
      var selectedProjectID = this.get('selectedProjectID');
      if(selectedProjectID === "")
      {
        selectedProjectID = window.AppkitENV.ProjectFolderID;
    } 
    else { 
      selectedProjectID = this.get('selectedProjectID');
    }
      var approvals = this.store.all("projectfolder").filterBy('id', selectedProjectID.toString())[0]._data.projectfolderapproval;
      var projectName = this.store.all('projectfolder').filterBy('id', selectedProjectID.toString())[0].get('ProjectFolderName');
      this.set('selectedProjectName', projectName);
      var names = [];
      this.store.all('projectfolder').forEach(function(project){
        var projectDetails = {ID: project.get('id'), Name: project.get('ProjectFolderName')};
        names.pushObject(projectDetails);
      });
      this.set('projectFolderNames', names);
      Ember.run.later(function(){
        self.hideUnselectedApprovalType();
        self.setScrollTop();
      });
       return approvals;
    }.property('model', 'selectedProjectID'),

    actions: {
      openFilterPopup: function() {
        var self = this;
        this.send('showFilterApprovalsDialogModal');
        var selectedProjectID = this.get('selectedProjectID');
      if(selectedProjectID === "")
      {
        selectedProjectID = window.AppkitENV.ProjectFolderID;
      } 
      var approvalTypes = "";
      var tagwords = "";
        this.store.adapterFor('annotation').loadTagwordsandApprovalCount(selectedProjectID, approvalTypes, tagwords ).then(function(result){
          self.set('controllers.projectfolder/filterapprovals.tagwords', result.tagwords);
          self.send('showFilterApprovalsDialogModal');
          Ember.$('.czn-approvalTypeImage-results')[0].innerText = result.NoOfImageApprovals + " " + "results";
          Ember.$('.czn-approvalTypeDocument-results')[0].innerText = result.NoOfDocumentApprovals + " " + "results";
          Ember.$('.czn-approvalTypeVideo-results')[0].innerText = result.NoOfVideoApprovals + " " + "results";
          Ember.$('.czn-approvalTypeHTML-results')[0].innerText = result.NoOfHTMLApprovals + " " + "results";
  
         });
      },
      selectedApproval: function(approvalId) {
        var selectedApprovalTypes = this.SelectedApprovalTypes();
        var scrollTop = Ember.$('#projectfolder').scrollTop();
        this.store.adapterFor('annotation').loadSelectedApprovalAsSession(approvalId, selectedApprovalTypes, scrollTop).then(function(result){
          if(result === true){
          window.location.reload();
          }
        });
      },
      changeSelectedProject: function(projectID){
        this.set('selectedProjectID', projectID);
      },
      projectNameDropDown: function(){
        if(Ember.$("#projectFolderNames")[0].classList[1] === "fa-caret-down"){
        Ember.$("#projectFolderNames")[0].className = "fa fa-caret-left unselectable czn-group-icon dropdown-toggle";
        } else {
        Ember.$("#projectFolderNames")[0].className = "fa fa-caret-down unselectable czn-group-icon dropdown-toggle";
        }
      }
    }
 });
export default ProjectFolderIndexController;
