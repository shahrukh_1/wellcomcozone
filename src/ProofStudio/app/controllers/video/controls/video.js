import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var VideoControlsController = Ember.Controller.extend(CurrentViewerMixin, {
    needs: ['sidebar/index','comments/index'],
    annotations: Ember.computed.alias('annotationsSortedByIndexWithFilterFlag'),
    playing: Ember.computed.alias('model.playing'),
    maxDuration: 0,
    currentTime: Ember.computed.alias('model.currentTime'),
    timeFrame: null,
    showAllTickets: false,
    currentAnnotation: Ember.computed.alias('controllers.comments/index.currentAnnotation'),

    versionChanged: function(){
        this.set('model', this.get('currentViewerController'));
    }.observes('controllers.approval.viewerControllers.@each.select', 'controllers.approval.viewerControllers'),

    annotationsShouldChange : function() {
      var self = this;
      Ember.run.later(function() {
        var currentViewController = self.get('controllers.comments/index').get('currentViewerController');
        if (currentViewController != undefined && currentViewController != null) {
          currentViewController.set('annotationsChanged', new Date());
        }

        self.get('controllers.comments/index').set('annotationsChanged', new Date());
      },500);
    }.observes('session.user.data.ShowAnnotationsOfAllPhases'),

    actions:
    {
        setDuration: function(duration){
            this.set('maxDuration', duration);
        },

        markerSelect: function(timeFrame){
            var time = (timeFrame/1000000);
            this.set('timeFrame', time);
        },
        showHideAllTickets: function(){
            this.set('showAllTickets', !this.get('showAllTickets'));
            this.send('toggleAllTickets', this.get('showAllTickets'));
        }
    },

    hasNoTickets: function(){
        return this.get('controllers.sidebar/index.annotations').length < 1;
    }.property('controllers.sidebar/index.annotations')
});
export default VideoControlsController;
