import Ember from "ember";
import AppConstants from 'appkit/config/constants';
import CanvasView from 'appkit/controllers/viewer/annotations/canvas-view';
import DrawMarqueeShape from 'appkit/views/viewer/annotations/draw-shape-marquee';

var VideoCanvasView = Ember.Mixin.create(CanvasView, DrawMarqueeShape, {
  mouseUp: true,
  isSelectedObjectChanging: false,
  svgTemp: null,
  initCanvas: function () {
      var self = this;

      var video = Ember.$("video:first");
      var layerPins = Ember.$("<div class='pinsvideo-layer'></div>").insertBefore(video);
      this.set('layerPins', layerPins);

      var layerCanvas = Ember.$("<canvas class='canvasvideo-layer'></canvas>").insertBefore(video);
      this.set('layerCanvas', layerCanvas);

      var fabricCanvas = this.get('videoFabricCanvas');
      if (!fabricCanvas) {
          fabricCanvas = new fabric.Canvas(layerCanvas.get(0));
          fabricCanvas.isDrawingMode = false;
          fabricCanvas.on('touch:drag', function(ev) {
            if (!(ev.e.clientX)) {// it is on tablet
              if (!ev.e.touches)
                return;
              ev.e.clientX = ev.e.touches[0].clientX;
              ev.e.clientY = ev.e.touches[0].clientY;
              ev.e.button = 0;
            }

            if (self.get("mouseUp") == true) {
              self.set("mouseUp", false);

              if (self.get('isDrawRectangleSelected')) {
                self.startDrawMarqueeRectangle(ev.e);
              }

            }
          }).on('path:created', function(e){
            self.canvasPathCreatedEvent(e);
          }).on('mouse:up', function(ev) {
              if (!(ev.e.clientX)) {// it is on tablet
                if (!ev.e.touches)
                  return;
                ev.e.clientX = ev.e.touches[0].clientX;
                ev.e.clientY = ev.e.touches[0].clientY;
                ev.e.button = 0;
              }
              if (self.get('isDrawRectangleSelected')) {
                self.startDrawMarqueeRectangle(ev.e);
                }

              if (self.get('mouseUp') == true) {
                if (self.get('isDrawRectangleSelected')) {
                  self.storeMarqueeRectangleOnMouseUp(ev.e);
                }
              }

              var isDrawShapeSelected = self.get('controller.controllers.navigation/annotation').get('isDrawShapeSelected');
              if (isDrawShapeSelected === true) {
                fabricCanvas.isDrawingMode = false;
                self.addShapeOnMouseUp(ev);
              }
            self.set("mouseUp", true);

          }).on('object:selected', function(){
            self.addNewSVG();
            self.updateCanvas();
          }).on('group:selected', function(){
            self.updateCanvas();
          }).on('selection:cleared', function() {
            self.updateCanvas();
          }).on('object:selected', function() {
          }).on('object:modified', function() {
            self.addNewSVG();
          }).on('mouse:move', function(ev){
            if (!(ev.e.clientX)) {// it is on tablet
              if (!ev.e.touches)
                return;
              ev.e.clientX = ev.e.touches[0].clientX;
              ev.e.clientY = ev.e.touches[0].clientY;
            }

            if (self.get('isDrawRectangleSelected') && (self.get('mouseUp') == false)) {
              self.adjustMarqueeRectangleOnMouseMove(ev.e);
            }
          }).on('object:scaling', function(){
            self.set('isSelectedObjectChanging', true);

          }).on('object:rotating', function(){
            self.set('isSelectedObjectChanging', true);

          }).on('object:moving', function(e) {
            self.set('isSelectedObjectChanging', true);

          });
          this.set('videoFabricCanvas', fabricCanvas);
      }

      this.set('marqueeFabricCanvas', fabricCanvas);

      Ember.$(document).on('keydown', function(e){
        self.onKeyDownHandler(e);
      });

      Ember.run.later(function(){
        self.syncLayer(self);
      });

      Ember.$(window).on('resize',function () {
        self.syncLayer(self);
        self.canvasLayerReDrawOnResizeEvent();
        self.syncVideoLayerOnResize(self);
      });

      layerPins.on('click', function(ev){
        self.newAnnotationClicked(ev, self);
      });
  },
  addShapeOnMouseUp: function (ev) {
    var canvas = this.get('videoFabricCanvas');
    var activeObject = canvas.getActiveObject();
    var annotationController = this.get('controller.controllers.navigation/annotation');
    if (annotationController.get('isDrawShapeSelected') == true &&
      !activeObject) {
      this.addShapeToCanvas(ev);
    }
  },
  onDrawingLineChanged: function(){
    var canvas = this.get('videoFabricCanvas');
    this.drawingLineChanged(canvas);
  }.observes('controller.controllers.navigation/annotation.isDrawLineSelected', 'controller.controllers.navigation/annotation.isMoveShapeSelected'),

  // this method does not return the correct scale
  scale:function () {
      var annotation = this.get('annotation');
      var layerCanvas = this.get('layerCanvas');
      var originalVideoWidth = annotation.get('Page.LargeThumbnailWidth');
      var originalVideoHeight = annotation.get('Page.LargeThumbnailHeight');

      if (originalVideoWidth === 0 || originalVideoHeight === 0) {
        throw new Error("No width or height exists into db.");
      }
      var scale = {x: layerCanvas.width()/originalVideoWidth, y: layerCanvas.height()/originalVideoHeight };
      return scale;
  }.property('annotation','layerCanvas','annotation.Page.LargeThumbnailWidth','annotation.Page.LargeThumbnailHeight'),

  canvasLayerReDraw: function(){
      var annotation = this.get('annotation');
      var singleAnnotationView = this.get('singleAnnotationView');
      var layerIsVisible = this.get('layerIsVisible');
      var layerCanvas = this.get('layerCanvas');
      var fabricCanvas = this.get('videoFabricCanvas');

      if(annotation && singleAnnotationView === true){
        if (layerIsVisible === true && layerCanvas && fabricCanvas){

          if (annotation.get('CommentType') === AppConstants.CommentTypes.DRAW_COMMENT && fabricCanvas.isDrawingMode === false){

            Ember.$('canvas.canvasvideo-layer').show();
            var fullWidth = annotation.get("OriginalVideoWidth"), //original video width
            fullHeight = annotation.get("OriginalVideoHeight"),//original video height
            width = Ember.$("video").width(), // current video width
              height = Ember.$("video").height(); //current video height
            var scale = {x: fullWidth / width, y: fullHeight / height};

            var shapes = this.get('controller').store.all('custom-shape').filter(function(custom_shape){
              return custom_shape.get('ApprovalAnnotation.id') === annotation.get('id');
            });
            shapes.forEach(function (shape, markerIndex) {
              var svg = shape.get('SVG');

              Ember.run.later(function() {
                var canvas = layerCanvas.get(0);
                var ctx = canvas.getContext('2d');
                ctx.clearRect(0, 0, canvas.width, canvas.height);

                fabricCanvas.selection = false;

                fabric.loadSVGFromString(svg, function (objects, options) {
                  var loadedObject = fabric.util.groupSVGElements(objects, options);

                  if (markerIndex === 0){
                    // clear canvas for the first marker (per annotation) that need to be displayed
                    fabricCanvas.clear();
                    fabricCanvas.draggable = false;
                  }

                  loadedObject.set({
                    hasBorders: false,
                    hasControls: false,
                    hasRotatingPoint: false,
                    lockMovementX: true,
                    lockMovementY: true,
                    scaleX: 1/scale.x,
                    scaleY: 1/scale.y,
                    selectable: false,
                    originX: 'left',
                    originY: 'top'
                  }).setCoords();
                  fabricCanvas.add(loadedObject).renderAll();
                });
              });
            });
          }

        }
      }
      else{
        if (fabricCanvas && fabricCanvas.isDrawingMode === false){
          fabricCanvas.clear();
        }
      }
  },

  canvasLayerReDrawOnResize: function () {
    var annotation = this.get('annotation');
    var singleAnnotationView = this.get('singleAnnotationView');
    var layerIsVisible = this.get('layerIsVisible');
    var layerCanvas = this.get('layerCanvas');
    var fabricCanvas = this.get('videoFabricCanvas');
    var shapes = this.get('svgTemp');
    if (singleAnnotationView === true && shapes != null) {
      if (layerIsVisible === true && layerCanvas && fabricCanvas) {
        Ember.$('canvas.canvasvideo-layer').show();
        var fullWidth = null, fullHeight = null;
        if (annotation == null) {
          fullWidth = fabricCanvas.OriginalWidth;
          fullHeight = fabricCanvas.OriginalHeight;
        }
        else {
          fullWidth = annotation.get("OriginalVideoWidth"); //original video width
          fullHeight = annotation.get("OriginalVideoHeight"); //original video height
        }

        var width = Ember.$("video").width(), // current video width
          height = Ember.$("video").height(); //current video height
        var scale = { x: fullWidth / width, y: fullHeight / height };

        shapes.forEach(function (svg, markerIndex) {
          Ember.run.later(function () {
            var canvas = layerCanvas.get(0);
            var ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            fabricCanvas.selection = false;

            fabric.loadSVGFromString(svg, function (objects, options) {
              var loadedObject = fabric.util.groupSVGElements(objects, options);

              if (markerIndex === 0) {
                // clear canvas for the first marker (per annotation) that need to be displayed
                fabricCanvas.clear();
                fabricCanvas.draggable = false;
              }

              loadedObject.set({
                hasBorders: false,
                hasControls: false,
                hasRotatingPoint: false,
                lockMovementX: true,
                lockMovementY: true,
                scaleX: 1 / scale.x,
                scaleY: 1 / scale.y,
                selectable: false,
                originX: 'left',
                originY: 'top'
              }).setCoords();
              fabricCanvas.add(loadedObject).renderAll();
            });
          });
        });


      }
    }
    else {
      if (fabricCanvas && fabricCanvas.isDrawingMode === false) {
        fabricCanvas.clear();
      }
    }
  },

  canvasLayerReDrawOnResizeEvent:function(){
    if(this.get('annotation') == null){
      this.canvasLayerReDrawOnResize();
    }
    else{
      this.canvasLayerReDraw();
    }
  },

  addShapeToCanvas:function(mouseEventDetails){
      var self = this;
      var shape;
      var fabricCanvas = this.get('videoFabricCanvas');
      var layerCanvas = this.get('layerCanvas');
      var newAnnotationAddingState = this.get('newAnnotationAddingState');
    var clickX = mouseEventDetails.e.offsetX;
    var clickY = mouseEventDetails.e.offsetY;

    if (newAnnotationAddingState === true) {
      var annotationController = this.get("controller.controllers.navigation/annotation");
      if (annotationController) {
        // Shape is selected for annotation
        if (annotationController.get('isDrawShapeSelected')) {
          var selectedColor = annotationController.get('drawColor');
          shape = annotationController.shape;
        }
      }
      var svg = shape.get('SVG');

      Ember.run.later(function () {
        self.set('annotation', null);

        fabric.loadSVGFromString(svg, function (objects, options) {
          var loadedObject = fabric.util.groupSVGElements(objects, options);
          if (loadedObject) {
            //fabricCanvas.draggable = true;
            loadedObject.set({
              left: clickX - loadedObject.width / 2,
              top: clickY - loadedObject.height / 2,
              angle: 0,
              cornerColor: 'white',
              cornerSize: 10,
              transparentCorners: false,
              lockMovementX: false,
              lockMovementY: false,
              hasRotatingPoint: true,
              fill: selectedColor,
              isCustomShape: true
            }).setCoords();

            fabricCanvas.add(loadedObject);
            fabricCanvas.setActiveObject(loadedObject);
          }

          var point = {x: Math.floor(clickX * self.get('width') / layerCanvas.width()), y: Math.floor(clickY * self.get('height') / layerCanvas.height())};
          self.get('controller').send('newComment', {
            Point: point,
            PageId: self.get('pageId'),
            TimeFrame: self.get('timeFrame'),
            CommentType: AppConstants.CommentTypes.DRAW_COMMENT
          });
        });
      });
    }

  },

  onKeyDownHandler:function(e) { // delete selected shape
      var fabricCanvas = this.get('videoFabricCanvas');
      switch (e.keyCode) {
        case 46: // delete
          var activeObject = fabricCanvas.getActiveObject();
          if (activeObject) {
            fabricCanvas.remove(activeObject);

            //disable "add comment" text area if no shapes were added on the canvas
            var objects = fabricCanvas.getObjects();
            if(objects.length === 0){
              Ember.$('#commentArea').attr('disabled', true);
            }
          }
          return;
        case 27:  //escape
          var controller = this.get('controller');
          if (controller) {
            var commentsController = controller.controllerFor('comments/index');
            // if there are comments and the confirmation dialog is not visible than open confirmation dialog
            if (commentsController.get('isUnSavedAnnotation') == true) {
              var closeDialogController = controller.controllerFor('confirm-dialog');
              if (closeDialogController.get('visible') == false) {
                controller.send('showConfirmDialogModal');
              }
            } else {
              controller.send('doneAnnotating');
            }
          }
          return;
      }
  },
  onDrawColorChange: function(){
    var fabricCanvas = this.get('videoFabricCanvas');
    this.drawColorChanged(fabricCanvas);
  }.observes('controller.controllers.navigation/annotation.drawColor'),

  drawingModeInVideoCanvasView: function(enter, lineWidth, lineColor){
      var fabricCanvas = this.get('videoFabricCanvas'),
        freeDrawingMode = this.get('freeDrawingMode');

      if (fabricCanvas){
        var selectedObject = fabricCanvas.getActiveObject();
        this.set('shapeColor', lineColor);
        if (selectedObject){
          if (lineColor) {
            // set stroke color and width only for path that were created with free drawing
            if (freeDrawingMode === true) {
              selectedObject.setStrokeWidth(lineWidth);
              selectedObject.setStroke(lineColor);
              this.set('lineWeight', lineWidth);
            }
            else{
              selectedObject.setFill(lineColor);
            }
            selectedObject.strokeDashArray = [];
            fabricCanvas.renderAll();
          }
        }
        else {
          fabricCanvas.isDrawingMode = enter;
          fabricCanvas.selection = false;
          fabricCanvas.draggable = false;
          if (enter === true) {
            this.set('freeDrawingMode', true);
            Ember.$('canvas.canvasvideo-layer').show();
            fabricCanvas.freeDrawingBrush.color = lineColor;
            fabricCanvas.freeDrawingBrush.width = lineWidth;
            fabricCanvas.freeDrawingBrush.shadowBlur = 0;
            this.set('annotation', null);
          }
        }
      }
  },

  clearCanvas: function(){
      Ember.$('canvas.canvasvideo-layer').show();
      var fabricCanvas = this.get('videoFabricCanvas');
      if (fabricCanvas) {
        fabricCanvas.clear();
      }
  },
  hideCanvas: function () {
      Ember.$('canvas.canvasvideo-layer').hide();
  },
  canvasPathCreatedEvent: function(e){
      this.updateCanvas();
      var path = e.path;
      path.perPixelTargetFind = true;
      path.targetFindTolerance = 0;
      path.lockMovementX = false;
      path.lockMovementY = false;
      path.hasRotatingPoint = true;
      path.angle = 0;
      path.hasControls = path.hasBorders = true;
      path.cornerColor = 'white';
      path.cornerSize =  10;
      path.transparentCorners = false;

      var point = {x: path.left, y: path.top};
      this.get('controller').send('newComment', {Point: point, PageId: this.get('pageId'), CommentType: AppConstants.CommentTypes.DRAW_COMMENT});
      this.addNewSVG();
  },
  updateCanvas: function() {
      var fabricCanvas = this.get('videoFabricCanvas');
      if (fabricCanvas){
        fabricCanvas.renderAll();
      }
  },
  //pathSelectedCanvas: function(){
  //    this.get('controller').send('drawingSelectionCreated');
  //},
  clearSelection: function(){
      var fabricCanvas = this.get('videoFabricCanvas');
      if (fabricCanvas){
        fabricCanvas.discardActiveObject();
        fabricCanvas.discardActiveGroup();
      }
  },

  addNewSVG: function(){
      var annotationId = this.get('annotation.id');
      // if annotationId is not null then return from this method
      if (annotationId){
        return;
      }

      var self = this;
      var timeFrame = this.get('timeFrame');
      Ember.run.later(function(){
        Ember.$('#commentArea').removeAttr('disabled');
        var fabricCanvas = self.get('videoFabricCanvas');

        var fullWidth = self.get('videoFabricCanvas.width'), //original video width
          fullHeight = self.get('videoFabricCanvas.height'),//original video height
          width = Ember.$("video").width(), // current video width
          height = Ember.$("video").height(); //current video height

        if(fullWidth === 0 || fullHeight === 0){
          throw new Error("Original video width or height is 0!");
        }

        var scale = [],
          objects = [],
          positions =[];
        var documentScale = {x: fullWidth / width, y: fullHeight / height};

        fabricCanvas.forEachObject(function(obj){
          var center = obj.getCenterPoint();

          // the position of the pin is in the center of the objects
          positions.pushObject({x:center.x ,y:center.y});

          //calculate scale of the svg
          scale.pushObject({scaleX:obj.scaleX * documentScale.x, scaleY:obj.scaleY * documentScale.y});

          var objectSvg = obj.toSVG();
          var svg = AppConstants.SVGFormat.replace(/{width}/g, fullWidth).replace(/{height}/g, fullHeight).replace(/{translate}/g, "scale(" + documentScale.x + " " + documentScale.y + ")").replace(/{svg}/g, objectSvg).replace(/stroke-dasharray: ;/g, '');
          objects.pushObject(svg);
        });
        scale.reverse();

        var poligon = new fabric.Polygon(positions);
        var center = poligon.getCenterPoint();

        var point = {x: (center.x * documentScale.x).toFixed(1) ,y: (center.y * documentScale.y).toFixed(1)};// scale the point to the original width and height
        var data = {Point: point, PageId: self.get('pageId'), TimeFrame: timeFrame, Svg: objects, Scale: scale, commentType: AppConstants.CommentTypes.DRAW_COMMENT}; //svgAnnotations
        self.get('controller').send('newSVGAnnotation', data);
        self.set('svgTemp', objects);
      });
  }.observes('drawColor', 'selectedWidth'),

  //show/hide drawing related buttons from navigation menu
  showSpecificButtons: function() {
      var canvas = this.get('videoFabricCanvas');
      var selectedShape = canvas.getActiveObject();
      if(selectedShape) {
        if(selectedShape.isCustomShape){
          this.send('showSpecificButtons', true);
          return true;
        }
        this.send('showSpecificButtons', false);
      }
  },

//--------------------- Marquee drawing tool -----------------------------

  onMouseUpChange:function() {
    if (this.get('mouseUp')) {
      this.set('isSelectedObjectChanging', false);
    }
  }.observes('mouseUp'),

  onSelectedDrawWidthChange: function() {
    this.changeSelectedMarqueRectangleWidth(this.get('selectedDrawWidth'));
    this.drawWidthChanged(this.get('videoFabricCanvas'), this.get('selectedDrawWidth'));
  }.observes('selectedDrawWidth'),

  sendMarkupsAndShapesToCommentPopup() {// empty method to be compatible with image version

  }
  //-----------------------------------------------------------------------
});

export default VideoCanvasView;
