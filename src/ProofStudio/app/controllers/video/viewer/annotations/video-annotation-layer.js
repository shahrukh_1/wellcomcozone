import Ember from "ember";
import AnnotationLayer from 'appkit/controllers/viewer/annotations/annotation-layer';
import AppConstants from 'appkit/config/constants';
import PlusDivIcon from 'appkit/components/video/viewer/annotations/plus-div-icon';
import NumberedDivIcon from 'appkit/components/video/viewer/annotations/numbered-div-icon';
import VideoCanvasView from 'appkit/controllers/video/viewer/annotations/video-canvas-view';

var VideoAnnotationLayer = AnnotationLayer.extend(Ember.ActionHandler, VideoCanvasView, {
    needs: ['navigation/main', 'navigation/annotation'],
    selectedWidth:  Ember.computed.alias('controller.controllers.navigation/annotation.selectedWidth'),
    isDrawRectangleSelected: Ember.computed.alias('controller.controllers.navigation/annotation.isDrawRectangleSelected'),
    drawColor: Ember.computed.alias('controller.controllers.navigation/annotation.drawColor'),
    layerPins:   null,                     // for pins
    layerCanvas: null,                     // for SVG
    fabricCanvas: null,
    freeDrawingMode: false,
    svgAnnotations : [],
    shapeColor: '',
    lineWeight: '',

    init: function () {
      this.initCanvas();

      this.addNewAnnotation();
    },

    isShowPinsObservable: function(){
      var showPins = this.get('controller.controllers.navigation/main.isShowPins');

      if(showPins) {
        this.refreshMultipleAnnotationsLayer();
      } else {
        this.clearMarkers();
      }
    }.observes('controller.controllers.navigation/main.isShowPins'),

    addNewAnnotation: function () {
    }.observes('newAnnotationAddingState'),

    newAnnotationClicked: function (ev, context) {
        var self = this;
        var position = { x: ev.offsetX, y: ev.offsetY };
        var layerPins = context.get('layerPins');
        var newAnnotationAddingState = context.get('newAnnotationAddingState');
        var plusMarker = context.get('plusMarker');

        if (newAnnotationAddingState === true && !plusMarker) {
          //create marker with plus icon and add it to the layerPins
          this.addPlusMarker(position);

          var point = { x: ev.offsetX, y: ev.offsetY };
          context.get('controller').send('newComment', {Point: point, PageId: context.get('pageId'), TimeFrame: context.get('timeFrame'), CommentType: AppConstants.CommentTypes.MARKER_COMMENT});

          var clickedPoint = {x: ev.originalEvent.clientX, y: ev.originalEvent.clientY - 20};
          Ember.run.later(function() {
            self.updateAnnotationPopupLine(clickedPoint);
          });
        }

        newAnnotationAddingState = false;
    },

    syncVideoLayerOnResize: function(){
      var video = Ember.$("video.ember-view:first");
      var annotation = this.get('annotation');
      var position = null;
      var originalPoint = null;
      var width = this.layerPins[0].clientWidth,// current video width
          height =this.layerPins[0].clientHeight; //current video height

      var offset = Ember.$(video).offset();
      var padding = parseInt(Ember.$(video).css('padding-top'));

      if(annotation == null){
        originalPoint = {offsetX: this.videoFabricCanvas.OriginalWidth, offsetY: this.videoFabricCanvas.OriginalHeight};
        position = {left: this.plusMarker.position.x, top: this.plusMarker.position.y};
      }
      else
      {
        originalPoint ={offsetX: this.get('annotation.OriginalVideoWidth'), offsetY: this.get('annotation.OriginalVideoHeight')};
        position = {left: this.get('annotation.CrosshairXCoord'), top: this.get('annotation.CrosshairYCoord')};
      }
       
      var xPostionPercentageChange = originalPoint.offsetX > 0 ? (width - originalPoint.offsetX) / originalPoint.offsetX : 0;
      var yPostionPercentageChange = originalPoint.offsetY > 0 ? (height - originalPoint.offsetY) / originalPoint.offsetY : 0;

      var pointx = Math.floor((position.left + 8) * (1 + xPostionPercentageChange));
      var pointy = Math.floor((position.top + 2) * (1 + yPostionPercentageChange));

      var positionToDraw = {x: (offset.left + padding + pointx), y: (offset.top + padding + pointy)-20};
      this.redrawAnnotationPopupLine(positionToDraw);
    },

    redrawAnnotationPopupLine: function(point) {
      var self = this;
      var commentsController = this.get('controller.controllers.comments/index');

      Ember.run.later(function () {
        var popupOffset = Ember.$(".czn-comment-modal").offset();
        if (popupOffset) {
          self.set('popup_Y', popupOffset.top);
          self.set('popup_X', popupOffset.left);

          if (commentsController) {
            commentsController.set("popup_X",popupOffset.left);
            commentsController.set("popup_Y",popupOffset.top);
            commentsController.send('setMarkerPosition', point.x, point.y + 20);
          }
        }
      },100);
      
    },

    addPlusMarker: function(position){
      var layerPins = this.get('layerPins');
      var marker = new PlusDivIcon();

      marker.set('position', position);
      marker.set('width', this.get('width'));
      marker.set('height', this.get('height'));
      marker.set('pinslayer', this);
      marker.addTo(layerPins);

      this.set('plusMarker', marker);
    },

    getCurrentSelectedAnnotationPlusMarker: function(currentAnnotation){
      var layerPins = this.get('layerPins');
      var getOriginalXpoint = currentAnnotation.get('OriginalVideoWidth') > 0 ? currentAnnotation.get('OriginalVideoWidth') : 0;
      var getOriginalYpoint = currentAnnotation.get('OriginalVideoHeight') > 0 ? currentAnnotation.get('OriginalVideoHeight') : 0;
      var xPostionPercentage = getOriginalXpoint == 0 ? 0 : (layerPins.width() - getOriginalXpoint)/getOriginalXpoint;
      var yPostionPercentage = getOriginalYpoint == 0 ? 0: (layerPins.height() - getOriginalYpoint)/getOriginalYpoint;

      var pointx = Math.floor(currentAnnotation.get('CrosshairXCoord') * (1 + xPostionPercentage));
      var pointy = Math.floor(currentAnnotation.get('CrosshairYCoord') *  (1 + yPostionPercentage));
      var point = {x: pointx, y: pointy};
      this.addPlusMarker(point);
    },

    updateAnnotationPopupLine: function(point) {
      this.setAnnotationPopupLine();
      var commentsController = this.get('controller.controllers.comments/index');
      if(commentsController) {
        commentsController.send('setMarkerPosition', point.x, point.y + 20);
      }
    },

    setAnnotationPopupLine: function(){
      var self = this;
      Ember.run.later(function(){
        var popupOffset = Ember.$(".czn-comment-modal").offset();
        if (popupOffset) {
          self.set('popup_Y', popupOffset.top);
          self.set('popup_X', popupOffset.left);
        }
      });
    },

    initAddMarker: function(annotationType){
        this._super();
        this.clearMarkers();
        this.set('annotation', null);

        var self = this;
        self.set('videoFabricCanvas.OriginalWidth',self.get("videoFabricCanvas").width);
        self.set('videoFabricCanvas.OriginalHeight',self.get("videoFabricCanvas").height);

        var fabricCanvas = self.get('videoFabricCanvas'),
            layerPins = this.get('layerPins'),
            layerCanvas = this.get('layerCanvas').parents('div.canvas-container');

        var isDrawShapeSelected = this.get('controller.controllers.navigation/annotation').get('isDrawShapeSelected');

        if (fabricCanvas){
            fabricCanvas.clear();
        }
        if (layerCanvas && layerPins) {
            if (annotationType === AppConstants.CommentTypes.MARKER_COMMENT || annotationType === AppConstants.CommentTypes.DRAW_COMMENT) {
              layerPins.show();

              if(!isDrawShapeSelected) {
                layerCanvas.show();
              } else {
                layerCanvas.hide();
              }
            }
        }
    },

    closeAddMarker: function(){
        var layerPins = this.get('layerPins');
        this.set("svgTemp", null);
        this._super();
        this.set('plusMarker', null);

        this.clearVideoFabricCanvas();

        if (layerPins){
            layerPins.show();
        }
    },

    clearVideoFabricCanvas: function(){
      var fabricCanvas = this.get('videoFabricCanvas');

      if (fabricCanvas){
        fabricCanvas.clear();
      }
    },

    refreshSingleAnnotationLayer: function () {
        this.syncLayer();
    }.observes('layerIsVisible', 'singleAnnotationView'),

    refreshMultipleAnnotationsLayer: function () {
        var self = this;
        var singleAnnotationView = this.get('singleAnnotationView');
        var annotations = this.get('annotations');
        if (annotations && singleAnnotationView === false) {
            this.clearMarkers();

            Ember.$("canvas.canvasvideo-layer").hide();

            var layerIsVisible = this.get('layerIsVisible');
            var showPins = this.get('controller.controllers.navigation/main.isShowPins');
            if (layerIsVisible === true && showPins) {
                this.createMarkers(annotations);
            }

            var fabricCanvas = self.get('videoFabricCanvas');
            if (fabricCanvas) {
                fabricCanvas.isDrawingMode = false;
            }
            this.syncLayer();
        }
    }.observes('layerIsVisible', 'singleAnnotationView'),


    createMarkers: function(annotations){
      var self = this;

      annotations.forEach(function (annotation) {
        annotation.set('Nr', annotation.get('AnnotationNumber'));
        self.createMarker(annotation);
      });
    },

    createMarker: function(annotationItem){
        var pinsLayer = this.get("layerPins");
        if (pinsLayer) {

          var marker = new NumberedDivIcon();
          marker.set('annotation', annotationItem);
          marker.set('width', this.get('width'));
          marker.set('height', this.get('height'));
          marker.set('pinslayer', this);
          marker.addTo(pinsLayer);

          var video = Ember.$("video.ember-view:first");
          var width = parseInt(Ember.$(video).css('width'));
          var height = parseInt(Ember.$(video).css('height'));
          var padding = parseInt(Ember.$(video).css('padding'));
          marker.syncPosition(width - 2 * padding, height - 2 * padding);

          var markers = this.get('markers');
          if (markers) {
            markers.push(marker);
            this.set('markers', markers);
          }
        }
    },

    clickMarker: function(ev){
        var pinsLayer = this.get('layerPins');
        var annotations = this.get('annotations');
        if (pinsLayer) {
            var annotation = null;
            if (annotations){
                var annotationId = Ember.$(ev.currentTarget).attr('annotationId');
                annotation = annotations.filterBy('id', annotationId).get('firstObject');
            }
            if (annotation){
                this.displaySingleAnnotation(annotation);
            }
        }
    },

    syncLayer: function(){
        var video = Ember.$("video.ember-view:first");

        var width = parseInt(Ember.$(video).css('width'));
        var height = parseInt(Ember.$(video).css('height'));

        var offset = Ember.$(video).offset();
        var padding = parseInt(Ember.$(video).css('padding-top'));

        var layerPins = this.get('layerPins');
        if (layerPins) {
            layerPins.css('width', width - 2 * padding);
            layerPins.css('height', height - 2 * padding);
            layerPins.offset({left: offset.left + padding, top: offset.top + padding });
        }

        this.updateCanvasLayers();

        var markers = this.get('markers');
        if (markers){
            markers.forEach(function(marker){
                marker.syncPosition(width - 2*padding, height-2*padding);
            });
        }
        var plusMarker = this.get('plusMarker');
        if (plusMarker){
            plusMarker.syncPosition(width - 2*padding, height - 2*padding, this.newAnnotationAddingState,this.videoFabricCanvas.OriginalWidth,this.videoFabricCanvas.OriginalHeight);
        }
    },

    updateCanvasLayers: function() {
      var video = Ember.$("video.ember-view:first");

      var offset = Ember.$(video).offset();
      var padding = parseInt(Ember.$(video).css('padding-top'));
      var width = parseInt(Ember.$(video).css('width'));
      var height = parseInt(Ember.$(video).css('height'));

      var layerCanvas = Ember.$('canvas.canvasvideo-layer');
      layerCanvas.css({
        left: offset.left + padding,
        top: padding + padding,
        width: width - 2 * padding,
        height: height - 2 * padding
      });
      layerCanvas.offset({top: offset.top + padding, left: offset.left + padding});

      var fabricCanvas = this.get('videoFabricCanvas');
      if (fabricCanvas) {
        fabricCanvas.setHeight(height - 2 * padding);
        fabricCanvas.setWidth(width - 2 * padding);
      }
    },

    clearMarkers: function(){
        var markers = this.get('markers');
        if (markers)
        {
            while (markers.length > 0){
                markers.pop();
            }

            markers = [];
        }
        this.set('markers', []);
        Ember.$('div.pinsvideo-layer .leaflet-marker-icon').remove();
    },

    selectAnnotation: function (annotation) {
      this.get('controller').send('selectAnnotation', annotation);
    },

    onSelectedWidthChange: function() {
      this.set('selectedDrawWidth', this.get('selectedWidth') * 4);// adjust width to video scale
    }.observes('selectedWidth')
});
export default VideoAnnotationLayer;
