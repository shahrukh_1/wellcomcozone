import Ember from "ember";

var VersionsController = Ember.ArrayController.extend({
    sortProperties: ['OrderInDropDownMenu'],
    sortAscending: true
});
export default VersionsController;
