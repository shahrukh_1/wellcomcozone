import Ember from "ember";
import AppConstants from 'appkit/config/constants';
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var NavigationController = Ember.Controller.extend(UserPermissions, {
    actions: {
      homeUrl: function () {
        if (window.AppkitENV.is_external_user) {
          this.showThankYouDialogModal();
        }
        else {
          class Event {
            constructor(name, obj) {
                this.event = name;
                this.obj_ = obj;
            }
          }
          class StudioClose {
            constructor(approval_guid , user_name , is_external_user) {
                this.approval_guid = approval_guid;
                this.user_name = user_name;
                this.is_external_user = is_external_user;
                
            }
          }
        const StudioClose_data = new StudioClose(window.AppkitENV.approval_guid,  window.AppkitENV.user_name , window.Appkit.is_external_user);
        const event = new Event('closeStudio', StudioClose_data);
        window.parent.postMessage(event, '*');

          window.location = window.AppkitENV.URL;
        }
      },
    },

    showThankYouDialogModal: function () {
      var controller = this.controllerFor('confirm-dialog');
      controller.set('title', Ember.I18n.t("lblThankYouTitle"));
      controller.set('message', Ember.I18n.t("lblThankYouMessage"));
      controller.set('yes', Ember.I18n.t("btnExit"));
      controller.set('no', Ember.I18n.t("btnContinue"));
      controller.set('url', window.AppkitENV.URL);
      controller.set('mode', AppConstants.ConfirmDialogMode.ExternalUserClose);
      controller.set('visible', true);
      controller.init();

      this.send('showConfirmDialog');
    },

	  brandingImageHeader: function(){
        if (this.get('session.user.BrandingHeaderLogo')) {
            return "background-image: url('" + this.get('session.user.BrandingHeaderLogo') + "');";
        }
        return '';
	  }.property('session.user.BrandingHeaderLogo')
});
export default NavigationController;
