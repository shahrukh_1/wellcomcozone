import Ember from "ember";

var NavigationVersionController = Ember.ObjectController.extend({
    needs: ['approval'],
    approval: Ember.computed.alias('controllers.approval'),
    actions: {
        selectVersion: function(version) {
            if(!this.get('versionIsNotAccesible')) {
                version.toggleProperty('selected');
            }
        }
    },
    versionIdAttr: function() {
        return 'czn-version-' + this.get('id');
    }.property('id'),

    versionIsNotAccesible: function() {
        return false;
    }.property(),

    versionTitle: function() {
        return  this.get('Title');
    }.property('Title'),

    //versionTitleNr: function() {
    //    var versionNumber = this.get('VersionLabel');
    //    return versionNumber != '' ? " - " + versionNumber: "";
    //}.property('VersionLabel'),

    isVideo: function(){
        var approvalType = this.get('approval.ApprovalType');
        return approvalType === 'Movie'? true : false;
    }.property('controllers.approval')
});
export default NavigationVersionController;
