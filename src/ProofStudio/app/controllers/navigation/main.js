import Ember from "ember";
import NavigationIndexController from 'appkit/controllers/navigation/index';
import SortedVersions from 'appkit/controllers/navigation/sortedversions';
import AppConstants from 'appkit/config/constants';
import UserPermissions from 'appkit/routes/mixins/user-permissions';
import TilesActions from "appkit/routes/image/mixins/tiles-actions";

var NavigationMainController = NavigationIndexController.extend(UserPermissions, {
    needs: ['approval', 'sidebar/decisions', 'image/addapprovals/addapprovals', 'image/controls/image','application','image/viewer/compare', 'comments/index', 'sidebar/index', 'navigation/custom-profiles/index', 'navigation/annotation'],
    decisions: Ember.computed.alias('controllers.sidebar/decisions'),
    approval: Ember.computed.alias('controllers.approval'),
    controlsController: Ember.computed.alias('controllers.image/controls/image'),
    selectedVersions: Ember.computed.alias('controllers.image/viewer/compare.versions'),
    applicationController: Ember.computed.alias('controllers.application'),
    isOverlayMode: Ember.computed.alias('controllers.image/controls/image.isDifferenceSelected'),
	  isCompareMode: Ember.computed.alias('controllers.image/controls/image.isCompareSelected'),
    currentSelectedProfileId: Ember.computed.alias('controllers.navigation/custom-profiles/index.currentSPP'),
    isRGBColorSpace: Ember.computed.alias('controllers.navigation/custom-profiles/index.isRGBColorSpace'),
    showAllTickets: Ember.computed.alias('controllers.image/controls/image.showAllTickets'), // show/hide balloons layer
    versions: [],

    viewButtonHidden: false,
    viewButtonDisabled: true,
    compareButtonEnabled: false,
    compareButtonVisible: false,
    isSinglePage: false,
    isVideo: false,
    isZip: false,
    newHeight: 0,
    newWidth: 0,
    isdisabled: true,
    isVisible: true,
    isDownloadButtonVisible: false,
    isPrintButtonVisible: false,
    isAllowMultiplePanelsOpen: true,
    isShowPrivateAnnotation : false,
    isShowPins: true,
    isNewVersionButtonVisible: true,
	  isSwitchToInch: window.AppkitENV.IsMeasurementsInInches,
    lastSelectedId:0,
    hasDeliverAccess: window.AppkitENV.HasDeliverAccess,
    initVersions: function() {
        var self = this;
        var versions = SortedVersions.create({
            model: self.get('controllers.approval.Versions')
        });

        var approvalType = this.get('approval.ApprovalType');
        this.set('isVideo', approvalType === 'Movie' || approvalType === 'Zip');
        this.set('isZip', approvalType === 'Zip');

        this.set('versions', versions);

        window.onresize = function(/*event*/) {
          self.resizeMap();
        };
    }.observes('controllers.approval.Versions'),

    showAllHtmlFiles: function () {
       if(window.AppkitENV.selectedHtmlPageId === "0")
       {
         return 'View All';
       } else{
         var fileName = '';
         var pages = this.store.all('page').content;
         pages.forEach(function(page){
           if( page.id === window.AppkitENV.selectedHtmlPageId && fileName === '')
           {
             fileName = page.get('_data').HTMLFileName;
           }
         })
         return fileName;
       }
    }.property('model'),
    isShowAnnotationsAllPhases: function () {
      return this.get('session.user.data.ShowAnnotationsOfAllPhases') || this.get('controller.session.user.data.ShowAnnotationsOfAllPhases');
    }.property('session.user', 'session.user.data.ShowAnnotationsOfAllPhases', 'controller.session.user','controller.session.user.data.ShowAnnotationsOfAllPhases'),

    isPhase: function() {
      var currentVersion = this.get('controllers.approval.currentVersion');
      return currentVersion ? currentVersion.get("CurrentPhase") != undefined && currentVersion.get("CurrentPhase") != null : true;
    }.property('controllers.approval.currentVersion'),

    isPDM: function() {
      var pdmID = this.store.all('approval-basic').content[0].get('PDM');
      if(pdmID != null){
       if(this.store.all('user').content[0].id === pdmID.toString())
        { return true;
        } else {  return false;
          }
      } else { return false};
    }.property('model'),

    htmlPages: function(){
      var records = this.store.all('page').content;
      var pages = records.sort(function(a, b){return a._data.id - b._data.id});
      var result = [];
      pages.forEach(function(page){
        var s = {Id: page.get('id'), FileName: page.get('_data').HTMLFileName};
        result.pushObject(s);
      });
      if(pages.length != 1){
      result.pushObject({Id: 0 , FileName: 'View All'});
      }

      if(window.AppkitENV.selectedHtmlPageId != "0" || result.length === 2){
        this.set('isSinglePage', true);
        var selectedPage;
        if(result.length === 2){
          selectedPage = this.store.all('page').content;
        } else {
          selectedPage = this.store.all('page').content.filterBy('id', window.AppkitENV.selectedHtmlPageId);
        }
        this.set('newHeight', selectedPage[0].get('OriginalImageHeight'));
        this.set('newWidth', selectedPage[0].get('OriginalImageWidth'));
      }
      return result;
    }.property('model'),

    hasCompareAccessPopUp: function(){
      if(!window.AppkitENV.is_external_user &&  !(Ember.$(window).width() < AppConstants.MediumDevicesWidth)){
        return true;
      }
      return false;
    }.property('is_external_user'),
    actions: {
      heightOfHtmlPage: function(){
        var newHeight = Ember.$('.html-height-input')[0].valueAsNumber;
        if(newHeight > 10000){
        Ember.$('.html-height-input')[0].valueAsNumber = this.get('newHeight');
        } else {
        Ember.$(".zip").css('height', newHeight);
        var newWidth = Ember.$('.html-width-input')[0].valueAsNumber;
        var OriginalImageHeight = this.get('newHeight');
        var OriginalImageWidth = this.get('newWidth');
        if(OriginalImageHeight === newHeight && OriginalImageWidth === newWidth){
          this.set('isdisabled', true);
        } else {
          this.set('isdisabled', false);
        }
      }
      },
      widthOfHtmlPage: function(){
         var newWidth = Ember.$('.html-width-input')[0].valueAsNumber;
         if(newWidth > 2000){
          Ember.$('.html-width-input')[0].valueAsNumber = this.get('newWidth');
          } else {
         Ember.$(".zip").css('width', newWidth);
         var newHeight = Ember.$('.html-height-input')[0].valueAsNumber;
         var OriginalImageHeight = this.get('newHeight');
         var OriginalImageWidth = this.get('newWidth');
         if(OriginalImageHeight === newHeight && OriginalImageWidth === newWidth){
           this.set('isdisabled', true);
         } else {
           this.set('isdisabled', false);
         }
        }
      },
      saveUpdatedHtmlHeightAndWidth: function() {
        var self = this;
        var newHeight = Ember.$('.html-height-input')[0].valueAsNumber;
        var newWidth = Ember.$('.html-width-input')[0].valueAsNumber;
        var pageID = window.AppkitENV.selectedHtmlPageId;
        var approvalid = window.AppkitENV.approval_id;

        this.store.adapterFor('annotation').updateHtmlHeightWidth(newHeight, newWidth, pageID, approvalid).then(function(result){
          if(result === true){
            window.location.reload();
            // self.set('isdisabled', true);
            // self.set('newHeight', newHeight);
            // self.set('newWidth', newWidth);
          }
        });
      },
        showAnnotationDecision: function(permision){
            this.set('isVisible', permision);
        },

        showSelectedHtmlPage: function(selectedHtmlPage) {
          
          if(window.AppkitENV.IsfromProjectFolder){
           var approvalId = window.AppkitENV.approval_id;
           this.store.adapterFor('annotation').loadPageIdAsSession(selectedHtmlPage.Id, approvalId).then(function(result){
            if(result === true){
            window.location.reload();
            }
          });
          } else {
            var approvalID = 0;
          this.store.adapterFor('annotation').loadPageIdAsSession(selectedHtmlPage.Id, approvalID).then(function(result){
            if(result === true){
            window.location.reload();
            }
          });
        }
        },

        selectDecision: function(decision) {
            //if(decision.get('id') !== this.get('selectedDecision.id')) {
                var decisionsController = this.get('decisions');
                decisionsController.send('selectDecision', decision);
                decisionsController.send('saveDecision', decision);
            //}
        },
        cloneCurrentVersion: function() {
          this.resetSelectedButtons();
          var versionToBeCloned = this.get('versions').filterBy('id', this.get('controllers.approval.currentVersion.id'));
          this.cloneGivenVersion(versionToBeCloned, versionToBeCloned[0]);
        },
        addVersions: function() {
          this.resetSelectedButtons();

          this.send('showAddApprovalsDialogModal');
          var self = this;
          Ember.run.later(function(){
            if(Ember.$('.czn-approval-item.czn-profile').length == 0){
              self.get('controllers.image/addapprovals/addapprovals').send('refreshApprovals');
            }
          }, 500);
        },
        cloneSelectedVersion: function() {
            this.resetSelectedButtons();
            var selectedVersions = this.get('versions').filterBy('selected', true);

            if (!selectedVersions[0].get("ApprovalType")) {// if the version is not loaded
              Ember.$("#czn-loading-screen").modal();
              var self = this;

              var orderInMenu = selectedVersions[0].get('OrderInDropDownMenu');
              var versionLabel = selectedVersions[0].get('VersionLabel');
              var versionParent = selectedVersions[0].get('Parent');
              self.store.find('version', selectedVersions[0].get('id'), { reload: true }).then(function (version) {// load the version with all details
                Ember.$("#czn-loading-screen").modal('hide');
                version.set('OrderInDropDownMenu', orderInMenu);
                version.set('VersionLabel', versionLabel);
                version.set('Parent', versionParent);

                var versions = self.get('controllers.approval.Versions');
                versions.pushObject(version);

                self.cloneGivenVersion(selectedVersions, version);
              });
            }
            else {
              this.cloneGivenVersion(selectedVersions, selectedVersions[0]);
            }
        },
        viewVersion: function() {
          var imageControlsController = this.get('controllers.image/controls/image');
	        if (imageControlsController) {
		        imageControlsController.set('isCompareSelected', false);
		        imageControlsController.set('isDifferenceSelected', false);
                imageControlsController.set('miniMapIsUp', false);
	        }
	        window.sessionStorage.removeItem('overlay-pages');
            var currentViewerController = this.get('currentViewerController');
            if(!this.get('isVideo')){
                this.get('controlsController').send('hideSeparations');
                if (currentViewerController) {
                    currentViewerController.send('resetSeparations');
                }
            }

            this.get('controlsController').set('isDifferenceSelected', false);

            var selectedVersions = this.get('versions').filterBy('selected', true);

            if(selectedVersions.length === 1) {

                var self = this,
                    selectedVersion = selectedVersions[0];

              if (!selectedVersion.get("ApprovalType")) {

                Ember.$("#czn-loading-screen").modal();

                var orderInMenu = selectedVersion.get('OrderInDropDownMenu');

                var versionParent = selectedVersion.get('Parent');
                self.store.find('version', selectedVersion.get('id'), { reload: true }).then(function (version) {
                  Ember.$("#czn-loading-screen").modal('hide');

                  version.set('OrderInDropDownMenu', orderInMenu);

                  version.set('Parent', versionParent);

                  var versions = self.get('controllers.approval.Versions');
                  versions.pushObject(version);

                  self.selectGivenVersion(currentViewerController, version);
                  self.initializeSoftProofingParams();

                  self.get('viewerChangedCommunicationService').ViewerChagned(selectedVersion.get('id'));
                });
              }
              else {
                self.selectGivenVersion(currentViewerController, selectedVersion);
                self.get('viewerChangedCommunicationService').ViewerChagned(selectedVersion.get('id'));
              }
            }
            window.sessionStorage.removeItem('overlay-pages');
        },
        compareVersion: function(){
            // reset the variables for pixelCompare
            myV1Img=null;
            myV2Img=null;
            myV1Version='';
            myV2Version='';

            this.resetSelectedButtons();
            var selectedVersions = this.get('versions').filterBy('selected', true);
            this.selectGivenVersionsForCompare(selectedVersions.slice(), selectedVersions);
        },

        resizeVersionsViews:function(){
            this.resizeMap();
        },

        saveBackgroundColor: function(color){
            var self = this;
            var accountSettings = this.store.createRecord('account-setting', {ProofStudioBackgroundColor: color, AccountId: 0});
            accountSettings.save().then(function(){
                  var account = self.get('session.account');
                if (account){
                    account.set('ProofStudioBackgroundColor', color);
                }
            });
        },
        newVersion: function(){
            window.open(window.AppkitENV.NewVersionUrl, "_blank");
        },

        printVersion: function(){
            var isOverlayMode = this.get('controllers.image/controls/image.isDifferenceSelected') === true,
                overlayVersions = this.get('controllers.approval.versionsForOverlay'),
                currentVersionId = isOverlayMode ? overlayVersions.filterBy('isFirstVersion', true).get('firstObject.id') : this.get('controllers.approval.currentVersion.id');
            this.send('printPopup', currentVersionId);
        },

        sendToDeliver: function(){
          var isOverlayMode = this.get('controllers.image/controls/image.isDifferenceSelected') === true,
            overlayVersions = this.get('controllers.approval.versionsForOverlay'),
            currentVersionId = isOverlayMode ? overlayVersions.filterBy('isFirstVersion', true).get('firstObject.id') : this.get('controllers.approval.currentVersion.id');
          this.send('sendToDeliverPopUp', currentVersionId);
        },

        downloadVersion: function(){
            this.downloadCurrentVersion();
        },
        allowMultiplePanelsOpen: function(){
            var option = !this.get('isAllowMultiplePanelsOpen'),
                session = this.get('session');
            if (option){
                session.set('isAnnotationsPanelOpen', true);
                session.set('isUsersPanelOpen', true);
            }
            else{
                session.set('isAnnotationsPanelOpen', false);
                session.set('isUsersPanelOpen', true);
            }
            session.set('allowMultiplePanelsOpen', option);
            this.set('isAllowMultiplePanelsOpen', option);

        },
        showPins: function() {
            var option = !this.get('isShowPins');
            this.set('isShowPins', option);
        },
        showPrivateAnnotations: function(){
          var annotations = this.get('session.selectedAnnotationsToMakePublic');
          annotations.forEach(function(annotation){
             Ember.set(annotation, 'annotationSelectedToPublic', false);
          });
          this.set('session.selectedAnnotationsToMakePublic', []);
          var option = !this.get('isShowPrivateAnnotation');
          this.set('session.showOnlyPrivateAnnotations', option);
          this.set('isShowPrivateAnnotation', option);

        },
        showAnnotationsAllPhases: function() {
            var option = !this.get('isShowAnnotationsAllPhases');
            var self = this;

            self.store.unloadAll('annotation');
            self.store.unloadAll('custom-shape');
            self.get('session').set('custom-shapes-per-page', '0');

            self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', true);

            self.store.adapterFor('annotation').updateShowAnnotationsAllPhases(option).then(function (result) {
              var versions = self.get('versions').sortBy('VersionNumber:desc');
              self.loadAnnotationsAllPhases(self, versions, option, versions.slice());
            });
        },
        switchToInch: function(){
            var option = !this.get('isSwitchToInch');
            this.set('isSwitchToInch', option);
            this.send('doneAnnotating');
        },
        toggleSidebar: function(){
            this.get('applicationController').send('toggleSidebar');
        },
        selectColorPicker: function(){
            var self = this;

            Ember.run.later(function(){
              Ember.$('#czn-marker-line').css('display','none');

                //self.send('hideComment');
                //self.send('closeAddMarker');
                self.send('initColorDensitometer');

            });
        },

        selectLine: function(){
            var self = this;
            Ember.run.later(function(){
                Ember.$('#czn-marker-line').css('display','none');
                self.send('hideComment');
                self.send('closeAddMarker');
                self.send('initAddMarker', AppConstants.CommentTypes.LINE_COMMENT, true);
            });
        },
    },
    cloneGivenVersion: function (selectedVersions, version) {// selectedVersions should be only one
      var self = this;

      var cloneVersion = Ember.Object.create(version);
      cloneVersion.set('isClone', true);

      cloneVersion.set('ApprovalType', version.get("ApprovalType"));
      cloneVersion.set('select', false);
      cloneVersion.set('VersionLabel', version.get('VersionLabel') + " - clone");
      cloneVersion.set('PageVersionLabel', "<span class='clonedlabel'><span class='versionstyle'>Clone</span>of " + version.get('PageVersionLabel') + "</span>");

      // copy all version values
      cloneVersion.set('Creator', version.get("Creator"));
      cloneVersion.set('ErrorMessage', version.get("ErrorMessage"));
      cloneVersion.set('Guid', version.get("Guid"));
      cloneVersion.set('IsDeleted', version.get("IsDeleted"));
      cloneVersion.set('Parent', version.get("Parent"));
      cloneVersion.set('Title', version.get("Title"));
      cloneVersion.set('VersionNumber', version.get("VersionNumber"));
      cloneVersion.set('IsLocked', version.get("IsLocked"));
      cloneVersion.set('AllowDownloadOriginal', version.get("AllowDownloadOriginal"));
      cloneVersion.set('Decision', version.get("Decision"));
      cloneVersion.set('MovieFilePath', version.get("MovieFilePath"));
      cloneVersion.set('VersionThumbnail', version.get("VersionThumbnail"));
      cloneVersion.set('PagesCount', version.get("PagesCount"));
      cloneVersion.set('JobStatus', version.get("JobStatus"));
      cloneVersion.set('IsRGBColorSpace', version.get("IsRGBColorSpace"));
      cloneVersion.set('IsCompleted', version.get("IsCompleted"));
      cloneVersion.set('CurrentPhase', version.get("CurrentPhase"));
      cloneVersion.set('CurrentPhaseName', version.get("CurrentPhaseName"));

      selectedVersions.pushObject(cloneVersion);

      // create clone for softproofing paramenters if it was not created before
      this.selectGivenVersionsForCompare(selectedVersions.slice(), selectedVersions);
    },

    selectGivenVersion:function(currentViewerController, selectedVersion) {
      var self = this;
      var approvalRole = self.getUserRoleKey(selectedVersion.get('id'));

      selectedVersion.set('userRole', approvalRole);
      //Annotation and decision buttons will be displayed based on approval role
      if (self.get('model.IsLocked') && ( approvalRole === AppConstants.Permissions.READ_ONLY ||
        approvalRole === AppConstants.Permissions.REVIEW)) {
        selectedVersion.set('isApprovalReviewer', false);
      }
      else {
        selectedVersion.set('isApprovalReviewer', true);
      }

      selectedVersion.set('isLocked', true);
      selectedVersion.set('isCompareMode', false);
      selectedVersion.set('paginationVisible', false);
      self.get('approval').send('changeCurrentVersion', selectedVersion);
      if (!self.get('isVideo')) {
        self.send('displayCompareControls', false);
        if (currentViewerController) {
          currentViewerController.send('clearAnnotationsLayer');
        }
      }
      self.resetVersionSelections();
    },

    selectGivenVersionsForCompare: function(versions, selectedVersions) {
      var self = this;
      if (versions.length == 0) {
        Ember.$("#czn-loading-screen").modal('hide');
        if (selectedVersions.filterBy('isClone',true).get('length') > 0) {
          var versionToBeCloned = selectedVersions.filterBy('isClone', false).get('firstObject');

          var versionToBeClonedSPP = this.get('session.softProofingParams').filter(function (ssp) {
              return ssp.versionId == versionToBeCloned.get('id') && ssp.isClone == false;
            }).get('firstObject');
          if (!versionToBeClonedSPP) {// the version was not loaded initially
              var sspOrig = this.store.all('soft-proofing-param').filter(function (spp) {
                  return spp.get('Version.id') == versionToBeCloned.get('id') && spp.get('DefaultSession') == true;
              }).get('firstObject');
            versionToBeClonedSPP = this.get('tilesActionsService').constructSPP(sspOrig, false);

            self.get('session.softProofingParams').pushObject(versionToBeClonedSPP);
          }

          // remove old cloned parameters if any
          var oldSoftProofingParamForClone = this.get('session.softProofingParams').filter(function (ssp) {
              return ssp.versionId == versionToBeCloned.get('id') && ssp.isClone == true;
          }).get('firstObject');
          if (oldSoftProofingParamForClone) {
            self.get('session.softProofingParams').removeObject(oldSoftProofingParamForClone);
          }
          var clonedSPP = this.cloneJSON(versionToBeClonedSPP);
          clonedSPP.isClone = true;
          // add soft proofing parameters for clone
          self.get('session.softProofingParams').pushObject(clonedSPP);
        }
        else {
          self.initializeSoftProofingParams(); // init soft proofing parameters for new loaded versions
        }

        if (selectedVersions.length > 1) {
          selectedVersions.forEach(function (version) {
            version.set('isCompareMode', true);
            version.set('userRole', self.getUserRoleKey(version.get('id')));
            version.set('canInitializeView', false);
            version.set('paginationVisible', false);
          });
          self.get('approval').send('displayMultipleVersions', selectedVersions);
          self.send('displayCompareControls', true);
          self.resizeMap();
        }

        self.resetVersionSelections();

        ////Hide dropdown menu
        self.set('compareButtonVisible', false);

        self.enableViewCompareButton();

        var imageControlsController = self.get('controllers.image/controls/image');
        if (imageControlsController) {
          imageControlsController.set('miniMapIsUp', false);
          imageControlsController.set('isPageSync', true);
          imageControlsController.set('isZoomSync', true);
        }
      }
      else { // check if versions should be reloaded and reload them if it is the case
        var analizedVersion = versions[0];
        if (analizedVersion.get("ApprovalType")) {// the version is already loaded
          versions.shift();// remove versions[0]
          self.selectGivenVersionsForCompare(versions, selectedVersions);
        }
        else {
          Ember.$("#czn-loading-screen").modal();

          var orderInMenu = analizedVersion.get('OrderInDropDownMenu');
          var versionLabel = analizedVersion.get('PageVersionLabel');
          var versionParent = analizedVersion.get('Parent');
          self.store.find('version', analizedVersion.get('id'), { reload: true }).then(function (version) {

            version.set('OrderInDropDownMenu', orderInMenu);
            version.set('PageVersionLabel', versionLabel);
            version.set('Parent', versionParent);

            var newVersions = self.get('controllers.approval.Versions');
            newVersions.pushObject(version);

            versions.shift();// remove versions[0]
            self.selectGivenVersionsForCompare(versions, selectedVersions);
          });
        }
      }
    },

    //load collaborators for all versions and then trigger ShowAnnotationsOfAllPhases
    loadColaboratorsAllPhases: function(self, versions, option) {
      if (versions.length == 0) { // trigger ShowAnnotationsOfAllPhases
          self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', false);
          self.get('currentViewerController').set('usersChanged', new Date());
          self.set('session.user.data.ShowAnnotationsOfAllPhases', option);
          return;
      }
      //load collaborators for all versions
      var ver = versions[0];
      self.store.find('collaborators-and-group', {approval_id:ver.get("id"), showAnnotationsOfAllPhases:option}).then(function(values) {
          versions.shift();
          self.loadColaboratorsAllPhases(self, versions, option);
      });
    },

    // Load annotations for all versions, than load collaborators for all versions and then trigger ShowAnnotationsOfAllPhases
    loadAnnotationsAllPhases: function(self, versions, option, originalVersions) {
      //var self = this;
      if (versions.length == 0) { //load collaborators for all versions and then trigger ShowAnnotationsOfAllPhases
        self.store.unloadAll('external-collaborators-link');
        self.store.unloadAll('internal-collaborators-link');
        self.loadColaboratorsAllPhases(self, originalVersions, option);
        return;
      }

      // Load annotations for all versions
      var ver = versions[0];
      self.store.find('annotation', {approvalId: ver.get("id")}).then(function () {
        versions.shift();
        self.loadAnnotationsAllPhases(self, versions, option, originalVersions);
      }).catch(function (/*e*/) {
        try {
          //if (self.get('isDestroyed')) {
          //  return false;
          //}
          self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', false);
          self.get('currentViewerController').set('usersChanged', new Date());
          self.get('currentViewerController').set('clearMarkers');
          self.set('session.user.data.ShowAnnotationsOfAllPhases', option);
        }
        catch (e) {
        }
      });
    },
    userPermissions: function(){
        return this.get('isVisible');
    }.property('isVisible'),

    selectedDecision: function() {
        return this.get('approval.currentVersion.Decision');
    }.property('approval.currentVersion.Decision'),

    enableViewCompareButton: function() {
        var isTablet = Ember.$(window).width() < AppConstants.MediumDevicesWidth;
        var selectedVersions = this.get('versions').filterBy('selected', true);
        var length = selectedVersions.length;

        if (isTablet || this.get("isVideo")) {
          // allow only one version to be selected
          if (length == 0) {
            this.set("lastSelectedId", 0);
          } else if (length == 1) {
            this.set("lastSelectedId", selectedVersions.get("firstObject").get("id"));
          } else if (length > 1) {
            var prevSel = this.get('versions').filterBy('id', this.get("lastSelectedId"));
            if (prevSel && prevSel.length > 0) {
              var prevSelected = prevSel.get("firstObject");
              prevSelected.set("selected", false);
            }

            selectedVersions = this.get('versions').filterBy('selected', true);
            var length = selectedVersions.length;
          }
        }

        if (length > 1) {
          if (isTablet) {
            this.set('compareButtonEnabled', false);
            this.set('viewButtonHidden', true);
            this.set('compareButtonVisible', false);
          } else {
            this.set('compareButtonEnabled', true);
            this.set('viewButtonHidden', true);
            this.set('compareButtonVisible', true);
          }
        }
        else if(length > 0) {
            this.set('viewButtonHidden', false);
            this.set('viewButtonDisabled', false);
            this.set('compareButtonEnabled', false);
        }
        else {
            this.set('viewButtonHidden', false);
            this.set('compareButtonEnabled', false);
            this.set('viewButtonDisabled', true);
        }
    }.observes('versions.@each.selected'),

    resetVersionSelections: function() {
        this.get('versions').filterBy('selected', true).forEach(function(version) {
            version.set('selected', false);
        });
    },

    //Recalculate width and height of each map
    resizeMap: function(){
        var isVertical = this.get('controlsController.isVertical');
        var overlayMode = this.get('controlsController.isDifferenceSelected');
        var compareMode = this.get('controlsController.isCompareMode');
        var versions = this.get('selectedVersions');
        var versionCount = versions.length;

        Ember.run.later(function(){
            var leafletContainer = Ember.$('.leaflet-container');
            var mainWidth = Ember.$('.czn-main').width();
            var mainHeight = Ember.$('.czn-main').height();
            var bottomBarHeight = Ember.$('.czn-controls-toolbar').height();

            var ulHeight = mainHeight - bottomBarHeight - 8;
            var ulWidth = mainWidth -  Ember.$('.czn-sidebar-handle').width() + 4;

            Ember.$('.czn-versions-ul').css('height', ulHeight);
            Ember.$('.czn-versions-ul').css('width', ulWidth);

            //Change the map dimensions after the view template has been rendered
            if(overlayMode){
                leafletContainer.css('width', ulWidth - 9);
                leafletContainer.css('height', ulHeight - 4);
                Ember.run.later(function(){
                    Ember.$('li.czn-versions-li').each(function(i,v){
                        Ember.$(v).css('height', ulHeight);
                        Ember.$(v).css('width', ulWidth - 5);
                    });
                });
            }
            else if(compareMode)
            {
                if(isVertical === true){
                    var liWidth = parseInt(ulWidth / versionCount);
                    leafletContainer.css('width', liWidth - 9);
                    leafletContainer.css('height', ulHeight - 4);
                    Ember.$('li.czn-versions-li').each(function(i,v){
                        Ember.$(v).css('width', liWidth - 2);
                        Ember.$(v).css('height', ulHeight);
                    });
                } else {
                    var liHeight = parseInt(ulHeight  / versionCount);
                    leafletContainer.css('width', ulWidth - 9);
                    leafletContainer.css('height', liHeight - 4);
                    Ember.run.later(function(){
                        Ember.$('li.czn-versions-li').each(function(i,v){
                            Ember.$(v).css('height', liHeight);
                            Ember.$(v).css('width', ulWidth - 5);
                        });
                    });
                }
            }
        });

    }.observes('controllers.application.sidebarShown', 'controllers.image/controls/image.isDifferenceSelected'),

    displayVersionsList: function(){
        return this.get('versions.length') > 1;
    }.property('versions'),

    resetSelectedButtons: function(){
        this.get('controlsController').set('isVertical', true);
        this.get('controlsController').set('isHorizontal', false);
        this.get('controlsController').set('active', false);
        this.get('controlsController').set('isCompareSelected', true);
        this.get('controlsController').set('isDifferenceSelected', false);
        this.get('controlsController').set('isFeaturePixelCompareSelected', false);
        this.get('controlsController').set('isPageSync', false);
        this.get('controlsController').set('isLocked', false);
    },

    monitorName: function(){
        return this.get('session.softproofingstatus.MonitorName');
    }.property('session'),

    calibrationEnabled: function(){
        return this.get('session.softproofingstatus.Enabled');
    }.property('session'),

    isOnCalibratedMonitor: function() {
      var softProofingStatus = this.get('session.softproofingstatus');
      return (softProofingStatus && softProofingStatus.get('DisplayHeight') == window.screen.height && softProofingStatus.get('DisplayWidth') == window.screen.width);

    }.property('session.softproofingstatus'),

    calibratedCssClass: function(){
        if(this.get('isOnCalibratedMonitor')) {
          var level = parseInt(this.get('session.softproofingstatus.Level'));
          if (level === AppConstants.SoftProofingLevel.Calibrated) {
            return "softproofing-calibrated";
          }
          else if (level === AppConstants.SoftProofingLevel.Expired) {
            return "softproofing-expired";
          }
          else if (level === AppConstants.SoftProofingLevel.NotCalibrated) {
            return "softproofing-not-calibrated";
          }
        }
      else {
          return "softproofing-not-calibrated";
        }
    }.property('session', 'isOnCalibratedMonitor'),

    versionChangedObservable: function(){
      var currentVersion = this.get('controllers.approval.currentVersion');

      var version = this.get('versions').sortBy('VersionNumber:desc').get('firstObject');
      var annotations = this.store.all('annotation').filter(function(annotation){
        return annotation.get('Page.Approval.id') === currentVersion.get('id') && !annotation.get('Parent');
      });

      var hasAnnotations = (annotations && annotations.get('length') > 0),
            approvalType = currentVersion ? currentVersion.get('ApprovalType') : null,
            key = this.get('session.user.UserRole.Key'),
            jobIsNotArchived = currentVersion.get('JobStatus') !== AppConstants.JobStatus.Archived,
            currentLoggedUserId = this.get('session.user.id'),
            currentUserIsCollaborator = this.userIsCollaborator(currentLoggedUserId, currentVersion.get('id')),
            showPrintButton;
            if(approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO)
            {
                var videoAnnotationsReport = this.store.all('annotation').filter(function(annotation){
                  return annotation.get('Page.Approval.id') === currentVersion.get('id') && !annotation.get('Parent') && annotation.get('ShowVideoReport');
                });
                showPrintButton = (videoAnnotationsReport && videoAnnotationsReport.get('length') > 0)
            }
        var hideAddNewVersion = false;
        var userID = this.store.all("user").content[0].id;
        if(this.store.all("internal-collaborators-link").filterBy("collaborator_id.id", userID).length > 0){
        var userDecision = this.store.all("internal-collaborators-link").filterBy("collaborator_id.id", userID)[0].get('Decision');
        var approvals = window.AppkitENV.approvals.split(',');
        var JobStatus = this.store.all('version').filterBy('id', approvals[0])[0].get('JobStatus');
        //var JobStatus = this.store.all('version').filterBy('id', window.AppkitENV.approvals)[0].get('JobStatus');
              
        if(userDecision != null && userDecision.get('Key') === 'APD' && JobStatus === 'COM'){
          hideAddNewVersion = true;
        }
      }
        this.set('isVideo', approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO || approvalType === AppConstants.ApprovalTypes.TYPE_ZIP );
        this.set('isZip', approvalType === AppConstants.ApprovalTypes.TYPE_ZIP );
        this.set('isNewVersionButtonVisible', !hideAddNewVersion && !window.AppkitENV.is_external_user && key !== AppConstants.UserRoles.CONTRIBUTOR && key !== AppConstants.UserRoles.VIEWER  && jobIsNotArchived && currentVersion.get('IsCompleted') == false);
        this.set('isPrintButtonVisible', hasAnnotations && (window.AppkitENV.ExternalSessionKey === 'False') && (!window.AppkitENV.is_external_user) && key !== AppConstants.UserRoles.VIEWER && jobIsNotArchived && ( approvalType === AppConstants.ApprovalTypes.TYPE_IMAGE || approvalType === AppConstants.ApprovalTypes.TYPE_WEBPAGE || (approvalType === AppConstants.ApprovalTypes.TYPE_VIDEO && showPrintButton)));
        this.set('isDownloadButtonVisible', currentVersion.get('AllowDownloadOriginal') && key !== AppConstants.UserRoles.VIEWER );
    }.observes('controllers.approval.currentVersion', 'currentViewerController','currentViewerController.annotationsChanged', 'controllers.approval.viewerControllers.@each.annotationsChanged', 'controllers.approval.viewerControllers.@each.select'),

    downloadCurrentVersion: function(){
        var allowDownload= this.get('controllers.approval.currentVersion.AllowDownloadOriginal');
        if (allowDownload === true){
            var url = window.AppkitENV.DownloadFileUrl + "&fileGuid=" + this.get('controllers.approval.currentVersion.Guid');
            window.open(url, 'download');
        }
    },

    isEnableSoftProofing: function(){
        var self = this,
            currentVersion = this.get('currentViewerController.currentVersion') || this.get('controllers.approval.currentVersion'),
            approvalType = currentVersion ? currentVersion.get('ApprovalType') : null,
            currentLoggedUserId = this.get('session.user.id'),
            currentUserIsCollaborator = this.userIsCollaborator(currentLoggedUserId, currentVersion.get('id'));

        return ( approvalType === AppConstants.ApprovalTypes.TYPE_IMAGE || approvalType === AppConstants.ApprovalTypes.TYPE_WEBPAGE ) && this.store.all('soft-proofing-param').filter(function(spp){
              return spp.get('Version.id') === currentVersion.get('id');
        }).get('length') > 0 && !this.get('controllers.image/controls/image.showAllTickets') && !this.get('controlsController.isDifferenceSelected') && currentUserIsCollaborator;

    }.property('currentViewerController', 'controllers.approval.viewerControllers.@each.select', 'controllers.image/controls/image.showAllTickets'),

    hasPrePressTools: function() {
      var availableFeatures = this.get('session.account.AvailableFeatures');
      return availableFeatures.any(function(feature, index){
        return (feature.get('id') == AppConstants.Features.Separations);
      });
    }.property('model'),

    hasProfilePreviewTool: function() {
      var availableFeatures = this.get('session.account.AvailableFeatures');
      return availableFeatures.any(function(feature, index){
        return (feature.get('id') == AppConstants.Features.ProfilePreview);
      });
    }.property('model'),



});

export default NavigationMainController;
