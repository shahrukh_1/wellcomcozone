import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var CustomProfilesIndex = Ember.Controller.extend(CurrentViewerMixin, {
  showSubstrateFromICCProfile: window.AppkitENV.ShowSubstrateFromICCProfile,
  OnChangeSelected: function() {
    var selectedProfile = this.get('profiles').filterBy('tmpSelected', true).get('firstObject');
    var selectedPapertint = this.get('papertints').filterBy('tmpSelected', true).get('firstObject');
    var warningMsg = { isWarningOnSelected: false, warning: "", deltaE: "", isDeltaE:false};
    if (selectedProfile && selectedPapertint && selectedPapertint.get('id') > 0)
    {
      var formatMismatch = "<ul><li>{0}</li> <li>{1}</li></ul>";
      var profile = Ember.I18n.t('lblProfile') + ": ";
      var paperTint = Ember.I18n.t('lblPaperTint') + ": ";
      var mediaCategoryWarning = "<li>" + Ember.I18n.t('lblPrintSubstrateMismatchProofStudio') + formatMismatch + "</li>";
      var measurementWarning = "<li>" + Ember.I18n.t('lblMeasurementCondMismatchProofStudio') + formatMismatch + "</li>";

      //check media category mismatch
      if (selectedProfile.get('MediaCategory') != null && selectedProfile.get('MediaCategory') != selectedPapertint.get('MediaCategory')) {
        warningMsg.warning = mediaCategoryWarning.replace("{0}",  profile + selectedProfile.get('MediaCategory')).replace("{1}",  paperTint + selectedPapertint.get('MediaCategory'));
        warningMsg.isWarningOnSelected = true;
      }

      //check measurement condition mismatch
      if(selectedProfile.get('MeasurementCondition') != null) {
        var measurementCond = Ember.A([]);
        selectedPapertint.get('PaperTintMeasurements').forEach(function (measurement) {
          measurementCond.pushObject(measurement.get('Measurement'));
        });
        if (!measurementCond.contains(selectedProfile.get('MeasurementCondition'))) {
          var measurementsW = measurementWarning.replace('{0}', profile + selectedProfile.get('MeasurementCondition')).replace('{1}',  paperTint + measurementCond.toArray().join(", "));

          warningMsg.warning = warningMsg.warning + measurementsW;
          warningMsg.isWarningOnSelected = true;
        }
      }

      //calculate DeltaE
      var profileLab = {
        L: selectedProfile.get('L'),
        A: selectedProfile.get('a'),
        B: selectedProfile.get('b')
      };

      if (selectedPapertint.get('id') > 0) {
        var measurements = selectedPapertint.get('PaperTintMeasurements');
        var matchingPaperTint = measurements.get('firstObject');

        if(measurements.get('length') > 1) {
          //try to get the one matching the profile measurement cond
          for (var i = 0; i < measurements.get('length'); i++) {
            if (measurements.objectAt(i).get('Measurement') === selectedProfile.get('MeasurementCondition')) {
              matchingPaperTint = measurements.objectAt(i);
              break;
            }
          }
        }

        if(matchingPaperTint) {
          var paperTintLab = {
            L: matchingPaperTint.get('L'),
            A: matchingPaperTint.get('a'),
            B: matchingPaperTint.get('b')
          };

          warningMsg.deltaE = DeltaE.getDeltaE00(profileLab, paperTintLab);
          warningMsg.isDeltaE = true;

          if(warningMsg.deltaE > 10) {
            warningMsg.isWarningOnSelected = true;
            warningMsg.warning = warningMsg.warning + "<li>" + Ember.I18n.t('lblDeltaEWarning') + "</li>";
          }
        }
      }
    }

    return warningMsg;

  }.property('papertints.@each.tmpSelected','profiles.@each.tmpSelected'),

  // ------ PAPERTINT -------------------------
  bkPaperTintTmpSelected: { selected:false, id:0 },

  papertints: function() {
      var papertints = Ember.A([]),
          defaultSoftProofingSession = this.get('currentSPP');

      this.store.all('soft-proofing-papertint').forEach(function(papertintFromStore) {
          var papertint = Ember.Object.create({
            id: papertintFromStore.get('id'),
            PaperTintName: papertintFromStore.get('PaperTintName'),
            MediaCategory: papertintFromStore.get('MediaCategory'),
            PaperTintMeasurements: papertintFromStore.get('PaperTintMeasurements'),
            selected: false,
            SameMediaCategory: true,
            CategoryChanged: false
          });
          papertints.pushObject(papertint);
      });

      //if no paper tint selected check no paper tint option
      var currentPaperTintId = '0';
      if(defaultSoftProofingSession && defaultSoftProofingSession.PaperTintId)
      {
        currentPaperTintId = defaultSoftProofingSession.PaperTintId;
      }

      var currentPaperTint = papertints.findBy('id', "" +  currentPaperTintId);

      if(currentPaperTint) {
        if (!this.get('bkPaperTintTmpSelected.selected')) {
          currentPaperTint.set('tmpSelected', true);
          this.set('bkPaperTintTmpSelected.id', currentPaperTintId);
          this.set('bkPaperTintTmpSelected.selected', true);
        }
        else {
          var tmpSelectedPaperTint = papertints.findBy('id', "" +  this.get('bkPaperTintTmpSelected.id'));
          if (tmpSelectedPaperTint) {
            tmpSelectedPaperTint.set('tmpSelected', true);
          }
        }

        currentPaperTint.set('selected', true);
      }

    //order paper tints based on the currentProfile MediaCategory
    var selectedProfile = this.get('tmpSelectedProfile');

    if (selectedProfile == undefined || selectedProfile == null) {
      selectedProfile = this.get('profiles').filterBy('selected', true).get('firstObject');
    }
    var noPapertint = papertints.filterBy('id','0').get('firstObject');
    if (noPapertint) {
      papertints = papertints.removeObject(noPapertint);

    }
    if(selectedProfile && selectedProfile.get('MediaCategory') != null) {
      var sortedPaperTints = papertints.toArray().sort(function(a, b) {
        if(a.MediaCategory == selectedProfile.get('MediaCategory') && b.MediaCategory != selectedProfile.get('MediaCategory')) {
          return -1;
        }
        else if (a.MediaCategory != selectedProfile.get('MediaCategory') && b.MediaCategory == selectedProfile.get('MediaCategory')) {
          return 1;
        }
        else
          return a.PaperTintName.localeCompare(b.PaperTintName);
      });
      papertints = Ember.A(sortedPaperTints);
      papertints.forEach(function(papertint) {
        if (papertint.get('MediaCategory') == selectedProfile.get('MediaCategory'))
        {
          papertint.set('SameMediaCategory', true);
        }
        else
        {
          papertint.set('SameMediaCategory', false);
        }
      });
    }
    else
    {
      papertints = papertints.sortBy('PaperTintName');
    }

    if (noPapertint) {
      papertints.unshiftObject(noPapertint);
    }

    // apply media category transit marked by CategoryChanged
    var newCategoryChanged = false;
    if (papertints.get('length') > 0) {
      papertints.objectAt(0).set('CategoryChanged', false);
      for (var i = 1; i < papertints.get('length'); i++) {
        if (!newCategoryChanged && (papertints.objectAt(i - 1).get('SameMediaCategory') != papertints.objectAt(i).get('SameMediaCategory'))) {
          newCategoryChanged = true;
          papertints.objectAt(i).set('CategoryChanged', true);
        }
        else
        {
          papertints.objectAt(i).set('CategoryChanged', false);
        }
      }
    }

    return papertints;
  }.property('currentSPP','profiles.@each.tmpSelected'),

  tmpSelectedProfile: function() {
    return this.get('profiles').filterBy('tmpSelected', true).get('firstObject');
  }.property('profiles','profiles.@each.tmpSelected'),

  // ------ END PAPERTINT -------------------------

  // ------ PROFILES --------------------------
  isRGBColorSpace: function(){
      return this.get('currentViewerController.model.IsRGBColorSpace');
  }.property('currentViewerController.model'),

//   isShowSubstrate: function(){
//     return window.AppkitENV.IsSubstrateFromICCProfileEnabled;
// },

	profiles: function(){
      var isRGBColorSpace = this.get('isRGBColorSpace'),
          currentVersionId = this.get('currentViewerController.model.id'),
          profiles = isRGBColorSpace === true ? (Ember.A([])) : this.store.all('custom-profile').filter(function(profile){
              return profile.get('VersionId') == null || profile.get('VersionId') == currentVersionId; //ignore temporary profiles
          }),

      defaultSoftProofingSession = this.get('currentSPP');

      if (defaultSoftProofingSession) {// it should be a default soft proofing session
        profiles.forEach(function (profile) {
          profile.set('selected', profile.get('DisplayName') === defaultSoftProofingSession.OutputProfileName);
        });
      } else {
        profiles.forEach(function (profile) {
          profile.set('selected', false);
        });
      }

      this.resetProfilesTmpSelection(profiles);

      return profiles.sortBy('DisplayName');
	}.property('currentSPP'),

	currentSPP: function(){
    var softProofingGuid = this.get('currentViewerController.renderer.softProofingGuidChanged');
    if (softProofingGuid == undefined && softProofingGuid == null) {
      return {};
    }

      var viewerId = this.get('currentViewerController.model.id'),
          viewerIsClone = this.get('currentViewerController.model.isClone'),

          softProofingParam = this.get('session.softProofingParams').filter(function(spp){
              return spp.versionId === viewerId && spp.isClone == viewerIsClone;
          }).get('firstObject');

    return softProofingParam || {};

	}.property('currentViewerController.renderer.softProofingGuidChanged',
        'currentViewerController.model.id','currentViewerController.model.isClone', 'session.softProofingParams'),

  currentRGBProfileName: function(){
      var currentSPP = this.get('currentSPP'),
        isRGBColorSpace = this.get('isRGBColorSpace');
      if (currentSPP && isRGBColorSpace)
      {
        return currentSPP.OutputRgbProfileName;
      }
      else
        return "";

      //return profile ? this.trimFileName(profile.DisplayProfileName) : null;
	}.property('currentSPP','isRGBColorSpace'),

	//trimFileName: function(filename){
	//	  return filename.replace(/.icc/g, '').replace(/_/g, ' ');
	//},
  // ------- END PROFILES --------------------
	actions: {
      selectPapertint: function(selectedPapertint){
        this.set('bkPaperTintTmpSelected.id', selectedPapertint.get('id'));
        this.set('bkPaperTintTmpSelected.selected', true);

        this.get('papertints').forEach(function(papertint){
          papertint.set('tmpSelected', papertint.get('id') === selectedPapertint.get('id'));
        });

      },

      selectProfile: function(profile){
          this.get('profiles').forEach(function(localeProfile){
              var selected = localeProfile.get('id') === profile.get('id');
              localeProfile.set('tmpSelected', selected);
          });
      },

      changeProfile: function() {
        this.loadSelectedFromTmpSelected();
          var selectedItems = [],
              itemId = this.get('currentViewerController.model.id'), // versionId
              isClone = this.get('currentViewerController.model.isClone'), // if is clone
              selectedProfile = this.get('profiles').filterBy('selected', true).get('firstObject'),
              selectedPaperTint = this.get('papertints').filterBy('selected', true).get('firstObject');


          selectedItems.pushObject(
            {
              versionId: itemId,
              isClone: isClone,

              profile: selectedProfile,
              paperTint: selectedPaperTint
            });
          this.send('changeCustomProfiles', selectedItems);
      },
      resetTmpSelected: function () {
        this.resetProfilesAndPaperTint();
      }
	},

  resetProfilesAndPaperTint: function() {
    this.resetProfilesTmpSelection(this.get('profiles'));
    this.resetPaperTintTmpSelection(this.get('papertints'));
  },

  loadSelectedFromTmpSelected: function () {
    this.get('profiles').forEach(function(profile) {
      profile.set('selected', profile.get('tmpSelected'));
    });
    this.get('papertints').forEach(function(papertint) {
      papertint.set('selected', papertint.get('tmpSelected'));
    });
  },

  resetProfilesTmpSelection: function(profiles) {
    profiles.forEach(function(profile) {
      profile.set('tmpSelected', profile.get('selected'));
    });
  },
  resetPaperTintTmpSelection: function(papertints) {
    papertints.forEach(function(papertint) {
      papertint.set('tmpSelected', papertint.get('selected'));
    });
  }

});
export default CustomProfilesIndex;
