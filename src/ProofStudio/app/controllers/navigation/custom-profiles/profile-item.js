import Ember from "ember";

var CustomProfilesItem = Ember.Controller.extend({
    isSelected: function(){
        return this.get('model.tmpSelected');
    }.property('model.tmpSelected'),

    title: function(){
      var embedded = this.get('model.IsEmbedded') === true && this.get('model.defaultProfile') === true ? Ember.I18n.t("lblEmbedded") : "";
      return this.get('model.DisplayName').replace(/.icc/g, '').replace(/_/g, ' ') + embedded;
    }.property('model')
});
export default CustomProfilesItem;
