import Ember from "ember";

var CustomProfilesItem = Ember.Controller.extend({
    isSelected: function(){
        return this.get('model.tmpSelected');
    }.property('model.tmpSelected'),

    isCategoryChanged: function(){
      return this.get('model.CategoryChanged');
    }.property('model.CategoryChanged'),

    title: function(){
        return this.get('model.PaperTintName');
    }.property('model')
});
export default CustomProfilesItem;
