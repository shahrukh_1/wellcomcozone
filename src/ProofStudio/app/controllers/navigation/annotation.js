import Ember from "ember";
import NavigationIndexController from 'appkit/controllers/navigation/index';
import AppConstants from "appkit/config/constants";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var NavigationAnnotationController = NavigationIndexController.extend(CurrentViewerMixin, {
    needs: ['image/controls/image','comments/index'],
    isOverlayMode: Ember.computed.alias('controllers.image/controls/image.isDifferenceSelected'),
    isSideAnnotation: Ember.computed.alias('controllers.comments/index.isSideAnnotation'),
    isDrawSelected: false,
    isSoftProofingSelected: false,
    isDrawLineSelected: false,
    isDrawShapeSelected: false,
    isMoveShapeSelected: false,
    isMarkerSelected: false,
    isTextSelected: false,
    isDrawRectangleSelected: false,
    shape: null,
    color:'',
    drawColor: AppConstants.ColorsHEX.Red,
    selectedWidth: 2,
    actions: {
        changeDrawingColor:function(newHexColor) {
          this.set('drawColor', newHexColor);
        },
        doneAnnotating: function() {
            this.resetAllSelectedAnnotations();
            //this.hidePinsLayer();
            return true;
        },
        reloadSelected: function() {
          var self = this;
          if (this.get("isDrawSelected") == true) {

            var drawType = "draw";

            if (this.get("isDrawShapeSelected") == true) {
              drawType = "shape";
            } else if (this.get("isMoveShapeSelected") == true) {
              drawType = "move";
            }

            Ember.run.later(function(){
              Ember.$('#czn-marker-line').css('display','none');
              self.send('hideComment');

              self.set('isDrawLineSelected', false);
              self.set('isDrawShapeSelected', false);
              self.set('isMoveShapeSelected', false);
              self.set('isMarkerSelected',false);
              self.set('isTextSelected', false);
              self.set('isSoftProofingSelected', false);
              self.send('closeAddMarker');
              self.send('clearCanvas');
            });
            this.get('currentViewerController').send('enableTextHighlighting', false);

            if (drawType == "draw") {
              Ember.run.later(function () {
                self.send('selectDrawPencil');
              });
            }

            if (drawType == "shape") {
              Ember.run.later(function(){
                self.send('selectDrawShape');
              });
            }
            else if (drawType == "move") {
              Ember.run.later(function(){
                self.send('selectAndMoveDraw');
              });
            }
          } else {
            if (self.get('isTextSelected') == true) {
              self.send('selectTextHighlighting');
            }
          }
        },

        selectAnnotationDraw: function() {
            this.pauseVideo();

            var self = this;
            this.set('isDrawSelected', true);

            Ember.run.later(function(){
            Ember.$('#czn-marker-line').css('display','none');
            self.send('hideComment');

            self.set('isDrawLineSelected', false);
            self.set('isDrawShapeSelected', false);
            self.set('isMoveShapeSelected', false);
            self.set('isMarkerSelected',false);
            self.set('isTextSelected', false);
            self.set('isSoftProofingSelected', false);
            self.send('closeAddMarker');
            self.send('clearCanvas');
            });
            this.get('currentViewerController').send('enableTextHighlighting', false);
            Ember.run.later(function(){
                self.send('selectDrawPencil');
            });

        },

        selectAnnotationSoftProofing: function(){
            this.set('isSoftProofingSelected', true);
            this.set('isDrawSelected', false);
            this.set('isDrawLineSelected', false);
            this.set('isDrawShapeSelected', false);
            this.set('isMarkerSelected',false);
            this.set('isTextSelected', false);

            var self = this;
            Ember.run.later(function(){
                Ember.$("input[value='soft-proofing'][name='annotation-type']").parents('.btn').removeClass('active');
                self.send('selectColorPicker');
            });
        },

      selectDrawRectangle: function() {
        this.set('isDrawShapeSelected', false);
        this.set('isDrawLineSelected', false);
        this.set('isMoveShapeSelected', false);

        this.set('isDrawRectangleSelected', true);
      },

        selectDrawPencil: function() {
            var wasDrawing = this.get('isAlreadyDrawing') == false;
            var wasRectangleMarque = this.get('isDrawRectangleSelected') == true;
            this.set('isDrawRectangleSelected', false);
            this.set('isDrawShapeSelected', false);
            this.set('isDrawLineSelected', true);
            this.set('isMoveShapeSelected', false);
            if (wasDrawing && !wasRectangleMarque) {
              this.startDrawMode();
            }
        },

        selectDrawShape: function() {
          var wasDrawing = this.get('isAlreadyDrawing') == false;
          this.set('isDrawRectangleSelected', false);
          this.set('isDrawLineSelected', false);
          this.set('isDrawShapeSelected', true);
          this.set('isMoveShapeSelected', false);
          //if (wasDrawing) {
          //  alert('x');
          //  //this.startDrawMode();
          //}
        },

        selectAndMoveDraw: function(){
          this.set('isDrawRectangleSelected', false);
          this.set('isMoveShapeSelected', true);
          this.set('isDrawShapeSelected', false);
          this.set('isDrawLineSelected', false);
        },

        showSpecificButtons:function(isCustomShape){
            if(isCustomShape){
                this.set('isDrawShapeSelected', true);
                this.set('isDrawLineSelected', false);
            }
            else{
                this.set('isDrawLineSelected', true);
                this.set('isDrawShapeSelected', false);
            }
            this.set('isMoveShapeSelected', true);
        },
        selectMarkerAnnotation: function(){
            this.set('isSideAnnotation',false);
            this.pauseVideo();

            this.send('closeAddMarker');
            this.send('initAddMarkerInDrawerActions', AppConstants.CommentTypes.MARKER_COMMENT);
            this.resetAllSelectedAnnotations();

            this.set('isMarkerSelected', true);

            this.get('currentViewerController').send('enableTextHighlighting', false);
        },

        selectTextHighlighting: function(){
            this.resetAllSelectedAnnotations();

            this.get('currentViewerController').send('enableTextHighlighting', true);
            this.send('closeAddMarker');
            this.send('initAddMarker', AppConstants.CommentTypes.TEXT_COMMENT);
            this.set('isTextSelected', true);
        },

        closeAddMarker: function(){
            this.set('isMarkerSelected', false);
            this.set('isTextSelected', false);
            //TODO temporary fixed - remove active icon from text highlighting icon
            Ember.$.each(Ember.$("input[name='annotation-type']"), function(i,v){
                Ember.$(v).parents('span.active.navbar-btn.btn.btn-default').removeClass('active');
            });
            return true;
        },

        selectShape: function(shape){
            this.set('shape', shape);
        },

        increaseLineWidth: function(){
            var currentValue = parseInt(Ember.$("#lineWidthSelector").val());
            if (!isNaN(currentValue) && currentValue < 40){
                currentValue += 1;
            }
            Ember.$("#lineWidthSelector").val(currentValue);
            this.updateLineWithAndColor();
        },

        decreaseLineWidth: function(){
            var currentValue = parseInt(Ember.$("#lineWidthSelector").val());
            if (!isNaN(currentValue) && currentValue > 1){
                currentValue -= 1;
            }
            Ember.$("#lineWidthSelector").val(currentValue);
            this.updateLineWithAndColor();
        },
        textHighlighting: function(enable){
            this.set('textAvailable', enable);
        },
        updateLineWithAndColorAction: function(){
            this.updateLineWithAndColor();
        }
    },

  startDrawMode: function() {
    this.set('isSoftProofingSelected', false);

    this.updateLineWithAndColor();
    this.send('clearSelection');
    this.get('currentViewerController').send('enableTextHighlighting', false);
    this.send('initAddMarkerInDrawerActions', AppConstants.CommentTypes.DRAW_COMMENT);

  },

    pauseVideo: function () {
      this.controllerFor("application").send('playPauseVideo', "pause");
    },

    updateLineWithAndColor: function(){
        var self = this;
        Ember.run.later(function(){
            var lineWidth = Ember.$("#lineWidthSelector").val();
            self.set('selectedWidth', lineWidth * 0.25);

            if (self.get('isDrawLineSelected') == true) {
              self.send('drawingModeInDrawerActions', true, lineWidth, self.get('drawColor'));
            }
        });
    },

    selectedShape: function(){
        //change selected shape
        return this.get('shape');
    }.property('shape'),

    isZip: function(){
      return !(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP)
    }.property('model'),

    shapes: function(){
        var items = [],
            self = this;

        //load available shapes
        var shapes = this.get('session.shapes');
        if(shapes){
            shapes.forEach(function(shape){
                //set selected shape
                if(self.get('shape') === null){
                    self.set('shape', shape);
                }
                items.push(shape);
            });
        }
        return items;
    }.property('session.shapes'),

    markerStyleClass: function(){
        //make marker button selected/unselected
        if(this.get('isMarkerSelected'))
        {
            return "navbar-btn btn btn-default active";
        }
        else
        {
            return "navbar-btn btn btn-default";
        }
    }.property('isMarkerSelected'),

    textStyleClass: function(){
        //make text button selected/unselected
        if(this.get('isTextSelected'))
        {
            return "navbar-btn btn btn-default active";
        }
        else
        {
            return "navbar-btn btn btn-default";
        }
    }.property('isTextSelected'),

    drawStyleClass: function(){
        //make draw button selected/unselected
        if(this.get('isDrawSelected') === true)
        {
            this.send('clearCanvas');
            return "navbar-btn btn btn-default active";
        }
        else
        {
            return "navbar-btn btn btn-default";
        }
    }.property('isDrawSelected'),

    penWidth: function(){
        return this.get('session.account.ProofStudioPenWidth');
    }.property('session.account.ProofStudioPenWidth'),

    showAnnotationButtons: function(){
        return this.get('isDrawSelected') || this.get('isDrawLineSelected') || this.get('isDrawShapeSelected') || this.get('isMarkerSelected') || this.get('isTextSelected');
    }.property('isDrawSelected', 'isDrawLineSelected', 'isDrawShapeSelected', 'isMarkerSelected', 'isTextSelected'),

    isAlreadyDrawing: function(){
      return this.get('isDrawLineSelected') || this.get('isDrawShapeSelected') || this.get('isMarkerSelected') || this.get('isTextSelected') || this.get('isMoveShapeSelected');
    }.property('isDrawLineSelected', 'isDrawShapeSelected', 'isMarkerSelected', 'isTextSelected', 'isMoveShapeSelected'),

    isDrawWidthNeeded: function() {
      return this.get('isDrawLineSelected') || this.get('isDrawRectangleSelected');
    }.property('isDrawLineSelected', 'isDrawRectangleSelected'),

    // hidePinsLayer() {
    //   if(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP){
    //     Ember.run.later(function(){
    //     Ember.$(".pinsvideo-layer").css('display','none');
    //     });
    // }
    // },
    resetAllSelectedAnnotations() {
      this.set('isDrawLineSelected', false);
      this.set('isDrawShapeSelected', false);
      this.set('isTextSelected', false);
      this.set('isMarkerSelected', false);
      this.set('isMoveShapeSelected', false);
      this.set('isDrawSelected', false);
      this.set('isSoftProofingSelected', false);
      this.set('isDrawRectangleSelected', false);
    }
});
export default NavigationAnnotationController;
