import Ember from "ember";
import TilesActions from "appkit/routes/image/mixins/tiles-actions";
import CurrentViewer from "appkit/routes/mixins/current-viewer";
import AppConstants from 'appkit/config/constants';

var NotificationsController = Ember.Controller.extend(TilesActions, CurrentViewer, {
    notifications: function(){
        return this.get('model.notifications');
    }.property('model.notifications'),

    currentPageId: function(){
        return this.get('model.currentPage.id');
    }.property('model.currentPage'),

    actions: {
        addNotification: function(msg){
            msg.inProgress = true;
            //msg.inProgressPartial = true;// used for loading partial tiles
            this.get('model').set('notifications', [msg]);

	        var self = this;
	        Ember.run.later(this, function(){
		        self.timerCheckIfFailed();// checks if messages failed and have to be closes
	        }, 50000);
            //this.timerCheck();
        },
        clearNotifications: function(){
          this.get('model').set('notifications', []);

          //cancel timer
          //if (this.get('timer')){
          //    Ember.run.cancel(this.get('timer'));
          //}
          if (this.get('timerIfFailed')){
            Ember.run.cancel(this.get('timerIfFailed'));
          }
        }
    },

    onInit: function() {
      this.get('instantNotificationsCommunicationService').RegisterOnShowNewTilesInstantNotification(this,'hideLoadingNotification');
    }.on('init'),

    onCleanup: function() {
      this.get('instantNotificationsCommunicationService').UnRegisterOnShowNewTilesInstantNotification(this,'hideLoadingNotification');
    }.on('willDestroyElement'),

    hideLoadingNotification: function() {
      this.send('clearNotifications');
    },

    //timerCheck: function(){
    //    if (this.get('timer')){
    //        Ember.run.cancel(this.get('timer'));
    //    }
	 //   var notifications = (this.get('notifications') || []).filter(function(notification){
		//    return notification.get('inProgress') === true && !notification.get('inError');
	 //   });
	 //   if (notifications && notifications.get('length') > 0) {
		//    var self = this,
		//        timer = Ember.run.later(self, function () {
		//		    //self.timerCheckCallback();
		//		    self.timerCheck();
		//        }, 2000);
		//    this.set('timer', timer);
	 //   }
    //},

    //timerCheckCallback: function(){
    //
	 //   if (this.get('isDestroyed') === true){
    //        return;
    //    }
    //
	 //   var notifications = (this.get('notifications') || []).filter(function(notification){
		//    return notification.get('inProgress') === true && !notification.get('inError');
	 //   });
	    //if (notifications && notifications.get('length') > 0) {

		    //var self = this,
			 //   activePages = this.store.all('page').filterBy('active', true);

		    //notifications.forEach(function (item) {
			 //   if (activePages.filterBy('id', item.get('pageId')).get('length') > 0) {
				//    if (item.get('inProgress')) {
             // if (item.get('inProgressPartial')) {
             //   self.checkIfTilesExist(item.tilesUrl + AppConstants.PartialTileUrl).done(function () {
             //     item.set('inProgressPartial', false);
             //     if (item.refresh) {
             //       item.refresh();
             //     }
             //     self.send('tilesReady');
             //   });
             // }
				//	    self.checkIfTilesExist(item.tilesUrl + AppConstants.SmallestTileUrl).done(function () {
				//		    item.set('inProgress', false);
				//		    self.get('notifications').removeObject(item);
				//		    if (item.refresh) {
				//			    item.refresh();
				//		    }
             //   self.send('tilesReady');
				//	    });
				//    }
				//    else {
				//       item.set('inProgress', false);
				//       self.get('notifications').removeObject(item);
				//       if (item.refresh) {
				//	       item.refresh();
				//       }
				//    }
			 //   }
		    //});
	    //}
    //},

	timerCheckIfFailed: function(){
		if (this.get('timerIfFailed')){
			Ember.run.cancel(this.get('timerIfFailed'));
		}
		var notifications = (this.get('notifications') || []).filter(function(notification){
			return notification.get('inProgress') === true && !notification.get('inError');
		});
		if (notifications && notifications.get('length') > 0) {
			var self = this,
			    timer = Ember.run.later(self, function () {
				    self.timerCheckIfFailedCallback();
				    self.timerCheckIfFailed();
			    }, 10000);
			this.set('timerIfFailed', timer);
		}
	},

	timerCheckIfFailedCallback: function(){

		if (this.get('isDestroyed') === true){
			return;
		}

		var notifications = (this.get('notifications') || []).filter(function(notification){
			return notification.get('inProgress') === true && !notification.get('inError');
		});

    if (notifications.length > 0 ) {
      notifications.forEach(function (item) {
				if (item.get('inProgress') && !item.get('inError')) {
          if (item.get('SoftProofingSessionGUID') && item.get('pageId')) { // it is message from soft proofing change

            //var activePages = this.store.all('page').filterBy('active', true),
              //&& activePages.filterBy('id', item.get('pageId')).get('length') > 0) {
            var  url = window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/soft-proofing-has-failed',
              hash = {
                type: "GET",
                data: {
                  //key: window.AppkitENV.key,
                  //user_key: window.AppkitENV.user_key,
                  //is_external_user: window.AppkitENV.is_external_user,

                  pageId: item.get('pageId'),
                  SoftProofingSessionGUID: item.get('SoftProofingSessionGUID')
                },
                url: url,
                cache: false
              };

            //hash.data.pageId = item.get('pageId');
            //hash.data.SoftProofingSessionGUID = item.get('SoftProofingSessionGUID');

            Ember.$.ajax(hash).then(function(result){
              // if soft proofing failed
              item.set('inError', result);
              if (result === true) {
                item.set('inProgress', false);

                Ember.$('.dropdown-profiles').show();
                Ember.$('#soft-proofing-menu').show();
              }
            });
          }
        }
			});
    }
		else {
			//this.set('timerIfFailed', null);
			Ember.run.cancel(this.get('timerIfFailed'));
		}
	}
});
export default NotificationsController;
