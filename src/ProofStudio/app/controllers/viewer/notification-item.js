import Ember from "ember";

var NotificationItemController = Ember.ObjectController.extend({
    isVisible: function(){
        return this.get('model.pageId') === this.get("parentController.parentController.model.currentPage.id");
    }.property('model')
});
export default NotificationItemController;
