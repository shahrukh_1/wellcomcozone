import Ember from "ember";

var CanvasView = Ember.Mixin.create({
  drawingLineChanged:function(fabricCanvas) {
    if (fabricCanvas) {
      var isDrawLine = this.get('controller.controllers.navigation/annotation.isDrawLineSelected') && !this.get('controller.controllers.navigation/annotation.isMoveShapeSelected')
                                        && !this.get('controller.controllers.navigation/annotation.isDrawRectangleSelected');

      fabricCanvas.isDrawingMode = isDrawLine;
      this.set('freeDrawingMode', isDrawLine);
    }
  },

  drawWidthChanged: function(fabricCanvas, width) {
    if (!fabricCanvas) {
      return;
    }

    fabricCanvas.freeDrawingBrush.width = width;
  },

  drawColorChanged: function(fabricCanvas) {
    var drawColor = this.get('controller.controllers.navigation/annotation.drawColor');
    if(fabricCanvas) {
      fabricCanvas.freeDrawingBrush.color = drawColor;
      this.updateSelectedObjectColor(fabricCanvas, drawColor);
    }
  },

  updateSelectedObjectColor: function(canvas, drawColor){
    var selectedObject = canvas.getActiveObject();
    if (selectedObject){
      if (!selectedObject.isCustomShape){
        selectedObject.setStroke(drawColor);
      }
      else{
        selectedObject.setFill(drawColor);
      }
      canvas.renderAll();
    }
  },
});

export default CanvasView;
