import Ember from "ember";

var AnnotationLayer = Ember.Controller.extend({
    needs: ['navigation/annotation'],
    navigationAnnotation: Ember.computed.alias('controllers.navigation/annotation'),
    singleAnnotationView: function() {
        return this.get('selectedAnnotation') ? true : false;
    }.property('selectedAnnotation'),

    selectedAnnotation: function(){
        return (this.get('model.annotations') || []).filterBy('selected', true).get('firstObject');
    }.property('model.annotations.@each.selected'),

    showLayer: function () {
       this.set('layerIsVisible', true);
    },

    hideLayer: function () {
        this.set('layerIsVisible', false);
    },

    displayMultipleAnnotations: function (annotations) {
        if (annotations.length > 0) {
            annotations.forEach(function (annotation) {
                annotation.set('isDisplayed', false);
            });
        }

        this.set('annotations', annotations);
        this.set('singleAnnotationView', false);
        this.refreshMultipleAnnotationsLayer();
    },

    displaySingleAnnotation: function (annotation) {
        if (this.get("controller.model.isOverlayMode") != true && this.get('controller.model.isCompareMode') === true && this.get('controller.model.select') === false){
            // if user clicks on a marker from an unselected version then call the displaySingleAnnotation again after 500ms until it is selected
            var self = this;
            Ember.run.later(function() {
                self.displaySingleAnnotation(annotation);
            },500);
        }
        else {
            annotation.set('isDisplayed', true);
            this.set('singleAnnotationView', true);
            this.set('annotation', annotation);
            this.selectAnnotation(annotation);
            annotation.set('isDisplayed', false);
            this.set('newAnnotationAddingState', false);
        }
    },



    initAddMarker: function (/*annotationType*/) {
        this.set('annotation', null);
        this.set('singleAnnotationView', true);
        this.set('newAnnotationAddingState', true);
    },

    closeAddMarker: function () {
        this.set('newAnnotationAddingState', false);
        this.set('singleAnnotationView', false);
    },

    hideCanvas: function(){

    },

    refreshSingleAnnotationLayer: function () {
        throw new Error("refreshSingleAnnotationLayer not implemented!");
    },


    clearCanvas: function(){
      var canvas = this.get('image-canvas-view');
      if (canvas){
        canvas.getContext().clearRect(0,0,canvas.getWidth(), canvas.getHeight());
        canvas.clear().renderAll();
      }
      this.set('startX', null);
      this.set('startY', null);
  },

  setObjectStyle: function(selectedObject, lineColor, lineWidth, layerCanvas) {
    if(lineColor) {
      if (selectedObject.isCustomShape) {
        selectedObject.setFill(lineColor);
      }
      else {
        selectedObject.setStroke(lineColor);
      }
      selectedObject.strokeDashArray = [];
      layerCanvas.set('shapeColor', lineColor);
    }
    if (lineWidth) {
      // set stroke color and width only for path that were created with free drawing
      if (!selectedObject.isCustomShape) {
        selectedObject.setStrokeWidth(lineWidth);
        selectedObject.setStroke(lineColor);
        layerCanvas.set('lineWeight', lineWidth);
      }
    }
  }
});
export default AnnotationLayer;
