import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var ViewerController = Ember.ObjectController.extend(CurrentViewerMixin, {
    needs: ['approval', 'comments/index', 'navigation/main', 'sidebar/index'],
    hasPager: Ember.computed.alias('controllers.approval.hasPager'),
    comment: Ember.computed.alias('controllers.comments/index'),
    versionIsChanged: false,

    actions:{
        initializeViewer: function(){
            this.set('versionIsChanged', !this.get('versionIsChanged'));
        },
        initVersion: function(model) {
            model.set('currentPage', null);
            model.setCurrentPageActive();
        }
    },

    showMeasurementsInInches: function(){
        return this.get('controllers.navigation/main.isSwitchToInch');
    }.property('controllers.navigation/main.isSwitchToInch'),

    annotations: function() {
        var currentPageId = this.get('currentPage.id');
        var annotations = this.get('parentAnnotations').filter(function(annotation){
            return annotation.get('Page.id') === currentPageId &&
                !annotation.get('isDeleted') &&
                !annotation.get('isDestroyed') &&
                (annotation.get('Creator.selected') === true || annotation.get('ExternalCreator.selected') === true);
        });
        return annotations;
    }.property('versionIsChanged', 'annotationsChanged', 'currentPage', 'parentAnnotations', 'parentAnnotations.@each.isDeleted', 'parentAnnotations.@each.isDestroyed', 'controllers.sidebar/index.users.@each.selected'),

    setCurrentPageActive: function() {
        var self = this;
        this.store.all('page').forEach(function(page) {
            if (page.get('Approval.id') === self.get('model.id')) {
                page.set('active', false);
            }
        });

        var currentPage = this.get('currentPage');
        if (currentPage) {
            currentPage.set('active', true);
        }
    },

    shapesObserves: function(){
        var pageId = this.get('currentPage.id'),
		        self = this,
		        visitedPages = (this.get('session.custom-shapes-per-page') || '0').split(',');

        var found = (Ember.$.inArray(pageId, visitedPages) > -1);

        if (pageId && !found) {
            this.store.find('custom-shape', {pageId: pageId}).then(function () {
                visitedPages.push(pageId);
                self.get('session').set('custom-shapes-per-page', visitedPages.join(','));
            }).catch(function(){
                // no annotations found
                visitedPages.push(pageId);
                self.get('session').set('custom-shapes-per-page', visitedPages.join(','));
            });
        }
    }.observes('currentPage.id','session.user.data.ShowAnnotationsOfAllPhases'),

    deselectAnnotations: function(annotation) {
        if(annotation) {
            annotation.set('selected', false);
        }
        else {
            this.store.all('annotation').filterBy('selected', true).forEach(function(localAnnotation){
                localAnnotation.set('selected', false);
            });
        }
    },

    deselectAnnotationObservable: function(){
        this.deselectAnnotations();
        this.get('comment').send('hide');
    }.observes('currentPage'),

    updateTotalPagesNumber: function(){
        var self = this;
        this.set('Pages', this.store.all('page').filter(function(page){
            return page.get('Approval.id') === self.get('model.id');
        }));
        this.set('totalPages', this.get('Pages.length') || 0);
    }.observes('controllers.approval.pagesChanged', 'model', 'currentViewerController')
});
export default ViewerController;
