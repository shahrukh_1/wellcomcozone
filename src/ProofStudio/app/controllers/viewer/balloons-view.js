import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var BalloonsViewController = Ember.ObjectController.extend(CurrentViewerMixin, {
    needs: ['video/viewer/video', 'comments/index', 'sidebar/index', 'image/controls/image', 'zip/viewer/zip'],
    actions: {
        initializeBalloonsView: function (view) {
            this.send('initializeBalloons', view, this);
        }
    },
    annotations: function(){
        return this.get('annotationsWithComments').filter(function(annotation){
            return annotation.get('Creator.selected') === true || annotation.get('ExternalCreator.selected') === true;
        });
    }.property('parentAnnotations', 'currentViewerController', 'currentViewerController.currentPage', 'currentViewerController.annotationsChanged', 'controllers.image/controls/image.showAllTickets', 'controllers.sidebar/index.users.@each.selected'),

    currentSelectedPage: function(){
        return this.get('model.model.currentPage') || this.get('model.model.Pages.firstObject');
    }.property('model.model.currentPage.id')
});
export default BalloonsViewController;
