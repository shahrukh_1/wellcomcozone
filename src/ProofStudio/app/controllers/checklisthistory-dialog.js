import Ember from "ember";

var ConfirmationModalController = Ember.ObjectController.extend({
  needs: ['comments/index'],
  title: "CHECKLIST ITEMS HISTORY",
  no: "Cancel",
  totalChecklistItems: [],
    
  actions: {
    cancel: function () {
      this.send('closeConfirmDialog');
    }
  },

});
export default ConfirmationModalController;
