import Ember from "ember";
import AppConstants from 'appkit/config/constants';
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var SidebarAnnotationController = Ember.ObjectController.extend(UserPermissions, {
   //annotationsSelectedToMakePublic: [],
    actions: {
        changeStatus: function(annotation, status) {
            var self = this;
            this.store.adapterFor('annotation').updateAnnotationStatus(annotation.get('id'), status.get('id')).then(function(result){
                annotation.set('Status', status);
                annotation.set('Parent', null);
                annotation.set('ModifiedDate', result.annotation.ModifiedDate);
                if(result.DisplayChangesCompletePopup) {
                  self.send('showChangesCompleteModal', result.annotation.Page);
                }
            });
        },

          selectMultipleAnnotations: function(annotation){
            annotation.toggleProperty('annotationSelectedToPublic');
            var annotationsSelectedToMakePublic = this.get('session.selectedAnnotationsToMakePublic');
            
               if(!annotationsSelectedToMakePublic.contains(annotation))
               {
                 annotationsSelectedToMakePublic.pushObject(annotation);
               } else{
                 annotationsSelectedToMakePublic.removeObject(annotation);
               }
               this.set('session.selectedAnnotationsToMakePublic', []);
               this.set('session.selectedAnnotationsToMakePublic', annotationsSelectedToMakePublic);
          }
    },

    annotationTypeClass: function() {
        var annotationType = this.get('CommentType');
        if(AppConstants.Annotation.AnnotationType.hasOwnProperty(annotationType)) {
            return AppConstants.Annotation.AnnotationType[annotationType].icon;
        }
        return '';
    }.property('CommentType'),

    annotationRepliesCount: function() {
      var repliesCount = 0;
      var annotationID = this.get('model').id;
      var annotatioNumber = this.store.all('annotation').forEach(function(item, index, annotations) {
        var parent = item._data.Parent;

        if(parent) {
          if (parent.id == annotationID) {
            repliesCount++;
          }
        }
      });
      Ember.run.later(function(){
        var annotations = Ember.$('.czn-list-annotation-text');
        if(annotations.length > 0){
          for(var i = 0; i < annotations.length; i++)
          {
            if(annotations[i].innerHTML === '')
            {
              annotations[i].innerHTML = annotations[i].title;
            }
          }
        }
      });
      return repliesCount;
    }.property('annotationsChanged'),

    annotationSoftProofingClass: function(){
        var level = this.get('SoftProofingLevel');
        switch (level) {
            case AppConstants.SoftProofingLevel.Calibrated:
                return 'czn-softproofing-calibrated';
            case AppConstants.SoftProofingLevel.Expired:
                return 'czn-softproofing-expired';
            case AppConstants.SoftProofingLevel.NotCalibrated:
                return 'czn-softproofing-notcalibrated';
            default:
                return 'czn-softproofing-none';
        }
    }.property('SoftProofingLevel'),

    canChangeStatus: function() {
        var roleKey = this.getUserRoleKey(this.get('model.Page.Approval.id'));
      return !(roleKey === AppConstants.Permissions.RETOUCHER) && (roleKey === AppConstants.Permissions.REVIEW || AppConstants.Permissions.canApprove[roleKey] || roleKey === AppConstants.Permissions.RETOUCHER);
    }.property('session.user.id', 'model.Page.Approval.id'),

    hasAttachments: function(){
        return this.get('model.AnnotationAttachments.length') > 0;
    }.property('model.AnnotationAttachments.length'),

    isSoftProofing: function(){
        return window.AppkitENV.machineId && window.AppkitENV.machineId.toString().length > 0;
    }.property(''),

    annotationDateFormated: function(){
      var date = this.get('model.AnnotatedDate');
      return this.formatDate(date);
    }.property('model.AnnotatedDate')

});
export default SidebarAnnotationController;
