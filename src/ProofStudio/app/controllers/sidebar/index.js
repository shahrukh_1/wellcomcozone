import Ember from "ember";
import AppConstants from "appkit/config/constants";
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var SidebarIndexController = Ember.ArrayController.extend(UserPermissions, {
    needs:['comments/index', 'sidebar/decisions', 'sidebar/annotation-statuses','navigation/main', 'navigation/custom-profiles/index'],
    sortBy: window.localStorage.getItem('annotationSortingType') === null ? 'name' : window.localStorage.getItem('annotationSortingType'),
    decisions: Ember.computed.alias('controllers.sidebar/decisions.model'),
    availableStatuses: Ember.computed.alias('controllers.sidebar/annotation-statuses.model'),
    currentSPP : Ember.computed.alias('controllers.navigation/custom-profiles/index.currentSPP'),
    annotationsLoadingInProgress: false,
    PhasesNamesFlag:false,
    IsSelectedAllUsers:true,

    viewer: function(){
        return this.get('currentViewerController');
    }.property('controllers.approval.viewerControllers.@each.select', 'controllers.approval.viewerControllers.length'),

    actions: {
        deselectGroupUsers: function(group){
            group.users.forEach(function(user){
               user.set('selected', false);
            });
            Ember.set(group,'IsGroupSelected', false);                  
           },

        selectGroupUsers: function(group){
        group.users.forEach(function(user){
            user.set('selected', true);
        }); 
        Ember.set(group,'IsGroupSelected', true);                  
        },

        selectAllUsers : function(users){
            var self = this;
            users.forEach(function(user){
               user.set('selected', true);
            });
            self.set('IsSelectedAllUsers', true);        
           },

        deselectAllUsers :  function(users){
            var self = this;
            users.forEach(function(user){
               user.set('selected', false);
            });  
            self.set('IsSelectedAllUsers', false);       
           },


        changeSorting: function(sort) {
            this.set('sortBy', sort);
            window.localStorage.setItem('annotationSortingType', sort);
        },
        toggleUser: function(user) {
            if (user) {
                user.toggleProperty('selected');
            }
        },
        togglePhase: function(phase) {
          if(phase) {
              phase.toggleProperty('selected');
            }
        },
        removeComment: function(comment) {
            var parentId = comment.get('id');
            var count = 0;

            this.store.all('annotation').forEach(function(localAnnotation){
                if(localAnnotation.get('Parent.id') === parentId) {
                    count++;
                }
            });

            if(count === 0){
                var self = this;
                comment.transitionTo('loaded.saved');
                comment.destroyRecord().then(function(){
                    self.get('store').unloadRecord(comment);
                    comment.transitionTo('deleted');
                    self.get('currentViewerController').set('annotationsChanged', new Date());
                    self.set('annotationsChanged', new Date());
                });
            }
        },
        collapseGroup: function(group){
            var groupElement = Ember.$("ul[group-id='" + group.id + "']");
            if (groupElement.is(":visible")){
                groupElement.hide();
                Ember.$(".czn-group[group-id='" + group.id + "']").find('i.czn-group-icon').addClass('czn-group-icon-rotated');
            }
            else{
                groupElement.show();
                Ember.$(".czn-group[group-id='" + group.id + "']").find('i.czn-group-icon').removeClass('czn-group-icon-rotated');
            }
        },
        collapseUsers: function(){
            var session = this.get('session'),
                allowMultiplePanelsOpen = session.get('allowMultiplePanelsOpen'),
                isUsersPanelOpen = session.get('isUsersPanelOpen'),
                isAnnotationsPanelOpen = session.get('isAnnotationsPanelOpen');

            if(isUsersPanelOpen)
            {
                Ember.$(".czn-users").find('i.czn-group-icon').addClass('czn-group-icon-rotated'); 
            }
            else
            {
                Ember.$(".czn-users").find('i.czn-group-icon').removeClass('czn-group-icon-rotated'); 
  
            }

            if (allowMultiplePanelsOpen){
                session.set('isUsersPanelOpen', !isUsersPanelOpen);
            }
            else{
                if (isUsersPanelOpen){
                    session.set('isUsersPanelOpen', false);
                }
                else if (!isAnnotationsPanelOpen && !isUsersPanelOpen){
                    session.set('isUsersPanelOpen', true);
                }
                else if (isAnnotationsPanelOpen && !isUsersPanelOpen){
                    session.set('isAnnotationsPanelOpen', false);
                    session.set('isUsersPanelOpen', true);
                }
                else if (!isAnnotationsPanelOpen && isUsersPanelOpen){
                    session.set('isAnnotationsPanelOpen', true);
                    session.set('isUsersPanelOpen', false);
                }
            }
        },
         makeAnnotationsPublic: function(){
         var self = this;
         var annotationsSelected = self.get('session.selectedAnnotationsToMakePublic');
          if(annotationsSelected.length > 0)
          {
              var annotationIds = [];
              annotationsSelected.forEach(function(annotation){
                    annotationIds.push(annotation.id);
              });
              self.set('session.selectedAnnotationsToMakePublic', []);
             this.store.adapterFor('annotation').updateAnnotationToPublic(annotationIds).then(function(result){
                var annotations = self.store.all('annotation');
                annotations.forEach(function(annotation){
                    if(annotationIds.contains(annotation.id))
                    {
                        Ember.set(annotation, 'selected', false);
                        if(annotation._data.IsPrivateAnnotation){
                            Ember.set(annotation._data, 'IsPrivateAnnotation', false);
                            Ember.set(annotation, 'IsPrivateAnnotation', false);
                        } else{
                           Ember.set(annotation._data, 'IsPrivateAnnotation', true);
                           Ember.set(annotation, 'IsPrivateAnnotation', true);
                        }
                                    Ember.set(annotation, 'annotationSelectedToPublic', false);
                    }
                });
            });
          }
        },
        collapseAnnotations: function() {
            var session = this.get('session'),
                allowMultiplePanelsOpen = session.get('allowMultiplePanelsOpen'),
                isUsersPanelOpen = session.get('isUsersPanelOpen'),
                isAnnotationsPanelOpen = session.get('isAnnotationsPanelOpen');

            if (allowMultiplePanelsOpen){
                session.set('isAnnotationsPanelOpen', !isAnnotationsPanelOpen);
            }
            else{
                if (isAnnotationsPanelOpen){
                    session.set('isAnnotationsPanelOpen', false);
                }
                else if (!isAnnotationsPanelOpen && !isUsersPanelOpen){
                    session.set('isAnnotationsPanelOpen', true);
                }
                else if (isAnnotationsPanelOpen && !isUsersPanelOpen){
                    session.set('isAnnotationsPanelOpen', false);
                    session.set('isUsersPanelOpen', true);
                }
                else if (!isAnnotationsPanelOpen && isUsersPanelOpen){
                    session.set('isAnnotationsPanelOpen', true);
                    session.set('isUsersPanelOpen', false);
                }
            }
        },
        collapsePhases: function() {
            var session = this.get('session');
            var isOpenPanel = session.get('isPhasesPanelOpen');
            session.set('isPhasesPanelOpen',!isOpenPanel);
        }
    },
    isUserPDM: function(){
        var pdmID = this.store.all('approval-basic').content[0].get('PDM');
        if(pdmID != null){
         if(this.store.all('user').content[0].id === pdmID.toString())
          { return true;
          } else {  return false;
            }
        } else { return false};
      }.property('model'),

    showOnlyPrivateAnnotations: function(){
        return this.get('session.showOnlyPrivateAnnotations');
    }.property('session.showOnlyPrivateAnnotations'),
      
    areAnnotationsSelected: function(){
        if(this.get('session.selectedAnnotationsToMakePublic').length > 0 && this.store.all('user').content[0].id === (this.store.all('approval-basic').content[0].get('PDM')).toString())
        {
            return true;
        } else {
            return false;
        }
    }.property('session.selectedAnnotationsToMakePublic'),
    isUsersPanelOpen: function() {
        return this.get('session.isUsersPanelOpen');
    }.property('session.isUsersPanelOpen'),

    isPhasesPanelOpen: function() {
        return this.get('session.isPhasesPanelOpen');
    }.property('session.isPhasesPanelOpen'),

    /* used to show the tab with phases*/
    isAllPhases: function() {
      var phases = this.get('phases');
      phases.forEach(function(phase) {
        phase.set('selected', true);
      });
      return this.get('controllers.navigation/main.isPhase') && this.get('session.user.data.ShowAnnotationsOfAllPhases');
    }.property('controllers.navigation/main.isPhase','session.user.data.ShowAnnotationsOfAllPhases','phases'),

    isAnnotationsPanelOpen: function(){
        return this.get('session.isAnnotationsPanelOpen');
    }.property('session.isAnnotationsPanelOpen'),

    isSortByName: function(){
        return this.get('sortBy') === "name";
    }.property('sortBy'),

    isSortByStatus: function(){
        return this.get('sortBy') === "status";
    }.property('sortBy'),

    isSortByPage: function(){
        return this.get('sortBy') === "page";
    }.property('sortBy'),

    users: function() {
        var collaborators = this.get('collaborators');

        var decisionsHash = [];
        this.store.all('decision').forEach(function(decision){
            decisionsHash[decision.get('id')] = decision.get('Name');
        });

        collaborators.forEach(function(collaborator){
            if(collaborator.get('SubstituteUserName') == '' && window.AppkitENV.substituteLoggedUser[0] == collaborator.id)
            {
                collaborator.set('SubstituteUserName',' (To ' + window.AppkitENV.substituteLoggedUser[2] + ')' );
            }
            var decisionId = collaborator.get('Decision.id') || 0;
            collaborator.set('decisionIcon', AppConstants.DecisionIcons[decisionId]);
            collaborator.set('decisionTitle', decisionsHash[decisionId]);
            collaborator.set('title', collaborator.get('GivenName') + " " + collaborator.get('FamilyName') + collaborator.get('SubstituteUserName'));
            collaborator.set('selected', true);
            collaborator.set('bgColor', "background-color: " + collaborator.get('UserColor'));
        });

        return collaborators;
    }.property('collaborators', 'users.@each.DecisionChanged', 'users.@each.UserGroup', 'currentViewerController.usersChanged', 'currentViewerController.model.id'),

    usersWithoutGroups: function(){
        var collaborators = this.get('users');
        return collaborators.filter(function(collaborator){
            var group = collaborator.get('UserGroup');
            return group === null || typeof(group) === 'undefined';
        });
    }.property('users'),

    groups: function(){
        var collaborators = this.get('users'),
            groups = [],
            self = this;

        collaborators.forEach(function(collaborator){
            var group = collaborator.get('UserGroup');
            if ( group !== null && typeof(group) !== 'undefined'){
                var currentGroup;
                groups.forEach(function(localGroup){
                    if (localGroup.id === group.get('id')){
                        currentGroup = localGroup;
                    }
                });
                if (!currentGroup){
                    var groupDecision,
                        localUserGroupLinks = self.store.all('internal-collaborators-link').filter(function(link){
                        return link.get('approval_version.id') === self.get('viewer.id') && link.get('UserGroup.id') === group.get('id');
                    });
                    if (localUserGroupLinks && localUserGroupLinks.get('length') > 0){
                        localUserGroupLinks.forEach(function(link){
                            if (link.get('Decision')) {
                                if (!groupDecision){
                                    groupDecision = link.get('Decision');
                                }
                                if (groupDecision && groupDecision.get('Priority') > link.get('Decision.Priority')) {
                                    groupDecision = link.get('Decision');
                                }
                            }
                        });
                    }

                    currentGroup = {
                        id: group.get('id'),
                        name: group.get('Name'),
                        selected: true,
                        collapsed: false,
                        IsGroupSelected: true,
                        decision: groupDecision,
                        users: [ collaborator ]
                    };
                    groups[groups.length] = currentGroup;
                }
                else{
                    currentGroup.users[currentGroup.users.length] = collaborator;
                }
            }
        });
        groups.forEach(function(group){
            if (group.decision) {
                group.decisionIcon = AppConstants.DecisionIcons[group.decision.get('id')];
                group.decisionTitle = group.decision.get('Name');
            }
        });
        return groups;
    }.property('users', 'users.@each.decisionIcon'),

    phases: function() {
      var currentViewerController = this.get('currentViewerController');
      if (currentViewerController)
      {
        var phases = this.get('PhaseNamesAllControllersViews').filterBy('controllerId',currentViewerController.get('model.id')).get('firstObject');
        return phases ? phases.get('phasesNames'): [];
      }
      else
      {
        return [];
      }
    }.property('currentViewerController','PhaseNamesAllControllersViews','PhasesNamesFlag'),

    selectedUsers: function() {
        return this.get('users').filter(function(user) {
            return !!user.get('selected');
        });
    }.property('users.@each.selected'),

    selectedPhases: function() {
      return this.get('phases').filter(function(phase) {
        return !!phase.get('selected');
      });
    }.property('phases.@each.selected','phases'),

    template: function() {
        return 'sidebar/annotations/by-' + this.get('sortBy');
    }.property('sortBy'),

    annotations: function() {
        var annotations = this.get('parentAnnotations'),
            selectedUsers = this.get('selectedUsers'),
            selectedPhases = this.get('selectedPhases'),
            self = this;

        var visibleAnnotations = !annotations ? [] : annotations.filter(function(annotation) {
          return self.isVisibleAnnotation(annotation, selectedUsers, selectedPhases);
        });


      if (this.get('currentVersionHasSoftProofing')) {
        var currentSPP = this.get('currentSPP');

        visibleAnnotations.forEach(function (annotation) {
          var annViewCond = annotation.get('ViewingCondition');
          if (annViewCond != null && (
            annViewCond.get('SimulationProfileId') != currentSPP.SimulationProfileId ||
            annViewCond.get('EmbeddedProfileId') != currentSPP.EmbeddedProfileId ||
            annViewCond.get('CustomProfileId') != currentSPP.CustomProfileId ||
            annViewCond.get('PaperTintId') != currentSPP.PaperTintId)) {

            var profileDisplayName = self.get('tilesActionsService').getProfileName({
              SimulationProfileId:annViewCond.get('SimulationProfileId'),
              EmbeddedProfileId:annViewCond.get('EmbeddedProfileId'),
              CustomProfileId:annViewCond.get('CustomProfileId')
            });

            var papertintName = self.getPaperTintName({PaperTintId:annViewCond.get('PaperTintId') || 0});

            annotation.set('isDifferentViewCondition', true);
            annotation.set('DifferentViewConditionText', Ember.I18n.t('lblSPPAnnotationWarning') + "<br><span class='spp-annotation-warning-profile'>" +
              profileDisplayName + "</span><br><span class='spp-annotation-warning-papertint'>" + papertintName + "</span>");
          }
          else {
            annotation.set('isDifferentViewCondition', false);
            annotation.set('DifferentViewConditionText', "");
          }
        });
      }
      return visibleAnnotations;
    }.property('viewer.model', 'selectedUsers','selectedPhases', 'parentAnnotations', 'viewer.currentPage', 'viewer.annotationsChanged',
               'controllers.comments/index.annotationsChanged', 'parentAnnotations.@each.isDeleted', 'parentAnnotations.@each.isDestroyed',
               'controllers.navigation/main.isShowAnnotationsAllPhases', 'currentSPP'),

    source: function() {
        var options = {
            name: 'contentByName',
            page: 'contentByPage',
            status: 'contentByStatus'
        };
        var sort = this.get('sortBy');
        return options.hasOwnProperty(sort) ? this.get(options[sort]) : [];
    }.property('sortBy', 'selectedUsers', 'annotations.@each.Status'),

    contentByName: function() {
        var self = this;
        var users = this.get('selectedUsers'); 
        users.forEach(function(user) {
            var annotations = [];
            self.get('annotations').forEach(function(annotation){
                if (annotation.get('Creator') === user ||
                    annotation.get('ExternalCreator') === user) {
                    annotations.push(annotation);
                }
            });
            user.set('annotations', annotations);
        });
        return users.filter(function(user) {
            return !Ember.isEmpty(user.get('annotations'));
        });
    }.property('annotations', 'sortBy'),

    contentByPage: function() {
        var result = [],
            object, value;

        this.get('annotations').forEach(function(item){
            value = item.get('Page.PageNumber');
            object = result.findProperty('PageNumber', value);
            if (!object) {
                object = {
                    PageNumber: value,
                    annotations: []
                };
                result.push(object);
            }
            return object.annotations.push(item);
        });
        return result.sortBy('PageNumber');
    }.property('annotations', 'sortBy'),

    contentByStatus: function() {
        var content = [];
        var added = [];
        var annotations = this.get('annotations');
        annotations.forEach(function(annotation) {
            var status = annotation.get('Status');
            if(!added.contains(status.get('id'))) {
                content.push(status);
                added.push(status.get('id'));
                status.set('annotations', []);
            }
            status.get('annotations').push(annotation);
        });
        return content;
    }.property('annotations.@each.Status'),

    hasUsersOrGroups: function(){
        return this.get('users.length') > 0 || this.get('annotations.length') > 0;
    }.property('users', 'annotations'),

    isSoftProofing: function(){
        return window.AppkitENV.machineId && window.AppkitENV.machineId.toString().length > 0;
    }.property(''),

    currentVersionHasSoftProofing: function(){
      var currentSPP = this.get('currentSPP');
      var isRGB = this.get('currentViewerController.model.IsRGBColorSpace');
      var isImage = this.get('currentViewerController.currentVersion.ApprovalType') == "Image";

      return (!isRGB && isImage && currentSPP)
    }.property('currentViewerController', 'currentViewerController.model', 'currentViewerController.model.IsRGBColorSpace',
      'currentViewerController.currentVersion.ApprovalType', 'currentSPP'),

    usersChanged: function() {
        var currentViewController = this.get('controllers.comments/index').get('currentViewerController');
        if (currentViewController != undefined && currentViewController != null) {
          currentViewController.set('annotationsChanged', new Date());
        }

        this.get('controllers.comments/index').set('annotationsChanged', new Date());
    }.observes('users.@each.selected'),

    phasesChanged: function() {
        var currentViewController = this.get('controllers.comments/index').get('currentViewerController');
        if (currentViewController != undefined && currentViewController != null) {
          currentViewController.set('annotationsChanged', new Date());
        }
        this.get('controllers.comments/index').set('annotationsChanged', new Date());
    }.observes('phases.@each.selected')
});
export default SidebarIndexController;
