import Ember from "ember";

var SidebarDecisionsController = Ember.ArrayController.extend({
    sortProperties: ['Priority'],
    sortAscending: true,

    approvalInit: function() {
        this.deselectAll();
        var currentDecision = this.get('approval.Decision');
        if(!Ember.isNone(currentDecision)) {
            currentDecision.set('selected', true);
        }
    }.observes('approval'),

    actions: {
        selectDecision: function(decision) {
            this.deselectAll();
            decision.set('selected', true);
        },
        saveDecision: function() {
            this.get('target').send('saveDecision', this.get('model').findBy('selected', true));
        }
    },

    deselectAll: function() {
        var decisions = this.get('model');
        if(decisions) {
            decisions.forEach(function(decision) {
                decision.set('selected', false);
            });
        }
    }

});
export default SidebarDecisionsController;
