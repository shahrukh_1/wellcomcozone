import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var SidebarAnnotationStatusesController = Ember.ArrayController.extend({
    sortProperties: ['Priority'],
    sortAscending: true,

    modelInit: function() {
        var constants = AppConstants.Annotation.AnnotationStatus;
        this.get('model').forEach(function(status) {
            var statusConstants = constants[status.get('id')];
            status.reopen({
                icon: statusConstants.icon,
                iconHeader: statusConstants.iconHeader
            });
        });
    }.observes('model')
});

export default SidebarAnnotationStatusesController;
