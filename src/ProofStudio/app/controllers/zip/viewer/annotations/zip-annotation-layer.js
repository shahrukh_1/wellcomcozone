import Ember from "ember";
import AnnotationLayer from 'appkit/controllers/viewer/annotations/annotation-layer';
import AppConstants from 'appkit/config/constants';
import PlusDivIcon from 'appkit/components/zip/viewer/annotations/plus-div-icon';
import NumberedDivIcon from 'appkit/components/zip/viewer/annotations/numbered-div-icon';
import ZipCanvasView from 'appkit/controllers/zip/viewer/annotations/zip-canvas-view';

var ZipAnnotationLayer = AnnotationLayer.extend(Ember.ActionHandler, ZipCanvasView, {
    needs: ['navigation/main', 'navigation/annotation', 'zip/viewer/zip'],
    selectedWidth:  Ember.computed.alias('controller.controllers.navigation/annotation.selectedWidth'),
    isDrawRectangleSelected: Ember.computed.alias('controller.controllers.navigation/annotation.isDrawRectangleSelected'),
    drawColor: Ember.computed.alias('controller.controllers.navigation/annotation.drawColor'),
    htmlMinHeights: Ember.computed.alias('controller.controllers.zip/viewer/zip.htmlMinHeights'),
    htmlMaxHeights: Ember.computed.alias('controller.controllers.zip/viewer/zip.htmlMaxHeights'),
    htmlMinWidths: Ember.computed.alias('controller.controllers.zip/viewer/zip.htmlMinWidths'),
    htmlMaxWidths: Ember.computed.alias('controller.controllers.zip/viewer/zip.htmlMaxWidths'),
    layerPins:   null,                     // for pins
    layerCanvas: null,                     // for SVG
    fabricCanvas: null,
    freeDrawingMode: false,
    svgAnnotations : [],
    shapeColor: '',
    lineWeight: '',

    init: function () {
      this.initCanvas();

      this.addNewAnnotation();
    },

    isShowPinsObservable: function(){
      var showPins = this.get('controller.controllers.navigation/main.isShowPins');

      if(showPins) {
        this.refreshMultipleAnnotationsLayer();
      } else {
        this.clearMarkers();
      }
    }.observes('controller.controllers.navigation/main.isShowPins'),

    addNewAnnotation: function () {
    }.observes('newAnnotationAddingState'),

    newAnnotationClicked: function (ev, context) {
        var self = this;
        var orderNumberOfHtmlPage = null;
        if(window.AppkitENV.selectedHtmlPageId === "0")
        {
         var htmlMinHeights = this.get('htmlMinHeights');
         var htmlMaxHeights = this.get('htmlMaxHeights');
         var htmlMinWidths = this.get('htmlMinWidths');
         var htmlMaxWidths = this.get('htmlMaxWidths');
         var htmlPages = [];
        for(var i = 0; i < htmlMinHeights.length; i++)
        {
           if(htmlMinHeights[i] <= ev.offsetY && htmlMaxHeights[i] >= ev.offsetY && htmlMinWidths[i] <= ev.offsetX && htmlMaxWidths[i] >= ev.offsetX && htmlPages.length === 0)
           {
            htmlPages.pushObject(i);
           }
        }

        if(htmlPages.length === 0)
        {
          return null;
        }
        else{
          orderNumberOfHtmlPage = htmlPages[0];
        }
      }
        var position = { x: ev.offsetX, y: ev.offsetY };
        var layerPins = context.get('layerPins');
        var newAnnotationAddingState = context.get('newAnnotationAddingState');
        var plusMarker = context.get('plusMarker');

        if (newAnnotationAddingState === true && !plusMarker) {
          //create marker with plus icon and add it to the layerPins
          this.addPlusMarker(position);

           var point;
           if(orderNumberOfHtmlPage != null){
             point = { x: ev.offsetX - htmlMinWidths[orderNumberOfHtmlPage], y: ev.offsetY - htmlMinHeights[orderNumberOfHtmlPage] };
           } else {
             point = { x: ev.offsetX, y: ev.offsetY };
           }
          context.get('controller').send('newComment', {OrderNumberOfHtmlPage: orderNumberOfHtmlPage, Point: point, PageId: context.get('pageId'), TimeFrame: context.get('timeFrame'), CommentType: AppConstants.CommentTypes.MARKER_COMMENT});

          var clickedPoint = {x: ev.originalEvent.clientX, y: ev.originalEvent.clientY - 20};
          Ember.run.later(function() {
            self.updateAnnotationPopupLine(clickedPoint);
          });
        }

        newAnnotationAddingState = false;
    },

    syncZipLayerOnResize: function(){
      var zip = Ember.$(".zip-view.ember-view:first");
      var annotation = this.get('annotation');
      var position = null;
      var originalPoint = null;
      var width = this.layerPins[0].clientWidth,// current webpage width
          height =this.layerPins[0].clientHeight; //current webpage height

      var offset = Ember.$(zip).offset();
      var padding = parseInt(Ember.$(zip).css('padding-top'));

      if(annotation == null){
        originalPoint = {offsetX: this.videoFabricCanvas.OriginalWidth, offsetY: this.videoFabricCanvas.OriginalHeight};
        position = {left: this.plusMarker.position.x, top: this.plusMarker.position.y};
      }
      else
      {
        originalPoint ={offsetX: this.get('annotation.OriginalVideoWidth'), offsetY: this.get('annotation.OriginalVideoHeight')};
        position = {left: this.get('annotation.CrosshairXCoord'), top: this.get('annotation.CrosshairYCoord')};
      }
       
      var xPostionPercentageChange = originalPoint.offsetX > 0 ? (width - originalPoint.offsetX) / originalPoint.offsetX : 0;
      var yPostionPercentageChange = originalPoint.offsetY > 0 ? (height - originalPoint.offsetY) / originalPoint.offsetY : 0;

      var pointx = Math.floor((position.left + 8) * (1 + xPostionPercentageChange));
      var pointy = Math.floor((position.top + 2) * (1 + yPostionPercentageChange));

      var positionToDraw = {x: (offset.left + padding + pointx), y: (offset.top + padding + pointy)-20};
      this.redrawAnnotationPopupLine(positionToDraw);
    },

    redrawAnnotationPopupLine: function(point) {
      var self = this;
      var commentsController = this.get('controller.controllers.comments/index');

      Ember.run.later(function () {
        var popupOffset = Ember.$(".czn-comment-modal").offset();
        if (popupOffset) {
          self.set('popup_Y', popupOffset.top);
          self.set('popup_X', popupOffset.left);

          if (commentsController) {
            commentsController.set("popup_X",popupOffset.left);
            commentsController.set("popup_Y",popupOffset.top);
            commentsController.send('setMarkerPosition', point.x, point.y + 20);
          }
        }
      },100);
      
    },

    addPlusMarker: function(position){
      var layerPins = this.get('layerPins');
      var marker = new PlusDivIcon();

      marker.set('position', position);
      marker.set('width', this.get('width'));
      marker.set('height', this.get('height'));
      marker.set('pinslayer', this);
      marker.addTo(layerPins);

      this.set('plusMarker', marker);
    },

    getCurrentSelectedAnnotationPlusMarker: function(currentAnnotation){
      var layerPins = this.get('layerPins');
      var pageNumber = null;
      var getOriginalXpoint = currentAnnotation.get('OriginalVideoWidth') > 0 ? currentAnnotation.get('OriginalVideoWidth') : 0;
      var getOriginalYpoint = currentAnnotation.get('OriginalVideoHeight') > 0 ? currentAnnotation.get('OriginalVideoHeight') : 0;
      var xPostionPercentage = getOriginalXpoint == 0 ? 0 : (layerPins.width() - getOriginalXpoint)/getOriginalXpoint;
      var yPostionPercentage = getOriginalYpoint == 0 ? 0: (layerPins.height() - getOriginalYpoint)/getOriginalYpoint;

       var pointx;
       var pointy;
       if(window.AppkitENV.selectedHtmlPageId === "0"){
          pageNumber = currentAnnotation._data.PageNumber - 1;
          pointx = currentAnnotation.get('CrosshairXCoord') + this.get('htmlMinWidths')[pageNumber];
          pointy = currentAnnotation.get('CrosshairYCoord') + this.get('htmlMinHeights')[pageNumber];
       } else {
          pointx = Math.floor(currentAnnotation.get('CrosshairXCoord'));
          pointy = Math.floor(currentAnnotation.get('CrosshairYCoord'));
       }
      var point = {x: pointx, y: pointy};
      this.addPlusMarker(point);
    },

    updateAnnotationPopupLine: function(point) {
      this.setAnnotationPopupLine();
      var commentsController = this.get('controller.controllers.comments/index');
      if(commentsController) {
        commentsController.send('setMarkerPosition', point.x, point.y + 20);
      }
    },

    setAnnotationPopupLine: function(){
      var self = this;
      Ember.run.later(function(){
        var popupOffset = Ember.$(".czn-comment-modal").offset();
        if (popupOffset) {
          self.set('popup_Y', popupOffset.top);
          self.set('popup_X', popupOffset.left);
        }
      });
    },

    initAddMarker: function(annotationType){
        this._super();
        this.clearMarkers();
        this.set('annotation', null);

        var self = this;
        self.set('videoFabricCanvas.OriginalWidth',self.get("videoFabricCanvas").width);
        self.set('videoFabricCanvas.OriginalHeight',self.get("videoFabricCanvas").height);

        var fabricCanvas = self.get('videoFabricCanvas'),
            layerPins = this.get('layerPins'),
            layerCanvas = this.get('layerCanvas').parents('div.canvas-container');

        var isDrawShapeSelected = this.get('controller.controllers.navigation/annotation').get('isDrawShapeSelected');

        if (fabricCanvas){
            fabricCanvas.clear();
        }
        if (layerCanvas && layerPins) {
            if (annotationType === AppConstants.CommentTypes.MARKER_COMMENT || annotationType === AppConstants.CommentTypes.DRAW_COMMENT) {
              layerPins.show();

              if(!isDrawShapeSelected) {
                layerCanvas.show();
              } else {
                layerCanvas.hide();
              }
            }
        }
    },

    closeAddMarker: function(){
        var layerPins = this.get('layerPins');
        this.set("svgTemp", null);
        this._super();
        this.set('plusMarker', null);

        this.clearVideoFabricCanvas();

        if (layerPins){
            layerPins.show();
        }
    },

    clearVideoFabricCanvas: function(){
      var fabricCanvas = this.get('videoFabricCanvas');

      if (fabricCanvas){
        fabricCanvas.clear();
      }
    },

    refreshSingleAnnotationLayer: function () {
        this.syncLayer();
    }.observes('layerIsVisible', 'singleAnnotationView'),

    refreshMultipleAnnotationsLayer: function () {
        var self = this;
        var singleAnnotationView = this.get('singleAnnotationView');
        var annotations = this.get('annotations');
        if (annotations && singleAnnotationView === false) {
            this.clearMarkers();

            Ember.$("canvas.canvasvideo-layer").hide();

            var layerIsVisible = this.get('layerIsVisible');
            var showPins = this.get('controller.controllers.navigation/main.isShowPins');
            if (layerIsVisible === true && showPins) {
                this.createMarkers(annotations);
            }

            var fabricCanvas = self.get('videoFabricCanvas');
            if (fabricCanvas) {
                fabricCanvas.isDrawingMode = false;
            }
            this.syncLayer();
        }
    }.observes('layerIsVisible', 'singleAnnotationView'),


    createMarkers: function(annotations){
      var self = this;
      var newAnnotationNumber = annotations.length;

      annotations.forEach(function (annotation) {
        if(annotation.get('AnnotationNumber') === undefined && annotation.get('Nr') === undefined){
          annotation.set('Nr', newAnnotationNumber);
          annotation.set('AnnotationNumber', newAnnotationNumber);
        } else {
        annotation.set('Nr', annotation.get('AnnotationNumber'));
        }
        self.createMarker(annotation);
      });
    },

    createMarker: function(annotationItem){
        var pinsLayer = this.get("layerPins");
        if (pinsLayer) {

          var pageNumber = annotationItem.get('_data').PageNumber - 1;
          var marker = new NumberedDivIcon();
          marker.set('annotation', annotationItem);
          marker.set('width', this.get('width'));
          marker.set('height', this.get('height'));
          marker.set('pinslayer', this);
          marker.addTo(pinsLayer);

          var zip = Ember.$(".zip-view.ember-view:first");
          var width = parseInt(Ember.$(zip).css('width'));
          var height = parseInt(Ember.$(zip).css('height'));
          var padding = parseInt(Ember.$(zip).css('padding'));
          var htmlMinHeights = this.get('htmlMinHeights');
          var htmlMaxHeights = this.get('htmlMaxHeights');
          var htmlMinWidths = this.get('htmlMinWidths');
          var htmlMaxWidths = this.get('htmlMaxWidths');
          if(htmlMinHeights.length === 0) {
           var offsetValues = Ember.$('.zip');
           for(var i = 0; i< offsetValues.length; i++ )
           {
              var minHeight  =  (offsetValues[i].offsetTop);
              var maxheight  =  (offsetValues[i].offsetTop + offsetValues[i].offsetHeight);
              var minWidth   =  (offsetValues[i].offsetLeft);
              var maxWidth   =  (offsetValues[i].offsetLeft + offsetValues[i].offsetWidth);
              htmlMinHeights.pushObject(minHeight);
              htmlMaxHeights.pushObject(maxheight);
              htmlMinWidths.pushObject(minWidth);
              htmlMaxWidths.pushObject(maxWidth);
           }
          }
          marker.set ('htmlMinMaxValues', [this.get('htmlMinHeights')[pageNumber], this.get('htmlMinWidths')[pageNumber]]);
          marker.syncPosition(width - 2 * padding, height - 2 * padding);

          var markers = this.get('markers');
          if (markers) {
            markers.push(marker);
            this.set('markers', markers);
          }
        }
    },

    clickMarker: function(ev){
        var pinsLayer = this.get('layerPins');
        var annotations = this.get('annotations');
        if (pinsLayer) {
            var annotation = null;
            if (annotations){
                var annotationId = Ember.$(ev.currentTarget).attr('annotationId');
                annotation = annotations.filterBy('id', annotationId).get('firstObject');
            }
            if (annotation){
                this.displaySingleAnnotation(annotation);
            }
        }
    },

    syncLayer: function(){
      var zip = Ember.$(".zip");
      if(zip.length > 1)
      {
        zip = Ember.$(".zip-view.ember-view:first");
        zip.css('display', 'inline-block');
        zip.css('position', 'relative');
        zip.css('top', '1px');
      } else if(zip.length === 1){
       var divHeight = parseInt(Ember.$("#main").height() / 2);
       var approvalHeight = parseInt(Ember.$(zip).height() / 2);
       var top = divHeight - approvalHeight;
       if(top > 8){
       zip.css('top', top +'px');
       } else {
         zip.css('top', '8px');
       }
       zip.css('position', 'relative');
      }
        var width = parseInt(Ember.$(zip).css('width'));
        var height = parseInt(Ember.$(zip).css('height'));

        var offset = Ember.$(zip).offset();
        var padding = parseInt(Ember.$(zip).css('padding-top'));

        var layerPins = this.get('layerPins');
        if (layerPins) {
            layerPins.css('width', width - 2 * padding);
            layerPins.css('height', height - 2 * padding);
            layerPins.offset({left: offset.left + padding, top: offset.top + padding });
        }

        this.updateCanvasLayers();

        var markers = this.get('markers');
        if (markers){
            markers.forEach(function(marker){
                marker.syncPosition(width - 2*padding, height-2*padding);
            });
        }
        var plusMarker = this.get('plusMarker');
        if (plusMarker){
            plusMarker.syncPosition(width - 2*padding, height - 2*padding, this.newAnnotationAddingState,this.videoFabricCanvas.OriginalWidth,this.videoFabricCanvas.OriginalHeight);
        }
    },

    updateCanvasLayers: function() {
      var zip = Ember.$(".zip-view.ember-view:first");

      var offset = Ember.$(zip).offset();
      var padding = parseInt(Ember.$(zip).css('padding-top'));
      var width = parseInt(Ember.$(zip).css('width'));
      var height = parseInt(Ember.$(zip).css('height'));

      var layerCanvas = Ember.$('canvas.canvasvideo-layer');
      layerCanvas.css({
        left: offset.left + padding,
        top: padding + padding,
        width: width - 2 * padding,
        height: height - 2 * padding
      });
      layerCanvas.offset({top: offset.top + padding, left: offset.left + padding});

      var fabricCanvas = this.get('videoFabricCanvas');
      if (fabricCanvas) {
        fabricCanvas.setHeight(height - 2 * padding);
        fabricCanvas.setWidth(width - 2 * padding);
      }
    },

    clearMarkers: function(){
        var markers = this.get('markers');
        if (markers)
        {
            while (markers.length > 0){
                markers.pop();
            }

            markers = [];
        }
        this.set('markers', []);
        Ember.$('div.pinsvideo-layer .leaflet-marker-icon').remove();
    },

    selectAnnotation: function (annotation) {
      this.get('controller').send('selectAnnotation', annotation);
    },

    onSelectedWidthChange: function() {
      this.set('selectedDrawWidth', this.get('selectedWidth') * 4);// adjust width to webpage scale
    }.observes('selectedWidth')
});
export default ZipAnnotationLayer;
