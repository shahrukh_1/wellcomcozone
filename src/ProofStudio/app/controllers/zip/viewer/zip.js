import Ember from "ember";
import ViewerController from 'appkit/controllers/viewer/viewer';
import AppConstants from 'appkit/config/constants';
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var ZipViewerController = ViewerController.extend(UserPermissions, {
    needs: ['zip/viewer/zip-player'],
    currentTime: null,
    duration: null,
    htmlMinHeights: [],
    htmlMaxHeights: [],
    htmlMinWidths: [],
    htmlMaxWidths: [],
    playing: Ember.computed.alias('controllers.zip/viewer/zip-player.playing'),

    actions: {

        initializeZipRenderer: function (renderer) {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('initializeZipRenderer', renderer);
            }
        },

        timeUpdate: function(currentTime, duration){
            this.set('currentTime', currentTime);
            this.set('duration', duration);
        },

        initializeView: function (view) {
            this.set('view', view);
            this.set('view-controller', view.get('controller'));
            this.send('initializeViewerController', this);
        },

        play: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('play');
            }
        },

        pause: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('pause');
            }
        },

        forward: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('forward');
            }
        },

        backward: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('backward');
            }
        },

        goToStart: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('goToStart');
            }
        },

        goToEnd: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('goToEnd');
            }
        },

        synchronizeWithVideo: function (time) {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('synchronizeWithVideo', time);
            }
        },

        setProgressBar: function (percent) {
            var controller = this.get('view');
            if (controller) {
                controller.send('setProgressBar', percent);
            }
        },

        renderAnnotation: function (/*markups*/) {
        },

        initAddMarkerInViewerController: function (annotationType) {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('initAddMarker', annotationType);
            }
            var htmlMinHeights = this.get('htmlMinHeights');
            var htmlMaxHeights = this.get('htmlMaxHeights');
            var htmlMinWidths = this.get('htmlMinWidths');
            var htmlMaxWidths = this.get('htmlMaxWidths');
            if(htmlMinHeights.length != 0)
            {
                htmlMinHeights = [];
                htmlMaxHeights = [];
                htmlMinWidths = [];
                htmlMaxWidths = [];
            }
            var offsetValues = Ember.$('.zip');

            for(var i = 0; i< offsetValues.length; i++ )
            {
              var minHeight  =  (offsetValues[i].offsetTop);
              var maxheight  =  (offsetValues[i].offsetTop + offsetValues[i].offsetHeight);
              var minWidth   =  (offsetValues[i].offsetLeft);
              var maxWidth   =  (offsetValues[i].offsetLeft + offsetValues[i].offsetWidth);
              htmlMinHeights.pushObject(minHeight);
              htmlMaxHeights.pushObject(maxheight);
              htmlMinWidths.pushObject(minWidth);
              htmlMaxWidths.pushObject(maxWidth);
            }
        },
        closeAddMarker: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('closeAddMarker');
            }
        },
        displaySingleAnnotation: function (annotation) {
            var controller = this.get('view-controller');
            var htmlMinHeights = this.get('htmlMinHeights');
            var htmlMaxHeights = this.get('htmlMaxHeights');
            var htmlMinWidths = this.get('htmlMinWidths');
            var htmlMaxWidths = this.get('htmlMaxWidths');
            if(htmlMinHeights.length === 0) {
             var offsetValues = Ember.$('.zip');
             for(var i = 0; i< offsetValues.length; i++ )
             {
                var minHeight  =  (offsetValues[i].offsetTop);
                var maxheight  =  (offsetValues[i].offsetTop + offsetValues[i].offsetHeight);
                var minWidth   =  (offsetValues[i].offsetLeft);
                var maxWidth   =  (offsetValues[i].offsetLeft + offsetValues[i].offsetWidth);
                htmlMinHeights.pushObject(minHeight);
                htmlMaxHeights.pushObject(maxheight);
                htmlMinWidths.pushObject(minWidth);
                htmlMaxWidths.pushObject(maxWidth);
             }
            }
            if (controller) {
                controller.send('displaySingleAnnotation', annotation);
            }
        },

        drawingModeInViewerController: function (enter, lineWidth, lineColor) {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('drawingModeInZipPlayer', enter, lineWidth, lineColor);
            }
        },

        clearCanvas: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('clearCanvas');
            }
        },

        clearSelection: function () {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('clearSelection');
            }
        },

        addShapeToCanvas: function (mouseEventDetails) {
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('addShapeToCanvas', mouseEventDetails);
            }
        },

        toggleVersionLock: function(model){
            var userCanAnnotate = this.get('userCanAnnotate'),
                userCanApprove = this.get('userCanApprove'),
                isLocked = model.get('isLocked');

            if (this.get('model.IsLocked') === true && !this.userCanUnlockVersion() ) {
                return false;
            }
            else if(model.get('userRole') === AppConstants.UserRoles.ADMIN || model.get('userRole')=== AppConstants.UserRoles.MANAGER)
            {
                this.get('model').set('isLocked', !isLocked);
                this.send('showAnnotationDecision', isLocked);
            }
            else if(model.get('userRole') === AppConstants.UserRoles.MODERATOR ||
                    model.get('userRole') === AppConstants.UserRoles.CONTRIBUTOR ||
                    model.get('userRole') === AppConstants.UserRoles.VIEWER){
                this.get('model').set('isLocked', !isLocked);
            }
            else{
                this.get('model').set('isLocked', !isLocked);
                this.send('showAnnotationDecision', isLocked);
            }
        },
        enableTextHighlighting: function(){
          return false;
        },
        initializeBalloons: function(view){
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('initializeBalloons', view);
            }
        },
        toggleAllTickets: function(show){
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('toggleAllTickets', show);
            }
        },
        selectAnnotationAsync: function(annotation){
            var controller = this.get('view-controller');
            if (controller) {
                controller.send('selectAnnotationAsync', annotation);
            }
        },
        changeLockType: function(){
            return false;
        },

        hideCanvas: function(){
          var controller = this.get('view-controller');
          if (controller) {
            controller.send('hideCanvas');
          }
        },
        removeMarkerOverlay: function(){
          var controller = this.get('view-controller');
          if (controller) {
            controller.send('removeMarkerOverlay');
          }
        },
        addMarkerOverlay:function(point, originalPoint) {
            var controller = this.get('view-controller');
            if (controller) {
              controller.send('addMarkerOverlay', point, originalPoint);
            }
        },
        showCanvasForAnnotation:function(annotationId){
          var controller = this.get('view-controller');
          if (controller) {
            controller.send('showCanvasForAnnotation');
          }
        },
        updateMarkerPosition: function(waitTime) {
          var controller = this.get('view-controller');
          if (controller) {
            controller.send('updateMarkerPosition', waitTime);
          }
        }
    },

   
    
    IsAllFilesLoaded: function() {
        return window.AppkitENV.selectedHtmlPageId === "0";
    }.property('model'),

    versionLocked: function(){
        return this.get('model.isLocked');
    }.property('model.isLocked'),

    unlockButtonClass: function(){
        var isLocked = this.get('model.isLocked'),
            userCanAnnotate = this.get('userCanAnnotate'),
            userCanApprove = this.get('userCanApprove');

        if (isLocked && !userCanAnnotate && !userCanApprove){
            return 'czn-lock-button-gray';
        }

        if (userCanAnnotate || userCanApprove) {

            if (isLocked === true) {
                return 'czn-lock-button-red';
            }
            else {
                return 'czn-lock-button-green';
            }

        }
        else{
            return 'czn-lock-button-gray'
        }
    }.property('model.isLocked', 'currentViewerController.model.id')
});
export default ZipViewerController;
