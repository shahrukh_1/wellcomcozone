import Ember from "ember";
import PinsVideo from 'appkit/controllers/zip/viewer/annotations/zip-annotation-layer';
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var ZipPlayerController = Ember.ObjectController.extend(CurrentViewerMixin, {
    needs: ['comments/index', 'sidebar/index','navigation/annotation','application', 'approval', 'zip/viewer/zip'],
    src: Ember.computed.alias('MovieFilePath'),
    navigationAnnotation: Ember.computed.alias('controllers.navigation/annotation'),
    sidebarShown: Ember.computed.alias('controllers.application.sidebarShown'),
    htmlMinHeights: Ember.computed.alias('controllers.zip/viewer/zip.htmlMinHeights'),
    htmlMaxHeights: Ember.computed.alias('controllers.zip/viewer/zip.htmlMaxHeights'),
    htmlMinWidths: Ember.computed.alias('controllers.zip/viewer/zip.htmlMinWidths'),
    htmlMaxWidths: Ember.computed.alias('controllers.zip/viewer/zip.htmlMaxWidths'),
    playing: false,
    isSingleHtmlPageSelected: false,
    currentTime: null,
    duration: null,
    annotations: Ember.computed.alias('controllers.zip/viewer/zip.annotations'),
    pins: null,
    commentsDialogIsVisible: Ember.computed.alias('controllers.comments/index.visible'),

    width: function(){
        return this.get('currentPage.LargeThumbnailWidth');
    }.property('currentPage'),
   
    htmlPages: function(){
        var self = this;
        var records = this.store.all('page').content;
        var pages = records.sort(function(a, b){return a._data.id - b._data.id});
        var a = [];
        if(window.AppkitENV.selectedHtmlPageId === "0")
        {
         pages.forEach(function(page){
            self.get('isSingleHtmlPageSelected', false);
            var s = {Annotation: page.get('id'), FileName: page.get('_data').HTMLFileName, HtmlFilePath: page.get('HTMLFilePath'), Height: 'top: 30px; float: left; height:'+page.get('OriginalImageHeight') +'px; width:'+page.get('OriginalImageWidth') +'px;'};
           a.pushObject(s);
         });
        } else {
            pages.forEach(function(page){
                if(window.AppkitENV.selectedHtmlPageId === page.id && a.length === 0){
                    self.set('isSingleHtmlPageSelected', true);
                  var s = {Annotation: page.get('id'), FileName: page.get('_data').HTMLFileName, HtmlFilePath: page.get('HTMLFilePath'), Height: 'top: 30px; margin: 0 auto; height:'+page.get('OriginalImageHeight') +'px; width:'+page.get('OriginalImageWidth') +'px;'};
                  a.pushObject(s);
                }
            })
        }
        
        return a;
        }.property('model'),

    height: function(){
        return this.get('currentPage.LargeThumbnailHeight');
    }.property('currentPage'),

    video: function() {
        var video = this.get('view').$();
        this.get('model').set('videoRenderer',this.get('view'));
        var self = this;

        video.on("ended", function() {
            self.set('playing', false);
        });
       
        video.on("timeupdate", function(event) {
            if (Math.abs(event.target.currentTime - self.get('currentTime')) >= 0.50) {
                self.set('currentTime', event.target.currentTime);
                self.set('duration', event.target.duration);
                self.send('timeUpdate', event.target.currentTime, event.target.duration);
            }
        });

        return video.get(0);
    }.property('view','currentTime'),

     actions: {
        reloadHtmlPage: function(){
            var self = this;
            var records = this.store.all('page').content;
            var pages = records.sort(function(a, b){return a._data.id - b._data.id});
            var a = [];
            if(window.AppkitENV.selectedHtmlPageId === "0")
            {
             pages.forEach(function(page){
                self.get('isSingleHtmlPageSelected', false);
                var s = {Annotation: page.get('id'), FileName: page.get('_data').HTMLFileName, HtmlFilePath: page.get('HTMLFilePath'), Height: 'top: 30px; float: left; height:'+page.get('OriginalImageHeight') +'px; width:'+page.get('OriginalImageWidth') +'px;'};
               a.pushObject(s);
             });
            } else {
                pages.forEach(function(page){
                    if(window.AppkitENV.selectedHtmlPageId === page.id && a.length === 0){
                        self.set('isSingleHtmlPageSelected', true);
                      var s = {Annotation: page.get('id'), FileName: page.get('_data').HTMLFileName, HtmlFilePath: page.get('HTMLFilePath'), Height: 'top: 30px; margin: 0 auto; height:'+page.get('OriginalImageHeight') +'px; width:'+page.get('OriginalImageWidth') +'px;'};
                      a.pushObject(s);
                    }
                })
            }
            
           this.set('htmlPages', a);
           Ember.run.later(function() {
            var pinsVideo = self.get('zip-annotation-layer');
            if (pinsVideo) {
                pinsVideo.syncLayer();
            }
           });
        },
        initializeView: function(/*view*/){
            this.getData();
            this.displayOrHidePinsLayer();
            return true;
        },
        play: function() {
            var video = this.get('video');
            this.send('hide');
            if(video.paused) {
                this.set('playing', true);
                this.get('navigationAnnotation').set('isDrawSelected', false);
                video.play();
            }
            var pinsVideo = this.get('video-annotation-layer');
            if (pinsVideo){
                pinsVideo.hideLayer();
            }
        },

        pause: function() {
            var video = this.get('video');
            if(!video.paused){
                video.pause();
                video.currentTime =  Ember.$("video")[0].currentTime;
                this.set('playing', false);
                this.get('navigationAnnotation').set('isDrawSelected', false);
            }
            var pinsVideo = this.get('video-annotation-layer');
            if (pinsVideo){
                pinsVideo.showLayer();
            }
        },

        forward: function() {
            this.send('hide');
            this.get('navigationAnnotation').set('isDrawSelected', false);

            var video = this.get('video');
            var time = video.duration / 10;
            var currTime = video.currentTime;
            var totalTime = currTime + time;

            if (currTime < Math.floor(video.duration)) {

                video.currentTime = totalTime < video.duration ? totalTime : Math.floor(video.duration);
                if(video.paused){
                    video.pause();
                    this.set('playing', false);
                }
                else{
                    video.play();
                    this.set('playing', true);
                }
            }
        },

        backward: function() {
            this.send('hide');
            this.get('navigationAnnotation').set('isDrawSelected', false);

            var video = this.get('video');
            var time = video.duration / 10;
            var currTime = video.currentTime;
            video.currentTime = currTime > time ? currTime - time : 0;

            if(video.paused){
               video.pause();
               this.set('playing', false);
            }
            else{
               video.play();
               this.set('playing', true);
            }
        },

        goToStart: function() {
            this.send('hide');
            this.get('navigationAnnotation').set('isDrawSelected', false);
            var video = this.get('video');
            video.currentTime = 0;
            if(video.paused){
                video.pause();
                this.set('playing', false);
            }
            else{
                video.play();
                this.set('playing', true);
            }
        },

        goToEnd: function() {
            this.send('hide');
            this.get('navigationAnnotation').set('isDrawSelected', false);
            var video = this.get('video');
            //the total time should be decreased in order
            video.currentTime = (video.duration - 0.001);
            video.play();
            this.set('playing', false);
        },

         synchronizeWithVideo: function(time){
             var video = this.get('video');
             video.currentTime = time > video.duration ? Math.floor(video.duration): time;
         },

         setProgressBar: function(percent){
             var parentHandle = Ember.$('.czn-slider-track');
             var handle = Ember.$('.czn-slider-selection');
             var width = percent * parentHandle.width() / 100;
             handle.css('width', width);
         },

         initializeZipRenderer: function(renderer){
             this.set('view', renderer);
         },

         renderAnnotation: function(/*markups*/){
         },

         initAddMarker: function(annotationType){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.initAddMarker(annotationType);
             }
         },
         closeAddMarker: function(){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.closeAddMarker();
             }
             this.loadAnnotations();
         },
         displaySingleAnnotation: function(annotation){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.displaySingleAnnotation(annotation);
             }
         },
         drawingModeInZipPlayer: function(enter, lineWidth, lineColor){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.drawingModeInZipCanvasView(enter, lineWidth, lineColor);
             }
         },
         clearCanvas: function(){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.clearCanvas();
             }
         },
         clearSelection: function(){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.clearSelection();
             }
         },
         addShapeToCanvas:function(mouseEventDetails){
             var pinsVideo = this.get('zip-annotation-layer');
             if (pinsVideo){
                pinsVideo.addShapeToCanvas(mouseEventDetails);
             }
         },
         initializeBalloons: function(viewBalloons){
             this.set('balloons-view', viewBalloons);
         },
         toggleAllTickets: function(show){
             var balloonsView = this.get('balloons-view');
             if (balloonsView){
                 if (show === true){
                     balloonsView.showLayer();
                 }
                 else{
                     balloonsView.hideLayer();
                 }
             }
         },
         selectAnnotationAsync: function(/*annotation*/){

         },
       hideCanvas:function() {
         var pinsVideo = this.get('zip-annotation-layer');
         if (pinsVideo) {
            pinsVideo.hideCanvas();
         }
       },
       removeMarkerOverlay:function() {
         var pinsVideo = this.get('zip-annotation-layer');

          var currentTime = this.get('currentTime');
          this.loadAnnotations(currentTime * 1000000);

          pinsVideo.showLayer();
          var annotations = this.store.all('annotation').content;
          pinsVideo.set('annotations', annotations);
          pinsVideo.refreshMultipleAnnotationsLayer();
       },

        addMarkerOverlay:function(point, originalPoint) {
            var pinsVideo = this.get('zip-annotation-layer');
            var commentsController = this.get('controllers.comments/index');
            if (commentsController && pinsVideo) {

            var  width = Ember.$(".zip-view").width(), // current webpage width
            height = Ember.$(".zip-view").height(); //current webpage height

            var zip = Ember.$(".zip-view.ember-view:first");
            var offset = Ember.$(zip).offset();
            var left = Ember.$(".pinsvideo-layer")[0].offsetLeft;
            var top = Ember.$(".pinsvideo-layer")[0].offsetTop;
            var padding = parseInt(Ember.$(zip).css('padding-top'));
        
            var pointx;
            var pointy;
            if(window.AppkitENV.selectedHtmlPageId === "0"){
                pointx = Math.floor(point[0] + 8) + this.get('htmlMinWidths')[originalPoint];
                pointy = Math.floor(-point[1] + 2) + this.get('htmlMinHeights')[originalPoint];
            } else {
                pointx = Math.floor((point[0] + 8));
                pointy = Math.floor((-point[1] + 2));
            }
            var point1 = {x: pointx, y: pointy};
            commentsController.send('setMarkerPosition', (offset.left + left + padding + point1.x), (offset.top + top + padding + point1.y ));
            }
        },

       updateMarkerPosition: function(waitTime) {
         var commentsController = this.get('controllers.comments/index');
         if (commentsController) {
           if(!waitTime) {
             waitTime = 5;
           }
           Ember.run.later(function(){
             commentsController.drawMarkerLine();
           }, waitTime);
         }
       },

       showCanvasForAnnotation: function() {
         var zipAnnotationLayer = this.get('zip-annotation-layer');
         var currentAnnotation = this.get("controllers.comments/index").get('annotations')[this.get("controllers.comments/index").get('currentAnnotation') - 1];

         zipAnnotationLayer.clearVideoFabricCanvas();
         zipAnnotationLayer.clearMarkers();
         zipAnnotationLayer.canvasLayerReDraw();
         zipAnnotationLayer.updateCanvasLayers();
         zipAnnotationLayer.getCurrentSelectedAnnotationPlusMarker(currentAnnotation);
       }
    },

    isAnnotationInCurrentTime:function (annotationTime, currentTime) {
      return annotationTime - 750 < currentTime && currentTime < annotationTime + 750;
    },

    loadAnnotations: function(currentTimeFrame){
        var pinsVideo = this.get('zip-annotation-layer');
        if (pinsVideo) {
            var currentTime = currentTimeFrame || pinsVideo.get('annotation.TimeFrame');
            var self = this;
            var annotations = (this.get('annotations') || []).filter(function (annotation) {
                var annotationTimeFrame = annotation.get('TimeFrame');
                return self.isAnnotationInCurrentTime(annotationTimeFrame, currentTime);
            });
            if (this.get('playing') === false) {
                pinsVideo.set('annotations', annotations);
            }
        }
    },

    createPinsLayer: function(){
        var pinsVideo = this.get('zip-annotation-layer');
        if (!pinsVideo){
            pinsVideo = new PinsVideo();
            this.set('zip-annotation-layer', pinsVideo);
        }
        pinsVideo.set('controller', this);
        pinsVideo.set('pageId', this.get('currentPage.id'));
        pinsVideo.set('width', this.get('width'));
        pinsVideo.set('height', this.get('height'));
        pinsVideo.showLayer();
        this.loadAnnotations();
    }.observes('currentPage'),

   currentTimeChanged: function() {
       var video = this.get('zip');
       if (video) {
           if(this.get('currentTime') == null){
               this.set('currentTime', 0.1);
           }
           this.send('setDuration', video.duration);
       }
   }.observes('duration'),

    markersObservable: function(){
        var pinsVideo = this.get('zip-annotation-layer');
        var balloonsView = this.get('balloons-view');

        var currentTime = this.get('currentTime');
        var self = this;

        var annotations = ( self.store.all('annotation').content || []).filter(function(annotation) {
          return self.isAnnotationInCurrentTime(annotation.get('TimeFrame'), currentTime * 1000000);
        });

        if (pinsVideo) {
            pinsVideo.set('timeFrame', currentTime);

          if (this.get('controllers.comments/index').get('currentAnnotation') == 0) {
            pinsVideo.displayMultipleAnnotations(annotations);
          } 
        }

        if (balloonsView){
            balloonsView.initializeData(null, Ember.$(this.get('video')));
            balloonsView.set('fullSize', {
                width: this.get('width'),
                height: this.get('height')
            });
        }

    }.observes('currentTime', 'annotations','currentViewerController.annotationsChanged', 'controllers.sidebar/index.users.@each.selected'),

    displayOrHidePinsLayer: function(){
//        observable function that show or hide the pins layer regarding the visibility of the comments dialog
        var pins = this.get('zip-annotation-layer');
        var commentsDialogIsShown = this.get('commentsDialogIsVisible');
        if (pins){
            if (commentsDialogIsShown === false){
                pins.set('singleAnnotationView', false);
            }
            else{
                pins.set('singleAnnotationView', true);
            }

            if (this.get('playing') === true){
                pins.set('singleAnnotationView', false);
            }
        }
    }.observes('commentsDialogIsVisible', 'playing'),

    syncCanvasLayer: function(){
        var self = this;
        Ember.run.later(function(){
            var pinsVideo = self.get('zip-annotation-layer');
            if (pinsVideo){
                pinsVideo.syncLayer();
            }
            var balloonsView = self.get('balloons-view');
            if (balloonsView){
                balloonsView.syncPosition();
                balloonsView.arrangeBalloons();
            }
        });
    }.observes('controllers.application.sidebarShown', 'annotationsChanged'),

  getData: function(){
      var self = this,
          approvalId = this.get('model.id');

      this.store.find('page', {approvalId: approvalId}).then(function(pages){
          self.set('pagesChanged', new Date());

          self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', true);
          self.store.find('annotation', {approvalId: approvalId, selectedHtmlPageId:window.AppkitENV.selectedHtmlPageId}).then(function(){
              self.set('annotationsChanged', new Date());
              self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', false);
          }).catch(function(/*e*/){
              self.get('controllers.sidebar/index').set('annotationsLoadingInProgress', false);
          });

          self.set('currentPage', Ember.isArray(pages) ? pages.get('firstObject'): pages);
      });
  }
});
export default ZipPlayerController;
