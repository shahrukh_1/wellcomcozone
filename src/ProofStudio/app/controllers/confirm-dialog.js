import Ember from "ember";
import AppConstants from 'appkit/config/constants';

var ConfirmationModalController = Ember.ObjectController.extend({
  needs: ['image/controls/image'],
  title: "",
  message: "",
  headingMessage: "",
  viewedByAllMessage: "",
  noteMessage: "",
  yes: "Continue",
  no: "Cancel",
  visible: false,
  annotationPopUp: null,
  url: null,
  mode: "",
  init: function () {
    var self = this;
    Ember.$(document).on('keydown.confirmDialog', function (e) {
      if (e.keyCode == 27) {
        self.send('closeConfirmDialog');
      }
    });
  },
  actions: {
    confirm: function () {
      switch (this.mode) {
        case AppConstants.ConfirmDialogMode.PhaseComplete:
          this.phaseCompleteAction();
          break;
        case AppConstants.ConfirmDialogMode.ChangesComplete:
          this.changesCompleteAction();
          break;
        case AppConstants.ConfirmDialogMode.AnnotationDoesNotFit:
          this.annotationDoesNotFitAction();
          break;
        case AppConstants.ConfirmDialogMode.AnnotationChanges:
        case AppConstants.ConfirmDialogMode.ExternalUserClose:
          this.baseDialogAction();
          break;
        default:
          break;
      }
    },
    cancel: function () {
      this.send('closeConfirmDialog');
    }
  },

  phaseCompleteAction: function () {
    var defer = Ember.RSVP.defer(),
      self = this;

    defer.promise.then(function(){
        self.send('closeConfirmDialog');
        window.location.reload();
      });

    this.controllerFor('approval').changeApprovalDecision(defer);
  },

  baseDialogAction: function () {
    var popUp = this.get('annotationPopUp');
    if (popUp) {
      popUp.send('hideCommentPopup');
    }
    var url = this.get('url');
    if (url) {
      location.href = url;
    }

    this.send('closeConfirmDialog');
  },

  changesCompleteAction: function() {
    var apiUrl = window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/job-changescomplete?key=' + window.AppkitENV.key + '&user_key=' +
      window.AppkitENV.user_key + '&is_external_user=' + window.AppkitENV.is_external_user;

    Ember.$.ajax({
      type: "POST",
      url: apiUrl,
      data: JSON.stringify({ pageId : this.get('model')}),
      contentType: "application/json; charset=utf-8",
      cache: false,
      dataType: 'json'
    });

    this.send('closeConfirmDialog');
  },

  annotationDoesNotFitAction: function() {
    this.get('controllers.image/controls/image').processAnnotationCopy();

    this.send('closeConfirmDialog');
  }
});
export default ConfirmationModalController;
