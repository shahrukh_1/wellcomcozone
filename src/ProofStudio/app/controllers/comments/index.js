import Ember from "ember";
import AppConstants from "appkit/config/constants";
import UserPermissions from 'appkit/routes/mixins/user-permissions';

var CommentsIndexController = Ember.ArrayController.extend(UserPermissions, {
    needs: ['comments/item','sidebar/index', 'navigation/annotation','approval','application', 'multiple-upload', 'navigation/custom-profiles/index', 'checklisthistory-dialog'],
    annotations: Ember.computed.alias('annotationsSortedByIndexWithFilterFlag'),
    navigationAnnotation: Ember.computed.alias('controllers.navigation/annotation'),
    currentVersion: Ember.computed.alias('controllers.approval.currentVersion'),
    isDrawSelected:  Ember.computed.alias('controllers.navigation/annotation.isDrawSelected'),
    currentSPP : Ember.computed.alias('controllers.navigation/custom-profiles/index.currentSPP'),
    navigationMain: Ember.computed.alias('controllers.navigation/main'),
    totalChecklistItems: Ember.computed.alias('controllers.checklisthistory-dialog.totalChecklistItems'),
    newComment:'',
    oldComment:'',
    globalQuill: '',
    globalEditQuill: '',
    commentType: AppConstants.CommentTypes.MARKER_COMMENT,
    approvalChecklistItems: [],
    approvalUsers: [],
    annotationID: null,
    canEnableSaveBtn: false,
    checklistHistoryCount: null,
    selectedUserToWhomMailShouldBeSent: [],
    savedAnnotationChecklist: [],
    tempAnnotationChecklist: [],
    visible: false,
    closeUsersList: false,
    checklistName: null,
    annotationPasted: false,
    isSideAnnotation: false,
    canPaste: false,
    isChecklistHasItems: false,
    copiedAnnotationsAndAttachments: null,
    copiedAnnotationChecklistItems: null,
    attachmentsToPaste: [],
    textToPaste: null,
    index: [],
    selectedUserIndex: [],
    isCommentSaving: false,
    selected: false,
    collaborator: null,
    marker_X: 0,
    marker_Y: 0,
    popup_X: 0,
    popup_Y: 0,
    svgAnnotations: {},
    annotationsChanged: new Date(),

   
    canAddAttachments: function(){
        var self = this;
        if(window.AppkitENV.approvalType === "Zip")
        {
        var currentPage = self.store.all('page').filterBy('id', self.get('pageId')).get('firstObject');
        
            if(window.AppkitENV.selectedHtmlPageId === "0"){
            var array =  this.get('orderNumberOfHtmlPage');
            currentPage = self.store.all('page').content[array];
            } else {
                currentPage = self.store.all('page').filterBy('id',window.AppkitENV.selectedHtmlPageId ).get('firstObject');
            }
        
            const message = JSON.stringify({
                message: 'EventCall',
            });

            if(currentPage != undefined) {
                var  iframeElement = document.getElementById(currentPage._data.id);
                iframeElement.contentWindow.postMessage(message, '*');
            }
        }
        var loggedUserId = this.get('session.user.id');
        return (
                  !this.get('model.firstObject') ||
                  this.get('model.firstObject.Creator.id') === loggedUserId ||
                  this.store.all('internal-collaborator').content.filterBy('id',this.get('model.firstObject.Creator.id')).length > 0 ||
                  this.get('model.firstObject.ExternalCreator.id') === loggedUserId
               ) &&
               (
                  !this.get('model.firstObject.AnnotationAttachments') ||
                  this.get('model.firstObject.AnnotationAttachments.length') < 5
               ) && ((!this.get('attachments') || this.get('attachments.length') < 5 ));
    }.property('model.firstObject.Creator', 'model.firstObject.ExternalCreator', 'model.firstObject.AnnotationAttachments.length', 'attachments.length'),

    canDeleteAttachments: function(){
        var loggedUserId = this.get('session.user.id');
        return (
              !this.get('model.firstObject') ||
              this.get('model.firstObject.Creator.id') === loggedUserId ||
              this.get('model.firstObject.ExternalCreator.id') === loggedUserId
              );
    }.property('canAddAttachments'),

    annotationId: function(){
        return this.get('model.firstObject.id');
    }.property('model'),

    isAnnotationPasted:function(){
       return this.get('annotationPasted');
     }.property('model','annotationPasted'),

     checkUserisRemoved:function(){
     var selectedUser = this.get('selectedUserToWhomMailShouldBeSent');
     if(!(selectedUser.length === 0) && !this.get('isCommentSaving')){
        var editor = Ember.$('.ql-editor');
        var comment = null;
        if(editor.length > 0){
            comment = editor[0].innerHTML;
        } else {
        comment = this.get('newComment');
        }
        var index = this.get('selectedUserIndex');
        for(var i = 0; i < selectedUser.length; i++){
            var selectedUserName = selectedUser[i].Name;
            var userindex = index[i] + 1;
            var userNameFromComment ="";
            var count = selectedUserName.length;
            if(comment.length >= userindex + count - 1){
                for(var q = 1; q <= count; q++){
                    userNameFromComment = userNameFromComment + comment[userindex + q];
                }
                if(selectedUserName === userNameFromComment){
                    return true;
                }else{
                    selectedUser.removeObject(selectedUser[i]);
                    index.removeObject(index[i]);
                }
            }else{
                selectedUser.removeObject(selectedUser[i]);
                index.removeObject(index[i]);
            }
        }
    }
     },

     canShowNoOfChanges:function(){
     return this.get('isSideAnnotation');
     }.property('isSideAnnotation'),

     canShowChecklistHistory:function(){
      if(this.get('checklistHistoryCount') > 0)
      {
          return true;
      }else{
          return false;
      }
     }.property('checklistHistoryCount'),
    canShowApprovalUsers:function(){
        if(this.get('closeUsersList')){
            this.set('closeUsersList',false);
         return false;
        }else{
     var approvalUsersDetails = [];
     var editor = Ember.$('.ql-editor');
     var comment = this.get('newComment');
     if(comment[comment.length - 1] === "@"){
            var index = this.get('index');
            if(editor.length > 0){
             var str = editor[0].innerHTML;
             index.addObject(str.indexOf('@<') - 1);
            } else {
            index.addObject(comment.length - 1);
            }
        this.set('index',index);
        var internalusers = this.store.all('internal-collaborator');
        var externalUsers = this.store.all('external-collaborator');
        if(externalUsers.get('length') > 0){
        externalUsers.forEach(function(user){
        if(user.get('IsAcessRevoked') == false){
        var allusers = {id: user.id, Name: user.get('GivenName')+" "+user.get('FamilyName'), bgColor: user.get('bgColor'), isExternal: true};
        approvalUsersDetails.pushObject(allusers);
        }
        });
        }
        internalusers.forEach(function(user){
        if(user.get('IsAcessRevoked') == false && user.get('UserStatus') == false){
        var allusers = {id: user.id, Name: user.get('GivenName')+" "+user.get('FamilyName'), bgColor: user.get('bgColor'), isExternal: false};
        approvalUsersDetails.pushObject(allusers);
        }
        });
        this.set('approvalUsers',approvalUsersDetails);
        this.set('selected', false);
      return true;
     }
     var index = this.get('index');
     var indexat = index[index.length - 1];
     if((!this.get('selected'))&&!(comment[indexat + 1] == "") && (comment[indexat] === "@")){
        var userNameFromComment ="";
        var count = comment.length - indexat;
        for(var i = 1; i < count; i++){
            userNameFromComment = userNameFromComment + comment[indexat + i];
        }
        var internalUsers = this.store.all('internal-collaborator');
        var externalUsers = this.store.all('external-collaborator');
        if(externalUsers.get('length') > 0){
        externalUsers.forEach(function(user){
        if(user.get('IsAcessRevoked') == false){
        var allusers = {id: user.id, Name: user.get('GivenName')+" "+user.get('FamilyName'), bgColor: user.get('bgColor'), isExternal: true};
        approvalUsersDetails.pushObject(allusers);
        }
        });
        }
       internalUsers.forEach(function(user){
       if(user.get('IsAcessRevoked') == false){
       var allUsers = {id: user.id, Name: user.get('GivenName')+" "+user.get('FamilyName'), bgColor: user.get('bgColor'), isExternal: false};
       approvalUsersDetails.pushObject(allUsers);
       }
       });
       this.set('approvalUsers',approvalUsersDetails.filter(function(user){
           return user.Name.toLowerCase().indexOf(userNameFromComment.toLowerCase()) !== -1;
       })
       );
       if(this.get('approvalUsers').length > 0){
          return true;
       }else{
          return false;
       }
     }else{
         this.checkUserisRemoved();
         return false;
     }
    }
    }.property('newComment','closeUsersList'),

    isuserSelected: function(){
     var editor = Ember.$('.ql-editor');
     var comment = null;
     if(editor.length > 0)
     {
        comment = editor[0].innerHTML;
     }else {
     comment = this.get('newComment');
     }
     var index = this.get('index');
     var commentWithoutUserName ="";
     var userName = this.get('selectedUserToWhomMailShouldBeSent');
     if(userName.length > 0){
     if(userName[userName.length - 1].id > 0){
     for(var i = 0; i <= index[index.length - 1]; i++){
        commentWithoutUserName = commentWithoutUserName + comment[i];
       }
     this.set('selected', true);
     var editor = Ember.$('.ql-editor');
     editor[0].innerHTML = commentWithoutUserName + '@' +userName[userName.length - 1].Name;
     this.set('newComment',commentWithoutUserName + '@' +userName[userName.length - 1].Name);
     return true;
     }else{
     return false;
     }
    }else{
        return false;
    }
    }.observes('selectedUserToWhomMailShouldBeSent'),
    
    canShowPaste:function(){
        var self = this;
        var copiedAnnotation = self.get('copiedAnnotationsAndAttachments');
        if(copiedAnnotation){
         return true;
        }else{
         return false;
        }
        }.property('model'),

    iscanPaste:function(){
        var canPaste = this.get('canPaste');
        var sideAnnotations = this.get('isSideAnnotation');
        if(canPaste == true && sideAnnotations == false){
            return true;
        }
        else{
            return false;
        }
      }.property('model'),

    isSaveEnabled: function(){
        if(this.get('canEnableSaveBtn') || !this.get('newComment') == "")
        {
            return false;
        }else{
           return true;
        }
    }.property('canEnableSaveBtn', 'newComment'),

    isApproved: function() {
      var currentAnnotationStatus = this.get('model.firstObject.Status.id');
      var approvedStatus = this.store.all('annotation-status').filterBy('Key', 'A').get('firstObject.id');
      return currentAnnotationStatus === approvedStatus;
    }.property('model'),

    attachmentUploading: function(){
        var attachments = this.get('attachments');
        if (attachments){
            var attachmentsInProgress = attachments.filter(function(attachment){
                return attachment.get('inProgress') === true;
            });
            return attachmentsInProgress && attachmentsInProgress.length > 0;
        }
        else{
          return false;
        }
    }.property('attachments.@each.inProgress'),
    
    checkBoxClicked: function(){
        var approvalChecklistItems = this.get('approvalChecklistItems');
        var self = this;
        Ember.$('.czn-checklist').off('keyup.checklistAutoExpand');
        Ember.$('.czn-checklist').on('keyup.checklistAutoExpand', function(e) {
            setTimeout(function() {
            if(e.target.textLength > 0){
                e.target.style.cssText = 'height:auto; padding:0';
            e.target.style.cssText = 'height:' + e.target.scrollHeight + "px";
            self.set('canEnableSaveBtn', true);
            }
            if(e.target.textLength == 0){
                e.target.style.cssText = 'height:'+ 26 +"px";
                self.set('canEnableSaveBtn', true);
            }
        },500);
       }); 
        approvalChecklistItems.forEach(function(member, index, enumerable){
          if(member.ItemValue !== null && member.ItemValue !== ""){
              Ember.set(self.get('approvalChecklistItems').objectAt(index),'IsItemChecked', true)
          }else{
              Ember.set(self.get('approvalChecklistItems').objectAt(index),'IsItemChecked', false)
              }
        });
    }.observes('approvalChecklistItems.@each.ItemValue'),

    initCollaborator: function(){
        this._super();
        var user = this.get('session.user');
        var self = this;
        if(user.get('UserIsExternal')) {
            this.store.find('external-collaborator', user.get('id')).then(function(externalCollaborator) {
                self.set('collaborator', externalCollaborator);
            });
        }
        else
        {
            this.store.find('internal-collaborator', user.get('id')).then(function(internalCollaborator) {
                self.set('collaborator', internalCollaborator);
            });
        }
        var self = this;
        Ember.run.later(function(){
            var quillToolBar = Ember.$('.ql-toolbar');
            if(quillToolBar.length === 0)
            {
            var toolbarOptions = [
                ['bold', 'italic', 'underline']
              ];
              var globalQuill = self.get('globalQuill');
              globalQuill = new Quill('#commentArea', {
                modules: {
                    toolbar: toolbarOptions
                  },
                theme: 'snow'
               });
               self.set('globalQuill', globalQuill);
            }
        });
    }.observes('model'),

    actions: {
        commenthandle: function(){
          var s = 10;
        },
        getCommentContent: function() {
            var Comment = Ember.$('#commentArea')[0].textContent;
            var oldComment = Ember.$('#commentArea')[0].children[0].innerHTML;
            this.set('oldComment', oldComment);
            this.set('newComment', Comment);
        },

        
        addLineInComment: function(){
         var globalQuill =  this.get('globalQuill');
            globalQuill.focus();
            var caretPosition = globalQuill.getSelection(true);
            globalQuill.clipboard.dangerouslyPasteHTML(caretPosition.index, '<p><br></p><p><br></p>');
        },

        selectUser: function(userDetails){
        var s  = this.get('selectedUserToWhomMailShouldBeSent');
        s.addObject(userDetails);
        var q = this.get('index');
        var p = this.get('selectedUserIndex');
        p.addObject(q[q.length - 1]);
        this.isuserSelected();
        },

        hide: function() {
            if(this.get('visible')) {
                this.hideDrawingOptions();
            }
        },
        hideCommentPopupAfter: function(comment){
            var tempAnnotationChecklists = this.get('tempAnnotationChecklist');
            if( comment.length > 0 && tempAnnotationChecklists.length > 0)
            {
            tempAnnotationChecklists.forEach(function(origignalitem){
                comment[0].get('ApprovalannotationChecklistitems').content.forEach(function(tempitem){
                 if(tempitem._data.id === origignalitem.ItemID && !(tempitem._data.ItemValue === origignalitem.ItemValue))
                 {
                Ember.set(tempitem._data, 'ItemValue', origignalitem.ItemValue);
                 }
                })
            });
        }
            this.send('hideCommentPopup');
        },

        hideCommentPopup: function(){
            if(this.get('visible')) {
                this.get('navigationAnnotation').set('isDrawSelected', false);
                this.get('navigationAnnotation').set('isSoftProofingSelected', false);
                this.hideDrawingOptions();
            }
            var multipleUploadController = this.get('controllers.multiple-upload');
            if (multipleUploadController){
                multipleUploadController.get('newFiles').clear();
            }
            this.set('attachments', []);
            this.send('hideCanvas');
            this.send('removeMarkerOverlay');
            this.set('textToPaste',null);
            this.set('attachmentsToPaste', []);
            this.set('isSideAnnotation', false);
            this.set('annotationPasted', false);
            this.set('isChecklistHasItems', false);
            this.set('approvalChecklistItems',[]);
            this.set('savedAnnotationChecklist',[]);
            this.set('selectedUserToWhomMailShouldBeSent',[]);
            this.set('selected', false);
            this.set('index', []);
            this.set('selectedUserIndex',[]);
            this.set('canEnableSaveBtn', false);
            this.set('tempAnnotationChecklist', []);
            this.set('checklistHistoryCount', null);
            this.set('annotationID', null);
            // if(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP){
            //     Ember.run.later(function(){
            //     Ember.$(".pinsvideo-layer").css('display','none');
            //     });
            // }
        },
        pasteAnnotation: function(){
            var self = this;
            var copiedAnnotationsAndAttachments = self.get('copiedAnnotationsAndAttachments');
            var copiedAttachments = copiedAnnotationsAndAttachments.get('AnnotationAttachments');
            if(copiedAttachments){
                copiedAttachments.forEach(function (attachment){
                if(attachment.get('id') === 0){
                    copiedAttachments.removeObject(attachment);
                }
                });
            }
            self.set('attachmentsToPaste',copiedAttachments);
            self.set('textToPaste',copiedAnnotationsAndAttachments.get('Comment'));
            self.set('isAttachmentBinded', true);
            self.set('annotationPasted', true);
            var listOfCheckListItems = [];
           var annotationChecklistItems = self.get('copiedAnnotationChecklistItems');
            var arrayOfCheckList = copiedAnnotationsAndAttachments._data.Page.store.all("approval-checklist").content
          arrayOfCheckList.forEach(function(content){
            if(content._data.ChecklistItem != undefined){
                if(content._data.ChecklistItem.length > 0){
                    self.set('isChecklistHasItems', true);
                  listOfCheckListItems.pushObjects(content._data.ChecklistItem);
                }
             }  
            });
            var listOfSortedCheckListItems=[];
            listOfSortedCheckListItems.pushObjects( listOfCheckListItems);
                if(annotationChecklistItems.length > 0){
                    listOfCheckListItems.forEach(function(listitem, index){
                    Ember.set(listOfSortedCheckListItems.objectAt(index),'ItemValue', null)
                    Ember.set(listOfSortedCheckListItems.objectAt(index),'IsItemChecked', false)
                    annotationChecklistItems.forEach(function(itemvalcontent){
                    if(listitem.Id == itemvalcontent.Id){
                        listOfSortedCheckListItems.removeAt(index);
                        listOfSortedCheckListItems.insertAt(index,itemvalcontent);
                        }
                    })
                    })
                }
               self.set('approvalChecklistItems',listOfSortedCheckListItems);
        },
        hideCommentPopupFromClose: function(){
          var isUnSavedAnnotation = this.get('isUnSavedAnnotation');

          if (isUnSavedAnnotation == true) {
            this.send('showConfirmDialogModal');
          }
          else {
            this.send('hideCommentPopup');
          }
        },
        show: function() {
            this.set('visible', true);
            this.send('showComment');
            this.set('newComment', '');
            this.set('oldComment', '');
            this.set('attachments', []);
        },
        getChecklistItems: function(comment){
            var checklistItemsClass = comment[0].get('AnnotationAllchecklistitems').content;
            var checklistItemValues = [];
            checklistItemsClass.forEach(function(checklistItem){
                checklistItemValues.push(checklistItem._data);
            });
            this.send('hideCommentPopup');  
            this.set('totalChecklistItems', checklistItemValues);
            this.send('showChecklistItemsHistoryDialog');
            
        },
        addAnnotation: function()
        {
            var self = this;
            this.set('isCommentSaving', true);
            var user = this.get('session.user');
            var quill = new Quill('#commentArea');
            var commentContent = quill.container.childNodes[0].innerHTML;
            var parentAnnotation = this.get("model.firstObject");
            var phaseId = this.store.all('approval-basic').content[0].get('CurrentPhaseId');
            var FirstPageId = this.store.all('approval-basic').content[0].get('FirstPageId');
            var annotationData = null;
            var isSideAnnotation = self.get('isSideAnnotation');
            var annotationPasted = self.get('annotationPasted');
            var approvalChecklistItems = self.get('approvalChecklistItems');
            var savedAnnotationChecklist = self.get('savedAnnotationChecklist');
            var selectedUserDetails = self.get('selectedUserToWhomMailShouldBeSent');
            var point = this.get('point');
            var timeFrame = Ember.$("video")[0] == undefined? null : Ember.$("video")[0].currentTime;
            var svg = this.get('svg');
            var isVideo = window.AppkitENV.approvalType == "Movie" ? true : false;
            var commentType = this.get('commentType');
            var currentSPP = this.get('currentSPP');
            var isRGB = this.get('currentViewerController.model.IsRGBColorSpace');
            var isImage = this.get('currentViewerController.currentVersion.ApprovalType') == "Image";
            var SPPsessionGUID = (!isRGB && isImage) ? currentSPP.SessionGuid : null;
            var calibrationStatus = 0;
            if(this.get('navigationMain.isOnCalibratedMonitor')) {
              calibrationStatus = parseInt(this.get('session.softproofingstatus.Level'));
            }
            else {
              calibrationStatus = 2;
            }

            var currentPage = self.store.all('page').filterBy('id', self.get('pageId')).get('firstObject');
            if(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP)
            {
                if(window.AppkitENV.selectedHtmlPageId === "0"){
                var array =  this.get('orderNumberOfHtmlPage');
                currentPage = self.store.all('page').content[array];
                } else {
                    currentPage = self.store.all('page').filterBy('id',window.AppkitENV.selectedHtmlPageId ).get('firstObject');
                }
            }
            if (parentAnnotation) {
                annotationData = {
                    Comment: commentContent,
                    base64: window.AppkitENV.html2CanvasStringBase64 ,
                    Parent: parentAnnotation,
                    PageNumber: parentAnnotation.get('PageNumber'),
                    Status: parentAnnotation.get('Status'),
                    Page: parentAnnotation.get('Page'),
                    CommentType: parentAnnotation.get('CommentType'),
                    SPPsessionGUID: SPPsessionGUID,
                    CalibrationStatus: calibrationStatus,
                    Phase: phaseId,
                    IsPrivateAnnotation: true,
                    IsVideo: isVideo,
                    IsSamePage: parentAnnotation.get('Page.content._data.id') === FirstPageId ? true : false
                };
            }
            else{
                // add a new annotation with no parent - this annotation will be displayed in the right sidebar
                if (point){

                    var highlightType = null;
                    var highlightData = null;
                    if (commentType === AppConstants.CommentTypes.TEXT_COMMENT) {
                        highlightType = this.get('HighlightType') || 1;
                        highlightData = this.get('HighlightData');
                    }
                    var startIndex = this.get('StartIndex') || 0;
                    var endIndex = this.get('EndIndex') || 0;
          
                    var neutralStatus = self.store.all('annotation-status').filterBy('Key', 'N').get('firstObject');
                    annotationData = {
                        Comment: commentContent,
                        Parent: null,
                        PageNumber: 0,
                        Status: neutralStatus,
                        Page: currentPage,
                        CommentType: commentType,
                        base64: window.AppkitENV.html2CanvasStringBase64,
                        CrosshairXCoord: parseInt(Math.abs(point[0] || point.x)),
                        CrosshairYCoord: parseInt(Math.abs(point[1] || point.y)),
                        StartIndex: startIndex,
                        EndIndex: endIndex,
                        OriginalVideoWidth: Ember.$("video").width(),
                        OriginalVideoHeight: Ember.$("video").height(),
                        TimeFrame: (timeFrame * 1000000),
                        ReferenceFilePath: '',
                        HighlightType: highlightType,
                        HighlightData: highlightData,
                        SPPsessionGUID: SPPsessionGUID,
                        CalibrationStatus: calibrationStatus,
                        Phase: phaseId,
                        IsPrivateAnnotation: true,
                        IsVideo: isVideo,
                        IsSamePage: currentPage._data.id === FirstPageId ? true : false
                    };
                }
            }

            if (annotationData) {
              annotationData.AnnotatedOnImageWidth = annotationData.Page.get('OutputImageWidth');
                if (user.get('UserIsExternal')) {
                    this.store.find('external-collaborator', user.get('id')).then(function (externalCollaborator) {
                        annotationData.ExternalCreator = externalCollaborator;
                        annotationData.ExternalModifier = externalCollaborator;
                        var newlist =[];
                        if(isSideAnnotation == true){
                         savedAnnotationChecklist.forEach(function(item){
                             if(item._data == undefined){
                               if(item.IsItemChecked == true){
                                   newlist.pushObject(item);
                               }
                             }else{
                          if(item.get('IsItemChecked') == true){
                             item._data.ID = 0;
                             item._data.ItemValue = item.get('ItemValue');
                              newlist.pushObject(item);
                          }
                         }
                         });
                    }else{
                       if(annotationPasted == true){
                         approvalChecklistItems.forEach(function(item){
                         if(item.IsItemChecked == true){
                                if(!(item.id == 0)){
                                item.ID = 0;
                              }
                                newlist.pushObject(item);
                         }
                         }); 
                     }
                     else{
                         approvalChecklistItems.forEach(function(item){
                             if(item.IsItemChecked == true){
                                 if(item.ID == !0){
                                 item._data.ID = 0;
                                 }
                                 newlist.pushObject(item);
                             }
                             }); 
                     }
                 }
                         annotationData.Checklist = JSON.stringify(newlist);
                         if(selectedUserDetails)
                         {
                             annotationData.SelectedUserDetails = JSON.stringify(selectedUserDetails);
                         }
                        var comment = self.store.createRecord('annotation', annotationData);
                        if(commentContent === "")
                        {
                            comment.id = self.get('annotationId');
                        }
                        var attachments = self.get('attachments');
                        if (attachments) {
                          attachments.forEach(function (attachment) {
                            comment.get('AnnotationAttachments').pushObject(attachment);
                          });
                        }

                        if(svg){
                            var annotationMarkups = self.createAnnotationsMarkups(svg);
                            comment.get('shapes').pushObjects(annotationMarkups);
                        }
                        comment.save().then(function (returnObject) {
                            self.set('newComment', '');
                            self.set('oldComment', '');
                            if(returnObject.get('IsDifferentPhase') === true){
                                self.send('showPhaseIsDifferentDialog');
                             } else {
                            if(self.get('model').length > 0){
                            if(returnObject.get('ApprovalannotationChecklistitems').content.length > 0)
                            {
                                Ember.set(self.get('model')[0].get('ApprovalannotationChecklistitems'), 'content', returnObject.get('ApprovalannotationChecklistitems').content);
                                Ember.set(self.get('model')[0].get('AnnotationAllchecklistitems'), 'content', returnObject.get('AnnotationAllchecklistitems').content);
                            }
                                var annotationId = self.get('model')[0].get('id');
                                var ParentId = 0;
                                if(returnObject.get('Parent') != null)
                                {
                                  ParentId = returnObject.get('Parent').get('id');
                                }
                                if(annotationId == ParentId){
                                self.get('model').addObject(returnObject);
                            }
                        }else{
                                self.get('model').addObject(returnObject);
                        }  
                    }  
                            if (point){
                                self.send('hide');
                            }
                            self.set('svg', null);
                            var multipleUploadController = self.get('controllers.multiple-upload');
                            if (multipleUploadController){
                                multipleUploadController.get('newFiles').clear();
                            }
                            self.get('currentViewerController').set('annotationsChanged', new Date());
                            self.set('annotationsChanged', new Date());
                            self.set('approvalChecklistItems',[]);
                        });
                    });
                }
                else {
                    this.store.find('internal-collaborator', user.get('id')).then(function (internalCollaborator) {
                        annotationData.Creator = internalCollaborator;
                        annotationData.Modifier = internalCollaborator;
                        var newlist =[];
                       if(isSideAnnotation == true){
                        savedAnnotationChecklist.forEach(function(item){
                            if(item._data == undefined){
                              if(item.IsItemChecked == true){
                                  newlist.pushObject(item);
                              }
                            }else{
                         if(item.get('IsItemChecked') == true){
                            item._data.ID = 0;
                            item._data.ItemValue = item.get('ItemValue');
                             newlist.pushObject(item);
                         }
                        }
                        });
                   }else{
                      if(annotationPasted == true){
                        approvalChecklistItems.forEach(function(item){
                        if(item.IsItemChecked == true){
                            if(!(item.id == 0)){
                            item.ID = 0;
                         }
                            newlist.pushObject(item);
                        }
                        }); 
                    }
                    else{
                        approvalChecklistItems.forEach(function(item){
                            if(item.IsItemChecked == true){
                                if(item.ID == !0){
                                item._data.ID = 0;
                                }
                                newlist.pushObject(item);
                            }
                            }); 
                    }
                }
                        annotationData.Checklist = JSON.stringify(newlist);
                        if(selectedUserDetails)
                        {
                            annotationData.SelectedUserDetails = JSON.stringify(selectedUserDetails);
                        }        
                        var comment = self.store.createRecord('annotation', annotationData);
                        if(commentContent === "")
                        {
                            comment.id = self.get('annotationId');
                        }
                        var attachments = self.get('attachments');
                        if (attachments) {
                            attachments.forEach(function (attachment) {
                                comment.get('AnnotationAttachments').pushObject(attachment);
                            });
                        }

                        if(svg){
                            var annotationMarkups = self.createAnnotationsMarkups(svg);
                            comment.get('shapes').pushObjects(annotationMarkups);
                        }
                        comment.save().then(function (returnObject) {
                            self.set('newComment', '');
                            self.set('oldComment', '');
                            if(returnObject.get('IsDifferentPhase') === true){
                               self.send('showPhaseIsDifferentDialog');
                            } else {
                        if(self.get('model').length > 0){
                            if(returnObject.get('ApprovalannotationChecklistitems').content.length > 0)
                            {
                                Ember.set(self.get('model')[0].get('ApprovalannotationChecklistitems'), 'content', returnObject.get('ApprovalannotationChecklistitems').content);
                                Ember.set(self.get('model')[0].get('AnnotationAllchecklistitems'), 'content', returnObject.get('AnnotationAllchecklistitems').content);
                            }

                            var annotationId = self.get('model')[0].get('id');
                                var ParentId = 0;
                                if(returnObject.get('Parent') != null)
                                {
                                  ParentId = returnObject.get('Parent').get('id');
                                }
                                if(annotationId == ParentId){
                                self.get('model').addObject(returnObject);
                                }
                            }else{
                                self.get('model').addObject(returnObject);
                        }  
                            var files = returnObject.get('AnnotationAttachments').filter(function(attachment){
                                return !attachment.get('id');
                            });
                            returnObject.get('AnnotationAttachments').removeObjects(files);
                        }
                            if (point){
                                self.send('hide');
                            }
                            self.set('svg', null);
                            var multipleUploadController = self.get('controllers.multiple-upload');
                            if (multipleUploadController){
                                multipleUploadController.get('newFiles').clear();
                            }
                            self.get('currentViewerController').set('annotationsChanged', new Date());
                            self.set('annotationsChanged', new Date());

                            self.set('approvalChecklistItems',[]);
                            self.set('selectedUserToWhomMailShouldBeSent',[]);
                            self.set('index', []);
                            self.set('selectedUserIndex',[]);                            
                            class Event {
                                constructor(name, obj) {
                                    this.event = name;
                                    this.obj_ = obj;
                                }
                            }
                            class AddApprovalAnnotation {
                                constructor(approval_guid, approvalAnnotationId , user_name , is_external_user) {
                                    this.approval_guid = approval_guid;
                                    this.approvalAnnotationId = approvalAnnotationId;
                                    this.user_name = user_name;
                                    this.is_external_user = is_external_user;
                                }
                            }
                            var userguid = window.Appkit.user_key
                            const addApprovalAnnotation_data = new AddApprovalAnnotation(window.AppkitENV.approval_guid, returnObject.id , window.Appkit.user_name , window.Appkit.is_external_user);
                            const event = new Event('AddAnnotation', addApprovalAnnotation_data);
                           window.parent.postMessage(event, '*'); 
                        });
                    });
                }
            }

            this.get('navigationAnnotation').set('isDrawSelected', false);
            this.get('navigationAnnotation').set('isSoftProofingSelected', false);
            this.send('clearCanvas');
            this.send('hideCanvas');
            this.send('removeMarkerOverlay');
            this.hideDrawingOptions();
            this.set('canPaste',false);
            this.set('textToPaste',null);
            this.set('attachmentsToPaste', []);
            this.set('annotationPasted', false);
            this.set('isSideAnnotation', false);
            this.set('isChecklistHasItems', false);
            this.set('selected', false);
            this.set('isCommentSaving', false);
            this.set('canEnableSaveBtn', false);
            this.set('annotationID', null);
            // if(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP){
            //     Ember.run.later(function(){
            //     Ember.$(".pinsvideo-layer").css('display','none');
            //     });
            // }
         // this.set('approvalChecklistItems', []);
        },
        nextAnnotation: function(){
            if (this.get('nextDisabled')){
                return false;
            }
            var annotations = this.get('annotations').filter(function(annotation)
            {
                return annotation.get('isShown') === true;
            });

            var self = this;
            annotations.any(function(annotation, index){
                if (annotation.get('selected'))
                {
                    if (annotations.length - 1 !== index)
                    {
                        var tempAnnotationChecklist = self.get('tempAnnotationChecklist');
                        if(tempAnnotationChecklist.length > 0)
                        {
                            tempAnnotationChecklist.forEach(function(origignalitem){
                                annotation.get('ApprovalannotationChecklistitems').content.forEach(function(tempitem){
                                    if(tempitem._data.id === origignalitem.ItemID && !(tempitem._data.ItemValue === origignalitem.ItemValue))
                                    {
                                   Ember.set(tempitem._data, 'ItemValue', origignalitem.ItemValue);
                                    }
                                });
                            });
                        }
                        self.send('selectAnnotation', annotations[index + 1]);
                        return true;
                    }
                }
            });
        },
        prevAnnotation: function(){
            if (this.get('prevDisabled')){
                return false;
            }
          var annotations = this.get('annotations').filter(function(annotation)
          {
            return annotation.get('isShown') === true;
          });

            var self = this;
            annotations.any(function(annotation, index){
                if (annotation.get('selected'))
                {
                    if (index > 0)
                    {
                        var tempAnnotationChecklist = self.get('tempAnnotationChecklist');
                        if(tempAnnotationChecklist.length > 0)
                        {
                            tempAnnotationChecklist.forEach(function(origignalitem){
                                annotation.get('ApprovalannotationChecklistitems').content.forEach(function(tempitem){
                                    if(tempitem._data.id === origignalitem.ItemID && !(tempitem._data.ItemValue === origignalitem.ItemValue))
                                    {
                                   Ember.set(tempitem._data, 'ItemValue', origignalitem.ItemValue);
                                    }
                                });
                            });
                        }
                        self.send('selectAnnotation', annotations[index - 1]);
                        return true;
                    }
                }
            });
        },
        setMarkerPosition: function(x, y){
            //the current position of marker
            this.set('marker_X', x);
            this.set('marker_Y', y);
            if(this.get('annotationID') != null && window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP){
               this.redrawcommentMarkerLine(y);
            }
        }
    },

    createAnnotationsMarkups:function(shapes){
        var markups = [],
            self = this,
            scales = this.get('scales');

        shapes.forEach(function(shape, j){
            for ( var i = 0; i < scales.length; i++){
                if(j === i)
                {
                    var newShape =  self.store.createRecord('custom-shape', {
                        FXG: '',
	                      IsCustom: 1,
                        SVG: shape,
                        ApprovalAnnotation: null,
                        BackgroundColor: '#ff0000',
                        Color: '#ff0000',
                        Rotation: 0,
                        ScaleX: scales[i].scaleX,
                        ScaleY: scales[i].scaleY,
                        Size: 0,
                        X: 1,
                        Y: 1
                    });
                    markups.pushObject(newShape);
                    break;
                }
            }
        });
        return markups;
    },

    prevDisabled: function(){
      var annotations = this.get('annotations').filter(function(annotation)
      {
        return annotation.get('isShown') === true;
      });

        return annotations.any(function(annotation, index){
            return !!(annotation.get('selected') && index === 0);
        });
    }.property('annotations.@each.selected', 'annotations'),

    nextDisabled: function(){
      var annotations = this.get('annotations').filter(function(annotation)
      {
        return annotation.get('isShown') === true;
      });

        return annotations.any(function(annotation, index){
            return !!(annotation.get('selected') && index === annotations.length - 1);
        });
    }.property('annotations.@each.selected', 'annotations'),

    currentAnnotation: function(){
        var annotations = this.get('annotations');
        var currentIndex = 0;
        annotations.any(function(annotation, index){
            if (annotation.get('selected'))
            {
                currentIndex = index + 1;
            }
        });
        return currentIndex;
    }.property('annotations.@each.selected'),

    totalAnnotations: function(){
        return this.get('annotations').length;
    }.property('annotations.@each.selected'),

    drawMarkerLine: function(){
        
      var degrees = this.degreesFromThreeOclock(this.get('marker_X'), this.get('popup_X'), this.get('marker_Y'), this.get('popup_Y'));
      //the width of the line
      var width = this.calculateWidth(this.get('marker_X'), this.get('popup_X'), this.get('marker_Y'), this.get('popup_Y'));
      var offsetLeft = this.get('marker_X') + 4;
      if(Ember.$('#projectfolder').length > 0){
        offsetLeft = offsetLeft - Ember.$('#projectfolder')[0].offsetWidth; 
      }
      if(this.get('marker_X') > 0 && this.get('marker_Y') > 0 &&
         this.get('popup_X') > 0 && this.get('popup_Y') > 0) {
         //create the line based on calculated values
         Ember.$('#czn-marker-line').css({
             width: parseInt(width) - 2,
             'transform-origin': '0px 0px',
             '-webkit-transform-origin': '0px 0px',
             '-ms-transform-origin': '0px 0px',
             transform: 'rotate(' + parseInt(degrees) + 'deg)',
             '-webkit-transform': 'rotate(' + parseInt(degrees) + 'deg)',
             '-ms-transform': 'rotate(' + parseInt(degrees) + 'deg)',
             top: this.get('marker_Y') - 45,
             left: offsetLeft
             //left: this.get('marker_X') + 4//add 4px in order to set the line position to start from center of marker
         });
       }
    }.observes('popup_X','popup_Y','marker_Y','marker_X'),

    calculateWidth: function(marker_x, popup_X, marker_y, popup_y){
        //calculate the width of the line between marker and comments popup
        return Math.sqrt(Math.pow(Math.abs(marker_x - popup_X), 2) + Math.pow(Math.abs(marker_y - popup_y), 2));
    },

    degreesFromThreeOclock: function (marker_x, popup_X, marker_y, popup_y) {
        //calculate the angle(theta)
        var theta = Math.atan2(popup_y - marker_y, popup_X - marker_x);

        //theta must be positive
        if (theta < 0) {
            theta += 2 * Math.PI;
        }
        //convert to degrees and rotate so 0 degrees = 3 o'clock
        var degrees = (theta * 180 / Math.PI) % 360;
        degrees = (degrees > 89 && degrees < 220) ? degrees : (degrees + 1);

        return degrees;
    },

    isCopiedCommentEmpty: function(){
        var self = this;
        if(this.get('textToPaste')){
            Ember.run.later(function(){
                var quillToolBar = Ember.$('.ql-toolbar');
                if(quillToolBar.length === 0)
                {
                var toolbarOptions = [
                    ['bold', 'italic', 'underline']
                  ];
                var quill = new Quill('#commentArea', {
                    modules: {
                        toolbar: toolbarOptions
                      },
                    theme: 'snow'
                   });
                   quill.container.childNodes[0].innerHTML = self.get('textToPaste');
                }
            });
            return true;
        }
        else{
            return false;
        }
       }.property('textToPaste'),

    isCheckList: function(){
        
         var currentVersion = this.get('controllers.approval.currentVersion');
         var checklistItems = currentVersion.get("Pages")[0].store.all('approval-checklist');
         var list = [];
         var name = null;
         var arrayContent = checklistItems.get('content');
          arrayContent.forEach(function(content){
            if(content._data.ChecklistItem != undefined)
            {
                if(content._data.ChecklistItem.length > 0){
                    name = content._data.Name;
                    list.pushObjects(content._data.ChecklistItem);
                }
             }
          });
          this.set('checklistName', name);       
        if(this.get('isSideAnnotation') == true){
            var savedAnnotationChecklist = this.get('savedAnnotationChecklist');
            this.set('approvalChecklistItems', savedAnnotationChecklist);
            if(savedAnnotationChecklist.length > 0){
                return true;
            }else{
                return false;
            }
        }else{
            if(this.get('isAnnotationPasted') == true && this.get('isChecklistHasItems') == true){
                return true;
            }else{
            var newList = [];
            list.forEach(function (member, index, enumerable){
                Ember.set(list.objectAt(index), 'ItemValue', null);
             Ember.set(list.objectAt(index),'IsItemChecked', false);
             Ember.set(list.objectAt(index), 'TotalChanges', 0);
             newList.pushObject(member);
                }); 
             this.set('approvalChecklistItems',newList);
             if(newList.length > 0){
                 return true;
             }else{
                 return false;
             }
            }
        }
        
    }.property('controllers.approval.currentVersion','approvalChecklistItems'),
    
    isCommentEmpty: function(){
        //disable the "Save" button if the comment box has no text entered
        if(this.get('newComment') === ''){
            return true;
        }
        return false;
    }.property('newComment'),
    isUnSavedAnnotation: function() {
      // if comments is not visible than there is not unsaved annotation
      var visible = this.get("visible");
      if (visible == false) {
        return false;
      }
      var parentAnnotation = this.get("model.firstObject");
      if (!(parentAnnotation)) {
        return true;
      }
      return !this.get('isCommentEmpty');

    }.property('visible','newComment','model.firstObject'),
    textareaAccessibility: function(){
        //disable text area for adding comment if the version is locked
        if(this.get('currentVersion.isLocked') && this.get('visible')){
            var self = this;
            Ember.run.later(function(){
                Ember.$('#commentArea').attr('disabled', true);
                self.set('newComment', '');
                self.set('oldComment', '');
            });
        }else{
            Ember.$('#commentArea').attr('disabled', false);
        }
    }.observes('visible','controllers.approval.currentVersion.isLocked'),

    hideDrawingOptions: function(){
        this.send('hideComment');
        this.set('visible', false);
        this.set('newComment', '');
        this.set('oldComment', '');
        this.get('navigationAnnotation').send('closeAddMarker');

        //hide all the buttons related with drawing
        this.get('navigationAnnotation').set('isDrawLineSelected', false);
        this.get('navigationAnnotation').set('isDrawShapeSelected', false);
        this.get('navigationAnnotation').set('isMoveShapeSelected', false);
        this.get('navigationAnnotation').set('isDrawRectangleSelected', false);
    },

    redrawcommentMarkerLine:function(markery){
        var self = this;
        //retrieves the current positions of the marker and the comment popup
        //in order to create a line between them
        Ember.run.later(function(){
          var popupOffset = Ember.$(".czn-comment-modal").offset();
          if (popupOffset) {
              self.set('popup_Y', popupOffset.top);
              self.set('popup_X', popupOffset.left);
          }
          if(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP)
            {
                if(popupOffset.top < 0)
                {
                    var updatedMarkerY = 55 - popupOffset.top + markery;
                    self.set('marker_Y', updatedMarkerY);
                    self.set('popup_Y', 55);
                }
            }
        });
    },

    reDrawCommentToMarkerLine: function(){
        var self = this;
        //retrieves the current positions of the marker and the comment popup
        //in order to create a line between them
        Ember.run.later(function(){
          var popupOffset = Ember.$(".czn-comment-modal").offset();
          if (popupOffset) {
              var popup_Y_value = self.get('popup_Y');
              if(popup_Y_value== popupOffset.top){
                popupOffset.top +=1;
              }
              self.set('popup_Y', popupOffset.top);
              self.set('popup_X', popupOffset.left);
          }
          if(window.AppkitENV.approvalType === AppConstants.ApprovalTypes.TYPE_ZIP && self.get('annotationID') === null)
            {
                if(popupOffset.top < 0)
                {
                    var updatedMarkerY = 55 - popupOffset.top + self.get('marker_Y');
                    self.set('marker_Y', updatedMarkerY);
                    self.set('popup_Y', 55);
                }
            }
        });
    }.observes('controllers.application.sidebarShown'),

    usersChanged: function(){
        //CZ-654
        if (this.get('model.firstObject.Creator.selected') === false){
            this.send('hideCommentPopup');
        }
    }.observes('controllers.sidebar/index.users.@each.selected')
});
export default CommentsIndexController;
