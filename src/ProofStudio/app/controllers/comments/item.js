import Ember from "ember";

var ItemController = Ember.ObjectController.extend({
    needs: ['approval', 'comments/index'],
    editCommentText:'',
    isEditing: false,
    neweditComment:'',
    oldeditComment:'',
    collaborator: null,
    updatedCommentText:'',
    userName: function(){
        var creator = this.get('Creator') || this.get('ExternalCreator');
        if(creator) {
            this.set('collaborator', creator);
            return creator.get('GivenName') + ' ' + creator.get('FamilyName');
        }
        return "";
    }.property('Creator', 'ExternalCreator'),

    userCanEditAnnotation: function(){
        var loggedUserId = this.get('session.user.id');
        return  (loggedUserId === this.get('Creator.id') || loggedUserId === this.get('ExternalCreator.id'));
    }.property('Creator', 'ExternalCreator', 'session.user.id'),

    userCanDeleteAnnotation: function(){
        var loggedUserId = this.get('session.user.id');
        var approval = this.get('controllers.approval.model');

        return   (loggedUserId === this.get('Creator.id') ||
                 loggedUserId === this.get('ExternalCreator.id'));
    }.property('Creator', 'ExternalCreator', 'session.user.id'),


    commentDateFormated: function(){
      var date = this.get("model").get('AnnotatedDate');
      Ember.run.later(function(){
          var commentTexts = Ember.$('.czn-comment-text');
          if(commentTexts.length > 0){
              for(var i = 0; i < commentTexts.length; i++ )
              {
                  if(commentTexts[i].innerHTML === "")
                  {
                    commentTexts[i].innerHTML = commentTexts[i].id;
                  }
              }
          }
      });
      return this.get('controllers.comments/index').formatDate(date);
    }.property('model.AnnotatedDate'),

    actions:{
        editcommenthandle: function(){
           
        },
        addLineInEditComment: function(){
            var oldcomment = this.get('oldeditComment').split('</p>');
            var newcomment = Ember.$('#editCommentArea')[0].children[0].innerHTML.split('</p>');
            var position = 0;
            var self = this;
 
            if(oldcomment.length != newcomment.length){
                for(var i = 0 ; i < newcomment.length; i++ ){
                    if(oldcomment[i] != newcomment[i]){
                        position = position + newcomment[i].length - 2;
                        break;
                    } else{
                        position = position + newcomment[i].length - 2;
                    }
                }
            }
            var globalEditQuill = self.get('controllers.comments/index.globalEditQuill');
            globalEditQuill.clipboard.dangerouslyPasteHTML(position, '<p><br></p><p><br></p>');
 
             var Comment = Ember.$('#editCommentArea')[0].textContent;
             var oldComment = Ember.$('#editCommentArea')[0].children[0].innerHTML;
             this.set('oldeditComment', oldComment);   
             this.set('neweditComment', Comment);
        },
        getEditCommentContent: function() {
            var Comment = Ember.$('#editCommentArea')[0].textContent;
            var oldComment = Ember.$('#editCommentArea')[0].children[0].innerHTML;
            this.set('oldeditComment', oldComment);
            this.set('neweditComment', Comment);
        },
        deleteAnnotation: function(annotation){
            if(annotation.get('Parent')){
                Ember.$("li.czn-list-user[commentid='" + annotation.id + "']").hide();
                annotation.destroyRecord();
                this.get("controllers.comments/index").set("annotationsChanged", new Date());
            }
            else{
                this.send('hide');
                this.send('removeComment', annotation);
            }

            class Event {
                constructor(name, obj) {
                    this.event = name;
                    this.obj_ = obj;
                }
            }
            class DeleteAnnotation {
                constructor(approval_guid , approvalAnnotationId,  user_name , is_external_user) {
                    this.approval_guid = approval_guid;
                    this.approvalAnnotationId = approvalAnnotationId;
                    this.user_name = user_name;
                    this.is_external_user = is_external_user;

                }
            }
            
            const Annotation_data = new DeleteAnnotation(window.AppkitENV.approval_guid ,  annotation.id  ,window.AppkitENV.user_name , window.Appkit.is_external_user  );
            const event = new Event('deleteAnnotation', Annotation_data);
            window.parent.postMessage(event, '*');
        },
        copyAnnotationComment: function(annotation){
            var checklistItems = annotation.get('ApprovalannotationChecklistitems').content;
            var AnnotationChecklistItems = [];
           for(var i = 0; i < checklistItems.length; i++)
           {
            var data = {Id: checklistItems[i]._data.Id , IsItemChecked: checklistItems[i]._data.IsItemChecked, Item: checklistItems[i]._data.Item, ItemValue: checklistItems[i]._data.ItemValue, ShowNoOfChanges: false, TotalChanges: 0};
            AnnotationChecklistItems.push(data);
            }
            this.get('controllers.comments/index').set('copiedAnnotationChecklistItems', AnnotationChecklistItems);
            this.get('controllers.comments/index').set('copiedAnnotationsAndAttachments',annotation);
            this.get('controllers.comments/index').set('canPaste',true);
           },
        editAnnotation: function(annotation){
            this.set('editCommentText', annotation.get('Comment'));
            this.set('isEditing', true);
            var self = this;

            Ember.run.later(function(){
                var toolbarOptions = [
                    ['bold', 'italic', 'underline']
                  ];
                var globalEditQuill = self.get('controllers.comments/index.globalEditQuill');
                globalEditQuill = new Quill('#editCommentArea', {
                    modules: {
                        toolbar: toolbarOptions
                      },
                    theme: 'snow'
                   });
                   var text = self.get('editCommentText');
                   self.set('oldeditComment', text);
                   globalEditQuill.container.childNodes[0].innerHTML = text;
                   self.set('controllers.comments/index.globalEditQuill', globalEditQuill);
            });
        },
        cancelEdit: function(){
            this.set('isEditing', false);
            this.set('oldeditComment', '');
            this.set('neweditComment', '');
            this.set('editCommentText', '');
            Ember.run.later(function(){
                var commentTexts = Ember.$('.czn-comment-text');
                if(commentTexts.length > 0){
                    for(var i = 0; i < commentTexts.length; i++ )
                    {
                        if(commentTexts[i].innerHTML === "")
                        {
                          commentTexts[i].innerHTML = commentTexts[i].id;
                        }
                    }
                }
            });
        },
        updateAnnotation: function(annotation)
        {         
            var self = this;
            var quill = new Quill('#editCommentArea');
            var approval_phase = null;
            if(this.store.all('approval-basic').content[0].get('CurrentPhaseId') != null)
            {
                approval_phase = this.store.all('approval-basic').content[0].get('CurrentPhaseId');
            }
            this.set('updatedCommentText', quill.container.childNodes[0].innerHTML);
            
             this.store.adapterFor('annotation').updateAnnotationComment(annotation.get('id'), approval_phase , quill.container.childNodes[0].innerHTML).then(function(result){
                annotation.set('Comment', self.editCommentText);
                annotation.set('ModifiedDate', result.annotation.ModifiedDate);
                self.set('isEditing', false);
            Ember.run.later(function(){
            var commentTexts = Ember.$('.czn-comment-text');
            if(commentTexts.length > 0){
                for(var i = 0; i < commentTexts.length; i++ )
                {
                    if(commentTexts[i].innerHTML === "" && commentTexts[i].id === self.editCommentText)
                    {
                      commentTexts[i].id = self.get('updatedCommentText');
                      commentTexts[i].innerHTML = self.get('updatedCommentText');
                    }
                }
            }
            var parentComment = Ember.$('.czn-list-annotation-text');
            if(parentComment.length > 0){
                for(var i = 0; i < parentComment.length; i++ )
                {
                    if(parentComment[i].innerHTML === self.editCommentText && parentComment[i].title === self.editCommentText)
                    {
                        parentComment[i].title = self.get('updatedCommentText');
                        parentComment[i].innerHTML = self.get("updatedCommentText");
                    }
                }
            }
            annotation.set('Comment', self.get("updatedCommentText"));
            self.set('updatedCommentText', '');
            self.set('oldeditComment', '');
            self.set('neweditComment', '');
            });
        });
        }
    }
});
export default ItemController;
