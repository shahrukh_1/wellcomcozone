import Ember from "ember";
import AppConstants from "appkit/config/constants";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var InstantNotificationsIndexController = Ember.ArrayController.extend(CurrentViewerMixin, {
  onShowInstantNotification(message) {
    this.addNotification(null, message, false);
  },
  registerEvents() {
    this.get('instantNotificationsCommunicationService').RegisterOnShowNewTilesInstantNotification(this, 'onShowInstantNotification');
    this.get('softProofingParamCommunicationService').RegisterOnUHRTriggeredChanged(this,'enableUHRInProgress');
  },

  onCleanup: function() {
    this.get('instantNotificationsCommunicationService').UnRegisterOnShowNewTilesInstantNotification(this, 'onShowInstantNotification');
    this.get('softProofingParamCommunicationService').UnRegisterOnUHRTriggeredChanged(this,'enableUHRInProgress');
  }.on('willDestroyElement'),

  uhrInProgressByCurrentUser: false,
  enableUHRInProgress(triggeredBy) {
    this.set('uhrInProgressByCurrentUser', true);
  },

  doNotDisplayNotification(message, isFromServer) {
    var currentUserId = this.get('session.user.id'),
      userIsExternal = this.get('session.user.UserIsExternal'),
      userCanSeeAllAnnotations = this.get('session.user.CanSeeAllAnnotations');

    if (
      (isFromServer) &&
      ((message.Creator && !userIsExternal && message.Creator.toString() === currentUserId.toString()) ||
      (message.ExternalCreator && message.ExternalCreator.toString() === currentUserId.toString() && userIsExternal))){
      //ignore notifications for current user
      return true;
    }

    if (userIsExternal && !userCanSeeAllAnnotations && message.EntityType.toString() === AppConstants.InstantNotificationEntityType.Annotation.toString()){
      // do not show the annotation instant notification if the current logged user is external and
      // he is not allowed to see all the annotations from the opened approval
      return true;
    }

    if (isFromServer && this.get('uhrInProgressByCurrentUser')) {
      if (message.EntityType == AppConstants.InstantNotificationEntityType.VersionSwitchedToHighRes) {
        this.set('uhrInProgressByCurrentUser', false);
        return true;
      }

    }

    return false;
  },

  addNotification(operationType, message, isFromServer){
    var model = this.get('model') || [],
      store = this.get('store');

    try {
      if (this.doNotDisplayNotification(message, isFromServer)) {
        return;
      }

      if (message.Creator){
        var creatorUser = store.all('internal-collaborator').filter(function(user){
          return user.get('id').toString() === message.Creator.toString();
        }).get('firstObject');
        message.Creator = creatorUser;
      }
      if (message.ExternalCreator){
        var creatorUser = store.all('external-collaborator').filter(function(user){
          return user.get('id').toString() === message.ExternalCreator.toString();
        }).get('firstObject');
        message.ExternalCreator = creatorUser;
      }
      if (message.Version){
        var version = store.all('version').filter(function(version){
          return version.get('id').toString() == message.Version.toString();
        }).get('firstObject');
        message.Version = version;
      }
      var notification = store.createRecord('instant-notification', message);
      notification.set('OperationType', operationType);
      notification.transitionTo('loaded.saved');
      model.pushObject(notification);
      this.set('model', model);
    }
    catch(err){
      console.log('err on add notification ', err);
    }
  },

    initializeSignalREvents: function(){
        this.registerEvents();

        var signalRConnection = Ember.$.connection || window.AppkitENV.signalRConnection,
            store = this.get('store');

        var hub = signalRConnection ? signalRConnection.instantNotificationsHub : null;

        if (hub){
          var self = this;
            hub.client.addnotification = function(operationType, message){
              self.addNotification(operationType, message, true);
            };

            signalRConnection.hub.start().done(function(){
                var ids = [];
                store.all('version').forEach(function(version){
                    ids.pushObject(version.get('id'));
                });
                hub.server.registerConnectionApprovalIds(ids.join('|'));
            });
        }
    }.on('init')
});
export default InstantNotificationsIndexController;
