import Ember from "ember";
import AppConstants from "appkit/config/constants";

var InstantNotificationsItemController = Ember.ObjectController.extend({
	refreshView: true,

	userDisplayAvatar: function(){
		if (this.get('model.Creator')){
			return this.get('model.Creator.CollaboratorAvatar');
		}
		else if (this.get('model.ExternalCreator')){
			return this.get('model.ExternalCreator.CollaboratorAvatar');
		}
		return "";
	}.property('model.Creator', 'model.ExternalCreator'),

	userDisplayName: function(){
    if (this.get('model.Creator')){
        return (this.get('model.Creator.GivenName') + ' ' + this.get('model.Creator.FamilyName'));
    }
    else if (this.get('model.ExternalCreator')){
        return (this.get('model.ExternalCreator.GivenName') + ' ' + this.get('model.ExternalCreator.FamilyName'));
    }
		return "N/A";
	}.property('model.Creator'),

	message: function(){
		var operationTye = this.get('model.OperationType'),
			entityType = this.get('model.EntityType'),
			versionNr = this.get('model.Version.VersionNumber'),
			PrivateAnnotations = this.store.all('user').get('content')[0].get('_data').PrivateAnnotations,
			PDMId = this.store.all('approval-basic').content[0].get('PDM'),
			LoggedUser = this.store.all('user').get('content')[0].get('_data').id;

			
		if (entityType === AppConstants.InstantNotificationEntityType.Annotation) {
			if(!PrivateAnnotations || (PDMId === LoggedUser)){
			if (operationTye === AppConstants.InstantNotificationOperationType.Added) {
				return Ember.I18n.t("lblInstantNotificationAnnotationAdded", {user: this.get('userDisplayName'), versionNr: versionNr});
			}
			else if (operationTye === AppConstants.InstantNotificationOperationType.Modified) {
				return Ember.I18n.t("lblInstantNotificationAnnotationModified", {user: this.get('userDisplayName'), versionNr: versionNr});
			}
			else if (operationTye === AppConstants.InstantNotificationOperationType.Deleted) {
				return Ember.I18n.t("lblInstantNotificationAnnotationRemoved", {user: this.get('userDisplayName'), versionNr: versionNr});
			}
		} else{
			this.set('refreshView', false);
                return "";
		 }
		}
		else if (entityType == AppConstants.InstantNotificationEntityType.Decision){
			return Ember.I18n.t("lblInstantNotificationDecisionChanged", {user: this.get('userDisplayName'), versionNr: versionNr});
		}
    else if (entityType == AppConstants.InstantNotificationEntityType.ViewingConditions) {
      return Ember.I18n.t("lblInstantNotificationVwCondChanged", {user: this.get('userDisplayName'), versionNr: versionNr});
    }
    else if (entityType == AppConstants.InstantNotificationEntityType.VersionSwitchedToHighRes) {
      return Ember.I18n.t("lblInstantNotificationSwitchedToHighResolution", {user: this.get('userDisplayName'), versionNr: versionNr});
    }
    else if (entityType == AppConstants.InstantNotificationEntityType.TilesAreReady) {
      return Ember.I18n.t("lblTilesAreReady", {user: this.get('userDisplayName'), versionNr: versionNr});
    }
		return "";
	}.property('model'),

  actions: {
    reloadPageOrSpecificZone() {
      var entityType = this.get('model.EntityType');
      if (entityType == AppConstants.InstantNotificationEntityType.TilesAreReady) {
        var versionId = this.get('model.Version.id');
        this.get('instantNotificationsCommunicationService').ConfirmedToShowNewTiles(versionId);
      }
      else {
        window.location.reload();
      }

    }
  }

});
export default InstantNotificationsItemController;
