import Ember from "ember";
import CurrentViewerMixin from 'appkit/routes/mixins/current-viewer';

var ApprovalController = Ember.ObjectController.extend(CurrentViewerMixin, {
    needs: ['sidebar/decisions', 'sidebar/index', 'navigation/main', 'image/controls/image'],
    currentVersion: null,
    selectedVersions: [],
    versionsForOverlay: [],
    viewerControllers: [],
    hasPager: false,
    pagesChanged: new Date(),
    decisions: Ember.computed.alias('controllers.sidebar/decisions'),
    navigationMainController: Ember.computed.alias('controllers.navigation/main'),
    selectedDecision: null,
    isFirstOverlayVersionOnTop:Ember.computed.alias('controllers.image/controls/image.isFirstOverlayVersionOnTop'),
    currentPhaseShouldBeViewedByAll: false,
    paginationVisible: false,

    actions: {
        saveDecision: function(decision) {
            var self = this;
            var approval = this.get('currentVersion');
            this.set('selectedDecision', decision);

            if(approval.get('CurrentPhase') != null){
                var apiUrl = window.AppkitENV.hostname + '/' + window.AppkitENV.namespace + '/phase-complete?key=' + window.AppkitENV.key + '&user_key=' +
                            window.AppkitENV.user_key + '&isExternal=' + this.get('session.user.UserIsExternal') + '&collaboratorId=' + this.get('session.user.id') +
                            '&currentPhase=' + approval.get('CurrentPhase') + '&decisionId=' + decision.id + '&Id=' + approval.get('id');

                Ember.$.ajax({
                  type: "GET",
                  url: apiUrl,
                  contentType: "application/json; charset=utf-8",
                  cache: false,
                  success: function(result){
                    if (self && self.get('isDestroyed') === false) {
                      if (result != null && result != "null" && (result.isRejected || result.isCompleted) && approval.get("")) {
                          var versionPhaseShouldBeViewedByAll = approval.get("CurrentPhaseShouldBeViewedByAll");
                          self.send('showPhaseCompleteModal', undefined, result.isRejected, result.isCompleted, versionPhaseShouldBeViewedByAll);
                      }else{
                        self.changeApprovalDecision();
                      }
                    }
                  }
                });
            }else {
              if (!approval.get('IsLocked')) {
                var apiUrl = window.AppkitENV.hostname + '/' +
                  window.AppkitENV.namespace + '/checkApprovalShouldBeLocked?key=' + window.AppkitENV.key +
                  '&collaboratorId=' + this.get('session.user.id') + '&approvalId=' + approval.get('id');
                Ember.$.ajax({
                  type: "GET",
                  url: apiUrl,
                  contentType: "application/json; charset=utf-8",
                  cache: false,
                  success: function (result) {
                    var lockApprovalAfterFirstDecision = result;
                    if (self && self.get('isDestroyed') === false) {
                      if (lockApprovalAfterFirstDecision != null && lockApprovalAfterFirstDecision != "null" && lockApprovalAfterFirstDecision) {
                        self.send('showPhaseCompleteModal', lockApprovalAfterFirstDecision);
                      } else {
                        self.changeApprovalDecision();
                      }
                    }
                  }
                });
              }
              else {
                self.changeApprovalDecision();
              }
            }
        },

        showDecision: function() {
            this.get('target').send('showDecision');
        },

        changeCurrentVersion: function(version) {
            if(!version.get('isCompareMode')){
                //depending on this property the view will be initialized or not
                version.set('canInitializeView', true);
            }
            this.set('currentVersion', version);

            var versionId = version.get('id');
            this.initApprovalRole(versionId);
            return true;
        },

        changeToOverlayMode:function(){
            this.setOverlayVersions();

            if (this.get('paginationVisible') == true) {
              this.send('togglePagination');
            }
            return true;
        },

        displayMultipleVersions: function(versions){
            this.set('versionsForOverlay', versions);
            //sort versions array by id descending
            versions = versions.sort(function(a,b){
                if (parseInt(a.get('id')) < parseInt(b.get('id'))){
                    return 1;
                }
                else if (parseInt(a.get('id')) > parseInt(b.get('id'))){
                    return -1;
                }
                else {
                    return 0;
                }
            });
            this.set('selectedVersions', versions);
            return true;
        },

      changeApprovalDecision: function(defer){
          this.changeApprovalDecision(defer);
        }
    },

    initApprovalRole: function(versionId){
    },

    setOverlayVersions: function() {
        var selectedFirst = this.get('isFirstOverlayVersionOnTop');

        var overlayObjects = [],
            versions = this.get('selectedVersions') || [],
            firstVersion = versions.filterBy('select', selectedFirst).get('firstObject'),
            secondVersion = versions.filterBy('select', !selectedFirst).get('firstObject');

        if (!firstVersion || !secondVersion) {
          if (versions.length == 2) {
            firstVersion = versions[0];
            secondVersion = versions[1];
            firstVersion.set('select', true);
            secondVersion.set('select', false);
          }
        }
        if (firstVersion && secondVersion) {
            firstVersion.set('isFirstVersion', false);
            firstVersion.set('isCompareMode', false);
            firstVersion.set('isOverlayMode', true);
            firstVersion.set('PageLabelVersion', firstVersion.get('Title') + ":" + firstVersion.get('VersionLabel'));

            secondVersion.set('isFirstVersion', true);
            secondVersion.set('isCompareMode', false);
            secondVersion.set('isOverlayMode', true);
            secondVersion.set('PageLabelVersion', secondVersion.get('Title') + ":" + secondVersion.get('VersionLabel'));

            overlayObjects.pushObject(secondVersion);
            overlayObjects.pushObject(firstVersion);

            this.set('versionsForOverlay', overlayObjects);
            this.set('currentVersion', firstVersion);

            this.send('showAnnotationDecision', !firstVersion.get('IsLocked') || !firstVersion.get('isLocked'));
        }
    },

  changeApprovalDecision: function(defer){
      var decision = this.get('selectedDecision');
      var self = this;
      var oldDecision = this.get('Decision');
      var approval = this.get('currentVersion');
      approval.set('Decision', decision);

      this.store.adapterFor(approval).saveApprovalDecision(approval, {
          collaboratorId: self.get('session.user.id'),
          isExternal: self.get('session.user.UserIsExternal')
      }).then(function(obj){
        if(obj.decision.JobStatus === 'COM' && obj.decision.Key === 'APD'){
            Ember.$('.fa.fa-lg.fa-plus-circle').hide();
        } else {
          Ember.$('.fa.fa-lg.fa-plus-circle').show();
        }
        var links = null, decision = null, link = null;
        if (obj) { // fix for Firefox issue
          if (self.get('session.user.UserIsExternal') && obj.external_collaborators_links) {
            links = obj.external_collaborators_links.filter(function (link) {
              return link.collaborator_id.toString() === self.get('session.user.id') && link.approval_version.toString() === approval.get('id');
            });
            if (links && links.length > 0 && links[0].Decision) {

              decision = self.store.all('decision').filterBy('id', links[0].Decision.toString()).get('firstObject');
              link = self.store.all('external_collaborators_link').filter(function (link) {
                return link.get('collaborator_id.id') === self.get('session.user.id') && link.get('approval_version.id') === approval.get('id');
              }).get('firstObject');

              if (link && decision) {
                link.set('Decision', decision);
                link.transitionTo('loaded.saved');
                self.get('currentViewerController').set('usersChanged', new Date());
              }
            }
          }
          else if (obj.internal_collaborators_links) {
            links = obj.internal_collaborators_links.filter(function (link) {
              return link.collaborator_id.toString() === self.get('session.user.id') && link.approval_version.toString() === approval.get('id');
            });
            if (links && links.length > 0 && links[0].Decision) {
              decision = self.store.all('decision').filterBy('id', links[0].Decision.toString()).get('firstObject');
              link = self.store.all('internal_collaborators_link').filter(function (link) {
                return link.get('collaborator_id.id') === self.get('session.user.id') && link.get('approval_version.id') === approval.get('id');
              }).get('firstObject');

              if (link && decision) {
                link.set('Decision', decision);
                link.transitionTo('loaded.saved');
                self.get('currentViewerController').set('usersChanged', new Date());
              }
            }
          }
        }
        if(defer) {
          defer.resolve();
        }
      }).catch(function(reason) {
        approval.set('Decision', oldDecision);
        self.get('target').send('error', reason);
        if(defer) {
          defer.reject();
        }
      });
    },

    modelInit: function() {
        var lastVersion = this.get('model.Versions').filterBy('id', window.AppkitENV.firstApprovalId.toString()).get('firstObject') || this.get('model.Versions.lastObject');
        this.set('currentVersion', lastVersion);
        this.set('hasPager', this.get('model.ApprovalType') === this.get('TYPE_IMAGE'));
        this.set('currentPhaseShouldBeViewedByAll',this.get('model.CurrentPhaseShouldBeViewedByAll'));

        var versionId = lastVersion.get('id');
        this.initApprovalRole(versionId);

        var versions = this.get('model.Versions');
        var allVersions = this.store.all('version');
        var self = this;

        allVersions.forEach(function (version) {
          version.set('isClone', false);
          if (version.get("Parent") != self.get('model').get("id")) {
            versions.pushObject(version);
          }
        });
    }.observes('model')
});
export default ApprovalController;
