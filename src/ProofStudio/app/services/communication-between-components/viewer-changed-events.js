import Ember from 'ember';

export default Ember.Service.extend(Ember.Evented, {
  ViewerChagned(approvalId) {
    this.trigger('ViewerChagned', approvalId);
  },
  RegisterOnViewerChagned(targetContext, methodName) {
    this.on('ViewerChagned', targetContext, methodName);
  },
  UnRegisterOnViewerChagned(targetContext, methodName) {
    this.off('ViewerChagned', targetContext, methodName);
  }
});
