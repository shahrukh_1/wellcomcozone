import Ember from 'ember';

export default Ember.Service.extend(Ember.Evented, {
  openUHRInProgressAlert() {
    this.OpenAlertDialog({title:Ember.I18n.t('lblHighResInProgressAlertTitle'), body: Ember.I18n.t('lblHighResInProgressAlertBody')});
  },
  openUHRInProgressAlertWaitingResponse(versionId) {
    this.OpenAlertDialog({title:Ember.I18n.t('lblHighResInProgressAlertTitle'), body: Ember.I18n.t('lblHighResInProgressAlertBody'), byVersionId:versionId});
  },
  openUHRFinishedPageRefreshMustBeDone(versionId) {
    this.OpenAlertDialog({title:Ember.I18n.t('lblHighResFinishedPageRefreshMustBeDoneTitle'), body: Ember.I18n.t('lblHighResFinishedPageRefreshMustBeDoneBody'), byVersionId:versionId});
  },
  OpenAlertDialog(message) {
    this.trigger('OpenAlertDialogEv', message);
  },
  RegisterOnOpenAlertDialog(targetContext, methodName) {
    this.on('OpenAlertDialogEv', targetContext, methodName);
  },
  UnRegisterOnOpenAlertDialog(targetContext, methodName) {
    this.off('OpenAlertDialogEv', targetContext, methodName);
  },
  CloseAlertUHRDialog(versionId) {
    this.trigger('CloseAlertUHRDialogEv', versionId);
  },
  RegisterOnCloseAlertUHRDialog(targetContext, methodName) {
    this.on('CloseAlertUHRDialogEv', targetContext, methodName);
  },
  UnRegisterOnCloseAlertUHRDialog(targetContext, methodName) {
    this.off('CloseAlertUHRDialogEv', targetContext, methodName);
  },

  TriggerCompareCanvases() {
    this.trigger('CompareCanvasesEvent');
  },
  RegisterCompareCanvasesEvent(targetContext, methodName) {
    this.on('CompareCanvasesEvent', targetContext, methodName);
  },
  UnRegisteCompareCanvasesEvent(targetContext, methodName) {
    this.off('CompareCanvasesEvent', targetContext, methodName);
  },



});
