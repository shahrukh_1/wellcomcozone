import Ember from 'ember';
import AppConstants from "appkit/config/constants";

export default Ember.Service.extend(Ember.Evented, {
  GetCurrentCreator() {
    var currentUserId = this.get('session.user.id'),
      userIsExternal = this.get('session.user.UserIsExternal');

    if (userIsExternal) {
      return null;
    }

    return currentUserId;
  },
  GetCurrentExternalCreator() {
    var currentUserId = this.get('session.user.id'),
      userIsExternal = this.get('session.user.UserIsExternal');

    if (!userIsExternal) {
      return null;
    }

    return currentUserId;
  },
  ShowNewTilesInstantNotification(versionId) {
    this.trigger('NewTilesInstantNotification', {
        EntityType: AppConstants.InstantNotificationEntityType.TilesAreReady,
        Creator: this.GetCurrentCreator(),
        ExternalCreator: this.GetCurrentExternalCreator(),
        Version: versionId
      });
  },
  ShowSwitchedToHighResInstantNotification(versionId) {
    this.trigger('NewTilesInstantNotification', {
        EntityType: AppConstants.InstantNotificationEntityType.VersionSwitchedToHighRes,
        Creator: this.GetCurrentCreator(),
        ExternalCreator: this.GetCurrentExternalCreator(),
        Version: versionId
      });
  },
  RegisterOnShowNewTilesInstantNotification(targetContext, methodName) {
    this.on('NewTilesInstantNotification', targetContext, methodName);
  },
  UnRegisterOnShowNewTilesInstantNotification(targetContext, methodName) {
    this.off('NewTilesInstantNotification', targetContext, methodName);
  },

  ConfirmedToShowNewTiles(versionId) {
    this.trigger('ConfirmedToShowNewTiles', versionId);
  },
  RegisterOnConfirmedToShowNewTiles(targetContext, methodName) {
    this.on('ConfirmedToShowNewTiles', targetContext, methodName);
  },
  UnRegisterOnConfirmedToShowNewTiles(targetContext, methodName) {
    this.off('ConfirmedToShowNewTiles', targetContext, methodName);
  }
});
