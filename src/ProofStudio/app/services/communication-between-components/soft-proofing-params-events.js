import Ember from 'ember';
import AppConstants from 'appkit/config/constants';

export default Ember.Service.extend(Ember.Evented, {
  SwitchToUltraHighResolution(version) {
    this.trigger('SwitchToUltraHighResolutionEvent', version);
  },
  RegisterOnSwitchToUltraHighResolution(targetContext, methodName) {
    this.on('SwitchToUltraHighResolutionEvent', targetContext, methodName);
  },
  UnRegisterOnSwitchToUltraHighResolution(targetContext, methodName) {
    this.off('SwitchToUltraHighResolutionEvent', targetContext, methodName);
  },

  SoftProofingChanged(versionId, isForClone) {
    this.trigger('SoftProofingChangedEvent', versionId, isForClone);
  },
  RegisterOnSoftProofingChanged(targetContext, methodName) {
    this.on('SoftProofingChangedEvent', targetContext, methodName);
  },
  UnRegisterOnSoftProofingChanged(targetContext, methodName) {
    this.off('SoftProofingChangedEvent', targetContext, methodName);
  },

  UHRTriggeredFirstTime(versionId) {
    this.trigger('UHRTriggeredEvent', {version: versionId, triggeredType:AppConstants.UltraHighResolutionTriggerStatus.UHRTriggeredFirstTimeByButton});
  },
  UHRTriggeredAndAlreadyInProgress(versionId) {
    this.trigger('UHRTriggeredEvent', {version: versionId, triggeredType:AppConstants.UltraHighResolutionTriggerStatus.UHRTriggeredAndAlreadyInProgress});
  },

  RegisterOnUHRTriggeredChanged(targetContext, methodName) {
    this.on('UHRTriggeredEvent', targetContext, methodName);
  },
  UnRegisterOnUHRTriggeredChanged(targetContext, methodName) {
    this.off('UHRTriggeredEvent', targetContext, methodName);
  },
});
