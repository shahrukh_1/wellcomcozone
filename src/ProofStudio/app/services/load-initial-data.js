import Ember from 'ember';
import LocaleFiles from '../locale/locale-keys';

var LoadInitialDataService = Ember.Service.extend({
  singleRecord: '',
  approvalsIds:'',
  approval_id:'',
  machineId: '',
  versionsIds:'',

  pageId:'',
  userKey: window.AppkitENV.user_key,
  selectedHtmlPageId: window.AppkitENV.selectedHtmlPageId,
  generateApprovalIdFromApprovalsIds() {
    if (!this.get('approvalsIds')) {
      return;
    }

    this.set('approval_id', this.get('approvalsIds').split(',')[0]);
  },

  triggerUltraHighResolutionIfItIsTheCase(store) {
    //debugger;
    var self = this;
    var softProofingParams = store.all('soft-proofing-param');
    softProofingParams.forEach(function(spp) {
      if (spp.get('HighResolutionInProgress') == false) {
        return;
      }

      var versionId = spp.get('Version.id');
      var highResInProgressApproval = store.all('approval-basic').filter(function(approval) {
        return approval.get('id') == versionId;
      }).get('firstObject');

      if (!highResInProgressApproval) { // it should be
        return;
      }

      Ember.run.later(function() {
        console.log('trigger uhr', versionId, highResInProgressApproval.get('FirstPageId'));
        self.get('softProofingParamCommunicationService').SwitchToUltraHighResolution({
          id: versionId,
          pageId: highResInProgressApproval.get('FirstPageId')
        });
      }, 500);
    });
  },

  loadApprovalsAndTriggerUltraHighResNotification(approvalsIds) {
    return this.loadApprovals(approvalsIds).then((approvals) => {
        this.triggerUltraHighResolutionIfItIsTheCase(approvals.get('store'));
        return approvals;
    });
  },
  loadApprovals(approvalsIds) {
    var store = this.get('store');
    var singleRecord = this.get('singleRecord');
    return Ember.RSVP.hash({
      approvals: store.findQuery('approval-basic', {
        approvalsIds: approvalsIds
      }, null, singleRecord).then(function (basicValues) {
        var approvalsBasicsStr = JSON.stringify(basicValues.store.all('approval-basic').get('content'));
        return Ember.RSVP.hash({
          initialVersions: store.findQuery('approval', {
            approvalBasic: approvalsBasicsStr
          }, null, singleRecord),
          usergroups: store.findQuery('user-group', {
            approvalBasic: approvalsBasicsStr
          }, null, singleRecord).catch(function (error) {
            return "";
          }),
          collaborators: store.findQuery('internal-collaborator', {
            approvalBasic: approvalsBasicsStr
          }, null, singleRecord).catch(function (error) {
            return "";
          }),
        }).then(function (approvals) {
          return approvals.initialVersions;
        })
      })
    }).then(function (values) {
      return values.approvals;
    });
  },

  loadVideoStartUpData: function() {
    var store = this.get('store');
    var singleRecord = this.get('singleRecord');
    var approvalsIds = this.get('approvalsIds');
    var approval_id = this.get('approval_id');
    var pageId = this.get('pageId');
    var selectedHtmlPage_id =  window.AppkitENV.selectedHtmlPageId;
    var adapter = this.store.adapterFor('approval');
    return Ember.RSVP.hash({
     translation: adapter.translations(this.userKey, LocaleFiles.Keys).then(function(result) {
        Ember.I18n.translations = result.Translations;
    }),
      approvals:this.loadApprovals(approvalsIds),
      user: store.findQuery('user', {// it is the logged user which is the same for different approvals in file comparison
        approval_id: approval_id
      }, null, singleRecord),
      annotations: store.find('annotation', {approvalId: approval_id, selectedHtmlPageId:selectedHtmlPage_id }),
      shapes: store.findAll('shape', null, singleRecord),
      account: store.findQuery('account', {}, null, singleRecord),
      collaborator_roles: store.findAll('collaborator-role', null, singleRecord),
      annotationStatuses: store.findAll('annotation-status'),
      decisions: store.findAll('decision'),
      roles: store.findAll('role'),
      pages:store.find('page', {approvalId: approval_id}),
    })
  },


  loadImageStartUpData: function() {
    var store = this.get('store');
    var singleRecord = this.get('singleRecord');
    var approvalsIds = this.get('approvalsIds');
    var approval_id = this.get('approval_id');
    var machineId = this.get('machineId');
    var versionsIds = this.get('versionsIds');
    var pageId = this.get('pageId');
    var adapter = this.store.adapterFor('approval');
    return Ember.RSVP.hash({
      translation: adapter.translations(this.userKey, LocaleFiles.Keys).then(function(result) {
        Ember.I18n.translations = result.Translations;
    }),
      approvals: this.loadApprovals(approvalsIds),
      user: store.findQuery('user', {// it is the logged user which is the same for different approvals in file comparison
        approval_id: approval_id
      }, null, singleRecord),
      textHighlightingWords: store.find('text-highlighting-word', { pageId: pageId, approvalId: approval_id}),
      annotations: store.find('annotation', {approvalId: approval_id}),
      shapes: store.findAll('shape', null, singleRecord),
      account: store.findQuery('account', {}, null, singleRecord),
      collaborator_roles: store.findAll('collaborator-role', null, singleRecord),
      softproofingstatus: store.findQuery('softproofingstatus', {machineId: machineId}, null, singleRecord),
      annotationStatuses: store.findAll('annotation-status'),
      decisions: store.findAll('decision'),
      roles: store.findAll('role'),
      paperTints: store.find('soft-proofing-papertint'),
      customProfiles: store.find('custom-profile', {
        versionsIds: versionsIds, nextProfileId: 0
      }),
      pages:store.find('page', {approvalId: approval_id}),
    })
  },

  // the access with jquery should be removed in the future. There should be a component that shows errors. This component would have a service that is injected in this service
  // and the state of it would change to show these errors
  showErrorOnLoadingData(error) {
    Ember.run.later(function () {
      Ember.$(".czn-application").hide();
      Ember.$("#czn-loading-screen").hide();
      Ember.$("#czn-no-access-screen").show();
      //Ember.$("#czn-no-access-screen").modal('show');
      Ember.$("#czn-error-screen").modal('hide');
      console.log('error when loading initial data, msg: ', error);
    });
  }
});

export default LoadInitialDataService
