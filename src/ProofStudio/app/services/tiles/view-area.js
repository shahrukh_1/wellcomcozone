import Ember from 'ember'
import AppConstants from 'appkit/config/constants';

export default Ember.Service.extend({
  GetStartAndEndViewArea(map, size) {
    var startingPointPosition = map ? map.getPixelFromCoordinate([0, 0]) : [0, 0],
      view = map ? map.getView() : null,
      resolution = view ? view.getResolution() : 1,
      rotation = view ? view.getRotation() : 0, // in radians
      angle = rotation * 180 / Math.PI, // in degrees
      scale = 1 / resolution,
      width = size.width * scale,
      height = size.height * scale;

    if (startingPointPosition) {
      var mapPosition = {
        left: startingPointPosition[0],
        top: startingPointPosition[1],
        width: width,
        height: height,
        angle: angle,
        mapSize: map.getSize()
      };
      var startView = [], endView = []; // top left tile and bottom right tile
      if (mapPosition.angle == 0) {
        // the starting point is defined by (left,top)
        // if starting point is pozitive the top left tile is 0-0
        // if starting point is negative the top left tile is defined by these coordinates
        if (mapPosition.left < 0) {
          startView[0] = Math.floor(-mapPosition.left);
        }
        else {
          startView[0] = 0;
        }
        if (mapPosition.top < 0) {
          startView[1] = Math.floor(-mapPosition.top);
        }
        else {
          startView[1] = 0;
        }

        // if (left,top) is positive than fewer tiles are used
        // if (left,top) is negative all tiles that can be in the view are considered
        if (mapPosition.mapSize[0] - mapPosition.left <= mapPosition.width) {
          endView[0] = Math.ceil((mapPosition.mapSize[0] - mapPosition.left));
        }
        else {
          endView[0] = Math.ceil((mapPosition.width));
        }

        if (mapPosition.mapSize[1] - mapPosition.top <= mapPosition.height) {
          endView[1] = Math.ceil((mapPosition.mapSize[1] - mapPosition.top));
        }
        else {
          endView[1] = Math.ceil(mapPosition.height);
        }
      }
      if (mapPosition.angle == 90) {
        // x y coordinates are reversed
        if (mapPosition.top < 0) {
          startView[0] = Math.floor((-mapPosition.top));
        }
        else {
          startView[0] = 0;
        }

        startView[1] = Math.floor(mapPosition.left - mapPosition.mapSize[0]);

        if (mapPosition.mapSize[1] - mapPosition.top <= mapPosition.width) {
          endView[0] = Math.ceil(mapPosition.mapSize[1] - mapPosition.top);
        }
        else {
          endView[0] = Math.ceil(mapPosition.width);
        }

        if (mapPosition.left - mapPosition.height > 0) {
          endView[1] = Math.ceil(mapPosition.height);
        }
        else {
          endView[1] = Math.ceil(mapPosition.left);
        }
      }

      if (mapPosition.angle == 180) {
        if (mapPosition.left > mapPosition.mapSize[0]) {
          startView[0] = Math.floor(mapPosition.left - mapPosition.mapSize[0]);
        }
        else {
          startView[0] = 0;
        }

        if (mapPosition.top > mapPosition.mapSize[1]) {
          startView[1] = Math.floor((mapPosition.top - mapPosition.mapSize[1]));
        }
        else {
          startView[1] = 0;
        }

        if (mapPosition.left > mapPosition.width) {
          endView[0] = Math.ceil(mapPosition.width);
        }
        else {
          endView[0] = Math.ceil(mapPosition.left);
        }
        if (mapPosition.top > mapPosition.height) {
          endView[1] = Math.ceil(mapPosition.height);
        }
        else {
          endView[1] = Math.ceil(mapPosition.top);
        }
      }

      if (mapPosition.angle == 270) {
        if (mapPosition.top > mapPosition.mapSize[1]) {
          startView[0] = Math.floor((mapPosition.top - mapPosition.mapSize[1]));
        }
        else {
          startView[0] = 0;
        }

        if (mapPosition.left > 0) {
          startView[1] = 0;
        }
        else {
          startView[1] = Math.floor((-mapPosition.left));
        }

        endView[0] = Math.ceil(mapPosition.top);
        if (mapPosition.mapSize[0] - mapPosition.left > mapPosition.height) {
          endView[1] = Math.ceil(mapPosition.height);
        }
        else {
          endView[1] = Math.ceil((mapPosition.mapSize[0] - mapPosition.left));
        }
      }

      if (startView.length == 2) {
        return {
          start: {x: startView[0], y: startView[1]},
          end: {x: endView[0], y: endView[1]}
        };
      }
    }
    return {start:{x:0, y:0}, end:{x:width, y:height}};
  },

  // returns the top left tile and bottom right tile in the matrix of tiles
  getCurrentViewTiles(map, size) {
    var viewArea = this.GetStartAndEndViewArea(map, size);

    var startTile = [], endTile = []; // top left tile and bottom right tile
    startTile[0] = Math.floor(viewArea.start.x / AppConstants.TileSize);
    startTile[0] = startTile[0] > 0 ? startTile[0] - 1 : 0;
    startTile[1] = Math.floor(viewArea.start.y / AppConstants.TileSize);
    startTile[1] = startTile[1] > 0 ? startTile[1] - 1 : 0;

    endTile[0] = Math.ceil(viewArea.end.x / AppConstants.TileSize);
    endTile[1] = Math.ceil(viewArea.end.y / AppConstants.TileSize);

    return "" + startTile[0] + "-" + startTile[1] + "," +
      endTile[0] + "-" + endTile[1];
  },

  // found another solution and it is not used. TODO: use it in another case or remove it
  IsClickEventOnTopPartOfViewArea(clickPoint, mapSize, map) {
    var startingPointPosition = map ? map.getPixelFromCoordinate([0, 0]) : [0, 0],
      view = map ? map.getView() : null,
      resolution = view ? view.getResolution() : 1,
      rotation = view ? view.getRotation() : 0, // in radians
      angle = rotation * 180 / Math.PI, // in degrees
      scale = 1 / resolution,
      clickX = clickPoint.x * scale,
      clickY = clickPoint.y * scale;

    var viewArea = this.GetStartAndEndViewArea(map, mapSize);

    if (!startingPointPosition) {
      return false;
    }

    if (angle == 0) {
      return clickY < (viewArea.start.y + viewArea.end.y)/2;
    }
    if (angle == 90) {
      return clickX < (viewArea.start.x + viewArea.end.x)/2;
    }
    if (angle == 180) {
      return clickY > (viewArea.start.y + viewArea.end.y)/2;
    }
    if (angle == 270) {
      return clickX > (viewArea.start.x + viewArea.end.x)/2;
    }

    return false;
  },
});
