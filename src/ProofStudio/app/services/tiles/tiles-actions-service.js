import Ember from 'ember'

export default Ember.Service.extend({
  registerEvents() {
    //alert('x');
    this.get('softProofingParamCommunicationService').RegisterOnSwitchToUltraHighResolution(this,'triggerUltraHighResolution');
  },

  unRegisterEvents() {
    //alert('y');
    this.get('softProofingParamCommunicationService').UnRegisterOnSwitchToUltraHighResolution(this,'triggerUltraHighResolution');
  },
  init() {// the service is singleton thus only one time is accessed
    this._super(...arguments);
    this.registerEvents();
  },

  softProofingParamCommunicationService: Ember.inject.service('communication-between-components/soft-proofing-params-events'),
  modalDialogsCommunicationService: Ember.inject.service('communication-between-components/modal-dialogs-events'),

  triggerUltraHighResolution(version) {
    var emptyZoom = {
      zoomLevel:0,
      viewTiles:'0-0,0-0'
    };

    var softProofing = {
      HighResolution: true,
      VersionId: version.id,
      VersionIsClone: false,
      pageId: version.pageId
    };

    this.updateSoftProofingConditions(softProofing, emptyZoom);
  },

  updateSoftProofingConditions(newSoftProofing, currentViewTilesAndZoom) {
    var self = this,
      versionId = newSoftProofing.VersionId,
      isForClone = newSoftProofing.VersionIsClone,
      currentSoftProofingCondition = this.get('session.softProofingParams').filter(function(sppItem){
        return sppItem.versionId === versionId && sppItem.isClone == isForClone;
      }).get('firstObject'),

      currentZoomLevel = currentViewTilesAndZoom.zoomLevel,
      currentViewTiles = currentViewTilesAndZoom.viewTiles;

    var isTriggeredByNewUHR = newSoftProofing.HighResolution || false;

    if (currentSoftProofingCondition) {
      // if one of the following item is null then that item should be retrieved from the session
      if (!newSoftProofing.SimulationProfileId && !newSoftProofing.EmbeddedProfileId && !newSoftProofing.CustomProfileId) {
        newSoftProofing.SimulationProfileId = currentSoftProofingCondition.SimulationProfileId;
        newSoftProofing.EmbeddedProfileId = currentSoftProofingCondition.EmbeddedProfileId;
        newSoftProofing.CustomProfileId = currentSoftProofingCondition.CustomProfileId;
      }

      if (newSoftProofing.PaperTintId == undefined) {
        newSoftProofing.PaperTintId = currentSoftProofingCondition.PaperTintId;
      }

      if (newSoftProofing.ActiveChannels == undefined) {
        newSoftProofing.ActiveChannels = currentSoftProofingCondition.ActiveChannels;
      }

      if (newSoftProofing.HighResolution == undefined) {
        newSoftProofing.HighResolution = currentSoftProofingCondition.HighResolution;
      }

      if (newSoftProofing.OutputRgbProfileName == undefined) {
        newSoftProofing.OutputRgbProfileName = currentSoftProofingCondition.OutputRgbProfileName;
      }
    }

    console.log('all spp', this.get('store').all('soft-proofing-param'));
    // check if soft-proofing-param exists in store
    var spp = this.get('store').all('soft-proofing-param').filter(function(spp){
      return spp.get('Version.id') === versionId &&

        spp.get('SimulationProfileId') === newSoftProofing.SimulationProfileId &&
        spp.get('EmbeddedProfileId') === newSoftProofing.EmbeddedProfileId &&
        spp.get('CustomProfileId') === newSoftProofing.CustomProfileId &&
        spp.get('PaperTintId') === newSoftProofing.PaperTintId &&

        spp.get('ActiveChannels') === newSoftProofing.ActiveChannels &&
        spp.get('HighResolution') === newSoftProofing.HighResolution //&&
      //spp.get('HighResolutionInProgress') === false;
    }).get('firstObject');

    console.log('old spp', spp);

    if (!spp){
      // if soft-proofing-param is not in the store then a request will be made for it

      this.get('store').find('soft-proofing-param', {
        pageId: newSoftProofing.pageId,
        Id: versionId,
        SimulationProfileId: newSoftProofing.SimulationProfileId,
        EmbeddedProfileId: newSoftProofing.EmbeddedProfileId,
        CustomProfileId: newSoftProofing.CustomProfileId,
        PaperTintId: newSoftProofing.PaperTintId,

        ActiveChannels: newSoftProofing.ActiveChannels,

        ZoomLevel: currentZoomLevel,
        ViewTiles: currentViewTiles,
        HighResolution: newSoftProofing.HighResolution,
        OutputRgbProfileName: newSoftProofing.OutputRgbProfileName
      }).then(function (sppList) {
        // if the result is an array get only the first item
        var spp = Ember.isArray(sppList) ? sppList.get('firstObject') : sppList;

        self.replaceSPPAndUpdateTiles(spp, isForClone, currentSoftProofingCondition, isTriggeredByNewUHR);
      }).catch(function(errReason){
        console.log('error', errReason);
      });
    }
    else{
      this.replaceSPPAndUpdateTiles(spp, isForClone, currentSoftProofingCondition, isTriggeredByNewUHR);
    }
  },

  replaceSPPAndUpdateTiles: function(spp, isForClone, currentSoftProofingCondition, isTriggeredByNewUHR) {
    var versionId = spp.get('Version.id');
    console.log('replaceSPPAndUpdateTiles ', isTriggeredByNewUHR, spp.get('HighResolutionInProgress'), versionId);
    console.log('new spp', spp);
    if (isTriggeredByNewUHR) {
      if (spp.get('HighResolutionInProgress') == true) {
        this.get('softProofingParamCommunicationService').UHRTriggeredAndAlreadyInProgress(versionId);
      }
      else {
        this.get('softProofingParamCommunicationService').UHRTriggeredFirstTime(versionId);
      }
    }
    else {
      if (spp.get('HighResolutionInProgress') == true) {
        this.get('modalDialogsCommunicationService').openUHRInProgressAlert();
        return;
      }
    }

    this.get('session.softProofingParams').removeObject(currentSoftProofingCondition);
    this.get('session.softProofingParams').pushObject(this.constructSPP(spp, isForClone));

    this.get('softProofingParamCommunicationService').SoftProofingChanged(versionId, isForClone);
  },
  constructSPP:function(spp, isForClone) {
    var newSPP = {};
    Ember.keys(spp.get('data')).forEach(function(prop) {
      if (spp.get(prop) != "object") {
        newSPP[prop] = spp.get(prop);
      }
    });
    newSPP.versionId = spp.get('Version.id');
    newSPP.isClone = isForClone;
    newSPP.OutputProfileName = this.getProfileName(newSPP);

    return newSPP;
  },
  getProfileName: function(softProofingParam) {
    var customProfile = this.get('store').all('custom-profile').filter(function (customProfile) {
      return customProfile.get('SimulationProfileId') == softProofingParam.SimulationProfileId &&
        customProfile.get('EmbeddedProfileId') == softProofingParam.EmbeddedProfileId &&
        customProfile.get('CustomProfileId') == softProofingParam.CustomProfileId;
    }).get('firstObject');
    if (customProfile)
    {
      return customProfile.get('DisplayName');
    }
    else
    {
      return "";
    }
  },

  initializeDefaultSoftProofingParams: function(){
    var softProofingParams = this.get('session.softProofingParams');

    if (!softProofingParams){
      softProofingParams = [];
      this.set('session.softProofingParams', softProofingParams);
    }
    else if (softProofingParams.get('length') > 0){
      softProofingParams.removeObjects(softProofingParams);
    }

    var self = this;
    // add the soft-proofing-param for the current version with the DefaultProfile on true
    console.log('current-viewer all', this.get('store').all('soft-proofing-param'));
    this.get('store').all('soft-proofing-param').forEach(function(spp){
      if (spp.get('DefaultSession') === true) {
        var spp = {
          ActiveChannels: spp.get('ActiveChannels'),
          DefaultSession: true,
          SimulationProfileId: spp.get('SimulationProfileId'),
          EmbeddedProfileId: spp.get('EmbeddedProfileId'),
          CustomProfileId: spp.get('CustomProfileId'),
          PaperTintId: spp.get('PaperTintId'),
          OutputRgbProfileName: spp.get('OutputRgbProfileName'),
          SessionGuid: spp.get('SessionGuid'),
          versionId: spp.get('Version.id'),
          isClone: spp.get('isClone'),
          HighResolution: spp.get('HighResolution'),
          EnableHighResolution: spp.get('EnableHighResolution')
        };

        spp.OutputProfileName = self.getProfileName(spp);

        softProofingParams.pushObject(spp);
      }
    });
  },

});
