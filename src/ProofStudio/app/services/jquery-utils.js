import Ember from 'ember'

export default Ember.Service.extend({
  popOverPlacement: function (tip, element) {
    var offset = Ember.$(element).offset(),
      height = Ember.$(document).outerHeight(),
      width = Ember.$(document).outerWidth(),
      vert = 0.5 * height - offset.top,
      vertPlacement = vert > 0 ? 'bottom' : 'top',
      horiz = 0.5 * width - offset.left,
      horizPlacement = horiz > 0 ? 'right' : 'left',
      placement = Math.abs(horiz) > Math.abs(vert) ?  horizPlacement : vertPlacement;
    return placement;
  }
});
