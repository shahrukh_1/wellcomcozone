import Ember from "ember";
import AttributeTranslations from "appkit/routes/mixins/attr-translations";
var ApplicationInitializer = {
    name: "Application Initializer",
    after: "store",
    initialize: function(container/*, application*/) {
        var store = container.lookup('store:main');
        var locale = container.lookup('locale:main');
        var session = container.lookup('session:main');


        var machineId = "";
        var result;
        var cookie = (result = new RegExp('(^|; )' + encodeURIComponent('spcn') + '=([^;]*)').exec(document.cookie)) ? result[2] : null;
        if (cookie){
            var data = cookie.split('|');
            if (data.length === 2){
                machineId = data[1];
            }
        }

        window.AppkitENV.machineId = machineId;

        Ember.$('.czn-no-access-screen button:first').on('click', function(){
            window.location = window.AppkitENV.URL;
        });

        window.AppkitENV.firstApprovalId = window.AppkitENV.approvals.split(',')[0];

        Ember.Controller.reopen(AttributeTranslations);
	    Ember.ObjectController.reopen(AttributeTranslations);
	    Ember.ArrayController.reopen(AttributeTranslations);
        Ember.Component.reopen(AttributeTranslations);
        //Ember.View.reopen(AttributeTranslations);
    }
};

export default ApplicationInitializer;
