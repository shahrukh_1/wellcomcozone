export function injectCommunicationBetweenComponents(application) {

  application.inject('controller:instant-notifications/index', 'instantNotificationsCommunicationService', 'service:communication-between-components/instant-notifications-events');
  application.inject('controller:image/viewer/image', 'instantNotificationsCommunicationService', 'service:communication-between-components/instant-notifications-events');
  application.inject('controller:instant-notifications/item', 'instantNotificationsCommunicationService', 'service:communication-between-components/instant-notifications-events');
  application.inject('view:instant-notifications/item', 'instantNotificationsCommunicationService', 'service:communication-between-components/instant-notifications-events');
  application.inject('controller:viewer/notifications', 'instantNotificationsCommunicationService', 'service:communication-between-components/instant-notifications-events');
  application.inject('service:load-initial-data', 'softProofingParamCommunicationService', 'service:communication-between-components/soft-proofing-params-events');
  application.inject('controller:image/viewer/image', 'softProofingParamCommunicationService', 'service:communication-between-components/soft-proofing-params-events');
  application.inject('component:image/controls/zoom-percentages', 'softProofingParamCommunicationService', 'service:communication-between-components/soft-proofing-params-events');
  application.inject('controller:instant-notifications/index', 'softProofingParamCommunicationService', 'service:communication-between-components/soft-proofing-params-events');

  application.inject('component:dialogs/modal-dialogs', 'modalDialogsCommunicationService', 'service:communication-between-components/modal-dialogs-events');

  application.inject('controller:image/viewer/compare-item', 'viewerChangedCommunicationService', 'service:communication-between-components/viewer-changed-events');
  application.inject('component:image/controls/zoom-percentages', 'viewerChangedCommunicationService', 'service:communication-between-components/viewer-changed-events');
  application.inject('controller:navigation/main', 'viewerChangedCommunicationService', 'service:communication-between-components/viewer-changed-events');
  application.inject('view:image/viewer/open-layers', 'modalDialogsCommunicationService', 'service:communication-between-components/modal-dialogs-events');
  application.inject('view:image/viewer/open-layers', 'instantNotificationsCommunicationService', 'service:communication-between-components/instant-notifications-events');
  application.inject('controller:image/viewer/open-layers', 'modalDialogsCommunicationService', 'service:communication-between-components/modal-dialogs-events');
  application.inject('controller:image/controls/image', 'modalDialogsCommunicationService', 'service:communication-between-components/modal-dialogs-events');
};

export default {
    name: "dependencyInjection",
    after: "store",


    initialize: function(container, application) {


      injectCommunicationBetweenComponents(application);
    }
};
