import Ember from "ember";
import DS from "ember-data";
export default {
    name: "overrideDefaultStore",
    before: "store",

    initialize: function(/*container, application*/) {

        function promiseArray(promise, label) {
            return DS.PromiseArray.create({
                promise: Ember.RSVP.Promise.cast(promise, label)
            });
        }

        function promiseObject(promise, label) {
            return DS.PromiseObject.create({
                promise: Ember.RSVP.Promise.cast(promise, label)
            });
        }

        function _findQuery(adapter, store, type, query, recordArray, id, extractType) {
            var promise = adapter.findQuery(store, type, query, id, recordArray),
                serializer = serializerForAdapter(adapter, type),
                label = "DS: Handle Adapter#findQuery of " + type;

            return Ember.RSVP.Promise.cast(promise, label).then(function(adapterPayload) {
                var payload = null;

                if(extractType === window.AppkitENV.singleRecord || !Ember.isNone(id)) {
                    extractType = 'find';
                    payload = serializer.extract(store, type, adapterPayload, id, extractType);
                    return store.push(type, payload);
                }
                extractType = 'findQuery';
                payload = serializer.extract(store, type, adapterPayload, null, extractType);
                Ember.assert("The response from a findQuery must be an Array, not " + Ember.inspect(payload), Ember.typeOf(payload) === 'array');
                recordArray.load(payload);
                return recordArray;

            }, null, "DS: Extract payload of findQuery " + type);
        }

        function serializerFor(container, type, defaultSerializer) {
            return container.lookup('serializer:'+type) ||
                container.lookup('serializer:application') ||
                container.lookup('serializer:' + defaultSerializer) ||
                container.lookup('serializer:-default');
        }

        function serializerForAdapter(adapter, type) {
            var serializer = adapter.serializer,
                defaultSerializer = adapter.defaultSerializer,
                container = adapter.container;

            if (container && serializer === undefined) {
                serializer = serializerFor(container, type.typeKey, defaultSerializer);
            }

            if (serializer === null || serializer === undefined) {
                serializer = {
                    extract: function(store, type, payload) { return payload; }
                };
            }

            return serializer;
        }

        function coerceId(id) {
            return id == null ? null : id+'';
        }

        DS.Store.reopen({
            find: function(type, id, query) {
                Ember.assert("You need to pass a type to the store's find method", arguments.length >= 1);
                Ember.assert("You may not pass `" + id + "` as id to the store's find method", arguments.length === 1 || !Ember.isNone(id));

                if (arguments.length === 1) {
                    return this.findAll(type);
                }

                // We are passed a query instead of an id.
                if (Ember.typeOf(id) === 'object') {
                    return this.findQuery(type, id);
                }

                if (Ember.typeOf(query) === 'object') {
                    return this.findQuery(type, query, coerceId(id));
                }

                return this.findById(type, coerceId(id));
            },

            findQuery: function(type, query, id, extractType) {
                type = this.modelFor(type);

                var array = this.recordArrayManager
                    .createAdapterPopulatedRecordArray(type, query);

                var adapter = this.adapterFor(type);

                Ember.assert("You tried to load a query but you have no adapter (for " + type + ")", adapter);
                //Ember.assert("You tried to load a query but your adapter does not implement `findQuery`", adapter.findQuery);

                if(extractType === window.AppkitENV.singleRecord || !Ember.isNone(id)) {
                    return promiseObject(_findQuery(adapter, this, type, query, array, id, extractType));
                }
                return promiseArray(_findQuery(adapter, this, type, query, array, id, extractType));
            }
        });
    }
};
