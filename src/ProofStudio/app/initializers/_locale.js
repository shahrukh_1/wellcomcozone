import Ember from "ember";
import LocaleFiles from '../locale/locale-keys';

var Locale = Ember.Object.extend({
    store: null,
    locale: window.AppkitENV.default_locale,
    localeFiles: null,

    change: function(localeName) {
        if (this.get('locale') == localeName)
          return;

        this.set('locale', localeName);
        this.initTranslations();
    }
});

var LocaleInitializer = {
    name: "Locale Initializer",
    after: "store",
    initialize: function(container, application) {
        var store = container.lookup('store:main');
        var locale = Locale.create({
            store: store
        });

        application.register('locale:main', locale, {  instantiate: false, singleton: true });

        container.typeInjection('controller', 'locale', 'locale:main');
        container.typeInjection('route', 'locale', 'locale:main');
        container.typeInjection('component', 'locale', 'locale:main');
    }
};

export default LocaleInitializer;
