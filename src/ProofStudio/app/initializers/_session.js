import Ember from "ember";

var Session = {
    name: "session",
    after: "store",

    initialize: function(container, application) {
        var session = Ember.Object.create();
        application.register('session:main', session, {  instantiate: false, singleton: true });

        container.typeInjection('controller', 'session', 'session:main');
        container.typeInjection('route', 'session', 'session:main');
        container.typeInjection('component', 'session', 'session:main');

        application.inject('service:communication-between-components/instant-notifications-events', 'session', 'session:main');
        application.inject('service:tiles/tiles-actions-service', 'session', 'session:main');
    }
};
export default Session;
