export default {
    name: "injectStoreIntoComponent",
    after: "store",

    initialize: function(container/*, application*/) {
        container.typeInjection('component', 'store', 'store:main');
        container.typeInjection('service', 'store', 'store:main');
        container.typeInjection('route', 'store', 'store:main');
    }
};
