import Ember from "ember";
import DS from "ember-data";

var ApplicationAdapter = DS.RESTAdapter.extend({
    host: window.AppkitENV.hostname,
    namespace: window.AppkitENV.namespace,

    buildURL: function(type, id, record, params) {
        params = params || {};

        //window.AppkitENV.machineId = '2015111023-5';

        if (window.AppkitENV.machineId) {
            Ember.$.extend(params, {
                key: window.AppkitENV.key,
                user_key: window.AppkitENV.user_key,
                is_external_user: window.AppkitENV.is_external_user,
                locale_name: window.AppkitENV.default_locale,
                session_key: window.AppkitENV.session_key,
                substituteLoggedUser: window.AppkitENV.substituteLoggedUser[1],
                machineId: window.AppkitENV.machineId
            });
        }
        else{
            Ember.$.extend(params, {
                key: window.AppkitENV.key,
                user_key: window.AppkitENV.user_key,
                is_external_user: window.AppkitENV.is_external_user,
                locale_name: window.AppkitENV.default_locale,
                session_key: window.AppkitENV.session_key,
                substituteLoggedUser: window.AppkitENV.substituteLoggedUser[1],
            });
        }

        if(id) {
            return this._super(type, id) + '?' + Ember.$.param(params);
        }
        return this._super(type) + '?' + Ember.$.param(params);
    },

    pathForType: function(type) {
        if(window.AppkitENV.singularModels.contains(type)){
            return type.dasherize();
        }
        return Ember.String.pluralize(type).dasherize();
    },

    findQuery: function(store, type, query, id) {
        return this.ajax(this.buildURL(type.typeKey, id), 'GET', { data: query, crossDomain: true });
    },

    //updateRecord: function(store, type, record) {
    //    var data = {};
    //    var serializer = store.serializerFor(type.typeKey);
    //
    //    serializer.serializeIntoHash(data, type, record);
    //
    //    var id = record.get('id');
    //    var params = {
    //        approval_id: window.AppkitENV.approval_id
    //    };
    //    return this.ajax(this.buildURL(type.typeKey, id, null, params), "PUT", { data: data });
    //},

    saveApprovalDecision: function(approval, data) {
        var id = approval.get('id');
        var Decision_id =approval.get('Decision.id');
        var currentPhase_id = approval.get('CurrentPhase');
        var params = {
            user_key: window.AppkitENV.user_key
        };
        Ember.$.extend(data, {
            decisionId: approval.get('Decision.id'),
            substituteLoggedUser: window.AppkitENV.substituteLoggedUser[1]
        });
        
        var ajaxResult = this.ajax(this.buildURL('approvaldecision', id, null, params), "PUT", { data: data });
        class Event {
            constructor(name, obj) {
                this.event = name;
                this.obj_ = obj;
            }
        }
        class ApprovalAnnotation_Update {
            constructor(approval_guid, user_name , is_external_user ,  approvalDecisionID) {
                this.approval_guid = approval_guid;
                this.user_name = user_name;
                this.is_external_user = is_external_user;
                this.approvalDecision = approvalDecisionID; 

            }
        }
        const approvalAnnotation_data = new ApprovalAnnotation_Update(window.AppkitENV.approval_guid, window.AppkitENV.user_name , window.AppkitENV.is_external_user,   Decision_id );
        const event = new Event('updateDecision', approvalAnnotation_data);
        window.parent.postMessage(event, '*');
        return ajaxResult;

    },

    copyAnnotation: function(annotation) {
      var params = {
        user_key: window.AppkitENV.user_key,
        key: window.AppkitENV.key
      };
      return this.ajax(this.buildURL('copy-annotation', null, null, params), "POST", { data: annotation });
    },
    loadPageIdAsSession: function(PageId, approval){
         return this.ajax(this.buildURL('loadPageIdAsSession'), "POST", {data: {Page:PageId, ApprovalId:approval } });
    },
    updateApprovalZoomLevel: function(zoomlevel, approval){
        return this.ajax(this.buildURL('approvalZoomLevel'), "POST", {data: {ZoomLevel:zoomlevel, ApprovalId:approval } });
   },
    loadSelectedApprovalAsSession: function(PageId, Approvaltypes, ScrolTop){
        return this.ajax(this.buildURL('loadSelectedApprovalAsSession'), "POST", {data: {Page:PageId, SelectedApprovalTypes: Approvaltypes, Top: ScrolTop } });
   },
   loadTagwordsandApprovalCount: function(SelectedProjectID, ApprovalTypes, Tagwords){
    return this.ajax(this.buildURL('loadTagwordsandApprovalCount'), "GET", {data: {ProjectID:SelectedProjectID, SelectedApprovalTypes:ApprovalTypes, SelectedTagwords:Tagwords } });
   },
    updateHtmlHeightWidth: function(height, width, pageid, approvalid){
          return this.ajax(this.buildURL('updateHtmlHeightWidth'), "POST" , {data: {Height:height, Width:width, Page:pageid, ApprovalId:approvalid}});
    },
    translations: function(locale, keys){
        return this.ajax(this.buildURL('translations'), "POST", { data: {TranslationKeys: keys, locale: locale } });
    },

    //updateVersionLocking: function(versions){
    //    var data = [];
    //     versions.forEach(function (version) {
    //         data.pushObject({Id: version.get('id'), isLocked: version.get('IsLocked')});
    //    });
    //
    //    var params = {
    //        user_key: window.AppkitENV.user_key
    //    };
    //
    //    return this.ajax(this.buildURL('versionlocking',null, null, params), "PUT", { data: {data:data} });
    //},

    updateAnnotationStatus: function(annotationId, annotationStatus){
        var params = {
            user_key: window.AppkitENV.user_key,
            key: window.AppkitENV.key
        };
       
        var ajaxResult = this.ajax(this.buildURL('update-annotation-status', null, params), "POST", { data: { id: annotationId, status: annotationStatus } });
        class Event {
            constructor(name, obj) {
                this.event = name;
                this.obj_ = obj;
            }
        }
        class approvalAnnotationDetail {
            constructor(approval_guid, approvalAnnotationId,approvalannotationStatus,  user_name , is_external_user ) {
                this.approval_guid = approval_guid;
                this.approvalAnnotationId = approvalAnnotationId;
                this.approvalannotationStatus = approvalannotationStatus;
                this.user_name = user_name;
                this.is_external_user = is_external_user;
            }
        }
        const approvalAnnotation_data = new approvalAnnotationDetail(window.AppkitENV.approval_guid , annotationId ,annotationStatus ,window.AppkitENV.user_name,  window.AppkitENV.is_external_user  );
        const event = new Event('updateAnnotationStatus', approvalAnnotation_data);
        window.parent.postMessage(event, '*');
        return ajaxResult;
    },
    updateAnnotationToPublic: function(annotationIds){
        var params = {
            user_key: window.AppkitENV.user_key,
            key: window.AppkitENV.key
        };
        return this.ajax(this.buildURL('update-public-annotation', null, null, params), "POST", { data: { id: annotationIds } });
    },
    updateAnnotationComment: function(annotationId, current_phase , annotationComment){
        var params = {
            user_key: window.AppkitENV.user_key,
            key: window.AppkitENV.key
        };
       
        var ajaxResult = this.ajax(this.buildURL('update-annotation-comment', null, null, params), "POST", { data: { id: annotationId, comment: annotationComment } });
        
        class Event {
            constructor(name, obj) {
                this.event = name;
                this.obj_ = obj;
            }
        }
        class ApprovalAnnotation_Update {
            constructor(approval_guid , approvalAnnotationId,  user_name, is_external_user) {
                this.approval_guid = approval_guid;
                this.approvalAnnotationId = approvalAnnotationId;
                this.user_name = user_name;
                this.is_external_user = is_external_user;
            }
        }
        const approvalAnnotation__data = new ApprovalAnnotation_Update(window.AppkitENV.approval_guid , annotationId , window.AppkitENV.user_name , window.AppkitENV.is_external_user  );
        const event = new Event('updateAnnotation', approvalAnnotation__data);
        window.parent.postMessage(event, '*');

        return ajaxResult;

    },
    updateShowAnnotationsAllPhases: function(isShowAnnotationsOfAllPhases){
      var params = {
        user_key: window.AppkitENV.user_key,
        key: window.AppkitENV.key
      };
      return this.ajax(this.buildURL('user-settings', null, null, params), "POST", { data: { showAnnotationsOfAllPhases: isShowAnnotationsOfAllPhases } });
    },

    updateOnApprovalInitialLoad: function(approvalsIds){
      return this.ajax(this.buildURL('approval-initial-load', null, null, null), "POST", { data: { approvalsIds: approvalsIds } });
    },

    SendNotificationThatVersionSwitchedToHighRes: function(versionId){
      return this.ajax(this.buildURL('version-switched-to-high-resolution', null, null, null), "POST", { data: { approvalId: versionId } });
    },

    checkHighresIsInProgress: function(versionId){
      return this.ajax(this.buildURL('check-highres-is-inprogress', null, null, null), "POST", { data: { approvalId: versionId } });
    }
});

export default ApplicationAdapter;
