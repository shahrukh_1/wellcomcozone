 var AppConstants = {};

AppConstants.MiniMap ={
  WIDTH: 200,
  HEIGHT: 200
};

AppConstants.ApprovalTypes = {
    TYPE_IMAGE: 'Image',
    TYPE_VIDEO: 'Movie',
    TYPE_WEBPAGE: 'WebPage',
    TYPE_ZIP: 'Zip'
};

AppConstants.SoftProofingLevel = {
    None: 0,
    Calibrated: 1,
    NotCalibrated: 2,
    Expired: 3
};

AppConstants.Annotation = {
    ANNOTATION_TYPE_MARKER: 1,
    ANNOTATION_TYPE_DRAW: 4,
    ANNOTATION_TYPE_TEXT: 3,
    ANNOTATION_TYPE_AREA: 2,
    ANNOTATION_TYPE_ADD_TEXT: 5,
    ANNOTATION_TYPE_COLOR_PICKER: 6,
    ANNOTATION_TYPE_LINE_MARKER: 7,

    ANNOTATION_STATUS_APPROVED: 1,
    ANNOTATION_STATUS_DECLINED: 2,
    ANNOTATION_STATUS_NEUTRAL: 3,

    AnnotationType: {}
};
AppConstants.Annotation.AnnotationType[AppConstants.Annotation.ANNOTATION_TYPE_MARKER] = {
    icon: 'fa-comment-o'
};
AppConstants.Annotation.AnnotationType[AppConstants.Annotation.ANNOTATION_TYPE_DRAW] = {
    icon: 'fa-pencil'
};
AppConstants.Annotation.AnnotationType[AppConstants.Annotation.ANNOTATION_TYPE_TEXT] = {
    icon: 'fa-text-height'
};
AppConstants.Annotation.AnnotationType[AppConstants.Annotation.ANNOTATION_TYPE_LINE_MARKER] = {
    icon: 'fa-minus'
};
AppConstants.Annotation.AnnotationType[AppConstants.Annotation.ANNOTATION_TYPE_COLOR_PICKER] = {
  icon: 'fa-eyedropper'
};

AppConstants.Annotation.AnnotationStatus = {};
AppConstants.Annotation.AnnotationStatus[AppConstants.Annotation.ANNOTATION_STATUS_APPROVED] = {
    text: 'approved',
    title: 'Approved',
    iconHeader: 'czn-icon-status-header-approved',
    icon: 'czn-icon-status-approved',
    key: 'A'
};
AppConstants.Annotation.AnnotationStatus[AppConstants.Annotation.ANNOTATION_STATUS_DECLINED] = {
    text: 'declined',
    title: 'Declined',
    iconHeader: 'czn-icon-status-header-declined',
    icon: 'czn-icon-status-declined',
    key: 'D'
};
AppConstants.Annotation.AnnotationStatus[AppConstants.Annotation.ANNOTATION_STATUS_NEUTRAL] = {
    text: 'neutral',
    title: 'Neutral',
    iconHeader: 'czn-icon-status-header-neutral',
    icon: 'czn-icon-status-neutral',
    key: 'N'
};

AppConstants.Date = {
    DateFormat: {
        'MM/dd/yyyy': 'MM/DD/YYYY', // 04/16/2012
        'dd/MM/yyyy': 'DD/MM/YYYY', // 16/04/2012
        'yyyy-MM-dd': 'YYYY-MM-DD', // 2012-04-16
        'dd.MM.yyyy': 'DD.MM.YYYY', // 16.04.2012
        'yyyy.MM.dd': 'YYYY.MM.DD', // 2012.04.16
        'yyyy/MM/dd': 'YYYY/MM/DD' // 2012/04/16
    }
};

AppConstants.Time = {
    HOURS_12: '12-hour clock',
    HOURS_24: '24-hour clock',
    TimeFormat: {
        DEFAULT: 'h:mm a'
    }
};
AppConstants.Time.TimeFormat[AppConstants.Time.HOURS_12] = 'h:mm:ss a';
AppConstants.Time.TimeFormat[AppConstants.Time.HOURS_24] = 'H:mm';


AppConstants.Permissions = {
    READ_ONLY: 'RDO',
    REVIEW: 'RVW',
    APPROVE_REVIEW: 'ANR',
    RETOUCHER: 'RET',
    canApprove: {},
    canAnnotate: {}
};

AppConstants.UserRoles = {
    ADMIN: 'ADM',
    MANAGER: 'MAN',
    MODERATOR: 'MOD',
    CONTRIBUTOR: 'CON',
    RETOUCHER: 'RET',
    VIEWER: 'VIE'
};

AppConstants.CommentTypes = {
    MARKER_COMMENT: 1,
    AREA_COMMENT: 2,
    TEXT_COMMENT: 3,
    DRAW_COMMENT: 4,
    ADD_TEXT_COMMENT: 5,
    COLOR_PICKER_COMMENT: 6,
    LINE_COMMENT: 7,
    MARQUEE_RECTANGLE: 8
};

AppConstants.HighlightType = {
    Normal: 1,
    Insert: 2,
    Replace: 3,
    Delete: 4
};

AppConstants.SoftProofingLevel = {
    Calibrated: 1,
    NotCalibrated: 2,
    Expired: 3
};

AppConstants.TetmlFile = 'TextDescriptor.xml';

AppConstants.Permissions.canApprove[AppConstants.Permissions.READ_ONLY] = true;
AppConstants.Permissions.canApprove[AppConstants.Permissions.REVIEW] = false;
AppConstants.Permissions.canApprove[AppConstants.Permissions.APPROVE_REVIEW] = true;
AppConstants.Permissions.canAnnotate[AppConstants.Permissions.READ_ONLY] = false;
AppConstants.Permissions.canAnnotate[AppConstants.Permissions.REVIEW] = true;
AppConstants.Permissions.canAnnotate[AppConstants.Permissions.APPROVE_REVIEW] = true;

AppConstants.SVGFormat = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"{width}\" height=\"{height}\" xml:space=\"preserve\"><g transform=\"{translate}\">{svg}</g></svg>";

AppConstants.PinMarker = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" width=\"130\" height=\"130\" xml:space=\"preserve\"><circle cx=\"65\" cy=\"65\" r=\"65\" style=\"stroke: none; stroke-width: 2; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; stroke: #000000; stroke-width: 1; fill-rule: nonzero; fill: #color;\"/><circle cx=\"65\" cy=\"65\" r=\"50\" style=\"stroke: none; stroke-width: 2; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: #000000; fill-rule: nonzero;\"/><g transform=\"translate(50 80)\"><text font-family=\"helvetica\" font-size=\"60\" font-weight=\"bold\" style=\"stroke: none; stroke-width: 1; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 10; fill: #ffffff; fill-rule: nonzero; opacity: 1;\"><tspan x=\"0\" y=\"0\" fill=\"#ffffff\">#index</tspan></text></g></svg>";

AppConstants.Decision = {
  None: 0,
  Pending: 1,
  ChangesRequired: 2,
  ApprovedWithChanges: 3,
  Approved: 4,
  NotApplicable: 5
  };

AppConstants.DecisionIcons = [];
AppConstants.DecisionIcons[AppConstants.Decision.None] = '';
AppConstants.DecisionIcons[AppConstants.Decision.Pending] = 'fa fa-minus-circle czn-decision-pending';
AppConstants.DecisionIcons[AppConstants.Decision.ChangesRequired] = 'fa fa-exclamation-circle czn-decision-changes-required';
AppConstants.DecisionIcons[AppConstants.Decision.ApprovedWithChanges] = 'fa fa-check-circle-o czn-decision-approved-with-changes';
AppConstants.DecisionIcons[AppConstants.Decision.Approved] = 'fa fa-check-circle czn-decision-approved';
AppConstants.DecisionIcons[AppConstants.Decision.NotApplicable] = 'fa fa-times-circle czn-decision-not-applicable';

AppConstants.SmallestTileUrl = 'CompletedMarker.txt';
AppConstants.PartialTileUrl = 'PartialCompletedMarker.txt';
AppConstants.TileSize = 256;
AppConstants.ZoomStep = 0.25;

AppConstants.Inch2Milimeters = 25.4; // once inch has 25.4mm

AppConstants.InstantNotificationEntityType = {
	Annotation: 1,
	Decision: 2,
	Collaborator: 3,
  ViewingConditions: 4,
  VersionSwitchedToHighRes: 5,
  TilesAreReady:6
};

AppConstants.InstantNotificationOperationType = {
	Added: 1,
	Modified: 2,
	Deleted: 3
};

AppConstants.JobStatus = {
    InProgress: 'INP',
    Completed:'CMP',
    Archived: 'ARC'
};

AppConstants.CMYK = {
    EmbeddedProfile: 'EmbeddedProfile'
};

AppConstants.Features = {
  RotateTool: 1,
  Separations: 2,
  Densitometer: 3,
  Ruler: 4,
  CustomProfile: 5,
  ProfilePreview: 6
};

AppConstants.ConfirmDialogMode = {
  PhaseComplete: 'PhaseComplete',
  AnnotationChanges:'AnnotationChanges',
  ExternalUserClose: 'ExternalUserClose',
  ChangesComplete: 'ChangesComplete',
  AnnotationDoesNotFit: 'AnnotationDoesNotFit'
};

AppConstants.ExtraSmallDevicesWidth = 768; // maximum width .hidden-xs
AppConstants.SmallDevicesWidth = 992; // maximum width .hidden-sm
AppConstants.MediumDevicesWidth = 1200; // maximum width .hidden-md
AppConstants.NumberOfApprovalsPerPageInProofStudio = 50;

AppConstants.ColorsHEX = {
  Red: '#FF0000'
};

AppConstants.BottomBarHeight = 38;

AppConstants.UltraHighResolutionTriggerStatus = {
  UHRNotTriggered: 0,
  UHRTriggeredFirstTimeByButton: 1,
  UHRTriggeredAndAlreadyInProgress: 2
}

export default AppConstants;
