import Ember from 'ember';

var Router = Ember.Router.extend({
  location: window.AppkitENV.locationType
});

Router.map(function() {
  this.route('approval', {path: '/'});
});

export default Router;
