import Ember from "ember";

var removeBadChars = Ember.Handlebars.makeBoundHelper(function(text) {
    var textChanged = Ember.String.htmlSafe(text);
    return textChanged;
});
export default removeBadChars;