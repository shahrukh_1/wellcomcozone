import Ember from "ember";

var digitalClock = Ember.Handlebars.makeBoundHelper(function(time) {
    var t = Math.round(time);
    var minutes = parseInt((t / 60) % 60);
    var seconds = parseInt(t % 60);
    var hours = parseInt(t/(60 * 60));

    if(seconds <= 9){
       seconds = '0' + seconds;
    }
    if(hours <= 9){
        hours = '0' + hours;
    }
    if(minutes <= 9){
        minutes = '0' + minutes;
    }

    if(hours === '00'){
        return [minutes, seconds].join(':');
    }else {
        return [hours, minutes, seconds].join(':');
    }
});
export default digitalClock;