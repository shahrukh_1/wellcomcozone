import Ember from "ember";

var breakLine = Ember.Handlebars.makeBoundHelper(function(text) {
    text = Ember.Handlebars.Utils.escapeExpression(text);
    text = text.replace(/(\r\n|\n|\r)/gm, '<br>');
    return new Ember.Handlebars.SafeString(text);
});
export default breakLine;
