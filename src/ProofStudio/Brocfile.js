/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp({
    storeConfigInMeta: false,
    fingerprint: {
        exclude: ['assets/images']
    },
    outputPaths: {
      app: {
        html: 'index.html',
        css: {
          'app': '/assets/app.min.css'
        },
        js: '/assets/app.min.js'
      },
      vendor: {
        css: '/assets/vendor.min.css',
        js: '/assets/vendor.min.js'
      }
    }
});

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

//app.import('bower_components/bootstrap/dist/css/bootstrap.css');
//app.import('bower_components/bootstrap/dist/css/bootstrap.css.map', {
//  destDir: 'assets'
//});
app.import('bower_components/openlayers3/build/ol.css');
app.import('app/styles/theme.css');
app.import('bower_components/spectrum/spectrum.css');

app.import('bower_components/jquery/dist/jquery.js');
app.import('bower_components/jquery-ui/jquery-ui.js');
app.import('bower_components/bootstrap/dist/js/bootstrap.js');
app.import('bower_components/ember-i18n/lib/i18n.js');

app.import('vendor/ol3/ol.js');
app.import('vendor/ol3/ol.css');
app.import('vendor/ol3/map.js');
app.import('vendor/pixelmatch.js');
app.import('vendor/pixelmatch_util.js');
app.import('bower_components/fabric/dist/fabric.js');
app.import('bower_components/moment/moment.js');
app.import('bower_components/spectrum/spectrum.js');
app.import('bower_components/jquery-cookie/jquery.cookie.js');
app.import('bower_components/jquery-ui-touch-punch/jquery.ui.touch-punch.js');
app.import('bower_components/delta-e/dist/deltae.global.min.js');
app.import('vendor/quill/quill.min.js');
app.import('vendor/quill/quill.snow.css');

module.exports = app.toTree();
