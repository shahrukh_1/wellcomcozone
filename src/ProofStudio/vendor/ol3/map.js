ol.Map.prototype.hasLayer = function(layer){
    return true;
};

ol.Map.prototype.clearLayers = function(){

};

ol.Overlay.prototype.clearLayers = function(){

};

ol.Map.prototype.getZoom = function(){
    var view = this.getView();
    return view.getZoom();
};

ol.Map.prototype.getMaxZoom = function(){
    return 4;
};

ol.Map.prototype.getMinZoom = function(){
    return 0;
};

/// the following methods were copied from Leaflet ///

ol.Map.unproject = function(point){
  return point;
};
ol.Map.project = function(latlng){
    return latlng;
};

ol.Map.prototype.latLngToContainerPoint = function(arr){
  return {x: 0, y: 0};
};

/// the above methods were copied and adapted from Leaflet ///

ol.Map.prototype.disablePan = function(){
   var interactions = this.getInteractions();
   if (interactions) {
      interactions.defaults({dragPan: false});
   }
};

ol.Map.prototype.enablePan = function(){
  var interactions = this.getInteractions();
  if (interactions) {
    interactions.defaults({dragPan: true});
  }
};




