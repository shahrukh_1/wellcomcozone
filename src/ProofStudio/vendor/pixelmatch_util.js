
var myDiffExecutionsCount=0;
var myV1Img=null, myV2Img=null;
var myV1Version='', myV2Version='';
var myThreshold=0.13;

function myChangeSelectedColor(mySelectedDiv) {
  var mySelectedColorList = document.getElementsByClassName('diffColorPickerSelected');
  for (var i=0;i<mySelectedColorList.length;i++)
  {
    mySelectedColorList[i].classList.remove("diffColorPickerSelected");
  }
  mySelectedDiv.classList.add("diffColorPickerSelected");
  myprm_DiffColor_div2.style.backgroundColor=mySelectedDiv.style.backgroundColor;
}
