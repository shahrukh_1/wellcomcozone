﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Xml.Linq;
using System.Diagnostics;

namespace GMGColorResourceFilesGenerator
{
    public partial class frmMain : Form
    {
        #region Fields

        Dictionary<string, string> dicResult;// = new Dictionary<string, string>();

        #endregion

        #region Conscturcors

        public frmMain()
        {
            InitializeComponent();
            InitFilePaths();
        }

        #endregion

        #region Events

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilePath.Text))
            {
                MessageBox.Show("Please select your xl file", "Please select", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (Path.GetExtension(txtFilePath.Text) != ".xlsx" && Path.GetExtension(txtFilePath.Text) != ".xls")
            {
                MessageBox.Show("Invalid xl file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (string.IsNullOrEmpty(txtTargetFolder.Text))
            {
                MessageBox.Show("Please select target folder", "Please select", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (!File.Exists(txtResourceProjectPath.Text + "\\" + "Resources.resx"))
            {
                MessageBox.Show("Invalid Resource Project Folder Path", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    progressBar1.Visible = true;
                    progressBar1.Minimum = 1;
                    progressBar1.Maximum = 15;
                    progressBar1.Value = 1;
                    Cursor.Current = Cursors.WaitCursor;

                    string resourceFolderPath = txtResourceProjectPath.Text;
                    string englishResourceFilePath = Path.Combine(resourceFolderPath, "Resources.resx");
                    string sourceXlFilePath = txtFilePath.Text;
                    string newXLFilePath = txtTargetFolder.Text;
                    string newXMLFilePath = Path.Combine(newXLFilePath, "XMLFiles");

                    bool isExtraElementFileCreated = false;
                    bool isDeletedElementFileCreated = false;
                    int newRecCount = 0;
                    int updatedRecCount = 0;
                    int extraElementCount = 0;
                    int deletedElementCount = 0;

                    if (!Directory.Exists(newXMLFilePath))
                    {
                        Directory.CreateDirectory(newXMLFilePath);
                    }

                    dicResult = new Dictionary<string, string>();
                    Dictionary<Languages, Dictionary<string, string>> dicXLValues = this.ReadXLFile(sourceXlFilePath);
                    List<Resource> lstEnglishResourceValues = this.ReadResourceFile(englishResourceFilePath);
                    Dictionary<string, string> dicEnglishXlValues = dicXLValues[Languages.English];

                    List<string> lstNewValuesToBeAdded = lstEnglishResourceValues.Select(o => o.ControllerID.ToUpper()).Except(dicEnglishXlValues.Select(o => o.Key.ToUpper())).ToList();//Exist on resource but not in Excel file
                    List<string> lstValuesToBeDeleted = dicEnglishXlValues.Select(o => o.Key.ToUpper()).Except(lstEnglishResourceValues.Select(o => o.ControllerID.ToUpper())).ToList(); //Exist on Excel but not in resource file

                    Dictionary<string, string> dicNewValuesToBeAdded = new Dictionary<string, string>();
                    Dictionary<string, string> dicValuesToBeDeleted = new Dictionary<string, string>();

                    if (lstNewValuesToBeAdded.Count > 0)
                    {
                        extraElementCount = lstNewValuesToBeAdded.Count;
                        foreach (string item in lstNewValuesToBeAdded)
                        {
                            string controllerID = lstEnglishResourceValues.Single(o => o.ControllerID.ToUpper() == item.ToUpper()).ControllerID;
                            string value = lstEnglishResourceValues.Single(o => o.ControllerID.ToUpper() == item.ToUpper()).Value;

                            dicNewValuesToBeAdded.Add(controllerID, value);
                        }

                        //this.CreateXLFile( Path.Combine( newXLFilePath, "NewlyAddedFields.xls"), dicNewValuesToBeAdded, "Newly Added Fields");
                        isExtraElementFileCreated = true;
                    }

                    if (lstValuesToBeDeleted.Count > 0)
                    {
                        deletedElementCount = lstValuesToBeDeleted.Count;
                        foreach (string item in lstValuesToBeDeleted)
                        {
                            string controllerID = dicEnglishXlValues.Single(o => o.Key.ToUpper() == item.ToUpper()).Key;
                            string value = dicEnglishXlValues[controllerID];

                            dicValuesToBeDeleted.Add(controllerID, value);
                        }

                        //this.CreateXLFile(Path.Combine(newXLFilePath, "DeletedFields.xls"), dicValuesToBeDeleted, "Deleted Fields");
                        isDeletedElementFileCreated = true;
                    }

                    for (int i = 1; i < 16; i++)
                    {   
                        string xmlFilePath = string.Empty;

                        switch (i)
                        {
                            case (int)Languages.English:
                                xmlFilePath = Path.Combine(newXMLFilePath, "English.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.English, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.English], lstEnglishResourceValues, Languages.English.ToString());
                                break;
                            case (int)Languages.German:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.de-DE.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.German, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.German], lstEnglishResourceValues, Languages.German.ToString());
                                break;
                            case (int)Languages.Korean:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.ko-KR.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Korean, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Korean], lstEnglishResourceValues, Languages.Korean.ToString());
                                break;
                            case (int)Languages.Japanese:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.ja-JP.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Japanese, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Japanese], lstEnglishResourceValues, Languages.Japanese.ToString());
                                break;
                            case (int)Languages.Chinese_Simplified:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.zh-Hans.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Chinese_Simplified, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Chinese_Simplified], lstEnglishResourceValues, Languages.Chinese_Simplified.ToString());
                                break;
                            case (int)Languages.Chinese_Traditional:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.zh-Hant.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Chinese_Traditional, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Chinese_Traditional], lstEnglishResourceValues, Languages.Chinese_Traditional.ToString());
                                break;
                            case (int)Languages.Portuguse:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.pt-PT.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Portuguse, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Portuguse], lstEnglishResourceValues, Languages.Portuguse.ToString());
                                break;
                            case (int)Languages.Spanish:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.es-ES.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Spanish, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Spanish], lstEnglishResourceValues, Languages.Spanish.ToString());
                                break;
                            case (int)Languages.French:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.fr-FR.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.French, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.French], lstEnglishResourceValues, Languages.French.ToString());
                                break;
                            case (int)Languages.Italian:
                                xmlFilePath = Path.Combine(newXMLFilePath, "Resources.it-IT.resx");
                                this.UpdateDictionary(ref dicXLValues, Languages.Italian, dicNewValuesToBeAdded, dicValuesToBeDeleted);
                                this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Italian], lstEnglishResourceValues, Languages.Italian.ToString());
                                break;
							case (int)Languages.Polish:
								xmlFilePath = Path.Combine(newXMLFilePath, "Resources.pl-PL.resx");
								this.UpdateDictionary(ref dicXLValues, Languages.Polish, dicNewValuesToBeAdded, dicValuesToBeDeleted);
								this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Polish], lstEnglishResourceValues, Languages.Polish.ToString());
								break;
							case (int)Languages.Danish:
								xmlFilePath = Path.Combine(newXMLFilePath, "Resources.da-DK.resx");
								this.UpdateDictionary(ref dicXLValues, Languages.Danish, dicNewValuesToBeAdded, dicValuesToBeDeleted);
								this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Danish], lstEnglishResourceValues, Languages.Danish.ToString());
								break;
							case (int)Languages.Dutch:
								xmlFilePath = Path.Combine(newXMLFilePath, "Resources.nl-NL.resx");
								this.UpdateDictionary(ref dicXLValues, Languages.Dutch, dicNewValuesToBeAdded, dicValuesToBeDeleted);
								this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Dutch], lstEnglishResourceValues, Languages.Dutch.ToString());
								break;
							case (int)Languages.Norwegian:
								xmlFilePath = Path.Combine(newXMLFilePath, "Resources.nn-NO.resx");
								this.UpdateDictionary(ref dicXLValues, Languages.Norwegian, dicNewValuesToBeAdded, dicValuesToBeDeleted);
								this.GenerateNewResourceFile(sourceXlFilePath, xmlFilePath, dicXLValues[Languages.Norwegian], lstEnglishResourceValues, Languages.Norwegian.ToString());
								break;
							default:
                                break;
                        }
                    }

                    //this.CreateXLFile(Path.Combine(newXLFilePath, "ForGMG.xls"), dicXLValues, "For GMG");
                    UpdateXLFile(sourceXlFilePath, dicNewValuesToBeAdded, dicValuesToBeDeleted);

                    Cursor.Current = Cursors.Default;
                    progressBar1.Value = 1;
                    progressBar1.Visible = false;
                    //MessageBox.Show("Resource file generation compleated sucessfully.\n" + updatedRecCount + " existing values updated.\n" + newRecCount + " new values added to the resource file."
                    //                , "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    MessageBox.Show("Files generation compleated sucessfully.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Resource file generation failed.\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            txtFilePath.Text = ((System.Windows.Forms.FileDialog)(sender)).FileName;
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtTargetFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btnBrowseProject_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtResourceProjectPath.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        #endregion

        #region Methods

        private void InitFilePaths()
        {
            DirectoryInfo projDir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            DirectoryInfo translationToolFolder = projDir.Parent.Parent.Parent.Parent;
            txtTargetFolder.Text = Path.Combine(translationToolFolder.FullName, "TranslationData");
            txtFilePath.Text = Path.Combine(txtTargetFolder.Text, "GMG_dotnet.xls");
            txtResourceProjectPath.Text = Path.Combine(translationToolFolder.Parent.Parent.FullName,"gmgcozone","GMGColorResources");

        }

        private void GenerateNewResourceFile(string xlFilePath, string resourceFilePath, Dictionary<string, string> dicResourceValues, List<Resource> lstResourceValues, string fileDisplayName)
        {
            using (XmlWriter writer = XmlWriter.Create(resourceFilePath))
            {
                string xmlNamespace = "http://www.w3.org/2001/XMLSchema";
                writer.WriteStartDocument();
                writer.WriteStartElement("root");

                #region xsd:schema

                writer.WriteStartElement("xsd", "schema", xmlNamespace);
                writer.WriteAttributeString("id", "root");
                writer.WriteAttributeString("xmlns", "");
                writer.WriteAttributeString("xmlns", "msdata", null, "urn:schemas-microsoft-com:xml-msdata");

                writer.WriteStartElement("import", xmlNamespace);
                writer.WriteAttributeString("namespace", "http://www.w3.org/XML/1998/namespace");
                writer.WriteEndElement();

                #region element

                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "root");
                writer.WriteAttributeString("msdata", "IsDataSet", null, "true");

                writer.WriteStartElement("complexType", xmlNamespace);
                writer.WriteStartElement("choice", xmlNamespace);
                writer.WriteAttributeString("maxOccurs", "unbounded");

                #region element 1

                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "metadata");

                writer.WriteStartElement("complexType", xmlNamespace);
                writer.WriteStartElement("sequence", xmlNamespace);
                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "value");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("minOccurs", "0");
                writer.WriteEndElement();//element
                writer.WriteEndElement();//sequence

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "name");
                writer.WriteAttributeString("use", "required");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "type");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "mimetype");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("ref", "xml:space");
                writer.WriteEndElement();

                writer.WriteEndElement();//complexType
                writer.WriteEndElement();//element

                #endregion

                #region element 2

                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "assembly");

                writer.WriteStartElement("complexType", xmlNamespace);

                //writer.WriteStartElement("attribute", xmlNamespace);
                //writer.WriteAttributeString("name", "name");
                //writer.WriteAttributeString("use", "required");
                //writer.WriteAttributeString("type", "xsd:string");
                //writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "alias");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "name");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteEndElement();

                writer.WriteEndElement();//complexType
                writer.WriteEndElement();//element

                #endregion

                #region element 3

                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "data");

                writer.WriteStartElement("complexType", xmlNamespace);
                writer.WriteStartElement("sequence", xmlNamespace);
                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "value");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("minOccurs", "0");
                writer.WriteAttributeString("msdata", "Ordinal", null, "1");
                writer.WriteEndElement();//element
                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "comment");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("minOccurs", "0");
                writer.WriteAttributeString("msdata", "Ordinal", null, "2");
                writer.WriteEndElement();//element
                writer.WriteEndElement();//sequence

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "name");
                writer.WriteAttributeString("use", "required");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("msdata", "Ordinal", null, "1");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "type");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("msdata", "Ordinal", null, "3");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "mimetype");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("msdata", "Ordinal", null, "4");
                writer.WriteEndElement();

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("ref", "xml:space");
                writer.WriteEndElement();

                writer.WriteEndElement();//complexType
                writer.WriteEndElement();//element

                #endregion

                #region element 4

                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "resheader");

                writer.WriteStartElement("complexType", xmlNamespace);
                writer.WriteStartElement("sequence", xmlNamespace);
                writer.WriteStartElement("element", xmlNamespace);
                writer.WriteAttributeString("name", "value");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteAttributeString("minOccurs", "0");
                writer.WriteAttributeString("msdata", "Ordinal", null, "1");
                writer.WriteEndElement();//element
                writer.WriteEndElement();//sequence

                writer.WriteStartElement("attribute", xmlNamespace);
                writer.WriteAttributeString("name", "name");
                writer.WriteAttributeString("use", "required");
                writer.WriteAttributeString("type", "xsd:string");
                writer.WriteEndElement();

                writer.WriteEndElement();//complexType
                writer.WriteEndElement();//element

                #endregion

                writer.WriteEndElement();//choice
                writer.WriteEndElement();//complexType
                writer.WriteEndElement();//element

                #endregion

                writer.WriteEndElement();//xsd

                #endregion

                #region resheaders

                writer.WriteStartElement("resheader");
                writer.WriteAttributeString("name", "resmimetype");
                writer.WriteStartElement("value");
                writer.WriteValue("text/microsoft-resx");
                writer.WriteEndElement();
                writer.WriteEndElement(); //resheader

                writer.WriteStartElement("resheader");
                writer.WriteAttributeString("name", "version");
                writer.WriteStartElement("value");
                writer.WriteValue("2.0");
                writer.WriteEndElement();
                writer.WriteEndElement(); //resheader

                writer.WriteStartElement("resheader");
                writer.WriteAttributeString("name", "reader");
                writer.WriteStartElement("value");
                writer.WriteValue("System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
                writer.WriteEndElement();
                writer.WriteEndElement(); //resheader

                writer.WriteStartElement("resheader");
                writer.WriteAttributeString("name", "writer");
                writer.WriteStartElement("value");
                writer.WriteValue("System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
                writer.WriteEndElement();
                writer.WriteEndElement(); //resheader

                #endregion

                foreach (KeyValuePair<string, string> item in dicResourceValues)
                {
                    Resource objResource = lstResourceValues.SingleOrDefault(o => o.ControllerID == item.Key);

                    writer.WriteStartElement("data");
                    writer.WriteAttributeString("name", item.Key);
                    writer.WriteAttributeString("xml", "space", null, "preserve");

                    writer.WriteStartElement("value");
                    writer.WriteValue(item.Value);
                    writer.WriteEndElement();

                    writer.WriteStartElement("comment");
                    writer.WriteValue((objResource != null) ? objResource.Comment : string.Empty);
                    writer.WriteEndElement();

                    writer.WriteEndElement(); //data                    
                }

                writer.WriteEndElement(); //root
                writer.WriteEndDocument();

                this.BindDataGrid("'" + fileDisplayName + "' File Generated sucessfully.", resourceFilePath);
            }
        }

        private Dictionary<Languages, Dictionary<string, string>> ReadXLFile(string xlFilePath)
        {
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excel.Workbooks.Open(xlFilePath);
            Worksheet sheet = (Worksheet)wb.ActiveSheet;
            Dictionary<Languages, Dictionary<string, string>> dicResourceValues = new Dictionary<Languages, Dictionary<string, string>>();

            try
            {
                for (int i = 2; i < 16; i++)
                {
                    int percent = (int)(((double)progressBar1.Value / (double)progressBar1.Maximum) * 100);
                    progressBar1.Value = progressBar1.Value + 1;
                    progressBar1.CreateGraphics().DrawString("Reading Excel File...( " + percent.ToString() + "% )", new System.Drawing.Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(progressBar1.Width / 2 - 10, progressBar1.Height / 2 - 7));

                    switch (i)
                    {
                        case (int)Languages.English://English
                            dicResourceValues.Add(Languages.English, this.ReadXLFile(sheet, (int)Languages.English));
                            break;
                        case (int)Languages.German://German
                            dicResourceValues.Add(Languages.German, this.ReadXLFile(sheet, (int)Languages.German));
                            break;
                        case (int)Languages.Korean://Korean
                            dicResourceValues.Add(Languages.Korean, this.ReadXLFile(sheet, (int)Languages.Korean));
                            break;
                        case (int)Languages.Japanese://Japanese
                            dicResourceValues.Add(Languages.Japanese, this.ReadXLFile(sheet, (int)Languages.Japanese));
                            break;
                        case (int)Languages.Chinese_Simplified://Chinese_Simplified
                            dicResourceValues.Add(Languages.Chinese_Simplified, this.ReadXLFile(sheet, (int)Languages.Chinese_Simplified));
                            break;
                        case (int)Languages.Chinese_Traditional://Chinese_Traditional
                            dicResourceValues.Add(Languages.Chinese_Traditional, this.ReadXLFile(sheet, (int)Languages.Chinese_Traditional));
                            break;
                        case (int)Languages.Portuguse://Portuguse
                            dicResourceValues.Add(Languages.Portuguse, this.ReadXLFile(sheet, (int)Languages.Portuguse));
                            break;
                        case (int)Languages.Spanish://Spanish
                            dicResourceValues.Add(Languages.Spanish, this.ReadXLFile(sheet, (int)Languages.Spanish));
                            break;
                        case (int)Languages.French://French
                            dicResourceValues.Add(Languages.French, this.ReadXLFile(sheet, (int)Languages.French));
                            break;
                        case (int)Languages.Italian://Italian
                            dicResourceValues.Add(Languages.Italian, this.ReadXLFile(sheet, (int)Languages.Italian));
                            break;
						case (int)Languages.Polish://Polish - Poland
							dicResourceValues.Add(Languages.Polish, this.ReadXLFile(sheet, (int)Languages.Polish));
							break;
						case (int)Languages.Danish://Danish - Denmark
							dicResourceValues.Add(Languages.Danish, this.ReadXLFile(sheet, (int)Languages.Danish));
							break;
						case (int)Languages.Dutch://Dutch - Netherlands
							dicResourceValues.Add(Languages.Dutch, this.ReadXLFile(sheet, (int)Languages.Dutch));
							break;
						case (int)Languages.Norwegian://Norwegian (Nynorsk) - Norway
							dicResourceValues.Add(Languages.Norwegian, this.ReadXLFile(sheet, (int)Languages.Norwegian));
							break;
						default:
                            break;
                    }
                }

                wb.Close(Missing.Value, Missing.Value, Missing.Value);
                excel.Quit();
                this.KillExcel();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            progressBar1.Refresh();
            return dicResourceValues;
        }

        private void UpdateXLFile(string xlFilePath, Dictionary<string, string> dicNewValuesToBeAdded, Dictionary<string, string> dicValuesToBeDeleted)
        {
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excel.Workbooks.Open(xlFilePath);
            Worksheet sheet = (Worksheet)wb.ActiveSheet;
            if (dicValuesToBeDeleted.Count > 0)
            {
                //get first column containing identifier
                Range firstColumn = sheet.UsedRange.Columns[1];
                foreach (KeyValuePair<string, string> item in dicValuesToBeDeleted)
                {
                    Range currentFind = null;
                    currentFind = firstColumn.Find(item.Key, Missing.Value,
                                    XlFindLookIn.xlValues, XlLookAt.xlPart,
                                    XlSearchOrder.xlByColumns, XlSearchDirection.xlNext, false,
                                    Missing.Value, Missing.Value);

                    if (currentFind != null)
                    {
                        //delete entire row from excel
                        ((Range)sheet.Rows[currentFind.Row]).Delete(XlDeleteShiftDirection.xlShiftUp);
                    }
                }
            }

            int rowCount = sheet.UsedRange.Rows.Count;

            int i = 1;
            //add new values from resources
            foreach (KeyValuePair<string, string> item in dicNewValuesToBeAdded)
            {
                sheet.Cells[rowCount + i, 1] = item.Key;
                sheet.Cells[rowCount + i, (int)Languages.English] = item.Value;
                i++;
            }

            wb.SaveAs(xlFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
            Missing.Value, Missing.Value, Missing.Value,
            Missing.Value, Missing.Value);
            wb.Close(Missing.Value, Missing.Value, Missing.Value);
            excel.Quit();
            KillExcel();
            sheet = null;
            wb = null;
        }

        /// <summary>
        /// Updates the translation excel file from the resources file for each language
        /// </summary>
        /// <param name="xlsFilePath"></param>
        /// <param name="resourceFolder"></param>
        private void UpdateXLSFileFromResources(string xlsFilePath, string resourceFolder)
        {
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excel.Workbooks.Open(xlsFilePath);
            Worksheet sheet = (Worksheet)wb.ActiveSheet;

            for (int i = 9; i < 16; i++)
            {
                string resourceFile = String.Empty;
                List<Resource> languageItems = new List<Resource>();
                switch (i)
                {
                    case (int)Languages.Korean:
                        resourceFile = Path.Combine(resourceFolder, "Resources.ko-KR.resx");
                        break;
                    case (int)Languages.Japanese:
                        resourceFile = Path.Combine(resourceFolder, "Resources.ja-JP.resx");
                        break;
                    case (int)Languages.Chinese_Simplified:
                        resourceFile = Path.Combine(resourceFolder, "Resources.zh-Hans.resx");
                        break;
                    case (int)Languages.Chinese_Traditional:
                        resourceFile = Path.Combine(resourceFolder, "Resources.zh-Hant.resx");
                        break;
                    case (int)Languages.Portuguse:
                        resourceFile = Path.Combine(resourceFolder, "Resources.pt-PT.resx");
                        break;
                    case (int)Languages.Spanish:
                        resourceFile = Path.Combine(resourceFolder, "Resources.es-ES.resx");
                        break;
                    case (int)Languages.French:
                        resourceFile = Path.Combine(resourceFolder, "Resources.fr-FR.resx");
                        break;
                    case (int)Languages.Italian:
                        resourceFile = Path.Combine(resourceFolder, "Resources.it-IT.resx");
                        break;
					case (int)Languages.Polish:
						resourceFile = Path.Combine(resourceFolder, "Resources.pl-PL.resx");
						break;
					case (int)Languages.Danish:
						resourceFile = Path.Combine(resourceFolder, "Resources.da-DK.resx");
						break;
					case (int)Languages.Dutch:
						resourceFile = Path.Combine(resourceFolder, "Resources.nl-NL.resx");
						break;
					case (int)Languages.Norwegian:
						resourceFile = Path.Combine(resourceFolder, "Resources.nn-NO.resx");
						break;
					default:
                        break;
                }

                languageItems = ReadResourceFile(resourceFile);
                UpdateLanguageColumnInXLS(sheet, languageItems, (Languages)i);
            }

           wb.SaveAs(xlsFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal,
           Missing.Value, Missing.Value, Missing.Value, Missing.Value, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive,
           Missing.Value, Missing.Value, Missing.Value,
           Missing.Value, Missing.Value);
            wb.Close(Missing.Value, Missing.Value, Missing.Value);
            sheet = null;
            wb = null;
        }

        private void UpdateLanguageColumnInXLS(Worksheet sheet, List<Resource> languageItems, Languages language)
        {
            int columnIndex = (int) language;
            int row = 2;

            while (sheet.Cells[row, 1] != null && sheet.Cells[row, 1].Value != null && languageItems.Count > 0)
            {
                Resource currentItem = languageItems.FirstOrDefault(t => t.ControllerID == sheet.Cells[row, 1].Value);
                if (currentItem != null)
                {
                    sheet.Cells[row, columnIndex] = currentItem.Value;
                    languageItems.Remove(currentItem);
                }
                row++;
            }
        }

        private Dictionary<string, string> ReadXLFile(Worksheet sheet, int columnIndex)
        {
            Dictionary<string, string> dicResourceValues = new Dictionary<string, string>();
            int row = 2;

            try
            {
                while (sheet.Cells[row, 1] != null && sheet.Cells[row, 1].Value != null)
                {
                    if (sheet.Cells[row, columnIndex] != null && sheet.Cells[row, columnIndex].Value != null)
                    {
                        dicResourceValues.Add(sheet.Cells[row, 1].Value.ToString(), sheet.Cells[row, columnIndex].Value.ToString());
                    }

                    row++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dicResourceValues;
        }

        private List<Resource> ReadResourceFile(string resourcePath)
        {
            List<Resource> lstResource = new List<Resource>();

            try
            {
                using (XmlReader reader = XmlReader.Create(resourcePath))
                {
                    bool isController = false;
                    string controllerID = string.Empty;
                    Resource objResource = new Resource();

                    while (reader.Read())
                    {
                        // Only detect start elements.
                        if (reader.IsStartElement())
                        {
                            switch (reader.Name)
                            {
                                case "data":
                                    objResource = new Resource();

                                    controllerID = reader["name"];
                                    objResource.ControllerID = controllerID;
                                    isController = true;
                                    break;
                                case "value":
                                    if (reader.Read() && isController)
                                    {
                                        objResource.Value = reader.Value;
                                    }
                                    break;
                                case "comment":
                                    if (reader.Read() && isController)
                                    {
                                        objResource.Comment = reader.Value;
                                        lstResource.Add(objResource);
                                    }
                                    isController = false;
                                    break;
                            }
                        }
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstResource;
        }

        private void UpdateDictionary(ref Dictionary<Languages, Dictionary<string, string>> dicSourceDic, Languages languageToBeUpdated, Dictionary<string, string> dicValuesToBeAdded, Dictionary<string, string> dicValuesToBeDeleted)
        {
            if (dicValuesToBeAdded.Count > 0)
            {
                foreach (KeyValuePair<string, string> item in dicValuesToBeAdded)
                {
                    dicSourceDic[languageToBeUpdated].Add(item.Key, ((languageToBeUpdated == Languages.English) ? item.Value : string.Empty));
                }
            }
            if (dicValuesToBeDeleted.Count > 0)
            {
                foreach (string key in dicValuesToBeDeleted.Select(o => o.Key).ToList())
                {
                    dicSourceDic[languageToBeUpdated].Remove(key);
                }
            }
        }

        private void CreateXLFile(string xlFilePath, Dictionary<string, string> dicNewXlValues, string fileDisplayName)
        {
            progressBar1.Value = 1;
            progressBar1.Maximum = dicNewXlValues.Count + 1;
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            try
            {
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                int i = 1;
                foreach (KeyValuePair<string, string> item in dicNewXlValues)
                {
                    int percent = (int)(((double)progressBar1.Value / (double)progressBar1.Maximum) * 100);
                    progressBar1.Value = progressBar1.Value + 1;
                    progressBar1.CreateGraphics().DrawString("Generating Excel File...(" + percent.ToString() + "%)", new System.Drawing.Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(progressBar1.Width / 2 - 10, progressBar1.Height / 2 - 7));

                    xlWorkSheet.Cells[i, 1] = item.Key;
                    xlWorkSheet.Cells[i, 2] = item.Value;
                    i++;
                }

                xlWorkBook.SaveAs(xlFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                progressBar1.Refresh();
                this.releaseObject(xlWorkSheet);
                this.releaseObject(xlWorkBook);
                this.releaseObject(xlApp);
                this.KillExcel();

                this.BindDataGrid("'" + fileDisplayName + "' File Generated sucessfully.", xlFilePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateXLFile(string xlFilePath, Dictionary<Languages, Dictionary<string, string>> dicNewXlValues, string fileDisplayName)
        {
            progressBar1.Value = 1;
            progressBar1.Maximum = dicNewXlValues.Count + 1;
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            try
            {
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);

                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                int row = 1;
                foreach (KeyValuePair<Languages, Dictionary<string, string>> item1 in dicNewXlValues)
                {
                    int percent = (int)(((double)progressBar1.Value / (double)progressBar1.Maximum) * 100);
                    progressBar1.Value = progressBar1.Value + 1;
                    progressBar1.CreateGraphics().DrawString("Generating Excel File ( " + percent.ToString() + "% )", new System.Drawing.Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(progressBar1.Width / 2 - 10, progressBar1.Height / 2 - 7));

                    xlWorkSheet.Cells[row, (int)item1.Key] = item1.Key.ToString();
                    row++;

                    foreach (KeyValuePair<string, string> item2 in item1.Value)
                    {
                        if (xlWorkSheet.Cells[row, 1] == null || xlWorkSheet.Cells[row, 1].Value == null || xlWorkSheet.Cells[row, 1].Value == string.Empty)
                            xlWorkSheet.Cells[row, 1] = item2.Key;
                        xlWorkSheet.Cells[row, (int)item1.Key] = item2.Value;
                        row++;
                    }
                    row = 1;
                }

                xlWorkBook.SaveAs(xlFilePath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                this.releaseObject(xlWorkSheet);
                this.releaseObject(xlWorkBook);
                this.releaseObject(xlApp);
                this.KillExcel();

                this.BindDataGrid("'" + fileDisplayName + "' File Generated sucessfully.", xlFilePath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void KillExcel()
        {
            Process[] AllProcesses = Process.GetProcessesByName("excel");

            // check to kill the right process
            foreach (Process ExcelProcess in AllProcesses)
            {
                //if (myHashtable.ContainsKey(ExcelProcess.Id) == false)
                ExcelProcess.Kill();
            }

            AllProcesses = null;
        }

        private void BindDataGrid(string fileName, string filePath)
        {
            if (this.dicResult == null)
            {
                this.dicResult = new Dictionary<string, string>();
            }

            this.dicResult.Add(fileName, filePath);
            this.dgResult.DataSource = dicResult.ToArray();

            dgResult.Columns[0].Width = 180;
            dgResult.Columns[1].Width = 614 - 180;
            dgResult.Columns[0].HeaderText = "";
            dgResult.Columns[1].HeaderText = "File Path";
        }

        /// <summary>
        /// Language and xl collumn Id
        /// </summary>
        public enum Languages
        {
            English = 2,
            German = 3,
            Korean = 4,
            Japanese = 5,
            Chinese_Simplified = 6,
            Chinese_Traditional = 7,
            Portuguse = 8,
            Spanish = 9,
            French = 10,
            Italian = 11,
			Polish = 12,
			Danish = 13,
			Dutch = 14,
			Norwegian = 15
        }

        #endregion
    }

    public class Resource
    {
        public string ControllerID { get; set; }
        public string Value { get; set; }
        public string Comment { get; set; }
    }
}
