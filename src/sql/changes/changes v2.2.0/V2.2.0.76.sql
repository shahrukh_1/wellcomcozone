
USE [GMGCoZone]
GO



-- Adding "Checklist" in secondary navigation

INSERT INTO ControllerAction
values('Settings','CheckLists','')


INSERT INTO MenuItem
VALUES(116,74,4,NULL,1,0,0,1,'CLCH', 'CheckList', 'CheckList')


INSERT INTO MenuItemAccountTypeRole
VALUES(144,75),
(144,76),
(144,77),
(144,78)


-- Adding AddEditChecklistItem in secondary navigation

INSERT INTO ControllerAction
values('Settings','AddEditChecklistItem','')


INSERT INTO MenuItem
VALUES(117,74,0,1,1,0,0,0,'CLCI', 'Add Edit Checklist Item', 'Add Edit Checklist Item')


INSERT INTO MenuItemAccountTypeRole
VALUES(145,75),
(145,76),
(145,77),
(145,78)

------------------------------------------------------------------------

/****** Object:  Table [dbo].[CheckList]    Script Date: 06-Jul-19 2:15:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CheckList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	
 CONSTRAINT [PK_CheckList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CheckList] ADD  DEFAULT (newid()) FOR [Guid]
GO

ALTER TABLE [dbo].[CheckList] ADD  CONSTRAINT [DF_CheckList_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[CheckList] ADD  CONSTRAINT [DF_CheckList_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO



ALTER TABLE [dbo].[CheckList]  WITH CHECK ADD  CONSTRAINT [FK_CheckList_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[CheckList] CHECK CONSTRAINT [FK_CheckList_Account]
GO

--------------------------------------------------------


/****** Object:  Table [dbo].[CheckListItem]    Script Date: 06-Jul-19 2:28:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CheckListItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CheckList] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Position] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_CheckListItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[CheckListItem] ADD  DEFAULT (newid()) FOR [Guid]
GO


ALTER TABLE [dbo].[CheckListItem] ADD  CONSTRAINT [DF_CheckListItem_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[CheckListItem] ADD  CONSTRAINT [DF_CheckListItem_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO


ALTER TABLE [dbo].[CheckListItem]  WITH CHECK ADD  CONSTRAINT [FK_CheckListItem_CheckList] FOREIGN KEY([CheckList])
REFERENCES [dbo].[CheckList] ([ID])
GO


--------------------------------------------------------


/****** Object:  Table [dbo].[ApprovalAnnotationCheckListItem]    Script Date: 07-Jul-19 10:56:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationCheckListItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[CheckListItem] [int] NOT NULL,
	[TextValue] [nvarchar](256) NOT NULL,
	[Guid] [nvarchar](36) NULL,
 CONSTRAINT [PK_ApprovalAnnotationCheckListItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationCheckListItem_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] CHECK CONSTRAINT [FK_ApprovalAnnotationCheckListItem_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationCheckListItem_CheckListItem] FOREIGN KEY([CheckListItem])
REFERENCES [dbo].[CheckListItem] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] CHECK CONSTRAINT [FK_ApprovalAnnotationCheckListItem_CheckListItem]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationCheckListItem', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationCheckListItem', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The CheckListItem id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationCheckListItem', @level2type=N'COLUMN',@level2name=N'CheckListItem'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The text value of the checklist' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationCheckListItem', @level2type=N'COLUMN',@level2name=N'TextValue'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationCheckListItem'
GO

--------------------------------------------------------

ALTER TABLE [dbo].[Approval]
  ADD Checklist int

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_Checklist] FOREIGN KEY([Checklist])
REFERENCES [dbo].[CheckList] ([ID])
GO






















