USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountParameters]    Script Date: 31/10/2019 13:05:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--Return QualityControl Billing plan info
ALTER PROCEDURE [dbo].[SPC_ReturnAccountParameters] ( @P_LoggedAccount INT )
AS 
    BEGIN
        SET NOCOUNT ON
        SELECT  A.ID AS [AccountId] ,
                A.NAME AS [AccountName] ,
                AT.ID AS AccountTypeID ,
                AT.Name AS AccountTypeName ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.ID
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.ID
                         END, 0) AS [CollaborateBillingPlanId] ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.Name
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.NAme
                         END, '') AS [CollborateBillingPlanName] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.ID
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.ID
                         END, 0) AS [DeliverBillingPlanId] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.Name
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.NAme
                         END, '') AS [DeliverBillingPlanName] ,
                AST.ID AS AccountStatusID ,
                ASt.Name AS AccountStatusName ,
                CTY.ID AS LocationID ,
                CTY.ShortName AS LocationName
        FROM    dbo.Account A
                INNER JOIN AccountType AT ON A.AccountType = AT.ID
                INNER JOIN AccountStatus AST ON A.[Status] = AST.ID
                INNER JOIN Company C ON C.Account = A.ID
                INNER JOIN Country CTY ON C.Country = CTY.ID
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     ASP.Account = A.ID
                            ) CASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     ASP.Account = A.ID
                            ) DASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     SPI.Account = A.ID
                            ) CSPI
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     SPI.Account = A.ID
                            ) DSPI
             WHERE (ast.[Key] != 'D' AND a.Parent = @P_LoggedAccount AND a.IsTemporary = 0) OR a.ID =@P_LoggedAccount 
    END

GO


