
USE [GMGCoZone]
GO


-- Taking backup of NotificationItem [ Except 8 days from System date ] 
select * INTO NotificationItem_Backup from NotificationItem
 where createdDate < DATEADD(DAY, -8, SYSDATETIME())


-- Adding  Primary Key to NotificationItem_Backup
ALTER TABLE NotificationItem_Backup
ADD CONSTRAINT pk_NotificationItem_Backup PRIMARY KEY (ID)