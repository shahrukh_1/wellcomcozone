USE [GMGCoZone]
GO

/****** Object:  Table [dbo].[UserGroupUser]    Script Date: 20-08-2020 10:19:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserGroupUserVisibility](
	[UserGroup] [int] NOT NULL,
	[User] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryVisiblility] [bit] NOT NULL,

 CONSTRAINT [PK_UserGroupUserVisibility] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserGroupUserVisibility] ADD  CONSTRAINT [DF__UserGroupVisibility__IsPri__43F60EC8]  DEFAULT ((0)) FOR [PrimaryVisiblility]
GO


ALTER TABLE [dbo].[UserGroupUserVisibility]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupUserVisibility_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[UserGroupUserVisibility] CHECK CONSTRAINT [FK_UserGroupUserVisibility_User]
GO

ALTER TABLE [dbo].[UserGroupUserVisibility]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupUserVisibility_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO

ALTER TABLE [dbo].[UserGroupUserVisibility] CHECK CONSTRAINT [FK_UserGroupUserVisibility_UserGroup]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUserVisibility', @level2type=N'COLUMN',@level2name=N'UserGroup'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUserVisibility', @level2type=N'COLUMN',@level2name=N'User'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUserVisibility'
GO


