USE [GMGCoZone]
GO

/****** Object:  Table [dbo].[OutOfOffice]    Script Date: 23-10-2019 16:10:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OutOfOffice](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Owner] [int] NOT NULL,
	[SubstituteUser] [int] NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[EndDate] [datetime2](7) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_OutOfOffice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[OutOfOffice]  WITH CHECK ADD  CONSTRAINT [FK_OutOfOffice_OnbehalfUser] FOREIGN KEY([SubstituteUser])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[OutOfOffice] CHECK CONSTRAINT [FK_OutOfOffice_OnbehalfUser]
GO

ALTER TABLE [dbo].[OutOfOffice]  WITH CHECK ADD  CONSTRAINT [FK_OutOfOffice_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[OutOfOffice] CHECK CONSTRAINT [FK_OutOfOffice_Owner]
GO


