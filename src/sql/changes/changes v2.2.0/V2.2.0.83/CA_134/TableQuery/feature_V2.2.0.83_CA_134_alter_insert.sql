-------------------->Inserted into ControllerAction<---------------------------------
USE [GMGCoZone]
GO

INSERT INTO [dbo].[ControllerAction]
           ([Controller],[Action],[Parameters])
     VALUES
           ('PersonalSettings','OutOfOffice',''),
		   ('PersonalSettings','AddNewOutOfOffice','')
GO


----------------------------->Inserted into MenuItem<------------------------------------
INSERT INTO [dbo].[MenuItem]
           ([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
     VALUES
           (121,45,5,null,1,0 ,0,1,'PSOO','OutOfOffice','PersonalSettings'),
		    (122,45,0,0,0,0,0,0,'PSNO','AddNewOutOfOffice','PersonalSettings')
GO



-------------------------------------->Inserted into [MenuItemAccountTypeRole]<--------------------------------------
INSERT INTO [dbo].[MenuItemAccountTypeRole]
           ([MenuItem],[AccountTypeRole])
     VALUES
           (151 ,1),(151 ,2),(151 ,3),(151 ,4),(151 ,5),(151 ,6),(151 ,7),(151 ,8),(151 ,9),(151 ,10),
		   (151 ,11),(151 ,12),(151 ,13),(151 ,14),(151 ,15),(151 ,16),(151 ,17),(151 ,18),(151 ,19),(151 ,20),
		   (151 ,21),(151 ,22),(151 ,23),(151 ,24),(151 ,25),(151 ,26),(151 ,27),(151 ,28),(151 ,29),(151 ,30),
		   (151 ,31),(151 ,32),(151 ,33),(151 ,34),(151 ,55),(151 ,56),(151 ,57),(151 ,58),(151 ,59),(151 ,60),
		   (151 ,61),(151 ,62),(151 ,63),(151 ,64),(151 ,65),(151 ,66),(151 ,67),(151 ,68),(151 ,69),(151 ,70),
		   (151 ,71),(151 ,72),(151 ,73),(151 ,74),(151 ,75),(151 ,76),(151 ,77),(151 ,78),(151 ,79),(151 ,80),
		   (151 ,81),(151 ,82),(151 ,111),(151 ,112),(151 ,113),(151 ,114),

		   (152 ,1),(152 ,2),(152 ,3),(152 ,4),(152 ,5),(152 ,6),(152 ,7),(152 ,8),(152 ,9),(152 ,10),
		   (152 ,11),(152 ,12),(152 ,13),(152 ,14),(152 ,15),(152 ,16),(152 ,17),(152 ,18),(152 ,19),(152 ,20),
		   (152 ,21),(152 ,22),(152 ,23),(152 ,24),(152 ,25),(152 ,26),(152 ,27),(152 ,28),(152 ,29),(152 ,30),
		   (152 ,31),(152 ,32),(152 ,33),(152 ,34),(152 ,55),(152 ,56),(152 ,57),(152 ,58),(152 ,59),(152 ,60),
		   (152 ,61),(152 ,62),(152 ,63),(152 ,64),(152 ,65),(152 ,66),(152 ,67),(152 ,68),(152 ,69),(152 ,70),
		   (152 ,71),(152 ,72),(152 ,73),(152 ,74),(152 ,75),(152 ,76),(152 ,77),(152 ,78),(152 ,79),(152 ,80),
		   (152 ,81),(152 ,82),(152 ,111),(152 ,112),(152 ,113),(152 ,114)

GO


------------------------------>Inserted into EventType<-------------------------------------------------
INSERT INTO [dbo].[EventType]
           ([EventGroup],[Key],[Name])
     VALUES
           (10,'OOF','OutOfOffice')


-------------------------->Altered [ApprovalCollaborator]<------------------------------------


ALTER TABLE [dbo].[ApprovalCollaborator]
  ADD SubstituteUser int

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaborator_SubstituteUser] FOREIGN KEY([SubstituteUser])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_SubstituteUser]
GO

