USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ApprovalCountsView]    Script Date: 21/10/2019 18:42:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[ApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount,
			0 AS MyOpenApprovalsCount,
			0 AS AdvanceSearchCount,
			0 AS SubstitutedApprovalCount



GO


