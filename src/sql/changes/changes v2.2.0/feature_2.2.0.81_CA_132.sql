


USE [GMGCoZone]
GO

/****** Object:  Table [dbo].[UserLoginFailedDetails]    Script Date: 20/09/2019 13:00:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserLoginFailedDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[IpAddress] [nvarchar](16) NOT NULL,
	[Attempts] [int] NOT NULL,
	[DateLastLoginFailed] [datetime2](7) NOT NULL,
	[IsUserLoginLocked] [bit] NOT NULL,
 CONSTRAINT [PK_UserLoginFailedDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserLoginFailedDetails] ADD  CONSTRAINT [DF_UserLoginFailedDetails_IsUserLoginLocked]  DEFAULT ((0)) FOR [IsUserLoginLocked]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who Login failed in this UserLoginFailedDetails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails', @level2type=N'COLUMN',@level2name=N'User'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The IP address that the User logged in from' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails', @level2type=N'COLUMN',@level2name=N'IpAddress'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Number of Attempts of Login failed ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails', @level2type=N'COLUMN',@level2name=N'Attempts'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user attempted to login' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails', @level2type=N'COLUMN',@level2name=N'DateLastLoginFailed'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User Login Locked If Attempts more than 10' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails', @level2type=N'COLUMN',@level2name=N'IsUserLoginLocked'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLoginFailedDetails'
GO


