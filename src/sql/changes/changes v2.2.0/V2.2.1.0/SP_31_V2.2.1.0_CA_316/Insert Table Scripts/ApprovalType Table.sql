



------------ Create [ApprovalTypeColour] Table
CREATE TABLE [dbo].[ApprovalTypeColour](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar] (4) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Colour] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ApprovalTypeColour] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
-------------------------------------------------


INSERT INTO [dbo].[ApprovalTypeColour]
           ([Key],[Name],[Colour])
     VALUES
           ('IMG', 'Image','#FFFFFF'),
		   ('VID', 'Video','#333333'),
		   ('DOC', 'Document','#ED1556'),
		   ('AUD', 'Audio','#666666'),
		   ('HTML', 'HTML','#78A352')
		   
GO

-------------------------------------

ALTER TABLE FileType ADD [ApprovalTypeColour] INT;

Update FileType set  [ApprovalTypeColour] = 1 where id in (1,2,4,5,6,7)

Update FileType set  [ApprovalTypeColour] = 2 where id in (8,9,10,11,12,13,14,15,16,17, 18 ,19,20)

Update FileType set  [ApprovalTypeColour] = 3 where id in (3)

Update FileType set  [ApprovalTypeColour] = 5 where id in (28)

update FileType set [ApprovalTypeColour] = 1 where [ApprovalTypeColour] is  null

GO


--ALTER TABLE FileType ALTER COLUMN [ApprovalTypeColour] INT NOT NULL;


ALTER TABLE [dbo].[FileType]  WITH CHECK ADD  CONSTRAINT [FK_FileType_ApprovalTypeColour] FOREIGN KEY([ApprovalTypeColour])
REFERENCES [dbo].[ApprovalTypeColour] ([ID])
GO


ALTER TABLE [dbo].[FileType] ADD  DEFAULT ((1)) FOR [ApprovalTypeColour]
GO


----------------------------------------

ALTER TABLE Approval ADD [ApprovalTypeColour] INT;
GO

UPDATE Approval set  [ApprovalTypeColour] = 1  
GO 

--ALTER TABLE Approval ALTER COLUMN [ApprovalTypeColour] INT NOT NULL;

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_ApprovalTypeColour] FOREIGN KEY([ApprovalTypeColour])
REFERENCES [dbo].[ApprovalTypeColour] ([ID])
GO




























