
USE [GMGCoZone]
GO

/****** Object:  Table [dbo].[OutOfOffice]    Script Date: 10-06-2020 10:48:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationReport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[User] [int] NOT NULL,
	[SerialNumber] [int] NOT NULL,
	[Base64String] [nvarchar](max) NOT NULL,
	
	 CONSTRAINT [PK_ApprovalAnnotationReport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] 

GO


ALTER TABLE [dbo].[ApprovalAnnotationReport]  WITH CHECK ADD  CONSTRAINT [FK_Approval_AnnotationReport] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO


ALTER TABLE [dbo].[ApprovalAnnotationReport]  WITH CHECK ADD  CONSTRAINT [FK_Approval_AnnotationReport_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO

























