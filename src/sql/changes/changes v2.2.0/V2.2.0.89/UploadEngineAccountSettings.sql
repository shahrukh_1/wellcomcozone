ALTER TABLE UploadEngineAccountSettings
ADD AllowApiStatusUpdate bit DEFAULT 0 NOT NULL, AllowApiApprovalDueDate bit DEFAULT 0 NOT NULL,
 AllowApiPhaseDueDate bit DEFAULT 0 NOT NULL, AllowApiOnAnnotation bit DEFAULT 0 NOT NULL

 ------------------------------------------------------------

 update UploadEngineAccountSettings
 set AllowApiStatusUpdate = 1
 where PostStatusUpdatesURL is not null