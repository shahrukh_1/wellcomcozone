
--Added New column in SharedApproval for block the url for By clicking the Exit button your ProofStudio session url will be locked
alter table [dbo].[SharedApproval]
add [IsBlockedURL] bit


update SharedApproval set IsBlockedURL=0;