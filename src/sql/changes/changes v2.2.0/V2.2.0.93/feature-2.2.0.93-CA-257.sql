
INSERT INTO [dbo].[Role]
           ([Key],[Name],[Priority],[AppModule])
     VALUES
           ('MOD','Moderator', 3 , 8)


INSERT INTO [dbo].[AccountTypeRole]
           ([AccountType],[Role])
     VALUES
           (2,34),(3,34),(4,34),(5,34)


INSERT INTO [dbo].[MenuItemAccountTypeRole]
           ([MenuItem],[AccountTypeRole])
     VALUES
           (148,134),(149,134),(150,134),
		   (148,135),(149,135),(150,135),
		   (148,136),(149,136),(150,136),
		   (148,137),(149,137),(150,137)
