USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnFileReportInfoView]    Script Date: 26/11/2019 16:53:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- CZ-2970 Extend User Report
ALTER VIEW [dbo].[ReturnFileReportInfoView] 
AS 
	SELECT 	'' AS AccountName,
			'' AS OwnerName,
			'' AS [FileName],
			0 AS V1PageCount,
			0 AS TotalVersion,
			CONVERT(DATETIME, GETDATE()) AS UploadDate,
			CONVERT(DATETIME, GETDATE()) AS CompletedDate,
			0 AS V1Users,
			0 AS LatestVersionUsers,
			'' AS Modifier,
			CONVERT(DATETIME, GETDATE()) AS ModifiedDate

GO


