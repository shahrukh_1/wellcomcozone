
----------------------- ALTERED APPROVALANNOTATIIONCHECKLISTITEM TABLE -------------------------------------------
ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] ADD [TotalChanges] [int] NULL

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] ADD [Creator] [int] NULL

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationCheckListItem_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] CHECK CONSTRAINT [FK_ApprovalAnnotationCheckListItem_Creator]
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] ADD [ExternalCreator] [int] NULL

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationCheckListItem_ExternalCreator] FOREIGN KEY([ExternalCreator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] CHECK CONSTRAINT [FK_ApprovalAnnotationCheckListItem_ExternalCreator]
GO

ALTER TABLE [dbo].[ApprovalAnnotationCheckListItem] ADD [CreatedDate] [datetime2](7) NULL



----------------------------------- INSERTING INTO EVENTTYPE TABLE ------------------------------------

INSERT INTO [dbo].[EventType]
           ([EventGroup]
           ,[Key]
           ,[Name])
     VALUES
           (1
           ,'CIE'
           ,'ChecklistItem was edited')
GO