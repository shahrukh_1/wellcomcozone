USE [GMGCoZone]
GO

/****** Object:  Table [dbo].[ApprovalAnnotationChecklistItemHistory]    Script Date: 17-12-2019 15:42:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationChecklistItemHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[ChecklistItem] [int] NOT NULL,
	[TextValue] [nvarchar](256) NOT NULL,
	[Guid] [nvarchar](36) NULL,
	[Creator] [int] NULL,
	[ExternalCreator] [int] NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_ApprovalAnnotationChecklistItemHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory] CHECK CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_ChecklistItem] FOREIGN KEY([ChecklistItem])
REFERENCES [dbo].[CheckListItem] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory] CHECK CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_ChecklistItem]
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory] CHECK CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_Creator]
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_ExternalCreator] FOREIGN KEY([ExternalCreator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO

ALTER TABLE [dbo].[ApprovalAnnotationChecklistItemHistory] CHECK CONSTRAINT [FK_ApprovalAnnotationChecklistItemHistory_ExternalCreator]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ChecklistItem id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'ChecklistItem'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The text value of the checklist' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'TextValue'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The internal user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'ExternalCreator'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this checklistitem was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationChecklistItemHistory'
GO


