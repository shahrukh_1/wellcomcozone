

USE [GMGCoZone]
GO

ALTER TABLE [dbo].[ApprovalPhase]
ADD DecisionsShouldBeMadeByAll [bit]


ALTER TABLE [dbo].[ApprovalPhase] ADD  DEFAULT ((0)) FOR [DecisionsShouldBeMadeByAll]
GO