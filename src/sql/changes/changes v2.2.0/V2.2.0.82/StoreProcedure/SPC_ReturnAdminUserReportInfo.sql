
CREATE PROCEDURE [dbo].[SPC_ReturnAdminUserReportInfo] (
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
		WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
	   -- Get the users				
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin,
		    (SELECT PathString) AS AccountPath,
		    (SELECT COUNT(app.ID) FROM [dbo].Approval AS app WHERE app.[Creator] = u.[ID]) AS CollaborateApprovalsNumber, 
		    (SELECT COUNT(dj.ID) FROM [dbo].DeliverJob AS dj WHERE dj.[Owner] = u.[ID]) AS DeliverJobsNumber		
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID		
		    INNER JOIN AccountTree act 
				ON a.ID = act.AccountID																						
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ((u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END

