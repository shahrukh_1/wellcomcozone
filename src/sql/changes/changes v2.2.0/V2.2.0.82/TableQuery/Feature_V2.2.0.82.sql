-- CA-16
-- Welcome Users
USE [GMGCoZone]
GO

INSERT INTO [dbo].[ControllerAction]
           ([Controller],[Action],[Parameters])
     VALUES
           ('Reports','NewUserReport','') ,
		   ('Reports','FileReport','')
GO

INSERT INTO [dbo].[MenuItem]
           ([ControllerAction] ,[Parent] ,[Position]  ,[IsAdminAppOwned],[IsAlignedLeft]
           ,[IsSubNav] ,[IsTopNav] ,[IsVisible] ,[Key] ,[Name] ,[Title])
     VALUES
           (118,23,5,1,1,0,0,1,'RPNU','NewUserReport','Report'),
		   (119,23,6,1,1,0,0,1,'RPFR','File Report','Report')
GO

INSERT INTO [dbo].[MenuItemAccountTypeRole]
           ([MenuItem],[AccountTypeRole])
     VALUES
           (146,1),(147,1),(146,23),(147,23),
		   (146,24),(147,24),(146,25),(147,25),
		   (146,26),(147,26),(146,75),(147,75),
		   (146,76),(147,76),(146,77),(147,77)
GO

--Non Welcome users 

INSERT INTO [dbo].[AppModule]
           ([Key],[Name])
     VALUES(7,'Reports')
GO

INSERT INTO [dbo].[Role]
           ([Key],[Name],[Priority],[AppModule])
     VALUES
           ('ADM','Administrator', 2 , 8),
		   ('NON','NoAccess', 1 , 8)
GO

INSERT INTO [dbo].[AccountTypeRole]
           ([AccountType],[Role])
     VALUES
           (2,32),(3,32),(4,32),(5,32)
GO

INSERT INTO [dbo].[ControllerAction]
           ([Controller],[Action],[Parameters])
     VALUES
           ('Reports','ReportAppIndex','')
GO

INSERT INTO [dbo].[MenuItem]
           ([ControllerAction] ,[Parent] ,[Position]  ,[IsAdminAppOwned],[IsAlignedLeft]
           ,[IsSubNav] ,[IsTopNav] ,[IsVisible] ,[Key] ,[Name] ,[Title])
     VALUES
           (120,NULL,5,0,1,0,1,1,'URIN','Report','ReportAppIndex')
GO

INSERT INTO [dbo].[MenuItem]
           ([ControllerAction] ,[Parent] ,[Position]  ,[IsAdminAppOwned],[IsAlignedLeft]
           ,[IsSubNav] ,[IsTopNav] ,[IsVisible] ,[Key] ,[Name] ,[Title])
     VALUES
           (118,148,5,1,1,0,0,1,'RPNU','NewUserReport','Report'),
		   (119,148,6,1,1,0,0,1,'RPFR','File Report','Report')
GO

INSERT INTO [dbo].[MenuItemAccountTypeRole]
           ([MenuItem],[AccountTypeRole])
     VALUES
           (148,130),(149,130),(150,130),
		   (148,131),(149,131),(150,131),
		   (148,132),(149,132),(150,132),
		   (148,133),(149,133),(150,133)
GO






