USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnAdminUserReportInfoView]    Script Date: 08/10/2019 14:42:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- CZ-2970 Extend User Report
CREATE VIEW [dbo].[ReturnAdminUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Name,
			'' AS StatusName,
			'' AS UserName,
			'' AS EmailAddress,
			GETDATE() AS DateLastLogin,
			'' AS AccountPath, 
			0 AS CollaborateApprovalsNumber,
			0 AS DeliverJobsNumber




GO


