USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 09-Oct-19 4:50:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- CZ-2970 Extend User Report
CREATE VIEW [dbo].[ReturnFileReportInfoView] 
AS 
	SELECT 	'' AS AccountName,
			'' AS [FileName],
			0 AS V1PageCount,
			0 AS TotalVersion,
			CONVERT(DATETIME, GETDATE()) AS UploadDate,
			CONVERT(DATETIME, GETDATE()) AS CompletedDate,
			0 AS V1Users,
			0 AS LatestVersionUsers,
			'' AS Modifier,
			CONVERT(DATETIME, GETDATE()) AS ModifiedDate
GO


