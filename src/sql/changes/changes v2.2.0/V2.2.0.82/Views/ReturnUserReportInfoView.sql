USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 08/10/2019 15:03:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- CZ-2970 Extend User Report
ALTER VIEW [dbo].[ReturnUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Account,
			'' AS Name,
			'' AS UserName,
			'' AS EmailAddress,
			0 AS NumberOfApprovalsIn,
			0 AS NumberOfUploads,
			CONVERT(DATETIME, GETDATE()) AS DateLastLogin,
			'' AS GroupMembership


GO


