USE GMGCoZone
GO
DECLARE @presetId INT
DECLARE @presetKey VARCHAR(1)
DECLARE @roleId INT
DECLARE @eventTypeId INT

DECLARE PresetsCursor CURSOR LOCAL FOR SELECT P.ID, P.[Key] FROM dbo.Preset P
OPEN PresetsCursor
FETCH NEXT FROM PresetsCursor INTO @presetId, @presetKey
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE RolesCursor CURSOR LOCAL FOR SELECT R.ID FROM dbo.Role R INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID WHERE   AM.[Key] = 0 AND R.[Key] = 'MOD'
        OPEN RolesCursor
        FETCH NEXT FROM RolesCursor INTO @roleId
        WHILE @@FETCH_STATUS = 0 
            BEGIN

                DECLARE EventTypesCursor CURSOR LOCAL FOR SELECT  ET.ID FROM dbo.EventType ET WHERE  (@presetKey = 'H' AND  ET.[Key] IN ( 'AAD', 'ADW', 'AAU' )) OR (@presetKey = 'M' AND  ET.[Key] IN ( 'ADW', 'AAU' ))
                OPEN EventTypesCursor
                FETCH NEXT FROM EventTypesCursor INTO @eventTypeId
                WHILE @@FETCH_STATUS = 0 
                    BEGIN
                    
                        INSERT  INTO dbo.RolePresetEventType
                                ( Role, Preset, EventType )
                        VALUES  ( @roleId, -- Role - int
                                  @presetId, -- Preset - int
                                  @eventTypeId  -- EventType - int
                                  )
                        FETCH NEXT FROM EventTypesCursor INTO @eventTypeId
          
                    END
                CLOSE EventTypesCursor
                DEALLOCATE EventTypesCursor
            
                FETCH NEXT FROM RolesCursor INTO @roleId
            END
        CLOSE RolesCursor
        DEALLOCATE RolesCursor

        FETCH NEXT FROM PresetsCursor INTO @presetId, @presetKey
    END
CLOSE PresetsCursor
DEALLOCATE PresetsCursor
GO
DECLARE @presetId INT
DECLARE @presetKey VARCHAR(1)
DECLARE @roleId INT
DECLARE @eventTypeId INT

DECLARE PresetsCursor CURSOR LOCAL FOR SELECT P.ID, P.[Key] FROM dbo.Preset P
OPEN PresetsCursor
FETCH NEXT FROM PresetsCursor INTO @presetId, @presetKey
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE RolesCursor CURSOR LOCAL FOR SELECT R.ID FROM dbo.Role R INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID WHERE   AM.[Key] = 0 AND R.[Key] = 'CON'
        OPEN RolesCursor
        FETCH NEXT FROM RolesCursor INTO @roleId
        WHILE @@FETCH_STATUS = 0 
            BEGIN

                DECLARE EventTypesCursor CURSOR LOCAL FOR SELECT  ET.ID FROM dbo.EventType ET WHERE  (@presetKey = 'H' AND  ET.[Key] IN ( 'AAA', 'AAS', 'AVC', 'AAD', 'ADW', 'AAU' )) OR (@presetKey = 'M' AND  ET.[Key] IN ( 'AAA', 'AAS', 'AVC', 'ADW', 'AAU' ))
                OPEN EventTypesCursor
                FETCH NEXT FROM EventTypesCursor INTO @eventTypeId
                WHILE @@FETCH_STATUS = 0 
                    BEGIN
                    
                        INSERT  INTO dbo.RolePresetEventType
                                ( Role, Preset, EventType )
                        VALUES  ( @roleId, -- Role - int
                                  @presetId, -- Preset - int
                                  @eventTypeId  -- EventType - int
                                  )
                        FETCH NEXT FROM EventTypesCursor INTO @eventTypeId
          
                    END
                CLOSE EventTypesCursor
                DEALLOCATE EventTypesCursor
            
                FETCH NEXT FROM RolesCursor INTO @roleId
            END
        CLOSE RolesCursor
        DEALLOCATE RolesCursor

        FETCH NEXT FROM PresetsCursor INTO @presetId, @presetKey
    END
CLOSE PresetsCursor
DEALLOCATE PresetsCursor
GO

--remove tiff iamge format

delete from dbo.ImageFormat 
where Name = 'TIFF'
GO

-- calculate the new roleId from the Admin module
DECLARE @roleIdAdministrator INT ;
SET @roleIdAdministrator = ( SELECT TOP 1
                                    R.ID
                             FROM   dbo.Role r
                                    INNER JOIN dbo.AppModule am ON r.AppModule = am.ID
                             WHERE  am.[Key] = 3 -- Admin module
                                    AND r.[Key] = 'ADM'
                           )

-- update role id from RolePresetEventType with the event types ('UUA', 'GGC', 'MPC', 'MAD', 'MAA') with the role id from admin module
DECLARE @rpetId INT
DECLARE MoveToAdmin CURSOR LOCAL FOR 
SELECT  DISTINCT
rpet.ID
FROM    dbo.EventType et
INNER JOIN dbo.RolePresetEventType rpet ON et.ID = rpet.EventType
INNER JOIN dbo.Role r ON rpet.Role = r.ID
INNER JOIN dbo.AppModule am ON r.AppModule = am.ID
WHERE   am.[Key] = 0
AND et.[Key] IN ( 'UUA', 'GGC', 'MPC', 'MAD', 'MAA' )
OPEN MoveToAdmin
FETCH NEXT FROM MoveToAdmin INTO @rpetId
WHILE @@FETCH_STATUS = 0 
    BEGIN
    
        UPDATE  dbo.RolePresetEventType
        SET     Role = @roleIdAdministrator
        WHERE   id = @rpetId
        		
        FETCH NEXT FROM MoveToAdmin INTO @rpetId
    END
CLOSE MoveToAdmin
DEALLOCATE MoveToAdmin
GO

-- CZD-99
DECLARE @atrId AS INT

DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 2 AND R.[Key] = 'ADM'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SESM' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

--CZD-473
DECLARE @eventTypeId INT
DECLARE AddToAdminMedium CURSOR LOCAL FOR SELECT  et.ID
FROM    dbo.EventType et
WHERE   et.[key] IN ( 'MAA', 'GGC', 'MPC' )
OPEN AddToAdminMedium
FETCH NEXT FROM AddToAdminMedium INTO @eventTypeId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @roleId INT
        SELECT  @roleId = r.ID
        FROM    dbo.Role r
                INNER JOIN dbo.AppModule am ON r.AppModule = am.ID
        WHERE   am.[Key] = 3
                AND r.[Key] = 'ADM'
        
        DECLARE @presetId INT
        SELECT  @presetId = p.ID
        FROM    dbo.Preset p
        WHERE   [Key] = 'M'
        
        INSERT  INTO dbo.RolePresetEventType
                ( Role, Preset, EventType )
        VALUES  ( @roleId, -- Role - int
                  @presetId, -- Preset - int
                  @eventTypeId  -- EventType - int
                  )
    
        FETCH NEXT FROM AddToAdminMedium INTO @eventTypeId
    END
CLOSE AddToAdminMedium
DEALLOCATE AddToAdminMedium
GO
DECLARE @eventTypeId INT
DECLARE AddToAdminMedium CURSOR LOCAL FOR SELECT  et.ID
FROM    dbo.EventType et
WHERE   et.[key] IN ( 'UUA', 'GGC')
OPEN AddToAdminMedium
FETCH NEXT FROM AddToAdminMedium INTO @eventTypeId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @roleId INT
        SELECT  @roleId = r.ID
        FROM    dbo.Role r
                INNER JOIN dbo.AppModule am ON r.AppModule = am.ID
        WHERE   am.[Key] = 3
                AND r.[Key] = 'ADM'
        
        DECLARE @presetId INT
        SELECT  @presetId = p.ID
        FROM    dbo.Preset p
        WHERE   [Key] = 'L'
        
        INSERT  INTO dbo.RolePresetEventType
                ( Role, Preset, EventType )
        VALUES  ( @roleId, -- Role - int
                  @presetId, -- Preset - int
                  @eventTypeId  -- EventType - int
                  )
    
        FETCH NEXT FROM AddToAdminMedium INTO @eventTypeId
    END
CLOSE AddToAdminMedium
DEALLOCATE AddToAdminMedium
GO
--CZD-429 - removes duplicates values (by User and RolePresetEventType) from dbo.UserCustomPresetEventTypeValue table
WITH    cte
          AS ( SELECT   * ,
                        ROW_NUMBER() OVER ( PARTITION BY [User],
                                            [RolePresetEventType] ORDER BY [User], [RolePresetEventType] ) 'RowRank'
               FROM     dbo.UserCustomPresetEventTypeValue
             )
    DELETE  cte
    WHERE   RowRank > 1
GO