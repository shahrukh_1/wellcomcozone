USE GMGCoZone
GO

--DAtabase performance
CREATE NONCLUSTERED INDEX [IX_Approval] ON [dbo].[Approval]
(
	[IsDeleted] ASC,
	[DeletePermanently] ASC,
	[Job] ASC
)
INCLUDE ( 	[Version]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_Approval_Owner] ON [dbo].[Approval]
(
	[Job] ASC,
	[IsDeleted] ASC,
	[DeletePermanently] ASC,
	[Owner] ASC
)
INCLUDE ( 	[Version]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_Approval_Phase] ON [dbo].[Approval]
(
	[ID] ASC,
	[IsDeleted] ASC,
	[Owner] ASC,
	[Job] ASC,
	[Type] ASC,
	[CurrentPhase] ASC,
	[PrimaryDecisionMaker] ASC,
	[ExternalPrimaryDecisionMaker] ASC,
	[DeletePermanently] ASC
)
INCLUDE ( 	[FileName],
	[Guid],
	[Version],
	[CreatedDate],
	[Creator],
	[Deadline],
	[ModifiedDate],
	[LockProofWhenAllDecisionsMade],
	[IsLocked],
	[AllowDownloadOriginal],
	[IsError],
	[AllowOtherUsersToBeAdded],
	[VersionSufix]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_ApprovalCollaboratorDecision] ON [dbo].[ApprovalCollaboratorDecision]
(
	[Approval] ASC,
	[Phase] ASC,
	[Decision] ASC,
	[ID] ASC,
	[Collaborator] ASC,
	[ExternalCollaborator] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

SET ANSI_PADDING ON

CREATE NONCLUSTERED INDEX [IX_ApprovalJobPhase] ON [dbo].[ApprovalJobPhase]
(
	[IsActive] ASC,
	[ApprovalJobWorkflow] ASC,
	[Position] ASC
)
INCLUDE ( 	[Name]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_FolderApproval_Approval]
ON [dbo].[FolderApproval] ([Approval])
GO

CREATE NONCLUSTERED INDEX [IX_ApprovalJobPhase_ApprovalJobWorkflow_Position_Name]
ON [dbo].[ApprovalJobPhase] ([ApprovalJobWorkflow],[Position])
INCLUDE ([Name])
GO

CREATE NONCLUSTERED INDEX [IX_Job_Account]
ON [dbo].[Job] ([Account])
INCLUDE ([ID],[Title],[Status],[JobOwner])
GO

CREATE NONCLUSTERED INDEX [IX_ApprovalCollaborator_Approval_Phase]
ON [dbo].[ApprovalCollaborator] ([Approval],[Phase])
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[up_updstats]
WITH EXECUTE AS 'dbo'
AS
EXEC sp_updatestats
GO

IF EXISTS(SELECT * FROM sys.indexes WHERE object_id = object_id('[dbo].[ApprovalCollaboratorDecision]') AND NAME ='IX_ApprovalCollaboratorDecision_Approval')
    DROP INDEX IX_ApprovalCollaboratorDecision_Approval ON [dbo].[ApprovalCollaboratorDecision];
GO
----------------------------------------------------------------------Launched on Test
----------------------------------------------------------------------Launched on Stage
----------------------------------------------------------------------Launched on Production



INSERT INTO [dbo].[ApprovalCommentType] VALUES ('MRC', 'Marquee rectangle', 8)
GO


ALTER TABLE 
	[GMGCoZone].[dbo].[AccountNotificationPreset]
ADD
	CreatedDate DATETIME NULL Default(NULL),
	CreatedBy int NULL Default(0),
	ModifiedDate DATETIME NULL Default(NULL),
	ModifiedBy int NULL Default(0)
	
ALTER TABLE 
	[GMGCoZone].[dbo].[NotificationScheduleUser]
ADD
	CreatedDate DATETIME NULL Default(NULL),
	CreatedBy int NULL Default(0)
	
ALTER TABLE 
    [GMGCoZone].[dbo].[UserCustomPresetEventTypeValue]
ADD
    CreatedDate DATETIME NULL Default(NULL),
    CreatedBy int NULL Default(0),
    ModifiedDate DATETIME NULL Default(NULL),
    ModifiedBy int NULL Default(0)
		  
ALTER TABLE 
    [GMGCoZone].[dbo].[NotificationSchedule]
ADD
    CreatedDate DATETIME NULL Default(NULL),
    CreatedBy int NULL Default(0),
    ModifiedDate DATETIME NULL Default(NULL),
    ModifiedBy int NULL Default(0)
	
ALTER TABLE 
    [GMGCoZone].[dbo].[NotificationScheduleUserGroup]
ADD
    CreatedDate DATETIME NULL Default(NULL),
    CreatedBy int NULL Default(0),
    ModifiedDate DATETIME NULL Default(NULL),
    ModifiedBy int NULL Default(0)

ALTER TABLE 
    [GMGCoZone].[dbo].[AccountNotificationPresetEventType]
ADD
    CreatedDate DATETIME NULL Default(NULL),
    CreatedBy int NULL Default(0),
    ModifiedDate DATETIME NULL Default(NULL),
    ModifiedBy int NULL Default(0)
	

--Add sub menu item to Settings - Custom SMTP Server
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'CustomSmtpServer', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
SELECT @controllerId , -- ControllerAction - int
      mi.ID , -- Parent - int
      8 , -- Position - int
      NULL , -- IsAdminAppOwned - bit
      1 , -- IsAlignedLeft - bit
      0 , -- IsSubNav - bit
      0 , -- IsTopNav - bit
      1 , -- IsVisible - bit
      'CSMT' , -- Key - nvarchar(4)
      'Custom Smtp Server' , -- Name - nvarchar(64)
      'Custom Smtp Server'  -- Title - nvarchar(128)
    FROM dbo.MenuItem mi
	where [Key] = 'SETT'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('CSMT')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

--Update AccountSettings table to add Creator, CreatedDate, Modifier and ModifiedDate columns
ALTER TABLE [dbo].[AccountSetting]
ADD [Creator] [INT] NULL,
       [CreatedDate] [DATETIME] NULL,
       [Modifier] [INT] NULL,
	   [ModifiedData] [DATETIME] NULL
GO
  
--Update Email table to add Account ID column
ALTER TABLE [dbo].[Email]
	ADD [Account] [INT] NULL
GO	

ALTER TABLE [dbo].[Account] ADD DeletedBy INT
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_DeletedBy]
GO

ALTER TABLE [dbo].[Account] ADD DeletedDate datetime2(7)
GO

----------------------------------------------------------------------------------
-- Update the new created column "DeletedDate" with value from "ModifiedDate" for deleted accounts, because is needed when permanently delete is executed
UPDATE acc
SET
    acc.[DeletedDate] = ac.ModifiedDate
FROM
    [dbo].[Account]  acc
    INNER JOIN [dbo].[Account] ac ON acc.ID = ac.ID
WHERE
    acc.[DeletedDate] IS NULL AND acc.Status = 5
GO  

ALTER TABLE [SharedApproval] ALTER COLUMN [Notes] nvarchar(1034)
GO 

----------------------------------------------------------------------Launched on Production
----------------------------------------------------------------------Launched on Test
----------------------------------------------------------------------Launched on Stage
