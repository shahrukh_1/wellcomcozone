USE GMGCoZone
GO
      
-- Add account type role foreach role and each account type(if the AccountTypeRole exists it will not be added again)
DECLARE @roleId INT
DECLARE Roles CURSOR LOCAL FOR       
SELECT  R.ID FROM    dbo.Role R
OPEN Roles
FETCH NEXT FROM Roles INTO @roleId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @accountTypeId INT
        DECLARE AccountTypes CURSOR LOCAL FOR SELECT AT.ID FROM dbo.AccountType AT
        OPEN AccountTypes
        FETCH NEXT FROM AccountTypes INTO @accountTypeId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.AccountTypeRole
                       WHERE    AccountType = @accountTypeId
                                AND Role = @roleId
                     ) = 0
                     AND @accountTypeId != 1
                   )  -- ignore the ADMIN account type
                    BEGIN
                        INSERT  INTO dbo.AccountTypeRole
                                ( AccountType, Role )
                        VALUES  ( @accountTypeId, -- AccountType - int
                                  @roleId  -- Role - int
                                  )
                    END
                FETCH NEXT FROM AccountTypes INTO @accountTypeId
            END
        CLOSE AccountTypes
        DEALLOCATE AccountTypes
        FETCH NEXT FROM Roles INTO @roleId
    END
CLOSE Roles
DEALLOCATE Roles
GO  

-- Update roles for all account owners
DECLARE @userId INT
DECLARE UsersCursor CURSOR LOCAL FOR SELECT A.Owner FROM dbo.Account A INNER JOIN dbo.AccountType AT ON AT.ID = A.AccountType WHERE   AT.[Key] != 'GMHQ'
OPEN UsersCursor
FETCH NEXT FROM UsersCursor INTO @userId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @moduleID INT
        DECLARE ModulesCursor CURSOR LOCAL FOR SELECT ID FROM dbo.AppModule WHERE [Key] != 4 -- ignore the sysAdmin module
        OPEN ModulesCursor
        FETCH NEXT FROM ModulesCursor INTO @moduleId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.[User] U
                                INNER JOIN dbo.UserRole UR ON UR.[User] = U.ID
                                INNER JOIN Role R ON R.ID = UR.Role
                                INNER JOIN dbo.AppModule AM ON AM.ID = R.AppModule
                       WHERE    U.ID = @userId
                                AND AM.ID = @moduleID
                     ) = 0 ) 
                    BEGIN
                        INSERT  INTO dbo.UserRole
                                ( [User] ,
                                  Role 
                                )
                                SELECT  @userId ,
                                        R.ID
                                FROM    dbo.AppModule AM
                                        INNER JOIN dbo.Role R ON R.AppModule = AM.ID
                                WHERE   R.[Key] = 'ADM' AND AM.ID = @moduleID
                    END
                        
                FETCH NEXT FROM ModulesCursor INTO @moduleId
            END
        CLOSE ModulesCursor
        DEALLOCATE ModulesCursor
        FETCH NEXT FROM UsersCursor INTO @userId
    END
CLOSE UsersCursor
DEALLOCATE UsersCursor
GO


-- Update roles for all other users per module
DECLARE @userId INT
DECLARE UsersCursor CURSOR LOCAL FOR SELECT ID FROM dbo.[User]
OPEN UsersCursor
FETCH NEXT FROM UsersCursor INTO @userId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @moduleID INT
        DECLARE ModulesCursor CURSOR LOCAL FOR SELECT ID FROM dbo.AppModule WHERE [Key] != 4 -- ignore the sysAdmin module
        OPEN ModulesCursor
        FETCH NEXT FROM ModulesCursor INTO @moduleId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.[User] U
                                INNER JOIN dbo.UserRole UR ON UR.[User] = U.ID
                                INNER JOIN Role R ON R.ID = UR.Role
                                INNER JOIN dbo.AppModule AM ON AM.ID = R.AppModule
                       WHERE    U.ID = @userId
                                AND AM.ID = @moduleID
                     ) = 0 ) 
                    BEGIN
                        INSERT  INTO dbo.UserRole
                                ( [User] ,
                                  Role 
                                )
                                SELECT  @userId ,
                                        R.ID
                                FROM    dbo.AppModule AM
                                        INNER JOIN dbo.Role R ON R.AppModule = AM.ID
                                WHERE   (R.[Key] = 'ADM' AND AM.ID = @moduleID and @moduleId = 1) OR       -- Admin for Colaborate
                                        (R.[Key] = 'NON' AND AM.ID = @moduleId AND @moduleId in (2,3,4,6) ) -- Viewer for Deliver and Administration
                    END
                        
                FETCH NEXT FROM ModulesCursor INTO @moduleId
            END
        CLOSE ModulesCursor
        DEALLOCATE ModulesCursor
        FETCH NEXT FROM UsersCursor INTO @userId
    END
CLOSE UsersCursor
DEALLOCATE UsersCursor
GO

-- Add user group ProfileServerOwnershipGroup for each existing account
-- Add the owner user of each account to the ProfileServerOwnershipGroup of the account.
DECLARE @userGroupName VARCHAR(64)
DECLARE @userGroupId INT
SET @userGroupName = 'ProfileServerOwnershipGroup'
DECLARE @accountOwnerUserId INT

DECLARE @accountId INT
DECLARE AccountCursor CURSOR LOCAL FOR SELECT A.ID FROM dbo.Account A INNER JOIN dbo.AccountType AT ON AT.ID = A.AccountType WHERE AT.[Key] != 'GMHQ'
OPEN AccountCursor
FETCH NEXT FROM AccountCursor INTO @accountId
WHILE @@FETCH_STATUS = 0 
    BEGIN    
		SET @accountOwnerUserId = (SELECT Owner FROM dbo.Account WHERE ID = @accountId)        
		IF ( ( SELECT   COUNT(1)
			   FROM     dbo.UserGroup UG
			   WHERE    UG.Account = @accountId
						AND UG.Name = @userGroupName
			  ) = 0 ) 
			BEGIN
				INSERT INTO dbo.UserGroup
						   ([Name]
						   ,[Account]
						   ,[Creator]
						   ,[CreatedDate]
						   ,[Modifier]
						   ,[ModifiedDate]
						   ,[Guid])
					 VALUES
						   (@userGroupName			--<Name, nvarchar(64),>
						   ,@accountId				--<Account, int,>
						   ,@accountOwnerUserId		--<Creator, int,>
						   ,GETDATE()				--<CreatedDate, datetime2(7),>
						   ,@accountOwnerUserId		--<Modifier, int,>
						   ,GETDATE()				--<ModifiedDate, datetime2(7),>
						   ,NEWID())					--<Guid, nvarchar(36),>
				SET @userGroupId = SCOPE_IDENTITY()
				
				INSERT INTO [GMGCoZone].[dbo].[UserGroupUser]
						   ([UserGroup]
						   ,[User])
					 VALUES
						   (@userGroupId			--<UserGroup, int,>
						   ,@accountOwnerUserId)	--<User, int,>)

			END                             
        FETCH NEXT FROM AccountCursor INTO @accountId
    END
CLOSE AccountCursor
DEALLOCATE AccountCursor
GO

--Alter UserRolesView
ALTER VIEW [dbo].[UserRolesView] AS
SELECT  U.ID ,
        U.GivenName ,
        u.FamilyName ,
        u.Username ,
        U.EmailAddress ,
        COALESCE(CR.[Key], 'NON') AS [CollaborateRole] ,
        COALESCE(DR.[Key], 'NON') AS [DeliverRole] ,
        COALESCE(AR.[Key], 'NON') AS [AdminRole] ,
        COALESCE(SR.[Key], 'NON') AS [SysAdminRole] ,
        u.DateLastLogin ,
        u.Account ,
        US.[Key] AS [UserStatus]
FROM    dbo.[User] AS U
        INNER JOIN dbo.UserStatus US ON U.Status = US.ID
        LEFT JOIN ( SELECT  CUR.[User] ,
                            CR.[Key]
                    FROM    dbo.UserRole CUR
                            INNER JOIN dbo.Role CR ON CUR.Role = CR.ID
                            INNER JOIN dbo.AppModule CAM ON CR.AppModule = CAM.ID
                                                            AND CAM.[Key] = 0
                  ) AS CR ON U.ID = CR.[User]
        LEFT JOIN ( SELECT  DUR.[User] ,
                            DR.[Key]
                    FROM    dbo.UserRole DUR
                            INNER JOIN dbo.Role DR ON DUR.Role = DR.ID
                            INNER JOIN dbo.AppModule DAM ON DR.AppModule = DAM.ID
                                                            AND DAM.[Key] = 2
                  ) AS DR ON U.ID = DR.[User]
        LEFT JOIN ( SELECT  AUR.[User] ,
                            AR.[Key]
                    FROM    dbo.UserRole AUR
                            INNER JOIN dbo.Role AR ON AUR.Role = AR.ID
                            INNER JOIN dbo.AppModule AAM ON AR.AppModule = AAM.ID
                                                            AND AAM.[Key] = 3
                  ) AS AR ON U.ID = AR.[User]
        LEFT JOIN ( SELECT  SUR.[User] ,
                            SR.[Key]
                    FROM    dbo.UserRole SUR
                            INNER JOIN dbo.Role SR ON SUR.Role = SR.ID
                            INNER JOIN dbo.AppModule SAM ON SR.AppModule = SAM.ID
                                                            AND SAM.[Key] = 4
                  ) AS SR ON U.ID = SR.[User]
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 100 100"><defs/><path id="" d="M99.1699,49.6357 L81.5918,32.0728C81.5918,32.0728 71.7354,41.9272 62.0293,51.6328L62.0293,0.000488281L37.1602,0L37.1602,51.6328L17.5781,32.0605L0,49.6357L49.5825,99.1738L99.1699,49.6357Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 1
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 100 100"><defs/>  <path id="" d="M0,0 L0,101.302L101.302,101.302L101.302,0L0,0Z M56.9824,75.9771 L37.9878,75.9771L56.9824,56.9824L18.9941,56.9824L18.9941,44.3198L56.9824,44.3198L37.9878,25.3252L56.9824,25.3252L82.3081,50.6514L56.9824,75.9771Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 2
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 50 100"><defs/>  <path id="" d="M30.1045,96.335 L30.1045,30.1045L48.167,30.1045L24.083,0L0,30.1045L18.0625,30.1045L18.0625,96.335L30.1045,96.335Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 3
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 100 90"><defs/>  <path id="" d="M89.3652,36.0716 L34.7549,36.0663L55.5547,15.2611C58.9102,11.9051 58.7686,6.27475 55.2354,2.74155C51.7012,-0.786285 46.0781,-0.928864 42.7197,2.42758L12.834,32.3148L12.834,32.3148L0,45.1483L9.99805,55.1454L12.834,57.9823L12.834,57.9823L41.9883,87.1419C45.2969,90.4383 50.875,90.2498 54.4072,86.722C57.9414,83.1893 58.1299,77.6053 54.8223,74.3089L34.7549,54.2298L89.3652,54.2245C93.4551,54.2298 96.8086,50.1414 96.8086,45.1424C96.8086,40.1497 93.4551,36.0663 89.3652,36.0716Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 4
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 105"><defs/>  <path id="" d="M51.8833,0 C23.231,0 0,23.231 0,51.8828C0,80.5371 23.231,103.767 51.8833,103.767C80.5371,103.767 103.768,80.5371 103.768,51.8828C103.768,23.231 80.5371,0 51.8833,0L51.8833,0Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 5
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 105"><defs/>  <path id="" d="M51.8828,6.48486 C76.9131,6.48486 97.2822,26.8535 97.2822,51.8828C97.2822,76.9126 76.9131,97.2817 51.8828,97.2817C26.8535,97.2817 6.48438,76.9126 6.48438,51.8828C6.48438,26.8535 26.8535,6.48486 51.8828,6.48486M51.8828,0 C23.2305,0 0,23.231 0,51.8828C0,80.5366 23.2305,103.766 51.8828,103.766C80.5371,103.766 103.768,80.5366 103.768,51.8828C103.768,23.231 80.5371,0 51.8828,0L51.8828,0Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 6
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 105"><defs/>  <path id="" d="M51.8545,0 C23.2197,0 0,23.2188 0,51.8535C0,80.4873 23.2197,103.707 51.8545,103.707C80.4883,103.707 103.708,80.4873 103.708,51.8535C103.708,23.2188 80.4883,0 51.8545,0Z M82.0898,68.7568 L68.7578,82.0889L51.8545,65.1846L34.9502,82.0889L21.6182,68.7568L38.5225,51.8535L21.6182,34.9492L34.9502,21.6172L51.8545,38.5215L68.7578,21.6172L82.0898,34.9492L65.1865,51.8535L82.0898,68.7568Z " style="fill:#000000;fill-opacity:1;"/></svg>'
   WHERE [dbo].[Shape].[ID] = 7
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 105"><defs/>  <path id="" d="M105.031,105.029 L0,105.029L0,0L105.031,0L105.031,105.029Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 8
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 105"><defs/>  <path id="" d="M105.031,105.029 L0,105.029L0,0L105.031,0L105.031,105.029Z M6.78809,98.2412 L98.2422,98.2412L98.2422,6.78809L6.78809,6.78809L6.78809,98.2412Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 9
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 105"><defs/>  <path id="" d="M53.0586,0 C23.7588,0 0,23.751 0,53.0498C0,82.3486 23.7588,106.101 53.0586,106.101C82.3486,106.101 106.108,82.3486 106.108,53.0498C106.108,23.751 82.3486,0 53.0586,0Z M50.5566,75.9795 L46.79,80.0479L43.1807,75.8428L22.7051,52.0225L29.375,44.8311L45.9707,57.0498L82.2412,27.2588L89.0059,34.4346L50.5566,75.9795Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 10
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 102 95"><defs/>  <path id="" d="M57.2407,12.7202 L25.1294,12.7202L25.1294,0.000488281L0.000488281,25.7622L25.1294,50.8804L25.1294,38.1655L57.2407,38.1597L57.2407,12.7202Z " transform="translate( 0 , -0.000488281) " style="fill:#000000;fill-opacity:1;"/>  <path id="" d="M57.2407,25.1167 L31.7993,0.000488281L31.7993,12.7202L0.000488281,12.7202L0.000488281,38.1597L31.7993,38.1597L31.7993,50.8794L57.2407,25.1167Z " transform="translate(44.5195 , 44.519) " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 11
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 90 90"><defs/>  <path id="" d="M91.8105,73.4512 L64.2646,45.8984L91.8037,18.3594L73.4375,0L45.8975,27.5391L18.3594,0L0,18.3594L27.5381,45.9121L0,73.4512L18.3594,91.8105L45.8975,64.2715L73.4443,91.8105L91.8105,73.4512Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 12
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 100 95"><defs/>  <path id="" d="M101.585,38.0947 L66.3799,38.0947L84.6211,10.5039L68.7354,0L50.7988,27.1328L32.8623,0L16.9639,10.5039L35.2168,38.0947L0,38.0947L0,57.1426L30.9521,57.1426L12.6982,84.7334L28.583,95.2373L50.793,61.6553L73.002,95.2373L88.8867,84.7334L70.6445,57.1426L101.585,57.1426L101.585,38.0947Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 13
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 105 85"><defs/>  <path id="" d="M93.6289,0 L36.5947,46.8457L10.5088,27.6289L0,38.9492L32.208,76.4043L37.8809,83.0039L43.7979,76.6104L104.254,11.2812L93.6289,0Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 14
GO

UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="32" height="24" viewBox="0 0 106 80"><defs/>  <path id="" d="M72.832,0.000488281 L0,0.000488281L0,79.4536L72.832,79.4536L105.938,39.7271L72.832,0.000488281Z M63.8442,56.0396 L57.4888,62.4028L41.4082,46.3218L25.3335,62.4028L18.9712,56.0396L35.0454,39.9595L18.9712,23.8853L25.3335,17.522L41.4082,33.603L57.4888,17.522L63.8442,23.8853L47.77,39.9595L63.8442,56.0396Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 15
GO


UPDATE [dbo].[Shape]
   SET [SVG] = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" width="30" height="25" viewBox="0 0 110 100"><defs/>  <path id="" d="M108.185,86.9669 C105.212,81.6115 62.4178,7.59976 59.7469,3.0773C57.3543,-0.994965 51.6512,-1.05649 49.2567,3.0773C45.6141,9.36636 3.04184,83.0939 0.927582,86.7794C-1.70132,91.3849 1.64145,96.0402 6.15219,96.0402C9.62387,96.0402 97.9647,96.0402 102.999,96.0402C107.951,96.0402 110.359,90.8976 108.185,86.9669Z M59.162,21.1408 L57.7977,67.8019L51.1893,67.8019L49.8289,21.1408L59.162,21.1408Z M54.4969,87.7277 L54.3963,87.7277C51.0956,87.7277 48.7586,85.1046 48.7586,81.7052C48.7586,78.2033 51.1893,75.6759 54.4969,75.6759C57.994,75.6759 60.2303,78.2033 60.2303,81.7052C60.2303,85.1046 57.994,87.7277 54.4969,87.7277Z " style="fill:#000000;fill-opacity:1;"/></svg>'
 WHERE [dbo].[Shape].[ID] = 16
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteApproval
-- Description:  This SP deletes the Approval and it's files
-- Date Created: Monday, 15 October 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
		
		-- Delete from NotificationEmailQueue
		DELETE FROM [dbo].[NotificationEmailQueue]
		WHERE Approval = @P_Id
		
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO

-- Modify Return Folders Approvals Page Info
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS [DocumentPagesCount]
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
				ORDER BY 
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN Deadline
					--END ASC,
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN Deadline
					--END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN f.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN f.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN f.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN f.ModifiedDate
					END DESC--,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN f.[Status]
					--END ASC,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN f.[Status]
					--END DESC		 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO
---------------------------------------------------------------------------------------------------------- Launched on Debug










