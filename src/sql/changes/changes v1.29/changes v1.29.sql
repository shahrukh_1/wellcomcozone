USE GMGCoZone
GO

CREATE TABLE [dbo].[CollaborateChangeDefaultGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Collaborator] [int] NOT NULL,
	[Account] [int] NOT NULL,
	[Group] [int] NOT NULL	
 CONSTRAINT [PK_CollaborateChangeDefaultGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CollaborateChangeDefaultGroup]  WITH CHECK ADD  CONSTRAINT [FK_CollaborateChangeDefaultGroup_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[CollaborateChangeDefaultGroup] CHECK CONSTRAINT [FK_CollaborateChangeDefaultGroup_Collaborator]
GO

ALTER TABLE [dbo].[CollaborateChangeDefaultGroup]  WITH CHECK ADD  CONSTRAINT [FK_CollaborateChangeDefaultGroup_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[CollaborateChangeDefaultGroup] CHECK CONSTRAINT [FK_CollaborateChangeDefaultGroup_Account]
GO

ALTER TABLE [dbo].[CollaborateChangeDefaultGroup]  WITH CHECK ADD  CONSTRAINT [FK_CollaborateChangeDefaultGroup_Group] FOREIGN KEY([Group])
REFERENCES [dbo].[UserGroup] ([ID])
GO

ALTER TABLE [dbo].[CollaborateChangeDefaultGroup] CHECK CONSTRAINT [FK_CollaborateChangeDefaultGroup_Group]
GO

ALTER TABLE [dbo].[UploadEngineAccountSettings]
ADD UseUploadSettingsFromPreviousVersion bit NOT NULL DEFAULT(0)
GO

---------------------------------------------------------------------------------------------------------------Launched on Debug EU

