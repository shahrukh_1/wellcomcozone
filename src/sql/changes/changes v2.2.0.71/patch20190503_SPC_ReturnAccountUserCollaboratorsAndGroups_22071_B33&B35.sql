USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]    Script Date: 06-May-19 11:57:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO




 -- get all collaborators except those who have no access to Collaborate module
ALTER PROCEDURE [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]
	-- Add the parameters for the stored procedure here
	@P_AccountID INT,
	@P_LoggedUserID INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	DECLARE @ApproverAndReviewer INT
	DECLARE @Reviewer INT
	DECLARE @ReadOnlyr INT
	DECLARE @Retoucher INT
	
	SELECT @ApproverAndReviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'ANR';
	SELECT @Reviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RVW';
	SELECT @ReadOnlyr = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RDO';
	SELECT @Retoucher = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RET';
	
	SELECT 
		CONVERT (bit, 0) AS IsGroup,
		CONVERT (bit, 0) AS IsExternal,
		CONVERT (bit, 0) AS IsChecked,
		u.ID AS ID,
		0 AS [Count],
		
		-- IDs concatenated as string
		COALESCE(
			(SELECT DISTINCT ' cfg' + convert(varchar, ug.ID)
				FROM dbo.UserGroup ug
				INNER JOIN dbo.UserGroupUser ugu ON ugu.[User] = u.ID 
							AND ugu.UserGroup = ug.ID
				FOR XML PATH('') ),
			'')
			AS Members,
        
        -- name as given mane + family name
		u.GivenName + ' ' + u.FamilyName
			AS Name,

			---- EmailAddress
			u.EmailAddress AS EmailAddress, 
		
		-- get user role for colaborate module
		(SELECT acr.ID
			FROM dbo.ApprovalCollaboratorRole acr
			WHERE acr.ID =  CASE r.Name					
								WHEN 'Administrator' THEN @ApproverAndReviewer
								WHEN 'Manager' THEN @ApproverAndReviewer
								WHEN 'Moderator' THEN @ApproverAndReviewer
								WHEN 'Contributor' THEN @Reviewer
								WHEN 'Retoucher' THEN @Retoucher
								ELSE @ReadOnlyr
							END)						
			AS [Role]
	FROM dbo.[User] u	
	INNER JOIN dbo.[UserRole] ur ON ur.[User] = u.ID
	INNER JOIN dbo.[Role] r ON ur.Role = r.ID
	WHERE Account = @P_AccountID 
			AND r.[Key] <> 'NON'
			AND r.AppModule = (SELECT am.ID 
							   FROM dbo.AppModule am
							   WHERE am.[Key] = 0)			
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'I')
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'D')
								
			AND ( 	(SELECT rl.[Key] 	
					FROM dbo.[User] u
					JOIN dbo.[UserRole] ur ON u.ID = ur.[User]
					JOIN dbo.[Role] rl ON ur.Role = rl.ID
					JOIN dbo.[AppModule] apm ON rl.AppModule = apm.ID
					WHERE apm.[Key] = 0 and u.ID = @P_LoggedUserID) != 'MOD'
					OR
					u.ID IN 
					(SELECT DISTINCT u.ID FROM dbo.[User] u
				     JOIN dbo.UserGroupUser ugu ON u.ID = ugu.[User]
                     JOIN dbo.UserGroupUser ug ON ugu.UserGroup = ug.UserGroup
                     JOIN dbo.UserGroup grp ON ug.UserGroup = grp.ID
                     WHERE ug.[User] = @P_LoggedUserID
                     UNION
                     SELECT @P_LoggedUserID)				
				)
END



GO


