USE [GMGCoZone]
GO

--------------------------------------------------------------------Launched on PRODUCTION

ALTER TABLE [dbo].[ApprovalJobDeleteHistory]
ADD [Job] INT 

GO

-- Alter Get Billing Cycles function to retrieve proofs count correctly --------------
ALTER FUNCTION [dbo].[GetBillingCycles]
    (
      @AccountID INT ,
      @AccountType NVARCHAR(10),
      @LoggedAccountID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @ModuleId INT
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN

        DECLARE @totalExceededCount INT = 0	
        DECLARE @cycleStartDate DATETIME = @StartDate
        DECLARE @cycleAllowedProofsCount INT
        DECLARE @PlanPrice DECIMAL = 0.0
        DECLARE @SelectedBillingPLan INT
        DECLARE @BillingPlanPrice INT
        DECLARE @moduleKey VARCHAR(10)
        DECLARE @monthCycle NVARCHAR(MAX) = ''
        
		
        IF ( @AccountType = 'SBSC' ) 
            BEGIN           
				-- subscriber
                SELECT  @SelectedBillingPLan = SPI.SelectedBillingPlan ,
                        @cycleAllowedProofsCount = CASE WHEN SPI.IsQuotaAllocationEnabled = 1 THEN SPI.NrOfCredits
												   ELSE BP.Proofs END,
                        @BillingPlanPrice = BP.Price 
                FROM    Account A
                        INNER JOIN dbo.SubscriberPlanInfo SPI ON A.ID = SPI.Account
                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId
            END
        ELSE
			-- all other account types 
            BEGIN
                SELECT  @SelectedBillingPLan = ASP.SelectedBillingPlan ,
                        @cycleAllowedProofsCount = BP.Proofs ,
                        @BillingPlanPrice = BP.Price 
                FROM    Account A
                        INNER JOIN dbo.AccountSubscriptionPlan ASP ON A.ID = ASP.Account
                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId
            END
	
       																		
        SET @PlanPrice = ( CASE WHEN ( @LoggedAccountID = 1 )
                                THEN @BillingPlanPrice
                                ELSE ( SELECT   NewPrice
                                       FROM     AttachedAccountBillingPlan
                                       WHERE    Account = @LoggedAccountID
                                                AND BillingPlan = @SelectedBillingPLan
                                     )
                           END )
						
																					
        SET @cycleStartDate = @StartDate
	
        WHILE ( datepart(year, @cycleStartDate) < datepart(year,@EndDate) OR
			  ( datepart(year, @cycleStartDate) = datepart(year,@EndDate) AND datepart(month, @cycleStartDate) <= datepart(month,@EndDate)) ) 
            BEGIN	
                DECLARE @currentCycleJobsCount INT = 0
				                                     	
                SELECT  @currentCycleJobsCount = CASE WHEN Am.[Key] = 0 -- Collaborate
                                                           THEN 
															   (
																SELECT
																ISNULL(SUM(r.ProofsUsed), 0)
																FROM (
																		SELECT
																			SUM(d.ProofsPerJob) AS ProofsUsed
																		FROM(
																			  SELECT
																				  CEILING(COUNT(t.ID) / CAST(4 AS FLOAT)) AS ProofsPerJob
																			  FROM 
																				(
																					SELECT ap.ID, j.Account, ap.CreatedDate, ap.FileName,ap.Job				
																					FROM
																							Job j
																							INNER JOIN Approval ap ON j.ID = ap.Job
																							WHERE
																							j.Account = @AccountID
																							AND datepart(month, ap.CreatedDate) = datepart(month, @cycleStartDate)
																							AND datepart(year, ap.CreatedDate) = datepart(year, @cycleStartDate)
																							
																					UNION ALL
																					SELECT a.ID, a.Account, a.CreatedDate, a.JobName, a.Job		
																							FROM dbo.ApprovalJobDeleteHistory a
																							WHERE 
																							a.Account = @AccountID
																							AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
																							AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
																							AND a.Job IS NOT NULL
																				 ) t
																			 GROUP BY t.Job
																			)d
																		UNION ALL
																		SELECT 
																			SUM(dn.ProofsPerJob) AS ProofsUsed
																		FROM(
																				SELECT 
																					CEILING(COUNT(a.ID) / CAST(4 AS FLOAT)) AS ProofsPerJob
																				FROM dbo.ApprovalJobDeleteHistory a
																				WHERE 
																				a.Account = @AccountID
																				AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
																				AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
																				AND a.Job IS NULL
																				GROUP BY a.JobName
																			)dn
																		)r
															)                                                           
                                                      WHEN Am.[Key] = 2 -- Deliver
														THEN                                                         
                                                           ( SELECT
                                                                COUNT(r.ID)
                                                              FROM (SELECT
																		dj.ID 
																	FROM Job j
																	INNER JOIN dbo.DeliverJob dj ON j.ID = dj.Job
																	WHERE
																	j.Account = @AccountID
																	AND datepart(month, dj.CreatedDate) = datepart(month, @cycleStartDate)
																	AND datepart(year, dj.CreatedDate) = datepart(year, @cycleStartDate)
																	UNION ALL
																	SELECT 
																		d.ID 
																	FROM dbo.DeliverJobDeleteHistory d
																	WHERE 
																	d.Account = @AccountID
																	AND datepart(month, d.CreatedDate) = datepart(month, @cycleStartDate)
																	AND datepart(year, d.CreatedDate) = datepart(year, @cycleStartDate)
																	) AS r
															) 
                                                              
                                                      ELSE 0 -- for other modules 0 jobs is returned
                                                 END
                FROM    dbo.AppModule AM
                WHERE   AM.ID = @ModuleId
				
																                                         							
                SET @totalExceededCount =  CASE WHEN ( @cycleAllowedProofsCount < @currentCycleJobsCount )
										   THEN @currentCycleJobsCount
												- @cycleAllowedProofsCount
										   ELSE 0
										   END
										
			    SET @monthCycle = @monthCycle + ',' +  CONVERT(NVARCHAR, LEFT(datename(month, @cycleStartDate), 3)) + '-' + CONVERT(NVARCHAR, datepart(year, @cycleStartDate) % 100) + '|' +  CONVERT(NVARCHAR, @currentCycleJobsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice) + '|' +  CONVERT(NVARCHAR, @cycleAllowedProofsCount)
			    
			    SET @cycleStartDate = DATEADD(MM, 1, @cycleStartDate)
                    
            END
		
        RETURN  @monthCycle
	
    END
GO
-----------------------------------------------------------------
--------------------------------------------------------------------Launched on STAGE
--------------------------------------------------------------------Launched on TESTEU