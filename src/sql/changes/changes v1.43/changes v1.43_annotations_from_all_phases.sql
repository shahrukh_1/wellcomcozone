USE GMGCoZone;

ALTER TABLE [dbo].[ApprovalJobPhase]
ADD [PhaseTemplateID] INT NOT NULL DEFAULT(0)
GO


CREATE TABLE [dbo].[PhaseDeadlineUnit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_PhaseDeadlineUnit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [dbo].[PhaseDeadlineUnit] VALUES(0, 'Hours'),(1, 'Days')
GO

CREATE TABLE [dbo].[PhaseDeadlineTrigger](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_PhaseDeadlineTrigger] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [dbo].[PhaseDeadlineTrigger] VALUES(0, 'After Upload'), (1, 'Last Phase Completed'), (2, 'Approval Deadline')
GO

ALTER TABLE [dbo].[ApprovalPhase]
ADD  [DeadlineUnitNumber] [int] NULL,
     [DeadlineUnit] [int] NULL,
	 [DeadlineTrigger] [int] NULL,	 
	 [Deadline] [datetime2](7) NULL
GO


ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineUnit] FOREIGN KEY([DeadlineUnit])
REFERENCES [dbo].[PhaseDeadlineUnit] ([ID])
GO

ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineUnit]
GO

ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineTrigger] FOREIGN KEY([DeadlineTrigger])
REFERENCES [dbo].[PhaseDeadlineTrigger] ([ID])
GO

ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineTrigger]
GO

ALTER TABLE [dbo].[ApprovalJobPhase]
ADD  [DeadlineUnitNumber] [int] NULL,
     [DeadlineUnit] [int] NULL,
	 [DeadlineTrigger] [int] NULL,	 
	 [Deadline] [datetime2](7) NULL
GO

ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineUnit] FOREIGN KEY([DeadlineUnit])
REFERENCES [dbo].[PhaseDeadlineUnit] ([ID])
GO

ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineUnit]
GO

ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineTrigger] FOREIGN KEY([DeadlineTrigger])
REFERENCES [dbo].[PhaseDeadlineTrigger] ([ID])
GO

ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineTrigger]
GO

-------------------------------------------------------Launched on TEST
-------------------------------------------------------Launched on STAGE
-------------------------------------------------------Launched on Production