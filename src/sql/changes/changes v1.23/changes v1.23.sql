USE GMGCoZone
GO
  
-- Changes for Encrypt ColorProof Instance Password In Database

IF NOT EXISTS (select * from sys.symmetric_keys where name like '%DatabaseMasterKey%') 
CREATE MASTER KEY ENCRYPTION BY 
PASSWORD = 'TOFILLDBPASSWORD'
 
IF NOT EXISTS ( SELECT * FROM sys.certificates WHERE name = 'ColorProofInstances' )
CREATE CERTIFICATE ColorProofInstances
WITH SUBJECT = 'ColorProofInstances';
GO

IF NOT EXISTS (SELECT * FROM sys.symmetric_keys WHERE [name] =  'ColorProofInstances')
CREATE SYMMETRIC KEY ColorProofInstances
WITH ALGORITHM = AES_256
ENCRYPTION BY CERTIFICATE ColorProofInstances;
GO
  
CREATE PROCEDURE [dbo].[SPC_ReturnColorProofInstanceLogin]
    (
      @P_Username VARCHAR(255) ,
      @P_Password VARCHAR(255)
    )
AS 
    BEGIN
      -- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
        SELECT	DISTINCT
                ci.*
        FROM    [dbo].[ColorProofInstance] ci
                
        WHERE   
                ci.[Username] = @P_Username
                AND CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), ci.Password, 2 ))) = @P_Password
                AND ci.IsDeleted = 0
    END
 GO
 
 CREATE PROCEDURE [dbo].[SPC_EncryptColorProofInstancesPassword] (
	@P_Password varchar(128)
)
AS
BEGIN
	-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances 
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
	SELECT CONVERT(VARCHAR(max), EncryptByKey(Key_GUID('ColorProofInstances'), @P_Password), 2) AS RetVal
END
GO

CREATE PROCEDURE [dbo].[SPC_DecryptColorProofInstancesPassword] (
	@P_Password varchar(max)
)
AS
BEGIN
-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
	SELECT  CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), @P_Password, 2 ))) as RetVal
END
GO

CREATE PROCEDURE [dbo].[SPC_ReturnColorProofInstanceLoginByPairingCode]
    (
      @P_PairingCode VARCHAR(255) 
    )
AS 
    BEGIN

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
        SELECT	DISTINCT
                ci.[UserName],               
                CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), ci.Password, 2 ))) AS [Password]
        FROM    [dbo].[ColorProofInstance] ci
                
        WHERE               
                ci.PairingCode = @P_PairingCode
                AND ci.IsDeleted = 0
    END
GO
 
ALTER TABLE dbo.ColorProofInstance
ALTER COLUMN Password VARCHAR(512) NOT NULL

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
UPDATE dbo.[ColorProofInstance] 
SET [Password] = (SELECT CONVERT(VARCHAR(max), EncryptByKey(Key_GUID('ColorProofInstances'), [Password]), 2))
GO

-----------------------------------------------------------------------------------
-- Add Return Account User Collaborators And Groups procedure
-- used for retrieving account user and collaborator group
ALTER PROCEDURE [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]
	-- Add the parameters for the stored procedure here
	@P_AccountID INT,
	@P_LoggedUserID INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	DECLARE @ApproverAndReviewer INT
	DECLARE @Reviewer INT
	DECLARE @ReadOnlyr INT
	
	SELECT @ApproverAndReviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'ANR';
	SELECT @Reviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RVW';
	SELECT @ReadOnlyr = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RDO';
	
	SELECT 
		CONVERT (bit, 0) AS IsGroup,
		CONVERT (bit, 0) AS IsExternal,
		CONVERT (bit, 0) AS IsChecked,
		u.ID AS ID,
		0 AS [Count],
		
		-- IDs concatenated as string
		COALESCE(
			(SELECT DISTINCT ' cfg' + convert(varchar, ug.ID)
				FROM dbo.UserGroup ug
				INNER JOIN dbo.UserGroupUser ugu ON ugu.[User] = u.ID 
							AND ugu.UserGroup = ug.ID
				FOR XML PATH('') ),
			'')
			AS Members,
        
        -- name as given mane + family name
		u.GivenName + ' ' + u.FamilyName
			AS Name,
		
		-- get user role for colaborate module
		(SELECT acr.ID
			FROM dbo.ApprovalCollaboratorRole acr
			WHERE acr.ID =  CASE (SELECT r.Name
								FROM dbo.UserRole ur 
								INNER JOIN dbo.[Role] r ON ur.Role = r.ID						
								WHERE ur.[User] = u.ID
									AND r.AppModule = (SELECT am.ID FROM dbo.AppModule am
														WHERE am.[Key] = 0))
														
								WHEN 'Administrator' THEN @ApproverAndReviewer
								WHEN 'Manager' THEN @ApproverAndReviewer
								WHEN 'Moderator' THEN @ApproverAndReviewer
								WHEN 'Contributor' THEN @Reviewer
								ELSE @ReadOnlyr
							END)						
			AS [Role]
			
	FROM dbo.[User] u
	WHERE Account = @P_AccountID
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'I')
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'D')
								
			AND ( 	(SELECT rl.[Key] 	
					FROM dbo.[User] u
					JOIN dbo.[UserRole] ur ON u.ID = ur.[User]
					JOIN dbo.[Role] rl ON ur.Role = rl.ID
					JOIN dbo.[AppModule] apm ON rl.AppModule = apm.ID
					WHERE apm.[Key] = 0 and u.ID = @P_LoggedUserID) != 'MOD'
					OR
					u.ID IN 
					(SELECT DISTINCT u.ID FROM dbo.[User] u
				     JOIN dbo.UserGroupUser ugu ON u.ID = ugu.[User]
                     JOIN dbo.UserGroupUser ug ON ugu.UserGroup = ug.UserGroup
                     WHERE ug.[User] = @P_LoggedUserID
                     UNION
                     SELECT @P_LoggedUserID)				
				)
END
GO

--Put White Label Settings Tab under Settings for Edit Account Menu
UPDATE mi 
  SET Parent = (SELECT TOP 1 [ID] FROM dbo.MenuItem 
				WHERE [Key] = 'ACSE')
  FROM [dbo].[MenuItem] mi
  WHERE mi.[Key] = 'ACWL'
GO

--Put Profile Menu Before Settings Menu
UPDATE [dbo].[MenuItem]
  SET Position = 1
  WHERE [Key] = 'ACPF'
GO
UPDATE [dbo].[MenuItem]
  SET Position = 2
  WHERE [Key] = 'ACSE'
GO

-- Add flag for account for default user notifications 
ALTER TABLE dbo.Account
ADD EnableNotificationsByDefault BIT NOT NULL DEFAULT(0)
 GO 
---------------------------------------------------------------Launched on debug

---------------------------------------------------------------Launched on release