USE GMGCoZone
GO
-- Upload Engine Dabatase Authorization
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT NULL , -- ControllerAction - int
          mi.ID , -- Parent - int
          7 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SEUE' , -- Key - nvarchar(4)
          'Upload Engine' , -- Name - nvarchar(64)
          'Upload Engine'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SETT'
GO

-- Upload Engine Templates Dabatase Authorization
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'UploadEngineTemplates', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'UETM' , -- Key - nvarchar(4)
          'Upload Engine Templates' , -- Name - nvarchar(64)
          'Upload Engine Templates'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SEUE'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('UETM')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

-- Upload Engine Settings Dabatase Authorization
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'UploadEngineSettings', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'UESE' , -- Key - nvarchar(4)
          'Upload Engine Settings' , -- Name - nvarchar(64)
          'Upload Engine Settings'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SEUE'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('UESE')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

--Create FTPProtocol Table

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'FTPProtocol' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[FTPProtocol]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Key] int NOT NULL,
			  [Name] [nvarchar](32) NOT NULL,				
              CONSTRAINT [PK_FTPProtocol] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]            
    END
ELSE 
    PRINT 'Table FTPProtocol already exists'
GO

INSERT  [dbo].[FTPProtocol]
        ( [Key], [Name] )
VALUES  ( 0, N'FTP' )
INSERT  [dbo].[FTPProtocol]
        ( [Key], [Name] )
VALUES  ( 1, N'FTPS' )

GO
--Create IO Presets Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEnginePreset' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[UploadEnginePreset]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Guid] [nvarchar](36) NULL ,
              [Account] INT NOT NULL ,
              [Name] [nvarchar](128) NOT NULL,
			  [IsEnabledInCollaborate] [bit] NOT NULL,
			  [IsEnabledInDeliver] [bit] NOT NULL,
			  [Host] [nvarchar](64) NOT NULL,
			  [Port] [int] NOT NULL,
			  [Username] [nvarchar](128) NULL,
			  [Password] [nvarchar](128) NULL,
			  [DefaultDirectory] [nvarchar](128) NULL,
			  [ReplicateCoZoneFolders] [bit] NOT NULL,
			  [IsActiveTransferMode] [bit] NOT NULL,
			  [Protocol] [int] NOT NULL,		  
              CONSTRAINT [PK_UploadEnginePreset] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
              
        ALTER TABLE [dbo].UploadEnginePreset  WITH CHECK ADD  CONSTRAINT [FK_UploadEnginePreset_Account] FOREIGN KEY([Account])
        REFERENCES [dbo].[Account] ([ID])
		
		ALTER TABLE [dbo].UploadEnginePreset  WITH CHECK ADD  CONSTRAINT [FK_UploadEnginePreset_FTPProtocol] FOREIGN KEY([Protocol])
        REFERENCES [dbo].[FTPProtocol] ([ID])

    END
ELSE 
    PRINT 'Table UploadEnginePreset already exists'
GO

--Add New I/O Preset ControllerAction with authorizations

DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditPreset', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AEPR' , -- Key - nvarchar(4)
          'Add Edit I/O Preset' , -- Name - nvarchar(64)
          'Add Edit I/O Preset'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'UETM'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('AEPR')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnFTPUserLogin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnFTPUserLogin]
GO

-- Add FTP Login stored procedure
CREATE PROCEDURE [dbo].[SPC_ReturnFTPUserLogin]
    (
      @P_Username VARCHAR(255) ,
      @P_Password VARCHAR(255)
    )
AS 
    BEGIN
        SELECT	DISTINCT
                u.*
        FROM    [dbo].[User] u
                INNER JOIN [dbo].[UserStatus] us ON u.[Status] = us.[ID]
                INNER JOIN [dbo].[Account] ac ON u.[Account] = ac.[ID]
                INNER JOIN [dbo].[AccountStatus] acs ON ac.[Status] = acs.[ID]
                INNER JOIN [dbo].[UserGroupUser] ugu ON u.[ID] = ugu.[User]
                INNER JOIN [dbo].[UserGroup] ug ON ug.[ID] = ugu.[UserGroup]
				INNER JOIN [dbo].[UploadEngineUserGroupPermissions] ugp ON ugu.[UserGroup] = ugp.[UserGroup]
				INNER JOIN [dbo].[UploadEnginePermission] uep ON ugp.[UploadEnginePermission] = uep.[ID]
        WHERE   u.[Username] = @P_Username  AND
				u.[Password] = CONVERT(VARCHAR(255), HashBytes('SHA1', @P_Password)) AND
				uep.[Key] = 1 AND
				us.[Key] = 'A' AND
				acs.[Key] = 'A'
    END
 GO

--Add New UploadEngine Profile ControllerAction with authorizations

DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditProfile', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AEPL' , -- Key - nvarchar(4)
          'Add Edit XML Engine Profile' , -- Name - nvarchar(64)
          'Add Edit XML Engine Profile'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'UETM'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('AEPL')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

--Create Profile Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineProfile' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[UploadEngineProfile]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Guid] [nvarchar](36) NULL ,
              [Account] INT NOT NULL ,
              [Name] [nvarchar](128) NOT NULL,
			  [HasInputSettings] [bit] NOT NULL,
			  [OutputPreset] [int] NULL,
			  [XMLTemplate] [int] NULL,
			  [IsActive] [bit] NOT NULL,
			  [IsDeleted] [bit] NOT NULL, 		
              CONSTRAINT [PK_UploadEngineProfile] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
              
        ALTER TABLE [dbo].UploadEngineProfile  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineProfile_Account] FOREIGN KEY([Account])
        REFERENCES [dbo].[Account] ([ID])
		
		ALTER TABLE [dbo].UploadEngineProfile  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineProfile_Preset] FOREIGN KEY([OutputPreset])
        REFERENCES [dbo].[UploadEnginePreset] ([ID])
					
    END
ELSE 
    PRINT 'Table UploadEngineProfile already exists'
GO

-- Add  Upload Settings model
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineDuplicateFileName' )
   )
	BEGIN
		CREATE TABLE [dbo].[UploadEngineDuplicateFileName]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Key] [int] NOT NULL,
				[Name] [nvarchar](32) NOT NULL,
				CONSTRAINT [PK_UploadEngineDuplicateFileName] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
						ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table UploadEngineDuplicateFileName already exists'
GO

INSERT [dbo].[UploadEngineDuplicateFileName]
	([KEY], [Name])
VALUES
	(0, 'Ignore'),
	(1, 'AppendNumberToEnd'),
	(2, 'CreateNewVersion')	
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineVersionName' )
   )
	BEGIN
		CREATE TABLE [dbo].[UploadEngineVersionName]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Key] [int] NOT NULL,
				[Name] [nvarchar](32) NOT NULL,
				CONSTRAINT [PK_UploadEngineVersionName] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
						ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table UploadEngineVersionName already exists'
GO

INSERT [dbo].[UploadEngineVersionName]
	([KEY], [Name])
VALUES
	(0, 'Keep Original FileName'),
	(1, 'AppendSuffixvX'),
	(2, 'AppendSuffix_vX'),
	(3, 'AppendvXAtPosition')	
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineAccountSettings' )
   )
	BEGIN
		CREATE TABLE [dbo].[UploadEngineAccountSettings]
			(
					[ID] [int] IDENTITY(1,1) NOT NULL,
					[DuplicateFileName] [int] NOT NULL,
					[VersionName] [int] NOT NULL,
					[VersionNumberPositionInName] [int] NULL,
					[VersionNumberPositionFromStart] [bit] NULL,
					[AllowPDF] [bit] NOT NULL,
					[AllowImage] [bit] NOT NULL,
					[AllowVideo] [bit] NOT NULL,
					[AllowSWF] [bit] NOT NULL,
					[AllowZip] [bit] NOT NULL,
					[ZipFilesWhenDownloading] [bit] NOT NULL,
				CONSTRAINT [PK_UploadEngineAccountSettings] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
					)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
							ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
				)
			ON [PRIMARY]

		ALTER TABLE [dbo].[UploadEngineAccountSettings]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineDuplicateFileName] FOREIGN KEY([DuplicateFileName])
		REFERENCES [dbo].[UploadEngineDuplicateFileName] ([ID])

		ALTER TABLE [dbo].[UploadEngineAccountSettings] CHECK CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineDuplicateFileName]

		ALTER TABLE [dbo].[UploadEngineAccountSettings]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineVersionName] FOREIGN KEY([VersionName])
		REFERENCES [dbo].[UploadEngineVersionName] ([ID])
	
		ALTER TABLE [dbo].[UploadEngineAccountSettings] CHECK CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineVersionName]
	
	END
ELSE
	PRINT 'Table UploadEngineAccountSettings already exists'
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineUserGroupPermissions' )
   )
	BEGIN
		CREATE TABLE [dbo].[UploadEngineUserGroupPermissions]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[UserGroup] [int] NOT NULL,
				[UploadEnginePermission] [int] NOT NULL,				
			CONSTRAINT [PK_UploadEngineUserGroupPermissions] PRIMARY KEY CLUSTERED 
			(
				[ID] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table UploadEngineUserGroupPermissions already exists'
GO

--Create UploadEnginePermission Table

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEnginePermission' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[UploadEnginePermission]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Key] int NOT NULL,
			  [Name] [nvarchar](32) NOT NULL,				
              CONSTRAINT [PK_UploadEnginePermission] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]            
    END
ELSE 
    PRINT 'Table UploadEnginePermission already exists'
GO

INSERT  [dbo].[UploadEnginePermission]
        ( [Key], [Name] )
VALUES  ( 1, N'Uploader' )
INSERT  [dbo].[UploadEnginePermission]
        ( [Key], [Name] )
VALUES  ( 4, N'DownloadFromCollaborate' )
INSERT  [dbo].[UploadEnginePermission]
        ( [Key], [Name] )
VALUES  ( 8, N'DownloadFromDeliver' )

GO

-- Add UploadEngineUserGroupPermissions relationships
ALTER TABLE [dbo].[UploadEngineUserGroupPermissions]
WITH CHECK ADD  CONSTRAINT [FK_UploadEngineUserGroupPermissions_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO

ALTER TABLE [dbo].[UploadEngineUserGroupPermissions]
WITH CHECK ADD  CONSTRAINT [FK_UploadEngineUserGroupPermissions_UploadEnginePermission] FOREIGN KEY([UploadEnginePermission])
REFERENCES [dbo].[UploadEnginePermission] ([ID])
GO

-- Add Account-UploadEngineAccountSettings relationship
ALTER TABLE dbo.[Account]
ADD [UploadEngineSetings] [int] NULL
GO

ALTER TABLE [dbo].[Account]
WITH CHECK ADD  CONSTRAINT [FK_Account_UploadEngineAccountSettings] FOREIGN KEY([UploadEngineSetings])
REFERENCES [dbo].[UploadEngineAccountSettings] ([ID])
GO

--Create FTPPresetJobDownload

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'FTPPresetJobDownload' )
   )
	BEGIN
		CREATE TABLE [dbo].[FTPPresetJobDownload]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Preset] [int] NOT NULL,
				[ApprovalJob] [int] NULL,
				[DeliverJob] [int] NULL,
				[GroupKey] nvarchar(64) NULL,
				[Status] [bit] NULL,
				[StatusDescription] nvarchar(128) NULL,
			CONSTRAINT [PK_FTPPresetJobDownload] PRIMARY KEY CLUSTERED 
			(
				[ID] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table FTPPresetJobDownload already exists'
GO

ALTER TABLE [dbo].[FTPPresetJobDownload]
WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_Approval] FOREIGN KEY([ApprovalJob])
REFERENCES [dbo].[Approval] ([ID])

ALTER TABLE [dbo].[FTPPresetJobDownload]
WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_DeliverJob] FOREIGN KEY([DeliverJob])
REFERENCES [dbo].[DeliverJob] ([ID])

ALTER TABLE [dbo].[FTPPresetJobDownload]
WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_UploadEnginePreset] FOREIGN KEY([Preset])
REFERENCES [dbo].[UploadEnginePreset] ([ID])
GO


--Create XML Templates tables

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineDefaultXMLTemplate' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[UploadEngineDefaultXMLTemplate]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
			  [Key] [int] NOT NULL,
              [NodeName] [nvarchar](128) NOT NULL,
			  [XPath] [nvarchar](256) NOT NULL,
              CONSTRAINT [PK_UploadEngineDefaultXMLTemplates] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]            
    END
ELSE 
    PRINT 'Table UploadEngineDefaultXMLTemplate already exists'
GO

INSERT INTO [GMGCoZone].[dbo].[UploadEngineDefaultXMLTemplate]
           ([Key]
		   ,[NodeName]
           ,[XPath])
     VALUES
           (0, 'JobName', '/CoZoneJob/Header/JobName'),
		   (1, 'Modules', '/CoZoneJob/Header/Modules'),
		   (2, 'InternalCollaborator', '/CoZoneJob/Collaborate/Collaborators/Internal//Collaborator'),
		   (3, 'ExternalCollaborator', '/CoZoneJob/Collaborate/Collaborators/External//Collaborator'),
		   (4, 'PrimaryDecisionMaker', '/CoZoneJob/Collaborate/PrimaryDecisionMaker'),
           (5, 'Owner','/CoZoneJob/Collaborate/Owner'),
           (6, 'GenerateSeparations','/CoZoneJob/Collaborate/GenerateSeparations'),
           (7, 'LockWhenAllDecisionsHaveBeenMade', '/CoZoneJob/Collaborate/LockWhenAllDecisionsHaveBeenMade'),
           (8, 'Deadline', '/CoZoneJob/Collaborate/Deadline'),
           (9, 'AllowUsersToDownloadOriginalFile', '/CoZoneJob/Collaborate/AllowUsersToDownloadOriginalFile'),
           (10, 'DestinationFolder', '/CoZoneJob/Collaborate/DestinationFolder'),
           (11, 'ColorProofCozoneInstanceName', '/CoZoneJob/Deliver/ColorProofCozoneInstanceName'),
           (12, 'ColorProofCozoneWorkflow', '/CoZoneJob/Deliver/ColorProofCozoneWorkflow'),
           (13, 'Pages', '/CoZoneJob/Deliver/Pages'),
           (14, 'LogProofControlResults', '/CoZoneJob/Deliver/LogProofControlResults'),
           (15, 'IncludeProofMetaInformationOnProofLabel', '/CoZoneJob/Deliver/IncludeProofMetaInformationOnProofLabel')
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineCustomXMLTemplate' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[UploadEngineCustomXMLTemplate]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Name] [nvarchar](32) NOT NULL,
			  [Account] int NOT NULL,
              CONSTRAINT [PK_UploadEngineCustomXMLTemplates] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]            
    END
ELSE 
    PRINT 'Table UploadEngineCustomXMLTemplate already exists'

ALTER TABLE [dbo].[UploadEngineCustomXMLTemplate]
WITH CHECK ADD  CONSTRAINT [FK_UploadEngineCustomXMLTemplate_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'UploadEngineCustomXMLTemplatesField' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[UploadEngineCustomXMLTemplatesField]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
		      [XPath] [nvarchar] (128) NOT NULL,
			  [UploadEngineCustomXMLTemplate] int NOT NULL,
			  [UploadEngineDefaultXMLTemplate] int NOT NULL,
              CONSTRAINT [PK_UploadEngineCustomXMLTemplatesFields] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]            
    END
ELSE 
    PRINT 'Table UploadEngineCustomXMLTemplatesField already exists'
GO

ALTER TABLE [dbo].[UploadEngineCustomXMLTemplatesField]
WITH CHECK ADD  CONSTRAINT [FK_UploadEngineCustomXMLTemplatesField_UploadEngineCustomXMLTemplate] FOREIGN KEY([UploadEngineCustomXMLTemplate])
REFERENCES [dbo].[UploadEngineCustomXMLTemplate] ([ID])

ALTER TABLE [dbo].[UploadEngineCustomXMLTemplatesField]
WITH CHECK ADD  CONSTRAINT [FK_UploadEngineCustomXMLTemplatesField_UploadEngineDefaultXMLTemplate] FOREIGN KEY([UploadEngineDefaultXMLTemplate])
REFERENCES [dbo].[UploadEngineDefaultXMLTemplate] ([ID])
GO

--Add reference to xml templates from profiles
ALTER TABLE [dbo].UploadEngineProfile  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineProfile_UploadEngineCustomXMLTemplate] FOREIGN KEY([XMLTemplate])
REFERENCES [dbo].[UploadEngineCustomXMLTemplate] ([ID])

--Add New XML Template ControllerAction with authorizations

DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditXMLTemplate', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AEXT' , -- Key - nvarchar(4)
          'Add Edit XML Templates' , -- Name - nvarchar(64)
          'Add Edit XML Templates'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'UETM'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('AEXT')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

if ( not exists ( select    *
                  from      information_schema.tables
                  where     table_schema = 'dbo'
                            and table_name = 'JobSource' )
   ) 
    begin
        create table [dbo].[JobSource]
            (
              [ID] [int] identity(1, 1)
                         not null ,
              [Key] [int] not null,
		      [Name] [nvarchar] (128) not null,
              constraint [pk_jobsourcetype] primary key clustered
                ( [id] asc )
                with ( pad_index = off, statistics_norecompute = off,
                       ignore_dup_key = off, allow_row_locks = on,
                       allow_page_locks = on ) on [primary]
            )
        on  [primary]            
    end
else 
    print 'table JobSource already exists'
go

insert into [GMGCoZone].[dbo].[JobSource]
           ([Key]
		   ,[Name]
           )
     values
           (0, 'Web UI'),
		   (1, 'Web Service'),
		   (2, 'Upload Engine')
go

--add new foreign key column to appoval , deliver jobs to specify job source

alter table [dbo].[Approval]
add JobSource int not null CONSTRAINT DF_Approval_JobSource default(1),
FTPSourceFolder nvarchar(128)
go

alter table [dbo].[Approval]
with check add  constraint [FK_Approval_JobSource] foreign key([JobSource])
references [dbo].[JobSource] ([id])
go

alter table [dbo].[DeliverJob]
add JobSource int not null CONSTRAINT DF_DeliverJob_JobSource default(1),
FTPSourceFolder nvarchar(128)
go

alter table [dbo].[DeliverJob]
with check add  constraint [FK_DeliverJob_JobSource] foreign key([JobSource])
references [dbo].[JobSource] ([id])
go

---------------------------------------------------------------Launched on debug
---------------------------------------------------------------Launched on release











