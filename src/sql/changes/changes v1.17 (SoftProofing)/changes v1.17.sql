USE GMGCoZone
GO
ALTER TABLE dbo.BillingPlan ADD
	SoftProofingWorkstations int NOT NULL CONSTRAINT DF_BillingPlan_SoftProofingWorkstations DEFAULT (0)
GO

CREATE TABLE dbo.WorkstationStatus
	(
	ID int NOT NULL IDENTITY (1, 1),
	[Key] int NOT NULL,
	Name nvarchar(64) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.WorkstationStatus ADD CONSTRAINT
	PK_WorkstationStatus PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE TABLE dbo.Workstation
	(
	ID int NOT NULL IDENTITY (1, 1),
	Guid varchar(36) NOT NULL,
	MachineId nvarchar(64) NOT NULL,
	[User] int NOT NULL,
	Status int NOT NULL,
	Location nvarchar(128) NOT NULL,
	IsCalibrated bit NULL,
	LastActivityDate datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Workstation ADD CONSTRAINT
	PK_Workstation PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Workstation ADD CONSTRAINT
	FK_Workstation_User FOREIGN KEY
	(
	[User]
	) REFERENCES dbo.[User]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Workstation ADD CONSTRAINT
	FK_Workstation_WorkstationStatus FOREIGN KEY
	(
	Status
	) REFERENCES dbo.WorkstationStatus
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Workstation ADD
	IsDeleted bit NOT NULL CONSTRAINT DF_Workstation_IsDeleted DEFAULT (0)
GO
CREATE TABLE dbo.WorkstationCalibrationData
	(
	ID int NOT NULL IDENTITY (1, 1),
	Guid varchar(36) NOT NULL,
	Filename nvarchar(128) NOT NULL,
	Processed bit NOT NULL,
	Workstation int NOT NULL,
	WorkstationName nvarchar(128) NULL,
	CalibrationSoftware nvarchar(256) NULL,
	CalibrationDate datetime NULL,
	UploadedDate datetime NOT NULL,
	DisplayIdentifier nvarchar(64) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.WorkstationCalibrationData ADD CONSTRAINT
	PK_WorkstationCalibrationData PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.WorkstationCalibrationData ADD CONSTRAINT
	FK_WorkstationCalibrationData_Workstation FOREIGN KEY
	(
	Workstation
	) REFERENCES dbo.Workstation
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.WorkstationCalibrationData ADD
	LastErrorMessage nvarchar(512) NOT NULL CONSTRAINT DF_WorkstationCalibrationData_LastErrorMessage DEFAULT ('')
GO
CREATE TABLE dbo.WorkstationValidationResult
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name nvarchar(64) NOT NULL,
	Value nvarchar(64) NOT NULL,
	WorkstationCalibrationData int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.WorkstationValidationResult ADD CONSTRAINT
	PK_WorkstationValidationResult PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.WorkstationValidationResult ADD CONSTRAINT
	FK_WorkstationValidationResult_WorkstationCalibrationData FOREIGN KEY
	(
	WorkstationCalibrationData
	) REFERENCES dbo.WorkstationCalibrationData
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
INSERT  INTO dbo.WorkstationStatus
        ( [Key], Name )
VALUES  ( 1, -- Key - int
          'Active'  -- Name - nvarchar(64)
          )
INSERT  INTO dbo.WorkstationStatus
        ( [Key], Name )
VALUES  ( 2, -- Key - int
          'Inactive'  -- Name - nvarchar(64)
          )
GO
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'SoftProofing', -- Controller - nvarchar(128)
          'Settings', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
GO

DECLARE @ParentMenuId AS INT
DECLARE @ControllerActionId AS INT
SELECT  @ParentMenuId = ID
FROM    dbo.MenuItem
WHERE   [Key] = 'SEIN'

SELECT  @ControllerActionId = ID
FROM    dbo.ControllerAction
WHERE   Controller = 'SoftProofing'
        AND Action = 'Settings'
      
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( @ControllerActionId , -- ControllerAction - int
          @ParentMenuId , -- Parent - int
          11 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SESP' , -- Key - nvarchar(4)
          'SoftProofing Settings' , -- Name - nvarchar(64)
          'Settings'  -- Title - nvarchar(128)
        )
GO

-- softproofing settings menu for collaborate module and ADM rights
DECLARE @atrId AS INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 0 AND R.[Key] = 'ADM'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SEIN', 'SESP' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO
INSERT INTO dbo.AccountSetting
        ( Account ,
          Name ,
          Value ,
          ValueDataType ,
          Description
        )
VALUES  ( null , -- Account - int
          'SoftproofingCalibrationTimeoutDays' , -- Name - nvarchar(64)
          '7' , -- Value - nvarchar(max)
          2 , -- ValueDataType - int
          'SoftproofingCalibrationTimeoutDays'  -- Description - nvarchar(512)
        )
GO
ALTER TABLE dbo.WorkstationCalibrationData ADD
	CalibrationUploadStatus bit NOT NULL CONSTRAINT DF_WorkstationCalibrationData_CalibrationUploadStatus DEFAULT (1)
GO
ALTER TABLE dbo.Workstation ADD
	WorkstationName nvarchar(128) NULL
GO
ALTER TABLE dbo.WorkstationCalibrationData
	DROP COLUMN WorkstationName
GO
-------------------------------------------------------------------------------
ALTER TABLE dbo.WorkstationCalibrationData ADD
	ProcessingInProgress varchar(36) NULL
GO
ALTER TABLE dbo.WorkstationCalibrationData ADD
	DisplayName nvarchar(128) NOT NULL CONSTRAINT DF_WorkstationCalibrationData_DisplayName DEFAULT ('')
GO
