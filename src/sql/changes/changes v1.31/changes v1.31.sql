USE [GMGCoZone]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

--Changes for displaying all the approvals from an account
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_RecCount int OUTPUT	
)
AS
BEGIN
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;
	
	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
					END DESC
				)) AS ID, 
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				a.[Guid],
				a.[FileName],
				js.[Key] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				at.[Key] AS ApprovalType,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				(SELECT [dbo].[GetApprovalIsCompleteChanges] (a.ID, @P_User)) as IsChangesComplete,
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL((SELECT CASE
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
							THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
													                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
														JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
														WHERE acd.Approval = a.ID
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount],
				(SELECT CASE
							WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
												FROM ApprovalCollaborator ac 
												WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)) ) THEN CONVERT(bit,1)
						    ELSE CONVERT(bit,0)
						END) AS IsCollaborator,
				CONVERT(bit,0) AS AllCollaboratorsDecisionRequired	
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID
				JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
				JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									 @P_ViewAll = 1 
							     OR
							     ( 
							        @P_ViewAll = 0 AND 
							        (
											(@P_TopLinkId = 1 
												AND ( 
													EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
													)
											)
										OR 
											(@P_TopLinkId = 2 
												AND av.[Owner] = @P_User
											)
										OR 
											(@P_TopLinkId = 3 
												AND av.[Owner] != @P_User
												AND EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
											)
									  )
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									 @P_ViewAll = 1 
							     OR
							     ( 
							        @P_ViewAll = 0 AND 
							        (
											(@P_TopLinkId = 1 
												AND ( 
													EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
													)
											)
										OR 
											(@P_TopLinkId = 2 
												AND av.[Owner] = @P_User
											)
										OR 
											(@P_TopLinkId = 3 
												AND av.[Owner] != @P_User
												AND EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
											)
									  )
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
END
GO

CREATE TABLE [dbo].[AccountNotificationPreset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL
 CONSTRAINT [PK_AccountNotificationPreset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AccountNotificationPreset]  WITH CHECK ADD  CONSTRAINT [FK_AccountNotificationPreset_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountNotificationPreset] CHECK CONSTRAINT [FK_AccountNotificationPreset_Account]
GO

-- create many to many table between notification preset and event type
CREATE TABLE [dbo].[AccountNotificationPresetEventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationPreset] [int] NOT NULL,
	[EventType] [int] NOT NULL
 CONSTRAINT [PK_AccountNotificationPresetEventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[AccountNotificationPresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_AccountNotificationPresetEventType_AccountNotificationPreset] FOREIGN KEY([NotificationPreset])
REFERENCES [dbo].[AccountNotificationPreset] ([ID])
GO

ALTER TABLE [dbo].[AccountNotificationPresetEventType] CHECK CONSTRAINT [FK_AccountNotificationPresetEventType_AccountNotificationPreset]
GO

ALTER TABLE [dbo].[AccountNotificationPresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_AccountNotificationPresetEventType_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO

ALTER TABLE [dbo].[AccountNotificationPresetEventType] CHECK CONSTRAINT [FK_AccountNotificationPresetEventType_EventType]
GO

--Create static tables needed for notification schedule
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'NotificationPresetFrequency' )
   )
	BEGIN
		CREATE TABLE [dbo].[NotificationPresetFrequency]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Key] [int] NOT NULL,
				[Name] [nvarchar](32) NOT NULL,
				CONSTRAINT [PK_NotificationPresetFrequency] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
						ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table NotificationPresetFrequency already exists'
GO

INSERT [dbo].[NotificationPresetFrequency]
	([KEY], [Name])
VALUES
	(0, 'As Event Occur'),
	(1, 'Every Hour'),
	(2, 'Every 2 Hour'),
	(3, 'Every 4 Hour'),
	(4, 'Every Day'),
	(5, 'Every Week')
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'NotificationPresetCollaborateUnit' )
   )
	BEGIN
		CREATE TABLE [dbo].[NotificationPresetCollaborateUnit]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Key] [int] NOT NULL,
				[Name] [nvarchar](32) NOT NULL,
				CONSTRAINT [PK_NotificationPresetCollaborateUnit] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
						ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table NotificationPresetCollaborateUnit already exists'
GO

INSERT [dbo].[NotificationPresetCollaborateUnit]
	([KEY], [Name])
VALUES
	(0, 'Hours'),
	(1, 'Days'),
	(2, 'Weeks')	
GO

IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'NotificationPresetCollaborateTrigger' )
   )
	BEGIN
		CREATE TABLE [dbo].[NotificationPresetCollaborateTrigger]
			(
				[ID] [int] IDENTITY(1,1) NOT NULL,
				[Key] [int] NOT NULL,
				[Name] [nvarchar](32) NOT NULL,
				CONSTRAINT [PK_NotificationPresetCollaborateTrigger] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
						ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			)
		ON [PRIMARY]
	END
ELSE
	PRINT 'Table NotificationPresetCollaborateTrigger already exists'
GO

INSERT [dbo].[NotificationPresetCollaborateTrigger]
	([KEY], [Name])
VALUES
	(0, 'After Start'),
	(1, 'Before End')	
GO

-- Create Notification Schedules Table
CREATE TABLE [dbo].[NotificationSchedule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationPreset] [int] NOT NULL,
	[NotificatonFrequency] [int] NOT NULL,
	[NotificationCollaborateUnit] [int] NULL,
	[NotificationCollaborateTrigger] [int] NULL,
	[NotificationCollaborateUnitNumber] [int] NULL,
 CONSTRAINT [PK_NotificationSchedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_AccountNotificationPreset] FOREIGN KEY([NotificationPreset])
REFERENCES [dbo].[AccountNotificationPreset] ([ID])
GO

ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_AccountNotificationPreset]
GO

ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_NotificationFrequency] FOREIGN KEY([NotificatonFrequency])
REFERENCES [dbo].[NotificationPresetFrequency] ([ID])
GO

ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_NotificationFrequency]
GO

ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateUnit] FOREIGN KEY([NotificationCollaborateUnit])
REFERENCES [dbo].[NotificationPresetCollaborateUnit] ([ID])
GO

ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateUnit]
GO

ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateTrigger] FOREIGN KEY([NotificationCollaborateTrigger])
REFERENCES [dbo].[NotificationPresetCollaborateTrigger] ([ID])
GO

ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateTrigger]
GO

--Create Many to many table between notification schedule and users
CREATE TABLE [dbo].[NotificationScheduleUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationSchedule] [int] NOT NULL,
	[User] [int] NOT NULL
 CONSTRAINT [PK_NotificationScheduleUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[NotificationScheduleUser]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUser_NotificationSchedule] FOREIGN KEY([NotificationSchedule])
REFERENCES [dbo].[NotificationSchedule] ([ID])
GO

ALTER TABLE [dbo].[NotificationScheduleUser] CHECK CONSTRAINT [FK_NotificationScheduleUser_NotificationSchedule]
GO

ALTER TABLE [dbo].[NotificationScheduleUser]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUser_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[NotificationScheduleUser] CHECK CONSTRAINT [FK_NotificationScheduleUser_User]
GO

--Create Many to many table between notification schedule and user groups
CREATE TABLE [dbo].[NotificationScheduleUserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationSchedule] [int] NOT NULL,
	[UserGroup] [int] NOT NULL
 CONSTRAINT [PK_NotificationScheduleUserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[NotificationScheduleUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserGroup_NotificationSchedule] FOREIGN KEY([NotificationSchedule])
REFERENCES [dbo].[NotificationSchedule] ([ID])
GO

ALTER TABLE [dbo].[NotificationScheduleUserGroup] CHECK CONSTRAINT [FK_NotificationScheduleUserGroup_NotificationSchedule]
GO

ALTER TABLE [dbo].[NotificationScheduleUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserGroup_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO

ALTER TABLE [dbo].[NotificationScheduleUserGroup] CHECK CONSTRAINT [FK_NotificationScheduleUserGroup_UserGroup]
GO


--Add sub menu item  RegionalSettings to 
DECLARE @controllerId INT

SELECT @controllerId = ControllerAction
FROM dbo.MenuItem
WHERE [Key] = 'SESS'

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SSRS' , -- Key - nvarchar(4)
          'Regional Settings' , -- Name - nvarchar(64)
          'Regional Settings' -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SESS'
GO

--Add Submenu Item - Notifications
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'GlobalNotifications', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title		
        )
SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          2 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SSN' , -- Key - nvarchar(4)
          'Notifications' , -- Name - nvarchar(64)
          'Notifications'  -- Title - nvarchar(128)		
        FROM dbo.MenuItem mi
		where [Key] = 'SESS'		
		
UPDATE 	dbo.MenuItem
SET ControllerAction = NULL
WHERE [KEY] = 'SESS'

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SSRS','SSN')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE R.[Key] IN('ADM','HQA','HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (0,1,2,3,4,5)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
		DELETE FROM dbo.MenuItemAccountTypeRole
		WHERE MenuItem IN(
						SELECT  MI.ID
						FROM    dbo.MenuItem MI
						WHERE   MI.[Key] IN ('SESS')
						)
						AND AccountTypeRole = @atrId
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

--Add Submenu Item - Add Edit Notification Preset
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditNotificationPreset', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AENP' , -- Key - nvarchar(4)
          'Add Edit Notification Preset' , -- Name - nvarchar(64)
          'Add Edit Notification Preset'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SSN'
GO

--Add Submenu Item - Add Edit Notification Schedule
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditNotificationSchedule', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AENS' , -- Key - nvarchar(4)
          'Add Edit Notification Schedule' , -- Name - nvarchar(64)
          'Add Edit Notification Schedule'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SSN'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('AENP','AENS')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO



ALTER TABLE dbo.ApprovalAnnotationAttachment ADD
	Guid nvarchar(36) NULL

ALTER TABLE dbo.ApprovalAnnotationAttachment
	DROP CONSTRAINT FK_ApprovalAnnotationAttachment_FileType
	
ALTER TABLE dbo.ApprovalAnnotationAttachment
	DROP COLUMN FileType

------------------- Add "Regional settings" submenu item for sysadmin --------
DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'HQA' OR R.[KEY] = 'HQM' ) AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SSRS')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

----------- Add "Regional settings" submenu item for child account-----------------
DECLARE @controllerId INT

SELECT @controllerId = ControllerAction
FROM dbo.MenuItem
WHERE [Key] = 'ACSS'

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SSRS' , -- Key - nvarchar(4)
          'Regional Settings' , -- Name - nvarchar(64)
          'Regional Settings' -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'ACSS'
GO
		
UPDATE 	dbo.MenuItem
SET ControllerAction = NULL
WHERE [KEY] = 'ACSS'

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM' OR R.[Key] = 'HQA' OR R.[KEY] = 'HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (3,4)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SSRS')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE R.[Key] IN('ADM','HQA','HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (0,1,2,3,4,5)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
		DELETE FROM dbo.MenuItemAccountTypeRole
		WHERE MenuItem IN(
						SELECT  MI.ID
						FROM    dbo.MenuItem MI
						WHERE   MI.[Key] IN ('ACSS')
						)
						AND AccountTypeRole = @atrId
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

-- create user-eventtype log schedule with last sent date
CREATE TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[LastSentDate] [DateTime] NOT NULL
 CONSTRAINT [PK_NotificationScheduleUserEventTypeEmailLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog] CHECK CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_User]
GO

ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO

ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog] CHECK CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_EventType]
GO


-- create user-approval reminder log schedule with last sent date
CREATE TABLE [dbo].[UserApprovalReminderEmailLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Approval] [int] NOT NULL,
	[LastSentDate] [DateTime] NOT NULL
 CONSTRAINT [PK_UserApprovalReminderEmailLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserApprovalReminderEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_UserApprovalReminderEmailLog_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[UserApprovalReminderEmailLog] CHECK CONSTRAINT [FK_UserApprovalReminderEmailLog_User]
GO

ALTER TABLE [dbo].[UserApprovalReminderEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_UserApprovalReminderEmailLog_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO

ALTER TABLE [dbo].[UserApprovalReminderEmailLog] CHECK CONSTRAINT [FK_UserApprovalReminderEmailLog_Approval]
GO

--modify delete approval procedure to delete user approval reminder log fk
ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		 --Delete annotations from ApprovalAnnotationStatusChangeLog
		DELETE [dbo].[ApprovalAnnotationStatusChangeLog]		 
		FROM   [dbo].[ApprovalAnnotationStatusChangeLog] aascl
				JOIN [dbo].[ApprovalAnnotation] aa
					ON aascl.Annotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id	    
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalDeleteHistory
		DELETE FROM [dbo].[ApprovalDeleteHistory]
		WHERE Approval = @P_Id
		
		-- Delete from UserApprovalReminderEmailLog
		DELETE FROM [dbo].[UserApprovalReminderEmailLog]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO

-- add user with role HQM notifications for account was added and account was deleted
  insert  into dbo.RolePresetEventType
 values(2, 3, 13)
 
   insert  into dbo.RolePresetEventType
 values(2, 3, 14)
 
   insert  into dbo.RolePresetEventType
 values(2, 4, 13)
 
   insert  into dbo.RolePresetEventType
 values(2, 4, 14)
 
    insert  into dbo.RolePresetEventType
 values(2, 1, 14)
 GO
 

--Changes for displaying DELETE version for administrators when view all colaborates is pressed
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_RecCount int OUTPUT	
)
AS
BEGIN
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;
	
	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
					END DESC
				)) AS ID, 
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				a.[Guid],
				a.[FileName],
				js.[Key] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				at.[Key] AS ApprovalType,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				(SELECT [dbo].[GetApprovalIsCompleteChanges] (a.ID, @P_User)) as IsChangesComplete,
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL((SELECT CASE
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
							THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
													                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
														JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
														WHERE acd.Approval = a.ID
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount],
				(SELECT CASE
							WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
												FROM ApprovalCollaborator ac 
												WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)) ) THEN CONVERT(bit,1)
						    ELSE CONVERT(bit,0)
						END) AS IsCollaborator,
				CONVERT(bit,0) AS AllCollaboratorsDecisionRequired	
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID
				JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
				JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									 @P_ViewAll = 1 
							     OR
							     ( 
							        @P_ViewAll = 0 AND 
							        (
											(@P_TopLinkId = 1 
												AND ( 
													EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
													)
											)
										OR 
											(@P_TopLinkId = 2 
												AND av.[Owner] = @P_User
											)
										OR 
											(@P_TopLinkId = 3 
												AND av.[Owner] != @P_User
												AND EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
											)
									  )
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									 @P_ViewAll = 1 
							     OR
							     ( 
							        @P_ViewAll = 0 AND 
							        (
											(@P_TopLinkId = 1 
												AND ( 
													EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
													)
											)
										OR 
											(@P_TopLinkId = 2 
												AND av.[Owner] = @P_User
											)
										OR 
											(@P_TopLinkId = 3 
												AND av.[Owner] != @P_User
												AND EXISTS (
																SELECT TOP 1 ac.ID 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
															)
											)
									  )
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
END
GO
 
--- add AccountPath  for User Reports ------------------- 

ALTER PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
		WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
	   -- Get the users				
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin,
		    (SELECT PathString) AS AccountPath	
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID		
		    INNER JOIN AccountTree act 
				ON a.ID = act.AccountID																						
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ((u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END
GO


ALTER VIEW [dbo].[ReturnUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Name,
			'' AS StatusName,
			'' AS UserName,
			'' AS EmailAddress,
			GETDATE() AS DateLastLogin,
			'' AS AccountPath
GO

------------------------------------------------------------------------------------------------------ Launched on Debug
------------------------------------------------------------------------------------------------------ Launched on ReleaseEU