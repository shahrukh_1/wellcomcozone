USE [GMGCoZone]
GO
DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM' OR R.[Key] = 'HQA' OR R.[KEY] = 'HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (3)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SCGL') and MI.[Parent] IN (SELECT ID FROM dbo.MenuItem WHERE [Key] = 'ACCU')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

-------------------------------------------------------
ALTER TABLE dbo.AccountNotificationPreset
ADD DisablePersonalNotifications bit NOT NULL DEFAULT(0)
GO

ALTER TABLE [dbo].[BrandingPreset]
ADD EmailLogoPath nvarchar(256)
GO

------------------------committed on Debug and Release-------------------