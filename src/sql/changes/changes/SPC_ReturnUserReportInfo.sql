USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserReportInfo]    Script Date: 17/01/2022 11:04:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- CZ-2970 Extend User Report
--- add AccountPath  for User Reports ------------------- 

ALTER PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
	
	@P_AccountIDList nvarchar(MAX)='',
	@P_userGroupList nvarchar(MAX)='',
	@P_LoggedAccount int,
	@P_StartDate datetime,
	@P_EndDate datetime
)
AS
BEGIN


WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
	   -- Get the users
	   SELECT * FROM (				
		SELECT	DISTINCT
			u.[ID],
			(SELECT PathString) AS Account,
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,	
			u.[Username],
			u.[EmailAddress],	
		(SELECT count(1) FROM Approval ap
		INNER JOIN ApprovalCollaborator ac ON ap.ID = ac.Approval
		WHERE ac.Approval = ap.ID and ac.Collaborator = u.ID) as [NumberOfApprovalsIn],

		(SELECT COUNT(app.ID) FROM [dbo].Approval AS app WHERE app.[Creator] = u.[ID]) AS [NumberofUploads], 

		ISNULL((select top 1 ul.DateLogin from [UserLogin] ul where [user]  = u.ID order by ul.DateLogin desc) , '1900/01/01') AS DateLastLogin,
		ISNULL(STUFF((
			SELECT ',' +ug.Name FROM UserGroup ug
			INNER JOIN UserGroupUser ugu ON ugu.UserGroup = ug.ID
			WHERE ugu.[User] = u.ID AND ug.Account = a.ID
			FOR XML PATH('')
		),1,1,''),'') as GroupMembership

		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID		
		    INNER JOIN AccountTree act 
				ON a.ID = act.AccountID																						
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND ((u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		AND U.DateLastLogin between @p_StartDate and @p_EndDate
		)TEMP  
		WHERE ((1=1 AND LEN(@P_userGroupList) = 0) OR
		((TEMP.ID in( SELECT DISTINCT ugu.[User] from UserGroupUser ugu where ugu.UserGroup in(
		Select val FROM dbo.splitString(@P_userGroupList, ',')
		)))AND LEN(@P_userGroupList) > 0))
END
	
	





GO


