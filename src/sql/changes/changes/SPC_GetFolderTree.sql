USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 30/12/2021 09:55:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- SPC_GetFolderTree_1 8766,1

ALTER PROCEDURE [dbo].[SPC_GetFolderTree] (
	@P_User int,
	@P_ViewAll bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

--	Declare @P_user int = 8766
--	Declare @P_ViewAll bit =1

	DECLARE @LoggedAccountID INT;

	SET @LoggedAccountID = (SELECT ac.ID
							FROM [dbo].[User] us
							LEFT JOIN  [dbo].[Account] ac on us.[Account] = ac.ID
							WHERE us.ID = @P_User)

	IF OBJECT_ID('tempdb..#FolderCollaborator') IS NOT NULL DROP TABLE #FolderCollaborator
	IF OBJECT_ID('tempdb..#FolderApproval') IS NOT NULL DROP TABLE #FolderApproval
	IF OBJECT_ID('tempdb..#Folder') IS NOT NULL DROP TABLE #Folder

	--IF OBJECT_ID('tempdb..#CollaboratorGroup') IS NOT NULL DROP TABLE #CollaboratorGroup
	--IF OBJECT_ID('tempdb..#CollaboratorList') IS NOT NULL DROP TABLE #CollaboratorList


	Select * into #FolderCollaborator from [dbo].FolderCollaborator where collaborator=@P_User

	Select * into #Folder from [dbo].Folder where Account=@LoggedAccountID and isDeleted=0;


	CREATE NONCLUSTERED	INDEX ix_tempFolder ON #Folder (ID);

	Create Table #FolderApproval
	(
		Folder int,
		Approval bigint,
	)
	CREATE INDEX IDX_NCFolder ON #FolderApproval(Folder)
	Insert Into #FolderApproval
	Select fa.* from FolderApproval fa, #Folder f where fa.Folder=f.id;
	--	Print convert(varchar,getdate(),109);
-- Inline function
  


--  SELECT FL.ID, CollaboratorGroup = 
--    STUFF((SELECT ', ' + convert(varchar(10),FCG.CollaboratorGroup)
--           FROM FolderCollaboratorGroup FCG
--           WHERE FCG.Folder = Fl.ID 
--         FOR XML PATH('')), 1, 2, '') into #CollaboratorGroup
--FROM #Folder Fl
--GROUP BY Fl.ID



--Print convert(varchar,getdate(),109);

--SELECT FL.ID, CollaboratorList = 
--    STUFF((SELECT ', ' + convert(varchar(10),FC.Collaborator)
--		   From #Folder Fld
--		   Left Join 
--           FolderCollaborator FC
--		   On Fld.ID=FC.Folder
--		   Inner Join dbo.[User] us
--		   On FC.Collaborator = us.ID
--		   Inner Join dbo.UserStatus ust
--		   ON	us.status=ust.id 
--		   WHERE FC.Folder = Fl.ID and ust.[Key] != 'D'
--         FOR XML PATH('')), 1, 2, '')  into #CollaboratorList
--FROM #Folder Fl 
--Group by Fl.ID;


--Print convert(varchar,getdate(),109);

	WITH tableSet AS 
		(
			SELECT f.ID,
				   f.Parent,
				   f.Name,
				   f.Creator,
				   f.AllCollaboratorsDecisionRequired,
				   (0) AS Level,
				    (CASE WHEN (exists(	SELECT	fc.ID 
										FROM	[dbo].#FolderCollaborator fc
										WHERE	fc.Folder = f.ID 
						               )
						        )
						  THEN  '1'
						  ELSE '0'
						  end)  AS HasAccess,
				  (CASE WHEN (exists(	SELECT	fa.Folder 
								FROM	#FolderApproval fa
								WHERE	fa.Folder = f.ID
				               )
				        ) 
				  THEN  CONVERT(bit, 1)
				  ELSE CONVERT(bit, 0)
				  end)  AS HasApprovals  
			FROM	#Folder f
			
			UNION ALL
			
			SELECT sf.ID,
				   sf.Parent,
				   sf.Name,
				   sf.Creator,
				   sf.AllCollaboratorsDecisionRequired,
				   (CASE WHEN (EXISTS (SELECT fc.ID 
										FROM	#FolderCollaborator fc 
										WHERE	fc.Folder = sf.ID)
								)
						 THEN  (h.[Level] + 1)
						 ELSE (0)
						 END
				   )  AS [Level],
				  (CASE WHEN (EXISTS (	SELECT fc.ID 
										FROM	#FolderCollaborator fc 
										WHERE	fc.Folder = sf.ID)
							  )
						THEN  '1'
						ELSE '0'
						END
				  )  AS HasAccess,
				  (CASE WHEN (exists(	SELECT	fa.Folder
								FROM	#FolderApproval fa
								WHERE	fa.Folder = sf.ID
				               )
				        ) 
						THEN  CONVERT(bit, 1)
						ELSE CONVERT(bit, 0)
						end
				  )  AS HasApprovals  
			FROM #Folder sf
			inner JOIN tableSet h
			ON h.ID	 = sf.Parent		 
		)		
		SELECT ts.ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level], AllCollaboratorsDecisionRequired,  
		MAX(ts.HasAccess)  As Collaborators,  HasApprovals
		FROM tableSet ts 
		WHERE ts.HasAccess = 1 OR @P_ViewAll = 1
		GROUP BY ts.ID, Parent, Name, Creator, AllCollaboratorsDecisionRequired, HasApprovals

		
		--SELECT ts.ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level], AllCollaboratorsDecisionRequired, isnull(CGL.CollaboratorGroup,'') + '#' + isnull(CL.CollaboratorList,'') 'Collaborators',  HasApprovals
		--FROM tableSet ts 
		--Inner Join #CollaboratorGroup CGL on ts.id=CGL.id 
		--Inner Join #CollaboratorList CL on ts.id=CL.ID
		--WHERE HasAccess = 1 OR @P_ViewAll = 1
		--GROUP BY ts.ID, Parent, Name, Creator, AllCollaboratorsDecisionRequired, HasApprovals, CGL.CollaboratorGroup,Cl.CollaboratorList

END




GO


