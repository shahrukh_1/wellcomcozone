USE [GMGCoZone]
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 9/3/2021 8:09:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_ViewAllFromRecycle bit = 0,
	@P_UserRoleKey NVARCHAR(4) = '',
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL
)
AS
BEGIN
	 --Get the approval counts	
	SET NOCOUNT ON


	DECLARE @ApprovalTemp TABLE  
 (  
    ID INT,  
    JobOwner INT,
	[Owner] INT,  
    CurrentPhase INT  ,
	[Key] nvarchar(10),
	Collaborator INT,
	Phase INT,
	Deadline datetime2(7),
	IsLocked bit
 )  

  	DECLARE @ApprovalCollTemp TABLE  
 (  
    Approval INT,  
	Collaborator INT,
	Phase INT
 )

 IF @P_ViewAll = 0
 BEGIN
	INSERT INTO @ApprovalCollTemp
	SELECT ac.Approval,ac.Collaborator,ac.Phase from ApprovalCollaborator ac where  ac.Collaborator = @P_User
END

  IF @P_ViewAll = 0
 BEGIN
	INSERT INTO @ApprovalTemp
    SELECT	DISTINCT(approval.ID), ISNULL(approval.JobOwner,0)JobOwner,approval.[Owner] ,ISNULL(approval.CurrentPhase,0)CurrentPhase,approval.[Key], ac.Collaborator,ac.Phase, approval.Deadline, approval.IsLocked
		   FROM 
		   (SELECT DISTINCT a.ID,rank() over (partition by a.Job order by a.[Version] desc) rnk,j.JobOwner,a.[Owner],a.CurrentPhase, js.[Key], a.Deadline, a.IsLocked
			  FROM Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]	
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
			) approval
			inner JOIN @ApprovalCollTemp ac
					ON ac.Approval = approval.ID	
			where rnk = 1
END
ELSE
BEGIN

	INSERT INTO @ApprovalTemp
    SELECT	DISTINCT(approval.ID), ISNULL(approval.JobOwner,0)JobOwner,approval.[Owner] ,ISNULL(approval.CurrentPhase,0)CurrentPhase,approval.[Key], ac.Collaborator,ac.Phase, approval.Deadline, approval.IsLocked
		   FROM 
		   (SELECT DISTINCT a.ID,rank() over (partition by a.Job order by a.[Version] desc) rnk,j.JobOwner,a.[Owner],a.CurrentPhase, js.[Key], a.Deadline, a.IsLocked
			  FROM Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]	
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
			) approval
			inner JOIN ApprovalCollaborator ac
					ON ac.Approval = approval.ID	
			where rnk = 1
END

		SELECT 
		(SELECT	COUNT(DISTINCT(tmp.ID))
		   FROM  @ApprovalTemp tmp
		    where tmp.[Key] != 'ARC' AND tmp.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					 AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (tmp.[Key] = 'CRQ' OR tmp.[Key] = 'CCM'))
					)	

			AND (
							   @P_ViewAll = 1 
								 OR
								 (
								   @P_ViewAll = 0 
								    	
										
									AND ( ((
									
									@P_User = ISNULL(tmp.JobOwner, 0) OR 
									
									(tmp.Collaborator = @P_User 
									
									AND ISNULL(tmp.Phase, 0) = ISNULL(tmp.CurrentPhase, 0)
									
									))))


								  )
							 )
		) AS AllCount,
		 
		--(SELECT					
		--		COUNT(DISTINCT(tmp.ID))
		--   FROM   @ApprovalTemp tmp
		--    where tmp.[Key] != 'ARC' AND tmp.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
		--			 AND (
		--				@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (tmp.[Key] = 'CRQ' OR tmp.[Key] = 'CCM'))
		--			)	

		
		--	AND ( (( (tmp.Collaborator IN (
		--	SELECT ou.[owner] from OutOfOffice ou where ou.substituteuser = @P_User
		--	AND CONVERT(varchar, ou.StartDate, 111) <= CONVERT(varchar, GETDATE(), 111)
		--	AND CONVERT(varchar, ou.EndDate, 111) >= CONVERT(varchar, GETDATE(), 111)
		--	)
		--	 AND ISNULL(tmp.Phase, 0) = ISNULL(tmp.CurrentPhase, 0))) -- ))
		--						  )
		--					 )
		--) AS SubstitutedApprovalCount

		(SELECT					
				COUNT(DISTINCT(approval.ID))
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk,j.JobOwner,a.CurrentPhase
			
			  FROM Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]	
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					 AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)	

			) approval
			JOIN ApprovalCollaborator ac WITH (NOLOCK)
					ON ac.Approval = approval.ID	
			where rnk = 1
			AND ( (( (ac.Collaborator IN (
			SELECT ou.[owner] from OutOfOffice ou where ou.substituteuser = @P_User
			AND CONVERT(varchar, ou.StartDate, 111) <= CONVERT(varchar, GETDATE(), 111)
			AND CONVERT(varchar, ou.EndDate, 111) >= CONVERT(varchar, GETDATE(), 111)
			)
			 AND ISNULL(ac.Phase, 0) = ISNULL(approval.CurrentPhase, 0))) -- ))
								  )
							 )
		) AS SubstitutedApprovalCount,
		
		( SELECT
			 COUNT(approval.ID)
		   FROM 
		   (SELECT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM	
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]							
			 WHERE j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					AND (ISNULL(j.JobOwner, 0) = @P_User OR a.[Owner] = @P_User)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)	
			) approval
		  where rnk = 1	
		) AS OwnedByMeCount
		
		,		
		(SELECT					
				COUNT(DISTINCT(tmp.ID))
		     FROM   @ApprovalTemp tmp
					where tmp.[Key] != 'ARC' AND tmp.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
						AND ( ISNULL(tmp.JobOwner, 0) != @P_User AND tmp.[Owner] != @P_User)
						AND (
								@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (tmp.[Key] = 'CRQ' OR tmp.[Key] = 'CCM'))
							)
			 AND ( ((
			 
			 @P_User = ISNULL(tmp.JobOwner, 0) OR 
			 
			 (tmp.Collaborator = @P_User 
			 
			 AND ISNULL(tmp.Phase, 0) = ISNULL(tmp.CurrentPhase, 0)
			 
			 ))))	
		) AS SharedCount
		
		,

	(SELECT					
				COUNT(rece.ID)
		   FROM 
		(SELECT DISTINCT auvi.ID, rank() over (partition by auvi.Approval order by auvi.ViewedDate desc) rnk, auvi.Approval,auvi.[User]

			  FROM ApprovalUserViewInfo auvi
			  inner join 

	 		(
			
			SELECT	DISTINCT(tmp.ID)
		     FROM   @ApprovalTemp tmp
					where tmp.[Key] != 'ARC'
			
				AND (
						@P_ViewAll = 1 
							OR
							(
							@P_ViewAll = 0 
								    											   
										 AND (
									   (
									   
									   @P_User = ISNULL(tmp.JobOwner, 0) OR 
									   
									   (tmp.Collaborator = @P_User AND ISNULL(tmp.Phase, 0) = ISNULL(tmp.CurrentPhase, 0)
			                   						  )
												  )
											 )
							
							)
						)



		
		) approv

		on auvi.Approval = approv.ID
		where auvi.[User] = @P_User
		)rece
		) AS RecentlyViewedCount


	,

	------------------------------------> ArchivedCount 10secs
		(SELECT					
				COUNT(DISTINCT(tmp.ID))
		     FROM   @ApprovalTemp tmp
					where tmp.[Key] = 'ARC'
			
				AND (
						@P_ViewAll = 1 
							OR
							(
							@P_ViewAll = 0 
								    											   
										 AND (
									   (
									   
									   @P_User = ISNULL(tmp.JobOwner, 0) OR 
									   
									   (tmp.Collaborator = @P_User AND ISNULL(tmp.Phase, 0) = ISNULL(tmp.CurrentPhase, 0)
			                   						  )
												  )
											 )
							
							)
						)


		 ) AS ArchivedCount
		 ,

		(SELECT 
			    SUM(recycle.Approvals) AS RecycleCount
			FROM (
			          SELECT COUNT(DISTINCT a.ID) AS Approvals
						FROM	Job j WITH (NOLOCK)
								INNER JOIN Approval a WITH (NOLOCK)
									ON a.Job = j.ID
								INNER JOIN JobStatus js WITH (NOLOCK)
									ON js.ID = j.[Status]
								INNER JOIN ApprovalCollaborator ac WITH (NOLOCK)
									ON ac.Approval = a.ID		
						WHERE j.Account = @P_Account
							AND a.IsDeleted = 1
							AND a.DeletePermanently = 0
							AND ((SELECT COUNT(f.ID) FROM Folder f WITH (NOLOCK) INNER JOIN FolderApproval fa WITH (NOLOCK) ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
							AND (
							      ISNULL((SELECT TOP 1 ac.ID
										FROM ApprovalCollaborator ac WITH (NOLOCK)
										WHERE ac.Approval = a.ID 
										
										 
										 
										 AND (
							   @P_ViewAll = 1 
								 OR
								 (
								   @P_ViewAll = 0 
								    											   
									AND (ac.Collaborator = @P_User)
								  )
							 )
										 
										 )
										, 
										(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph WITH (NOLOCK)
										WHERE aph.Approval = a.ID 
										
										 AND (
							   @P_ViewAll = 1 
								 OR
								 (
								   @P_ViewAll = 0 
								    											   
									AND (aph.[User] = @P_User)
								  )))) IS NOT NULL)
						
						AND (
							   @P_ViewAll = 1 
								 OR
								 (@P_ViewAll = 0 							   
									AND (@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User 
									AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)))))
							
					UNION
						SELECT COUNT(f.ID) AS Approvals
						FROM	Folder f WITH (NOLOCK)
						WHERE	f.Account = @P_Account
								AND f.IsDeleted = 1
								AND ((SELECT COUNT(pf.ID) FROM Folder pf WITH (NOLOCK) WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
								AND f.Creator = @P_User								
					
				)recycle
		) AS RecycleCount,
					
		(SELECT	
		
		COUNT(DISTINCT(approval.ID))
		   FROM (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk,j.JobOwner,a.CurrentPhase
			
			  FROM Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]	
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0 
					AND a.DeletePermanently = 0 
					AND a.IsLocked = 0
					AND js.[Key] != 'ARC'
					AND (js.[Key] != 'COM')

			) approval
			JOIN ApprovalCollaboratorDecision ac WITH (NOLOCK)
					ON ac.Approval = approval.ID	
			where rnk = 1
			 AND  ac.Collaborator = @P_User
			 
			  AND ISNULL(ac.Phase, 0) = ISNULL(approval.CurrentPhase, 0) AND ac.Decision IS NULL 
		) AS MyOpenApprovalsCount,

		0 AS AdvanceSearchCount,
				 
		(SELECT	COUNT(DISTINCT(tmp.ID))
		   FROM  @ApprovalTemp tmp
		    where tmp.[Key] != 'ARC' AND tmp.[Key] != 'COM' AND tmp.IsLocked = 0
					AND (tmp.Deadline < GETDATE() and DATEDIFF(year,tmp.Deadline,GETDATE()) <= 100) 

			AND (
							   @P_ViewAll = 1 
								 OR
								 (
								   @P_ViewAll = 0 
								    	
										
									AND ( ((
									
									@P_User = ISNULL(tmp.JobOwner, 0) OR 
									
									(tmp.Collaborator = @P_User 
									
									AND ISNULL(tmp.Phase, 0) = ISNULL(tmp.CurrentPhase, 0)
									
									))))


								  )
							 )
		) AS OverdueApprovalsCount


	--OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))

END












