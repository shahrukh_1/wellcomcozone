

USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFileReportInfo]    Script Date: 20/08/2020 11:19:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SPC_ReturnFileReportInfo] (
	@P_Account nvarchar(MAX)='',
	@P_userGroupList nvarchar(MAX)='',
	@P_StartDate datetime,
	@P_EndDate datetime,
	@P_ReportType int
)
AS
BEGIN
	
	
		DECLARE @TempItems TABLE
		(
		   ID int IDENTITY PRIMARY KEY,
		   ApprovalID int,
		   PhaseName nvarchar(150),
		   PrimaryDecisionMakerName nvarchar(150)
		)
			
		DECLARE @TempOnlyIDs TABLE
		(
		   ApprovalID int
		)

		if @P_ReportType = 3
		BEGIN
			INSERT INTO @TempOnlyIDs(ApprovalID)
			select id FROM
			(
				select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
				FROM approval a
				INNER JOIN job j on j.id=a.job
				INNER JOIN ApprovalCollaborator ac
				on ac.Approval = a.ID
				where j.account IN (Select val FROM dbo.splitString(@P_Account, ','))
				AND
				CONVERT(varchar, a.ModifiedDate, 111) between @p_StartDate and @p_EndDate
				AND ((1=1 AND LEN(@P_userGroupList) = 0) OR
				((ac.Collaborator in( SELECT DISTINCT ugu.[User] from UserGroupUser ugu where ugu.UserGroup in(
				Select val FROM dbo.splitString(@P_userGroupList, ',')
				)))AND LEN(@P_userGroupList) > 0))

			) x 
			WHERE x.r = 1
		END
		ELSE
		BEGIN
		INSERT INTO @TempOnlyIDs(ApprovalID)
			select id FROM
			(
				select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
				FROM approval a
				INNER JOIN job j on j.id=a.job
				INNER JOIN ApprovalCollaborator ac
				on ac.Approval = a.ID
				where j.account IN (Select val FROM dbo.splitString(@P_Account, ','))
				AND CONVERT(varchar, a.CreatedDate, 111) between @p_StartDate and @p_EndDate
				AND ((1=1 AND LEN(@P_userGroupList) = 0) OR
				((ac.Collaborator in( SELECT DISTINCT ugu.[User] from UserGroupUser ugu where ugu.UserGroup in(
				Select val FROM dbo.splitString(@P_userGroupList, ',')
				)))AND LEN(@P_userGroupList) > 0))

			) x 
			WHERE x.r = 1
		END


			INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
			SELECT a.id, CP.PhaseName,PDMN.PrimaryDecisionMakerName
			from @TempOnlyIDs t 
			inner join approval a on a.id=t.ApprovalID
			inner join job j on j.id=a.job
			inner join jobstatus js on js.id=j.status
				CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
											THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
											ELSE ''								
							 END) CP (PhaseName)	
				CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
											THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
										 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
											THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
										 ELSE ''
									END) PDMN (PrimaryDecisionMakerName)		
			WHERE 

			(@p_ReportType = 0 and a.DeletePermanently = 0) OR
			(@p_ReportType = 1 AND js.[Key] != 'ARC' and a.IsDeleted = 0 and a.DeletePermanently = 0 ) OR
			(@p_ReportType = 2 AND js.[Key] = 'ARC' and a.IsDeleted = 0 and a.DeletePermanently = 0 ) OR
			(@p_ReportType = 3 AND a.IsDeleted != 0 ) OR
			(@p_ReportType = 4 AND js.[Key] != 'ARC' and a.IsDeleted = 0 and a.DeletePermanently = 0 and CONVERT(varchar, a.Deadline, 111) < CONVERT(varchar, GETDATE(), 111) 
			 and a.Deadline != '0001-01-01 00:00:00.0000000'
			)


		SELECT
		(Select ac.Name from Account ac WHERE ac.ID = j.Account) as AccountName, a.ID,
				a.[FileName],
				(SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.owner) as OwnerName,

				ISNULL(STUFF((
				SELECT ',' +ug.Name FROM UserGroup ug
				INNER JOIN UserGroupUser ugu ON ugu.UserGroup = ug.ID
				WHERE ugu.[User] = a.[Owner] AND ug.Account = j.Account
				FOR XML PATH('') ),1,1,''),'') as OwnerGroupMembership,

				ISNULL((SELECT Max(ap.Number) FROM ApprovalPage ap where ap.Approval = a.ID group by ap.Approval),0)as V1PageCount,

				ISNULL((SELECT MAX([Version])
				FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0),0)AS TotalVersion,

				a.CreatedDate as UploadDate,
				
				 
					ISNULL((a.Deadline),null) as OverDue,
	
				(	SELECT max(ac.CompletedDate) from Approval ap
					INNER JOIN ApprovalCollaboratorDecision ac on ac.Approval = ap.ID
					INNER JOIN Job j on ap.Job = j.ID
					where a.IsLocked = 1 and j.[Status] in (3, 4) and ap.ID = a.ID
					AND ac.Decision is not null group by ac.Approval
				) AS CompletedDate,
				ISNULL((SELECT DISTINCT COUNT(*)  FROM Approval a1
				INNER JOIN ApprovalCollaborator ac
				ON a1.ID = ac.Approval
				 where a1.Job = a.Job and a1.[Version] = 1),0) as V1Users,

				 ISNULL((SELECT DISTINCT COUNT(*)  FROM ApprovalCollaborator ac
				 where ac.Approval = a.ID),0) as LatestVersionUsers,

				 CASE WHEN j.[Status] = 4 THEN 
				  	ISNULL((
						SELECT u.GivenName + ' ' + u.FamilyName FROM [User] u WHERE u.ID = a.Modifier
					  ),'') 
					 ELSE ''
				  END as Modifier,

				   CASE WHEN j.[Status] = 4 THEN 
				  	ISNULL((j.ModifiedDate),null)
					ELSE null
				  END as ModifiedDate,

				 	ISNULL((
						SELECT top 1 u.GivenName + ' ' + u.FamilyName FROM ApprovalUserRecycleBinHistory auc
						INNER JOIN [User] u ON auc.[User] = u.ID  WHERE auc.Approval = a.ID order by auc.ID desc
					  ),'') DeletedByUser ,

				  CASE WHEN a.IsDeleted = 1 or a.DeletePermanently = 1 THEN 
				  ISNULL((a.ModifiedDate),null)
				  ELSE null
				  END as DeletedDate

		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID
				JOIN @TempItems t ON t.ApprovalID = a.ID
				JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
				JOIN JobStatus js
					ON js.ID = j.[Status]					
		ORDER BY t.ID



END


GO


