USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ApprovalCountsView]    Script Date: 25-Aug-21 11:56:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[ApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount,
			0 AS MyOpenApprovalsCount,
			0 AS AdvanceSearchCount,
			0 AS SubstitutedApprovalCount,
			0 AS OverdueApprovalsCount




GO


