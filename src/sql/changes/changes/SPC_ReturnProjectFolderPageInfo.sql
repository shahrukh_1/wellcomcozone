USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnProjectFolderPageInfo]    Script Date: 15/11/2021 11:35:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[SPC_ReturnProjectFolderPageInfo] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_MaxRows int = 100,
	@P_SearchText nvarchar(100) = '',
	@P_FilterField int = 1, 
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Due Date 
								-- 4   Status
								-- 5 - Project Name
								
	@P_Order bit = 0,
	@P_Status int = 5,			-- 0 All
								-- 1 Not Assigned
								-- 2 Ongoing
								-- 3 Completed
								-- 4 Archived
								-- 5 NotAssigned OR Ongoing (On page load )	

	@P_SelectedTagwordIds nvarchar(max) = ''
	
)
AS
BEGIN
	 --Get the approval counts	
	SET NOCOUNT ON
	Declare @pfStatus nvarchar(5)

	if @P_Status = 5
	BEGIN
		SET @pfStatus = '1,2'
	END
	ELSE
	BEGIN
		SET @pfStatus = @P_Status
	END

	DECLARE @ApprovalTemp TABLE  
 (  
    ID INT, 
	ProjectFolder INT,
    JobOwner INT,
	[Owner] INT,  
	[Key] INT,
	Overdue datetime2,
	IsLocked bit,
	CurrentPhase INT
 )  

	INSERT INTO @ApprovalTemp
    SELECT	DISTINCT(approval.ID), approval.ProjectFolder, ISNULL(approval.JobOwner,0)JobOwner,approval.[Owner],
	approval.[Status], approval.Deadline, approval.IsLocked,approval.CurrentPhase
		   FROM 
		   (SELECT DISTINCT a.ID,rank() over (partition by a.Job order by a.[Version] desc) rnk,j.JobOwner,a.[Owner],
		   a.CurrentPhase, j.[Status], j.ProjectFolder, a.Deadline,a.IsLocked
			  FROM Job j WITH (NOLOCK)
			  INNER JOIN ProjectFolder pf
					ON j.ProjectFolder = pf.ID
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						
			) approval
			WHERE rnk = 1

SELECT * FROM (

SELECT distinct(pf.ID), pf.Name AS Name ,pf.createdDate AS CreatedDate ,
ISNULL(pf.deadline, pf.CreatedDate) AS Deadline,pf.ModifiedDate,
pf.[Status] AS [Status], pf.IsArchived AS  [IsArchived],
(
  SELECT u.GivenName + ' ' + u.FamilyName FROM [User] u WHERE u.ID = pf.Creator
) as ProjectOwner,

(
SELECT count(*) FROM ProjectFolderCollaborator pfc1 WHERE pfc1.ProjectFolder = pf.ID
)as TeamSize,

(
select count(*) from Job j
inner join Approval a
on a.Job = j.ID
 where j.ProjectFolder = pf.ID and a.IsDeleted = 0 and a.DeletePermanently = 0
)as TotalJobs,

(SELECT COUNT(DISTINCT(tmp.ID))
	FROM   @ApprovalTemp tmp
	WHERE tmp.ID NOT IN (
		SELECT ac.Approval FROM ApprovalCollaborator ac 
	) and pf.ID = tmp.ProjectFolder
)as UnAssignedCount,



(SELECT	COUNT(DISTINCT(tmp.ID))
	FROM   @ApprovalTemp tmp
INNER JOIN ApprovalCollaborator acd
on acd.Approval = tmp.ID and tmp.[Key] in  (1,2)
WHERE tmp.ProjectFolder = pf.ID and tmp.IsLocked = 0
)as InProgressCount,


0 as OverDueCount,

(SELECT	COUNT(DISTINCT(tmp.ID))
	FROM   @ApprovalTemp tmp
INNER JOIN ApprovalCollaborator ac
on ac.Approval = tmp.ID
inner join ApprovalCollaboratorDecision acd
on acd.Approval = tmp.ID AND ((acd.Phase = tmp.CurrentPhase OR acd.Phase IS NULL) and ( ISNULL(acd.Decision,0) = 3 OR ISNULL(acd.Decision,0) = 4 ) )
inner join ApprovalDecision ad on acd.Decision = ad.ID
WHERE tmp.ProjectFolder = pf.ID  and tmp.[Key] != 3
 --and tmp.IsLocked = 0  

)
as ApprovedCount,



(SELECT	COUNT(DISTINCT(tmp.ID))
	FROM   @ApprovalTemp tmp
		where tmp.[Key] = 3 and tmp.ProjectFolder = pf.ID 
		-- and tmp.IsLocked = 1
) AS CompletedCount,

(SELECT	COUNT(DISTINCT(tmp.ID))
	FROM   @ApprovalTemp tmp
		where tmp.[Key] = 3
) AS TotalCount


 FROM ProjectFolder pf
INNER JOIN  ProjectFolderCollaborator pfc
ON pf.ID = pfc.ProjectFolder
WHERE pf.Account =@P_Account and (pfc.Collaborator = @P_User or 'ADM' = [dbo].[GetUserRoleByUserIdAndModuleId] (@P_User, 0))

AND ( 
		pf.Name LIKE '%' + @P_SearchText + '%' )
		and (@P_Status = 0  or pf.[Status] In (Select val FROM dbo.splitString(@pfStatus, ',')))

		) as pf

-- ADD Status filter here		

ORDER BY 
		CASE
			WHEN (@P_FilterField = 1 AND @P_Order = 0) THEN pf.CreatedDate
		END ASC,
		CASE
			WHEN (@P_FilterField = 1 AND @P_Order = 1) THEN pf.CreatedDate
		END DESC,
		CASE
			WHEN (@P_FilterField = 2 AND @P_Order = 0) THEN pf.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_FilterField = 2 AND @P_Order = 1) THEN pf.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_FilterField = 3 AND @P_Order = 0) THEN pf.Deadline
		END ASC,
		CASE
			WHEN (@P_FilterField = 3 AND @P_Order = 1) THEN pf.Deadline
		END DESC,
		CASE
			WHEN (@P_FilterField = 4 AND @P_Order = 0) THEN pf.[Status]
		END ASC,
		CASE
			WHEN (@P_FilterField = 4 AND @P_Order = 1) THEN  pf.[Status]
		END DESC,
			CASE
			WHEN (@P_FilterField = 5 AND @P_Order = 0) THEN pf.Name
		END ASC,
		CASE
			WHEN (@P_FilterField = 5 AND @P_Order = 1) THEN pf.Name
		END DESC
		
END

GO


