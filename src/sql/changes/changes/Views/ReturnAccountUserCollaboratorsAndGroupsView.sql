USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnAccountUserCollaboratorsAndGroupsView]    Script Date: 10-Sep-20 7:29:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO







-----------------------------------------------------------------------------------
-- Add Return Account User Collaborators And Groups procedure
ALTER VIEW [dbo].[ReturnAccountUserCollaboratorsAndGroupsView]
AS
SELECT  CONVERT(BIT, 0) AS IsGroup ,
        CONVERT(BIT, 0) AS IsExternal ,
        CONVERT(BIT, 0) AS IsChecked ,
        0 AS ID ,
        0 AS Count ,
        '' AS Members ,
        '' AS Name ,
        0 AS [Role],
		'' AS RoleName,
		'' AS EmailAddress,
		'' AS PhotoPath





GO


