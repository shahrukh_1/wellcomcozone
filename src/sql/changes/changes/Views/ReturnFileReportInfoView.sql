
USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnFileReportInfoView]    Script Date: 29/05/2020 09:19:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- CZ-2970 Extend User Report
ALTER VIEW [dbo].[ReturnFileReportInfoView] 
AS 
	SELECT 	'' AS AccountName,
			'' AS OwnerName,
			'' AS OwnerGroupMembership,
			'' AS [FileName],
			0 AS V1PageCount,
			0 AS TotalVersion,
			CONVERT(DATETIME, GETDATE()) AS UploadDate,
			CONVERT(DATETIME, GETDATE()) AS OverDue,
			CONVERT(DATETIME, GETDATE()) AS CompletedDate,
			0 AS V1Users,
			0 AS LatestVersionUsers,
			'' AS Modifier,
			CONVERT(DATETIME, GETDATE()) AS ModifiedDate,
			'' AS DeletedByUser,
			CONVERT(DATETIME, GETDATE()) AS DeletedDate




GO


