USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 11/11/2020 10:45:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--Add IsExternalPrimaryDecisionMaker property
ALTER VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			'' AS [Status],
			'' AS [Guid],
			'' AS [FileName],
			0 AS Job,
			0 AS [Version],
			CONVERT(bit, 0) AS PrivateAnnotations,
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			'' AS PrimaryDecisionMakerName,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			CONVERT(bit, 0) AS HasAnnotations,
			'' AS Decision,
			CONVERT(bit, 0) AS IsCollaborator,
			CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,
			CONVERT(DateTime, GETDATE()) AS ApprovalApprovedDate,
			0 AS CurrentPhase,
			'' AS PhaseName,
			'' AS WorkflowName,
			'' AS NextPhase,
			'' AS DecisionMakers,
			0 AS DecisionType,
			'' AS OwnerName,
			CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
			NULL AS JobOwner,
			CONVERT(bit, 0) AS IsPhaseComplete,
			'' AS [VersionSufix],
			CONVERT(bit, 0) AS IsLocked,
			CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker,
			0 AS IsVideoReportEnabled,
			'' AS ApprovalTypeColour,
			'' AS StyleColor,
			CONVERT(bit, 0) AS IsLandscapeImage



GO


