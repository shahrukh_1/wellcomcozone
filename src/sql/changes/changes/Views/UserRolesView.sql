USE [GMGCoZone]
GO

/****** Object:  View [dbo].[UserRolesView]    Script Date: 13/01/2022 21:45:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO



--Alter UserRolesView
ALTER VIEW [dbo].[UserRolesView] AS
SELECT  U.ID ,
        U.GivenName ,
        u.FamilyName ,
        u.Username ,
        U.EmailAddress ,
        COALESCE(CR.[Key], 'NON') AS [CollaborateRole] ,
        COALESCE(DR.[Key], 'NON') AS [DeliverRole] ,
        COALESCE(AR.[Key], 'NON') AS [AdminRole] ,
		COALESCE(FR.[Key], 'NON') AS [FileTransferRole] ,
        COALESCE(SR.[Key], 'NON') AS [SysAdminRole] ,
		COALESCE(RR.[Key], 'NON') AS [ReportRole] ,
		CASE WHEN u.[PrivateAnnotations] = 1
		THEN 'ONLY'
		ELSE '-'
		end [PrivateAnnotations],
		(select top 1 ul.DateLogin from [UserLogin] ul where [user]  = u.ID order by ul.DateLogin desc) as DateLastLogin ,
        u.Account ,
        US.[Key] AS [UserStatus],
	    [IsUserLoginLocked]
FROM    dbo.[User] AS U
        INNER JOIN dbo.UserStatus US ON U.Status = US.ID
        LEFT JOIN ( SELECT  CUR.[User] ,
                            CR.[Key]
                    FROM    dbo.UserRole CUR
                            INNER JOIN dbo.Role CR ON CUR.Role = CR.ID
                            INNER JOIN dbo.AppModule CAM ON CR.AppModule = CAM.ID
                                                            AND CAM.[Key] = 0
                  ) AS CR ON U.ID = CR.[User]
        LEFT JOIN ( SELECT  DUR.[User] ,
                            DR.[Key]
                    FROM    dbo.UserRole DUR
                            INNER JOIN dbo.Role DR ON DUR.Role = DR.ID
                            INNER JOIN dbo.AppModule DAM ON DR.AppModule = DAM.ID
                                                            AND DAM.[Key] = 2
                  ) AS DR ON U.ID = DR.[User]
        LEFT JOIN ( SELECT  AUR.[User] ,
                            AR.[Key]
                    FROM    dbo.UserRole AUR
                            INNER JOIN dbo.Role AR ON AUR.Role = AR.ID
                            INNER JOIN dbo.AppModule AAM ON AR.AppModule = AAM.ID
                                                            AND AAM.[Key] = 3
                  ) AS AR ON U.ID = AR.[User]
		LEFT JOIN ( SELECT  FUR.[User] ,
                            FR.[Key]
                    FROM    dbo.UserRole FUR
                            INNER JOIN dbo.Role FR ON FUR.Role = FR.ID
                            INNER JOIN dbo.AppModule FAM ON FR.AppModule = FAM.ID
                                                            AND FAM.[Key] = 6
                  ) AS FR ON U.ID = FR.[User]
        LEFT JOIN ( SELECT  SUR.[User] ,
                            SR.[Key]
                    FROM    dbo.UserRole SUR
                            INNER JOIN dbo.Role SR ON SUR.Role = SR.ID
                            INNER JOIN dbo.AppModule SAM ON SR.AppModule = SAM.ID
                                                            AND SAM.[Key] = 4
                  ) AS SR ON U.ID = SR.[User]

		LEFT JOIN ( SELECT  RUR.[User] ,
                            RR.[Key]
                    FROM    dbo.UserRole RUR
                            INNER JOIN dbo.Role RR ON RUR.Role = RR.ID
                            INNER JOIN dbo.AppModule RAM ON RR.AppModule = RAM.ID
                                                            AND RAM.[Key] = 7
                  ) AS RR ON U.ID = RR.[User]

				LEFT JOIN( SELECT ulfd.[User],
				                  ulfd.IsUserLoginLocked
				           FROM dbo.UserLoginFailedDetails ulfd
				  ) AS  IsUserLoginLocked ON U.ID = IsUserLoginLocked.[user]
       



GO


