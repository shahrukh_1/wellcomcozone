USE [GMGCoZone]
GO

/****** Object:  UserDefinedFunction [dbo].[GetApprovalPrimaryDecisionMakers]    Script Date: 27/04/2021 16:23:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER FUNCTION [dbo].[GetApprovalPrimaryDecisionMakers]
(
	@P_CurrentPhase INT,
	@P_Approval INT,
	@P_PrimaryDecisionMaker INT,
	@P_ExternalPrimaryDecisionMaker INT
)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN	
	DECLARE @String NVARCHAR(MAX);
	DECLARE @Names NVARCHAR(MAX);
	SET @String =  ISNULL((SELECT CASE WHEN @P_CurrentPhase IS NOT NULL 
								THEN(SELECT  CASE WHEN (SELECT TOP 1 dt.[Key]  
													   FROM dbo.DecisionType dt
													   JOIN dbo.ApprovalJobPhase ajp on dt.ID = ajp.DecisionType
													   JOIN dbo.ApprovalJobPhaseApproval a ON ajp.ID = a.Phase											   
													   WHERE a.Approval = @P_Approval AND a.Phase = @P_CurrentPhase) = 'PDM'
													   								
												 THEN (SELECT CASE WHEN ISNULL(@P_PrimaryDecisionMaker, 0) != 0 
																THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = @P_PrimaryDecisionMaker)
															 WHEN ISNULL(@P_ExternalPrimaryDecisionMaker, 0) != 0
																THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = @P_ExternalPrimaryDecisionMaker)
															 ELSE NULL
															 END)
																									 			 
												 ELSE (SELECT STUFF(( SELECT TOP 6 ', ' + result.Name
																	  FROM
																			(
																			SELECT u.GivenName + ' ' + u.FamilyName + ' '+ '(INACTIVE)' As Name
																			 FROM [User] u
																			 INNER JOIN ApprovalCollaborator ac ON u.ID = ac.Collaborator
																			 INNER JOIN ApprovalCollaboratorRole acr on ac.ApprovalCollaboratorRole = acr.ID
																			 INNER JOIN dbo.UserStatus us ON u.[Status] = us.ID
																			 WHERE ac.Approval = @P_Approval AND ac.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND us.[Key] = 'I'
																			 UNION
																			 SELECT u.GivenName + ' ' + u.FamilyName As Name
																			 FROM [User] u
																			 INNER JOIN ApprovalCollaborator ac ON u.ID = ac.Collaborator
																			 INNER JOIN ApprovalCollaboratorRole acr on ac.ApprovalCollaboratorRole = acr.ID
																			 INNER JOIN dbo.UserStatus us ON u.[Status] = us.ID
																			 WHERE ac.Approval = @P_Approval AND ac.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND us.[Key] != 'D' AND us.[Key] != 'I'
																			 UNION
																			 SELECT ec.GivenName + ' ' + ec.FamilyName As Name
																			 FROM ExternalCollaborator ec
																			 JOIN SharedApproval sa ON ec.ID = sa.ExternalCollaborator
																			 JOIN ApprovalCollaboratorRole acr on sa.ApprovalCollaboratorRole = acr.ID
																			 WHERE sa.Approval = @P_Approval AND sa.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND ec.IsDeleted = 0) result 
																			 FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)'),1,2,'') )
											   END
										 )
								   ELSE NULL
								   END)	,											 
		           (SELECT CASE WHEN ISNULL(@P_PrimaryDecisionMaker, 0) != 0 
									THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = @P_PrimaryDecisionMaker)
								WHEN ISNULL(@P_ExternalPrimaryDecisionMaker, 0) != 0
									THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = @P_ExternalPrimaryDecisionMaker)
								ELSE ''
								END)
					)       
  	   	  RETURN  @String;					 	  				   
    
END





GO


