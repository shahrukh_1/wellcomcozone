
USE [GMGCoZone]
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo_Optimized]    Script Date: 11/23/2021 10:46:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* Update SPC_ReturnApprovalsPageInfo to fix CZ-3503  - Approval owner is present in Collaborate UI access window even tough is not in the collaborators list sent through API -- "Display approvals in the dashboard if the user is creator or collaborator" */	
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
								-- 10 - Approved
								-- 11 - Rejected
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL,
	@P_RecCount int OUTPUT,
	@P_SelectedApprovalTypeIds nvarchar(100) = '',
	@P_SelectedTagwordIds nvarchar(max) = ''
)
AS
BEGIN
--exec [SPC_ReturnApprovalsPageInfo] 2228,8043,1,10,1,1,1,'',0,0,0,null,null,null,null,null,null,null,null,0,'',''

----Declare @P_Account int =@P_Account
----Declare @P_User int = 8043
----Declare @P_TopLinkId int = 1
----Declare @P_MaxRows int = 10
----Declare @P_Set int = 1
----Declare @P_SearchField int = 1 	-- 0 - Deadline
----Declare @P_Order bit = 1
----Declare @P_SearchText nvarchar(100) = ''
----Declare @P_Status int = 0
----Declare @P_ViewAll bit = 0
----Declare @P_HideCompletedApprovals bit = 0
----Declare @P_IsOverdue bit = NULL
----Declare @P_DeadlineMin datetime2 = NULL
----Declare @P_DeadlineMax datetime2 = NULL
----Declare @P_SharedWithGroups nvarchar(100) = NULL
----Declare @P_SharedWithInternalUsers nvarchar(100) = NULL
----Declare @P_SharedWithExternalUsers nvarchar(100) = NULL
----Declare @P_ByWorkflow nvarchar(100) = NULL
----Declare @P_ByPhase nvarchar(100) = NULL
----Declare @P_RecCount int =0
----Declare @P_SelectedApprovalTypeIds nvarchar(100) = ''
----Declare @P_SelectedTagwordIds nvarchar(max) = ''
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
   BEGIN TRY

    IF OBJECT_ID(N'tempdb..#Approvals') IS NOT NULL Drop Table #Approvals
	IF OBJECT_ID(N'tempdb..#Approvals_Row') IS NOT NULL Drop Table #Approvals_Row
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(150),
	   PrimaryDecisionMakerName nvarchar(150)
	)
		
	DECLARE @TempOnlyIDs TABLE
	(
	   ApprovalID int
	)

	IF @P_ViewAll = 1 AND @P_TopLinkId != 9
	BEGIN
		INSERT INTO @TempOnlyIDs(ApprovalID)
		select id FROM
		(
			select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
			FROM approval a
			INNER JOIN job j on j.id=a.job
			where @P_Account=j.account and a.IsDeleted=0 and a.DeletePermanently = 0 and (@P_SelectedApprovalTypeIds = '' or a.ApprovalTypeColour in (Select val FROM dbo.splitString(@P_SelectedApprovalTypeIds, ',')))
		) x 
		WHERE x.r = 1
	END
	ELSE IF @P_ViewAll = 1 AND @P_TopLinkId = 9
	BEGIN
		INSERT INTO @TempOnlyIDs
    SELECT	DISTINCT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,rank() over (partition by a.Job order by a.[Version] desc) rnk
			  FROM Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]	
				 WHERE j.Account = @P_Account
						AND A.IsLocked = 0
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC' AND js.[Key] != 'COM'
					AND (a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) 
			) approval
			inner JOIN ApprovalCollaborator ac
					ON ac.Approval = approval.ID	
			where rnk = 1
	END
	ELSE
	BEGIN 
		-- Temporary select all the Approvals for the user, with current phase = NULL
		BEGIN
			Select 
				a.*
			into 
				#Approvals 
			From 
				approval a 
			inner join 
				Job j on j.id=a.job
			Where 
				a.IsDeleted = 0 
				and a.DeletePermanently = 0 and j.account=@P_Account 
				and CurrentPhase is null and j.[Status] != 4 and (@P_SelectedApprovalTypeIds = '' or a.ApprovalTypeColour in (Select val FROM dbo.splitString(@P_SelectedApprovalTypeIds, ',')))

			INSERT INTO @TempOnlyIDs(ApprovalID)

			SELECT DISTINCT a.id
			FROM #Approvals a
			LEFT JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator
			WHERE (@P_TopLinkId IN (1,8)  and ac.ID is not null)

			--SELECT DISTINCT a.id
			--FROM #Approvals a
			--where a.Owner =@P_User
			--and @P_TopLinkId IN (1,8)

			UNION
			SELECT DISTINCT a.id
			FROM #Approvals a
			INNER JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator
			WHERE 
				(@P_TopLinkId = 3 AND a.[Owner] != @P_User)
			UNION

			SELECT 
				DISTINCT a.ID 
			FROM 
				#Approvals a 
			WHERE @P_TopLinkId = 2 
				AND a.[Owner] = @P_User
			
			UNION
			SELECT DISTINCT a.id
			FROM Job j
			INNER JOIN jobStatus js on js.ID=j.Status
			INNER JOIN #Approvals a on a.job=j.id
			INNER JOIN dbo.ApprovalCollaboratorDecision ac on ac.Approval = a.ID AND @P_User = ac.Collaborator 
--			AND ac.Decision IS NULL AND a.IsLocked = 0
			WHERE (ac.Decision IS NULL AND a.IsLocked = 0 and @P_TopLinkId = 7 AND js.[Key] != 'COM' AND a.IsLocked = 0) OR
				  (@P_TopLinkId = 9 AND js.[Key] != 'COM' AND js.[Key] != 'ARC' AND ((a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100)))
				
			delete @TempOnlyIDs
			from @TempOnlyIDs t
			inner join
			(
				select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
				from @TempOnlyIDs t
				inner join approval a on a.id=t.ApprovalID
			) x on x.ID=t.ApprovalID and x.r > 1
		END

		-- Temporary select all the Approvals for the user, with current phase = NOT NULL
		BEGIN
			SELECT j.JobOwner, a.*, ROW_NUMBER() OVER (PARTITION BY a.job ORDER BY Version DESC, a.ID DESC) RowNumber into #Approvals_Row
			FROM Approval a
			inner join Job j on j.id=a.job
			WHERE j.account=@P_Account and a.IsDeleted=0 and a.DeletePermanently = 0 and 
			CurrentPhase is not null and j.[Status] != 4 and 
			(@P_SelectedApprovalTypeIds = '' or a.ApprovalTypeColour in 
			(Select val FROM dbo.splitString(@P_SelectedApprovalTypeIds, ',')))

			INSERT INTO @TempOnlyIDs(ApprovalID)

			SELECT DISTINCT a.id 
			FROM #Approvals_Row a
			LEFT JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator and ac.phase=a.currentphase
			WHERE (@P_TopLinkId in (1,8) and RowNumber=1 AND (a.[JobOwner] = @P_User or ac.ID is not null)) 

			--SELECT DISTINCT a.id 
			--FROM #Approvals_Row a
			--WHERE (@P_TopLinkId in (1,8) and RowNumber=1 AND a.[JobOwner] = @P_User) 

			UNION
			SELECT DISTINCT a.id 
			FROM #Approvals_Row a
			INNER JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator and ac.phase=a.currentphase
			WHERE (@P_TopLinkId = 3 and RowNumber=1 AND ISNULL(a.JobOwner,0) != @P_User)

			UNION
			SELECT DISTINCT a.ID 
			FROM #Approvals_Row a 
			WHERE @P_TopLinkId = 2 and RowNumber=1
				AND ISNULL(a.JobOwner,0) = @P_User

			UNION

			--update #Approvals_Row set CurrentPhase=CurrentPhase+1  where job=140171
			SELECT  distinct a.id
			FROM Job j
			INNER JOIN jobStatus js on js.ID=j.Status
			INNER JOIN #Approvals_Row a on a.job=j.id
			INNER JOIN dbo.ApprovalCollaboratorDecision ac on ac.Approval = a.ID AND @P_User = ac.Collaborator  
			WHERE 	(@P_TopLinkId = 9 AND a.IsLocked = 0 AND js.[Key] != 'COM' AND js.[Key] != 'ARC' AND a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100)
					OR ((ac.phase=a.currentphase AND ac.Decision IS NULL)  AND @P_TopLinkId = 7 and RowNumber=1 AND js.[Key] != 'COM' AND a.IsLocked = 0) 

			--delete duplicates
			delete @TempOnlyIDs
			from @TempOnlyIDs t
			inner join
			(
				select t.ApprovalID, ROW_NUMBER() OVER (PARTITION BY ApprovalID ORDER BY ApprovalID DESC) r
				from @TempOnlyIDs t
			) x on x.ApprovalID=t.ApprovalID and x.r > 1

		END


	END

	--------------------------------------------------------------
	--------------------------------------------------------------

	--select * from @TempOnlyIDs

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	
	select a.id,
			CP.PhaseName,
			PDMN.PrimaryDecisionMakerName
	from @TempOnlyIDs t 
	inner join approval a on a.id=t.ApprovalID
	inner join job j on j.id=a.job
	inner join jobstatus js on js.id=j.status
	CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
	CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)		
	where 
			js.[Key] != 'ARC'
			AND js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			
			AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
			AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
			AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
			AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
			AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
			 OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
			AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
			AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (SELECT ajp.ID
															 FROM ApprovalJobPhase as ajp
															 WHERE ajp.PhaseTemplateID in (SELECT ajp.PhaseTemplateID
															 FROM ApprovalJobPhase as ajp
															 where ajp.ID in (Select val FROM dbo.splitString(@P_ByPhase, ','))))))
			AND ((@P_SelectedTagwordIds = '') OR (a.ID in (
			SELECT at.Approval FROM ApprovalTagwords as at
			WHERE at.PredefinedTagwords in (Select val FROM dbo.splitString(@P_SelectedTagwordIds, ',')) 
						)))
			
	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PDMN.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PDMN.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN CP.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN CP.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC,
				CASE
			WHEN (@P_SearchField_Local = 10 AND @P_Order_Local = 0) THEN 
			(
				(CASE WHEN j.[Status] = 3 THEN 1 ELSE 0 END)
			)
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 10 AND @P_Order_Local = 1) THEN  
			(
				(CASE WHEN j.[Status] = 3 THEN 1 ELSE 0 END)
			)
		END ASC ,
		CASE
			WHEN (@P_SearchField_Local = 11 AND @P_Order_Local = 0) THEN 
			(
				CASE WHEN  
				(select COUNT(1)
				from ApprovalCollaboratorDecision acd 
				where acd.Approval = t.ApprovalID and acd.decision =2) > 0
				 THEN 1 ELSE 0 END
			)
		END DESC ,
		CASE
			WHEN (@P_SearchField_Local = 11 AND @P_Order_Local = 1) THEN 
			(
				CASE WHEN 
				(select COUNT(1)
				from ApprovalCollaboratorDecision acd 
				where acd.Approval = t.ApprovalID and acd.decision =2) > 0
				THEN 1 ELSE 0 END
			)
		END ASC 

		
	--select getdate()

	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			a.PrivateAnnotations,
			(SELECT CASE 
					WHEN (a.IsError = 1 OR a.Guid = '') THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,			
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT DISTINCT ISNULL(STUFF((SELECT ',' + pt.Name
			FROM PredefinedTagwords pt
			inner join ApprovalTagwords at on pt.ID = at.PredefinedTagwords
			WHERE at.Approval = a.ID
			FOR XML PATH('')
			),1,1,''),'') ) AS Tagwords,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			 (SELECT Count(*) from ApprovalPage ap INNER JOIN ApprovalAnnotation aa
	on ap.ID = aa.[Page] where ap.Approval = t.ApprovalID and aa.ShowVideoReport = 1) AS IsVideoReportEnabled,
		(
			SELECT 
		CASE 	WHEN ((SELECT COUNT(1) from dbo.[User] u WHERE u.ID = @P_User AND u.PrivateAnnotations = 1 ) > 0 AND a.PrivateAnnotations = 1)
		THEN ( CASE WHEN 
				(	SELECT count(1) from (
					SELECT aa.* from ApprovalPage ap inner join ApprovalAnnotation aa on ap.ID = aa.[Page] 
					where ap.Approval = a.Id and a.CurrentPhase = aa.Phase)temp where temp.Creator = @P_User or temp.IsPrivateAnnotation = 0 
				) > 0
				THEN CONVERT(bit,1)
				ELSE CONVERT(bit,0)
			END)
		ELSE (
			CASE
				WHEN (						
					SELECT count(1) from ApprovalPage ap inner join ApprovalAnnotation aa on ap.ID = aa.[Page] 
					where ap.Approval = a.Id)>0
				THEN CONVERT(bit,1)
				ELSE CONVERT(bit,0)
			END) END
		) AS HasAnnotations,
								
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
			                       WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT top 1 ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			 '' AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT CASE WHEN ((SELECT [Status] FROM [User] WHERE ID = j.[JobOwner]) = 3)
						                   THEN (SELECT GivenName + ' ' + FamilyName + ' ' + '(INACTIVE)' FROM [User] WHERE ID = j.[JobOwner])
										   ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner]) END)
						ELSE (SELECT CASE WHEN ((SELECT [Status] FROM [User] WHERE ID = a.[Owner]) = 3)
						                   THEN (SELECT GivenName + ' ' + FamilyName + ' ' + '(INACTIVE)' FROM [User] WHERE ID = a.[Owner])
										   ELSE( SELECT
						 GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner]) END)
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			
			CONVERT(BIT, 0) AS IsPhaseComplete,

				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker,
					ISNULL((SELECT CASE
						WHEN (a.FileType IS NOT NULL) THEN (SELECT apc.Name FROM ApprovalTypeColour apc
						 WHERE  apc.ID = a.ApprovalTypeColour)
					    ELSE ''
					END),'') AS ApprovalTypeColour,
					ISNULL((SELECT CASE
						WHEN (a.FileType IS NOT NULL) THEN (SELECT apc.Colour FROM ApprovalTypeColour apc
						 WHERE  apc.ID = a.ApprovalTypeColour)
					    ELSE ''
					END),'') AS StyleColor,

					(SELECT CASE
						WHEN (EXISTS(SELECT TOP 1 ap.ID 
											FROM ApprovalPage ap 
											WHERE ap.Approval = a.ID and ap.Number =1 and (ap.PageSmallThumbnailWidth > ap.PageSmallThumbnailHeight))) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsLandscapeImage
			

	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
   
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END -- End WHILE loop
END














