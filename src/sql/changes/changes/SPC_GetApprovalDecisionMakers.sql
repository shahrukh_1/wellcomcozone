USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_GetApprovalDecisionMakers]    Script Date: 29/04/2021 12:13:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[SPC_GetApprovalDecisionMakers] (
	@P_CurrentPhase INT,
	@P_Approval INT,
	@P_PrimaryDecisionMaker INT,
	@P_ExternalPrimaryDecisionMaker INT,
	@P_IsLoadAll bit 
	
)

AS
BEGIN

IF @P_IsLoadAll = 1 
	BEGIN
	select top 1
	ISNULL([dbo].[GetAllDecisionMakers] (@P_CurrentPhase, @P_Approval, @P_PrimaryDecisionMaker, @P_ExternalPrimaryDecisionMaker), '') AS RetVal
			from JobStatus
	END	
	ELSE
	BEGIN
	select top 1
	ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (@P_CurrentPhase, @P_Approval, @P_PrimaryDecisionMaker, @P_ExternalPrimaryDecisionMaker), '') AS RetVal
			from JobStatus
	END
				 	  				   
    
END








GO


