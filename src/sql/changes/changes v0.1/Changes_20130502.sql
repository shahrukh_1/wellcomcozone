USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 04/04/2013 10:15:35 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnApprovalsPageView]'))
DROP VIEW [dbo].[ReturnApprovalsPageView]
GO

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 04/04/2013 10:20:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			0 AS [Status],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			'' AS Collaborators,
			0 AS Decision
GO

/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 04/04/2013 10:16:18 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnFolderTreeView]'))
DROP VIEW [dbo].[ReturnFolderTreeView]
GO

/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 04/04/2013 10:20:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReturnFolderTreeView] 
AS 
	SELECT  0 AS ID,
			0 AS Parent,
			'' AS Name,
			0 AS Creator,
			0 AS [Level],
			'' AS Collaborators

GO

/****** Object:  View [dbo].[ApprovalCountsView]    Script Date: 04/04/2013 10:16:34 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ApprovalCountsView]'))
DROP VIEW [dbo].[ApprovalCountsView]
GO

/****** Object:  View [dbo].[ApprovalCountsView]    Script Date: 04/04/2013 10:20:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount   

GO

/****** Object:  UserDefinedFunction [dbo].[GetAccessFolders]    Script Date: 04/04/2013 10:19:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccessFolders]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetAccessFolders]
GO

/****** Object:  UserDefinedFunction [dbo].[GetAccessFolders]    Script Date: 04/04/2013 10:26:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetAccessFolders]
(	
	-- Add the parameters for the function here
	@UserId int,
	@FolderId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT f.ID, f.Parent, f.Name, (0) AS Level, (CASE WHEN 
											(@UserId IN (	SELECT	fc.Collaborator 
															FROM	[dbo].FolderCollaborator fc
															WHERE	fc.Folder = f.ID 
					) ) THEN  '1' ELSE '0' end)  AS HasAccess 
		FROM	[dbo].[Folder] f
		WHERE	f.ID = @FolderId		 
		
		UNION ALL
		
		SELECT sf.ID, sf.Parent,sf.Name, (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  '1' ELSE '0' END)  AS HasAccess 
		FROM [dbo].[Folder] sf
			JOIN tableSet h
		ON h.ID	 = sf.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
    WHERE [Level] = 1
)

GO

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 04/04/2013 10:19:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetApprovalCollaborators]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetApprovalCollaborators]
GO

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 04/04/2013 10:26:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 03/25/2013 15:10:33 ******/

CREATE FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac
			 ON  a.ID = ac.Approval
		WHERE a.ID = @P_Approval ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCollaborator IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;

GO

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 04/04/2013 10:19:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFolderCollaborators]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFolderCollaborators]
GO

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 04/04/2013 10:26:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 03/25/2013 15:11:35 ******/

CREATE FUNCTION [dbo].[GetFolderCollaborators] (@P_Folder int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT fcg.CollaboratorGroup
		FROM Folder f 
				JOIN FolderCollaboratorGroup fcg
					ON  f.ID = fcg.Folder
		WHERE f.ID = @P_Folder ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT fc.Collaborator
		FROM Folder f 
				JOIN FolderCollaborator fc
			 ON  f.ID = fc.Folder
		WHERE f.ID = @P_Folder ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
			
	RETURN	(@GroupList + '#' + @CollaboratorList ) 

END;

GO

/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 04/04/2013 10:44:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_GetFolderTree]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_GetFolderTree]
GO

/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 04/04/2013 10:59:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_GetFolderTree
-- Description:  This Sp returns accessed folders that needed for Approval index Page
-- Date Created: Monday, 23 April 2010
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_GetFolderTree] (
	@P_User int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH tableSet AS 
		(
			SELECT f.ID, f.Parent, f.Name, f.Creator, (0) AS Level, (CASE WHEN 
												(@P_User IN (	SELECT	fc.Collaborator 
																FROM	[dbo].FolderCollaborator fc
																WHERE	fc.Folder = f.ID 
						) ) THEN  '1' ELSE '0' end)  AS HasAccess 
			FROM	[dbo].[Folder] f
			WHERE f.IsDeleted = 0	 
			
			UNION ALL
			
			SELECT sf.ID, sf.Parent,sf.Name, sf.Creator, (CASE WHEN (@P_User IN (	SELECT fc.Collaborator 
						FROM	[dbo].FolderCollaborator fc
						WHERE	fc.Folder = sf.ID 
						 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@P_User IN (	SELECT fc.Collaborator 
						FROM	[dbo].FolderCollaborator fc
						WHERE	fc.Folder = sf.ID 
						 )) THEN  '1' ELSE '0' END)  AS HasAccess 
			FROM [dbo].[Folder] sf
				JOIN tableSet h
			ON h.ID	 = sf.Parent
			WHERE sf.IsDeleted = 0			 
		)	
	
	SELECT ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level],(SELECT dbo.GetFolderCollaborators(ID)) AS Collaborators
	FROM tableSet
	WHERE HasAccess = 1
	GROUP BY ID, Parent, Name, Creator
	
END



GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 04/04/2013 10:44:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnApprovalCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnApprovalCounts]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 04/04/2013 10:59:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalCounts
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;
	
	-- Get All Approval count
	SET @AllCount = (	SELECT COUNT(a.ID)
						FROM	Job j
								INNER JOIN Approval a 
									ON a.Job = j.ID
								INNER JOIN JobStatus js
									ON js.ID = j.[Status]		
						WHERE	j.Account = @P_Account
							AND a.IsDeleted = 0
							AND js.[Key] != 'ARC'
							AND	(	SELECT MAX([Version])
									FROM Approval av 
									WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND @P_User IN (	SELECT ac.Collaborator 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID
															)
								) = a.[Version] )
						
	-- Get Owned by me count		
	SET @OwnedByMeCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] = @P_User
									) = a.[Version]	)
			
	-- Get Shared with me count
	SET @SharedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] != @P_User
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version]	)
			
	-- Get Recently viewed count
	SET @RecentlyViewedCount = (	SELECT COUNT(DISTINCT j.ID)
									FROM	ApprovalUserViewInfo auvi
											INNER JOIN Approval a
													ON a.ID = auvi.Approval 
											INNER JOIN Job j
													ON j.ID = a.Job 
											INNER JOIN JobStatus js
													ON js.ID = j.[Status]		
									WHERE	auvi.[User] =  @P_User
										AND j.Account = @P_Account
										AND a.IsDeleted = 0
										AND js.[Key] != 'ARC'
										AND @P_User IN (	SELECT ac.Collaborator 
																			FROM ApprovalCollaborator ac 
																			WHERE ac.Approval = a.ID
																		)										
										)
			
	-- Get Archived count
	SET @ArchivedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] = 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version])
			
	-- Get Recycle bin count
	SET @RecycleCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (@P_User IN (	SELECT ac.Collaborator 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = a.ID
												)			
									)
						) 
						+
						(	SELECT COUNT(f.ID)
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)
									AND (@P_User IN (	SELECT fc.Collaborator 
														FROM FolderCollaborator fc 
														WHERE fc.Folder = f.ID
													)
										)													
						)
							
	SELECT 	@AllCount AS AllCount,
			@OwnedByMeCount AS OwnedByMeCount,
			@SharedCount AS SharedCount,
			@RecentlyViewedCount AS RecentlyViewedCount,
			@ArchivedCount AS ArchivedCount,
			@RecycleCount AS RecycleCount
END


GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 04/04/2013 10:44:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 04/04/2013 11:00:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalsPageInfo
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC					
				)) AS ID, 
				a.ID AS Approval,	
				0 AS Folder,
				j.Title,							 
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,				
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )					
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)								
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)										
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )					
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)								
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)										
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END

END



GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnArchivedApprovalInfo]    Script Date: 04/04/2013 10:44:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnArchivedApprovalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnArchivedApprovalInfo]    Script Date: 04/04/2013 11:03:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnArchivedApprovalInfo
-- Description:  This SP returns archived folders and approvals
-- Date Created: Friday, 21 September 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC					
				)) AS ID, 			
				a.ID AS Approval,	
				0 AS Folder,
				j.Title,							 
				j.[Status] AS [Status],
				j.ID AS Job,	
				a.[Version],
				100 AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				0 AS MaxVersion,				
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				'######' AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
		FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
		WHERE	j.Account = @P_Account
				AND a.IsDeleted = 0	
				AND js.[Key] = 'ARC'									
				AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
				AND
				(	SELECT MAX([Version])
					FROM Approval av 
					WHERE 
						av.Job = a.Job								
						AND av.IsDeleted = 0
						AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
				) = a.[Version]	
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	 
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM	(
			SELECT 	a.ID
			FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0	
					AND js.[Key] = 'ARC'									
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job								
							AND av.IsDeleted = 0
							AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
					) = a.[Version]		
				)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	 
END


GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 04/04/2013 10:45:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnFoldersApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 04/04/2013 11:05:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnFoldersApprovalsPageInfo
-- Description:  This SP returns folders and approvals
-- Date Created: Monday, 30 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC					
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,	
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]	 						
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )												
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]	 						
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )												
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END


GO


/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]    Script Date: 04/04/2013 10:45:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]    Script Date: 04/04/2013 11:27:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnRecentViewedApprovalsPageInfo
-- Description:  This Sp returns recenr viwed approvals
-- Date Created: Monday, 12 June 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
			SELECT 	TOP (@P_Set * @P_MaxRows)
					CONVERT(int, ROW_NUMBER() OVER(
					ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
					END DESC					
				)) AS ID, 
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,							 
					j.[Status] AS [Status],	
					j.ID AS Job,
					a.[Version],
					100 AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					(SELECT MAX([Version])
					 FROM Approval av
					 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT 	a.ID
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	  
END


GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecycleBinPageInfo]    Script Date: 04/04/2013 10:45:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnRecycleBinPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecycleBinPageInfo]    Script Date: 04/04/2013 11:28:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnRecycleBinPageInfo
-- Description:  This SP returns deleted folders and approvals
-- Date Created: Monday, 30 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END


GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USE [GMGCoZone]
GO

UPDATE [GMGCoZone].[dbo].[Shape]
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
				<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
				  <Library/>
				  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Arrow 1">
					<Group d:type="layer" d:userLabel="Layer 1">
					  <Path winding="nonZero" ai:seqID="1" d:userLabel="Arrow 1" data="M99.1699 49.6357 81.5918 32.0728C81.5918 32.0728 71.7354 41.9272 62.0293 51.6328L62.0293 0.000488281 37.1602 0 37.1602 51.6328 17.5781 32.0605 0 49.6357 49.5825 99.1738 99.1699 49.6357Z">
						<fill>
						  <SolidColor color="#000000"/>
						</fill>
					  </Path>
					</Group>
				  </Group>
				  <Private/>
				</Graphic>'
WHERE ID = 1
GO

UPDATE [GMGCoZone].[dbo].[Shape]
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Arrow 2">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="2" data="M0 0 0 101.302 101.302 101.302 101.302 0 0 0ZM56.9824 75.9771 37.9878 75.9771 56.9824 56.9824 18.9941 56.9824 18.9941 44.3198 56.9824 44.3198 37.9878 25.3252 56.9824 25.3252 82.3081 50.6514 56.9824 75.9771Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 2
GO

UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Arrow 3">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="3" data="M30.1045 96.335 30.1045 30.1045 48.167 30.1045 24.083 0 0 30.1045 18.0625 30.1045 18.0625 96.335 30.1045 96.335Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 3
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Arrow 4">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="4" data="M89.3652 36.0716 34.7549 36.0663 55.5547 15.2611C58.9102 11.9051 58.7686 6.27475 55.2354 2.74155 51.7012 -0.786285 46.0781 -0.928864 42.7197 2.42758L12.834 32.3148 12.834 32.3148 0 45.1483 9.99805 55.1454 12.834 57.9823 12.834 57.9823 41.9883
			 87.1419C45.2969 90.4383 50.875 90.2498 54.4072 86.722 57.9414 83.1893 58.1299 77.6053 54.8223 74.3089L34.7549 54.2298 89.3652 54.2245C93.4551 54.2298 96.8086 50.1414 96.8086 45.1424 96.8086 40.1497 93.4551 36.0663 89.3652 36.0716Z">
					<fill>
					  <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 4
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Circle solid">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="5" data="M51.8833 0C23.231 0 0 23.231 0 51.8828 0 80.5371 23.231 103.767 51.8833 103.767 80.5371 103.767 103.768 80.5371 103.768 51.8828 103.768 23.231 80.5371 0 51.8833 0L51.8833 0Z">
					<fill>
					 <SolidColor color="#000000"/>  
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 5
GO

UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Circle Outline">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="8" data="M51.8828 6.48486C76.9131 6.48486 97.2822 26.8535 97.2822 51.8828 97.2822 76.9126 76.9131 97.2817 51.8828 97.2817 26.8535 97.2817 6.48438 76.9126 6.48438 51.8828 6.48438 26.8535 26.8535 6.48486 51.8828 6.48486M51.8828 0C23.2305 0 0 23.231 0 51.8828
			 0 80.5366 23.2305 103.766 51.8828 103.766 80.5371 103.766 103.768 80.5366 103.768 51.8828 103.768 23.231 80.5371 0 51.8828 0L51.8828 0Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 6
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Circle Cross">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="12" data="M51.8545 0C23.2197 0 0 23.2188 0 51.8535 0 80.4873 23.2197 103.707 51.8545 103.707 80.4883 103.707 103.708 80.4873 103.708 51.8535 103.708 23.2188 80.4883 0 51.8545 0ZM82.0898 68.7568 68.7578 82.0889 51.8545 65.1846 34.9502 82.0889 21.6182 68.7568
			 38.5225 51.8535 21.6182 34.9492 34.9502 21.6172 51.8545 38.5215 68.7578 21.6172 82.0898 34.9492 65.1865 51.8535 82.0898 68.7568Z">
					<fill>
						 <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 7
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Square Solid">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="6" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 8
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Square Outline">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="6" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029ZM6.78809 98.2412 98.2422 98.2412 98.2422 6.78809 6.78809 6.78809 6.78809 98.2412Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 9
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Circle Tick">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="10" data="M53.0586 0C23.7588 0 0 23.751 0 53.0498 0 82.3486 23.7588 106.101 53.0586 106.101 82.3486 106.101 106.108 82.3486 106.108 53.0498 106.108 23.751 82.3486 0 53.0586 0ZM50.5566 75.9795 46.79 80.0479 43.1807 75.8428 22.7051 52.0225 29.375 44.8311
			 45.9707 57.0498 82.2412 27.2588 89.0059 34.4346 50.5566 75.9795Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 10
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Switch">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Group ai:seqID="15">
					<Path y="-0.000488281" winding="nonZero" ai:seqID="16" data="M57.2407 12.7202 25.1294 12.7202 25.1294 0.000488281 0.000488281 25.7622 25.1294 50.8804 25.1294 38.1655 57.2407 38.1597 57.2407 12.7202Z">
					  <fill>
						 <SolidColor color="#000000"/>
					  </fill>
					</Path>
					<Path x="44.5195" y="44.519" winding="nonZero" ai:seqID="17" data="M57.2407 25.1167 31.7993 0.000488281 31.7993 12.7202 0.000488281 12.7202 0.000488281 38.1597 31.7993 38.1597 31.7993 50.8794 57.2407 25.1167Z">
					  <fill>
						<SolidColor color="#000000"/>
					  </fill>
					</Path>
				  </Group>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 11
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Cross">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="11" data="M91.8105 73.4512 64.2646 45.8984 91.8037 18.3594 73.4375 0 45.8975 27.5391 18.3594 0 0 18.3594 27.5381 45.9121 0 73.4512 18.3594 91.8105 45.8975 64.2715 73.4443 91.8105 91.8105 73.4512Z">
					<fill>
						 <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 12
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Asterisk">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="18" data="M101.585 38.0947 66.3799 38.0947 84.6211 10.5039 68.7354 0 50.7988 27.1328 32.8623 0 16.9639 10.5039 35.2168 38.0947 0 38.0947 0 57.1426 30.9521 57.1426 12.6982 84.7334 28.583 95.2373 50.793 61.6553 73.002 95.2373 88.8867 84.7334 70.6445 57.1426
			 101.585 57.1426 101.585 38.0947Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 13
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Tick">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="13" data="M93.6289 0 36.5947 46.8457 10.5088 27.6289 0 38.9492 32.208 76.4043 37.8809 83.0039 43.7979 76.6104 104.254 11.2812 93.6289 0Z">
					<fill>
					 <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 14
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Delete Arrow">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path winding="nonZero" ai:seqID="9" data="M72.832 0.000488281 0 0.000488281 0 79.4536 72.832 79.4536 105.938 39.7271 72.832 0.000488281ZM63.8442 56.0396 57.4888 62.4028 41.4082 46.3218 25.3335 62.4028 18.9712 56.0396 35.0454 39.9595 18.9712 23.8853 25.3335 17.522 41.4082 33.603 57.4888
			 17.522 63.8442 23.8853 47.77 39.9595 63.8442 56.0396Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 15
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] ='<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" viewHeight="128" viewWidth="128" ai:appVersion="16.0.0.682" ATE:version="1.0.0" flm:version="1.0.0" d:using="" xmlns="http://ns.adobe.com/fxg/2008" xmlns:ATE="http://ns.adobe.com/ate/2009" xmlns:ai="http://ns.adobe.com/ai/2009" xmlns:d="http://ns.adobe.com/fxg/2008/dt" xmlns:flm="http://ns.adobe.com/flame/2008">
			  <Library/>
			  <Group blendMode="normal" d:layerType="page" d:pageHeight="128" d:pageWidth="128" d:type="layer" d:userLabel="Warning">
				<Group d:type="layer" d:userLabel="Layer 1">
				  <Path ai:seqID="14" data="M108.185 86.9669C105.212 81.6115 62.4178 7.59976 59.7469 3.0773 57.3543 -0.994965 51.6512 -1.05649 49.2567 3.0773 45.6141 9.36636 3.04184 83.0939 0.927582 86.7794 -1.70132 91.3849 1.64145 96.0402 6.15219 96.0402 9.62387 96.0402 97.9647 96.0402
			 102.999 96.0402 107.951 96.0402 110.359 90.8976 108.185 86.9669ZM59.162 21.1408 57.7977 67.8019 51.1893 67.8019 49.8289 21.1408 59.162 21.1408ZM54.4969 87.7277 54.3963 87.7277C51.0956 87.7277 48.7586 85.1046 48.7586 81.7052 48.7586 78.2033 51.1893
			 75.6759 54.4969 75.6759 57.994 75.6759 60.2303 78.2033 60.2303 81.7052 60.2303 85.1046 57.994 87.7277 54.4969 87.7277Z">
					<fill>
					 <SolidColor color="#000000"/>
					</fill>
				  </Path>
				</Group>
			  </Group>
			  <Private/>
			</Graphic>'
WHERE ID = 16
GO







