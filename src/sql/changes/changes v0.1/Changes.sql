USE [GMGCoZone]
GO

--**--**--**--**--**--**--**--**--**--** BillingReportError-10072013  --**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  UserDefinedFunction [dbo].[GetParentAccounts]    Script Date: 07/10/2013 16:01:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetParentAccounts]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetParentAccounts]
GO

/****** Object:  UserDefinedFunction [dbo].[GetParentAccounts]    Script Date: 07/10/2013 15:59:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetParentAccounts]
(	
	-- Add the parameters for the function here
	@AccountId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH parentAccounts AS (
		SELECT a.ID, a.Parent, a.Name, a.GPPDiscount
		FROM	[dbo].[Account] a
		WHERE	a.ID = @AccountId		 
		
		UNION ALL
		
		SELECT pa.ID, pa.Parent, pa.Name, pa.GPPDiscount
		FROM [dbo].[Account] pa
			JOIN parentAccounts pas
		ON pa.ID = pas.Parent		
	)	

	SELECT *
	FROM  parentAccounts	
)

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 07/10/2013 16:20:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBillingCycles]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetBillingCycles]
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 07/10/2013 12:55:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetBillingCycles](
	@AccountID int,
	@LoggedAccountID int,
	@StartDate datetime,
	@EndDate dateTime	
) 
RETURNS nvarchar(MAX) 
AS BEGIN

	DECLARE @cycleCount INT = 0
	DECLARE @AddedProofsCount INT = 0
	DECLARE @totalExceededCount INT = 0	
	DECLARE @cycleStartDate DATETIME = @StartDate
	DECLARE @cycleEndDate DATETIME
	DECLARE @cycleAllowedProofsCount INT
	DECLARE @IsMonthly bit = 1 
	DECLARE @PlanPrice decimal = 0.0
	DECLARE @ParentAccount INT
	DECLARE @SelectedBillingPLan INT
	DECLARE @BillingPlanPrice INT
	
	SET @ParentAccount = (SELECT Parent FROM Account WHERE ID = @AccountID)
	SET @SelectedBillingPLan = (SELECT SelectedBillingPLan FROM Account WHERE ID = @AccountID)
	
	SET @cycleAllowedProofsCount = (SELECT bp.Proofs 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @cycleAllowedProofsCount = (CASE WHEN (@IsMonthly = 1)
						THEN @cycleAllowedProofsCount
						ELSE (@cycleAllowedProofsCount*12)
						END
						) 										
											
	SET @BillingPlanPrice = (SELECT bp.Price 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @IsMonthly = (SELECT IsMonthlyBillingFrequency 
						FROM Account a
						WHERE a.ID = @AccountID
						)
						
	SET @PlanPrice = (CASE WHEN (@LoggedAccountID = 1)
						THEN @BillingPlanPrice
						ELSE (SELECT NewPrice FROM AttachedAccountBillingPlan
								WHERE Account = @LoggedAccountID AND BillingPlan = @SelectedBillingPLan)
						END)
						
	SET @PlanPrice = (CASE WHEN (@IsMonthly = 1)
						THEN @PlanPrice
						ELSE (@PlanPrice*12)
						END
						) 
																					
	SET @cycleStartDate = @StartDate
	
	WHILE (@cycleStartDate < @EndDate)
	BEGIN	
		DECLARE @currentCycleApprovalsCount INT = 0
				
		SET @cycleEndDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END	
	
		SET @currentCycleApprovalsCount = ( SELECT COUNT(ap.ID) FROM Job j 
														INNER JOIN Approval ap
															ON j.ID = ap.Job
														WHERE j.Account = @AccountID
														AND ap.CreatedDate >= @cycleStartDate
														AND ap.CreatedDate <= @cycleEndDate	)
														 
		SET @AddedProofsCount = @AddedProofsCount + @currentCycleApprovalsCount
													
		SET @cycleStartDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END
								
		SET @cycleCount = @cycleCount + 1;	
		SET @totalExceededCount = @totalExceededCount + CASE WHEN (@cycleAllowedProofsCount < @currentCycleApprovalsCount)  
										THEN @currentCycleApprovalsCount - @cycleAllowedProofsCount
										ELSE 0
										END													
	END
		
	RETURN  CONVERT(NVARCHAR, @cycleCount) + '|' +  CONVERT(NVARCHAR, @AddedProofsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice)
	
END

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[ReturnAccountParametersView]    Script Date: 07/15/2013 15:32:33 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnAccountParametersView]'))
DROP VIEW [dbo].[ReturnAccountParametersView]
GO

/****** Object:  View [dbo].[ReturnAccountParametersView]    Script Date: 07/15/2013 15:31:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnAccountParametersView] 
AS 
	SELECT  0 AS AccountID,
			'' AS AccountName,
			0 AS AccountTypeID,
			'' AS AccountTypeName,
			0 AS BillingPlanID,
			'' AS BillingPlanName,
			0 AS AccountStatusID,
			'' AS AccountStatusName,
			0 AS LocationID,
			'' AS LocationName

GO


--**--**--**--**--**--**--**--**--**--** BillingReportChanges-16072013  --**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountParameters]    Script Date: 07/16/2013 17:24:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnAccountParameters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnAccountParameters]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountParameters]    Script Date: 07/16/2013 17:23:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnAccountParameters
-- Description:  This Sp returns multiple result sets that needed for Account/Billing Report parameters
-- Date Created: Wendsday, 15 July 2013
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnAccountParameters] (		
	@P_LoggedAccount int 
)
AS
BEGIN

SET NOCOUNT ON
			
		SELECT DISTINCT					
				a.ID AS AccountID,
				a.Name AS AccountName,
				at.ID AS AccountTypeID,
				at.Name AS AccountTypeName,
				ISNULL (bp.ID, 0) AS BillingPlanID,
				ISNULL (bp.Name, '') AS BillingPlanName,
				ast.ID AS AccountStatusID,
				ast.Name AS AccountStatusName,
				cty.ID AS LocationID,
				cty.ShortName AS LocationName
		FROM	Account a		
		INNER JOIN AccountType at 
				ON a.AccountType = at.ID
		INNER JOIN AccountStatus ast 
				ON a.[Status] = ast.ID		
		LEFT OUTER JOIN BillingPlan bp 
				ON a.SelectedBillingPlan = bp.ID
		INNER JOIN Company c 
				ON c.Account = a.ID
		INNER JOIN Country cty 
				ON c.Country = cty.ID		
		WHERE a.Parent = @P_LoggedAccount
		AND a.IsSuspendedOrDeletedByParent = 0
		AND a.IsTemporary = 0
		AND (ast.[Key] = 'A')
END

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 07/16/2013 17:02:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnBillingReportInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 07/16/2013 17:01:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnBillingReportInfo
-- Description:  This Sp returns multiple result sets that needed for Billing Report
-- Date Created: Wendsday, 15 May 2012
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnBillingReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountNameList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',
	@P_BillingPlanList nvarchar(MAX)='',
	@P_StartDate datetime ,
	@P_EndDate dateTime,
	@P_IsMonthlyBilling bit = 1,
	@P_LoggedAccount int 
)
AS
BEGIN
	DECLARE @subAccounts TABLE (ID INT)
	INSERT INTO @subAccounts SELECT a.ID FROM [dbo].[Account] a
								INNER JOIN Company c 
									ON c.Account = a.ID
								WHERE Parent = @P_LoggedAccount
								--AND a.NeedSubscriptionPlan = 1
								--AND a.ContractStartDate IS NOT NULL
								AND a.IsTemporary = 0
								--AND (ast.[Key] = 'A')
								--AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
								AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
								AND (@P_AccountNameList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountNameList, ',')))
								AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
								AND (@P_BillingPlanList = '' OR a.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_BillingPlanList, ',')))

	DECLARE @childAccounts TABLE (ID INT)		
	
	DECLARE @subAccount_ID int
	DECLARE Cursor_SubAccounts Cursor
	FOR SELECT ID FROM @subAccounts	
	OPEN Cursor_SubAccounts
	FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
	WHILE @@FETCH_STATUS = 0

	BEGIN	
	INSERT INTO @childAccounts SELECT ID FROM [dbo].[GetChildAccounts](@subAccount_ID)
	FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
	END

	CLOSE Cursor_SubAccounts
	DEALLOCATE Cursor_SubAccounts
		
	-- Get the accounts	
	SET NOCOUNT ON
			
		SELECT DISTINCT					
				a.ID,
				a.Name AS AccountName,
				at.Name AS AccountTypeName,
				ISNULL(bp.Name,'') AS BillingPlanName,
				ISNULL ((SELECT CASE WHEN a.Parent = 1
							THEN a.GPPDiscount
							ELSE
								(SELECT GPPDiscount FROM [GMGCoZone].[dbo].[GetParentAccounts] (a.ID)
										WHERE Parent = @P_LoggedAccount)
						END), 0.00)		 AS GPPDiscount,
				ISNULL (bp.Proofs, 0) AS Proofs,
				ISNULL((SELECT [dbo].[GetBillingCycles](a.ID,@P_LoggedAccount, @P_StartDate, @P_EndDate )), '') AS BillingCycleDetails,
				a.Parent,
				ISNULL(a.SelectedBillingPlan, 0) AS BillingPlan,
				ISNULL(bp.IsFixed, 0) AS IsFixedPlan,				
				ISNULL((SELECT sa.Name+',' 
					FROM Account sa
					INNER JOIN AccountStatus sast 
						ON sa.[Status] = sast.ID
					WHERE sa.Parent= a.ID
					AND sa.IsTemporary = 0
					AND sa.ContractStartDate IS NOT NULL
					AND (sast.[Key] = 'A')
					GROUP BY sa.Name FOR XML PATH('')),'') AS Subordinates													
		FROM	Account a		
		INNER JOIN AccountType at 
				ON a.AccountType = at.ID
		INNER JOIN AccountStatus ast 
				ON a.[Status] = ast.ID		
		LEFT OUTER JOIN BillingPlan bp 
				ON a.SelectedBillingPlan = bp.ID				
		WHERE
		(a.ID IN (SELECT * FROM @subAccounts) OR a.ID IN (SELECT * FROM @childAccounts))
		--AND a.NeedSubscriptionPlan = 1
		--AND a.ContractStartDate IS NOT NULL
		AND a.IsTemporary = 0
		AND (ast.[Key] = 'A')		
END

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

UPDATE [dbo].[Account]
	SET [IsMonthlyBillingFrequency] = 1
WHERE [Status] = (SELECT ID FROM [dbo].[AccountStatus] WHERE [Key] = 'A')  
	AND [IsMonthlyBillingFrequency] IS NULL
	AND [SelectedBillingPlan] IS NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN X decimal(22, 8) NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN Y decimal(22, 8) NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN Width decimal(22, 8) NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN Height decimal(22, 8) NOT NULL
GO

--**--**--**--**--**--**--**--**--**--** NewApprovalChanges-19072013  --**--**--**--**--**--**--**--**--**--**--**--**--**--**

ALTER TABLE [dbo].[Approval]
	ALTER  COLUMN Size decimal(18, 2) NOT NULL
GO