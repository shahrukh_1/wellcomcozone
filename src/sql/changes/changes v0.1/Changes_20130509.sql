USE [GMGCoZone]
GO

UPDATE [GMGCoZone].[dbo].[Shape]
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero"  d:userLabel="Arrow 1" data="M99.1699 49.6357 81.5918 32.0728C81.5918 32.0728 71.7354 41.9272 62.0293 51.6328L62.0293 0.000488281 37.1602 0 37.1602 51.6328 17.5781 32.0605 0 49.6357 49.5825 99.1738 99.1699 49.6357Z">
					<fill>
					  <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 1
GO

UPDATE [GMGCoZone].[dbo].[Shape]
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M0 0 0 101.302 101.302 101.302 101.302 0 0 0ZM56.9824 75.9771 37.9878 75.9771 56.9824 56.9824 18.9941 56.9824 18.9941 44.3198 56.9824 44.3198 37.9878 25.3252 56.9824 25.3252 82.3081 50.6514 56.9824 75.9771Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 2
GO

UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M30.1045 96.335 30.1045 30.1045 48.167 30.1045 24.083 0 0 30.1045 18.0625 30.1045 18.0625 96.335 30.1045 96.335Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 3
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M89.3652 36.0716 34.7549 36.0663 55.5547 15.2611C58.9102 11.9051 58.7686 6.27475 55.2354 2.74155 51.7012 -0.786285 46.0781 -0.928864 42.7197 2.42758L12.834 32.3148 12.834 32.3148 0 45.1483 9.99805 55.1454 12.834 57.9823 12.834 57.9823 41.9883
			 87.1419C45.2969 90.4383 50.875 90.2498 54.4072 86.722 57.9414 83.1893 58.1299 77.6053 54.8223 74.3089L34.7549 54.2298 89.3652 54.2245C93.4551 54.2298 96.8086 50.1414 96.8086 45.1424 96.8086 40.1497 93.4551 36.0663 89.3652 36.0716Z">
					<fill>
					  <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 4
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M51.8833 0C23.231 0 0 23.231 0 51.8828 0 80.5371 23.231 103.767 51.8833 103.767 80.5371 103.767 103.768 80.5371 103.768 51.8828 103.768 23.231 80.5371 0 51.8833 0L51.8833 0Z">
					<fill>
					 <SolidColor color="#000000"/>  
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 5
GO

UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M51.8828 6.48486C76.9131 6.48486 97.2822 26.8535 97.2822 51.8828 97.2822 76.9126 76.9131 97.2817 51.8828 97.2817 26.8535 97.2817 6.48438 76.9126 6.48438 51.8828 6.48438 26.8535 26.8535 6.48486 51.8828 6.48486M51.8828 0C23.2305 0 0 23.231 0 51.8828
			 0 80.5366 23.2305 103.766 51.8828 103.766 80.5371 103.766 103.768 80.5366 103.768 51.8828 103.768 23.231 80.5371 0 51.8828 0L51.8828 0Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 6
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M51.8545 0C23.2197 0 0 23.2188 0 51.8535 0 80.4873 23.2197 103.707 51.8545 103.707 80.4883 103.707 103.708 80.4873 103.708 51.8535 103.708 23.2188 80.4883 0 51.8545 0ZM82.0898 68.7568 68.7578 82.0889 51.8545 65.1846 34.9502 82.0889 21.6182 68.7568
			 38.5225 51.8535 21.6182 34.9492 34.9502 21.6172 51.8545 38.5215 68.7578 21.6172 82.0898 34.9492 65.1865 51.8535 82.0898 68.7568Z">
					<fill>
						 <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 7
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029Z">
					<fill>
					   <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 8
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero"  data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029ZM6.78809 98.2412 98.2422 98.2412 98.2422 6.78809 6.78809 6.78809 6.78809 98.2412Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 9
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M53.0586 0C23.7588 0 0 23.751 0 53.0498 0 82.3486 23.7588 106.101 53.0586 106.101 82.3486 106.101 106.108 82.3486 106.108 53.0498 106.108 23.751 82.3486 0 53.0586 0ZM50.5566 75.9795 46.79 80.0479 43.1807 75.8428 22.7051 52.0225 29.375 44.8311
			 45.9707 57.0498 82.2412 27.2588 89.0059 34.4346 50.5566 75.9795Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 10
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
					<Path y="-0.000488281" winding="nonZero" data="M57.2407 12.7202 25.1294 12.7202 25.1294 0.000488281 0.000488281 25.7622 25.1294 50.8804 25.1294 38.1655 57.2407 38.1597 57.2407 12.7202Z">
					  <fill>
						 <SolidColor color="#000000"/>
					  </fill>
					</Path>
					<Path x="44.5195" y="44.519" winding="nonZero" data="M57.2407 25.1167 31.7993 0.000488281 31.7993 12.7202 0.000488281 12.7202 0.000488281 38.1597 31.7993 38.1597 31.7993 50.8794 57.2407 25.1167Z">
					  <fill>
						<SolidColor color="#000000"/>
					  </fill>
					</Path>
			  <Private/>
			</Graphic>'
WHERE ID = 11
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M91.8105 73.4512 64.2646 45.8984 91.8037 18.3594 73.4375 0 45.8975 27.5391 18.3594 0 0 18.3594 27.5381 45.9121 0 73.4512 18.3594 91.8105 45.8975 64.2715 73.4443 91.8105 91.8105 73.4512Z">
					<fill>
						 <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 12
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M101.585 38.0947 66.3799 38.0947 84.6211 10.5039 68.7354 0 50.7988 27.1328 32.8623 0 16.9639 10.5039 35.2168 38.0947 0 38.0947 0 57.1426 30.9521 57.1426 12.6982 84.7334 28.583 95.2373 50.793 61.6553 73.002 95.2373 88.8867 84.7334 70.6445 57.1426
			 101.585 57.1426 101.585 38.0947Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 13
GO
		    
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M93.6289 0 36.5947 46.8457 10.5088 27.6289 0 38.9492 32.208 76.4043 37.8809 83.0039 43.7979 76.6104 104.254 11.2812 93.6289 0Z">
					<fill>
					 <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 14
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" data="M72.832 0.000488281 0 0.000488281 0 79.4536 72.832 79.4536 105.938 39.7271 72.832 0.000488281ZM63.8442 56.0396 57.4888 62.4028 41.4082 46.3218 25.3335 62.4028 18.9712 56.0396 35.0454 39.9595 18.9712 23.8853 25.3335 17.522 41.4082 33.603 57.4888
			 17.522 63.8442 23.8853 47.77 39.9595 63.8442 56.0396Z">
					<fill>
						<SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 15
GO
		   
UPDATE [GMGCoZone].[dbo].[Shape] 
SET [FXG] = '<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path data="M108.185 86.9669C105.212 81.6115 62.4178 7.59976 59.7469 3.0773 57.3543 -0.994965 51.6512 -1.05649 49.2567 3.0773 45.6141 9.36636 3.04184 83.0939 0.927582 86.7794 -1.70132 91.3849 1.64145 96.0402 6.15219 96.0402 9.62387 96.0402 97.9647 96.0402
			 102.999 96.0402 107.951 96.0402 110.359 90.8976 108.185 86.9669ZM59.162 21.1408 57.7977 67.8019 51.1893 67.8019 49.8289 21.1408 59.162 21.1408ZM54.4969 87.7277 54.3963 87.7277C51.0956 87.7277 48.7586 85.1046 48.7586 81.7052 48.7586 78.2033 51.1893
			 75.6759 54.4969 75.6759 57.994 75.6759 60.2303 78.2033 60.2303 81.7052 60.2303 85.1046 57.994 87.7277 54.4969 87.7277Z">
					<fill>
					 <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'
WHERE ID = 16
GO







