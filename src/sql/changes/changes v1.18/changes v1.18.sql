USE GMGCoZone
GO
-- changes for caching the mails
CREATE TABLE dbo.Email
	(
	ID int NOT NULL IDENTITY (1, 1),
	fromName nvarchar(255) NOT NULL,
	toName nvarchar(255) NOT NULL,
	toEmail nvarchar(120) NOT NULL,
	fromEmail nvarchar(120) NOT NULL,
	toCC nvarchar(255) NOT NULL,
	subject nvarchar(255) NOT NULL,
	bodyText nvarchar(MAX) NOT NULL,
	bodyHtml nvarchar(MAX) NOT NULL,
	ProcessingInProgress nvarchar(36) NULL,
	TryCount INT NULL,
	LastErrorMessage nvarchar(256) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Email ADD CONSTRAINT
	PK_Email PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

CREATE TABLE dbo.Attachment
	(
	ID int NOT NULL IDENTITY (1, 1),
	FileName nvarchar(120) NOT NULL,
	Guid varchar(36) NOT NULL,
	Email int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Attachment ADD CONSTRAINT
	PK_Attachment PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Attachment ADD CONSTRAINT
	FK_Attachment_Email FOREIGN KEY
	(
	Email
	) REFERENCES dbo.Email
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Approval ADD
	ProcessingInProgress varchar(36) NULL,
	SubmitGroupKey varchar(36) NULL,
	ScreenshotUrl nvarchar(256) NULL,
	GenerateSeparations bit NULL,
	WebPageSnapshotDelay int NULL
GO
ALTER TABLE dbo.Approval ADD CONSTRAINT
	DF_Approval_ProcessingInProgress DEFAULT ('completed') FOR ProcessingInProgress
GO

ALTER TABLE dbo.Email ALTER COLUMN toName NVARCHAR(255) NULL
GO

--Add Option to Enable/Disable SoftProofing

ALTER TABLE dbo.BillingPlanType
ADD EnableSoftProofingTools bit NOT NULL DEFAULT(0)
GO

ALTER VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS CollaborateBillingPlanName,
0 AS CollaborateProofs,
'' AS CollaborateBillingCycleDetails,
0 AS CollaborateBillingPlan,
CONVERT(BIT, 0) AS CollaborateIsFixedPlan,
'' AS DeliverBillingPlanName,
0 AS DeliverProofs,
'' AS DeliverBillingCycleDetails,
0 AS DeliverBillingPlan,
CONVERT(BIT, 0) AS DeliverIsFixedPlan,
0.0 AS GPPDiscount,
0 AS Parent,
'' AS Subordinates
GO
ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_IsMonthlyBilling BIT = 1 ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              AccountStatus VARCHAR(10) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateCycleDetails NVARCHAR(100) ,
              CollaborateModuleId INT ,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverCycleDetails NVARCHAR(100) ,
              DeliverModuleId INT 
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  AccountStatus ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverCycleDetails ,
                  DeliverModuleId 
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        AcS.[Key] AS [AccountStatus] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CSPIBP.ModuleId)
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CASPBP.ModuleId)
                               END, '0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,

                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DASPBP.ModuleId)
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DSPIBP.ModuleId)
                               END, '0|0|0|0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId]
                        
                FROM    dbo.Account A
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    AcS.[Key]
                                      FROM      dbo.AccountStatus AcS
                                      WHERE     AcS.ID = A.Status
                                    ) AcS ( [Key] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
   
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                            
        DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN @t BP ON BP.AccountId = A.ID
                WHERE   Parent = @P_LoggedAccount
								--AND a.NeedSubscriptionPlan = 1
								--AND a.ContractStartDate IS NOT NULL
                        AND a.IsTemporary = 0
								--AND (ast.[Key] = 'A')
								--AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR BP.CollaborateBillingPlanID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_BillingPlanList,
                                                        ',') )
                             
                              OR BP.DeliverBillingPlanID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_BillingPlanList,
                                                        ',') )
                            )

        DECLARE @childAccounts TABLE ( ID INT )

        DECLARE @subAccount_ID INT
        DECLARE Cursor_SubAccounts CURSOR
        FOR SELECT ID FROM @subAccounts	
        OPEN Cursor_SubAccounts
        FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
        WHILE @@FETCH_STATUS = 0 
            BEGIN	
                INSERT  INTO @childAccounts
                        SELECT  ID
                        FROM    [dbo].[GetChildAccounts](@subAccount_ID)
                FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
            END

        CLOSE Cursor_SubAccounts
        DEALLOCATE Cursor_SubAccounts

     -- Get the accounts
        SET NOCOUNT OFF

        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
                ISNULL(( SELECT SLCT.Name + ','
                         FROM   ( SELECT    DISTINCT
                                            sa.Name
                                  FROM      Account sa
                                            INNER JOIN AccountStatus sast ON sa.[Status] = sast.ID
                                            INNER JOIN dbo.SubscriberPlanInfo SPI ON SPI.Account = sa.ID
                                  WHERE     sa.Parent = a.ID
                                            AND sa.IsTemporary = 0
                                            AND SPI.ContractStartDate IS NOT NULL
                                            AND ( sast.[Key] = 'A' )
                                  UNION
                                  SELECT    DISTINCT
                                            sa.Name
                                  FROM      Account sa
                                            INNER JOIN AccountStatus sast ON sa.[Status] = sast.ID
                                            INNER JOIN dbo.AccountSubscriptionPlan ASP ON ASP.Account = sa.ID
                                  WHERE     sa.Parent = a.ID
                                            AND sa.IsTemporary = 0
                                            AND ASP.ContractStartDate IS NOT NULL
                                            AND ( sast.[Key] = 'A' )
                                ) AS SLCT
                         GROUP BY SLCT.Name
                       FOR
                         XML PATH('')
                       ), '') AS Subordinates
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID
        WHERE   ( a.ID IN ( SELECT  *
                            FROM    @subAccounts )
                  OR a.ID IN ( SELECT   *
                               FROM     @childAccounts )
                )
                AND A.IsTemporary = 0
                AND ( TBL.[AccountStatus] = 'A' )
    END
GO

DECLARE @atrId AS INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 0 AND R.[Key] = 'ADM'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SESM' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 1 AND R.[Key] = 'ADM'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SEIN', 'SESM' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

ALTER TABLE dbo.DeliverJob
ADD FileCheckSum nvarchar(128) NULL
GO
