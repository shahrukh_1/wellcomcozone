USE GMGCoZone;
GO

ALTER TABLE dbo.NotificationItem
Add IsDeleted BIT NOT NULL DEFAULT 0
GO

ALTER PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN	
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath,
	    (SELECT ISNULL(CASPBP.[SubscriptionPlanName] + '; ','') +
	            ISNULL(CSPIBP.[SubscriberPlanName] + '; ','')) AS Plans
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		LEFT OUTER JOIN dbo.SubscriberPlanInfo spi
			ON a.ID = spi.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID
		OUTER APPLY ( SELECT STUFF(( SELECT '; ' + ASPBP.Name
					  FROM      dbo.AccountSubscriptionPlan ASP
								INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
					  WHERE     ASP.Account = A.ID FOR XML PATH('')),1,1,'') AS [SubscriptionPlanName]
					) CASPBP ( [SubscriptionPlanName])
		OUTER APPLY ( SELECT STUFF(( SELECT '; ' + SPIBP.Name
					  FROM      dbo.SubscriberPlanInfo SPI
								INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
					  WHERE     SPI.Account = A.ID FOR XML PATH('')),1,1,'') AS [SubscriberPlanName]
					) CSPIBP ( [SubscriberPlanName])
		
	WHERE	
		s.[Key]<>'D' AND
		a.ID <> @P_LoggedAccount AND
		a.IsTemporary = 0 AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ','))
													 OR spi.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END

GO

--Account Report changes
ALTER VIEW [dbo].[AccountsReportView]
AS
SELECT
	0  AS ID, 
	'' AS Name,
	0 AS StatusId,
	'' AS StatusName,
	'' AS [Key],
	'' AS Domain,
	CONVERT(BIT, 0) AS IsCustomDomainActive,-- not null
	CAST('' AS NVARCHAR(128)) AS CustomDomain,--null
	0 AS AccountTypeID,
	GETDATE() AS CreatedDate,
	CONVERT(BIT, 0) AS IsTemporary,-- not null
	'' AS AccountTypeName,
	'' AS GivenName,
	'' AS FamilyName,
	0 AS Company ,
	0 AS Country ,
	'' AS CountryName,	
	CAST(0 AS INT) AS NoOfAccounts,
	CONVERT(DATETIME,GETDATE()) AS LastActivity,
	CAST('' AS NVARCHAR(128)) AS ChildAccounts,
	'' AS AccountPath,
	'' AS Plans
GO

DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Reports', -- Controller - nvarchar(128)
          'Plans', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          6 , -- Position - int
          1, -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'RPPL' , -- Key - nvarchar(4)
          'Plans' , -- Name - nvarchar(64)
          'Account Reports'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'RPIN'
GO

DECLARE @controllerId INT
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Reports', -- Controller - nvarchar(128)
          'BillingInfo', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          7 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'RPBI' , -- Key - nvarchar(4)
          'Billing Info' , -- Name - nvarchar(64)
          'Account Reports'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'RPIN'
GO


DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE R.[Key] IN ('HQA', 'HQM') AND AT.[Key] in ('GMHQ') AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('RPBI', 'RPPL')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

UPDATE [dbo].[MenuItem]
SET IsVisible = 1, Position = 5, [Parent] = (SELECT ID FROM [MenuItem] WHERE [Key] = 'RPIN')
FROM [dbo].[MenuItem]
WHERE [Key] = 'RPAD'
GO

ALTER TABLE [dbo].[AdvancedSearch] ADD JobTitle NVARCHAR(25) NULL;
--------------------------------------------------------------------------------------------------Launched Test
--------------------------------------------------------------------------------------------------Launched Stage

UPDATE [dbo].[EventType] SET [Key] = 'TMP' WHERE [Key] = 'ACP'
UPDATE [dbo].[EventType] SET [Key] = 'ACP', Name = 'Approval status changed by PDM' WHERE [Key] = 'AWR'
UPDATE [dbo].[EventType] SET [Key] = 'AWR', Name = 'New SoftProofing Workstation Registered' WHERE [Key] = 'TMP'
GO

--- Removed OPTIMIZE option and UNIONs -------
----------------------------------------------
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_ViewAllFromRecycle bit = 0,
	@P_UserRoleKey NVARCHAR(4) = '',
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON	
	SELECT
		(SELECT
			COUNT(a.ID)					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]				
			WHERE	
					j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					AND a.[Version] =(SELECT MAX([Version]) AS [Version] 
									  FROM Approval av 
									  WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND (
												   @P_ViewAll = 1 
													 OR
													 (
													   @P_ViewAll = 0 AND 											   
														(@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(	SELECT TOP 1 ac.ID 
																									FROM ApprovalCollaborator ac 
																									WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																								  )
														)
													  )
												 )
									)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
						)
		) AS AllCount, 
		(SELECT
				COUNT(a.ID)
		 FROM Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]							
		 WHERE j.Account = @P_Account			
				AND a.IsDeleted = 0
				AND a.DeletePermanently = 0
				AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
				AND a.[Version] = (SELECT MAX([Version]) AS [Version] 
								   FROM Approval av											
								   WHERE av.Job = a.Job AND (ISNULL(j.JobOwner, 0) = @P_User OR av.[Owner] = @P_User))
				AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
						)	
		) AS OwnedByMeCount, 
		(SELECT					
				COUNT(a.ID)
		 FROM Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]									
		 WHERE j.Account = @P_Account			
				AND a.IsDeleted = 0
				AND a.DeletePermanently = 0
				AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
				AND a.[Version]	= (SELECT MAX([Version]) AS [Version] 
								   FROM Approval av										   
								   WHERE av.Job = a.Job AND ( ISNULL(j.JobOwner, 0) != @P_User AND av.[Owner] != @P_User)
															  AND EXISTS (
																			SELECT TOP 1 ac.ID 
																			FROM ApprovalCollaborator ac 
																			WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																		)	
								   )					
				AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)	
		) AS SharedCount, 
		(SELECT 
				COUNT(DISTINCT j.ID)
		 FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a
					ON a.ID = auvi.Approval 
			INNER JOIN Job j
					ON j.ID = a.Job 
			INNER JOIN JobStatus js
					ON js.ID = j.[Status]					
		 WHERE	auvi.[User] =  @P_User
			AND j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
			AND auvi.[ViewedDate] = (
									  SELECT max(vuvi.[ViewedDate]) 
									  FROM ApprovalUserViewInfo vuvi
									  INNER JOIN Approval av ON  av.ID = vuvi.[Approval]				
									  WHERE av.Job = a.Job			 					
									) 
			AND (
					@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
				)	
			AND (EXISTS(
						SELECT TOP 1 ac.ID 
						FROM ApprovalCollaborator ac 
						WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
					  )
					 OR ISNULL(j.JobOwner, 0) = @P_User
				)						
		) AS RecentlyViewedCount, 
		(SELECT						
			COUNT(a.ID)
		 FROM	
			Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]						
		 WHERE j.Account = @P_Account			
				AND a.IsDeleted = 0
				AND a.DeletePermanently = 0
				AND js.[Key] = 'ARC' 
				AND a.[Version] = (  SELECT MAX([Version]) AS [Version] 
									 FROM Approval av 
									 WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND (
												   @P_ViewAll = 1 
													 OR
													 (
													   @P_ViewAll = 0 AND 											   
														(@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(	SELECT TOP 1 ac.ID 
																									FROM ApprovalCollaborator ac 
																									WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																								  )
														)
													  )
												 )
									)
		 ) AS ArchivedCount,
		(SELECT 
			    SUM(recycle.Approvals) AS RecycleCount
			FROM (
			          SELECT COUNT(a.ID) AS Approvals
						FROM	Job j
								INNER JOIN Approval a 
									ON a.Job = j.ID
								INNER JOIN JobStatus js
									ON js.ID = j.[Status]
						WHERE j.Account = @P_Account
							AND a.IsDeleted = 1
							AND a.DeletePermanently = 0
							AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
							AND (
							      ISNULL((SELECT TOP 1 ac.ID
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
										, 
										(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
										WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
									  ) IS NOT NULL				
								)
							AND (
								 @P_ViewAllFromRecycle = 1 
								 OR
								 ( 
									@P_ViewAllFromRecycle = 0 AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS (SELECT TOP 1 ac.ID 
																											  FROM ApprovalCollaborator ac 
																											  WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
																										      )
																										  
													               )
								 )	
							)	
					 
					UNION
						SELECT COUNT(f.ID) AS Approvals
						FROM	Folder f 
						WHERE	f.Account = @P_Account
								AND f.IsDeleted = 1
								AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
								AND f.Creator = @P_User								
					
				)recycle
		) AS RecycleCount,
		(SELECT					
				COUNT(a.ID)
			FROM Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]									
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'COM'
					AND a.[Version]	= (SELECT MAX([Version]) AS [Version] 
									   FROM Approval av										   
									   WHERE av.Job = a.Job 
											  AND EXISTS (
															SELECT TOP 1 ac.ID 
															FROM ApprovalCollaboratorDecision ac 
															WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0) AND ac.Decision IS NULL
														)	
									   )					
					AND (
							js.[Key] != 'COM'
						)
		) AS MyOpenApprovalsCount,
		(SELECT				
			COUNT(a.ID)
		FROM	
			Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]				
		WHERE	
				j.Account = @P_Account			
				AND a.IsDeleted = 0
				AND a.DeletePermanently = 0
				AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
				AND a.[Version] = (SELECT MAX([Version]) AS [Version] 
								  FROM Approval av 
								  WHERE	av.Job = a.Job
										AND av.IsDeleted = 0
										AND (
											   @P_ViewAll = 1 
												 OR
												 (
												   @P_ViewAll = 0 AND 											   
													(@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(	SELECT TOP 1 ac.ID 
																								FROM ApprovalCollaborator ac 
																								WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																							  )
													)
												  )
											 )
								)
				AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)
				AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
				AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
				AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
				AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
				AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
					 OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
				AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
				AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (Select ajp.ID  FROM ApprovalJobPhase ajp WHERE ajp.PhaseTemplateID in (Select val FROM dbo.splitString(@P_ByPhase, ',')))))								) AS AdvanceSearchCount				
END
GO

----- Indexes chnages------------------

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ApprovalCollaborator]') AND name = N'ApprovalCollaborator_Collaborator_IX')
DROP INDEX [ApprovalCollaborator_Collaborator_IX] ON [dbo].[ApprovalCollaborator] WITH ( ONLINE = OFF )
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ApprovalCollaborator]') AND name = N'IX_ApprovalCollaborator_Collaborator')
DROP INDEX [IX_ApprovalCollaborator_Collaborator] ON [dbo].[ApprovalCollaborator] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_ApprovalCollaborator_Collaborator] ON [dbo].[ApprovalCollaborator] 
(
	[Collaborator] ASC
)
INCLUDE ( [Approval], [Phase]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
------------------------------------------
