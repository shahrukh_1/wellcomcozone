
  USE GMGCoZone
  GO
  
  --Filter deleted users from approval & folder collaborators
ALTER FUNCTION [dbo].[GetFolderCollaborators] (@P_Folder int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT fcg.CollaboratorGroup
		FROM Folder f 
				JOIN FolderCollaboratorGroup fcg
					ON  f.ID = fcg.Folder
		WHERE f.ID = @P_Folder ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT fc.Collaborator
		FROM Folder f 
				JOIN FolderCollaborator fc
			 ON  f.ID = fc.Folder
				JOIN dbo.[User] us
			 ON fc.Collaborator = us.ID
			    JOIN dbo.[UserStatus] ust
			 ON us.Status = ust.ID   	
		WHERE f.ID = @P_Folder AND ust.[Key] != 'D' ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
			
	RETURN	(@GroupList + '#' + @CollaboratorList ) 

END;
  GO
  
  
ALTER FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac			
			 ON  a.ID = ac.Approval
			    JOIN dbo.[User] us
			 ON ac.Collaborator = us.ID
			    JOIN dbo.[UserStatus] ust
			 ON us.Status = ust.ID   
		WHERE a.ID = @P_Approval AND ust.[Key] != 'D' ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCreator IS NULL AND aa.ExternalModifier IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCreator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCreator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;

GO

---Fix for CZD-694 Sys admin account users are displayed on User reports page on all accounts
ALTER PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
	-- Get the users	
	SET NOCOUNT ON
			
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID																							
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND (u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount)
		
END
GO

--Rename Manage group to Account since Manage will be used for Manage Module Items
UPDATE dbo.EventGroup
SET [Name] = 'Account',
    [Key] = 'AC'
WHERE [Key] = 'M'
GO

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
		SELECT eg.ID, 'ASC', 'Approval Status Changed'
		from dbo.EventGroup eg
		WHERE eg.[Key] = 'A'
		
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
		SELECT eg.ID, 'AWR', 'New SoftProofing Workstation Registered'
		from dbo.EventGroup eg
		WHERE eg.[Key] = 'A'
		
GO

--Set authorization for new Event types in RolePresetEventType

DECLARE @PresetId INT
DECLARE @RoleId INT
DECLARE @ModuleId INT

--Deliver Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 2);

----Low Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'L');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT')
	   
----Medium Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'M');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT')
	   
----High Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT', 'DFC')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT', 'DFC')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT', 'DFC')
	   
----Custom Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT', 'DFC')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT', 'DFC')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('DFS', 'DFE', 'DFT', 'DFC')
	   
	   
--Add Approval Status Changed Event type

--Collaborate Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 0);

----Low Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'L');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'CNC')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'CNC')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'CNC')

------ Contributor User	   
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'CNC')
	   
--Delete Replis to my Comment event from low preset	   
DELETE rpev from [dbo].[RolePresetEventType] rpev
JOIN dbo.[Role] rl on rpev.Role = rl.ID
JOIN dbo.[Preset] pr on rpev.Preset = pr.ID
JOIN dbo.[EventType] ev on rpev.EventType = ev.ID
WHERE rl.[Key] in ('ADM', 'MAN', 'MOD', 'CON') AND pr.ID = @PresetId AND ev.[Key] = 'CRC'
	   
----Medium Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'M');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ASC'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ASC'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ASC'
	   
DELETE rpev from [dbo].[RolePresetEventType] rpev
JOIN dbo.[Role] rl on rpev.Role = rl.ID
JOIN dbo.[Preset] pr on rpev.Preset = pr.ID
JOIN dbo.[EventType] ev on rpev.EventType = ev.ID
WHERE rl.[Key] in ('ADM', 'MAN', 'MOD') AND pr.ID = @PresetId AND ev.[Key] in ('ADW','AAU','AAS')
	   
----High Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'AWR')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'AWR')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'AWR')
	   
----Custom Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'AWR')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'AWR')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] in ('ASC', 'AWR')
	   
-- Administration Module

SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 3);

----Low Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'L');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'MPC'
	   
DELETE rpev from [dbo].[RolePresetEventType] rpev
JOIN dbo.[Role] rl on rpev.Role = rl.ID
JOIN dbo.[Preset] pr on rpev.Preset = pr.ID
JOIN dbo.[EventType] ev on rpev.EventType = ev.ID
WHERE rl.ID = @RoleId AND pr.ID = @PresetId AND ev.[Key] = 'GGC'

----Medium Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'M');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');
	   
DELETE rpev from [dbo].[RolePresetEventType] rpev
JOIN dbo.[Role] rl on rpev.Role = rl.ID
JOIN dbo.[Preset] pr on rpev.Preset = pr.ID
JOIN dbo.[EventType] ev on rpev.EventType = ev.ID
WHERE rl.ID = @RoleId AND pr.ID = @PresetId AND ev.[Key] = 'MAA'
GO





ALTER PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN		
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath	
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID	
		
	WHERE	
		s.[Key]<>'D' AND
		a.ID <> @P_LoggedAccount AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END


GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'HQA' OR R.[Key] = 'ADM') AND AT.[Key] in ('DELR', 'CLNT') AND AM.[Key] in (3, 4)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DELETE FROM [dbo].[MenuItemAccountTypeRole]
		WHERE	MenuItem = (SELECT ID FROM dbo.MenuItem WHERE [Key] IN ('RPAR'))
				AND AccountTypeRole = @atrId
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

ALTER PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
	-- Get the users	
	SET NOCOUNT ON
			
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID																							
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ((u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END

GO
--- db changes for open approvals into HTML5 viewer
DECLARE @parentMenuId AS INT
DECLARE @controllerId AS INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Studio', -- Controller - nvarchar(128)
          'Html', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

SET @parentMenuId = ( SELECT TOP 1
                                ID
                      FROM      dbo.MenuItem
                      WHERE     [Key] = 'STIN'
                    )
                    
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( @controllerId , -- ControllerAction - int
          @parentMenuId , -- Parent - int
          4 , -- Position - int
          0 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          1 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'STHT' , -- Key - nvarchar(4)
          'Html' , -- Name - nvarchar(64)
          'Studio' -- Title - nvarchar(128)
        )
GO
DECLARE @atrId AS INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE  AM.[Key] = 0
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'STHT' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

--Update NotificationQueue table for deliver notifications

ALTER TABLE dbo.[NotificationEmailQueue]
ADD JobGuid nvarchar(64) NULL
GO

-- Alter Get Billing Cycles function
ALTER FUNCTION [dbo].[GetBillingCycles]
    (
      @AccountID INT ,
      @LoggedAccountID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @ModuleId INT	
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN

        DECLARE @cycleCount INT = 0
        DECLARE @AddedProofsCount INT = 0
        DECLARE @totalExceededCount INT = 0	
        DECLARE @cycleStartDate DATETIME = @StartDate
        DECLARE @cycleEndDate DATETIME
        DECLARE @cycleAllowedProofsCount INT
        DECLARE @IsMonthly BIT = 1 
        DECLARE @PlanPrice DECIMAL = 0.0
        DECLARE @ParentAccount INT
        DECLARE @SelectedBillingPLan INT
        DECLARE @BillingPlanPrice INT
        DECLARE @moduleKey VARCHAR(10)
        DECLARE @accountTypeKey VARCHAR(10)
        
        SELECT  @accountTypeKey = AT.[Key]
        FROM    dbo.AccountType AT
                INNER JOIN dbo.Account A ON A.AccountType = AT.ID
        WHERE   A.ID = @AccountID
		
        IF ( @accountTypeKey = 'SBSC' ) 
            BEGIN
				-- subscriber
                SELECT  @SelectedBillingPLan = SPI.SelectedBillingPlan ,
                        @ParentAccount = A.Parent ,
                        @cycleAllowedProofsCount = BP.Proofs ,
                        @BillingPlanPrice = BP.Price ,
                        @IsMonthly = CONVERT(BIT, 1)
                FROM    Account A
                        INNER JOIN dbo.SubscriberPlanInfo SPI ON A.ID = SPI.Account
                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId            
            END
        ELSE
			-- all other account types 
            BEGIN
                SELECT  @SelectedBillingPLan = ASP.SelectedBillingPlan ,
                        @ParentAccount = A.Parent ,
                        @cycleAllowedProofsCount = BP.Proofs ,
                        @BillingPlanPrice = BP.Price ,
                        @IsMonthly = ASP.IsMonthlyBillingFrequency
                FROM    Account A
                        INNER JOIN dbo.AccountSubscriptionPlan ASP ON A.ID = ASP.Account
                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId
            END
	
        SET @cycleAllowedProofsCount = ( CASE WHEN ( @IsMonthly = 1 )
                                              THEN @cycleAllowedProofsCount
                                              ELSE ( @cycleAllowedProofsCount
                                                     * 12 )
                                         END ) 										
											
        SET @PlanPrice = ( CASE WHEN ( @LoggedAccountID = 1 )
                                THEN @BillingPlanPrice
                                ELSE ( SELECT   NewPrice
                                       FROM     AttachedAccountBillingPlan
                                       WHERE    Account = @LoggedAccountID
                                                AND BillingPlan = @SelectedBillingPLan
                                     )
                           END )
						
        SET @PlanPrice = ( CASE WHEN ( @IsMonthly = 1 ) THEN @PlanPrice
                                ELSE ( @PlanPrice * 12 )
                           END ) 
																					
        SET @cycleStartDate = @StartDate
	
        WHILE ( @cycleStartDate <= @EndDate ) 
            BEGIN	
                DECLARE @currentCycleJobsCount INT = 0
				
                SET @cycleEndDate = CASE WHEN ( @IsMonthly = 1 )
                                         THEN DATEADD(MM, 1, @cycleStartDate)
                                         ELSE DATEADD(YYYY, 1, @cycleStartDate)
                                    END	
	
                SELECT  @currentCycleJobsCount = CASE WHEN Am.[Key] = 0 -- Collaborate
                                                           THEN ( SELECT
                                                              COUNT(ap.ID)
                                                              FROM
                                                              Job j
                                                              INNER JOIN Approval ap ON j.ID = ap.Job
                                                              WHERE
                                                              j.Account = @AccountID
                                                              AND ap.CreatedDate >= @cycleStartDate
                                                              AND ap.CreatedDate <= @cycleEndDate
                                                              )
                                                      WHEN Am.[Key] = 2 -- Deliver
                                                           THEN ( SELECT
                                                              COUNT(dj.ID)
                                                              FROM
                                                              Job j
                                                              INNER JOIN dbo.DeliverJob dj ON j.ID = dj.Job
                                                              WHERE
                                                              j.Account = @AccountID
                                                              AND dj.CreatedDate >= @cycleStartDate
                                                              AND dj.CreatedDate <= @cycleEndDate
                                                              )
                                                      ELSE 0 -- for other modules 0 jobs is returned
                                                 END
                FROM    dbo.AppModule AM
                WHERE   AM.ID = @ModuleId
				
			
                SET @AddedProofsCount = @AddedProofsCount
                    + @currentCycleJobsCount
													
                SET @cycleStartDate = CASE WHEN ( @IsMonthly = 1 )
                                           THEN DATEADD(MM, 1, @cycleStartDate)
                                           ELSE DATEADD(YYYY, 1,
                                                        @cycleStartDate)
                                      END
								
                SET @cycleCount = @cycleCount + 1 ;	
                SET @totalExceededCount = @totalExceededCount
                    + CASE WHEN ( @cycleAllowedProofsCount < @currentCycleJobsCount )
                           THEN @currentCycleJobsCount
                                - @cycleAllowedProofsCount
                           ELSE 0
                      END
            END
		
        RETURN  CONVERT(NVARCHAR, @cycleCount) + '|' +  CONVERT(NVARCHAR, @AddedProofsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice)
	
    END

GO

ALTER TABLE dbo.[NotificationEmailQueue]
ADD WorkStationName nvarchar(64) NULL,
NewDecision int NULL
GO

ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_IsMonthlyBilling BIT = 1 ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              AccountStatus VARCHAR(10) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateCycleDetails NVARCHAR(100) ,
              CollaborateModuleId INT ,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverCycleDetails NVARCHAR(100) ,
              DeliverModuleId INT 
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  AccountStatus ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverCycleDetails ,
                  DeliverModuleId
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        AcS.[Key] AS [AccountStatus] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CSPIBP.ModuleId)
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CASPBP.ModuleId)
                               END, '0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,

                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DASPBP.ModuleId)
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DSPIBP.ModuleId)
                               END, '0|0|0|0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId]
                        
                FROM    dbo.Account A
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    AcS.[Key]
                                      FROM      dbo.AccountStatus AcS
                                      WHERE     AcS.ID = A.Status
                                    ) AcS ( [Key] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )

                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                            
        DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN @t BP ON BP.AccountId = A.ID
                WHERE   Parent = @P_LoggedAccount
								--AND a.NeedSubscriptionPlan = 1
								--AND a.ContractStartDate IS NOT NULL
                        AND a.IsTemporary = 0
								--AND (ast.[Key] = 'A')
								--AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR BP.CollaborateBillingPlanID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_BillingPlanList,
                                                        ',') )
 
                              OR BP.DeliverBillingPlanID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_BillingPlanList,
                                                        ',') )
                            )

        DECLARE @childAccounts TABLE ( ID INT )

        DECLARE @subAccount_ID INT
        DECLARE Cursor_SubAccounts CURSOR
        FOR SELECT ID FROM @subAccounts	
        OPEN Cursor_SubAccounts
        FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
        WHILE @@FETCH_STATUS = 0 
            BEGIN	
                INSERT  INTO @childAccounts
                        SELECT  ID
                        FROM    [dbo].[GetChildAccounts](@subAccount_ID)
                FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
            END

        CLOSE Cursor_SubAccounts
        DEALLOCATE Cursor_SubAccounts

     -- Get the accounts
        SET NOCOUNT OFF

        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
                ISNULL(( SELECT SLCT.Name + ','
                         FROM   ( SELECT    DISTINCT
                                            sa.Name
                                  FROM      Account sa
                                            INNER JOIN AccountStatus sast ON sa.[Status] = sast.ID
                                            INNER JOIN dbo.SubscriberPlanInfo SPI ON SPI.Account = sa.ID
                                  WHERE     sa.Parent = a.ID
                                            AND sa.IsTemporary = 0
                                            AND SPI.ContractStartDate IS NOT NULL
                                            AND ( sast.[Key] = 'A' )
                                  UNION
                                  SELECT    DISTINCT
                                            sa.Name
                                  FROM      Account sa
                                            INNER JOIN AccountStatus sast ON sa.[Status] = sast.ID
                                            INNER JOIN dbo.AccountSubscriptionPlan ASP ON ASP.Account = sa.ID
                                  WHERE     sa.Parent = a.ID
                                            AND sa.IsTemporary = 0
                                            AND ASP.ContractStartDate IS NOT NULL
                                            AND ( sast.[Key] = 'A' )
                                ) AS SLCT
                         GROUP BY SLCT.Name
                       FOR
                         XML PATH('')
                       ), '') AS Subordinates
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID
        WHERE   ( a.ID IN ( SELECT  *
                            FROM    @subAccounts )
                  OR a.ID IN ( SELECT   *
                               FROM     @childAccounts )
                )
                AND A.IsTemporary = 0
                AND ( TBL.[AccountStatus] = 'A' )
    END

GO
---------------------------------------------------------------Launched on debug
