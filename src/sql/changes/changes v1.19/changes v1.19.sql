USE GMGCoZone

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 04/08/2014 15:37:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add Return Approvals Page Info procedure
-- Fix the bug with multiple decisions for the same collaborator on an approval (temporary fix for an existing issue with adding a decision instead of updating the existing one)
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID, 
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
END
GO
-- Alter Procedure to add ReturnArchivedApprovalInfo
ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID,
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				100 AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				0 AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				'######' AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
				AND a.IsDeleted = 0	
				AND js.[Key] = 'ARC'
				AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
				AND
				(	SELECT MAX([Version])
					FROM Approval av 
					WHERE 
						av.Job = a.Job
						AND av.IsDeleted = 0
						AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
				) = a.[Version]	
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	 
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM	(
			SELECT 	a.ID
			FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0	
					AND js.[Key] = 'ARC'
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
					) = a.[Version]
				)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	 
END
GO
-- Add Return Folders Approvals Page Info
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0
						) AS ApprovalsCount,
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS [DocumentPagesCount]
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
				ORDER BY 
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN Deadline
					--END ASC,
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN Deadline
					--END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN f.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN f.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN f.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN f.ModifiedDate
					END DESC--,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN f.[Status]
					--END ASC,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN f.[Status]
					--END DESC		 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO

-- Alter Procedure to add DocumentPagesCount to return
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS DocumentPagesCount
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END
GO

-- Add Documents Page Count to procedure
ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
			SELECT 	TOP (@P_Set * @P_MaxRows)
					CONVERT(int, ROW_NUMBER() OVER(
					ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
					END DESC					
				)) AS ID, 
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,							 
					j.[Status] AS [Status],	
					j.ID AS Job,
					a.[Version],
					100 AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					(SELECT MAX([Version])
					 FROM Approval av
					 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT 	a.ID
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	  
END
GO
-- changes for tablet version
ALTER TABLE dbo.ApprovalAnnotation ADD
	ExternalModifier int NULL,
	ExternalCreator int NULL
GO
ALTER TABLE dbo.ApprovalAnnotation ADD CONSTRAINT
	FK_ApprovalAnnotation_ExternalCollaborator_Modifier FOREIGN KEY
	(
	ExternalModifier
	) REFERENCES dbo.ExternalCollaborator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ApprovalAnnotation ADD CONSTRAINT
	FK_ApprovalAnnotation_ExternalCollaborator_Creator FOREIGN KEY
	(
	ExternalCreator
	) REFERENCES dbo.ExternalCollaborator
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
UPDATE dbo.ApprovalAnnotation
SET ExternalCreator = ExternalCollaborator
GO
ALTER TABLE dbo.ApprovalAnnotation
	DROP CONSTRAINT FK_ApprovalAnnotation_ExternalCollaborator
GO
ALTER TABLE dbo.ApprovalAnnotation
      DROP COLUMN ExternalCollaborator
GO
ALTER FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac
			 ON  a.ID = ac.Approval
		WHERE a.ID = @P_Approval ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCreator IS NULL AND aa.ExternalModifier IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCreator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCreator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;
GO
-------------------------------------------------------------------------------------------------