USE GMGCoZone;

ALTER TABLE dbo.Approval
DROP COLUMN GenerateSeparations
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Changes for displaying all the approvals from an account

ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_RecCount int OUTPUT	
)
AS
BEGIN
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID)
	SELECT 
			a.ID
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]								
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND (
						(@P_Status = 0) OR
						((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
						((@P_Status = 2) AND (js.[Key] = 'INP')) OR
						((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
						((@P_Status = 4) AND (js.[Key] = 'COM')) OR
						((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
						((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
						((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
						--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
						((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
						((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
						((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
						((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
						((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
						((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
						((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
					)				
			AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
									(@P_TopLinkId = 1 
										AND ( 
											EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
							  )
							)
						)		
			) = a.[Version]	
	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT Progress FROM ApprovalPage ap
							WHERE ap.Approval = a.ID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.[User] u ON aa.Creator = u.ID
							 INNER JOIN dbo.ApprovalCollaborator ac ON u.ID = ac.Collaborator
							 INNER JOIN dbo.ApprovalCollaboratorDecision acd ON u.ID = acd.Collaborator
							 INNER JOIN dbo.UserStatus us ON u.[Status] = us.ID
							 WHERE ap.Approval = t.ApprovalID AND (us.[Key] != 'D' OR acd.Decision IS NOT NULL)
							 UNION
							 SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN ExternalCollaborator ex ON aa.ExternalCreator = ex.ID
                             INNER JOIN SharedApproval sa ON ex.ID = sa.ExternalCollaborator
							 INNER JOIN dbo.ApprovalCollaboratorDecision acd ON ex.ID = acd.ExternalCollaborator							
							 WHERE ap.Approval = t.ApprovalID AND (ex.IsDeleted = 0 OR acd.Decision IS NOT NULL)	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,				
			(SELECT CASE
			 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL  
			     THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
			     ELSE CONVERT(bit,0)
			 END) as IsChangesComplete,
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
				CONVERT(bit,0) AS AllCollaboratorsDecisionRequired	
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID

	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0

END
GO

-- Alter Procedure to change the pagination to more efficient method ----
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval)
	SELECT 
			a.Approval,
			IsApproval
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						) 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE						
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.Approval
		END ASC,
		CASE						
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.Approval
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.IsChangesComplete,
			result.ApprovalApprovedDate
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT Progress FROM ApprovalPage ap
							WHERE ap.Approval = a.ID and ap.Number = 1), 0 ) 					
					END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			(SELECT CASE
			 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL 
			     THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
			     ELSE CONVERT(bit,0)
			 END) as IsChangesComplete,
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1		
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			JOIN Approval a ON fa.Approval = a.ID	
	                    WHERE fa.Folder = f.ID AND a.DeletePermanently = 0
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					CONVERT(bit, 0) as IsChangesComplete,
					NULL as ApprovalApprovedDate
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 		
	SET @P_RecCount = 0

END
GO

-- Alter Procedure to change the pagination to more efficient method ----
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit	   
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval)
	SELECT 
			a.Approval,
			IsApproval
	FROM (				
			SELECT 	a.ID AS Approval,
					a.Deadline,
					a.CreatedDate,
					a.ModifiedDate,
					js.[Key] AS [Status],
					CONVERT(bit, 1) as IsApproval
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					LEFT OUTER JOIN FolderApproval fa
						ON fa.Approval = a.ID
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
							(@P_Status = 0) OR
							((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
							((@P_Status = 2) AND (js.[Key] = 'INP')) OR
							((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
							((@P_Status = 4) AND (js.[Key] = 'COM')) OR
							((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
							((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
							((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
							--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
							((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
							((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
							((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
							((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
							((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
							((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
							((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
						)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE av.Job = a.Job
								AND av.IsDeleted = 0
								AND EXISTS (
												SELECT TOP 1 ac.ID 
												FROM ApprovalCollaborator ac 
												WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
											)
						) = a.[Version]
					AND	@P_FolderId = fa.Folder
				
			UNION
			
			SELECT 	f.ID AS Approval,
					GetDate() AS Deadline,
					f.CreatedDate,
					f.ModifiedDate,
					'' AS [Status],
					CONVERT(bit, 0) as IsApproval
			FROM	Folder f
			WHERE	f.Account = @P_Account
					AND (@P_Status = 0)
					AND f.IsDeleted = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
					AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN a.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN a.[Status]
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.IsChangesComplete,
			result.ApprovalApprovedDate
		FROM (
				SELECT  t.ID AS ID,
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						a.[Guid],
						a.[FileName],
						js.[Key] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						ELSE ISNULL((SELECT Progress FROM ApprovalPage ap
								     WHERE ap.Approval = a.ID and ap.Number = 1), 0 ) 					
						END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						at.[Key] AS ApprovalType,
						a.[Version] AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT CASE
						 WHEN ( EXISTS (SELECT aa.ID					
										 FROM dbo.ApprovalAnnotation aa
										 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
										 WHERE ap.Approval = t.ApprovalID			 
										 )
								) THEN CONVERT(bit,1)
								  ELSE CONVERT(bit,0)
						 END)  AS HasAnnotations,				
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
													                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
														JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
														WHERE acd.Approval = a.ID
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				CONVERT(bit, 1) AS IsCollaborator,
				CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,
				(SELECT CASE
				 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL 
					 THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
					 ELSE CONVERT(bit,0)
				 END) as IsChangesComplete,
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN dbo.ApprovalType at
							ON 	at.ID = a.[Type]
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID	
						JOIN @TempItems t 
							ON t.ApprovalID = a.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 1
						
				UNION
				
				SELECT 	t.ID AS ID,
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						'' AS [Guid],
						'' AS [FileName],
						'' AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS ApprovalType,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						CONVERT(bit, 0) AS HasAnnotations,
						'' AS Decision,
						CONVERT(bit, 1) AS IsCollaborator,
						f.AllCollaboratorsDecisionRequired,
						CONVERT(bit, 0) as IsChangesComplete,
						NULL as ApprovalApprovedDate
				FROM	Folder f
						JOIN @TempItems t 
							ON t.ApprovalID = f.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 0				
			) result		
	ORDER BY result.ID	

	SET ROWCOUNT 0

	---- Send the total rows count ---- 
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND EXISTS (
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO

ALTER TABLE dbo.[ApprovalSeparationPlate]
ADD ChannelIndex int,
	IsSpotChannel bit NOT NULL DEFAULT (0)
GO

INSERT INTO dbo.ApprovalCommentType
        ( [Key], Name, Priority )
VALUES  ( 'MLC', -- Key - nvarchar(4)
          'Measurement Line Comment', -- Name - nvarchar(64)
          7  -- Priority - int
          )
GO
ALTER TABLE dbo.ApprovalAnnotation ADD
	machineId nvarchar(64) NOT NULL CONSTRAINT DF_ApprovalAnnotation_machineId DEFAULT ('')
GO

CREATE TABLE [dbo].[SoftProofingSessionParams](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[OutputProfileName] nvarchar(128) NOT NULL,
	[SessionGuid] nvarchar(64) NOT NULL,
	[ActiveChannels] nvarchar(128),
	[SimulatePaperTint] [bit] NOT NULL,
	[DefaultProfile] [bit] NOT NULL,
	CONSTRAINT [PK_SoftProofingSessionParams] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SoftProofingSessionParams]  WITH CHECK ADD FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO

CREATE TABLE [dbo].[LabColorMeasurement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalSeparationPlate] [int] NOT NULL,
	[Step] float NOT NULL,
	[L] float NOT NULL,
	[a] float NOT NULL,
	[b] float NOT NULL,
	CONSTRAINT [PK_LabColorMeasurement] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LabColorMeasurement]
ADD CONSTRAINT [FK_LabColorMeasurement_ApprovalSeparationPlate] FOREIGN KEY ([ApprovalSeparationPlate]) REFERENCES [dbo].[ApprovalSeparationPlate] ([ID])    
GO



ALTER TABLE dbo.[ApprovalPage]
ADD OutputRenderHeight int,
	OutputRenderWidth int
GO

CREATE TABLE [dbo].[SoftProofingSessionError](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SoftProofingSessionParams] [int] NOT NULL,
	[ApprovalPage] [int] NOT NULL,
	[Error] [nvarchar](500) NOT NULL,
	[LogDate] DATETIME NOT NULL,
 CONSTRAINT [PK_SoftProofingSessionError] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SoftProofingSessionError]  WITH CHECK ADD  CONSTRAINT [FK_SoftProofingSessionError_ApprovalPage] FOREIGN KEY([ApprovalPage])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO

ALTER TABLE [dbo].[SoftProofingSessionError] CHECK CONSTRAINT [FK_SoftProofingSessionError_ApprovalPage]
GO

ALTER TABLE [dbo].[SoftProofingSessionError]  WITH CHECK ADD  CONSTRAINT [FK_SoftProofingSessionError_SoftProofingSessionParams] FOREIGN KEY([SoftProofingSessionParams])
REFERENCES [dbo].[SoftProofingSessionParams] ([ID])
GO

ALTER TABLE [dbo].[SoftProofingSessionError] CHECK CONSTRAINT [FK_SoftProofingSessionError_SoftProofingSessionParams]
GO

ALTER TABLE dbo.ApprovalAnnotation ADD
CalibrationStatus INT NOT NULL CONSTRAINT DF_ApprovalAnnotation_CalibrationStatus DEFAULT (0)
GO

ALTER TABLE [GMGCoZone].[dbo].[SoftProofingSessionParams]
ADD OutputRGBProfileName nvarchar(128) 
GO

------------------- Committed to SoftProofing ---------------------

CREATE TABLE [dbo].[ApprovalEmbeddedProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int],
	[ApprovalPage] [int],
	[Name] [nvarchar](128) NOT NULL,
	[IsRGB] bit NOT NULL,
 CONSTRAINT [PK_ApprovalEmbeddedProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ApprovalEmbeddedProfile]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalEmbeddedProfile_ApprovalPage] FOREIGN KEY([ApprovalPage])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO

ALTER TABLE [dbo].[ApprovalEmbeddedProfile]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalEmbeddedProfile_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO

--modify delete approval procedure to delete softProofingSessions, softproofingsesionerror, approvalembProfile and add transaction to delete procedure
ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	SET XACT_ABORT ON;  
	BEGIN TRANSACTION
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		 --Delete annotations from ApprovalAnnotationStatusChangeLog
		DELETE [dbo].[ApprovalAnnotationStatusChangeLog]		 
		FROM   [dbo].[ApprovalAnnotationStatusChangeLog] aascl
				JOIN [dbo].[ApprovalAnnotation] aa
					ON aascl.Annotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id	    
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[ApprovalSetting] 
				WHERE Approval = @P_Id
		
		-- Delete from LabColorMeasurements
		DELETE [dbo].[LabColorMeasurement] 
		FROM	[dbo].[LabColorMeasurement] lbm
				JOIN [dbo].[ApprovalSeparationPlate] asp
					ON lbm.ApprovalSeparationPlate = asp.ID
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		--Delete from SoftProofing Session Error
		DELETE [dbo].[SoftProofingSessionError]
		FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
		WHERE ap.Approval = @P_Id
				
		--Delete from ApprovalEmbedded Profile foreign keys to approval
		DELETE [dbo].[ApprovalEmbeddedProfile]
		FROM [dbo].[ApprovalEmbeddedProfile] apep			
		WHERE apep.Approval = @P_Id			
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalUserRecycleBinHistory
		DELETE FROM [dbo].[ApprovalUserRecycleBinHistory]
		WHERE Approval = @P_Id
		
		-- Delete from UserApprovalReminderEmailLog
		DELETE FROM [dbo].[UserApprovalReminderEmailLog]
		WHERE Approval = @P_Id
		
		-- Delete from SoftProofingSessionParams
		DELETE FROM [dbo].[SoftProofingSessionParams]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
	    COMMIT TRANSACTION
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		IF (XACT_STATE()) = -1 
		BEGIN 
			SET @P_Success = ERROR_MESSAGE()
			ROLLBACK TRANSACTION;  
		END
		
		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO

ALTER TABLE dbo.ApprovalEmbeddedProfile
DROP CONSTRAINT FK_ApprovalEmbeddedProfile_ApprovalPage
GO

ALTER TABLE dbo.ApprovalEmbeddedProfile
DROP COLUMN ApprovalPage
GO

ALTER TABLE [dbo].[UploadEngineAccountSettings]
ADD EnableVersionMirroring bit NOT NULL DEFAULT(0)
GO

----Alter procedure for returning all shared deliver jobs even if the job owner was deleted

ALTER PROCEDURE [dbo].[SPC_ReturnDeliversPageInfo]
    (
      @P_Account INT ,
      @P_User INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;
        
        DECLARE @Delivers TABLE
            (
              ID INT ,
              [Title] NVARCHAR(128) ,
              [User] NVARCHAR(64) ,
              [Pages] NVARCHAR(50) ,
              [CPName] NVARCHAR(50) ,
              [Workflow] NVARCHAR(256) ,
              [CreatedDate] DATETIME ,
              [StatusKey] INT ,
              [Account] INT ,
              [OwnUser] INT,
              [IsShared] BIT
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  Title ,
                  [User] ,
                  Pages ,
                  CPName ,
                  Workflow ,
                  CreatedDate ,
                  StatusKey ,
                  Account ,
                  OwnUser,
                  IsShared
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] = @P_User
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'CON' -- contributor should only see his jobs
                  UNION
                --- deliver jobs for admin users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' -- admin should see all the jobs from current account
                  UNION
                  --- deliver shared jobs for admins -----
                  SELECT DISTINCT	DJ.ID ,
									J.Title ,
									[User].GivenName AS [User] ,
									DJ.Pages ,
									CPI.SystemName AS [CPName] ,
									CPCZW.Name AS [Workflow] ,
									DJ.CreatedDate ,
									Status.[Key] AS [StatusKey] ,
									J.Account AS [Account] ,
									DJ.[owner] AS [OwnUser],
									CONVERT(BIT, 1) AS [IsShared]
                  FROM DeliverJob DJ
                       JOIN dbo.Job J ON DJ.Job = J.ID
                       JOIN dbo.ColorProofCoZoneWorkflow CPCZW ON CPCZW.ID = DJ.CPWorkflow
                       JOIN dbo.SharedColorProofWorkflow SCPW ON SCPW.ColorProofCoZoneWorkflow = CPCZW.ID
                       CROSS APPLY (SELECT U.GivenName,
										   U.Account
									FROM dbo.[User] U
									JOIN Account ACC on U.Account = ACC.ID
								    WHERE U.Account = (SELECT Account FROM [User] WHERE ID = SCPW.[USER]) AND U.ID = DJ.[Owner]
                                 ) [User] ( GivenName, Account )
                       JOIN dbo.ColorProofWorkflowInstanceView CPWIV ON SCPW.ColorProofCoZoneWorkflow = CPWIV.ColorProofCoZoneWorkflowID
                       JOIN dbo.ColorProofInstance CPI ON CPWIV.ColorProofInstance = CPI.ID
                       JOIN dbo.[User] CPUO ON CPI.Owner = CPUO.ID
                       JOIN dbo.Account AC ON CPUO.Account = AC.ID                    
                       JOIN dbo.AccountStatus ACS ON AC.[Status] = ACS.ID
                       CROSS APPLY (SELECT  DJS.[Key]
									FROM  dbo.DeliverJobStatus DJS
									WHERE  DJS.ID = DJ.Status
									) Status ( [Key] ) 
                  WHERE ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                          )
                          AND DJ.IsDeleted = 0
                          AND J.Account <> @P_Account
			              AND ACS.[Key] != 'D'
                          AND AC.ID = @P_Account 
                          AND SCPW.IsAccepted = 1 
                          AND SCPW.IsEnabled = 1
                          AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' 
                  UNION
                --- deliver jobs for manager and moderator users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND DJ.[owner] IN (
                            SELECT  *
                            FROM    dbo.[GetUserListWhereBelongs](@P_User) )
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) IN (
                            'MAN', 'MOD' ) -- moderator and manager should see their jobs + jobs of the users that belongs to the same user group                  
                )
                
            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.CreatedDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.CreatedDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.Title
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.Title
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.[User]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.[User]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Pages]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Pages]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CpName]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CpName]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[Workflow]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[Workflow]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[StatusKey]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[StatusKey]
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset
        
        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT

    END
GO

ALTER TABLE [dbo].[Approval]
ADD LockProofWhenFirstDecisionsMade bit NOT NULL DEFAULT(0)
GO

-- get all collaborators except those who have no access to Collaborate module
ALTER PROCEDURE [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]
	-- Add the parameters for the stored procedure here
	@P_AccountID INT,
	@P_LoggedUserID INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	DECLARE @ApproverAndReviewer INT
	DECLARE @Reviewer INT
	DECLARE @ReadOnlyr INT
	
	SELECT @ApproverAndReviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'ANR';
	SELECT @Reviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RVW';
	SELECT @ReadOnlyr = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RDO';
	
	SELECT 
		CONVERT (bit, 0) AS IsGroup,
		CONVERT (bit, 0) AS IsExternal,
		CONVERT (bit, 0) AS IsChecked,
		u.ID AS ID,
		0 AS [Count],
		
		-- IDs concatenated as string
		COALESCE(
			(SELECT DISTINCT ' cfg' + convert(varchar, ug.ID)
				FROM dbo.UserGroup ug
				INNER JOIN dbo.UserGroupUser ugu ON ugu.[User] = u.ID 
							AND ugu.UserGroup = ug.ID
				FOR XML PATH('') ),
			'')
			AS Members,
        
        -- name as given mane + family name
		u.GivenName + ' ' + u.FamilyName
			AS Name,
		
		-- get user role for colaborate module
		(SELECT acr.ID
			FROM dbo.ApprovalCollaboratorRole acr
			WHERE acr.ID =  CASE r.Name					
								WHEN 'Administrator' THEN @ApproverAndReviewer
								WHEN 'Manager' THEN @ApproverAndReviewer
								WHEN 'Moderator' THEN @ApproverAndReviewer
								WHEN 'Contributor' THEN @Reviewer
								ELSE @ReadOnlyr
							END)						
			AS [Role]
	FROM dbo.[User] u	
	INNER JOIN dbo.[UserRole] ur ON ur.[User] = u.ID
	INNER JOIN dbo.[Role] r ON ur.Role = r.ID
	WHERE Account = @P_AccountID 
			AND r.[Key] <> 'NON'
			AND r.AppModule = (SELECT am.ID 
							   FROM dbo.AppModule am
							   WHERE am.[Key] = 0)			
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'I')
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'D')
								
			AND ( 	(SELECT rl.[Key] 	
					FROM dbo.[User] u
					JOIN dbo.[UserRole] ur ON u.ID = ur.[User]
					JOIN dbo.[Role] rl ON ur.Role = rl.ID
					JOIN dbo.[AppModule] apm ON rl.AppModule = apm.ID
					WHERE apm.[Key] = 0 and u.ID = @P_LoggedUserID) != 'MOD'
					OR
					u.ID IN 
					(SELECT DISTINCT u.ID FROM dbo.[User] u
				     JOIN dbo.UserGroupUser ugu ON u.ID = ugu.[User]
                     JOIN dbo.UserGroupUser ug ON ugu.UserGroup = ug.UserGroup
                     JOIN dbo.UserGroup grp ON ug.UserGroup = grp.ID
                     WHERE ug.[User] = @P_LoggedUserID 
                     UNION
                     SELECT @P_LoggedUserID)				
				)
END

GO
  
CREATE TABLE [dbo].[ApprovalCustomDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Decision] [int] NOT NULL,
	[Name] [nvarchar](64) NULL,
CONSTRAINT [PK_ApprovalCustomDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
	
ALTER TABLE [dbo].[ApprovalCustomDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCustomDecision_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[ApprovalCustomDecision] CHECK CONSTRAINT [FK_ApprovalCustomDecision_Account]
GO

ALTER TABLE [dbo].[ApprovalCustomDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCustomDecision_Decision] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO

ALTER TABLE [dbo].[ApprovalCustomDecision] CHECK CONSTRAINT [FK_ApprovalCustomDecision_Decision]
GO

--Add sub menu item  General 
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'ApprovalCustomDecision', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          2 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'CLCD' , -- Key - nvarchar(4)
          'Approval Status' , -- Name - nvarchar(64)
          'Approval Status'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'COLS'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('CLCD')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

-------- Calculate approvals from recycle bin based on @P_ViewAllFromRecycle parameter ------ 
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_ViewAllFromRecycle bit = 0
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;	
	
	SELECT 
			SUM(result.AllCount) AS AllCount,
			SUM(result.OwnedByMeCount) AS OwnedByMeCount,
			SUM(result.SharedCount) AS SharedCount,
			SUM(result.RecentlyViewedCount) AS RecentlyViewedCount,
			SUM(result.ArchivedCount) AS ArchivedCount,
			SUM(resulT.RecycleCount) AS RecycleCount
	FROM (
			SELECT
				sum(case when js.[Key] != 'ARC' then 1 else 0 end) AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				(SELECT 
						COUNT(DISTINCT j.ID)
					FROM	ApprovalUserViewInfo auvi
						INNER JOIN Approval a
								ON a.ID = auvi.Approval 
						INNER JOIN Job j
								ON j.ID = a.Job 
						INNER JOIN JobStatus js
								ON js.ID = j.[Status]		
					WHERE	auvi.[User] =  @P_User
						AND j.Account = @P_Account
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC'
						AND (	SELECT MAX([ViewedDate]) 
								FROM ApprovalUserViewInfo vuvi
									INNER JOIN Approval av
										ON  av.ID = vuvi.[Approval]
									INNER JOIN Job vj
										ON vj.ID = av.Job	
								WHERE  av.Job = a.Job
									AND av.IsDeleted = 0
							) = auvi.[ViewedDate]
						AND EXISTS (
										SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
									)							
					) RecentlyViewedCount,			
				0 AS ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE	av.Job = a.Job
									AND av.IsDeleted = 0
									AND (
										   @P_ViewAll = 1 
											 OR
											 (
											   @P_ViewAll = 0 AND
											   EXISTS(	SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
													  )
											  )
										 )
						) = a.[Version] 
						
			UNION
				SELECT
				0 AS AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				0 AS RecentlyViewedCount,			
				sum(case when js.[Key] = 'ARC' then 1 else 0 end) ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE	av.Job = a.Job
									AND av.IsDeleted = 0
									AND  EXISTS( SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
									           )
						) = a.[Version] 
						
			UNION ----- select owned by me count
				SELECT
					0 AS AllCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,					
					0 AS ArchivedCount,	
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]			
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE	av.Job = a.Job AND
										av.[Owner] = @P_User 
										AND av.IsDeleted = 0											
							) = a.[Version]	
			UNION --- select shared approvals count
				SELECT
					0 AS AllCount,
					0 AS OwnedByMeCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]			
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE	av.Job = a.Job AND
										av.[Owner] != @P_User 
										AND av.IsDeleted = 0							
										AND EXISTS(	SELECT TOP 1 ac.ID
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
												   )														
							) = a.[Version]	
							
			UNION ------ select deleted items count
				SELECT 
					0 AS AllCount,
					0 AS OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
				    SUM(recycle.Approvals) AS RecycleCount
				FROM (
				          SELECT COUNT(a.ID) AS Approvals
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND a.DeletePermanently = 0
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (
								      ISNULL((SELECT TOP 1 ac.ID
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
											, 
											(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
											WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
										  ) IS NOT NULL				
									)
								AND (
									   @P_ViewAllFromRecycle = 1 
										 OR
										 (
										   @P_ViewAllFromRecycle = 0 AND
										   EXISTS(	SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User
												  )
										  )
									 )
						 
						UNION
							SELECT COUNT(f.ID) AS Approvals
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
									AND f.Creator = @P_User								
						
					) recycle
	) result
	OPTION(OPTIMIZE FOR(@P_User UNKNOWN, @P_Account UNKNOWN))	

END
GO


-- Added the ViewAll parameter and retrieve data based on it ----
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,
	@P_ViewAll bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval)
	SELECT 
			a.Approval,
			IsApproval
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						)
					AND (
						 @P_ViewAll = 1 
						 OR
						 ( 
							@P_ViewAll = 0 AND 	EXISTS (
														 SELECT TOP 1 ac.ID 
														 FROM ApprovalCollaborator ac 
														 WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
														)
					     )	
					    )	 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE						
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.Approval
		END ASC,
		CASE						
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.Approval
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.IsChangesComplete,
			result.ApprovalApprovedDate
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT Progress FROM ApprovalPage ap
							WHERE ap.Approval = a.ID and ap.Number = 1), 0 ) 					
					END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			(SELECT CASE
			 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL 
			     THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
			     ELSE CONVERT(bit,0)
			 END) as IsChangesComplete,
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			JOIN Approval a ON fa.Approval = a.ID	
	                    WHERE fa.Folder = f.ID AND a.DeletePermanently = 0
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					CONVERT(bit, 0) as IsChangesComplete,
					NULL as ApprovalApprovedDate
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 		
	SET @P_RecCount = 0

END
GO

----------------------------------------------------------------------------------lAUNCHED ON DEBUG
----------------------------------------------------------------------------------lAUNCHED ON ReleaseEU







