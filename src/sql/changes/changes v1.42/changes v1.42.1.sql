  use GMGCoZone;
  
-- Add Locale column to the ApprovalCustomDecision table
ALTER TABLE dbo.[ApprovalCustomDecision]
  ADD Locale INT
  GO
  
  UPDATE acd
  SET acd.Locale = a.Locale
  FROM dbo.[ApprovalCustomDecision] acd
  JOIN dbo.[Account] a ON acd.Account = a.ID
  GO
  
  ALTER TABLE dbo.[ApprovalCustomDecision] ALTER COLUMN [Locale] INTEGER NOT NULL
  GO
  
ALTER TABLE [dbo].[ApprovalCustomDecision]  WITH CHECK ADD FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO

--Add new table for keeping track of active/inactive approval statuses
USE [GMGCoZone]
GO

CREATE TABLE [dbo].[InactiveApprovalStatuses](
	[ID] INT IDENTITY(1,1) NOT NULL,
	[Account] INT NOT NULL,
	[ApprovalDecision] INT NOT NULL
	CONSTRAINT [PK_ID] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[InactiveApprovalStatuses]  WITH CHECK ADD FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[InactiveApprovalStatuses]  WITH CHECK ADD FOREIGN KEY([ApprovalDecision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO

---------------------------------------------------------------------Launched on TestEU
--Add a new table for storing ProofStudio sessions
USE [GMGCoZone]
GO

CREATE TABLE [dbo].[ProofStudioSessions](
	[ID] INT IDENTITY(1,1) NOT NULL,
	[Guid] nvarchar(36) NOT NULL,
    [CreatedDate] datetime NOT NULL,
	[IsCompleted] Bit NOT NULL,
	[User] INT NOT NULL,
	CONSTRAINT [PK_GUID] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ProofStudioSessions] WITH CHECK ADD FOREIGN KEY([USER])
REFERENCES [dbo].[USER] ([ID])
GO

--Update Notification Item with reference column to new ProofStudioSession table
ALTER TABLE dbo.[NotificationItem]
  ADD PSSessionID INT
  GO
  
ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD FOREIGN KEY([PSSessionID])
REFERENCES [dbo].[ProofStudioSessions] ([ID])
GO

--Update NotificationSchedule table with the new send emails per ProofStudio session
ALTER TABLE [dbo].[NotificationSchedule]
ADD NotificationPerPSSession Bit NOT NULL DEFAULT 0
GO

----------------------------------------------------------------------------
-- Update approvals count taking into consideration the access on each phase
----------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_ViewAllFromRecycle bit = 0,
	@P_UserRoleKey NVARCHAR(4) = ''
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;	
	
	SELECT 
			SUM(result.AllCount) AS AllCount,
			SUM(result.OwnedByMeCount) AS OwnedByMeCount,
			SUM(result.SharedCount) AS SharedCount,
			SUM(result.RecentlyViewedCount) AS RecentlyViewedCount,
			SUM(result.ArchivedCount) AS ArchivedCount,
			SUM(resulT.RecycleCount) AS RecycleCount
	FROM (
			SELECT
				sum(case when js.[Key] != 'ARC' then 1 else 0 end) AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				(SELECT 
					COUNT(DISTINCT j.ID)
				FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a
							ON a.ID = auvi.Approval 
					INNER JOIN Job j
							ON j.ID = a.Job 
					INNER JOIN JobStatus js
							ON js.ID = j.[Status]					
				WHERE	auvi.[User] =  @P_User
					AND j.Account = @P_Account
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC'
					AND auvi.[ViewedDate] = (
											  SELECT max(vuvi.[ViewedDate]) 
											  FROM ApprovalUserViewInfo vuvi
											  INNER JOIN Approval av ON  av.ID = vuvi.[Approval]				
											  WHERE av.Job = a.Job			 					
											) 
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
						)	
					AND (EXISTS(
								SELECT TOP 1 ac.ID 
								FROM ApprovalCollaborator ac 
								WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
							  )
							 OR ISNULL(j.JobOwner, 0) = @P_User
						)						
					) RecentlyViewedCount,			
				0 AS ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]				
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND a.[Version] =(SELECT MAX([Version]) AS [Version] 
									  FROM Approval av 
									  WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND (
												   @P_ViewAll = 1 
													 OR
													 (
													   @P_ViewAll = 0 AND 											   
														(@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(	SELECT TOP 1 ac.ID 
																									FROM ApprovalCollaborator ac 
																									WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																								  )
														)
													  )
												 )
									)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
						)
						
			UNION
				SELECT
				0 AS AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				0 AS RecentlyViewedCount,			
				sum(case when js.[Key] = 'ARC' then 1 else 0 end) ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]						
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND a.[Version] = (  SELECT MAX([Version]) AS [Version] 
										 FROM Approval av 
										 WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND (
													   @P_ViewAll = 1 
														 OR
														 (
														   @P_ViewAll = 0 AND 											   
															(@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(	SELECT TOP 1 ac.ID 
																										FROM ApprovalCollaborator ac 
																										WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																									  )
															)
														  )
													 )
										)
						
			UNION ----- select owned by me count
				SELECT
					0 AS AllCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,					
					0 AS ArchivedCount,	
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]							
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND a.[Version] = (SELECT MAX([Version]) AS [Version] 
										   FROM Approval av											
										   WHERE av.Job = a.Job AND (ISNULL(j.JobOwner, 0) = @P_User OR av.[Owner] = @P_User))
						AND (
								@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)	
			UNION --- select shared approvals count
				SELECT
					0 AS AllCount,
					0 AS OwnedByMeCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]									
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND a.[Version]	= (SELECT MAX([Version]) AS [Version] 
										   FROM Approval av										   
										   WHERE av.Job = a.Job AND ( ISNULL(j.JobOwner, 0) != @P_User AND av.[Owner] != @P_User)
																	  AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(av.CurrentPhase, 0)
																				)	
										   )					
						AND (
								@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)	
							
			UNION ------ select deleted items count
				SELECT 
					0 AS AllCount,
					0 AS OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
				    SUM(recycle.Approvals) AS RecycleCount
				FROM (
				          SELECT COUNT(a.ID) AS Approvals
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND a.DeletePermanently = 0
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (
								      ISNULL((SELECT TOP 1 ac.ID
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
											, 
											(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
											WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
										  ) IS NOT NULL				
									)
								AND (
									 @P_ViewAllFromRecycle = 1 
									 OR
									 ( 
										@P_ViewAllFromRecycle = 0 AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS (SELECT TOP 1 ac.ID 
																												  FROM ApprovalCollaborator ac 
																												  WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
																											      )
																											  
														               )
									 )	
								)	
						 
						UNION
							SELECT COUNT(f.ID) AS Approvals
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
									AND f.Creator = @P_User								
						
					) recycle
	) result
	OPTION(OPTIMIZE FOR(@P_User UNKNOWN, @P_Account UNKNOWN))	

END

GO


------ Approval phasing - add columns
ALTER TABLE [dbo].[ApprovalPhase]
ADD [ShowAnnotationsToUsersOfOtherPhases] BIT NOT NULL DEFAULT(1)
GO

ALTER TABLE [dbo].[ApprovalJobPhase]
ADD [ShowAnnotationsToUsersOfOtherPhases] BIT NOT NULL DEFAULT(1)
GO

ALTER TABLE [dbo].[ApprovalPhase]
ADD [ShowAnnotationsOfOtherPhasesToExternalUsers] BIT NOT NULL DEFAULT(0)
GO

ALTER TABLE [dbo].[ApprovalJobPhase]
ADD [ShowAnnotationsOfOtherPhasesToExternalUsers] BIT NOT NULL DEFAULT(0)
GO

ALTER TABLE [dbo].[ExternalCollaborator]
ADD [ShowAnnotationsOfAllPhases] BIT NOT NULL DEFAULT(0)
GO

--Alter Procedure to include per phase approvals
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)						
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL and
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0								
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
								(@P_TopLinkId = 1 
										AND EXISTS(
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												  )
									)									
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
							  )
							)
						)		
			) = a.[Version]	)
			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (
														 @P_ViewAll = 1 
													 OR
													 ( 
														@P_ViewAll = 0 AND 
														(
															(@P_TopLinkId = 1 AND (@P_User = j.JobOwner OR EXISTS(
																													SELECT TOP 1 ac.ID 
																													FROM ApprovalCollaborator ac 
																													WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																												  )
																					)
															 )									
															OR 
																(@P_TopLinkId = 2 
																	AND @P_User = ISNULL(j.JobOwner,0)
																)
															OR 
																(@P_TopLinkId = 3 
																	AND ISNULL(j.JobOwner,0) != @P_User
																	AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																				)
																)
														  )
														)
													)										 		
								      )
				)
			)
	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT Progress FROM ApprovalPage ap
							WHERE ap.Approval = a.ID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.Approval aproval ON ap.Approval = aproval.ID
							 WHERE aproval.ID = t.ApprovalID AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = aproval.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																														WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase )
																												)
																			           )
																					   
															       )	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,							
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = a.ID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = a.ID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = a.ID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID

	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0

END
GO

-- Alter Procedure to include per phase approvals ----
ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			INNER JOIN Job j 
					ON j.ID = a.Job
			INNER JOIN JobStatus js 
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)									
	WHERE	j.Account = @P_Account					
			AND a.IsDeleted = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND		auvi.[User] = @P_User
			AND ((a.CurrentPhase IS NULL AND(	SELECT MAX([ViewedDate]) 
												FROM ApprovalUserViewInfo vuvi
													INNER JOIN Approval av
														ON  av.ID = vuvi.[Approval]
													INNER JOIN Job vj
														ON vj.ID = av.Job	
												WHERE  av.Job = a.Job
													AND av.IsDeleted = 0
											) = auvi.[ViewedDate]
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
													)
			)						
		   OR (a.CurrentPhase IS NOT NULL  AND a.CurrentPhase = (SELECT av.CurrentPhase 
																  FROM ApprovalUserViewInfo avi
																  INNER JOIN Approval av ON av.ID =  avi.Approval
																  WHERE av.Job = a.Job
																  AND avi.[ViewedDate] = auvi.[ViewedDate]
																		AND avi.[ViewedDate] =( SELECT MAX([ViewedDate]) 
																								FROM ApprovalUserViewInfo vuvi
																								INNER JOIN Approval avv
																									ON  avv.ID = vuvi.[Approval]
																								INNER JOIN Job vj
																									ON vj.ID = avv.Job	
																								WHERE  avv.Job = a.Job
																								AND avv.IsDeleted = 0)
																		AND (EXISTS(
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Phase = av.CurrentPhase
																				  )
																				  OR ISNULL(j.JobOwner, 0) = @P_User
																			)
																		 
																)
				)
			)
	ORDER BY 
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE						
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
		END DESC,		
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT DISTINCT	t.ID,
			a.ID AS Approval,	
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],							 
			js.[Key] AS [Status],	
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			a.[Version] AS MaxVersion,						
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
				FROM Approval av
				WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.Approval aproval ON ap.Approval = aproval.ID
							 WHERE aproval.ID = t.ApprovalID AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = aproval.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																														WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase )
																												)
																			           )
																					   
															       )	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,			
			(SELECT CASE 
				WHEN a.LockProofWhenAllDecisionsMade = 1
					THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
				ELSE
					NULL
				END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0) 
				THEN(						     
						(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
						   THEN
								(SELECT TOP 1 appcd.[Key]
									FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
											JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
											WHERE acd.Approval = a.ID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
											ORDER BY ad.[Priority] ASC
										) appcd
								)
							ELSE
							(							    
								(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
											                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
								 THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
								     )
								 ELSE '0'
								 END 
								 )   			   
							)
							END
						)								
					)		
				WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
				  THEN
					(SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				ELSE
				 (SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				END	
				), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = a.ID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = a.ID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = a.ID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			JOIN @TempItems t 
					ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
			INNER JOIN Job j
					ON j.ID = a.Job
			INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	  
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
		SET @P_RecCount = 0 
END
GO

-- Modify delete approval procedure to delete job status change logs
--------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	SET XACT_ABORT ON;  
	BEGIN TRANSACTION
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		 --Delete annotations from ApprovalAnnotationStatusChangeLog
		DELETE [dbo].[ApprovalAnnotationStatusChangeLog]		 
		FROM   [dbo].[ApprovalAnnotationStatusChangeLog] aascl
				JOIN [dbo].[ApprovalAnnotation] aa
					ON aascl.Annotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id	    
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval]
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalJobPhaseApproval
		DELETE FROM [dbo].[ApprovalJobPhaseApproval]	
				WHERE Approval = @P_Id	
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[ApprovalSetting] 
				WHERE Approval = @P_Id
				
		-- Delete from ApprovalCustomICC
		DELETE FROM [dbo].[ApprovalCustomICCProfile] 
				WHERE Approval = @P_Id
		
		-- Delete from LabColorMeasurements
		DELETE [dbo].[LabColorMeasurement] 
		FROM	[dbo].[LabColorMeasurement] lbm
				JOIN [dbo].[ApprovalSeparationPlate] asp
					ON lbm.ApprovalSeparationPlate = asp.ID
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		--Delete from SoftProofing Session Error
		DELETE [dbo].[SoftProofingSessionError]
		FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
		WHERE ap.Approval = @P_Id
				
		--Delete from ApprovalEmbedded Profile foreign keys to approval
		DELETE [dbo].[ApprovalEmbeddedProfile]
		FROM [dbo].[ApprovalEmbeddedProfile] apep			
		WHERE apep.Approval = @P_Id			
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalUserRecycleBinHistory
		DELETE FROM [dbo].[ApprovalUserRecycleBinHistory]
		WHERE Approval = @P_Id
		
		-- Delete from UserApprovalReminderEmailLog
		DELETE FROM [dbo].[UserApprovalReminderEmailLog]
		WHERE Approval = @P_Id
		
		-- Delete from SoftProofingSessionParams
		DELETE FROM [dbo].[SoftProofingSessionParams]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
			
				-- Delete all phases related with this job
				DELETE [dbo].[ApprovalJobPhase] 
				FROM [dbo].[ApprovalJobPhase] ajp
					JOIN [dbo].[ApprovalJobWorkflow] ajw ON ajp.[ApprovalJobWorkflow] = ajw.ID
				WHERE ajw.Job = @Job_ID
				
				-- Delete all approval workflows related with this job
				DELETE FROM [dbo].[ApprovalJobWorkflow]
				WHERE Job = @Job_ID
				
				-- Delete all job status change logs
				DELETE FROM [dbo].[JobStatusChangeLog]
				WHERE Job = @Job_ID
			
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
	    COMMIT TRANSACTION
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		IF (XACT_STATE()) = -1 
		BEGIN 
			SET @P_Success = ERROR_MESSAGE()
			ROLLBACK TRANSACTION;  
		END
		
		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO

---------------------------------------------------------------------Launched on StageEU
---------------------------------------------------------------------Launched on TEST
---------------------------------------------------------------------Launched on NewProduction