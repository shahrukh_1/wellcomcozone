USE [GMGCoZone]
GO

INSERT INTO [GMGCoZone].[dbo].[Locale] VALUES ('pl-PL','Polish - Poland')
GO
INSERT INTO [GMGCoZone].[dbo].[Locale] VALUES ('da-DK','Danish - Denmark')
GO
INSERT INTO [GMGCoZone].[dbo].[Locale] VALUES ('nl-NL','Dutch - The Netherlands')
GO
INSERT INTO [GMGCoZone].[dbo].[Locale] VALUES ('nn-NO','Norwegian (Nynorsk) - Norway')
GO