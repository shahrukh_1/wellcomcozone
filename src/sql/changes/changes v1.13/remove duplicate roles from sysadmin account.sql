USE GMGCoZone

DELETE  dbo.UserRole
WHERE   ID IN ( SELECT  UR.ID
                FROM    dbo.UserRole UR
                        INNER JOIN dbo.Role R ON UR.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.[User] U ON UR.[User] = U.ID
                        INNER JOIN dbo.Account A ON U.Account = A.ID
                        INNER JOIN dbo.AccountType AT ON A.AccountType = AT.ID
                WHERE   AT.[Key] = 'GMHQ'
                        AND AM.[Key] != 4 )
GO