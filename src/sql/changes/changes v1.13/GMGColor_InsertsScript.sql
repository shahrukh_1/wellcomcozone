USE GMGCoZone
GO
IF NOT EXISTS ( SELECT  RPET.ID
                FROM    dbo.RolePresetEventType RPET
                        INNER JOIN dbo.EventType ET ON RPET.EventType = ET.ID
                        INNER JOIN dbo.Role R ON RPET.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.Preset P ON RPET.Preset = P.ID
                WHERE   AM.[Key] = 4			-- syadmin module
                        AND ET.[Key] = 'MPC'	-- plan was changed
                        AND P.[Key] = 'M'		-- low
                        AND R.[Key] = 'HQA' ) 
    BEGIN
        INSERT  INTO dbo.RolePresetEventType
                ( Role ,
                  Preset ,
                  EventType 
                )
                SELECT  ( SELECT    R.ID
                          FROM      dbo.Role R
                          WHERE     R.[Key] = 'HQA'
                        ) AS [Role] ,
                        ( SELECT    P.ID
                          FROM      dbo.Preset P
                          WHERE     P.[Key] = 'M'
                        ) AS [Preset] ,
                        ( SELECT    ET.ID
                          FROM      dbo.EventType ET
                          WHERE     Et.[Key] = 'MPC'
                        ) AS [EventType] 
    END

IF NOT EXISTS ( SELECT  RPET.ID
                FROM    dbo.RolePresetEventType RPET
                        INNER JOIN dbo.EventType ET ON RPET.EventType = ET.ID
                        INNER JOIN dbo.Role R ON RPET.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.Preset P ON RPET.Preset = P.ID
                WHERE   AM.[Key] = 4			-- syadmin module
                        AND ET.[Key] = 'UUA'	-- user was added
                        AND P.[Key] = 'M'		-- low
                        AND R.[Key] = 'HQA' ) 
    BEGIN
        PRINT 'INSERTING UUA, M, HQA'
        INSERT  INTO dbo.RolePresetEventType
                ( Role ,
                  Preset ,
                  EventType 
                )
                SELECT  ( SELECT    R.ID
                          FROM      dbo.Role R
                          WHERE     R.[Key] = 'HQA'
                        ) AS [Role] ,
                        ( SELECT    P.ID
                          FROM      dbo.Preset P
                          WHERE     P.[Key] = 'M'
                        ) AS [Preset] ,
                        ( SELECT    ET.ID
                          FROM      dbo.EventType ET
                          WHERE     Et.[Key] = 'UUA'
                        ) AS [EventType] 
    END

IF NOT EXISTS ( SELECT  RPET.ID
                FROM    dbo.RolePresetEventType RPET
                        INNER JOIN dbo.EventType ET ON RPET.EventType = ET.ID
                        INNER JOIN dbo.Role R ON RPET.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.Preset P ON RPET.Preset = P.ID
                WHERE   AM.[Key] = 4			-- syadmin module
                        AND ET.[Key] = 'MPC'	-- plan was changed
                        AND P.[Key] = 'L'		-- low
                        AND R.[Key] = 'HQA' ) 
    BEGIN
        PRINT 'INSERTING MPC, L, HQA'
        INSERT  INTO dbo.RolePresetEventType
                ( Role ,
                  Preset ,
                  EventType 
                )
                SELECT  ( SELECT    R.ID
                          FROM      dbo.Role R
                          WHERE     R.[Key] = 'HQA'
                        ) AS [Role] ,
                        ( SELECT    P.ID
                          FROM      dbo.Preset P
                          WHERE     P.[Key] = 'L'
                        ) AS [Preset] ,
                        ( SELECT    ET.ID
                          FROM      dbo.EventType ET
                          WHERE     Et.[Key] = 'MPC'
                        ) AS [EventType] 
    END

IF NOT EXISTS ( SELECT  RPET.ID
                FROM    dbo.RolePresetEventType RPET
                        INNER JOIN dbo.EventType ET ON RPET.EventType = ET.ID
                        INNER JOIN dbo.Role R ON RPET.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.Preset P ON RPET.Preset = P.ID
                WHERE   AM.[Key] = 4			-- syadmin module
                        AND ET.[Key] = 'MPC'	-- plan was changed
                        AND P.[Key] = 'M'		-- low
                        AND R.[Key] = 'HQM' ) 
    BEGIN
        PRINT 'INSERTING MPC , M, HQM'
        INSERT  INTO dbo.RolePresetEventType
                ( Role ,
                  Preset ,
                  EventType 
                )
                SELECT  ( SELECT    R.ID
                          FROM      dbo.Role R
                          WHERE     R.[Key] = 'HQM'
                        ) AS [Role] ,
                        ( SELECT    P.ID
                          FROM      dbo.Preset P
                          WHERE     P.[Key] = 'M'
                        ) AS [Preset] ,
                        ( SELECT    ET.ID
                          FROM      dbo.EventType ET
                          WHERE     Et.[Key] = 'MPC'
                        ) AS [EventType] 
    END

IF NOT EXISTS ( SELECT  RPET.ID
                FROM    dbo.RolePresetEventType RPET
                        INNER JOIN dbo.EventType ET ON RPET.EventType = ET.ID
                        INNER JOIN dbo.Role R ON RPET.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.Preset P ON RPET.Preset = P.ID
                WHERE   AM.[Key] = 4			-- syadmin module
                        AND ET.[Key] = 'UUA'	-- plan was changed
                        AND P.[Key] = 'M'		-- low
                        AND R.[Key] = 'HQM' ) 
    BEGIN
        PRINT 'INSERTING UUA, M, HQM'
        INSERT  INTO dbo.RolePresetEventType
                ( Role ,
                  Preset ,
                  EventType 
                )
                SELECT  ( SELECT    R.ID
                          FROM      dbo.Role R
                          WHERE     R.[Key] = 'HQM'
                        ) AS [Role] ,
                        ( SELECT    P.ID
                          FROM      dbo.Preset P
                          WHERE     P.[Key] = 'M'
                        ) AS [Preset] ,
                        ( SELECT    ET.ID
                          FROM      dbo.EventType ET
                          WHERE     Et.[Key] = 'UUA'
                        ) AS [EventType] 
    END

IF NOT EXISTS ( SELECT  RPET.ID
                FROM    dbo.RolePresetEventType RPET
                        INNER JOIN dbo.EventType ET ON RPET.EventType = ET.ID
                        INNER JOIN dbo.Role R ON RPET.Role = R.ID
                        INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
                        INNER JOIN dbo.Preset P ON RPET.Preset = P.ID
                WHERE   AM.[Key] = 4			-- syadmin module
                        AND ET.[Key] = 'MPC'	-- plan was changed
                        AND P.[Key] = 'L'		-- low
                        AND R.[Key] = 'HQM' ) 
    BEGIN
        PRINT 'INSERTING MPC, L, HQM'
        INSERT  INTO dbo.RolePresetEventType
                ( Role ,
                  Preset ,
                  EventType 
                )
                SELECT  ( SELECT    R.ID
                          FROM      dbo.Role R
                          WHERE     R.[Key] = 'HQM'
                        ) AS [Role] ,
                        ( SELECT    P.ID
                          FROM      dbo.Preset P
                          WHERE     P.[Key] = 'L'
                        ) AS [Preset] ,
                        ( SELECT    ET.ID
                          FROM      dbo.EventType ET
                          WHERE     Et.[Key] = 'MPC'
                        ) AS [EventType] 
    END
GO
