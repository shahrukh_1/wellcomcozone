USE [GMGCoZone]
GO

--Changes for HasAnnotations check

ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_RecCount int OUTPUT	
)
AS
BEGIN
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID)
	SELECT 
			a.ID
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]								
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND (
						(@P_Status = 0) OR
						((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
						((@P_Status = 2) AND (js.[Key] = 'INP')) OR
						((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
						((@P_Status = 4) AND (js.[Key] = 'COM')) OR
						((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
						((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
						((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
						--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
						((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
						((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
						((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
						((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
						((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
						((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
						((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
					)				
			AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
									(@P_TopLinkId = 1 
										AND ( 
											EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
							  )
							)
						)		
			) = a.[Version]	
	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
							WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
					WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
							WHERE ap.Approval = a.ID ) = 100 THEN '100'
					WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
							WHERE ap.Approval = a.ID ) = 0 THEN '0'
					ELSE '50'
				END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			a.[Version] AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.[User] u ON aa.Creator = u.ID
							 INNER JOIN dbo.ApprovalCollaborator ac ON u.ID = ac.Collaborator
							 INNER JOIN dbo.ApprovalCollaboratorDecision acd ON u.ID = acd.Collaborator
							 INNER JOIN dbo.UserStatus us ON u.[Status] = us.ID
							 WHERE ap.Approval = t.ApprovalID AND (us.[Key] != 'D' OR acd.Decision IS NOT NULL)
							 UNION
							 SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN ExternalCollaborator ex ON aa.ExternalCreator = ex.ID
                             INNER JOIN SharedApproval sa ON ex.ID = sa.ExternalCollaborator
							 INNER JOIN dbo.ApprovalCollaboratorDecision acd ON ex.ID = acd.ExternalCollaborator							
							 WHERE ap.Approval = t.ApprovalID AND (ex.IsDeleted = 0 OR acd.Decision IS NOT NULL)	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,				
			(SELECT CASE
			 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL  
			     THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
			     ELSE CONVERT(bit,0)
			 END) as IsChangesComplete,
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
				CONVERT(bit,0) AS AllCollaboratorsDecisionRequired	
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID

	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0

END

GO


--Alter procedure for returning shared deliver jobs

ALTER PROCEDURE [dbo].[SPC_ReturnDeliversPageInfo]
    (
      @P_Account INT ,
      @P_User INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;
        
        DECLARE @Delivers TABLE
            (
              ID INT ,
              [Title] NVARCHAR(128) ,
              [User] NVARCHAR(64) ,
              [Pages] NVARCHAR(50) ,
              [CPName] NVARCHAR(50) ,
              [Workflow] NVARCHAR(256) ,
              [CreatedDate] DATETIME ,
              [StatusKey] INT ,
              [Account] INT ,
              [OwnUser] INT,
              [IsShared] BIT
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  Title ,
                  [User] ,
                  Pages ,
                  CPName ,
                  Workflow ,
                  CreatedDate ,
                  StatusKey ,
                  Account ,
                  OwnUser,
                  IsShared
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] = @P_User
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'CON' -- contributor should only see his jobs
                  UNION
                --- deliver jobs for admin users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' -- admin should see all the jobs from current account
                  UNION
                  --- deliver shared jobs for admins -----
                  SELECT DISTINCT	DJ.ID ,
									J.Title ,
									[User].GivenName AS [User] ,
									DJ.Pages ,
									CPI.SystemName AS [CPName] ,
									CPCZW.Name AS [Workflow] ,
									DJ.CreatedDate ,
									Status.[Key] AS [StatusKey] ,
									J.Account AS [Account] ,
									DJ.[owner] AS [OwnUser],
									CONVERT(BIT, 1) AS [IsShared]
                  FROM DeliverJob DJ
                       JOIN dbo.Job J ON DJ.Job = J.ID
                       JOIN dbo.ColorProofCoZoneWorkflow CPCZW ON CPCZW.ID = DJ.CPWorkflow
                       JOIN dbo.SharedColorProofWorkflow SCPW ON SCPW.ColorProofCoZoneWorkflow = CPCZW.ID
                       CROSS APPLY ( SELECT U.GivenName,
											U.Account
									FROM dbo.[User] U
									WHERE U.ID = DJ.[Owner] AND U.ID IN (SELECT  * FROM dbo.[GetUserListWhereBelongs](SCPW.[User]))
                                 ) [User] ( GivenName, Account )
                       JOIN dbo.ColorProofWorkflowInstanceView CPWIV ON SCPW.ColorProofCoZoneWorkflow = CPWIV.ColorProofCoZoneWorkflowID
                       JOIN dbo.ColorProofInstance CPI ON CPWIV.ColorProofInstance = CPI.ID
                       JOIN dbo.[User] CPUO ON CPI.Owner = CPUO.ID
                       JOIN dbo.Account AC ON CPUO.Account = AC.ID                    
                       JOIN dbo.AccountStatus ACS ON AC.[Status] = ACS.ID
                       CROSS APPLY (SELECT  DJS.[Key]
									FROM  dbo.DeliverJobStatus DJS
									WHERE  DJS.ID = DJ.Status
									) Status ( [Key] ) 
                  WHERE ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                          )
                          AND DJ.IsDeleted = 0
                          AND J.Account <> @P_Account
			              AND ACS.[Key] != 'D'
                          AND AC.ID = @P_Account 
                          AND SCPW.IsAccepted = 1 
                          AND SCPW.IsEnabled = 1
                          AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' 
                  UNION
                --- deliver jobs for manager and moderator users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND DJ.[owner] IN (
                            SELECT  *
                            FROM    dbo.[GetUserListWhereBelongs](@P_User) )
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) IN (
                            'MAN', 'MOD' ) -- moderator and manager should see their jobs + jobs of the users that belongs to the same user group                  
                )
                
            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.CreatedDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.CreatedDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.Title
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.Title
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.[User]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.[User]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Pages]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Pages]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CpName]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CpName]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[Workflow]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[Workflow]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[StatusKey]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[StatusKey]
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset
        
        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT

    END

GO

-----------------------------------------------------------------------------------
-- Add Return Delivers Page View
ALTER VIEW [dbo].[ReturnDeliversPageView] 
AS 
SELECT  0 AS [ID] ,
'' AS [Title] ,
'' AS [User] ,
'' AS [Pages] ,
'' AS [CPName] ,
'' AS [Workflow] ,
GETDATE() AS [CreatedDate] ,
0 AS [StatusKey],
CONVERT(BIT, 0) AS [IsShared]

GO

CREATE TABLE [dbo].[DeliverJobData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeliverJob] [int] NOT NULL,
	[EmailOptionalMsg] nvarchar(MAX),
	CONSTRAINT [PK_DeliverJobData] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DeliverJobData]
ADD CONSTRAINT [FK_DeliverJobData_DeliverJob] FOREIGN KEY ([DeliverJob]) REFERENCES [dbo].[DeliverJob] ([ID])    
GO

ALTER PROCEDURE [dbo].[SPC_DeleteFolder] (
	@P_Id INT,
    @P_Modifier INT
 )
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY
	
	   DECLARE @FolderCreator INT;
	   DECLARE @ApprovalID INT, @ApprovalOwner INT;
	   
	   SET @FolderCreator = (SELECT Creator FROM Folder WHERE ID = @P_Id)
	   
		-- Delete from FolderCollaborator 
		DELETE FROM	[dbo].[FolderCollaborator]
				WHERE Folder = @P_Id
		
		-- Delete from FolderCollaboratorGroup 
		DELETE FROM	[dbo].[FolderCollaboratorGroup] 
				WHERE Folder = @P_Id
			
		DECLARE folder_cursor CURSOR
		FOR SELECT a.ID, a.[Owner] FROM [dbo].[Approval] a INNER JOIN [dbo].[FolderApproval] fa ON a.ID = fa.Approval WHERE fa.Folder = @P_Id
		
		OPEN folder_cursor
		
		FETCH NEXT
		FROM folder_cursor INTO @ApprovalID, @ApprovalOwner 
			WHILE @@FETCH_STATUS = 0
			BEGIN
						
			IF @ApprovalOwner = @P_Modifier -- mark approval to be deleted only if the user that made the action is approval owner
				BEGIN
					UPDATE [dbo].[Approval]
					SET [Version] = 0, [DeletePermanently] = 1
				    WHERE ID = @ApprovalID
				END
			
			IF @FolderCreator = @P_Modifier -- remove the relation with this folder for the approvals where the user that made the action is not the owner
				BEGIN
					EXECUTE [dbo].[SPC_MoveApproval] @ApprovalID, 0
				END		
			
			
			FETCH NEXT
			FROM folder_cursor INTO @ApprovalID, @ApprovalOwner
			END
		CLOSE folder_cursor
		DEALLOCATE folder_cursor
		
		--Mark parent as null for child folders
		UPDATE [dbo].[Folder]
		SET Parent = NULL
		where Parent = @P_Id	
 
	    -- Delete from Folder
		DELETE FROM [dbo].[Folder]
		WHERE ID = @P_Id	    
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END

GO

-------- Exclude approvals marked to be permanently deleted ------ 
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;	
	
	SELECT 
			SUM(result.AllCount) AS AllCount,
			SUM(result.OwnedByMeCount) AS OwnedByMeCount,
			SUM(result.SharedCount) AS SharedCount,
			SUM(result.RecentlyViewedCount) AS RecentlyViewedCount,
			SUM(result.ArchivedCount) AS ArchivedCount,
			SUM(resulT.RecycleCount) AS RecycleCount
	FROM (
			SELECT
				sum(case when js.[Key] != 'ARC' then 1 else 0 end) AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				(SELECT 
						COUNT(DISTINCT j.ID)
					FROM	ApprovalUserViewInfo auvi
						INNER JOIN Approval a
								ON a.ID = auvi.Approval 
						INNER JOIN Job j
								ON j.ID = a.Job 
						INNER JOIN JobStatus js
								ON js.ID = j.[Status]		
					WHERE	auvi.[User] =  @P_User
						AND j.Account = @P_Account
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC'
						AND (	SELECT MAX([ViewedDate]) 
								FROM ApprovalUserViewInfo vuvi
									INNER JOIN Approval av
										ON  av.ID = vuvi.[Approval]
									INNER JOIN Job vj
										ON vj.ID = av.Job	
								WHERE  av.Job = a.Job
									AND av.IsDeleted = 0
							) = auvi.[ViewedDate]
						AND EXISTS (
										SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
									)							
					) RecentlyViewedCount,			
				0 AS ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE	av.Job = a.Job
									AND av.IsDeleted = 0
									AND (
										   @P_ViewAll = 1 
											 OR
											 (
											   @P_ViewAll = 0 AND
											   EXISTS(	SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
													  )
											  )
										 )
						) = a.[Version] 
						
			UNION
				SELECT
				0 AS AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				0 AS RecentlyViewedCount,			
				sum(case when js.[Key] = 'ARC' then 1 else 0 end) ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE	av.Job = a.Job
									AND av.IsDeleted = 0
									AND  EXISTS( SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
									           )
						) = a.[Version] 
						
			UNION ----- select owned by me count
				SELECT
					0 AS AllCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,					
					0 AS ArchivedCount,	
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]			
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE	av.Job = a.Job AND
										av.[Owner] = @P_User 
										AND av.IsDeleted = 0											
							) = a.[Version]	
			UNION --- select shared approvals count
				SELECT
					0 AS AllCount,
					0 AS OwnedByMeCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]			
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE	av.Job = a.Job AND
										av.[Owner] != @P_User 
										AND av.IsDeleted = 0							
										AND EXISTS(	SELECT TOP 1 ac.ID
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
												   )														
							) = a.[Version]	
							
			UNION ------ select deleted items count
				SELECT 
					0 AS AllCount,
					0 AS OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
				    SUM(recycle.Approvals) AS RecycleCount
				FROM (
				          SELECT COUNT(a.ID) AS Approvals
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND a.DeletePermanently = 0
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (
								      ISNULL((SELECT TOP 1 ac.ID
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
											, 
											(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
											WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
										  ) IS NOT NULL				
									)
						 
						UNION
							SELECT COUNT(f.ID) AS Approvals
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
									AND f.Creator = @P_User								
						
					) recycle
	) result
	OPTION(OPTIMIZE FOR(@P_User UNKNOWN, @P_Account UNKNOWN))	

END

GO


-- Alter Procedure to change the pagination to more efficient method ----
ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID)
	SELECT 
			a.ID
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID			
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] = 'ARC'
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND EXISTS (
									SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
								)
			) = a.[Version]	
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 WHERE ap.Approval = t.ApprovalID			 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,				
			(SELECT CASE
			 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL  
			     THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
			     ELSE CONVERT(bit,0)
			 END) as IsChangesComplete,
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t 
				ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	 
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
		SET @P_RecCount = 0
	 
END

GO

------------------------------------------------------------------------------------------------------ Launched on Debug
------------------------------------------------------------------------------------------------------ Launched on ReleaseEU











