USE GMGCoZone
GO

ALTER TABLE dbo.Company ADD
	Guid nvarchar(36) NOT NULL CONSTRAINT DF_Company_Guid DEFAULT (newid())
GO

INSERT  INTO dbo.ConversionStatus
        ( [Key], Name )
VALUES  ( 4, -- Key - int
          'Waiting'  -- Name - nvarchar(64)
          )


INSERT INTO dbo.PreflightStatus
        ( [Key], Name )
VALUES  ( 4, -- Key - int
          'Waiting'  -- Name - nvarchar(64)
          )
GO
