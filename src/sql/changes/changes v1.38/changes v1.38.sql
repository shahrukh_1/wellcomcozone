USE GMGCoZone
GO

-- Add account type role foreach role and each account type(if the AccountTypeRole exists it will not be added again)
DECLARE @roleId INT
DECLARE Roles CURSOR LOCAL FOR       
SELECT  R.ID FROM    dbo.Role R
OPEN Roles
FETCH NEXT FROM Roles INTO @roleId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @accountTypeId INT
        DECLARE AccountTypes CURSOR LOCAL FOR SELECT AT.ID FROM dbo.AccountType AT
        OPEN AccountTypes
        FETCH NEXT FROM AccountTypes INTO @accountTypeId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.AccountTypeRole
                       WHERE    AccountType = @accountTypeId
                                AND Role = @roleId
                     ) = 0
                     AND @accountTypeId != 1
                   )  -- ignore the ADMIN account type
                    BEGIN
                        INSERT  INTO dbo.AccountTypeRole
                                ( AccountType, Role )
                        VALUES  ( @accountTypeId, -- AccountType - int
                                  @roleId  -- Role - int
                                  )
                    END
                FETCH NEXT FROM AccountTypes INTO @accountTypeId
            END
        CLOSE AccountTypes
        DEALLOCATE AccountTypes
        FETCH NEXT FROM Roles INTO @roleId
    END
CLOSE Roles
DEALLOCATE Roles
GO  

--Alter UserRolesView
ALTER VIEW [dbo].[UserRolesView] AS
SELECT  U.ID ,
        U.GivenName ,
        u.FamilyName ,
        u.Username ,
        U.EmailAddress ,
        COALESCE(CR.[Key], 'NON') AS [CollaborateRole] ,
        COALESCE(DR.[Key], 'NON') AS [DeliverRole] ,
        COALESCE(AR.[Key], 'NON') AS [AdminRole] ,
        COALESCE(SR.[Key], 'NON') AS [SysAdminRole] ,
        u.DateLastLogin ,
        u.Account ,
        US.[Key] AS [UserStatus]
FROM    dbo.[User] AS U
        INNER JOIN dbo.UserStatus US ON U.Status = US.ID
        LEFT JOIN ( SELECT  CUR.[User] ,
                            CR.[Key]
                    FROM    dbo.UserRole CUR
                            INNER JOIN dbo.Role CR ON CUR.Role = CR.ID
                            INNER JOIN dbo.AppModule CAM ON CR.AppModule = CAM.ID
                                                            AND CAM.[Key] = 0
                  ) AS CR ON U.ID = CR.[User]
        LEFT JOIN ( SELECT  DUR.[User] ,
                            DR.[Key]
                    FROM    dbo.UserRole DUR
                            INNER JOIN dbo.Role DR ON DUR.Role = DR.ID
                            INNER JOIN dbo.AppModule DAM ON DR.AppModule = DAM.ID
                                                            AND DAM.[Key] = 2
                  ) AS DR ON U.ID = DR.[User]
        LEFT JOIN ( SELECT  AUR.[User] ,
                            AR.[Key]
                    FROM    dbo.UserRole AUR
                            INNER JOIN dbo.Role AR ON AUR.Role = AR.ID
                            INNER JOIN dbo.AppModule AAM ON AR.AppModule = AAM.ID
                                                            AND AAM.[Key] = 3
                  ) AS AR ON U.ID = AR.[User]
        LEFT JOIN ( SELECT  SUR.[User] ,
                            SR.[Key]
                    FROM    dbo.UserRole SUR
                            INNER JOIN dbo.Role SR ON SUR.Role = SR.ID
                            INNER JOIN dbo.AppModule SAM ON SR.AppModule = SAM.ID
                                                            AND SAM.[Key] = 4
                  ) AS SR ON U.ID = SR.[User]
GO

--alter billing report view
ALTER VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS CollaborateBillingPlanName,
0 AS CollaborateProofs,
'' AS CollaborateBillingCycleDetails,
0 AS CollaborateBillingPlan,
CONVERT(BIT, 0) AS CollaborateIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS CollaborateContractStartDate,
CONVERT(BIT, 0) as CollaborateIsMonthlyBillingFrequency,  
'' AS DeliverBillingPlanName,
0 AS DeliverProofs,
'' AS DeliverBillingCycleDetails,
0 AS DeliverBillingPlan,
CONVERT(BIT, 0) AS DeliverIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS DeliverContractStartDate,
CONVERT(BIT, 0) AS DeliverIsMonthlyBillingFrequency,
0.0 AS GPPDiscount,
0 AS Parent,
'' AS AccountPath

GO

USE [GMGCoZone]
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 04/06/2015 15:40:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

--alter billing report info procedure
ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        
         DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN dbo.AccountStatus ast ON a.[Status] = ast.ID 
                WHERE   Parent = @P_LoggedAccount
						AND a.NeedSubscriptionPlan = 1
                        AND a.IsTemporary = 0
						AND (ast.[Key] = 'A')
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR ( @P_BillingPlanList != '' AND (
																 EXISTS (SELECT    val
																		 FROM      dbo.splitString(@P_BillingPlanList, ',')
																		 where val in (
																					   SELECT bp.SelectedBillingPlan from dbo.AccountSubscriptionPlan bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   UNION
																					   SELECT bp.SelectedBillingPlan from dbo.SubscriberPlanInfo bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   )
																		)																 
																)
								 )
                            )

        DECLARE @childAccounts TABLE ( ID INT )

        DECLARE @subAccount_ID INT
        DECLARE Cursor_SubAccounts CURSOR
        FOR SELECT ID FROM @subAccounts	
        OPEN Cursor_SubAccounts
        FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
        WHILE @@FETCH_STATUS = 0 
            BEGIN	
                INSERT  INTO @childAccounts
                            SELECT a.ID
                            from dbo.Account a
                            join dbo.AccountStatus acs on a.[Status] = acs.ID
							where a.Parent = @subAccount_ID AND acs.[Key] = 'A' AND a.IsTemporary = 0 AND a.NeedSubscriptionPlan = 1
                FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
            END

        CLOSE Cursor_SubAccounts
        DEALLOCATE Cursor_SubAccounts
        
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateContractStartDate DATETIME,
              CollaborateIsMonthlyBillingFrequency BIT,              
              CollaborateCycleDetails NVARCHAR(MAX) ,
              CollaborateModuleId INT ,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverContractStartDate DATETIME,
              DeliverIsMonthlyBillingFrequency BIT,
              DeliverCycleDetails NVARCHAR(MAX) ,
              DeliverModuleId INT 
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateContractStartDate,
                  CollaborateIsMonthlyBillingFrequency,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverContractStartDate,
                  DeliverIsMonthlyBillingFrequency,
                  DeliverCycleDetails ,
                  DeliverModuleId 
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.ContractStartDate
                             ELSE CASPBP.ContractStartDate
                        END AS CollaborateContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(CASPBP.IsMonthlyBillingFrequency, 0)
                        END AS CollaborateIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CSPIBP.ModuleId,
                                                              default)
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CASPBP.ModuleId,
                                                              default)
                               END, '0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.ContractStartDate
                             ELSE DASPBP.ContractStartDate
                        END AS DeliverContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(DASPBP.IsMonthlyBillingFrequency, 0)
                        END AS DeliverIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DASPBP.ModuleId,
                                                              default)
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DSPIBP.ModuleId,
                                                              default)
                               END, '0|0|0|0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId]
                        
                FROM    dbo.Account A
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency] )

                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency] )

                        WHERE  ( A.ID IN ( SELECT  *
									FROM    @subAccounts )
								 OR
								 A.ID IN ( SELECT   *
									FROM     @childAccounts )
								)
                
                
                                
     -- Get the accounts
        SET NOCOUNT OFF
        
        ;WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
        
       
        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.CollaborateContractStartDate,
				TBL.CollaborateIsMonthlyBillingFrequency, 
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                TBL.DeliverContractStartDate,
                TBL.DeliverIsMonthlyBillingFrequency,
                 ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
				 (SELECT PathString) AS AccountPath
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID	
                INNER JOIN AccountTree act ON a.ID = act.AccountID		
    END
GO

-----------------------------------------------------------------------------------
-- Alter Return Account Parameters view
ALTER VIEW [dbo].[ReturnAccountParametersView] 
AS 
SELECT  0 AS AccountID,
'' AS AccountName,
0 AS AccountTypeID,
'' AS AccountTypeName,
0 AS CollaborateBillingPlanId,
'' AS CollborateBillingPlanName,
0 AS DeliverBillingPlanId,
'' AS DeliverBillingPlanName,
0 AS AccountStatusID,
'' AS AccountStatusName,
0 AS LocationID,
'' AS LocationName


GO

ALTER PROCEDURE [dbo].[SPC_ReturnAccountParameters] ( @P_LoggedAccount INT )
AS 
    BEGIN
        SET NOCOUNT ON
        SELECT  A.ID AS [AccountId] ,
                A.NAME AS [AccountName] ,
                AT.ID AS AccountTypeID ,
                AT.Name AS AccountTypeName ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.ID
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.ID
                         END, 0) AS [CollaborateBillingPlanId] ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.Name
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.NAme
                         END, '') AS [CollborateBillingPlanName] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.ID
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.ID
                         END, 0) AS [DeliverBillingPlanId] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.Name
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.NAme
                         END, '') AS [DeliverBillingPlanName] ,
                AST.ID AS AccountStatusID ,
                ASt.Name AS AccountStatusName ,
                CTY.ID AS LocationID ,
                CTY.ShortName AS LocationName
        FROM    dbo.Account A
                INNER JOIN AccountType AT ON A.AccountType = AT.ID
                INNER JOIN AccountStatus AST ON A.[Status] = AST.ID
                INNER JOIN Company C ON C.Account = A.ID
                INNER JOIN Country CTY ON C.Country = CTY.ID
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     ASP.Account = A.ID
                            ) CASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     ASP.Account = A.ID
                            ) DASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     SPI.Account = A.ID
                            ) CSPI
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     SPI.Account = A.ID
                            ) DSPI
             WHERE ast.[Key] != 'D' AND a.Parent = @P_LoggedAccount AND a.IsTemporary = 0
    END
GO

ALTER PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId
           
        DECLARE @Count INT;		
		DECLARE @enableSoftProofingTools AS BIT = 0;       
		WITH TempTable
		AS
		( SELECT  count(1) as ResultCount,
							bpt.EnableSoftProofingTools
				  FROM      dbo.Account acc
							INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
							INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
				  GROUP BY  bpt.EnableSoftProofingTools
				  UNION ALL
				  SELECT    count(1) as ResultCount,
							bpt.EnableSoftProofingTools
				  FROM      dbo.Account acc
							INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
							INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
				  GROUP BY  bpt.EnableSoftProofingTools
		)
		SELECT @Count = sum(ResultCount), @enableSoftProofingTools = COALESCE(EnableSoftProofingTools, 0) FROM TempTable GROUP BY EnableSoftProofingTools
		
	    DECLARE @enableCollaborateSettings BIT = (SELECT CASE WHEN @Count > 0
														THEN CONVERT(BIT, 1)
														ELSE CONVERT(BIT, 0)
                                                    END
                                                  )

        --Remove Profile tab for users that don't have access to Admin Module        
        DECLARE @enableProfileMenu BIT = ( SELECT   CASE WHEN ( SELECT
                                                              COUNT(rl.ID)
                                                              FROM
                                                              dbo.UserRole ur
                                                              JOIN dbo.Role rl ON ur.Role = rl.ID
                                                              JOIN dbo.AppModule apm ON rl.AppModule = apm.ID
                                                              WHERE
                                                              ur.[User] = @P_userId
                                                              AND (apm.[Key] = 3 OR apm.[Key] = 4)
                                                              AND (RL.[Key] = 'ADM' OR rl.[Key] = 'HQM' OR rl.[Key] = 'HQA')
                                                              ) > 0
                                                         THEN CONVERT(BIT, 1)
                                                         ELSE CONVERT(BIT, 0)
                                                    END
                                         )
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
                                             
        IF ( @enableSoftProofingTools = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SESP'  -- MenuKey - varchar(4)
                          )  
            END
            
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
                          
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'PSVI'  -- MenuKey - varchar(4)
                          )
            END
                       
        IF(@enableCollaborateSettings = 0)
			BEGIN
				 INSERT  INTO @ignoreMenuKeys
							( MenuKey )
				 VALUES  ( 'CLGS'  -- MenuKey - varchar(4)
							)
            END
             
        IF ( @enableProfileMenu = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEPF' --MenuKey - varchar(4)
                          )
            END   
            
        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END
GO

--Add Database Support for Printer Companies
CREATE TABLE [dbo].[PrinterCompanyType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] nvarchar(128) NOT NULL,
	CONSTRAINT [PK_PrinterCompanyType] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[PrinterCompanyType]
           ([Key]
           ,[Name])
     VALUES (0,'Supplier'),
			(1,'Client'),
			(2,'Internal')
GO

CREATE TABLE [dbo].[PrinterCompany](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] nvarchar(128) NOT NULL,
	[Type] int NOT NULL,
	CONSTRAINT [PK_PrinterCompany] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PrinterCompany]
ADD CONSTRAINT [FK_PrinterCompany_Account] FOREIGN KEY ([Account]) REFERENCES [dbo].[Account] ([ID])    
GO

ALTER TABLE [dbo].[PrinterCompany]
ADD CONSTRAINT [FK_PrinterCompany_PrinterCompanyType] FOREIGN KEY ([Type]) REFERENCES [dbo].[PrinterCompanyType] ([ID])    
GO

ALTER TABLE dbo.[User]
ADD PrinterCompany int 
GO

ALTER TABLE [dbo].[User]
ADD CONSTRAINT [FK_User_PrinterCompany] FOREIGN KEY ([PrinterCompany]) REFERENCES [dbo].[PrinterCompany] ([ID])    
GO

USE [GMGCoZone]
GO
/****** Object:  StoredProcedure [dbo].[SPC_AccountsReport]    Script Date: 04/09/2015 10:48:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


ALTER PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN		
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath	
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		LEFT OUTER JOIN dbo.SubscriberPlanInfo spi
			ON a.ID = spi.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID	
		
	WHERE	
		s.[Key]<>'D' AND
		a.ID <> @P_LoggedAccount AND
		a.IsTemporary = 0 AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ','))
													 OR spi.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END
GO

USE [GMGCoZone]
GO

CREATE TABLE [dbo].[DeliverExternalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[GivenName] [nvarchar](64) NULL,
	[FamilyName] [nvarchar](64) NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[Creator] [int] NOT NULL
 CONSTRAINT [PK_DeliverExternalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[DeliverExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_DeliverExternalCollaborator_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[DeliverExternalCollaborator] CHECK CONSTRAINT [FK_DeliverExternalCollaborator_Creator]
GO

ALTER TABLE [dbo].[DeliverExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_DeliverExternalCollaborator_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[DeliverExternalCollaborator] CHECK CONSTRAINT [FK_DeliverExternalCollaborator_UserAccount]
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[DeliverSharedJob](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeliverJob] [int] NOT NULL,
	[DeliverExternalCollaborator] [int] NOT NULL,
	[NotificationPreset] [int] NOT NULL
 CONSTRAINT [PK_DeliverSharedJob] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DeliverSharedJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverSharedJob_DeliverJob] FOREIGN KEY([DeliverJob])
REFERENCES [dbo].[DeliverJob] ([ID])
GO

ALTER TABLE [dbo].[DeliverSharedJob] CHECK CONSTRAINT [FK_DeliverSharedJob_DeliverJob]
GO

ALTER TABLE [dbo].[DeliverSharedJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverSharedJob_DeliverExternalCollaborator] FOREIGN KEY([DeliverExternalCollaborator])
REFERENCES [dbo].[DeliverExternalCollaborator] ([ID])
GO

ALTER TABLE [dbo].[DeliverSharedJob] CHECK CONSTRAINT [FK_DeliverSharedJob_DeliverExternalCollaborator]
GO

ALTER TABLE [dbo].[DeliverSharedJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverSharedJob_AccountNotificationPreset] FOREIGN KEY([NotificationPreset])
REFERENCES [dbo].[AccountNotificationPreset] ([ID])
GO

ALTER TABLE [dbo].[DeliverSharedJob] CHECK CONSTRAINT [FK_DeliverSharedJob_AccountNotificationPreset]
GO

ALTER TABLE [dbo].[DeliverExternalCollaborator]
ADD Guid NVARCHAR(36) NOT NULL DEFAULT newid() -- generate new guid

