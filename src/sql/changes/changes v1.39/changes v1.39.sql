USE [GMGCoZone]
GO

--------- Rename ApprovalDeleteHistory table ------------
EXEC sp_rename 'ApprovalDeleteHistory', 'ApprovalUserRecycleBinHistory'
GO

CREATE TABLE [dbo].[DeliverJobDeleteHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[JobName] [nvarchar](128)
 CONSTRAINT [PK_DeliverJobDeleteHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[DeliverJobDeleteHistory]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobDeleteHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[DeliverJobDeleteHistory] CHECK CONSTRAINT [FK_DeliverJobDeleteHistory_Account]
GO

CREATE TABLE [dbo].[ApprovalJobDeleteHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [DateTime] NOT NULL,
	[JobName] [nvarchar](128)
 CONSTRAINT [PK_ApprovalJobDeleteHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[ApprovalJobDeleteHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobDeleteHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[ApprovalJobDeleteHistory] CHECK CONSTRAINT [FK_ApprovalJobDeleteHistory_Account]
GO

-------- Add permanently deleted flag ------
ALTER TABLE dbo.Approval ADD DeletePermanently BIT NOT NULL DEFAULT 0
GO

-------- Exclude approvals marked to be permanently deleted ------ 
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;	
	
	SELECT 
			SUM(result.AllCount) AS AllCount,
			SUM(result.OwnedByMeCount) AS OwnedByMeCount,
			SUM(result.SharedCount) AS SharedCount,
			SUM(result.RecentlyViewedCount) AS RecentlyViewedCount,
			SUM(result.ArchivedCount) AS ArchivedCount,
			SUM(resulT.RecycleCount) AS RecycleCount
	FROM (
			SELECT
				sum(case when js.[Key] != 'ARC' then 1 else 0 end) AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				(SELECT 
						COUNT(DISTINCT j.ID)
					FROM	ApprovalUserViewInfo auvi
						INNER JOIN Approval a
								ON a.ID = auvi.Approval 
						INNER JOIN Job j
								ON j.ID = a.Job 
						INNER JOIN JobStatus js
								ON js.ID = j.[Status]		
					WHERE	auvi.[User] =  @P_User
						AND j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (	SELECT MAX([ViewedDate]) 
								FROM ApprovalUserViewInfo vuvi
									INNER JOIN Approval av
										ON  av.ID = vuvi.[Approval]
									INNER JOIN Job vj
										ON vj.ID = av.Job	
								WHERE  av.Job = a.Job
									AND av.IsDeleted = 0
							) = auvi.[ViewedDate]
						AND EXISTS (
										SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
									)							
					) RecentlyViewedCount,			
				0 AS ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE	av.Job = a.Job
									AND av.IsDeleted = 0
									AND (
										   @P_ViewAll = 1 
											 OR
											 (
											   @P_ViewAll = 0 AND
											   EXISTS(	SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
													  )
											  )
										 )
						) = a.[Version] 
						
			UNION
				SELECT
				0 AS AllCount,
				0 AS OwnedByMeCount,
				0 AS SharedCount,
				0 AS RecentlyViewedCount,			
				sum(case when js.[Key] = 'ARC' then 1 else 0 end) ArchivedCount,
				0 AS RecycleCount					
			FROM	
				Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND	(	SELECT MAX([Version])
							FROM Approval av 
							WHERE	av.Job = a.Job
									AND av.IsDeleted = 0
									AND  EXISTS( SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
									           )
						) = a.[Version] 
						
			UNION ----- select owned by me count
				SELECT
					0 AS AllCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,					
					0 AS ArchivedCount,	
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]			
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE	av.Job = a.Job AND
										av.[Owner] = @P_User 
										AND av.IsDeleted = 0											
							) = a.[Version]	
			UNION --- select shared approvals count
				SELECT
					0 AS AllCount,
					0 AS OwnedByMeCount,
					sum(case when js.[Key] != 'ARC' then 1 else 0 end) SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
					0 AS RecycleCount	
				FROM Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]			
				WHERE	j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE	av.Job = a.Job AND
										av.[Owner] != @P_User 
										AND av.IsDeleted = 0							
										AND EXISTS(	SELECT TOP 1 ac.ID
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND ac.Collaborator = @P_User
												   )														
							) = a.[Version]	
							
			UNION ------ select deleted items count
				SELECT 
					0 AS AllCount,
					0 AS OwnedByMeCount,
					0 AS SharedCount,
					0 AS RecentlyViewedCount,				
					0 AS ArchivedCount,
				    SUM(recycle.Approvals) AS RecycleCount
				FROM (
				          SELECT COUNT(a.ID) AS Approvals
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND a.DeletePermanently = 0
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (
								      ISNULL((SELECT TOP 1 ac.ID
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
											, 
											(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
											WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
										  ) IS NOT NULL				
									)
						 
						UNION
							SELECT COUNT(f.ID) AS Approvals
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
									AND f.Creator = @P_User								
						
					) recycle
	) result
	OPTION(OPTIMIZE FOR(@P_User UNKNOWN, @P_Account UNKNOWN))	

END
GO


-- Exclude approvals marked to be permanently deleted ----
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
	
	DECLARE @changeDefGrId bit;
	
	SET @changeDefGrId =  (SELECT TOP 1 cdg.ID
							FROM dbo.CollaborateChangeDefaultGroup cdg
							join dbo.Account ac on cdg.Account = ac.ID
							join dbo.[User] u on ac.ID = u.Account
							join dbo.[UserGroupUser] ugu on u.ID = ugu.[User]
							where u.ID = @P_User and ugu.[UserGroup] = cdg.[Group])
	
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval)
	SELECT 
			a.Approval,
			IsApproval
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						) 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE						
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.Approval
		END ASC,
		CASE						
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.Approval
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.IsChangesComplete,
			result.ApprovalApprovedDate
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
							WHEN a.IsError = 1 THEN -1
							WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
									WHERE ap.Approval = a.ID ) = 100 THEN '100'
							WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
									WHERE ap.Approval = a.ID ) = 0 THEN '0'
							ELSE '50'
						END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS null))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = a.ID and ac.Collaborator = @P_User)
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			(SELECT CASE
			 WHEN js.[Key] != 'COM' AND @changeDefGrId IS NOT NULL 
			     THEN [dbo].[GetApprovalIsCompleteChanges] (a.ID)
			     ELSE CONVERT(bit,0)
			 END) as IsChangesComplete,
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (a.ID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1		
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			WHERE fa.Folder = f.ID
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					CONVERT(bit, 0) as IsChangesComplete,
					NULL as ApprovalApprovedDate
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 		
	SET @P_RecCount = 0

END
GO

--modify delete approval procedure to delete approval setting fk
ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		 --Delete annotations from ApprovalAnnotationStatusChangeLog
		DELETE [dbo].[ApprovalAnnotationStatusChangeLog]		 
		FROM   [dbo].[ApprovalAnnotationStatusChangeLog] aascl
				JOIN [dbo].[ApprovalAnnotation] aa
					ON aascl.Annotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id	    
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
			-- Delete from SharedApproval
		DELETE FROM [dbo].[ApprovalSetting] 
				WHERE Approval = @P_Id
				
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalUserRecycleBinHistory
		DELETE FROM [dbo].[ApprovalUserRecycleBinHistory]
		WHERE Approval = @P_Id
		
		-- Delete from UserApprovalReminderEmailLog
		DELETE FROM [dbo].[UserApprovalReminderEmailLog]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO


-- Alter Get Billing Cycles function to retrieve proofs count correctly --------------
ALTER FUNCTION [dbo].[GetBillingCycles]
    (
      @AccountID INT ,
      @AccountType NVARCHAR(10),
      @LoggedAccountID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @ModuleId INT
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN

        DECLARE @totalExceededCount INT = 0	
        DECLARE @cycleStartDate DATETIME = @StartDate
        DECLARE @cycleAllowedProofsCount INT
        DECLARE @PlanPrice DECIMAL = 0.0
        DECLARE @SelectedBillingPLan INT
        DECLARE @BillingPlanPrice INT
        DECLARE @moduleKey VARCHAR(10)
        DECLARE @monthCycle NVARCHAR(MAX) = ''
        
		
        IF ( @AccountType = 'SBSC' ) 
            BEGIN           
				-- subscriber
                SELECT  @SelectedBillingPLan = SPI.SelectedBillingPlan ,
                        @cycleAllowedProofsCount = CASE WHEN SPI.IsQuotaAllocationEnabled = 1 THEN SPI.NrOfCredits
												   ELSE BP.Proofs END,
                        @BillingPlanPrice = BP.Price 
                FROM    Account A
                        INNER JOIN dbo.SubscriberPlanInfo SPI ON A.ID = SPI.Account
                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId
            END
        ELSE
			-- all other account types 
            BEGIN
                SELECT  @SelectedBillingPLan = ASP.SelectedBillingPlan ,
                        @cycleAllowedProofsCount = BP.Proofs ,
                        @BillingPlanPrice = BP.Price 
                FROM    Account A
                        INNER JOIN dbo.AccountSubscriptionPlan ASP ON A.ID = ASP.Account
                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId
            END
	
       																		
        SET @PlanPrice = ( CASE WHEN ( @LoggedAccountID = 1 )
                                THEN @BillingPlanPrice
                                ELSE ( SELECT   NewPrice
                                       FROM     AttachedAccountBillingPlan
                                       WHERE    Account = @LoggedAccountID
                                                AND BillingPlan = @SelectedBillingPLan
                                     )
                           END )
						
																					
        SET @cycleStartDate = @StartDate
	
        WHILE ( datepart(year, @cycleStartDate) < datepart(year,@EndDate) OR
			  ( datepart(year, @cycleStartDate) = datepart(year,@EndDate) AND datepart(month, @cycleStartDate) <= datepart(month,@EndDate)) ) 
            BEGIN	
                DECLARE @currentCycleJobsCount INT = 0
				                                     	
                SELECT  @currentCycleJobsCount = CASE WHEN Am.[Key] = 0 -- Collaborate
                                                           THEN 
															   (SELECT
																COUNT(r.ID)
																FROM (	SELECT
																			ap.ID
																		FROM
																		Job j
																		INNER JOIN Approval ap ON j.ID = ap.Job
																		WHERE
																		j.Account = @AccountID
																		AND datepart(month, ap.CreatedDate) = datepart(month, @cycleStartDate)
																		AND datepart(year, ap.CreatedDate) = datepart(year, @cycleStartDate)
																		UNION ALL
																		SELECT 
																			a.ID 
																		FROM dbo.ApprovalJobDeleteHistory a
																		WHERE 
																		a.Account = @AccountID
																		AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
																		AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
																		) AS r
																) 
                                                           
                                                      WHEN Am.[Key] = 2 -- Deliver
														THEN                                                         
                                                           ( SELECT
                                                                COUNT(r.ID)
                                                              FROM (SELECT
																		dj.ID 
																	FROM Job j
																	INNER JOIN dbo.DeliverJob dj ON j.ID = dj.Job
																	WHERE
																	j.Account = @AccountID
																	AND datepart(month, dj.CreatedDate) = datepart(month, @cycleStartDate)
																	AND datepart(year, dj.CreatedDate) = datepart(year, @cycleStartDate)
																	UNION ALL
																	SELECT 
																		d.ID 
																	FROM dbo.DeliverJobDeleteHistory d
																	WHERE 
																	d.Account = @AccountID
																	AND datepart(month, d.CreatedDate) = datepart(month, @cycleStartDate)
																	AND datepart(year, d.CreatedDate) = datepart(year, @cycleStartDate)
																	) AS r
															) 

                                                      ELSE 0 -- for other modules 0 jobs is returned
                                                 END
                FROM    dbo.AppModule AM
                WHERE   AM.ID = @ModuleId
				
																                                         							
                SET @totalExceededCount =  CASE WHEN ( @cycleAllowedProofsCount < @currentCycleJobsCount )
										   THEN @currentCycleJobsCount
												- @cycleAllowedProofsCount
										   ELSE 0
										   END
										
			    SET @monthCycle = @monthCycle + ',' +  CONVERT(NVARCHAR, LEFT(datename(month, @cycleStartDate), 3)) + '-' + CONVERT(NVARCHAR, datepart(year, @cycleStartDate) % 100) + '|' +  CONVERT(NVARCHAR, @currentCycleJobsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice) + '|' +  CONVERT(NVARCHAR, @cycleAllowedProofsCount)
			    
			    SET @cycleStartDate = DATEADD(MM, 1, @cycleStartDate)
                    
            END
		
        RETURN  @monthCycle
	
    END

GO


-- Add AllowOverdraw column ---------

ALTER TABLE dbo.AccountSubscriptionPlan
ADD AllowOverdraw bit NOT NULL DEFAULT(0)
GO

ALTER TABLE dbo.SubscriberPlanInfo
ADD AllowOverdraw bit NOT NULL DEFAULT(0)
GO

--alter billing report info procedure
ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        
         DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN dbo.AccountStatus ast ON a.[Status] = ast.ID 
                WHERE   Parent = @P_LoggedAccount
						AND a.NeedSubscriptionPlan = 1
                        AND a.IsTemporary = 0
						AND (ast.[Key] = 'A')
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR ( @P_BillingPlanList != '' AND (
																 EXISTS (SELECT    val
																		 FROM      dbo.splitString(@P_BillingPlanList, ',')
																		 where val in (
																					   SELECT bp.SelectedBillingPlan from dbo.AccountSubscriptionPlan bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   UNION
																					   SELECT bp.SelectedBillingPlan from dbo.SubscriberPlanInfo bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   )
																		)																 
																)
								 )
                            )

        DECLARE @childAccounts TABLE ( ID INT )

        DECLARE @subAccount_ID INT
        DECLARE Cursor_SubAccounts CURSOR
        FOR SELECT ID FROM @subAccounts	
        OPEN Cursor_SubAccounts
        FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
        WHILE @@FETCH_STATUS = 0 
            BEGIN	
                INSERT  INTO @childAccounts
                            SELECT a.ID
                            from dbo.Account a
                            join dbo.AccountStatus acs on a.[Status] = acs.ID
							where a.Parent = @subAccount_ID AND acs.[Key] = 'A' AND a.IsTemporary = 0 AND a.NeedSubscriptionPlan = 1
                FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
            END

        CLOSE Cursor_SubAccounts
        DEALLOCATE Cursor_SubAccounts
        
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateContractStartDate DATETIME,
              CollaborateIsMonthlyBillingFrequency BIT,              
              CollaborateCycleDetails NVARCHAR(MAX) ,
              CollaborateModuleId INT ,
              CollaborateIsQuotaAllocationEnabled BIT,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverContractStartDate DATETIME,
              DeliverIsMonthlyBillingFrequency BIT,
              DeliverCycleDetails NVARCHAR(MAX) ,
              DeliverModuleId INT ,
              DeliverIsQuotaAllocationEnabled BIT
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateContractStartDate,
                  CollaborateIsMonthlyBillingFrequency,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  CollaborateIsQuotaAllocationEnabled,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverContractStartDate,
                  DeliverIsMonthlyBillingFrequency,
                  DeliverCycleDetails ,
                  DeliverModuleId ,
                  DeliverIsQuotaAllocationEnabled
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.ContractStartDate
                             ELSE CASPBP.ContractStartDate
                        END AS CollaborateContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(CASPBP.IsMonthlyBillingFrequency, 0)
                        END AS CollaborateIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CSPIBP.ModuleId
                                                              )
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CASPBP.ModuleId
                                                              )
                               END, '0|0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.IsQuotaAllocationEnabled
                             ELSE CASPBP.IsQuotaAllocationEnabled
                        END AS [CollaborateIsQuotaAllocationEnabled],
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.ContractStartDate
                             ELSE DASPBP.ContractStartDate
                        END AS DeliverContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(DASPBP.IsMonthlyBillingFrequency, 0)
                        END AS DeliverIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DASPBP.ModuleId
                                                              )
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DSPIBP.ModuleId
                                                              )
                               END, '0|0|0|0"0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.IsQuotaAllocationEnabled
                             ELSE DASPBP.IsQuotaAllocationEnabled
                        END AS [DeliverIsQuotaAllocationEnabled]
                        
                FROM    dbo.Account A
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency],[IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )

					   OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
 
                        WHERE  ( A.ID IN ( SELECT  *
									FROM    @subAccounts )
								 OR
								 A.ID IN ( SELECT   *
									FROM     @childAccounts )
								)
                
                
                                
     -- Get the accounts
        SET NOCOUNT OFF
        
        ;WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
        
       
        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.CollaborateContractStartDate,
				TBL.CollaborateIsMonthlyBillingFrequency,
				TBL.CollaborateIsQuotaAllocationEnabled, 
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                TBL.DeliverContractStartDate,
                TBL.DeliverIsMonthlyBillingFrequency,
                TBL.DeliverIsQuotaAllocationEnabled,
                 ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
				 (SELECT PathString) AS AccountPath
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID	
                INNER JOIN AccountTree act ON a.ID = act.AccountID		
    END

GO

--alter billing report view
ALTER VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS CollaborateBillingPlanName,
0 AS CollaborateProofs,
'' AS CollaborateBillingCycleDetails,
0 AS CollaborateBillingPlan,
CONVERT(BIT, 0) AS CollaborateIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS CollaborateContractStartDate,
CONVERT(BIT, 0) as CollaborateIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [CollaborateIsQuotaAllocationEnabled],  
'' AS DeliverBillingPlanName,
0 AS DeliverProofs,
'' AS DeliverBillingCycleDetails,
0 AS DeliverBillingPlan,
CONVERT(BIT, 0) AS DeliverIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS DeliverContractStartDate,
CONVERT(BIT, 0) AS DeliverIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [DeliverIsQuotaAllocationEnabled],
0.0 AS GPPDiscount,
0 AS Parent,
'' AS AccountPath


GO

-------- Add events for Plan Limit Reached and Plan Volume Warning  notification emails ----
DECLARE @GroupId INT 
SET @GroupId =  (SELECT ID FROM [GMGCoZone].[dbo].[EventGroup] WHERE [Key] = 'IE')
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'ILR', 'Plan Limit Reached'),
		   (@GroupId, 'IVW', 'Plan Limit Warning')
GO

--Authorization for data input users to dashboard action
DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] in ('DIN') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 6
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('QCDB')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO