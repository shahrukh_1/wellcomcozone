USE [GMGCoZone]
GO

ALTER TABLE [dbo].[AccountSubscriptionPlan]
ADD AccountPlanActivationDate [datetime2](7) NULL
GO

ALTER TABLE [dbo].[SubscriberPlanInfo]
ADD AccountPlanActivationDate [datetime2](7) NULL
GO

ALTER TABLE [dbo].[BillingPlan]
ADD IsDemo [bit] NOT NULL DEFAULT(0)
GO

---- Add new event type for demo plan ----
DECLARE @GroupId INT 
SET @GroupId =  (SELECT ID FROM [GMGCoZone].[dbo].[EventGroup] WHERE [Key] = 'IE')
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'DPE', 'Demo Plan Expired')
GO

CREATE TABLE [dbo].[BrandingPreset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Guid] nvarchar(36) NOT NULL,
	[Name] nvarchar(128) NOT NULL,
	[Theme] int NOT NULL,
	[HeaderLogoPath] nvarchar(256),
	CONSTRAINT [PK_BrandingPreset] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BrandingPreset]
ADD CONSTRAINT [FK_BrandingPreset_Account] FOREIGN KEY ([Account]) REFERENCES [dbo].[Account] ([ID])    
GO

ALTER TABLE [dbo].[BrandingPreset]
ADD CONSTRAINT [FK_BrandingPreset_Theme] FOREIGN KEY ([Theme]) REFERENCES [dbo].[Theme] ([ID])    
GO

CREATE TABLE [dbo].[BrandingPresetUserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BrandingPreset] int NOT NULL,
	[UserGroup] int NOT NULL,
	CONSTRAINT [PK_BrandingPresetUserGroup] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BrandingPresetUserGroup]
ADD CONSTRAINT [FK_BrandingPresetUserGroup_BrandingPreset] FOREIGN KEY ([BrandingPreset]) REFERENCES [dbo].[BrandingPreset] ([ID])    
GO

ALTER TABLE [dbo].[BrandingPresetUserGroup]
ADD CONSTRAINT [FK_BrandingPresetUserGroup_UserGroup] FOREIGN KEY ([UserGroup]) REFERENCES [dbo].[UserGroup] ([ID])
GO



--Add sub menu item  General 
DECLARE @controllerId INT

SELECT @controllerId = ControllerAction
FROM dbo.MenuItem
WHERE [Key] = 'SECU'

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SCGL' , -- Key - nvarchar(4)
          'Global' , -- Name - nvarchar(64)
          'Global' -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SECU'
GO

--Add Controller Action - BrandingCustomization
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'BrandingCustomization', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

-- Add sub-menu item - Branding
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title		
        )
SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          2 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SCB' , -- Key - nvarchar(4)
          'Branding' , -- Name - nvarchar(64)
          'Branding'  -- Title - nvarchar(128)		
        FROM dbo.MenuItem mi
		where [Key] = 'SECU'		
		
UPDATE 	dbo.MenuItem
SET ControllerAction = NULL
WHERE [KEY] = 'SECU'

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SCB','SCGL')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE R.[Key] IN('ADM','HQA','HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (0,1,2,3,4,5)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
		DELETE FROM dbo.MenuItemAccountTypeRole
		WHERE MenuItem IN(
						SELECT  MI.ID
						FROM    dbo.MenuItem MI
						WHERE   MI.[Key] IN ('SECU')
						)
						AND AccountTypeRole = @atrId
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

--Add Submenu Item - Add Edit Branding Preset
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditBrandingPreset', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AEBP' , -- Key - nvarchar(4)
          'Add Edit Branding Preset' , -- Name - nvarchar(64)
          'Add Edit Branding Preset'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SCB'
GO
DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('AEBP')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

------------------- Add "General" customization  submenu item for sysadmin --------
DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'HQA' OR R.[KEY] = 'HQM' ) AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SCGL')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

----------- Add "Global" submenu item for child account-----------------
DECLARE @controllerId INT

SELECT @controllerId = ControllerAction
FROM dbo.MenuItem
WHERE [Key] = 'ACCU'

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SCGL' , -- Key - nvarchar(4)
          'Global' , -- Name - nvarchar(64)
          'Global' -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'ACCU'
GO
				
UPDATE 	dbo.MenuItem
SET ControllerAction = NULL
WHERE [KEY] = 'ACCU'

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM' OR R.[Key] = 'HQA' OR R.[KEY] = 'HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (4)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SCGL') and MI.[Parent] IN (SELECT ID FROM dbo.MenuItem WHERE [Key] = 'ACCU')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE R.[Key] IN('ADM','HQA','HQM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] IN (0,1,2,3,4,5)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
		DELETE FROM dbo.MenuItemAccountTypeRole
		WHERE MenuItem IN(
						SELECT  MI.ID
						FROM    dbo.MenuItem MI
						WHERE   MI.[Key] IN ('ACCU')
						)
						AND AccountTypeRole = @atrId
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

DROP TABLE dbo.AccountPrePressFunction
GO

----- added check for expired demo plans ------
ALTER PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId
           
        DECLARE @Count INT;		
		DECLARE @enableSoftProofingTools AS BIT = 0;       
		WITH TempTable
		AS
		( SELECT  count(1) as ResultCount,
							bpt.EnableSoftProofingTools
				  FROM      dbo.Account acc
							INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
							INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
				  GROUP BY  bpt.EnableSoftProofingTools
				  UNION ALL
				  SELECT    count(1) as ResultCount,
							bpt.EnableSoftProofingTools
				  FROM      dbo.Account acc
							INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
							INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
				  GROUP BY  bpt.EnableSoftProofingTools
		)
		SELECT @Count = sum(ResultCount), @enableSoftProofingTools = COALESCE(EnableSoftProofingTools, 0) FROM TempTable GROUP BY EnableSoftProofingTools
		
	    DECLARE @enableCollaborateSettings BIT = (SELECT CASE WHEN @Count > 0
														THEN CONVERT(BIT, 1)
														ELSE CONVERT(BIT, 0)
                                                    END
                                                  )

        --Remove Profile tab for users that don't have access to Admin Module        
        DECLARE @enableProfileMenu BIT = ( SELECT   CASE WHEN ( SELECT
                                                              COUNT(rl.ID)
                                                              FROM
                                                              dbo.UserRole ur
                                                              JOIN dbo.Role rl ON ur.Role = rl.ID
                                                              JOIN dbo.AppModule apm ON rl.AppModule = apm.ID
                                                              WHERE
                                                              ur.[User] = @P_userId
                                                              AND (apm.[Key] = 3 OR apm.[Key] = 4)
                                                              AND (RL.[Key] = 'ADM' OR rl.[Key] = 'HQM' OR rl.[Key] = 'HQA')
                                                              ) > 0
                                                         THEN CONVERT(BIT, 1)
                                                         ELSE CONVERT(BIT, 0)
                                                    END
                                         )
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
        
        IF ( @enableSoftProofingTools = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SESP'  -- MenuKey - varchar(4)
                          )  
            END
            
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
                          
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'PSVI'  -- MenuKey - varchar(4)
                          )
            END
                       
        IF(@enableCollaborateSettings = 0)
			BEGIN
				 INSERT  INTO @ignoreMenuKeys
							( MenuKey )
				 VALUES  ( 'CLGS'  -- MenuKey - varchar(4)
							)
            END
             
        IF ( @enableProfileMenu = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEPF' --MenuKey - varchar(4)
                          )
            END   
        
        

        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END

GO



