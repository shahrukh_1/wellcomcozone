USE GMGCoZone
GO
-- update position for Deliver module
UPDATE dbo.MenuItem
SET Position = 4
WHERE [Key] IN ('DLIN', 'DLIM')
GO

INSERT  INTO dbo.ConversionStatus
        ( [Key], Name )
VALUES  ( 0, -- Key - int
          'None'  -- Name - nvarchar(64)
          )
INSERT  INTO dbo.ConversionStatus
        ( [Key], Name )
VALUES  ( 1, -- Key - int
          'Past'  -- Name - nvarchar(64)
          )
INSERT  INTO dbo.ConversionStatus
        ( [Key], Name )
VALUES  ( 2, -- Key - int
          'Fails'  -- Name - nvarchar(64)
          )
INSERT  INTO dbo.ConversionStatus
        ( [Key], Name )
VALUES  ( 3, -- Key - int
          'Processing'  -- Name - nvarchar(64)
          )


INSERT INTO dbo.PreflightStatus
        ( [Key], Name )
VALUES  ( 0, -- Key - int
          'None'  -- Name - nvarchar(64)
          )
INSERT INTO dbo.PreflightStatus
        ( [Key], Name )
VALUES  ( 1, -- Key - int
          'Past'  -- Name - nvarchar(64)
          )
INSERT INTO dbo.PreflightStatus
        ( [Key], Name )
VALUES  ( 2, -- Key - int
          'Failed'  -- Name - nvarchar(64)
          )
INSERT INTO dbo.PreflightStatus
        ( [Key], Name )
VALUES  ( 3, -- Key - int
          'Processing'  -- Name - nvarchar(64)
          )
GO
