USE GMGCoZone
alter table dbo.DeliverJob
add BackLinkUrl nvarchar(256)
GO
DELETE  dbo.MenuItemAccountTypeRole
WHERE   ID IN (
        SELECT  MIATR.ID
        FROM    dbo.MenuItemAccountTypeRole MIATR
                INNER JOIN dbo.AccountTypeRole ATR ON MIATR.AccountTypeRole = ATR.ID
                INNER JOIN dbo.Role R ON ATR.Role = R.ID
                INNER JOIN dbo.MenuItem M ON MIATR.MenuItem = M.ID
                INNER JOIN dbo.AccountType AT ON ATR.AccountType = AT.ID
        WHERE   AT.[Key] = 'GMHQ'
                AND ( R.[Key] = 'HQA'
                      OR R.[Key] = 'HQM'
                    )
                AND M.[Key] = 'SEEX' )
GO