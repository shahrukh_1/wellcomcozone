USE GMGCoZone
GO

CREATE TABLE [dbo].[NotificationItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InternalRecipient] [int] NULL,
	[NotificationType] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Creator] [int] NULL,
	[CustomFields] [nvarchar](MAX) NULL,
 CONSTRAINT [PK_NotificationItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD  CONSTRAINT [FK_NotificationItem_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[NotificationItem] CHECK CONSTRAINT [FK_NotificationItem_Creator]
GO

ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD  CONSTRAINT [FK_NotificationItem_InternalRecipient] FOREIGN KEY([InternalRecipient])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[NotificationItem] CHECK CONSTRAINT [FK_NotificationItem_InternalRecipient]
GO

ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD  CONSTRAINT [FK_NotificationItem_NotificationType] FOREIGN KEY([NotificationType])
REFERENCES [dbo].[EventType] ([ID])
GO

ALTER TABLE [dbo].[NotificationItem] CHECK CONSTRAINT [FK_NotificationItem_NotificationType]
GO

--Add new events for Instant Emails that don't require authorizations
DECLARE @GroupId int

INSERT INTO [GMGCoZone].[dbo].[EventGroup]
           ([Key]
           ,[Name])
     VALUES
           ('IE'
           ,'InstantEmails')
		   
SET @GroupId = SCOPE_IDENTITY()

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IBL', 'Billing Report')

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IWL', 'Welcome') 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'ISW', 'Shared Workflow') 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IDP', 'Deliver New PairingCode')

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IAM', 'Approval Reminder')
	 
 INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'ILP', 'Lost Password')
	 
 INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IRP', 'Reset Password')
GO

 UPDATE [dbo].[UploadEngineDuplicateFileName] 
 SET Name = 'MatchFileNameExactly'
 WHERE [Key] = 2
 INSERT INTO [dbo].[UploadEngineDuplicateFileName] VALUES (3, 'FileWithSuffix_vX')
 GO
 
 ------------------------------------------------------------------------------------------------------------------------Launched on Release
DECLARE @GroupId INT 
SET @GroupId =  (SELECT ID FROM [GMGCoZone].[dbo].[EventGroup] WHERE [Key] = 'IE')
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IAS', 'Account Support')
GO

ALTER TABLE dbo.Email
ADD Attachments nvarchar(MAX) NULL
GO

DROP TABLE dbo.Attachment
GO
 
DROP TABLE dbo.NotificationEmailQueue
GO

--modify delete approval procedure to delete approvaldeletehistory
ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalDeleteHistory
		DELETE FROM [dbo].[ApprovalDeleteHistory]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO

ALTER PROCEDURE [dbo].[SPC_DeleteFolder] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY
	
		-- Delete from FolderCollaborator 
		DELETE FROM	[dbo].[FolderCollaborator]
				WHERE Folder = @P_Id
		
		-- Delete from FolderCollaboratorGroup 
		DELETE FROM	[dbo].[FolderCollaboratorGroup] 
				WHERE Folder = @P_Id
		
		DECLARE @ApprovalID INT
		DECLARE @getApprovalID CURSOR
		SET @getApprovalID = CURSOR FOR SELECT Approval FROM [dbo].[FolderApproval] WHERE Folder = @P_Id
		OPEN @getApprovalID
		
		FETCH NEXT
		FROM @getApprovalID INTO @ApprovalID
			WHILE @@FETCH_STATUS = 0
			BEGIN
			EXECUTE [dbo].[SPC_DeleteApproval] @ApprovalID
			
			FETCH NEXT
			FROM @getApprovalID INTO @ApprovalID
			END
		CLOSE @getApprovalID
		DEALLOCATE @getApprovalID
		
		--Mark parent as null for child folders
		UPDATE [dbo].[Folder]
		SET Parent = NULL
		where Parent = @P_Id	
 
	    -- Delete from Folder
		DELETE FROM [dbo].[Folder]
		WHERE ID = @P_Id	    
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO
 ------------------------------------------------------------------------------------------------------------------------Launched on Debug
------------------------------------------------------------------------------------------------------------------------Launched on Release