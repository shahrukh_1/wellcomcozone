USE GMGCoZone

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 04/08/2014 15:37:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add Return Approvals Page Info procedure
-- Fix the bug with multiple decisions for the same collaborator on an approval (temporary fix for an existing issue with adding a decision instead of updating the existing one)
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID, 
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
END
GO
-- Alter Procedure to add ReturnArchivedApprovalInfo
ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID,
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				100 AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				0 AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				'######' AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
				AND a.IsDeleted = 0	
				AND js.[Key] = 'ARC'
				AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
				AND
				(	SELECT MAX([Version])
					FROM Approval av 
					WHERE 
						av.Job = a.Job
						AND av.IsDeleted = 0
						AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
				) = a.[Version]	
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	 
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM	(
			SELECT 	a.ID
			FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0	
					AND js.[Key] = 'ARC'
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
					) = a.[Version]
				)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	 
END
GO
-- Add Return Folders Approvals Page Info
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0
						) AS ApprovalsCount,
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS [DocumentPagesCount]
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
				ORDER BY 
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN Deadline
					--END ASC,
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN Deadline
					--END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN f.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN f.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN f.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN f.ModifiedDate
					END DESC--,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN f.[Status]
					--END ASC,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN f.[Status]
					--END DESC		 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO

-- Alter Procedure to add DocumentPagesCount to return
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS DocumentPagesCount
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END
GO

-- Add Documents Page Count to procedure
ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
			SELECT 	TOP (@P_Set * @P_MaxRows)
					CONVERT(int, ROW_NUMBER() OVER(
					ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
					END DESC					
				)) AS ID, 
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,							 
					j.[Status] AS [Status],	
					j.ID AS Job,
					a.[Version],
					100 AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					(SELECT MAX([Version])
					 FROM Approval av
					 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT 	a.ID
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	  
END
GO
ALTER FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac
			 ON  a.ID = ac.Approval
		WHERE a.ID = @P_Approval ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCreator IS NULL AND aa.ExternalModifier IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCreator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCreator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;
GO
--Contact Support Email in White Label Settings for an account.
INSERT INTO dbo.AccountSetting
        ( Account ,
          Name ,
          Value ,
          ValueDataType ,
          Description
        )
VALUES  ( null , -- Account - int
          'ContactSupportEmail' , -- Name - nvarchar(64)
          'local.help@gmgcozone.com' , -- Value - nvarchar(max)
          1 , -- ValueDataType - int
          'ContactSupportEmail'  -- Description - nvarchar(512)
        )
GO
-- CZD-629 - stored procedure for move approval to another folder or delete link between approval and folder
CREATE PROCEDURE [dbo].[SPC_MoveApproval]
    (
      @P_ApprovalId INT ,
      @P_FolderId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @P_Success NVARCHAR(512) = ''

        BEGIN TRY
            
            DELETE  dbo.FolderApproval
            WHERE   Approval = @P_ApprovalId

            IF @P_FolderId > 0 
                BEGIN
                    INSERT  INTO dbo.FolderApproval
                            ( Folder, Approval )
                    VALUES  ( @P_FolderId, -- Folder - int
                              @P_ApprovalId  -- Approval - int
                              )
                END
    
            SET @P_Success = ''
    
        END TRY
        BEGIN CATCH
            SET @P_Success = ERROR_MESSAGE()
        END CATCH ;

        SELECT  @P_Success AS RetVal
  
    END
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalCounts
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;
	
	-- Get All Approval count
	SET @AllCount = (	SELECT COUNT(a.ID)
						FROM	Job j
								INNER JOIN Approval a 
									ON a.Job = j.ID
								INNER JOIN JobStatus js
									ON js.ID = j.[Status]		
						WHERE	j.Account = @P_Account
							AND a.IsDeleted = 0
							AND js.[Key] != 'ARC'
							AND	(	SELECT MAX([Version])
									FROM Approval av 
									WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND @P_User IN (	SELECT ac.Collaborator 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID
															)
								) = a.[Version] )
						
	-- Get Owned by me count		
	SET @OwnedByMeCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] = @P_User
									) = a.[Version]	)
			
	-- Get Shared with me count
	SET @SharedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] != @P_User
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version]	)
			
	-- Get Recently viewed count
	SET @RecentlyViewedCount = (	SELECT COUNT(DISTINCT j.ID)
									FROM	ApprovalUserViewInfo auvi
											INNER JOIN Approval a
													ON a.ID = auvi.Approval 
											INNER JOIN Job j
													ON j.ID = a.Job 
											INNER JOIN JobStatus js
													ON js.ID = j.[Status]		
									WHERE	auvi.[User] =  @P_User
										AND j.Account = @P_Account
										AND a.IsDeleted = 0
										AND js.[Key] != 'ARC'
										AND @P_User IN (	SELECT ac.Collaborator 
																			FROM ApprovalCollaborator ac 
																			WHERE ac.Approval = a.ID
																		)										
										)
			
	-- Get Archived count
	SET @ArchivedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] = 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version])
			
	-- Get Recycle bin count
	SET @RecycleCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (@P_User IN (	SELECT ac.Collaborator 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = a.ID
												)			
									)
						) 
						+
						(	SELECT COUNT(f.ID)
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)
									AND f.Creator = @P_User								
						)
							
	SELECT 	@AllCount AS AllCount,
			@OwnedByMeCount AS OwnedByMeCount,
			@SharedCount AS SharedCount,
			@RecentlyViewedCount AS RecentlyViewedCount,
			@ArchivedCount AS ArchivedCount,
			@RecycleCount AS RecycleCount
END
GO

-- Alter Procedure to add DocumentPagesCount to return
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						ELSE 
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS DocumentPagesCount
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND f.Creator = @P_User						
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND f.Creator = @P_User
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END
GO

--Add support to delete Deliver Jobs

ALTER TABLE dbo.DeliverJob
ADD IsDeleted BIT NOT NULL DEFAULT(0)
GO

--Alter procedure for not returning deleted deliver jobs

ALTER PROCEDURE [dbo].[SPC_ReturnDeliversPageInfo]
    (
      @P_Account INT ,
      @P_User INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;
        
        DECLARE @Delivers TABLE
            (
              ID INT ,
              [Title] NVARCHAR(128) ,
              [User] NVARCHAR(64) ,
              [Pages] NVARCHAR(50) ,
              [CPName] NVARCHAR(50) ,
              [Workflow] NVARCHAR(256) ,
              [CreatedDate] DATETIME ,
              [StatusKey] INT ,
              [Account] INT ,
              [OwnUser] INT
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  Title ,
                  [User] ,
                  Pages ,
                  CPName ,
                  Workflow ,
                  CreatedDate ,
                  StatusKey ,
                  Account ,
                  OwnUser
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] = @P_User
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'CON' -- contributor should only see his jobs
                  UNION
                --- deliver jobs for admin users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' -- admin should see all the jobs from current account
                  UNION
                --- deliver jobs for manager and moderator users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND DJ.[owner] IN (
                            SELECT  *
                            FROM    dbo.[GetUserListWhereBelongs](@P_User) )
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) IN (
                            'MAN', 'MOD' ) -- moderator and manager should see their jobs + jobs of the users that belongs to the same user group
                  
                )
                
            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.CreatedDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.CreatedDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.Title
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.Title
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.[User]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.[User]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Pages]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Pages]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CpName]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CpName]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[Workflow]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[Workflow]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[StatusKey]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[StatusKey]
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset
        
        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT

    END
GO

ALTER TABLE [dbo].[CMSVideo] ADD 
[VideoTitle] [nvarchar](256) NOT NULL DEFAULT('')

GO
--Move New user in the same view as edit user
 UPDATE [GMGCoZone].[dbo].[ControllerAction]
  SET action = 'AddEditUser'
  WHERE action = 'EditUser'
 GO
 
ALTER TABLE dbo.MenuItem ALTER COLUMN ControllerAction INT NULL
GO

ALTER VIEW [dbo].[UserMenuItemRoleView]
AS
SELECT TOP 100 PERCENT
        *
FROM    ( SELECT    atr.[AccountType] ,
                    atr.[Role] ,
                    mi.[ID] AS [MenuItem] ,
                    mi.[Key] ,
                    mi.[Name] AS [MenuName] ,
                    mi.[Title] ,
                    ISNULL(mi.[Parent], 0) AS [Parent] ,
                    mi.[Position] ,
                    mi.[IsAdminAppOwned] ,
                    mi.[IsAlignedLeft] ,
                    mi.[IsSubNav] ,
                    mi.[IsTopNav] ,
                    mi.[IsVisible] ,
                    ca.[ID] AS [ControllerAction] ,
                    ca.[Controller] ,
                    ca.[Action] ,
                    ca.[Parameters]
          FROM      [dbo].[AccountTypeRole] atr
                    JOIN [dbo].[MenuItemAccountTypeRole] mirat ON atr.[ID] = mirat.[AccountTypeRole]
                    JOIN [dbo].[MenuItem] mi ON mirat.[MenuItem] = mi.[ID]
                    LEFT OUTER JOIN [dbo].[ControllerAction] ca ON mi.ControllerAction = ca.ID
          UNION
          SELECT    0 AS [AccountType] ,
                    0 AS [Role] ,
                    mi.[ID] AS [MenuItem] ,
                    mi.[Key] ,
                    mi.[Name] AS [MenuName] ,
                    mi.[Title] ,
                    ISNULL(mi.[Parent], 0) AS [Parent] ,
                    mi.[Position] ,
                    mi.[IsAdminAppOwned] ,
                    mi.[IsAlignedLeft] ,
                    mi.[IsSubNav] ,
                    mi.[IsTopNav] ,
                    mi.[IsVisible] ,
                    0 AS [ControllerAction] ,
                    '' AS [Controller] ,
                    '' AS [Action] ,
                    '' AS [Parameters]
          FROM      dbo.MenuItem mi
          WHERE     ControllerAction IS NULL
        ) SLCT
ORDER BY SLCT.[AccountType] ASC ,
        SLCT.[Role] ASC ,
        SLCT.[MenuItem] ASC ,
        SLCT.[Parent] ,
        SLCT.[Position]
GO
DECLARE @accountSettingsMenuId INT = 0
DECLARE @settingsMenuId INT = 0
DECLARE @usersAndGroupsMenuId INT = 0
DECLARE @applicationSettingsMenuId INT = 0
DECLARE @applicationSettingsCollaborateMenuId INT = 0
DECLARE @editAccountSettingsMenuId INT = 0

SELECT  @editAccountSettingsMenuId = mi.ID
FROM    dbo.ControllerAction ca
        JOIN dbo.MenuItem mi ON ca.id = mi.ControllerAction
WHERE   ca.Controller = 'Account'
        AND ca.Action = 'Index'
        AND Parent IS NULL


SELECT  @accountSettingsMenuId = mi.ID
FROM    dbo.ControllerAction ca
        JOIN dbo.MenuItem mi ON ca.id = mi.ControllerAction
WHERE   ca.Controller = 'Settings'
        AND ca.Action = 'Index'
        AND Parent IS NULL

------------------------------------- Settings -------------------------------------

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @accountSettingsMenuId , -- Parent - int
          30 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SETT' , -- Key - nvarchar(4)
          'Account settings' , -- Name - nvarchar(64)
          'Settings' -- Title - nvarchar(128)
        )
        
SET @settingsMenuId = SCOPE_IDENTITY()

UPDATE  dbo.MenuItem
SET     Parent = @settingsMenuId
WHERE   [Key] IN ( 'SECU', 'SESS', 'SEDN', 'SEWL' )

------------------------------------- Users & Groups -------------------------------------

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @accountSettingsMenuId , -- Parent - int
          40 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'USGR' , -- Key - nvarchar(4)
          'Users & Groups' , -- Name - nvarchar(64)
          'Settings' -- Title - nvarchar(128)
        )
        
SET @usersAndGroupsMenuId = SCOPE_IDENTITY()

UPDATE  dbo.MenuItem
SET     Parent = @usersAndGroupsMenuId
WHERE   [Key] IN ( 'SEUS', 'SEGR', 'SEEX' )


------------------------------------- Application Settings -------------------------------------

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @accountSettingsMenuId , -- Parent - int
          50 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'APPS' , -- Key - nvarchar(4)
          'Application settings' , -- Name - nvarchar(64)
          'Settings' -- Title - nvarchar(128)
        )
        
SET @applicationSettingsMenuId = SCOPE_IDENTITY()

------------------------------------- Application Settings - Collaborate menu -------------------------------------
       
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @applicationSettingsMenuId , -- Parent - int
          4 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'COLS' , -- Key - nvarchar(4)
          'Application settings - Collaborate' , -- Name - nvarchar(64)
          'Settings Collaborate' -- Title - nvarchar(128)
        )
        
SET @applicationSettingsCollaborateMenuId = SCOPE_IDENTITY()

UPDATE  dbo.MenuItem
SET     Parent = @applicationSettingsCollaborateMenuId
WHERE   [Key] IN ( 'SESP')

------------------------------------- Application Settings - Deliver menu -------------------------------------
       
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @applicationSettingsMenuId , -- Parent - int
          4 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'DEVS' , -- Key - nvarchar(4)
          'Application settings - Deliver' , -- Name - nvarchar(64)
          'Settings Deliver' -- Title - nvarchar(128)
        )
        
SET @applicationSettingsCollaborateMenuId = SCOPE_IDENTITY()

UPDATE  dbo.MenuItem
SET     Parent = @applicationSettingsCollaborateMenuId
WHERE   [Key] IN ( 'SEDS')

------------------------------------- EditAccount Settings -------------------------------------

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @editAccountSettingsMenuId , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'ACSE' , -- Key - nvarchar(4)
          'EditAccount settings' , -- Name - nvarchar(64)
          'Settings' -- Title - nvarchar(128)
        )
        
SET @settingsMenuId = SCOPE_IDENTITY()

UPDATE  dbo.MenuItem
SET     Parent = @settingsMenuId
WHERE   [Key] IN ( 'ACCU', 'ACSS','ACDN' )

GO

-------------------------Add menu item and sub menu items-------------------------------
UPDATE  dbo.MenuItem
SET     Position= 1
WHERE   [Key] IN ( 'CMSA')

UPDATE dbo.ControllerAction
SET Action = 'Announcements'
WHERE Controller= 'CMS' AND Action = 'Index'
GO


DECLARE @settingsMenuId INT = 0
DECLARE @globalSettingsParentId INT
DECLARE @controllerId INT

SELECT  @globalSettingsParentId = MI.ID
FROM    dbo.MenuItem MI
        INNER JOIN dbo.ControllerAction CA ON CA.ID = MI.ControllerAction
WHERE   Action = 'Global'
        AND Controller = 'Settings'
        AND MI.Parent IS NULL	

--Add Menu item  Content Management
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( NULL , -- ControllerAction - int
          @globalSettingsParentId , -- Parent - int
          3 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'CMSI' , -- Key - nvarchar(4)
          'CMS' , -- Name - nvarchar(64)
          'Content Management' -- Title - nvarchar(128)
        )
		
SET @settingsMenuId = SCOPE_IDENTITY()

--Add Submenu Item - Videos

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'CMS', -- Controller - nvarchar(128)
          'Videos', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title		
        )
VALUES  ( @controllerId , -- ControllerAction - int
          @globalSettingsParentId , -- Parent - int
          2 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'CMSV' , -- Key - nvarchar(4)
          'Videos' , -- Name - nvarchar(64)
          'Videos'  -- Title - nvarchar(128)		
        )
		
--Add Submenu Item - Manuals

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'CMS', -- Controller - nvarchar(128)
          'Manuals', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title		
        )
VALUES  ( @controllerId , -- ControllerAction - int
          @globalSettingsParentId , -- Parent - int
          3 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'CMSM' , -- Key - nvarchar(4)
          'Manuals' , -- Name - nvarchar(64)
          'Manuals'  -- Title - nvarchar(128)		
        )
		
--Add Submenu Item - Downloads

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'CMS', -- Controller - nvarchar(128)
          'Downloads', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title		
        )
VALUES  ( @controllerId , -- ControllerAction - int
          @globalSettingsParentId , -- Parent - int
          4 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'CMSD' , -- Key - nvarchar(4)
          'Downloads' , -- Name - nvarchar(64)
          'Downloads'  -- Title - nvarchar(128)		
        )			
		
		
--Set parent for Submenu Items
UPDATE  dbo.MenuItem
SET     Parent = @settingsMenuId
WHERE   [Key] IN ( 'CMSA', 'CMSV','CMSM','CMSD')
GO

-- SysAdmin rights
DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] = 'HQA' AND AT.[Key] = 'GMHQ' AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('CMSV', 'CMSM', 'CMSD')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

CREATE VIEW [dbo].[SecondaryNavigationMenuItem] 
AS 
SELECT  0 AS [MenuId], 
        0 AS [Parent], 
        '' AS [Action], 
        '' AS [Controller], 
        '' AS [DisplayName], 
        CONVERT(BIT, 0) AS [Active], 
        '' AS [MenuKey], 
        0 AS [Level],
        0 AS [Position]
GO

CREATE PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT ) ;
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) ;
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId ;
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId ;
        
        DECLARE @enableSoftProofingTools AS BIT = ( SELECT  bpt.EnableSoftProofingTools
                                                    FROM    dbo.Account acc
                                                            INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                            INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                            INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                            INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                    WHERE   am.[Key] = 0 -- collaborate
                                                            AND acc.ID = @P_accountId
                                                    UNION ALL
                                                    SELECT  bpt.EnableSoftProofingTools
                                                    FROM    dbo.Account acc
                                                            INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                            INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                            INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                            INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                    WHERE   am.[Key] = 0 -- collaborate
                                                            AND acc.ID = @P_accountId
                                                  ) ;
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
        IF ( @enableSoftProofingTools = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SESP'  -- MenuKey - varchar(4)
                          )
            END
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
            END
        
        

        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END

GO

--Account Report changes
ALTER VIEW [dbo].[AccountsView]
AS
SELECT
	a.[ID], 
	a.Name,
	a.[Status] AS StatusId,
	s.[Name] AS StatusName,
	s.[Key],
	a.Domain,
	a.IsCustomDomainActive,
	a.CustomDomain,
	a.AccountType AS AccountTypeID,
	a.CreatedDate,
	a.IsTemporary,
	at.Name AS AccountTypeName,
	u.GivenName,
	u.FamilyName,
	co.ID AS Company,
	c.ID AS Country,
	c.ShortName AS CountryName,
	--a.IsDraft,
	(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
	(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
	(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts
	
FROM
	[dbo].[AccountType] at 
	JOIN [dbo].[Account] a
		ON at.[ID] = a.AccountType
	JOIN [dbo].[AccountStatus] s
		ON s.[ID] = a.[Status]
	LEFT OUTER JOIN [dbo].[User] u
		ON u.ID= a.[Owner]
	JOIN [dbo].[Company] co
		ON a.[ID] = co.[Account]
	JOIN [dbo].[Country] c
		ON c.[ID] = co.[Country]
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'HQA' OR R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'GMHQ', 'DELR', 'CLNT') AND AM.[Key] in (3, 4)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('RPAR')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO


-- Create CMSManual
CREATE TABLE [dbo].[CMSManual](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileGuid] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
	[IsDeleted] [bit] NOT NULL	
CONSTRAINT [PK_CMSManual] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (	PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, 
		IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
		ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[CMSManual] ADD  CONSTRAINT [DF_CMSManual_Name]  DEFAULT ('') FOR [Name]
ALTER TABLE [dbo].[CMSManual] ADD  CONSTRAINT [DF_CMSManual_Description]  DEFAULT ('') FOR [Description]

GO

-- Create CMSDownload
CREATE TABLE [dbo].[CMSDownload](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileGuid] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
	[IsDeleted] [bit] NOT NULL	
CONSTRAINT [PK_CMSDownload] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (	PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, 
		IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, 
		ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[CMSDownload] ADD  CONSTRAINT [DF_CMSDownload_Name]  DEFAULT ('') FOR [Name]
ALTER TABLE [dbo].[CMSDownload] ADD  CONSTRAINT [DF_CMSDownload_Description]  DEFAULT ('') FOR [Description]

GO

-------------------------------------------------------------------------------------------------------------------------------------------- Launched On Debug