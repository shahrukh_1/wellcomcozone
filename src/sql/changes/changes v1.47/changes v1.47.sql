--------------------------------------------------------
---- Add new events(notification) type for "Approval overdue" and "Approval status changed by PDM"
--------------------------------------------------------
DECLARE @GroupId INT 
SET @GroupId =  (SELECT ID FROM [GMGCoZone].[dbo].[EventGroup] WHERE [Key] = 'A')
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'APO', 'Approval overdue')
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'ACP', 'Approval status changed by PDM')

GO

DECLARE @PresetId INT
DECLARE @RoleId INT
DECLARE @ModuleId INT

--Collaborate Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 0);

---------------------------------------------------------------------	   
---- High Preset
---------------------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
-------------------------------------------------------	   
---- Custom Preset
-------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'

GO


DECLARE @PresetId INT
DECLARE @RoleId INT
DECLARE @ModuleId INT

--Collaborate Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 0);

---------------------------------------------------------------------	   
---- High Preset
---------------------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
-------------------------------------------------------	   
---- Custom Preset
-------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'APO'

GO



DECLARE @PresetId INT
DECLARE @RoleId INT
DECLARE @ModuleId INT

--Collaborate Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 0);

---------------------------------------------------------------------	   
---- High Preset
---------------------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
-------------------------------------------------------	   
---- Custom Preset
-------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] = 'ACP'

GO



--Add IsExternalPrimaryDecisionMaker property
ALTER VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			'' AS [Status],
			'' AS [Guid],
			'' AS [FileName],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			'' AS PrimaryDecisionMakerName,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			CONVERT(bit, 0) AS HasAnnotations,
			'' AS Decision,
			CONVERT(bit, 0) AS IsCollaborator,
			CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,
			CONVERT(DateTime, GETDATE()) AS ApprovalApprovedDate,
			0 AS CurrentPhase,
			'' AS PhaseName,
			'' AS WorkflowName,
			'' AS NextPhase,
			'' AS DecisionMakers,
			0 AS DecisionType,
			'' AS OwnerName,
			CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
			NULL AS JobOwner,
			CONVERT(bit, 0) AS IsPhaseComplete,
			'' AS [VersionSufix],
			CONVERT(bit, 0) AS IsLocked,
			CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker

GO


ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_HideCompletedApprovals bit,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(50),
	   PrimaryDecisionMakerName nvarchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)						
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL and
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0								
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
								(@P_TopLinkId = 1 
										AND EXISTS(
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												  )
									)									
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
							  )
							)
						)		
			) = a.[Version]	)
			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (
														 @P_ViewAll = 1 
													 OR
													 ( 
														@P_ViewAll = 0 AND 
														(
															(@P_TopLinkId = 1 AND (@P_User = j.JobOwner OR EXISTS(
																													SELECT TOP 1 ac.ID 
																													FROM ApprovalCollaborator ac 
																													WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																												  )
																					)
															 )									
															OR 
																(@P_TopLinkId = 2 
																	AND @P_User = ISNULL(j.JobOwner,0)
																)
															OR 
																(@P_TopLinkId = 3 
																	AND ISNULL(j.JobOwner,0) != @P_User
																	AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																				)
																)
														  )
														)
													)										 		
								      )
				)
			)
	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,			
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
					WHEN ( EXISTS (SELECT ANNOTATION					
							FROM dbo.ApprovalPage ap							
							CROSS APPLY (SELECT TOP 1 aa.ID AS ANNOTATION FROM dbo.ApprovalAnnotation aa WHERE aa.Page = ap.ID 
							AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = a.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																													  WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase 
																													  )
																				)
													  )
																					   
								)	 
							) ANN (ANNOTATION)						
							WHERE ap.Approval = t.ApprovalID 
						 )
					) 
					THEN CONVERT(bit,1)
					ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,						
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = a.ID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID

	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop

END

GO


-- Alter Procedure by adding IsExternalPrimaryDecisionMaker property
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,
	@P_ViewAll bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						)
					AND (
						 @P_ViewAll = 1 
						 OR
						 ( 
							@P_ViewAll = 0 AND((a.CurrentPhase IS NULL AND	EXISTS (
																					 SELECT TOP 1 ac.ID 
																					 FROM ApprovalCollaborator ac 
																					 WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
																					))
										  OR (a.CurrentPhase IS NOT NULL AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																												SELECT TOP 1 ac.ID 
																												FROM ApprovalCollaborator ac 
																												WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator AND ac.Phase = a.CurrentPhase
																											  )
											                                  )
											   )
											  )
					     )	
					    )	 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase,
					GetDate() AS Deadline
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
					END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,	
					t.PrimaryDecisionMakerName,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND acd.Phase = a.CurrentPhase
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,			
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = a.ID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'' AS PrimaryDecisionMakerName,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			JOIN Approval a ON fa.Approval = a.ID	
	                    WHERE fa.Folder = f.ID AND a.DeletePermanently = 0
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					NULL as ApprovalApprovedDate,
					0 AS CurrentPhase,
					'' AS PhaseName,
					'' AS WorkflowName,
					'' AS NextPhase,
					'' AS DecisionMakers,
					0 AS DecisionType,
					'' AS OwnerName,
					CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,			
					NULL AS JobOwner,
					CONVERT(bit, 0) AS IsPhaseComplete,
					'' AS VersionSufix,
					CONVERT(bit, 0) AS IsLocked,
					CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop

END


GO


-- Alter Procedure by adding IsExternalPrimaryDecisionMaker property
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)	   
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.Deadline,
					a.CreatedDate,
					a.ModifiedDate,
					js.[Key] AS [Status],
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					LEFT OUTER JOIN FolderApproval fa
						ON fa.Approval = a.ID
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC'
					AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((a.CurrentPhase IS NULL 
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND EXISTS (
											SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
										)
					) = a.[Version])			
					OR (a.CurrentPhase IS NOT NULL 
						AND a.CurrentPhase = (SELECT avv.CurrentPhase 
											  FROM approval avv 
											  WHERE avv.Job = a.Job 
													AND a.[Version] = avv.[Version]
													AND avv.[Version] = (SELECT MAX([Version])
																		 FROM Approval av 
																		 WHERE av.Job = a.Job AND av.IsDeleted = 0)

													AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																						SELECT TOP 1 ac.ID 
																						FROM ApprovalCollaborator ac 
																						WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																					  )
																			)
																								 		
											  )
						)
					)	
					AND	@P_FolderId = fa.Folder
				
			UNION
			
			SELECT 	f.ID AS Approval,
					GetDate() AS Deadline,
					f.CreatedDate,
					f.ModifiedDate,
					'' AS [Status],
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase
			FROM	Folder f
			WHERE	f.Account = @P_Account
					AND (@P_Status = 0)
					AND f.IsDeleted = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
					AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN a.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN a.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
		FROM (
				SELECT  t.ID AS ID,
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						a.[Guid],
						a.[FileName],
						js.[Key] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
								     WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
						END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						t.PrimaryDecisionMakerName,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						at.[Key] AS ApprovalType,
						a.[Version] AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT CASE
						 WHEN ( EXISTS (SELECT aa.ID					
										 FROM dbo.ApprovalAnnotation aa
										 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
										 WHERE ap.Approval = t.ApprovalID			 
										 )
								) THEN CONVERT(bit,1)
								  ELSE CONVERT(bit,0)
						 END)  AS HasAnnotations,				
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
													                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
														JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
														WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				CONVERT(bit, 1) AS IsCollaborator,
				CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,				
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
				t.PhaseName,
				ISNULL((SELECT CASE
							WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
							ELSE ''
						END),'') AS WorkflowName,
				ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
				ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
				ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
				(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
							 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
							 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
							 END) AS OwnerName,
				a.AllowOtherUsersToBeAdded,
				j.JobOwner,
				(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = a.ID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker				
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN dbo.ApprovalType at
							ON 	at.ID = a.[Type]
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID	
						JOIN @TempItems t 
							ON t.ApprovalID = a.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 1
						
				UNION
				
				SELECT 	t.ID AS ID,
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						'' AS [Guid],
						'' AS [FileName],
						'' AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'' AS PrimaryDecisionMakerName,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS ApprovalType,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						CONVERT(bit, 0) AS HasAnnotations,
						'' AS Decision,
						CONVERT(bit, 1) AS IsCollaborator,
						f.AllCollaboratorsDecisionRequired,
						NULL as ApprovalApprovedDate,
						0 AS CurrentPhase,
						'' AS PhaseName,
						'' AS WorkflowName,
						'' AS NextPhase,
						'' AS DecisionMakers,
						0 AS DecisionType,
						'' AS OwnerName,
						CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
						NULL AS JobOwner,
						CONVERT(bit, 0) AS IsPhaseComplete,
						'' AS VersionSufix,
						CONVERT(bit, 0) AS IsLocked,
					    CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
				FROM	Folder f
						JOIN @TempItems t 
							ON t.ApprovalID = f.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 0
			) result		
	ORDER BY result.ID	

	SET ROWCOUNT 0

	---- Send the total rows count ---- 
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
						AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND EXISTS (
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END


GO


-- Alter Procedure by adding IsExternalPrimaryDecisionMaker property
ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID			
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] = 'ARC'
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND EXISTS (
									SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
								)
			) = a.[Version])			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																				SELECT TOP 1 ac.ID 
																				FROM ApprovalCollaborator ac 
																				WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																			  )
																	)
																						 		
								      )
				)
			)	
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,			
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 WHERE ap.Approval = t.ApprovalID			 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,							
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = a.ID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t 
				ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	 
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
	 
END
GO

-- Alter Procedure by adding IsExternalPrimaryDecisionMaker property
ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_HideCompletedApprovals bit, 
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			INNER JOIN Job j 
					ON j.ID = a.Job
			INNER JOIN JobStatus js 
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)									
	WHERE 
			js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END AND
			j.Account = @P_Account					
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND		auvi.[User] = @P_User
			AND ((a.CurrentPhase IS NULL AND(	SELECT MAX([ViewedDate]) 
												FROM ApprovalUserViewInfo vuvi
													INNER JOIN Approval av
														ON  av.ID = vuvi.[Approval]
													INNER JOIN Job vj
														ON vj.ID = av.Job	
												WHERE  av.Job = a.Job
													AND av.IsDeleted = 0
											) = auvi.[ViewedDate]
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
													)
			)						
		   OR (a.CurrentPhase IS NOT NULL  AND a.CurrentPhase = (SELECT av.CurrentPhase 
																  FROM ApprovalUserViewInfo avi
																  INNER JOIN Approval av ON av.ID =  avi.Approval
																  WHERE av.Job = a.Job
																  AND avi.[ViewedDate] = auvi.[ViewedDate]
																		AND avi.[ViewedDate] =( SELECT MAX([ViewedDate]) 
																								FROM ApprovalUserViewInfo vuvi
																								INNER JOIN Approval avv
																									ON  avv.ID = vuvi.[Approval]
																								INNER JOIN Job vj
																									ON vj.ID = avv.Job	
																								WHERE  avv.Job = a.Job
																								AND avv.IsDeleted = 0)
																		AND (EXISTS(
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Phase = av.CurrentPhase
																				  )
																				  OR ISNULL(j.JobOwner, 0) = @P_User
																			)
																		 
																)
				)
			)
	ORDER BY 
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE						
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
		END DESC,		
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT DISTINCT	t.ID,
			a.ID AS Approval,	
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],							 
			js.[Key] AS [Status],	
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			a.[Version] AS MaxVersion,						
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
				FROM Approval av
				WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.Approval aproval ON ap.Approval = aproval.ID
							 WHERE aproval.ID = t.ApprovalID AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = aproval.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																														WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase )
																												)
																			           )
																					   
															       )	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,			
			(SELECT CASE 
				WHEN a.LockProofWhenAllDecisionsMade = 1
					THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
				ELSE
					NULL
				END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0) 
				THEN(						     
						(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
						   THEN
								(SELECT TOP 1 appcd.[Key]
									FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
											JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
											WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
											ORDER BY ad.[Priority] ASC
										) appcd
								)
							ELSE
							(							    
								(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
											                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
								 THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
								     )
								 ELSE '0'
								 END 
								 )   			   
							)
							END
						)								
					)		
				WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
				  THEN
					(SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				ELSE
				 (SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				END	
				), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = a.ID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			JOIN @TempItems t 
					ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
			INNER JOIN Job j
					ON j.ID = a.Job
			INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	  
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
END
GO

ALTER TABLE [dbo].[Approval] ADD OverdueNotificationSent BIT NULL DEFAULT 0
GO
--------------------------------------------------------------------------------------------------Launched Test
--------------------------------------------------------------------------------------------------Launched STAGE
--------------------------------------------------------------------------------------------------Launched PRODUCTION