USE GMGCoZone
GO
---Change the precision for decimal type ---	
---CZD-693 Plans are not saved when Japanese Yen is selected as currency---

ALTER TABLE dbo.AttachedAccountBillingPlan ALTER COLUMN [NewPrice] [decimal](11,2) NULL
ALTER TABLE dbo.AttachedAccountBillingPlan ALTER COLUMN	[NewProofPrice] [decimal](11,2) NULL

ALTER TABLE [dbo].[BillingPlan] ALTER COLUMN [Price] [decimal](11,2) NOT NULL

ALTER TABLE [dbo].[AccountBillingPlanChangeLog] ALTER COLUMN [PreviousPlanAmount] [decimal](11,2) NULL
	
ALTER TABLE [dbo].[AccountBillingTransactionHistory] ALTER COLUMN [BaseAmount] [decimal](11,2) NOT NULL
ALTER TABLE [dbo].[AccountBillingTransactionHistory] ALTER COLUMN [DiscountAmount] [decimal](11,2) NOT NULL

GO

ALTER TABLE dbo.ColorProofInstance
ADD PairingCode NVARCHAR(64)
GO 

--Add StartProcessingTimeStamp for Define Service(avoid processing the same job twice)

ALTER TABLE dbo.Approval
ADD StartProcessingDate datetime
GO 

--Remove SoftProofing tab from account settings in case account doesn't have subscription plan for collaborate

ALTER PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId
        
        DECLARE @enableSoftProofingTools AS BIT = 0
        SELECT  @enableSoftProofingTools = COALESCE(SLCT.EnableSoftProofingTools,
                                                    0)
        FROM    ( SELECT    bpt.EnableSoftProofingTools
                  FROM      dbo.Account acc
                            INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                            INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                            INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                            INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                  WHERE     am.[Key] = 0 -- collaborate
                            AND acc.ID = @P_accountId
                  UNION ALL
                  SELECT    bpt.EnableSoftProofingTools
                  FROM      dbo.Account acc
                            INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                            INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                            INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                            INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                  WHERE     am.[Key] = 0 -- collaborate
                            AND acc.ID = @P_accountId
                ) AS SLCT
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
        IF ( @enableSoftProofingTools = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SESP'  -- MenuKey - varchar(4)
                          )
            END
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
            END
        
        

        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END

GO

-- Add column in table 'ApprovalAnnotation'
ALTER TABLE dbo.ApprovalAnnotation
ADD Guid nvarchar(36)  NOT NULL CONSTRAINT DF_ApprovalAnnotation_Guid DEFAULT (newid())
Go

-- Add Support to set external user as PDM
ALTER TABLE dbo.Approval
ADD ExternalPrimaryDecisionMaker  int 
Go

 ALTER TABLE [dbo].Approval  WITH CHECK ADD  CONSTRAINT [FK_Approval_ExternalCollaborator] FOREIGN KEY([ExternalPrimaryDecisionMaker])
   REFERENCES [dbo].[ExternalCollaborator] ([ID])  
GO

--Changes for supporting external user as PDM
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID, 
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
										WHERE Approval = a.ID
										ORDER BY acd.CompletedDate DESC
									) appcd
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.Decision
								FROM (SELECT TOP 1 acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker ORDER BY CompletedDate DESC) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
END
GO

--Filter For Billing Report Remove deleted accounts and show only direct childs
ALTER PROCEDURE [dbo].[SPC_ReturnAccountParameters] ( @P_LoggedAccount INT )
AS 
    BEGIN
        SET NOCOUNT ON
        SELECT  A.ID AS [AccountId] ,
                A.NAME AS [AccountName] ,
                AT.ID AS AccountTypeID ,
                AT.Name AS AccountTypeName ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.ID
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.ID
                         END, 0) AS [CollaborateBillingPlanId] ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.Name
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.NAme
                         END, '') AS [CollborateBillingPlanName] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.ID
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.ID
                         END, 0) AS [DeliverBillingPlanId] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.Name
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.NAme
                         END, '') AS [DeliverBillingPlanName] ,
                AST.ID AS AccountStatusID ,
                ASt.Name AS AccountStatusName ,
                CTY.ID AS LocationID ,
                CTY.ShortName AS LocationName
        FROM    dbo.Account A
                INNER JOIN AccountType AT ON A.AccountType = AT.ID
                INNER JOIN AccountStatus AST ON A.[Status] = AST.ID
                INNER JOIN Company C ON C.Account = A.ID
                INNER JOIN Country CTY ON C.Country = CTY.ID
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     ASP.Account = A.ID
                            ) CASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     ASP.Account = A.ID
                            ) DASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     SPI.Account = A.ID
                            ) CSPI
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     SPI.Account = A.ID
                            ) DSPI
             WHERE ast.[Key] != 'D' AND a.Parent = @P_LoggedAccount AND a.IsTemporary = 0
    END
GO	


--Account Report changes
CREATE VIEW [dbo].[AccountsReportView]
AS
SELECT
	0  AS ID, 
	'' AS Name,
	0 AS StatusId,
	'' AS StatusName,
	'' AS [Key],
	'' AS Domain,
	CONVERT(BIT, 0) AS IsCustomDomainActive,-- not null
	CAST('' AS NVARCHAR(128)) AS CustomDomain,--null
	0 AS AccountTypeID,
	GETDATE() AS CreatedDate,
	CONVERT(BIT, 0) AS IsTemporary,-- not null
	'' AS AccountTypeName,
	'' AS GivenName,
	'' AS FamilyName,
	0 AS Company ,
	0 AS Country ,
	'' AS CountryName,	
	CAST(0 AS INT) AS NoOfAccounts,
	CONVERT(DATETIME,GETDATE()) AS LastActivity,
	CAST('' AS NVARCHAR(128)) AS ChildAccounts,
	'' AS AccountPath

GO


CREATE PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN		
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath	
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID	
		
	WHERE	
		a.ID <> @P_LoggedAccount AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END

GO

--Remove SoftProofing tab from account settings in case account doesn't have subscription plan for collaborate

ALTER PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId
        
        DECLARE @enableSoftProofingTools AS BIT = 0
        SELECT  @enableSoftProofingTools = COALESCE(SLCT.EnableSoftProofingTools,
                                                    0)
        FROM    ( SELECT    bpt.EnableSoftProofingTools
                  FROM      dbo.Account acc
                            INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                            INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                            INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                            INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                  WHERE     am.[Key] = 0 -- collaborate
                            AND acc.ID = @P_accountId
                  UNION ALL
                  SELECT    bpt.EnableSoftProofingTools
                  FROM      dbo.Account acc
                            INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                            INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                            INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                            INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                  WHERE     am.[Key] = 0 -- collaborate
                            AND acc.ID = @P_accountId
                ) AS SLCT
        
        --Remove Profile tab for users that don't have access to Admin Module        
        DECLARE @enableProfileMenu BIT = ( SELECT   CASE WHEN ( SELECT
                                                              COUNT(rl.ID)
                                                              FROM
                                                              dbo.UserRole ur
                                                              JOIN dbo.Role rl ON ur.Role = rl.ID
                                                              JOIN dbo.AppModule apm ON rl.AppModule = apm.ID
                                                              WHERE
                                                              ur.[User] = @P_userId
                                                              AND (apm.[Key] = 3 OR apm.[Key] = 4)
                                                              AND (RL.[Key] = 'ADM' OR rl.[Key] = 'HQM' OR rl.[Key] = 'HQA')
                                                              ) > 0
                                                         THEN CONVERT(BIT, 1)
                                                         ELSE CONVERT(BIT, 0)
                                                    END
                                         )
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
        IF ( @enableSoftProofingTools = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SESP'  -- MenuKey - varchar(4)
                          )
            END
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
            END
            
        IF ( @enableProfileMenu = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEPF' --MenuKey - varchar(4)
                          )
            END   
        
        

        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END
	
GO


-- Account Report Details Dabatase Authorization
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Reports', -- Controller - nvarchar(128)
          'AccountReportDetails', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT  @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'RPAD' , -- Key - nvarchar(4)
          'Account Report Details' , -- Name - nvarchar(64)
          'Account Report Details'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'RPAR'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'HQA' OR R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'GMHQ') AND AM.[Key] in (3, 4)
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('RPAD')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

-- Add column in table 'UserGroup'
ALTER TABLE dbo.UserGroup
ADD Guid nvarchar(36)  NOT NULL CONSTRAINT DF_UserGroup_Guid DEFAULT (newid())
GO 

------------------------------------------------------------------------------Launched on debug


