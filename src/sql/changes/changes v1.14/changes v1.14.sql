USE GMGCoZone
GO
ALTER PROCEDURE [dbo].[SPC_ReturnUserLogin]
    (
      @P_Account INT ,
      @P_Username VARCHAR(255) ,
      @P_Password VARCHAR(255)
    )
AS 
    BEGIN
        SELECT	DISTINCT
                u.*
        FROM    [dbo].[User] u
                JOIN [dbo].[UserStatus] us ON u.[Status] = us.ID
        WHERE   u.[Account] = @P_Account
                AND u.[Username] = @P_Username
                AND u.[Password] = CONVERT(VARCHAR(255), HashBytes('SHA1',
                                                              @P_Password))
                AND ( us.[Key] = 'A'
                      OR us.[Key] = 'I'
                    )
    END
GO


ALTER VIEW [dbo].[ColorProofWorkflowInstanceView]
AS
SELECT  dbo.ColorProofCoZoneWorkflow.ID AS ColorProofCoZoneWorkflowID ,
        dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow ,
        dbo.ColorProofCoZoneWorkflow.Name AS CoZoneName ,
        dbo.ColorProofCoZoneWorkflow.IsAvailable ,
        dbo.ColorProofCoZoneWorkflow.TransmissionTimeout ,
        dbo.ColorProofWorkflow.ID AS ColorProofWorkflowID ,
        dbo.ColorProofWorkflow.Guid ,
        dbo.ColorProofWorkflow.Name ,
        dbo.ColorProofWorkflow.ProofStandard ,
        dbo.ColorProofWorkflow.MaximumUsablePaperWidth ,
        dbo.ColorProofWorkflow.IsActivated ,
        dbo.ColorProofWorkflow.IsInlineProofVerificationSupported ,
        dbo.ColorProofWorkflow.SupportedSpotColors ,
        dbo.ColorProofWorkflow.ColorProofInstance,
        dbo.ColorProofInstance.SystemName
FROM    dbo.ColorProofCoZoneWorkflow
        INNER JOIN dbo.ColorProofWorkflow ON dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow = dbo.ColorProofWorkflow.ID
        INNER JOIN dbo.ColorProofInstance ON dbo.ColorProofWorkflow.ColorProofInstance = dbo.ColorProofInstance.ID
WHERE   dbo.ColorProofInstance.IsDeleted = 0
        AND dbo.ColorProofCoZoneWorkflow.IsDeleted = 0
GO


CREATE VIEW UserRolesView AS
SELECT  U.ID ,
        U.GivenName ,
        u.FamilyName ,
        u.Username ,
        U.EmailAddress ,
        COALESCE(CR.[Key], 'NON') AS [CollaborateRole] ,
        COALESCE(DR.[Key], 'NON') AS [DeliverRole] ,
        COALESCE(AR.[Key], 'NON') AS [AdminRole] ,
        COALESCE(SR.[Key], 'NON') AS [SysAdminRole] ,
        u.DateLastLogin ,
        u.Account ,
        US.[Key] AS [UserStatus]
FROM    dbo.[User] AS U
        INNER JOIN dbo.UserStatus US ON U.Status = US.ID
        LEFT JOIN ( SELECT  CUR.[User] ,
                            CR.[Key]
                    FROM    dbo.UserRole CUR
                            INNER JOIN dbo.Role CR ON CUR.Role = CR.ID
                            INNER JOIN dbo.AppModule CAM ON CR.AppModule = CAM.ID
                                                            AND CAM.[Key] = 0
                  ) AS CR ON U.ID = CR.[User]
        LEFT JOIN ( SELECT  DUR.[User] ,
                            DR.[Key]
                    FROM    dbo.UserRole DUR
                            INNER JOIN dbo.Role DR ON DUR.Role = DR.ID
                            INNER JOIN dbo.AppModule DAM ON DR.AppModule = DAM.ID
                                                            AND DAM.[Key] = 2
                  ) AS DR ON U.ID = DR.[User]
        LEFT JOIN ( SELECT  AUR.[User] ,
                            AR.[Key]
                    FROM    dbo.UserRole AUR
                            INNER JOIN dbo.Role AR ON AUR.Role = AR.ID
                            INNER JOIN dbo.AppModule AAM ON AR.AppModule = AAM.ID
                                                            AND AAM.[Key] = 3
                  ) AS AR ON U.ID = AR.[User]
        LEFT JOIN ( SELECT  SUR.[User] ,
                            SR.[Key]
                    FROM    dbo.UserRole SUR
                            INNER JOIN dbo.Role SR ON SUR.Role = SR.ID
                            INNER JOIN dbo.AppModule SAM ON SR.AppModule = SAM.ID
                                                            AND SAM.[Key] = 4
                  ) AS SR ON U.ID = SR.[User]
GO

ALTER TABLE dbo.Account ADD IsTrial bit NOT NULL CONSTRAINT DF_Account_IsTrial DEFAULT (0)
GO

ALTER table dbo.MediaService
DROP constraint DF_MediaService_ApplySimulateOverprinting
GO

ALTER table dbo.MediaService
DROP column ApplySimulateOverprinting
GO
