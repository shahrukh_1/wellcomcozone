BEGIN TRANSACTION MigrationScript

SET NOCOUNT ON

-- STEP 1 Migrate Accounts to The New Plans

PRINT '-------- STEP 1 Migrate Collaborate Plans';  

-- Plan is 'Collaborate 50', ID is 63
-- Accounts

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = 63
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1399, 1562, 1592, 1708)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Plan is 'Collaborate  100', ID is 60	
-- Accounts

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = 60
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1669, 1819, 1837)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

-- Subscribers 
UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = 60
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1840)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Plan is 'Collaborate  150', ID is 59
-- Accounts

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = 59
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1723, 1750, 1800)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

-- Plan is 'Collaborate  250', ID is 62	
-- Accounts

UPDATE 
	ASP
SET  
	[SelectedBillingPlan] = 62
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (16, 1213, 1359, 1579, 1711, 1740)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Subscribers 
UPDATE 
	SPI
SET
	[SelectedBillingPlan] = 62
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1609, 1726, 1757, 1782, 1832, 1866, 1867, 1868)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Plan is 'Collaborate  500', ID is 65	
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 65
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1336)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Plan is 'Collaborate  1000', ID is 66	
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 66
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1774)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

-- Plan is 'Collaborate 2000', ID is 67	
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 67
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1447)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Plan is 'Collaborate Demo (30 Days)', ID is 39	
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 39
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1486,1630,1695,1698,1701,1713,1714,1722,1728,1739,1742,1747,1764,1770,1775,1776,1779,1785,1789,1814,1815,1824,1854,1858,1875,1876,1892,1900)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Subscribers 
UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = 39
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1878,1509,1677,1751,1760,1765,1768,1777,1780,1786,1803,1826,1827,1831,1844,1872,1893,1895)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

-- Plan is 'Collaborate Demo (No Timer)', ID is 10	
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 10
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1870,5,8,12,17,21,39,49,64,67,900,945,946,949,996,1020,1092,1127,1141,1292,1318,1319,1348,1363,1388,1394,1403,1405,1418,1445,1454,1476,1480,1493,1500,1501,1549,1555,1565,1572,1575,1581,1611,1649,1651,1653,1656,1666,1667,1680,1683,1697,1704,1755,1794,1801,1809,1810,1818,1822,1828,1841,1842,1845,1869,1888,1890,1891,1894,1896)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

-- Subscribers 
UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = 10
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1315,1806,1808,1811)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

-- Plan is 'Collaborate-Wellcom', ID is 68	
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 68
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1689,1795)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
-- Subscribers 
UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = 68
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1796,1797,1798,1799,1807,1821,1829,1830,1874,1886,1887)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)

PRINT '-------- STEP 2 Migrate Deliver Plans';

-- Plan is '1-2 ColorProof Connections', ID is 41
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 41
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1641)
AND 
	BPT.ID IN (4, 9)
	
-- Plan is '3-5 ColorProof Connections', ID is 42
-- Accounts

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = 42
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1456)
AND 
	BPT.ID IN (4, 9)

-- Subscribers 
UPDATE 
	SPI
SET  
	[SelectedBillingPlan] = 42
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1509)
AND 
	BPT.ID IN (4, 9)
	
-- Plan is 'Deliver Demo', ID is 46
-- Accounts

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = 46
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1390,1870,16,949,1213,1383,1476,1486,1500,1501,1565,1714,1728,1764,1770,1776,1785,1815,1818,1854,1858,1876,1892)
AND 
	BPT.ID IN (4, 9)
	
-- Subscribers 
UPDATE 
	SPI
SET
	[SelectedBillingPlan] = 46
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1777,1780,1786,1893,1895)
AND 
	BPT.ID IN (4, 9)

-- Plan is 'Deliver Demo (No Timer)', ID is 48
-- Accounts

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = 48
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (1555,5,12,17,21,39,49,64,67,900,945,946,996,1020,1092,1141,1292,1319,1348,1363,1388,1394,1403,1405,1445,1454,1480,1493,1549,1572,1581,1611,1649,1651,1653,1656,1666,1667,1680,1689,1795,1801,1809,1810,1828,1841,1842,1845,1888,1890,1894,1896)
AND 
	BPT.ID IN (4, 9)

-- Subscribers 
UPDATE 
	SPI
SET
	[SelectedBillingPlan] = 48
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1315,1677,1805,1808,1811,1831)
AND 
	BPT.ID IN (4, 9)

PRINT '-------- STEP 2.1 Insert missing accounts subscription plans'

-- Acccount 1888 has no entry in AccountSubscriptionPlan for deliver billing plan type
INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan]
           ([SelectedBillingPlan]
           ,[IsMonthlyBillingFrequency]
           ,[ChargeCostPerProofIfExceeded]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (48
           ,1
           ,0
           ,GETDATE()
           ,1888
           ,GETDATE()
           ,0)

-- Account 1811 has no entry in SubscriberPlanInfo for collaborate billing plan type
INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo]
           ([NrOfCredits]
           ,[IsQuotaAllocationEnabled]
           ,[ChargeCostPerProofIfExceeded]
           ,[SelectedBillingPlan]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (0
           ,0
           ,0
           ,10
           ,GETDATE()
           ,1811
           ,GETDATE()
           ,1)

-- STEP 3 Add Change Log?

-- They have to be added only if plan changes [AccountBillingPlanChangeLog]

-- STEP 4 Verify Collaborate Accounts & Plans

PRINT '-------- STEP 4 Verify Collaborate Accounts & Plans';  

-- Plan is 'Collaborate 50', ID is 63
-- Accounts
SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 63) IS NULL THEN A.ID END AS 'Accounts - Collaborate 50'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1399, 1562, 1592, 1708)
	
-- Plan is 'Collaborate  100', ID is 60	
-- Accounts
SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 60) IS NULL THEN A.ID END AS 'Accounts - Collaborate 100'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1669, 1819, 1837)

-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 60) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 100'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1840)
	
-- Plan is 'Collaborate  150', ID is 59
-- Accounts
SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 59) IS NULL THEN A.ID END AS 'Accounts - Collaborate 100'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1723, 1750, 1800)
	
-- Plan is 'Collaborate  250', ID is 62	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 62) IS NULL THEN A.ID END AS 'Accounts - Collaborate 250'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (16, 1213, 1359, 1579, 1711, 1740)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 62) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 250'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1609, 1726, 1757, 1782, 1832, 1866, 1867, 1868)

-- Plan is 'Collaborate  500', ID is 65	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 65) IS NULL THEN A.ID END AS 'Accounts - Collaborate  500'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1336)
	
-- Plan is 'Collaborate  1000', ID is 66	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 66) IS NULL THEN A.ID END AS 'Accounts - Collaborate  1000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1774)
	
-- Plan is 'Collaborate 2000', ID is 67	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 67) IS NULL THEN A.ID END AS 'Accounts - Collaborate  2000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1447)
	
-- Plan is 'Collaborate Demo (30 Days)', ID is 39	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 39) IS NULL THEN A.ID END AS 'Accounts - Collaborate Demo (30 Days)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1486,1630,1695,1698,1701,1713,1714,1722,1728,1739,1742,1747,1764,1770,1775,1776,1779,1785,1789,1814,1815,1824,1854,1858,1875,1876,1892,1900)

-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 39) IS NULL THEN A.ID END AS 'Subscribers - Collaborate Demo (30 Days)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1878,1509,1677,1751,1760,1765,1768,1777,1780,1786,1803,1826,1827,1831,1844,1872,1893,1895)
	
-- Plan is 'Collaborate Demo (No Timer)', ID is 10	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 10) IS NULL THEN A.ID END AS 'Accounts - Collaborate Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1870,5,8,12,17,21,39,49,64,67,900,945,946,949,996,1020,1092,1127,1141,1292,1318,1319,1348,1363,1388,1394,1403,1405,1418,1445,1454,1476,1480,1493,1500,1501,1549,1555,1565,1572,1575,1581,1611,1649,1651,1653,1656,1666,1667,1680,1683,1697,1704,1755,1794,1801,1809,1810,1818,1822,1828,1841,1842,1845,1869,1888,1890,1891,1894,1896)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 10) IS NULL THEN A.ID END AS 'Subscribers - Collaborate Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1315,1806,1808,1811)
	
-- Plan is 'Collaborate-Wellcom', ID is 68	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 68) IS NULL THEN A.ID END AS 'Accounts - Collaborate-Wellcom'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1689,1795)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 68) IS NULL THEN A.ID END AS 'Subscribers - Collaborate-Wellcom'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1796,1797,1798,1799,1807,1821,1829,1830,1874,1886,1887)
	
-- STEP 5 Verify Deliver Accounts & Plans

PRINT '-------- STEP 5 Verify Deliver Accounts & Plans';  

-- Plan is '1-2 ColorProof Connections', ID is 41
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 41) IS NULL THEN A.ID END AS 'Accounts - 1-2 ColorProof Connections'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1641)
	
-- Plan is '3-5 ColorProof Connections', ID is 42
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 42) IS NULL THEN A.ID END AS 'Accounts - 3-5 ColorProof Connections'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1456)

-- Subscribers
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 42) IS NULL THEN A.ID END AS 'Subscribers - 3-5 ColorProof Connections'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1509)
	
-- Plan is 'Deliver Demo', ID is 46
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 46) IS NULL THEN A.ID END AS 'Accounts - Deliver Demo'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1390,1870,16,949,1213,1383,1476,1486,1500,1501,1565,1714,1728,1764,1770,1776,1785,1815,1818,1854,1858,1876,1892)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 46) IS NULL THEN A.ID END AS 'Subscribers - Deliver Demo'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1777,1780,1786,1893,1895)
	
-- Plan is 'Deliver Demo (No Timer)', ID is 48
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = 48) IS NULL THEN A.ID END AS 'Accounts - Deliver Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1555,5,12,17,21,39,49,64,67,900,945,946,996,1020,1092,1141,1292,1319,1348,1363,1388,1394,1403,1405,1445,1454,1480,1493,1549,1572,1581,1611,1649,1651,1653,1656,1666,1667,1680,1689,1795,1801,1809,1810,1828,1841,1842,1845,1888,1890,1894,1896)

-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = 48) IS NULL THEN A.ID END AS 'Subscribers - Deliver Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (1315,1677,1805,1808,1811,1831)

-- STEP 6 Delete/Keep? Old Data
PRINT '-------- STEP 6 Cleanup';  

-- UPDATE BELLOW IDS OF BILLING PLANS BOTH COLLABORATE AND DELIVER THAT WILL BE DELETED

declare @billingPlansToBeDeleted table (id int);
insert @billingPlansToBeDeleted(id) values (1),(2),(3),(4),(5),(6),(7),(11),(12),(13),(14),(15),(16),(17),(18),(21),(22),(23),(24),(25),(28),(32),(34),(35),(36),(37),(38),(40),(49),(50),(51),(52),(53),(57),(58),(61),(69)

DELETE 
	appcm
FROM 
	[GMGCoZone].[dbo].AccountPricePlanChangedMessage appcm
INNER JOIN 
	[GMGCoZone].[dbo].ChangedBillingPlan cbp
ON 
	appcm.ChangedBillingPlan = cbp.ID
WHERE 
	cbp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted) OR appcm.ChangedBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].ChangedBillingPlan WHERE AttachedAccountBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].AttachedAccountBillingPlan AS aabp WHERE aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)))

DELETE 
	CBP 
FROM
	[GMGCoZone].[dbo].ChangedBillingPlan AS CBP
WHERE 
	CBP.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted) OR CBP.AttachedAccountBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].AttachedAccountBillingPlan AS aabp WHERE aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted))

DELETE 
	aabp
FROM 
	[GMGCoZone].[dbo].AttachedAccountBillingPlan aabp
WHERE 
	aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)

DELETE 
	ASP
FROM 
	[GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
WHERE 
	SelectedBillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)
	
DELETE 
	SPI
FROM 
	[GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
WHERE 
	SelectedBillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)

DELETE 
	[GMGCoZone].[dbo].AccountBillingPlanChangeLog
WHERE 
	CurrentPlan IN (SELECT id FROM @billingPlansToBeDeleted)

DELETE 
	[GMGCoZone].[dbo].BillingPlan
WHERE 
	ID IN (SELECT id FROM @billingPlansToBeDeleted)

SET NOCOUNT OFF

ROLLBACK TRANSACTION MigrationScript;
