--servicegraphics.gmgcozone.com
--	Collaborate 30 Day Demo
--thevalleygroup.gmgcozone.com
--	Collaborate 30 Day Demo
--esope.gmgcozone.com
--	Collaborate Demo no timer
--ezflow.gmgcozone.com
--	Collaborate Demo no timer
--bigfish.gmgcozone.com
--	Collaborate 30 Day Demo
--citenum.gmgcozone.com
--	Colaborate Demo no timer
--dtouch.gmgcozone.com
--	Colaborate Demo no timer

BEGIN TRAN 

-- Collaborate Demo 30 Day 
--SELECT * FROM [GMGCoZone].[dbo].BillingPlan WHERE ID = 39

-- Accounts 1919, 1920, 1959
--1919	Service Graphics '2017-03-13 15:26:19.2263781'
--1920	The Valley Group '2017-03-14 11:35:40.3964410'
--1959	Big Fish '2017-06-07 08:25:39.5101367'

UPDATE 
	ASP
SET 
	[ContractStartDate] = '2017-03-13 15:26:19.2263781'
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
WHERE 
	ASP.[Account] = 1919
	
UPDATE 
	ASP
SET 
	[ContractStartDate] = '2017-03-14 11:35:40.3964410'
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
WHERE 
	ASP.[Account] = 1920

UPDATE 
	ASP
SET 
	[ContractStartDate] = '2017-06-07 08:25:39.5101367'
FROM 
	[GMGCoZone].[dbo].SubscriberPlanInfo AS ASP
WHERE 
	ASP.[Account] = 1959
	
SELECT * FROM [GMGCoZone].[dbo].[Account] WHERE ID IN (1919, 1920, 1959)
SELECT * FROM [GMGCoZone].[dbo].AccountSubscriptionPlan WHERE Account IN (1919, 1920, 1959)
SELECT * FROM [GMGCoZone].[dbo].SubscriberPlanInfo WHERE Account IN (1919, 1920, 1959)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan]
           ([SelectedBillingPlan]
           ,[IsMonthlyBillingFrequency]
           ,[ChargeCostPerProofIfExceeded]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (39
           ,1
           ,0
           ,GETDATE()
           ,1919
           ,GETDATE()
           ,0)
           
INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan]
           ([SelectedBillingPlan]
           ,[IsMonthlyBillingFrequency]
           ,[ChargeCostPerProofIfExceeded]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (39
           ,1
           ,0
           ,GETDATE()
           ,1920
           ,GETDATE()
           ,0)
           
INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo]
           ([NrOfCredits]
           ,[IsQuotaAllocationEnabled]
           ,[ChargeCostPerProofIfExceeded]
           ,[SelectedBillingPlan]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (0
           ,0
           ,0
           ,39
           ,GETDATE()
           ,1959
           ,GETDATE()
           ,0)

-- Collaborate Demo No Timer
--1779	CITENUM
--1900	Dtouch
--1931	ESOPE '2017-04-06 09:02:28.3098000'
--1933	ezFlow '2017-04-14 15:11:29.3809151'
--SELECT * FROM [GMGCoZone].[dbo].BillingPlan WHERE ID = 10

UPDATE 
	ASP
SET 
	[ContractStartDate] = '2017-04-14 15:11:29.3809151'
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
WHERE 
	ASP.[Account] = 1933

UPDATE 
	ASP
SET 
	[ContractStartDate] = '2017-04-06 09:02:28.3098000'
FROM 
	[GMGCoZone].[dbo].SubscriberPlanInfo AS ASP
WHERE 
	ASP.[Account] = 1931

-- Accounts 1931, 1933, 1779, 1900
SELECT * FROM [GMGCoZone].[dbo].[Account] WHERE ID IN (1931, 1933, 1779, 1900)
SELECT * FROM [GMGCoZone].[dbo].AccountSubscriptionPlan WHERE Account IN (1931, 1933, 1779, 1900)
SELECT * FROM [GMGCoZone].[dbo].SubscriberPlanInfo WHERE Account IN (1931, 1933, 1779, 1900)

-- Pay attention to existing contract start date!
UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = 10
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
WHERE 
	ASP.[Account] IN (1779, 1900)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo]
           ([NrOfCredits]
           ,[IsQuotaAllocationEnabled]
           ,[ChargeCostPerProofIfExceeded]
           ,[SelectedBillingPlan]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (0
           ,0
           ,0
           ,10
           ,GETDATE()
           ,1931
           ,GETDATE()
           ,0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan]
           ([SelectedBillingPlan]
           ,[IsMonthlyBillingFrequency]
           ,[ChargeCostPerProofIfExceeded]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
     VALUES
           (10
           ,1
           ,0
           ,GETDATE()
           ,1933
           ,GETDATE()
           ,0)

ROLLBACK 
