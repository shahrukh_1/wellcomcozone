--SELECT * FROM Account WHERE Name LIKE '%Taylors%'
--SELECT * FROM AccountSubscriptionPlan WHERE Account = 1602
--SELECT * FROM attachedAccountBillingPlan WHERE Account = 1602

--SELECT * FROM BillingPlan WHERE ID = 24
--SELECT * FROM BillingPlanType WHERE ID = 1

BEGIN TRAN 

--DELETE FROM BillingPlan WHERE ID = 24

ROLLBACK 

BEGIN TRAN 

-- Should this plan made available only for Taylors? Test specify Account!

DECLARE @TaylorsAccountID int 
SELECT @TaylorsAccountID = ID FROM Account WHERE Name LIKE '%Taylors%'

INSERT INTO 
	BillingPlanType ([Key], Name, MediaService, EnablePrePressTools, EnableMediaTools, AppModule, EnableSoftProofingTools, Account)
VALUES 
	('Essentials', 'Essentials', 1, 1, 1, 1, 0, @TaylorsAccountID)

DECLARE @BillingPlanTypeID int 
SELECT @BillingPlanTypeID = ID FROM BillingPlanType WHERE Name = 'Essentials'

INSERT INTO	
	BillingPlan (Name, [Type], Proofs, Price, IsFixed, SoftProofingWorkstations, ColorProofConnections, IsDemo, MaxUsers, MaxStorageInGb, Account)
VALUES 
	('Essentials PayPerUse', @BillingPlanTypeID, 1, 1.00, 0, 0, NULL, 0, NULL, NULL, @TaylorsAccountID)

DECLARE @BillingPlanID int 
SELECT @BillingPlanID = ID FROM BillingPlan WHERE Name = 'Essentials PayPerUse'

INSERT INTO	
	AccountSubscriptionPlan	(SelectedBillingPlan, IsMonthlyBillingFrequency, ChargeCostPerProofIfExceeded, ContractStartDate, Account, AccountPlanActivationDate, AllowOverdraw)
VALUES 
	(@BillingPlanID,1,1, '2015-03-17 14:50:54.0760000', @TaylorsAccountID, '2015-06-11 18:57:14.0384340', 1)

INSERT INTO
	AttachedAccountBillingPlan (Account, BillingPlan, NewPrice, IsAppliedCurrentRate, NewProofPrice, IsModifiedProofPrice)
VALUES 
	(@TaylorsAccountID, @BillingPlanID, 1.00, 1, 1.00, 0)

SELECT * FROM BillingPlanType WHERE ID = @BillingPlanTypeID
SELECT * FROM BillingPlan WHERE ID = @BillingPlanID
SELECT * FROM AccountSubscriptionPlan WHERE Account = @TaylorsAccountID
SELECT * FROM AttachedAccountBillingPlan WHERE BillingPlan = @BillingPlanID 

ROLLBACK 