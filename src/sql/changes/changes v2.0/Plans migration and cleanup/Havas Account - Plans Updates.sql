BEGIN TRAN 

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = 68
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (1831)
AND 
	BPT.ID IN (1, 2, 3, 8, 15)
	
ROLLBACK 

-- Existing data
-- [SubscriberPlanInfo]
-- 18	Demo Unlimited (GMG Only)	3	10000	0.00	0	10	0	0	NULL	NULL	NULL
-- 48	Deliver Demo (No Timer)	9	10	0.00	0	0	99	0	NULL	NULL	NULL