BEGIN TRANSACTION MigrateBillingPlansScript

-- STEP 1 Migrate Accounts to The New Plans

PRINT '-------- STEP 1 Migrate Collaborate Plans';  

-- Plan is 'Collaborate 50', ID is 135
-- Accounts

declare @accountsForCollaborate50 table (id int);
insert @accountsForCollaborate50(id) values(646),(621),(281),(82),(630),(547),(650),(154),(647),(620),(626),(346),(4),(631),(633),(671),(673),(488),(643),(415),(657),(667),(645),(629)

declare @collaborate50 int
set @collaborate50 = 135

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = @collaborate50 
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate50)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Subscribers 

declare @subscribersForCollaborate50 table (id int);
insert @subscribersForCollaborate50(id) values(648),(658),(660),(613),(623),(632)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate50
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate50)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Plan is 'Collaborate  250', ID is 138
-- Accounts

declare @accountsForCollaborate250 table (id int);
insert @accountsForCollaborate250(id) values(234),(625),(87),(618)

declare @collaborate250 int
set @collaborate250 = 138

UPDATE 
	ASP
SET  
	[SelectedBillingPlan] = @collaborate250
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate250)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Subscribers

declare @subscribersForCollaborate250 table (id int);
insert @subscribersForCollaborate250(id) values(661),(662),(614)
 
UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate250
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate250)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Plan is 'Collaborate  500', ID is 139
-- Accounts

declare @collaborate500 int
set @collaborate500 = 139

declare @accountsForCollaborate500 table (id int);
insert @accountsForCollaborate500(id) values(653),(627)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaborate500
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate500)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Subscribers

declare @subscribersForCollaborate500 table (id int);
insert @subscribersForCollaborate500(id) values(499),(649)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate500
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate500)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Plan is 'Collaborate  1000', ID is 140
-- Accounts

declare @collaborate1000 int
set @collaborate1000 = 140

declare @accountsForCollaborate1000 table (id int);
insert @accountsForCollaborate1000(id) values(624)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaborate1000
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate1000)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)

-- Plan is 'Collaborate 2000', ID is 141
-- Accounts

declare @collaborate2000 int
set @collaborate2000 = 141

declare @accountsForCollaborate2000 table (id int);
insert @accountsForCollaborate2000(id) values(70),(622)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaborate2000
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate2000)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Subscribers

declare @subscribersForCollaborate2000 table (id int);
insert @subscribersForCollaborate2000(id) values(617)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate2000
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate2000)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)
	
-- Plan is 'Collaborate Demo (No Timer)', ID is 143
-- Accounts

declare @collaboratedemonotimer int
set @collaboratedemonotimer = 143

declare @accountsForCollaborateDemoNoTimer table (id int);
insert @accountsForCollaborateDemoNoTimer(id) values(2),(3),(359),(659),(454),(655),(674),(670)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaboratedemonotimer
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT ID FROM @accountsForCollaborateDemoNoTimer)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)

-- Subscribers 

declare @subscribersForCollaborateDemoNoTimer table (id int);
insert @subscribersForCollaborateDemoNoTimer(id) values(644),(656)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaboratedemonotimer
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborateDemoNoTimer)
AND 
	BPT.ID IN (1,2,7,10,11,15,16,29,35,36,39,40,45,46,48,51,53,54,55,57,59,60)

PRINT '-------- STEP 2 Migrate Deliver Plans';
	
-- Plan is 'Deliver Demo', ID is 145
-- Accounts

declare @deliverdemo int
set @deliverdemo = 145

declare @accountsForDeliverDemo table (id int);
insert @accountsForDeliverDemo(id) values(645),(624),(234),(646),(621),(281),(82),(547),(647),(618),(620),(4),(671),(359),(454),(488),(670)

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = @deliverdemo
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT ID FROM @accountsForDeliverDemo)
AND 
	BPT.ID IN (3,4,9,21,42,61)
	
-- Subscribers 

declare @subscribersForDeliverDemo table (id int);
insert @subscribersForDeliverDemo(id) values(648),(658),(613)

UPDATE 
	SPI
SET
	[SelectedBillingPlan] = @deliverdemo
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT ID FROM @subscribersForDeliverDemo)
AND 
	BPT.ID IN (3,4,9,21,42,61)

-- Plan is 'Deliver Demo (No Timer)', ID is 146
-- Accounts

declare @deliverdemonotimer int
set @deliverdemonotimer = 146

declare @accountsForDeliverDemoNoTimer table (id int);
insert @accountsForDeliverDemoNoTimer(id) values(2),(629),(653),(625),(3),(87),(643)


UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @deliverdemonotimer
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT ID FROM @accountsForDeliverDemoNoTimer)
AND 
	BPT.ID IN (3,4,9,21,42,61)

-- Subscribers 

declare @subscribersForDeliverDemoNoTimer table (id int);
insert @subscribersForDeliverDemoNoTimer(id) values(617),(499),(614)

UPDATE 
	SPI
SET
	[SelectedBillingPlan] = @deliverdemonotimer
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForDeliverDemoNoTimer)
AND 
	BPT.ID IN (3,4,9,21,42,61)

PRINT '-------- STEP 2.1 Insert missing accounts subscription plans'

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@deliverdemonotimer,GETDATE(),617,GETDATE(),1)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),620,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),645,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),646,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),647,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@deliverdemo,GETDATE(),648,GETDATE(),1)

-- STEP 3 Add Change Log?

-- They have to be added only if plan changes [AccountBillingPlanChangeLog]

-- STEP 4 Verify Collaborate Accounts & Plans

PRINT '-------- STEP 4 Verify Collaborate Accounts & Plans';  

-- Plan is 'Collaborate 50', ID is 135
-- Accounts
SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate50) IS NULL THEN A.ID END AS 'Accounts - Collaborate 50'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate50)

-- Subscribers	
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaborate50) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 50'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborate50)
	
-- Plan is 'Collaborate  250', ID is 138
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate250) IS NULL THEN A.ID END AS 'Accounts - Collaborate 100'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate250)

-- Subscribers
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaborate250) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 50'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborate250)

-- Plan is 'Collaborate 500', ID is 139
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate500) IS NULL THEN A.ID END AS 'Accounts - Collaborate  500'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate500)
	
-- Subscribers	
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaborate500) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 500'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborate500)
	
-- Plan is 'Collaborate 1000', ID is 140	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate1000) IS NULL THEN A.ID END AS 'Accounts - Collaborate  1000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate1000)
	
-- Plan is 'Collaborate 2000', ID is 141	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate2000) IS NULL THEN A.ID END AS 'Accounts - Collaborate 2000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate2000)
	
-- Subscribers	
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaborate2000) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 2000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborate2000)

-- Plan is 'Collaborate Demo (No Timer)', ID is 143	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaboratedemonotimer) IS NULL THEN A.ID END AS 'Accounts - Collaborate Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborateDemoNoTimer)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaboratedemonotimer) IS NULL THEN A.ID END AS 'Subscribers - Collaborate Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborateDemoNoTimer)
	
-- STEP 5 Verify Deliver Accounts & Plans

PRINT '-------- STEP 5 Verify Deliver Accounts & Plans';  

-- Plan is 'Deliver Demo', ID is 145
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @deliverdemo) IS NULL THEN A.ID END AS 'Accounts - Deliver Demo'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForDeliverDemo)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @deliverdemo) IS NULL THEN A.ID END AS 'Subscribers - Deliver Demo'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForDeliverDemo)
	
-- Plan is 'Deliver Demo (No Timer)', ID is 146
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @deliverdemonotimer) IS NULL THEN A.ID END AS 'Accounts - Deliver Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForDeliverDemoNoTimer)

-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @deliverdemonotimer) IS NULL THEN A.ID END AS 'Subscribers - Deliver Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForDeliverDemoNoTimer)

-- STEP 5 Delete/Keep? Old Data

-- UPDATE BELLOW IDS OF BILLING PLANS

--declare @billingPlansToBeDeleted table (id int);
--insert @billingPlansToBeDeleted(id) values(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(15),(16),(17),(18),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(40),(41),(42),(43),(44),(46),(47),(51),(52),(53),(54),(55),(62),(67),(71),(72),(78),(79),(80),(81),(82),(84),(85),(86),(87),(88),(91),(93),(94),(95),(98),(99),(100),(101),(103),(104),(105),(107),(109),(110),(111),(113),(115),(118),(120),(122),(124),(131),(132),(133),(134); 	

--DELETE 
--	appcm 
--FROM 
--	[GMGCoZone].[dbo].AccountPricePlanChangedMessage appcm
--INNER JOIN 
--	[GMGCoZone].[dbo].ChangedBillingPlan cbp
--ON 
--	appcm.ChangedBillingPlan = cbp.ID
--WHERE 
--	cbp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted) OR appcm.ChangedBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].ChangedBillingPlan WHERE AttachedAccountBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].AttachedAccountBillingPlan AS aabp WHERE aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)))

--DELETE 
--	CBP 
--FROM
--	[GMGCoZone].[dbo].ChangedBillingPlan AS CBP
--WHERE 
--	CBP.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted) OR CBP.AttachedAccountBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].AttachedAccountBillingPlan AS aabp WHERE aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted))

--DELETE 
--	aabp
--FROM 
--	[GMGCoZone].[dbo].AttachedAccountBillingPlan aabp
--WHERE 
--	aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)

--DELETE 
--	ASP
--FROM 
--	[GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
--WHERE 
--	SelectedBillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)
	
--DELETE 
--	SPI
--FROM 
--	[GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
--WHERE 
--	SelectedBillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)

--DELETE 
--	[GMGCoZone].[dbo].AccountBillingPlanChangeLog
--WHERE
--	CurrentPlan IN (SELECT id FROM @billingPlansToBeDeleted)
	
--DELETE 
--	[GMGCoZone].[dbo].BillingPlan
--WHERE 
--	ID IN (SELECT id FROM @billingPlansToBeDeleted)
	
--DELETE 
--	[GMGCoZone].[dbo].BillingPlanType
--WHERE
--	ID IN (1,2,3,4,7,9,10,11,15,16,21,29,35,36,39,40,42,45,46,48,51,53,54,55,57)

ROLLBACK TRANSACTION MigrateBillingPlansScript;	