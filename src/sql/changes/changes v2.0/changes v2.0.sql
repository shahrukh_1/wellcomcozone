USE GMGCoZone
GO

---Add IsSSOUser column to Users table
ALTER TABLE [GMGCoZone].[dbo].[User]
ADD
	IsSsoUser BIT NOT NULL DEFAULT 0
GO 

--Add sub menu item to Settings - Single sign-on
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
                    'SingleSignOn', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
SELECT @controllerId , -- ControllerAction - int
      mi.ID , -- Parent - int
      8 , -- Position - int
      NULL , -- IsAdminAppOwned - bit
      1 , -- IsAlignedLeft - bit
      0 , -- IsSubNav - bit
      0 , -- IsTopNav - bit
      1 , -- IsVisible - bit
      'SISO' , -- Key - nvarchar(4)
      'Single Sign On' , -- Name - nvarchar(64)
      'Single Sign On'  -- Title - nvarchar(128)
    FROM dbo.MenuItem mi
    where [Key] = 'SETT'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 3
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SISO')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

ALTER PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_ViewAllFromRecycle bit = 0,
	@P_UserRoleKey NVARCHAR(4) = '',
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
    SELECT
		( SELECT Count(approval.ID)
		  FROM (SELECT DISTINCT a.ID,
					   rank() over (partition by a.Job order by a.[Version] desc) rnk
				 FROM	
						Job j WITH (NOLOCK)
						INNER JOIN Approval a WITH (NOLOCK)
							ON a.Job = j.ID
						INNER JOIN JobStatus js WITH (NOLOCK)
							ON js.ID = j.[Status]
						JOIN ApprovalCollaborator ac WITH (NOLOCK) 
							ON ac.Approval = a.ID				
					WHERE	
						j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
						AND (
							   @P_ViewAll = 1 
								 OR
								 (
								   @P_ViewAll = 0 
								   AND 											   
									(@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0))	)
								  )
							 )
					 AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)			   
						
				) approval
		 WHERE rnk = 1
		) AS AllCount, 
		( SELECT
			 COUNT(approval.ID)
		   FROM 
		   (SELECT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM	
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]							
			 WHERE j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					AND (ISNULL(j.JobOwner, 0) = @P_User OR a.[Owner] = @P_User)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)	
			) approval
		  where rnk = 1	
		) AS OwnedByMeCount,		
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM	
		            Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]									
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
						AND ( ISNULL(j.JobOwner, 0) != @P_User AND a.[Owner] != @P_User)
						 AND EXISTS (
										SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac WITH (NOLOCK)
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
									)																				
										   					
						AND (
								@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)
			 ) approval
		  where rnk = 1		
		) AS SharedCount,
		(SELECT COUNT(DISTINCT j.ID)
			    FROM ApprovalUserViewInfo auvi WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
						ON a.ID = auvi.Approval 
				INNER JOIN Job j WITH (NOLOCK)
						ON j.ID = a.Job 
				INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]
				INNER JOIN ApprovalCollaborator ac WITH (NOLOCK)
						ON ac.Approval = a.ID				
			 WHERE	auvi.[User] = @P_User
				AND j.Account = @P_Account
				AND a.IsDeleted = 0
				AND a.DeletePermanently = 0
				AND js.[Key] != 'ARC'
				AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)			
				AND (
					 @P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)	
				AND ((@P_User = ac.Collaborator AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
						  )
						 OR ISNULL(j.JobOwner, 0) = @P_User)
		) AS RecentlyViewedCount,
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]
				JOIN ApprovalCollaborator ac WITH (NOLOCK)
					ON ac.Approval = a.ID					
			 WHERE j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] = 'ARC'
					AND (
						   @P_ViewAll = 1 
							 OR
							 (
							   @P_ViewAll = 0 AND 											   
								(@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
																		  )
								)
							  )
						 )
			) approval
			where rnk = 1
		 ) AS ArchivedCount,
		(SELECT 
			    SUM(recycle.Approvals) AS RecycleCount
			FROM (
			          SELECT COUNT(DISTINCT a.ID) AS Approvals
						FROM	Job j WITH (NOLOCK)
								INNER JOIN Approval a WITH (NOLOCK)
									ON a.Job = j.ID
								INNER JOIN JobStatus js WITH (NOLOCK)
									ON js.ID = j.[Status]
								INNER JOIN ApprovalCollaborator ac WITH (NOLOCK)
									ON ac.Approval = a.ID		
						WHERE j.Account = @P_Account
							AND a.IsDeleted = 1
							AND a.DeletePermanently = 0
							AND ((SELECT COUNT(f.ID) FROM Folder f WITH (NOLOCK) INNER JOIN FolderApproval fa WITH (NOLOCK) ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
							AND (
							      ISNULL((SELECT TOP 1 ac.ID
										FROM ApprovalCollaborator ac WITH (NOLOCK)
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
										, 
										(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph WITH (NOLOCK)
										WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
									  ) IS NOT NULL				
								)
							AND (
								 @P_ViewAllFromRecycle = 1 
								 OR
								 ( 
									@P_ViewAllFromRecycle = 0 AND (@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)))
								 )	
							)	
					 
					UNION
						SELECT COUNT(f.ID) AS Approvals
						FROM	Folder f WITH (NOLOCK)
						WHERE	f.Account = @P_Account
								AND f.IsDeleted = 1
								AND ((SELECT COUNT(pf.ID) FROM Folder pf WITH (NOLOCK) WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
								AND f.Creator = @P_User								
					
				)recycle
		) AS RecycleCount,	
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk			
			FROM Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]
				JOIN ApprovalCollaboratorDecision acd WITH (NOLOCK)
					ON acd.Approval = a.ID								
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'COM'
					AND  acd.Collaborator = @P_User AND ISNULL(acd.Phase, 0) = ISNULL(a.CurrentPhase, 0) AND acd.Decision IS NULL
					AND (
							js.[Key] != 'COM'
						)
		     ) approval
			where rnk = 1
		) AS MyOpenApprovalsCount,
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			FROM	
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]	
				JOIN ApprovalCollaborator ac WITH (NOLOCK)
					ON ac.Approval = a.ID
			WHERE	
					j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					AND  (@P_ViewAll = 1 
													 OR
													 (
													   @P_ViewAll = 0 AND 											   
														(@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
																								  )
														)
													  )
									)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
						)
					AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
					AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
					AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
					AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WITH (NOLOCK) WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
					AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WITH (NOLOCK) WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
						 OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WITH (NOLOCK) WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
					AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WITH (NOLOCK) WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw WITH (NOLOCK) where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
					AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (Select ajp.ID  FROM ApprovalJobPhase ajp WITH (NOLOCK) WHERE ajp.PhaseTemplateID in (Select val FROM dbo.splitString(@P_ByPhase, ',')))))							
					) approval
			where rnk = 1
			) AS AdvanceSearchCount
	--OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
END
GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalJobPhase] 
ADD [OverdueNotificationSent] BIT NOT NULL DEFAULT(0)
GO




