BEGIN TRAN 

SELECT * FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan]

DECLARE @AccountID int 

DECLARE @ParentIds TABLE (ID INT)
DELETE FROM @ParentIds
INSERT INTO @ParentIds SELECT ID FROM [GMGCoZone].[dbo].[Account] WHERE (Parent IS NOT NULL)

DECLARE MY_CURSOR CURSOR 
FOR SELECT Id FROM [GMGCoZone].[dbo].[Account] WHERE (Parent IS NOT NULL)

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @AccountID
WHILE @@FETCH_STATUS = 0
BEGIN 
	DECLARE @ChildIds TABLE (ID INT)
	DELETE FROM @ChildIds
	INSERT INTO @ChildIds SELECT ID FROM [GMGCoZone].[dbo].[Account] WHERE Parent = @AccountID AND Parent IS NOT NULL 
	
	DECLARE @MissingBillingPlanIds TABLE (ID INT)
	DELETE FROM @MissingBillingPlanIds
	INSERT INTO @MissingBillingPlanIds SELECT DISTINCT SelectedBillingPlan FROM [GMGCoZone].[dbo].AccountSubscriptionPlan WHERE Account IN (SELECT * FROM @ChildIds) AND SelectedBillingPlan IN (10,39,46,48,59,60,62,63,65,66,67)
	-- Accounts: INSERT THEM TO ATTACHED BILLING PLANS
	DECLARE @MissingBillingPlanId int 
	DECLARE MY_CURSOR1 CURSOR 
	FOR SELECT ID FROM @MissingBillingPlanIds

	OPEN MY_CURSOR1
	FETCH NEXT FROM MY_CURSOR1 INTO @MissingBillingPlanId
	WHILE @@FETCH_STATUS = 0
	BEGIN 
	
	INSERT INTO 
		[GMGCoZone].[dbo].[AttachedAccountBillingPlan] (Account, BillingPlan, NewPrice, IsAppliedCurrentRate, NewProofPrice, IsModifiedProofPrice)
	VALUES( 
		@AccountId, 
		@MissingBillingPlanId, 
		(SELECT Price FROM [GMGCoZone].[dbo].BillingPlan WHERE ID = @MissingBillingPlanId),
		COALESCE((SELECT TOP 1 IsAppliedCurrentRate FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan] WHERE BillingPlan = @MissingBillingPlanId), 1),
		COALESCE((SELECT TOP 1 NewProofPrice FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan] WHERE BillingPlan = @MissingBillingPlanId), 0),
		COALESCE((SELECT TOP 1 IsModifiedProofPrice FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan] WHERE BillingPlan = @MissingBillingPlanId), 0))
		
	FETCH NEXT FROM MY_CURSOR1 INTO @MissingBillingPlanId
	END
	CLOSE MY_CURSOR1
	DEALLOCATE MY_CURSOR1
	
	DELETE FROM @MissingBillingPlanIds
	INSERT INTO @MissingBillingPlanIds SELECT DISTINCT SelectedBillingPlan FROM [GMGCoZone].[dbo].SubscriberPlanInfo WHERE Account IN (SELECT * FROM @ChildIds) AND SelectedBillingPlan IN (10,39,46,48,59,60,62,63,65,66,67)
	-- Subscribers: INSERT THEM TO ATTACHED BILLING PLANS
	DECLARE MY_CURSOR2 CURSOR 
	FOR SELECT ID FROM @MissingBillingPlanIds

	OPEN MY_CURSOR2
	FETCH NEXT FROM MY_CURSOR2 INTO @MissingBillingPlanId
	WHILE @@FETCH_STATUS = 0
	BEGIN 
	
	INSERT INTO 
		[GMGCoZone].[dbo].[AttachedAccountBillingPlan] (Account, BillingPlan, NewPrice, IsAppliedCurrentRate, NewProofPrice, IsModifiedProofPrice)
	VALUES( 
		@AccountId, 
		@MissingBillingPlanId, 
		(SELECT Price FROM [GMGCoZone].[dbo].BillingPlan WHERE ID = @MissingBillingPlanId),
		COALESCE((SELECT TOP 1 IsAppliedCurrentRate FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan] WHERE BillingPlan = @MissingBillingPlanId), 1),
		COALESCE((SELECT TOP 1 NewProofPrice FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan] WHERE BillingPlan = @MissingBillingPlanId), 0),
		COALESCE((SELECT TOP 1 IsModifiedProofPrice FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan] WHERE BillingPlan = @MissingBillingPlanId), 0))
		
	FETCH NEXT FROM MY_CURSOR2 INTO @MissingBillingPlanId
	END
	CLOSE MY_CURSOR2
	DEALLOCATE MY_CURSOR2
	
	PRINT @AccountID
	
    -- Do something with Id here
    FETCH NEXT FROM MY_CURSOR INTO @AccountID
END
CLOSE MY_CURSOR
DEALLOCATE MY_CURSOR

SELECT * FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan]

ROLLBACK

