BEGIN TRANSACTION MigrateBillingPlansScript

-- STEP 1 Migrate Accounts to The New Plans

PRINT '-------- STEP 1 Migrate Collaborate Plans';  

-- Plan is 'Collaborate 50', ID is 73
-- Accounts

declare @accountsForCollaborate50 table (id int);
insert @accountsForCollaborate50(id) values(6),(376),(39)

declare @collaborate50 int
set @collaborate50 = 73

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = @collaborate50 
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate50)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

	
-- Plan is 'Collaborate  100', ID is 74
-- Accounts

declare @accountsForCollaborate100 table (id int);
insert @accountsForCollaborate100(id) values(15),(367),(372),(364),(361),(369),(8),(375),(9),(10),(374),(11),(344),(342),(357),(85),(283),(356),(129),(38),(16),(353),(347),(326),(368),(349),(351)

declare @collaborate100 int
set @collaborate100 = 74

UPDATE 
	ASP
SET  
	[SelectedBillingPlan] = @collaborate100
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate100)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)
	
-- Subscribers

declare @subscribersForCollaborate100 table (id int);
insert @subscribersForCollaborate100(id) values(340)
 
UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate100
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate100)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

-- Plan is 'Collaborate  250', ID is 76
-- Accounts

declare @accountsForCollaborate250 table (id int);
insert @accountsForCollaborate250(id) values(366)

declare @collaborate250 int
set @collaborate250 = 76

UPDATE 
	ASP
SET  
	[SelectedBillingPlan] = @collaborate250
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate250)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

-- Plan is 'Collaborate  500', ID is 77
-- Accounts

declare @collaborate500 int
set @collaborate500 = 77

declare @accountsForCollaborate500 table (id int);
insert @accountsForCollaborate500(id) values(350),(339),(337),(338),(23),(12),(2)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaborate500
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate500)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)
	
-- Subscribers

declare @subscribersForCollaborate500 table (id int);
insert @subscribersForCollaborate500(id) values(362),(43)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate500
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate500)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)
	
-- Plan is 'Collaborate  1000', ID is 78
-- Accounts

declare @collaborate1000 int
set @collaborate1000 = 78

declare @accountsForCollaborate1000 table (id int);
insert @accountsForCollaborate1000(id) values(31),(19),(18),(17),(317),(346),(325),(4),(76),(358),(352),(348),(373),(343),(13),(331),(14),(34),(332)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaborate1000
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate1000)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

-- Subscribers

declare @subscribersForCollaborate1000 table (id int);
insert @subscribersForCollaborate1000(id) values(20),(40),(312)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaborate1000
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborate1000)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

-- Plan is 'Collaborate 2000', ID is 79
-- Accounts

declare @collaborate2000 int
set @collaborate2000 = 79

declare @accountsForCollaborate2000 table (id int);
insert @accountsForCollaborate2000(id) values(334)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaborate2000
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT id FROM @accountsForCollaborate2000)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)
	
-- Plan is 'Collaborate Demo (No Timer)', ID is 81
-- Accounts

declare @collaboratedemonotimer int
set @collaboratedemonotimer = 81

declare @accountsForCollaborateDemoNoTimer table (id int);
insert @accountsForCollaborateDemoNoTimer(id) values(22),(3),(359),(319),(381),(355)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @collaboratedemonotimer
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT ID FROM @accountsForCollaborateDemoNoTimer)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

-- Subscribers 

declare @subscribersForCollaborateDemoNoTimer table (id int);
insert @subscribersForCollaborateDemoNoTimer(id) values(5),(363)

UPDATE 
	SPI
SET 
	[SelectedBillingPlan] = @collaboratedemonotimer
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT id FROM @subscribersForCollaborateDemoNoTimer)
AND 
	BPT.ID IN (1,2,22,24,26,27,28,31,32)

PRINT '-------- STEP 2 Migrate Deliver Plans';
	
-- Plan is 'Deliver Demo', ID is 83
-- Accounts

declare @deliverdemo int
set @deliverdemo = 83

declare @accountsForDeliverDemo table (id int);
insert @accountsForDeliverDemo(id) values(31),(19),(18),(17),(346),(350),(339),(337),(325),(338),(6),(376),(4),(39),(15),(23),(3),(367),(372),(364),(12),(359),(361),(369),(8),(375),(9),(76),(10),(374),(11),(344),(342),(357),(358),(319),(352),(381),(348),(355),(373),(343),(13),(331),(14),(366),(2),(85),(34),(283),(356),(129),(332),(334),(38),(16),(353),(347),(326),(368),(349),(351)

UPDATE 
	ASP
SET 
	[SelectedBillingPlan] = @deliverdemo
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT ID FROM @accountsForDeliverDemo)
AND 
	BPT.ID IN (3,4,33)
	
-- Subscribers 

declare @subscribersForDeliverDemo table (id int);
insert @subscribersForDeliverDemo(id) values(20),(362),(43),(340),(345),(5),(40),(312),(363)

UPDATE 
	SPI
SET
	[SelectedBillingPlan] = @deliverdemo
FROM 
	[GMGCoZone].[dbo].[SubscriberPlanInfo] AS SPI
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	SPI.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	SPI.[Account] IN (SELECT ID FROM @subscribersForDeliverDemo)
AND 
	BPT.ID IN (3,4,33)

-- Plan is 'Deliver Demo (No Timer)', ID is 84
-- Accounts

declare @deliverdemonotimer int
set @deliverdemonotimer = 84

declare @accountsForDeliverDemoNoTimer table (id int);
insert @accountsForDeliverDemoNoTimer(id) values(317),(22)

UPDATE 
	ASP
SET
	[SelectedBillingPlan] = @deliverdemonotimer
FROM 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan] AS ASP
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlan] as BP 
ON
	ASP.SelectedBillingPlan = BP.ID
INNER JOIN 
	[GMGCoZone].[dbo].[BillingPlanType] as BPT
ON
	BP.[Type] = BPT.ID
WHERE 
	ASP.[Account] IN (SELECT ID FROM @accountsForDeliverDemoNoTimer)
AND 
	BPT.ID IN (3,4,33)

PRINT '-------- STEP 2.1 Insert missing accounts subscription plans'

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@collaborate100,1,0,GETDATE(),38,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@collaborate500,GETDATE(),43,GETDATE(),1)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),34,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),38,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),129,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),325,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),326,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),331,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),342,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),343,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),344,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),346,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),347,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),348,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),349,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),350,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),351,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),352,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),353,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),355,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),356,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),357,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),358,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),361,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),364,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),368,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),369,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),373,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[AccountSubscriptionPlan] ([SelectedBillingPlan],[IsMonthlyBillingFrequency],[ChargeCostPerProofIfExceeded],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (@deliverdemo,1,0,GETDATE(),381,GETDATE(),0)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@deliverdemo,GETDATE(),340,GETDATE(),1)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@deliverdemo,GETDATE(),345,GETDATE(),1)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@deliverdemo,GETDATE(),362,GETDATE(),1)

INSERT INTO [GMGCoZone].[dbo].[SubscriberPlanInfo] ([NrOfCredits],[IsQuotaAllocationEnabled],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[ContractStartDate],[Account],[AccountPlanActivationDate],[AllowOverdraw])
VALUES (0,0,0,@deliverdemo,GETDATE(),363,GETDATE(),1)

-- STEP 3 Add Change Log?

-- They have to be added only if plan changes [AccountBillingPlanChangeLog]

-- STEP 4 Verify Collaborate Accounts & Plans

PRINT '-------- STEP 4 Verify Collaborate Accounts & Plans';  

-- Plan is 'Collaborate 50', ID is 73
-- Accounts
SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate50) IS NULL THEN A.ID END AS 'Accounts - Collaborate 50'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate50)
	
-- Plan is 'Collaborate 100', ID is 74
-- Accounts
SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate100) IS NULL THEN A.ID END AS 'Accounts - Collaborate 100'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate100)

-- Subscribers	
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaborate100) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 100'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborate100)
	
-- Plan is 'Collaborate  250', ID is 76
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate250) IS NULL THEN A.ID END AS 'Accounts - Collaborate 250'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate250)

-- Plan is 'Collaborate 500', ID is 77
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate500) IS NULL THEN A.ID END AS 'Accounts - Collaborate  500'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate500)
	
-- Subscribers	
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaborate500) IS NULL THEN A.ID END AS 'Subscribers - Collaborate 500'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborate500)
	
-- Plan is 'Collaborate 1000', ID is 78	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate1000) IS NULL THEN A.ID END AS 'Accounts - Collaborate  1000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate1000)
	
-- Plan is 'Collaborate 2000', ID is 79	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaborate2000) IS NULL THEN A.ID END AS 'Accounts - Collaborate 2000'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborate2000)

-- Plan is 'Collaborate Demo (No Timer)', ID is 81	
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @collaboratedemonotimer) IS NULL THEN A.ID END AS 'Accounts - Collaborate Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForCollaborateDemoNoTimer)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @collaboratedemonotimer) IS NULL THEN A.ID END AS 'Subscribers - Collaborate Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForCollaborateDemoNoTimer)
	
-- STEP 5 Verify Deliver Accounts & Plans

PRINT '-------- STEP 5 Verify Deliver Accounts & Plans';  

-- Plan is 'Deliver Demo', ID is 83
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @deliverdemo) IS NULL THEN A.ID END AS 'Accounts - Deliver Demo'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForDeliverDemo)
	
-- Subscribers 
SELECT 
	CASE WHEN (SELECT TOP 1 SPI.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
               WHERE  A.ID = SPI.Account AND SPI.SelectedBillingPlan = @deliverdemo) IS NULL THEN A.ID END AS 'Subscribers - Deliver Demo'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @subscribersForDeliverDemo)
	
-- Plan is 'Deliver Demo (No Timer)', ID is 84
-- Accounts

SELECT 
	CASE WHEN (SELECT TOP 1 asp.SelectedBillingPlan as HasPlan
               FROM   [GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
               WHERE  A.ID = ASP.Account AND ASP.SelectedBillingPlan = @deliverdemonotimer) IS NULL THEN A.ID END AS 'Accounts - Deliver Demo (No Timer)'
FROM   
	[GMGCoZone].[dbo].Account AS A
WHERE  
	A.ID IN (SELECT id FROM @accountsForDeliverDemoNoTimer)

-- STEP 5 Delete/Keep? Old Data

-- TODO: UPDATE BELLOW IDS OF BILLING PLANS

--declare @billingPlansToBeDeleted table (id int);
--insert @billingPlansToBeDeleted(id) values(1),(2),(3),(4),(5),(6),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(23),(24),(26),(27),(28),(30),(58),(59),(60),(61),(62),(63),(65),(66),(67),(68),(69),(85);

--DELETE 
--	appcm 
--FROM 
--	[GMGCoZone].[dbo].AccountPricePlanChangedMessage appcm
--INNER JOIN 
--	[GMGCoZone].[dbo].ChangedBillingPlan cbp
--ON 
--	appcm.ChangedBillingPlan = cbp.ID
--WHERE 
--	cbp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted) OR appcm.ChangedBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].ChangedBillingPlan WHERE AttachedAccountBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].AttachedAccountBillingPlan AS aabp WHERE aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)))

--DELETE 
--	CBP 
--FROM
--	[GMGCoZone].[dbo].ChangedBillingPlan AS CBP
--WHERE 
--	CBP.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted) OR CBP.AttachedAccountBillingPlan IN (SELECT ID FROM [GMGCoZone].[dbo].AttachedAccountBillingPlan AS aabp WHERE aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted))

--DELETE 
--	aabp
--FROM 
--	[GMGCoZone].[dbo].AttachedAccountBillingPlan aabp
--WHERE 
--	aabp.BillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)

--DELETE 
--	ASP
--FROM 
--	[GMGCoZone].[dbo].AccountSubscriptionPlan AS ASP
--WHERE 
--	SelectedBillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)
	
--DELETE 
--	SPI
--FROM 
--	[GMGCoZone].[dbo].SubscriberPlanInfo AS SPI
--WHERE 
--	SelectedBillingPlan IN (SELECT id FROM @billingPlansToBeDeleted)

--DELETE 
--	[GMGCoZone].[dbo].AccountBillingPlanChangeLog
--WHERE
--	CurrentPlan IN (SELECT id FROM @billingPlansToBeDeleted)
	
--DELETE 
--	[GMGCoZone].[dbo].BillingPlan
--WHERE 
--	ID IN (SELECT id FROM @billingPlansToBeDeleted)
	
--DELETE 
--	[GMGCoZone].[dbo].BillingPlanType
--WHERE
--	ID IN (1,2,3,4,22,24,26,27,28)

ROLLBACK TRANSACTION MigrateBillingPlansScript;	