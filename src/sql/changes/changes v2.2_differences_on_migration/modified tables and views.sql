ALTER TABLE [dbo].[RolePresetEventType] ALTER COLUMN [Preset] INT NULL;
GO

ALTER TABLE [dbo].[USER] ALTER COLUMN [Password] NVARCHAR(64) NOT NULL;
GO

-- Columns

CREATE TABLE [dbo].[CollaborateAPILog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Date] [datetime] NOT NULL,
[Thread] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Level] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Logger] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Exception] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

/****** Object:  View [dbo].[ReturnApprovalCountsView]    Script Date: 04/04/2013 10:16:34 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnApprovalCountsView]'))
DROP VIEW [dbo].[ReturnApprovalCountsView]
GO

-------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount,
			0 AS MyOpenApprovalsCount,
			0 AS AdvanceSearchCount


GO

ALTER FUNCTION [dbo].[GetApprovalPrimaryDecisionMakers]
(
	@P_CurrentPhase INT,
	@P_Approval INT,
	@P_PrimaryDecisionMaker INT,
	@P_ExternalPrimaryDecisionMaker INT
)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN	
	DECLARE @String NVARCHAR(MAX);
	DECLARE @Names NVARCHAR(MAX);
	SET @String =  ISNULL((SELECT CASE WHEN @P_CurrentPhase IS NOT NULL 
								THEN(SELECT CASE WHEN (SELECT TOP 1 dt.[Key]  
													   FROM dbo.DecisionType dt
													   JOIN dbo.ApprovalJobPhase ajp on dt.ID = ajp.DecisionType
													   JOIN dbo.ApprovalJobPhaseApproval a ON ajp.ID = a.Phase											   
													   WHERE a.Approval = @P_Approval AND a.Phase = @P_CurrentPhase) = 'PDM'

												 THEN (SELECT CASE WHEN ISNULL(@P_PrimaryDecisionMaker, 0) != 0 
																THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = @P_PrimaryDecisionMaker)
															 WHEN ISNULL(@P_ExternalPrimaryDecisionMaker, 0) != 0
																THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = @P_ExternalPrimaryDecisionMaker)
															 ELSE NULL
															 END)

												 ELSE (SELECT STUFF(( SELECT ', ' + result.Name
																	  FROM
																			(SELECT u.GivenName + ' ' + u.FamilyName As Name
																			 FROM [User] u
																			 INNER JOIN ApprovalCollaborator ac ON u.ID = ac.Collaborator
																			 INNER JOIN ApprovalCollaboratorRole acr on ac.ApprovalCollaboratorRole = acr.ID
																			 INNER JOIN dbo.UserStatus us ON u.[Status] = us.ID
																			 WHERE ac.Approval = @P_Approval AND ac.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND us.[Key] != 'D'
																			 UNION
																			 SELECT ec.GivenName + ' ' + ec.FamilyName As Name
																			 FROM ExternalCollaborator ec
																			 JOIN SharedApproval sa ON ec.ID = sa.ExternalCollaborator
																			 JOIN ApprovalCollaboratorRole acr on sa.ApprovalCollaboratorRole = acr.ID
																			 WHERE sa.Approval = @P_Approval AND sa.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND ec.IsDeleted = 0) result 
																			 FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)'),1,2,'') )
											   END
										 )
								   ELSE NULL
								   END)	,											 
		           (SELECT CASE WHEN ISNULL(@P_PrimaryDecisionMaker, 0) != 0 
									THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = @P_PrimaryDecisionMaker)
								WHEN ISNULL(@P_ExternalPrimaryDecisionMaker, 0) != 0
									THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = @P_ExternalPrimaryDecisionMaker)
								ELSE ''
								END)
					)       
  	   	  RETURN  @String;					 	  				   

END


GO

-- Stored Procedure


--Alter procedure to retry in case of deadlock
ALTER PROCEDURE [dbo].[SPC_ReturnDeliversPageInfo]
    (
      @P_Account INT ,
      @P_User INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
    DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;

        DECLARE @Delivers TABLE
            (
              ID INT ,
              [Title] NVARCHAR(128) ,
              [User] NVARCHAR(64) ,
              [Pages] NVARCHAR(50) ,
              [CPName] NVARCHAR(50) ,
              [Workflow] NVARCHAR(256) ,
              [CreatedDate] DATETIME ,
              [StatusKey] INT ,
              [Account] INT ,
              [OwnUser] INT,
              [IsShared] BIT
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  Title ,
                  [User] ,
                  Pages ,
                  CPName ,
                  Workflow ,
                  CreatedDate ,
                  StatusKey ,
                  Account ,
                  OwnUser,
                  IsShared
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] = @P_User
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'CON' -- contributor should only see his jobs
                  UNION
                --- deliver jobs for admin users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' -- admin should see all the jobs from current account
                  UNION
                  --- deliver shared jobs for admins -----
                  SELECT DISTINCT	DJ.ID ,
									J.Title ,
									[User].GivenName AS [User] ,
									DJ.Pages ,
									CPI.SystemName AS [CPName] ,
									CPCZW.Name AS [Workflow] ,
									DJ.CreatedDate ,
									Status.[Key] AS [StatusKey] ,
									J.Account AS [Account] ,
									DJ.[owner] AS [OwnUser],
									CONVERT(BIT, 1) AS [IsShared]
                  FROM DeliverJob DJ
                       JOIN dbo.Job J ON DJ.Job = J.ID
                       JOIN dbo.ColorProofCoZoneWorkflow CPCZW ON CPCZW.ID = DJ.CPWorkflow
                       JOIN dbo.SharedColorProofWorkflow SCPW ON SCPW.ColorProofCoZoneWorkflow = CPCZW.ID
                       CROSS APPLY (SELECT U.GivenName,
										   U.Account
									FROM dbo.[User] U
									JOIN Account ACC on U.Account = ACC.ID
								    WHERE U.Account = (SELECT Account FROM [User] WHERE ID = SCPW.[USER]) AND U.ID = DJ.[Owner]
                                 ) [User] ( GivenName, Account )
                       JOIN dbo.ColorProofWorkflowInstanceView CPWIV ON SCPW.ColorProofCoZoneWorkflow = CPWIV.ColorProofCoZoneWorkflowID
                       JOIN dbo.ColorProofInstance CPI ON CPWIV.ColorProofInstance = CPI.ID
                       JOIN dbo.[User] CPUO ON CPI.Owner = CPUO.ID
                       JOIN dbo.Account AC ON CPUO.Account = AC.ID                    
                       JOIN dbo.AccountStatus ACS ON AC.[Status] = ACS.ID
                       CROSS APPLY (SELECT  DJS.[Key]
									FROM  dbo.DeliverJobStatus DJS
									WHERE  DJS.ID = DJ.Status
									) Status ( [Key] ) 
                  WHERE ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                          )
                          AND DJ.IsDeleted = 0
                          AND J.Account <> @P_Account
			              AND ACS.[Key] != 'D'
                          AND AC.ID = @P_Account 
                          AND SCPW.IsAccepted = 1 
                          AND SCPW.IsEnabled = 1
                          AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' 
                  UNION
                --- deliver jobs for manager and moderator users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND DJ.[owner] IN (
                            SELECT  *
                            FROM    dbo.[GetUserListWhereBelongs](@P_User) )
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) IN (
                            'MAN', 'MOD' ) -- moderator and manager should see their jobs + jobs of the users that belongs to the same user group                  
                )

            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.CreatedDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.CreatedDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.Title
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.Title
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.[User]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.[User]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Pages]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Pages]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CpName]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CpName]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[Workflow]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[Workflow]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[StatusKey]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[StatusKey]
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset

        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT

                SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			IF (ERROR_NUMBER() = 1205)
				SET @retry = @retry - 1;
			ELSE
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop	

    END
GO

-- Stored Procedure

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteNotCompletedApprovals
-- Description:  This SP deletes the Approvals from tne database but need to manually delete files from S3
-- Date Created: Tuesday, 09 Aprill 2013
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_DeleteNotCompletedApprovals]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY

	DECLARE @ApprovalID INT
	DECLARE @getApprovalID CURSOR
	SET @getApprovalID = CURSOR FOR 		
		SELECT	a.ID
		FROM [dbo].[Approval] a
		WHERE (a.CreatedDate < DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), -1))
				AND (SELECT MIN(Progress) FROM ApprovalPage ap WHERE ap.Approval = a.ID ) < 100 
	OPEN @getApprovalID

	FETCH NEXT
		FROM @getApprovalID INTO @ApprovalID
			WHILE @@FETCH_STATUS = 0
			BEGIN
				EXECUTE [dbo].[SPC_DeleteApproval] @ApprovalID
			FETCH NEXT
			FROM @getApprovalID INTO @ApprovalID
			END
	CLOSE @getApprovalID
	DEALLOCATE @getApprovalID

	SET @P_Success = ''

    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;

	SELECT @P_Success AS RetVal

END



GO

-- Stored Procedure

CREATE PROCEDURE [dbo].[SPC_DeleteAccount](
	@P_Id int
)
AS
	BEGIN
	SET XACT_ABORT ON;  
	BEGIN TRAN

    --DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY

	----- Get users ids of the deleted accounts -----
	DECLARE @DeletedUsers TABLE
	(	  
	   UserID INT
	)

	INSERT INTO @DeletedUsers (UserID)
	SELECT ID FROM [dbo].[User] 
	WHERE Account = @P_Id	
	--------------------------------------------------

	---- Delete records base on deleted accounts ids -------
	DELETE FROM [dbo].[AccountBillingPlanChangeLog]
	WHERE Account = @P_Id

	DELETE FROM [dbo].[AccountBillingTransactionHistory]
	WHERE Account = @P_Id

	DELETE FROM [dbo].[AccountHelpCentre]
	WHERE Account = @P_Id	

	DELETE FROM [dbo].[AccountSetting]
	WHERE Account = @P_Id

	DELETE FROM [dbo].[AccountSubscriptionPlan]
	WHERE Account = @P_Id	

	DELETE FROM [dbo].[Company]
	WHERE Account = @P_Id

	DELETE FROM [dbo].[SubscriberPlanInfo]
	WHERE Account = @P_Id

	DELETE FROM [dbo].[UploadEngineProfile]
	WHERE Account = @P_Id	

	DELETE FROM [dbo].[AccountPricePlanChangedMessage]
	WHERE ChildAccount = @P_Id

	DELETE [dbo].[ThemeColorScheme]
	FROM [dbo].[ThemeColorScheme] tcs
		JOIN [dbo].[AccountTheme] act ON tcs.AccountTheme = act.ID
	WHERE act.Account = @P_Id

	DELETE [dbo].[AccountTheme]
	FROM [dbo].[AccountTheme] act
	WHERE act.Account = @P_Id

    DELETE [dbo].[ThemeColorScheme]
	FROM [dbo].[ThemeColorScheme] tcs
		JOIN [dbo].[BrandingPresetTheme] bpt ON tcs.BrandingPresetTheme = bpt.ID
		JOIN [dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	WHERE bp.Account = @P_Id

	DELETE [dbo].[BrandingPresetTheme]
	FROM [dbo].[BrandingPresetTheme] bpt
		JOIN [dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	where bp.Account = @P_Id

	DELETE [dbo].[BrandingPresetUserGroup]
	FROM [dbo].[BrandingPresetUserGroup] bpug
			JOIN [dbo].[BrandingPreset] bp ON bpug.BrandingPreset = bp.ID			
	WHERE bp.[Account] = @P_Id

	DELETE FROM [dbo].[ApprovalCustomDecision]
	WHERE Account = @P_Id

    DELETE FROM [GMGCoZone].[dbo].[InactiveApprovalStatuses]
	WHERE Account = @P_Id

	---------------------------------------------------------

	----- Delete record from CMS tables ---------	

	DELETE FROM [GMGCoZone].[dbo].[CMSAnnouncement]
	WHERE Account = @P_Id

	DELETE FROM [GMGCoZone].[dbo].[CMSDownload]
	WHERE Account = @P_Id

	DELETE FROM [GMGCoZone].[dbo].[CMSManual]
	WHERE Account = @P_Id

	DELETE FROM [GMGCoZone].[dbo].[CMSVideo]
	WHERE Account = @P_Id

	---------------------------------------------------------

	----- Delete record from tables related to user ---------	
	DELETE FROM [dbo].[UserSetting] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[UserRole] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)	

	DELETE FROM [dbo].[UserLogin] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[UserHistory] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers) ---- OR [Creator]

	DELETE [dbo].[UserGroupUser] 
	FROM [dbo].[UserGroupUser] ugu
			JOIN [dbo].[UserGroup] ug ON ugu.UserGroup = ug.ID
	WHERE ug.Account = @P_Id

	DELETE FROM [dbo].[UserCustomPresetEventTypeValue] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[UserApprovalReminderEmailLog] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[StudioSetting] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[SharedColorProofWorkflow] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[SharedColorProofWorkflow] 
    FROM [dbo].[SharedColorProofWorkflow] scp	
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON scp.ColorProofCoZoneWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[NotificationScheduleUserEventTypeEmailLog] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)	

	DELETE FROM [dbo].[NotificationItem] 
	WHERE [InternalRecipient] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[NotificationItem] 
	WHERE [Creator] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[CurrencyRate] 
	WHERE [Creator] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[ApprovalUserViewInfo] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[ApprovalUserRecycleBinHistory] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[ApprovalCollaborator] 
	WHERE [Collaborator] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[ApprovalCollaborator] 
	FROM [dbo].[ApprovalCollaborator] ac
			JOIN [dbo].[Approval] a ON ac.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE FROM [dbo].[ApprovalAnnotationStatusChangeLog] 
	FROM [dbo].[ApprovalAnnotationStatusChangeLog]  aasc
			JOIN [dbo].[ApprovalAnnotation] aa ON aasc.Annotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE FROM [dbo].[ApprovalAnnotationPrivateCollaborator] 
	WHERE [Collaborator] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[ApprovalAnnotationPrivateCollaborator]
	FROM [dbo].[ApprovalAnnotationPrivateCollaborator] aapc
			JOIN [dbo].[ApprovalAnnotation] aa ON aapc.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id						
	---------------------------------------------------------


	-- Delete records from tables taht are related with Approval --------
	DELETE FROM [dbo].[ApprovalCollaboratorDecision] 	
	WHERE Collaborator IN (SELECT UserID FROM @DeletedUsers)	

	DELETE [dbo].[ApprovalJobPhaseApproval]
	FROM [dbo].[ApprovalJobPhaseApproval] ajpa
			JOIN [dbo].[Approval] a ON ajpa.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id 

	DELETE [dbo].[ApprovalCollaboratorDecision]
	FROM [dbo].[ApprovalCollaboratorDecision] acd
			JOIN [dbo].[Approval] a ON acd.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE [dbo].[ApprovalJobViewingCondition]
	FROM [dbo].[ApprovalJobViewingCondition] ajc
			JOIN [dbo].[Job] j ON ajc.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE [dbo].[SharedApproval]
	FROM [dbo].[SharedApproval] sa
			JOIN [dbo].[Approval] a ON sa.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id 

	DELETE [dbo].[DeliverSharedJob]
	FROM [dbo].[DeliverSharedJob] dsj
			JOIN [dbo].[DeliverExternalCollaborator] dc ON dsj.DeliverExternalCollaborator = dc.ID			
	WHERE dc.Account = @P_Id

	DELETE [dbo].[DeliverSharedJob] 
	FROM [dbo].[DeliverSharedJob] dsj
			JOIN [dbo].[DeliverJob] dj ON dsj.[DeliverJob] =  dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[ApprovalAnnotationAttachment]
	FROM [dbo].[ApprovalAnnotationAttachment] at
			JOIN [dbo].[ApprovalAnnotation] aa ON at.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalAnnotationMarkup]
	FROM [dbo].[ApprovalAnnotationMarkup] am
			JOIN [dbo].[ApprovalAnnotation] aa ON am.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[LabColorMeasurement] 
	FROM	[dbo].[LabColorMeasurement] lbm
			JOIN [dbo].[ApprovalSeparationPlate] asp ON lbm.ApprovalSeparationPlate = asp.ID
			JOIN [dbo].[ApprovalPage] ap ON asp.Page = ap.ID
			JOIN [dbo].[Approval] a ON a.ID = ap.Approval 
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalSeparationPlate]
	FROM [dbo].[ApprovalSeparationPlate] asp
			JOIN [dbo].[ApprovalPage] ap ON asp.Page = ap.ID				
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE [dbo].[ApprovalSetting]
	FROM [dbo].[ApprovalSetting] ast				
			JOIN [dbo].[Approval] a ON ast.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalCustomICCProfile]
	FROM [dbo].[ApprovalCustomICCProfile] acp				
			JOIN [dbo].[Approval] a ON acp.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[SoftProofingSessionError]
	FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE [dbo].[SoftProofingSessionParams]
	FROM [dbo].[SoftProofingSessionParams] sfse
			JOIN [dbo].[Approval] a ON sfse.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ViewingCondition]
	FROM [dbo].ViewingCondition vwc
			JOIN [dbo].[ApprovalEmbeddedProfile] am	ON vwc.EmbeddedProfile = am.ID		
			JOIN [dbo].[Approval] a ON am.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalEmbeddedProfile]
	FROM [dbo].[ApprovalEmbeddedProfile] am			
			JOIN [dbo].[Approval] a ON am.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[DeliverJobPageVerificationResult]
	FROM [dbo].[DeliverJobPageVerificationResult] djpvr				
			JOIN [dbo].[DeliverJobPage] djp ON djpvr.DeliverJobPage = djp.ID
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id	

	DELETE [dbo].[NotificationScheduleUserGroup]
	FROM [dbo].[NotificationScheduleUserGroup] nsug			
			JOIN [dbo].[UserGroup] ug ON nsug.UserGroup = ug.ID
	WHERE ug.Account = @P_Id

	DELETE [dbo].[NotificationScheduleUser]
	FROM [dbo].[NotificationScheduleUser] nsu 
			JOIN [dbo].[NotificationSchedule] ns ON nsu.NotificationSchedule = ns.ID
			JOIN [dbo].[AccountNotificationPreset] anp ON ns.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id			

	---------------------------------------------------------------------
	UPDATE u
	SET PrinterCompany = NULL
	FROM dbo.[User] u
	WHERE u.ID IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[PrinterCompany]
	FROM [dbo].[PrinterCompany] pcm
	WHERE pcm.Account = @P_Id

	---------------------------------------------------------------------

	---------------------------------------------------------------------
	--- Delete Administration - Approval Phasing related tables -----------
	---------------------------------------------------------------------
	DELETE [dbo].[ApprovalPhaseCollaborator]
	FROM [dbo].[ApprovalPhaseCollaborator] apc
				JOIN [dbo].[ApprovalPhase] ap ON apc.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id

	DELETE [dbo].[SharedApprovalPhase]
	FROM [dbo].[SharedApprovalPhase] sap
				JOIN [dbo].[ApprovalPhase] ap ON sap.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id

	DELETE [dbo].[ApprovalPhaseCollaboratorGroup]
	FROM [dbo].[ApprovalPhaseCollaboratorGroup] apcg
				JOIN [dbo].[ApprovalPhase] ap ON apcg.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id	

	DELETE [dbo].[ApprovalPhase]
	FROM [dbo].[ApprovalPhase] ap				
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id

	DELETE [dbo].[ApprovalWorkflow]
	WHERE [Account] = @P_Id	
	----------------------------------------------------------------------


	----- ---------------------------------------------------
	DELETE [dbo].[AccountNotificationPresetEventType]
	FROM [dbo].[AccountNotificationPresetEventType] anpet
				JOIN [dbo].[AccountNotificationPreset] anp ON anpet.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id

	DELETE [dbo].[UploadEngineCustomXMLTemplatesField] 
	FROM [dbo].[UploadEngineCustomXMLTemplatesField] uecxtf
				JOIN [dbo].[UploadEngineCustomXMLTemplate] uecxt ON uecxtf.UploadEngineCustomXMLTemplate = uecxt.ID
	WHERE uecxt.Account = @P_Id

	DELETE [dbo].[FTPPresetJobDownload]
	FROM [dbo].[FTPPresetJobDownload] fpjd
				JOIN [dbo].[UploadEnginePreset] uep ON fpjd.Preset = uep.ID
	WHERE uep.[Account] = @P_Id

	DELETE [dbo].[FTPPresetJobDownload]
	FROM [dbo].[FTPPresetJobDownload] fpjd			
			JOIN [dbo].[DeliverJob] dj ON fpjd.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)	

	DELETE [dbo].[WorkstationValidationResult]
	FROM [dbo].[WorkstationValidationResult] wvr
				JOIN [dbo].[WorkstationCalibrationData] wcd ON wvr.WorkstationCalibrationData = wcd.ID
				JOIN [dbo].[Workstation] w ON wcd.Workstation = w.ID
	WHERE w.[User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[ApprovalCollaboratorGroup]
	FROM [dbo].[ApprovalCollaboratorGroup] acg			
			JOIN [dbo].[UserGroup] ug ON acg.CollaboratorGroup = ug.ID
	WHERE ug.Account = @P_Id 	

	DELETE [dbo].[ColorProofCoZoneWorkflowUserGroup]
	FROM [dbo].[ColorProofCoZoneWorkflowUserGroup] ccwug			
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON  ccwug.ColorProofCoZoneWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)	

	DELETE [dbo].[ColorProofCoZoneWorkflowUserGroup]
	FROM [dbo].[ColorProofCoZoneWorkflowUserGroup] ccwug			
			JOIN [dbo].[UserGroup] ug ON  ccwug.UserGroup = ug.ID			
	WHERE ug.Account = @P_Id	

	DELETE [dbo].[UploadEngineUserGroupPermissions]
	FROM [dbo].[UploadEngineUserGroupPermissions] ueugp			
			JOIN [dbo].[UserGroup] ug ON ueugp.UserGroup = ug.ID
	WHERE ug.Account = @P_Id
	----------------------------------------------------------------------


	-------- Delete related to folders based on deleted accounts ids -----
	DELETE [dbo].[FolderApproval]
	FROM [dbo].[FolderApproval] fa			
			JOIN [dbo].[Folder] f ON fa.Folder = f.ID
	WHERE f.Account = @P_Id 

	DELETE [dbo].[FolderCollaboratorGroup]
	FROM [dbo].[FolderCollaboratorGroup] fcg			
			JOIN [dbo].[Folder] f ON fcg.Folder = f.ID
	WHERE f.Account = @P_Id 

	DELETE [dbo].[FolderCollaborator]
	FROM [dbo].[FolderCollaborator] fc			
			JOIN [dbo].[Folder] f ON fc.Folder = f.ID
	WHERE f.Account = @P_Id
	---------------------------------------------------------------------- 

	--------- LEVEL 2 -------------
	----------------------------------------------------------------------	
	DELETE FROM [dbo].[DeliverExternalCollaborator]	
	WHERE Account = @P_Id

	DELETE [dbo].[ApprovalAnnotation]
	FROM [dbo].[ApprovalAnnotation] aa						
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[DeliverJobPage]
	FROM  [dbo].[DeliverJobPage] djp 
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id

	DELETE [dbo].[DeliverJobData]
	FROM  [dbo].[DeliverJobData] djd
			JOIN [dbo].[DeliverJob] dj ON djd.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[DeliverJobData]
	FROM  [dbo].[DeliverJobData] djd
			JOIN [dbo].[DeliverJob] dj ON djd.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id

	DELETE [dbo].[DeliverJobPage]
	FROM  [dbo].[DeliverJobPage] djp 
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[WorkstationCalibrationData]
	FROM [dbo].[WorkstationCalibrationData] wcd
			JOIN [dbo].[Workstation] w ON wcd.Workstation = w.ID
	WHERE w.[User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[Folder]
	WHERE Account = @P_Id	

	DELETE [dbo].[ChangedBillingPlan] 
	FROM [dbo].[ChangedBillingPlan] cbp
			JOIN [dbo].[AttachedAccountBillingPlan] aabp ON cbp.AttachedAccountBillingPlan = aabp.ID
	WHERE aabp.Account = @P_Id

	DELETE FROM [dbo].[UploadEngineCustomXMLTemplate]
	WHERE Account = @P_Id

	DELETE FROM [dbo].[UploadEnginePreset]
	WHERE [Account] = @P_Id 

	DELETE [dbo].[NotificationSchedule]
	FROM [dbo].[NotificationSchedule] ns
			JOIN [dbo].[AccountNotificationPreset] anp ON ns.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id	

	DELETE FROM [dbo].[BrandingPreset]			
	WHERE [Account] = @P_Id

	DELETE FROM [dbo].[AccountStorageUsage]
	WHERE Account = @P_Id
	----------------------------------------------------------------------


	------ LEVEL 3 -------------------------------------------------------
	----------------------------------------------------------------------
	DELETE [dbo].[DeliverJob]
	FROM [dbo].[DeliverJob] dj 
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[DeliverJob]
	FROM [dbo].[DeliverJob] dj	
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account =@P_Id

	DELETE [dbo].[DeliverJobDeleteHistory]
	FROM [dbo].[DeliverJobDeleteHistory] djh	
	WHERE djh.Account =@P_Id	

	DELETE FROM [dbo].[Workstation] ---- need review
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)

	DELETE [dbo].[ApprovalPage]
	FROM [dbo].[ApprovalPage] ap
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE FROM [dbo].[AttachedAccountBillingPlan] 
	WHERE Account = @P_Id

	DELETE FROM [dbo].[BillingPlan] 
	WHERE Account = @P_Id

	DELETE FROM [dbo].[BillingPlanType] 
	WHERE Account = @P_Id

	DELETE FROM [dbo].[AccountNotificationPreset] 
	WHERE Account = @P_Id

	DELETE FROM [dbo].[ExcludedAccountDefaultProfiles]
	WHERE Account = @P_Id

	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
	JOIN [dbo].[SimulationProfile] sim ON vwc.SimulationProfile = sim.ID
	WHERE sim.AccountId = @P_Id	

	DELETE FROM [dbo].[SimulationProfile]
	WHERE AccountId = @P_Id	

	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
	JOIN [dbo].[PaperTint] ppt ON vwc.PaperTint = ppt.ID
	WHERE ppt.AccountId = @P_Id	

	DELETE [dbo].[PaperTintMeasurement]
	FROM [dbo].[PaperTintMeasurement] pptms
	JOIN [dbo].[PaperTint] ppt ON pptms.PaperTint = ppt.ID
	WHERE ppt.AccountId = @P_Id	

	DELETE FROM [dbo].[PaperTint]
	WHERE AccountId = @P_Id	
	----------------------------------------------------------------------


	------ LEVEL 4 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE [dbo].[Approval]
	FROM [dbo].[Approval] a
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalJobPhase] 
	FROM [dbo].[ApprovalJobPhase] ajp
		JOIN [dbo].[ApprovalJobWorkflow] ajw ON ajp.[ApprovalJobWorkflow] = ajw.ID
		JOIN [dbo].[Job] j ON ajw.Job = j.ID 
	WHERE j.Account = @P_Id		

	DELETE [dbo].[ApprovalJobWorkflow] 
	FROM [dbo].[ApprovalJobWorkflow] ajw
		JOIN [dbo].[Job] j ON ajw.Job = j.ID 
	WHERE j.Account = @P_Id	

	DELETE [dbo].[ApprovalJobDeleteHistory]
	FROM [dbo].[ApprovalJobDeleteHistory] ajh	
	WHERE ajh.Account =@P_Id	

	DELETE FROM [dbo].[UserGroup] 
	WHERE Account = @P_Id

	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
			JOIN [dbo].[AccountCustomICCProfile] acp ON vwc.CustomProfile = acp.ID			
	WHERE acp.Account = @P_Id

	DELETE FROM [dbo].[AccountCustomICCProfile] 
	WHERE Account = @P_Id	

	DELETE [dbo].[ColorProofCoZoneWorkflow]
	FROM [dbo].[ColorProofCoZoneWorkflow] cczw
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].ProofStudioSessions
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)		
	----------------------------------------------------------------------


	------ LEVEL 5 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE [dbo].[ColorProofWorkflow]
	FROM [dbo].[ColorProofWorkflow] cpw 
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)

	DELETE FROM [dbo].[ExternalCollaborator]
	WHERE  Account = @P_Id

	DELETE [dbo].[JobStatusChangeLog]
	FROM [dbo].[JobStatusChangeLog] jcl
		JOIN dbo.[Job] j ON jcl.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE FROM [dbo].[Job]
	WHERE Account = @P_Id	
	----------------------------------------------------------------------


	------ LEVEL 6 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE FROM [dbo].[ColorProofInstance]
	WHERE [Owner] IN (SELECT UserID FROM @DeletedUsers)
	----------------------------------------------------------------------


	------ LEVEL 7-------------------------------------------------------
	--------------------------------------------------------------------
	--exec sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'

	-- Deletion of records from User and Account tables cannot be done because of circular references
	-- so, the two lines below are needed to disable the tables constraints and enable after delete operation is complete
	ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT ALL
	ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT ALL

	DELETE [dbo].[User] FROM [dbo].[User] 
	WHERE ID IN (SELECT UserID FROM @DeletedUsers)		

	----------------------------------------------------------------------

	-------- LEVEL 8-------------------------------------------------------
	------------------------------------------------------------------------
	DELETE FROM [dbo].[Account] WHERE ID = @P_Id

	ALTER TABLE [dbo].[User] WITH CHECK CHECK CONSTRAINT ALL
	ALTER TABLE [dbo].[Account] WITH CHECK CHECK CONSTRAINT ALL	
  	--exec sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'	
	------------------------------------------------------------------------

	COMMIT TRAN

	END TRY
	BEGIN CATCH	
		DECLARE @error int, @message varchar(4000), @xstate int;
		SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();

		IF (XACT_STATE()) = -1  
			BEGIN 		
				ROLLBACK TRANSACTION;  				
			END;  

		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;  

       RAISERROR ('SPC_DeleteAccount: %d: %s', 16, 1, @error, @message) ;
	END CATCH
	END


GO

