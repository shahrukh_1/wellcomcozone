USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnFileTransferHistoryPageView]    Script Date: 2/26/2019 3:59:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





  -- Add Return File Transfer History Page View
ALTER VIEW [dbo].[ReturnFileTransferHistoryPageView] 
AS 
SELECT  0 AS [ID] ,
		'' AS [FileName] ,
		'' AS [Creator] ,
		'' AS [Recipients] ,
		'' AS [OptionalMessage] ,
		0 AS [Account],
		CONVERT(DateTime, GETDATE()) AS [SentDate],
		CONVERT(DateTime, GETDATE()) AS [ExpirationDate] 
GO


----------------------Applied on Stage --------------