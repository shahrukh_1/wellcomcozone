-- Add Return File Transfer History Page View
CREATE VIEW [dbo].[ReturnFileTransferHistoryPageView] 
AS 
SELECT  0 AS [ID] ,
		'' AS [FileName] ,
		'' AS [Creator] ,
		'' AS [Recipients] ,
		'' AS [OptionalMessage] ,
		0 AS [Account],
		CONVERT(DateTime, GETDATE()) AS [SentDate] 
GO

---------------------------------------------------------------------------------------- Applied on Stage