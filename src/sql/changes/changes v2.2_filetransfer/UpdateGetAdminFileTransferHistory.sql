USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAdministratorFileTransfersHistoryPageInfo]    Script Date: 2/11/2019 3:51:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- Add Return File Transfer History Page Info procedure
ALTER PROCEDURE [dbo].[SPC_ReturnAdministratorFileTransfersHistoryPageInfo]
    (
      @P_Account INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
	  @P_SearchSenderText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;
        
        DECLARE @Delivers TABLE
            (
              ID INT NOT NULL Default(0) ,
              [FileName] NVARCHAR(128) ,
              [Creator] NVARCHAR(64) ,
              [Recipients] NVARCHAR(256) ,
			  
              [OptionalMessage] NVARCHAR(50) ,       
              [Account] INT NOT NULL Default(0) ,
              [SentDate] DATETIME,
			  [ExpirationDate] DATETIME
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  FileName ,
                  Creator,
                  Recipients ,
				  
                  OptionalMessage,
                  Account, 
                  SentDate,
				  ExpirationDate
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            FT.ID ,
                            FT.FileName,
                            U.Username,
							 isNull( STUFF(
							 (SELECT ', ' + U.Username 
							  FROM dbo.[User] U
							  WHERE U.ID  in (SELECT I.UserInternalID FROM dbo.FileTransferUserInternal I WHERE FT.ID=I.FileTransferID)
							  FOR XML PATH (''))
							 , 1, 1, ''),'') + ','+
							 isNull(STUFF(
							 (SELECT ', ' + EX.EmailAddress
							  FROM dbo.ExternalCollaborator EX
							  WHERE EX.ID  in (SELECT I.UserExternalID FROM dbo.FileTransferUserExternal I WHERE FT.ID=I.FileTransferID)
							  FOR XML PATH (''))
							 , 1, 1, ''),'')
							 as Recipients  ,

                            isnull(FT.Message, ''),
							
                            FT.AccountID,
							FT.CreatedDate,
							FT.ExpirationDate 
                            
                  FROM      dbo.FileTransfer FT
                            LEFT JOIN dbo.FileTransferUserInternal I ON FT.ID=I.FileTransferID
							LEFT JOIN dbo.FileTransferUserExternal E ON FT.ID=E.FileTransferID
							JOIN dbo.[User] U ON FT.Creator=U.ID
                            
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR FT.FileName LIKE '%' + @P_SearchText + '%'
                            )
							AND ( @P_SearchSenderText IS NULL
                              OR @P_SearchSenderText = ''
                              OR U.Username LIKE '%' + @P_SearchSenderText + '%'
                            )
                            AND FT.AccountID = @P_Account
							AND FT.IsDeleted=0
                            
                  )
                
               
                
            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.SentDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.SentDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.FileName
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.FileName
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.Creator
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.Creator
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Recipients]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Recipients]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.OptionalMessage
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.OptionalMessage
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset
        
        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT

    END
 
GO
----------------------Applied on Stage --------------

