USE [GMGCoZone]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FileTransfer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[FileName] [nvarchar](255) NOT NULL,
	[FolderPath] [nvarchar](255) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[ExpirationDate] [datetime2](7) NULL,
	[Message] [nvarchar](max) NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_FileTransfer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[FileTransfer]  WITH CHECK ADD  CONSTRAINT [FK_FileTransfer_Account] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[FileTransfer] CHECK CONSTRAINT [FK_FileTransfer_Account]
GO

ALTER TABLE [dbo].[FileTransfer]  WITH CHECK ADD  CONSTRAINT [FK_FileTransfer_User] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[FileTransfer] CHECK CONSTRAINT [FK_FileTransfer_User]
GO

ALTER TABLE [dbo].[FileTransfer]  WITH CHECK ADD  CONSTRAINT [FK_FileTransfer_User1] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[FileTransfer] CHECK CONSTRAINT [FK_FileTransfer_User1]
GO

---

CREATE TABLE [dbo].[FileTransferUserExternal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileTransferID] [int] NOT NULL,
	[UserExternalID] [int] NOT NULL,
 CONSTRAINT [PK_FileTransferUserInternal] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[FileTransferUserExternal]  WITH CHECK ADD  CONSTRAINT [FK_FileTransferUserExternal_ExternalCollaborator] FOREIGN KEY([UserExternalID])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO

ALTER TABLE [dbo].[FileTransferUserExternal] CHECK CONSTRAINT [FK_FileTransferUserExternal_ExternalCollaborator]
GO

ALTER TABLE [dbo].[FileTransferUserExternal]  WITH CHECK ADD  CONSTRAINT [FK_FileTransferUserExternal_FileTransfer] FOREIGN KEY([FileTransferID])
REFERENCES [dbo].[FileTransfer] ([ID])
GO

ALTER TABLE [dbo].[FileTransferUserExternal] CHECK CONSTRAINT [FK_FileTransferUserExternal_FileTransfer]
GO

---

CREATE TABLE [dbo].[FileTransferUserInternal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileTransferID] [int] NOT NULL,
	[UserInternalID] [int] NOT NULL,
 CONSTRAINT [PK_FileTransferUserExternal] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FileTransferUserInternal]  WITH CHECK ADD  CONSTRAINT [FK_FileTransferUserInternal_FileTransfer] FOREIGN KEY([FileTransferID])
REFERENCES [dbo].[FileTransfer] ([ID])
GO

ALTER TABLE [dbo].[FileTransferUserInternal] CHECK CONSTRAINT [FK_FileTransferUserInternal_FileTransfer]
GO

ALTER TABLE [dbo].[FileTransferUserInternal]  WITH CHECK ADD  CONSTRAINT [FK_FileTransferUserInternal_User] FOREIGN KEY([UserInternalID])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[FileTransferUserInternal] CHECK CONSTRAINT [FK_FileTransferUserInternal_User]
GO

------

insert into EventType(EventGroup, [Key], Name)
values (8, 'FTA', 'File Transfer was added')

------

-- filetransfer menu --

SET IDENTITY_INSERT AppModule ON;
insert into AppModule
           ([ID]
		   ,[Key]
           ,[Name])
SELECT 7, 6, 'FileTransfer'
SET IDENTITY_INSERT AppModule OFF;


SET IDENTITY_INSERT [Role] ON;
INSERT INTO [dbo].[Role]
           ([ID]
		   ,[Key]
           ,[Name]
           ,[Priority]
           ,[AppModule])
SELECT 29, 'ADM', 'Administrator', 1, 7
UNION SELECT 30, 'MAN', 'Manager', 2, 7
SET IDENTITY_INSERT [Role] OFF;

INSERT INTO [dbo].AccountTypeRole
           ([AccountType]
           ,[Role])
SELECT 1, 29
UNION SELECT 2, 29
UNION SELECT 3, 29
UNION SELECT 4, 29
UNION SELECT 5, 29
UNION SELECT 1, 30
UNION SELECT 2, 30
UNION SELECT 3, 30
UNION SELECT 4, 30
UNION SELECT 5, 30

--=============

DECLARE @ControllerActionId AS INT
DECLARE @ParentMenuItemId AS INT

INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'FileTransfer' ,
          'Index' ,
          ''
        )
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId ,
          NULL ,
          4 ,
          0 ,
          1 ,
          0 ,
          1 ,
          1 ,
          'TRAN' ,
          'FileTransfer' ,
          'File transfer'
        )
SET @ParentMenuItemId = SCOPE_IDENTITY()


DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] in ('ADM', 'MAN') AND AM.[Key] = 6
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('TRAN')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

--=============

---- Set access for Adm/Manager users 
USE [GMGCoZone]
GO

INSERT INTO [dbo].[UserRole] ([User], [Role])
SELECT [User], 29 FROM [GMGCoZone].[dbo].[UserRole] where [Role] = 3 or [Role] = 4
GO

ALTER TABLE [dbo].[FileTransferUserInternal] ADD [DownloadDate] [datetime2](7) 
GO

ALTER TABLE [dbo].[FileTransferUserExternal] ADD [DownloadDate] [datetime2](7)
GO

DECLARE @ControllerActionId_2 AS INT
DECLARE @ParentMenuItemId_2 AS INT

INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'FileTransfer' ,
          'NewFileTransfer' ,
          ''
        )
SET @ControllerActionId_2 = SCOPE_IDENTITY()

INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId_2 ,
          120,
          0 ,
          0 ,
          0 ,
          1 ,
          0 ,
          1 ,
          'TRNT' ,
          'New FileTransfer' ,
          'File transfer'
        )
SET @ParentMenuItemId_2 = SCOPE_IDENTITY()

DECLARE @atrId_2 INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] in ('ADM', 'MAN') AND AM.[Key] = 6
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId_2
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId_2
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('TRNT')
        FETCH NEXT FROM ATRCursor INTO @atrId_2
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor



  ALTER TABLE [GMGCoZone].[dbo].[FileTransfer]
  ADD IsDeleted bit NOT NULL DEFAULT ((1));
  
 ALTER TABLE [GMGCoZone].[dbo].[FileTransfer] ADD IndividualFileNames nvarchar(255);

 ALTER TABLE [GMGCoZone].[dbo].[FileTransfer] ALTER COLUMN [IndividualFileNames] nvarchar(max);


 ----New file transfer approval-----
 

DECLARE @ControllerActionId_3 AS INT
DECLARE @ParentMenuItemId_3 AS INT

INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'FileTransfer' ,
          'NewApprovalTransfer' ,
          ''
        )
SET @ControllerActionId_3 = SCOPE_IDENTITY()

INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId_3 ,
          120,
          0 ,
          0 ,
          0 ,
          0,
          0 ,
          1 ,
          'TRNA' ,
          'ApprovalTransfer' ,
          'New Approval Transfer'
        )
SET @ParentMenuItemId_3 = SCOPE_IDENTITY()

DECLARE @atrId_3 INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] in ('ADM', 'MAN') AND AM.[Key] = 6
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId_3
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId_3
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('TRNA')
        FETCH NEXT FROM ATRCursor INTO @atrId_3
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

 ----New file transfer folder-----
 
DECLARE @ControllerActionId_4 AS INT
DECLARE @ParentMenuItemId_4 AS INT

INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'FileTransfer' ,
          'NewFolderTransfer' ,
          ''
        )
SET @ControllerActionId_4 = SCOPE_IDENTITY()

INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId_4 ,
          120,
          0 ,
          0 ,
          0 ,
          0,
          0 ,
          1 ,
          'TRFO' ,
          'FolderTransfer' ,
          'New Folder Transfer'
        )
SET @ParentMenuItemId_4 = SCOPE_IDENTITY()

DECLARE @atrId_4 INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] in ('ADM', 'MAN') AND AM.[Key] = 6
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId_4
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId_4
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('TRFO')
        FETCH NEXT FROM ATRCursor INTO @atrId_4
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor


insert into EventType(EventGroup, [Key], Name)
values (8, 'FTD', 'File Transfer was downloaded')


ALTER TABLE [GMGCoZone].[dbo].[Account]
ADD IsFileTransferEnabled bit NOT NULL DEFAULT ((0));
  
SET IDENTITY_INSERT [Role] ON;
INSERT INTO [dbo].[Role]
           ([ID]
		   ,[Key]
           ,[Name]
           ,[Priority]
           ,[AppModule])
SELECT 31, 'NON', 'NoAccess', 10, 7

SET IDENTITY_INSERT [Role] OFF;


INSERT INTO [dbo].AccountTypeRole
           ([AccountType]
           ,[Role])

SELECT 1, 31
UNION SELECT 2, 31
UNION SELECT 3, 31
UNION SELECT 4, 31
UNION SELECT 5, 31


UPDATE [GMGCoZone].[dbo].[MenuItem]
SET [IsSubNav] = 0, [IsVisible] = 0
WHERE [ID]=141

UPDATE [GMGCoZone].[dbo].[MenuItem]
SET  [IsVisible] = 0
WHERE [ID]=142

UPDATE [GMGCoZone].[dbo].[MenuItem]
SET  [IsVisible] = 0
WHERE [ID]=143

UPDATE [GMGCoZone].[dbo].[MenuItem]
SET [Parent]=140
WHERE [ID]=141

UPDATE [GMGCoZone].[dbo].[MenuItem]
SET  [Parent]=140
WHERE [ID]=142

UPDATE [GMGCoZone].[dbo].[MenuItem]
SET  [Parent]=140
WHERE [ID]=143
---------------------------------------------------------------------------------------- Applied on Stage

