USE GMGCoZone
GO

ALTER TABLE [GMGCoZone].[dbo].[Approval]
ADD [SOADState] INT NOT NULL default(0)
GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalCollaborator]
ADD [SOADState] INT NOT NULL default(0)
USE [GMGCoZone]
GO

ALTER TABLE [GMGCoZone].[dbo].[SharedApproval]
ADD [SOADState] INT NOT NULL default(0)
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalJobPhaseApproval]
ADD [SOADState] INT NOT NULL default(0)
GO

--------------------------------------------------------------------------------------------------Launched Production
--------------------------------------------------------------------------------------------------Launched Test
--------------------------------------------------------------------------------------------------Launched Stage
