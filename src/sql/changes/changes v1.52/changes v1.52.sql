USE GMGCoZone;
GO

--- Create the new tables
	----- Create AccountThemes table
	CREATE TABLE [dbo].[AccountTheme]
	(
		[ID] [INT] IDENTITY(1, 1) NOT NULL,
		[Account] [INT] NOT NULL CONSTRAINT FK_AccountTheme_ID  FOREIGN KEY([Account]) REFERENCES [dbo].[Account](ID),
		[Key][NVARCHAR](1) NOT NULL,
		[ThemeName] [NVARCHAR](25) NOT NULL,
		[Active] [Bit] NOT NULL,
		CONSTRAINT [PK_AccountTheme_ID] PRIMARY KEY CLUSTERED 
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	)
	GO
	----- Create UserGroupBradingPresetThemes
	CREATE TABLE [dbo].[BrandingPresetTheme]
	(
		[ID] [INT] IDENTITY(1, 1) NOT NULL,
		[BrandingPreset] [INT] NOT NULL CONSTRAINT FK_BrandingPreset_ID  FOREIGN KEY([BrandingPreset]) REFERENCES [dbo].[BrandingPreset](ID),
		[Key][NVARCHAR](1) NOT NULL,
		[ThemeName] [NVARCHAR](25) NOT NULL,
		[Active] [Bit] NOT NULL,
		CONSTRAINT [PK_UserGroupdPresetTheme_ID] PRIMARY KEY CLUSTERED 
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	)
	GO
	
	----- Create DefaultTheme table
	CREATE TABLE [dbo].[DefaultTheme]
	(
		[ID] [INT] IDENTITY(1, 1) NOT NULL,	
		[Key][NVARCHAR](1) NOT NULL,
		[ThemeName] [NVARCHAR](25) NOT NULL,
		CONSTRAINT [PK_DefaultTheme_ID] PRIMARY KEY CLUSTERED 
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	)
	GO
	
	--- Create ThemesColorScheme table
	CREATE TABLE [dbo].[ThemeColorScheme] 
	(
		[ID] [INT] IDENTITY(1,1) NOT NULL,
		[DefaultTheme] [INT]  NULL CONSTRAINT FK_DefaultTheme_Colors_ID  FOREIGN KEY([DefaultTheme]) REFERENCES [dbo].[DefaultTheme](ID),
		[AccountTheme] [INT]  NULL CONSTRAINT FK_AccountTheme_Colors_ID  FOREIGN KEY([AccountTheme]) REFERENCES [dbo].[AccountTheme](ID),
		[BrandingPresetTheme] [INT] NULL CONSTRAINT FK_BrandingPresetTheme_Colors_ID  FOREIGN KEY([BrandingPresetTheme]) REFERENCES [dbo].[BrandingPresetTheme](ID),
		[Name] [NVARCHAR](20) NOT NULL,
		[THex] [NVARCHAR](10) NULL,
		[BHex] [NVARCHAR](10) NULL,
		CONSTRAINT [PK_ThemeColorScheme_ID] PRIMARY KEY CLUSTERED 
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	)
	GO
	
--- Populate tables with static data
	---Populate DefaultTheme table
	INSERT INTO [dbo].[DefaultTheme]
		([Key], [ThemeName])
	VALUES
		('D', 'Default'),
		('S', 'Pink'),
		('J', 'Juicy'),
		('F', 'Fresh'),
		('M','Clear')
	GO
	-----Populate ThemesColorScheme with default colors
	INSERT INTO [dbo].[ThemeColorScheme] 
		([DefaultTheme], [AccountTheme], [BrandingPresetTheme], [Name], [THex], [BHex])
	VALUES
		(1, NULL, NULL, 'Header','#FFFFFF', '#F2F2F2'),
		(1, NULL, NULL, 'Footer','#333333', NULL),
		(1, NULL, NULL, 'Buttons', '#ED1556', '#CA1048'),
		(1, NULL, NULL, 'Links', '#555', NULL),
		
		(2, NULL, NULL, 'Header','#FFFFFF', '#FFFFFF'),
		(2, NULL, NULL, 'Footer','#333333', NULL),
		(2, NULL, NULL, 'Buttons', '#555555', '#3B3B3B'),
		(2, NULL, NULL, 'Links', '#666666', NULL),
		
		(3, NULL, NULL, 'Header','#78A352', '#6C9249'),
		(3, NULL, NULL, 'Footer','#333333', NULL),
		(3, NULL, NULL, 'Buttons', '#F58220', '#D8690A'),
		(3, NULL, NULL, 'Links', '#d9d9d9', NULL),
		
		(4, NULL, NULL, 'Header','#115C92', '#0E4E7B'),
		(4, NULL, NULL, 'Footer','#333333', NULL),
		(4, NULL, NULL, 'Buttons', '#1368A4', '#0E4B76'),
		(4, NULL, NULL, 'Links', '#cccccc', NULL),
		
		(5, NULL, NULL, 'Header','#333333', '#111111'),
		(5, NULL, NULL, 'Footer','#333333', NULL),
		(5, NULL, NULL, 'Buttons', '#ED1556', '#C00F44'),
		(5, NULL, NULL, 'Links', '#8a8a8a', NULL)
	GO
--- MIGRATING TO NEW STRUCTURE
	--- MIGRATING BRANDING PRESET THEMES
	DECLARE @ActiveThemeKey NVARCHAR(1), @PresetID INT
	DECLARE @BrandingPresetThemesCursor CURSOR;

	BEGIN
		SET @BrandingPresetThemesCursor = 
			CURSOR FOR 
				SELECT DISTINCT
					thm.[Key] AS ActiveThemeKey,
					bp.[ID] AS PresetID
				FROM 
					[dbo].[User] AS us
					Inner JOIN [dbo].[UserGroupUser] AS usgu ON us.[ID] = usgu.[User]
					Inner JOIN [dbo].[UserGroup] AS ug ON usgu.[UserGroup] = ug.[ID]
					Inner JOIN [dbo].[BrandingPresetUserGroup] AS bpug ON ug.[ID] = bpug.[UserGroup]
					Inner JOIN [dbo].[BrandingPreset] AS bp ON bpug.[BrandingPreset] = bp.[ID]
					Inner JOIN [dbo].[Theme] AS thm ON bp.[Theme] = thm.[ID]
		
		OPEN @BrandingPresetThemesCursor
		FETCH NEXT FROM @BrandingPresetThemesCursor
		INTO @ActiveThemeKey, @PresetID;
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @ThemeKey NVARCHAR(1), @ThemeName NVARCHAR(25)
			DECLARE @ThemeCursor CURSOR
			DECLARE @BradingPresetThemeId INT
			
			BEGIN
				SET @ThemeCursor = 
					CURSOR FOR 
						SELECT
							thm.[Key] AS ThemeKey,
							thm.[Name] AS ThemeName
						FROM 
							[dbo].[Theme] AS thm
				OPEN @ThemeCursor
				FETCH NEXT FROM @ThemeCursor
				INTO @ThemeKey, @ThemeName
				
				DECLARE @cnt INT = 0;
				DECLARE @jmp INT = 0;
				
				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO 
						[dbo].[BrandingPresetTheme]
					VALUES
						(@PresetID, @ThemeKey, @ThemeName, CASE @ThemeKey WHEN @ActiveThemeKey THEN 1 ELSE 0 END)
					SET @BradingPresetThemeId = SCOPE_IDENTITY()
					
					---CREATE COLORS FROM BRADING PRESET THEMES
					DECLARE @BradingPresetThemeColorCursor CURSOR
					DECLARE @ElementName NVARCHAR(20), @THex NVARCHAR(10), @BHex NVARCHAR(10)

					BEGIN
						SET @BradingPresetThemeColorCursor = 
							CURSOR FOR
								WITH BatchTheme AS
								(
									SELECT 
										RownNumber = ROW_NUMBER() OVER(ORDER BY tcs.[ID] ASC),
										tcs.[Name] AS ElementName,
										tcs.[THex] AS THex,
										tcs.[BHex] AS BHex
									FROM 
										[dbo].[ThemeColorScheme] AS tcs
									WHERE 
										tcs.[DefaultTheme] IS NOT NULL
								)
								SELECT 
										ElementName,
										THex,
										BHex
								FROM 
									BatchTheme
								WHERE RownNumber BETWEEN @cnt + 1 AND @cnt + 4

						OPEN @BradingPresetThemeColorCursor
						FETCH NEXT FROM @BradingPresetThemeColorCursor
						INTO @ElementName, @THex, @BHex
						
						WHILE @@FETCH_STATUS = 0
						BEGIN
							INSERT INTO 
								[dbo].[ThemeColorScheme]
							VALUES (NULL, NULL, @BradingPresetThemeId, @ElementName, @THex, @BHex)
							FETCH NEXT FROM @BradingPresetThemeColorCursor
							INTO @ElementName, @THex, @BHex
						END;
						CLOSE @BradingPresetThemeColorCursor;
						DEALLOCATE @BradingPresetThemeColorCursor;
						
						SET @cnt = @cnt + 4;
					END;	
					FETCH NEXT FROM @ThemeCursor
					INTO @ThemeKey, @ThemeName
				END;
				CLOSE @ThemeCursor;
				DEALLOCATE @ThemeCursor;
			END;
		FETCH NEXT FROM @BrandingPresetThemesCursor
		INTO @ActiveThemeKey, @PresetID;
		END;
		
		CLOSE @BrandingPresetThemesCursor;
		DEALLOCATE @BrandingPresetThemesCursor;
	END;
	GO
	
	--- MIGRATE ACCOUNT THEMES
	DECLARE @AccountThemesCursor CURSOR
	DECLARE @AccountID INT, @ActiveThemeKey NVARCHAR(1)

	BEGIN
		SET @AccountThemesCursor =
			CURSOR FOR
				SELECT 
					acc.[ID] AS AccountID,
					acthm.[Key] AS ActiveThemeKey
				FROM 
					[dbo].[Account] AS acc
					LEFT JOIN [dbo].[Theme] AS acthm on acc.[Theme] = acthm.[ID]

		OPEN @AccountThemesCursor
		FETCH NEXT FROM @AccountThemesCursor
		INTO @AccountID, @ActiveThemeKey
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @ThemeKey NVARCHAR(1), @ThemeName NVARCHAR(25)
			DECLARE @ThemeCursor CURSOR
			DECLARE @AccountThemeId INT
			BEGIN
				SET @ThemeCursor = 
					CURSOR FOR 
						SELECT
							thm.[Key] AS ThemeKey,
							thm.[Name] AS ThemeName
						FROM 
							[dbo].[Theme] AS thm
				OPEN @ThemeCursor
				FETCH NEXT FROM @ThemeCursor
				INTO @ThemeKey, @ThemeName
				
				DECLARE @cnt INT = 0;
				DECLARE @jmp INT = 0;

				WHILE @@FETCH_STATUS = 0
				BEGIN
					INSERT INTO
						[dbo].[AccountTheme]
					VALUES
						(@AccountID, @ThemeKey, @ThemeName, CASE @ThemeKey WHEN @ActiveThemeKey THEN 1 ELSE 0 END)
					SET @AccountThemeId = SCOPE_IDENTITY()
					
					---CREATE COLORS FOR ACCOUNT THEMES
					DECLARE @AccountThemeColorCursor CURSOR
					DECLARE @ElementName NVARCHAR(20), @THex NVARCHAR(10), @BHex NVARCHAR(10)
					BEGIN
						SET @AccountThemeColorCursor =
							CURSOR FOR
							WITH BatchTheme AS
							(
								SELECT 
									RownNumber = ROW_NUMBER() OVER(ORDER BY tcs.[ID] ASC),
									tcs.[Name] AS ElementName,
									tcs.[THex] AS THex,
									tcs.[BHex] AS BHex
								FROM 
									[dbo].[ThemeColorScheme] AS tcs
								WHERE 
									tcs.[DefaultTheme] IS NOT NULL
							)
							SELECT 
									ElementName,
									THex,
									BHex
							FROM 
								BatchTheme
							WHERE RownNumber BETWEEN @cnt + 1 AND @cnt + 4					
						OPEN @AccountThemeColorCursor
						FETCH NEXT FROM @AccountThemeColorCursor
						INTO @ElementName, @THex, @BHex
						
						WHILE @@FETCH_STATUS = 0
						BEGIN
							INSERT INTO 
								[dbo].[ThemeColorScheme]
							VALUES 
								(NULL, @AccountThemeId, NULL, @ElementName, @THex, @BHex)
							FETCH NEXT FROM @AccountThemeColorCursor
							INTO @ElementName, @THex, @BHex
						END;
						
						CLOSE @AccountThemeColorCursor;
						DEALLOCATE @AccountThemeColorCursor;
						
						SET @cnt = @cnt + 4;
					END;
					FETCH NEXT FROM @ThemeCursor
					INTO @ThemeKey, @ThemeName
				END;
				CLOSE @ThemeCursor;
				DEALLOCATE @ThemeCursor;
			END;
			FETCH NEXT FROM @AccountThemesCursor
			INTO @AccountID, @ActiveThemeKey
		END;
		CLOSE @AccountThemesCursor;
		DEALLOCATE @AccountThemesCursor;
	END;
	GO

--- Remove Theme column from Account 
	--- Drop Theme FOREIGN key from Account table
	ALTER TABLE [dbo].[Account] DROP CONSTRAINT FK_Account_Theme   
	GO
	
	--- Drop Theme column from Account table
	ALTER TABLE [dbo].[Account] DROP COLUMN [Theme]
	GO
	
--- Remove Theme Column from BradingPreset
	--- Drop FOREIGN key for BrandingPreset table
	ALTER TABLE [dbo].[BrandingPreset] DROP CONSTRAINT FK_BrandingPreset_Theme   
	GO
	
	--- Drop Theme column from BrandingPreset table
	ALTER TABLE [dbo].[BrandingPreset] DROP COLUMN [Theme]
	GO

--- Drop Theme Table
	DROP TABLE [dbo].[Theme]
	GO

USE GMGCoZone;
GO

ALTER TABLE [dbo].[ApprovalJobPhase]
ADD IsActive BIT NOT NULL Default(1)
GO

--- Added "IsActive" check for NextPhase column
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(150),
	   PrimaryDecisionMakerName nvarchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)						
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL and
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0								
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
								(@P_TopLinkId = 1 
										AND EXISTS(
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												  )
									)									
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
								OR 
									(@P_TopLinkId = 7 
										AND js.[Key] != 'COM' AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM dbo.ApprovalCollaboratorDecision ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Decision IS NULL
													)
									)
							  )
							)
						)		
			) = a.[Version]	)
			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (
														 @P_ViewAll = 1 
													 OR
													 ( 
														@P_ViewAll = 0 AND 
														(
															(@P_TopLinkId = 1 AND (@P_User = j.JobOwner OR EXISTS(
																													SELECT TOP 1 ac.ID 
																													FROM ApprovalCollaborator ac 
																													WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																												  )
																					)
															 )									
															OR 
																(@P_TopLinkId = 2 
																	AND @P_User = ISNULL(j.JobOwner,0)
																)
															OR 
																(@P_TopLinkId = 3 
																	AND ISNULL(j.JobOwner,0) != @P_User
																	AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																				)
																)
																OR 
																(@P_TopLinkId = 7 
																	AND js.[Key] != 'COM' AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM dbo.ApprovalCollaboratorDecision ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase AND ac.Decision IS NULL
																				)
																)
														  )
														)
													)										 		
								      )
				)
			)
			AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
			AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
			AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
			AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
			AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
			     OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
			AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
			AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (Select ajp.ID  FROM ApprovalJobPhase ajp WHERE ajp.PhaseTemplateID in (Select val FROM dbo.splitString(@P_ByPhase, ',')))))

	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,			
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
					WHEN ( EXISTS (SELECT ANNOTATION					
							FROM dbo.ApprovalPage ap							
							CROSS APPLY (SELECT TOP 1 aa.ID AS ANNOTATION FROM dbo.ApprovalAnnotation aa WHERE aa.Page = ap.ID 
							AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = a.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																													  WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase 
																													  )
																				)
													  )
																					   
								)	 
							) ANN (ANNOTATION)						
							WHERE ap.Approval = t.ApprovalID 
						 )
					) 
					THEN CONVERT(bit,1)
					ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,						
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID

	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop

END

GO

--- Added "IsActive" check for NextPhase column
ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_HideCompletedApprovals bit, 
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			INNER JOIN Job j 
					ON j.ID = a.Job
			INNER JOIN JobStatus js 
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)									
	WHERE 
			js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END AND
			j.Account = @P_Account					
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND		auvi.[User] = @P_User
			AND ((a.CurrentPhase IS NULL AND(	SELECT MAX([ViewedDate]) 
												FROM ApprovalUserViewInfo vuvi
													INNER JOIN Approval av
														ON  av.ID = vuvi.[Approval]
													INNER JOIN Job vj
														ON vj.ID = av.Job	
												WHERE  av.Job = a.Job
													AND av.IsDeleted = 0
											) = auvi.[ViewedDate]
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
													)
			)						
		   OR (a.CurrentPhase IS NOT NULL  AND a.CurrentPhase = (SELECT av.CurrentPhase 
																  FROM ApprovalUserViewInfo avi
																  INNER JOIN Approval av ON av.ID =  avi.Approval
																  WHERE av.Job = a.Job
																  AND avi.[ViewedDate] = auvi.[ViewedDate]
																		AND avi.[ViewedDate] =( SELECT MAX([ViewedDate]) 
																								FROM ApprovalUserViewInfo vuvi
																								INNER JOIN Approval avv
																									ON  avv.ID = vuvi.[Approval]
																								INNER JOIN Job vj
																									ON vj.ID = avv.Job	
																								WHERE  avv.Job = a.Job
																								AND avv.IsDeleted = 0)
																		AND (EXISTS(
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Phase = av.CurrentPhase
																				  )
																				  OR ISNULL(j.JobOwner, 0) = @P_User
																			)
																		 
																)
				)
			)
	ORDER BY 
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE						
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
		END DESC,		
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT DISTINCT	t.ID,
			a.ID AS Approval,	
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],							 
			js.[Key] AS [Status],	
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			a.[Version] AS MaxVersion,						
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
				FROM Approval av
				WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.Approval aproval ON ap.Approval = aproval.ID
							 WHERE aproval.ID = t.ApprovalID AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = aproval.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																														WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase )
																												)
																			           )
																					   
															       )	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,			
			(SELECT CASE 
				WHEN a.LockProofWhenAllDecisionsMade = 1
					THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
				ELSE
					NULL
				END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0) 
				THEN(						     
						(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
						   THEN
								(SELECT TOP 1 appcd.[Key]
									FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
											JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
											WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
											ORDER BY ad.[Priority] ASC
										) appcd
								)
							ELSE
							(							    
								(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
											                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
								 THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
								     )
								 ELSE '0'
								 END 
								 )   			   
							)
							END
						)								
					)		
				WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
				  THEN
					(SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				ELSE
				 (SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				END	
				), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			JOIN @TempItems t 
					ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
			INNER JOIN Job j
					ON j.ID = a.Job
			INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	  
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
END
GO

--- Added "IsActive" check for NextPhase column
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)	   
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.Deadline,
					a.CreatedDate,
					a.ModifiedDate,
					js.[Key] AS [Status],
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					LEFT OUTER JOIN FolderApproval fa
						ON fa.Approval = a.ID
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC'
					AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((a.CurrentPhase IS NULL 
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND EXISTS (
											SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
										)
					) = a.[Version])			
					OR (a.CurrentPhase IS NOT NULL 
						AND a.CurrentPhase = (SELECT avv.CurrentPhase 
											  FROM approval avv 
											  WHERE avv.Job = a.Job 
													AND a.[Version] = avv.[Version]
													AND avv.[Version] = (SELECT MAX([Version])
																		 FROM Approval av 
																		 WHERE av.Job = a.Job AND av.IsDeleted = 0)

													AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																						SELECT TOP 1 ac.ID 
																						FROM ApprovalCollaborator ac 
																						WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																					  )
																			)
																								 		
											  )
						)
					)	
					AND	@P_FolderId = fa.Folder
				
			UNION
			
			SELECT 	f.ID AS Approval,
					GetDate() AS Deadline,
					f.CreatedDate,
					f.ModifiedDate,
					'' AS [Status],
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase
			FROM	Folder f
			WHERE	f.Account = @P_Account
					AND (@P_Status = 0)
					AND f.IsDeleted = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
					AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN a.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN a.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
		FROM (
				SELECT  t.ID AS ID,
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						a.[Guid],
						a.[FileName],
						js.[Key] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
								     WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
						END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						t.PrimaryDecisionMakerName,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						at.[Key] AS ApprovalType,
						a.[Version] AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT CASE
						 WHEN ( EXISTS (SELECT aa.ID					
										 FROM dbo.ApprovalAnnotation aa
										 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
										 WHERE ap.Approval = t.ApprovalID			 
										 )
								) THEN CONVERT(bit,1)
								  ELSE CONVERT(bit,0)
						 END)  AS HasAnnotations,				
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
													                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
														JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
														WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				CONVERT(bit, 1) AS IsCollaborator,
				CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,				
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
				t.PhaseName,
				ISNULL((SELECT CASE
							WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
							ELSE ''
						END),'') AS WorkflowName,
				ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
				ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
				ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
				(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
							 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
							 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
							 END) AS OwnerName,
				a.AllowOtherUsersToBeAdded,
				j.JobOwner,
				(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker				
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN dbo.ApprovalType at
							ON 	at.ID = a.[Type]
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID	
						JOIN @TempItems t 
							ON t.ApprovalID = a.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 1
						
				UNION
				
				SELECT 	t.ID AS ID,
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						'' AS [Guid],
						'' AS [FileName],
						'' AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'' AS PrimaryDecisionMakerName,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS ApprovalType,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						CONVERT(bit, 0) AS HasAnnotations,
						'' AS Decision,
						CONVERT(bit, 1) AS IsCollaborator,
						f.AllCollaboratorsDecisionRequired,
						NULL as ApprovalApprovedDate,
						0 AS CurrentPhase,
						'' AS PhaseName,
						'' AS WorkflowName,
						'' AS NextPhase,
						'' AS DecisionMakers,
						0 AS DecisionType,
						'' AS OwnerName,
						CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
						NULL AS JobOwner,
						CONVERT(bit, 0) AS IsPhaseComplete,
						'' AS VersionSufix,
						CONVERT(bit, 0) AS IsLocked,
					    CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
				FROM	Folder f
						JOIN @TempItems t 
							ON t.ApprovalID = f.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 0
			) result		
	ORDER BY result.ID	

	SET ROWCOUNT 0

	---- Send the total rows count ---- 
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
						AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND EXISTS (
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO

--- Added "IsActive" check for NextPhase column
ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID			
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] = 'ARC'
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND EXISTS (
									SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
								)
			) = a.[Version])			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																				SELECT TOP 1 ac.ID 
																				FROM ApprovalCollaborator ac 
																				WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																			  )
																	)
																						 		
								      )
				)
			)	
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,			
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 WHERE ap.Approval = t.ApprovalID			 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,							
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t 
				ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	 
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
	 
END
GO

--- Added "IsActive" check for NextPhase column
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,
	@P_ViewAll bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						)
					AND (
						 @P_ViewAll = 1 
						 OR
						 ( 
							@P_ViewAll = 0 AND((a.CurrentPhase IS NULL AND	EXISTS (
																					 SELECT TOP 1 ac.ID 
																					 FROM ApprovalCollaborator ac 
																					 WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
																					))
										  OR (a.CurrentPhase IS NOT NULL AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																												SELECT TOP 1 ac.ID 
																												FROM ApprovalCollaborator ac 
																												WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator AND ac.Phase = a.CurrentPhase
																											  )
											                                  )
											   )
											  )
					     )	
					    )	 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase,
					GetDate() AS Deadline
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
					END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,	
					t.PrimaryDecisionMakerName,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND acd.Phase = a.CurrentPhase
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,			
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'' AS PrimaryDecisionMakerName,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			JOIN Approval a ON fa.Approval = a.ID	
	                    WHERE fa.Folder = f.ID AND a.DeletePermanently = 0
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					NULL as ApprovalApprovedDate,
					0 AS CurrentPhase,
					'' AS PhaseName,
					'' AS WorkflowName,
					'' AS NextPhase,
					'' AS DecisionMakers,
					0 AS DecisionType,
					'' AS OwnerName,
					CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,			
					NULL AS JobOwner,
					CONVERT(bit, 0) AS IsPhaseComplete,
					'' AS VersionSufix,
					CONVERT(bit, 0) AS IsLocked,
					CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop

END
GO


--------------------------------------------------------
---- Add new event(notification) type for "Approval was rejected" ----
--------------------------------------------------------
DECLARE @GroupId INT 
SET @GroupId =  (SELECT ID FROM [GMGCoZone].[dbo].[EventGroup] WHERE [Key] = 'A')
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'PWD', 'Phase was deactivated'), (@GroupId, 'PWA', 'Phase was activated'), (@GroupId, 'WPR', 'Workflow phase rerun')
GO


DECLARE @PresetId INT
DECLARE @RoleId INT
DECLARE @ModuleId INT

--Collaborate Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 0);

---------------------------------------------------------------------	   
---- High Preset
---------------------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Contributor Viewer

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'VIE');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
-------------------------------------------------------	   
---- Custom Preset
-------------------------------------------------------
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Moderator User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MOD');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
	   
------ Contributor User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'CON');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')

------ Viewer User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'VIE');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('PWD','PWA', 'WPR')
GO

--------------------------------------------------------------------------------------------------Launched Test
--------------------------------------------------------------------------------------------------Launched Stage
--------------------------------------------------------------------------------------------------Launched Production



