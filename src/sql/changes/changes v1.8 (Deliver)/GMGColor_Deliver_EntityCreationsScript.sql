USE GMGCoZone
--------------------------------------------------------------------------------------------------------------
-- Create ColorProof Workflow Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'ColorProofInstance' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[ColorProofInstance]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Guid] [nvarchar](36) NULL ,
              [Owner] INT NOT NULL ,
              [Version] [nvarchar](50) NULL ,
              [UserName] [nvarchar](128) NOT NULL ,
              [Password] [nvarchar](128) NOT NULL ,
              [ComputerName] [nvarchar](50) NOT NULL ,
              [SystemName] [nvarchar](50) NULL ,
              [SerialNumber] [nvarchar](128) NULL ,
              [IsOnline] [bit] NULL ,
              [LastSeen] [bigint] NULL ,
              [State] [int] NOT NULL ,
              [AdminName] [nvarchar](128) NULL ,
              [Company] [nvarchar](128) NULL ,
              [Address] [nvarchar](256) NULL ,
              [Email] [nvarchar](50) NULL ,
              [Phone] [nvarchar](50) NULL ,
              [Website] [nvarchar](128) NULL ,
              [WorkflowsLastModifiedTimestamp] [bigint] NULL ,
              CONSTRAINT [PK_ColorProofInstance] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    
        ALTER TABLE dbo.ColorProofInstance ADD
        IsDeleted BIT NOT NULL CONSTRAINT DF_ColorProofInstance_IsDeleted DEFAULT (0)
            
        ALTER TABLE [dbo].ColorProofInstance  WITH CHECK ADD  CONSTRAINT [FK_ColorProofInstance_User] FOREIGN KEY([Owner])
        REFERENCES [dbo].[User] ([ID])

    END
ELSE 
    PRINT 'Table ColorProofInstance already exists'
GO


--------------------------------------------------------------------------------------------------------------
-- Create ColorProof Workflow Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'ColorProofWorkflow' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[ColorProofWorkflow]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Guid] [nvarchar](36) NULL ,
              [Name] [nvarchar](256) NULL ,
              [OldName] [nvarchar](256) NULL ,
              [ProofStandard] [nvarchar](256) NULL ,
              [MaximumUsablePaperWidth] [int] NULL ,
              [IsActivated] [bit] NULL ,
              [IsInlineProofVerificationSupported] [bit] NULL ,
              [SupportedSpotColors] [nvarchar](MAX) NULL ,
              [ColorProofInstance] [int] NOT NULL ,
              CONSTRAINT [PK_ColorProofWorkflow] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]

        ALTER TABLE [dbo].[ColorProofWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofWorkflow_ColorProofInstance] FOREIGN KEY([ColorProofInstance])
        REFERENCES [dbo].[ColorProofInstance] ([ID])

        ALTER TABLE [dbo].[ColorProofWorkflow] CHECK CONSTRAINT [FK_ColorProofWorkflow_ColorProofInstance]

    END
ELSE 
    PRINT 'Table ColorProofWorkflow already exists'
GO

--------------------------------------------------------------------------------------------------------------
-- Create ColorProof CoZone Workflow Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'ColorProofCoZoneWorkflow' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[ColorProofCoZoneWorkflow]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [ColorProofWorkflow] [int] NOT NULL ,
              [Name] [nvarchar](256) NULL ,
              [IsAvailable] [bit] NULL ,
              [TransmissionTimeout] [int] NULL ,
              CONSTRAINT [PK_ColorProofCoZoneWorkflow] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]

        ALTER TABLE dbo.ColorProofCoZoneWorkflow ADD
        IsDeleted BIT NOT NULL CONSTRAINT DF_ColorProofCoZoneWorkflow_IsDeleted DEFAULT (0)

        ALTER TABLE [dbo].[ColorProofCoZoneWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofCoZoneWorkflow_ColorProofWorkflow] FOREIGN KEY([ColorProofWorkflow])
        REFERENCES [dbo].[ColorProofWorkflow] ([ID])

        ALTER TABLE [dbo].[ColorProofCoZoneWorkflow] CHECK CONSTRAINT [FK_ColorProofCoZoneWorkflow_ColorProofWorkflow]
    END
ELSE 
    PRINT 'Table ColorProofWorkflow already exists'
GO

--------------------------------------------------------------------------------------------------------------
-- Create ColorProof CoZone Workflow UserGroup Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'ColorProofCoZoneWorkflowUserGroup' )
   ) 
    BEGIN
	
        CREATE TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [ColorProofCoZoneWorkflow] [int] NOT NULL ,
              [UserGroup] [int] NOT NULL ,
              CONSTRAINT [PK_ColorProofCoZoneWorkflowUserGroup] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]

        ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_ColorProofCoZoneWorkflow] FOREIGN KEY([ColorProofCoZoneWorkflow])
        REFERENCES [dbo].[ColorProofCoZoneWorkflow] ([ID])

        ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup] CHECK CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_ColorProofCoZoneWorkflow]

        ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_UserGroup] FOREIGN KEY([UserGroup])
        REFERENCES [dbo].[UserGroup] ([ID])

        ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup] CHECK CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_UserGroup]

    END
ELSE 
    PRINT 'Table ColorProofCoZoneWorkflowUserGroup already exists'
GO

--------------------------------------------------------------------------------------------------------------
-- Create ColorProof Workflow Instace View
GO
CREATE VIEW [dbo].[ColorProofWorkflowInstanceView]
AS
SELECT  dbo.ColorProofCoZoneWorkflow.ID AS ColorProofCoZoneWorkflowID ,
dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow ,
dbo.ColorProofCoZoneWorkflow.Name AS CoZoneName ,
dbo.ColorProofCoZoneWorkflow.IsAvailable ,
dbo.ColorProofCoZoneWorkflow.TransmissionTimeout ,
dbo.ColorProofWorkflow.ID AS ColorProofWorkflowID ,
dbo.ColorProofWorkflow.Guid ,
dbo.ColorProofWorkflow.Name ,
dbo.ColorProofWorkflow.ProofStandard ,
dbo.ColorProofWorkflow.MaximumUsablePaperWidth ,
dbo.ColorProofWorkflow.IsActivated ,
dbo.ColorProofWorkflow.IsInlineProofVerificationSupported ,
dbo.ColorProofWorkflow.SupportedSpotColors ,
dbo.ColorProofWorkflow.ColorProofInstance
FROM    dbo.ColorProofCoZoneWorkflow
INNER JOIN dbo.ColorProofWorkflow ON dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow = dbo.ColorProofWorkflow.ID
INNER JOIN dbo.ColorProofInstance ON dbo.ColorProofWorkflow.ColorProofInstance = dbo.ColorProofInstance.ID
WHERE   dbo.ColorProofInstance.IsDeleted = 0
AND dbo.ColorProofCoZoneWorkflow.IsDeleted = 0
GO

--------------------------------------------------------------------------------------------------------------
-- Create Deliver Job Status Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'DeliverJobStatus' )
   ) 
    BEGIN
        CREATE TABLE dbo.DeliverJobStatus
            (
              ID INT NOT NULL
                     IDENTITY(1, 1) ,
              [Key] INT NOT NULL ,
              Name NVARCHAR(64) NOT NULL
            )
        ON  [PRIMARY]

        ALTER TABLE dbo.DeliverJobStatus ADD CONSTRAINT
        DF_DeliverJobStatus_Name DEFAULT ('') FOR Name

        ALTER TABLE dbo.DeliverJobStatus ADD CONSTRAINT
        PK_DeliverJobStatus PRIMARY KEY CLUSTERED 
        (
        ID
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

        ALTER TABLE dbo.DeliverJobStatus ADD CONSTRAINT
        IX_DeliverJobStatus UNIQUE NONCLUSTERED 
        (
        [Key]
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    END
ELSE 
    PRINT 'Table DeliverJobStatus already exists'
GO


--------------------------------------------------------------------------------------------------------------
-- Create Deliver Job Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'DeliverJob' )
   ) 
    BEGIN
        CREATE TABLE dbo.DeliverJob
            (
              ID INT NOT NULL
                     IDENTITY(1, 1) ,
              Job INT NOT NULL ,
              Guid NVARCHAR(36) NOT NULL ,
              Owner INT NOT NULL ,
              FileName NVARCHAR(128) NOT NULL ,
              CPWorkflow INT NOT NULL ,
              Pages NVARCHAR(50) NOT NULL ,
              CreatedDate DATETIME NOT NULL ,
              IncludeProofMetaInformationOnLabel BIT NOT NULL ,
              LogProofControlResults BIT NOT NULL ,
              [Size] DECIMAL(18, 2) NOT NULL ,
              Status INT NOT NULL ,
              IsAlertSent BIT NOT NULL ,
              IsCancelRequested BIT NOT NULL ,
              TotalNrOfPages INT NOT NULL
            )
        ON  [PRIMARY]
        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        DF_DeliverJob_CreatedDate DEFAULT (GETDATE()) FOR CreatedDate

        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        DF_DeliverJob_IncludeProofMetaInformationOnLabel DEFAULT (0) FOR IncludeProofMetaInformationOnLabel

        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        DF_DeliverJob_LogProofControlResults DEFAULT (0) FOR LogProofControlResults

        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        PK_DeliverJob PRIMARY KEY CLUSTERED 
        (
        ID
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        FK_DeliverJob_Job FOREIGN KEY
        (
        Job
        ) REFERENCES dbo.Job
        (
        ID
        ) ON UPDATE  NO ACTION 
        ON DELETE  NO ACTION 

        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        FK_DeliverJob_User FOREIGN KEY
        (
        Owner
        ) REFERENCES dbo.[User]
        (
        ID
        ) ON UPDATE  NO ACTION 
        ON DELETE  NO ACTION 

        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        FK_DeliverJob_ColorProofWorkflow FOREIGN KEY
        (
        CPWorkflow
        ) REFERENCES dbo.ColorProofCoZoneWorkflow
        (
        ID
        ) ON UPDATE  NO ACTION 
        ON DELETE  NO ACTION 
        
        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        FK_DeliverJob_DeliverJobStatus FOREIGN KEY
        (
        Status
        ) REFERENCES dbo.DeliverJobStatus
        (
        ID
        ) ON UPDATE  NO ACTION 
        ON DELETE  NO ACTION
        
        ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
        DF_DeliverJob_Size DEFAULT (0) FOR [Size]
    END
ELSE 
    PRINT 'Table DeliverJob already exists'    
GO


--------------------------------------------------------------------------------------------------------------
-- Create Color Proof Instance State
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'ColorProofInstanceState' )
   ) 
    BEGIN
        CREATE TABLE dbo.ColorProofInstanceState
            (
              ID INT NOT NULL
                     IDENTITY(1, 1) ,
              [Key] INT NOT NULL ,
              Name NVARCHAR(64) NOT NULL
            )
        ON  [PRIMARY]
        ALTER TABLE dbo.ColorProofInstanceState ADD CONSTRAINT
        DF_ColorProofInstanceState_Name DEFAULT ('') FOR Name
        ALTER TABLE dbo.ColorProofInstanceState ADD CONSTRAINT
        PK_ColorProofInstanceState PRIMARY KEY CLUSTERED 
        (
        ID
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        
        ALTER TABLE dbo.ColorProofInstance ADD CONSTRAINT
        FK_ColorProofInstance_ColorProofInstanceState FOREIGN KEY
        (
        State
        ) REFERENCES dbo.ColorProofInstanceState
        (
        ID
        ) ON UPDATE  NO ACTION 
        ON DELETE  NO ACTION 

        ALTER TABLE dbo.ColorProofInstanceState ADD CONSTRAINT
        IX_ColorProofInstanceState UNIQUE NONCLUSTERED 
        (
        [Key]
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

    END
ELSE 
    PRINT 'Table ColorProofInstanceState already exists'
GO


--------------------------------------------------------------------------------------------------------------
-- Create Color Proof Job Status
CREATE TABLE dbo.ColorProofJobStatus
    (
      ID INT NOT NULL
             IDENTITY(1, 1) ,
      [Key] INT NOT NULL ,
      Name NVARCHAR(64) NOT NULL
    )
ON  [PRIMARY]
GO
ALTER TABLE dbo.ColorProofJobStatus ADD CONSTRAINT
DF_ColorProofJobStatus_Name DEFAULT ('') FOR Name
GO
ALTER TABLE dbo.ColorProofJobStatus ADD CONSTRAINT
PK_ColorProofJobStatus PRIMARY KEY CLUSTERED 
(
[ID]
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ColorProofJobStatus ADD CONSTRAINT
IX_ColorProofJobStatus UNIQUE NONCLUSTERED 
(
[Key]
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.DeliverJob ADD StatusInColorProof INT NOT NULL
GO
ALTER TABLE dbo.DeliverJob ADD CONSTRAINT
FK_DeliverJob_ColorProofJobStatus FOREIGN KEY
(
StatusInColorProof
) REFERENCES dbo.ColorProofJobStatus
(
ID
) ON UPDATE  NO ACTION 
ON DELETE  NO ACTION 
GO


-------------------------------------------------------------------------------------
-- Create AppModule table
CREATE TABLE dbo.AppModule
    (
      ID INT NOT NULL
             IDENTITY(1, 1) ,
      [Key] INT NOT NULL ,
      Name NVARCHAR(50) NOT NULL
    )
ON  [PRIMARY]
GO
ALTER TABLE dbo.AppModule ADD CONSTRAINT
DF_AppModule_Name DEFAULT ('') FOR Name
GO
ALTER TABLE dbo.AppModule ADD CONSTRAINT
PK_AppModule PRIMARY KEY CLUSTERED 
(
ID
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_AppModule ON dbo.AppModule
(
[Key]
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


----------------------------------------------------------------------------------------
-- Add AppModule FK to BillingPlanType
ALTER TABLE dbo.BillingPlanType
ADD AppModule INT NOT NULL DEFAULT (1)
GO

----------------------------------------------------------------------------------------
-- Create SubscriberPlanInfo table
CREATE TABLE dbo.SubscriberPlanInfo
    (
      ID INT NOT NULL
             IDENTITY(1, 1) ,
      NrOfCredits INT NOT NULL ,
      IsQuotaAllocationEnabled BIT NOT NULL ,
      ChargeCostPerProofIfExceeded BIT NOT NULL ,
      SelectedBillingPlan INT NOT NULL ,
      ContractStartDate DATETIME2(7) NULL ,
      Account INT NOT NULL
    )
ON  [PRIMARY]
ALTER TABLE dbo.SubscriberPlanInfo ADD CONSTRAINT
DF_SubscriberPlanInfo_NrOfCredits DEFAULT 0 FOR NrOfCredits

ALTER TABLE dbo.SubscriberPlanInfo ADD CONSTRAINT
DF_SubscriberPlanInfo_IsQuotaAllocationEnabled DEFAULT 0 FOR IsQuotaAllocationEnabled
ALTER TABLE dbo.SubscriberPlanInfo ADD CONSTRAINT
PK_SubscriberPlanInfo PRIMARY KEY CLUSTERED 
(
ID
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE dbo.SubscriberPlanInfo ADD CONSTRAINT
FK_SubscriberPlanInfo_Account FOREIGN KEY
(
Account
) REFERENCES dbo.Account
(
ID
) ON UPDATE  NO ACTION 
ON DELETE  NO ACTION 
	
ALTER TABLE dbo.SubscriberPlanInfo ADD CONSTRAINT
FK_SubscriberPlanInfo_BillingPlan FOREIGN KEY
(
SelectedBillingPlan
) REFERENCES dbo.BillingPlan
(
ID
) ON UPDATE  NO ACTION 
ON DELETE  NO ACTION 
	
GO

-----------------------------------------------------------------------------------
-- Add UserRole unique key as (Role, User)
ALTER TABLE dbo.UserRole ADD CONSTRAINT
IX_UserRole UNIQUE NONCLUSTERED 
(
Role,
[User]
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-----------------------------------------------------------------------------------
-- Create table for extend the billingplan per module
CREATE TABLE dbo.AccountSubscriptionPlan
    (
      ID INT NOT NULL
             IDENTITY(1, 1) ,
      SelectedBillingPlan INT NOT NULL ,
      IsMonthlyBillingFrequency BIT NOT NULL ,
      ChargeCostPerProofIfExceeded BIT NOT NULL ,
      ContractStartDate DATETIME2 NULL ,
      Account INT NOT NULL
    )
ON  [PRIMARY]
GO
ALTER TABLE dbo.AccountSubscriptionPlan ADD CONSTRAINT
PK_AccountSubscriptionPlan PRIMARY KEY CLUSTERED 
(
ID
) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE dbo.AccountSubscriptionPlan ADD CONSTRAINT
FK_AccountSubscriptionPlan_Account FOREIGN KEY
(
Account
) REFERENCES dbo.Account
(
ID
) ON UPDATE  NO ACTION 
ON DELETE  NO ACTION 
	
ALTER TABLE dbo.AccountSubscriptionPlan ADD CONSTRAINT
FK_AccountSubscriptionPlan_BillingPlan FOREIGN KEY
(
SelectedBillingPlan
) REFERENCES dbo.BillingPlan
(
ID
) ON UPDATE  NO ACTION 
ON DELETE  NO ACTION 
	
GO

-----------------------------------------------------------------------------------
-- Alter Return Billing Report Info View
ALTER VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS BillingPlanName,
0.0 AS GPPDiscount,
0 AS Proofs,
'' AS BillingCycleDetails,
0 AS Parent,
0 AS BillingPlan,
CONVERT(BIT, 0) AS IsFixedPlan,
'' AS Subordinates,
0 AS AppModule
GO


-----------------------------------------------------------------------------------
-- Alter Get Billing Cycles function
ALTER FUNCTION [dbo].[GetBillingCycles]
    (
      @AccountID INT ,
      @LoggedAccountID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @ModuleId INT	
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN

        DECLARE @cycleCount INT = 0
        DECLARE @AddedProofsCount INT = 0
        DECLARE @totalExceededCount INT = 0	
        DECLARE @cycleStartDate DATETIME = @StartDate
        DECLARE @cycleEndDate DATETIME
        DECLARE @cycleAllowedProofsCount INT
        DECLARE @IsMonthly BIT = 1 
        DECLARE @PlanPrice DECIMAL = 0.0
        DECLARE @ParentAccount INT
        DECLARE @SelectedBillingPLan INT
        DECLARE @BillingPlanPrice INT
        DECLARE @moduleKey VARCHAR(10)
        DECLARE @accountTypeKey VARCHAR(10)
        
        SELECT  @accountTypeKey = AT.[Key]
        FROM    dbo.AccountType AT
                INNER JOIN dbo.Account A ON A.AccountType = AT.ID
        WHERE   A.ID = @AccountID
		
        IF ( @accountTypeKey = 'SBSC' ) 
            BEGIN
				-- subscriber
                SELECT  @SelectedBillingPLan = SPI.SelectedBillingPlan ,
                        @ParentAccount = A.Parent ,
                        @cycleAllowedProofsCount = BP.Proofs ,
                        @BillingPlanPrice = BP.Price ,
                        @IsMonthly = CONVERT(BIT, 1)
                FROM    Account A
                        INNER JOIN dbo.SubscriberPlanInfo SPI ON A.ID = SPI.Account
                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId            
            END
        ELSE
			-- all other account types 
            BEGIN
                SELECT  @SelectedBillingPLan = ASP.SelectedBillingPlan ,
                        @ParentAccount = A.Parent ,
                        @cycleAllowedProofsCount = BP.Proofs ,
                        @BillingPlanPrice = BP.Price ,
                        @IsMonthly = ASP.IsMonthlyBillingFrequency
                FROM    Account A
                        INNER JOIN dbo.AccountSubscriptionPlan ASP ON A.ID = ASP.Account
                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                        INNER JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.Type
                WHERE   A.ID = @AccountID
                        AND BPT.AppModule = @ModuleId
            END
	
        SET @cycleAllowedProofsCount = ( CASE WHEN ( @IsMonthly = 1 )
                                              THEN @cycleAllowedProofsCount
                                              ELSE ( @cycleAllowedProofsCount
                                                     * 12 )
                                         END ) 										
											
        SET @PlanPrice = ( CASE WHEN ( @LoggedAccountID = 1 )
                                THEN @BillingPlanPrice
                                ELSE ( SELECT   NewPrice
                                       FROM     AttachedAccountBillingPlan
                                       WHERE    Account = @LoggedAccountID
                                                AND BillingPlan = @SelectedBillingPLan
                                     )
                           END )
						
        SET @PlanPrice = ( CASE WHEN ( @IsMonthly = 1 ) THEN @PlanPrice
                                ELSE ( @PlanPrice * 12 )
                           END ) 
																					
        SET @cycleStartDate = @StartDate
	
        WHILE ( @cycleStartDate < @EndDate ) 
            BEGIN	
                DECLARE @currentCycleJobsCount INT = 0
				
                SET @cycleEndDate = CASE WHEN ( @IsMonthly = 1 )
                                         THEN DATEADD(MM, 1, @cycleStartDate)
                                         ELSE DATEADD(YYYY, 1, @cycleStartDate)
                                    END	
	
                SELECT  @currentCycleJobsCount = CASE WHEN Am.[Key] = 0 -- Collaborate
                                                           THEN ( SELECT
                                                              COUNT(ap.ID)
                                                              FROM
                                                              Job j
                                                              INNER JOIN Approval ap ON j.ID = ap.Job
                                                              WHERE
                                                              j.Account = @AccountID
                                                              AND ap.CreatedDate >= @cycleStartDate
                                                              AND ap.CreatedDate <= @cycleEndDate
                                                              )
                                                      WHEN Am.[Key] = 2 -- Deliver
                                                           THEN ( SELECT
                                                              COUNT(dj.ID)
                                                              FROM
                                                              Job j
                                                              INNER JOIN dbo.DeliverJob dj ON j.ID = dj.Job
                                                              WHERE
                                                              j.Account = @AccountID
                                                              AND dj.CreatedDate >= @cycleStartDate
                                                              AND dj.CreatedDate <= @cycleEndDate
                                                              )
                                                      ELSE 0 -- for other modules 0 jobs is returned
                                                 END
                FROM    dbo.AppModule AM
                WHERE   AM.ID = @ModuleId
				
			
                SET @AddedProofsCount = @AddedProofsCount
                    + @currentCycleJobsCount
													
                SET @cycleStartDate = CASE WHEN ( @IsMonthly = 1 )
                                           THEN DATEADD(MM, 1, @cycleStartDate)
                                           ELSE DATEADD(YYYY, 1,
                                                        @cycleStartDate)
                                      END
								
                SET @cycleCount = @cycleCount + 1 ;	
                SET @totalExceededCount = @totalExceededCount
                    + CASE WHEN ( @cycleAllowedProofsCount < @currentCycleJobsCount )
                           THEN @currentCycleJobsCount
                                - @cycleAllowedProofsCount
                           ELSE 0
                      END
            END
		
        RETURN  CONVERT(NVARCHAR, @cycleCount) + '|' +  CONVERT(NVARCHAR, @AddedProofsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice)
	
    END
GO


-----------------------------------------------------------------------------------
-- Alter Return Account Parameters procedure
ALTER PROCEDURE [dbo].[SPC_ReturnAccountParameters] ( @P_LoggedAccount INT )
AS 
    BEGIN

        SET NOCOUNT ON
        DECLARE @t TABLE
            (
              AccountId INT ,
              CollaborateBillingPlanId INT ,
              CollborateBillingPlanName NVARCHAR(100) ,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100)
            )
        INSERT  INTO @t
                ( AccountId ,
                  CollaborateBillingPlanId ,
                  CollborateBillingPlanName ,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName
                )
                SELECT  DISTINCT
                        A.ID AS [AccountId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CASPBP.ID, 0)
                             ELSE ISNULL(CASPBP.ID, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CASPBP.Name, '')
                             ELSE ISNULL(CASPBP.Name, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DASPBP.ID, 0)
                             ELSE ISNULL(DASPBP.ID, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DASPBP.Name, '')
                             ELSE ISNULL(DASPBP.Name, '')
                        END AS DeliverBillingPlanName
                FROM    dbo.Account A
                        INNER JOIN AccountType AT ON A.AccountType = AT.ID
                        LEFT OUTER JOIN dbo.AccountSubscriptionPlan CASP ON CASP.Account = A.ID
                        LEFT OUTER JOIN dbo.BillingPlan CASPBP ON CASPBP.ID = CASP.SelectedBillingPlan
                        LEFT OUTER JOIN dbo.BillingPlanType CASPBPT ON CASPBP.[Type] = CASPBPT.ID
                        LEFT OUTER JOIN dbo.AppModule CASPAM ON CASPAM.ID = CASPBPT.AppModule
                                                              AND CASPAM.[Key] = 0 -- Collaborate
                        LEFT OUTER JOIN dbo.SubscriberPlanInfo CSPI ON CSPI.Account = A.ID
                        LEFT OUTER JOIN dbo.BillingPlan CSPIBP ON CSPIBP.ID = CSPI.SelectedBillingPlan
                        LEFT OUTER JOIN dbo.BillingPlanType CSPIBPT ON CSPIBP.Type = CSPIBPT.ID
                        LEFT OUTER JOIN dbo.AppModule CSPIAM ON CASPAM.ID = CSPIBPT.AppModule
                                                              AND CSPIAM.[Key] = 0 -- Collaborate for Subscriber
                        LEFT OUTER JOIN dbo.AccountSubscriptionPlan DASP ON DASP.Account = A.ID
                        LEFT OUTER JOIN dbo.BillingPlan DASPBP ON DASPBP.ID = DASP.SelectedBillingPlan
                        LEFT OUTER JOIN dbo.BillingPlanType DASPBPT ON DASPBP.[Type] = DASPBPT.ID
                        LEFT OUTER JOIN dbo.AppModule DASPAM ON DASPAM.ID = DASPBPT.AppModule
                                                              AND DASPAM.[Key] = 2 -- Deliver
                        LEFT OUTER JOIN dbo.SubscriberPlanInfo DSPI ON DSPI.Account = A.ID
                        LEFT OUTER JOIN dbo.BillingPlan DSPIBP ON DSPIBP.ID = DSPI.SelectedBillingPlan
                        LEFT OUTER JOIN dbo.BillingPlanType DSPIBPT ON DSPIBP.Type = DSPIBPT.ID
                        LEFT OUTER JOIN dbo.AppModule DSPIAM ON DASPAM.ID = DSPIBPT.AppModule
                                                              AND DSPIAM.[Key] = 2 -- Deliver for Subscriber
                WHERE   ( CASPAM.ID IS NOT NULL
                          OR CSPIAM.ID IS NOT NULL
                        )
                        AND ( DASPAM.ID IS NOT NULL
                              OR DSPIAM.ID IS NOT NULL
                            )

        SELECT DISTINCT
                a.ID AS AccountID ,
                a.Name AS AccountName ,
                at.ID AS AccountTypeID ,
                at.Name AS AccountTypeName ,
                BP.CollaborateBillingPlanId ,
                BP.CollborateBillingPlanName ,
                BP.DeliverBillingPlanId ,
                BP.DeliverBillingPlanName ,
                ast.ID AS AccountStatusID ,
                ast.Name AS AccountStatusName ,
                cty.ID AS LocationID ,
                cty.ShortName AS LocationName
        FROM    Account a
                INNER JOIN AccountType at ON a.AccountType = at.ID
                INNER JOIN AccountStatus ast ON a.[Status] = ast.ID
                INNER JOIN Company c ON c.Account = a.ID
                INNER JOIN Country cty ON c.Country = cty.ID
                INNER JOIN @t BP ON BP.AccountId = A.ID
        WHERE   a.Parent = @P_LoggedAccount
                AND a.IsSuspendedOrDeletedByParent = 0
                AND a.IsTemporary = 0
                AND ( ast.[Key] = 'A' )
    END


GO


-----------------------------------------------------------------------------------
-- Alter Return Account Parameters view
ALTER VIEW [dbo].[ReturnAccountParametersView] 
AS 
SELECT  0 AS AccountID,
'' AS AccountName,
0 AS AccountTypeID,
'' AS AccountTypeName,
0 AS CollaborateBillingPlanId,
'' AS CollborateBillingPlanName,
0 AS DeliverBillingPlanId,
'' AS DeliverBillingPlanName,
0 AS AccountStatusID,
'' AS AccountStatusName,
0 AS LocationID,
'' AS LocationName

GO

-----------------------------------------------------------------------------------
-- Alter Return Billing Report Info procedure
-- Description:  This Sp returns multiple result sets that needed for Billing Report
ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_IsMonthlyBilling BIT = 1 ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              AccountStatus VARCHAR(10) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateCycleDetails NVARCHAR(100) ,
              CollaborateModuleId INT ,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverCycleDetails NVARCHAR(100) ,
              DeliverModuleId INT
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  AccountStatus ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverCycleDetails ,
                  DeliverModuleId
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        AcS.[Key] AS [AccountStatus] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CSPIBP.ModuleId)
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CASPBP.ModuleId)
                               END, '0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DASPBP.ModuleId)
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DSPIBP.ModuleId)
                               END, '0|0|0|0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId]
                FROM    dbo.Account A
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    AcS.[Key]
                                      FROM      dbo.AccountStatus AcS
                                      WHERE     AcS.ID = A.Status
                                    ) AcS ( [Key] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )

                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed] )
                
                
                
                            
        DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN @t BP ON BP.AccountId = A.ID
                WHERE   Parent = @P_LoggedAccount
								--AND a.NeedSubscriptionPlan = 1
								--AND a.ContractStartDate IS NOT NULL
                        AND a.IsTemporary = 0
								--AND (ast.[Key] = 'A')
								--AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR BP.CollaborateBillingPlanID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_BillingPlanList,
                                                        ',') )
                              OR BP.DeliverBillingPlanID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_BillingPlanList,
                                                        ',') )
                            )

        DECLARE @childAccounts TABLE ( ID INT )		
	
        DECLARE @subAccount_ID INT
        DECLARE Cursor_SubAccounts CURSOR
        FOR SELECT ID FROM @subAccounts	
        OPEN Cursor_SubAccounts
        FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
        WHILE @@FETCH_STATUS = 0 
            BEGIN	
                INSERT  INTO @childAccounts
                        SELECT  ID
                        FROM    [dbo].[GetChildAccounts](@subAccount_ID)
                FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
            END

        CLOSE Cursor_SubAccounts
        DEALLOCATE Cursor_SubAccounts
		
	-- Get the accounts	
        SET NOCOUNT OFF
			
        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
                ISNULL(( SELECT SLCT.Name + ','
                         FROM   ( SELECT    DISTINCT
                                            sa.Name
                                  FROM      Account sa
                                            INNER JOIN AccountStatus sast ON sa.[Status] = sast.ID
                                            INNER JOIN dbo.SubscriberPlanInfo SPI ON SPI.Account = sa.ID
                                  WHERE     sa.Parent = a.ID
                                            AND sa.IsTemporary = 0
                                            AND SPI.ContractStartDate IS NOT NULL
                                            AND ( sast.[Key] = 'A' )
                                  UNION
                                  SELECT    DISTINCT
                                            sa.Name
                                  FROM      Account sa
                                            INNER JOIN AccountStatus sast ON sa.[Status] = sast.ID
                                            INNER JOIN dbo.AccountSubscriptionPlan ASP ON ASP.Account = sa.ID
                                  WHERE     sa.Parent = a.ID
                                            AND sa.IsTemporary = 0
                                            AND ASP.ContractStartDate IS NOT NULL
                                            AND ( sast.[Key] = 'A' )
                                ) AS SLCT
                         GROUP BY SLCT.Name
                       FOR
                         XML PATH('')
                       ), '') AS Subordinates
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID
        WHERE   ( a.ID IN ( SELECT  *
                            FROM    @subAccounts )
                  OR a.ID IN ( SELECT   *
                               FROM     @childAccounts )
                )
                AND A.IsTemporary = 0
                AND ( TBL.[AccountStatus] = 'A' )
    END

GO



-----------------------------------------------------------------------------------
-- Alter Return Billing Report Info view
ALTER VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS CollaborateBillingPlanName,
0 AS CollaborateProofs,
'' AS CollaborateBillingCycleDetails,
0 AS CollaborateBillingPlan,
CONVERT(BIT, 0) AS CollaborateIsFixedPlan,
'' AS DeliverBillingPlanName,
0 AS DeliverProofs,
'' AS DeliverBillingCycleDetails,
0 AS DeliverBillingPlan,
CONVERT(BIT, 0) AS DeliverIsFixedPlan,
0.0 AS GPPDiscount,
0 AS Parent,
'' AS Subordinates

GO


-----------------------------------------------------------------------------------
-- Add Deliver Job Page Status table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'DeliverJobPageStatus' )
   ) 
    BEGIN
        CREATE TABLE dbo.DeliverJobPageStatus
            (
              ID INT NOT NULL
                     IDENTITY(1, 1) ,
              [Key] INT NOT NULL ,
              Name NVARCHAR(64) NOT NULL
            )
        ON  [PRIMARY]

        ALTER TABLE dbo.DeliverJobPageStatus ADD CONSTRAINT
        DF_DeliverJobPageStatus_Name DEFAULT ('') FOR Name

        ALTER TABLE dbo.DeliverJobPageStatus ADD CONSTRAINT
        PK_DeliverJobPageStatus PRIMARY KEY CLUSTERED 
        (
        ID
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

        ALTER TABLE dbo.DeliverJobPageStatus ADD CONSTRAINT
        IX_DDeliverJobPageStatus UNIQUE NONCLUSTERED 
        (
        [Key]
        ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    END
ELSE 
    PRINT 'Table DeliverJobPageStatus already exists'
GO

-----------------------------------------------------------------------------------
-- Add Deliver Job Page table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'DeliverJobImage' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[DeliverJobPage]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [UserFriendlyName] [nvarchar](256) NOT NULL ,
              [Status] INT NOT NULL ,
              [ErrorMessage] [nvarchar](1024) NULL ,
              [DeliverJob] INT NOT NULL ,
              CONSTRAINT [PK_DeliverJobImage] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
       
        ALTER TABLE [dbo].[DeliverJobPage]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobPage_DDeliverJobPageStatus] FOREIGN KEY([Status])
        REFERENCES [dbo].[DeliverJobPageStatus] ([ID])
        
        ALTER TABLE [dbo].[DeliverJobPage]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobPage_DeliverJob] FOREIGN KEY([DeliverJob])
        REFERENCES [dbo].[DeliverJob] ([ID])
    END
ELSE 
    PRINT 'Table DeliverJobPage already exists'
GO



-----------------------------------------------------------------------------------
-- Add Deliver Job Page Verification Result table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'DeliverJobPageVerificationResult' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[DeliverJobPageVerificationResult]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [StripName] [nvarchar](512) NOT NULL ,
              [MaxDelta] FLOAT NOT NULL ,
              [AvgDelta] FLOAT NOT NULL ,
              [ProofResult] BIT NULL ,
              [AllowedTolleranceMax] FLOAT NULL ,
              [AllowedTolleranceAvg] FLOAT NULL ,
              [IsSpotColorVerification] BIT NOT NULL ,
              [DeliverJobPage] INT NOT NULL ,
              CONSTRAINT [PK_DeliverJobPageVerificationResult] PRIMARY KEY CLUSTERED
                ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
       
        ALTER TABLE [dbo].[DeliverJobPageVerificationResult]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobPageVerificationResult_DeliverJobPage] FOREIGN KEY([DeliverJobPage])
        REFERENCES [dbo].[DeliverJobPage] ([ID])
    END
ELSE 
    PRINT 'Table DeliverJobPageVerificationResult already exists'
GO



-----------------------------------------------------------------------------------
-- Add Approval Type table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'ApprovalType' )
   ) 
    BEGIN
        CREATE TABLE [dbo].[ApprovalType]
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL ,
              [Key] INT NOT NULL ,
              [Name] [nvarchar](256) NOT NULL ,
              CONSTRAINT [PK_ApprovalType] PRIMARY KEY CLUSTERED ( [ID] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
        
        
        ALTER TABLE [dbo].[Approval] ADD [Type] INT NOT NULL DEFAULT(1)
    
    END
ELSE 
    PRINT 'Table ApprovalType already exists'
GO


-----------------------------------------------------------------------------------
-- Add Return Delivers Page View
CREATE VIEW [dbo].[ReturnDeliversPageView] 
AS 
SELECT  0 AS [ID] ,
'' AS [Title] ,
'' AS [User] ,
'' AS [Pages] ,
'' AS [CPName] ,
'' AS [Workflow] ,
GETDATE() AS [CreatedDate] ,
0 AS [StatusKey]
GO

-----------------------------------------------------------------------------------
-- Alter Role table to add the module column
ALTER TABLE dbo.Role ADD AppModule INT NOT NULL CONSTRAINT DF_Role_AppModule DEFAULT (1)
GO

-----------------------------------------------------------------------------------
-- Add Return Account User Collaborators And Groups procedure
-- used for retrieving account user and collaborator group
CREATE PROCEDURE [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]
	-- Add the parameters for the stored procedure here
	@P_AccountID INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	DECLARE @ApproverAndReviewer INT
	DECLARE @Reviewer INT
	DECLARE @ReadOnlyr INT
	
	SELECT @ApproverAndReviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'ANR';
	SELECT @Reviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RVW';
	SELECT @ReadOnlyr = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RDO';
	
	SELECT 
		CONVERT (bit, 0) AS IsGroup,
		CONVERT (bit, 0) AS IsExternal,
		CONVERT (bit, 0) AS IsChecked,
		u.ID AS ID,
		0 AS [Count],
		
		-- IDs concatenated as string
		COALESCE(
			(SELECT DISTINCT ' cfg' + convert(varchar, ug.ID)
				FROM dbo.UserGroup ug
				INNER JOIN dbo.UserGroupUser ugu ON ugu.[User] = u.ID 
							AND ugu.UserGroup = ug.ID
				FOR XML PATH('') ),
			'')
			AS Members,
        
        -- name as given mane + family name
		u.GivenName + ' ' + u.FamilyName
			AS Name,
		
		-- get user role for colaborate module
		(SELECT acr.ID
			FROM dbo.ApprovalCollaboratorRole acr
			WHERE acr.ID =  CASE (SELECT r.Name
								FROM dbo.UserRole ur 
								INNER JOIN dbo.[Role] r ON ur.Role = r.ID						
								WHERE ur.[User] = u.ID
									AND r.AppModule = (SELECT am.ID FROM dbo.AppModule am
														WHERE am.[Key] = 0))
														
								WHEN 'Administrator' THEN @ApproverAndReviewer
								WHEN 'Manager' THEN @ApproverAndReviewer
								WHEN 'Moderator' THEN @ApproverAndReviewer
								WHEN 'Contributor' THEN @Reviewer
								ELSE @ReadOnlyr
							END)						
			AS [Role]
			
	FROM dbo.[User] u
	WHERE Account = @P_AccountID
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'I')
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'D')
	
END
GO

-----------------------------------------------------------------------------------
-- Add Return Account User Collaborators And Groups procedure
CREATE VIEW [dbo].[ReturnAccountUserCollaboratorsAndGroupsView]
AS
SELECT  CONVERT(BIT, 0) AS IsGroup ,
        CONVERT(BIT, 0) AS IsExternal ,
        CONVERT(BIT, 0) AS IsChecked ,
        0 AS ID ,
        0 AS Count ,
        '' AS Members ,
        '' AS Name ,
        0 AS Role
GO

-----------------------------------------------------------------------------------
-- Add Timeline field for ApprovalAnnotation for Video File Support
ALTER TABLE dbo.ApprovalAnnotation
ADD TimeFrame INT NULL
GO


-----------------------------------------------------------------------------------
-- Add Return Approvals Page Info procedure
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID, 
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END

END
GO


-----------------------------------------------------------------------------------
-- Add Return Approvals Page View
ALTER VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			0 AS [Status],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			'' AS Collaborators,
			0 AS Decision,
			CONVERT(INT, 0) AS DocumentPagesCount
GO


-----------------------------------------------------------------------------------
-- Add Return Folders Approvals Page Info
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0
						) AS ApprovalsCount,
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS [DocumentPagesCount]
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
				ORDER BY 
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN Deadline
					--END ASC,
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN Deadline
					--END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN f.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN f.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN f.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN f.ModifiedDate
					END DESC--,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN f.[Status]
					--END ASC,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN f.[Status]
					--END DESC		 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO


-----------------------------------------------------------------------------------
-- Alter Account table to add options for landing page
ALTER TABLE dbo.Account ADD
	DisableLandingPage bit NOT NULL CONSTRAINT DF_Account_DisableLandingPage DEFAULT (0),
	DashboardToShow int NOT NULL CONSTRAINT DF_Account_DashboardToShow DEFAULT (0)
GO


-----------------------------------------------------------------------------------
-- Add CMS Video table
CREATE TABLE dbo.CMSVideo
	(
	ID int NOT NULL IDENTITY (1, 1),
	IsTrainning bit NOT NULL,
	VideoUrl nvarchar(512) NOT NULL
	)  ON [PRIMARY]
ALTER TABLE dbo.CMSVideo ADD CONSTRAINT
	DF_CMSVideo_IsTrainning DEFAULT (0) FOR IsTrainning
ALTER TABLE dbo.CMSVideo ADD CONSTRAINT
	DF_CMSVideo_VideoUrl DEFAULT ('') FOR VideoUrl
ALTER TABLE dbo.CMSVideo ADD CONSTRAINT
	PK_CMSVideo PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-----------------------------------------------------------------------------------
-- Add CMS Announcement table
CREATE TABLE dbo.CMSAnnouncement
	(
	ID int NOT NULL IDENTITY (1, 1),
	[Content] nvarchar(1024) NOT NULL
	)  ON [PRIMARY]
ALTER TABLE dbo.CMSAnnouncement ADD CONSTRAINT
	PK_CMSAnnouncement PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-----------------------------------------------------------------------------------
---Extend FileType Table with a new column with the extention type: movie , image or swf
ALTER Table dbo.FileType 
Add [Type] nvarchar(256)
GO


-----------------------------------------------------------------------------------
-- Add Documents Page Count to procedure
ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
			SELECT 	TOP (@P_Set * @P_MaxRows)
					CONVERT(int, ROW_NUMBER() OVER(
					ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
					END DESC					
				)) AS ID, 
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,							 
					j.[Status] AS [Status],	
					j.ID AS Job,
					a.[Version],
					100 AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					(SELECT MAX([Version])
					 FROM Approval av
					 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT 	a.ID
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	  
END

GO



-----------------------------------------------------------------------------------
-- Alter Procedure to add DocumentPagesCount to return
ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision,
			result.DocumentPagesCount
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision,
						0 AS DocumentPagesCount
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END

GO

-----------------------------------------------------------------------------------
-- Alter Procedure to add ReturnArchivedApprovalInfo
ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
				)) AS ID,
				a.ID AS Approval,
				0 AS Folder,
				j.Title,
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				100 AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				0 AS MaxVersion,
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				'######' AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision,
				Document.PagesCount AS [DocumentPagesCount]
		FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
				OUTER APPLY (SELECT COUNT(AP.ID) FROM dbo.ApprovalPage AP WHERE AP.Approval = a.ID) Document(PagesCount)
		WHERE	j.Account = @P_Account
				AND a.IsDeleted = 0	
				AND js.[Key] = 'ARC'
				AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
				AND
				(	SELECT MAX([Version])
					FROM Approval av 
					WHERE 
						av.Job = a.Job
						AND av.IsDeleted = 0
						AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
				) = a.[Version]	
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	 
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM	(
			SELECT 	a.ID
			FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0	
					AND js.[Key] = 'ARC'
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
					) = a.[Version]
				)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	 
END
GO

-----------------------------------------------------------------------------------
-- Add Field for transcoding videos id
ALTER TABLE dbo.[Approval]
Add TranscodingJobId nvarchar(256) NULL
GO

-----------------------------------------------------------------------------------
-- Get role for user and module
CREATE FUNCTION [dbo].[GetUserRoleByUserIdAndModuleId]
    (
      @UserId INT ,
      @ModuleId INT	
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN
        DECLARE @result VARCHAR(MAX) = 'NON'
        
        SELECT  @result = R.[Key]
        FROM    dbo.[User] U
                INNER JOIN dbo.UserRole UR ON UR.[User] = U.ID
                INNER JOIN dbo.Role R ON UR.Role = R.ID
                INNER JOIN dbo.AppModule AM ON AM.ID = R.AppModule
        WHERE   U.ID = @userId
                AND Am.[Key] = @ModuleId

        RETURN @result ;
    END
GO

-----------------------------------------------------------------------------------
-- get all users that are in the same user group with the user that is provided as parameter
CREATE FUNCTION [dbo].[GetUserListWhereBelongs] ( @userId INT )
RETURNS TABLE
AS RETURN
    ( SELECT DISTINCT
                SLCT.[User]
      FROM      ( SELECT    UGU.[User]
                  FROM      dbo.UserGroupUser UGU
                  WHERE     UGU.[User] = @userId
                            OR UGU.UserGroup IN (
                            SELECT    DISTINCT
                                    UGU2.UserGroup
                            FROM    dbo.UserGroupUser UGU2
                            WHERE   UGU2.[User] = @userId )
                  UNION
                  SELECT    @userId AS [User]
                ) AS SLCT
    )
GO
-----------------------------------------------------------------------------------
-- Add Return Delivers Page Info procedure
CREATE PROCEDURE [dbo].[SPC_ReturnDeliversPageInfo]
    (
      @P_Account INT ,
      @P_User INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;
        
        DECLARE @Delivers TABLE
            (
              ID INT ,
              [Title] NVARCHAR(128) ,
              [User] NVARCHAR(64) ,
              [Pages] NVARCHAR(50) ,
              [CPName] NVARCHAR(50) ,
              [Workflow] NVARCHAR(256) ,
              [CreatedDate] DATETIME ,
              [StatusKey] INT ,
              [Account] INT ,
              [OwnUser] INT
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  Title ,
                  [User] ,
                  Pages ,
                  CPName ,
                  Workflow ,
                  CreatedDate ,
                  StatusKey ,
                  Account ,
                  OwnUser
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] = @P_User
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'CON' -- contributor should only see his jobs
                  UNION
                --- deliver jobs for admin users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' -- admin should see all the jobs from current account
                  UNION
                --- deliver jobs for manager and moderator users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] IN (
                            SELECT  *
                            FROM    dbo.[GetUserListWhereBelongs](@P_User) )
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) IN (
                            'MAN', 'MOD' ) -- moderator and manager should see their jobs + jobs of the users that belongs to the same user group
                  
                )
                
            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.CreatedDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.CreatedDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.Title
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.Title
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.[User]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.[User]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Pages]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Pages]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CpName]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CpName]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[Workflow]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[Workflow]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[StatusKey]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[StatusKey]
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset
        
        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT

    END
GO