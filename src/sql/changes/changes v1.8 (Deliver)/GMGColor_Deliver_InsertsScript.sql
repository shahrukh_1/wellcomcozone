USE GMGCoZone
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'DeliverSettings', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
GO
DECLARE @controllerId INT
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Home', -- Controller - nvarchar(128)
          'Home', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
			
        )
VALUES  ( @controllerId , -- ControllerAction - int
          NULL , -- Parent - int
          -1 , -- Position - int
          0 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          1 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'HOME' , -- Key - nvarchar(4)
          'Home' , -- Name - nvarchar(64)
          'Home'  -- Title - nvarchar(128)
			
        )
GO
DECLARE @controllerId INT
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'CMS', -- Controller - nvarchar(128)
          'Index', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

DECLARE @globalSettingsParentId INT
SELECT  @globalSettingsParentId = MI.ID
FROM    dbo.MenuItem MI
        INNER JOIN dbo.ControllerAction CA ON CA.ID = MI.ControllerAction
WHERE   Action = 'Global'
        AND Controller = 'Settings'
        AND MI.Parent IS NULL

DECLARE @parentMenuItemId AS INT

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
		
        )
VALUES  ( @controllerId , -- ControllerAction - int
          @globalSettingsParentId , -- Parent - int
          3 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'CMSA' , -- Key - nvarchar(4)
          'CMS' , -- Name - nvarchar(64)
          'Content Management'  -- Title - nvarchar(128)
		
        )
--SET @parentMenuItemId = SCOPE_IDENTITY()
GO
DECLARE @ParentMenuId AS INT
DECLARE @ControllerActionId AS INT

SELECT  @ParentMenuId = ID
FROM    dbo.MenuItem
WHERE   [Key] = 'SEIN'

SELECT  @ControllerActionId = ID
FROM    dbo.ControllerAction
WHERE   Controller = 'Settings'
        AND Action = 'DeliverSettings'
      
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
VALUES  ( @ControllerActionId , -- ControllerAction - int
          @ParentMenuId , -- Parent - int
          11 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'SEDS' , -- Key - nvarchar(4)
          'Deliver Settings' , -- Name - nvarchar(64)
          'Settings'  -- Title - nvarchar(128)
        )
GO

DECLARE @MenuItemId AS INT

SELECT  @MenuItemId = ID
FROM    dbo.MenuItem
WHERE   [Key] = 'SEDS'

INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 0, N'Submitting' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 1, N'Created' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 2, N'Downloading' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 3, N'Waiting' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 4, N'Processing' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 5, N'Completed' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 6, N'Cancelled' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key] ,
          [Name]
        )
VALUES  ( 7 ,
          N'Download to ColorProof Failed'
        )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key] ,
          [Name]
        )
VALUES  ( 8 ,
          N'Validation of Job Ticket in ColorProof Failed'
        )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 9, N'Processing Error' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 10, N'Completed&Deleted' )
INSERT  [dbo].[DeliverJobStatus]
        ( [Key], [Name] )
VALUES  ( 11, N'Error&Deleted' )

GO
DECLARE @ControllerActionId AS INT
DECLARE @ParentMenuItemId AS INT

INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'Deliver' ,
          'Index' ,
          ''
        )
SET @ControllerActionId = SCOPE_IDENTITY()

-- Index (for deliver) --
INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId ,
          NULL ,
          3 ,
          0 ,
          1 ,
          0 ,
          1 ,
          1 ,
          'DLIN' ,
          'Deliver' ,
          'Deliver'
        )
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId ,
          @ParentMenuItemId ,
          3 ,
          1 ,
          0 ,
          1 ,
          1 ,
          1 ,
          'DLIM' ,
          'Deliver' ,
          'Deliver'
        )
     
INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'Deliver' ,
          'JobDetailLink' ,
          ''
        )
SET @ControllerActionId = SCOPE_IDENTITY()


INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId ,
          @ParentMenuItemId ,
          0 ,
          0 ,
          0 ,
          0 ,
          0 ,
          0 ,
          'DLJD' ,
          'Deliver' ,
          'Deliver JobDetail'
        )

INSERT  INTO [dbo].[ControllerAction]
        ( [Controller] ,
          [Action] ,
          [Parameters]
        )
VALUES  ( 'Deliver' ,
          'NewDeliver' ,
          ''
        )
SET @ControllerActionId = SCOPE_IDENTITY()
	
INSERT  INTO [dbo].[MenuItem]
        ( [ControllerAction] ,
          [Parent] ,
          [Position] ,
          [IsAdminAppOwned] ,
          [IsAlignedLeft] ,
          [IsSubNav] ,
          [IsTopNav] ,
          [IsVisible] ,
          [Key] ,
          [Name] ,
          [Title]
        )
VALUES  ( @ControllerActionId ,
          @ParentMenuItemId ,
          0 ,
          0 ,
          0 ,
          0 ,
          0 ,
          0 ,
          'DLNJ' ,
          'Deliver' ,
          'Deliver NewJob'
        )
GO

INSERT  [dbo].[ColorProofInstanceState]
        ( [Key], [Name] )
VALUES  ( 1, N'Waiting' )
INSERT  [dbo].[ColorProofInstanceState]
        ( [Key], [Name] )
VALUES  ( 2, N'Ready' )
INSERT  [dbo].[ColorProofInstanceState]
        ( [Key], [Name] )
VALUES  ( 3, N'Stopped' )
INSERT  [dbo].[ColorProofInstanceState]
        ( [Key], [Name] )
VALUES  ( 4, N'Running' )
GO

INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 0, N'Created' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 1, N'Downloading' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 2, N'Waiting' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 3, N'Processing' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 4, N'Completed' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 5, N'Cancelled' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 6, N'Deleted' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 7, N'DownloadFailed' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 8, N'ValidationFailed' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 9, N'ProcessingError' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 10, N'CancelNotPossible' )
INSERT  [dbo].[ColorProofJobStatus]
        ( [Key], [Name] )
VALUES  ( 11, N'Submitting' )
GO

SET IDENTITY_INSERT dbo.[AppModule] ON;
INSERT  INTO dbo.AppModule
        ([ID], [Key], Name )
VALUES  (1,  0, 'Collaborate' )
INSERT  INTO dbo.AppModule
        ([ID],  [Key], Name )
VALUES  (3,  2, 'Deliver' )
INSERT  INTO dbo.AppModule
        ([ID],  [Key], Name )
VALUES  (4,  3, 'Admin' )
INSERT  INTO dbo.AppModule
        ([ID],  [Key], Name )
VALUES  (5,  4, 'SysAdmin' )
SET IDENTITY_INSERT dbo.[AppModule] OFF;
GO

ALTER TABLE [dbo].BillingPlanType WITH CHECK ADD CONSTRAINT [TBL_BillingPlan_AppModule] FOREIGN KEY([AppModule])
REFERENCES [dbo].[AppModule] ([ID])
GO

UPDATE  dbo.Role
SET     AppModule = 5
WHERE   [Key] = 'HQA'
        OR [Key] = 'HQM'
UPDATE  dbo.Role
SET     AppModule = 1
WHERE   [Key] <> 'HQA'
        AND [Key] <> 'HQM'
GO

ALTER TABLE dbo.Role ADD CONSTRAINT
FK_Role_AppModule FOREIGN KEY
(
AppModule
) REFERENCES dbo.AppModule
(
ID
) ON UPDATE  NO ACTION 
ON DELETE  NO ACTION 
GO

/*
1	HQA		Administrator			1		4		-- SysAdmin
2	HQM		Manager					2		4		-- SysAdmin
3	ADM		Administrator			1		1		-- Collaborate
4	MAN		Manager					2		1		-- Collaborate
5	MOD		Moderator				3		1		-- Collaborate
6	CON		Contributor				4		1		-- Collaborate
7	VIE		Viewer					5		1		-- Collaborate
8	NON		NoAccess				10		1		-- Collaborate
14	ADM		Administrator			1		3		-- Deliver
15	MAN		Manager					2		3		-- Deliver
16	MOD		Moderator				3		3		-- Deliver
17	CON		Contributor				4		3		-- Deliver
18	NON		NoAccess				10		3		-- Deliver
19	ADM		Administrator			1		4		-- Admin
20	NON		NoAccess				10		4		-- Admin
*/
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'NON', 'NoAccess', 10, 1 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'ADM', 'Administrator', 1, 3 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'MAN', 'Manager', 2, 3 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'MOD', 'Moderator', 3, 3 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'CON', 'Contributor', 4, 3 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'NON', 'NoAccess', 10, 3 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'ADM', 'Administrator', 1, 4 )
INSERT  INTO dbo.Role
        ( [Key], Name, Priority, AppModule )
VALUES  ( 'NON', 'NoAccess', 10, 4 )
GO


-- add account type role foreach role and each account type
DECLARE @roleId INT
DECLARE Roles CURSOR LOCAL FOR       
SELECT  R.ID FROM    dbo.Role R
OPEN Roles
FETCH NEXT FROM Roles INTO @roleId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @accountTypeId INT
        DECLARE AccountTypes CURSOR LOCAL FOR SELECT AT.ID FROM dbo.AccountType AT
        OPEN AccountTypes
        FETCH NEXT FROM AccountTypes INTO @accountTypeId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.AccountTypeRole
                       WHERE    AccountType = @accountTypeId
                                AND Role = @roleId
                     ) = 0
                     AND @accountTypeId != 1
                   )  -- ignore the ADMIN account type
                    BEGIN
                        INSERT  INTO dbo.AccountTypeRole
                                ( AccountType, Role )
                        VALUES  ( @accountTypeId, -- AccountType - int
                                  @roleId  -- Role - int
                                  )
                    END
                FETCH NEXT FROM AccountTypes INTO @accountTypeId
            END
        CLOSE AccountTypes
        DEALLOCATE AccountTypes
        FETCH NEXT FROM Roles INTO @roleId
    END
CLOSE Roles
DEALLOCATE Roles
GO


INSERT  INTO dbo.SubscriberPlanInfo
        ( NrOfCredits ,
          IsQuotaAllocationEnabled ,
          ChargeCostPerProofIfExceeded ,
          ContractStartDate ,
          SelectedBillingPlan ,
          Account
        )
        ( SELECT    0 AS [NrOfCredits] ,
                    0 AS [IsQuotaAllocationEnabled] ,
                    ChargeCostPerProofIfExceeded ,
                    ContractStartDate ,
                    SelectedBillingPlan ,
                    ID AS [Account]
          FROM      dbo.Account
          WHERE     AccountType = 5
                    AND SelectedBillingPlan IS NOT NULL
        )
GO

INSERT  INTO dbo.AccountSubscriptionPlan
        ( SelectedBillingPlan ,
          IsMonthlyBillingFrequency ,
          ContractStartDate ,
          ChargeCostPerProofIfExceeded ,
          Account
        )
        ( SELECT    SelectedBillingPlan ,
                    IsMonthlyBillingFrequency ,
                    ContractStartDate ,
                    ChargeCostPerProofIfExceeded ,
                    ID AS [Account]
          FROM      dbo.Account
          WHERE     SelectedBillingPlan IS NOT NULL
                    AND AccountType != 5
        )
GO


ALTER TABLE dbo.Account
DROP COLUMN ChargeCostPerProofIfExceeded
ALTER TABLE dbo.Account
DROP CONSTRAINT FK_Account_BillingPlan
ALTER TABLE dbo.Account
DROP COLUMN SelectedBillingPlan
ALTER TABLE dbo.Account
DROP COLUMN IsMonthlyBillingFrequency
ALTER TABLE dbo.Account
DROP COLUMN ContractStartDate
Go



-- Update roles for all account owners
DECLARE @userId INT
DECLARE UsersCursor CURSOR LOCAL FOR SELECT A.Owner FROM dbo.Account A INNER JOIN dbo.AccountType AT ON AT.ID = A.AccountType WHERE   AT.[Key] != 'GMHQ'
OPEN UsersCursor
FETCH NEXT FROM UsersCursor INTO @userId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @moduleID INT
        DECLARE ModulesCursor CURSOR LOCAL FOR SELECT ID FROM dbo.AppModule WHERE [Key] != 4 -- ignore the sysAdmin module
        OPEN ModulesCursor
        FETCH NEXT FROM ModulesCursor INTO @moduleId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.[User] U
                                INNER JOIN dbo.UserRole UR ON UR.[User] = U.ID
                                INNER JOIN Role R ON R.ID = UR.Role
                                INNER JOIN dbo.AppModule AM ON AM.ID = R.AppModule
                       WHERE    U.ID = @userId
                                AND AM.ID = @moduleID
                     ) = 0 ) 
                    BEGIN
                        INSERT  INTO dbo.UserRole
                                ( [User] ,
                                  Role 
                                )
                                SELECT  @userId ,
                                        R.ID
                                FROM    dbo.AppModule AM
                                        INNER JOIN dbo.Role R ON R.AppModule = AM.ID
                                WHERE   R.[Key] = 'ADM' AND AM.ID = @moduleID
                    END
                        
                FETCH NEXT FROM ModulesCursor INTO @moduleId
            END
        CLOSE ModulesCursor
        DEALLOCATE ModulesCursor
        FETCH NEXT FROM UsersCursor INTO @userId
    END
CLOSE UsersCursor
DEALLOCATE UsersCursor
GO


-- Update roles for all other users per module
DECLARE @userId INT
DECLARE UsersCursor CURSOR LOCAL FOR SELECT ID FROM dbo.[User]
OPEN UsersCursor
FETCH NEXT FROM UsersCursor INTO @userId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @moduleID INT
        DECLARE ModulesCursor CURSOR LOCAL FOR SELECT ID FROM dbo.AppModule WHERE [Key] != 4 -- ignore the sysAdmin module
        OPEN ModulesCursor
        FETCH NEXT FROM ModulesCursor INTO @moduleId
        WHILE @@FETCH_STATUS = 0 
            BEGIN
                IF ( ( SELECT   COUNT(1)
                       FROM     dbo.[User] U
                                INNER JOIN dbo.UserRole UR ON UR.[User] = U.ID
                                INNER JOIN Role R ON R.ID = UR.Role
                                INNER JOIN dbo.AppModule AM ON AM.ID = R.AppModule
                       WHERE    U.ID = @userId
                                AND AM.ID = @moduleID
                     ) = 0 ) 
                    BEGIN
                        INSERT  INTO dbo.UserRole
                                ( [User] ,
                                  Role 
                                )
                                SELECT  @userId ,
                                        R.ID
                                FROM    dbo.AppModule AM
                                        INNER JOIN dbo.Role R ON R.AppModule = AM.ID
                                WHERE   (R.[Key] = 'ADM' AND AM.ID = @moduleID and @moduleId = 1) OR       -- Admin for Colaborate
                                        (R.[Key] = 'NON' AND AM.ID = @moduleId AND @moduleId in (2,3,4) ) -- Viewer for Deliver, Manager and Administration
                    END
                        
                FETCH NEXT FROM ModulesCursor INTO @moduleId
            END
        CLOSE ModulesCursor
        DEALLOCATE ModulesCursor
        FETCH NEXT FROM UsersCursor INTO @userId
    END
CLOSE UsersCursor
DEALLOCATE UsersCursor
GO


-- Delete all the menu items per account type role from db
DELETE  dbo.MenuItemAccountTypeRole


-- recreate menu items per account type role
DECLARE @atrId AS INT
-- Administrator for all modules and all account types          
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   (R.[Key] = 'ADM' AND AM.[Key] = 3 AND AT.[Key] != 'SBSC') OR R.[Key] = 'HQA'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'ACIN', 'ACST', 'ACNW', 'ACAI', 'ACAP',
                                      'ACAC', 'ACCA', 'ACSM', 'ACPF', 'ACCU',
                                      'ACSS', 'ACWL', 'ACDN', 'ACPL', 'ACBI',
                                      'ACUS', 'ACEU', 'ACSA', 'PLIN', 'PLBP',
                                      'RPIN', 'RPUR', 'RPBR', 'RPPR', 'SEIN',
                                      'SESM', 'SEPF', 'SECU', 'SESS', 'SEWL',
                                      'SEDN', 'SEUS', 'SEEU', 'SEGR', 'SEEG',
                                      'SEEX' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor



-- User menu rights for the HQM user
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] = 'HQM'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SERD', 'SEIN', 'SESM', 'SEPF', 'SECU', 
                                      'SESS', 'SEUS', 'SEEU', 'SEGR', 'SEEG', 
                                      'ACIN', 'ACST', 'ACNW', 'ACAI', 'ACAP', 
                                      'ACAC', 'ACCA', 'ACSM', 'ACPF', 'ACCU', 
                                      'ACSS', 'ACWL', 'ACDN', 'ACPL', 'ACBI', 
                                      'ACUS', 'ACEU', 'ACSA'
                                     )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor



-- SysAdmin
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] = 'HQA' AND AT.[Key] = 'GMHQ' AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SVIN', 'SVMS', 'GSET', 'GSEP', 'SERD',
                                      'CMSA', 'ANDE', 'ANSA' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

-- Subscriber
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE   R.[Key] = 'ADM' AND AM.[Key] = 3 AND AT.[Key] = 'SBSC'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SEIN', 'SESM', 'SEPF', 'SECU', 'SESS',
                                      'SEWL', 'SEDN', 'SEUS', 'SEEU', 'SEGR',
                                      'SEEG', 'SEEX' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor


-- module Collaborate, Roles: Admin, Manager, Moderator and Contributor
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE  R.[Key] IN ('ADM', 'MAN', 'MOD', 'CON') AND AM.[Key] = 0
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'STIN', 'STAD', 'STFL', 'APIN', 'APDB',
                                      'APPO', 'APNA', 'APDE', 'APDE' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor



-- module Collaborate, Roles: Viewer
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE  R.[Key] IN ('VIE') AND AM.[Key] = 0
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'STFL', 'APIN', 'APDB', 'APPO' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor


-- personal settings - all modules, all roles
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'PSIN', 'PSPI', 'PSPH', 'PSPW', 'PSNT' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

-- home page - all modules except admin, all roles
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE  AM.[Key] != 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'HOME' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor



-- deliver settings menu for deliver module and ADM rights
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 2 AND R.[Key] = 'ADM'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'SEIN', 'SEDS' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor


-- deliver index menu for deliver module and all users except non acces
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 2 AND R.[Key] != 'NON'
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'DLIN', 'DLIM' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

-- deliver index menu for deliver module and all users except non acces and contributor
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 2 AND (R.[Key] != 'NON' AND R.[Key] != 'CON')
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'DLNJ' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

  --rights for Deliver JobDetail         
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT  DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE AM.[Key] = 2 AND (R.[Key] != 'ADM' OR R.[Key] != 'MAN')
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ( 'DLJD' )
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO


INSERT  [dbo].[DeliverJobPageStatus]
        ( [Key], [Name] )
VALUES  ( 0, N'Created' )
INSERT  [dbo].[DeliverJobPageStatus]
        ( [Key], [Name] )
VALUES  ( 1, N'Waiting' )
INSERT  [dbo].[DeliverJobPageStatus]
        ( [Key], [Name] )
VALUES  ( 2, N'Printing' )
INSERT  [dbo].[DeliverJobPageStatus]
        ( [Key], [Name] )
VALUES  ( 3, N'Completed' )
INSERT  [dbo].[DeliverJobPageStatus]
        ( [Key], [Name] )
VALUES  ( 4, N'Deleted' )
INSERT  [dbo].[DeliverJobPageStatus]
        ( [Key], [Name] )
VALUES  ( 5, N'Failed' )       
GO


INSERT  [dbo].[ApprovalType]
        ( [Key], [Name] )
VALUES  ( 0, N'Image' )
INSERT  [dbo].[ApprovalType]
        ( [Key], [Name] )
VALUES  ( 1, N'Movie' )
INSERT  [dbo].[ApprovalType]
        ( [Key], [Name] )
VALUES  ( 2, N'WebPage' )         
GO    

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_ApprovalType] FOREIGN KEY([Type])
        REFERENCES [dbo].[ApprovalType] ([ID])
GO


INSERT  [dbo].[FileType]
        ( [Extention], [Name] )
VALUES  ( 'swf', N'Shockwave Flash' )
INSERT  [dbo].[FileType]
        ( [Extention], [Name] )
VALUES  ( 'qt', N'QuickTime' )
INSERT  [dbo].[FileType]
        ( [Extention], [Name] )
VALUES  ( 'mjpeg', N'Motion JPEG' )
INSERT  [dbo].[FileType]
        ( [Extention], [Name] )
VALUES  ( 'vob', N'Video Object' )
INSERT  [dbo].[FileType]
        ( [Extention], [Name] )
VALUES  ( 'xvid', '' )
INSERT  [dbo].[FileType]
        ( [Extention], [Name] )
VALUES  ( '3vix', '' )
GO

UPDATE  dbo.FileType
SET     [Type] = 'image'
WHERE   Extention IN ( 'jpeg', 'jpg', 'pdf', 'png', 'tiff', 'tif', 'eps' )
UPDATE  dbo.FileType
SET     [Type] = 'video'
WHERE   Extention IN ( 'mp4', 'm4v', 'flv', 'f4v', 'mov', 'mpg', 'mpg2', '3gp',
                       'wmv', 'avi', 'asf', 'fm', 'ogv', 'qt', 'mjpeg', 'vob',
                       'xvid', '3vix' )
UPDATE  dbo.FileType
SET     [Type] = 'swf'
WHERE   Extention IN ( 'swf' )
GO
