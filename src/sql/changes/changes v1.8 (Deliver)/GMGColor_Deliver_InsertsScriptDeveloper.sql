USE GMGCoZone
--Insert Deliver Plans and Plans Type

DECLARE @BillingTypeID AS INT
DECLARE @BillingPlanID AS INT
INSERT  INTO dbo.BillingPlanType
        ( [Key] ,
          Name ,
          MediaService ,
          EnablePrePressTools ,
          EnableMediaTools ,
          AppModule
        )
VALUES  ( 'Deliver Essentials' /* Key - NVARCHAR(64) NOT NULL */ ,
          'Deliver Essentials' /* Name - NVARCHAR(64) NOT NULL */ ,
          1 /* MediaService - INT NOT NULL */ ,
          0 /* EnablePrePressTools - BIT NOT NULL */ ,
          0 /* EnableMediaTools - BIT NOT NULL */ ,
          3/* AppModule - INT NOT NULL */
        )
	        
SET @BillingTypeID = SCOPE_IDENTITY()

INSERT  INTO dbo.BillingPlan
        ( Name ,
          Type ,
          Proofs ,
          Price ,
          IsFixed
        )
VALUES  ( 'Deliver Essentials Free' /* Name - NVARCHAR(128) NOT NULL */ ,
          @BillingTypeID /* Type - INT NOT NULL */ ,
          10 /* Proofs - INT */ ,
          128.25 /* Price - DECIMAL(9, 2) NOT NULL */ ,
          0/* IsFixed - BIT NOT NULL */
        )
	        
INSERT  INTO dbo.BillingPlan
        ( Name ,
          Type ,
          Proofs ,
          Price ,
          IsFixed
        )
VALUES  ( 'Deliver Essential 600' /* Name - NVARCHAR(128) NOT NULL */ ,
          @BillingTypeID /* Type - INT NOT NULL */ ,
          600 /* Proofs - INT */ ,
          550 /* Price - DECIMAL(9, 2) NOT NULL */ ,
          0/* IsFixed - BIT NOT NULL */
        )
	        
SET @BillingPlanID = SCOPE_IDENTITY()

INSERT  INTO dbo.AccountSubscriptionPlan
        ( SelectedBillingPlan ,
          IsMonthlyBillingFrequency ,
          ChargeCostPerProofIfExceeded ,
          ContractStartDate ,
          Account
        )
        SELECT  @BillingPlanID /* SelectedBillingPlan - INT NOT NULL */ ,
                1 /* IsMonthlyBillingFrequency - BIT NOT NULL */ ,
                0 /* ChargeCostPerProofIfExceeded - BIT NOT NULL */ ,
                GETDATE() /* 'YYYY-MM-DD hh:mm:ss[.nnnnnnn]' ContractStartDate - DATETIME2 */ ,
                ID
        FROM    dbo.Account/* Account - INT NOT NULL */
        WHERE   AccountType IN ( 2, -- SBSY
                                 3, -- DELR
                                 4 -- CLNT
                                )

INSERT  INTO dbo.SubscriberPlanInfo
        ( NrOfCredits ,
          IsQuotaAllocationEnabled ,
          ChargeCostPerProofIfExceeded ,
          ContractStartDate ,
          SelectedBillingPlan ,
          Account
        )
        SELECT  155 AS [NrOfCredits] ,
                1 AS [IsQuotaAllocationEnabled] ,
                1 ,
                GETDATE() ,
                @BillingPlanID ,
                ID AS [Account]
        FROM    dbo.Account
        WHERE   AccountType = 5
        
INSERT  INTO dbo.AttachedAccountBillingPlan
        ( Account ,
          BillingPlan ,
          NewPrice ,
          IsAppliedCurrentRate ,
          NewProofPrice ,
          IsModifiedProofPrice
        )
        SELECT  ID AS [Account] ,
                @BillingPlanID AS [BillingPlan] ,
                550 AS [NewPrice] ,
                1 AS [IsAppliedCurrentRate] ,
                2 AS [NewProofPrice] ,
                1 AS [IsModifiedProofPrice]
        FROM    dbo.Account
        WHERE Parent is NOT NULL

INSERT  INTO dbo.BillingPlanType
        ( [Key] ,
          Name ,
          MediaService ,
          EnablePrePressTools ,
          EnableMediaTools ,
          AppModule
        )
VALUES  ( 'Deliver Pre Press' /* Key - NVARCHAR(64) NOT NULL */ ,
          'Deliver Pre Press' /* Name - NVARCHAR(64) NOT NULL */ ,
          1 /* MediaService - INT NOT NULL */ ,
          1 /* EnablePrePressTools - BIT NOT NULL */ ,
          0 /* EnableMediaTools - BIT NOT NULL */ ,
          3/* AppModule - INT NOT NULL */
        )
	        
SET @BillingTypeID = SCOPE_IDENTITY()

INSERT  INTO dbo.BillingPlan
        ( Name ,
          Type ,
          Proofs ,
          Price ,
          IsFixed
        )
VALUES  ( 'Deliver PrePress 300' /* Name - NVARCHAR(128) NOT NULL */ ,
          @BillingTypeID /* Type - INT NOT NULL */ ,
          300 /* Proofs - INT */ ,
          256 /* Price - DECIMAL(9, 2) NOT NULL */ ,
          0/* IsFixed - BIT NOT NULL */
        )

-- change billing plan type media tools and pre press options for collaborate
UPDATE  dbo.BillingPlanType
SET     EnableMediaTools = 1 ,
        EnablePrePressTools = 1
WHERE   AppModule = 1
GO