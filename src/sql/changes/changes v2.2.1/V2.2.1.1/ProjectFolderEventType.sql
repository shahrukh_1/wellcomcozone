USE [GMGCoZone]
GO
-----------------------------------------------------
INSERT INTO [dbo].[EventGroup]
           ([Key],[Name])
     VALUES
           ('PF','ProjectFolder')
GO --12 

------------------------------------------------------


INSERT INTO [dbo].[EventType]
           ([EventGroup],[Key],[Name])
     VALUES
           (12,'PFN','New Project Folder Added')
GO --60

---------------------------------------------------------

INSERT INTO [dbo].[RolePresetEventType]
           ([Role],[Preset],[EventType])
     VALUES
           (35,1,60),(35,2,60),(35,3,60),(35,4,60)
GO

---------------------------------------------------------