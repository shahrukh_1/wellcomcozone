USE [GMGCoZone]
GO

/****** Object:  Table [dbo].[ProjectFolderCollaborator]    Script Date: 30-Nov-20 12:58:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProjectFolderCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectFolder] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[ProjectFolderCollaboratorRole] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_ProjectFolderCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProjectFolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ProjectFolderCollaborator_ProjectFolderCollaboratorRole] FOREIGN KEY([ProjectFolderCollaboratorRole])
REFERENCES [dbo].[ProjectFolderCollaboratorRole] ([ID])
GO

ALTER TABLE [dbo].[ProjectFolderCollaborator] CHECK CONSTRAINT [FK_ProjectFolderCollaborator_ProjectFolderCollaboratorRole]
GO

ALTER TABLE [dbo].[ProjectFolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ProjectFolderCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[ProjectFolderCollaborator] CHECK CONSTRAINT [FK_ProjectFolderCollaborator_Collaborator]
GO

ALTER TABLE [dbo].[ProjectFolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ProjectFolderCollaborator_ProjectFolder] FOREIGN KEY([ProjectFolder])
REFERENCES [dbo].[ProjectFolder] ([ID])
GO

ALTER TABLE [dbo].[ProjectFolderCollaborator] CHECK CONSTRAINT [FK_ProjectFolderCollaborator_ProjectFolder]
GO


