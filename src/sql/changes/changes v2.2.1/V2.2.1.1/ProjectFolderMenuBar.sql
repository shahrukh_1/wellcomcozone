

------------------------ 
USE [GMGCoZone]
GO

INSERT INTO [dbo].[ControllerAction]
           ([Controller],[Action],[Parameters])
     VALUES
           ('ProjectFolders' ,'Index',''),
		   ('ProjectFolders' ,'NewProject','')
GO

INSERT INTO [dbo].[MenuItem]
           ([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],
		   [IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
     VALUES
           (125,Null ,3,0,1,0,1,1,'MYPF','MyProjectFolders','MyProjectFolders')
GO

INSERT INTO [dbo].[MenuItem]
           ([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],
		   [IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
     VALUES
           (125,155 ,3,1,0,1,1,1,'MPFI','MyProjectFolders Index','MyProjectFolders')
GO

INSERT INTO [dbo].[MenuItemAccountTypeRole]
           ([MenuItem],[AccountTypeRole])
     VALUES
           (155,3),(156,3),(155,4),(156,4),(155,5),(156,5),(155,6),(156,6),(155,7),(156,7),
		   (155,8),(156,8),(155,9),(156,9),(155,10),(156,10),(155,11),(156,11),(155,12),(156,12),
		   (155,13),(156,13),(155,14),(156,14),(155,15),(156,15),(155,16),(156,16),(155,17),(156,17),
		   (155,18),(156,18),(155,19),(156,19),(155,20),(156,20),(155,21),(156,21),(155,22),(156,22),
		   (155,111),(156,111),(155,112),(156,112),(155,113),(156,113),(155,114),(156,114)
GO


INSERT INTO [dbo].[MenuItem]
           ([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],
		   [IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
     VALUES
           (126,155,0,0,0,0,0,0,'MPFN','New ProjectFolders','ProjectFolders')
GO

INSERT INTO [dbo].[MenuItemAccountTypeRole]
           ([MenuItem],[AccountTypeRole])
     VALUES
           (157,3),(157,4),(157,5),(157,6),(157,8),(157,9),(157,10),(157,11),
		   (157,13),(157,14),(157,15),(157,16),(157,18),(157,19),(157,20),(157,21)
GO

----------------------