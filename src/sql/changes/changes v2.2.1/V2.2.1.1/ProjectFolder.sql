
CREATE TABLE [dbo].[ProjectFolder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](Max) NULL,
	[Account] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[PageGrid] [int] NOT NULL,
	[Deadline] [datetime2](7) NULL,
	[IsDeleted] [bit] NULL,
	[Status] [bit] NULL
 CONSTRAINT [PK_ProjectFolder] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProjectFolder] ADD  CONSTRAINT [DF_ProjectFolder_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO


ALTER TABLE [dbo].[ProjectFolder] ADD  DEFAULT (newid()) FOR [Guid]
GO

ALTER TABLE [dbo].[ProjectFolder]  WITH CHECK ADD  CONSTRAINT [FK_ProjectFolder_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[ProjectFolder] CHECK CONSTRAINT [FK_ProjectFolder_Account]
GO

ALTER TABLE [dbo].[ProjectFolder]  WITH CHECK ADD  CONSTRAINT [FK_ProjectFolder_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[ProjectFolder] CHECK CONSTRAINT [FK_ProjectFolder_Creator]
GO

ALTER TABLE [dbo].[ProjectFolder]  WITH CHECK ADD  CONSTRAINT [FK_ProjectFolder_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO

ALTER TABLE [dbo].[ProjectFolder] CHECK CONSTRAINT [FK_ProjectFolder_Modifier]
GO


