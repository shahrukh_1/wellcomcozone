USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_GetApprovalDecisionMakers]    Script Date: 26/04/2021 09:07:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_GetApprovalDecisionMakers] (
	@P_CurrentPhase INT,
	@P_Approval INT,
	@P_PrimaryDecisionMaker INT,
	@P_ExternalPrimaryDecisionMaker INT
	
)

AS
BEGIN	
select top 1
	ISNULL([dbo].[GetAllDecisionMakers] (@P_CurrentPhase, @P_Approval, null, null), '') AS RetVal
			from Approval				 	  				   
    
END





GO


