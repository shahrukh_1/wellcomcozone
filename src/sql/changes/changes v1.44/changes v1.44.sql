USE [GMGCoZone]
GO

--Add SoftProofing Session LastAccessedDate
ALTER TABLE [dbo].[SoftProofingSessionParams] 
ADD [LastAccessedDate] DateTime NOT NULL
CONSTRAINT [LastAccessedDate] DEFAULT CURRENT_TIMESTAMP
GO

--SoftProofing Changes

ALTER TABLE [GMGCoZone].[dbo].[WorkstationCalibrationData]
  ADD DisplayProfileName NVARCHAR(64) NOT NULL DEFAULT(''),
	  DisplayWidth INT NOT NULL DEFAULT(0),
	  DisplayHeight INT NOT NULL DEFAULT(0)
GO


USE [GMGCoZone]
GO

--- Add Media Category table
CREATE TABLE [GMGCoZone].[dbo].[MediaCategory]
(
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[Key] [INT] NOT NULL,
	[Name] [NVARCHAR](50) NOT NULL
	CONSTRAINT [PK_MediaCategoryId] PRIMARY KEY CLUSTERED
	(
		[ID] ASC
    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--- Add Media Category table data
INSERT INTO [GMGCoZone].[dbo].[MediaCategory] 
	([Key], [Name])
VALUES
	(0, 'PS1: Premium coated'),
	(1, 'PS2: Improved coated'),
	(2, 'PS3: Standard coated glossy'),
	(3, 'PS4: Standard coated matte'),
	(4, 'PS5: Wood-free uncoated'),
	(5, 'PS6: Super calendered'),
	(6, 'PS7: Improved uncoated'),
	(7, 'PS8: Standard uncoated')


--- Add SoftProofing Measurements table
CREATE TABLE [GMGCoZone].[dbo].[MeasurementCondition]
(
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[KEY] [INT] NOT NULL,
	[Name] [NVARCHAR](50) NOT NULL
	CONSTRAINT [PK_MeasurementConditionsId] PRIMARY KEY CLUSTERED
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)ON [PRIMARY]

--- Add SoftProofingMeasurements table data
INSERT INTO [GMGCoZone].[dbo].[MeasurementCondition]
	([KEY], [Name])
VALUES
	(0,'M0'),
	(1,'M1'),
	(2,'M2'),
	(3,'M3')

--- Add SoftProofing Paper Tint table
CREATE TABLE [GMGCoZone].[dbo].[PaperTint](
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[AccountId] [INT] NOT NULL,
	[Name] [NVARCHAR](50) NOT NULL,
	[MediaCategory] [INT] NOT NULL,
	[Creator] [INT]  NOT NULL,
	[CreatedDate] [DATETIME] NOT NULL,
	[UpdatedDate] [DATETIME] NOT NULL,
		CONSTRAINT [FK_PaperTintAccount]  FOREIGN KEY (AccountId)
			REFERENCES [Account] (ID),
		CONSTRAINT [FK_PaperTintUser]  FOREIGN KEY (Creator)
			REFERENCES [User] (ID),
		CONSTRAINT [FK_PaperTintMediaCategory]  FOREIGN KEY (MediaCategory)
			REFERENCES [MediaCategory] (ID),
	CONSTRAINT [PK_PaperTintId] PRIMARY KEY CLUSTERED
	(
		[ID] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--- Add Paper Tint measurements table
CREATE TABLE [GMGCoZone].[dbo].[PaperTintMeasurement]
(
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[PaperTint] [INT] NOT NULL,
	[MeasurementCondition] [INT] NOT NULL,
	[L] [FLOAT] NOT NULL,
	[a] [FLOAT] NOT NULL,
	[b] [FLOAT] NOT NULL,
		CONSTRAINT [FK_PaperTintMeasurementId]  FOREIGN KEY (PaperTint)
			REFERENCES [PaperTint] (ID),
		CONSTRAINT [FK_PaperTintMeasurementCondition]  FOREIGN KEY (MeasurementCondition)
			REFERENCES [MeasurementCondition] (ID),
		CONSTRAINT [Pk_PaperTintMeasurementId] PRIMARY KEY CLUSTERED
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)ON [PRIMARY]

--- Create SimulationProfile table
CREATE TABLE [dbo].[SimulationProfile](
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[AccountId] [INT] NULL,
	[Creator] [INT] NULL,
	[ProfileName] [NVARCHAR](50) NOT NULL,
	[FileName] [NVARCHAR](50) NOT NULL,
	[Guid] [NVARCHAR](50) NOT NULL,
	[MediaCategory] [INT],
	[MeasurementCondition] [INT] NOT NULL,
	[L] [FLOAT] NULL,
	[a] [FLOAT] NULL,
	[b] [FLOAT] NULL,
	[CreatedDate] [DATETIME] NOT NULL,
	[UpdatedDate] [DATETIME] NOT NULL,
	[IsValid] bit,
	CONSTRAINT [FK_SimulationProfileAccount]  FOREIGN KEY (AccountId)
		REFERENCES [Account] (ID),
	CONSTRAINT [FK_SimulationProfileUsert]  FOREIGN KEY (Creator)
			REFERENCES [User] (ID),
	CONSTRAINT [FK_SimulationProfileMediaCategory]  FOREIGN KEY (MediaCategory)
			REFERENCES [MediaCategory] (ID),
	CONSTRAINT [FK_SimulationProfileMeasurementCondition]  FOREIGN KEY (MeasurementCondition)
			REFERENCES [MeasurementCondition] (ID),
	CONSTRAINT [PK_SimulationProfileId] PRIMARY KEY CLUSTERED
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

--Add sub menu item for Add New Paper Tint
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'SoftProofing', -- Controller - nvarchar(128)
          'AddEditPaperTint', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'SPPT' , -- Key - nvarchar(4)
          'Add Edit Paper Tint' , -- Name - nvarchar(64)
          'Add Edit Paper Tint'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'COLS'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 0
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SPPT')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

---Add sub menu item for Add New Simulation Profile
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'SoftProofing', -- Controller - nvarchar(128)
          'AddEditSimulationProfile', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'SPSP' , -- Key - nvarchar(4)
          'Add Edit Simulation Profile' , -- Name - nvarchar(64)
          'Add Edit Simulation Profile'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'COLS'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('SBSY', 'CLNT', 'DELR', 'SBSC') AND AM.[Key] = 0
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('SPSP')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO


---Create table ICCProfileCreatorSignatures
CREATE TABLE [dbo].[ICCProfileCreatorSignatures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Signature] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_ICCProfileCreatorSignatures] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( 'bICC', N'BasICColor Display' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'LOGO', N'GretagMacBeth ProfileMaker' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'MONS', N'Monaco Profiler' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'AURE', N'Aurelon' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'HDM', N'ColorOpen' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'HDPP', N'Heidelberg ColorOpen' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'APPL', N'Apple Computer, Inc.' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'ERGO', N'ErgoSoft' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'CREO', N'Creo-Scitex' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'CoLg', N'ColorLogic' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'PANT', N'Pantone' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'ADBE', N'Adobe Systems Incorporated' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'Eizo', N'EIZO NANAO CORPORATION' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'NEC', N'NEC Corporation' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'XRIT', N'X-Rite' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'CMiX', N'CHROMiX' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'ESKO', N'Esko-Graphics' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'GMB', N'Gretagmacbeth' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'GMG', N'GMG GmbH & Co. KG' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'GTMB', N'Gretagmacbeth' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'HDM', N'Heidelberger Druckmaschinen AG' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'KNCA', N'Konica Corporation' )
INSERT  [dbo].[ICCProfileCreatorSignatures]
        ( [Signature], [Name] )
VALUES  ( N'MSFT', N'Microsoft Corporation' )
GO

--- Add Version Sufix column to the Approval table 
ALTER TABLE [dbo].[Approval]
ADD [VersionSufix] NVARCHAR(36) NULL
GO


--- Update StoredProcedure [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

-- Alter Procedure by adding VersionSufix, replacing approval id with id from temp table and changing error handling

ALTER PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			INNER JOIN Job j 
					ON j.ID = a.Job
			INNER JOIN JobStatus js 
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)									
	WHERE	j.Account = @P_Account					
			AND a.IsDeleted = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND		auvi.[User] = @P_User
			AND ((a.CurrentPhase IS NULL AND(	SELECT MAX([ViewedDate]) 
												FROM ApprovalUserViewInfo vuvi
													INNER JOIN Approval av
														ON  av.ID = vuvi.[Approval]
													INNER JOIN Job vj
														ON vj.ID = av.Job	
												WHERE  av.Job = a.Job
													AND av.IsDeleted = 0
											) = auvi.[ViewedDate]
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
													)
			)						
		   OR (a.CurrentPhase IS NOT NULL  AND a.CurrentPhase = (SELECT av.CurrentPhase 
																  FROM ApprovalUserViewInfo avi
																  INNER JOIN Approval av ON av.ID =  avi.Approval
																  WHERE av.Job = a.Job
																  AND avi.[ViewedDate] = auvi.[ViewedDate]
																		AND avi.[ViewedDate] =( SELECT MAX([ViewedDate]) 
																								FROM ApprovalUserViewInfo vuvi
																								INNER JOIN Approval avv
																									ON  avv.ID = vuvi.[Approval]
																								INNER JOIN Job vj
																									ON vj.ID = avv.Job	
																								WHERE  avv.Job = a.Job
																								AND avv.IsDeleted = 0)
																		AND (EXISTS(
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Phase = av.CurrentPhase
																				  )
																				  OR ISNULL(j.JobOwner, 0) = @P_User
																			)
																		 
																)
				)
			)
	ORDER BY 
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE						
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
		END DESC,		
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT DISTINCT	t.ID,
			a.ID AS Approval,	
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],							 
			js.[Key] AS [Status],	
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			a.[Version] AS MaxVersion,						
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
				FROM Approval av
				WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.Approval aproval ON ap.Approval = aproval.ID
							 WHERE aproval.ID = t.ApprovalID AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = aproval.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																														WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase )
																												)
																			           )
																					   
															       )	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,			
			(SELECT CASE 
				WHEN a.LockProofWhenAllDecisionsMade = 1
					THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
				ELSE
					NULL
				END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0) 
				THEN(						     
						(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
						   THEN
								(SELECT TOP 1 appcd.[Key]
									FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
											JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
											WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
											ORDER BY ad.[Priority] ASC
										) appcd
								)
							ELSE
							(							    
								(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
											                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
								 THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
								     )
								 ELSE '0'
								 END 
								 )   			   
							)
							END
						)								
					)		
				WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
				  THEN
					(SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				ELSE
				 (SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				END	
				), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			JOIN @TempItems t 
					ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
			INNER JOIN Job j
					ON j.ID = a.Job
			INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	  
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
END
GO


--- Updated  View [dbo].[ReturnApprovalsPageView]  by adding VersionSufix column
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Remove ischangescomplete
ALTER VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			'' AS [Status],
			'' AS [Guid],
			'' AS [FileName],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			'' AS PrimaryDecisionMakerName,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			CONVERT(bit, 0) AS HasAnnotations,
			'' AS Decision,
			CONVERT(bit, 0) AS IsCollaborator,
			CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,
			CONVERT(DateTime, GETDATE()) AS ApprovalApprovedDate,
			0 AS CurrentPhase,
			'' AS PhaseName,
			'' AS WorkflowName,
			'' AS NextPhase,
			'' AS DecisionMakers,
			0 AS DecisionType,
			'' AS OwnerName,
			CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
			NULL AS JobOwner,
			CONVERT(bit, 0) AS IsPhaseComplete,
			'' AS [VersionSufix]




GO


--- Updated  StoredProcedure [dbo].[SPC_ReturnArchivedApprovalInfo] 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

-- Alter Procedure by adding VersionSufix, replacing approval id with id from temp table and changing error handling

ALTER PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID			
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] = 'ARC'
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND EXISTS (
									SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
								)
			) = a.[Version])			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																				SELECT TOP 1 ac.ID 
																				FROM ApprovalCollaborator ac 
																				WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																			  )
																	)
																						 		
								      )
				)
			)	
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,			
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 WHERE ap.Approval = t.ApprovalID			 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,							
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t 
				ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	 
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
	 
END
GO


--- Update  StoredProcedure [dbo].[SPC_ReturnRecycleBinPageInfo]
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

-- Alter Procedure by adding VersionSufix, replacing approval id with id from temp table and changing error handling

ALTER PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,
	@P_ViewAll bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						)
					AND (
						 @P_ViewAll = 1 
						 OR
						 ( 
							@P_ViewAll = 0 AND((a.CurrentPhase IS NULL AND	EXISTS (
																					 SELECT TOP 1 ac.ID 
																					 FROM ApprovalCollaborator ac 
																					 WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
																					))
										  OR (a.CurrentPhase IS NOT NULL AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																												SELECT TOP 1 ac.ID 
																												FROM ApprovalCollaborator ac 
																												WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator AND ac.Phase = a.CurrentPhase
																											  )
											                                  )
											   )
											  )
					     )	
					    )	 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase,
					GetDate() AS Deadline
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
					END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					t.PrimaryDecisionMakerName,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND acd.Phase = a.CurrentPhase
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,			
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'' AS PrimaryDecisionMakerName,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			JOIN Approval a ON fa.Approval = a.ID	
	                    WHERE fa.Folder = f.ID AND a.DeletePermanently = 0
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					NULL as ApprovalApprovedDate,
					0 AS CurrentPhase,
					'' AS PhaseName,
					'' AS WorkflowName,
					'' AS NextPhase,
					'' AS DecisionMakers,
					0 AS DecisionType,
					'' AS OwnerName,
					CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,			
					NULL AS JobOwner,
					CONVERT(bit, 0) AS IsPhaseComplete,
					'' AS VersionSufix
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop

END
GO

--- Update  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

--Alter Procedure by adding VersionSufix, replacing approval id with id from temp table and changing error handling
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(50),
	   PrimaryDecisionMakerName nvarchar(50)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)						
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL and
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0								
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
								(@P_TopLinkId = 1 
										AND EXISTS(
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												  )
									)									
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
							  )
							)
						)		
			) = a.[Version]	)
			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (
														 @P_ViewAll = 1 
													 OR
													 ( 
														@P_ViewAll = 0 AND 
														(
															(@P_TopLinkId = 1 AND (@P_User = j.JobOwner OR EXISTS(
																													SELECT TOP 1 ac.ID 
																													FROM ApprovalCollaborator ac 
																													WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																												  )
																					)
															 )									
															OR 
																(@P_TopLinkId = 2 
																	AND @P_User = ISNULL(j.JobOwner,0)
																)
															OR 
																(@P_TopLinkId = 3 
																	AND ISNULL(j.JobOwner,0) != @P_User
																	AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																				)
																)
														  )
														)
													)										 		
								      )
				)
			)
	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
					WHEN ( EXISTS (SELECT ANNOTATION					
							FROM dbo.ApprovalPage ap							
							CROSS APPLY (SELECT TOP 1 aa.ID AS ANNOTATION FROM dbo.ApprovalAnnotation aa WHERE aa.Page = ap.ID 
							AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = a.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																													  WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase 
																													  )
																				)
													  )
																					   
								)	 
							) ANN (ANNOTATION)						
							WHERE ap.Approval = t.ApprovalID 
						 )
					) 
					THEN CONVERT(bit,1)
					ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,						
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = t.ApprovalID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = t.ApprovalID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID

	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop

END
GO

--- Update  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]  
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

-- Alter Procedure by adding VersionSufix, replacing approval id with id from temp table and changing error handling

ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(50),
	   PrimaryDecisionMakerName varchar(50)	   
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.Deadline,
					a.CreatedDate,
					a.ModifiedDate,
					js.[Key] AS [Status],
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					LEFT OUTER JOIN FolderApproval fa
						ON fa.Approval = a.ID
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC'
					AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((a.CurrentPhase IS NULL 
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND EXISTS (
											SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
										)
					) = a.[Version])			
					OR (a.CurrentPhase IS NOT NULL 
						AND a.CurrentPhase = (SELECT avv.CurrentPhase 
											  FROM approval avv 
											  WHERE avv.Job = a.Job 
													AND a.[Version] = avv.[Version]
													AND avv.[Version] = (SELECT MAX([Version])
																		 FROM Approval av 
																		 WHERE av.Job = a.Job AND av.IsDeleted = 0)

													AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																						SELECT TOP 1 ac.ID 
																						FROM ApprovalCollaborator ac 
																						WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																					  )
																			)
																								 		
											  )
						)
					)	
					AND	@P_FolderId = fa.Folder
				
			UNION
			
			SELECT 	f.ID AS Approval,
					GetDate() AS Deadline,
					f.CreatedDate,
					f.ModifiedDate,
					'' AS [Status],
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase
			FROM	Folder f
			WHERE	f.Account = @P_Account
					AND (@P_Status = 0)
					AND f.IsDeleted = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
					AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN a.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN a.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix
		FROM (
				SELECT  t.ID AS ID,
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						a.[Guid],
						a.[FileName],
						js.[Key] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
								     WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
						END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						t.PrimaryDecisionMakerName,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						at.[Key] AS ApprovalType,
						a.[Version] AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT CASE
						 WHEN ( EXISTS (SELECT aa.ID					
										 FROM dbo.ApprovalAnnotation aa
										 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
										 WHERE ap.Approval = t.ApprovalID			 
										 )
								) THEN CONVERT(bit,1)
								  ELSE CONVERT(bit,0)
						 END)  AS HasAnnotations,				
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
													                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
														JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
														WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
										JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				CONVERT(bit, 1) AS IsCollaborator,
				CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,				
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
				t.PhaseName,
				ISNULL((SELECT CASE
							WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
							ELSE ''
						END),'') AS WorkflowName,
				ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
					  ORDER BY ap.Position ),'') AS NextPhase,
				ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
				ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
				(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
							 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
							 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
							 END) AS OwnerName,
				a.AllowOtherUsersToBeAdded,
				j.JobOwner,
				(SELECT CASE WHEN ISNULL(a.CurrentPhase, 0) <> 0
						 THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.Collaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN dbo.ApprovalType at
							ON 	at.ID = a.[Type]
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID	
						JOIN @TempItems t 
							ON t.ApprovalID = a.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 1
						
				UNION
				
				SELECT 	t.ID AS ID,
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						'' AS [Guid],
						'' AS [FileName],
						'' AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'' AS PrimaryDecisionMakerName,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS ApprovalType,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						CONVERT(bit, 0) AS HasAnnotations,
						'' AS Decision,
						CONVERT(bit, 1) AS IsCollaborator,
						f.AllCollaboratorsDecisionRequired,
						NULL as ApprovalApprovedDate,
						0 AS CurrentPhase,
						'' AS PhaseName,
						'' AS WorkflowName,
						'' AS NextPhase,
						'' AS DecisionMakers,
						0 AS DecisionType,
						'' AS OwnerName,
						CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
						NULL AS JobOwner,
						CONVERT(bit, 0) AS IsPhaseComplete,
						'' AS VersionSufix
				FROM	Folder f
						JOIN @TempItems t 
							ON t.ApprovalID = f.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 0
			) result		
	ORDER BY result.ID	

	SET ROWCOUNT 0

	---- Send the total rows count ---- 
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
						AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND EXISTS (
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END

GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalSeparationPlate]
ADD RGBHex NVARCHAR(12)
GO


--- Add Viewing Conditions table
CREATE TABLE [GMGCoZone].[dbo].[ViewingCondition]
(
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[PaperTint] [INT] NULL,
	[SimulationProfile] [INT] NOT NULL,
		CONSTRAINT [FK_PaperTint_ViewingCondition]  FOREIGN KEY (PaperTint)
			REFERENCES [PaperTint] (ID),
		CONSTRAINT [FK_SimulationProfile_ViewingCondition]  FOREIGN KEY (SimulationProfile)
			REFERENCES [SimulationProfile] (ID),
		CONSTRAINT [Pk_ViewingCondition] PRIMARY KEY CLUSTERED
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)ON [PRIMARY]
GO

--- Add Viewing Conditions table
CREATE TABLE [GMGCoZone].[dbo].[ApprovalJobViewingCondition]
(
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[Job] [INT] NOT NULL,
	[ViewingCondition] [INT] NOT NULL,
		CONSTRAINT [FK_ApprovalJobViewingCondition_Job]  FOREIGN KEY (Job)
			REFERENCES [Job] (ID),
		CONSTRAINT [FK_ApprovalJobViewingCondition_ViewingCondition]  FOREIGN KEY (ViewingCondition)
			REFERENCES [ViewingCondition] (ID),
		CONSTRAINT [Pk_ApprovalJobViewingCondition] PRIMARY KEY CLUSTERED
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)ON [PRIMARY]
GO


--tag annotations with viewing condition
ALTER TABLE dbo.ApprovalAnnotation
ADD ViewingCondition INT,
CONSTRAINT [FK_ApprovalAnnotation_ViewingCondition] FOREIGN KEY(ViewingCondition) REFERENCES [ViewingCondition](ID)
GO

ALTER TABLE [GMGCoZone].[dbo].[SimulationProfile]
ADD [IsDeleted] [BIT] NOT NULL DEFAULT 0
GO

ALTER TABLE dbo.SoftProofingSessionParams
ADD ViewingCondition INT,
CONSTRAINT [FK_SoftProofingSessionParams_ViewingCondition]  FOREIGN KEY (ViewingCondition)
			REFERENCES [ViewingCondition] (ID)
GO


 ALTER TABLE [GMGCoZone].[dbo].[ApprovalEmbeddedProfile]
  ADD L FLOAT,
  a FLOAT,
  b FLOAT,
  PrintSubstrate INT,
  CONSTRAINT [FK_ApprovalEmbeddedProfile_PrintSubstrate]  FOREIGN KEY (PrintSubstrate)
			REFERENCES [MediaCategory] (ID)

GO

 ALTER TABLE [GMGCoZone].[dbo].[AccountCustomICCProfile]
  ADD L FLOAT,
  a FLOAT,
  b FLOAT,
  PrintSubstrate INT,
  CONSTRAINT [FK_AccountCustomICCProfile_PrintSubstrate]  FOREIGN KEY (PrintSubstrate)
			REFERENCES [MediaCategory] (ID)
			
GO

ALTER TABLE dbo.ViewingCondition
ALTER COLUMN SimulationProfile INT
GO

ALTER TABLE dbo.ViewingCondition
ADD EmbeddedProfile INT,
    CustomProfile INT,
    CONSTRAINT [FK_ViewingCondition_EmbeddedProfile]  FOREIGN KEY (EmbeddedProfile)
			REFERENCES [ApprovalEmbeddedProfile] (ID),
	 CONSTRAINT [FK_ViewingCondition_CustomProfile]  FOREIGN KEY (CustomProfile)
	REFERENCES [AccountCustomICCProfile] (ID)
GO

  ALTER TABLE dbo.SoftProofingSessionParams
ADD DefaultSession BIT NOT NULL DEFAULT(0)
GO

--- Add ExcludedAccountDefaultProfiles table
CREATE TABLE [GMGCoZone].[dbo].[ExcludedAccountDefaultProfiles]
(
	[ID] [INT] IDENTITY(1,1) NOT NULL,
	[Account] [INT] NOT NULL,
	[SimulationProfile] [INT] NOT NULL,
		CONSTRAINT [FK_ExcludedAccountDefaultProfiles_Account]  FOREIGN KEY (Account)
			REFERENCES [Account] (ID),
		CONSTRAINT [FK_ExcludedAccountDefaultProfiles_SimulationProfile]  FOREIGN KEY (SimulationProfile)
			REFERENCES [SimulationProfile] (ID),
		CONSTRAINT [Pk_ExcludedAccountDefaultProfiles] PRIMARY KEY CLUSTERED
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)ON [PRIMARY]
GO

ALTER PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	SET XACT_ABORT ON;  
	BEGIN TRANSACTION
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		 --Delete annotations from ApprovalAnnotationStatusChangeLog
		DELETE [dbo].[ApprovalAnnotationStatusChangeLog]		 
		FROM   [dbo].[ApprovalAnnotationStatusChangeLog] aascl
				JOIN [dbo].[ApprovalAnnotation] aa
					ON aascl.Annotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id	    
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval]
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalJobPhaseApproval
		DELETE FROM [dbo].[ApprovalJobPhaseApproval]	
				WHERE Approval = @P_Id	
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[ApprovalSetting] 
				WHERE Approval = @P_Id
				
		-- Delete from ApprovalCustomICC
		DELETE FROM [dbo].[ApprovalCustomICCProfile] 
				WHERE Approval = @P_Id
		
		-- Delete from LabColorMeasurements
		DELETE [dbo].[LabColorMeasurement] 
		FROM	[dbo].[LabColorMeasurement] lbm
				JOIN [dbo].[ApprovalSeparationPlate] asp
					ON lbm.ApprovalSeparationPlate = asp.ID
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		--Delete from SoftProofing Session Error
		DELETE [dbo].[SoftProofingSessionError]
		FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
		WHERE ap.Approval = @P_Id
				
		--Delete from ApprovalEmbedded Profile foreign keys to approval
		DELETE [dbo].[ApprovalEmbeddedProfile]
		FROM [dbo].[ApprovalEmbeddedProfile] apep			
		WHERE apep.Approval = @P_Id			
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalUserRecycleBinHistory
		DELETE FROM [dbo].[ApprovalUserRecycleBinHistory]
		WHERE Approval = @P_Id
		
		-- Delete from UserApprovalReminderEmailLog
		DELETE FROM [dbo].[UserApprovalReminderEmailLog]
		WHERE Approval = @P_Id
		
		-- Delete from SoftProofingSessionParams
		DELETE FROM [dbo].[SoftProofingSessionParams]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
			
				-- Delete all phases related with this job
				DELETE [dbo].[ApprovalJobPhase] 
				FROM [dbo].[ApprovalJobPhase] ajp
					JOIN [dbo].[ApprovalJobWorkflow] ajw ON ajp.[ApprovalJobWorkflow] = ajw.ID
				WHERE ajw.Job = @Job_ID
				
				-- Delete all approval workflows related with this job
				DELETE FROM [dbo].[ApprovalJobWorkflow]
				WHERE Job = @Job_ID
				
				-- Delete all job status change logs
				DELETE FROM [dbo].[JobStatusChangeLog]
				WHERE Job = @Job_ID
				
				--Delete job viewing condition
				DELETE FROM [dbo].[ApprovalJobViewingCondition]
				WHERE Job = @Job_ID
			
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
	    COMMIT TRANSACTION
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		IF (XACT_STATE()) = -1 
		BEGIN 
			SET @P_Success = ERROR_MESSAGE()
			ROLLBACK TRANSACTION;  
		END
		
		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO

--- Update BillingPlan table
ALTER TABLE [GMGCoZone].[dbo].[BillingPlan]
ADD 
	[MaxUsers] [INT] NULL,
    [MaxStorageInGb] [INT] NULL
GO

--- Add Account Storage Usage table
CREATE TABLE [GMGCoZone].[dbo].[AccountStorageUsage]
(
	[ID] [INT] IDENTITY(1, 1) NOT NULL,
	[Account] [INT] NOT NULL,
	[UsedStorage] [FLOAT] NOT NULL,
		CONSTRAINT [FK_AccountStorageUsage_Account] FOREIGN KEY ([Account])
			REFERENCES [Account](ID),
		CONSTRAINT [PK_AccountStorageUsage_Id] PRIMARY KEY CLUSTERED
		(
			[ID] ASC
		)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
)ON [PRIMARY]
GO

---Add sub menu item for Add New  Collaborate Billing Plan
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Plans', -- Controller - nvarchar(128)
          'AddEditNewCollaborateBillingPlan', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          0 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'PLNC' , -- Key - nvarchar(4)
          'Add Edit New Collaborate Billing Plan' , -- Name - nvarchar(64)
          'Add Edit New Collaborate Billing Plan'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'PLIN'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'HQA') AND AT.[Key] in ('GMHQ') AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('PLNC')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

ALTER TABLE [dbo].[Approval]
ADD RestrictedZoom BIT NOT NULL DEFAULT(0)
GO

-- Print Substrate/Sim Profile SysAdmin Dabatase Authorization
INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT NULL , -- ControllerAction - int
          mi.ID , -- Parent - int
          7 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'GSSP' , -- Key - nvarchar(4)
          'Global Settings - SoftProofing' , -- Name - nvarchar(64)
          'Global Settings - SoftProofing'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'GSET'
GO

-- Upload Engine Templates Dabatase Authorization
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'SoftProofing', -- Controller - nvarchar(128)
          'PrintSubstrates', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          1 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'GSPS' , -- Key - nvarchar(4)
          'Print Substrates' , -- Name - nvarchar(64)
          'Print Substrates'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'GSSP'

		
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'SoftProofing', -- Controller - nvarchar(128)
          'DefaultSimulationProfiles', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          NULL , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          1 , -- IsVisible - bit
          'GSSM' , -- Key - nvarchar(4)
          'Default Simulation Profile' , -- Name - nvarchar(64)
          'Default Simulation Profile'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'GSSP'
		
--AddEdit Print Substrate Action		
INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'SoftProofing', -- Controller - nvarchar(128)
          'AddEditPrintSubstrate', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'PSAE' , -- Key - nvarchar(4)
          'AddEditPrintSubstrate' , -- Name - nvarchar(64)
          'AddEditPrintSubstrate'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'GSSP'		
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE R.[Key] IN ('HQA', 'HQM') AND AT.[Key] in ('GMHQ') AND AM.[Key] = 4
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('GSSM', 'GSPS', 'PSAE', 'SPSP')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor

GO

ALTER TABLE [dbo].[MediaCategory]
DROP COLUMN [Key]
GO

--- Create ReturnApprovalInTree StoredProcedure
USE [GMGCoZone]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsInTree]
	@P_UserKey NVARCHAR(36),
	@P_FolderID INT,
	@P_ApprovalType INT,
	@P_PageNumber INT,
	@P_MaxNumberOfRows INT = 50
AS
BEGIN
	SET ARITHABORT ON
	DECLARE @userKey NVARCHAR(36)
	DECLARE @folderID INT
	DECLARE @approvalType INT
	DECLARE @userID INT
	DECLARE @startOffset INT
	DECLARE @pageNr INT
	DECLARE @validApprovals TABLE(
		[ID] INT NOT NULL,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Version] INT,
		[Job] INT,
		[CreatedDate] DATETIME2(7),
		[OwnerID] INT
	);
	DECLARE @approvals TABLE(
		[ID] INT NOT NULL,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Version] INT,
		[Job] INT,
		[CreatedDate] DATETIME2(7),
		[OwnerID] INT
	);
	DECLARE @tempData TABLE(
	    ID int IDENTITY PRIMARY KEY,
		[ApprovalID] INT,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Region] NVARCHAR(128),
		[Domain] NVARCHAR(255),
		[ApprovalOwner] NVARCHAR(255),
		[FolderID] INT,
		[CreatedDate] DATETIME2(7)
	);
	
	SET @userKey = @P_UserKey;
	SET @folderID = @P_FolderID;
	SET @approvalType = @P_ApprovalType;
	SET @pageNr = @P_PageNumber;
	SET @startOffset = (@pageNr - 1) * @P_MaxNumberOfRows;
	
	SELECT @userID = u.ID FROM [User] as u WHERE u.Guid = @userKey;
	
	INSERT INTO @validApprovals
	SELECT 
		a.[ID],
		a.[FileName],
		a.[Guid],
		a.[Version],
		a.[Job],
		a.[CreatedDate],
		a.[Owner]
	FROM GMGCoZone.dbo.[Approval] AS a
	JOIN GMGCoZone.dbo.[ApprovalType] AS apt ON a.Type = apt.ID
	WHERE apt.[Key] != @approvalType AND a.DeletePermanently = 0 AND a.IsDeleted = 0;

	INSERT INTO @approvals
	SELECT 
		va.[ID],
		va.[FileName],
		va.[Guid],
		va.[Version],
		va.[Job],
		va.[CreatedDate],
		va.[OwnerID]
	FROM @validApprovals AS va 
	INNER JOIN 
	(SELECT a.Job, MAX(a.Version) as vr
		FROM @validApprovals as a
		GROUP By a.Job
	) maxVa ON va.Job = maxVa.Job AND va.Version = maxVa.vr;
	
	INSERT INTO @tempData
	SELECT 
		DISTINCT a.[ID] as ApprovalID,
		a.[FileName],
		a.[Guid],
		acc.[Region],
		acc.[Domain],
		u.[GivenName] + ' ' + u.[FamilyName] as ApprovalOwner,
		ISNULL(fa.[Folder], 0) as FolderID,
		a.[CreatedDate]
	FROM GMGCoZone.dbo.[ApprovalCollaborator] AS ac 
	INNER JOIN @approvals AS a ON ac.[Approval] = a.[ID]
	JOIN GMGCoZone.dbo.[User] AS u ON u.ID = a.OwnerID
	JOIN GMGCoZone.dbo.[Account] AS acc ON u.[Account] = acc.[ID]
	LEFT JOIN GMGCoZone.dbo.[FolderApproval] AS fa ON a.[ID] = fa.[Approval]	
	WHERE ac.[Collaborator] = @userID
	ORDER BY a.[CreatedDate] DESC
	
	IF(@folderID = 0)
		IF(@P_PageNumber = 0)
			BEGIN
				SELECT 
					apr.[ApprovalID],
					apr.[FileName],
					apr.[Guid],
					apr.[Region],
					apr.[Domain],
					apr.[ApprovalOwner],
					apr.[FolderID]
				FROM @tempData as apr
			END
		ELSE
			BEGIN
				SET ROWCOUNT @P_MaxNumberOfRows
				SELECT 
					apr.[ApprovalID],
					apr.[FileName],
					apr.[Guid],
					apr.[Region],
					apr.[Domain],
					apr.[ApprovalOwner],
					apr.[FolderID]
				FROM @tempData as apr WHERE apr.ID > @startOffset
			END
	ELSE
		IF(@P_PageNumber = 0)
			BEGIN
				SELECT  
					apr.[ApprovalID],
					apr.[FileName],
					apr.[Guid],
					apr.[Region],
					apr.[Domain],
					apr.[ApprovalOwner],
					apr.[FolderID]
				FROM @tempData AS apr
				JOIN FolderApproval AS fa ON apr.[ApprovalID] = fa.Approval
				WHERE fa.Folder = @folderID
			END
		ELSE
			BEGIN
				SET ROWCOUNT @P_MaxNumberOfRows
				SELECT  
					apr.[ApprovalID],
					apr.[FileName],
					apr.[Guid],
					apr.[Region],
					apr.[Domain],
					apr.[ApprovalOwner],
					apr.[FolderID]
				FROM @tempData AS apr
				JOIN FolderApproval AS fa ON apr.[ApprovalID] = fa.Approval
				WHERE fa.Folder = @folderID AND apr.ID > @startOffset
			END
END
GO

--- Create ReturnApprovalInTree View

USE [GMGCoZone]
GO

/****** Object:  View [dbo].[ReturnApprovalInTree]    Script Date: 06/17/2016 16:42:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


-----------------------------------------------------------------------------------
-- Alter Return Account Parameters view
CREATE VIEW [dbo].[ReturnApprovalInTreeView] 
AS 
SELECT  0 AS [ApprovalID],
'' AS [FileName],
'' AS [Guid],
'' AS [Region],
'' AS [Domain],
'' AS [ApprovalOwner],
0 AS FolderID

GO

USE [GMGCoZone]
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsInTree]    Script Date: 6/24/2016 4:34:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsInTree]
	@P_UserKey NVARCHAR(36),
	@P_FolderID INT,
	@P_ApprovalType INT,
	@P_PageNumber INT,
	@P_MaxNumberOfRows INT = 50
AS
BEGIN
	SET ARITHABORT ON
	DECLARE @userKey NVARCHAR(36)
	DECLARE @folderID INT
	DECLARE @approvalType INT
	DECLARE @userID INT
	DECLARE @startOffset INT
	DECLARE @pageNr INT
	DECLARE @validApprovals TABLE(
		[ID] INT NOT NULL,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Version] INT,
		[Job] INT,
		[CreatedDate] DATETIME2(7),
		[OwnerID] INT
	);
	DECLARE @approvals TABLE(
		[ID] INT NOT NULL,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Version] INT,
		[Job] INT,
		[CreatedDate] DATETIME2(7),
		[OwnerID] INT
	);
	DECLARE @tempData TABLE(
	    ID int IDENTITY PRIMARY KEY,
		[ApprovalID] INT,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Region] NVARCHAR(128),
		[Domain] NVARCHAR(255),
		[ApprovalOwner] NVARCHAR(255),
		[FolderID] INT,
		[CreatedDate] DATETIME2(7)
	);
	
	SET @userKey = @P_UserKey;
	SET @folderID = @P_FolderID;
	SET @approvalType = @P_ApprovalType;
	SET @pageNr = @P_PageNumber;
	SET @startOffset = (@pageNr - 1) * @P_MaxNumberOfRows;
	
	SELECT @userID = u.ID FROM [User] as u WHERE u.Guid = @userKey;
	
	INSERT INTO @validApprovals
	SELECT 
		a.[ID],
		a.[FileName],
		a.[Guid],
		a.[Version],
		a.[Job],
		a.[CreatedDate],
		a.[Owner]
	FROM GMGCoZone.dbo.[Approval] AS a
	JOIN GMGCoZone.dbo.[ApprovalType] AS apt ON a.Type = apt.ID
	WHERE apt.[Key] != @approvalType AND a.DeletePermanently = 0 AND a.IsDeleted = 0;

	INSERT INTO @approvals
	SELECT 
		va.[ID],
		va.[FileName],
		va.[Guid],
		va.[Version],
		va.[Job],
		va.[CreatedDate],
		va.[OwnerID]
	FROM @validApprovals AS va 
	INNER JOIN 
	(SELECT a.Job, MAX(a.Version) as vr
		FROM @validApprovals as a
		GROUP By a.Job
	) maxVa ON va.Job = maxVa.Job AND va.Version = maxVa.vr;
	
	IF(@folderID = 0)
		BEGIN
			INSERT INTO @tempData
			SELECT 
				DISTINCT a.[ID] as ApprovalID,
				a.[FileName],
				a.[Guid],
				acc.[Region],
				acc.[Domain],
				u.[GivenName] + " " + u.[FamilyName] as ApprovalOwner,
				ISNULL(fa.[Folder], 0) as FolderID,
				a.[CreatedDate]
			FROM GMGCoZone.dbo.[ApprovalCollaborator] AS ac 
			INNER JOIN @approvals AS a ON ac.[Approval] = a.[ID]
			JOIN GMGCoZone.dbo.[User] AS u ON u.ID = a.OwnerID
			JOIN GMGCoZone.dbo.[Account] AS acc ON u.[Account] = acc.[ID]
			LEFT JOIN GMGCoZone.dbo.[FolderApproval] AS fa ON a.[ID] = fa.[Approval]	
			WHERE ac.[Collaborator] = @userID
			ORDER BY a.[CreatedDate] DESC

			IF(@P_PageNumber = 0)
				BEGIN
					SELECT 
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData as apr
				END
			ELSE
				BEGIN
					SET ROWCOUNT @P_MaxNumberOfRows
					SELECT 
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData as apr WHERE apr.ID > @startOffset
				END
		END
	ELSE
		BEGIN
			INSERT INTO @tempData
			SELECT 
				DISTINCT a.[ID] as ApprovalID,
				a.[FileName],
				a.[Guid],
				acc.[Region],
				acc.[Domain],
				u.[GivenName] + " " + u.[FamilyName] as ApprovalOwner,
				ISNULL(fa.[Folder], 0) as FolderID,
				a.[CreatedDate]
			FROM GMGCoZone.dbo.[ApprovalCollaborator] AS ac 
			INNER JOIN @approvals AS a ON ac.[Approval] = a.[ID]
			JOIN GMGCoZone.dbo.[User] AS u ON u.ID = a.OwnerID
			JOIN GMGCoZone.dbo.[Account] AS acc ON u.[Account] = acc.[ID]
			LEFT JOIN GMGCoZone.dbo.[FolderApproval] AS fa ON a.[ID] = fa.[Approval]	
			WHERE ac.[Collaborator] = @userID AND fa.Folder = @folderID
			ORDER BY a.[CreatedDate] DESC
			
			IF(@P_PageNumber = 0)
				BEGIN
					SELECT  
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData AS apr							
				END
			ELSE
				BEGIN
					SET ROWCOUNT @P_MaxNumberOfRows
					SELECT  
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData AS apr
					WHERE apr.ID > @startOffset
				END
		END
END
--------------------------------------------------------------------Lauched on TESTEU
--------------------------------------------------------------------Launched on STAGE
--------------------------------------------------------------------Lauched on PRODUCTION