USE GMGCoZone;
GO

SELECT 
	  ap.Guid AS ApprovalGuid,	
      spm.[SessionGuid]
     
  FROM [GMGCoZone].[dbo].[SoftProofingSessionParams] spm
  JOIN dbo.Approval ap ON spm.Approval = ap.ID
  
  WHERE spm.OutputProfileName IN (
   'CoatedFOGRA27.icc',
    'CoatedFOGRA39.icc',
  'JapanColor2003WebCoated.icc',
   'JapanColor2002Newspaper.icc',
    'JapanColor2001Coated.icc',
    'JapanColor2001Uncoated.icc',
    'JapanWebCoated.icc',
     'USWebCoatedSWOP.icc',
     'UncoatedFOGRA29.icc',
     'USWebUncoated.icc',
     'WebCoatedFOGRA28.icc')