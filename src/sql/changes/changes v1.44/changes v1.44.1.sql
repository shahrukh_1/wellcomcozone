INSERT INTO [GMGCoZone].[dbo].[UploadEngineDefaultXMLTemplate]
           ([Key]
           ,[NodeName]
           ,[XPath])
     VALUES
           (20
           ,'LockWhenFirstDecisionHasBeenMade'
           ,'/CoZoneJob/Collaborate/LockWhenFirstDecisionHasBeenMade')
GO

-- Add missing column to SoftProofingSessionParams
ALTER TABLE [GMGCoZone].[dbo].[SoftProofingSessionParams]
ADD [LastAccessedDate] [DATETIME] NULL
GO

ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        
         DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN dbo.AccountStatus ast ON a.[Status] = ast.ID 
                WHERE	a.ID IN (SELECT ID FROM dbo.[GetChildAccounts](@P_LoggedAccount))
						AND a.ID != 1
                        AND a.IsTemporary = 0
						AND (ast.[Key] = 'A')
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR ( @P_BillingPlanList != '' AND (
																 EXISTS (SELECT    val
																		 FROM      dbo.splitString(@P_BillingPlanList, ',')
																		 where val in (
																					   SELECT bp.SelectedBillingPlan from dbo.AccountSubscriptionPlan bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   UNION
																					   SELECT bp.SelectedBillingPlan from dbo.SubscriberPlanInfo bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   )
																		)																 
																)
								 )
                            )

               
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateContractStartDate DATETIME,
              CollaborateIsMonthlyBillingFrequency BIT,              
              CollaborateCycleDetails NVARCHAR(MAX) ,
              CollaborateModuleId INT ,
              CollaborateIsQuotaAllocationEnabled BIT,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverContractStartDate DATETIME,
              DeliverIsMonthlyBillingFrequency BIT,
              DeliverCycleDetails NVARCHAR(MAX) ,
              DeliverModuleId INT ,
              DeliverIsQuotaAllocationEnabled BIT
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateContractStartDate,
                  CollaborateIsMonthlyBillingFrequency,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  CollaborateIsQuotaAllocationEnabled,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverContractStartDate,
                  DeliverIsMonthlyBillingFrequency,
                  DeliverCycleDetails ,
                  DeliverModuleId ,
                  DeliverIsQuotaAllocationEnabled
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.ContractStartDate
                             ELSE CASPBP.ContractStartDate
                        END AS CollaborateContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(CASPBP.IsMonthlyBillingFrequency, 0)
                        END AS CollaborateIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CSPIBP.ModuleId,
                                                              default)
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              CASPBP.ModuleId,
                                                              default)
                               END, '0|0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.IsQuotaAllocationEnabled
                             ELSE CASPBP.IsQuotaAllocationEnabled
                        END AS [CollaborateIsQuotaAllocationEnabled],
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.ContractStartDate
                             ELSE DASPBP.ContractStartDate
                        END AS DeliverContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(DASPBP.IsMonthlyBillingFrequency, 0)
                        END AS DeliverIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DASPBP.ModuleId,
                                                              default)
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN dbo.GetBillingCycles(A.ID,
															  AT.[Key],
                                                              @P_LoggedAccount,
                                                              @P_StartDate,
                                                              @P_EndDate,
                                                              DSPIBP.ModuleId,
                                                              default)
                               END, '0|0|0|0"0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.IsQuotaAllocationEnabled
                             ELSE DASPBP.IsQuotaAllocationEnabled
                        END AS [DeliverIsQuotaAllocationEnabled]
                        
                FROM    dbo.Account A
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency],[IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
 
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
    
                        WHERE  ( A.ID IN ( SELECT  *
									FROM    @subAccounts )
								
								)
                
                
                                
     -- Get the accounts
        SET NOCOUNT OFF
        
        ;WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
        
       
        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.CollaborateContractStartDate,
				TBL.CollaborateIsMonthlyBillingFrequency,
				TBL.CollaborateIsQuotaAllocationEnabled, 
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                TBL.DeliverContractStartDate,
                TBL.DeliverIsMonthlyBillingFrequency,
                TBL.DeliverIsQuotaAllocationEnabled,
                 ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
				 (SELECT PathString) AS AccountPath
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID	
                INNER JOIN AccountTree act ON a.ID = act.AccountID		
    END
GO
--------------------------------------------------------------------Lauched on Production
--------------------------------------------------------------------Lauched on TESTEU
--------------------------------------------------------------------Lauched on STAGE
