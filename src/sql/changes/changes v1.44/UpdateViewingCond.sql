USE GMGCoZone;

DECLARE @MyCursor CURSOR;
DECLARE @embeddedProfile INT;
DECLARE @customProfile INT;
DECLARE @simulationProfile INT;
DECLARE @softProofingParamId INT;
DECLARE @approvalId INT;
DECLARE @viewingCondId INT;
DECLARE @profileName NVARCHAR(50)
DECLARE @isDefProfile BIT;


BEGIN
    SET @MyCursor = CURSOR FOR
    select ID from dbo.SoftProofingSessionParams
    WHERE ViewingCondition IS NULL
    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @softProofingParamId

    WHILE @@FETCH_STATUS = 0
    BEGIN
		SET @approvalId = (SELECT TOP 1 Approval
						FROM dbo.SoftProofingSessionParams
					    WHERE ID = @softProofingParamId)
					    
		SET @isDefProfile = (SELECT TOP 1 DefaultProfile
						FROM dbo.SoftProofingSessionParams
					    WHERE ID = @softProofingParamId)
					    
	    SET @profileName = (SELECT TOP 1 OutputProfileName
							FROM dbo.SoftProofingSessionParams
							WHERE ID = @softProofingParamId)
		
		IF @isDefProfile = 1
			BEGIN
			SET @embeddedProfile = (SELECT TOP 1 ID 
									FROM dbo.ApprovalEmbeddedProfile
									WHERE Approval = @approvalId)
			END
		ELSE
			SET @embeddedProfile = NULL
		
		IF @embeddedProfile IS NULL AND @isDefProfile = 1
			BEGIN						
			SET @customProfile = (SELECT TOP 1 accst.ID 
								FROM dbo.ApprovalCustomICCProfile apcst
								JOIN dbo.AccountCustomICCProfile accst ON apcst.CustomICCProfile = accst.ID
								WHERE apcst.Approval = @approvalId)
			END
		ELSE
			SET @customProfile = NULL
							
		
		IF @embeddedProfile IS NULL AND @customProfile IS NULL
			BEGIN					
			SET @simulationProfile = (SELECT TOP 1 ID 
									FROM dbo.SimulationProfile simProf
									WHERE AccountId IS NULL AND FileName = @profileName)
			END
		ELSE
			SET @simulationProfile = NULL
		
							
				
		INSERT INTO [GMGCoZone].[dbo].[ViewingCondition]
           ([PaperTint]
           ,[SimulationProfile]
           ,[EmbeddedProfile]
           ,[CustomProfile])
		VALUES
           (NULL
           ,@simulationProfile
           ,@embeddedProfile
           ,@customProfile)
           
        SET @viewingCondId = SCOPE_IDENTITY()
        
        UPDATE dbo.SoftProofingSessionParams
        SET ViewingCondition = @viewingCondId
        WHERE ID = @softProofingParamId
		                        			   			    
      FETCH NEXT FROM @MyCursor 
      INTO @softProofingParamId 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;
GO

UPDATE dbo.SoftProofingSessionParams
SET DefaultSession = 1
WHERE DefaultProfile = 1 AND ActiveChannels IS NULL
GO


