

insert into [dbo].[FileType] values ('doc', 'word document', 'document', 3)

insert into [dbo].[FileType] values ('docx', 'word document', 'document', 3)
   
insert into [dbo].[FileType] values ('ppt', 'word document', 'document', 3)

insert into [dbo].[FileType] values ('pptx', 'word document', 'document', 3)


ALTER TABLE [dbo].[UploadEngineAccountSettings]
ADD AllowDoc  [bit] NOT NULL DEFAULT ((1))


---------------------------------


ALTER TABLE CheckList
    ADD IsDeleted bit 
    CONSTRAINT CheckList_IsDeleted DEFAULT 0 
    WITH VALUES;


ALTER TABLE CheckListItem
    ADD IsDeleted bit 
    CONSTRAINT CheckListItem_IsDeleted DEFAULT 0 
    WITH VALUES;

-------------------------------------------------------

ALTER TABLE [ApprovalCollaborator]
ADD IsReminderMailSent  [bit]  NULL;

ALTER TABLE [ApprovalCollaborator]
ADD [MailSentDate] [datetime2](7)  NULL


-------------------------------------------------------


ALTER TABLE [Account]
ADD IsMeasurementsInInches bit ;


ALTER TABLE [dbo].[Account] ADD  DEFAULT ((0)) FOR [IsMeasurementsInInches]
GO

UPDATE Account set IsMeasurementsInInches = 0;

ALTER TABLE [account] ALTER COLUMN IsMeasurementsInInches bit NOT NULL

