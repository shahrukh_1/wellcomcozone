SET XACT_ABORT ON

BEGIN TRANSACTION

---Update Account Table
DECLARE @AccountToUpdateName NVARCHAR(50) = 'acctest' --jbmadmin
DECLARE @AccountType NVARCHAR(50) = 'CLNT' --CLNT
DECLARE @AccountParent NVARCHAR(50) = 'Subsidiary' --GMG UK
 
DECLARE @AccountToUdateId INT = (SELECT ID FROM [GMGCoZone].[dbo].[Account] WHERE [Name]  = @AccountToUpdateName)
DECLARE @NewAccountType INT = (SELECT ID FROM [GMGCoZone].[dbo].[AccountType] WHERE [Key] = @AccountType)
DECLARE @NewAccountParentId INT = (SELECT ID FROM [GMGCoZone].[dbo].[Account] where [Name] = @AccountParent) 

UPDATE [GMGCoZone].[dbo].[Account]
SET 
	Parent = @NewAccountParentId,
	AccountType = @NewAccountType
WHERE 
	ID = @AccountToUdateId

---Update AccountSubscriptionPlan table
INSERT INTO 
	[GMGCoZone].[dbo].[AccountSubscriptionPlan]
	(
		SelectedBillingPlan,
		ChargeCostPerProofIfExceeded,
		ContractStartDate,
		Account,
		AccountPlanActivationDate,
		AllowOverdraw,
		IsMonthlyBillingFrequency
	)
SELECT
	SelectedBillingPlan,
    ChargeCostPerProofIfExceeded,
    ContractStartDate,
    Account,
	AccountPlanActivationDate,
	AllowOverdraw, 
	1
FROM [GMGCoZone].[dbo].[SubscriberPlanInfo] AS spi
WHERE spi.Account = @AccountToUdateId

--Delete plan info from the SubscriberPlanInfo table
DELETE FROM [GMGCoZone].[dbo].[SubscriberPlanInfo]
WHERE Account = @AccountToUdateId

SELECT * FROM [GMGCoZone].[dbo].[Account] WHERE [Name] = @AccountToUpdateName

COMMIT TRANSACTION