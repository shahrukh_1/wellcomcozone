USE GMGCoZone
GO

---Update GetFolderTree Stored Procedure by adding a new ViewAll parameter
ALTER PROCEDURE [dbo].[SPC_GetFolderTree] (
	@P_User int,
	@P_ViewAll bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @LoggedAccountID INT;
	SET @LoggedAccountID = (SELECT ac.ID
							FROM [dbo].[User] us
							LEFT JOIN  [dbo].[Account] ac on us.[Account] = ac.ID
							WHERE us.ID = @P_User);

	WITH tableSet AS 
		(
			SELECT f.ID,
				   f.Parent,
				   f.Name,
				   f.Creator,
				   f.AllCollaboratorsDecisionRequired,
				   (0) AS Level,
				    (CASE WHEN (exists(	SELECT	fc.ID 
										FROM	[dbo].FolderCollaborator fc
										WHERE	fc.Folder = f.ID and fc.Collaborator = @P_User
						               )
						        )
						  THEN  '1'
						  ELSE '0'
						  end)  AS HasAccess,
				  (CASE WHEN (exists(	SELECT	fa.Folder 
								FROM	[dbo].FolderApproval fa
								WHERE	fa.Folder = f.ID
				               )
				        ) 
				  THEN  CONVERT(bit, 1)
				  ELSE CONVERT(bit, 0)
				  end)  AS HasApprovals  
			FROM	[dbo].[Folder] f
			WHERE f.IsDeleted = 0	AND f.Account = @LoggedAccountID
			
			UNION ALL
			
			SELECT sf.ID,
				   sf.Parent,
				   sf.Name,
				   sf.Creator,
				   sf.AllCollaboratorsDecisionRequired,
				   (CASE WHEN (EXISTS (SELECT fc.ID 
										FROM	[dbo].FolderCollaborator fc 
										WHERE	fc.Folder = sf.ID AND fc.Collaborator = @P_User)
								)
						 THEN  (h.[Level] + 1)
						 ELSE (0)
						 END
				   )  AS [Level],
				  (CASE WHEN (EXISTS (	SELECT fc.ID 
										FROM	[dbo].FolderCollaborator fc
										WHERE	fc.Folder = sf.ID AND fc.Collaborator = @P_User)
							  )
						THEN  '1'
						ELSE '0'
						END
				  )  AS HasAccess,
				  (CASE WHEN (exists(	SELECT	fa.Folder
								FROM	[dbo].FolderApproval fa
								WHERE	fa.Folder = sf.ID
				               )
				        ) 
						THEN  CONVERT(bit, 1)
						ELSE CONVERT(bit, 0)
						end
				  )  AS HasApprovals  
			FROM [dbo].[Folder] sf
				JOIN tableSet h
			ON h.ID	 = sf.Parent
			WHERE sf.IsDeleted = 0			 
		)	
	
	SELECT ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level], AllCollaboratorsDecisionRequired, (SELECT dbo.GetFolderCollaborators(ID)) AS Collaborators, HasApprovals
	FROM tableSet
	WHERE HasAccess = 1 OR @P_ViewAll = 1
	GROUP BY ID, Parent, Name, Creator, AllCollaboratorsDecisionRequired, HasApprovals
	
END

GO

-- CZ-2970 Extend User Report
ALTER VIEW [dbo].[ReturnUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Name,
			'' AS StatusName,
			'' AS UserName,
			'' AS EmailAddress,
			GETDATE() AS DateLastLogin,
			'' AS AccountPath, 
			0 AS CollaborateApprovalsNumber,
			0 AS DeliverJobsNumber


GO

-- CZ-2970 Extend User Report
--- add AccountPath  for User Reports ------------------- 

ALTER PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
		WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
	   -- Get the users				
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin,
		    (SELECT PathString) AS AccountPath,
		    (SELECT COUNT(app.ID) FROM [dbo].Approval AS app WHERE app.[Creator] = u.[ID]) AS CollaborateApprovalsNumber, 
		    (SELECT COUNT(dj.ID) FROM [dbo].DeliverJob AS dj WHERE dj.[Owner] = u.[ID]) AS DeliverJobsNumber		
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID		
		    INNER JOIN AccountTree act 
				ON a.ID = act.AccountID																						
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ((u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END

GO

-- CZ-2971 Extend Account Report
ALTER PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN	
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath,
	    (SELECT ISNULL(CASPBP.[SubscriptionPlanName] + '; ','') +
	            ISNULL(CSPIBP.[SubscriberPlanName] + '; ','')) AS Plans, 
	    (SELECT COUNT(APP.ID) FROM dbo.[Approval] AS APP INNER JOIN dbo.Job AS J ON APP.Job = J.ID WHERE J.Account = a.[ID]) AS ApprovalsCount
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		LEFT OUTER JOIN dbo.SubscriberPlanInfo spi
			ON a.ID = spi.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID
		OUTER APPLY ( SELECT STUFF(( SELECT '; ' + ASPBP.Name
					  FROM      dbo.AccountSubscriptionPlan ASP
								INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
					  WHERE     ASP.Account = A.ID FOR XML PATH('')),1,1,'') AS [SubscriptionPlanName]
					) CASPBP ( [SubscriptionPlanName])
		OUTER APPLY ( SELECT STUFF(( SELECT '; ' + SPIBP.Name
					  FROM      dbo.SubscriberPlanInfo SPI
								INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
					  WHERE     SPI.Account = A.ID FOR XML PATH('')),1,1,'') AS [SubscriberPlanName]
					) CSPIBP ( [SubscriberPlanName])
		
	WHERE	
		s.[Key]<>'D' AND
		a.ID <> @P_LoggedAccount AND
		a.IsTemporary = 0 AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ','))
													 OR spi.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END
GO
-- CZ-2971 Extend Account Report
--Account Report changes
ALTER VIEW [dbo].[AccountsReportView]
AS
SELECT
	0  AS ID, 
	'' AS Name,
	0 AS StatusId,
	'' AS StatusName,
	'' AS [Key],
	'' AS Domain,
	CONVERT(BIT, 0) AS IsCustomDomainActive,-- not null
	CAST('' AS NVARCHAR(128)) AS CustomDomain,--null
	0 AS AccountTypeID,
	GETDATE() AS CreatedDate,
	CONVERT(BIT, 0) AS IsTemporary,-- not null
	'' AS AccountTypeName,
	'' AS GivenName,
	'' AS FamilyName,
	0 AS Company ,
	0 AS Country ,
	'' AS CountryName,	
	CAST(0 AS INT) AS NoOfAccounts,
	CONVERT(DATETIME,GETDATE()) AS LastActivity,
	CAST('' AS NVARCHAR(128)) AS ChildAccounts,
	'' AS AccountPath,
	'' AS Plans, 
	0 As ApprovalsCount

GO

-- CZ-2967 Add createdate / modifieddate in Approvalworkflow tables
ALTER TABLE dbo.ApprovalWorkflow ADD
	CreatedDate datetime2(7),
	ModifiedDate datetime2(7)

ALTER TABLE dbo.ApprovalWorkflow ADD
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalWorkflow_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalWorkflow_ModifiedBy DEFAULT 0
	
GO

-- CZ-2967 Add createdate / modifieddate in Approvalworkflow tables
ALTER TABLE dbo.ApprovalPhase ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalPhase_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalPhase_ModifiedBy DEFAULT 0
	
GO

-- CZ-2967 Add createdate / modifieddate in Approvalworkflow tables
ALTER TABLE dbo.ApprovalJobWorkflow ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalJobWorkflow_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalJobWorkflow_ModifiedBy DEFAULT 0

ALTER TABLE dbo.ApprovalJobPhase ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalJobPhase_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalJobPhase_ModifiedBy DEFAULT 0	
	
GO	

-- CZ-2967 Add createdate / modifieddate in Approvalworkflow tables
ALTER TABLE dbo.ApprovalJobPhaseApproval ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalJobPhaseApproval_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalJobPhaseApproval_ModifiedBy DEFAULT 0
	
GO 

ALTER TABLE dbo.ApprovalPhaseCollaborator ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalPhaseCollaborator_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalPhaseCollaborator_ModifiedBy DEFAULT 0
	
ALTER TABLE dbo.ApprovalPhaseCollaboratorGroup ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_ApprovalPhaseCollaboratorGroup_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_ApprovalPhaseCollaboratorGroup_ModifiedBy DEFAULT 0
	
ALTER TABLE dbo.SharedApprovalPhase ADD
	CreatedDate datetime2(7) NULL,
	ModifiedDate datetime2(7) NULL,
	CreatedBy int NOT NULL CONSTRAINT DF_SharedApprovalPhase_CreatedBy DEFAULT 0,
	ModifiedBy int NOT NULL CONSTRAINT DF_SharedApprovalPhase_ModifiedBy DEFAULT 0
	
GO



  ALTER TABLE [GMGCoZone].[dbo].[Workstation] DROP CONSTRAINT [FK_Workstation_WorkstationStatus]
  GO 
  ALTER TABLE [dbo].[Workstation] DROP COLUMN [Status]
  GO
  DROP TABLE [GMGCoZone].[dbo].[WorkstationStatus]
  GO
  DECLARE @constraintName NVARCHAR(MAX)
  SELECT @constraintName = name
  FROM sys.default_constraints  WHERE name LIKE '%DF__BillingPl__Enabl__%'
  EXEC ('ALTER TABLE [GMGCoZone].[dbo].[BillingPlanType] DROP CONSTRAINT ' + @constraintName)
  GO 
  ALTER TABLE [GMGCoZone].[dbo].[BillingPlanType] DROP COLUMN [EnableSoftProofingTools]
  GO  
  ALTER TABLE [GMGCoZone].[dbo].[BillingPlan] DROP CONSTRAINT [DF_BillingPlan_SoftProofingWorkstations]
  GO
  ALTER TABLE [GMGCoZone].[dbo].[BillingPlan] DROP COLUMN [SoftProofingWorkstations]
  GO  


---- remove the logic related with EnableSoftProofingTools column (related to CZ-2073) ------
ALTER PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId
           
        DECLARE @Count INT;		
		
		WITH TempTable (ResultCount)
		AS
		( SELECT  count(1) as ResultCount
				  FROM      dbo.Account acc
							INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
							INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))				
				  UNION ALL
				  SELECT    count(1) as ResultCount
				  FROM      dbo.Account acc
							INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
							INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))				 
		)

		SELECT @Count = sum(ResultCount) FROM TempTable 
		
	    DECLARE @enableCollaborateSettings BIT = (SELECT CASE WHEN @Count > 0
														THEN CONVERT(BIT, 1)
														ELSE CONVERT(BIT, 0)
                                                    END
                                                  )

        --Remove Profile tab for users that don't have access to Admin Module        
        DECLARE @enableProfileMenu BIT = ( SELECT   CASE WHEN ( SELECT
                                                              COUNT(rl.ID)
                                                              FROM
                                                              dbo.UserRole ur
                                                              JOIN dbo.Role rl ON ur.Role = rl.ID
                                                              JOIN dbo.AppModule apm ON rl.AppModule = apm.ID
                                                              WHERE
                                                              ur.[User] = @P_userId
                                                              AND (apm.[Key] = 3 OR apm.[Key] = 4)
                                                              AND (RL.[Key] = 'ADM' OR rl.[Key] = 'HQM' OR rl.[Key] = 'HQA')
                                                              ) > 0
                                                         THEN CONVERT(BIT, 1)
                                                         ELSE CONVERT(BIT, 0)
                                                    END
                                         )
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
												  
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
                          
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'PSVI'  -- MenuKey - varchar(4)
                          )
            END
                       
        IF(@enableCollaborateSettings = 0)
			BEGIN
				 INSERT  INTO @ignoreMenuKeys
							( MenuKey )
				 VALUES  ( 'CLGS'  -- MenuKey - varchar(4)
							)
            END

       IF ( @enableProfileMenu = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEPF' --MenuKey - varchar(4)
                          )
            END   
            			
        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END

GO





---Update incorrect foreign key for AdvancedSearchApprovalPhase
DECLARE @constraintName NVARCHAR(MAX)
SELECT @constraintName = CONSTRAINT_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS  WHERE CONSTRAINT_NAME LIKE '%FK__AdvancedS__Appro__%'
EXEC ('ALTER TABLE [GMGCoZone].[dbo].[AdvancedSearchApprovalPhase] DROP CONSTRAINT ' + @constraintName) 
GO
DELETE FROM AdvancedSearchApprovalPhase
GO
ALTER TABLE [dbo].[AdvancedSearchApprovalPhase] 
	ADD CONSTRAINT FK_AdvancedS_AppJobPhase 
	FOREIGN KEY (ApprovalPhaseId) REFERENCES ApprovalJobPhase(ID); 

GO

-- CZ-102 Typo in page name
UPDATE 
	[GMGCoZone].[dbo].MenuItem 
SET 
	[Name] = 'Customize'
WHERE 
	[Key] = 'ACCU' 

UPDATE 
	[GMGCoZone].[dbo].MenuItem 
SET 
	[Name] = 'Account Customize'
WHERE 
	[Key] = 'SECU'

UPDATE 
	[GMGCoZone].[dbo].MenuItem 
SET 	
	[Name] = 'Account Customize'
WHERE 
	[Key] = 'ACAC'

UPDATE 
	[GMGCoZone].[dbo].ControllerAction
SET 
	[Action] = 'Customize'
WHERE 
	[ID] IN (SELECT ID FROM [GMGCoZone].[dbo].[ControllerAction] WHERE [Action] LIKE 'Customise')

UPDATE 
	[GMGCoZone].[dbo].[ControllerAction] 
SET 
	[Action] = 'AccountCustomize'
WHERE 
	[ID] IN (SELECT ID FROM [GMGCoZone].[dbo].[ControllerAction] WHERE [Action] LIKE 'AccountCustomise')
	
GO

--- Update SPC_ReturnApprovalsPageInfo to fix CZ-3166
ALTER PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(150),
	   PrimaryDecisionMakerName nvarchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)						
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL and
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0								
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
								(@P_TopLinkId = 1 
										AND EXISTS(
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												  )
									)									
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
								OR 
									(@P_TopLinkId = 7 
										AND js.[Key] != 'COM' AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM dbo.ApprovalCollaboratorDecision ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Decision IS NULL
													)
									)
							  )
							)
						)		
			) = a.[Version]	)
			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (
														 @P_ViewAll = 1 
													 OR
													 ( 
														@P_ViewAll = 0 AND 
														(
															(@P_TopLinkId = 1 AND (@P_User = j.JobOwner OR EXISTS(
																													SELECT TOP 1 ac.ID 
																													FROM ApprovalCollaborator ac 
																													WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																												  )
																					)
															 )									
															OR 
																(@P_TopLinkId = 2 
																	AND @P_User = ISNULL(j.JobOwner,0)
																)
															OR 
																(@P_TopLinkId = 3 
																	AND ISNULL(j.JobOwner,0) != @P_User
																	AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																				)
																)
																OR 
																(@P_TopLinkId = 7 
																	AND js.[Key] != 'COM' AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM dbo.ApprovalCollaboratorDecision ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase AND ac.Decision IS NULL
																				)
																)
														  )
														)
													)										 		
								      )
				)
			)
			AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
			AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
			AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
			AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
			AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
			     OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
			AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
			AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (SELECT ajp.ID
															 FROM ApprovalJobPhase as ajp
															 WHERE ajp.PhaseTemplateID in (SELECT ajp.PhaseTemplateID
															 FROM ApprovalJobPhase as ajp
															 where ajp.ID in (Select val FROM dbo.splitString(@P_ByPhase, ','))))))

	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,			
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
					WHEN ( EXISTS (SELECT ANNOTATION					
							FROM dbo.ApprovalPage ap							
							CROSS APPLY (SELECT TOP 1 aa.ID AS ANNOTATION FROM dbo.ApprovalAnnotation aa WHERE aa.Page = ap.ID 
							AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = a.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																													  WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase 
																													  )
																				)
													  )
																					   
								)	 
							) ANN (ANNOTATION)						
							WHERE ap.Approval = t.ApprovalID 
						 )
					) 
					THEN CONVERT(bit,1)
					ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,						
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
   
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop

END

GO

---Update ReturnFoldersApprovalsPageInfo Stored Procedure by adding a new ViewAll parameter
ALTER PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)	   
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.Deadline,
					a.CreatedDate,
					a.ModifiedDate,
					js.[Key] AS [Status],
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase
			FROM	Job j WITH (NOLOCK)
					INNER JOIN Approval a WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]
					LEFT OUTER JOIN FolderApproval fa WITH (NOLOCK)
						ON fa.Approval = a.ID
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WITH (NOLOCK) WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WITH (NOLOCK) WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WITH (NOLOCK) WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC'
					AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((a.CurrentPhase IS NULL 
					AND
					(	SELECT MAX([Version])
						FROM Approval av WITH (NOLOCK) 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
								@P_ViewAll = 1
								OR
								EXISTS (
											SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac WITH (NOLOCK)
											WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
										)
								)
					) = a.[Version])			
					OR (a.CurrentPhase IS NOT NULL 
						AND a.CurrentPhase = (SELECT avv.CurrentPhase 
											  FROM approval avv WITH (NOLOCK)
											  WHERE avv.Job = a.Job 
													AND a.[Version] = avv.[Version]
													AND avv.[Version] = (SELECT MAX([Version])
																		 FROM Approval av WITH (NOLOCK)
																		 WHERE av.Job = a.Job AND av.IsDeleted = 0)

													AND (
														@P_User = ISNULL(j.JobOwner, 0) 
														OR @P_ViewAll = 1
														OR EXISTS(
																	SELECT TOP 1 ac.ID 
																	FROM ApprovalCollaborator ac WITH (NOLOCK)
																	WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase 
																	)
														)
																								 		
											  )
						)
					)	
					AND	@P_FolderId = fa.Folder
				
			UNION
			
			SELECT 	f.ID AS Approval,
					GetDate() AS Deadline,
					f.CreatedDate,
					f.ModifiedDate,
					'' AS [Status],
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase
			FROM	Folder f WITH (NOLOCK)
			WHERE	f.Account = @P_Account
					AND (@P_Status = 0)
					AND f.IsDeleted = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
					AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN a.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN a.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap WITH (NOLOCK)
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WITH (NOLOCK) WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap WITH (NOLOCK)
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WITH (NOLOCK) WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
		FROM (
				SELECT  t.ID AS ID,
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						a.[Guid],
						a.[FileName],
						js.[Key] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap WITH (NOLOCK)
								     WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
						END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						t.PrimaryDecisionMakerName,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						at.[Key] AS ApprovalType,
						a.[Version] AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av WITH (NOLOCK)
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT CASE
						 WHEN ( EXISTS (SELECT aa.ID					
										 FROM dbo.ApprovalAnnotation aa WITH (NOLOCK)
										 INNER JOIN dbo.ApprovalPage ap WITH (NOLOCK) ON aa.Page = ap.ID
										 WHERE ap.Approval = t.ApprovalID			 
										 )
								) THEN CONVERT(bit,1)
								  ELSE CONVERT(bit,0)
						 END)  AS HasAnnotations,				
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
													JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
													                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
														JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
														WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
										JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
										JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				CONVERT(bit, 1) AS IsCollaborator,
				CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,				
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
				t.PhaseName,
				ISNULL((SELECT CASE
							WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WITH (NOLOCK) WHERE ajw.Job = a.Job)
							ELSE ''
						END),'') AS WorkflowName,
				ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap WITH (NOLOCK)
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WITH (NOLOCK) WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
				ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
				ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WITH (NOLOCK) WHERE ID = a.CurrentPhase), 0) AS DecisionType,
				(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
							 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WITH (NOLOCK) WHERE ID = j.[JobOwner])
							 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WITH (NOLOCK) WHERE ID = a.[Owner])
							 END) AS OwnerName,
				a.AllowOtherUsersToBeAdded,
				j.JobOwner,
				(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WITH (NOLOCK) WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
														   JOIN ApprovalJobPhase ajp WITH (NOLOCK) ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WITH (NOLOCK) WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
														  JOIN ApprovalJobPhase ajp WITH (NOLOCK) ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker				
				FROM	Job j WITH (NOLOCK)
						INNER JOIN Approval a WITH (NOLOCK)
							ON a.Job = j.ID
						INNER JOIN dbo.ApprovalType at WITH (NOLOCK)
							ON 	at.ID = a.[Type]
						INNER JOIN JobStatus js WITH (NOLOCK)
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa WITH (NOLOCK)
							ON fa.Approval = a.ID	
						JOIN @TempItems t
							ON t.ApprovalID = a.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 1
						
				UNION
				
				SELECT 	t.ID AS ID,
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						'' AS [Guid],
						'' AS [FileName],
						'' AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'' AS PrimaryDecisionMakerName,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS ApprovalType,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1 WITH (NOLOCK)
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa WITH (NOLOCK)
                				INNER JOIN Approval av WITH (NOLOCK)
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo WITH (NOLOCK) ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos WITH (NOLOCK) ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						CONVERT(bit, 0) AS HasAnnotations,
						'' AS Decision,
						CONVERT(bit, 1) AS IsCollaborator,
						f.AllCollaboratorsDecisionRequired,
						NULL as ApprovalApprovedDate,
						0 AS CurrentPhase,
						'' AS PhaseName,
						'' AS WorkflowName,
						'' AS NextPhase,
						'' AS DecisionMakers,
						0 AS DecisionType,
						'' AS OwnerName,
						CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
						NULL AS JobOwner,
						CONVERT(bit, 0) AS IsPhaseComplete,
						'' AS VersionSufix,
						CONVERT(bit, 0) AS IsLocked,
					    CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
				FROM	Folder f WITH (NOLOCK)
						JOIN @TempItems t 
							ON t.ApprovalID = f.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 0
			) result		
	ORDER BY result.ID	

	SET ROWCOUNT 0

	---- Send the total rows count ---- 
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j WITH (NOLOCK)
						INNER JOIN Approval a WITH (NOLOCK)
							ON a.Job = j.ID
						INNER JOIN JobStatus js WITH (NOLOCK)
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa WITH (NOLOCK)
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
						AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av WITH (NOLOCK)
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND EXISTS (
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac WITH (NOLOCK)
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f WITH (NOLOCK)
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END

GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalJobWorkflow]
ADD [RerunWorkflow] BIT NOT NULL DEFAULT(0)
GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalWorkflow]
ADD [RerunWorkflow] BIT NOT NULL DEFAULT(0)
GO

ALTER PROCEDURE [dbo].[SPC_DeleteAccount](
	@P_Id int
)
AS
	BEGIN
	SET XACT_ABORT ON;  
	BEGIN TRAN

    --DECLARE @P_Success nvarchar(512) = ''
    
	BEGIN TRY
		
	----- Get users ids of the deleted accounts -----
	DECLARE @DeletedUsers TABLE
	(	  
	   UserID INT
	)
	
	INSERT INTO @DeletedUsers (UserID)
	SELECT ID FROM [dbo].[User] 
	WHERE Account = @P_Id	
	--------------------------------------------------
	
	---- Delete records base on deleted accounts ids -------
	DELETE FROM [dbo].[AccountBillingPlanChangeLog]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountBillingTransactionHistory]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountHelpCentre]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[AccountSetting]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountSubscriptionPlan]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[Company]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[SubscriberPlanInfo]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[UploadEngineProfile]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[AccountPricePlanChangedMessage]
	WHERE ChildAccount = @P_Id
		
	DELETE [dbo].[ThemeColorScheme]
	FROM [dbo].[ThemeColorScheme] tcs
		JOIN [dbo].[AccountTheme] act ON tcs.AccountTheme = act.ID
	WHERE act.Account = @P_Id
	
	DELETE [dbo].[AccountTheme]
	FROM [dbo].[AccountTheme] act
	WHERE act.Account = @P_Id
	
    DELETE [dbo].[ThemeColorScheme]
	FROM [dbo].[ThemeColorScheme] tcs
		JOIN [dbo].[BrandingPresetTheme] bpt ON tcs.BrandingPresetTheme = bpt.ID
		JOIN [dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	WHERE bp.Account = @P_Id
	
	DELETE [dbo].[BrandingPresetTheme]
	FROM [dbo].[BrandingPresetTheme] bpt
		JOIN [dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	where bp.Account = @P_Id
	
	DELETE [dbo].[BrandingPresetUserGroup]
	FROM [dbo].[BrandingPresetUserGroup] bpug
			JOIN [dbo].[BrandingPreset] bp ON bpug.BrandingPreset = bp.ID			
	WHERE bp.[Account] = @P_Id
	
	DELETE FROM [dbo].[ApprovalCustomDecision]
	WHERE Account = @P_Id
    
    DELETE FROM [GMGCoZone].[dbo].[InactiveApprovalStatuses]
	WHERE Account = @P_Id

	---------------------------------------------------------
	
	----- Delete record from CMS tables ---------	
	
	DELETE FROM [GMGCoZone].[dbo].[CMSAnnouncement]
	WHERE Account = @P_Id
	
	DELETE FROM [GMGCoZone].[dbo].[CMSDownload]
	WHERE Account = @P_Id
	
	DELETE FROM [GMGCoZone].[dbo].[CMSManual]
	WHERE Account = @P_Id
	
	DELETE FROM [GMGCoZone].[dbo].[CMSVideo]
	WHERE Account = @P_Id
	
	---------------------------------------------------------
	
	----- Delete record from tables related to user ---------	
	DELETE FROM [dbo].[UserSetting] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[UserRole] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE FROM [dbo].[UserLogin] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[UserHistory] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers) ---- OR [Creator]
	
	DELETE [dbo].[UserGroupUser] 
	FROM [dbo].[UserGroupUser] ugu
			JOIN [dbo].[UserGroup] ug ON ugu.UserGroup = ug.ID
	WHERE ug.Account = @P_Id

	DELETE FROM [dbo].[UserCustomPresetEventTypeValue] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[UserApprovalReminderEmailLog] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[StudioSetting] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[SharedColorProofWorkflow] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[SharedColorProofWorkflow] 
    FROM [dbo].[SharedColorProofWorkflow] scp	
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON scp.ColorProofCoZoneWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[NotificationScheduleUserEventTypeEmailLog] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE FROM [dbo].[NotificationItem] 
	WHERE [InternalRecipient] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[NotificationItem] 
	WHERE [Creator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[CurrencyRate] 
	WHERE [Creator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalUserViewInfo] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalUserRecycleBinHistory] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalCollaborator] 
	WHERE [Collaborator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalCollaborator] 
	FROM [dbo].[ApprovalCollaborator] ac
			JOIN [dbo].[Approval] a ON ac.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE FROM [dbo].[ApprovalAnnotationStatusChangeLog] 
	FROM [dbo].[ApprovalAnnotationStatusChangeLog]  aasc
			JOIN [dbo].[ApprovalAnnotation] aa ON aasc.Annotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE FROM [dbo].[ApprovalAnnotationPrivateCollaborator] 
	WHERE [Collaborator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ApprovalAnnotationPrivateCollaborator]
	FROM [dbo].[ApprovalAnnotationPrivateCollaborator] aapc
			JOIN [dbo].[ApprovalAnnotation] aa ON aapc.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id						
	---------------------------------------------------------
	
	
	-- Delete records from tables taht are related with Approval --------
	DELETE FROM [dbo].[ApprovalCollaboratorDecision] 	
	WHERE Collaborator IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE [dbo].[ApprovalJobPhaseApproval]
	FROM [dbo].[ApprovalJobPhaseApproval] ajpa
			JOIN [dbo].[Approval] a ON ajpa.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id 
	
	DELETE [dbo].[ApprovalCollaboratorDecision]
	FROM [dbo].[ApprovalCollaboratorDecision] acd
			JOIN [dbo].[Approval] a ON acd.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ApprovalJobViewingCondition]
	FROM [dbo].[ApprovalJobViewingCondition] ajc
			JOIN [dbo].[Job] j ON ajc.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[SharedApproval]
	FROM [dbo].[SharedApproval] sa
			JOIN [dbo].[Approval] a ON sa.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id 
	
	DELETE [dbo].[DeliverSharedJob]
	FROM [dbo].[DeliverSharedJob] dsj
			JOIN [dbo].[DeliverExternalCollaborator] dc ON dsj.DeliverExternalCollaborator = dc.ID			
	WHERE dc.Account = @P_Id
	
	DELETE [dbo].[DeliverSharedJob] 
	FROM [dbo].[DeliverSharedJob] dsj
			JOIN [dbo].[DeliverJob] dj ON dsj.[DeliverJob] =  dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ApprovalAnnotationAttachment]
	FROM [dbo].[ApprovalAnnotationAttachment] at
			JOIN [dbo].[ApprovalAnnotation] aa ON at.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalAnnotationMarkup]
	FROM [dbo].[ApprovalAnnotationMarkup] am
			JOIN [dbo].[ApprovalAnnotation] aa ON am.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[LabColorMeasurement] 
	FROM	[dbo].[LabColorMeasurement] lbm
			JOIN [dbo].[ApprovalSeparationPlate] asp ON lbm.ApprovalSeparationPlate = asp.ID
			JOIN [dbo].[ApprovalPage] ap ON asp.Page = ap.ID
			JOIN [dbo].[Approval] a ON a.ID = ap.Approval 
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalSeparationPlate]
	FROM [dbo].[ApprovalSeparationPlate] asp
			JOIN [dbo].[ApprovalPage] ap ON asp.Page = ap.ID				
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ApprovalSetting]
	FROM [dbo].[ApprovalSetting] ast				
			JOIN [dbo].[Approval] a ON ast.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalCustomICCProfile]
	FROM [dbo].[ApprovalCustomICCProfile] acp				
			JOIN [dbo].[Approval] a ON acp.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[SoftProofingSessionError]
	FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE [dbo].[SoftProofingSessionParams]
	FROM [dbo].[SoftProofingSessionParams] sfse
			JOIN [dbo].[Approval] a ON sfse.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[ViewingCondition]
	FROM [dbo].ViewingCondition vwc
			JOIN [dbo].[ApprovalEmbeddedProfile] am	ON vwc.EmbeddedProfile = am.ID		
			JOIN [dbo].[Approval] a ON am.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalEmbeddedProfile]
	FROM [dbo].[ApprovalEmbeddedProfile] am			
			JOIN [dbo].[Approval] a ON am.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[DeliverJobPageVerificationResult]
	FROM [dbo].[DeliverJobPageVerificationResult] djpvr				
			JOIN [dbo].[DeliverJobPage] djp ON djpvr.DeliverJobPage = djp.ID
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[NotificationScheduleUserGroup]
	FROM [dbo].[NotificationScheduleUserGroup] nsug			
			JOIN [dbo].[UserGroup] ug ON nsug.UserGroup = ug.ID
	WHERE ug.Account = @P_Id
	
	DELETE [dbo].[NotificationScheduleUser]
	FROM [dbo].[NotificationScheduleUser] nsu 
			JOIN [dbo].[NotificationSchedule] ns ON nsu.NotificationSchedule = ns.ID
			JOIN [dbo].[AccountNotificationPreset] anp ON ns.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id			
	
	---------------------------------------------------------------------
	
	UPDATE u
	SET PrinterCompany = NULL
	FROM dbo.[User] u
	WHERE u.ID IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[PrinterCompany]
	FROM [dbo].[PrinterCompany] pcm
	WHERE pcm.Account = @P_Id
				
	---------------------------------------------------------------------
	
	---------------------------------------------------------------------
	--- Delete Administration - Approval Phasing related tables -----------
	---------------------------------------------------------------------
	DELETE [dbo].[ApprovalPhaseCollaborator]
	FROM [dbo].[ApprovalPhaseCollaborator] apc
				JOIN [dbo].[ApprovalPhase] ap ON apc.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id
	
	DELETE [dbo].[SharedApprovalPhase]
	FROM [dbo].[SharedApprovalPhase] sap
				JOIN [dbo].[ApprovalPhase] ap ON sap.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id
	
	DELETE [dbo].[ApprovalPhaseCollaboratorGroup]
	FROM [dbo].[ApprovalPhaseCollaboratorGroup] apcg
				JOIN [dbo].[ApprovalPhase] ap ON apcg.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id	
	
	DELETE [dbo].[ApprovalPhase]
	FROM [dbo].[ApprovalPhase] ap				
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id
	
	DELETE [dbo].[ApprovalWorkflow]
	WHERE [Account] = @P_Id	
	----------------------------------------------------------------------
	
	
	----- ---------------------------------------------------
	DELETE [dbo].[AccountNotificationPresetEventType]
	FROM [dbo].[AccountNotificationPresetEventType] anpet
				JOIN [dbo].[AccountNotificationPreset] anp ON anpet.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id
	
	DELETE [dbo].[UploadEngineCustomXMLTemplatesField] 
	FROM [dbo].[UploadEngineCustomXMLTemplatesField] uecxtf
				JOIN [dbo].[UploadEngineCustomXMLTemplate] uecxt ON uecxtf.UploadEngineCustomXMLTemplate = uecxt.ID
	WHERE uecxt.Account = @P_Id
	
	DELETE [dbo].[FTPPresetJobDownload]
	FROM [dbo].[FTPPresetJobDownload] fpjd
				JOIN [dbo].[UploadEnginePreset] uep ON fpjd.Preset = uep.ID
	WHERE uep.[Account] = @P_Id
	
	DELETE [dbo].[FTPPresetJobDownload]
	FROM [dbo].[FTPPresetJobDownload] fpjd			
			JOIN [dbo].[DeliverJob] dj ON fpjd.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE [dbo].[WorkstationValidationResult]
	FROM [dbo].[WorkstationValidationResult] wvr
				JOIN [dbo].[WorkstationCalibrationData] wcd ON wvr.WorkstationCalibrationData = wcd.ID
				JOIN [dbo].[Workstation] w ON wcd.Workstation = w.ID
	WHERE w.[User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ApprovalCollaboratorGroup]
	FROM [dbo].[ApprovalCollaboratorGroup] acg			
			JOIN [dbo].[UserGroup] ug ON acg.CollaboratorGroup = ug.ID
	WHERE ug.Account = @P_Id 	
	
	DELETE [dbo].[ColorProofCoZoneWorkflowUserGroup]
	FROM [dbo].[ColorProofCoZoneWorkflowUserGroup] ccwug			
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON  ccwug.ColorProofCoZoneWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE [dbo].[ColorProofCoZoneWorkflowUserGroup]
	FROM [dbo].[ColorProofCoZoneWorkflowUserGroup] ccwug			
			JOIN [dbo].[UserGroup] ug ON  ccwug.UserGroup = ug.ID			
	WHERE ug.Account = @P_Id	
	
	DELETE [dbo].[UploadEngineUserGroupPermissions]
	FROM [dbo].[UploadEngineUserGroupPermissions] ueugp			
			JOIN [dbo].[UserGroup] ug ON ueugp.UserGroup = ug.ID
	WHERE ug.Account = @P_Id
	----------------------------------------------------------------------
	
	
	-------- Delete related to folders based on deleted accounts ids -----
	DELETE [dbo].[FolderApproval]
	FROM [dbo].[FolderApproval] fa			
			JOIN [dbo].[Folder] f ON fa.Folder = f.ID
	WHERE f.Account = @P_Id 
	
	DELETE [dbo].[FolderCollaboratorGroup]
	FROM [dbo].[FolderCollaboratorGroup] fcg			
			JOIN [dbo].[Folder] f ON fcg.Folder = f.ID
	WHERE f.Account = @P_Id 
	
	DELETE [dbo].[FolderCollaborator]
	FROM [dbo].[FolderCollaborator] fc			
			JOIN [dbo].[Folder] f ON fc.Folder = f.ID
	WHERE f.Account = @P_Id
	---------------------------------------------------------------------- 

	--------- LEVEL 2 -------------
	----------------------------------------------------------------------	
	DELETE FROM [dbo].[DeliverExternalCollaborator]	
	WHERE Account = @P_Id
	
	DELETE [dbo].[ApprovalAnnotation]
	FROM [dbo].[ApprovalAnnotation] aa						
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[DeliverJobPage]
	FROM  [dbo].[DeliverJobPage] djp 
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id

	DELETE [dbo].[DeliverJobData]
	FROM  [dbo].[DeliverJobData] djd
			JOIN [dbo].[DeliverJob] dj ON djd.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[DeliverJobData]
	FROM  [dbo].[DeliverJobData] djd
			JOIN [dbo].[DeliverJob] dj ON djd.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[DeliverJobPage]
	FROM  [dbo].[DeliverJobPage] djp 
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[WorkstationCalibrationData]
	FROM [dbo].[WorkstationCalibrationData] wcd
			JOIN [dbo].[Workstation] w ON wcd.Workstation = w.ID
	WHERE w.[User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[Folder]
	WHERE Account = @P_Id	
	
	DELETE [dbo].[ChangedBillingPlan] 
	FROM [dbo].[ChangedBillingPlan] cbp
			JOIN [dbo].[AttachedAccountBillingPlan] aabp ON cbp.AttachedAccountBillingPlan = aabp.ID
	WHERE aabp.Account = @P_Id
	
	DELETE FROM [dbo].[UploadEngineCustomXMLTemplate]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[UploadEnginePreset]
	WHERE [Account] = @P_Id 
	
	DELETE [dbo].[NotificationSchedule]
	FROM [dbo].[NotificationSchedule] ns
			JOIN [dbo].[AccountNotificationPreset] anp ON ns.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id	
	
	DELETE FROM [dbo].[BrandingPreset]			
	WHERE [Account] = @P_Id
	
	DELETE FROM [dbo].[AccountStorageUsage]
	WHERE Account = @P_Id
	----------------------------------------------------------------------
	
	
	------ LEVEL 3 -------------------------------------------------------
	----------------------------------------------------------------------
	DELETE [dbo].[DeliverJob]
	FROM [dbo].[DeliverJob] dj 
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[DeliverJob]
	FROM [dbo].[DeliverJob] dj	
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account =@P_Id
	
	DELETE [dbo].[DeliverJobDeleteHistory]
	FROM [dbo].[DeliverJobDeleteHistory] djh	
	WHERE djh.Account =@P_Id	
	
	DELETE FROM [dbo].[Workstation] ---- need review
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ApprovalPage]
	FROM [dbo].[ApprovalPage] ap
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE FROM [dbo].[AttachedAccountBillingPlan] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[BillingPlan] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[BillingPlanType] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountNotificationPreset] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[ExcludedAccountDefaultProfiles]
	WHERE Account = @P_Id
	
	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
	JOIN [dbo].[SimulationProfile] sim ON vwc.SimulationProfile = sim.ID
	WHERE sim.AccountId = @P_Id	
	
	DELETE FROM [dbo].[SimulationProfile]
	WHERE AccountId = @P_Id	
	
	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
	JOIN [dbo].[PaperTint] ppt ON vwc.PaperTint = ppt.ID
	WHERE ppt.AccountId = @P_Id	
	
	DELETE [dbo].[PaperTintMeasurement]
	FROM [dbo].[PaperTintMeasurement] pptms
	JOIN [dbo].[PaperTint] ppt ON pptms.PaperTint = ppt.ID
	WHERE ppt.AccountId = @P_Id	
	
	DELETE FROM [dbo].[PaperTint]
	WHERE AccountId = @P_Id	
	----------------------------------------------------------------------
	
	
	------ LEVEL 4 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE [dbo].[Approval]
	FROM [dbo].[Approval] a
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalJobPhase] 
	FROM [dbo].[ApprovalJobPhase] ajp
		JOIN [dbo].[ApprovalJobWorkflow] ajw ON ajp.[ApprovalJobWorkflow] = ajw.ID
		JOIN [dbo].[Job] j ON ajw.Job = j.ID 
	WHERE j.Account = @P_Id		
	
	DELETE [dbo].[ApprovalJobWorkflow] 
	FROM [dbo].[ApprovalJobWorkflow] ajw
		JOIN [dbo].[Job] j ON ajw.Job = j.ID 
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ApprovalJobDeleteHistory]
	FROM [dbo].[ApprovalJobDeleteHistory] ajh	
	WHERE ajh.Account =@P_Id	
	
	DELETE FROM [dbo].[UserGroup] 
	WHERE Account = @P_Id

	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
			JOIN [dbo].[AccountCustomICCProfile] acp ON vwc.CustomProfile = acp.ID			
	WHERE acp.Account = @P_Id
	
	DELETE FROM [dbo].[AccountCustomICCProfile] 
	WHERE Account = @P_Id	

	DELETE [dbo].[ColorProofCoZoneWorkflow]
	FROM [dbo].[ColorProofCoZoneWorkflow] cczw
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].ProofStudioSessions
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)		
	----------------------------------------------------------------------
	
	
	------ LEVEL 5 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE [dbo].[ColorProofWorkflow]
	FROM [dbo].[ColorProofWorkflow] cpw 
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ExternalCollaborator]
	WHERE  Account = @P_Id
	
	DELETE [dbo].[JobStatusChangeLog]
	FROM [dbo].[JobStatusChangeLog] jcl
		JOIN dbo.[Job] j ON jcl.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE FROM [dbo].[Job]
	WHERE Account = @P_Id	
	----------------------------------------------------------------------
	
	
	------ LEVEL 6 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE FROM [dbo].[ColorProofInstance]
	WHERE [Owner] IN (SELECT UserID FROM @DeletedUsers)
	----------------------------------------------------------------------

	
	------ LEVEL 7-------------------------------------------------------
	--------------------------------------------------------------------
	--exec sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'
	
	-- Deletion of records from User and Account tables cannot be done because of circular references
	-- so, the two lines below are needed to disable the tables constraints and enable after delete operation is complete
	ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT ALL
	ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT ALL
		
	DELETE [dbo].[User] FROM [dbo].[User] 
	WHERE ID IN (SELECT UserID FROM @DeletedUsers)		

	----------------------------------------------------------------------
	
	-------- LEVEL 8-------------------------------------------------------
	------------------------------------------------------------------------
	DELETE FROM [dbo].[Account] WHERE ID = @P_Id
			
	ALTER TABLE [dbo].[User] WITH CHECK CHECK CONSTRAINT ALL
	ALTER TABLE [dbo].[Account] WITH CHECK CHECK CONSTRAINT ALL	
  	--exec sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'	
	------------------------------------------------------------------------

	COMMIT TRAN

	END TRY
	BEGIN CATCH	
		DECLARE @error int, @message varchar(4000), @xstate int;
		SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
		
		IF (XACT_STATE()) = -1  
			BEGIN 		
				ROLLBACK TRANSACTION;  				
			END;  
		    
		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;  
	
       RAISERROR ('SPC_DeleteAccount: %d: %s', 16, 1, @error, @message) ;
	END CATCH
	END

GO
----------------------------------------------------------------------Launched on Stage
----------------------------------------------------------------------Launched on Production

