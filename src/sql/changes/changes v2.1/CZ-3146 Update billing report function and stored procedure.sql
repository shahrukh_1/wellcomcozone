USE [GMGCoZone]
GO

DROP FUNCTION [dbo].[GetBillingCycles]

GO

IF (TYPE_ID(N'[dbo].AccountIDsTableType') IS NULL) 
	CREATE TYPE [dbo].AccountIDsTableType AS TABLE ([ID] [int] NULL); 
GO 

USE [GMGCoZone]
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 09/13/2017 13:16:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

-- Create Get Billing Cycles function to support custom plans and not only attached
CREATE FUNCTION [dbo].[GetBillingCycles]
    (
      @LoggedAccountID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @Accounts AccountIDsTableType READONLY 
    )
RETURNS  @BillingCycles TABLE 
(
    -- columns returned by the function
    AccountID INT,
    AppModule INT, 
    SelectedBillingPlan INT,
    CycleAllowedProofsCount INT,
    BillingPlanPrice INT,
    Collaborate NVARCHAR(MAX),
    Deliver NVARCHAR(MAX)
)
AS 
    BEGIN

        DECLARE @cycleStartDate DATETIME = @StartDate
        
        BEGIN 
			INSERT INTO @BillingCycles (AccountID, AppModule, SelectedBillingPlan, CycleAllowedProofsCount, BillingPlanPrice)
			SELECT
				A.ID,
				CASE 
					WHEN BPT2.AppModule IS NOT NULL THEN BPT2.AppModule
					ELSE BPT.AppModule
				END,
				CASE --
					WHEN 
						AT.[Key] = 'SBSC' THEN  SPI.SelectedBillingPlan
					ELSE 
						ASP.SelectedBillingPlan END AS SEL,
				CASE --
					WHEN 
						SPI.IsQuotaAllocationEnabled = 1 THEN SPI.NrOfCredits
					WHEN 
						AT.[Key] = 'SBSC' THEN BP.Proofs
					ELSE 
						BP2.Proofs END,
				CASE --
					WHEN ( @LoggedAccountID = 1 )
					THEN 
						CASE
							WHEN 
								AT.[Key] = 'SBSC' THEN BP.Price
							ELSE 
								BP2.Price 
						END
					ELSE 
						( ISNULL((SELECT   
										NewPrice
								  FROM     
										AttachedAccountBillingPlan
								  WHERE    
										Account = @LoggedAccountID AND 
										BillingPlan = CASE 
														WHEN SPI.SelectedBillingPlan IS NOT NULL THEN SPI.SelectedBillingPlan 
														ELSE ASP.SelectedBillingPlan 
													  END),
                           CASE
								WHEN 
									AT.[Key] = 'SBSC' THEN BP.Price
								ELSE 
									BP2.Price 
							END)
						 )  
					END
			FROM    
				Account A
				LEFT JOIN dbo.AccountType AT ON AT.ID = A.AccountType
				LEFT JOIN dbo.SubscriberPlanInfo SPI ON A.ID = SPI.Account
				LEFT JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
				LEFT JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.[Type]
				LEFT JOIN dbo.AccountSubscriptionPlan ASP ON A.ID = ASP.Account
				LEFT JOIN dbo.BillingPlan BP2 ON ASP.SelectedBillingPlan = BP2.ID
				LEFT JOIN dbo.BillingPlanType BPT2 ON BPT2.ID = BP2.Type
			WHERE
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Collaborate')) OR
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Deliver')) OR
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT2.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Collaborate')) OR
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT2.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Deliver')) OR 
				(A.ID IN (SELECT ID FROM @Accounts))
				
		END			
																					
        SET @cycleStartDate = @StartDate
	
        WHILE ( datepart(year, @cycleStartDate) < datepart(year,@EndDate) OR
			  ( datepart(year, @cycleStartDate) = datepart(year,@EndDate) AND datepart(month, @cycleStartDate) <= datepart(month,@EndDate)) ) 
            BEGIN	
				       UPDATE @BillingCycles 
				       SET Collaborate =                           	
						CASE WHEN AppModule = 1 THEN (SELECT
							(ISNULL(Collaborate, '') + ',' +  CONVERT(NVARCHAR, LEFT(datename(month, @cycleStartDate), 3)) + '-' + CONVERT(NVARCHAR, datepart(year, @cycleStartDate) % 100) + '|' +  CONVERT(NVARCHAR, COALESCE(SUM(cr.ProofsUsed), 0)) + '|' + ( CASE WHEN (COALESCE(SUM(cr.ProofsUsed), 0) - CycleAllowedProofsCount) < 0 THEN '0' ELSE CONVERT(NVARCHAR, ((COALESCE(SUM(cr.ProofsUsed), 0) - CycleAllowedProofsCount))) END ) + '|' +  CONVERT(NVARCHAR, BillingPlanPrice) + '|' +  CONVERT(NVARCHAR, CycleAllowedProofsCount))
						FROM (
							SELECT
								COALESCE(SUM(d.ProofsPerJob), '0') AS ProofsUsed
							FROM(
								  SELECT
									  CEILING(COUNT(t.ID) / CAST(4 AS FLOAT)) AS ProofsPerJob
								  FROM 
									(
										SELECT ap.ID, j.Account, ap.CreatedDate, ap.FileName,ap.Job				
										FROM
												Job j
												INNER JOIN Approval ap ON j.ID = ap.Job
												WHERE
												j.Account = AccountID
												AND datepart(month, ap.CreatedDate) = datepart(month, @cycleStartDate)
												AND datepart(year, ap.CreatedDate) = datepart(year, @cycleStartDate)
												
										UNION ALL
										SELECT a.ID, a.Account, a.CreatedDate, a.JobName, a.Job		
												FROM dbo.ApprovalJobDeleteHistory a
												WHERE 
												a.Account = AccountID
												AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
												AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
												AND a.Job IS NOT NULL
									 ) t
								 GROUP BY t.Job
								)d
							UNION ALL
							SELECT 
								SUM(dn.ProofsPerJob) AS ProofsUsed
							FROM(
									SELECT 
										CEILING(COUNT(a.ID) / CAST(4 AS FLOAT)) AS ProofsPerJob
									FROM dbo.ApprovalJobDeleteHistory a
									WHERE 
									a.Account = AccountID
									AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
									AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
									AND a.Job IS NULL
									GROUP BY a.JobName
								)  dn
							  )cr
						) END,
					Deliver = 
				    CASE WHEN AppModule = 3 THEN (SELECT  
						(ISNULL(Deliver, '') + ',' +  CONVERT(NVARCHAR, LEFT(datename(month, @cycleStartDate), 3)) + '-' + CONVERT(NVARCHAR, datepart(year, @cycleStartDate) % 100) + '|' +  CONVERT(NVARCHAR, COALESCE(COUNT(dr.ID), 0)) + '|' +  (CASE WHEN (COUNT(dr.ID) - CycleAllowedProofsCount) < 0 THEN '0' ELSE CONVERT(NVARCHAR, (COUNT(dr.ID) - CycleAllowedProofsCount)) END) + '|' +  CONVERT(NVARCHAR, BillingPlanPrice) + '|' +  CONVERT(NVARCHAR, CycleAllowedProofsCount))
					 FROM (SELECT
								dj.ID 
							FROM Job j
							INNER JOIN dbo.DeliverJob dj ON j.ID = dj.Job
							WHERE
							j.Account = AccountID
							AND datepart(month, dj.CreatedDate) = datepart(month, @cycleStartDate)
							AND datepart(year, dj.CreatedDate) = datepart(year, @cycleStartDate)
							UNION ALL
							SELECT 
								d.ID 
							FROM dbo.DeliverJobDeleteHistory d
							WHERE 
							d.Account = AccountID
							AND datepart(month, d.CreatedDate) = datepart(month, @cycleStartDate)
							AND datepart(year, d.CreatedDate) = datepart(year, @cycleStartDate)
							) AS dr) END
				
			    SET @cycleStartDate = DATEADD(MM, 1, @cycleStartDate)
                    
            END
            
        RETURN  
	
    END

GO

USE [GMGCoZone]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 09/13/2017 13:16:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


--alter billing report to return all accounts 
ALTER PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        
         DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN dbo.AccountStatus ast ON a.[Status] = ast.ID 
                WHERE	a.ID IN (SELECT ID FROM dbo.[GetChildAccounts](@P_LoggedAccount))
						AND a.ID != 1
                        AND a.IsTemporary = 0
						AND (ast.[Key] = 'A')
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR ( @P_BillingPlanList != '' AND (
																 EXISTS (SELECT    val
																		 FROM      dbo.splitString(@P_BillingPlanList, ',')
																		 where val in (
																					   SELECT bp.SelectedBillingPlan from dbo.AccountSubscriptionPlan bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   UNION
																					   SELECT bp.SelectedBillingPlan from dbo.SubscriberPlanInfo bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   )
																		)																 
																)
								 )
                            )
                            
        DECLARE @AccountIDs AS AccountIDsTableType;  

		INSERT INTO @AccountIDs (ID)  
			SELECT ID
			FROM @subAccounts;
			
		DECLARE @billingCycles TABLE 
		(
			AccountID INT, 
			AppModule INT,
			SelectedBillingPlan INT,
			CycleAllowedProofsCount INT,
			BillingPlanPrice INT,
			Collaborate NVARCHAR(MAX),
			Deliver NVARCHAR(MAX)
		);
		
		INSERT INTO @billingCycles
		SELECT * FROM [dbo].[GetBillingCycles](@P_LoggedAccount, @P_StartDate, @P_EndDate, @AccountIDs)
               
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateContractStartDate DATETIME,
              CollaborateIsMonthlyBillingFrequency BIT,              
              CollaborateCycleDetails NVARCHAR(MAX) ,
              CollaborateModuleId INT ,
              CollaborateIsQuotaAllocationEnabled BIT,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverContractStartDate DATETIME,
              DeliverIsMonthlyBillingFrequency BIT,
              DeliverCycleDetails NVARCHAR(MAX) ,
              DeliverModuleId INT ,
              DeliverIsQuotaAllocationEnabled BIT
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateContractStartDate,
                  CollaborateIsMonthlyBillingFrequency,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  CollaborateIsQuotaAllocationEnabled,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverContractStartDate,
                  DeliverIsMonthlyBillingFrequency,
                  DeliverCycleDetails ,
                  DeliverModuleId ,
                  DeliverIsQuotaAllocationEnabled
			  )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.ContractStartDate
                             ELSE CASPBP.ContractStartDate
                        END AS CollaborateContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(CASPBP.IsMonthlyBillingFrequency, 0)
                        END AS CollaborateIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Collaborate, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = CSPIBP.BillingPlan OR SelectedBillingPlan IS NULL))
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Collaborate, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = CASPBP.BillingPlan OR SelectedBillingPlan IS NULL))
                               END, '0|0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.IsQuotaAllocationEnabled
                             ELSE CASPBP.IsQuotaAllocationEnabled
                        END AS [CollaborateIsQuotaAllocationEnabled],
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.ContractStartDate
                             ELSE DASPBP.ContractStartDate
                        END AS DeliverContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(DASPBP.IsMonthlyBillingFrequency, 0)
                        END AS DeliverIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Deliver, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = DASPBP.BillingPlan OR SelectedBillingPlan IS NULL))
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Deliver, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = DSPIBP.BillingPlan OR SelectedBillingPlan IS NULL))
                               END, '0|0|0|0"0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.IsQuotaAllocationEnabled
                             ELSE DASPBP.IsQuotaAllocationEnabled
                        END AS [DeliverIsQuotaAllocationEnabled]
                        
                FROM    dbo.Account A
				
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency],[IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )

                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
            
                        WHERE  ( A.ID IN ( SELECT  *
									FROM    @subAccounts )
								)
								
                                
     -- Get the accounts
        SET NOCOUNT OFF
        
        ;WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
        
       
        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.CollaborateContractStartDate,
				TBL.CollaborateIsMonthlyBillingFrequency,
				TBL.CollaborateIsQuotaAllocationEnabled, 
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                TBL.DeliverContractStartDate,
                TBL.DeliverIsMonthlyBillingFrequency,
                TBL.DeliverIsQuotaAllocationEnabled,
                 ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
				 (SELECT PathString) AS AccountPath
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID	
                INNER JOIN AccountTree act ON a.ID = act.AccountID		
    END

GO