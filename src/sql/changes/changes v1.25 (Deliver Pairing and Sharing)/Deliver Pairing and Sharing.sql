USE GMGCoZone
GO
--Add Submenu Item - Add Edit Color Proof Instance
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditColorProofInstance', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AECI' , -- Key - nvarchar(4)
          'Add Edit Color Proof Instance' , -- Name - nvarchar(64)
          'Add Edit Color Proof Instance'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SEDS'
GO

--Add Submenu Item - Add Edit Color Proof Workflow
DECLARE @controllerId INT

INSERT  INTO dbo.ControllerAction
        ( Controller, Action, Parameters )
VALUES  ( 'Settings', -- Controller - nvarchar(128)
          'AddEditColorProofWorkflow', -- Action - nvarchar(128)
          ''  -- Parameters - nvarchar(128)
          )
SET @controllerId = SCOPE_IDENTITY()

INSERT  INTO dbo.MenuItem
        ( ControllerAction ,
          Parent ,
          Position ,
          IsAdminAppOwned ,
          IsAlignedLeft ,
          IsSubNav ,
          IsTopNav ,
          IsVisible ,
          [Key] ,
          Name ,
          Title
        )
   SELECT @controllerId , -- ControllerAction - int
          mi.ID , -- Parent - int
          0 , -- Position - int
          1 , -- IsAdminAppOwned - bit
          1 , -- IsAlignedLeft - bit
          0 , -- IsSubNav - bit
          0 , -- IsTopNav - bit
          0 , -- IsVisible - bit
          'AECW' , -- Key - nvarchar(4)
          'Add Edit Color Proof Workflow' , -- Name - nvarchar(64)
          'Add Edit Color Proof Workflow'  -- Title - nvarchar(128)
        FROM dbo.MenuItem mi
		where [Key] = 'SEDS'
GO

DECLARE @atrId INT
DECLARE ATRCursor CURSOR LOCAL FOR  
SELECT DISTINCT ATR.ID
FROM    dbo.AccountTypeRole ATR
INNER JOIN dbo.AccountType AT ON AT.ID = ATR.AccountType
INNER JOIN dbo.Role R ON ATR.Role = R.ID
INNER JOIN dbo.AppModule AM ON R.AppModule = AM.ID
WHERE (R.[Key] = 'ADM') AND AT.[Key] in ('GMHQ','SBSY','DELR','CLNT','SBSC') AND AM.[Key] = 2
OPEN ATRCursor
FETCH NEXT FROM ATRCursor INTO @atrId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        INSERT  INTO dbo.MenuItemAccountTypeRole
                ( MenuItem ,
                  AccountTypeRole 
                )
                SELECT  MI.ID ,
                        @atrId
                FROM    dbo.MenuItem MI
                WHERE   MI.[Key] IN ('AECW','AECI')
        FETCH NEXT FROM ATRCursor INTO @atrId
    END
CLOSE ATRCursor
DEALLOCATE ATRCursor
GO

-- Remove unused columns from ColorProof Instance
ALTER TABLE dbo.ColorProofInstance 
DROP COLUMN Phone, Company, Website
GO

-- Add pairing code expiration date
ALTER TABLE dbo.ColorProofInstance
ADD PairingCodeExpirationDate DATETIME
GO

-- Set computer name column as nullable
ALTER TABLE dbo.ColorProofInstance
ALTER COLUMN ComputerName NVARCHAR(50) NULL
GO

-- Workflow Share Table
IF ( NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'SharedColorProofWorkflow' )
   )
	BEGIN
		CREATE TABLE [dbo].[SharedColorProofWorkflow]
		(
			[ID] [int] IDENTITY(1,1) NOT NULL,
			[ColorProofCoZoneWorkflow] [int] NOT NULL,
			[User] [int] NOT NULL,
			[IsAccepted] [bit] NULL,
			[IsEnabled] [bit] NOT NULL,
			[Token] nvarchar(64) NOT NULL,
			
			CONSTRAINT [PK_SharedColorProofWorkflow] PRIMARY KEY CLUSTERED 
				(
					[ID] ASC
				)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF,
						ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
		)
		ON [PRIMARY]

		ALTER TABLE [dbo].[SharedColorProofWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_SharedColorProofWorkflow_ColorProofCoZoneWorkflow] FOREIGN KEY([ColorProofCoZoneWorkflow])
		REFERENCES [dbo].[ColorProofCoZoneWorkflow] ([ID])

		ALTER TABLE [dbo].[SharedColorProofWorkflow] CHECK CONSTRAINT [FK_SharedColorProofWorkflow_ColorProofCoZoneWorkflow]

		ALTER TABLE [dbo].[SharedColorProofWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_SharedColorProofWorkflow_User] FOREIGN KEY([User])
		REFERENCES [dbo].[User] ([ID])

		ALTER TABLE [dbo].[SharedColorProofWorkflow] CHECK CONSTRAINT [FK_SharedColorProofWorkflow_User]		
	END
ELSE
	PRINT 'Table SharedColorProofWorkflow already exists'
GO

--Add New Statuses for ColorProof Instance
INSERT INTO [dbo].[ColorProofInstanceState]
           ([Key]
           ,[Name])
     VALUES
           (5
           ,'Error')
GO

UPDATE [dbo].[ColorProofInstanceState]
SET Name = 'Pending'
WHERE [Key] = 1
GO

-- Add IsFromCurrentAccount column
ALTER VIEW [dbo].[ColorProofWorkflowInstanceView]
AS
SELECT  dbo.ColorProofCoZoneWorkflow.ID AS ColorProofCoZoneWorkflowID ,
        dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow ,
        dbo.ColorProofCoZoneWorkflow.Name AS CoZoneName ,
        dbo.ColorProofCoZoneWorkflow.IsAvailable ,
        dbo.ColorProofCoZoneWorkflow.TransmissionTimeout ,
        dbo.ColorProofWorkflow.ID AS ColorProofWorkflowID ,
        dbo.ColorProofWorkflow.Guid ,
        dbo.ColorProofWorkflow.Name ,
        dbo.ColorProofWorkflow.ProofStandard ,
        dbo.ColorProofWorkflow.MaximumUsablePaperWidth ,
        dbo.ColorProofWorkflow.IsActivated ,
        dbo.ColorProofWorkflow.IsInlineProofVerificationSupported ,
        dbo.ColorProofWorkflow.SupportedSpotColors ,
        dbo.ColorProofWorkflow.ColorProofInstance,
        dbo.ColorProofInstance.SystemName,
        convert(bit, 0) AS IsFromCurrentAccount          
FROM    dbo.ColorProofCoZoneWorkflow
        INNER JOIN dbo.ColorProofWorkflow ON dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow = dbo.ColorProofWorkflow.ID
        INNER JOIN dbo.ColorProofInstance ON dbo.ColorProofWorkflow.ColorProofInstance = dbo.ColorProofInstance.ID
WHERE   dbo.ColorProofInstance.IsDeleted = 0
        AND dbo.ColorProofCoZoneWorkflow.IsDeleted = 0
GO

ALTER PROCEDURE [dbo].[SPC_ReturnColorProofInstanceLoginByPairingCode]
    (
      @P_PairingCode VARCHAR(255) 
    )
AS 
    BEGIN

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
        SELECT DISTINCT
               ci.[ID]
			  ,ci.[Guid]
			  ,ci.[Owner]
			  ,ci.[Version]
			  ,ci.[UserName]			  
			  ,ci.[ComputerName]
			  ,ci.[SystemName]
			  ,ci.[SerialNumber]
			  ,ci.[IsOnline]
			  ,ci.[LastSeen]
			  ,ci.[State]
			  ,ci.[AdminName]
			  ,ci.[Address]
			  ,ci.[Email]
			  ,ci.[WorkflowsLastModifiedTimestamp]
			  ,ci.[IsDeleted]
			  ,ci.[PairingCode]
			  ,ci.[PairingCodeExpirationDate]           
              ,CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), ci.Password, 2 ))) AS [Password]
        FROM   [dbo].[ColorProofInstance] ci
                
        WHERE               
               ci.PairingCode = @P_PairingCode
               AND ci.IsDeleted = 0
    END
GO

--Add new events for Deliver module
DECLARE @GroupId int

INSERT INTO [GMGCoZone].[dbo].[EventGroup]
           ([Key]
           ,[Name])
     VALUES
           ('IU'
           ,'InvitedUser')
		   
SET @GroupId = SCOPE_IDENTITY()

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IAR', 'Access was revoked')

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IUD', 'User was deleted') 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'IFS', 'File Submitted')      

INSERT INTO [GMGCoZone].[dbo].[EventGroup]
           ([Key]
           ,[Name])
     VALUES
           ('HA'
           ,'HostAdmin')
		   
SET @GroupId = SCOPE_IDENTITY()

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'HSA', 'Share request was accepted') 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'HSD', 'Shared workflow was deleted')

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'HUD', 'User was deleted') 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'HFS', 'File Submitted')    

GO

DECLARE @PresetId INT
DECLARE @RoleId INT
DECLARE @ModuleId INT

--Deliver Module
SET @ModuleId = (SELECT TOP 1 am.ID from dbo.[AppModule] am
				WHERE am.[Key] = 2);

----Low Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'L');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS', 'HFS')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS')   

	   
----Medium Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'M');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS', 'HFS')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS')
	   
----High Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'H');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS', 'IAR', 'IUD', 'HSA', 'HSD', 'HUD', 'HFS')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS', 'IAR', 'IUD')
	   
----Custom Preset
SET @PresetId = (SELECT TOP 1 pr.ID from dbo.[Preset] pr
				WHERE pr.[Key] = 'C');
				
------ Administrator User
SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'ADM');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS', 'IAR', 'IUD', 'HSA', 'HSD', 'HUD', 'HFS')
	   
------ Manager User

SET @RoleId = (SELECT TOP 1 rl.ID from dbo.[Role] rl
			  WHERE rl.AppModule = @ModuleId AND rl.[Key] = 'MAN');

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	   SELECT @RoleId, @PresetId, ev.ID
	   FROM dbo.EventType ev
	   WHERE ev.[Key] IN ('IFS', 'IAR', 'IUD')
	   
------ Add new columns to NotificationEmailQueue table
ALTER TABLE [dbo].[NotificationEmailQueue]
ADD WorkflowId INT,
	WorkflowName NVARCHAR(256),
	ExternalUser INT
GO

------------------------------------------------------------------------------------------------------------------------Launched on Debug