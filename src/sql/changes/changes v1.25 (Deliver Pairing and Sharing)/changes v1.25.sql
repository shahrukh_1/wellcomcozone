USE GMGCoZone
GO
--Plan changes for Profile Server
ALTER TABLE [dbo].[BillingPlan]
ADD ColorProofConnections int
GO

--Set Default value for deliver billing plans
UPDATE bp
  SET bp.ColorProofConnections = 10
  FROM [dbo].[BillingPlan] AS bp
  INNER JOIN [dbo].[BillingPlanType] AS bpt ON bp.[Type] = bpt.ID
  INNER JOIN [dbo].[AppModule] AS apm ON bpt.AppModule = apm.ID
  WHERE apm.[Key] = 2;
GO

------------------------------------------------------------------------------------------------------------------------Launched on Debug