
ALTER TABLE [GMGCoZone].[dbo].[SoftProofingSessionParams]
ADD [HighResolution] BIT NOT NULL DEFAULT(0)

GO

ALTER TABLE [GMGCoZone].[dbo].[ApprovalAnnotation] 
ADD AnnotatedOnImageWidth int  NOT NULL DEFAULT(1)

GO

ALTER TABLE [dbo].[SoftProofingSessionParams]
ADD [HighResolutionInProgress] BIT NOT NULL DEFAULT(0)

GO

--adapt approvalannotation column for high res
update [GMGCoZone].[dbo].[ApprovalAnnotation] set [GMGCoZone].[dbo].[ApprovalAnnotation].[AnnotatedOnImageWidth] = ap.[OutputRenderWidth] 
from [GMGCoZone].[dbo].[ApprovalPage] ap inner join [GMGCoZone].[dbo].[ApprovalAnnotation] aa on ap.ID = aa.Page
where ap.[OutputRenderWidth] is not null
GO
----------------------------------------------------------------------Launched on Test
----------------------------------------------------------------------Launched on Stage
----------------------------------------------------------------------Launched on Production
