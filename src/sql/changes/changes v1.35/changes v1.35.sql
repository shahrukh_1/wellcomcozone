USE [GMGCoZone]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


ALTER PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN		
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath	
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID	
		
	WHERE	
		s.[Key]<>'D' AND
		a.ID <> @P_LoggedAccount AND
		a.IsTemporary = 0 AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END
GO

