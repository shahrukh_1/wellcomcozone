USE GMGCoZone
GO
ALTER PROCEDURE [dbo].[SPC_ReturnUserLogin] (
	@P_Account int,
	@P_Username varchar(255),
	@P_Password varchar(255)
)
AS
BEGIN
	SELECT	DISTINCT
			u.[ID],
			u.[Account],
			u.[Status],  
			u.[Username],
			u.[Password],
			u.[GivenName],
			u.[FamilyName],
			u.[EmailAddress],
			u.[OfficeTelephoneNumber],
			u.[MobileTelephoneNumber],
			u.[HomeTelephoneNumber],
			u.[Guid],
			u.[PhotoPath],
			us.[ID] AS [StatusId],
			us.[Key],
			us.[Name],
			u.[Creator],
			u.[CreatedDate],
			u.[Modifier],
			u.[ModifiedDate],
			u.[DateLastLogin],
			u.[IncludeMyOwnActivity],
			u.NeedReLogin
	FROM	[dbo].[User] u
			JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID 
	WHERE 	u.[Account] = @P_Account AND
			u.[Username] = @P_Username AND 
			u.[Password] = CONVERT(varchar(255), HashBytes('SHA1', @P_Password)) AND
			(us.[Key] = 'A' OR us.[Key] = 'I')
END
GO
