-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]
GO
/****** Object:  UserDefinedTableType [dbo].[AccountIDsTableType]    Script Date: 8/8/2018 11:34:23 AM ******/
CREATE TYPE [dbo].[AccountIDsTableType] AS TABLE(
	[ID] [int] NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting]    Script Date: 8/8/2018 11:34:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting]
(
	@P_Approval INT, @P_Account INT
)
RETURNS bit
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @returnSetting bit;
	
	SET @returnSetting = (SELECT TOP 1 f.AllCollaboratorsDecisionRequired
						 FROM dbo.FolderApproval fa
						 JOIN dbo.Folder f ON fa.Folder = f.ID
						 WHERE fa.Approval = @P_Approval)
	
	IF @returnSetting IS NULL
	  BEGIN
	     DECLARE @accSetting NVARCHAR(16);
	     
	     SET @accSetting = ISNULL((SELECT acset.Value FROM dbo.AccountSetting acset WHERE Account = @P_Account AND Name = 'AllCollaboratorsDecisionRequired'), 'False')
	      
	      IF @accSetting = 'False'
	      BEGIN
	         SET @returnSetting = 0;
	      END
	      ELSE
	         SET @returnSetting = 1;  	   	    					 
	  END
	  				   
    RETURN @returnSetting
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetApprovalApprovedDate]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[GetApprovalApprovedDate]
(
	@P_Approval INT, @P_PrimaryDecisionMaker INT, @P_ExternalPrimaryDecisionMaker INT
)
RETURNS DateTime
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @returnSetting DateTime;
	
	SET @returnSetting =  (SELECT CASE WHEN (@P_PrimaryDecisionMaker = 0 AND @P_ExternalPrimaryDecisionMaker = 0 ) 
									THEN(						     
											SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
																		   WHERE acd.Approval = @P_Approval AND acd.Decision IS null)
															
															   and not exists (SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
																				join ApprovalDecision ad on Decision = ad.ID 
																				WHERE acd.Approval = @P_Approval AND ad.[Key] != 'AWC' and ad.[Key] != 'APD')
															  )
											 THEN
												(	SELECT TOP 1 CompletedDate FROM ApprovalCollaboratorDecision acd															
													WHERE acd.Approval = @P_Approval
													ORDER BY CompletedDate DESC															
												 )
											 ELSE NULL
											 END 											    				
										)		
									WHEN (@P_PrimaryDecisionMaker != 0)
									  THEN
										(SELECT TOP 1 CompletedDate FROM ApprovalCollaboratorDecision
												join ApprovalDecision ad on Decision = ad.ID 												
												WHERE Approval = @P_Approval AND Collaborator = @P_PrimaryDecisionMaker AND (ad.[Key] = 'AWC' or ad.[Key] = 'APD')
										)
									ELSE
									 (SELECT TOP 1 CompletedDate FROM ApprovalCollaboratorDecision
											join ApprovalDecision ad on Decision = ad.ID 	
											WHERE Approval = @P_Approval AND ExternalCollaborator = @P_ExternalPrimaryDecisionMaker AND (ad.[Key] = 'AWC' or ad.[Key] = 'APD')
									  )
									END	
							)	  				   
    RETURN @returnSetting
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
CREATE FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac			
			 ON  a.ID = ac.Approval
			    JOIN dbo.[User] us
			 ON ac.Collaborator = us.ID
			    JOIN dbo.[UserStatus] ust
			 ON us.Status = ust.ID   
		WHERE a.ID = @P_Approval AND ust.[Key] != 'D' ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCreator IS NULL AND aa.ExternalModifier IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCreator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCreator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;

GO
/****** Object:  UserDefinedFunction [dbo].[GetApprovalIsCompleteChanges]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[GetApprovalIsCompleteChanges]
(
	@P_Approval INT
)
RETURNS bit
WITH EXECUTE AS CALLER
AS
BEGIN
	DECLARE @returnSetting bit;
		     
	     SET @returnSetting = (SELECT CASE WHEN (not exists (select an.ID  
															     from dbo.ApprovalAnnotation an
															     join dbo.ApprovalAnnotationStatus ans on an.Status = ans.ID
															     join dbo.ApprovalPage app on an.Page = app.ID
															     where app.Approval = @P_Approval and an.Parent is null and ans.[Key] != 'A')
												)
											THEN 1
											ELSE 0
											END
								)
	      
  	   	    					 	  				   
   RETURN @returnSetting
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetApprovalPrimaryDecisionMakers]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetApprovalPrimaryDecisionMakers]
(
	@P_CurrentPhase INT,
	@P_Approval INT,
	@P_PrimaryDecisionMaker INT,
	@P_ExternalPrimaryDecisionMaker INT
)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN	
	DECLARE @String NVARCHAR(MAX);
	DECLARE @Names NVARCHAR(MAX);
	SET @String =  ISNULL((SELECT CASE WHEN @P_CurrentPhase IS NOT NULL 
								THEN(SELECT CASE WHEN (SELECT TOP 1 dt.[Key]  
													   FROM dbo.DecisionType dt
													   JOIN dbo.ApprovalJobPhase ajp on dt.ID = ajp.DecisionType
													   JOIN dbo.ApprovalJobPhaseApproval a ON ajp.ID = a.Phase											   
													   WHERE a.Approval = @P_Approval AND a.Phase = @P_CurrentPhase) = 'PDM'
													   								
												 THEN (SELECT CASE WHEN ISNULL(@P_PrimaryDecisionMaker, 0) != 0 
																THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = @P_PrimaryDecisionMaker)
															 WHEN ISNULL(@P_ExternalPrimaryDecisionMaker, 0) != 0
																THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = @P_ExternalPrimaryDecisionMaker)
															 ELSE NULL
															 END)
																									 			 
												 ELSE (SELECT STUFF(( SELECT ', ' + result.Name
																	  FROM
																			(SELECT u.GivenName + ' ' + u.FamilyName As Name
																			 FROM [User] u
																			 INNER JOIN ApprovalCollaborator ac ON u.ID = ac.Collaborator
																			 INNER JOIN ApprovalCollaboratorRole acr on ac.ApprovalCollaboratorRole = acr.ID
																			 INNER JOIN dbo.UserStatus us ON u.[Status] = us.ID
																			 WHERE ac.Approval = @P_Approval AND ac.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND us.[Key] != 'D'
																			 UNION
																			 SELECT ec.GivenName + ' ' + ec.FamilyName As Name
																			 FROM ExternalCollaborator ec
																			 JOIN SharedApproval sa ON ec.ID = sa.ExternalCollaborator
																			 JOIN ApprovalCollaboratorRole acr on sa.ApprovalCollaboratorRole = acr.ID
																			 WHERE sa.Approval = @P_Approval AND sa.Phase = @P_CurrentPhase AND acr.[Key] = 'ANR' AND ec.IsDeleted = 0) result 
																			 FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)'),1,2,'') )
											   END
										 )
								   ELSE NULL
								   END)	,											 
		           (SELECT CASE WHEN ISNULL(@P_PrimaryDecisionMaker, 0) != 0 
									THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = @P_PrimaryDecisionMaker)
								WHEN ISNULL(@P_ExternalPrimaryDecisionMaker, 0) != 0
									THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = @P_ExternalPrimaryDecisionMaker)
								ELSE ''
								END)
					)       
  	   	  RETURN  @String;					 	  				   
    
END


GO
/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----------

-- Create Get Billing Cycles function to support custom plans and not only attached
CREATE FUNCTION [dbo].[GetBillingCycles]
    (
      @LoggedAccountID INT ,
      @StartDate DATETIME ,
      @EndDate DATETIME ,
      @Accounts AccountIDsTableType READONLY 
    )
RETURNS  @BillingCycles TABLE 
(
    -- columns returned by the function
    AccountID INT,
    AppModule INT, 
    SelectedBillingPlan INT,
    CycleAllowedProofsCount INT,
    BillingPlanPrice INT,
    Collaborate NVARCHAR(MAX),
    Deliver NVARCHAR(MAX)
)
AS 
    BEGIN

        DECLARE @cycleStartDate DATETIME = @StartDate
        
        BEGIN 
			INSERT INTO @BillingCycles (AccountID, AppModule, SelectedBillingPlan, CycleAllowedProofsCount, BillingPlanPrice)
			SELECT
				A.ID,
				CASE 
					WHEN BPT2.AppModule IS NOT NULL THEN BPT2.AppModule
					ELSE BPT.AppModule
				END,
				CASE --
					WHEN 
						AT.[Key] = 'SBSC' THEN  SPI.SelectedBillingPlan
					ELSE 
						ASP.SelectedBillingPlan END AS SEL,
				CASE --
					WHEN 
						SPI.IsQuotaAllocationEnabled = 1 THEN SPI.NrOfCredits
					WHEN 
						AT.[Key] = 'SBSC' THEN BP.Proofs
					ELSE 
						BP2.Proofs END,
				CASE --
					WHEN ( @LoggedAccountID = 1 )
					THEN 
						CASE
							WHEN 
								AT.[Key] = 'SBSC' THEN BP.Price
							ELSE 
								BP2.Price 
						END
					ELSE 
						( ISNULL((SELECT   
										NewPrice
								  FROM     
										AttachedAccountBillingPlan
								  WHERE    
										Account = @LoggedAccountID AND 
										BillingPlan = CASE 
														WHEN SPI.SelectedBillingPlan IS NOT NULL THEN SPI.SelectedBillingPlan 
														ELSE ASP.SelectedBillingPlan 
													  END),
                           CASE
								WHEN 
									AT.[Key] = 'SBSC' THEN BP.Price
								ELSE 
									BP2.Price 
							END)
						 )  
					END
			FROM    
				Account A
				LEFT JOIN dbo.AccountType AT ON AT.ID = A.AccountType
				LEFT JOIN dbo.SubscriberPlanInfo SPI ON A.ID = SPI.Account
				LEFT JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
				LEFT JOIN dbo.BillingPlanType BPT ON BPT.ID = BP.[Type]
				LEFT JOIN dbo.AccountSubscriptionPlan ASP ON A.ID = ASP.Account
				LEFT JOIN dbo.BillingPlan BP2 ON ASP.SelectedBillingPlan = BP2.ID
				LEFT JOIN dbo.BillingPlanType BPT2 ON BPT2.ID = BP2.Type
			WHERE
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Collaborate')) OR
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Deliver')) OR
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT2.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Collaborate')) OR
				(A.ID IN (SELECT ID FROM @Accounts) AND BPT2.AppModule = (SELECT ID FROM dbo.AppModule WHERE NAME LIKE 'Deliver')) OR 
				(A.ID IN (SELECT ID FROM @Accounts))
				
		END			
																					
        SET @cycleStartDate = @StartDate
	
        WHILE ( datepart(year, @cycleStartDate) < datepart(year,@EndDate) OR
			  ( datepart(year, @cycleStartDate) = datepart(year,@EndDate) AND datepart(month, @cycleStartDate) <= datepart(month,@EndDate)) ) 
            BEGIN	
				       UPDATE @BillingCycles 
				       SET Collaborate =                           	
						CASE WHEN AppModule = 1 THEN (SELECT
							(ISNULL(Collaborate, '') + ',' +  CONVERT(NVARCHAR, LEFT(datename(month, @cycleStartDate), 3)) + '-' + CONVERT(NVARCHAR, datepart(year, @cycleStartDate) % 100) + '|' +  CONVERT(NVARCHAR, COALESCE(SUM(cr.ProofsUsed), 0)) + '|' + ( CASE WHEN (COALESCE(SUM(cr.ProofsUsed), 0) - CycleAllowedProofsCount) < 0 THEN '0' ELSE CONVERT(NVARCHAR, ((COALESCE(SUM(cr.ProofsUsed), 0) - CycleAllowedProofsCount))) END ) + '|' +  CONVERT(NVARCHAR, BillingPlanPrice) + '|' +  CONVERT(NVARCHAR, CycleAllowedProofsCount))
						FROM (
							SELECT
								COALESCE(SUM(d.ProofsPerJob), '0') AS ProofsUsed
							FROM(
								  SELECT
									  CEILING(COUNT(t.ID) / CAST(4 AS FLOAT)) AS ProofsPerJob
								  FROM 
									(
										SELECT ap.ID, j.Account, ap.CreatedDate, ap.FileName,ap.Job				
										FROM
												Job j
												INNER JOIN Approval ap ON j.ID = ap.Job
												WHERE
												j.Account = AccountID
												AND datepart(month, ap.CreatedDate) = datepart(month, @cycleStartDate)
												AND datepart(year, ap.CreatedDate) = datepart(year, @cycleStartDate)
												
										UNION ALL
										SELECT a.ID, a.Account, a.CreatedDate, a.JobName, a.Job		
												FROM dbo.ApprovalJobDeleteHistory a
												WHERE 
												a.Account = AccountID
												AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
												AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
												AND a.Job IS NOT NULL
									 ) t
								 GROUP BY t.Job
								)d
							UNION ALL
							SELECT 
								SUM(dn.ProofsPerJob) AS ProofsUsed
							FROM(
									SELECT 
										CEILING(COUNT(a.ID) / CAST(4 AS FLOAT)) AS ProofsPerJob
									FROM dbo.ApprovalJobDeleteHistory a
									WHERE 
									a.Account = AccountID
									AND datepart(month, a.CreatedDate) = datepart(month, @cycleStartDate)
									AND datepart(year, a.CreatedDate) = datepart(year, @cycleStartDate)
									AND a.Job IS NULL
									GROUP BY a.JobName
								)  dn
							  )cr
						) END,
					Deliver = 
				    CASE WHEN AppModule = 3 THEN (SELECT  
						(ISNULL(Deliver, '') + ',' +  CONVERT(NVARCHAR, LEFT(datename(month, @cycleStartDate), 3)) + '-' + CONVERT(NVARCHAR, datepart(year, @cycleStartDate) % 100) + '|' +  CONVERT(NVARCHAR, COALESCE(COUNT(dr.ID), 0)) + '|' +  (CASE WHEN (COUNT(dr.ID) - CycleAllowedProofsCount) < 0 THEN '0' ELSE CONVERT(NVARCHAR, (COUNT(dr.ID) - CycleAllowedProofsCount)) END) + '|' +  CONVERT(NVARCHAR, BillingPlanPrice) + '|' +  CONVERT(NVARCHAR, CycleAllowedProofsCount))
					 FROM (SELECT
								dj.ID 
							FROM Job j
							INNER JOIN dbo.DeliverJob dj ON j.ID = dj.Job
							WHERE
							j.Account = AccountID
							AND datepart(month, dj.CreatedDate) = datepart(month, @cycleStartDate)
							AND datepart(year, dj.CreatedDate) = datepart(year, @cycleStartDate)
							UNION ALL
							SELECT 
								d.ID 
							FROM dbo.DeliverJobDeleteHistory d
							WHERE 
							d.Account = AccountID
							AND datepart(month, d.CreatedDate) = datepart(month, @cycleStartDate)
							AND datepart(year, d.CreatedDate) = datepart(year, @cycleStartDate)
							) AS dr) END
				
			    SET @cycleStartDate = DATEADD(MM, 1, @cycleStartDate)
                    
            END
            
        RETURN  
	
    END


GO
/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  --Filter deleted users from approval & folder collaborators
CREATE FUNCTION [dbo].[GetFolderCollaborators] (@P_Folder int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT fcg.CollaboratorGroup
		FROM Folder f 
				JOIN FolderCollaboratorGroup fcg
					ON  f.ID = fcg.Folder
		WHERE f.ID = @P_Folder ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT fc.Collaborator
		FROM Folder f 
				JOIN FolderCollaborator fc
			 ON  f.ID = fc.Folder
				JOIN dbo.[User] us
			 ON fc.Collaborator = us.ID
			    JOIN dbo.[UserStatus] ust
			 ON us.Status = ust.ID   	
		WHERE f.ID = @P_Folder AND ust.[Key] != 'D' ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
			
	RETURN	(@GroupList + '#' + @CollaboratorList ) 

END;
GO
/****** Object:  UserDefinedFunction [dbo].[GetUserRoleByUserIdAndModuleId]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------------------------------------------
-- Get role for user and module
CREATE FUNCTION [dbo].[GetUserRoleByUserIdAndModuleId]
    (
      @UserId INT ,
      @ModuleId INT	
    )
RETURNS NVARCHAR(MAX)
AS 
    BEGIN
        DECLARE @result VARCHAR(MAX) = 'NON'
        
        SELECT  @result = R.[Key]
        FROM    dbo.[User] U
                INNER JOIN dbo.UserRole UR ON UR.[User] = U.ID
                INNER JOIN dbo.Role R ON UR.Role = R.ID
                INNER JOIN dbo.AppModule AM ON AM.ID = R.AppModule
        WHERE   U.ID = @userId
                AND Am.[Key] = @ModuleId

        RETURN @result ;
    END
GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SplitString](
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    
    RETURN
END
GO
/****** Object:  Table [dbo].[Account]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[SiteName] [nvarchar](128) NOT NULL,
	[Domain] [nvarchar](255) NOT NULL,
	[AccountType] [int] NOT NULL,
	[Parent] [int] NULL,
	[Owner] [int] NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[ContactFirstName] [nvarchar](128) NOT NULL,
	[ContactLastName] [nvarchar](128) NOT NULL,
	[ContactPhone] [nvarchar](20) NULL,
	[ContactEmailAddress] [nvarchar](64) NOT NULL,
	[HeaderLogoPath] [nvarchar](256) NULL,
	[LoginLogoPath] [nvarchar](256) NULL,
	[EmailLogoPath] [nvarchar](256) NULL,
	[CustomColor] [nvarchar](8) NULL,
	[CurrencyFormat] [int] NOT NULL,
	[Locale] [int] NOT NULL,
	[TimeZone] [nvarchar](128) NOT NULL,
	[DateFormat] [int] NOT NULL,
	[TimeFormat] [int] NOT NULL,
	[GPPDiscount] [decimal](4, 2) NULL,
	[NeedSubscriptionPlan] [bit] NOT NULL,
	[ContractPeriod] [int] NOT NULL,
	[ChildAccountsVolume] [int] NULL,
	[CustomFromAddress] [nvarchar](256) NULL,
	[CustomFromName] [nvarchar](256) NULL,
	[SuspendReason] [nvarchar](256) NULL,
	[IsSuspendedOrDeletedByParent] [bit] NOT NULL,
	[CustomDomain] [nvarchar](255) NULL,
	[IsCustomDomainActive] [bit] NOT NULL,
	[IsHideLogoInFooter] [bit] NOT NULL,
	[IsHideLogoInLoginFooter] [bit] NOT NULL,
	[IsNeedProfileManagement] [bit] NOT NULL,
	[IsNeedPDFsToBeColorManaged] [bit] NOT NULL,
	[IsRemoveAllGMGCollaborateBranding] [bit] NOT NULL,
	[Status] [int] NOT NULL,
	[IsEnabledLog] [bit] NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsHelpCentreActive] [bit] NOT NULL,
	[DealerNumber] [nvarchar](32) NULL,
	[CustomerNumber] [nvarchar](32) NULL,
	[VATNumber] [nvarchar](32) NULL,
	[IsAgreedToTermsAndConditions] [bit] NOT NULL,
	[Region] [nvarchar](128) NOT NULL,
	[IsTemporary] [bit] NOT NULL,
	[DisableLandingPage] [bit] NOT NULL,
	[DashboardToShow] [int] NOT NULL,
	[IsTrial] [bit] NOT NULL,
	[EnableNotificationsByDefault] [bit] NOT NULL,
	[UploadEngineSetings] [int] NULL,
	[FavIconPath] [nvarchar](256) NULL,
	[DeletedBy] [int] NULL,
	[DeletedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountBillingPlanChangeLog]    Script Date: 8/8/2018 11:34:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBillingPlanChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[LogMessage] [nvarchar](256) NOT NULL,
	[PreviousPlanAmount] [decimal](11, 2) NULL,
	[PreviousProofsAmount] [int] NULL,
	[GPPDiscount] [decimal](3, 2) NULL,
	[PreviousPlan] [int] NULL,
	[CurrentPlan] [int] NULL,
 CONSTRAINT [PK_AccountBillingPlanChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountBillingTransactionHistory]    Script Date: 8/8/2018 11:34:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBillingTransactionHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[BaseAmount] [decimal](11, 2) NOT NULL,
	[DiscountAmount] [decimal](11, 2) NOT NULL,
 CONSTRAINT [PK_AccountBillingTransactionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountCustomICCProfile]    Script Date: 8/8/2018 11:34:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountCustomICCProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Guid] [nvarchar](64) NOT NULL,
	[Uploader] [int] NULL,
	[UploadDate] [datetime] NOT NULL,
	[IsRGB] [bit] NULL,
	[IsValid] [bit] NULL,
	[IsActive] [bit] NOT NULL,
	[L] [float] NULL,
	[a] [float] NULL,
	[b] [float] NULL,
	[PrintSubstrate] [int] NULL,
 CONSTRAINT [PK_AccountCustomICCProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountHelpCentre]    Script Date: 8/8/2018 11:34:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountHelpCentre](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[ContactEmail] [nvarchar](64) NOT NULL,
	[ContactPhone] [nvarchar](32) NULL,
	[SupportDays] [nvarchar](128) NULL,
	[SupportHours] [nvarchar](64) NULL,
	[UserGuideLocation] [nvarchar](256) NULL,
	[UserGuideName] [nvarchar](256) NULL,
	[UserGuideUpdatedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_AccountHelpCentre] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountNotificationPreset]    Script Date: 8/8/2018 11:34:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountNotificationPreset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[DisablePersonalNotifications] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_AccountNotificationPreset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountNotificationPresetEventType]    Script Date: 8/8/2018 11:34:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountNotificationPresetEventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationPreset] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_AccountNotificationPresetEventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountPricePlanChangedMessage]    Script Date: 8/8/2018 11:34:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPricePlanChangedMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedBillingPlan] [int] NOT NULL,
	[ChildAccount] [int] NOT NULL,
 CONSTRAINT [PK_AccountPricePlanChangedMessage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountSetting]    Script Date: 8/8/2018 11:34:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[ValueDataType] [int] NOT NULL,
	[Description] [nvarchar](512) NULL,
	[Creator] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Modifier] [int] NULL,
	[ModifiedData] [datetime] NULL,
 CONSTRAINT [PK_AccountSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountStatus]    Script Date: 8/8/2018 11:34:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AccountStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountStorageUsage]    Script Date: 8/8/2018 11:34:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountStorageUsage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[UsedStorage] [float] NOT NULL,
 CONSTRAINT [PK_AccountStorageUsage_Id] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountSubscriptionPlan]    Script Date: 8/8/2018 11:34:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountSubscriptionPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SelectedBillingPlan] [int] NOT NULL,
	[IsMonthlyBillingFrequency] [bit] NOT NULL,
	[ChargeCostPerProofIfExceeded] [bit] NOT NULL,
	[ContractStartDate] [datetime2](7) NULL,
	[Account] [int] NOT NULL,
	[IsManageAPI] [bit] NOT NULL,
	[AccountPlanActivationDate] [datetime2](7) NULL,
	[AllowOverdraw] [bit] NOT NULL,
 CONSTRAINT [PK_AccountSubscriptionPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountTheme]    Script Date: 8/8/2018 11:34:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountTheme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Key] [nvarchar](1) NOT NULL,
	[ThemeName] [nvarchar](25) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_AccountTheme_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountType]    Script Date: 8/8/2018 11:34:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountTypeRole]    Script Date: 8/8/2018 11:34:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountTypeRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [int] NOT NULL,
	[Role] [int] NOT NULL,
 CONSTRAINT [PK_AccountTypeRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdvancedSearch]    Script Date: 8/8/2018 11:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedSearch](
	[AdvancedSearchId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[OverdueFiles] [bit] NULL,
	[MinTimeDeadline] [datetime2](7) NOT NULL,
	[MaxTimeDeadline] [datetime2](7) NOT NULL,
	[WithinATimeframe] [bit] NULL,
	[JobTitle] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[AdvancedSearchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdvancedSearchApprovalPhase]    Script Date: 8/8/2018 11:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedSearchApprovalPhase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AdvancedSearchId] [int] NOT NULL,
	[ApprovalPhaseId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdvancedSearchGroups]    Script Date: 8/8/2018 11:34:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedSearchGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AdvancedSearchId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdvancedSearchUser]    Script Date: 8/8/2018 11:34:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedSearchUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AdvancedSearchId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdvancedSearchWorkflow]    Script Date: 8/8/2018 11:34:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedSearchWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AdvancedSearchId] [int] NOT NULL,
	[Workflow] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppModule]    Script Date: 8/8/2018 11:34:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppModule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AppModule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Approval]    Script Date: 8/8/2018 11:34:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Approval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorMessage] [nvarchar](512) NULL,
	[FileName] [nvarchar](128) NOT NULL,
	[FolderPath] [nvarchar](512) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Job] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[Deadline] [datetime2](7) NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsPageSpreads] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LockProofWhenAllDecisionsMade] [bit] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[OnlyOneDecisionRequired] [bit] NOT NULL,
	[PrimaryDecisionMaker] [int] NULL,
	[AllowDownloadOriginal] [bit] NOT NULL,
	[FileType] [int] NOT NULL,
	[Size] [decimal](18, 2) NOT NULL,
	[Owner] [int] NOT NULL,
	[IsError] [bit] NOT NULL,
	[Type] [int] NOT NULL,
	[TranscodingJobId] [nvarchar](256) NULL,
	[ProcessingInProgress] [varchar](36) NULL,
	[SubmitGroupKey] [varchar](36) NULL,
	[ScreenshotUrl] [nvarchar](1024) NULL,
	[WebPageSnapshotDelay] [int] NULL,
	[StartProcessingDate] [datetime] NULL,
	[ExternalPrimaryDecisionMaker] [int] NULL,
	[JobSource] [int] NOT NULL,
	[FTPSourceFolder] [nvarchar](128) NULL,
	[DeletePermanently] [bit] NOT NULL,
	[LockProofWhenFirstDecisionsMade] [bit] NOT NULL,
	[CurrentPhase] [int] NULL,
	[AllowOtherUsersToBeAdded] [bit] NOT NULL,
	[VersionSufix] [nvarchar](36) NULL,
	[RestrictedZoom] [bit] NOT NULL,
	[OverdueNotificationSent] [bit] NULL,
	[SOADState] [int] NOT NULL,
 CONSTRAINT [PK_Approval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalAnnotation]    Script Date: 8/8/2018 11:34:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[Parent] [int] NULL,
	[Page] [int] NOT NULL,
	[OrderNumber] [int] NULL,
	[Creator] [int] NULL,
	[Modifier] [int] NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[ReferenceFilepath] [nvarchar](512) NULL,
	[AnnotatedDate] [datetime2](7) NOT NULL,
	[CommentType] [int] NOT NULL,
	[StartIndex] [int] NULL,
	[EndIndex] [int] NULL,
	[HighlightType] [int] NULL,
	[CrosshairXCoord] [int] NULL,
	[CrosshairYCoord] [int] NULL,
	[HighlightData] [nvarchar](max) NULL,
	[IsPrivate] [bit] NOT NULL,
	[TimeFrame] [int] NULL,
	[ExternalModifier] [int] NULL,
	[ExternalCreator] [int] NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[machineId] [nvarchar](64) NOT NULL,
	[CalibrationStatus] [int] NOT NULL,
	[Phase] [int] NULL,
	[ViewingCondition] [int] NULL,
	[AnnotatedOnImageWidth] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalAnnotation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalAnnotationAttachment]    Script Date: 8/8/2018 11:34:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
	[Guid] [nvarchar](36) NULL,
 CONSTRAINT [PK_ApprovalAnnotationAttachment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalAnnotationMarkup]    Script Date: 8/8/2018 11:34:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationMarkup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[X] [decimal](22, 8) NOT NULL,
	[Y] [decimal](22, 8) NOT NULL,
	[Width] [decimal](22, 8) NOT NULL,
	[Height] [decimal](22, 8) NOT NULL,
	[Color] [nvarchar](8) NULL,
	[Size] [int] NULL,
	[BackgroundColor] [nvarchar](8) NULL,
	[Rotation] [int] NULL,
	[Shape] [int] NULL,
 CONSTRAINT [PK_ApprovalAnnotationTypeMarkup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalAnnotationPrivateCollaborator]    Script Date: 8/8/2018 11:34:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationPrivateCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_ApprovalAnnotationPrivateCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalAnnotationStatus]    Script Date: 8/8/2018 11:34:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_ApprovalAnnotationStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalAnnotationStatusChangeLog]    Script Date: 8/8/2018 11:34:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationStatusChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Annotation] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Modifier] [int] NULL,
	[ExternalModifier] [int] NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ApprovalAnnotationStatusChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCollaborator]    Script Date: 8/8/2018 11:34:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[ApprovalCollaboratorRole] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
	[Phase] [int] NULL,
	[SOADState] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCollaboratorDecision]    Script Date: 8/8/2018 11:34:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCollaboratorDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Decision] [int] NULL,
	[Collaborator] [int] NULL,
	[ExternalCollaborator] [int] NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
	[OpenedDate] [datetime2](7) NULL,
	[CompletedDate] [datetime2](7) NULL,
	[Phase] [int] NULL,
 CONSTRAINT [PK_ApprovalCollaboratorDecisionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCollaboratorGroup]    Script Date: 8/8/2018 11:34:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
	[Phase] [int] NULL,
 CONSTRAINT [PK_ApprovalCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCollaboratorRole]    Script Date: 8/8/2018 11:34:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCollaboratorRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_ApprovalCollaboratorRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCommentType]    Script Date: 8/8/2018 11:34:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCommentType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_ApprovalCommentType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCustomDecision]    Script Date: 8/8/2018 11:34:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCustomDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Decision] [int] NOT NULL,
	[Name] [nvarchar](64) NULL,
	[Locale] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalCustomDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalCustomICCProfile]    Script Date: 8/8/2018 11:34:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalCustomICCProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomICCProfile] [int] NOT NULL,
	[Approval] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalCustomICCProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalDecision]    Script Date: 8/8/2018 11:34:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
	[Description] [nvarchar](128) NULL,
 CONSTRAINT [PK_ApprovalDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalEmbeddedProfile]    Script Date: 8/8/2018 11:34:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalEmbeddedProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NULL,
	[Name] [nvarchar](128) NOT NULL,
	[IsRGB] [bit] NOT NULL,
	[L] [float] NULL,
	[a] [float] NULL,
	[b] [float] NULL,
	[PrintSubstrate] [int] NULL,
 CONSTRAINT [PK_ApprovalEmbeddedProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalJobDeleteHistory]    Script Date: 8/8/2018 11:34:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalJobDeleteHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[JobName] [nvarchar](128) NULL,
	[Job] [int] NULL,
 CONSTRAINT [PK_ApprovalJobDeleteHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalJobPhase]    Script Date: 8/8/2018 11:34:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalJobPhase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalJobWorkflow] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Position] [int] NOT NULL,
	[DecisionType] [int] NOT NULL,
	[PrimaryDecisionMaker] [int] NULL,
	[ExternalPrimaryDecisionMaker] [int] NULL,
	[ApprovalTrigger] [int] NOT NULL,
	[ShowAnnotationsToUsersOfOtherPhases] [bit] NOT NULL,
	[ShowAnnotationsOfOtherPhasesToExternalUsers] [bit] NOT NULL,
	[PhaseTemplateID] [int] NOT NULL,
	[DeadlineUnitNumber] [int] NULL,
	[DeadlineUnit] [int] NULL,
	[DeadlineTrigger] [int] NULL,
	[Deadline] [datetime2](7) NULL,
	[IsActive] [bit] NOT NULL,
	[OverdueNotificationSent] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalJobPhase] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalJobPhaseApproval]    Script Date: 8/8/2018 11:34:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalJobPhaseApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Phase] [int] NOT NULL,
	[PhaseMarkedAsCompleted] [bit] NOT NULL,
	[SOADState] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalJobPhaseApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalJobViewingCondition]    Script Date: 8/8/2018 11:34:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalJobViewingCondition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Job] [int] NOT NULL,
	[ViewingCondition] [int] NOT NULL,
 CONSTRAINT [Pk_ApprovalJobViewingCondition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalJobWorkflow]    Script Date: 8/8/2018 11:34:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalJobWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Job] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[RerunWorkflow] [bit] NOT NULL,
 CONSTRAINT [PK_ApprovalJobWorkflow] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalPage]    Script Date: 8/8/2018 11:34:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalPage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NOT NULL,
	[Approval] [int] NOT NULL,
	[PageSmallThumbnailHeight] [int] NOT NULL,
	[PageSmallThumbnailWidth] [int] NOT NULL,
	[PageLargeThumbnailHeight] [int] NOT NULL,
	[PageLargeThumbnailWidth] [int] NOT NULL,
	[DPI] [int] NOT NULL,
	[Progress] [int] NOT NULL,
	[OriginalImageHeight] [int] NULL,
	[OriginalImageWidth] [int] NULL,
	[OutputRenderHeight] [int] NULL,
	[OutputRenderWidth] [int] NULL,
 CONSTRAINT [PK_ApprovalPage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalPhase]    Script Date: 8/8/2018 11:34:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalPhase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalWorkflow] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Position] [int] NOT NULL,
	[DecisionType] [int] NOT NULL,
	[PrimaryDecisionMaker] [int] NULL,
	[ExternalPrimaryDecisionMaker] [int] NULL,
	[ApprovalTrigger] [int] NOT NULL,
	[ShowAnnotationsToUsersOfOtherPhases] [bit] NOT NULL,
	[ShowAnnotationsOfOtherPhasesToExternalUsers] [bit] NOT NULL,
	[DeadlineUnitNumber] [int] NULL,
	[DeadlineUnit] [int] NULL,
	[DeadlineTrigger] [int] NULL,
	[Deadline] [datetime2](7) NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[ApprovalShouldBeViewedByAll] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalPhase] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalPhaseCollaborator]    Script Date: 8/8/2018 11:34:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalPhaseCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Phase] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[ApprovalPhaseCollaboratorRole] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalPhaseCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalPhaseCollaboratorGroup]    Script Date: 8/8/2018 11:34:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalPhaseCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Phase] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalPhaseCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalSeparationPlate]    Script Date: 8/8/2018 11:34:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalSeparationPlate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Page] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[ChannelIndex] [int] NULL,
	[IsSpotChannel] [bit] NOT NULL,
	[RGBHex] [nvarchar](12) NULL,
 CONSTRAINT [PK_ApprovalSeparationPlate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalSetting]    Script Date: 8/8/2018 11:34:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalSetting](
	[Approval] [int] NOT NULL,
	[UpdateJobName] [bit] NULL,
 CONSTRAINT [PK__Approval__83F15DF87F16D496] PRIMARY KEY CLUSTERED 
(
	[Approval] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalType]    Script Date: 8/8/2018 11:34:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ApprovalType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalUserRecycleBinHistory]    Script Date: 8/8/2018 11:34:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalUserRecycleBinHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[User] [int] NOT NULL,
 CONSTRAINT [PK_ApprovalDeleteHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalUserViewInfo]    Script Date: 8/8/2018 11:34:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalUserViewInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[User] [int] NULL,
	[ViewedDate] [datetime2](7) NOT NULL,
	[Phase] [int] NULL,
	[ExternalUser] [int] NULL,
 CONSTRAINT [PK_ApprovalUserViewInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApprovalWorkflow]    Script Date: 8/8/2018 11:34:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[RerunWorkflow] [bit] NOT NULL,
 CONSTRAINT [PK_ApprovalWorkflow] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AttachedAccountBillingPlan]    Script Date: 8/8/2018 11:34:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttachedAccountBillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[BillingPlan] [int] NOT NULL,
	[NewPrice] [decimal](11, 2) NULL,
	[IsAppliedCurrentRate] [bit] NOT NULL,
	[NewProofPrice] [decimal](11, 2) NULL,
	[IsModifiedProofPrice] [bit] NOT NULL,
 CONSTRAINT [PK_AttachedAccountBillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillingPlan]    Script Date: 8/8/2018 11:34:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Type] [int] NOT NULL,
	[Proofs] [int] NULL,
	[Price] [decimal](11, 2) NOT NULL,
	[IsFixed] [bit] NOT NULL,
	[ColorProofConnections] [int] NULL,
	[IsDemo] [bit] NOT NULL,
	[MaxUsers] [int] NULL,
	[MaxStorageInGb] [int] NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_BillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BillingPlanType]    Script Date: 8/8/2018 11:34:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingPlanType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](64) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[MediaService] [int] NOT NULL,
	[EnablePrePressTools] [bit] NOT NULL,
	[EnableMediaTools] [bit] NOT NULL,
	[AppModule] [int] NOT NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_BillingPlanType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BouncedEmail]    Script Date: 8/8/2018 11:34:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BouncedEmail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_BouncedEmail_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BrandingPreset]    Script Date: 8/8/2018 11:34:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BrandingPreset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[HeaderLogoPath] [nvarchar](256) NULL,
	[EmailLogoPath] [nvarchar](256) NULL,
	[FavIconPath] [nvarchar](256) NULL,
 CONSTRAINT [PK_BrandingPreset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BrandingPresetTheme]    Script Date: 8/8/2018 11:34:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BrandingPresetTheme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BrandingPreset] [int] NOT NULL,
	[Key] [nvarchar](1) NOT NULL,
	[ThemeName] [nvarchar](25) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_UserGroupdPresetTheme_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BrandingPresetUserGroup]    Script Date: 8/8/2018 11:34:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BrandingPresetUserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BrandingPreset] [int] NOT NULL,
	[UserGroup] [int] NOT NULL,
 CONSTRAINT [PK_BrandingPresetUserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangedBillingPlan]    Script Date: 8/8/2018 11:34:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangedBillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[BillingPlan] [int] NULL,
	[AttachedAccountBillingPlan] [int] NULL,
 CONSTRAINT [PK_ChangedBillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMSAnnouncement]    Script Date: 8/8/2018 11:34:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMSAnnouncement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [nvarchar](1024) NOT NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_CMSAnnouncement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMSDownload]    Script Date: 8/8/2018 11:34:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMSDownload](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileGuid] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_CMSDownload] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMSManual]    Script Date: 8/8/2018 11:34:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMSManual](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileGuid] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_CMSManual] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CMSVideo]    Script Date: 8/8/2018 11:34:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CMSVideo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IsTrainning] [bit] NOT NULL,
	[VideoUrl] [nvarchar](512) NOT NULL,
	[VideoDescription] [nvarchar](256) NOT NULL,
	[VideoTitle] [nvarchar](256) NOT NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_CMSVideo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CollaborateAPIJobMetadata]    Script Date: 8/8/2018 11:34:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CollaborateAPIJobMetadata](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Metadata] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ResourceID] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_CollaborateAPIJobMetadata] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CollaborateAPILog]    Script Date: 8/8/2018 11:34:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CollaborateAPILog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](5000) NOT NULL,
	[Exception] [varchar](2000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CollaborateAPISession]    Script Date: 8/8/2018 11:34:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CollaborateAPISession](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[SessionKey] [nvarchar](64) NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[IsConfirmed] [bit] NOT NULL,
 CONSTRAINT [PK_CollaborateAPISession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofContainer]    Script Date: 8/8/2018 11:34:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofContainer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Container] [int] NOT NULL,
	[ColorProofInstance] [int] NOT NULL,
	[ContainerSchedule] [int] NOT NULL,
 CONSTRAINT [PK_ColorProofContainer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofCoZoneWorkflow]    Script Date: 8/8/2018 11:34:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofCoZoneWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ColorProofWorkflow] [int] NOT NULL,
	[Name] [nvarchar](256) NULL,
	[IsAvailable] [bit] NULL,
	[TransmissionTimeout] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ColorProofCoZoneWorkflow] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofCoZoneWorkflowUserGroup]    Script Date: 8/8/2018 11:34:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ColorProofCoZoneWorkflow] [int] NOT NULL,
	[UserGroup] [int] NOT NULL,
 CONSTRAINT [PK_ColorProofCoZoneWorkflowUserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofInstance]    Script Date: 8/8/2018 11:34:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofInstance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NULL,
	[Owner] [int] NOT NULL,
	[Version] [nvarchar](50) NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[Password] [varchar](512) NOT NULL,
	[ComputerName] [nvarchar](50) NULL,
	[SystemName] [nvarchar](50) NULL,
	[SerialNumber] [nvarchar](128) NULL,
	[IsOnline] [bit] NULL,
	[LastSeen] [bigint] NULL,
	[State] [int] NOT NULL,
	[AdminName] [nvarchar](128) NULL,
	[Address] [nvarchar](256) NULL,
	[Email] [nvarchar](50) NULL,
	[WorkflowsLastModifiedTimestamp] [bigint] NULL,
	[IsDeleted] [bit] NOT NULL,
	[PairingCode] [nvarchar](64) NULL,
	[PairingCodeExpirationDate] [datetime] NULL,
 CONSTRAINT [PK_ColorProofInstance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofInstanceState]    Script Date: 8/8/2018 11:34:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofInstanceState](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ColorProofInstanceState] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_ColorProofInstanceState] UNIQUE NONCLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofJobStatus]    Script Date: 8/8/2018 11:34:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofJobStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ColorProofJobStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_ColorProofJobStatus] UNIQUE NONCLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ColorProofWorkflow]    Script Date: 8/8/2018 11:34:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ColorProofWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NULL,
	[Name] [nvarchar](256) NULL,
	[OldName] [nvarchar](256) NULL,
	[ProofStandard] [nvarchar](256) NULL,
	[MaximumUsablePaperWidth] [int] NULL,
	[IsActivated] [bit] NULL,
	[IsInlineProofVerificationSupported] [bit] NULL,
	[SupportedSpotColors] [nvarchar](max) NULL,
	[ColorProofInstance] [int] NOT NULL,
 CONSTRAINT [PK_ColorProofWorkflow] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Colorspace]    Script Date: 8/8/2018 11:34:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colorspace](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_Colorspace] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommentEmailSendDecision]    Script Date: 8/8/2018 11:34:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentEmailSendDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_CommentEmailSendDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 8/8/2018 11:34:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[Number] [nvarchar](32) NULL,
	[Address1] [nvarchar](128) NOT NULL,
	[Address2] [nvarchar](128) NULL,
	[City] [nvarchar](64) NOT NULL,
	[Postcode] [nvarchar](20) NULL,
	[State] [nvarchar](20) NULL,
	[Phone] [nvarchar](20) NOT NULL,
	[Mobile] [nvarchar](20) NULL,
	[Email] [nvarchar](64) NULL,
	[Country] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Container]    Script Date: 8/8/2018 11:34:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Container](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[ContainerType] [nvarchar](128) NOT NULL,
	[CreatorAccountName] [nvarchar](128) NOT NULL,
	[OwnerGroup] [int] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Container] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerAccount]    Script Date: 8/8/2018 11:34:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerAccount](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Container] [int] NOT NULL,
 CONSTRAINT [PK_ContainerAccount] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerClientStatus]    Script Date: 8/8/2018 11:34:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerClientStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](500) NOT NULL,
	[State] [int] NOT NULL,
	[ContainerClient] [int] NOT NULL,
	[ContainerVersion] [int] NOT NULL,
 CONSTRAINT [PK_ContainerClientStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerDistributionGroup]    Script Date: 8/8/2018 11:34:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerDistributionGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Account] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ContainerDistributionGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerDistributionGroupColorProofInstance]    Script Date: 8/8/2018 11:34:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerDistributionGroupColorProofInstance](
	[ContainerDistributionGroup] [int] NOT NULL,
	[ColorProofInstance] [int] NOT NULL,
 CONSTRAINT [PK_ContainerDistributionGroupColorProofInstance] PRIMARY KEY CLUSTERED 
(
	[ContainerDistributionGroup] ASC,
	[ColorProofInstance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerFile]    Script Date: 8/8/2018 11:34:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerFile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Checksum] [nvarchar](256) NOT NULL,
	[Size] [bigint] NOT NULL,
	[ContainerVersion] [int] NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[ResponseID] [nvarchar](256) NULL,
	[IsComplete] [bit] NOT NULL,
 CONSTRAINT [PK_ContainerFile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerGroup]    Script Date: 8/8/2018 11:34:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Account] [int] NOT NULL,
	[Description] [nvarchar](256) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_ContainerGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerGroupContainer]    Script Date: 8/8/2018 11:34:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerGroupContainer](
	[ContainerGroup] [int] NOT NULL,
	[Container] [int] NOT NULL,
 CONSTRAINT [PK_ContainerGroupContainer] PRIMARY KEY CLUSTERED 
(
	[ContainerGroup] ASC,
	[Container] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerSchedule]    Script Date: 8/8/2018 11:34:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDirty] [bit] NOT NULL,
	[Account] [int] NOT NULL,
 CONSTRAINT [PK_ContainerSchedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerVersion]    Script Date: 8/8/2018 11:34:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerVersion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Version] [int] NOT NULL,
	[Container] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ContainerVersion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContainerVersionOrganization]    Script Date: 8/8/2018 11:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContainerVersionOrganization](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Container] [int] NOT NULL,
	[ContainerVersion] [int] NOT NULL,
 CONSTRAINT [PK_ContainerVersionOrganization] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractPeriod]    Script Date: 8/8/2018 11:34:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractPeriod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ContractPeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ControllerAction]    Script Date: 8/8/2018 11:34:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControllerAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Controller] [nvarchar](128) NOT NULL,
	[Action] [nvarchar](128) NULL,
	[Parameters] [nvarchar](128) NULL,
 CONSTRAINT [TBL_ControllerAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ConversionStatus]    Script Date: 8/8/2018 11:34:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConversionStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ConversionStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_ConversionStatus] UNIQUE NONCLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 8/8/2018 11:34:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Iso2] [nvarchar](2) NOT NULL,
	[Iso3] [nvarchar](3) NOT NULL,
	[IsoCountryNumber] [int] NOT NULL,
	[DialingPrefix] [int] NULL,
	[Name] [nvarchar](64) NOT NULL,
	[ShortName] [nvarchar](50) NOT NULL,
	[HasLocationData] [bit] NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Currency]    Script Date: 8/8/2018 11:34:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Code] [nvarchar](4) NOT NULL,
	[Symbol] [nvarchar](5) NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CurrencyRate]    Script Date: 8/8/2018 11:34:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [int] NOT NULL,
	[Rate] [decimal](10, 2) NOT NULL,
	[Creator] [int] NOT NULL,
	[Modifier] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_CurrencyRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DateFormat]    Script Date: 8/8/2018 11:34:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DateFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pattern] [nvarchar](16) NOT NULL,
	[Result] [nvarchar](16) NOT NULL,
 CONSTRAINT [PK_DateFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DecisionType]    Script Date: 8/8/2018 11:34:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DecisionType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_DecisionType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DefaultTheme]    Script Date: 8/8/2018 11:34:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefaultTheme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](1) NOT NULL,
	[ThemeName] [nvarchar](25) NOT NULL,
 CONSTRAINT [PK_DefaultTheme_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverExternalCollaborator]    Script Date: 8/8/2018 11:35:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverExternalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[GivenName] [nvarchar](64) NULL,
	[FamilyName] [nvarchar](64) NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[Creator] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Locale] [int] NOT NULL,
 CONSTRAINT [PK_DeliverExternalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJob]    Script Date: 8/8/2018 11:35:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJob](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Job] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Owner] [int] NOT NULL,
	[FileName] [nvarchar](128) NOT NULL,
	[CPWorkflow] [int] NOT NULL,
	[Pages] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IncludeProofMetaInformationOnLabel] [bit] NOT NULL,
	[LogProofControlResults] [bit] NOT NULL,
	[Size] [decimal](18, 2) NOT NULL,
	[Status] [int] NOT NULL,
	[IsAlertSent] [bit] NOT NULL,
	[IsCancelRequested] [bit] NOT NULL,
	[TotalNrOfPages] [int] NOT NULL,
	[StatusInColorProof] [int] NOT NULL,
	[BackLinkUrl] [nvarchar](256) NULL,
	[FileCheckSum] [nvarchar](128) NULL,
	[IsDeleted] [bit] NOT NULL,
	[JobSource] [int] NOT NULL,
	[FTPSourceFolder] [nvarchar](128) NULL,
 CONSTRAINT [PK_DeliverJob] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJobData]    Script Date: 8/8/2018 11:35:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJobData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeliverJob] [int] NOT NULL,
	[EmailOptionalMsg] [nvarchar](max) NULL,
 CONSTRAINT [PK_DeliverJobData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJobDeleteHistory]    Script Date: 8/8/2018 11:35:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJobDeleteHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[JobName] [nvarchar](128) NULL,
 CONSTRAINT [PK_DeliverJobDeleteHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJobPage]    Script Date: 8/8/2018 11:35:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJobPage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserFriendlyName] [nvarchar](256) NOT NULL,
	[Status] [int] NOT NULL,
	[ErrorMessage] [nvarchar](1024) NULL,
	[DeliverJob] [int] NOT NULL,
 CONSTRAINT [PK_DeliverJobImage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJobPageStatus]    Script Date: 8/8/2018 11:35:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJobPageStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_DeliverJobPageStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_DDeliverJobPageStatus] UNIQUE NONCLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJobPageVerificationResult]    Script Date: 8/8/2018 11:35:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJobPageVerificationResult](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StripName] [nvarchar](512) NOT NULL,
	[MaxDelta] [float] NOT NULL,
	[AvgDelta] [float] NOT NULL,
	[ProofResult] [bit] NULL,
	[AllowedTolleranceMax] [float] NULL,
	[AllowedTolleranceAvg] [float] NULL,
	[IsSpotColorVerification] [bit] NOT NULL,
	[DeliverJobPage] [int] NOT NULL,
 CONSTRAINT [PK_DeliverJobPageVerificationResult] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverJobStatus]    Script Date: 8/8/2018 11:35:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverJobStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_DeliverJobStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_DeliverJobStatus] UNIQUE NONCLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeliverSharedJob]    Script Date: 8/8/2018 11:35:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeliverSharedJob](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DeliverJob] [int] NOT NULL,
	[DeliverExternalCollaborator] [int] NOT NULL,
	[NotificationPreset] [int] NOT NULL,
 CONSTRAINT [PK_DeliverSharedJob] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Email]    Script Date: 8/8/2018 11:35:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Email](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[fromName] [nvarchar](255) NOT NULL,
	[toName] [nvarchar](255) NULL,
	[toEmail] [nvarchar](120) NOT NULL,
	[fromEmail] [nvarchar](120) NOT NULL,
	[toCC] [nvarchar](255) NOT NULL,
	[subject] [nvarchar](255) NOT NULL,
	[bodyText] [nvarchar](max) NOT NULL,
	[bodyHtml] [nvarchar](max) NOT NULL,
	[ProcessingInProgress] [nvarchar](36) NULL,
	[TryCount] [int] NULL,
	[LastErrorMessage] [nvarchar](256) NULL,
	[Attachments] [nvarchar](max) NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventGroup]    Script Date: 8/8/2018 11:35:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_EventGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EventType]    Script Date: 8/8/2018 11:35:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventGroup] [int] NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExcludedAccountDefaultProfiles]    Script Date: 8/8/2018 11:35:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExcludedAccountDefaultProfiles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[SimulationProfile] [int] NOT NULL,
 CONSTRAINT [Pk_ExcludedAccountDefaultProfiles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExternalCollaborator]    Script Date: 8/8/2018 11:35:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[GivenName] [nvarchar](64) NULL,
	[FamilyName] [nvarchar](64) NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[DateLoggedIn] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ProofStudioColor] [nvarchar](16) NULL,
	[Locale] [int] NOT NULL,
	[ShowAnnotationsOfAllPhases] [bit] NOT NULL,
	[BouncedEmailID] [int] NULL,
 CONSTRAINT [PK_ExternalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileType]    Script Date: 8/8/2018 11:35:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Extention] [nvarchar](8) NOT NULL,
	[Name] [nvarchar](64) NULL,
	[Type] [nvarchar](256) NULL,
 CONSTRAINT [PK_FileType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folder]    Script Date: 8/8/2018 11:35:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Parent] [int] NULL,
	[Account] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsPrivate] [bit] NOT NULL,
	[AllCollaboratorsDecisionRequired] [bit] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FolderApproval]    Script Date: 8/8/2018 11:35:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderApproval](
	[Folder] [int] NOT NULL,
	[Approval] [int] NOT NULL,
 CONSTRAINT [PK_FolderApproval] PRIMARY KEY CLUSTERED 
(
	[Folder] ASC,
	[Approval] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FolderCollaborator]    Script Date: 8/8/2018 11:35:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Folder] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_FolderCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FolderCollaboratorGroup]    Script Date: 8/8/2018 11:35:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Folder] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_FolderCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FTPPresetJobDownload]    Script Date: 8/8/2018 11:35:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FTPPresetJobDownload](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Preset] [int] NOT NULL,
	[ApprovalJob] [int] NULL,
	[DeliverJob] [int] NULL,
	[ManageJob] [int] NULL,
	[GroupKey] [nvarchar](64) NULL,
	[Status] [bit] NULL,
	[StatusDescription] [nvarchar](128) NULL,
 CONSTRAINT [PK_FTPPresetJobDownload] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FTPProtocol]    Script Date: 8/8/2018 11:35:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FTPProtocol](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_FTPProtocol] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GlobalSettings]    Script Date: 8/8/2018 11:35:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GlobalSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](64) NOT NULL,
	[DataType] [int] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_GlobalSettings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HighlightType]    Script Date: 8/8/2018 11:35:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HighlightType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_HighlightType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ICCProfileCreatorSignatures]    Script Date: 8/8/2018 11:35:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ICCProfileCreatorSignatures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Signature] [nvarchar](6) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_ICCProfileCreatorSignatures] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImageFormat]    Script Date: 8/8/2018 11:35:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImageFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Parent] [int] NULL,
 CONSTRAINT [PK_ImageFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InactiveApprovalStatuses]    Script Date: 8/8/2018 11:35:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InactiveApprovalStatuses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[ApprovalDecision] [int] NOT NULL,
 CONSTRAINT [PK_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Job]    Script Date: 8/8/2018 11:35:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Job](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
	[Account] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[JobOwner] [int] NULL,
	[Guid] [nvarchar](36) NOT NULL,
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobSource]    Script Date: 8/8/2018 11:35:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobSource](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [pk_jobsourcetype] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobStatus]    Script Date: 8/8/2018 11:35:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_JobStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobStatusChangeLog]    Script Date: 8/8/2018 11:35:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobStatusChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Job] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_JobStatusChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LabColorMeasurement]    Script Date: 8/8/2018 11:35:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LabColorMeasurement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalSeparationPlate] [int] NOT NULL,
	[Step] [float] NOT NULL,
	[L] [float] NOT NULL,
	[a] [float] NOT NULL,
	[b] [float] NOT NULL,
 CONSTRAINT [PK_LabColorMeasurement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locale]    Script Date: 8/8/2018 11:35:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locale](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](16) NULL,
	[DisplayName] [nvarchar](64) NULL,
 CONSTRAINT [PK_Locale] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogAction]    Script Date: 8/8/2018 11:35:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[LogModule] [int] NOT NULL,
	[Message] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_LogAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogApproval]    Script Date: 8/8/2018 11:35:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[LogAction] [int] NOT NULL,
	[ActualMessage] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_LogApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LogModule]    Script Date: 8/8/2018 11:35:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogModule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_LogModule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManageAPISession]    Script Date: 8/8/2018 11:35:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManageAPISession](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SessionKey] [nvarchar](256) NOT NULL,
	[User] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_ManageAPISession] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManageConversion]    Script Date: 8/8/2018 11:35:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManageConversion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[InputColorSpace] [varchar](120) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ManageConversion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManageJob]    Script Date: 8/8/2018 11:35:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManageJob](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Job] [int] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Owner] [int] NOT NULL,
	[FileName] [nvarchar](128) NOT NULL,
	[Size] [decimal](18, 2) NOT NULL,
	[OutputColorSpace] [int] NULL,
	[Preflight] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[PreflightStatus] [int] NOT NULL,
	[ConversionStatus] [int] NOT NULL,
	[ApplyColorManagementIfPreflightFails] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ErrorMessage] [nvarchar](1024) NULL,
	[PreflightTimeStamp] [datetime] NULL,
	[ColorConversionTimeStamp] [datetime] NULL,
	[CallbackUrl] [nvarchar](256) NULL,
	[FileType] [int] NOT NULL,
	[FileHash] [nvarchar](32) NULL,
	[StartProcessingTimeStamp] [datetime] NULL,
	[JobSource] [int] NOT NULL,
	[FTPSourceFolder] [nvarchar](128) NULL,
 CONSTRAINT [PK_ManageJob] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManageJobDeleteHistory]    Script Date: 8/8/2018 11:35:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManageJobDeleteHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[JobName] [nvarchar](128) NULL,
 CONSTRAINT [PK_ManageJobDeleteHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManageOutputColorSpace]    Script Date: 8/8/2018 11:35:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManageOutputColorSpace](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[FileGuid] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Conversion] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ManageGUI] [bit] NOT NULL,
	[ManageAPI] [bit] NOT NULL,
 CONSTRAINT [PK_ManageOutputColorSpace] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ManagePreflight]    Script Date: 8/8/2018 11:35:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ManagePreflight](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileGuid] [nvarchar](36) NOT NULL,
	[FileName] [nvarchar](256) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ManageGUI] [bit] NOT NULL,
	[ManageAPI] [bit] NOT NULL,
 CONSTRAINT [PK_ManagePreflight] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MeasurementCondition]    Script Date: 8/8/2018 11:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MeasurementCondition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KEY] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MeasurementConditionsId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MediaCategory]    Script Date: 8/8/2018 11:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_MediaCategoryId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MediaService]    Script Date: 8/8/2018 11:35:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaService](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Smoothing] [int] NOT NULL,
	[Resolution] [int] NOT NULL,
	[Colorspace] [int] NOT NULL,
	[ImageFormat] [int] NOT NULL,
	[PageBox] [int] NOT NULL,
	[TileImageFormat] [int] NOT NULL,
	[JPEGCompression] [int] NOT NULL,
 CONSTRAINT [PK_MediaService] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItem]    Script Date: 8/8/2018 11:35:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ControllerAction] [int] NULL,
	[Parent] [int] NULL,
	[Position] [int] NOT NULL,
	[IsAdminAppOwned] [bit] NULL,
	[IsAlignedLeft] [bit] NOT NULL,
	[IsSubNav] [bit] NOT NULL,
	[IsTopNav] [bit] NOT NULL,
	[IsVisible] [bit] NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Title] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_MenuItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MenuItemAccountTypeRole]    Script Date: 8/8/2018 11:35:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItemAccountTypeRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MenuItem] [int] NOT NULL,
	[AccountTypeRole] [int] NOT NULL,
 CONSTRAINT [PK_MenuItemAccountTypeRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NativeDataType]    Script Date: 8/8/2018 11:35:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NativeDataType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_NativeDataType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationFrequency]    Script Date: 8/8/2018 11:35:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationFrequency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_NotificationFrequency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationItem]    Script Date: 8/8/2018 11:35:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InternalRecipient] [int] NULL,
	[NotificationType] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Creator] [int] NULL,
	[CustomFields] [nvarchar](max) NULL,
	[PSSessionID] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_NotificationItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationPresetCollaborateTrigger]    Script Date: 8/8/2018 11:35:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationPresetCollaborateTrigger](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_NotificationPresetCollaborateTrigger] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationPresetCollaborateUnit]    Script Date: 8/8/2018 11:35:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationPresetCollaborateUnit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_NotificationPresetCollaborateUnit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationPresetFrequency]    Script Date: 8/8/2018 11:35:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationPresetFrequency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_NotificationPresetFrequency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationSchedule]    Script Date: 8/8/2018 11:35:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSchedule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationPreset] [int] NOT NULL,
	[NotificatonFrequency] [int] NOT NULL,
	[NotificationCollaborateUnit] [int] NULL,
	[NotificationCollaborateTrigger] [int] NULL,
	[NotificationCollaborateUnitNumber] [int] NULL,
	[NotificationPerPSSession] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_NotificationSchedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationScheduleUser]    Script Date: 8/8/2018 11:35:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationScheduleUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationSchedule] [int] NOT NULL,
	[User] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_NotificationScheduleUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationScheduleUserEventTypeEmailLog]    Script Date: 8/8/2018 11:35:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[LastSentDate] [datetime] NOT NULL,
 CONSTRAINT [PK_NotificationScheduleUserEventTypeEmailLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationScheduleUserGroup]    Script Date: 8/8/2018 11:35:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationScheduleUserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NotificationSchedule] [int] NOT NULL,
	[UserGroup] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_NotificationScheduleUserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageBox]    Script Date: 8/8/2018 11:35:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageBox](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_PageBox] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaperTint]    Script Date: 8/8/2018 11:35:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaperTint](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MediaCategory] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PaperTintId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PaperTintMeasurement]    Script Date: 8/8/2018 11:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaperTintMeasurement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PaperTint] [int] NOT NULL,
	[MeasurementCondition] [int] NOT NULL,
	[L] [float] NOT NULL,
	[a] [float] NOT NULL,
	[b] [float] NOT NULL,
 CONSTRAINT [Pk_PaperTintMeasurementId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhaseDeadlineTrigger]    Script Date: 8/8/2018 11:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhaseDeadlineTrigger](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_PhaseDeadlineTrigger] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhaseDeadlineUnit]    Script Date: 8/8/2018 11:35:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhaseDeadlineUnit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_PhaseDeadlineUnit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PreflightStatus]    Script Date: 8/8/2018 11:35:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PreflightStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_PreflightStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_PreflightStatus] UNIQUE NONCLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PrePressFunction]    Script Date: 8/8/2018 11:35:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrePressFunction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_PrePressFunction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Preset]    Script Date: 8/8/2018 11:35:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_Preset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PrinterCompany]    Script Date: 8/8/2018 11:35:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrinterCompany](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Type] [int] NOT NULL,
 CONSTRAINT [PK_PrinterCompany] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PrinterCompanyType]    Script Date: 8/8/2018 11:35:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrinterCompanyType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_PrinterCompanyType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProofStudioSessions]    Script Date: 8/8/2018 11:35:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProofStudioSessions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[IsCompleted] [bit] NOT NULL,
	[User] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_GUID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProofStudioUserColor]    Script Date: 8/8/2018 11:35:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProofStudioUserColor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[HEXValue] [nvarchar](16) NOT NULL,
 CONSTRAINT [PK_ProofStudioUserColor] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlDataCaptureDetails]    Script Date: 8/8/2018 11:35:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlDataCaptureDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PrinterCompany] [int] NOT NULL,
	[PrintProcess] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[Product] [int] NOT NULL,
	[Paper] [int] NOT NULL,
	[Section] [int] NOT NULL,
	[Copies] [int] NOT NULL,
	[UploadDate] [datetime] NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[HasOriginalFile] [bit] NOT NULL,
 CONSTRAINT [PK_QualityControlDataCaptureDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlDataCaptureMeasurement]    Script Date: 8/8/2018 11:35:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlDataCaptureMeasurement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataCaptureDetails] [int] NOT NULL,
	[Copies] [int] NOT NULL,
	[Density] [int] NOT NULL,
	[InkName] [nvarchar](128) NOT NULL,
	[Measurement] [decimal](18, 4) NULL,
	[Threshold] [decimal](18, 4) NOT NULL,
	[TargetDensityValue] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_QualityControlDataCaptureMeasurement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlInkDensity]    Script Date: 8/8/2018 11:35:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlInkDensity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlSettings] [int] NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_QualityControlInkDensity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlInkDensityTolerance]    Script Date: 8/8/2018 11:35:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlInkDensityTolerance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlInkDensity] [int] NOT NULL,
	[Value] [decimal](18, 4) NOT NULL,
 CONSTRAINT [PK_QualityControlInkDensityTolerance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlPaper]    Script Date: 8/8/2018 11:35:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlPaper](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlSettings] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_QualityControlPaper] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlPrintProcess]    Script Date: 8/8/2018 11:35:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlPrintProcess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlSettings] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Interval] [int] NOT NULL,
 CONSTRAINT [PK_QualityControlPrintProcess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlProduct]    Script Date: 8/8/2018 11:35:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlProduct](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlSettings] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_QualityControlProduct] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlSection]    Script Date: 8/8/2018 11:35:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlSection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlSettings] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_QualityControlSection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlSettings]    Script Date: 8/8/2018 11:35:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
 CONSTRAINT [PK_QualityControlSettings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QualityControlVersion]    Script Date: 8/8/2018 11:35:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityControlVersion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[QualityControlSettings] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_QualityControlVersion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReservedDomainName]    Script Date: 8/8/2018 11:35:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReservedDomainName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ReservedDomainName] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 8/8/2018 11:35:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
	[AppModule] [int] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePresetEventType]    Script Date: 8/8/2018 11:35:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePresetEventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [int] NOT NULL,
	[Preset] [int] NULL,
	[EventType] [int] NOT NULL,
 CONSTRAINT [PK_RolePresetEventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shape]    Script Date: 8/8/2018 11:35:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shape](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IsCustom] [bit] NOT NULL,
	[FXG] [nvarchar](max) NULL,
	[SVG] [nvarchar](max) NULL,
 CONSTRAINT [PK_Shape] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SharedApproval]    Script Date: 8/8/2018 11:35:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SharedApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[ExternalCollaborator] [int] NOT NULL,
	[ApprovalCollaboratorRole] [int] NOT NULL,
	[SharedDate] [datetime2](7) NOT NULL,
	[IsSharedURL] [bit] NOT NULL,
	[IsSharedDownloadURL] [bit] NOT NULL,
	[IsExpired] [bit] NOT NULL,
	[ExpireDate] [datetime2](7) NOT NULL,
	[Notes] [nvarchar](1034) NULL,
	[Phase] [int] NULL,
	[SOADState] [int] NOT NULL,
 CONSTRAINT [PK_SharedApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SharedApprovalPhase]    Script Date: 8/8/2018 11:35:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SharedApprovalPhase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Phase] [int] NOT NULL,
	[ExternalCollaborator] [int] NOT NULL,
	[ApprovalPhaseCollaboratorRole] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[CreatedBy] [int] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_SharedApprovalPhase] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SharedColorProofWorkflow]    Script Date: 8/8/2018 11:35:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SharedColorProofWorkflow](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ColorProofCoZoneWorkflow] [int] NOT NULL,
	[User] [int] NOT NULL,
	[IsAccepted] [bit] NULL,
	[IsEnabled] [bit] NOT NULL,
	[Token] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_SharedColorProofWorkflow] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SimulationProfile]    Script Date: 8/8/2018 11:35:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SimulationProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NULL,
	[Creator] [int] NULL,
	[ProfileName] [nvarchar](50) NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[Guid] [nvarchar](50) NOT NULL,
	[MediaCategory] [int] NULL,
	[MeasurementCondition] [int] NOT NULL,
	[L] [float] NULL,
	[a] [float] NULL,
	[b] [float] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[IsValid] [bit] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_SimulationProfileId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Smoothing]    Script Date: 8/8/2018 11:35:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Smoothing](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_Smoothing] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoftProofingSessionError]    Script Date: 8/8/2018 11:35:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoftProofingSessionError](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SoftProofingSessionParams] [int] NOT NULL,
	[ApprovalPage] [int] NOT NULL,
	[Error] [nvarchar](500) NOT NULL,
	[LogDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SoftProofingSessionError] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SoftProofingSessionParams]    Script Date: 8/8/2018 11:35:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SoftProofingSessionParams](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[SessionGuid] [nvarchar](64) NOT NULL,
	[ActiveChannels] [nvarchar](128) NULL,
	[SimulatePaperTint] [bit] NOT NULL,
	[OutputRGBProfileName] [nvarchar](128) NULL,
	[ZoomLevel] [int] NOT NULL,
	[ViewTiles] [nvarchar](50) NULL,
	[LastAccessedDate] [datetime] NOT NULL,
	[ViewingCondition] [int] NULL,
	[DefaultSession] [bit] NOT NULL,
	[HighResolution] [bit] NOT NULL,
	[HighResolutionInProgress] [bit] NOT NULL,
 CONSTRAINT [PK_SoftProofingSessionParams] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudioSetting]    Script Date: 8/8/2018 11:35:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudioSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [int] NOT NULL,
	[User] [int] NULL,
	[NotificationFrequency] [int] NOT NULL,
	[IncludeMyOwnActivity] [bit] NOT NULL,
	[WhenCommentMade] [int] NOT NULL,
	[RepliesMyComment] [int] NOT NULL,
	[NewVesionAdded] [int] NOT NULL,
	[FinalDecisionMade] [int] NOT NULL,
	[EnableColorCorrection] [bit] NOT NULL,
 CONSTRAINT [PK_StudioSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudioSettingType]    Script Date: 8/8/2018 11:35:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudioSettingType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_StudioSettingType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubscriberPlanInfo]    Script Date: 8/8/2018 11:35:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubscriberPlanInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NrOfCredits] [int] NOT NULL,
	[IsQuotaAllocationEnabled] [bit] NOT NULL,
	[ChargeCostPerProofIfExceeded] [bit] NOT NULL,
	[SelectedBillingPlan] [int] NOT NULL,
	[ContractStartDate] [datetime2](7) NULL,
	[Account] [int] NOT NULL,
	[IsManageAPI] [bit] NOT NULL,
	[AccountPlanActivationDate] [datetime2](7) NULL,
	[AllowOverdraw] [bit] NOT NULL,
 CONSTRAINT [PK_SubscriberPlanInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ThemeColorScheme]    Script Date: 8/8/2018 11:35:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThemeColorScheme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DefaultTheme] [int] NULL,
	[AccountTheme] [int] NULL,
	[BrandingPresetTheme] [int] NULL,
	[Name] [nvarchar](20) NOT NULL,
	[THex] [nvarchar](10) NULL,
	[BHex] [nvarchar](10) NULL,
 CONSTRAINT [PK_ThemeColorScheme_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TileImageFormat]    Script Date: 8/8/2018 11:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TileImageFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_TileImageFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeFormat]    Script Date: 8/8/2018 11:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Pattern] [nvarchar](16) NOT NULL,
	[Result] [nvarchar](16) NOT NULL,
 CONSTRAINT [PK_TimeFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnsentEmailFrom_23032018]    Script Date: 8/8/2018 11:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnsentEmailFrom_23032018](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[fromName] [nvarchar](255) NOT NULL,
	[toName] [nvarchar](255) NULL,
	[toEmail] [nvarchar](120) NOT NULL,
	[fromEmail] [nvarchar](120) NOT NULL,
	[toCC] [nvarchar](255) NOT NULL,
	[subject] [nvarchar](255) NOT NULL,
	[bodyText] [nvarchar](max) NOT NULL,
	[bodyHtml] [nvarchar](max) NOT NULL,
	[ProcessingInProgress] [nvarchar](36) NULL,
	[TryCount] [int] NULL,
	[LastErrorMessage] [nvarchar](256) NULL,
	[Attachments] [nvarchar](max) NULL,
	[Account] [int] NULL,
 CONSTRAINT [PK_UnsentEmail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineAccountSettings]    Script Date: 8/8/2018 11:35:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineAccountSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DuplicateFileName] [int] NOT NULL,
	[VersionName] [int] NOT NULL,
	[VersionNumberPositionInName] [int] NULL,
	[VersionNumberPositionFromStart] [bit] NULL,
	[AllowPDF] [bit] NOT NULL,
	[AllowImage] [bit] NOT NULL,
	[AllowVideo] [bit] NOT NULL,
	[AllowSWF] [bit] NOT NULL,
	[AllowZip] [bit] NOT NULL,
	[ZipFilesWhenDownloading] [bit] NOT NULL,
	[ApplyUploadSettingsForNewApproval] [bit] NOT NULL,
	[UseUploadSettingsFromPreviousVersion] [bit] NOT NULL,
	[EnableVersionMirroring] [bit] NOT NULL,
	[PostStatusUpdatesURL] [nvarchar](128) NULL,
 CONSTRAINT [PK_UploadEngineAccountSettings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineCustomXMLTemplate]    Script Date: 8/8/2018 11:35:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineCustomXMLTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
	[Account] [int] NOT NULL,
 CONSTRAINT [PK_UploadEngineCustomXMLTemplates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineCustomXMLTemplatesField]    Script Date: 8/8/2018 11:35:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineCustomXMLTemplatesField](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[XPath] [nvarchar](128) NOT NULL,
	[UploadEngineCustomXMLTemplate] [int] NOT NULL,
	[UploadEngineDefaultXMLTemplate] [int] NOT NULL,
 CONSTRAINT [PK_UploadEngineCustomXMLTemplatesFields] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineDefaultXMLTemplate]    Script Date: 8/8/2018 11:35:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineDefaultXMLTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[NodeName] [nvarchar](128) NOT NULL,
	[XPath] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_UploadEngineDefaultXMLTemplates] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineDuplicateFileName]    Script Date: 8/8/2018 11:35:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineDuplicateFileName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_UploadEngineDuplicateFileName] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEnginePermission]    Script Date: 8/8/2018 11:35:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEnginePermission](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_UploadEnginePermission] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEnginePreset]    Script Date: 8/8/2018 11:35:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEnginePreset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[IsEnabledInCollaborate] [bit] NOT NULL,
	[IsEnabledInDeliver] [bit] NOT NULL,
	[IsEnabledInManage] [bit] NOT NULL,
	[MakeDefaultForManage] [bit] NOT NULL,
	[Host] [nvarchar](64) NOT NULL,
	[Port] [int] NOT NULL,
	[Username] [nvarchar](128) NULL,
	[Password] [nvarchar](128) NULL,
	[DefaultDirectory] [nvarchar](128) NULL,
	[ReplicateCoZoneFolders] [bit] NOT NULL,
	[IsActiveTransferMode] [bit] NOT NULL,
	[Protocol] [int] NOT NULL,
 CONSTRAINT [PK_UploadEnginePreset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineProfile]    Script Date: 8/8/2018 11:35:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [nvarchar](36) NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[HasInputSettings] [bit] NOT NULL,
	[OutputPreset] [int] NULL,
	[XMLTemplate] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UploadEngineProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineUserGroupPermissions]    Script Date: 8/8/2018 11:35:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineUserGroupPermissions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserGroup] [int] NOT NULL,
	[UploadEnginePermission] [int] NOT NULL,
 CONSTRAINT [PK_UploadEngineUserGroupPermissions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadEngineVersionName]    Script Date: 8/8/2018 11:35:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadEngineVersionName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [int] NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_UploadEngineVersionName] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 8/8/2018 11:35:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Username] [nvarchar](128) NOT NULL,
	[Password] [nvarchar](64) NOT NULL,
	[GivenName] [nvarchar](64) NOT NULL,
	[FamilyName] [nvarchar](64) NOT NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[PhotoPath] [nvarchar](256) NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[MobileTelephoneNumber] [nvarchar](20) NULL,
	[HomeTelephoneNumber] [nvarchar](20) NULL,
	[OfficeTelephoneNumber] [nvarchar](20) NOT NULL,
	[NotificationFrequency] [int] NOT NULL,
	[DateLastLogin] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Preset] [int] NOT NULL,
	[IncludeMyOwnActivity] [bit] NOT NULL,
	[NeedReLogin] [bit] NOT NULL,
	[ProofStudioColor] [int] NOT NULL,
	[PrinterCompany] [int] NULL,
	[Locale] [int] NOT NULL,
	[BouncedEmailID] [int] NULL,
	[IsSsoUser] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserApprovalReminderEmailLog]    Script Date: 8/8/2018 11:35:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserApprovalReminderEmailLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Approval] [int] NOT NULL,
	[LastSentDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UserApprovalReminderEmailLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserCustomPresetEventTypeValue]    Script Date: 8/8/2018 11:35:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCustomPresetEventTypeValue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[RolePresetEventType] [int] NOT NULL,
	[Value] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_UserCustomPresetEventTypeValue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 8/8/2018 11:35:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Account] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[IsProfileServerOwnershipGroup] [bit] NOT NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserGroupUser]    Script Date: 8/8/2018 11:35:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupUser](
	[UserGroup] [int] NOT NULL,
	[User] [int] NOT NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IsPrimary] [bit] NOT NULL,
 CONSTRAINT [PK_UserGroupUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserHistory]    Script Date: 8/8/2018 11:35:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Company] [int] NOT NULL,
	[Username] [nvarchar](32) NOT NULL,
	[Password] [varchar](255) NULL,
	[GivenName] [nvarchar](32) NULL,
	[FamilyName] [nvarchar](32) NOT NULL,
	[EmailAddress] [nvarchar](64) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLogin]    Script Date: 8/8/2018 11:35:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[IpAddress] [nvarchar](16) NOT NULL,
	[Success] [bit] NOT NULL,
	[DateLogin] [datetime2](7) NOT NULL,
	[DateLogout] [datetime2](7) NULL,
 CONSTRAINT [PK_UserLogin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 8/8/2018 11:35:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Role] [int] NOT NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UserRole] UNIQUE NONCLUSTERED 
(
	[Role] ASC,
	[User] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSetting]    Script Date: 8/8/2018 11:35:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NULL,
	[Setting] [nvarchar](64) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
	[ValueDataType] [int] NOT NULL,
 CONSTRAINT [PK_UserSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserStatus]    Script Date: 8/8/2018 11:35:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_UserStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValueDataType]    Script Date: 8/8/2018 11:35:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValueDataType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ValueDataType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ViewingCondition]    Script Date: 8/8/2018 11:35:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ViewingCondition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PaperTint] [int] NULL,
	[SimulationProfile] [int] NULL,
	[EmbeddedProfile] [int] NULL,
	[CustomProfile] [int] NULL,
 CONSTRAINT [Pk_ViewingCondition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workstation]    Script Date: 8/8/2018 11:35:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workstation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [varchar](36) NOT NULL,
	[MachineId] [nvarchar](64) NOT NULL,
	[User] [int] NOT NULL,
	[Location] [nvarchar](128) NOT NULL,
	[IsCalibrated] [bit] NULL,
	[LastActivityDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[WorkstationName] [nvarchar](128) NULL,
 CONSTRAINT [PK_Workstation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkstationCalibrationData]    Script Date: 8/8/2018 11:35:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkstationCalibrationData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Guid] [varchar](36) NOT NULL,
	[Filename] [nvarchar](128) NOT NULL,
	[Processed] [bit] NOT NULL,
	[Workstation] [int] NOT NULL,
	[CalibrationSoftware] [nvarchar](256) NULL,
	[CalibrationDate] [datetime] NULL,
	[UploadedDate] [datetime] NOT NULL,
	[DisplayIdentifier] [nvarchar](64) NOT NULL,
	[LastErrorMessage] [nvarchar](512) NOT NULL,
	[CalibrationUploadStatus] [bit] NOT NULL,
	[ProcessingInProgress] [varchar](36) NULL,
	[DisplayName] [nvarchar](128) NOT NULL,
	[DisplayProfileName] [nvarchar](64) NOT NULL,
	[DisplayWidth] [int] NOT NULL,
	[DisplayHeight] [int] NOT NULL,
 CONSTRAINT [PK_WorkstationCalibrationData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkstationValidationResult]    Script Date: 8/8/2018 11:35:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkstationValidationResult](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Value] [nvarchar](64) NOT NULL,
	[WorkstationCalibrationData] [int] NOT NULL,
 CONSTRAINT [PK_WorkstationValidationResult] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[GetAccessFolders]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetAccessFolders]
(	
	-- Add the parameters for the function here
	@UserId int,
	@FolderId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT f.ID, f.Parent, f.Name, (0) AS Level, (CASE WHEN 
											(@UserId IN (	SELECT	fc.Collaborator 
															FROM	[dbo].FolderCollaborator fc
															WHERE	fc.Folder = f.ID 
					) ) THEN  '1' ELSE '0' end)  AS HasAccess 
		FROM	[dbo].[Folder] f
		WHERE	f.ID = @FolderId		 
		
		UNION ALL
		
		SELECT sf.ID, sf.Parent,sf.Name, (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  '1' ELSE '0' END)  AS HasAccess 
		FROM [dbo].[Folder] sf
			JOIN tableSet h
		ON h.ID	 = sf.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
    WHERE [Level] = 1
)

GO
/****** Object:  UserDefinedFunction [dbo].[GetChildAccounts]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[GetChildAccounts]
(	
	-- Add the parameters for the function here
	@LoggedAccountId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT a.ID
		FROM	[dbo].[Account] a
		WHERE	a.ID = @LoggedAccountId		 
		
		UNION ALL
		
		SELECT sa.ID
		FROM [dbo].[Account] sa
			JOIN tableSet h
		ON h.ID	 = sa.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
)


GO
/****** Object:  UserDefinedFunction [dbo].[GetParentAccounts]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetParentAccounts]
(	
	-- Add the parameters for the function here
	@AccountId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH parentAccounts AS (
		SELECT a.ID, a.Parent, a.Name, a.GPPDiscount
		FROM	[dbo].[Account] a
		WHERE	a.ID = @AccountId		 
		
		UNION ALL
		
		SELECT pa.ID, pa.Parent, pa.Name, pa.GPPDiscount
		FROM [dbo].[Account] pa
			JOIN parentAccounts pas
		ON pa.ID = pas.Parent		
	)	

	SELECT *
	FROM  parentAccounts	
)

GO
/****** Object:  UserDefinedFunction [dbo].[GetUserListWhereBelongs]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------------------------------------------
-- get all users that are in the same user group with the user that is provided as parameter
CREATE FUNCTION [dbo].[GetUserListWhereBelongs] ( @userId INT )
RETURNS TABLE
AS RETURN
    ( SELECT DISTINCT
                SLCT.[User]
      FROM      ( SELECT    UGU.[User]
                  FROM      dbo.UserGroupUser UGU
                  WHERE     UGU.[User] = @userId
                            OR UGU.UserGroup IN (
                            SELECT    DISTINCT
                                    UGU2.UserGroup
                            FROM    dbo.UserGroupUser UGU2
                            WHERE   UGU2.[User] = @userId )
                  UNION
                  SELECT    @userId AS [User]
                ) AS SLCT
    )
GO
/****** Object:  View [dbo].[AccountsReportView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- CZ-2971 Extend Account Report
--Account Report changes
CREATE VIEW [dbo].[AccountsReportView]
AS
SELECT
	0  AS ID, 
	'' AS Name,
	0 AS StatusId,
	'' AS StatusName,
	'' AS [Key],
	'' AS Domain,
	CONVERT(BIT, 0) AS IsCustomDomainActive,-- not null
	CAST('' AS NVARCHAR(128)) AS CustomDomain,--null
	0 AS AccountTypeID,
	GETDATE() AS CreatedDate,
	CONVERT(BIT, 0) AS IsTemporary,-- not null
	'' AS AccountTypeName,
	'' AS GivenName,
	'' AS FamilyName,
	0 AS Company ,
	0 AS Country ,
	'' AS CountryName,	
	CAST(0 AS INT) AS NoOfAccounts,
	CONVERT(DATETIME,GETDATE()) AS LastActivity,
	CAST('' AS NVARCHAR(128)) AS ChildAccounts,
	'' AS AccountPath,
	'' AS Plans, 
	0 As ApprovalsCount


GO
/****** Object:  View [dbo].[AccountsView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Account Report changes
CREATE VIEW [dbo].[AccountsView]
AS
SELECT
	a.[ID], 
	a.Name,
	a.[Status] AS StatusId,
	s.[Name] AS StatusName,
	s.[Key],
	a.Domain,
	a.IsCustomDomainActive,
	a.CustomDomain,
	a.AccountType AS AccountTypeID,
	a.CreatedDate,
	a.IsTemporary,
	at.Name AS AccountTypeName,
	u.GivenName,
	u.FamilyName,
	co.ID AS Company,
	c.ID AS Country,
	c.ShortName AS CountryName,
	--a.IsDraft,
	(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
	(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
	(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts
	
FROM
	[dbo].[AccountType] at 
	JOIN [dbo].[Account] a
		ON at.[ID] = a.AccountType
	JOIN [dbo].[AccountStatus] s
		ON s.[ID] = a.[Status]
	LEFT OUTER JOIN [dbo].[User] u
		ON u.ID= a.[Owner]
	JOIN [dbo].[Company] co
		ON a.[ID] = co.[Account]
	JOIN [dbo].[Country] c
		ON c.[ID] = co.[Country]
GO
/****** Object:  View [dbo].[ApprovalCountsView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount,
			0 AS MyOpenApprovalsCount,
			0 AS AdvanceSearchCount


GO
/****** Object:  View [dbo].[ColorProofWorkflowInstanceView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add IsFromCurrentAccount column
CREATE VIEW [dbo].[ColorProofWorkflowInstanceView]
AS
SELECT  dbo.ColorProofCoZoneWorkflow.ID AS ColorProofCoZoneWorkflowID ,
        dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow ,
        dbo.ColorProofCoZoneWorkflow.Name AS CoZoneName ,
        dbo.ColorProofCoZoneWorkflow.IsAvailable ,
        dbo.ColorProofCoZoneWorkflow.TransmissionTimeout ,
        dbo.ColorProofWorkflow.ID AS ColorProofWorkflowID ,
        dbo.ColorProofWorkflow.Guid ,
        dbo.ColorProofWorkflow.Name ,
        dbo.ColorProofWorkflow.ProofStandard ,
        dbo.ColorProofWorkflow.MaximumUsablePaperWidth ,
        dbo.ColorProofWorkflow.IsActivated ,
        dbo.ColorProofWorkflow.IsInlineProofVerificationSupported ,
        dbo.ColorProofWorkflow.SupportedSpotColors ,
        dbo.ColorProofWorkflow.ColorProofInstance,
        dbo.ColorProofInstance.SystemName,
        convert(bit, 0) AS IsFromCurrentAccount          
FROM    dbo.ColorProofCoZoneWorkflow
        INNER JOIN dbo.ColorProofWorkflow ON dbo.ColorProofCoZoneWorkflow.ColorProofWorkflow = dbo.ColorProofWorkflow.ID
        INNER JOIN dbo.ColorProofInstance ON dbo.ColorProofWorkflow.ColorProofInstance = dbo.ColorProofInstance.ID
WHERE   dbo.ColorProofInstance.IsDeleted = 0
        AND dbo.ColorProofCoZoneWorkflow.IsDeleted = 0
GO
/****** Object:  View [dbo].[ReturnAccountParametersView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

-----------------------------------------------------------------------------------
-- Alter Return Account Parameters view
CREATE VIEW [dbo].[ReturnAccountParametersView] 
AS 
SELECT  0 AS AccountID,
'' AS AccountName,
0 AS AccountTypeID,
'' AS AccountTypeName,
0 AS CollaborateBillingPlanId,
'' AS CollborateBillingPlanName,
0 AS ManageBillingPlanId,
'' AS ManageBillingPlanName,
0 AS DeliverBillingPlanId,
'' AS DeliverBillingPlanName,
0 AS QualityControlBillingPlanId,
'' AS QualityControlBillingPlanName,
0 AS AccountStatusID,
'' AS AccountStatusName,
0 AS LocationID,
'' AS LocationName


GO
/****** Object:  View [dbo].[ReturnAccountUserCollaboratorsAndGroupsView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------------------------------------------
-- Add Return Account User Collaborators And Groups procedure
CREATE VIEW [dbo].[ReturnAccountUserCollaboratorsAndGroupsView]
AS
SELECT  CONVERT(BIT, 0) AS IsGroup ,
        CONVERT(BIT, 0) AS IsExternal ,
        CONVERT(BIT, 0) AS IsChecked ,
        0 AS ID ,
        0 AS Count ,
        '' AS Members ,
        '' AS Name ,
        0 AS Role
GO
/****** Object:  View [dbo].[ReturnApprovalInTreeView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------------------------------------------
-- Alter Return Account Parameters view
CREATE VIEW [dbo].[ReturnApprovalInTreeView] 
AS 
SELECT  0 AS [ApprovalID],
'' AS [FileName],
'' AS [Guid],
'' AS [Region],
'' AS [Domain],
'' AS [ApprovalOwner],
0 AS FolderID

GO
/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--Add IsExternalPrimaryDecisionMaker property
CREATE VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			'' AS [Status],
			'' AS [Guid],
			'' AS [FileName],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			'' AS PrimaryDecisionMakerName,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			CONVERT(bit, 0) AS HasAnnotations,
			'' AS Decision,
			CONVERT(bit, 0) AS IsCollaborator,
			CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,
			CONVERT(DateTime, GETDATE()) AS ApprovalApprovedDate,
			0 AS CurrentPhase,
			'' AS PhaseName,
			'' AS WorkflowName,
			'' AS NextPhase,
			'' AS DecisionMakers,
			0 AS DecisionType,
			'' AS OwnerName,
			CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
			NULL AS JobOwner,
			CONVERT(bit, 0) AS IsPhaseComplete,
			'' AS [VersionSufix],
			CONVERT(bit, 0) AS IsLocked,
			CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker

GO
/****** Object:  View [dbo].[ReturnBillingReportInfoView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--alter billing report view
CREATE VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS CollaborateBillingPlanName,
0 AS CollaborateProofs,
'' AS CollaborateBillingCycleDetails,
0 AS CollaborateBillingPlan,
CONVERT(BIT, 0) AS CollaborateIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS CollaborateContractStartDate,
CONVERT(BIT, 0) as CollaborateIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [CollaborateIsQuotaAllocationEnabled],  
'' AS ManageBillingPlanName,
0 AS ManageProofs,
'' AS ManageBillingCycleDetails,
0 AS ManageBillingPlan,
CONVERT(BIT, 0) AS ManageIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS ManageContractStartDate,
CONVERT(BIT, 0) AS ManageIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [ManageIsQuotaAllocationEnabled],
'' AS DeliverBillingPlanName,
0 AS DeliverProofs,
'' AS DeliverBillingCycleDetails,
0 AS DeliverBillingPlan,
CONVERT(BIT, 0) AS DeliverIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS DeliverContractStartDate,
CONVERT(BIT, 0) AS DeliverIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [DeliverIsQuotaAllocationEnabled],
'' AS ManageApiBillingPlanName,
0 AS ManageApiProofs,
'' AS ManageApiBillingCycleDetails,
0 AS ManageApiBillingPlan,
CONVERT(BIT, 0) AS ManageApiIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS ManageApiContractStartDate ,
CONVERT(BIT, 0) AS ManageApiIsMonthlyBillingFrequency ,
CONVERT(BIT, 0) as [ManageApiIsQuotaAllocationEnabled],
'' as [QualityControlBillingPlanName],
0 as[QualityControlProofs],
'' [QualityControlBillingCycleDetails],
0 [QualityControlBillingPlan],
CONVERT(BIT, 0) [QualityControlIsFixedPlan],
CONVERT(DATETIME, GETDATE()) as [QualityControlContractStartDate],
CONVERT(BIT, 0) as [QualityControlIsMonthlyBillingFrequency],
CONVERT(BIT, 0) as [QualityControlIsQuotaAllocationEnabled],
0.0 AS GPPDiscount,
0 AS Parent,
'' AS AccountPath


GO
/****** Object:  View [dbo].[ReturnDeliversPageView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-----------------------------------------------------------------------------------
-- Add Return Delivers Page View
CREATE VIEW [dbo].[ReturnDeliversPageView] 
AS 
SELECT  0 AS [ID] ,
'' AS [Title] ,
'' AS [User] ,
'' AS [Pages] ,
'' AS [CPName] ,
'' AS [Workflow] ,
GETDATE() AS [CreatedDate] ,
0 AS [StatusKey],
CONVERT(BIT, 0) AS [IsShared]

GO
/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE VIEW [dbo].[ReturnFolderTreeView] 
AS 
	SELECT  0 AS ID,
			0 AS Parent,
			'' AS Name,
			0 AS Creator,
			0 AS [Level],
			CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,
			'' AS Collaborators,
			CONVERT(bit, 0) AS HasApprovals

GO
/****** Object:  View [dbo].[ReturnStringView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnStringView] 
AS 
	SELECT  '' as RetVal

GO
/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- CZ-2970 Extend User Report
CREATE VIEW [dbo].[ReturnUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Name,
			'' AS StatusName,
			'' AS UserName,
			'' AS EmailAddress,
			GETDATE() AS DateLastLogin,
			'' AS AccountPath, 
			0 AS CollaborateApprovalsNumber,
			0 AS DeliverJobsNumber



GO
/****** Object:  View [dbo].[SecondaryNavigationMenuItem]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[SecondaryNavigationMenuItem] 
AS 
SELECT  0 AS [MenuId], 
        0 AS [Parent], 
        '' AS [Action], 
        '' AS [Controller], 
        '' AS [DisplayName], 
        CONVERT(BIT, 0) AS [Active], 
        '' AS [MenuKey], 
        0 AS [Level],
        0 AS [Position]
GO
/****** Object:  View [dbo].[UserMenuItemRoleView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserMenuItemRoleView]
AS
SELECT TOP 100 PERCENT
        *
FROM    ( SELECT    atr.[AccountType] ,
                    atr.[Role] ,
                    mi.[ID] AS [MenuItem] ,
                    mi.[Key] ,
                    mi.[Name] AS [MenuName] ,
                    mi.[Title] ,
                    ISNULL(mi.[Parent], 0) AS [Parent] ,
                    mi.[Position] ,
                    mi.[IsAdminAppOwned] ,
                    mi.[IsAlignedLeft] ,
                    mi.[IsSubNav] ,
                    mi.[IsTopNav] ,
                    mi.[IsVisible] ,
                    ca.[ID] AS [ControllerAction] ,
                    ca.[Controller] ,
                    ca.[Action] ,
                    ca.[Parameters]
          FROM      [dbo].[AccountTypeRole] atr
                    JOIN [dbo].[MenuItemAccountTypeRole] mirat ON atr.[ID] = mirat.[AccountTypeRole]
                    JOIN [dbo].[MenuItem] mi ON mirat.[MenuItem] = mi.[ID]
                    LEFT OUTER JOIN [dbo].[ControllerAction] ca ON mi.ControllerAction = ca.ID
          UNION
          SELECT    0 AS [AccountType] ,
                    0 AS [Role] ,
                    mi.[ID] AS [MenuItem] ,
                    mi.[Key] ,
                    mi.[Name] AS [MenuName] ,
                    mi.[Title] ,
                    ISNULL(mi.[Parent], 0) AS [Parent] ,
                    mi.[Position] ,
                    mi.[IsAdminAppOwned] ,
                    mi.[IsAlignedLeft] ,
                    mi.[IsSubNav] ,
                    mi.[IsTopNav] ,
                    mi.[IsVisible] ,
                    0 AS [ControllerAction] ,
                    '' AS [Controller] ,
                    '' AS [Action] ,
                    '' AS [Parameters]
          FROM      dbo.MenuItem mi
          WHERE     ControllerAction IS NULL
        ) SLCT
ORDER BY SLCT.[AccountType] ASC ,
        SLCT.[Role] ASC ,
        SLCT.[MenuItem] ASC ,
        SLCT.[Parent] ,
        SLCT.[Position]
GO
/****** Object:  View [dbo].[UserRolesView]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Alter UserRolesView
CREATE VIEW [dbo].[UserRolesView] AS
SELECT  U.ID ,
        U.GivenName ,
        u.FamilyName ,
        u.Username ,
        U.EmailAddress ,
        COALESCE(CR.[Key], 'NON') AS [CollaborateRole] ,
        COALESCE(MR.[Key], 'NON') AS [ManageRole] ,
        COALESCE(DR.[Key], 'NON') AS [DeliverRole] ,
        COALESCE(AR.[Key], 'NON') AS [AdminRole] ,
        COALESCE(SR.[Key], 'NON') AS [SysAdminRole] ,
        COALESCE(PR.[Key], 'NON') AS [ProfileServerRole] ,
		COALESCE(QR.[Key], 'NON') AS [QualityControlRole] ,
        u.DateLastLogin ,
        u.Account ,
        US.[Key] AS [UserStatus]
FROM    dbo.[User] AS U
        INNER JOIN dbo.UserStatus US ON U.Status = US.ID
        LEFT JOIN ( SELECT  CUR.[User] ,
                            CR.[Key]
                    FROM    dbo.UserRole CUR
                            INNER JOIN dbo.Role CR ON CUR.Role = CR.ID
                            INNER JOIN dbo.AppModule CAM ON CR.AppModule = CAM.ID
                                                            AND CAM.[Key] = 0
                  ) AS CR ON U.ID = CR.[User]
        LEFT JOIN ( SELECT  MUR.[User] ,
                            MR.[Key]
                    FROM    dbo.UserRole MUR
                            INNER JOIN dbo.Role MR ON MUR.Role = MR.ID
                            INNER JOIN dbo.AppModule MAM ON MR.AppModule = MAM.ID
                                                            AND MAM.[Key] = 1
                  ) AS MR ON U.ID = MR.[User]
        LEFT JOIN ( SELECT  DUR.[User] ,
                            DR.[Key]
                    FROM    dbo.UserRole DUR
                            INNER JOIN dbo.Role DR ON DUR.Role = DR.ID
                            INNER JOIN dbo.AppModule DAM ON DR.AppModule = DAM.ID
                                                            AND DAM.[Key] = 2
                  ) AS DR ON U.ID = DR.[User]
        LEFT JOIN ( SELECT  AUR.[User] ,
                            AR.[Key]
                    FROM    dbo.UserRole AUR
                            INNER JOIN dbo.Role AR ON AUR.Role = AR.ID
                            INNER JOIN dbo.AppModule AAM ON AR.AppModule = AAM.ID
                                                            AND AAM.[Key] = 3
                  ) AS AR ON U.ID = AR.[User]
        LEFT JOIN ( SELECT  SUR.[User] ,
                            SR.[Key]
                    FROM    dbo.UserRole SUR
                            INNER JOIN dbo.Role SR ON SUR.Role = SR.ID
                            INNER JOIN dbo.AppModule SAM ON SR.AppModule = SAM.ID
                                                            AND SAM.[Key] = 4
                  ) AS SR ON U.ID = SR.[User]
		LEFT JOIN ( SELECT  PUR.[User] ,
					PR.[Key]
					FROM    dbo.UserRole PUR
							INNER JOIN dbo.Role PR ON PUR.Role = PR.ID
							INNER JOIN dbo.AppModule PAM ON PR.AppModule = PAM.ID
															AND PAM.[Key] = 5
					) AS PR ON U.ID = PR.[User]
		LEFT JOIN ( SELECT  QUR.[User] ,
							QR.[Key]
					FROM    dbo.UserRole QUR
							INNER JOIN dbo.Role QR ON QUR.Role = QR.ID
							INNER JOIN dbo.AppModule QAM ON QR.AppModule = QAM.ID
															AND QAM.[Key] = 6
					) AS QR ON U.ID = QR.[User]

GO
/****** Object:  Index [IX_AppModule]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_AppModule] ON [dbo].[AppModule]
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Approval]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_Approval] ON [dbo].[Approval]
(
	[IsDeleted] ASC,
	[DeletePermanently] ASC,
	[Job] ASC
)
INCLUDE ( 	[Version]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Approval_Owner]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_Approval_Owner] ON [dbo].[Approval]
(
	[Job] ASC,
	[IsDeleted] ASC,
	[DeletePermanently] ASC,
	[Owner] ASC
)
INCLUDE ( 	[Version]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Approval_Phase]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_Approval_Phase] ON [dbo].[Approval]
(
	[ID] ASC,
	[IsDeleted] ASC,
	[Owner] ASC,
	[Job] ASC,
	[Type] ASC,
	[CurrentPhase] ASC,
	[PrimaryDecisionMaker] ASC,
	[ExternalPrimaryDecisionMaker] ASC,
	[DeletePermanently] ASC
)
INCLUDE ( 	[FileName],
	[Guid],
	[Version],
	[CreatedDate],
	[Creator],
	[Deadline],
	[ModifiedDate],
	[LockProofWhenAllDecisionsMade],
	[IsLocked],
	[AllowDownloadOriginal],
	[IsError],
	[AllowOtherUsersToBeAdded],
	[VersionSufix]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ApprovalAnnotation_Page]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalAnnotation_Page] ON [dbo].[ApprovalAnnotation]
(
	[Page] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ApprovalCollaborator_Approval_Phase]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalCollaborator_Approval_Phase] ON [dbo].[ApprovalCollaborator]
(
	[Approval] ASC,
	[Phase] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ApprovalCollaborator_Collaborator]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalCollaborator_Collaborator] ON [dbo].[ApprovalCollaborator]
(
	[Collaborator] ASC
)
INCLUDE ( 	[Approval],
	[Phase]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ApprovalCollaboratorDecision]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalCollaboratorDecision] ON [dbo].[ApprovalCollaboratorDecision]
(
	[Approval] ASC,
	[Phase] ASC,
	[Decision] ASC,
	[ID] ASC,
	[Collaborator] ASC,
	[ExternalCollaborator] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ApprovalJobPhase]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalJobPhase] ON [dbo].[ApprovalJobPhase]
(
	[IsActive] ASC,
	[ApprovalJobWorkflow] ASC,
	[Position] ASC
)
INCLUDE ( 	[Name]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ApprovalJobPhase_ApprovalJobWorkflow_Position_Name]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalJobPhase_ApprovalJobWorkflow_Position_Name] ON [dbo].[ApprovalJobPhase]
(
	[ApprovalJobWorkflow] ASC,
	[Position] ASC
)
INCLUDE ( 	[Name]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ApprovalPage_Approval]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_ApprovalPage_Approval] ON [dbo].[ApprovalPage]
(
	[Approval] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ApprovalUserViewInfo_ViewedDate_IX]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [ApprovalUserViewInfo_ViewedDate_IX] ON [dbo].[ApprovalUserViewInfo]
(
	[Approval] ASC
)
INCLUDE ( 	[ViewedDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_FolderApproval_Approval]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_FolderApproval_Approval] ON [dbo].[FolderApproval]
(
	[Approval] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Job_Account]    Script Date: 8/8/2018 11:35:44 AM ******/
CREATE NONCLUSTERED INDEX [IX_Job_Account] ON [dbo].[Job]
(
	[Account] ASC
)
INCLUDE ( 	[ID],
	[Title],
	[Status],
	[JobOwner]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsSuspendedOrDeletedByParent]  DEFAULT ((0)) FOR [IsSuspendedOrDeletedByParent]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsCustomDomainActive]  DEFAULT ((0)) FOR [IsCustomDomainActive]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsHideLogoInFooter]  DEFAULT ((0)) FOR [IsHideLogoInFooter]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsHideLogoInLoginFooter]  DEFAULT ((0)) FOR [IsHideLogoInLoginFooter]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsNeedProfileManagement]  DEFAULT ((0)) FOR [IsNeedProfileManagement]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsNeedPDFsToBeColorManaged]  DEFAULT ((0)) FOR [IsNeedPDFsToBeColorManaged]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsRemoveAllGMGCollaborateBranding]  DEFAULT ((0)) FOR [IsRemoveAllGMGCollaborateBranding]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsEnabledLog]  DEFAULT ((0)) FOR [IsEnabledLog]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsHelpCentreActive]  DEFAULT ((0)) FOR [IsHelpCentreActive]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsAgreedToTermsAndConditions]  DEFAULT ((0)) FOR [IsAgreedToTermsAndConditions]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_DisableLandingPage]  DEFAULT ((0)) FOR [DisableLandingPage]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_DashboardToShow]  DEFAULT ((0)) FOR [DashboardToShow]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsTrial]  DEFAULT ((0)) FOR [IsTrial]
GO
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF__Account__EnableN__70FDBF69]  DEFAULT ((0)) FOR [EnableNotificationsByDefault]
GO
ALTER TABLE [dbo].[AccountNotificationPreset] ADD  CONSTRAINT [DF__AccountNo__Disab__0B7CAB7B]  DEFAULT ((0)) FOR [DisablePersonalNotifications]
GO
ALTER TABLE [dbo].[AccountNotificationPreset] ADD  DEFAULT (NULL) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AccountNotificationPreset] ADD  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[AccountNotificationPreset] ADD  DEFAULT (NULL) FOR [ModifiedDate]
GO
ALTER TABLE [dbo].[AccountNotificationPreset] ADD  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType] ADD  DEFAULT (NULL) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType] ADD  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType] ADD  DEFAULT (NULL) FOR [ModifiedDate]
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType] ADD  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[AccountSubscriptionPlan] ADD  CONSTRAINT [DF_AccountSubscriptionPlan_IsManageAPI]  DEFAULT ((0)) FOR [IsManageAPI]
GO
ALTER TABLE [dbo].[AccountSubscriptionPlan] ADD  CONSTRAINT [DF__AccountSu__Allow__4B622666]  DEFAULT ((0)) FOR [AllowOverdraw]
GO
ALTER TABLE [dbo].[AppModule] ADD  CONSTRAINT [DF_AppModule_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_LockProofWhenAllDecisionsMade]  DEFAULT ((0)) FOR [LockProofWhenAllDecisionsMade]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_OnlyOneDecisionRequired]  DEFAULT ((0)) FOR [OnlyOneDecisionRequired]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_AllowDownloadOriginal]  DEFAULT ((0)) FOR [AllowDownloadOriginal]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_Size]  DEFAULT ((0)) FOR [Size]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Account_IsError]  DEFAULT ((0)) FOR [IsError]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF__Approval__Type__689D8392]  DEFAULT ((1)) FOR [Type]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Approval_ProcessingInProgress]  DEFAULT ('completed') FOR [ProcessingInProgress]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF_Approval_JobSource]  DEFAULT ((1)) FOR [JobSource]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF__Approval__Delete__4A6E022D]  DEFAULT ((0)) FOR [DeletePermanently]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF__Approval__LockPr__6521F869]  DEFAULT ((0)) FOR [LockProofWhenFirstDecisionsMade]
GO
ALTER TABLE [dbo].[Approval] ADD  CONSTRAINT [DF__Approval__AllowO__1E5A75C5]  DEFAULT ((0)) FOR [AllowOtherUsersToBeAdded]
GO
ALTER TABLE [dbo].[Approval] ADD  DEFAULT ((0)) FOR [RestrictedZoom]
GO
ALTER TABLE [dbo].[Approval] ADD  DEFAULT ((0)) FOR [OverdueNotificationSent]
GO
ALTER TABLE [dbo].[Approval] ADD  DEFAULT ((0)) FOR [SOADState]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD  CONSTRAINT [DF_ApprovalAnnotation_StartIndex]  DEFAULT ((0)) FOR [StartIndex]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD  CONSTRAINT [DF_ApprovalAnnotation_EndIndex]  DEFAULT ((0)) FOR [EndIndex]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD  CONSTRAINT [DF_ApprovalAnnotation_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD  CONSTRAINT [DF_ApprovalAnnotation_machineId]  DEFAULT ('') FOR [machineId]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD  CONSTRAINT [DF_ApprovalAnnotation_CalibrationStatus]  DEFAULT ((0)) FOR [CalibrationStatus]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD  DEFAULT ((1)) FOR [AnnotatedOnImageWidth]
GO
ALTER TABLE [dbo].[ApprovalCollaborator] ADD  DEFAULT ((0)) FOR [SOADState]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  DEFAULT ((1)) FOR [ShowAnnotationsToUsersOfOtherPhases]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  DEFAULT ((0)) FOR [ShowAnnotationsOfOtherPhasesToExternalUsers]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  DEFAULT ((0)) FOR [PhaseTemplateID]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  DEFAULT ((0)) FOR [OverdueNotificationSent]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  CONSTRAINT [DF_ApprovalJobPhase_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalJobPhase] ADD  CONSTRAINT [DF_ApprovalJobPhase_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval] ADD  DEFAULT ((0)) FOR [PhaseMarkedAsCompleted]
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval] ADD  DEFAULT ((0)) FOR [SOADState]
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval] ADD  CONSTRAINT [DF_ApprovalJobPhaseApproval_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval] ADD  CONSTRAINT [DF_ApprovalJobPhaseApproval_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow] ADD  CONSTRAINT [DF_ApprovalJobWorkflow_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow] ADD  CONSTRAINT [DF_ApprovalJobWorkflow_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow] ADD  DEFAULT ((0)) FOR [RerunWorkflow]
GO
ALTER TABLE [dbo].[ApprovalPhase] ADD  DEFAULT ((1)) FOR [ShowAnnotationsToUsersOfOtherPhases]
GO
ALTER TABLE [dbo].[ApprovalPhase] ADD  DEFAULT ((0)) FOR [ShowAnnotationsOfOtherPhasesToExternalUsers]
GO
ALTER TABLE [dbo].[ApprovalPhase] ADD  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[ApprovalPhase] ADD  DEFAULT ((0)) FOR [ApprovalShouldBeViewedByAll]
GO
ALTER TABLE [dbo].[ApprovalPhase] ADD  CONSTRAINT [DF_ApprovalPhase_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalPhase] ADD  CONSTRAINT [DF_ApprovalPhase_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator] ADD  CONSTRAINT [DF_ApprovalPhaseCollaborator_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator] ADD  CONSTRAINT [DF_ApprovalPhaseCollaborator_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaboratorGroup] ADD  CONSTRAINT [DF_ApprovalPhaseCollaboratorGroup_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaboratorGroup] ADD  CONSTRAINT [DF_ApprovalPhaseCollaboratorGroup_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalSeparationPlate] ADD  CONSTRAINT [DF__ApprovalS__IsSpo__53F76C67]  DEFAULT ((0)) FOR [IsSpotChannel]
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] ADD  DEFAULT (NULL) FOR [ExternalUser]
GO
ALTER TABLE [dbo].[ApprovalWorkflow] ADD  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[ApprovalWorkflow] ADD  CONSTRAINT [DF_ApprovalWorkflow_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[ApprovalWorkflow] ADD  CONSTRAINT [DF_ApprovalWorkflow_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[ApprovalWorkflow] ADD  DEFAULT ((0)) FOR [RerunWorkflow]
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] ADD  CONSTRAINT [DF_AttachedAccountBillingPlan_IsAppliedCurrentRate]  DEFAULT ((0)) FOR [IsAppliedCurrentRate]
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] ADD  CONSTRAINT [DF_AttachedAccountBillingPlan_IsModifiedProofPrice]  DEFAULT ((0)) FOR [IsModifiedProofPrice]
GO
ALTER TABLE [dbo].[BillingPlan] ADD  CONSTRAINT [DF_BillingPlan_Price]  DEFAULT ((0.0)) FOR [Price]
GO
ALTER TABLE [dbo].[BillingPlan] ADD  CONSTRAINT [DF_BillingPlan_IsFixed]  DEFAULT ((0)) FOR [IsFixed]
GO
ALTER TABLE [dbo].[BillingPlan] ADD  CONSTRAINT [DF__BillingPl__IsDem__02E7657A]  DEFAULT ((0)) FOR [IsDemo]
GO
ALTER TABLE [dbo].[BillingPlanType] ADD  CONSTRAINT [DF_BillingPlanType_EnablePrePressTools]  DEFAULT ((0)) FOR [EnablePrePressTools]
GO
ALTER TABLE [dbo].[BillingPlanType] ADD  CONSTRAINT [DF_BillingPlanType_EnableMediaTools]  DEFAULT ((0)) FOR [EnableMediaTools]
GO
ALTER TABLE [dbo].[BillingPlanType] ADD  CONSTRAINT [DF__BillingPl__AppMo__50C5FA01]  DEFAULT ((1)) FOR [AppModule]
GO
ALTER TABLE [dbo].[CMSDownload] ADD  CONSTRAINT [DF_CMSDownload_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[CMSDownload] ADD  CONSTRAINT [DF_CMSDownload_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[CMSManual] ADD  CONSTRAINT [DF_CMSManual_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[CMSManual] ADD  CONSTRAINT [DF_CMSManual_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[CMSVideo] ADD  CONSTRAINT [DF_CMSVideo_IsTrainning]  DEFAULT ((0)) FOR [IsTrainning]
GO
ALTER TABLE [dbo].[CMSVideo] ADD  CONSTRAINT [DF_CMSVideo_VideoUrl]  DEFAULT ('') FOR [VideoUrl]
GO
ALTER TABLE [dbo].[CMSVideo] ADD  CONSTRAINT [DF_CMSVideo_VideoDescription]  DEFAULT ('') FOR [VideoDescription]
GO
ALTER TABLE [dbo].[CMSVideo] ADD  CONSTRAINT [DF__CMSVideo__VideoT__442B18F2]  DEFAULT ('') FOR [VideoTitle]
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflow] ADD  CONSTRAINT [DF_ColorProofCoZoneWorkflow_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ColorProofInstance] ADD  CONSTRAINT [DF_ColorProofInstance_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ColorProofInstanceState] ADD  CONSTRAINT [DF_ColorProofInstanceState_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ColorProofJobStatus] ADD  CONSTRAINT [DF_ColorProofJobStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[Company] ADD  CONSTRAINT [DF_Company_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[Container] ADD  CONSTRAINT [DF__Container__Statu__284DF453]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[ContainerFile] ADD  CONSTRAINT [DF__Container__IsCom__6C390A4C]  DEFAULT ((0)) FOR [IsComplete]
GO
ALTER TABLE [dbo].[ConversionStatus] ADD  CONSTRAINT [DF_ConversionStatus_Key]  DEFAULT ((0)) FOR [Key]
GO
ALTER TABLE [dbo].[ConversionStatus] ADD  CONSTRAINT [DF_ConversionStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator] ADD  CONSTRAINT [DF__DeliverExt__Guid__40E497F3]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[DeliverJob] ADD  CONSTRAINT [DF_DeliverJob_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[DeliverJob] ADD  CONSTRAINT [DF_DeliverJob_IncludeProofMetaInformationOnLabel]  DEFAULT ((0)) FOR [IncludeProofMetaInformationOnLabel]
GO
ALTER TABLE [dbo].[DeliverJob] ADD  CONSTRAINT [DF_DeliverJob_LogProofControlResults]  DEFAULT ((0)) FOR [LogProofControlResults]
GO
ALTER TABLE [dbo].[DeliverJob] ADD  CONSTRAINT [DF_DeliverJob_Size]  DEFAULT ((0)) FOR [Size]
GO
ALTER TABLE [dbo].[DeliverJob] ADD  CONSTRAINT [DF__DeliverJo__IsDel__4336F4B9]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[DeliverJob] ADD  CONSTRAINT [DF_DeliverJob_JobSource]  DEFAULT ((1)) FOR [JobSource]
GO
ALTER TABLE [dbo].[DeliverJobPageStatus] ADD  CONSTRAINT [DF_DeliverJobPageStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[DeliverJobStatus] ADD  CONSTRAINT [DF_DeliverJobStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ExternalCollaborator] ADD  CONSTRAINT [DF__ExternalC__IsDel__4959E263]  DEFAULT ((1)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ExternalCollaborator] ADD  DEFAULT ((0)) FOR [ShowAnnotationsOfAllPhases]
GO
ALTER TABLE [dbo].[Folder] ADD  CONSTRAINT [DF_Folder_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Folder] ADD  CONSTRAINT [DF_Folder_IsPrivate]  DEFAULT ((1)) FOR [IsPrivate]
GO
ALTER TABLE [dbo].[Folder] ADD  CONSTRAINT [DF__Folder__AllColla__39788055]  DEFAULT ((0)) FOR [AllCollaboratorsDecisionRequired]
GO
ALTER TABLE [dbo].[Folder] ADD  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[Job] ADD  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[ManageConversion] ADD  CONSTRAINT [DF_ManageConversion_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ManageConversion] ADD  CONSTRAINT [DF_ManageConversion_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ManageJob] ADD  CONSTRAINT [DF__ManageJob__FileT__1D114BD1]  DEFAULT ((3)) FOR [FileType]
GO
ALTER TABLE [dbo].[ManageJob] ADD  CONSTRAINT [DF_ManageJob_JobSource]  DEFAULT ((1)) FOR [JobSource]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] ADD  CONSTRAINT [DF_ManageOutputColorSpace_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] ADD  CONSTRAINT [DF_ManageOutputColorSpace_FileGuid]  DEFAULT ('') FOR [FileGuid]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] ADD  CONSTRAINT [DF_ManageOutputColorSpace_FileName]  DEFAULT ('') FOR [FileName]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] ADD  CONSTRAINT [DF_ManageOutputColorSpace_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] ADD  CONSTRAINT [DF_ManageOutputColorSpace_ManageGUI]  DEFAULT ((0)) FOR [ManageGUI]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] ADD  CONSTRAINT [DF_ManageOutputColorSpace_ManageAPI]  DEFAULT ((0)) FOR [ManageAPI]
GO
ALTER TABLE [dbo].[ManagePreflight] ADD  CONSTRAINT [DF_ManagePreflight_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ManagePreflight] ADD  CONSTRAINT [DF_ManagePreflight_ManageGUI]  DEFAULT ((0)) FOR [ManageGUI]
GO
ALTER TABLE [dbo].[ManagePreflight] ADD  CONSTRAINT [DF_ManagePreflight_ManageAPI]  DEFAULT ((0)) FOR [ManageAPI]
GO
ALTER TABLE [dbo].[NotificationItem] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[NotificationSchedule] ADD  DEFAULT ((0)) FOR [NotificationPerPSSession]
GO
ALTER TABLE [dbo].[NotificationSchedule] ADD  DEFAULT (NULL) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[NotificationSchedule] ADD  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[NotificationSchedule] ADD  DEFAULT (NULL) FOR [ModifiedDate]
GO
ALTER TABLE [dbo].[NotificationSchedule] ADD  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[NotificationScheduleUser] ADD  DEFAULT (NULL) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[NotificationScheduleUser] ADD  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup] ADD  DEFAULT (NULL) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup] ADD  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup] ADD  DEFAULT (NULL) FOR [ModifiedDate]
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup] ADD  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[PreflightStatus] ADD  CONSTRAINT [DF_PreflightStatus_Key]  DEFAULT ((0)) FOR [Key]
GO
ALTER TABLE [dbo].[PreflightStatus] ADD  CONSTRAINT [DF_PreflightStatus_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[ProofStudioSessions] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] ADD  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] ADD  DEFAULT ((0)) FOR [HasOriginalFile]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_Role_AppModule]  DEFAULT ((1)) FOR [AppModule]
GO
ALTER TABLE [dbo].[Shape] ADD  CONSTRAINT [DF_Shape_IsCustom]  DEFAULT ((0)) FOR [IsCustom]
GO
ALTER TABLE [dbo].[SharedApproval] ADD  CONSTRAINT [DF__SharedApp__IsSha__4D2A7347]  DEFAULT ((1)) FOR [IsSharedURL]
GO
ALTER TABLE [dbo].[SharedApproval] ADD  CONSTRAINT [DF__SharedApp__IsSha__4E1E9780]  DEFAULT ((1)) FOR [IsSharedDownloadURL]
GO
ALTER TABLE [dbo].[SharedApproval] ADD  CONSTRAINT [DF__SharedApp__IsExp__4F12BBB9]  DEFAULT ((0)) FOR [IsExpired]
GO
ALTER TABLE [dbo].[SharedApproval] ADD  DEFAULT ((0)) FOR [SOADState]
GO
ALTER TABLE [dbo].[SharedApprovalPhase] ADD  CONSTRAINT [DF_SharedApprovalPhase_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[SharedApprovalPhase] ADD  CONSTRAINT [DF_SharedApprovalPhase_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[SimulationProfile] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] ADD  CONSTRAINT [DF__SoftProof__ZoomL__764C846B]  DEFAULT ((0)) FOR [ZoomLevel]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] ADD  CONSTRAINT [DF__SoftProof__ViewT__7740A8A4]  DEFAULT (NULL) FOR [ViewTiles]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] ADD  CONSTRAINT [LastAccessedDate]  DEFAULT (getdate()) FOR [LastAccessedDate]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] ADD  DEFAULT ((0)) FOR [DefaultSession]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] ADD  DEFAULT ((0)) FOR [HighResolution]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] ADD  DEFAULT ((0)) FOR [HighResolutionInProgress]
GO
ALTER TABLE [dbo].[StudioSetting] ADD  CONSTRAINT [DF_StudioSetting_IncludeMyOwnActivity]  DEFAULT ((0)) FOR [IncludeMyOwnActivity]
GO
ALTER TABLE [dbo].[StudioSetting] ADD  CONSTRAINT [DF_StudioSetting_EnableColorCorrection]  DEFAULT ((0)) FOR [EnableColorCorrection]
GO
ALTER TABLE [dbo].[SubscriberPlanInfo] ADD  CONSTRAINT [DF_SubscriberPlanInfo_NrOfCredits]  DEFAULT ((0)) FOR [NrOfCredits]
GO
ALTER TABLE [dbo].[SubscriberPlanInfo] ADD  CONSTRAINT [DF_SubscriberPlanInfo_IsQuotaAllocationEnabled]  DEFAULT ((0)) FOR [IsQuotaAllocationEnabled]
GO
ALTER TABLE [dbo].[SubscriberPlanInfo] ADD  CONSTRAINT [DF_SubscriberPlanInfo_IsManageAPI]  DEFAULT ((0)) FOR [IsManageAPI]
GO
ALTER TABLE [dbo].[SubscriberPlanInfo] ADD  CONSTRAINT [DF__Subscribe__Allow__4C564A9F]  DEFAULT ((0)) FOR [AllowOverdraw]
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings] ADD  CONSTRAINT [DF__UploadEng__Apply__3B60C8C7]  DEFAULT ((0)) FOR [ApplyUploadSettingsForNewApproval]
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings] ADD  CONSTRAINT [DF__UploadEng__UseUp__55209ACA]  DEFAULT ((0)) FOR [UseUploadSettingsFromPreviousVersion]
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings] ADD  CONSTRAINT [DF__UploadEng__Enabl__642DD430]  DEFAULT ((0)) FOR [EnableVersionMirroring]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_IncludeMyOwnActivity]  DEFAULT ((0)) FOR [IncludeMyOwnActivity]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_NeedReLogin]  DEFAULT ((0)) FOR [NeedReLogin]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF__User__ProofStudi__3E3D3572]  DEFAULT ((1)) FOR [ProofStudioColor]
GO
ALTER TABLE [dbo].[User] ADD  DEFAULT ((0)) FOR [IsSsoUser]
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] ADD  DEFAULT (NULL) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] ADD  DEFAULT ((0)) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] ADD  DEFAULT (NULL) FOR [ModifiedDate]
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] ADD  DEFAULT ((0)) FOR [ModifiedBy]
GO
ALTER TABLE [dbo].[UserGroup] ADD  CONSTRAINT [DF_UserGroup_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[UserGroup] ADD  CONSTRAINT [DF__UserGroup__IsPro__6B44E613]  DEFAULT ((0)) FOR [IsProfileServerOwnershipGroup]
GO
ALTER TABLE [dbo].[UserGroupUser] ADD  CONSTRAINT [DF__UserGroup__IsPri__43F60EC8]  DEFAULT ((0)) FOR [IsPrimary]
GO
ALTER TABLE [dbo].[UserHistory] ADD  CONSTRAINT [DF_UserHistory_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[UserLogin] ADD  CONSTRAINT [DF_UserLogin_Success]  DEFAULT ((0)) FOR [Success]
GO
ALTER TABLE [dbo].[Workstation] ADD  CONSTRAINT [DF_Workstation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] ADD  CONSTRAINT [DF_WorkstationCalibrationData_LastErrorMessage]  DEFAULT ('') FOR [LastErrorMessage]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] ADD  CONSTRAINT [DF_WorkstationCalibrationData_CalibrationUploadStatus]  DEFAULT ((1)) FOR [CalibrationUploadStatus]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] ADD  CONSTRAINT [DF_WorkstationCalibrationData_DisplayName]  DEFAULT ('') FOR [DisplayName]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] ADD  DEFAULT ('') FOR [DisplayProfileName]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] ADD  DEFAULT ((0)) FOR [DisplayWidth]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] ADD  DEFAULT ((0)) FOR [DisplayHeight]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_AccountStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[AccountStatus] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountStatus]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountType]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_ContractPeriod] FOREIGN KEY([ContractPeriod])
REFERENCES [dbo].[ContractPeriod] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_ContractPeriod]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Creator]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Currency] FOREIGN KEY([CurrencyFormat])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Currency]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_DateFormat] FOREIGN KEY([DateFormat])
REFERENCES [dbo].[DateFormat] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_DateFormat]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_DeletedBy] FOREIGN KEY([DeletedBy])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_DeletedBy]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Locale] FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Locale]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Modifier]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Owner]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Parent]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_TimeFormat] FOREIGN KEY([TimeFormat])
REFERENCES [dbo].[TimeFormat] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_TimeFormat]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_UploadEngineAccountSettings] FOREIGN KEY([UploadEngineSetings])
REFERENCES [dbo].[UploadEngineAccountSettings] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_UploadEngineAccountSettings]
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_AccountBillingPlanChangeLog_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_Account]
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_AccountBillingPlanChangeLog_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_Creator]
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_AccountBillingPlanChangeLog_CurrentPlan] FOREIGN KEY([CurrentPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_CurrentPlan]
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_AccountBillingPlanChangeLog_PreviousPlan] FOREIGN KEY([PreviousPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_PreviousPlan]
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory]  WITH CHECK ADD  CONSTRAINT [FK_AccountBillingTransactionHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory] CHECK CONSTRAINT [FK_AccountBillingTransactionHistory_Account]
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory]  WITH CHECK ADD  CONSTRAINT [FK_AccountBillingTransactionHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory] CHECK CONSTRAINT [FK_AccountBillingTransactionHistory_Creator]
GO
ALTER TABLE [dbo].[AccountCustomICCProfile]  WITH CHECK ADD  CONSTRAINT [FK_AccountCustomICCProfile_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountCustomICCProfile] CHECK CONSTRAINT [FK_AccountCustomICCProfile_Account]
GO
ALTER TABLE [dbo].[AccountCustomICCProfile]  WITH CHECK ADD  CONSTRAINT [FK_AccountCustomICCProfile_PrintSubstrate] FOREIGN KEY([PrintSubstrate])
REFERENCES [dbo].[MediaCategory] ([ID])
GO
ALTER TABLE [dbo].[AccountCustomICCProfile] CHECK CONSTRAINT [FK_AccountCustomICCProfile_PrintSubstrate]
GO
ALTER TABLE [dbo].[AccountCustomICCProfile]  WITH CHECK ADD  CONSTRAINT [FK_AccountCustomICCProfile_User] FOREIGN KEY([Uploader])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountCustomICCProfile] CHECK CONSTRAINT [FK_AccountCustomICCProfile_User]
GO
ALTER TABLE [dbo].[AccountHelpCentre]  WITH CHECK ADD  CONSTRAINT [FK_AccountHelpCentre_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountHelpCentre] CHECK CONSTRAINT [FK_AccountHelpCentre_Account]
GO
ALTER TABLE [dbo].[AccountNotificationPreset]  WITH CHECK ADD  CONSTRAINT [FK_AccountNotificationPreset_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountNotificationPreset] CHECK CONSTRAINT [FK_AccountNotificationPreset_Account]
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_AccountNotificationPresetEventType_AccountNotificationPreset] FOREIGN KEY([NotificationPreset])
REFERENCES [dbo].[AccountNotificationPreset] ([ID])
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType] CHECK CONSTRAINT [FK_AccountNotificationPresetEventType_AccountNotificationPreset]
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_AccountNotificationPresetEventType_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[AccountNotificationPresetEventType] CHECK CONSTRAINT [FK_AccountNotificationPresetEventType_EventType]
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage]  WITH CHECK ADD  CONSTRAINT [FK_AccountPricePlanChangedMessage_ChangedBillingPlan] FOREIGN KEY([ChangedBillingPlan])
REFERENCES [dbo].[ChangedBillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage] CHECK CONSTRAINT [FK_AccountPricePlanChangedMessage_ChangedBillingPlan]
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage]  WITH CHECK ADD  CONSTRAINT [FK_AccountPricePlanChangedMessage_ChildAccount] FOREIGN KEY([ChildAccount])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage] CHECK CONSTRAINT [FK_AccountPricePlanChangedMessage_ChildAccount]
GO
ALTER TABLE [dbo].[AccountSetting]  WITH CHECK ADD  CONSTRAINT [TBL_AccountSetting_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountSetting] CHECK CONSTRAINT [TBL_AccountSetting_Account]
GO
ALTER TABLE [dbo].[AccountSetting]  WITH CHECK ADD  CONSTRAINT [TBL_AccountSetting_ValueDataType] FOREIGN KEY([ValueDataType])
REFERENCES [dbo].[ValueDataType] ([ID])
GO
ALTER TABLE [dbo].[AccountSetting] CHECK CONSTRAINT [TBL_AccountSetting_ValueDataType]
GO
ALTER TABLE [dbo].[AccountStorageUsage]  WITH CHECK ADD  CONSTRAINT [FK_AccountStorageUsage_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountStorageUsage] CHECK CONSTRAINT [FK_AccountStorageUsage_Account]
GO
ALTER TABLE [dbo].[AccountSubscriptionPlan]  WITH CHECK ADD  CONSTRAINT [FK_AccountSubscriptionPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountSubscriptionPlan] CHECK CONSTRAINT [FK_AccountSubscriptionPlan_Account]
GO
ALTER TABLE [dbo].[AccountSubscriptionPlan]  WITH CHECK ADD  CONSTRAINT [FK_AccountSubscriptionPlan_BillingPlan] FOREIGN KEY([SelectedBillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountSubscriptionPlan] CHECK CONSTRAINT [FK_AccountSubscriptionPlan_BillingPlan]
GO
ALTER TABLE [dbo].[AccountTheme]  WITH CHECK ADD  CONSTRAINT [FK_AccountTheme_ID] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountTheme] CHECK CONSTRAINT [FK_AccountTheme_ID]
GO
ALTER TABLE [dbo].[AccountTypeRole]  WITH CHECK ADD  CONSTRAINT [FK_AccountTypeRole_AccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[AccountTypeRole] CHECK CONSTRAINT [FK_AccountTypeRole_AccountType]
GO
ALTER TABLE [dbo].[AccountTypeRole]  WITH CHECK ADD  CONSTRAINT [FK_AccountTypeRole_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[AccountTypeRole] CHECK CONSTRAINT [FK_AccountTypeRole_Role]
GO
ALTER TABLE [dbo].[AdvancedSearch]  WITH CHECK ADD FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AdvancedSearchApprovalPhase]  WITH CHECK ADD FOREIGN KEY([AdvancedSearchId])
REFERENCES [dbo].[AdvancedSearch] ([AdvancedSearchId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdvancedSearchApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_AdvancedS_AppJobPhase] FOREIGN KEY([ApprovalPhaseId])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[AdvancedSearchApprovalPhase] CHECK CONSTRAINT [FK_AdvancedS_AppJobPhase]
GO
ALTER TABLE [dbo].[AdvancedSearchGroups]  WITH CHECK ADD FOREIGN KEY([AdvancedSearchId])
REFERENCES [dbo].[AdvancedSearch] ([AdvancedSearchId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdvancedSearchGroups]  WITH CHECK ADD FOREIGN KEY([GroupId])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[AdvancedSearchUser]  WITH CHECK ADD FOREIGN KEY([AdvancedSearchId])
REFERENCES [dbo].[AdvancedSearch] ([AdvancedSearchId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AdvancedSearchWorkflow]  WITH CHECK ADD FOREIGN KEY([AdvancedSearchId])
REFERENCES [dbo].[AdvancedSearch] ([AdvancedSearchId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_ApprovalType] FOREIGN KEY([Type])
REFERENCES [dbo].[ApprovalType] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_ApprovalType]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Creator]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_ExternalCollaborator] FOREIGN KEY([ExternalPrimaryDecisionMaker])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_ExternalCollaborator]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_FileType] FOREIGN KEY([FileType])
REFERENCES [dbo].[FileType] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_FileType]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Job]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_JobSource] FOREIGN KEY([JobSource])
REFERENCES [dbo].[JobSource] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_JobSource]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Modifier]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Owner]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_PrimaryDecisionMaker] FOREIGN KEY([PrimaryDecisionMaker])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_PrimaryDecisionMaker]
GO
ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Aproval_ApprovalJobPhase] FOREIGN KEY([CurrentPhase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Aproval_ApprovalJobPhase]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_ApprovalJobPhase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ApprovalJobPhase]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_ApprovalPage] FOREIGN KEY([Page])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ApprovalPage]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_CommentType] FOREIGN KEY([CommentType])
REFERENCES [dbo].[ApprovalCommentType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_CommentType]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Creator]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator_Creator] FOREIGN KEY([ExternalCreator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator_Creator]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator_Modifier] FOREIGN KEY([ExternalModifier])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator_Modifier]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_HighlightType] FOREIGN KEY([HighlightType])
REFERENCES [dbo].[HighlightType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_HighlightType]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Modifier]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Parent]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[ApprovalAnnotationStatus] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Status]
GO
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotation_ViewingCondition] FOREIGN KEY([ViewingCondition])
REFERENCES [dbo].[ViewingCondition] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ViewingCondition]
GO
ALTER TABLE [dbo].[ApprovalAnnotationAttachment]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationAttachment_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationAttachment] CHECK CONSTRAINT [FK_ApprovalAnnotationAttachment_ApprovalAnnotation]
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationMarkup_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup] CHECK CONSTRAINT [FK_ApprovalAnnotationMarkup_ApprovalAnnotation]
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationMarkup_Shape] FOREIGN KEY([Shape])
REFERENCES [dbo].[Shape] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup] CHECK CONSTRAINT [FK_ApprovalAnnotationMarkup_Shape]
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator] CHECK CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_ApprovalAnnotation]
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator] CHECK CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_Collaborator]
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_ApprovalAnnotation] FOREIGN KEY([Annotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog] CHECK CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_ApprovalAnnotation]
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_ApprovalAnnotationStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[ApprovalAnnotationStatus] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog] CHECK CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_ApprovalAnnotationStatus]
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_ExternalCollaborator] FOREIGN KEY([ExternalModifier])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog] CHECK CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_ExternalCollaborator]
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_User] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationStatusChangeLog] CHECK CONSTRAINT [FK_ApprovalAnnotationStatusChangeLog_User]
GO
ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaborator_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_Approval]
GO
ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaborator_ApprovalCollaboratorRole] FOREIGN KEY([ApprovalCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_ApprovalCollaboratorRole]
GO
ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaborator_ApprovalJobPhase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_ApprovalJobPhase]
GO
ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_Collaborator]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Approval]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_ApprovalJobPhase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_ApprovalJobPhase]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Collaborator]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Decision] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Decision]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_ExternalCollaborator]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorGroup_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_Approval]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorGroup_ApprovalJobPhase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_ApprovalJobPhase]
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_CollaboratorGroup]
GO
ALTER TABLE [dbo].[ApprovalCustomDecision]  WITH CHECK ADD FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCustomDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCustomDecision_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCustomDecision] CHECK CONSTRAINT [FK_ApprovalCustomDecision_Account]
GO
ALTER TABLE [dbo].[ApprovalCustomDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCustomDecision_Decision] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCustomDecision] CHECK CONSTRAINT [FK_ApprovalCustomDecision_Decision]
GO
ALTER TABLE [dbo].[ApprovalCustomICCProfile]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCustomICCProfile_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCustomICCProfile] CHECK CONSTRAINT [FK_ApprovalCustomICCProfile_Approval]
GO
ALTER TABLE [dbo].[ApprovalCustomICCProfile]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCustomICCProfile_CustomICCProfile] FOREIGN KEY([CustomICCProfile])
REFERENCES [dbo].[AccountCustomICCProfile] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCustomICCProfile] CHECK CONSTRAINT [FK_ApprovalCustomICCProfile_CustomICCProfile]
GO
ALTER TABLE [dbo].[ApprovalEmbeddedProfile]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalEmbeddedProfile_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalEmbeddedProfile] CHECK CONSTRAINT [FK_ApprovalEmbeddedProfile_Approval]
GO
ALTER TABLE [dbo].[ApprovalEmbeddedProfile]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalEmbeddedProfile_PrintSubstrate] FOREIGN KEY([PrintSubstrate])
REFERENCES [dbo].[MediaCategory] ([ID])
GO
ALTER TABLE [dbo].[ApprovalEmbeddedProfile] CHECK CONSTRAINT [FK_ApprovalEmbeddedProfile_PrintSubstrate]
GO
ALTER TABLE [dbo].[ApprovalJobDeleteHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobDeleteHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobDeleteHistory] CHECK CONSTRAINT [FK_ApprovalJobDeleteHistory_Account]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_ApprovalJobWorkflow] FOREIGN KEY([ApprovalJobWorkflow])
REFERENCES [dbo].[ApprovalJobWorkflow] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_ApprovalJobWorkflow]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_ApprovalTrigger] FOREIGN KEY([ApprovalTrigger])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_ApprovalTrigger]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_DecisionType] FOREIGN KEY([DecisionType])
REFERENCES [dbo].[DecisionType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_DecisionType]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_ExternalPrimaryDecisionMaker] FOREIGN KEY([ExternalPrimaryDecisionMaker])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_ExternalPrimaryDecisionMaker]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineTrigger] FOREIGN KEY([DeadlineTrigger])
REFERENCES [dbo].[PhaseDeadlineTrigger] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineTrigger]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineUnit] FOREIGN KEY([DeadlineUnit])
REFERENCES [dbo].[PhaseDeadlineUnit] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_PhaseDeadlineUnit]
GO
ALTER TABLE [dbo].[ApprovalJobPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhase_PrimaryDecisionMaker] FOREIGN KEY([PrimaryDecisionMaker])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhase] CHECK CONSTRAINT [FK_ApprovalJobPhase_PrimaryDecisionMaker]
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhaseApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval] CHECK CONSTRAINT [FK_ApprovalJobPhaseApproval_Approval]
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobPhaseApproval_Phase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobPhaseApproval] CHECK CONSTRAINT [FK_ApprovalJobPhaseApproval_Phase]
GO
ALTER TABLE [dbo].[ApprovalJobViewingCondition]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobViewingCondition_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobViewingCondition] CHECK CONSTRAINT [FK_ApprovalJobViewingCondition_Job]
GO
ALTER TABLE [dbo].[ApprovalJobViewingCondition]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobViewingCondition_ViewingCondition] FOREIGN KEY([ViewingCondition])
REFERENCES [dbo].[ViewingCondition] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobViewingCondition] CHECK CONSTRAINT [FK_ApprovalJobViewingCondition_ViewingCondition]
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobWorkflow_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow] CHECK CONSTRAINT [FK_ApprovalJobWorkflow_Account]
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalJobWorkflow_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[ApprovalJobWorkflow] CHECK CONSTRAINT [FK_ApprovalJobWorkflow_Job]
GO
ALTER TABLE [dbo].[ApprovalPage]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPage_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPage] CHECK CONSTRAINT [FK_ApprovalPage_Approval]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_ApprovalTrigger] FOREIGN KEY([ApprovalTrigger])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_ApprovalTrigger]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_ApprovalWorkflow] FOREIGN KEY([ApprovalWorkflow])
REFERENCES [dbo].[ApprovalWorkflow] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_ApprovalWorkflow]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_DecisionType] FOREIGN KEY([DecisionType])
REFERENCES [dbo].[DecisionType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_DecisionType]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_ExternalPrimaryDecisionMaker] FOREIGN KEY([ExternalPrimaryDecisionMaker])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_ExternalPrimaryDecisionMaker]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineTrigger] FOREIGN KEY([DeadlineTrigger])
REFERENCES [dbo].[PhaseDeadlineTrigger] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineTrigger]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineUnit] FOREIGN KEY([DeadlineUnit])
REFERENCES [dbo].[PhaseDeadlineUnit] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_PhaseDeadlineUnit]
GO
ALTER TABLE [dbo].[ApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhase_PrimaryDecisionMaker] FOREIGN KEY([PrimaryDecisionMaker])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhase] CHECK CONSTRAINT [FK_ApprovalPhase_PrimaryDecisionMaker]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhaseCollaborator_ApprovalPhaseCollaboratorRole] FOREIGN KEY([ApprovalPhaseCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator] CHECK CONSTRAINT [FK_ApprovalPhaseCollaborator_ApprovalPhaseCollaboratorRole]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhaseCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator] CHECK CONSTRAINT [FK_ApprovalPhaseCollaborator_Collaborator]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhaseCollaborator_Phase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaborator] CHECK CONSTRAINT [FK_ApprovalPhaseCollaborator_Phase]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhaseCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalPhaseCollaboratorGroup_CollaboratorGroup]
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalPhaseCollaboratorGroup_Phase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalPhase] ([ID])
GO
ALTER TABLE [dbo].[ApprovalPhaseCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalPhaseCollaboratorGroup_Phase]
GO
ALTER TABLE [dbo].[ApprovalSeparationPlate]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalSeparationPlate_Page] FOREIGN KEY([Page])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[ApprovalSeparationPlate] CHECK CONSTRAINT [FK_ApprovalSeparationPlate_Page]
GO
ALTER TABLE [dbo].[ApprovalSetting]  WITH CHECK ADD  CONSTRAINT [FK__ApprovalS__Appro__00FF1D08] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalSetting] CHECK CONSTRAINT [FK__ApprovalS__Appro__00FF1D08]
GO
ALTER TABLE [dbo].[ApprovalUserRecycleBinHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalDeleteHistory_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserRecycleBinHistory] CHECK CONSTRAINT [FK_ApprovalDeleteHistory_Approval]
GO
ALTER TABLE [dbo].[ApprovalUserRecycleBinHistory]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalDeleteHistory_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserRecycleBinHistory] CHECK CONSTRAINT [FK_ApprovalDeleteHistory_User]
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalUserViewInfo_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_Approval]
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalUserViewInfo_ExternalCollaborator] FOREIGN KEY([ExternalUser])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_ExternalCollaborator]
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalUserViewInfo_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_User]
GO
ALTER TABLE [dbo].[ApprovalWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalWorkflow_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ApprovalWorkflow] CHECK CONSTRAINT [FK_ApprovalWorkflow_Account]
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_AttachedAccountBillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] CHECK CONSTRAINT [FK_AttachedAccountBillingPlan_Account]
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_AttachedAccountBillingPlan_BillingPlan] FOREIGN KEY([BillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] CHECK CONSTRAINT [FK_AttachedAccountBillingPlan_BillingPlan]
GO
ALTER TABLE [dbo].[BillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_BillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[BillingPlan] CHECK CONSTRAINT [FK_BillingPlan_Account]
GO
ALTER TABLE [dbo].[BillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_BillingPlan_Type] FOREIGN KEY([Type])
REFERENCES [dbo].[BillingPlanType] ([ID])
GO
ALTER TABLE [dbo].[BillingPlan] CHECK CONSTRAINT [FK_BillingPlan_Type]
GO
ALTER TABLE [dbo].[BillingPlanType]  WITH CHECK ADD  CONSTRAINT [FK_BillingPlanType_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[BillingPlanType] CHECK CONSTRAINT [FK_BillingPlanType_Account]
GO
ALTER TABLE [dbo].[BillingPlanType]  WITH CHECK ADD  CONSTRAINT [FK_BillingPlanType_MediaService] FOREIGN KEY([MediaService])
REFERENCES [dbo].[MediaService] ([ID])
GO
ALTER TABLE [dbo].[BillingPlanType] CHECK CONSTRAINT [FK_BillingPlanType_MediaService]
GO
ALTER TABLE [dbo].[BillingPlanType]  WITH CHECK ADD  CONSTRAINT [TBL_BillingPlan_AppModule] FOREIGN KEY([AppModule])
REFERENCES [dbo].[AppModule] ([ID])
GO
ALTER TABLE [dbo].[BillingPlanType] CHECK CONSTRAINT [TBL_BillingPlan_AppModule]
GO
ALTER TABLE [dbo].[BrandingPreset]  WITH CHECK ADD  CONSTRAINT [FK_BrandingPreset_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[BrandingPreset] CHECK CONSTRAINT [FK_BrandingPreset_Account]
GO
ALTER TABLE [dbo].[BrandingPresetTheme]  WITH CHECK ADD  CONSTRAINT [FK_BrandingPreset_ID] FOREIGN KEY([BrandingPreset])
REFERENCES [dbo].[BrandingPreset] ([ID])
GO
ALTER TABLE [dbo].[BrandingPresetTheme] CHECK CONSTRAINT [FK_BrandingPreset_ID]
GO
ALTER TABLE [dbo].[BrandingPresetUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_BrandingPresetUserGroup_BrandingPreset] FOREIGN KEY([BrandingPreset])
REFERENCES [dbo].[BrandingPreset] ([ID])
GO
ALTER TABLE [dbo].[BrandingPresetUserGroup] CHECK CONSTRAINT [FK_BrandingPresetUserGroup_BrandingPreset]
GO
ALTER TABLE [dbo].[BrandingPresetUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_BrandingPresetUserGroup_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[BrandingPresetUserGroup] CHECK CONSTRAINT [FK_BrandingPresetUserGroup_UserGroup]
GO
ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_ChangedBillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_Account]
GO
ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_ChangedBillingPlan_AttachedAccountBillingPlan] FOREIGN KEY([AttachedAccountBillingPlan])
REFERENCES [dbo].[AttachedAccountBillingPlan] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_AttachedAccountBillingPlan]
GO
ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD  CONSTRAINT [FK_ChangedBillingPlan_BillingPlan] FOREIGN KEY([BillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_BillingPlan]
GO
ALTER TABLE [dbo].[CMSAnnouncement]  WITH CHECK ADD FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[CMSDownload]  WITH CHECK ADD FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[CMSManual]  WITH CHECK ADD FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[CMSVideo]  WITH CHECK ADD FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[CollaborateAPISession]  WITH CHECK ADD FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ColorProofContainer]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofContainer_ColorProofInstance] FOREIGN KEY([ColorProofInstance])
REFERENCES [dbo].[ColorProofInstance] ([ID])
GO
ALTER TABLE [dbo].[ColorProofContainer] CHECK CONSTRAINT [FK_ColorProofContainer_ColorProofInstance]
GO
ALTER TABLE [dbo].[ColorProofContainer]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofContainer_Container] FOREIGN KEY([Container])
REFERENCES [dbo].[Container] ([ID])
GO
ALTER TABLE [dbo].[ColorProofContainer] CHECK CONSTRAINT [FK_ColorProofContainer_Container]
GO
ALTER TABLE [dbo].[ColorProofContainer]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofContainer_ContainerSchedule] FOREIGN KEY([ContainerSchedule])
REFERENCES [dbo].[ContainerSchedule] ([Id])
GO
ALTER TABLE [dbo].[ColorProofContainer] CHECK CONSTRAINT [FK_ColorProofContainer_ContainerSchedule]
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofCoZoneWorkflow_ColorProofWorkflow] FOREIGN KEY([ColorProofWorkflow])
REFERENCES [dbo].[ColorProofWorkflow] ([ID])
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflow] CHECK CONSTRAINT [FK_ColorProofCoZoneWorkflow_ColorProofWorkflow]
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_ColorProofCoZoneWorkflow] FOREIGN KEY([ColorProofCoZoneWorkflow])
REFERENCES [dbo].[ColorProofCoZoneWorkflow] ([ID])
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup] CHECK CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_ColorProofCoZoneWorkflow]
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[ColorProofCoZoneWorkflowUserGroup] CHECK CONSTRAINT [FK_ColorProofCoZoneWorkflowUserGroup_UserGroup]
GO
ALTER TABLE [dbo].[ColorProofInstance]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofInstance_ColorProofInstanceState] FOREIGN KEY([State])
REFERENCES [dbo].[ColorProofInstanceState] ([ID])
GO
ALTER TABLE [dbo].[ColorProofInstance] CHECK CONSTRAINT [FK_ColorProofInstance_ColorProofInstanceState]
GO
ALTER TABLE [dbo].[ColorProofInstance]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofInstance_User] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ColorProofInstance] CHECK CONSTRAINT [FK_ColorProofInstance_User]
GO
ALTER TABLE [dbo].[ColorProofWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_ColorProofWorkflow_ColorProofInstance] FOREIGN KEY([ColorProofInstance])
REFERENCES [dbo].[ColorProofInstance] ([ID])
GO
ALTER TABLE [dbo].[ColorProofWorkflow] CHECK CONSTRAINT [FK_ColorProofWorkflow_ColorProofInstance]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Account]
GO
ALTER TABLE [dbo].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Country] FOREIGN KEY([Country])
REFERENCES [dbo].[Country] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Country]
GO
ALTER TABLE [dbo].[Container]  WITH CHECK ADD  CONSTRAINT [FK_Container_UserGroup] FOREIGN KEY([OwnerGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[Container] CHECK CONSTRAINT [FK_Container_UserGroup]
GO
ALTER TABLE [dbo].[ContainerAccount]  WITH CHECK ADD  CONSTRAINT [FK_ContainerAccount_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ContainerAccount] CHECK CONSTRAINT [FK_ContainerAccount_Account]
GO
ALTER TABLE [dbo].[ContainerAccount]  WITH CHECK ADD  CONSTRAINT [FK_ContainerAccount_Container] FOREIGN KEY([Container])
REFERENCES [dbo].[Container] ([ID])
GO
ALTER TABLE [dbo].[ContainerAccount] CHECK CONSTRAINT [FK_ContainerAccount_Container]
GO
ALTER TABLE [dbo].[ContainerClientStatus]  WITH CHECK ADD  CONSTRAINT [FK_ContainerClientStatus_ContainerClient] FOREIGN KEY([ContainerClient])
REFERENCES [dbo].[ColorProofInstance] ([ID])
GO
ALTER TABLE [dbo].[ContainerClientStatus] CHECK CONSTRAINT [FK_ContainerClientStatus_ContainerClient]
GO
ALTER TABLE [dbo].[ContainerClientStatus]  WITH CHECK ADD  CONSTRAINT [FK_ContainerClientStatus_ContainerVersion] FOREIGN KEY([ContainerVersion])
REFERENCES [dbo].[ContainerVersion] ([ID])
GO
ALTER TABLE [dbo].[ContainerClientStatus] CHECK CONSTRAINT [FK_ContainerClientStatus_ContainerVersion]
GO
ALTER TABLE [dbo].[ContainerDistributionGroup]  WITH CHECK ADD  CONSTRAINT [FK_ContainerDistributionGroup_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ContainerDistributionGroup] CHECK CONSTRAINT [FK_ContainerDistributionGroup_Account]
GO
ALTER TABLE [dbo].[ContainerDistributionGroupColorProofInstance]  WITH CHECK ADD  CONSTRAINT [FK_ContainerDistributionGroupColorProofInstance_ColorProofInstance] FOREIGN KEY([ColorProofInstance])
REFERENCES [dbo].[ColorProofInstance] ([ID])
GO
ALTER TABLE [dbo].[ContainerDistributionGroupColorProofInstance] CHECK CONSTRAINT [FK_ContainerDistributionGroupColorProofInstance_ColorProofInstance]
GO
ALTER TABLE [dbo].[ContainerDistributionGroupColorProofInstance]  WITH CHECK ADD  CONSTRAINT [FK_ContainerDistributionGroupColorProofInstance_ContainerDistributionGroup] FOREIGN KEY([ContainerDistributionGroup])
REFERENCES [dbo].[ContainerDistributionGroup] ([ID])
GO
ALTER TABLE [dbo].[ContainerDistributionGroupColorProofInstance] CHECK CONSTRAINT [FK_ContainerDistributionGroupColorProofInstance_ContainerDistributionGroup]
GO
ALTER TABLE [dbo].[ContainerFile]  WITH CHECK ADD  CONSTRAINT [FK_ContainerFile_Container] FOREIGN KEY([ContainerVersion])
REFERENCES [dbo].[ContainerVersion] ([ID])
GO
ALTER TABLE [dbo].[ContainerFile] CHECK CONSTRAINT [FK_ContainerFile_Container]
GO
ALTER TABLE [dbo].[ContainerGroup]  WITH CHECK ADD  CONSTRAINT [FK_ContainerGroup_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ContainerGroup] CHECK CONSTRAINT [FK_ContainerGroup_Account]
GO
ALTER TABLE [dbo].[ContainerGroupContainer]  WITH CHECK ADD  CONSTRAINT [FK_ContainerGroupContainer_Container] FOREIGN KEY([Container])
REFERENCES [dbo].[Container] ([ID])
GO
ALTER TABLE [dbo].[ContainerGroupContainer] CHECK CONSTRAINT [FK_ContainerGroupContainer_Container]
GO
ALTER TABLE [dbo].[ContainerGroupContainer]  WITH CHECK ADD  CONSTRAINT [FK_ContainerGroupContainer_ContainerGroup] FOREIGN KEY([ContainerGroup])
REFERENCES [dbo].[ContainerGroup] ([ID])
GO
ALTER TABLE [dbo].[ContainerGroupContainer] CHECK CONSTRAINT [FK_ContainerGroupContainer_ContainerGroup]
GO
ALTER TABLE [dbo].[ContainerSchedule]  WITH CHECK ADD  CONSTRAINT [FK_ContainerSchedule_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ContainerSchedule] CHECK CONSTRAINT [FK_ContainerSchedule_Account]
GO
ALTER TABLE [dbo].[ContainerVersion]  WITH CHECK ADD  CONSTRAINT [FK_ContainerVersion_Container] FOREIGN KEY([Container])
REFERENCES [dbo].[Container] ([ID])
GO
ALTER TABLE [dbo].[ContainerVersion] CHECK CONSTRAINT [FK_ContainerVersion_Container]
GO
ALTER TABLE [dbo].[ContainerVersionOrganization]  WITH CHECK ADD  CONSTRAINT [FK_ContainerOrganization_Container] FOREIGN KEY([Container])
REFERENCES [dbo].[Container] ([ID])
GO
ALTER TABLE [dbo].[ContainerVersionOrganization] CHECK CONSTRAINT [FK_ContainerOrganization_Container]
GO
ALTER TABLE [dbo].[ContainerVersionOrganization]  WITH CHECK ADD  CONSTRAINT [FK_ContainerOrganization_ContainerVersion] FOREIGN KEY([ContainerVersion])
REFERENCES [dbo].[ContainerVersion] ([ID])
GO
ALTER TABLE [dbo].[ContainerVersionOrganization] CHECK CONSTRAINT [FK_ContainerOrganization_ContainerVersion]
GO
ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD  CONSTRAINT [FK_CurrencyRate_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Creator]
GO
ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD  CONSTRAINT [FK_CurrencyRate_Currency] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Currency]
GO
ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD  CONSTRAINT [FK_CurrencyRate_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Modifier]
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK__DeliverEx__Local__7A1D154F] FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator] CHECK CONSTRAINT [FK__DeliverEx__Local__7A1D154F]
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_DeliverExternalCollaborator_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator] CHECK CONSTRAINT [FK_DeliverExternalCollaborator_Creator]
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_DeliverExternalCollaborator_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[DeliverExternalCollaborator] CHECK CONSTRAINT [FK_DeliverExternalCollaborator_UserAccount]
GO
ALTER TABLE [dbo].[DeliverJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJob_ColorProofJobStatus] FOREIGN KEY([StatusInColorProof])
REFERENCES [dbo].[ColorProofJobStatus] ([ID])
GO
ALTER TABLE [dbo].[DeliverJob] CHECK CONSTRAINT [FK_DeliverJob_ColorProofJobStatus]
GO
ALTER TABLE [dbo].[DeliverJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJob_ColorProofWorkflow] FOREIGN KEY([CPWorkflow])
REFERENCES [dbo].[ColorProofCoZoneWorkflow] ([ID])
GO
ALTER TABLE [dbo].[DeliverJob] CHECK CONSTRAINT [FK_DeliverJob_ColorProofWorkflow]
GO
ALTER TABLE [dbo].[DeliverJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJob_DeliverJobStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[DeliverJobStatus] ([ID])
GO
ALTER TABLE [dbo].[DeliverJob] CHECK CONSTRAINT [FK_DeliverJob_DeliverJobStatus]
GO
ALTER TABLE [dbo].[DeliverJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJob_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[DeliverJob] CHECK CONSTRAINT [FK_DeliverJob_Job]
GO
ALTER TABLE [dbo].[DeliverJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJob_JobSource] FOREIGN KEY([JobSource])
REFERENCES [dbo].[JobSource] ([ID])
GO
ALTER TABLE [dbo].[DeliverJob] CHECK CONSTRAINT [FK_DeliverJob_JobSource]
GO
ALTER TABLE [dbo].[DeliverJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJob_User] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[DeliverJob] CHECK CONSTRAINT [FK_DeliverJob_User]
GO
ALTER TABLE [dbo].[DeliverJobData]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobData_DeliverJob] FOREIGN KEY([DeliverJob])
REFERENCES [dbo].[DeliverJob] ([ID])
GO
ALTER TABLE [dbo].[DeliverJobData] CHECK CONSTRAINT [FK_DeliverJobData_DeliverJob]
GO
ALTER TABLE [dbo].[DeliverJobDeleteHistory]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobDeleteHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[DeliverJobDeleteHistory] CHECK CONSTRAINT [FK_DeliverJobDeleteHistory_Account]
GO
ALTER TABLE [dbo].[DeliverJobPage]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobPage_DDeliverJobPageStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[DeliverJobPageStatus] ([ID])
GO
ALTER TABLE [dbo].[DeliverJobPage] CHECK CONSTRAINT [FK_DeliverJobPage_DDeliverJobPageStatus]
GO
ALTER TABLE [dbo].[DeliverJobPage]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobPage_DeliverJob] FOREIGN KEY([DeliverJob])
REFERENCES [dbo].[DeliverJob] ([ID])
GO
ALTER TABLE [dbo].[DeliverJobPage] CHECK CONSTRAINT [FK_DeliverJobPage_DeliverJob]
GO
ALTER TABLE [dbo].[DeliverJobPageVerificationResult]  WITH CHECK ADD  CONSTRAINT [FK_DeliverJobPageVerificationResult_DeliverJobPage] FOREIGN KEY([DeliverJobPage])
REFERENCES [dbo].[DeliverJobPage] ([ID])
GO
ALTER TABLE [dbo].[DeliverJobPageVerificationResult] CHECK CONSTRAINT [FK_DeliverJobPageVerificationResult_DeliverJobPage]
GO
ALTER TABLE [dbo].[DeliverSharedJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverSharedJob_AccountNotificationPreset] FOREIGN KEY([NotificationPreset])
REFERENCES [dbo].[AccountNotificationPreset] ([ID])
GO
ALTER TABLE [dbo].[DeliverSharedJob] CHECK CONSTRAINT [FK_DeliverSharedJob_AccountNotificationPreset]
GO
ALTER TABLE [dbo].[DeliverSharedJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverSharedJob_DeliverExternalCollaborator] FOREIGN KEY([DeliverExternalCollaborator])
REFERENCES [dbo].[DeliverExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[DeliverSharedJob] CHECK CONSTRAINT [FK_DeliverSharedJob_DeliverExternalCollaborator]
GO
ALTER TABLE [dbo].[DeliverSharedJob]  WITH CHECK ADD  CONSTRAINT [FK_DeliverSharedJob_DeliverJob] FOREIGN KEY([DeliverJob])
REFERENCES [dbo].[DeliverJob] ([ID])
GO
ALTER TABLE [dbo].[DeliverSharedJob] CHECK CONSTRAINT [FK_DeliverSharedJob_DeliverJob]
GO
ALTER TABLE [dbo].[EventType]  WITH CHECK ADD  CONSTRAINT [FK_EventType_Group] FOREIGN KEY([EventGroup])
REFERENCES [dbo].[EventGroup] ([ID])
GO
ALTER TABLE [dbo].[EventType] CHECK CONSTRAINT [FK_EventType_Group]
GO
ALTER TABLE [dbo].[ExcludedAccountDefaultProfiles]  WITH CHECK ADD  CONSTRAINT [FK_ExcludedAccountDefaultProfiles_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ExcludedAccountDefaultProfiles] CHECK CONSTRAINT [FK_ExcludedAccountDefaultProfiles_Account]
GO
ALTER TABLE [dbo].[ExcludedAccountDefaultProfiles]  WITH CHECK ADD  CONSTRAINT [FK_ExcludedAccountDefaultProfiles_SimulationProfile] FOREIGN KEY([SimulationProfile])
REFERENCES [dbo].[SimulationProfile] ([ID])
GO
ALTER TABLE [dbo].[ExcludedAccountDefaultProfiles] CHECK CONSTRAINT [FK_ExcludedAccountDefaultProfiles_SimulationProfile]
GO
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK__ExternalC__Local__7928F116] FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK__ExternalC__Local__7928F116]
GO
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_BouncedEmail_ID] FOREIGN KEY([BouncedEmailID])
REFERENCES [dbo].[BouncedEmail] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_BouncedEmail_ID]
GO
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_Creator]
GO
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_Modifier]
GO
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_UserAccount]
GO
ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Account]
GO
ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Creator]
GO
ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Modifier]
GO
ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Parent]
GO
ALTER TABLE [dbo].[FolderApproval]  WITH CHECK ADD  CONSTRAINT [FK_FolderApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[FolderApproval] CHECK CONSTRAINT [FK_FolderApproval_Approval]
GO
ALTER TABLE [dbo].[FolderApproval]  WITH CHECK ADD  CONSTRAINT [FK_FolderApproval_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderApproval] CHECK CONSTRAINT [FK_FolderApproval_Folder]
GO
ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_Collaborator]
GO
ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_Folder]
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaboratorGroup_Approval] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup] CHECK CONSTRAINT [FK_FolderCollaboratorGroup_Approval]
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup] CHECK CONSTRAINT [FK_FolderCollaboratorGroup_CollaboratorGroup]
GO
ALTER TABLE [dbo].[FTPPresetJobDownload]  WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_Approval] FOREIGN KEY([ApprovalJob])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[FTPPresetJobDownload] CHECK CONSTRAINT [FK_FTPPresetJobDownload_Approval]
GO
ALTER TABLE [dbo].[FTPPresetJobDownload]  WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_DeliverJob] FOREIGN KEY([DeliverJob])
REFERENCES [dbo].[DeliverJob] ([ID])
GO
ALTER TABLE [dbo].[FTPPresetJobDownload] CHECK CONSTRAINT [FK_FTPPresetJobDownload_DeliverJob]
GO
ALTER TABLE [dbo].[FTPPresetJobDownload]  WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_ManageJob] FOREIGN KEY([ManageJob])
REFERENCES [dbo].[ManageJob] ([ID])
GO
ALTER TABLE [dbo].[FTPPresetJobDownload] CHECK CONSTRAINT [FK_FTPPresetJobDownload_ManageJob]
GO
ALTER TABLE [dbo].[FTPPresetJobDownload]  WITH CHECK ADD  CONSTRAINT [FK_FTPPresetJobDownload_UploadEnginePreset] FOREIGN KEY([Preset])
REFERENCES [dbo].[UploadEnginePreset] ([ID])
GO
ALTER TABLE [dbo].[FTPPresetJobDownload] CHECK CONSTRAINT [FK_FTPPresetJobDownload_UploadEnginePreset]
GO
ALTER TABLE [dbo].[GlobalSettings]  WITH CHECK ADD  CONSTRAINT [FK_GlobalSettings_NativeDataType] FOREIGN KEY([DataType])
REFERENCES [dbo].[NativeDataType] ([ID])
GO
ALTER TABLE [dbo].[GlobalSettings] CHECK CONSTRAINT [FK_GlobalSettings_NativeDataType]
GO
ALTER TABLE [dbo].[ImageFormat]  WITH CHECK ADD  CONSTRAINT [FK_ImageFormat_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ImageFormat] ([ID])
GO
ALTER TABLE [dbo].[ImageFormat] CHECK CONSTRAINT [FK_ImageFormat_Parent]
GO
ALTER TABLE [dbo].[InactiveApprovalStatuses]  WITH CHECK ADD FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[InactiveApprovalStatuses]  WITH CHECK ADD FOREIGN KEY([ApprovalDecision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Account]
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_JobStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[JobStatus] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_JobStatus]
GO
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_User] FOREIGN KEY([JobOwner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_User]
GO
ALTER TABLE [dbo].[JobStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK__JobStatus__Modif__75586032] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[JobStatusChangeLog] CHECK CONSTRAINT [FK__JobStatus__Modif__75586032]
GO
ALTER TABLE [dbo].[JobStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK__JobStatus__Statu__74643BF9] FOREIGN KEY([Status])
REFERENCES [dbo].[JobStatus] ([ID])
GO
ALTER TABLE [dbo].[JobStatusChangeLog] CHECK CONSTRAINT [FK__JobStatus__Statu__74643BF9]
GO
ALTER TABLE [dbo].[JobStatusChangeLog]  WITH CHECK ADD  CONSTRAINT [FK__JobStatusCh__Job__737017C0] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[JobStatusChangeLog] CHECK CONSTRAINT [FK__JobStatusCh__Job__737017C0]
GO
ALTER TABLE [dbo].[LabColorMeasurement]  WITH CHECK ADD  CONSTRAINT [FK_LabColorMeasurement_ApprovalSeparationPlate] FOREIGN KEY([ApprovalSeparationPlate])
REFERENCES [dbo].[ApprovalSeparationPlate] ([ID])
GO
ALTER TABLE [dbo].[LabColorMeasurement] CHECK CONSTRAINT [FK_LabColorMeasurement_ApprovalSeparationPlate]
GO
ALTER TABLE [dbo].[LogAction]  WITH CHECK ADD  CONSTRAINT [FK_LogAction_LogModule] FOREIGN KEY([LogModule])
REFERENCES [dbo].[LogModule] ([ID])
GO
ALTER TABLE [dbo].[LogAction] CHECK CONSTRAINT [FK_LogAction_LogModule]
GO
ALTER TABLE [dbo].[LogApproval]  WITH CHECK ADD  CONSTRAINT [FK_LogApproval_LogAction] FOREIGN KEY([LogAction])
REFERENCES [dbo].[LogAction] ([ID])
GO
ALTER TABLE [dbo].[LogApproval] CHECK CONSTRAINT [FK_LogApproval_LogAction]
GO
ALTER TABLE [dbo].[ManageAPISession]  WITH CHECK ADD  CONSTRAINT [FK_ManageAPISession_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ManageAPISession] CHECK CONSTRAINT [FK_ManageAPISession_User]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_ConversionStatus] FOREIGN KEY([ConversionStatus])
REFERENCES [dbo].[ConversionStatus] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_ConversionStatus]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_FileType] FOREIGN KEY([FileType])
REFERENCES [dbo].[FileType] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_FileType]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_Job]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_JobSource] FOREIGN KEY([JobSource])
REFERENCES [dbo].[JobSource] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_JobSource]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_ManageOutputColorSpace] FOREIGN KEY([OutputColorSpace])
REFERENCES [dbo].[ManageOutputColorSpace] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_ManageOutputColorSpace]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_Preflight] FOREIGN KEY([Preflight])
REFERENCES [dbo].[ManagePreflight] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_Preflight]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_PreflightStatus] FOREIGN KEY([PreflightStatus])
REFERENCES [dbo].[PreflightStatus] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_PreflightStatus]
GO
ALTER TABLE [dbo].[ManageJob]  WITH CHECK ADD  CONSTRAINT [FK_ManageJob_User] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ManageJob] CHECK CONSTRAINT [FK_ManageJob_User]
GO
ALTER TABLE [dbo].[ManageJobDeleteHistory]  WITH CHECK ADD  CONSTRAINT [FK_ManageJobDeleteHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ManageJobDeleteHistory] CHECK CONSTRAINT [FK_ManageJobDeleteHistory_Account]
GO
ALTER TABLE [dbo].[ManageOutputColorSpace]  WITH CHECK ADD  CONSTRAINT [FK_ManageOutputColorSpace_ManageConversion] FOREIGN KEY([Conversion])
REFERENCES [dbo].[ManageConversion] ([ID])
GO
ALTER TABLE [dbo].[ManageOutputColorSpace] CHECK CONSTRAINT [FK_ManageOutputColorSpace_ManageConversion]
GO
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD  CONSTRAINT [FK_MediaService_Colorspace] FOREIGN KEY([Colorspace])
REFERENCES [dbo].[Colorspace] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_Colorspace]
GO
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD  CONSTRAINT [FK_MediaService_ImageFormat] FOREIGN KEY([ImageFormat])
REFERENCES [dbo].[ImageFormat] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_ImageFormat]
GO
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD  CONSTRAINT [FK_MediaService_PageBox] FOREIGN KEY([PageBox])
REFERENCES [dbo].[PageBox] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_PageBox]
GO
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD  CONSTRAINT [FK_MediaService_Smoothing] FOREIGN KEY([Smoothing])
REFERENCES [dbo].[Smoothing] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_Smoothing]
GO
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD  CONSTRAINT [FK_MediaService_TileImageFormat] FOREIGN KEY([TileImageFormat])
REFERENCES [dbo].[TileImageFormat] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_TileImageFormat]
GO
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD  CONSTRAINT [FK_MenuItem_ControllerAction] FOREIGN KEY([ControllerAction])
REFERENCES [dbo].[ControllerAction] ([ID])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_ControllerAction]
GO
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD  CONSTRAINT [FK_MenuItem_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[MenuItem] ([ID])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_Parent]
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemAccountTypeRole_AccountTypeRole] FOREIGN KEY([AccountTypeRole])
REFERENCES [dbo].[AccountTypeRole] ([ID])
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_AccountTypeRole]
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD  CONSTRAINT [FK_MenuItemAccountTypeRole_MenuItem] FOREIGN KEY([MenuItem])
REFERENCES [dbo].[MenuItem] ([ID])
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_MenuItem]
GO
ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD FOREIGN KEY([PSSessionID])
REFERENCES [dbo].[ProofStudioSessions] ([ID])
GO
ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD  CONSTRAINT [FK_NotificationItem_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationItem] CHECK CONSTRAINT [FK_NotificationItem_Creator]
GO
ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD  CONSTRAINT [FK_NotificationItem_InternalRecipient] FOREIGN KEY([InternalRecipient])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationItem] CHECK CONSTRAINT [FK_NotificationItem_InternalRecipient]
GO
ALTER TABLE [dbo].[NotificationItem]  WITH CHECK ADD  CONSTRAINT [FK_NotificationItem_NotificationType] FOREIGN KEY([NotificationType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[NotificationItem] CHECK CONSTRAINT [FK_NotificationItem_NotificationType]
GO
ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_AccountNotificationPreset] FOREIGN KEY([NotificationPreset])
REFERENCES [dbo].[AccountNotificationPreset] ([ID])
GO
ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_AccountNotificationPreset]
GO
ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateTrigger] FOREIGN KEY([NotificationCollaborateTrigger])
REFERENCES [dbo].[NotificationPresetCollaborateTrigger] ([ID])
GO
ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateTrigger]
GO
ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateUnit] FOREIGN KEY([NotificationCollaborateUnit])
REFERENCES [dbo].[NotificationPresetCollaborateUnit] ([ID])
GO
ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_NotificationCollaborateUnit]
GO
ALTER TABLE [dbo].[NotificationSchedule]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSchedule_NotificationFrequency] FOREIGN KEY([NotificatonFrequency])
REFERENCES [dbo].[NotificationPresetFrequency] ([ID])
GO
ALTER TABLE [dbo].[NotificationSchedule] CHECK CONSTRAINT [FK_NotificationSchedule_NotificationFrequency]
GO
ALTER TABLE [dbo].[NotificationScheduleUser]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUser_NotificationSchedule] FOREIGN KEY([NotificationSchedule])
REFERENCES [dbo].[NotificationSchedule] ([ID])
GO
ALTER TABLE [dbo].[NotificationScheduleUser] CHECK CONSTRAINT [FK_NotificationScheduleUser_NotificationSchedule]
GO
ALTER TABLE [dbo].[NotificationScheduleUser]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUser_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationScheduleUser] CHECK CONSTRAINT [FK_NotificationScheduleUser_User]
GO
ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog] CHECK CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_EventType]
GO
ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationScheduleUserEventTypeEmailLog] CHECK CONSTRAINT [FK_NotificationScheduleUserEventTypeEmailLog_User]
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserGroup_NotificationSchedule] FOREIGN KEY([NotificationSchedule])
REFERENCES [dbo].[NotificationSchedule] ([ID])
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup] CHECK CONSTRAINT [FK_NotificationScheduleUserGroup_NotificationSchedule]
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_NotificationScheduleUserGroup_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[NotificationScheduleUserGroup] CHECK CONSTRAINT [FK_NotificationScheduleUserGroup_UserGroup]
GO
ALTER TABLE [dbo].[PaperTint]  WITH CHECK ADD  CONSTRAINT [FK_PaperTintAccount] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[PaperTint] CHECK CONSTRAINT [FK_PaperTintAccount]
GO
ALTER TABLE [dbo].[PaperTint]  WITH CHECK ADD  CONSTRAINT [FK_PaperTintMediaCategory] FOREIGN KEY([MediaCategory])
REFERENCES [dbo].[MediaCategory] ([ID])
GO
ALTER TABLE [dbo].[PaperTint] CHECK CONSTRAINT [FK_PaperTintMediaCategory]
GO
ALTER TABLE [dbo].[PaperTint]  WITH CHECK ADD  CONSTRAINT [FK_PaperTintUser] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[PaperTint] CHECK CONSTRAINT [FK_PaperTintUser]
GO
ALTER TABLE [dbo].[PaperTintMeasurement]  WITH CHECK ADD  CONSTRAINT [FK_PaperTintMeasurementCondition] FOREIGN KEY([MeasurementCondition])
REFERENCES [dbo].[MeasurementCondition] ([ID])
GO
ALTER TABLE [dbo].[PaperTintMeasurement] CHECK CONSTRAINT [FK_PaperTintMeasurementCondition]
GO
ALTER TABLE [dbo].[PaperTintMeasurement]  WITH CHECK ADD  CONSTRAINT [FK_PaperTintMeasurementId] FOREIGN KEY([PaperTint])
REFERENCES [dbo].[PaperTint] ([ID])
GO
ALTER TABLE [dbo].[PaperTintMeasurement] CHECK CONSTRAINT [FK_PaperTintMeasurementId]
GO
ALTER TABLE [dbo].[PrinterCompany]  WITH CHECK ADD  CONSTRAINT [FK_PrinterCompany_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[PrinterCompany] CHECK CONSTRAINT [FK_PrinterCompany_Account]
GO
ALTER TABLE [dbo].[PrinterCompany]  WITH CHECK ADD  CONSTRAINT [FK_PrinterCompany_PrinterCompanyType] FOREIGN KEY([Type])
REFERENCES [dbo].[PrinterCompanyType] ([ID])
GO
ALTER TABLE [dbo].[PrinterCompany] CHECK CONSTRAINT [FK_PrinterCompany_PrinterCompanyType]
GO
ALTER TABLE [dbo].[ProofStudioSessions]  WITH CHECK ADD FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureDetails_Paper] FOREIGN KEY([Paper])
REFERENCES [dbo].[QualityControlPaper] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] CHECK CONSTRAINT [FK_QualityControlDataCaptureDetails_Paper]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureDetails_PrinterCompany] FOREIGN KEY([PrinterCompany])
REFERENCES [dbo].[PrinterCompany] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] CHECK CONSTRAINT [FK_QualityControlDataCaptureDetails_PrinterCompany]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureDetails_PrintProcess] FOREIGN KEY([PrintProcess])
REFERENCES [dbo].[QualityControlPrintProcess] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] CHECK CONSTRAINT [FK_QualityControlDataCaptureDetails_PrintProcess]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureDetails_Product] FOREIGN KEY([Product])
REFERENCES [dbo].[QualityControlProduct] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] CHECK CONSTRAINT [FK_QualityControlDataCaptureDetails_Product]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureDetails_Section] FOREIGN KEY([Section])
REFERENCES [dbo].[QualityControlSection] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] CHECK CONSTRAINT [FK_QualityControlDataCaptureDetails_Section]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureDetails_Version] FOREIGN KEY([Version])
REFERENCES [dbo].[QualityControlVersion] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureDetails] CHECK CONSTRAINT [FK_QualityControlDataCaptureDetails_Version]
GO
ALTER TABLE [dbo].[QualityControlDataCaptureMeasurement]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlDataCaptureMeasurement_DataCaptureDetails] FOREIGN KEY([DataCaptureDetails])
REFERENCES [dbo].[QualityControlDataCaptureDetails] ([ID])
GO
ALTER TABLE [dbo].[QualityControlDataCaptureMeasurement] CHECK CONSTRAINT [FK_QualityControlDataCaptureMeasurement_DataCaptureDetails]
GO
ALTER TABLE [dbo].[QualityControlInkDensity]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlInkDensity_QualityControlSettings] FOREIGN KEY([QualityControlSettings])
REFERENCES [dbo].[QualityControlSettings] ([ID])
GO
ALTER TABLE [dbo].[QualityControlInkDensity] CHECK CONSTRAINT [FK_QualityControlInkDensity_QualityControlSettings]
GO
ALTER TABLE [dbo].[QualityControlInkDensityTolerance]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlInkDensityTolerance_QualityControlInkDensity] FOREIGN KEY([QualityControlInkDensity])
REFERENCES [dbo].[QualityControlInkDensity] ([ID])
GO
ALTER TABLE [dbo].[QualityControlInkDensityTolerance] CHECK CONSTRAINT [FK_QualityControlInkDensityTolerance_QualityControlInkDensity]
GO
ALTER TABLE [dbo].[QualityControlPaper]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlPaper_QualityControlSettings] FOREIGN KEY([QualityControlSettings])
REFERENCES [dbo].[QualityControlSettings] ([ID])
GO
ALTER TABLE [dbo].[QualityControlPaper] CHECK CONSTRAINT [FK_QualityControlPaper_QualityControlSettings]
GO
ALTER TABLE [dbo].[QualityControlPrintProcess]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlPrintProcess_QualityControlSettings] FOREIGN KEY([QualityControlSettings])
REFERENCES [dbo].[QualityControlSettings] ([ID])
GO
ALTER TABLE [dbo].[QualityControlPrintProcess] CHECK CONSTRAINT [FK_QualityControlPrintProcess_QualityControlSettings]
GO
ALTER TABLE [dbo].[QualityControlProduct]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlProduct_QualityControlSettings] FOREIGN KEY([QualityControlSettings])
REFERENCES [dbo].[QualityControlSettings] ([ID])
GO
ALTER TABLE [dbo].[QualityControlProduct] CHECK CONSTRAINT [FK_QualityControlProduct_QualityControlSettings]
GO
ALTER TABLE [dbo].[QualityControlSection]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlSection_QualityControlSettings] FOREIGN KEY([QualityControlSettings])
REFERENCES [dbo].[QualityControlSettings] ([ID])
GO
ALTER TABLE [dbo].[QualityControlSection] CHECK CONSTRAINT [FK_QualityControlSection_QualityControlSettings]
GO
ALTER TABLE [dbo].[QualityControlSettings]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlSettings_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[QualityControlSettings] CHECK CONSTRAINT [FK_QualityControlSettings_Account]
GO
ALTER TABLE [dbo].[QualityControlVersion]  WITH CHECK ADD  CONSTRAINT [FK_QualityControlVersion_QualityControlSettings] FOREIGN KEY([QualityControlSettings])
REFERENCES [dbo].[QualityControlSettings] ([ID])
GO
ALTER TABLE [dbo].[QualityControlVersion] CHECK CONSTRAINT [FK_QualityControlVersion_QualityControlSettings]
GO
ALTER TABLE [dbo].[Role]  WITH CHECK ADD  CONSTRAINT [FK_Role_AppModule] FOREIGN KEY([AppModule])
REFERENCES [dbo].[AppModule] ([ID])
GO
ALTER TABLE [dbo].[Role] CHECK CONSTRAINT [FK_Role_AppModule]
GO
ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_RolePresetEventType_Level] FOREIGN KEY([Preset])
REFERENCES [dbo].[Preset] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Level]
GO
ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_RolePresetEventType_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Role]
GO
ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD  CONSTRAINT [FK_RolePresetEventType_Type] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Type]
GO
ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_Approval]
GO
ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ApprovalCollaboratorRole] FOREIGN KEY([ApprovalCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ApprovalCollaboratorRole]
GO
ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ApprovalJobPhase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalJobPhase] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ApprovalJobPhase]
GO
ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ExternalCollaborator]
GO
ALTER TABLE [dbo].[SharedApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_SharedApprovalPhase_ApprovalPhaseCollaboratorRole] FOREIGN KEY([ApprovalPhaseCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[SharedApprovalPhase] CHECK CONSTRAINT [FK_SharedApprovalPhase_ApprovalPhaseCollaboratorRole]
GO
ALTER TABLE [dbo].[SharedApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_SharedApprovalPhase_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[SharedApprovalPhase] CHECK CONSTRAINT [FK_SharedApprovalPhase_ExternalCollaborator]
GO
ALTER TABLE [dbo].[SharedApprovalPhase]  WITH CHECK ADD  CONSTRAINT [FK_SharedApprovalPhase_Phase] FOREIGN KEY([Phase])
REFERENCES [dbo].[ApprovalPhase] ([ID])
GO
ALTER TABLE [dbo].[SharedApprovalPhase] CHECK CONSTRAINT [FK_SharedApprovalPhase_Phase]
GO
ALTER TABLE [dbo].[SharedColorProofWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_SharedColorProofWorkflow_ColorProofCoZoneWorkflow] FOREIGN KEY([ColorProofCoZoneWorkflow])
REFERENCES [dbo].[ColorProofCoZoneWorkflow] ([ID])
GO
ALTER TABLE [dbo].[SharedColorProofWorkflow] CHECK CONSTRAINT [FK_SharedColorProofWorkflow_ColorProofCoZoneWorkflow]
GO
ALTER TABLE [dbo].[SharedColorProofWorkflow]  WITH CHECK ADD  CONSTRAINT [FK_SharedColorProofWorkflow_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[SharedColorProofWorkflow] CHECK CONSTRAINT [FK_SharedColorProofWorkflow_User]
GO
ALTER TABLE [dbo].[SimulationProfile]  WITH CHECK ADD  CONSTRAINT [FK_SimulationProfileAccount] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[SimulationProfile] CHECK CONSTRAINT [FK_SimulationProfileAccount]
GO
ALTER TABLE [dbo].[SimulationProfile]  WITH CHECK ADD  CONSTRAINT [FK_SimulationProfileMeasurementCondition] FOREIGN KEY([MeasurementCondition])
REFERENCES [dbo].[MeasurementCondition] ([ID])
GO
ALTER TABLE [dbo].[SimulationProfile] CHECK CONSTRAINT [FK_SimulationProfileMeasurementCondition]
GO
ALTER TABLE [dbo].[SimulationProfile]  WITH CHECK ADD  CONSTRAINT [FK_SimulationProfileMediaCategory] FOREIGN KEY([MediaCategory])
REFERENCES [dbo].[MediaCategory] ([ID])
GO
ALTER TABLE [dbo].[SimulationProfile] CHECK CONSTRAINT [FK_SimulationProfileMediaCategory]
GO
ALTER TABLE [dbo].[SimulationProfile]  WITH CHECK ADD  CONSTRAINT [FK_SimulationProfileUsert] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[SimulationProfile] CHECK CONSTRAINT [FK_SimulationProfileUsert]
GO
ALTER TABLE [dbo].[SoftProofingSessionError]  WITH CHECK ADD  CONSTRAINT [FK_SoftProofingSessionError_ApprovalPage] FOREIGN KEY([ApprovalPage])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[SoftProofingSessionError] CHECK CONSTRAINT [FK_SoftProofingSessionError_ApprovalPage]
GO
ALTER TABLE [dbo].[SoftProofingSessionError]  WITH CHECK ADD  CONSTRAINT [FK_SoftProofingSessionError_SoftProofingSessionParams] FOREIGN KEY([SoftProofingSessionParams])
REFERENCES [dbo].[SoftProofingSessionParams] ([ID])
GO
ALTER TABLE [dbo].[SoftProofingSessionError] CHECK CONSTRAINT [FK_SoftProofingSessionError_SoftProofingSessionParams]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams]  WITH CHECK ADD  CONSTRAINT [FK__SoftProof__Appro__57C7FD4B] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] CHECK CONSTRAINT [FK__SoftProof__Appro__57C7FD4B]
GO
ALTER TABLE [dbo].[SoftProofingSessionParams]  WITH CHECK ADD  CONSTRAINT [FK_SoftProofingSessionParams_ViewingCondition] FOREIGN KEY([ViewingCondition])
REFERENCES [dbo].[ViewingCondition] ([ID])
GO
ALTER TABLE [dbo].[SoftProofingSessionParams] CHECK CONSTRAINT [FK_SoftProofingSessionParams_ViewingCondition]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_FinalDecisionMade] FOREIGN KEY([FinalDecisionMade])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_FinalDecisionMade]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_NewVesionAdded] FOREIGN KEY([NewVesionAdded])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_NewVesionAdded]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_NotificationFrequency] FOREIGN KEY([NotificationFrequency])
REFERENCES [dbo].[NotificationFrequency] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_NotificationFrequency]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_RepliesMyComment] FOREIGN KEY([RepliesMyComment])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_RepliesMyComment]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_StudioSettingType] FOREIGN KEY([Type])
REFERENCES [dbo].[StudioSettingType] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_StudioSettingType]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_User]
GO
ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD  CONSTRAINT [FK_StudioSetting_WhenCommentMade] FOREIGN KEY([WhenCommentMade])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_WhenCommentMade]
GO
ALTER TABLE [dbo].[SubscriberPlanInfo]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberPlanInfo_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[SubscriberPlanInfo] CHECK CONSTRAINT [FK_SubscriberPlanInfo_Account]
GO
ALTER TABLE [dbo].[SubscriberPlanInfo]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberPlanInfo_BillingPlan] FOREIGN KEY([SelectedBillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[SubscriberPlanInfo] CHECK CONSTRAINT [FK_SubscriberPlanInfo_BillingPlan]
GO
ALTER TABLE [dbo].[ThemeColorScheme]  WITH CHECK ADD  CONSTRAINT [FK_AccountTheme_Colors_ID] FOREIGN KEY([AccountTheme])
REFERENCES [dbo].[AccountTheme] ([ID])
GO
ALTER TABLE [dbo].[ThemeColorScheme] CHECK CONSTRAINT [FK_AccountTheme_Colors_ID]
GO
ALTER TABLE [dbo].[ThemeColorScheme]  WITH CHECK ADD  CONSTRAINT [FK_BrandingPresetTheme_Colors_ID] FOREIGN KEY([BrandingPresetTheme])
REFERENCES [dbo].[BrandingPresetTheme] ([ID])
GO
ALTER TABLE [dbo].[ThemeColorScheme] CHECK CONSTRAINT [FK_BrandingPresetTheme_Colors_ID]
GO
ALTER TABLE [dbo].[ThemeColorScheme]  WITH CHECK ADD  CONSTRAINT [FK_DefaultTheme_Colors_ID] FOREIGN KEY([DefaultTheme])
REFERENCES [dbo].[DefaultTheme] ([ID])
GO
ALTER TABLE [dbo].[ThemeColorScheme] CHECK CONSTRAINT [FK_DefaultTheme_Colors_ID]
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineDuplicateFileName] FOREIGN KEY([DuplicateFileName])
REFERENCES [dbo].[UploadEngineDuplicateFileName] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings] CHECK CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineDuplicateFileName]
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineVersionName] FOREIGN KEY([VersionName])
REFERENCES [dbo].[UploadEngineVersionName] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineAccountSettings] CHECK CONSTRAINT [FK_UploadEngineAccountSettings_UploadEngineVersionName]
GO
ALTER TABLE [dbo].[UploadEngineCustomXMLTemplate]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineCustomXMLTemplate_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineCustomXMLTemplate] CHECK CONSTRAINT [FK_UploadEngineCustomXMLTemplate_Account]
GO
ALTER TABLE [dbo].[UploadEngineCustomXMLTemplatesField]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineCustomXMLTemplatesField_UploadEngineCustomXMLTemplate] FOREIGN KEY([UploadEngineCustomXMLTemplate])
REFERENCES [dbo].[UploadEngineCustomXMLTemplate] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineCustomXMLTemplatesField] CHECK CONSTRAINT [FK_UploadEngineCustomXMLTemplatesField_UploadEngineCustomXMLTemplate]
GO
ALTER TABLE [dbo].[UploadEngineCustomXMLTemplatesField]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineCustomXMLTemplatesField_UploadEngineDefaultXMLTemplate] FOREIGN KEY([UploadEngineDefaultXMLTemplate])
REFERENCES [dbo].[UploadEngineDefaultXMLTemplate] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineCustomXMLTemplatesField] CHECK CONSTRAINT [FK_UploadEngineCustomXMLTemplatesField_UploadEngineDefaultXMLTemplate]
GO
ALTER TABLE [dbo].[UploadEnginePreset]  WITH CHECK ADD  CONSTRAINT [FK_UploadEnginePreset_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[UploadEnginePreset] CHECK CONSTRAINT [FK_UploadEnginePreset_Account]
GO
ALTER TABLE [dbo].[UploadEnginePreset]  WITH CHECK ADD  CONSTRAINT [FK_UploadEnginePreset_FTPProtocol] FOREIGN KEY([Protocol])
REFERENCES [dbo].[FTPProtocol] ([ID])
GO
ALTER TABLE [dbo].[UploadEnginePreset] CHECK CONSTRAINT [FK_UploadEnginePreset_FTPProtocol]
GO
ALTER TABLE [dbo].[UploadEngineProfile]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineProfile_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineProfile] CHECK CONSTRAINT [FK_UploadEngineProfile_Account]
GO
ALTER TABLE [dbo].[UploadEngineProfile]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineProfile_Preset] FOREIGN KEY([OutputPreset])
REFERENCES [dbo].[UploadEnginePreset] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineProfile] CHECK CONSTRAINT [FK_UploadEngineProfile_Preset]
GO
ALTER TABLE [dbo].[UploadEngineProfile]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineProfile_UploadEngineCustomXMLTemplate] FOREIGN KEY([XMLTemplate])
REFERENCES [dbo].[UploadEngineCustomXMLTemplate] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineProfile] CHECK CONSTRAINT [FK_UploadEngineProfile_UploadEngineCustomXMLTemplate]
GO
ALTER TABLE [dbo].[UploadEngineUserGroupPermissions]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineUserGroupPermissions_UploadEnginePermission] FOREIGN KEY([UploadEnginePermission])
REFERENCES [dbo].[UploadEnginePermission] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineUserGroupPermissions] CHECK CONSTRAINT [FK_UploadEngineUserGroupPermissions_UploadEnginePermission]
GO
ALTER TABLE [dbo].[UploadEngineUserGroupPermissions]  WITH CHECK ADD  CONSTRAINT [FK_UploadEngineUserGroupPermissions_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[UploadEngineUserGroupPermissions] CHECK CONSTRAINT [FK_UploadEngineUserGroupPermissions_UserGroup]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK__User__Locale__7834CCDD] FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK__User__Locale__7834CCDD]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_BouncedEmail_ID] FOREIGN KEY([BouncedEmailID])
REFERENCES [dbo].[BouncedEmail] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_BouncedEmail_ID]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Creator]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Modifier]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_NotificationFrequency] FOREIGN KEY([NotificationFrequency])
REFERENCES [dbo].[NotificationFrequency] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_NotificationFrequency]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Preset] FOREIGN KEY([Preset])
REFERENCES [dbo].[Preset] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Preset]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_PrinterCompany] FOREIGN KEY([PrinterCompany])
REFERENCES [dbo].[PrinterCompany] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_PrinterCompany]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_ProofStudioUserColor] FOREIGN KEY([ProofStudioColor])
REFERENCES [dbo].[ProofStudioUserColor] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_ProofStudioUserColor]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserAccount]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[UserStatus] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserStatus]
GO
ALTER TABLE [dbo].[UserApprovalReminderEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_UserApprovalReminderEmailLog_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[UserApprovalReminderEmailLog] CHECK CONSTRAINT [FK_UserApprovalReminderEmailLog_Approval]
GO
ALTER TABLE [dbo].[UserApprovalReminderEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_UserApprovalReminderEmailLog_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserApprovalReminderEmailLog] CHECK CONSTRAINT [FK_UserApprovalReminderEmailLog_User]
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue]  WITH CHECK ADD  CONSTRAINT [FK_UserCustomPresetEventTypeValue_RolePresetEventType] FOREIGN KEY([RolePresetEventType])
REFERENCES [dbo].[RolePresetEventType] ([ID])
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] CHECK CONSTRAINT [FK_UserCustomPresetEventTypeValue_RolePresetEventType]
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue]  WITH CHECK ADD  CONSTRAINT [FK_UserCustomPresetEventTypeValue_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] CHECK CONSTRAINT [FK_UserCustomPresetEventTypeValue_User]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Account]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Creator]
GO
ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Modifier]
GO
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupUser_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_User]
GO
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD  CONSTRAINT [FK_UserGroupUser_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_UserGroup]
GO
ALTER TABLE [dbo].[UserHistory]  WITH CHECK ADD  CONSTRAINT [FK_UserHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserHistory] CHECK CONSTRAINT [FK_UserHistory_Creator]
GO
ALTER TABLE [dbo].[UserHistory]  WITH CHECK ADD  CONSTRAINT [FK_UserHistory_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserHistory] CHECK CONSTRAINT [FK_UserHistory_User]
GO
ALTER TABLE [dbo].[UserLogin]  WITH CHECK ADD  CONSTRAINT [FK_UserLogin_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserLogin] CHECK CONSTRAINT [FK_UserLogin_User]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
ALTER TABLE [dbo].[UserSetting]  WITH CHECK ADD  CONSTRAINT [TBL_UserSetting_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserSetting] CHECK CONSTRAINT [TBL_UserSetting_User]
GO
ALTER TABLE [dbo].[UserSetting]  WITH CHECK ADD  CONSTRAINT [TBL_UserSetting_ValueDataType] FOREIGN KEY([ValueDataType])
REFERENCES [dbo].[ValueDataType] ([ID])
GO
ALTER TABLE [dbo].[UserSetting] CHECK CONSTRAINT [TBL_UserSetting_ValueDataType]
GO
ALTER TABLE [dbo].[ViewingCondition]  WITH CHECK ADD  CONSTRAINT [FK_PaperTint_ViewingCondition] FOREIGN KEY([PaperTint])
REFERENCES [dbo].[PaperTint] ([ID])
GO
ALTER TABLE [dbo].[ViewingCondition] CHECK CONSTRAINT [FK_PaperTint_ViewingCondition]
GO
ALTER TABLE [dbo].[ViewingCondition]  WITH CHECK ADD  CONSTRAINT [FK_SimulationProfile_ViewingCondition] FOREIGN KEY([SimulationProfile])
REFERENCES [dbo].[SimulationProfile] ([ID])
GO
ALTER TABLE [dbo].[ViewingCondition] CHECK CONSTRAINT [FK_SimulationProfile_ViewingCondition]
GO
ALTER TABLE [dbo].[ViewingCondition]  WITH CHECK ADD  CONSTRAINT [FK_ViewingCondition_CustomProfile] FOREIGN KEY([CustomProfile])
REFERENCES [dbo].[AccountCustomICCProfile] ([ID])
GO
ALTER TABLE [dbo].[ViewingCondition] CHECK CONSTRAINT [FK_ViewingCondition_CustomProfile]
GO
ALTER TABLE [dbo].[ViewingCondition]  WITH CHECK ADD  CONSTRAINT [FK_ViewingCondition_EmbeddedProfile] FOREIGN KEY([EmbeddedProfile])
REFERENCES [dbo].[ApprovalEmbeddedProfile] ([ID])
GO
ALTER TABLE [dbo].[ViewingCondition] CHECK CONSTRAINT [FK_ViewingCondition_EmbeddedProfile]
GO
ALTER TABLE [dbo].[Workstation]  WITH CHECK ADD  CONSTRAINT [FK_Workstation_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Workstation] CHECK CONSTRAINT [FK_Workstation_User]
GO
ALTER TABLE [dbo].[WorkstationCalibrationData]  WITH CHECK ADD  CONSTRAINT [FK_WorkstationCalibrationData_Workstation] FOREIGN KEY([Workstation])
REFERENCES [dbo].[Workstation] ([ID])
GO
ALTER TABLE [dbo].[WorkstationCalibrationData] CHECK CONSTRAINT [FK_WorkstationCalibrationData_Workstation]
GO
ALTER TABLE [dbo].[WorkstationValidationResult]  WITH CHECK ADD  CONSTRAINT [FK_WorkstationValidationResult_WorkstationCalibrationData] FOREIGN KEY([WorkstationCalibrationData])
REFERENCES [dbo].[WorkstationCalibrationData] ([ID])
GO
ALTER TABLE [dbo].[WorkstationValidationResult] CHECK CONSTRAINT [FK_WorkstationValidationResult_WorkstationCalibrationData]
GO
/****** Object:  StoredProcedure [dbo].[SPC_AccountsReport]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- CZ-2971 Extend Account Report
CREATE PROCEDURE [dbo].[SPC_AccountsReport] (
		
	@P_SelectedLocations nvarchar(MAX) = '',
    @P_SelectedAccountTypes nvarchar(MAX) = '',
	@P_SelectedAccountStatuses nvarchar(MAX) = '',
	@P_SelectedAccountSubsciptionPlans nvarchar(MAX) = '',		
	@P_LoggedAccount INT
	
)
AS
BEGIN	
	WITH AccountTree (AccountID, PathString )
	AS(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
	)	
	SELECT DISTINCT
		a.[ID], 
		a.Name,
		a.[Status] AS StatusId,
		s.[Name] AS StatusName,
		s.[Key],
		a.Domain,
		a.IsCustomDomainActive,
		a.CustomDomain,
		a.AccountType AS AccountTypeID,
		a.CreatedDate,
		a.IsTemporary,
		at.Name AS AccountTypeName,
		u.GivenName,
		u.FamilyName,
		co.ID AS Company,
		c.ID AS Country,
		c.ShortName AS CountryName,		
		(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts,
		(SELECT MAX(usr.DateLastLogin) FROM [dbo].[User] usr WHERE usr.Account = a.ID) AS LastActivity,
		(SELECT STUFF((SELECT ', ' + ac.Name FROM [dbo].Account ac WHERE ac.Parent = a.ID FOR XML PATH('')),1, 2, '')) AS ChildAccounts,
		(SELECT PathString) AS AccountPath,
	    (SELECT ISNULL(CASPBP.[SubscriptionPlanName] + '; ','') +
	            ISNULL(CSPIBP.[SubscriberPlanName] + '; ','')) AS Plans, 
	    (SELECT COUNT(APP.ID) FROM dbo.[Approval] AS APP INNER JOIN dbo.Job AS J ON APP.Job = J.ID WHERE J.Account = a.[ID]) AS ApprovalsCount
	FROM
		[dbo].[AccountType] at 	
		JOIN [dbo].[Account] a
			ON at.[ID] = a.AccountType
		JOIN [dbo].[AccountStatus] s
			ON s.[ID] = a.[Status]
		LEFT OUTER JOIN [dbo].[User] u
			ON u.ID= a.[Owner]
		JOIN [dbo].[Company] co
			ON a.[ID] = co.[Account]
		JOIN [dbo].[Country] c
			ON c.[ID] = co.[Country]
		LEFT OUTER JOIN dbo.AccountSubscriptionPlan asp
			ON a.ID = asp.Account
		LEFT OUTER JOIN dbo.SubscriberPlanInfo spi
			ON a.ID = spi.Account
		INNER JOIN AccountTree act 
			ON a.ID = act.AccountID
		OUTER APPLY ( SELECT STUFF(( SELECT '; ' + ASPBP.Name
					  FROM      dbo.AccountSubscriptionPlan ASP
								INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
					  WHERE     ASP.Account = A.ID FOR XML PATH('')),1,1,'') AS [SubscriptionPlanName]
					) CASPBP ( [SubscriptionPlanName])
		OUTER APPLY ( SELECT STUFF(( SELECT '; ' + SPIBP.Name
					  FROM      dbo.SubscriberPlanInfo SPI
								INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
					  WHERE     SPI.Account = A.ID FOR XML PATH('')),1,1,'') AS [SubscriberPlanName]
					) CSPIBP ( [SubscriberPlanName])
		
	WHERE	
		s.[Key]<>'D' AND
		a.ID <> @P_LoggedAccount AND
		a.IsTemporary = 0 AND
		(@P_SelectedLocations = '' OR co.Country IN (Select val FROM dbo.splitString(@P_SelectedLocations, ',')))
		AND (@P_SelectedAccountTypes = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_SelectedAccountTypes, ',')))
		AND (@P_SelectedAccountStatuses = '' OR a.Status IN (Select val FROM dbo.splitString(@P_SelectedAccountStatuses, ',')))	
		AND (@P_SelectedAccountSubsciptionPlans = '' OR asp.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ','))
													 OR spi.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_SelectedAccountSubsciptionPlans, ',')))				
			
END

GO
/****** Object:  StoredProcedure [dbo].[SPC_CalculateApprovalStatus]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------Lauched on Production

CREATE PROCEDURE [dbo].[SPC_CalculateApprovalStatus] 
	@P_ApprovalID int,
	@P_Account int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT RetVal =
					CASE
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
							THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
									THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = a.ID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = a.ID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE ''
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = a.ID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END
			FROM Approval a 
			where a.ID = @P_ApprovalID
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_DecryptColorProofInstancesPassword]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_DecryptColorProofInstancesPassword] (
	@P_Password varchar(max)
)
AS
BEGIN
-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
	SELECT  CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), @P_Password, 2 ))) as RetVal
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_DeleteAccount]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_DeleteAccount](
	@P_Id int
)
AS
	BEGIN
	SET XACT_ABORT ON;  
	BEGIN TRAN

    --DECLARE @P_Success nvarchar(512) = ''
    
	BEGIN TRY
		
	----- Get users ids of the deleted accounts -----
	DECLARE @DeletedUsers TABLE
	(	  
	   UserID INT
	)
	
	INSERT INTO @DeletedUsers (UserID)
	SELECT ID FROM [dbo].[User] 
	WHERE Account = @P_Id	
	--------------------------------------------------
	
	---- Delete records base on deleted accounts ids -------
	DELETE FROM [dbo].[AccountBillingPlanChangeLog]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountBillingTransactionHistory]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountHelpCentre]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[AccountSetting]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountSubscriptionPlan]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[Company]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[SubscriberPlanInfo]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[UploadEngineProfile]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[AccountPricePlanChangedMessage]
	WHERE ChildAccount = @P_Id
		
	DELETE FROM [dbo].[ContainerAccount]
	WHERE Account = @P_Id	
	
	DELETE [dbo].[ColorProofContainer]
	FROM [dbo].[ColorProofContainer] cpc
			JOIN [dbo].[ContainerSchedule] cs ON cpc.ContainerSchedule = cs.ID			
	WHERE cs.[Account] = @P_Id
	
	DELETE [dbo].[ThemeColorScheme]
	FROM [dbo].[ThemeColorScheme] tcs
		JOIN [dbo].[AccountTheme] act ON tcs.AccountTheme = act.ID
	WHERE act.Account = @P_Id
	
	DELETE [dbo].[AccountTheme]
	FROM [dbo].[AccountTheme] act
	WHERE act.Account = @P_Id
	
    DELETE [dbo].[ThemeColorScheme]
	FROM [dbo].[ThemeColorScheme] tcs
		JOIN [dbo].[BrandingPresetTheme] bpt ON tcs.BrandingPresetTheme = bpt.ID
		JOIN [dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	WHERE bp.Account = @P_Id
	
	DELETE [dbo].[BrandingPresetTheme]
	FROM [dbo].[BrandingPresetTheme] bpt
		JOIN [dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	where bp.Account = @P_Id
	
	DELETE [dbo].[BrandingPresetUserGroup]
	FROM [dbo].[BrandingPresetUserGroup] bpug
			JOIN [dbo].[BrandingPreset] bp ON bpug.BrandingPreset = bp.ID			
	WHERE bp.[Account] = @P_Id
	
	DELETE FROM [dbo].[ApprovalCustomDecision]
	WHERE Account = @P_Id
    
    DELETE FROM [GMGCoZone].[dbo].[InactiveApprovalStatuses]
	WHERE Account = @P_Id

	---------------------------------------------------------
	
	----- Delete record from CMS tables ---------	
	
	DELETE FROM [GMGCoZone].[dbo].[CMSAnnouncement]
	WHERE Account = @P_Id
	
	DELETE FROM [GMGCoZone].[dbo].[CMSDownload]
	WHERE Account = @P_Id
	
	DELETE FROM [GMGCoZone].[dbo].[CMSManual]
	WHERE Account = @P_Id
	
	DELETE FROM [GMGCoZone].[dbo].[CMSVideo]
	WHERE Account = @P_Id
	
	---------------------------------------------------------
	
	----- Delete record from tables related to user ---------	
	DELETE FROM [dbo].[UserSetting] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[UserRole] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE FROM [dbo].[UserLogin] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[UserHistory] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers) ---- OR [Creator]
	
	DELETE [dbo].[UserGroupUser] 
	FROM [dbo].[UserGroupUser] ugu
			JOIN [dbo].[UserGroup] ug ON ugu.UserGroup = ug.ID
	WHERE ug.Account = @P_Id

	DELETE FROM [dbo].[UserCustomPresetEventTypeValue] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[UserApprovalReminderEmailLog] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[StudioSetting] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[SharedColorProofWorkflow] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[SharedColorProofWorkflow] 
    FROM [dbo].[SharedColorProofWorkflow] scp	
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON scp.ColorProofCoZoneWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[NotificationScheduleUserEventTypeEmailLog] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE FROM [dbo].[NotificationItem] 
	WHERE [InternalRecipient] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[NotificationItem] 
	WHERE [Creator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[CurrencyRate] 
	WHERE [Creator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ManageAPISession] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalUserViewInfo] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalUserRecycleBinHistory] 
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalCollaborator] 
	WHERE [Collaborator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ApprovalCollaborator] 
	FROM [dbo].[ApprovalCollaborator] ac
			JOIN [dbo].[Approval] a ON ac.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE FROM [dbo].[ApprovalAnnotationStatusChangeLog] 
	FROM [dbo].[ApprovalAnnotationStatusChangeLog]  aasc
			JOIN [dbo].[ApprovalAnnotation] aa ON aasc.Annotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE FROM [dbo].[ApprovalAnnotationPrivateCollaborator] 
	WHERE [Collaborator] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ApprovalAnnotationPrivateCollaborator]
	FROM [dbo].[ApprovalAnnotationPrivateCollaborator] aapc
			JOIN [dbo].[ApprovalAnnotation] aa ON aapc.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id						
	---------------------------------------------------------
	
	
	-- Delete records from tables taht are related with Approval --------
	DELETE FROM [dbo].[ApprovalCollaboratorDecision] 	
	WHERE Collaborator IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE [dbo].[ApprovalJobPhaseApproval]
	FROM [dbo].[ApprovalJobPhaseApproval] ajpa
			JOIN [dbo].[Approval] a ON ajpa.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id 
	
	DELETE [dbo].[ApprovalCollaboratorDecision]
	FROM [dbo].[ApprovalCollaboratorDecision] acd
			JOIN [dbo].[Approval] a ON acd.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ApprovalJobViewingCondition]
	FROM [dbo].[ApprovalJobViewingCondition] ajc
			JOIN [dbo].[Job] j ON ajc.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[SharedApproval]
	FROM [dbo].[SharedApproval] sa
			JOIN [dbo].[Approval] a ON sa.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id 
	
	DELETE [dbo].[DeliverSharedJob]
	FROM [dbo].[DeliverSharedJob] dsj
			JOIN [dbo].[DeliverExternalCollaborator] dc ON dsj.DeliverExternalCollaborator = dc.ID			
	WHERE dc.Account = @P_Id
	
	DELETE [dbo].[DeliverSharedJob] 
	FROM [dbo].[DeliverSharedJob] dsj
			JOIN [dbo].[DeliverJob] dj ON dsj.[DeliverJob] =  dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ApprovalAnnotationAttachment]
	FROM [dbo].[ApprovalAnnotationAttachment] at
			JOIN [dbo].[ApprovalAnnotation] aa ON at.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalAnnotationMarkup]
	FROM [dbo].[ApprovalAnnotationMarkup] am
			JOIN [dbo].[ApprovalAnnotation] aa ON am.ApprovalAnnotation = aa.ID				
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[LabColorMeasurement] 
	FROM	[dbo].[LabColorMeasurement] lbm
			JOIN [dbo].[ApprovalSeparationPlate] asp ON lbm.ApprovalSeparationPlate = asp.ID
			JOIN [dbo].[ApprovalPage] ap ON asp.Page = ap.ID
			JOIN [dbo].[Approval] a ON a.ID = ap.Approval 
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalSeparationPlate]
	FROM [dbo].[ApprovalSeparationPlate] asp
			JOIN [dbo].[ApprovalPage] ap ON asp.Page = ap.ID				
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ApprovalSetting]
	FROM [dbo].[ApprovalSetting] ast				
			JOIN [dbo].[Approval] a ON ast.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalCustomICCProfile]
	FROM [dbo].[ApprovalCustomICCProfile] acp				
			JOIN [dbo].[Approval] a ON acp.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[SoftProofingSessionError]
	FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id	

	DELETE [dbo].[SoftProofingSessionParams]
	FROM [dbo].[SoftProofingSessionParams] sfse
			JOIN [dbo].[Approval] a ON sfse.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[ViewingCondition]
	FROM [dbo].ViewingCondition vwc
			JOIN [dbo].[ApprovalEmbeddedProfile] am	ON vwc.EmbeddedProfile = am.ID		
			JOIN [dbo].[Approval] a ON am.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
		
	DELETE [dbo].[ApprovalEmbeddedProfile]
	FROM [dbo].[ApprovalEmbeddedProfile] am			
			JOIN [dbo].[Approval] a ON am.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[DeliverJobPageVerificationResult]
	FROM [dbo].[DeliverJobPageVerificationResult] djpvr				
			JOIN [dbo].[DeliverJobPage] djp ON djpvr.DeliverJobPage = djp.ID
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[NotificationScheduleUserGroup]
	FROM [dbo].[NotificationScheduleUserGroup] nsug			
			JOIN [dbo].[UserGroup] ug ON nsug.UserGroup = ug.ID
	WHERE ug.Account = @P_Id
	
	DELETE [dbo].[NotificationScheduleUser]
	FROM [dbo].[NotificationScheduleUser] nsu 
			JOIN [dbo].[NotificationSchedule] ns ON nsu.NotificationSchedule = ns.ID
			JOIN [dbo].[AccountNotificationPreset] anp ON ns.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id			
	
	---------------------------------------------------------------------
	
	----- Delete records from tables that are related with QualityControl------------	
	
	DELETE [dbo].[QualityControlDataCaptureMeasurement]
	FROM [dbo].[QualityControlDataCaptureMeasurement] dcm
				JOIN [dbo].[QualityControlDataCaptureDetails] dcd ON dcm.DataCaptureDetails = dcd.ID
				JOIN [dbo].[QualityControlPaper] pap ON dcd.Paper = pap.ID
				JOIN [dbo].[QualityControlSettings] qcs ON pap.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlDataCaptureDetails]
	FROM [dbo].[QualityControlDataCaptureDetails] dcd
				JOIN [dbo].[QualityControlPaper] pap ON dcd.Paper = pap.ID
				JOIN [dbo].[QualityControlSettings] qcs ON pap.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlInkDensityTolerance]
	FROM [dbo].[QualityControlInkDensityTolerance] ict
				JOIN [dbo].[QualityControlInkDensity] ink ON ict.QualityControlInkDensity = ink.ID
				JOIN [dbo].[QualityControlSettings] qcs ON ink.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlPaper]
	FROM [dbo].[QualityControlPaper] pap
				JOIN [dbo].[QualityControlSettings] qcs ON pap.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlProduct]
	FROM [dbo].[QualityControlProduct] prd
				JOIN [dbo].[QualityControlSettings] qcs ON prd.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlVersion]
	FROM [dbo].[QualityControlVersion] vers
				JOIN [dbo].[QualityControlSettings] qcs ON vers.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlPrintProcess]
	FROM [dbo].[QualityControlPrintProcess] prp
				JOIN [dbo].[QualityControlSettings] qcs ON prp.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlSection]
	FROM [dbo].[QualityControlSection] sec
				JOIN [dbo].[QualityControlSettings] qcs ON sec.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
		
	DELETE [dbo].[QualityControlInkDensity]
	FROM [dbo].[QualityControlInkDensity] den
				JOIN [dbo].[QualityControlSettings] qcs ON den.QualityControlSettings = qcs.ID
	WHERE qcs.Account = @P_Id
	
	DELETE [dbo].[QualityControlSettings]
	FROM [dbo].[QualityControlSettings] qcs
	WHERE qcs.Account = @P_Id
	
	UPDATE u
	SET PrinterCompany = NULL
	FROM dbo.[User] u
	WHERE u.ID IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[PrinterCompany]
	FROM [dbo].[PrinterCompany] pcm
	WHERE pcm.Account = @P_Id
				
	---------------------------------------------------------------------
	
	---------------------------------------------------------------------
	--- Delete Administration - Approval Phasing related tables -----------
	---------------------------------------------------------------------
	DELETE [dbo].[ApprovalPhaseCollaborator]
	FROM [dbo].[ApprovalPhaseCollaborator] apc
				JOIN [dbo].[ApprovalPhase] ap ON apc.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id
	
	DELETE [dbo].[SharedApprovalPhase]
	FROM [dbo].[SharedApprovalPhase] sap
				JOIN [dbo].[ApprovalPhase] ap ON sap.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id
	
	DELETE [dbo].[ApprovalPhaseCollaboratorGroup]
	FROM [dbo].[ApprovalPhaseCollaboratorGroup] apcg
				JOIN [dbo].[ApprovalPhase] ap ON apcg.Phase = ap.ID
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id	
	
	DELETE [dbo].[ApprovalPhase]
	FROM [dbo].[ApprovalPhase] ap				
				JOIN [dbo].[ApprovalWorkflow] aw ON ap.[ApprovalWorkflow] = aw.ID
	WHERE aw.[Account] = @P_Id
	
	DELETE [dbo].[ApprovalWorkflow]
	WHERE [Account] = @P_Id	
	----------------------------------------------------------------------
	
	
	----- ---------------------------------------------------
	DELETE [dbo].[AccountNotificationPresetEventType]
	FROM [dbo].[AccountNotificationPresetEventType] anpet
				JOIN [dbo].[AccountNotificationPreset] anp ON anpet.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id
	
	DELETE [dbo].[UploadEngineCustomXMLTemplatesField] 
	FROM [dbo].[UploadEngineCustomXMLTemplatesField] uecxtf
				JOIN [dbo].[UploadEngineCustomXMLTemplate] uecxt ON uecxtf.UploadEngineCustomXMLTemplate = uecxt.ID
	WHERE uecxt.Account = @P_Id
	
	DELETE [dbo].[ContainerDistributionGroupColorProofInstance]
	FROM [dbo].[ContainerDistributionGroupColorProofInstance] cdgci
				JOIN [dbo].[ContainerDistributionGroup] cdg ON cdgci.ContainerDistributionGroup = cdg.ID
	WHERE cdg.Account = @P_Id  
	
	DELETE [dbo].[ContainerGroupContainer]
	FROM [dbo].[ContainerGroupContainer] cgc
				JOIN [dbo].[ContainerGroup] cg ON cgc.ContainerGroup = cg.ID
	WHERE cg.Account = @P_Id
	
	DELETE [dbo].[FTPPresetJobDownload]
	FROM [dbo].[FTPPresetJobDownload] fpjd
				JOIN [dbo].[UploadEnginePreset] uep ON fpjd.Preset = uep.ID
	WHERE uep.[Account] = @P_Id
	
	DELETE [dbo].[FTPPresetJobDownload]
	FROM [dbo].[FTPPresetJobDownload] fpjd			
			JOIN [dbo].[DeliverJob] dj ON fpjd.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE [dbo].[WorkstationValidationResult]
	FROM [dbo].[WorkstationValidationResult] wvr
				JOIN [dbo].[WorkstationCalibrationData] wcd ON wvr.WorkstationCalibrationData = wcd.ID
				JOIN [dbo].[Workstation] w ON wcd.Workstation = w.ID
	WHERE w.[User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ContainerClientStatus]
	FROM [dbo].[ContainerClientStatus] ccs
			JOIN [dbo].[ContainerVersion] cv ON ccs.ContainerVersion = cv.ID
			JOIN [dbo].[Container] c ON cv.Container = c.ID
			JOIN [dbo].[UserGroup] ug ON c.OwnerGroup = ug.ID
	WHERE ug.Account = @P_Id
	
	DELETE [dbo].[ContainerFile]
	FROM [dbo].[ContainerFile] cf
			JOIN [dbo].[ContainerVersion] cv ON cf.ContainerVersion = cv.ID
			JOIN [dbo].[Container] c ON cv.Container = c.ID
			JOIN [dbo].[UserGroup] ug ON c.OwnerGroup = ug.ID
	WHERE ug.Account = @P_Id 
	
	DELETE [dbo].[ContainerVersionOrganization]
	FROM [dbo].[ContainerVersionOrganization] cvo
			JOIN [dbo].[ContainerVersion] cv ON cvo.ContainerVersion = cv.ID
			JOIN [dbo].[Container] c ON cv.Container = c.ID
			JOIN [dbo].[UserGroup] ug ON c.OwnerGroup = ug.ID
	WHERE ug.Account = @P_Id 
	
	DELETE [dbo].[ApprovalCollaboratorGroup]
	FROM [dbo].[ApprovalCollaboratorGroup] acg			
			JOIN [dbo].[UserGroup] ug ON acg.CollaboratorGroup = ug.ID
	WHERE ug.Account = @P_Id 	
	
	DELETE [dbo].[ColorProofCoZoneWorkflowUserGroup]
	FROM [dbo].[ColorProofCoZoneWorkflowUserGroup] ccwug			
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON  ccwug.ColorProofCoZoneWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)	
	
	DELETE [dbo].[ColorProofCoZoneWorkflowUserGroup]
	FROM [dbo].[ColorProofCoZoneWorkflowUserGroup] ccwug			
			JOIN [dbo].[UserGroup] ug ON  ccwug.UserGroup = ug.ID			
	WHERE ug.Account = @P_Id	
	
	DELETE [dbo].[UploadEngineUserGroupPermissions]
	FROM [dbo].[UploadEngineUserGroupPermissions] ueugp			
			JOIN [dbo].[UserGroup] ug ON ueugp.UserGroup = ug.ID
	WHERE ug.Account = @P_Id
	----------------------------------------------------------------------
	
	
	-------- Delete related to folders based on deleted accounts ids -----
	DELETE [dbo].[FolderApproval]
	FROM [dbo].[FolderApproval] fa			
			JOIN [dbo].[Folder] f ON fa.Folder = f.ID
	WHERE f.Account = @P_Id 
	
	DELETE [dbo].[FolderCollaboratorGroup]
	FROM [dbo].[FolderCollaboratorGroup] fcg			
			JOIN [dbo].[Folder] f ON fcg.Folder = f.ID
	WHERE f.Account = @P_Id 
	
	DELETE [dbo].[FolderCollaborator]
	FROM [dbo].[FolderCollaborator] fc			
			JOIN [dbo].[Folder] f ON fc.Folder = f.ID
	WHERE f.Account = @P_Id
	---------------------------------------------------------------------- 

	--------- LEVEL 2 -------------
	----------------------------------------------------------------------	
	DELETE FROM [dbo].[ContainerSchedule] 	
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[DeliverExternalCollaborator]	
	WHERE Account = @P_Id
	
	DELETE [dbo].[ApprovalAnnotation]
	FROM [dbo].[ApprovalAnnotation] aa						
			JOIN [dbo].[ApprovalPage] ap ON aa.Page = ap.ID
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[DeliverJobPage]
	FROM  [dbo].[DeliverJobPage] djp 
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id

	DELETE [dbo].[DeliverJobData]
	FROM  [dbo].[DeliverJobData] djd
			JOIN [dbo].[DeliverJob] dj ON djd.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[DeliverJobData]
	FROM  [dbo].[DeliverJobData] djd
			JOIN [dbo].[DeliverJob] dj ON djd.DeliverJob = dj.ID
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account = @P_Id
	
	DELETE [dbo].[DeliverJobPage]
	FROM  [dbo].[DeliverJobPage] djp 
			JOIN [dbo].[DeliverJob] dj ON djp.DeliverJob = dj.ID
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[ContainerVersion]
	FROM [dbo].[ContainerVersion] cv 
			JOIN [dbo].[Container] c ON cv.Container = c.ID
			JOIN [dbo].[UserGroup] ug ON c.OwnerGroup = ug.ID
	WHERE ug.Account = @P_Id
	
	DELETE [dbo].[WorkstationCalibrationData]
	FROM [dbo].[WorkstationCalibrationData] wcd
			JOIN [dbo].[Workstation] w ON wcd.Workstation = w.ID
	WHERE w.[User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [ManageJob]
	FROM [ManageJob] mj 
			JOIN [dbo].[Job] j ON mj.Job = j.ID 
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ManageJobDeleteHistory]
	FROM [dbo].[ManageJobDeleteHistory] mjh	
	WHERE mjh.Account =@P_Id	
	
	DELETE FROM [dbo].[Folder]
	WHERE Account = @P_Id	
	
	DELETE [dbo].[ChangedBillingPlan] 
	FROM [dbo].[ChangedBillingPlan] cbp
			JOIN [dbo].[AttachedAccountBillingPlan] aabp ON cbp.AttachedAccountBillingPlan = aabp.ID
	WHERE aabp.Account = @P_Id
	
	DELETE FROM [dbo].[UploadEngineCustomXMLTemplate]
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[UploadEnginePreset]
	WHERE [Account] = @P_Id 
	
	DELETE FROM [dbo].[ContainerGroup]
	WHERE Account = @P_Id	
	
	DELETE FROM [dbo].[ContainerDistributionGroup]
	WHERE Account = @P_Id 
	
	DELETE [dbo].[NotificationSchedule]
	FROM [dbo].[NotificationSchedule] ns
			JOIN [dbo].[AccountNotificationPreset] anp ON ns.NotificationPreset = anp.ID
	WHERE anp.Account = @P_Id	
	
	DELETE FROM [dbo].[BrandingPreset]			
	WHERE [Account] = @P_Id
	
	DELETE FROM [dbo].[AccountStorageUsage]
	WHERE Account = @P_Id
	----------------------------------------------------------------------
	
	
	------ LEVEL 3 -------------------------------------------------------
	----------------------------------------------------------------------
	DELETE [dbo].[DeliverJob]
	FROM [dbo].[DeliverJob] dj 
			JOIN [dbo].[ColorProofCoZoneWorkflow] cczw ON dj.CPWorkflow = cczw.ID
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[DeliverJob]
	FROM [dbo].[DeliverJob] dj	
			JOIN [dbo].[Job] j ON dj.Job = j.ID 
	WHERE j.Account =@P_Id
	
	DELETE [dbo].[DeliverJobDeleteHistory]
	FROM [dbo].[DeliverJobDeleteHistory] djh	
	WHERE djh.Account =@P_Id	
	
	DELETE FROM [dbo].[Workstation] ---- need review
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE [dbo].[Container]
	FROM [dbo].[Container] c 
			JOIN [dbo].[UserGroup] ug ON c.OwnerGroup = ug.ID
	WHERE ug.Account = @P_Id
	
	DELETE [dbo].[ApprovalPage]
	FROM [dbo].[ApprovalPage] ap
			JOIN [dbo].[Approval] a ON ap.Approval = a.ID
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE FROM [dbo].[AttachedAccountBillingPlan] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[BillingPlan] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[BillingPlanType] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[AccountNotificationPreset] 
	WHERE Account = @P_Id
	
	DELETE FROM [dbo].[ExcludedAccountDefaultProfiles]
	WHERE Account = @P_Id
	
	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
	JOIN [dbo].[SimulationProfile] sim ON vwc.SimulationProfile = sim.ID
	WHERE sim.AccountId = @P_Id	
	
	DELETE FROM [dbo].[SimulationProfile]
	WHERE AccountId = @P_Id	
	
	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
	JOIN [dbo].[PaperTint] ppt ON vwc.PaperTint = ppt.ID
	WHERE ppt.AccountId = @P_Id	
	
	DELETE [dbo].[PaperTintMeasurement]
	FROM [dbo].[PaperTintMeasurement] pptms
	JOIN [dbo].[PaperTint] ppt ON pptms.PaperTint = ppt.ID
	WHERE ppt.AccountId = @P_Id	
	
	DELETE FROM [dbo].[PaperTint]
	WHERE AccountId = @P_Id	
	----------------------------------------------------------------------
	
	
	------ LEVEL 4 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE [dbo].[Approval]
	FROM [dbo].[Approval] a
			JOIN [dbo].[Job] j ON a.Job = j.ID
	WHERE j.Account = @P_Id

	DELETE [dbo].[ApprovalJobPhase] 
	FROM [dbo].[ApprovalJobPhase] ajp
		JOIN [dbo].[ApprovalJobWorkflow] ajw ON ajp.[ApprovalJobWorkflow] = ajw.ID
		JOIN [dbo].[Job] j ON ajw.Job = j.ID 
	WHERE j.Account = @P_Id		
	
	DELETE [dbo].[ApprovalJobWorkflow] 
	FROM [dbo].[ApprovalJobWorkflow] ajw
		JOIN [dbo].[Job] j ON ajw.Job = j.ID 
	WHERE j.Account = @P_Id	
	
	DELETE [dbo].[ApprovalJobDeleteHistory]
	FROM [dbo].[ApprovalJobDeleteHistory] ajh	
	WHERE ajh.Account =@P_Id	
	
	DELETE FROM [dbo].[UserGroup] 
	WHERE Account = @P_Id

	DELETE [dbo].[ViewingCondition]
	FROM [dbo].[ViewingCondition] vwc
			JOIN [dbo].[AccountCustomICCProfile] acp ON vwc.CustomProfile = acp.ID			
	WHERE acp.Account = @P_Id
	
	DELETE FROM [dbo].[AccountCustomICCProfile] 
	WHERE Account = @P_Id	

	DELETE [dbo].[ColorProofCoZoneWorkflow]
	FROM [dbo].[ColorProofCoZoneWorkflow] cczw
			JOIN [dbo].[ColorProofWorkflow] cpw ON cczw.ColorProofWorkflow = cpw.ID
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].ProofStudioSessions
	WHERE [User] IN (SELECT UserID FROM @DeletedUsers)		
	----------------------------------------------------------------------
	
	
	------ LEVEL 5 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE [dbo].[ColorProofWorkflow]
	FROM [dbo].[ColorProofWorkflow] cpw 
			JOIN [dbo].[ColorProofInstance] cpi ON cpw.ColorProofInstance = cpi.ID
	WHERE cpi.[Owner] IN (SELECT UserID FROM @DeletedUsers)
	
	DELETE FROM [dbo].[ExternalCollaborator]
	WHERE  Account = @P_Id
	
	DELETE [dbo].[JobStatusChangeLog]
	FROM [dbo].[JobStatusChangeLog] jcl
		JOIN dbo.[Job] j ON jcl.Job = j.ID
	WHERE j.Account = @P_Id
	
	DELETE FROM [dbo].[Job]
	WHERE Account = @P_Id	
	----------------------------------------------------------------------
	
	
	------ LEVEL 6 -------------------------------------------------------
	----------------------------------------------------------------------	
	DELETE FROM [dbo].[ColorProofInstance]
	WHERE [Owner] IN (SELECT UserID FROM @DeletedUsers)
	----------------------------------------------------------------------

	
	------ LEVEL 7-------------------------------------------------------
	--------------------------------------------------------------------
	--exec sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'
	
	-- Deletion of records from User and Account tables cannot be done because of circular references
	-- so, the two lines below are needed to disable the tables constraints and enable after delete operation is complete
	ALTER TABLE [dbo].[User] NOCHECK CONSTRAINT ALL
	ALTER TABLE [dbo].[Account] NOCHECK CONSTRAINT ALL
		
	DELETE [dbo].[User] FROM [dbo].[User] 
	WHERE ID IN (SELECT UserID FROM @DeletedUsers)		

	----------------------------------------------------------------------
	
	-------- LEVEL 8-------------------------------------------------------
	------------------------------------------------------------------------
	DELETE FROM [dbo].[Account] WHERE ID = @P_Id
			
	ALTER TABLE [dbo].[User] WITH CHECK CHECK CONSTRAINT ALL
	ALTER TABLE [dbo].[Account] WITH CHECK CHECK CONSTRAINT ALL	
  	--exec sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'	
	------------------------------------------------------------------------

	COMMIT TRAN

	END TRY
	BEGIN CATCH	
		DECLARE @error int, @message varchar(4000), @xstate int;
		SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
		
		IF (XACT_STATE()) = -1  
			BEGIN 		
				ROLLBACK TRANSACTION;  				
			END;  
		    
		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;  
	
       RAISERROR ('SPC_DeleteAccount: %d: %s', 16, 1, @error, @message) ;
	END CATCH
	END


GO
/****** Object:  StoredProcedure [dbo].[SPC_DeleteApproval]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	SET XACT_ABORT ON;  
	BEGIN TRANSACTION
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		 --Delete annotations from ApprovalAnnotationStatusChangeLog
		DELETE [dbo].[ApprovalAnnotationStatusChangeLog]		 
		FROM   [dbo].[ApprovalAnnotationStatusChangeLog] aascl
				JOIN [dbo].[ApprovalAnnotation] aa
					ON aascl.Annotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id	    
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval]
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalJobPhaseApproval
		DELETE FROM [dbo].[ApprovalJobPhaseApproval]	
				WHERE Approval = @P_Id	
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[ApprovalSetting] 
				WHERE Approval = @P_Id
				
		-- Delete from ApprovalCustomICC
		DELETE FROM [dbo].[ApprovalCustomICCProfile] 
				WHERE Approval = @P_Id
		
		-- Delete from LabColorMeasurements
		DELETE [dbo].[LabColorMeasurement] 
		FROM	[dbo].[LabColorMeasurement] lbm
				JOIN [dbo].[ApprovalSeparationPlate] asp
					ON lbm.ApprovalSeparationPlate = asp.ID
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		--Delete from SoftProofing Session Error
		DELETE [dbo].[SoftProofingSessionError]
		FROM [dbo].[SoftProofingSessionError] spse
			JOIN [dbo].[ApprovalPage] ap ON spse.ApprovalPage = ap.ID				
		WHERE ap.Approval = @P_Id
				
		--Delete from ApprovalEmbedded Profile foreign keys to approval
		DELETE [dbo].[ApprovalEmbeddedProfile]
		FROM [dbo].[ApprovalEmbeddedProfile] apep			
		WHERE apep.Approval = @P_Id			
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
				
		-- Delete from FTPJobPresetDownload
		DELETE FROM [dbo].[FTPPresetJobDownload]
		WHERE ApprovalJob = @P_Id
		
		-- Delete from ApprovalUserRecycleBinHistory
		DELETE FROM [dbo].[ApprovalUserRecycleBinHistory]
		WHERE Approval = @P_Id
		
		-- Delete from UserApprovalReminderEmailLog
		DELETE FROM [dbo].[UserApprovalReminderEmailLog]
		WHERE Approval = @P_Id
		
		-- Delete from SoftProofingSessionParams
		DELETE FROM [dbo].[SoftProofingSessionParams]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
			
				-- Delete all phases related with this job
				DELETE [dbo].[ApprovalJobPhase] 
				FROM [dbo].[ApprovalJobPhase] ajp
					JOIN [dbo].[ApprovalJobWorkflow] ajw ON ajp.[ApprovalJobWorkflow] = ajw.ID
				WHERE ajw.Job = @Job_ID
				
				-- Delete all approval workflows related with this job
				DELETE FROM [dbo].[ApprovalJobWorkflow]
				WHERE Job = @Job_ID
				
				-- Delete all job status change logs
				DELETE FROM [dbo].[JobStatusChangeLog]
				WHERE Job = @Job_ID
				
				--Delete job viewing condition
				DELETE FROM [dbo].[ApprovalJobViewingCondition]
				WHERE Job = @Job_ID
			
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
	    COMMIT TRANSACTION
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		IF (XACT_STATE()) = -1 
		BEGIN 
			SET @P_Success = ERROR_MESSAGE()
			ROLLBACK TRANSACTION;  
		END
		
		IF (XACT_STATE()) = 1  
		BEGIN
			COMMIT TRANSACTION;     
		END;
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_DeleteFolder]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_DeleteFolder] (
	@P_Id INT,
    @P_Modifier INT
 )
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY
	
	   DECLARE @FolderCreator INT;
	   DECLARE @ApprovalID INT, @ApprovalOwner INT;
	   
	   SET @FolderCreator = (SELECT Creator FROM Folder WHERE ID = @P_Id)
	   
		-- Delete from FolderCollaborator 
		DELETE FROM	[dbo].[FolderCollaborator]
				WHERE Folder = @P_Id
		
		-- Delete from FolderCollaboratorGroup 
		DELETE FROM	[dbo].[FolderCollaboratorGroup] 
				WHERE Folder = @P_Id
			
		DECLARE folder_cursor CURSOR
		FOR SELECT a.ID, a.[Owner] FROM [dbo].[Approval] a INNER JOIN [dbo].[FolderApproval] fa ON a.ID = fa.Approval WHERE fa.Folder = @P_Id
		
		OPEN folder_cursor
		
		FETCH NEXT
		FROM folder_cursor INTO @ApprovalID, @ApprovalOwner 
			WHILE @@FETCH_STATUS = 0
			BEGIN
						
			IF @ApprovalOwner = @P_Modifier -- mark approval to be deleted only if the user that made the action is approval owner
				BEGIN
					UPDATE [dbo].[Approval]
					SET [Version] = 0, [DeletePermanently] = 1
				    WHERE ID = @ApprovalID
				END
			
			IF @FolderCreator = @P_Modifier -- remove the relation with this folder for the approvals where the user that made the action is not the owner
				BEGIN
					EXECUTE [dbo].[SPC_MoveApproval] @ApprovalID, 0
				END		
			
			
			FETCH NEXT
			FROM folder_cursor INTO @ApprovalID, @ApprovalOwner
			END
		CLOSE folder_cursor
		DEALLOCATE folder_cursor
		
		--Mark parent as null for child folders
		UPDATE [dbo].[Folder]
		SET Parent = NULL
		where Parent = @P_Id	
 
	    -- Delete from Folder
		DELETE FROM [dbo].[Folder]
		WHERE ID = @P_Id	    
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END

GO
/****** Object:  StoredProcedure [dbo].[SPC_DeleteNotCompletedApprovals]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteNotCompletedApprovals
-- Description:  This SP deletes the Approvals from tne database but need to manually delete files from S3
-- Date Created: Tuesday, 09 Aprill 2013
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_DeleteNotCompletedApprovals]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY
	
	DECLARE @ApprovalID INT
	DECLARE @getApprovalID CURSOR
	SET @getApprovalID = CURSOR FOR 		
		SELECT	a.ID
		FROM [dbo].[Approval] a
		WHERE (a.CreatedDate < DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), -1))
				AND (SELECT MIN(Progress) FROM ApprovalPage ap WHERE ap.Approval = a.ID ) < 100 
	OPEN @getApprovalID
	
	FETCH NEXT
		FROM @getApprovalID INTO @ApprovalID
			WHILE @@FETCH_STATUS = 0
			BEGIN
				EXECUTE [dbo].[SPC_DeleteApproval] @ApprovalID
			FETCH NEXT
			FROM @getApprovalID INTO @ApprovalID
			END
	CLOSE @getApprovalID
	DEALLOCATE @getApprovalID
	
	SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END



GO
/****** Object:  StoredProcedure [dbo].[SPC_EncryptColorProofInstancesPassword]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 CREATE PROCEDURE [dbo].[SPC_EncryptColorProofInstancesPassword] (
	@P_Password varchar(128)
)
AS
BEGIN
	-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances 
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
	SELECT CONVERT(VARCHAR(max), EncryptByKey(Key_GUID('ColorProofInstances'), @P_Password), 2) AS RetVal
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_EncryptedPassword]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_EncryptedPassword
-- Description:  Select an encrypted password based on the supplied one
-- Date Created: Wednesday, 29 September 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE   PROCEDURE [dbo].[SPC_EncryptedPassword] (
	@P_Password varchar(64)
)
AS
	SELECT CONVERT(varchar(255),HashBytes('SHA1', @P_Password)) as RetVal

GO
/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---Update GetFolderTree Stored Procedure by adding a new ViewAll parameter
CREATE PROCEDURE [dbo].[SPC_GetFolderTree] (
	@P_User int,
	@P_ViewAll bit = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @LoggedAccountID INT;
	SET @LoggedAccountID = (SELECT ac.ID
							FROM [dbo].[User] us
							LEFT JOIN  [dbo].[Account] ac on us.[Account] = ac.ID
							WHERE us.ID = @P_User);

	WITH tableSet AS 
		(
			SELECT f.ID,
				   f.Parent,
				   f.Name,
				   f.Creator,
				   f.AllCollaboratorsDecisionRequired,
				   (0) AS Level,
				    (CASE WHEN (exists(	SELECT	fc.ID 
										FROM	[dbo].FolderCollaborator fc
										WHERE	fc.Folder = f.ID and fc.Collaborator = @P_User
						               )
						        )
						  THEN  '1'
						  ELSE '0'
						  end)  AS HasAccess,
				  (CASE WHEN (exists(	SELECT	fa.Folder 
								FROM	[dbo].FolderApproval fa
								WHERE	fa.Folder = f.ID
				               )
				        ) 
				  THEN  CONVERT(bit, 1)
				  ELSE CONVERT(bit, 0)
				  end)  AS HasApprovals  
			FROM	[dbo].[Folder] f
			WHERE f.IsDeleted = 0	AND f.Account = @LoggedAccountID
			
			UNION ALL
			
			SELECT sf.ID,
				   sf.Parent,
				   sf.Name,
				   sf.Creator,
				   sf.AllCollaboratorsDecisionRequired,
				   (CASE WHEN (EXISTS (SELECT fc.ID 
										FROM	[dbo].FolderCollaborator fc 
										WHERE	fc.Folder = sf.ID AND fc.Collaborator = @P_User)
								)
						 THEN  (h.[Level] + 1)
						 ELSE (0)
						 END
				   )  AS [Level],
				  (CASE WHEN (EXISTS (	SELECT fc.ID 
										FROM	[dbo].FolderCollaborator fc
										WHERE	fc.Folder = sf.ID AND fc.Collaborator = @P_User)
							  )
						THEN  '1'
						ELSE '0'
						END
				  )  AS HasAccess,
				  (CASE WHEN (exists(	SELECT	fa.Folder
								FROM	[dbo].FolderApproval fa
								WHERE	fa.Folder = sf.ID
				               )
				        ) 
						THEN  CONVERT(bit, 1)
						ELSE CONVERT(bit, 0)
						end
				  )  AS HasApprovals  
			FROM [dbo].[Folder] sf
				JOIN tableSet h
			ON h.ID	 = sf.Parent
			WHERE sf.IsDeleted = 0			 
		)	
	
	SELECT ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level], AllCollaboratorsDecisionRequired, (SELECT dbo.GetFolderCollaborators(ID)) AS Collaborators, HasApprovals
	FROM tableSet
	WHERE HasAccess = 1 OR @P_ViewAll = 1
	GROUP BY ID, Parent, Name, Creator, AllCollaboratorsDecisionRequired, HasApprovals
	
END


GO
/****** Object:  StoredProcedure [dbo].[SPC_GetScheduleGroups]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create store procedure for Schedule group data set
CREATE PROCEDURE [dbo].[SPC_GetScheduleGroups] (
	@P_Schedule INT	,
	@P_Account INT	
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @SelectedPoints INT
	DECLARE @TotalPoints INT
	
	DECLARE @Status INT
	DECLARE @ContainerGroup INT
	DECLARE @DistributionGroup INT	
	
	DECLARE @t TABLE
            (
              ContainerGroup INT ,
              DistributionGroup INT ,
              Status INT              
            )
	
	BEGIN TRY	
		DECLARE CDGCGCursor CURSOR LOCAL FOR 
		SELECT cg.ID, cdg.ID 
		FROM   dbo.ContainerGroup cg
				,dbo.ContainerDistributionGroup cdg
				INNER JOIN dbo.ContainerSchedule cs ON cdg.Account = cs.Account
				WHERE cg.Account= @P_Account  AND cdg.Account= @P_Account AND cs.Id= @P_Schedule AND cg.IsActive=1 AND cdg.IsActive=1		
		OPEN CDGCGCursor
		FETCH NEXT FROM CDGCGCursor INTO @ContainerGroup, @DistributionGroup
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			-- Get the selected number of points from a scheduleGroup
			SET @SelectedPoints = ( SELECT COUNT(dbo.ColorProofInstance.ID)									
									FROM dbo.ColorProofContainer 
									INNER JOIN dbo.Container ON dbo.ColorProofContainer.Container = dbo.Container.ID
									INNER JOIN dbo.ContainerGroupContainer ON dbo.Container.ID = dbo.ContainerGroupContainer.Container
									INNER JOIN dbo.ColorProofInstance ON dbo.ColorProofContainer.ColorProofInstance = dbo.ColorProofInstance.ID
									INNER JOIN dbo.ContainerDistributionGroupColorProofInstance ON dbo.ColorProofInstance.ID = dbo.ContainerDistributionGroupColorProofInstance.ColorProofInstance
									INNER JOIN dbo.ContainerSchedule ON dbo.ColorProofContainer.ContainerSchedule = dbo.ContainerSchedule.Id
									WHERE ContainerGroup = @ContainerGroup AND ContainerDistributionGroup = @DistributionGroup AND dbo.ColorProofContainer.ContainerSchedule = @P_Schedule)
			-- Get the total number of points from a scheduleGroup						
			SET @TotalPoints = ( SELECT COUNT(dbo.ColorProofInstance.ID)								
								FROM  dbo.Container 
								INNER JOIN dbo.ContainerGroupContainer ON dbo.Container.ID = dbo.ContainerGroupContainer.Container,
								  dbo.ColorProofInstance 
								INNER JOIN dbo.ContainerDistributionGroupColorProofInstance ON dbo.ColorProofInstance.ID = dbo.ContainerDistributionGroupColorProofInstance.ColorProofInstance
								WHERE ContainerGroup= @ContainerGroup AND ContainerDistributionGroup= @DistributionGroup)	
			IF(@SelectedPoints = 0)			
				SET @Status = 1
			ELSE IF(@SelectedPoints = @TotalPoints)
				SET @Status = 2
			ELSE
				SET @Status = 3
				
				INSERT INTO @t
				        ( ContainerGroup ,
				          DistributionGroup ,
				          Status
				        )
				VALUES  ( @ContainerGroup , -- ContainerGroup - int
				          @DistributionGroup , -- DistributionGroup - int
				          @Status  -- Status - int
				        )		
				

			FETCH NEXT FROM CDGCGCursor INTO @ContainerGroup, @DistributionGroup
		END
		CLOSE CDGCGCursor
		DEALLOCATE CDGCGCursor
	
		SET @P_Success = ''
	    
	END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT	TBL.ContainerGroup AS ContainerGroup
			,TBL.DistributionGroup AS DistributionGroup
			,TBL.Status AS Status	
			FROM @t	TBL
	  
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_GetSecondaryNavigationMenuItems]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


---- remove the logic related with EnableSoftProofingTools column (related to CZ-2073) ------
CREATE PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 
        
        INSERT  INTO @userRoles
                ( RoleId 
                
                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId
         
        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId
           
        DECLARE @Count INT;		
		
		WITH TempTable (ResultCount)
		AS
		( SELECT  count(1) as ResultCount
				  FROM      dbo.Account acc
							INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
							INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))				
				  UNION ALL
				  SELECT    count(1) as ResultCount
				  FROM      dbo.Account acc
							INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
							INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))				 
		)

		SELECT @Count = sum(ResultCount) FROM TempTable 
		
	    DECLARE @enableCollaborateSettings BIT = (SELECT CASE WHEN @Count > 0
														THEN CONVERT(BIT, 1)
														ELSE CONVERT(BIT, 0)
                                                    END
                                                  )

        --Remove Profile tab for users that don't have access to Admin Module        
        DECLARE @enableProfileMenu BIT = ( SELECT   CASE WHEN ( SELECT
                                                              COUNT(rl.ID)
                                                              FROM
                                                              dbo.UserRole ur
                                                              JOIN dbo.Role rl ON ur.Role = rl.ID
                                                              JOIN dbo.AppModule apm ON rl.AppModule = apm.ID
                                                              WHERE
                                                              ur.[User] = @P_userId
                                                              AND (apm.[Key] = 3 OR apm.[Key] = 4)
                                                              AND (RL.[Key] = 'ADM' OR rl.[Key] = 'HQM' OR rl.[Key] = 'HQA')
                                                              ) > 0
                                                         THEN CONVERT(BIT, 1)
                                                         ELSE CONVERT(BIT, 0)
                                                    END
                                         )
                                                  
        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
                                             
        DECLARE @enableQualityControlSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 6 -- qc
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 6 -- qc
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;
        
                  
        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )
                          
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'PSVI'  -- MenuKey - varchar(4)
                          )
            END
                       
        IF(@enableCollaborateSettings = 0)
			BEGIN
				 INSERT  INTO @ignoreMenuKeys
							( MenuKey )
				 VALUES  ( 'CLGS'  -- MenuKey - varchar(4)
							)
            END
             
        IF ( @enableProfileMenu = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEPF' --MenuKey - varchar(4)
                          )
            END   
            
        IF ( @enableQualityControlSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'QCAS' --MenuKey - varchar(4)
                          ),('SEPC')
            END 
        
        
        SET NOCOUNT OFF ;
        

        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;
  
    END

GO
/****** Object:  StoredProcedure [dbo].[SPC_MoveApproval]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- CZD-629 - stored procedure for move approval to another folder or delete link between approval and folder
CREATE PROCEDURE [dbo].[SPC_MoveApproval]
    (
      @P_ApprovalId INT ,
      @P_FolderId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @P_Success NVARCHAR(512) = ''

        BEGIN TRY
            
            DELETE  dbo.FolderApproval
            WHERE   Approval = @P_ApprovalId

            IF @P_FolderId > 0 
                BEGIN
                    INSERT  INTO dbo.FolderApproval
                            ( Folder, Approval )
                    VALUES  ( @P_FolderId, -- Folder - int
                              @P_ApprovalId  -- Approval - int
                              )
                END
    
            SET @P_Success = ''
    
        END TRY
        BEGIN CATCH
            SET @P_Success = ERROR_MESSAGE()
        END CATCH ;

        SELECT  @P_Success AS RetVal
  
    END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountParameters]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

--Return QualityControl Billing plan info
CREATE PROCEDURE [dbo].[SPC_ReturnAccountParameters] ( @P_LoggedAccount INT )
AS 
    BEGIN
        SET NOCOUNT ON
        SELECT  A.ID AS [AccountId] ,
                A.NAME AS [AccountName] ,
                AT.ID AS AccountTypeID ,
                AT.Name AS AccountTypeName ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.ID
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.ID
                         END, 0) AS [CollaborateBillingPlanId] ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.Name
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.NAme
                         END, '') AS [CollborateBillingPlanName] ,
                COALESCE(CASE WHEN MASP.ID IS NOT NULL THEN MASP.ID
                              WHEN MSPI.ID IS NOT NULL THEN MSPI.ID
                         END, 0) AS [ManageBillingPlanId] ,
                COALESCE(CASE WHEN MASP.ID IS NOT NULL THEN MASP.Name
                              WHEN MSPI.ID IS NOT NULL THEN MSPI.NAme
                         END, '') AS [ManageBillingPlanName] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.ID
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.ID
                         END, 0) AS [DeliverBillingPlanId] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.Name
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.NAme
                         END, '') AS [DeliverBillingPlanName] ,
                COALESCE(CASE WHEN QASP.ID IS NOT NULL THEN QASP.ID
							  WHEN QSPI.ID IS NOT NULL THEN QSPI.ID
                         END, '') AS [QualityControlBillingPlanId] ,         
				COALESCE(CASE WHEN QASP.ID IS NOT NULL THEN QASP.Name
							  WHEN QSPI.ID IS NOT NULL THEN QSPI.Name
                         END, '') AS [QualityControlBillingPlanName] ,
                AST.ID AS AccountStatusID ,
                ASt.Name AS AccountStatusName ,
                CTY.ID AS LocationID ,
                CTY.ShortName AS LocationName
        FROM    dbo.Account A
                INNER JOIN AccountType AT ON A.AccountType = AT.ID
                INNER JOIN AccountStatus AST ON A.[Status] = AST.ID
                INNER JOIN Company C ON C.Account = A.ID
                INNER JOIN Country CTY ON C.Country = CTY.ID
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     ASP.Account = A.ID
                            ) CASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 1 -- Manage
                              WHERE     ASP.Account = A.ID
                            ) MASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     ASP.Account = A.ID
                            ) DASP
				 OUTER APPLY ( SELECT    BP.ID ,
										 BP.Name
								         FROM      dbo.AccountSubscriptionPlan ASP
									     INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
										 INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
										 INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
				                         AND AM.[Key] = 6 -- QualityControl
							   WHERE     ASP.Account = A.ID
							  ) QASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     SPI.Account = A.ID
                            ) CSPI
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 1 -- Manage
                              WHERE     SPI.Account = A.ID
                            ) MSPI
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     SPI.Account = A.ID
                            ) DSPI
                OUTER APPLY ( SELECT    BP.ID ,
										BP.Name
							  FROM      dbo.SubscriberPlanInfo SPI
										INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
										INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
										INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
															  AND AM.[Key] = 6 -- QualityControl
							  WHERE     SPI.Account = A.ID
							) QSPI
             WHERE ast.[Key] != 'D' AND a.Parent = @P_LoggedAccount AND a.IsTemporary = 0
    END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 -- get all collaborators except those who have no access to Collaborate module
CREATE PROCEDURE [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]
	-- Add the parameters for the stored procedure here
	@P_AccountID INT,
	@P_LoggedUserID INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	DECLARE @ApproverAndReviewer INT
	DECLARE @Reviewer INT
	DECLARE @ReadOnlyr INT
	DECLARE @Retoucher INT
	
	SELECT @ApproverAndReviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'ANR';
	SELECT @Reviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RVW';
	SELECT @ReadOnlyr = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RDO';
	SELECT @Retoucher = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RET';
	
	SELECT 
		CONVERT (bit, 0) AS IsGroup,
		CONVERT (bit, 0) AS IsExternal,
		CONVERT (bit, 0) AS IsChecked,
		u.ID AS ID,
		0 AS [Count],
		
		-- IDs concatenated as string
		COALESCE(
			(SELECT DISTINCT ' cfg' + convert(varchar, ug.ID)
				FROM dbo.UserGroup ug
				INNER JOIN dbo.UserGroupUser ugu ON ugu.[User] = u.ID 
							AND ugu.UserGroup = ug.ID
				FOR XML PATH('') ),
			'')
			AS Members,
        
        -- name as given mane + family name
		u.GivenName + ' ' + u.FamilyName
			AS Name,
		
		-- get user role for colaborate module
		(SELECT acr.ID
			FROM dbo.ApprovalCollaboratorRole acr
			WHERE acr.ID =  CASE r.Name					
								WHEN 'Administrator' THEN @ApproverAndReviewer
								WHEN 'Manager' THEN @ApproverAndReviewer
								WHEN 'Moderator' THEN @ApproverAndReviewer
								WHEN 'Contributor' THEN @Reviewer
								WHEN 'Retoucher' THEN @Retoucher
								ELSE @ReadOnlyr
							END)						
			AS [Role]
	FROM dbo.[User] u	
	INNER JOIN dbo.[UserRole] ur ON ur.[User] = u.ID
	INNER JOIN dbo.[Role] r ON ur.Role = r.ID
	WHERE Account = @P_AccountID 
			AND r.[Key] <> 'NON'
			AND r.AppModule = (SELECT am.ID 
							   FROM dbo.AppModule am
							   WHERE am.[Key] = 0)			
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'I')
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'D')
								
			AND ( 	(SELECT rl.[Key] 	
					FROM dbo.[User] u
					JOIN dbo.[UserRole] ur ON u.ID = ur.[User]
					JOIN dbo.[Role] rl ON ur.Role = rl.ID
					JOIN dbo.[AppModule] apm ON rl.AppModule = apm.ID
					WHERE apm.[Key] = 0 and u.ID = @P_LoggedUserID) != 'MOD'
					OR
					u.ID IN 
					(SELECT DISTINCT u.ID FROM dbo.[User] u
				     JOIN dbo.UserGroupUser ugu ON u.ID = ugu.[User]
                     JOIN dbo.UserGroupUser ug ON ugu.UserGroup = ug.UserGroup
                     JOIN dbo.UserGroup grp ON ug.UserGroup = grp.ID
                     WHERE ug.[User] = @P_LoggedUserID AND grp.IsProfileServerOwnershipGroup = 0
                     UNION
                     SELECT @P_LoggedUserID)				
				)
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int,
	@P_ViewAll bit = 0,
	@P_ViewAllFromRecycle bit = 0,
	@P_UserRoleKey NVARCHAR(4) = '',
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
    SELECT
		( SELECT Count(approval.ID)
		  FROM (SELECT DISTINCT a.ID,
					   rank() over (partition by a.Job order by a.[Version] desc) rnk
				 FROM	
						Job j WITH (NOLOCK)
						INNER JOIN Approval a WITH (NOLOCK)
							ON a.Job = j.ID
						INNER JOIN JobStatus js WITH (NOLOCK)
							ON js.ID = j.[Status]
						JOIN ApprovalCollaborator ac WITH (NOLOCK) 
							ON ac.Approval = a.ID				
					WHERE	
						j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
						AND (
							   @P_ViewAll = 1 
								 OR
								 (
								   @P_ViewAll = 0 
								   AND 											   
									(@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0))	)
								  )
							 )
					 AND (
						@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)			   
						
				) approval
		 WHERE rnk = 1
		) AS AllCount, 
		( SELECT
			 COUNT(approval.ID)
		   FROM 
		   (SELECT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM	
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]							
			 WHERE j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					AND (ISNULL(j.JobOwner, 0) = @P_User OR a.[Owner] = @P_User)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)	
			) approval
		  where rnk = 1	
		) AS OwnedByMeCount,		
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM	
		            Job j WITH (NOLOCK)
					INNER JOIN Approval a  WITH (NOLOCK)
						ON a.Job = j.ID
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]									
				 WHERE j.Account = @P_Account			
						AND a.IsDeleted = 0
						AND a.DeletePermanently = 0
						AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
						AND ( ISNULL(j.JobOwner, 0) != @P_User AND a.[Owner] != @P_User)
						 AND EXISTS (
										SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac WITH (NOLOCK)
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
									)																				
										   					
						AND (
								@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
							)
			 ) approval
		  where rnk = 1		
		) AS SharedCount,
		(SELECT COUNT(DISTINCT j.ID)
			    FROM ApprovalUserViewInfo auvi WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
						ON a.ID = auvi.Approval 
				INNER JOIN Job j WITH (NOLOCK)
						ON j.ID = a.Job 
				INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]
				INNER JOIN ApprovalCollaborator ac WITH (NOLOCK)
						ON ac.Approval = a.ID				
			 WHERE	auvi.[User] = @P_User
				AND j.Account = @P_Account
				AND a.IsDeleted = 0
				AND a.DeletePermanently = 0
				AND js.[Key] != 'ARC'
				AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)			
				AND (
					 @P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
					)	
				AND ((@P_User = ac.Collaborator AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
						  )
						 OR ISNULL(j.JobOwner, 0) = @P_User)
		) AS RecentlyViewedCount,
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			 FROM
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]
				JOIN ApprovalCollaborator ac WITH (NOLOCK)
					ON ac.Approval = a.ID					
			 WHERE j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] = 'ARC'
					AND (
						   @P_ViewAll = 1 
							 OR
							 (
							   @P_ViewAll = 0 AND 											   
								(@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
																		  )
								)
							  )
						 )
			) approval
			where rnk = 1
		 ) AS ArchivedCount,
		(SELECT 
			    SUM(recycle.Approvals) AS RecycleCount
			FROM (
			          SELECT COUNT(DISTINCT a.ID) AS Approvals
						FROM	Job j WITH (NOLOCK)
								INNER JOIN Approval a WITH (NOLOCK)
									ON a.Job = j.ID
								INNER JOIN JobStatus js WITH (NOLOCK)
									ON js.ID = j.[Status]
								INNER JOIN ApprovalCollaborator ac WITH (NOLOCK)
									ON ac.Approval = a.ID		
						WHERE j.Account = @P_Account
							AND a.IsDeleted = 1
							AND a.DeletePermanently = 0
							AND ((SELECT COUNT(f.ID) FROM Folder f WITH (NOLOCK) INNER JOIN FolderApproval fa WITH (NOLOCK) ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
							AND (
							      ISNULL((SELECT TOP 1 ac.ID
										FROM ApprovalCollaborator ac WITH (NOLOCK)
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
										, 
										(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph WITH (NOLOCK)
										WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
									  ) IS NOT NULL				
								)
							AND (
								 @P_ViewAllFromRecycle = 1 
								 OR
								 ( 
									@P_ViewAllFromRecycle = 0 AND (@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)))
								 )	
							)	
					 
					UNION
						SELECT COUNT(f.ID) AS Approvals
						FROM	Folder f WITH (NOLOCK)
						WHERE	f.Account = @P_Account
								AND f.IsDeleted = 1
								AND ((SELECT COUNT(pf.ID) FROM Folder pf WITH (NOLOCK) WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)
								AND f.Creator = @P_User								
					
				)recycle
		) AS RecycleCount,	
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk			
			FROM Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]
				JOIN ApprovalCollaboratorDecision acd WITH (NOLOCK)
					ON acd.Approval = a.ID								
			WHERE	j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'COM'
					AND  acd.Collaborator = @P_User AND ISNULL(acd.Phase, 0) = ISNULL(a.CurrentPhase, 0) AND acd.Decision IS NULL
					AND (
							js.[Key] != 'COM'
						)
		     ) approval
			where rnk = 1
		) AS MyOpenApprovalsCount,
		(SELECT					
				COUNT(approval.ID)
		   FROM 
		   (SELECT DISTINCT a.ID,
				   rank() over (partition by a.Job order by a.[Version] desc) rnk
			FROM	
				Job j WITH (NOLOCK)
				INNER JOIN Approval a WITH (NOLOCK)
					ON a.Job = j.ID
				INNER JOIN JobStatus js WITH (NOLOCK)
					ON js.ID = j.[Status]	
				JOIN ApprovalCollaborator ac WITH (NOLOCK)
					ON ac.Approval = a.ID
			WHERE	
					j.Account = @P_Account			
					AND a.IsDeleted = 0
					AND a.DeletePermanently = 0
					AND js.[Key] != 'ARC' AND js.[Key] != (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END)
					AND  (@P_ViewAll = 1 
													 OR
													 (
													   @P_ViewAll = 0 AND 											   
														(@P_User = ISNULL(j.JobOwner, 0) OR (ac.Collaborator = @P_User AND ISNULL(ac.Phase, 0) = ISNULL(a.CurrentPhase, 0)
																								  )
														)
													  )
									)
					AND (
							@P_UserRoleKey != 'RET' OR (@P_UserRoleKey = 'RET' AND (js.[Key] = 'CRQ' OR js.[Key] = 'CCM'))
						)
					AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
					AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
					AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
					AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WITH (NOLOCK) WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
					AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WITH (NOLOCK) WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
						 OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WITH (NOLOCK) WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
					AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WITH (NOLOCK) WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw WITH (NOLOCK) where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
					AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (Select ajp.ID  FROM ApprovalJobPhase ajp WITH (NOLOCK) WHERE ajp.PhaseTemplateID in (Select val FROM dbo.splitString(@P_ByPhase, ',')))))							
					) approval
			where rnk = 1
			) AS AdvanceSearchCount
	--OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
END
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsInTree]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--------------------------------------------------------------------Lauched on TESTEU

CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsInTree]
	@P_UserKey NVARCHAR(36),
	@P_FolderID INT,
	@P_ApprovalType INT,
	@P_PageNumber INT,
	@P_MaxNumberOfRows INT = 50
AS
BEGIN
	SET ARITHABORT ON
	DECLARE @userKey NVARCHAR(36)
	DECLARE @folderID INT
	DECLARE @approvalType INT
	DECLARE @userID INT
	DECLARE @startOffset INT
	DECLARE @pageNr INT
	DECLARE @validApprovals TABLE(
		[ID] INT NOT NULL,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Version] INT,
		[Job] INT,
		[CreatedDate] DATETIME2(7),
		[OwnerID] INT
	);
	DECLARE @approvals TABLE(
		[ID] INT NOT NULL,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Version] INT,
		[Job] INT,
		[CreatedDate] DATETIME2(7),
		[OwnerID] INT
	);
	DECLARE @tempData TABLE(
	    ID int IDENTITY PRIMARY KEY,
		[ApprovalID] INT,
		[FileName] NVARCHAR(128),
		[Guid] NVARCHAR(36),
		[Region] NVARCHAR(128),
		[Domain] NVARCHAR(255),
		[ApprovalOwner] NVARCHAR(255),
		[FolderID] INT,
		[CreatedDate] DATETIME2(7)
	);
	
	SET @userKey = @P_UserKey;
	SET @folderID = @P_FolderID;
	SET @approvalType = @P_ApprovalType;
	SET @pageNr = @P_PageNumber;
	SET @startOffset = (@pageNr - 1) * @P_MaxNumberOfRows;
	
	SELECT @userID = u.ID FROM [User] as u WHERE u.Guid = @userKey;
	
	INSERT INTO @validApprovals
	SELECT 
		a.[ID],
		a.[FileName],
		a.[Guid],
		a.[Version],
		a.[Job],
		a.[CreatedDate],
		a.[Owner]
	FROM GMGCoZone.dbo.[Approval] AS a
	JOIN GMGCoZone.dbo.[ApprovalType] AS apt ON a.Type = apt.ID
	WHERE apt.[Key] != @approvalType AND a.DeletePermanently = 0 AND a.IsDeleted = 0;

	INSERT INTO @approvals
	SELECT 
		va.[ID],
		va.[FileName],
		va.[Guid],
		va.[Version],
		va.[Job],
		va.[CreatedDate],
		va.[OwnerID]
	FROM @validApprovals AS va 
	INNER JOIN 
	(SELECT a.Job, MAX(a.Version) as vr
		FROM @validApprovals as a
		GROUP By a.Job
	) maxVa ON va.Job = maxVa.Job AND va.Version = maxVa.vr;
	
	IF(@folderID = 0)
		BEGIN
			INSERT INTO @tempData
			SELECT 
				DISTINCT a.[ID] as ApprovalID,
				a.[FileName],
				a.[Guid],
				acc.[Region],
				acc.[Domain],
				u.[GivenName] + ' ' + u.[FamilyName] as ApprovalOwner,
				ISNULL(fa.[Folder], 0) as FolderID,
				a.[CreatedDate]
			FROM GMGCoZone.dbo.[ApprovalCollaborator] AS ac 
			INNER JOIN @approvals AS a ON ac.[Approval] = a.[ID]
			JOIN GMGCoZone.dbo.[User] AS u ON u.ID = a.OwnerID
			JOIN GMGCoZone.dbo.[Account] AS acc ON u.[Account] = acc.[ID]
			LEFT JOIN GMGCoZone.dbo.[FolderApproval] AS fa ON a.[ID] = fa.[Approval]	
			WHERE ac.[Collaborator] = @userID
			ORDER BY a.[CreatedDate] DESC

			IF(@P_PageNumber = 0)
				BEGIN
					SELECT 
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData as apr
				END
			ELSE
				BEGIN
					SET ROWCOUNT @P_MaxNumberOfRows
					SELECT 
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData as apr WHERE apr.ID > @startOffset
				END
		END
	ELSE
		BEGIN
			INSERT INTO @tempData
			SELECT 
				DISTINCT a.[ID] as ApprovalID,
				a.[FileName],
				a.[Guid],
				acc.[Region],
				acc.[Domain],
				u.[GivenName] + ' ' + u.[FamilyName] as ApprovalOwner,
				ISNULL(fa.[Folder], 0) as FolderID,
				a.[CreatedDate]
			FROM GMGCoZone.dbo.[ApprovalCollaborator] AS ac 
			INNER JOIN @approvals AS a ON ac.[Approval] = a.[ID]
			JOIN GMGCoZone.dbo.[User] AS u ON u.ID = a.OwnerID
			JOIN GMGCoZone.dbo.[Account] AS acc ON u.[Account] = acc.[ID]
			LEFT JOIN GMGCoZone.dbo.[FolderApproval] AS fa ON a.[ID] = fa.[Approval]	
			WHERE ac.[Collaborator] = @userID AND fa.Folder = @folderID
			ORDER BY a.[CreatedDate] DESC
			
			IF(@P_PageNumber = 0)
				BEGIN
					SELECT  
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData AS apr							
				END
			ELSE
				BEGIN
					SET ROWCOUNT @P_MaxNumberOfRows
					SELECT  
						apr.[ApprovalID],
						apr.[FileName],
						apr.[Guid],
						apr.[Region],
						apr.[Domain],
						apr.[ApprovalOwner],
						apr.[FolderID]
					FROM @tempData AS apr
					WHERE apr.ID > @startOffset
				END
		END
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* Update SPC_ReturnApprovalsPageInfo to fix CZ-3503  - Approval owner is present in Collaborate UI access window even tough is not in the collaborators list sent through API -- "Display approvals in the dashboard if the user is creator or collaborator" */	
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(150),
	   PrimaryDecisionMakerName nvarchar(150)
	)
		
	DECLARE @TempOnlyIDs TABLE
	(
	   ApprovalID int
	)

	IF @P_ViewAll = 1 
	BEGIN
		INSERT INTO @TempOnlyIDs(ApprovalID)
		select id FROM
		(
			select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
			FROM approval a
			INNER JOIN job j on j.id=a.job
			where @P_Account=j.account and a.IsDeleted=0 and a.DeletePermanently = 0
		) x 
		WHERE x.r = 1
	END
	ELSE
	BEGIN 
		-- Temporary select all the Approvals for the user, with current phase = NULL
		BEGIN
			;WITH AllVersions as 
			(
				select a.* from approval a 
				inner join Job j on j.id=a.job
				where j.account=@P_Account and a.IsDeleted=0 and a.DeletePermanently = 0 and CurrentPhase is null
			)

			INSERT INTO @TempOnlyIDs(ApprovalID)

			SELECT DISTINCT a.id
			FROM AllVersions a
			LEFT JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator
			WHERE @P_TopLinkId = 1 
				and (ac.ID is not null)

			UNION

			SELECT DISTINCT a.ID 
			FROM AllVersions a 
			WHERE @P_TopLinkId = 2 
				AND a.[Owner] = @P_User

			UNION
			SELECT DISTINCT a.id
			FROM AllVersions a 
			INNER JOIN ApprovalCollaborator ac on ac.Approval = a.ID AND @P_User = ac.Collaborator
			WHERE @P_TopLinkId = 3 
				AND a.[Owner] != @P_User

			UNION
			SELECT DISTINCT a.id
			FROM Job j
			INNER JOIN jobStatus js on js.ID=j.Status
			INNER JOIN AllVersions a on a.job=j.id
			INNER JOIN dbo.ApprovalCollaboratorDecision ac on ac.Approval = a.ID AND @P_User = ac.Collaborator AND ac.Decision IS NULL
			WHERE @P_TopLinkId = 7 
				AND js.[Key] != 'COM'
			OPTION(OPTIMIZE FOR (@P_TopLinkId UNKNOWN, @P_Account UNKNOWN))


			delete @TempOnlyIDs
			from @TempOnlyIDs t
			inner join
			(
				select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
				from @TempOnlyIDs t
				inner join approval a on a.id=t.ApprovalID
			) x on x.ID=t.ApprovalID and x.r > 1
		END

		-- Temporary select all the Approvals for the user, with current phase = NOT NULL
		BEGIN

			;WITH AllVersions AS 
			(
				SELECT j.JobOwner, a.*, ROW_NUMBER() OVER (PARTITION BY a.job ORDER BY Version DESC, a.ID DESC) RowNumber
				FROM Approval a
				inner join Job j on j.id=a.job
				WHERE j.account=@P_Account and a.IsDeleted=0 and a.DeletePermanently = 0 and CurrentPhase is not null
			)

			INSERT INTO @TempOnlyIDs(ApprovalID)

			SELECT DISTINCT a.id 
			FROM AllVersions a
			LEFT JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator and ac.phase=a.currentphase
			WHERE @P_TopLinkId = 1 and RowNumber=1
				and (a.[JobOwner] = @P_User or ac.ID is not null)

			UNION

			SELECT DISTINCT a.ID 
			FROM AllVersions a 
			WHERE @P_TopLinkId = 2 and RowNumber=1
				AND ISNULL(a.JobOwner,0) = @P_User

			UNION
			SELECT DISTINCT a.id
			FROM AllVersions a 
			INNER JOIN ApprovalCollaborator ac on ac.Approval = a.ID AND @P_User = ac.Collaborator  and ac.phase=a.currentphase
			WHERE @P_TopLinkId = 3 and RowNumber=1
				AND ISNULL(a.JobOwner,0) != @P_User

			UNION
			SELECT DISTINCT a.id
			FROM Job j
			INNER JOIN jobStatus js on js.ID=j.Status
			INNER JOIN AllVersions a on a.job=j.id
			INNER JOIN dbo.ApprovalCollaboratorDecision ac on ac.Approval = a.ID AND @P_User = ac.Collaborator  and ac.phase=a.currentphase AND ac.Decision IS NULL
			WHERE @P_TopLinkId = 7 and RowNumber=1
				AND js.[Key] != 'COM'
			OPTION(OPTIMIZE FOR (@P_TopLinkId UNKNOWN, @P_Account UNKNOWN))

			--delete duplicates
			delete @TempOnlyIDs
			from @TempOnlyIDs t
			inner join
			(
				select t.ApprovalID, ROW_NUMBER() OVER (PARTITION BY ApprovalID ORDER BY ApprovalID DESC) r
				from @TempOnlyIDs t
			) x on x.ApprovalID=t.ApprovalID and x.r > 1

		END


	END

	--------------------------------------------------------------
	--------------------------------------------------------------

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	
	select a.id,
			CP.PhaseName,
			PDMN.PrimaryDecisionMakerName
	from @TempOnlyIDs t 
	inner join approval a on a.id=t.ApprovalID
	inner join job j on j.id=a.job
	inner join jobstatus js on js.id=j.status
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)		
	where 
			js.[Key] != 'ARC'
			AND js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			
			AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
			AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
			AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
			AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
			AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
			     OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
			AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
			AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (SELECT ajp.ID
															 FROM ApprovalJobPhase as ajp
															 WHERE ajp.PhaseTemplateID in (SELECT ajp.PhaseTemplateID
															 FROM ApprovalJobPhase as ajp
															 where ajp.ID in (Select val FROM dbo.splitString(@P_ByPhase, ','))))))

	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PDMN.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PDMN.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN CP.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN CP.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		
	--select getdate()

	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,			
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
					WHEN ( EXISTS (SELECT ANNOTATION					
							FROM dbo.ApprovalPage ap							
							CROSS APPLY (SELECT TOP 1 aa.ID AS ANNOTATION FROM dbo.ApprovalAnnotation aa WHERE aa.Page = ap.ID 
							AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = a.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																													  WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase 
																													  )
																				)
													  )
																					   
								)	 
							) ANN (ANNOTATION)						
							WHERE ap.Approval = t.ApprovalID 
						 )
					) 
					THEN CONVERT(bit,1)
					ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,						
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
   
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo_revert]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo_revert] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
								-- 4 - Title
								-- 5 - Primary Decision Maker Name
								-- 6 - Phase Name
								-- 7 - Next Phase Name
								-- 9 - Phase Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit = 0,
	@P_HideCompletedApprovals bit,
	@P_IsOverdue bit = NULL,
	@P_DeadlineMin datetime2 = NULL,
	@P_DeadlineMax datetime2 = NULL,
	@P_SharedWithGroups nvarchar(100) = NULL,
	@P_SharedWithInternalUsers nvarchar(100) = NULL,
	@P_SharedWithExternalUsers nvarchar(100) = NULL,
	@P_ByWorkflow nvarchar(100) = NULL,
	@P_ByPhase nvarchar(100) = NULL,
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Avoid parameter sniffing
	
	/*Try to avoid parameter sniffing. The SP returns the same reuslts no matter what filter criteria (filter text and asceding descending) is set*/
	DECLARE @P_SearchField_Local INT;
	SET @P_SearchField_Local = @P_SearchField;	
	DECLARE @P_Order_Local BIT;
	SET @P_Order_Local = @P_Order;	
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName nvarchar(150),
	   PrimaryDecisionMakerName nvarchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID			
			JOIN JobStatus js
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)						
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL and
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0								
					AND (
							 @P_ViewAll = 1 
					     OR
					     ( 
					        @P_ViewAll = 0 AND 
					        (
								(@P_TopLinkId = 1 
										AND (
										av.Creator = @P_User
										OR EXISTS(
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												  )
										)
									)									
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
													)
									)
								OR 
									(@P_TopLinkId = 7 
										AND js.[Key] != 'COM' AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM dbo.ApprovalCollaboratorDecision ac 
														WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Decision IS NULL
													)
									)
							  )
							)
						)		
			) = a.[Version]	)
			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (
														 @P_ViewAll = 1 
													 OR
													 ( 
														@P_ViewAll = 0 AND 
														(
															(@P_TopLinkId = 1 AND (@P_User = j.JobOwner OR EXISTS(
																													SELECT TOP 1 ac.ID 
																													FROM ApprovalCollaborator ac 
																													WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																												  )
																					)
															 )									
															OR 
																(@P_TopLinkId = 2 
																	AND @P_User = ISNULL(j.JobOwner,0)
																)
															OR 
																(@P_TopLinkId = 3 
																	AND ISNULL(j.JobOwner,0) != @P_User
																	AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																				)
																)
																OR 
																(@P_TopLinkId = 7 
																	AND js.[Key] != 'COM' AND EXISTS (
																					SELECT TOP 1 ac.ID 
																					FROM dbo.ApprovalCollaboratorDecision ac 
																					WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase AND ac.Decision IS NULL
																				)
																)
														  )
														)
													)										 		
								      )
				)
			)
			AND ((@P_IsOverdue is NULL) OR (@P_IsOverdue = 1 and a.Deadline < GETDATE() and DATEDIFF(year,a.Deadline,GETDATE()) <= 100) OR (@P_IsOverdue = 0 and (a.Deadline >= GETDATE() or a.Deadline is NULL or DATEDIFF(year,a.Deadline,GETDATE()) > 100)))
			AND ((@P_DeadlineMin is NULL) OR (a.Deadline >= @P_DeadlineMin))
			AND ((@P_DeadlineMax is NULL) OR (a.Deadline <= @P_DeadlineMax))
			AND ((@P_SharedWithGroups is NULL) OR (a.ID in (SELECT ac.Approval FROM ApprovalCollaboratorGroup ac WHERE ac.CollaboratorGroup in (Select val FROM dbo.splitString(@P_SharedWithGroups, ',')))))
			AND (((@P_SharedWithInternalUsers is NULL AND @P_SharedWithExternalUsers is NULL) OR (a.ID in (SELECT iac.Approval FROM ApprovalCollaborator iac WHERE iac.Collaborator in (Select val FROM dbo.splitString(@P_SharedWithInternalUsers, ',')))))
			     OR (a.ID in (SELECT sa.Approval FROM SharedApproval sa WHERE sa.ExternalCollaborator in (Select val FROM dbo.splitString(@P_SharedWithExternalUsers, ',')))))
			AND ((@P_ByWorkflow is NULL) OR (a.CurrentPhase in (SELECT ajp.ID FROM ApprovalJobPhase ajp WHERE ajp.ApprovalJobWorkflow in (SELECT ajw.ID FROM dbo.ApprovalJobWorkflow ajw where ajw.Name in (SELECT val FROM dbo.splitString(@P_ByWorkflow, ','))))))
			AND ((@P_ByPhase is NULL) OR (a.CurrentPhase in (SELECT ajp.ID
															 FROM ApprovalJobPhase as ajp
															 WHERE ajp.PhaseTemplateID in (SELECT ajp.PhaseTemplateID
															 FROM ApprovalJobPhase as ajp
															 where ajp.ID in (Select val FROM dbo.splitString(@P_ByPhase, ','))))))

	ORDER BY 
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 0 AND @P_Order_Local = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 1 AND @P_Order_Local = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 2 AND @P_Order_Local = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 3 AND @P_Order_Local = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 4 AND @P_Order_Local = 1) THEN  j.Title
		END DESC,
			CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 5 AND @P_Order_Local = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 6 AND @P_Order_Local = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 7 AND @P_Order_Local = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField_Local = 8 AND @P_Order_Local = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT	t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
			END) AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,			
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			  (SELECT MAX([Version]) 
			 FROM Approval av WHERE av.Job = a.Job AND av.IsDeleted = 0 AND av.DeletePermanently = 0)AS MaxVersion,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND (@P_ViewAll = 1 OR @P_ViewAll = 0 AND  av.[Owner] = @P_User) AND av.IsDeleted = 0) AS ApprovalsCount,
			(SELECT CASE
					WHEN ( EXISTS (SELECT ANNOTATION					
							FROM dbo.ApprovalPage ap							
							CROSS APPLY (SELECT TOP 1 aa.ID AS ANNOTATION FROM dbo.ApprovalAnnotation aa WHERE aa.Page = ap.ID 
							AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = a.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																													  WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase 
																													  )
																				)
													  )
																					   
								)	 
							) ANN (ANNOTATION)						
							WHERE ap.Approval = t.ApprovalID 
						 )
					) 
					THEN CONVERT(bit,1)
					ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,						
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE
						WHEN (@P_ViewAll = 0 OR ( @P_ViewAll = 1 AND EXISTS(SELECT TOP 1 ac.ID 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))) ) THEN CONVERT(bit,1)
					    ELSE CONVERT(bit,0)
					END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t ON t.ApprovalID = a.ID
			JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			JOIN JobStatus js
				ON js.ID = j.[Status]					
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
   
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop

END
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnArchivedApprovalInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID			
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
	WHERE	j.Account = @P_Account
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] = 'ARC'
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND ((a.CurrentPhase IS NULL AND
			(	SELECT MAX([Version])
				FROM Approval av 
				WHERE 
					av.Job = a.Job
					AND av.IsDeleted = 0
					AND EXISTS (
									SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
								)
			) = a.[Version])			
			OR (a.CurrentPhase IS NOT NULL 
			    AND a.CurrentPhase = (SELECT avv.CurrentPhase 
									  FROM approval avv 
									  WHERE avv.Job = a.Job 
									        AND a.[Version] = avv.[Version]
									        AND avv.[Version] = (SELECT MAX([Version])
																				 FROM Approval av 
																				 WHERE av.Job = a.Job AND av.IsDeleted = 0)

											AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																				SELECT TOP 1 ac.ID 
																				FROM ApprovalCollaborator ac 
																				WHERE ac.Approval = avv.ID AND @P_User = ac.Collaborator AND ac.Phase = avv.CurrentPhase
																			  )
																	)
																						 		
								      )
				)
			)	
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  t.ID,
			a.ID AS Approval,
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],
			js.[Key] AS [Status],
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,			
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			(SELECT COUNT(av.ID)
			 FROM Approval av
			 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 WHERE ap.Approval = t.ApprovalID			 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END)  AS HasAnnotations,							
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (a.ID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	Job j
			INNER JOIN Approval a 
				ON a.Job = j.ID
			JOIN @TempItems t 
				ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
				ON 	at.ID = a.[Type]
			INNER JOIN JobStatus js
				ON js.ID = j.[Status]
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	 
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
	 
END
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--alter billing report to return all accounts 
CREATE PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    
        
         DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN dbo.AccountStatus ast ON a.[Status] = ast.ID 
                WHERE	a.ID IN (SELECT ID FROM dbo.[GetChildAccounts](@P_LoggedAccount))
						AND a.ID != 1
                        AND a.IsTemporary = 0
						AND (ast.[Key] = 'A')
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR ( @P_BillingPlanList != '' AND (
																 EXISTS (SELECT    val
																		 FROM      dbo.splitString(@P_BillingPlanList, ',')
																		 where val in (
																					   SELECT bp.SelectedBillingPlan from dbo.AccountSubscriptionPlan bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   UNION
																					   SELECT bp.SelectedBillingPlan from dbo.SubscriberPlanInfo bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   )
																		)																 
																)
								 )
                            )
                            
        DECLARE @AccountIDs AS AccountIDsTableType;  

		INSERT INTO @AccountIDs (ID)  
			SELECT ID
			FROM @subAccounts;
			
		DECLARE @billingCycles TABLE 
		(
			AccountID INT, 
			AppModule INT,
			SelectedBillingPlan INT,
			CycleAllowedProofsCount INT,
			BillingPlanPrice INT,
			Collaborate NVARCHAR(MAX),
			Deliver NVARCHAR(MAX)
		);
		
		INSERT INTO @billingCycles
		SELECT * FROM [dbo].[GetBillingCycles](@P_LoggedAccount, @P_StartDate, @P_EndDate, @AccountIDs)
               
        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateContractStartDate DATETIME,
              CollaborateIsMonthlyBillingFrequency BIT,              
              CollaborateCycleDetails NVARCHAR(MAX) ,
              CollaborateModuleId INT ,
              CollaborateIsQuotaAllocationEnabled BIT,
              ManageBillingPlanId INT ,
              ManageBillingPlanName NVARCHAR(100) ,
              ManageProofs INT ,
              ManageIsFixedPlan BIT ,
              ManageContractStartDate DATETIME,
              ManageIsMonthlyBillingFrequency BIT,
              ManageCycleDetails NVARCHAR(MAX) ,
              ManageModuleId INT ,
              ManageIsQuotaAllocationEnabled BIT,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverContractStartDate DATETIME,
              DeliverIsMonthlyBillingFrequency BIT,
              DeliverCycleDetails NVARCHAR(MAX) ,
              DeliverModuleId INT ,
              DeliverIsQuotaAllocationEnabled BIT,
              ManageApiBillingPlanId INT ,
              ManageApiBillingPlanName NVARCHAR(100) ,
              ManageApiProofs INT ,
              ManageApiIsFixedPlan BIT ,
              ManageApiContractStartDate DATETIME,
              ManageApiIsMonthlyBillingFrequency BIT,
              ManageApiCycleDetails NVARCHAR(MAX) ,
              ManageApiModuleId INT,
              ManageApiIsQuotaAllocationEnabled BIT,
              QualityControlBillingPlanId INT ,
              QualityControlBillingPlanName NVARCHAR(100) ,
              QualityControlProofs INT ,
              QualityControlIsFixedPlan BIT ,
              QualityControlContractStartDate DATETIME,
              QualityControlIsMonthlyBillingFrequency BIT,
              QualityControlCycleDetails NVARCHAR(MAX) ,
              QualityControlModuleId INT,
              QualityControlIsQuotaAllocationEnabled BIT  
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateContractStartDate,
                  CollaborateIsMonthlyBillingFrequency,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  CollaborateIsQuotaAllocationEnabled,
                  ManageBillingPlanId ,
                  ManageBillingPlanName ,
                  ManageProofs ,
                  ManageIsFixedPlan ,
                  ManageContractStartDate,
                  ManageIsMonthlyBillingFrequency,
                  ManageCycleDetails ,
                  ManageModuleId ,
                  ManageIsQuotaAllocationEnabled,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverContractStartDate,
                  DeliverIsMonthlyBillingFrequency,
                  DeliverCycleDetails ,
                  DeliverModuleId ,
                  DeliverIsQuotaAllocationEnabled,
                  ManageApiBillingPlanId ,
                  ManageApiBillingPlanName ,
                  ManageApiProofs ,
                  ManageApiIsFixedPlan ,
                  ManageApiContractStartDate,
                  ManageApiIsMonthlyBillingFrequency,
                  ManageApiCycleDetails ,
                  ManageApiModuleId,
                  ManageApiIsQuotaAllocationEnabled,
                  QualityControlBillingPlanId,
				  QualityControlBillingPlanName ,
				  QualityControlProofs,
				  QualityControlIsFixedPlan,
				  QualityControlContractStartDate,
				  QualityControlIsMonthlyBillingFrequency,
				  QualityControlCycleDetails,
				  QualityControlModuleId,
				  QualityControlIsQuotaAllocationEnabled 
                )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.ContractStartDate
                             ELSE CASPBP.ContractStartDate
                        END AS CollaborateContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(CASPBP.IsMonthlyBillingFrequency, 0)
                        END AS CollaborateIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Collaborate, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = CSPIBP.BillingPlan OR SelectedBillingPlan IS NULL))
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Collaborate, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = CASPBP.BillingPlan OR SelectedBillingPlan IS NULL))
                               END, '0|0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.IsQuotaAllocationEnabled
                             ELSE CASPBP.IsQuotaAllocationEnabled
                        END AS [CollaborateIsQuotaAllocationEnabled],
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MSPIBP.BillingPlan, 0)
                             ELSE ISNULL(MASPBP.BillingPlan, 0)
                        END AS ManageBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MSPIBP.BillingPlanName, '')
                             ELSE ISNULL(MASPBP.BillingPlanName, '')
                        END AS ManageBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MSPIBP.Proofs, '')
                             ELSE ISNULL(MASPBP.Proofs, '')
                        END AS ManageProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MSPIBP.IsFixed, 0)
                             ELSE ISNULL(MASPBP.IsFixed, 0)
                        END AS ManageIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN MSPIBP.ContractStartDate
                             ELSE MASPBP.ContractStartDate
                        END AS ManageContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(MASPBP.IsMonthlyBillingFrequency, 0)
                        END AS ManageIsMonthlyBillingFrequency ,
                        '0|0|0|0|0' AS ManageCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN MSPIBP.ModuleId
                             ELSE MASPBP.ModuleId
                        END AS [ManageModuleId] ,
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN MSPIBP.IsQuotaAllocationEnabled
                             ELSE MASPBP.IsQuotaAllocationEnabled
                        END AS [ManageIsQuotaAllocationEnabled],
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.ContractStartDate
                             ELSE DASPBP.ContractStartDate
                        END AS DeliverContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(DASPBP.IsMonthlyBillingFrequency, 0)
                        END AS DeliverIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Deliver, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = DASPBP.BillingPlan OR SelectedBillingPlan IS NULL))
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Deliver, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = DSPIBP.BillingPlan OR SelectedBillingPlan IS NULL))
                               END, '0|0|0|0"0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.IsQuotaAllocationEnabled
                             ELSE DASPBP.IsQuotaAllocationEnabled
                        END AS [DeliverIsQuotaAllocationEnabled],
                        
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MASPIBP.BillingPlan, 0)
                             ELSE ISNULL(MAASPBP.BillingPlan, 0)
                        END AS ManageApiBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MASPIBP.BillingPlanName, '')
                             ELSE ISNULL(MAASPBP.BillingPlanName, '')
                        END AS ManageApiBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MASPIBP.Proofs, '')
                             ELSE ISNULL(MAASPBP.Proofs, '')
                        END AS ManageApiProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MASPIBP.IsFixed, 0)
                             ELSE ISNULL(MAASPBP.IsFixed, 0)
                        END AS ManageApiIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN MASPIBP.ContractStartDate
                             ELSE MAASPBP.ContractStartDate
                        END AS ManageApiContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(MASPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(MAASPBP.IsMonthlyBillingFrequency, 0)
                        END AS ManageApiIsMonthlyBillingFrequency ,
                        '0|0|0|0|0' AS ManageApiCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN MASPIBP.ModuleId
                             ELSE MAASPBP.ModuleId
                        END AS [ManageApiModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN MASPIBP.IsQuotaAllocationEnabled
                             ELSE MAASPBP.IsQuotaAllocationEnabled
                        END AS [ManageApiIsQuotaAllocationEnabled],
                        
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(QSPIBP.BillingPlan, 0)
                             ELSE ISNULL(QASPBP.BillingPlan, 0)
                        END AS QualityControlBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(QSPIBP.BillingPlanName, '')
                             ELSE ISNULL(QASPBP.BillingPlanName, '')
                        END AS QualityControlBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(QSPIBP.Proofs, '')
                             ELSE ISNULL(QASPBP.Proofs, '')
                        END AS QualityControlProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(QSPIBP.IsFixed, 0)
                             ELSE ISNULL(QASPBP.IsFixed, 0)
                        END AS QualityControlIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN QSPIBP.ContractStartDate
                             ELSE QASPBP.ContractStartDate
                        END AS QualityControlContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(QSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(QASPBP.IsMonthlyBillingFrequency, 0)
                        END AS QualityControlIsMonthlyBillingFrequency ,
                        '0|0|0|0|0' AS QualityControlCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN QSPIBP.ModuleId
                             ELSE QASPBP.ModuleId
                        END AS [QualityControlModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN QSPIBP.IsQuotaAllocationEnabled
                             ELSE QASPBP.IsQuotaAllocationEnabled
                        END AS [QualityControlIsQuotaAllocationEnabled]
                        
                        
                        
                FROM    dbo.Account A
				
                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency],[IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 1 -- Manage
                                      WHERE     ASP.Account = A.ID AND ASP.IsManageAPI = 0
                                    ) MASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 1 -- Manage for Subscriber
                                      WHERE     SPI.Account = A.ID AND SPI.IsManageAPI = 0
                                    ) MSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 1 -- ManageApi for Non Subscriber
                                      WHERE     ASP.Account = A.ID AND ASP.IsManageAPI = 1
                                    ) MAASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 1 -- ManageApi for Subscriber
                                      WHERE     SPI.Account = A.ID AND SPI.IsManageAPI = 1
                                    ) MASPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                          OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 6 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) QASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 6 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) QSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )                                         
                        WHERE  ( A.ID IN ( SELECT  *
									FROM    @subAccounts )
								)
								
                                
     -- Get the accounts
        SET NOCOUNT OFF
        
        ;WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
        
       
        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.CollaborateContractStartDate,
				TBL.CollaborateIsMonthlyBillingFrequency,
				TBL.CollaborateIsQuotaAllocationEnabled, 
                TBL.ManageBillingPlanName ,
                TBL.ManageProofs ,
                TBL.ManageCycleDetails AS [ManageBillingCycleDetails] ,
                TBL.ManageModuleId ,
                TBL.ManageBillingPlanID AS [ManageBillingPlan] ,
                TBL.ManageIsFixedPlan ,
                TBL.ManageContractStartDate,
                TBL.ManageIsMonthlyBillingFrequency,
                TBL.ManageIsQuotaAllocationEnabled,
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                TBL.DeliverContractStartDate,
                TBL.DeliverIsMonthlyBillingFrequency,
                TBL.DeliverIsQuotaAllocationEnabled,
                TBL.ManageApiBillingPlanName ,
                TBL.ManageApiProofs ,
                TBL.ManageApiCycleDetails AS [ManageApiBillingCycleDetails] ,
                TBL.ManageApiModuleId ,
                TBL.ManageApiBillingPlanID AS [ManageApiBillingPlan],
                TBL.ManageApiIsFixedPlan ,
                TBL.ManageApiContractStartDate,
                TBL.ManageApiIsMonthlyBillingFrequency,
                TBL.ManageApiIsQuotaAllocationEnabled,
                TBL.QualityControlBillingPlanName ,
                TBL.QualityControlProofs ,
                TBL.QualityControlCycleDetails AS [QualityControlBillingCycleDetails] ,
                TBL.QualityControlModuleId ,
                TBL.QualityControlBillingPlanID AS [QualityControlBillingPlan] ,
                TBL.QualityControlIsFixedPlan ,
                TBL.QualityControlContractStartDate,
                TBL.QualityControlIsMonthlyBillingFrequency,
                TBL.QualityControlIsQuotaAllocationEnabled,
                 ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
				 (SELECT PathString) AS AccountPath
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID	
                INNER JOIN AccountTree act ON a.ID = act.AccountID		
    END

GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnColorProofInstanceLogin]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROCEDURE [dbo].[SPC_ReturnColorProofInstanceLogin]
    (
      @P_Username VARCHAR(255) ,
      @P_Password VARCHAR(255)
    )
AS 
    BEGIN
      -- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
        SELECT	DISTINCT
                ci.*
        FROM    [dbo].[ColorProofInstance] ci
                
        WHERE   
                ci.[Username] = @P_Username
                AND CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), ci.Password, 2 ))) = @P_Password
                AND ci.IsDeleted = 0
    END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnColorProofInstanceLoginByPairingCode]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPC_ReturnColorProofInstanceLoginByPairingCode]
    (
      @P_PairingCode VARCHAR(255) 
    )
AS 
    BEGIN

-- Open the symmetric key with which to encrypt the data.
	OPEN SYMMETRIC KEY ColorProofInstances
	   DECRYPTION BY CERTIFICATE ColorProofInstances;
	   
        SELECT DISTINCT
               ci.[ID]
			  ,ci.[Guid]
			  ,ci.[Owner]
			  ,ci.[Version]
			  ,ci.[UserName]			  
			  ,ci.[ComputerName]
			  ,ci.[SystemName]
			  ,ci.[SerialNumber]
			  ,ci.[IsOnline]
			  ,ci.[LastSeen]
			  ,ci.[State]
			  ,ci.[AdminName]
			  ,ci.[Address]
			  ,ci.[Email]
			  ,ci.[WorkflowsLastModifiedTimestamp]
			  ,ci.[IsDeleted]
			  ,ci.[PairingCode]
			  ,ci.[PairingCodeExpirationDate]           
              ,CONVERT(VARCHAR(max), DECRYPTBYKEY(CONVERT(VARBINARY(8000), ci.Password, 2 ))) AS [Password]
        FROM   [dbo].[ColorProofInstance] ci
                
        WHERE               
               ci.PairingCode = @P_PairingCode
               AND ci.IsDeleted = 0
    END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnDeliversPageInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Alter procedure to retry in case of deadlock
CREATE PROCEDURE [dbo].[SPC_ReturnDeliversPageInfo]
    (
      @P_Account INT ,
      @P_User INT ,
      @P_MaxRows INT = 100 ,
      @P_Set INT = 1 ,
      @P_SearchField INT = 0 ,
      @P_Order BIT = 0 ,
      @P_SearchText NVARCHAR(100) = '' ,
      @P_RecCount INT OUTPUT
    )
AS 
    BEGIN
    DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals
        SET NOCOUNT ON
        DECLARE @StartOffset INT ;
        SET @StartOffset = ( @P_Set - 1 ) * @P_MaxRows ;
        
        DECLARE @Delivers TABLE
            (
              ID INT ,
              [Title] NVARCHAR(128) ,
              [User] NVARCHAR(64) ,
              [Pages] NVARCHAR(50) ,
              [CPName] NVARCHAR(50) ,
              [Workflow] NVARCHAR(256) ,
              [CreatedDate] DATETIME ,
              [StatusKey] INT ,
              [Account] INT ,
              [OwnUser] INT,
              [IsShared] BIT
            )
	-- Get the records to the Delivers
        INSERT  INTO @Delivers
                ( ID ,
                  Title ,
                  [User] ,
                  Pages ,
                  CPName ,
                  Workflow ,
                  CreatedDate ,
                  StatusKey ,
                  Account ,
                  OwnUser,
                  IsShared
                )
                (
                --- deliver jobs for contributor users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.[owner] = @P_User
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'CON' -- contributor should only see his jobs
                  UNION
                --- deliver jobs for admin users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' -- admin should see all the jobs from current account
                  UNION
                  --- deliver shared jobs for admins -----
                  SELECT DISTINCT	DJ.ID ,
									J.Title ,
									[User].GivenName AS [User] ,
									DJ.Pages ,
									CPI.SystemName AS [CPName] ,
									CPCZW.Name AS [Workflow] ,
									DJ.CreatedDate ,
									Status.[Key] AS [StatusKey] ,
									J.Account AS [Account] ,
									DJ.[owner] AS [OwnUser],
									CONVERT(BIT, 1) AS [IsShared]
                  FROM DeliverJob DJ
                       JOIN dbo.Job J ON DJ.Job = J.ID
                       JOIN dbo.ColorProofCoZoneWorkflow CPCZW ON CPCZW.ID = DJ.CPWorkflow
                       JOIN dbo.SharedColorProofWorkflow SCPW ON SCPW.ColorProofCoZoneWorkflow = CPCZW.ID
                       CROSS APPLY (SELECT U.GivenName,
										   U.Account
									FROM dbo.[User] U
									JOIN Account ACC on U.Account = ACC.ID
								    WHERE U.Account = (SELECT Account FROM [User] WHERE ID = SCPW.[USER]) AND U.ID = DJ.[Owner]
                                 ) [User] ( GivenName, Account )
                       JOIN dbo.ColorProofWorkflowInstanceView CPWIV ON SCPW.ColorProofCoZoneWorkflow = CPWIV.ColorProofCoZoneWorkflowID
                       JOIN dbo.ColorProofInstance CPI ON CPWIV.ColorProofInstance = CPI.ID
                       JOIN dbo.[User] CPUO ON CPI.Owner = CPUO.ID
                       JOIN dbo.Account AC ON CPUO.Account = AC.ID                    
                       JOIN dbo.AccountStatus ACS ON AC.[Status] = ACS.ID
                       CROSS APPLY (SELECT  DJS.[Key]
									FROM  dbo.DeliverJobStatus DJS
									WHERE  DJS.ID = DJ.Status
									) Status ( [Key] ) 
                  WHERE ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                          )
                          AND DJ.IsDeleted = 0
                          AND J.Account <> @P_Account
			              AND ACS.[Key] != 'D'
                          AND AC.ID = @P_Account 
                          AND SCPW.IsAccepted = 1 
                          AND SCPW.IsEnabled = 1
                          AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) = 'ADM' 
                  UNION
                --- deliver jobs for manager and moderator users
                  SELECT DISTINCT
                            DJ.ID ,
                            J.Title ,
                            [User].GivenName AS [User] ,
                            DJ.Pages ,
                            Cp.SystemName AS [CPName] ,
                            Cp.WorkflowName AS [Workflow] ,
                            DJ.CreatedDate ,
                            Status.[Key] AS [StatusKey] ,
                            J.Account AS [Account] ,
                            DJ.[owner] AS [OwnUser],
							CONVERT(BIT, 0) AS [IsShared]
                  FROM      dbo.DeliverJob DJ
                            JOIN dbo.Job J ON DJ.Job = J.ID
                            CROSS APPLY ( SELECT    U.GivenName
                                          FROM      dbo.[User] U
                                          WHERE     U.ID = Dj.Owner
                                        ) [User] ( GivenName )
                            CROSS APPLY ( SELECT    CPI.SystemName ,
                                                    CPCZW.Name
                                          FROM      dbo.ColorProofCoZoneWorkflow CPCZW
                                                    INNER JOIN dbo.ColorProofWorkflow CPW ON CPW.ID = CPCZW.ColorProofWorkflow
                                                    INNER JOIN dbo.ColorProofInstance CPI ON CPI.ID = CPW.ColorProofInstance
                                          WHERE     CPCZW.ID = DJ.CPWorkflow
                                        ) CP ( SystemName, WorkflowName )
                            CROSS APPLY ( SELECT    DJS.[Key]
                                          FROM      dbo.DeliverJobStatus DJS
                                          WHERE     DJS.ID = DJ.Status
                                        ) Status ( [Key] )
                  WHERE     ( @P_SearchText IS NULL
                              OR @P_SearchText = ''
                              OR J.Title LIKE '%' + @P_SearchText + '%'
                            )
                            AND J.[Account] = @P_Account
                            AND DJ.IsDeleted = 0
                            AND DJ.[owner] IN (
                            SELECT  *
                            FROM    dbo.[GetUserListWhereBelongs](@P_User) )
                            AND dbo.GetUserRoleByUserIdAndModuleId(@P_User, 2) IN (
                            'MAN', 'MOD' ) -- moderator and manager should see their jobs + jobs of the users that belongs to the same user group                  
                )
                
            -- Return the total effected records
        SELECT  *
        FROM    ( SELECT TOP ( @P_Set * @P_MaxRows )
                            D.* ,
                            CONVERT(INT, ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.CreatedDate
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 0
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.CreatedDate
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 0
                                                              ) THEN D.Title
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 1
                                                              AND @P_Order = 1
                                                              ) THEN D.Title
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 0
                                                              ) THEN D.[User]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 2
                                                              AND @P_Order = 1
                                                              ) THEN D.[User]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 0
                                                              ) THEN D.[Pages]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 3
                                                              AND @P_Order = 1
                                                              ) THEN D.[Pages]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CpName]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 4
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CpName]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[Workflow]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 5
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[Workflow]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 6
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[CreatedDate]
                                                              END ASC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 0
                                                              )
                                                              THEN D.[StatusKey]
                                                              END DESC , CASE
                                                              WHEN ( @P_SearchField = 7
                                                              AND @P_Order = 1
                                                              )
                                                              THEN D.[StatusKey]
                                                              END ASC )) AS [Row]
                  FROM      @Delivers AS D
                ) AS SLCT
        WHERE   Row > @StartOffset
        
        SELECT  @P_RecCount = COUNT(1)
        FROM    ( SELECT    D.ID
                  FROM      @Delivers D
                ) AS SLCT
                
                SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			IF (ERROR_NUMBER() = 1205)
				SET @retry = @retry - 1;
			ELSE
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
END; -- End WHILE loop	

    END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---Update ReturnFoldersApprovalsPageInfo Stored Procedure by adding a new ViewAll parameter
CREATE PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_ViewAll bit,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)	   
	)

	DECLARE @TempOnlyIDs TABLE
	(
	   ApprovalID int
	)

	IF @P_ViewAll = 1 
	BEGIN
		INSERT INTO @TempOnlyIDs(ApprovalID)
		select id FROM
		(
			select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
			FROM approval a
			INNER JOIN job j on j.id=a.job
			where @P_Account=j.account and a.IsDeleted=0 and a.DeletePermanently = 0
		) x 
		WHERE x.r = 1
	END
	ELSE
	BEGIN 
		-- Temporary select all the Approvals for the user, with current phase = NULL
		BEGIN
			;WITH AllVersions as 
			(
				select a.* from approval a 
				inner join Job j on j.id=a.job
				where j.account=@P_Account and a.IsDeleted=0 and a.DeletePermanently = 0 and CurrentPhase is null
			)

			INSERT INTO @TempOnlyIDs(ApprovalID)

			SELECT DISTINCT a.id
			FROM AllVersions a
			LEFT JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator
			WHERE 
				(ac.ID is not null)
			OPTION(OPTIMIZE FOR (@P_Account UNKNOWN))

			delete @TempOnlyIDs
			from @TempOnlyIDs t
			inner join
			(
				select a.job, a.id, ROW_NUMBER() OVER (PARTITION BY job ORDER BY Version DESC, a.ID DESC) r
				from @TempOnlyIDs t
				inner join approval a on a.id=t.ApprovalID
			) x on x.ID=t.ApprovalID and x.r > 1
		END

		-- Temporary select all the Approvals for the user, with current phase = NOT NULL
		BEGIN

			;WITH AllVersions AS 
			(
				SELECT j.JobOwner, a.*, ROW_NUMBER() OVER (PARTITION BY a.job ORDER BY Version DESC, a.ID DESC) RowNumber
				FROM Approval a
				inner join Job j on j.id=a.job
				WHERE j.account=@P_Account and a.IsDeleted=0 and a.DeletePermanently = 0 and CurrentPhase is not null
			)

			INSERT INTO @TempOnlyIDs(ApprovalID)

			SELECT DISTINCT a.id 
			FROM AllVersions a
			LEFT JOIN ApprovalCollaborator ac on ac.Approval=a.id AND @P_User = ac.Collaborator and ac.phase=a.currentphase
			WHERE RowNumber=1
				and (a.[JobOwner] = @P_User or ac.ID is not null)
			OPTION(OPTIMIZE FOR (@P_Account UNKNOWN))

			--delete duplicates
			delete @TempOnlyIDs
			from @TempOnlyIDs t
			inner join
			(
				select t.ApprovalID, ROW_NUMBER() OVER (PARTITION BY ApprovalID ORDER BY ApprovalID DESC) r
				from @TempOnlyIDs t
			) x on x.ApprovalID=t.ApprovalID and x.r > 1

		END


	END

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.Deadline,
					a.CreatedDate,
					a.ModifiedDate,
					js.[Key] AS [Status],
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase
			from @TempOnlyIDs t 
					INNER JOIN Approval a WITH (NOLOCK)
						ON a.id= t.ApprovalID
					inner join Job j WITH (NOLOCK)
						ON j.id=a.job
					INNER JOIN JobStatus js WITH (NOLOCK)
						ON js.ID = j.[Status]
					LEFT OUTER JOIN FolderApproval fa WITH (NOLOCK)
						ON fa.Approval = a.ID
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WITH (NOLOCK) WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WITH (NOLOCK) WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WITH (NOLOCK) WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)
			WHERE	
					js.[Key] != 'ARC'
					AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND	@P_FolderId = fa.Folder
				
			UNION
			
			SELECT 	f.ID AS Approval,
					GetDate() AS Deadline,
					f.CreatedDate,
					f.ModifiedDate,
					'' AS [Status],
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase
			FROM	Folder f WITH (NOLOCK)
			WHERE	f.Account = @P_Account
					AND (@P_Status = 0)
					AND f.IsDeleted = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
					AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
			) a
	ORDER BY 
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN a.[Status]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN a.[Status]
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap WITH (NOLOCK)
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WITH (NOLOCK) WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap WITH (NOLOCK)
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WITH (NOLOCK) WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
		FROM (
				SELECT  t.ID AS ID,
						a.ID AS Approval,
						0 AS Folder,
						j.Title,
						a.[Guid],
						a.[FileName],
						js.[Key] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap WITH (NOLOCK)
								     WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
						END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						t.PrimaryDecisionMakerName,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						at.[Key] AS ApprovalType,
						a.[Version] AS MaxVersion,
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av WITH (NOLOCK)
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,
						(SELECT CASE
						 WHEN ( EXISTS (SELECT aa.ID					
										 FROM dbo.ApprovalAnnotation aa WITH (NOLOCK)
										 INNER JOIN dbo.ApprovalPage ap WITH (NOLOCK) ON aa.Page = ap.ID
										 WHERE ap.Approval = t.ApprovalID			 
										 )
								) THEN CONVERT(bit,1)
								  ELSE CONVERT(bit,0)
						 END)  AS HasAnnotations,				
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
						THEN(						     
								(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
								   THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
													JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
										)
									ELSE
									(							    
										(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
													                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
										 THEN
											(SELECT TOP 1 appcd.[Key]
												FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
														JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
														WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
														ORDER BY ad.[Priority] ASC
													) appcd
										     )
										 ELSE '0'
										 END 
										 )   			   
									)
									END
								)								
							)		
						WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
						  THEN
							(SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
										JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						ELSE
						 (SELECT TOP 1 appcd.[Key]
								FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
										JOIN dbo.ApprovalDecision ad WITH (NOLOCK) ON acd.Decision = ad.ID 
									WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
							)
						END	
				), 0 ) AS Decision,
				CONVERT(bit, 1) AS IsCollaborator,
				CONVERT(bit, 0) AS AllCollaboratorsDecisionRequired,				
				(SELECT CASE 
						WHEN a.LockProofWhenAllDecisionsMade = 1
							THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
						ELSE
							NULL
						END	
				) as ApprovalApprovedDate,
				ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
				t.PhaseName,
				ISNULL((SELECT CASE
							WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WITH (NOLOCK) WHERE ajw.Job = a.Job)
							ELSE ''
						END),'') AS WorkflowName,
				ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap WITH (NOLOCK)
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WITH (NOLOCK) WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
				ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
				ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WITH (NOLOCK) WHERE ID = a.CurrentPhase), 0) AS DecisionType,
				(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
							 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WITH (NOLOCK) WHERE ID = j.[JobOwner])
							 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WITH (NOLOCK) WHERE ID = a.[Owner])
							 END) AS OwnerName,
				a.AllowOtherUsersToBeAdded,
				j.JobOwner,
				(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WITH (NOLOCK) WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
														   JOIN ApprovalJobPhase ajp WITH (NOLOCK) ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WITH (NOLOCK) WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd WITH (NOLOCK)
														  JOIN ApprovalJobPhase ajp WITH (NOLOCK) ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker				
				FROM	Job j WITH (NOLOCK)
						INNER JOIN Approval a WITH (NOLOCK)
							ON a.Job = j.ID
						INNER JOIN dbo.ApprovalType at WITH (NOLOCK)
							ON 	at.ID = a.[Type]
						INNER JOIN JobStatus js WITH (NOLOCK)
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa WITH (NOLOCK)
							ON fa.Approval = a.ID	
						JOIN @TempItems t
							ON t.ApprovalID = a.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 1
						
				UNION
				
				SELECT 	t.ID AS ID,
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						'' AS [Guid],
						'' AS [FileName],
						'' AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'' AS PrimaryDecisionMakerName,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS ApprovalType,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1 WITH (NOLOCK)
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa WITH (NOLOCK)
                				INNER JOIN Approval av WITH (NOLOCK)
                					ON av.ID = fa.Approval
                				INNER JOIN dbo.Job jo WITH (NOLOCK) ON av.Job = jo.ID
                				INNER JOIN dbo.JobStatus jos WITH (NOLOCK) ON jo.Status = jos.ID
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0 AND jos.[Key] != 'ARC'
						) AS ApprovalsCount,
						CONVERT(bit, 0) AS HasAnnotations,
						'' AS Decision,
						CONVERT(bit, 1) AS IsCollaborator,
						f.AllCollaboratorsDecisionRequired,
						NULL as ApprovalApprovedDate,
						0 AS CurrentPhase,
						'' AS PhaseName,
						'' AS WorkflowName,
						'' AS NextPhase,
						'' AS DecisionMakers,
						0 AS DecisionType,
						'' AS OwnerName,
						CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,
						NULL AS JobOwner,
						CONVERT(bit, 0) AS IsPhaseComplete,
						'' AS VersionSufix,
						CONVERT(bit, 0) AS IsLocked,
					    CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
				FROM	Folder f WITH (NOLOCK)
						JOIN @TempItems t 
							ON t.ApprovalID = f.ID
				WHERE t.ID >= @StartOffset AND t.IsApproval = 0
			) result		
	ORDER BY result.ID	

	SET ROWCOUNT 0

	---- Send the total rows count ---- 
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j WITH (NOLOCK)
						INNER JOIN Approval a WITH (NOLOCK)
							ON a.Job = j.ID
						INNER JOIN JobStatus js WITH (NOLOCK)
							ON js.ID = j.[Status]
						LEFT OUTER JOIN FolderApproval fa WITH (NOLOCK)
							ON fa.Approval = a.ID
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))					
						AND ( @P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND	(	SELECT MAX([Version])
								FROM Approval av WITH (NOLOCK)
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND EXISTS (
													SELECT TOP 1 ac.ID 
													FROM ApprovalCollaborator ac WITH (NOLOCK)
													WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator
												)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f WITH (NOLOCK)
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFTPUserLogin]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Add FTP Login stored procedure
CREATE PROCEDURE [dbo].[SPC_ReturnFTPUserLogin]
    (
      @P_Username VARCHAR(255) ,
      @P_Password VARCHAR(255)
    )
AS 
    BEGIN
        SELECT	DISTINCT
                u.*
        FROM    [dbo].[User] u
                INNER JOIN [dbo].[UserStatus] us ON u.[Status] = us.[ID]
                INNER JOIN [dbo].[Account] ac ON u.[Account] = ac.[ID]
                INNER JOIN [dbo].[AccountStatus] acs ON ac.[Status] = acs.[ID]
                INNER JOIN [dbo].[UserGroupUser] ugu ON u.[ID] = ugu.[User]
                INNER JOIN [dbo].[UserGroup] ug ON ug.[ID] = ugu.[UserGroup]
				INNER JOIN [dbo].[UploadEngineUserGroupPermissions] ugp ON ugu.[UserGroup] = ugp.[UserGroup]
				INNER JOIN [dbo].[UploadEnginePermission] uep ON ugp.[UploadEnginePermission] = uep.[ID]
        WHERE   u.[Username] = @P_Username  AND
				u.[Password] = CONVERT(VARCHAR(255), HashBytes('SHA1', @P_Password)) AND
				uep.[Key] = 1 AND
				us.[Key] = 'A' AND
				acs.[Key] = 'A'
    END
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnInkDensityToleranceReport]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
--Reports SPC
CREATE PROCEDURE [dbo].[SPC_ReturnInkDensityToleranceReport]
	@PrinterCompany INT,
	@PrintProcess INT,
	@Product INT,
	@Version INT,
	@Section INT,
	@Inks NVARCHAR(1024)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DataCaptureDetails TABLE (Id INT) 
	INSERT into @DataCaptureDetails (Id)
	SELECT ID FROM dbo.QualityControlDataCaptureDetails q
	WHERE q.PrintProcess = @PrintProcess AND 
			q.Product = @Product AND
			q.Version = @Version AND
			q.Section =	@Section AND
			q.PrinterCompany = @PrinterCompany

	DECLARE @DataCaptureDetailsIDs AS NVARCHAR(MAX)
	SELECT @DataCaptureDetailsIDs = COALESCE(@DataCaptureDetailsIDs + ',' ,'') + CAST(Id AS NVARCHAR(10))
	FROM @DataCaptureDetails			

	DECLARE @Cols AS NVARCHAR(MAX);
	DECLARE @Query AS NVARCHAR(MAX);

	SELECT @Cols = COALESCE(@Cols + ', ', '') + QUOTENAME(Copies)
	FROM
	(
		SELECT DISTINCT Copies
		FROM QualityControlDataCaptureMeasurement 
		WHERE DataCaptureDetails in ( select ID from @DataCaptureDetails )
		GROUP BY Copies
	)AS B
	ORDER BY B.Copies
	
	-- ADD Quotes between ink names wo use them in IN clause
	SET @Inks = REPLACE( @Inks ,',', ''',''')

	SET @Query = '
	WITH PivotData AS
	(
		SELECT Copies, Density, InkName COLLATE Latin1_General_CS_AS as InkName, Threshold, TargetDensityValue, AVG(Measurement) AS Measurement
		FROM QualityControlDataCaptureMeasurement 
		WHERE DataCaptureDetails IN ('+ @DataCaptureDetailsIDs + ')
		AND InkName COLLATE Latin1_General_CS_AS IN (''' + @Inks + ''')
		GROUP BY InkName COLLATE Latin1_General_CS_AS, Density, Copies, Threshold, TargetDensityValue
	)

	SELECT Density, InkName, Threshold,TargetDensityValue,' + @Cols + '
	FROM PivotData
	PIVOT
	(
		SUM(Measurement)
		FOR Copies
		IN (' + @Cols + ')
	)AS PivotResult
	ORDER BY InkName'

	EXEC (@Query)
    
END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Added "IsActive" check for NextPhase column
CREATE PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_HideCompletedApprovals bit, 
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

	------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.ID,
			PhaseName,
			PrimaryDecisionMakerName
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			INNER JOIN Job j 
					ON j.ID = a.Job
			INNER JOIN JobStatus js 
					ON js.ID = j.[Status]
			CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
			CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)									
	WHERE 
			js.[Key] != CASE WHEN @P_Status = 3 THEN '' ELSE (CASE WHEN @P_HideCompletedApprovals = 1 THEN 'COM' ELSE '' END) END AND
			j.Account = @P_Account					
			AND a.IsDeleted = 0
			AND a.DeletePermanently = 0
			AND js.[Key] != 'ARC'
			AND ((@P_Status = 0) OR ((@P_Status = 6) AND ((js.[Key] = 'CCM') OR (js.[Key] = 'CRQ'))) OR (js.ID = @P_Status))				
			AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
			AND		auvi.[User] = @P_User
			AND ((a.CurrentPhase IS NULL AND(	SELECT MAX([ViewedDate]) 
												FROM ApprovalUserViewInfo vuvi
													INNER JOIN Approval av
														ON  av.ID = vuvi.[Approval]
													INNER JOIN Job vj
														ON vj.ID = av.Job	
												WHERE  av.Job = a.Job
													AND av.IsDeleted = 0 AND vuvi.[User] = @P_User
											) = auvi.[ViewedDate]
										AND EXISTS (
														SELECT TOP 1 ac.ID 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
													)
			)						
		   OR (a.CurrentPhase IS NOT NULL  AND a.CurrentPhase = (SELECT av.CurrentPhase 
																  FROM ApprovalUserViewInfo avi
																  INNER JOIN Approval av ON av.ID =  avi.Approval
																  WHERE av.Job = a.Job AND avi.[User] = @P_User
																  AND avi.[ViewedDate] = auvi.[ViewedDate]
																		AND avi.[ViewedDate] =( SELECT MAX([ViewedDate]) 
																								FROM ApprovalUserViewInfo vuvi
																								INNER JOIN Approval avv
																									ON  avv.ID = vuvi.[Approval]
																								INNER JOIN Job vj
																									ON vj.ID = avv.Job	
																								WHERE  avv.Job = a.Job  AND vuvi.[User] = @P_User
																								AND avv.IsDeleted = 0)
																		AND (EXISTS(
																					SELECT TOP 1 ac.ID 
																					FROM ApprovalCollaborator ac 
																					WHERE ac.Approval = av.ID AND @P_User = ac.Collaborator AND ac.Phase = av.CurrentPhase
																				  )
																				  OR ISNULL(j.JobOwner, 0) = @P_User
																			)
																		 
																)
				)
			)
	ORDER BY 
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE						
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
		END ASC,
		CASE						
			WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
		END ASC,
		CASE
			WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
		END DESC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
		END ASC,
		CASE
			WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
		END DESC,		
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN j.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN  j.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.ID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT DISTINCT	t.ID,
			a.ID AS Approval,	
			0 AS Folder,
			j.Title,
			a.[Guid],
			a.[FileName],							 
			js.[Key] AS [Status],	
			j.ID AS Job,
			a.[Version],
			100 AS Progress,
			a.CreatedDate,
			a.ModifiedDate,
			ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
			a.Creator,
			a.[Owner],
			ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,
			t.PrimaryDecisionMakerName,
			a.AllowDownloadOriginal,
			a.IsDeleted,
			at.[Key] AS ApprovalType,
			a.[Version] AS MaxVersion,						
			ISNULL(a.[VersionSufix],'') AS VersionSufix,
			0 AS SubFoldersCount,
			(SELECT COUNT(av.ID)
				FROM Approval av
				WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
			(SELECT CASE
			 WHEN ( EXISTS (SELECT aa.ID					
							 FROM dbo.ApprovalAnnotation aa
							 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
							 INNER JOIN dbo.Approval aproval ON ap.Approval = aproval.ID
							 WHERE aproval.ID = t.ApprovalID AND (aa.Phase IS NULL OR (aa.Phase IS NOT NULL AND (aa.Phase = aproval.CurrentPhase  OR EXISTS (SELECT apj.ID FROM dbo.ApprovalJobPhase apj
																														WHERE apj.ShowAnnotationsToUsersOfOtherPhases = 1 AND apj.ID = aa.Phase )
																												)
																			           )
																					   
															       )	 
							 )
					) THEN CONVERT(bit,1)
					  ELSE CONVERT(bit,0)
			 END) AS HasAnnotations,			
			(SELECT CASE 
				WHEN a.LockProofWhenAllDecisionsMade = 1
					THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
				ELSE
					NULL
				END	
			) as ApprovalApprovedDate,
			ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0) 
				THEN(						     
						(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
						   THEN
								(SELECT TOP 1 appcd.[Key]
									FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
											JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
											WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
											ORDER BY ad.[Priority] ASC
										) appcd
								)
							ELSE
							(							    
								(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
											                   WHERE acd.Approval = t.ApprovalID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
								 THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
												ORDER BY ad.[Priority] ASC
											) appcd
								     )
								 ELSE '0'
								 END 
								 )   			   
							)
							END
						)								
					)		
				WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
				  THEN
					(SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				ELSE
				 (SELECT TOP 1 appcd.[Key]
						FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
								JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
							WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
					)
				END	
				), 0 ) AS Decision,
			CONVERT(bit, 1) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') AS VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
	FROM	ApprovalUserViewInfo auvi
			INNER JOIN Approval a	
					ON a.ID =  auvi.Approval
			JOIN @TempItems t 
					ON t.ApprovalID = a.ID
			INNER JOIN dbo.ApprovalType at
					ON 	at.ID = a.[Type]
			INNER JOIN Job j
					ON j.ID = a.Job
			INNER JOIN JobStatus js
					ON js.ID = j.[Status]			
	WHERE t.ID >= @StartOffset
	ORDER BY t.ID
	
	SET ROWCOUNT 0
	  
	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop
END

GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecycleBinPageInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Added "IsActive" check for NextPhase column
CREATE PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,
	@P_ViewAll bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
DECLARE @retry INT;
SET @retry = 5;
WHILE (@retry > 0)
BEGIN
    BEGIN TRY
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set -1) * @P_MaxRows + 1;
		
	DECLARE @TempItems TABLE
	(
	   ID int IDENTITY PRIMARY KEY,
	   ApprovalID int,
	   IsApproval bit,
	   PhaseName varchar(150),
	   PrimaryDecisionMakerName varchar(150)
	)
		
	DECLARE @maxRow INT

	SET @maxRow = (@StartOffset + @P_MaxRows)

	SET ROWCOUNT @maxRow

    ------- Insert approval id in temp table ------------
	INSERT INTO @TempItems (ApprovalID, IsApproval, PhaseName, PrimaryDecisionMakerName)
	SELECT 
			a.Approval,
			IsApproval,
			a.PhaseName,
			a.PrimaryDecisionMakerName
	FROM (				
			SELECT 	a.ID AS Approval,
					a.ModifiedDate,
					CONVERT(bit, 1) as IsApproval,
					PhaseName,
					PrimaryDecisionMakerName,
					j.Title,
					a.PrimaryDecisionMaker,
					a.ExternalPrimaryDecisionMaker,
					a.CurrentPhase,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]
					CROSS APPLY (SELECT CASE WHEN a.CurrentPhase IS NOT NULL 
										THEN (SELECT Name FROM ApprovalJobPhase WHERE ID = a.CurrentPhase)
										ELSE ''								
						 END) CP (PhaseName)	
					CROSS APPLY (SELECT CASE WHEN ISNULL(a.PrimaryDecisionMaker, 0) != 0 
										THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.PrimaryDecisionMaker)
									 WHEN ISNULL(a.ExternalPrimaryDecisionMaker, 0) != 0
										THEN (SELECT GivenName + ' ' + FamilyName FROM ExternalCollaborator WHERE ID = a.ExternalPrimaryDecisionMaker)
									 ELSE ''
								END) PDMN (PrimaryDecisionMakerName)		 							
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 1
					AND a.DeletePermanently = 0
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
					AND (
							ISNULL(	(SELECT TOP 1 ac.ID 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID AND ac.Collaborator = @P_User)
									, 
									(SELECT TOP 1 aph.ID FROM dbo.ApprovalUserRecycleBinHistory aph
							        WHERE aph.Approval = a.ID AND aph.[User] = @P_User)
							      ) IS NOT NULL						
						)
					AND (
						 @P_ViewAll = 1 
						 OR
						 ( 
							@P_ViewAll = 0 AND((a.CurrentPhase IS NULL AND	EXISTS (
																					 SELECT TOP 1 ac.ID 
																					 FROM ApprovalCollaborator ac 
																					 WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator
																					))
										  OR (a.CurrentPhase IS NOT NULL AND (@P_User = ISNULL(j.JobOwner, 0) OR EXISTS(
																												SELECT TOP 1 ac.ID 
																												FROM ApprovalCollaborator ac 
																												WHERE ac.Approval = a.ID AND @P_User = ac.Collaborator AND ac.Phase = a.CurrentPhase
																											  )
											                                  )
											   )
											  )
					     )	
					    )	 					
			UNION
			
			SELECT 	f.ID AS Approval,
					f.ModifiedDate,
					CONVERT(bit, 0) as IsApproval,
					'' AS PhaseName,
					'' AS PrimaryDecisionMakerName,
					'' AS Title,
					0 AS PrimaryDecisionMaker,
					0 AS ExternalPrimaryDecisionMaker,
					0 AS CurrentPhase,
					GetDate() AS Deadline
			FROM	Folder f							
			WHERE	f.Account = @P_Account
					AND f.IsDeleted = 1
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
					AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.Creator = f.Creator AND pf.IsDeleted = 1) = 0)				
					AND f.Creator = @P_User
			) a
	ORDER BY
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
		END ASC,
		CASE
			WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
		END DESC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN a.Title
		END ASC,
		CASE
			WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN a.Title
		END DESC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN a.PrimaryDecisionMakerName
		END ASC,
		CASE
			WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN a.PrimaryDecisionMakerName
		END DESC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 0) THEN a.PhaseName
		END ASC,
		CASE
			WHEN (@P_SearchField = 6 AND @P_Order = 1) THEN a.PhaseName
		END DESC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 0) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END ASC,
		CASE
			WHEN (@P_SearchField = 7 AND @P_Order = 1) THEN (SELECT TOP 1 ap.Name       
																		  FROM ApprovalJobPhase ap
																		  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
																		  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position
																		  ORDER BY ap.Position )
		END DESC,	
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 0) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END ASC,
		CASE
			WHEN (@P_SearchField = 8 AND @P_Order = 1) THEN [dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, a.Approval, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker)
		END DESC
		OPTION(OPTIMIZE FOR (@P_User UNKNOWN, @P_Account UNKNOWN))
	--------------- End of select --------------------------------------------------------- 
		
	SET ROWCOUNT @P_MaxRows

	--------------- Select all -------------------------------------------------------------
	SELECT  result.ID,
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Guid],
			result.[FileName],
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.PrimaryDecisionMakerName,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.ApprovalType,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.HasAnnotations,
			result.Decision,
			result.IsCollaborator,
			result.AllCollaboratorsDecisionRequired,
			result.ApprovalApprovedDate,
			result.CurrentPhase,
			result.PhaseName,
			result.WorkflowName,
			result.NextPhase,
			result.DecisionMakers,
			result.DecisionType,
			result.OwnerName,
			result.AllowOtherUsersToBeAdded,
			result.JobOwner,
			result.IsPhaseComplete,
			result.VersionSufix,
			result.IsLocked,
			result.IsExternalPrimaryDecisionMaker
	FROM (				
			SELECT 	t.ID AS ID,
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,
					a.[Guid],
					a.[FileName],							 
					js.[Key] AS [Status],
					j.ID AS Job,
					a.[Version],
					(SELECT CASE 
					WHEN a.IsError = 1 THEN -1
					ELSE ISNULL((SELECT TOP 1 Progress FROM ApprovalPage ap
							WHERE ap.Approval = t.ApprovalID and ap.Number = 1), 0 ) 					
					END) AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, ISNULL(a.ExternalPrimaryDecisionMaker, 0)) AS PrimaryDecisionMaker,	
					t.PrimaryDecisionMakerName,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					at.[Key] AS ApprovalType,
					a.[Version] AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT CASE
					 WHEN ( EXISTS (SELECT aa.ID					
									 FROM dbo.ApprovalAnnotation aa
									 INNER JOIN dbo.ApprovalPage ap ON aa.Page = ap.ID
									 WHERE ap.Approval = t.ApprovalID			 
									 )
							) THEN CONVERT(bit,1)
							  ELSE CONVERT(bit,0)
					 END)  AS HasAnnotations,					
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 ) 
					THEN(						     
							(SELECT CASE WHEN ((SELECT [dbo].[GetApprovalAllCollaboratorsDecisionRequiredSetting] (t.ApprovalID, @P_Account)) = 0)
							   THEN
									(SELECT TOP 1 appcd.[Key]
										FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
												JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
												WHERE acd.Approval = t.ApprovalID AND acd.Phase = a.CurrentPhase
												ORDER BY ad.[Priority] ASC
											) appcd
									)
								ELSE
								(							    
									(SELECT CASE WHEN ( not exists(SELECT acd.[ID] FROM ApprovalCollaboratorDecision acd
												                   WHERE acd.Approval = a.ID AND acd.Decision IS NULL AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)))
									 THEN
										(SELECT TOP 1 appcd.[Key]
											FROM (	SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
													JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
													WHERE acd.Approval = t.ApprovalID AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL)
													ORDER BY ad.[Priority] ASC
												) appcd
									     )
									 ELSE '0'
									 END 
									 )   			   
								)
								END
							)								
						)		
					WHEN (ISNULL(a.PrimaryDecisionMaker, 0) != 0)
					  THEN
						(SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.Collaborator = a.PrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					ELSE
					 (SELECT TOP 1 appcd.[Key]
							FROM (SELECT TOP 1 ad.[Key] FROM ApprovalCollaboratorDecision acd
									JOIN dbo.ApprovalDecision ad ON acd.Decision = ad.ID 
								WHERE acd.Approval = t.ApprovalID AND acd.ExternalCollaborator = a.ExternalPrimaryDecisionMaker AND (acd.Phase = a.CurrentPhase OR acd.Phase IS NULL) ORDER BY ad.[Priority] ASC) appcd
						)
					END	
			), 0 ) AS Decision,
			(SELECT CASE WHEN EXISTS(SELECT TOP 1 ac.ID 
									FROM ApprovalCollaborator ac 
									WHERE ac.Approval = t.ApprovalID and ac.Collaborator = @P_User AND (ac.Phase = a.CurrentPhase OR ac.Phase IS NULL))
						 THEN CONVERT(bit,1)
					     ELSE CONVERT(bit,0)
			END) AS IsCollaborator,
			CONVERT(bit,0) AS AllCollaboratorsDecisionRequired,			
			(SELECT CASE 
					WHEN a.LockProofWhenAllDecisionsMade = 1
						THEN (SELECT [dbo].[GetApprovalApprovedDate] (t.ApprovalID, ISNULL(a.PrimaryDecisionMaker, 0), ISNULL(a.ExternalPrimaryDecisionMaker, 0)))
					ELSE
						NULL
					END	
			) as ApprovalApprovedDate,
			ISNULL(a.CurrentPhase, 0) AS CurrentPhase,
			t.PhaseName,
			ISNULL((SELECT CASE
						WHEN (a.CurrentPhase IS NOT NULL) THEN (SELECT ajw.Name FROM ApprovalJobWorkflow ajw WHERE ajw.Job = a.Job)
					    ELSE ''
					END),'') AS WorkflowName,
			ISNULL((SELECT TOP 1 ap.Name       
					  FROM ApprovalJobPhase ap
					  CROSS APPLY (SELECT ApprovalJobWorkflow AS Workflow, aj.Position  FROM  ApprovalJobPhase aj WHERE aj.ID = a.CurrentPhase) AJP(Workflow, Position)
					  WHERE ap.ApprovalJobWorkflow = AJP.Workflow AND ap.Position > AJP.Position AND ap.IsActive = 1
					  ORDER BY ap.Position ),'') AS NextPhase,
			ISNULL([dbo].[GetApprovalPrimaryDecisionMakers] (a.CurrentPhase, t.ApprovalID, a.PrimaryDecisionMaker, a.ExternalPrimaryDecisionMaker), '') AS DecisionMakers,
			ISNULL((SELECT DecisionType FROM dbo.ApprovalJobPhase WHERE ID = a.CurrentPhase), 0) AS DecisionType,
			(SELECT CASE WHEN a.CurrentPhase IS NOT NULL
						 THEN (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = j.[JobOwner])
						 ELSE (SELECT GivenName + ' ' + FamilyName FROM [User] WHERE ID = a.[Owner])
						 END) AS OwnerName,
			a.AllowOtherUsersToBeAdded,
			j.JobOwner,
			(SELECT 
				CASE 
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0 AND (SELECT TOP 1 ajpa.PhaseMarkedAsCompleted FROM ApprovalJobPhaseApproval AS ajpa WHERE ajpa.Phase = a.CurrentPhase AND ajpa.Approval = t.ApprovalID) <> 0)
						THEN CONVERT(BIT, 1)
					WHEN (ISNULL(a.CurrentPhase, 0) <> 0)
						THEN (SELECT CASE  WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0 AND ISNULL(a.ExternalPrimaryDecisionMaker, 0) = 0 )
											THEN 
												CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														   JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID) = (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd  WHERE acd.Phase = a.CurrentPhase AND acd.Approval = t.ApprovalID)											
													THEN CONVERT(BIT, 1) 
													ELSE CONVERT(BIT, 0) 
												END
											ELSE
											   CASE WHEN (SELECT COUNT(acd.ID) FROM ApprovalCollaboratorDecision acd 
														  JOIN ApprovalJobPhase ajp ON acd.Phase = ajp.ID 
														  WHERE acd.Phase = a.CurrentPhase AND acd.Decision = ajp.ApprovalTrigger AND acd.Approval = t.ApprovalID AND (a.PrimaryDecisionMaker = acd.Collaborator OR a.ExternalPrimaryDecisionMaker = acd.ExternalCollaborator)
														 ) > 0
													THEN CONVERT(BIT, 1)
													ELSE CONVERT(BIT, 0) 
												END
											END
								)
						ELSE CONVERT(BIT, 0)
						END
				) AS IsPhaseComplete,
				ISNULL(a.[VersionSufix], '') as VersionSufix,
				a.IsLocked,				
				(SELECT CASE
					WHEN ISNULL(a.PrimaryDecisionMaker,0) = 0
					THEN (SELECT CASE 
							WHEN ISNULL(a.ExternalPrimaryDecisionMaker,0) = 0
							THEN CONVERT(BIT, 0) 
							ELSE CONVERT(BIT, 1) 
						 END)
					ELSE CONVERT(BIT, 0)
					END)  AS IsExternalPrimaryDecisionMaker
			FROM	Job j
					INNER JOIN Approval a 
						ON a.Job = j.ID
					INNER JOIN dbo.ApprovalType at
						ON 	at.ID = a.[Type]
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		 							
					JOIN @TempItems t 
						ON t.ApprovalID = a.ID
			WHERE t.ID >= @StartOffset	AND t.IsApproval = 1
			UNION				
			SELECT  0 AS ID,
					0 AS Approval, 
					f.ID AS Folder,
					f.Name AS Title,
					'' AS [Guid],
					'' AS [FileName],
					'' AS [Status],
					0 AS Job,
					0 AS [Version],
					0 AS Progress,						
					f.CreatedDate,
					f.ModifiedDate,
					GetDate() AS Deadline,
					f.Creator,
					0 AS [Owner],
					0 AS PrimaryDecisionMaker,
					'' AS PrimaryDecisionMakerName,
					'false' AS AllowDownloadOriginal,
					f.IsDeleted,
					0 AS ApprovalType,
					0 AS MaxVersion,
					(	SELECT COUNT(f1.ID)
            			FROM Folder f1
            			WHERE f1.Parent = f.ID
					) AS SubFoldersCount,
					(	SELECT COUNT(fa.Approval)
            			FROM FolderApproval fa
            			JOIN Approval a ON fa.Approval = a.ID	
	                    WHERE fa.Folder = f.ID AND a.DeletePermanently = 0
					) AS ApprovalsCount,				
					CONVERT(bit,0) AS HasAnnotations,
					'' AS Decision,
					CONVERT(bit, 1) AS IsCollaborator,
					f.AllCollaboratorsDecisionRequired,
					NULL as ApprovalApprovedDate,
					0 AS CurrentPhase,
					'' AS PhaseName,
					'' AS WorkflowName,
					'' AS NextPhase,
					'' AS DecisionMakers,
					0 AS DecisionType,
					'' AS OwnerName,
					CONVERT(bit, 0) AS AllowOtherUsersToBeAdded,			
					NULL AS JobOwner,
					CONVERT(bit, 0) AS IsPhaseComplete,
					'' AS VersionSufix,
					CONVERT(bit, 0) AS IsLocked,
					CONVERT(bit, 0) AS IsExternalPrimaryDecisionMaker
			FROM	Folder f					
					JOIN @TempItems t 
						ON t.ApprovalID = f.ID
			WHERE t.ID >= @StartOffset AND t.IsApproval = 0					
		) result
	ORDER BY result.ID
	
	SET ROWCOUNT 0

	---- Legacy parameter that calculated total number of approvals , now it is returned by approval counts procedure ---- 	
	SET @P_RecCount = 0
	SET @retry = 0;
	END TRY
    BEGIN CATCH 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();
			-- Check error number.
			-- If deadlock victim error,
			-- then reduce retry count
			-- for next update retry. 
			-- If some other error
			-- occurred, then exit
			-- retry WHILE loop.
			SET @retry = @retry - 1;
			IF (ERROR_NUMBER() <> 1205)	
			RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
    END CATCH;
	END; -- End WHILE loop

END

GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserLogin]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPC_ReturnUserLogin]
    (
      @P_Account INT ,
      @P_Username VARCHAR(255) ,
      @P_Password VARCHAR(255)
    )
AS 
    BEGIN
        SELECT	DISTINCT
                u.*
        FROM    [dbo].[User] u
                JOIN [dbo].[UserStatus] us ON u.[Status] = us.ID
        WHERE   u.[Account] = @P_Account
                AND u.[Username] = @P_Username
                AND u.[Password] = CONVERT(VARCHAR(255), HashBytes('SHA1',
                                                              @P_Password))
                AND ( us.[Key] = 'A'
                      OR us.[Key] = 'I'
                    )
    END
GO
/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserReportInfo]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- CZ-2970 Extend User Report
--- add AccountPath  for User Reports ------------------- 

CREATE PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
		WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)
	   -- Get the users				
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin,
		    (SELECT PathString) AS AccountPath,
		    (SELECT COUNT(app.ID) FROM [dbo].Approval AS app WHERE app.[Creator] = u.[ID]) AS CollaborateApprovalsNumber, 
		    (SELECT COUNT(dj.ID) FROM [dbo].DeliverJob AS dj WHERE dj.[Owner] = u.[ID]) AS DeliverJobsNumber		
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID		
		    INNER JOIN AccountTree act 
				ON a.ID = act.AccountID																						
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ((u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END


GO
/****** Object:  StoredProcedure [dbo].[up_updstats]    Script Date: 8/8/2018 11:35:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[up_updstats]
WITH EXECUTE AS 'dbo'
AS
EXEC sp_updatestats
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The unique identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The site name of the account in while labels' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SiteName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account Domain for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Domain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Owner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A unique string that can be used to identify the account in login-less situations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'First name of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactFirstName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last name of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactLastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone # of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email address of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the header logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'HeaderLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the login logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'LoginLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the email logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'EmailLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom color value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomColor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default currency type of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CurrencyFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default locale of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Locale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time zone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'TimeZone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date format of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'DateFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The time format of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'TimeFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Global Price Plan Discount for this accounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'GPPDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This account need subscription plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'NeedSubscriptionPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract period for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContractPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many child accounts this account can have?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ChildAccountsVolume'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom from address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomFromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom from name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomFromName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The reason this account was suspended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SuspendReason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suspended or deleted by parent?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsSuspendedOrDeletedByParent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The custom Domain for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomDomain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this account has coustom domain?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsCustomDomainActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hide the logo in the footer?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHideLogoInFooter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hide the logo in the login footer?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHideLogoInLoginFooter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is need profile management for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsNeedProfileManagement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Need PDFs to be color managed for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsNeedPDFsToBeColorManaged'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is remove all GMGCollaborate branding for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsRemoveAllGMGCollaborateBranding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Log enabled for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsEnabledLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User who created the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date the Account was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TheUser who last modified this Account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date the Account was last modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Help center is avaialble for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHelpCentreActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dealer number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'DealerNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The customer number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomerNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The VAT number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'VATNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is agreed to the terms and conditions in welcome page when activating the account?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsAgreedToTermsAndConditions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts region' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this account created temporaryly?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsTemporary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'LogMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousPlanAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous proof amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousProofsAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GPPDiscount of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'GPPDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current plan id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'CurrentPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'BaseAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account which this help center details are belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center email address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center contact number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center open days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'SupportDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center open hours' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'SupportHours'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide location' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide updated date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideUpdatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ChangedBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ChildAccount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account of the account setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the account setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the value of the account setting as a string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value''s data type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'ValueDataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A description of the Account Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the AccountType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the AccountType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Error Message of the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ErrorMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the uploaded file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file folder path' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FolderPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A uniquely identifying string that can be used to retrive the Material in login-less scenarios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Job ID of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Job'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version number of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this Approval was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who created this Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime by which this Approval should be completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Deadline'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who last modified the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the Approval was last modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to if this approval is deleted or not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsPageSpreads'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, Page spreads is et in Flex component' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lock the proof when all decisions are made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'LockProofWhenAllDecisionsMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The proof is in locked state' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsLocked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Only one decision is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'OnlyOneDecisionRequired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary decision maker, if only one decion is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'PrimaryDecisionMaker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allow users to download the original' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'AllowDownloadOriginal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FileType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file size' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Size'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Owner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Proof is in Error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsError'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Comment status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Parent Approval Comment of this Approval Comment if any' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval Page that contains this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Page'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Order number of the Annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'OrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Collaborator who made this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Collaborator who modify this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date that this comment has been modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The textual content of the Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TODO: remove this property' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ReferenceFilepath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this Approval Comment was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'AnnotatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of the comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CommentType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start index for text metric of annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'StartIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End index for text metric of annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'EndIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'HighlightType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crosshair X Coordinate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CrosshairXCoord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crosshair Y Coordinate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CrosshairYCoord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'HighlightData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this private annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'IsPrivate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The display name of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the comment markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval Comment that the markup belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The X ordinate of the origin of the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'X'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Y ordinate of the origin of the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Y'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Width if the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Width'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Height if the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Height'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The color of the text control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Color'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The size of the font' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Size'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The backgroundcolor of the text control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'BackgroundColor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rotaion value of the shape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Rotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shape information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Shape'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval collaborator role id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator assigned to this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval History record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval to which the History record belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Decision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The assigned user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time is assigend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time is opened' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'OpenedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time this decision has been completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'CompletedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The order to be shown on UIs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval to which this ApprovalPage belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page small thumbnail height' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageSmallThumbnailHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page small thumbnail width' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageSmallThumbnailWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page large thumbnail height' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageLargeThumbnailHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page large thumbnail width' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageLargeThumbnailWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resolution of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'DPI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Progress of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Progress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval page Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'Page'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval that user has been viewed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Viewed date and time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'ViewedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Attached Account Billing Plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account id this billing plan is assigned to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The billing plan of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The billing monthly plan new currency type value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'NewPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New value is based on the current rate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'IsAppliedCurrentRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The single proof price based on new currency type value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'NewProofPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User has been modified the proof price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'IsModifiedProofPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Proof count allow by this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Proofs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The price of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this fixed plan?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'IsFixed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The media service attached to this Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'MediaService'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enable pre press tools for this plan type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'EnablePrePressTools'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enable media tools for this Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'EnableMediaTools'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'AttachedAccountBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Colorspace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Colorspace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Comment Email Send Decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Comment Email Send Decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account to which the Company belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ABN/ACN or similar of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address1 of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address2 of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The city of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The postcode of eh Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Postcode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The state of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The phone number of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mobile number of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Mobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The email address of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Country of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Contract Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Contract Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Controller' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Controller'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Controller Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Parameeters of the Controller Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Parameters'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO2 code for the country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Iso2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO3 code for the Country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Iso3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO Country number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'IsoCountryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dialing prefix for the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'DialingPrefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Country''s name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Country''s short name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'ShortName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'true if the country has location data in the Location table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'HasLocationData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Symbol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Rate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The pattern of the date format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'Pattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sample result of the pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the EventGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the EventGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The EventGroup of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'EventGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account that this user belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The guid associated with this user that can be used instead of authentication under certain circumstances' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this user last authenticated with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'DateLoggedIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who modified this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the activeness of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The extention of the FileType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'Extention'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the FileType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Collaborator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The parent folder of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The creator of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The created date of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The modifier of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The modified date of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder is deleted?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this private folder?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsPrivate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Folder id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator assigned date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the global settings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dta type of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'DataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The value of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this highlight type will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The parent of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Title of this Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Status of this Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account associated with the Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The status modified date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Locale display name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The LogModule of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'LogModule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The action message with formatted info string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The log action id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'LogAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'ActualMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the LogModule' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The smoothing attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Smoothing'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The resolution attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Resolution'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The colorspace attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Colorspace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The image format attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page box attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'PageBox'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The tile image format attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'TileImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The jpeg compression attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'JPEGCompression'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'ControllerAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent of the Menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item position' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Position'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item is visible for Admin App?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsAdminAppOwned'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to weather manu item is aligned to left or right?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsAlignedLeft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is sub navigation?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsSubNav'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item is placed inside main or top level menu?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsTopNav'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item visible?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsVisible'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The title of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'MenuItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'AccountTypeRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the NotificationFrequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the NotificationFrequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Page Box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Page Box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the PrePress Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the PrePress Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Preset' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Preset' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the reserved domain name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The order to be shown on UIs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Role of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Preset of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The EventType of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'EventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this custom shape?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The FXG of the shape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'FXG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'SVG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external users role for this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The shared date of this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'SharedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can share the url with external user?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsSharedURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can share the download url with external user?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsSharedDownloadURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The this share expired?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsExpired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The expire date of this share' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notes of this share' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Smoothing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Smoothing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the studio Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user belongs to these setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notification frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Include my own activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'IncludeMyOwnActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When a comment is made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'WhenCommentMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Replies to my comments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'RepliesMyComment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New version added' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'NewVesionAdded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Final decisions made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'FinalDecisionMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disable or enable color correction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'EnableColorCorrection'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Studio Setting Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Studio Setting Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Tile Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Tile Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the time format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The pattern of the time format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Pattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sample result of the pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account that this user belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The username used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The password used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Photo of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'PhotoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The guid associated with this user that can be used instead of authentication under certain circumstances' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mobile telephone number number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'MobileTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The home telephone number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'HomeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The office telephone number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'OfficeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Notification frequency to send emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this user last authenticated with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'DateLastLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who last modified this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was last updated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preset level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Include my own activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IncludeMyOwnActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The RolePresetEventType of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'RolePresetEventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Value of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the UserGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account id of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modifier of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modified date id of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser', @level2type=N'COLUMN',@level2name=N'UserGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User to which this History pertains' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The company that this User belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The username used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The password used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is active and is able to authenticate with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'IsActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is deleted and is no longer available to the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who created this UserLogin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The IP address that the User logged in from' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'IpAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether or not the login attempt was successful' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'Success'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user attempted to login' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'DateLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user logged out (if any)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'DateLogout'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the DataType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the DataType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType'
GO

-- Changes for Encrypt ColorProof Instance Password In Database

IF NOT EXISTS (select * from sys.symmetric_keys where name like '%DatabaseMasterKey%') 
CREATE MASTER KEY ENCRYPTION BY 
PASSWORD = 'CoZone2W3llc0m'
 
IF NOT EXISTS ( SELECT * FROM sys.certificates WHERE name = 'ColorProofInstances' )
CREATE CERTIFICATE ColorProofInstances
WITH SUBJECT = 'ColorProofInstances';
GO

IF NOT EXISTS (SELECT * FROM sys.symmetric_keys WHERE [name] =  'ColorProofInstances')
CREATE SYMMETRIC KEY ColorProofInstances
WITH ALGORITHM = AES_256
ENCRYPTION BY CERTIFICATE ColorProofInstances;
GO 

-- create a temp table with the Wellcom records from NotificationItem 
CREATE TABLE [dbo].[NotificationItem_Wellcom](
	[ID] [int] NOT NULL,
	[InternalRecipient] [int] NULL,
	[NotificationType] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Creator] [int] NULL,
	[CustomFields] [nvarchar](max) NULL,
	[PSSessionID] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	CONSTRAINT [PK_NotificationItem_Wellcom] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
