-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze-all-plus-wellcom" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]

print 'START DROP QualityControl, Container, Manage tables and related records'

begin tran

	print 'start dropping tables for Manage/ProfileServer/QualityControl'
	alter table [dbo].[AccountSubscriptionPlan] drop constraint DF_AccountSubscriptionPlan_IsManageAPI
	alter table [dbo].[AccountSubscriptionPlan] drop column [IsManageAPI] 
	alter table [dbo].[FTPPresetJobDownload] drop constraint [FK_FTPPresetJobDownload_ManageJob] 
	alter table [dbo].[FTPPresetJobDownload] drop column [ManageJob] 
	alter table [dbo].[SubscriberPlanInfo] drop constraint DF_SubscriberPlanInfo_IsManageAPI
	alter table [dbo].[SubscriberPlanInfo] drop column [IsManageAPI] 
	alter table [dbo].[UploadEnginePreset] drop column [IsEnabledInManage] 
	alter table [dbo].[UploadEnginePreset] drop column [MakeDefaultForManage]
	alter table [dbo].[UserGroup] drop constraint DF__UserGroup__IsPro__6B44E613 	
	alter table [dbo].[UserGroup] drop column [IsProfileServerOwnershipGroup] 

	drop table ColorProofContainer
	drop table ContainerAccount
	drop table ContainerClientStatus
	drop table ContainerDistributionGroupColorProofInstance
	drop table ContainerDistributionGroup
	drop table ContainerFile
	drop table ContainerGroupContainer
	drop table ContainerGroup
	drop table ContainerSchedule
	drop table ContainerVersionOrganization
	drop table ContainerVersion
	drop table Container
	drop table ManageAPISession
	drop table ManageJobDeleteHistory
	drop table ManageJob
	drop table ManagePreflight
	drop table ManageOutputColorSpace
	drop table ManageConversion
	drop table QualityControlDataCaptureMeasurement
	drop table QualityControlDataCaptureDetails
	drop table QualityControlInkDensityTolerance
	drop table QualityControlInkDensity
	drop table QualityControlPaper
	drop table QualityControlPrintProcess
	drop table QualityControlProduct
	drop table QualityControlSection
	drop table QualityControlVersion
	drop table QualityControlSettings

	drop procedure SPC_GetScheduleGroups
	drop procedure SPC_ReturnInkDensityToleranceReport
	drop procedure SPC_ReturnApprovalsPageInfo_revert

	print 'start deleting all FK references where AppModuleId=6 (ProfileServer)'
    delete ur
	from UserRole ur
	inner join [Role] r on r.id=ur.role
	inner join appmodule am on am.id=r.AppModule
	where am.[key]='5' --profileserver

    delete rpet
	from RolePresetEventType rpet
	inner join [Role] r on r.id=rpet.role
	inner join appmodule am on am.id=r.AppModule
	where am.[key]='5' --profileserver

    delete miatr
	from MenuItemAccountTypeRole miatr
	inner join AccountTypeRole atr on atr.id=miatr.accounttyperole
	inner join [Role] r on r.id=atr.role
	inner join appmodule am on am.id=r.AppModule
	where am.[key]='5' --profileserver
	
	delete atr
	from AccountTypeRole atr
	inner join [Role] r on r.id=atr.role
	inner join appmodule am on am.id=r.AppModule
	where am.[key]='5' --profileserver
	
	delete r
	from  [Role] r 
	inner join appmodule am on am.id=r.appmodule
	where am.[key]='5' --profileserver

	delete appmodule where [key]='5' --profileserver

	print 'start deleting Manage/ProfileServer/QualityControl from MenuItems where AppModuleId=4 (Admin)'
	delete miatr
	from MenuItemAccountTypeRole miatr
	inner join MenuItem mi on mi.id=miatr.menuitem
	inner join ControllerAction ca on ca.id=mi.controlleraction 
	where ca.controller in ('Manage', 'ProfileServer', 'QualityControl')

	delete mi
	from MenuItem mi
	inner join ControllerAction ca on ca.id=mi.controlleraction 
	where ca.controller in ('Manage', 'ProfileServer', 'QualityControl')

	delete ControllerAction where controller in ('Manage', 'ProfileServer', 'QualityControl')

	print 'start deleting Manage from EventGroup'
	delete anpet
	from AccountNotificationPresetEventType anpet
	inner join EventType et on et.id=anpet.eventtype
	inner join EventGroup eg on eg.id=et.eventgroup
	where eg.[key] in ('MN', 'DC')

	delete et
	From EventType et
	inner join EventGroup eg on eg.id=et.eventgroup
	where eg.[key] in ('MN', 'DC')

	delete [EventGroup] where [key] in ('MN', 'DC')

	print 'start deleting other records'
	delete [ReservedDomainName] where name='containerserver'

	delete ueugp
	from UploadEngineUserGroupPermissions ueugp
	inner join [UploadEnginePermission] uep on uep.id=ueugp.UploadEnginePermission
	where [key] in (2, 16)

	delete [UploadEnginePermission] where [key] in (2, 16)

	delete menuitem where [key] in ('PSVI', 'QCAS') -- direct menus: profile server, qualitycontrol
	
commit

print 'END DROP QualityControl, Container, Manage tables and related records'

print 'START ALTER PROCEDURES AND VIEWS:'

GO

-----------------------------------------------------------------------------------
-- Alter Return Account Parameters view
alter VIEW [dbo].[ReturnAccountParametersView] 
AS 
SELECT  0 AS AccountID,
'' AS AccountName,
0 AS AccountTypeID,
'' AS AccountTypeName,
0 AS CollaborateBillingPlanId,
'' AS CollborateBillingPlanName,
0 AS DeliverBillingPlanId,
'' AS DeliverBillingPlanName,
0 AS AccountStatusID,
'' AS AccountStatusName,
0 AS LocationID,
'' AS LocationName
GO


--alter billing report view
alter VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
SELECT  0 AS ID,
'' AS AccountName,
'' AS AccountTypeName,
'' AS CollaborateBillingPlanName,
0 AS CollaborateProofs,
'' AS CollaborateBillingCycleDetails,
0 AS CollaborateBillingPlan,
CONVERT(BIT, 0) AS CollaborateIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS CollaborateContractStartDate,
CONVERT(BIT, 0) as CollaborateIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [CollaborateIsQuotaAllocationEnabled],  
'' AS DeliverBillingPlanName,
0 AS DeliverProofs,
'' AS DeliverBillingCycleDetails,
0 AS DeliverBillingPlan,
CONVERT(BIT, 0) AS DeliverIsFixedPlan,
CONVERT(DATETIME, GETDATE()) AS DeliverContractStartDate,
CONVERT(BIT, 0) AS DeliverIsMonthlyBillingFrequency,
CONVERT(BIT, 0) as [DeliverIsQuotaAllocationEnabled],
0.0 AS GPPDiscount,
0 AS Parent,
'' AS AccountPath
GO

--Alter UserRolesView
alter VIEW [dbo].[UserRolesView] AS
SELECT  U.ID ,
        U.GivenName ,
        u.FamilyName ,
        u.Username ,
        U.EmailAddress ,
        COALESCE(CR.[Key], 'NON') AS [CollaborateRole] ,
        COALESCE(DR.[Key], 'NON') AS [DeliverRole] ,
        COALESCE(AR.[Key], 'NON') AS [AdminRole] ,
        COALESCE(SR.[Key], 'NON') AS [SysAdminRole] ,
        u.DateLastLogin ,
        u.Account ,
        US.[Key] AS [UserStatus]
FROM    dbo.[User] AS U
        INNER JOIN dbo.UserStatus US ON U.Status = US.ID
        LEFT JOIN ( SELECT  CUR.[User] ,
                            CR.[Key]
                    FROM    dbo.UserRole CUR
                            INNER JOIN dbo.Role CR ON CUR.Role = CR.ID
                            INNER JOIN dbo.AppModule CAM ON CR.AppModule = CAM.ID
                                                            AND CAM.[Key] = 0
                  ) AS CR ON U.ID = CR.[User]
        LEFT JOIN ( SELECT  DUR.[User] ,
                            DR.[Key]
                    FROM    dbo.UserRole DUR
                            INNER JOIN dbo.Role DR ON DUR.Role = DR.ID
                            INNER JOIN dbo.AppModule DAM ON DR.AppModule = DAM.ID
                                                            AND DAM.[Key] = 2
                  ) AS DR ON U.ID = DR.[User]
        LEFT JOIN ( SELECT  AUR.[User] ,
                            AR.[Key]
                    FROM    dbo.UserRole AUR
                            INNER JOIN dbo.Role AR ON AUR.Role = AR.ID
                            INNER JOIN dbo.AppModule AAM ON AR.AppModule = AAM.ID
                                                            AND AAM.[Key] = 3
                  ) AS AR ON U.ID = AR.[User]
        LEFT JOIN ( SELECT  SUR.[User] ,
                            SR.[Key]
                    FROM    dbo.UserRole SUR
                            INNER JOIN dbo.Role SR ON SUR.Role = SR.ID
                            INNER JOIN dbo.AppModule SAM ON SR.AppModule = SAM.ID
                                                            AND SAM.[Key] = 4
                  ) AS SR ON U.ID = SR.[User]

GO


---- remove the logic related with EnableSoftProofingTools column (related to CZ-2073) ------
alter PROCEDURE [dbo].[SPC_GetSecondaryNavigationMenuItems]
    (
      @P_userId INT ,
      @P_accountId INT ,
      @P_ParentMenuId INT
    )
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @userRoles AS TABLE ( RoleId INT )
        DECLARE @accountTypeId INT ;
        DECLARE @ignoreMenuKeys AS TABLE ( MenuKey VARCHAR(4) ) 

        INSERT  INTO @userRoles
                ( RoleId 

                )
                SELECT  ur.Role
                FROM    dbo.UserRole ur
                WHERE   ur.[User] = @P_userId

        SELECT  @accountTypeId = acc.AccountType
        FROM    dbo.Account acc
        WHERE   acc.ID = @P_accountId

        DECLARE @Count INT;		

		WITH TempTable (ResultCount)
		AS
		( SELECT  count(1) as ResultCount
				  FROM      dbo.Account acc
							INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
							INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))				
				  UNION ALL
				  SELECT    count(1) as ResultCount
				  FROM      dbo.Account acc
							INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
							INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
							INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
							INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
				  WHERE     am.[Key] = 0 -- collaborate
							AND acc.ID = @P_accountId
							AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))				 
		)

		SELECT @Count = sum(ResultCount) FROM TempTable 

	    DECLARE @enableCollaborateSettings BIT = (SELECT CASE WHEN @Count > 0
														THEN CONVERT(BIT, 1)
														ELSE CONVERT(BIT, 0)
                                                    END
                                                  )

        --Remove Profile tab for users that don't have access to Admin Module        
        DECLARE @enableProfileMenu BIT = ( SELECT   CASE WHEN ( SELECT
                                                              COUNT(rl.ID)
                                                              FROM
                                                              dbo.UserRole ur
                                                              JOIN dbo.Role rl ON ur.Role = rl.ID
                                                              JOIN dbo.AppModule apm ON rl.AppModule = apm.ID
                                                              WHERE
                                                              ur.[User] = @P_userId
                                                              AND (apm.[Key] = 3 OR apm.[Key] = 4)
                                                              AND (RL.[Key] = 'ADM' OR rl.[Key] = 'HQM' OR rl.[Key] = 'HQA')
                                                              ) > 0
                                                         THEN CONVERT(BIT, 1)
                                                         ELSE CONVERT(BIT, 0)
                                                    END
                                         )

        DECLARE @enableDeliverSettings BIT = ( SELECT   CASE WHEN ( ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.AccountSubscriptionPlan asp ON acc.ID = asp.Account
                                                              INNER JOIN dbo.BillingPlan bp ON asp.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, asp.AccountPlanActivationDate, GETDATE()) < 30))
                                                              )
                                                              + ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              dbo.Account acc
                                                              INNER JOIN dbo.SubscriberPlanInfo spi ON acc.ID = spi.Account
                                                              INNER JOIN dbo.BillingPlan bp ON spi.SelectedBillingPlan = bp.ID
                                                              INNER JOIN dbo.BillingPlanType bpt ON bp.Type = bpt.ID
                                                              INNER JOIN dbo.AppModule am ON bpt.AppModule = am.ID
                                                              WHERE
                                                              am.[Key] = 2 -- deliver
                                                              AND acc.ID = @P_accountId
                                                              AND (bp.IsDemo = 0 OR (bp.IsDemo = 1 AND DATEDIFF(day, spi.AccountPlanActivationDate, GETDATE()) < 30))
                                                              ) ) > 0
                                                             THEN CONVERT(BIT, 1)
                                                             ELSE CONVERT(BIT, 0)
                                                        END
                                             ) ;



        IF ( @enableDeliverSettings = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEDS'  -- MenuKey - varchar(4)
                          )

                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'PSVI'  -- MenuKey - varchar(4)
                          )
            END

        IF(@enableCollaborateSettings = 0)
			BEGIN
				 INSERT  INTO @ignoreMenuKeys
							( MenuKey )
				 VALUES  ( 'CLGS'  -- MenuKey - varchar(4)
							)
            END

       IF ( @enableProfileMenu = 0 ) 
            BEGIN
                INSERT  INTO @ignoreMenuKeys
                        ( MenuKey )
                VALUES  ( 'SEPF' --MenuKey - varchar(4)
                          )
            END   



        SET NOCOUNT OFF ;


        WITH    Hierarchy ( [Action], [Controller], [DisplayName], [Active], [Parent], [MenuId], [MenuKey], [Position], [AccountType], [Role], Level )
                  AS ( SELECT   Action ,
                                Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                Parent ,
                                MenuItem AS [MenuId] ,
                                [Key] AS [MenuKey] ,
                                Position ,
                                AccountType ,
                                Role ,
                                0 AS Level
                       FROM     dbo.UserMenuItemRoleView umirv
                       WHERE    ( umirv.Parent = @P_ParentMenuId
                                  AND IsVisible = 1
                                  AND IsTopNav = 0
                                  AND IsSubNav = 0
                                  AND IsAlignedLeft = 1
                                )
                                OR ( LEN(umirv.Action) = 0
                                     AND LEN(umirv.Controller) = 0
                                     AND IsVisible = 1
                                     AND IsTopNav = 0
                                     AND IsSubNav = 0
                                     AND IsAlignedLeft = 1
                                     AND umirv.MenuItem != @P_ParentMenuId
                                     AND AccountType = @accountTypeId
                                     AND ( ( Role IN ( SELECT UR.RoleId
                                                       FROM   @userRoles UR )
                                             AND AccountType = @accountTypeId
                                           )
                                           OR ( LEN(Action) = 0
                                                AND LEN(Controller) = 0
                                              )
                                         )
                                   )
                       UNION ALL
                       SELECT   SM.Action ,
                                SM.Controller ,
                                '' AS [DisplayName] ,
                                CONVERT(BIT, 0) AS [Active] ,
                                SM.Parent ,
                                SM.MenuItem AS [MenuId] ,
                                SM.[Key] AS [MenuKey] ,
                                SM.Position ,
                                SM.AccountType ,
                                SM.Role ,
                                Level + 1
                       FROM     dbo.UserMenuItemRoleView SM
                                INNER JOIN Hierarchy PM ON SM.Parent = PM.MenuId
                       WHERE    SM.IsVisible = 1
                                AND SM.IsTopNav = 0
                                AND SM.IsSubNav = 0
                                AND SM.IsAlignedLeft = 1
                                AND ( ( SM.Role IN ( SELECT UR.RoleId
                                                     FROM   @userRoles UR )
                                        AND SM.AccountType = @accountTypeId
                                      )
                                      OR ( LEN(SM.Action) = 0
                                           AND LEN(SM.Controller) = 0
                                         )
                                    )
                     )
            SELECT  DISTINCT
                    H.MenuId ,
                    H.Parent ,
                    H.Action ,
                    H.Controller ,
                    H.DisplayName ,
                    H.Active ,
                    H.MenuKey ,
                    H.Level ,
                    H.Position
            FROM    Hierarchy H
                    LEFT JOIN @ignoreMenuKeys IMK ON H.MenuKey = IMK.MenuKey
            WHERE   IMK.MenuKey IS NULL
            ORDER BY Level ;

    END

GO


--Return QualityControl Billing plan info
alter PROCEDURE [dbo].[SPC_ReturnAccountParameters] ( @P_LoggedAccount INT )
AS 
    BEGIN
        SET NOCOUNT ON
        SELECT  A.ID AS [AccountId] ,
                A.NAME AS [AccountName] ,
                AT.ID AS AccountTypeID ,
                AT.Name AS AccountTypeName ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.ID
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.ID
                         END, 0) AS [CollaborateBillingPlanId] ,
                COALESCE(CASE WHEN CASP.ID IS NOT NULL THEN CASP.Name
                              WHEN CSPI.ID IS NOT NULL THEN CSPI.NAme
                         END, '') AS [CollborateBillingPlanName] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.ID
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.ID
                         END, 0) AS [DeliverBillingPlanId] ,
                COALESCE(CASE WHEN DASP.ID IS NOT NULL THEN DASP.Name
                              WHEN DSPI.ID IS NOT NULL THEN DSPI.NAme
                         END, '') AS [DeliverBillingPlanName] ,
                AST.ID AS AccountStatusID ,
                ASt.Name AS AccountStatusName ,
                CTY.ID AS LocationID ,
                CTY.ShortName AS LocationName
        FROM    dbo.Account A
                INNER JOIN AccountType AT ON A.AccountType = AT.ID
                INNER JOIN AccountStatus AST ON A.[Status] = AST.ID
                INNER JOIN Company C ON C.Account = A.ID
                INNER JOIN Country CTY ON C.Country = CTY.ID
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     ASP.Account = A.ID
                            ) CASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.AccountSubscriptionPlan ASP
                                        INNER JOIN dbo.BillingPlan BP ON ASP.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     ASP.Account = A.ID
                            ) DASP
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 0 -- Collaborate
                              WHERE     SPI.Account = A.ID
                            ) CSPI
                OUTER APPLY ( SELECT    BP.ID ,
                                        BP.Name
                              FROM      dbo.SubscriberPlanInfo SPI
                                        INNER JOIN dbo.BillingPlan BP ON SPI.SelectedBillingPlan = BP.ID
                                        INNER JOIN dbo.BillingPlanType BPT ON Bp.Type = BPT.ID
                                        INNER JOIN dbo.AppModule AM ON AM.ID = BPT.AppModule
                                                              AND AM.[Key] = 2 -- Deliver
                              WHERE     SPI.Account = A.ID
                            ) DSPI
             WHERE ast.[Key] != 'D' AND a.Parent = @P_LoggedAccount AND a.IsTemporary = 0
    END
GO


 -- get all collaborators except those who have no access to Collaborate module
alter PROCEDURE [dbo].[SPC_ReturnAccountUserCollaboratorsAndGroups]
	-- Add the parameters for the stored procedure here
	@P_AccountID INT,
	@P_LoggedUserID INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT OFF;

	DECLARE @ApproverAndReviewer INT
	DECLARE @Reviewer INT
	DECLARE @ReadOnlyr INT
	DECLARE @Retoucher INT

	SELECT @ApproverAndReviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'ANR';
	SELECT @Reviewer = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RVW';
	SELECT @ReadOnlyr = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RDO';
	SELECT @Retoucher = acr.ID FROM dbo.ApprovalCollaboratorRole acr WHERE [Key] = 'RET';

	SELECT 
		CONVERT (bit, 0) AS IsGroup,
		CONVERT (bit, 0) AS IsExternal,
		CONVERT (bit, 0) AS IsChecked,
		u.ID AS ID,
		0 AS [Count],

		-- IDs concatenated as string
		COALESCE(
			(SELECT DISTINCT ' cfg' + convert(varchar, ug.ID)
				FROM dbo.UserGroup ug
				INNER JOIN dbo.UserGroupUser ugu ON ugu.[User] = u.ID 
							AND ugu.UserGroup = ug.ID
				FOR XML PATH('') ),
			'')
			AS Members,

        -- name as given mane + family name
		u.GivenName + ' ' + u.FamilyName
			AS Name,

		-- get user role for colaborate module
		(SELECT acr.ID
			FROM dbo.ApprovalCollaboratorRole acr
			WHERE acr.ID =  CASE r.Name					
								WHEN 'Administrator' THEN @ApproverAndReviewer
								WHEN 'Manager' THEN @ApproverAndReviewer
								WHEN 'Moderator' THEN @ApproverAndReviewer
								WHEN 'Contributor' THEN @Reviewer
								WHEN 'Retoucher' THEN @Retoucher
								ELSE @ReadOnlyr
							END)						
			AS [Role]
	FROM dbo.[User] u	
	INNER JOIN dbo.[UserRole] ur ON ur.[User] = u.ID
	INNER JOIN dbo.[Role] r ON ur.Role = r.ID
	WHERE Account = @P_AccountID 
			AND r.[Key] <> 'NON'
			AND r.AppModule = (SELECT am.ID 
							   FROM dbo.AppModule am
							   WHERE am.[Key] = 0)			
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'I')
			AND u.Status <> (SELECT us.ID FROM dbo.UserStatus us
								WHERE us.[Key] = 'D')

			AND ( 	(SELECT rl.[Key] 	
					FROM dbo.[User] u
					JOIN dbo.[UserRole] ur ON u.ID = ur.[User]
					JOIN dbo.[Role] rl ON ur.Role = rl.ID
					JOIN dbo.[AppModule] apm ON rl.AppModule = apm.ID
					WHERE apm.[Key] = 0 and u.ID = @P_LoggedUserID) != 'MOD'
					OR
					u.ID IN 
					(SELECT DISTINCT u.ID FROM dbo.[User] u
				     JOIN dbo.UserGroupUser ugu ON u.ID = ugu.[User]
                     JOIN dbo.UserGroupUser ug ON ugu.UserGroup = ug.UserGroup
                     JOIN dbo.UserGroup grp ON ug.UserGroup = grp.ID
                     WHERE ug.[User] = @P_LoggedUserID
                     UNION
                     SELECT @P_LoggedUserID)				
				)
END
GO


--alter billing report to return all accounts 
alter PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
    (
      @P_AccountTypeList NVARCHAR(MAX) = '' ,
      @P_AccountNameList NVARCHAR(MAX) = '' ,
      @P_LocationList NVARCHAR(MAX) = '' ,
      @P_BillingPlanList NVARCHAR(MAX) = '' ,
      @P_StartDate DATETIME ,
      @P_EndDate DATETIME ,
      @P_LoggedAccount INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON    

         DECLARE @subAccounts TABLE ( ID INT )
        INSERT  INTO @subAccounts
                SELECT DISTINCT
                        a.ID
                FROM    [dbo].[Account] a
                        INNER JOIN Company c ON c.Account = a.ID
                        INNER JOIN dbo.AccountStatus ast ON a.[Status] = ast.ID 
                WHERE	a.ID IN (SELECT ID FROM dbo.[GetChildAccounts](@P_LoggedAccount))
						AND a.ID != 1
                        AND a.IsTemporary = 0
						AND (ast.[Key] = 'A')
                        AND ( @P_AccountTypeList = ''
                              OR a.AccountType IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountTypeList,
                                                        ',') )
                            )
                        AND ( @P_AccountNameList = ''
                              OR a.ID IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_AccountNameList,
                                                        ',') )
                            )
                        AND ( @P_LocationList = ''
                              OR c.Country IN (
                              SELECT    val
                              FROM      dbo.splitString(@P_LocationList, ',') )
                            )
                        AND ( @P_BillingPlanList = ''
                              OR ( @P_BillingPlanList != '' AND (
																 EXISTS (SELECT    val
																		 FROM      dbo.splitString(@P_BillingPlanList, ',')
																		 where val in (
																					   SELECT bp.SelectedBillingPlan from dbo.AccountSubscriptionPlan bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   UNION
																					   SELECT bp.SelectedBillingPlan from dbo.SubscriberPlanInfo bp
																					   where a.ID = bp.Account AND bp.ContractStartDate is not null
																					   )
																		)																 
																)
								 )
                            )

        DECLARE @AccountIDs AS AccountIDsTableType;  

		INSERT INTO @AccountIDs (ID)  
			SELECT ID
			FROM @subAccounts;

		DECLARE @billingCycles TABLE 
		(
			AccountID INT, 
			AppModule INT,
			SelectedBillingPlan INT,
			CycleAllowedProofsCount INT,
			BillingPlanPrice INT,
			Collaborate NVARCHAR(MAX),
			Deliver NVARCHAR(MAX)
		);

		INSERT INTO @billingCycles
		SELECT * FROM [dbo].[GetBillingCycles](@P_LoggedAccount, @P_StartDate, @P_EndDate, @AccountIDs)

        DECLARE @t TABLE
            (
              AccountId INT ,
              AccountName NVARCHAR(100) ,
              AccountType VARCHAR(10) ,
              AccountTypeName VARCHAR(100) ,
              CollaborateBillingPlanId INT ,
              CollaborateBillingPlanName NVARCHAR(100) ,
              CollaborateProofs INT ,
              CollaborateIsFixedPlan BIT ,
              CollaborateContractStartDate DATETIME,
              CollaborateIsMonthlyBillingFrequency BIT,              
              CollaborateCycleDetails NVARCHAR(MAX) ,
              CollaborateModuleId INT ,
              CollaborateIsQuotaAllocationEnabled BIT,
              DeliverBillingPlanId INT ,
              DeliverBillingPlanName NVARCHAR(100) ,
              DeliverProofs INT ,
              DeliverIsFixedPlan BIT ,
              DeliverContractStartDate DATETIME,
              DeliverIsMonthlyBillingFrequency BIT,
              DeliverCycleDetails NVARCHAR(MAX) ,
              DeliverModuleId INT ,
              DeliverIsQuotaAllocationEnabled BIT
            )
        INSERT  INTO @t
                ( AccountId ,
                  AccountName ,
                  AccountType ,
                  AccountTypeName ,
                  CollaborateBillingPlanId ,
                  CollaborateBillingPlanName ,
                  CollaborateProofs ,
                  CollaborateIsFixedPlan ,
                  CollaborateContractStartDate,
                  CollaborateIsMonthlyBillingFrequency,
                  CollaborateCycleDetails ,
                  CollaborateModuleId ,
                  CollaborateIsQuotaAllocationEnabled,
                  DeliverBillingPlanId ,
                  DeliverBillingPlanName ,
                  DeliverProofs ,
                  DeliverIsFixedPlan ,
                  DeliverContractStartDate,
                  DeliverIsMonthlyBillingFrequency,
                  DeliverCycleDetails ,
                  DeliverModuleId ,
                  DeliverIsQuotaAllocationEnabled
			  )
                SELECT DISTINCT
                        A.ID AS [AccountId] ,
                        A.Name AS [AccountName] ,
                        AT.[Key] AS [AccountType] ,
                        AT.[Name] AS [AccountTypeName] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlan, 0)
                             ELSE ISNULL(CASPBP.BillingPlan, 0)
                        END AS CollaborateBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.BillingPlanName, '')
                             ELSE ISNULL(CASPBP.BillingPlanName, '')
                        END AS CollaborateBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.Proofs, '')
                             ELSE ISNULL(CASPBP.Proofs, '')
                        END AS CollaborateProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsFixed, 0)
                             ELSE ISNULL(CASPBP.IsFixed, 0)
                        END AS CollaborateIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.ContractStartDate
                             ELSE CASPBP.ContractStartDate
                        END AS CollaborateContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(CSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(CASPBP.IsMonthlyBillingFrequency, 0)
                        END AS CollaborateIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN CSPIBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Collaborate, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = CSPIBP.BillingPlan OR SelectedBillingPlan IS NULL))
                                    WHEN CASPBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Collaborate, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = CASPBP.BillingPlan OR SelectedBillingPlan IS NULL))
                               END, '0|0|0|0|0') AS CollaborateCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN CSPIBP.ModuleId
                             ELSE CASPBP.ModuleId
                        END AS [CollaborateModuleId] ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN CSPIBP.IsQuotaAllocationEnabled
                             ELSE CASPBP.IsQuotaAllocationEnabled
                        END AS [CollaborateIsQuotaAllocationEnabled],

                        CASE WHEN ( at.[Key] = 'SBSC' )

                             THEN ISNULL(DSPIBP.BillingPlan, 0)
                             ELSE ISNULL(DASPBP.BillingPlan, 0)
                        END AS DeliverBillingPlanID ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.BillingPlanName, '')
                             ELSE ISNULL(DASPBP.BillingPlanName, '')
                        END AS DeliverBillingPlanName ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.Proofs, '')
                             ELSE ISNULL(DASPBP.Proofs, '')
                        END AS DeliverProofs ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsFixed, 0)
                             ELSE ISNULL(DASPBP.IsFixed, 0)
                        END AS DeliverIsFixedPlan ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.ContractStartDate
                             ELSE DASPBP.ContractStartDate
                        END AS DeliverContractStartDate ,
                        CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN ISNULL(DSPIBP.IsMonthlyBillingFrequency, 0)
                             ELSE ISNULL(DASPBP.IsMonthlyBillingFrequency, 0)
                        END AS DeliverIsMonthlyBillingFrequency ,
                        ISNULL(CASE WHEN DASPBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Deliver, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = DASPBP.BillingPlan OR SelectedBillingPlan IS NULL))
                                    WHEN DSPIBP.ModuleId IS NOT NULL
                                    THEN (SELECT ISNULL(Deliver, '0|0|0|0|0') FROM @billingCycles WHERE AccountID = A.ID AND (SelectedBillingPlan = DSPIBP.BillingPlan OR SelectedBillingPlan IS NULL))
                               END, '0|0|0|0"0') AS DeliverCycleDetails ,
                        CASE WHEN ( AT.[KEY] = 'SBSC' ) THEN DSPIBP.ModuleId
                             ELSE DASPBP.ModuleId
                        END AS [DeliverModuleId],
                         CASE WHEN ( at.[Key] = 'SBSC' )
                             THEN DSPIBP.IsQuotaAllocationEnabled
                             ELSE DASPBP.IsQuotaAllocationEnabled
                        END AS [DeliverIsQuotaAllocationEnabled]






                FROM    dbo.Account A

                        OUTER APPLY ( SELECT    AT.[Key] ,
                                                AT.[Name]
                                      FROM      AccountType AT
                                      WHERE     A.AccountType = AT.ID
                                    ) AT ( [Key], [Name] )
                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 0 -- Collaborate 
                                      WHERE     ASP.Account = A.ID
                                    ) CASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency],[IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 0 -- Collaborate for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) CSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )

                        OUTER APPLY ( SELECT    ASPBP.ID AS [BillingPlan] ,
                                                ASPBP.Name AS [BillingPlanName] ,
                                                ASPAM.ID AS [ModuleId] ,
                                                ASPBP.Proofs AS [Proofs] ,
                                                ASPBP.IsFixed AS [IsFixed],
                                                ASP.ContractStartDate AS [ContractStartDate],
                                                ASP.[IsMonthlyBillingFrequency] AS [IsMonthlyBillingFrequency],
                                                CONVERT(bit, 0) AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.AccountSubscriptionPlan ASP
                                                INNER JOIN dbo.BillingPlan ASPBP ON ASPBP.ID = ASP.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType ASPBPT ON ASPBP.[Type] = ASPBPT.ID
                                                INNER JOIN dbo.AppModule ASPAM ON ASPAM.ID = ASPBPT.AppModule
                                                              AND ASPAM.[Key] = 2 -- Deliver
                                      WHERE     ASP.Account = A.ID
                                    ) DASPBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )
                        OUTER APPLY ( SELECT    SPIBP.ID AS [BillingPlan] ,
                                                SPIBP.Name AS [BillingPlanName] ,
                                                SPIAM.ID AS [ModuleId] ,
                                                SPIBP.Proofs AS [Proofs] ,
                                                SPIBP.IsFixed AS [IsFixed],
                                                SPI.ContractStartDate AS [ContractStartDate],
                                                CONVERT(bit, 1) AS [IsMonthlyBillingFrequency],
                                                SPI.IsQuotaAllocationEnabled AS [IsQuotaAllocationEnabled]
                                      FROM      dbo.SubscriberPlanInfo SPI
                                                INNER JOIN dbo.BillingPlan SPIBP ON SPIBP.ID = SPI.SelectedBillingPlan
                                                INNER JOIN dbo.BillingPlanType SPIBPT ON SPIBP.Type = SPIBPT.ID
                                                INNER JOIN dbo.AppModule SPIAM ON SPIAM.ID = SPIBPT.AppModule
                                                              AND SPIAM.[Key] = 2 -- Deliver for Subscriber
                                      WHERE     SPI.Account = A.ID
                                    ) DSPIBP ( [BillingPlan],
                                               [BillingPlanName], [ModuleId],
                                               [Proofs], [IsFixed],
                                               [ContractStartDate], [IsMonthlyBillingFrequency], [IsQuotaAllocationEnabled] )

                        WHERE  ( A.ID IN ( SELECT  *
									FROM    @subAccounts )
								)


     -- Get the accounts
        SET NOCOUNT OFF

        ;WITH AccountTree (AccountID, PathString )
		AS(		
			SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM dbo.Account WHERE ID= @P_LoggedAccount
			UNION ALL
			SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM dbo.Account INNER JOIN AccountTree ON dbo.Account.Parent = AccountTree.AccountID		
		)


        SELECT DISTINCT
                a.ID ,
                a.Name AS AccountName ,
                TBL.AccountTypeName AS AccountTypeName ,
                TBL.CollaborateBillingPlanName ,
                TBL.CollaborateProofs ,
                TBL.CollaborateCycleDetails AS [CollaborateBillingCycleDetails] ,
                TBL.CollaborateModuleId ,
                TBL.AccountType ,
                TBL.CollaborateBillingPlanID AS [CollaborateBillingPlan] ,
                TBL.CollaborateIsFixedPlan ,
                TBL.CollaborateContractStartDate,
				TBL.CollaborateIsMonthlyBillingFrequency,
				TBL.CollaborateIsQuotaAllocationEnabled, 
                TBL.DeliverBillingPlanName ,
                TBL.DeliverProofs ,
                TBL.DeliverCycleDetails AS [DeliverBillingCycleDetails] ,
                TBL.DeliverModuleId ,
                TBL.DeliverBillingPlanID AS [DeliverBillingPlan] ,
                TBL.DeliverIsFixedPlan ,
                TBL.DeliverContractStartDate,
                TBL.DeliverIsMonthlyBillingFrequency,
                TBL.DeliverIsQuotaAllocationEnabled,
                 ISNULL(( SELECT CASE WHEN a.Parent = 1 THEN a.GPPDiscount
                                     ELSE ( SELECT  GPPDiscount
                                            FROM    [dbo].[GetParentAccounts](a.ID)
                                            WHERE   Parent = @P_LoggedAccount
                                          )
                                END
                       ), 0.00) AS GPPDiscount ,
                a.Parent ,
				 (SELECT PathString) AS AccountPath
        FROM    Account a
                INNER JOIN @t TBL ON TBL.AccountId = a.ID	
                INNER JOIN AccountTree act ON a.ID = act.AccountID		
    END

GO



