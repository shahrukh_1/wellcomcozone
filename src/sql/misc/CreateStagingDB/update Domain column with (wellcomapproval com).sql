USE [GMGCoZone_PartiallyMigrated3]
GO

	-- update Account: domain column: replace 'XXXXdomainXXXX' with 'wellcomapproval.com'

	DECLARE @OldDomainname_GMG nvarchar(100) = 'wellcomapproval.com'
	DECLARE @NewDomainname_Wellcom nvarchar(100) = 'staging.wellcomapproval.com'

	update [GMGCoZone_PartiallyMigrated3].[dbo].Account set 
		domain=replace(Domain, @OldDomainname_GMG, @NewDomainname_Wellcom)

