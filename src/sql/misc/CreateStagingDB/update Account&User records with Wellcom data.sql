-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze-all-plus-wellcom" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]
GO

BEGIN TRAN

	print '===> START Update Account&User records with Wellcom data'

	DECLARE @NewHelpdesk_MailAddress nvarchar(100) = 'helpdesk@wellcomww.co.uk'
	DECLARE @NewMainUser_MailAddress nvarchar(100) = 'shaun.gray@wellcomww.com.au'
	DECLARE @NewMainUser_Username nvarchar(100) = 'shaun.gray'
	DECLARE @NewMainUser_FamilyName nvarchar(100) = 'Shaun'
	DECLARE @NewMainUser_LastName nvarchar(100) = 'Gray'
	DECLARE @NewMainUser_Password VARBINARY(100) = CONVERT(VARBINARY(100), CONVERT(NVARCHAR, (CONVERT([VARCHAR], 0x3B7C4A487AB798EB13FB0D0EDA5C07E40D7ED2EE)))) -- password is NVARCHAR in production
	DECLARE @NewRegion nvarchar(100) = 'ap-southeast-1'
	DECLARE @OldDomainname_GMG nvarchar(100) = 'gmgcozone.com' -- gmg domain name, DO NOT MODIFY THIS VARIABLE.
	DECLARE @NewDomainname_Wellcom nvarchar(100) = 'XXXXdomainXXXX' -- new domain name, DO NOT MODIFY THIS VARIABLE (the next script will replace the Domain with the final value)
	DECLARE @NAME_GMG nvarchar(100) = 'GMG'
	DECLARE @NAME_WELLCOM nvarchar(100) = 'Wellcom'
	DECLARE @EMPTYSTRING NVARCHAR = ''
	DECLARE @ZEROSTRING NVARCHAR = '0'

	-- update User table for UserID=1 and 11 (Isabel -> Shaun)
	update [GMGCoZone_PartiallyMigrated3].[dbo].[user] set
		Username=@NewMainUser_Username
		,GivenName=@NewMainUser_FamilyName
		,FamilyName=@NewMainUser_LastName
		,EmailAddress=@NewMainUser_MailAddress
		,[Password]=@NewMainUser_Password
		,[MobileTelephoneNumber]=NULL 
		,[HomeTelephoneNumber]=NULL
		,[OfficeTelephoneNumber]=@ZEROSTRING 
	WHERE ID IN (
		1
		,11
		,3983
		,3230
		,3982
		,3991
		,3994
		,4857
	)

	-- update User table for UserID=3982 to avoid collision with the same username in the same AccountID=1689 (shaun.gray -> shaun.gray1)
	update [GMGCoZone_PartiallyMigrated3].[dbo].[user] set Username=@NewMainUser_Username + '1'
	WHERE ID=3982

	-- update Account table for AccountID=1 and 8 (Head Office, GMG UK)
	update [GMGCoZone_PartiallyMigrated3].[dbo].Account set 
		Name=replace(SiteName, @NAME_GMG, @NAME_WELLCOM)
		,SiteName=replace(SiteName, @NAME_GMG, @NAME_WELLCOM)
		,ContactFirstName=@NewMainUser_FamilyName
		,ContactLastName=@NewMainUser_LastName
		,ContactEmailAddress=@NewMainUser_MailAddress
		,[ContactPhone]=@EMPTYSTRING
		,[CustomFromAddress]=NULL
		,[CustomFromName]=NULL
	where id in (1, 8)

	-- update Account: new domain, new region='ap-southeast-1'
	update [GMGCoZone_PartiallyMigrated3].[dbo].Account set 
		domain=replace(Domain, @OldDomainname_GMG, @NewDomainname_Wellcom)
		,Region=@NewRegion

  	-- update Account: set name and email for the accounts: Wellcom, WellcomApprovalStaging, WellcomApprovalStaging->Subsciber1
	update [GMGCoZone_PartiallyMigrated3].[dbo].Account set 
		ContactFirstName=@NewMainUser_FamilyName
		,ContactLastName=@NewMainUser_LastName
		,ContactEmailAddress=@NewMainUser_MailAddress
	where id in (1689, 1795, 1796)

	-- update Account and User: set email for some dummy accounts from Wellcom parent
	update [GMGCoZone_PartiallyMigrated3].[dbo].Account set 
		ContactEmailAddress=@NewMainUser_MailAddress
	where id in (2007, 2008, 2058, 2062, 2063)

	update [GMGCoZone_PartiallyMigrated3].[dbo].[user] set Username=@NewMainUser_Username + '1'
	where account in (2007, 2008, 2058, 2062, 2063)

	-- update Company: new mailaddress
	update [GMGCoZone_PartiallyMigrated3].[dbo].Company	set
		[Name]=replace([Name], @NAME_GMG, @NAME_WELLCOM)
		,[Number]=NULL
		,[Address1]='address'
		,[Address2]=NULL 
		,[City]='city' 
		,[Postcode]=@ZEROSTRING 
		,[State]=NULL 
		,[Phone]=@ZEROSTRING
		,[Mobile]=NULL 
		,[Email]=@NewMainUser_MailAddress
	WHERE Account IN (1, 8)

	--set emailaddress for helpdesk: helpdesk@wellcomww.co.uk
	insert into accounthelpcentre(account, contactemail) values (1, @NewHelpdesk_MailAddress)
	insert into accounthelpcentre(account, contactemail) values (8, @NewHelpdesk_MailAddress)
	update accounthelpcentre 
	set ContactEmail=@NewHelpdesk_MailAddress -- instead of marcus.wright@gmgcolor.com, nico@gmgcolor.com
	where account in (1689, 1795, 1796)
	update accountsetting 
	set value=@NewHelpdesk_MailAddress -- instead of local.help@gmgcozone.com
	where account is null and name='ContactSupportEmail' 

	print '===> Delete the temp account (UKDemos) from Level3 - delete also its linked records from ExternalCollaborator'
	DECLARE @AccountID_1394 INT=1394
	DECLARE @ExternalCollaborator_1054 INT=1054
	delete [GMGCoZone_PartiallyMigrated3].[dbo].Sharedapproval where externalcollaborator=@ExternalCollaborator_1054
	delete [GMGCoZone_PartiallyMigrated3].[dbo].ApprovalCollaboratorDecision	where externalcollaborator=@ExternalCollaborator_1054
	delete [GMGCoZone_PartiallyMigrated3].[dbo].Externalcollaborator	where id=@ExternalCollaborator_1054
	update [GMGCoZone_PartiallyMigrated3].[dbo].Account set [owner]=1 where id=@AccountID_1394
	delete [GMGCoZone_PartiallyMigrated3].[dbo].[user] where account=@AccountID_1394
	delete [GMGCoZone_PartiallyMigrated3].[dbo].Accounttheme where account=@AccountID_1394
	delete [GMGCoZone_PartiallyMigrated3].[dbo].Account where id=@AccountID_1394

	-- DEBUG: display Accounts and Users tables:
	select * from [GMGCoZone_PartiallyMigrated3].[dbo].[Account]
	select * from [GMGCoZone_PartiallyMigrated3].[dbo].[user]
	select * from [GMGCoZone_PartiallyMigrated3].dbo.company

	print '===> END Update Account&User records with Wellcom data'

COMMIT TRAN
