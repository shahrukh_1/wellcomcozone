-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]
GO

create PROCEDURE [dbo].[SPC_CopyAccountsAndUsers](
    @Accounts AS AccountIDsTableType READONLY,
	@Users AS AccountIDsTableType READONLY
)
AS
BEGIN
		-- THIS PROCEDURE COPIES ACCOUNT AND USER TABLES, ONLY THE RECORDS WITH THE IDS FROM THE PARAMETERS
		print 'START [SPC_CopyAccountsAndUsers] ' --+ convert(nvarchar,@AccountID)

		-------- LEVEL 8-------------------------------------------------------
		------------------------------------------------------------------------
		print '[PrinterCompany]'
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrinterCompany] ON;
		INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PrinterCompany]
			([ID]
			,[Account]
			,[Name]
			,[Type])
		SELECT * FROM [GMGCoZone].[dbo].[PrinterCompany]
		WHERE [Account] in (SELECT ID FROM @Accounts)
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrinterCompany] OFF;

		print '[BouncedEmail]'
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BouncedEmail] ON;
		INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BouncedEmail]
			([ID]
			,[Message]
			,[Email])
		SELECT x.* 
		FROM (
			SELECT b.* FROM [GMGCoZone].[dbo].[BouncedEmail] b
			INNER JOIN [GMGCoZone].[dbo].[User] u on u.BouncedEmailID=b.ID
			WHERE u.ID in (SELECT ID FROM @Users)
			UNION 
			SELECT b.* FROM [GMGCoZone].[dbo].[BouncedEmail] b
			INNER JOIN [GMGCoZone].[dbo].[ExternalCollaborator] e on e.BouncedEmailID=b.ID
			WHERE e.[Account] in (SELECT ID FROM @Accounts)
		) x
		LEFT JOIN [GMGCoZone_PartiallyMigrated3].[dbo].[BouncedEmail] b_exists on b_exists.ID = x.ID
		WHERE b_exists.ID IS NULL
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BouncedEmail] OFF;

		print '[Account]'
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.Account ON;
		INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Account] (
			[ID]
			,[Name]
			,[SiteName]
			,[Domain]
			,[AccountType]
			,[Parent]
			,[Owner]
			,[Guid]
			,[ContactFirstName]
			,[ContactLastName]
			,[ContactPhone]
			,[ContactEmailAddress]
			,[HeaderLogoPath]
			,[LoginLogoPath]
			,[EmailLogoPath]
			,[CustomColor]
			,[CurrencyFormat]
			,[Locale]
			,[TimeZone]
			,[DateFormat]
			,[TimeFormat]
			,[GPPDiscount]
			,[NeedSubscriptionPlan]
			,[ContractPeriod]
			,[ChildAccountsVolume]
			,[CustomFromAddress]
			,[CustomFromName]
			,[SuspendReason]
			,[IsSuspendedOrDeletedByParent]
			,[CustomDomain]
			,[IsCustomDomainActive]
			,[IsHideLogoInFooter]
			,[IsHideLogoInLoginFooter]
			,[IsNeedProfileManagement]
			,[IsNeedPDFsToBeColorManaged]
			,[IsRemoveAllGMGCollaborateBranding]
			,[Status]
			,[IsEnabledLog]
			,[Creator]
			,[CreatedDate]
			,[Modifier]
			,[ModifiedDate]
			,[IsHelpCentreActive]
			,[DealerNumber]
			,[CustomerNumber]
			,[VATNumber]
			,[IsAgreedToTermsAndConditions]
			,[Region]
			,[IsTemporary]
			,[DisableLandingPage]
			,[DashboardToShow]
			,[IsTrial]
			,[EnableNotificationsByDefault]
			,[UploadEngineSetings]
			,[FavIconPath]
			,[DeletedBy]
			,[DeletedDate])
		SELECT 
			[ID]
			,[Name]
			,[SiteName]
			,[Domain]
			,[AccountType]
			,[Parent]
			,[Owner]
			,[Guid]
			,[ContactFirstName]
			,[ContactLastName]
			,[ContactPhone]
			,[ContactEmailAddress]
			,[HeaderLogoPath]
			,[LoginLogoPath]
			,[EmailLogoPath]
			,[CustomColor]
			,[CurrencyFormat]
			,[Locale]
			,[TimeZone]
			,[DateFormat]
			,[TimeFormat]
			,[GPPDiscount]
			,[NeedSubscriptionPlan]
			,[ContractPeriod]
			,[ChildAccountsVolume]
			,[CustomFromAddress]
			,[CustomFromName]
			,[SuspendReason]
			,[IsSuspendedOrDeletedByParent]
			,[CustomDomain]
			,[IsCustomDomainActive]
			,[IsHideLogoInFooter]
			,[IsHideLogoInLoginFooter]
			,[IsNeedProfileManagement]
			,[IsNeedPDFsToBeColorManaged]
			,[IsRemoveAllGMGCollaborateBranding]
			,[Status]
			,[IsEnabledLog]
			,[Creator]
			,[CreatedDate]
			,[Modifier]
			,[ModifiedDate]
			,[IsHelpCentreActive]
			,[DealerNumber]
			,[CustomerNumber]
			,[VATNumber]
			,[IsAgreedToTermsAndConditions]
			,[Region]
			,[IsTemporary]
			,[DisableLandingPage]
			,[DashboardToShow]
			,[IsTrial]
			,[EnableNotificationsByDefault]
			,[UploadEngineSetings]
			,[FavIconPath]
			,[DeletedBy]
			,[DeletedDate]
		FROM [GMGCoZone].[dbo].[Account]
		WHERE ID in (SELECT ID FROM @Accounts)
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.Account OFF;

		------ LEVEL 7-------------------------------------------------------
		--------------------------------------------------------------------
		print '[User]'
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[User] ON;
		INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[User](
			[ID]
			,[Account]
			,[Status]
			,[Username]
			,[Password]
			,[GivenName]
			,[FamilyName]
			,[EmailAddress]
			,[PhotoPath]
			,[Guid]
			,[MobileTelephoneNumber]
			,[HomeTelephoneNumber]
			,[OfficeTelephoneNumber]
			,[NotificationFrequency]
			,[DateLastLogin]
			,[Creator]
			,[CreatedDate]
			,[Modifier]
			,[ModifiedDate]
			,[Preset]
			,[IncludeMyOwnActivity]
			,[NeedReLogin]
			,[ProofStudioColor]
			,[PrinterCompany]
			,[Locale]
			,[BouncedEmailID]
			,[IsSsoUser])
		SELECT * FROM [GMGCoZone].[dbo].[User] u
		WHERE ID in (SELECT ID FROM @Users)
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[User] OFF;

		print 'END [SPC_CopyAccountsAndUsers] ' --+ convert(nvarchar,@AccountID)

END
GO
