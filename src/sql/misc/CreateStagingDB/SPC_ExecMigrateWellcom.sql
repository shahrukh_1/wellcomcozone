-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]
GO

CREATE PROCEDURE [dbo].[SPC_ExecMigrateWellcom]
AS
BEGIN
	-- THIS PROCEDURE EXECUTES THE MIGRATION PROCEDURES FOR WELLCOM ACCOUNT
	print 'START [SPC_ExecMigrateWellcom]'
	
	print '===> set main AccountID and main UserID'
	DECLARE @MainMigratedHeadUserID INT = 1
	DECLARE @MainMigratedWellcomAccountID INT = 1795
	DECLARE @IncludeChildren INT = 1 -- setting to copy all hierarchy of accounts, including children
	DECLARE @EMPTYSTRING NVARCHAR = ''

	print '===> Level1: create the list of accounts for full copy, for wellcom accounts'
	DECLARE @Level1Accounts AccountIDsTableType
	DECLARE @Level1Users AccountIDsTableType
	DECLARE @Level1Accounts_OnlyTopParents AccountIDsTableType
	INSERT INTO @Level1Accounts_OnlyTopParents VALUES(@MainMigratedWellcomAccountID); --- Copy whole account hierarchy and data for Account 1689 (Wellcom)
	--INSERT INTO @Level1Accounts_OnlyTopParents VALUES(1795); --- Copy whole account hierarchy and data for Account 1795 (WellcomApprovalStaging): it has 404 jobs.  // not copying it gives error: ApprovalCollaboratorDecision.Collaborator not found in @users!
	INSERT INTO @Level1Accounts_OnlyTopParents VALUES(2229); --- Copy whole account hierarchy 2229 (testaccount) // it is used for verifying the migration.
	;WITH AccountTree (AccountID, PathString ) AS
	(		
		SELECT ID, CAST(Name as varchar(259)) AS PathString  FROM [GMGCoZone].dbo.Account a WHERE a.ID in (SELECT ID FROM @Level1Accounts_OnlyTopParents)
		UNION ALL
		SELECT ID, CAST(PathString+'/'+Name as varchar(259))AS PathString  FROM [GMGCoZone].dbo.Account a INNER JOIN AccountTree ON a.Parent = AccountTree.AccountID		
	)		
	INSERT INTO @Level1Accounts
	SELECT AccountID FROM AccountTree WHERE @IncludeChildren=1
	UNION SELECT ID FROM @Level1Accounts_OnlyTopParents WHERE @IncludeChildren<>1
	
	INSERT INTO @Level1Users
	SELECT ID FROM [GMGCoZone].[dbo].[User]
	WHERE Account in (SELECT ID FROM @Level1Accounts)

	print '===> Level2: create the list of accounts for partial copy, for head and uk accounts (copy only accounts, users, userroles, and a few more tables)'
	DECLARE @Level2Accounts AccountIDsTableType
	DECLARE @Level2Users AccountIDsTableType
	INSERT INTO @Level2Accounts VALUES(1);
	INSERT INTO @Level2Accounts VALUES(8);
	INSERT INTO @Level2Users VALUES(@MainMigratedHeadUserID);
	INSERT INTO @Level2Users VALUES(11);

	print '===> Level3: create the list of accounts for temp copy, for demo account (copy only accounts, users)'
	print '===> (this is needed because they are referenced by other FK, but in the end this temporary data will be deleted)'
	DECLARE @Level3Accounts AccountIDsTableType
	DECLARE @Level3Users AccountIDsTableType
	INSERT INTO @Level3Accounts VALUES(1394);
	INSERT INTO @Level3Users VALUES(3048); -- used in ExternalCollaborator.Owner, externalcollaborator is linked to SharedApproval: alex.crickmore from Account-1394(UKDemos)m child of Account-8.
	INSERT INTO @Level3Users VALUES(2029); -- used in Account.Owner: isabel.ukdemos from Account-1394(UKDemos)m child of Account-8.

	print '===> begin transaction'
	SET XACT_ABORT ON;  
	BEGIN TRAN
	BEGIN TRY
	
		print '===> CopyNomenclatures'
		exec [GMGCoZone_PartiallyMigrated3].dbo.[SPC_CopyNomenclatures]
	
		print '===> Disable constraints for account and user'
		ALTER TABLE [GMGCoZone_PartiallyMigrated3].[dbo].[User] NOCHECK CONSTRAINT ALL
		ALTER TABLE [GMGCoZone_PartiallyMigrated3].[dbo].[Account] NOCHECK CONSTRAINT ALL
		ALTER TABLE [GMGCoZone_PartiallyMigrated3].[dbo].[PrinterCompany] NOCHECK CONSTRAINT ALL

		print '===> Copy Level2 and Level3 - Accounts, Users, UserRoles and a few more tables'
		BEGIN
			print '[PrinterCompany]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrinterCompany] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PrinterCompany]
				([ID]
				,[Account]
				,[Name]
				,[Type])
			SELECT * FROM [GMGCoZone].[dbo].[PrinterCompany]
			WHERE [Account] in (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrinterCompany] OFF;

			print '[BouncedEmail]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BouncedEmail] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BouncedEmail]
				([ID]
				,[Message]
				,[Email])
			SELECT
				b.[ID]
				,@EMPTYSTRING [Message]
				,@EMPTYSTRING [Email]
			FROM [GMGCoZone].[dbo].[BouncedEmail] b
			INNER JOIN [GMGCoZone].[dbo].[User] u on u.BouncedEmailID=b.ID
			LEFT JOIN [GMGCoZone_PartiallyMigrated3].[dbo].[BouncedEmail] b_exists on b_exists.ID = b.ID
			WHERE u.ID in (SELECT ID FROM @Level2Users)
				and b_exists.ID IS NULL
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BouncedEmail] OFF;

			print '[Account]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.Account ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Account]
			   (
			   [ID]
			   ,[Name]
			   ,[SiteName]
			   ,[Domain]
			   ,[AccountType]
			   ,[Parent]
			   ,[Owner]
			   ,[Guid]
			   ,[ContactFirstName]
			   ,[ContactLastName]
			   ,[ContactPhone]
			   ,[ContactEmailAddress]
			   ,[HeaderLogoPath]
			   ,[LoginLogoPath]
			   ,[EmailLogoPath]
			   ,[CustomColor]
			   ,[CurrencyFormat]
			   ,[Locale]
			   ,[TimeZone]
			   ,[DateFormat]
			   ,[TimeFormat]
			   ,[GPPDiscount]
			   ,[NeedSubscriptionPlan]
			   ,[ContractPeriod]
			   ,[ChildAccountsVolume]
			   ,[CustomFromAddress]
			   ,[CustomFromName]
			   ,[SuspendReason]
			   ,[IsSuspendedOrDeletedByParent]
			   ,[CustomDomain]
			   ,[IsCustomDomainActive]
			   ,[IsHideLogoInFooter]
			   ,[IsHideLogoInLoginFooter]
			   ,[IsNeedProfileManagement]
			   ,[IsNeedPDFsToBeColorManaged]
			   ,[IsRemoveAllGMGCollaborateBranding]
			   ,[Status]
			   ,[IsEnabledLog]
			   ,[Creator]
			   ,[CreatedDate]
			   ,[Modifier]
			   ,[ModifiedDate]
			   ,[IsHelpCentreActive]
			   ,[DealerNumber]
			   ,[CustomerNumber]
			   ,[VATNumber]
			   ,[IsAgreedToTermsAndConditions]
			   ,[Region]
			   ,[IsTemporary]
			   ,[DisableLandingPage]
			   ,[DashboardToShow]
			   ,[IsTrial]
			   ,[EnableNotificationsByDefault]
			   ,[UploadEngineSetings]
			   ,[FavIconPath]
			   ,[DeletedBy]
			   ,[DeletedDate])
			SELECT *
			FROM [GMGCozone].dbo.Account
			WHERE ID in (SELECT ID FROM @Level2Accounts UNION SELECT ID FROM @Level3Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.Account OFF;

			print '[Account]: update Creator,Modifier'
			UPDATE Account SET 
				Owner=@MainMigratedHeadUserID 
				,[Creator]=@MainMigratedHeadUserID 
				,[Modifier]=@MainMigratedHeadUserID 
			WHERE ID in (SELECT ID FROM @Level2Accounts UNION SELECT ID FROM @Level3Accounts)

			print '[User]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[User] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[User](
				 [ID]
			  ,[Account]
			  ,[Status]
			  ,[Username]
			  ,[Password]
			  ,[GivenName]
			  ,[FamilyName]
			  ,[EmailAddress]
			  ,[PhotoPath]
			  ,[Guid]
			  ,[MobileTelephoneNumber]
			  ,[HomeTelephoneNumber]
			  ,[OfficeTelephoneNumber]
			  ,[NotificationFrequency]
			  ,[DateLastLogin]
			  ,[Creator]
			  ,[CreatedDate]
			  ,[Modifier]
			  ,[ModifiedDate]
			  ,[Preset]
			  ,[IncludeMyOwnActivity]
			  ,[NeedReLogin]
			  ,[ProofStudioColor]
			  ,[PrinterCompany]
			  ,[Locale]
			  ,[BouncedEmailID]
			  ,[IsSsoUser])
			SELECT *
			FROM [GMGCoZone].[dbo].[User] 
			WHERE ID in (SELECT ID FROM @Level2Users UNION SELECT ID FROM @Level3Users)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[User] OFF;

			print '[User]: update Creator,Modifier'
			UPDATE [User] SET 
				[Creator]=@MainMigratedHeadUserID 
				,[Modifier]=@MainMigratedHeadUserID 
			WHERE ID in (SELECT ID FROM @Level2Users UNION SELECT ID FROM @Level3Users)

			print '[AccountTheme]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountTheme] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTheme] (
					[ID]
				   ,[Account]
				   ,[Key]
				   ,[ThemeName]
				   ,[Active])
			SELECT act.*
			FROM [GMGCozone].[dbo].[AccountTheme] act
			WHERE act.Account IN (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTheme] OFF;	

			print '[ThemeColorScheme]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ThemeColorScheme] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ThemeColorScheme] (
					[ID]
				   ,[DefaultTheme]
				   ,[AccountTheme]
				   ,[BrandingPresetTheme]
				   ,[Name]
				   ,[THex]
				   ,[BHex])
		   			SELECT tcs.*
			FROM [GMGCozone].[dbo].[ThemeColorScheme] tcs
			INNER JOIN [GMGCozone].[dbo].[AccountTheme] at ON at.ID=tcs.AccountTheme
			WHERE at.Account IN (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[ThemeColorScheme] OFF;	

			print '[UserRole]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UserRole] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UserRole] (
					[ID]
				   ,[User]
				   ,[Role])
			SELECT *
			FROM [GMGCozone].[dbo].[UserRole]
			WHERE [User] IN (SELECT ID FROM @Level2Users)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[UserRole] OFF;	

			print '[Company]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Company] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Company] (
					[ID]
				   ,[Account]
				   ,[Name]
				   ,[Number]
				   ,[Address1]
				   ,[Address2]
				   ,[City]
				   ,[Postcode]
				   ,[State]
				   ,[Phone]
				   ,[Mobile]
				   ,[Email]
				   ,[Country]
				   ,[Guid])
			SELECT *
			FROM [GMGCozone].[dbo].[Company]
			WHERE Account IN (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[Company] OFF;	

			print '[AccountSubscriptionPlan]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountSubscriptionPlan] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountSubscriptionPlan] (
					[ID]
				   ,[SelectedBillingPlan]
				   ,[IsMonthlyBillingFrequency]
				   ,[ChargeCostPerProofIfExceeded]
				   ,[ContractStartDate]
				   ,[Account]
				   ,[AccountPlanActivationDate]
				   ,[AllowOverdraw])
			SELECT *
			FROM [GMGCozone].[dbo].[AccountSubscriptionPlan]
			WHERE Account IN (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountSubscriptionPlan] OFF;	

			print '[AttachedAccountBillingPlan]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AttachedAccountBillingPlan] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AttachedAccountBillingPlan] (
					[ID]
				   ,[Account]
				   ,[BillingPlan]
				   ,[NewPrice]
				   ,[IsAppliedCurrentRate]
				   ,[NewProofPrice]
				   ,[IsModifiedProofPrice])
			SELECT *
			FROM [GMGCozone].[dbo].[AttachedAccountBillingPlan]
			WHERE Account IN (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AttachedAccountBillingPlan] OFF;	

			print '[ChangedBillingPlan]'
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ChangedBillingPlan] ON;
			INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ChangedBillingPlan] (
					[ID]
				   ,[Account]
				   ,[BillingPlan]
				   ,[AttachedAccountBillingPlan])
			SELECT *
			FROM [GMGCozone].[dbo].[ChangedBillingPlan]
			WHERE Account IN (SELECT ID FROM @Level2Accounts)
			SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[ChangedBillingPlan] OFF;	
		END

		print '===> CopyAccountsAndUsers including children accounts'
		exec [GMGCoZone_PartiallyMigrated3].dbo.[SPC_CopyAccountsAndUsers] @Level1Accounts, @Level1Users

		print '===> Enable constraints for account and user'
		ALTER TABLE [GMGCoZone_PartiallyMigrated3].[dbo].[User] WITH CHECK CHECK CONSTRAINT ALL
		ALTER TABLE [GMGCoZone_PartiallyMigrated3].[dbo].[Account] WITH CHECK CHECK CONSTRAINT ALL
		ALTER TABLE [GMGCoZone_PartiallyMigrated3].[dbo].[PrinterCompany] WITH CHECK CHECK CONSTRAINT ALL

		print '===> CopyTablesRelatedToAccountAndUser'
		exec [GMGCoZone_PartiallyMigrated3].dbo.[SPC_CopyTablesRelatedToAccountsAndUsers] @Level1Accounts, @Level1Users

		print '[CurrencyRate]'
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[CurrencyRate] ON;
		INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[CurrencyRate]
			([ID]
			,[Currency]
			,[Rate]
			,[Creator]
			,[Modifier]
			,[CreatedDate]
			,[ModifiedDate])
		SELECT * 
		FROM [GMGCoZone].[dbo].[CurrencyRate]
		SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[CurrencyRate] OFF;
		
		------------------------------------------------------------------------------------
		print '===> START DEBUG:'

		declare @MigrattedTotalTables VARCHAR(100)
		declare @MigrattedTotalRecords VARCHAR(100)
		declare @MigrattedTablesList table
		(
			SchemaName nvarchar(128),
			TableName nvarchar(128),
			TotalRowCount bigint
		)
		;WITH MigrattedTablesList AS (
				SELECT SCHEMA_NAME(schema_id) AS [SchemaName],
					[Tables].name AS [TableName],
					SUM([Partitions].[rows]) AS [TotalRowCount]
					FROM [GMGCoZone_PartiallyMigrated3].sys.tables AS [Tables]
					JOIN [GMGCoZone_PartiallyMigrated3].sys.partitions AS [Partitions]
					ON [Tables].[object_id] = [Partitions].[object_id]
					AND [Partitions].index_id IN ( 0, 1 )
					GROUP BY SCHEMA_NAME(schema_id), [Tables].name
		)
		insert into @MigrattedTablesList
		SELECT * from MigrattedTablesList

		SELECT @MigrattedTotalTables=count(*), @MigrattedTotalRecords=sum(totalRowCount)
		FROM @MigrattedTablesList

		select * from @MigrattedTablesList

		select * From [GMGCoZone_PartiallyMigrated3].[dbo].account
		select * From [GMGCoZone_PartiallyMigrated3].[dbo].[user]	

		------------
		PRINT 'MigrattedTotalTables: ' + @MigrattedTotalTables + '. MigrattedTotalRecords: ' + @MigrattedTotalRecords + '.'

		DECLARE @CountReal VARCHAR(100)
		DECLARE @CountEstim VARCHAR(100)
		DECLARE @Msg VARCHAR(100)

		select @CountEstim='5', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone_PartiallyMigrated3].dbo.CurrencyRate
		PRINT 'CurrencyRate: ' + @Msg

		select @CountEstim='16', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone_PartiallyMigrated3].dbo.Shape where IsCustom=0
		PRINT 'Shape: ' + @Msg

		select @CountEstim='0', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone].dbo.UserGroup u1
		INNER JOIN [GMGCoZone_PartiallyMigrated3].dbo.UserGroup u2 on u1.ID=u2.ID
		where 0=1
		PRINT 'UserGroup with IsProfileServerOwnershipGroup: ' + @Msg

		select @CountEstim='60', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone_PartiallyMigrated3].dbo.Account WHERE ID<=2241
		PRINT 'Account WHERE ID<=2241: ' + @Msg

		select @CountEstim='0', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone_PartiallyMigrated3].dbo.Account WHERE ID>2241
		PRINT 'Account WHERE ID>2241: ' + @Msg

		select @CountEstim='1240', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone_PartiallyMigrated3].dbo.[User] WHERE ID<=7847
		PRINT 'User WHERE ID<=7847: ' + @Msg

		select @CountEstim='0', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		From [GMGCoZone_PartiallyMigrated3].dbo.[User] WHERE ID>7847
		PRINT 'User WHERE ID>7847: ' + @Msg

		select @CountEstim='40', @CountReal=count(*), @Msg=@CountReal + ' rows' + CASE WHEN @CountEstim=@CountReal THEN '.' ELSE ' ==> ERROR: EXPECTED RESULT IS ' + @CountEstim + ' rows!' END
		FROM [GMGCoZone_PartiallyMigrated3].[dbo].[ThemeColorScheme] tcs
		INNER JOIN [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTheme] at ON at.ID=tcs.AccountTheme
		WHERE at.Account IN (1, 8)
		PRINT 'ThemeColorScheme WHERE ID in (1, 8): ' + @Msg

		print '===> END DEBUG.'
		------------------------------------------------------------------------------------

		print '===> COMMIT'
		COMMIT TRAN

	END TRY
	BEGIN CATCH	
		print 'ERROR HANDLING: start'

		--- START DEBUG
		select * From [GMGCoZone_PartiallyMigrated3].[dbo].account
		select * From [GMGCoZone_PartiallyMigrated3].[dbo].[user]
		--- END DEBUG

		DECLARE @error int, @message varchar(4000), @xstate int;
		SELECT @error = ERROR_NUMBER(), @message = ERROR_MESSAGE(), @xstate = XACT_STATE();
		
		IF (XACT_STATE()) = -1  
			BEGIN 		
				print 'ERROR HANDLING: ROLLBACK'
				ROLLBACK TRANSACTION;  				
			END;  
		    
		IF (XACT_STATE()) = 1  
		BEGIN
			print 'ERROR HANDLING: COMMIT'
			COMMIT TRANSACTION;     
		END;  
	
        RAISERROR ('SPC_ExecMigrateWellcom: %d: %s', 16, 1, @error, @message) ;
		print 'ERROR HANDLING: end'
	END CATCH

	print 'END [SPC_ExecMigrateWellcom]'
END
GO
