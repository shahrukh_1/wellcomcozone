USE [GMGCoZone_PartiallyMigrated3]
GO

begin tran
 update account set creator=11, modifier=11, owner=11 where id between 1795 and 1799
 update [user] set creator=11, modifier=11 where account between 1795 and 1799
 delete ur from [userrole] ur inner join [user] u on ur.[user]=u.id where u.account between 1795 and 1799 and u.id not in (3983, 3984, 3992, 3991, 3999, 3997)
 delete us from [usersetting] us inner join [user] u on us.[user]=u.id where u.account between 1795 and 1799 and u.id not in (3983, 3984, 3992, 3991, 3999, 3997)
 delete u from [user] u where u.account between 1795 and 1799 and u.id not in (3983, 3984, 3992, 3991, 3999, 3997)
 delete company where account=1799
 delete accounthelpcentre where account=1799
 delete AccountSetting where account=1799
 delete t from ThemeColorScheme t inner join AccountTheme at on at.account=1799
 delete AccountTheme where account=1799
 delete SubscriberPlanInfo where account=1799
 delete account where id=1799
commit
