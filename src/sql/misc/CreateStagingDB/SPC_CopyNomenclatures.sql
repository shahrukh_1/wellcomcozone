-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]
GO

create PROCEDURE [dbo].[SPC_CopyNomenclatures]
AS
BEGIN
	-- THIS PROCEDURE COPIES THE NOMENCLATURES TABLES
	print 'START CopyNomenclatures'

	-- Needed for [Account]
	print '[AccountSTATUS]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountSTATUS] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountStatus]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[AccountStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountSTATUS] OFF;

	print '[AccountType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountType]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[AccountType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountType] OFF;
		
	print '[ContractPeriod]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ContractPeriod] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ContractPeriod]
        ([ID]
		,[Key]
        ,[Name])
	SELECT *
	FROM [GMGCoZone].[dbo].[ContractPeriod]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ContractPeriod] OFF;

	print '[Currency]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Currency] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Currency]
        ([ID]
        ,[Name]
		,[Code]
		,[Symbol])
	SELECT * 
	FROM [GMGCoZone].[dbo].[Currency]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Currency] OFF;

	print '[DateFormat]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DateFormat] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[DateFormat]
        ([ID]
        ,[Pattern]
		,[Result])
	SELECT * 
	FROM [GMGCoZone].[dbo].[DateFormat]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DateFormat] OFF;

	print '[Locale]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Locale] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Locale]
        ([ID]
        ,[Name]
		,[DisplayName])
	SELECT * 
	FROM [GMGCoZone].[dbo].[Locale]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Locale] OFF;

	print '[TimeFormat]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[TimeFormat] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[TimeFormat]
        ([ID]
		,[Key]
        ,[Pattern]
		,[Result])
	SELECT * 
	FROM [GMGCoZone].[dbo].[TimeFormat]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[TimeFormat] OFF;
		
	print '[UploadEngineDuplicateFileName]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineDuplicateFileName] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UploadEngineDuplicateFileName]
        ([ID]
		,[Key]
        ,[Name])
	SELECT *
	FROM [GMGCoZone].[dbo].[UploadEngineDuplicateFileName]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineDuplicateFileName] OFF;

	print '[UploadEngineVersionName]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineVersionName] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UploadEngineVersionName]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[UploadEngineVersionName]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineVersionName] OFF;
		
	print '[UploadEngineAccountSettings]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineAccountSettings] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UploadEngineAccountSettings]
        ([ID]
		,[DuplicateFileName]
        ,[VersionName]
        ,[VersionNumberPositionInName]
        ,[VersionNumberPositionFromStart]
        ,[AllowPDF]
        ,[AllowImage]
        ,[AllowVideo]
        ,[AllowSWF]
        ,[AllowZip]
        ,[ZipFilesWhenDownloading]
        ,[ApplyUploadSettingsForNewApproval]
        ,[UseUploadSettingsFromPreviousVersion]
        ,[EnableVersionMirroring]
        ,[PostStatusUpdatesURL])
	SELECT * 
	FROM [GMGCoZone].[dbo].[UploadEngineAccountSettings]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineAccountSettings] OFF;

	--- Needed for [User]
	print '[NotificationFrequency]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NotificationFrequency] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[NotificationFrequency]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[NotificationFrequency]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NotificationFrequency] OFF;

	print '[Preset]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Preset] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Preset]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[Preset]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Preset] OFF;
		
	print '[PrinterCompanyType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrinterCompanyType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PrinterCompanyType]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[PrinterCompanyType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrinterCompanyType] OFF;

	print '[ProofStudioUserColor]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ProofStudioUserColor] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ProofStudioUserColor]
        ([ID]
        ,[Name]
		,[HEXValue])
	SELECT * 
	FROM [GMGCoZone].[dbo].[ProofStudioUserColor]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ProofStudioUserColor] OFF;

	print '[UserStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UserStatus] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UserStatus]
        ([ID]
		,[Key]
        ,[Name])
	SELECT * 
	FROM [GMGCoZone].[dbo].[UserStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UserStatus] OFF;

	-- needed for ColorProofInstance
	print '[ColorProofInstanceState]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ColorProofInstanceState] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ColorProofInstanceState] (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[ColorProofInstanceState]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ColorProofInstanceState] OFF;

	-- needed for Job
	print '[JobStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[JobStatus] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[JobStatus] (
           [ID]
		   ,[Key]
           ,[Name]
		   ,[Priority])
	SELECT *
	FROM [GMGCozone].[dbo].[JobStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[JobStatus] OFF;

	-- needed for ApprovalJobPhase
	print '[ApprovalDecision]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalDecision] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ApprovalDecision] (
           [ID]
		   ,[Key]
           ,[Name]
		   ,[Priority]
		   ,[Description])
	SELECT *
	FROM [GMGCozone].[dbo].[ApprovalDecision]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalDecision] OFF;

	print '[DecisionType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DecisionType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[DecisionType] (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[DecisionType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DecisionType] OFF;

	print '[PhaseDeadlineTrigger]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PhaseDeadlineTrigger] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PhaseDeadlineTrigger] (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[PhaseDeadlineTrigger]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PhaseDeadlineTrigger] OFF;

	print '[PhaseDeadlineUnit]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PhaseDeadlineUnit] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PhaseDeadlineUnit] (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[PhaseDeadlineUnit]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PhaseDeadlineUnit] OFF;

	-- needed for Approval
	print '[ApprovalType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ApprovalType] (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[ApprovalType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalType] OFF;

	print '[FileType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[FileType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[FileType] (
           [ID]
		   ,[Extention]
           ,[Name]
		   ,[Type])
	SELECT *
	FROM [GMGCozone].[dbo].[FileType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[FileType] OFF;

	print '[JobSource]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.JobSource ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].JobSource (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].JobSource
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.JobSource OFF;

	-- needed for DeliverJob
	print '[ColorProofJobStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.ColorProofJobStatus ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].ColorProofJobStatus (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].ColorProofJobStatus
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.ColorProofJobStatus OFF;

	print '[DeliverJobStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.DeliverJobStatus ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].DeliverJobStatus (
           [ID]
		   ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].DeliverJobStatus
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.DeliverJobStatus OFF;
	
	-- neede for PaperTint
	print '[MediaCategory]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.MediaCategory ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].MediaCategory (
           [ID]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].MediaCategory
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.MediaCategory OFF;

	print '[MeasurementCondition]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.MeasurementCondition ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].MeasurementCondition (
           [ID]
		   ,[KEY]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].MeasurementCondition
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.MeasurementCondition OFF;	

	-- needed for BillingPlanType
	print '[Smoothing]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Smoothing] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Smoothing] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[Smoothing]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Smoothing] OFF;

	print '[Colorspace]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Colorspace] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Colorspace] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[Colorspace]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Colorspace] OFF;

	print '[ImageFormat]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ImageFormat] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ImageFormat] (
			[ID]
           ,[Key]
           ,[Name]
		   ,[Parent])
	SELECT *
	FROM [GMGCozone].[dbo].[ImageFormat]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ImageFormat] OFF;

	print '[PageBox]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PageBox] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PageBox] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[PageBox]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PageBox] OFF;

	print '[TileImageFormat]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[TileImageFormat] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[TileImageFormat] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[TileImageFormat]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[TileImageFormat] OFF;

	print '[MediaService]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[MediaService] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[MediaService] (
			[ID]
           ,[Name]
           ,[Smoothing]
           ,[Resolution]
           ,[Colorspace]
           ,[ImageFormat]
           ,[PageBox]
           ,[TileImageFormat]
           ,[JPEGCompression])
	SELECT *
	FROM [GMGCozone].[dbo].[MediaService]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[MediaService] OFF;

	print '[AppModule]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AppModule] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AppModule] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[AppModule]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AppModule] OFF;	

	print '[BillingPlanType]' -- BillingPlanType where Account is null, this is a nomenclator, needed for AttachedAccountBillingPlan
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlanType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BillingPlanType] (
			[ID]
           ,[Key]
           ,[Name]
           ,[MediaService]
           ,[EnablePrePressTools]
           ,[EnableMediaTools]
           ,[AppModule]
           ,[Account])
	SELECT *
	FROM [GMGCozone].[dbo].[BillingPlanType]
	WHERE Account IS NULL
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlanType] OFF;

	print '[BillingPlan]' -- BillingPlan where Account is null, this is a nomenclator, needed for AttachedAccountBillingPlan
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlan] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BillingPlan] (
			[ID]
           ,[Name]
           ,[Type]
           ,[Proofs]
           ,[Price]
           ,[IsFixed]
           ,[ColorProofConnections]
           ,[IsDemo]
           ,[MaxUsers]
           ,[MaxStorageInGb]
           ,[Account])
	SELECT *
	FROM [GMGCozone].[dbo].[BillingPlan]
	WHERE Account IS NULL
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlan] OFF;

	-- needed for NotificationSchedule
	print '[NotificationPresetFrequency]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NotificationPresetFrequency] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[NotificationPresetFrequency] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[NotificationPresetFrequency]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NotificationPresetFrequency] OFF;	

	print '[FTPProtocol]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[FTPProtocol] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[FTPProtocol] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[FTPProtocol]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[FTPProtocol] OFF;	

	print '[ConversionStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ConversionStatus] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ConversionStatus] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[ConversionStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ConversionStatus] OFF;	
	
	print '[PreflightStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PreflightStatus] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PreflightStatus] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[PreflightStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PreflightStatus] OFF;		

	print '[DeliverJobPageStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DeliverJobPageStatus] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[DeliverJobPageStatus] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[DeliverJobPageStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DeliverJobPageStatus] OFF;	
	
	print '[ApprovalCommentType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalCommentType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ApprovalCommentType] (
			[ID]
           ,[Key]
           ,[Name]
		   ,[Priority])
	SELECT *
	FROM [GMGCozone].[dbo].[ApprovalCommentType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalCommentType] OFF;	

	print '[ApprovalAnnotationStatus]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalAnnotationStatus] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ApprovalAnnotationStatus] (
			[ID]
           ,[Key]
           ,[Name]
		   ,[Priority])
	SELECT *
	FROM [GMGCozone].[dbo].[ApprovalAnnotationStatus]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalAnnotationStatus] OFF;	

	print '[HighlightType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[HighlightType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[HighlightType] (
			[ID]
           ,[Key]
           ,[Name]
		   ,[Priority])
	SELECT *
	FROM [GMGCozone].[dbo].[HighlightType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[HighlightType] OFF;	

	-------------


	print '[UploadEnginePermission]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEnginePermission] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UploadEnginePermission] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[UploadEnginePermission]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEnginePermission] OFF;	

	--needed for UploadEngineCustomXMLTemplatesField
	print '[UploadEngineDefaultXMLTemplate]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineDefaultXMLTemplate] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UploadEngineDefaultXMLTemplate] (
			[ID]
           ,[Key]
           ,[NodeName]
		   ,[XPath])
	SELECT *
	FROM [GMGCozone].[dbo].[UploadEngineDefaultXMLTemplate]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UploadEngineDefaultXMLTemplate] OFF;	

	print '[EventGroup]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[EventGroup] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[EventGroup] (
			[ID]
           ,[Key]
		   ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[EventGroup]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[EventGroup] OFF;	

	print '[EventType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[EventType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[EventType] (
			[ID]
           ,[EventGroup]
           ,[Key]
		   ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[EventType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[EventType] OFF;	

	print '[ApprovalCollaboratorRole]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalCollaboratorRole] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ApprovalCollaboratorRole] (
			[ID]
           ,[Key]
           ,[Name]
		   ,[Priority])
	SELECT *
	FROM [GMGCozone].[dbo].[ApprovalCollaboratorRole]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ApprovalCollaboratorRole] OFF;	

	-- needed for UserCustomPresetEventTypeValue
	print '[Role]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Role] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Role] (
			[ID]
           ,[Key]
           ,[Name]
           ,[Priority]
           ,[AppModule])
	SELECT *
	FROM [GMGCozone].[dbo].[Role]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Role] OFF;	

	print '[RolePresetEventType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[RolePresetEventType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[RolePresetEventType] (
			[ID]
           ,[Role]
           ,[Preset]
           ,[EventType])
	SELECT *
	FROM [GMGCozone].[dbo].[RolePresetEventType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[RolePresetEventType] OFF;	

	print '[ValueDataType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ValueDataType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ValueDataType] (
			[ID]
           ,[Key]
		   ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[ValueDataType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ValueDataType] OFF;	

	-- needed for Company
	print '[Country]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Country] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Country] (
			[ID]
           ,[Iso2]
           ,[Iso3]
           ,[IsoCountryNumber]
           ,[DialingPrefix]
           ,[Name]
           ,[ShortName]
           ,[HasLocationData])
	SELECT *
	FROM [GMGCozone].[dbo].[Country]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Country] OFF;	

	---------
	-- some other tables
	--------

	print '[AccountTypeRole]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountTypeRole] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTypeRole] (
			[ID]
           ,[AccountType]
           ,[Role])
	SELECT *
	FROM [GMGCozone].[dbo].[AccountTypeRole]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTypeRole] OFF;	

	print '[ControllerAction]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ControllerAction] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ControllerAction] (
			[ID]
           ,[Controller]
           ,[Action]
           ,[Parameters])
	SELECT *
	FROM [GMGCozone].[dbo].[ControllerAction]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[ControllerAction] OFF;	

	print '[DefaultTheme]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[DefaultTheme] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[DefaultTheme] (
			[ID]
           ,[Key]
           ,[ThemeName])
	SELECT *
	FROM [GMGCozone].[dbo].[DefaultTheme]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[DefaultTheme] OFF;	

	print '[NativeDataType]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NativeDataType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[NativeDataType] (
			[ID]
           ,[Type])
	SELECT *
	FROM [GMGCozone].[dbo].[NativeDataType]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[NativeDataType] OFF;	
	
	print '[GlobalSettings]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[GlobalSettings] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[GlobalSettings] (
			[ID]
           ,[Key]
           ,[DataType]
           ,[Value])
	SELECT *
	FROM [GMGCozone].[dbo].[GlobalSettings]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[GlobalSettings] OFF;		
	
	print '[ICCProfileCreatorSignatures]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ICCProfileCreatorSignatures] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ICCProfileCreatorSignatures] (
			[ID]
           ,[Signature]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[ICCProfileCreatorSignatures]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[ICCProfileCreatorSignatures] OFF;	

	print '[MenuItem]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[MenuItem] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[MenuItem] (
			[ID]
           ,[ControllerAction]
           ,[Parent]
           ,[Position]
           ,[IsAdminAppOwned]
           ,[IsAlignedLeft]
           ,[IsSubNav]
           ,[IsTopNav]
           ,[IsVisible]
           ,[Key]
           ,[Name]
           ,[Title])
	SELECT *
	FROM [GMGCozone].[dbo].[MenuItem]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[MenuItem] OFF;	

	print '[MenuItemAccountTypeRole]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[MenuItemAccountTypeRole] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[MenuItemAccountTypeRole] (
			[ID]
           ,[MenuItem]
           ,[AccountTypeRole])
	SELECT *
	FROM [GMGCozone].[dbo].[MenuItemAccountTypeRole]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[MenuItemAccountTypeRole] OFF;	

	print '[NotificationPresetCollaborateTrigger]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NotificationPresetCollaborateTrigger] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[NotificationPresetCollaborateTrigger] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[NotificationPresetCollaborateTrigger]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[NotificationPresetCollaborateTrigger] OFF;	

	print '[NotificationPresetCollaborateUnit]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[NotificationPresetCollaborateUnit] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[NotificationPresetCollaborateUnit] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[NotificationPresetCollaborateUnit]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[NotificationPresetCollaborateUnit] OFF;	

	print '[PrePressFunction]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[PrePressFunction] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[PrePressFunction] (
			[ID]
           ,[Key]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[PrePressFunction]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[PrePressFunction] OFF;	

	print '[ReservedDomainName]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ReservedDomainName] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ReservedDomainName] (
			[ID]
           ,[Name])
	SELECT *
	FROM [GMGCozone].[dbo].[ReservedDomainName]
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[ReservedDomainName] OFF;	
	
	print 'END CopyNomenclatures'

END
GO
