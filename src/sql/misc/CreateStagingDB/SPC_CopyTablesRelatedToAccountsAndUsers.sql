-------
-- RUN THIS SCRIPT ON GMG RDS instance "cozone-prod-freeze", database "GMGCoZone_PartiallyMigrated3".
-- DO NOT RUN IT BEFORE snapshot "cozone-prod-freeze" is created on GMG Virginia.
-------

USE [GMGCoZone_PartiallyMigrated3]
GO

create PROCEDURE [dbo].[SPC_CopyTablesRelatedToAccountsAndUsers] (
    @Accounts AS AccountIDsTableType READONLY,
	@Users AS AccountIDsTableType READONLY
)
AS
BEGIN
	-- THIS PROCEDURE COPIES TablesRelatedToAccountsAndUsers, ONLY THE RECORDS RELATED TO ACCOUNTS AND USERS WITH THE IDS FROM THE PARAMETERS
	print 'START [SPC_CopyTablesRelatedToAccountsAndUsers]'

	------ LEVEL 6 -------------------------------------------------------
	----------------------------------------------------------------------	

	print '[AccountCustomICCProfile]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountCustomICCProfile] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountCustomICCProfile] (
			[ID]
           ,[Account]
           ,[Name]
           ,[Guid]
           ,[Uploader]
           ,[UploadDate]
           ,[IsRGB]
           ,[IsValid]
           ,[IsActive]
           ,[L]
           ,[a]
           ,[b]
           ,[PrintSubstrate])
	SELECT * 
	FROM [GMGCoZone].[dbo].[AccountCustomICCProfile]
	WHERE Account IN (SELECT ID FROM @Accounts)

	UNION ------------ new !!!!!!!!!!!!!!!
	SELECT DISTINCT x.*
	FROM [GMGCoZone].[dbo].[AccountCustomICCProfile] x
	INNER JOIN (
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[ViewingCondition] vwc
		LEFT JOIN [GMGCoZone].[dbo].[PaperTint] ppt ON vwc.PaperTint = ppt.ID
		LEFT JOIN [GMGCoZone].[dbo].[SimulationProfile] sim ON vwc.SimulationProfile = sim.ID
		LEFT JOIN [GMGCoZone].[dbo].[AccountCustomICCProfile] acp ON vwc.CustomProfile = acp.ID			
		LEFT JOIN [GMGCoZone].[dbo].ApprovalEmbeddedProfile aep on vwc.EmbeddedProfile=aep.ID
		LEFT JOIN [GMGCoZone].[dbo].Approval a on a.id=aep.approval
		LEFT JOIN [GMGCoZone].[dbo].job j on j.id=a.job
		WHERE ppt.AccountId IN (SELECT ID FROM @Accounts)
			or sim.AccountId IN (SELECT ID FROM @Accounts)
			or acp.Account IN (SELECT ID FROM @Accounts)
			or j.Account IN (SELECT ID FROM @Accounts)
		UNION
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[ApprovalAnnotation] aa
		JOIN [GMGCoZone].[dbo].[ApprovalPage] ap ON aa.Page = ap.ID
		JOIN [GMGCoZone].[dbo].[Approval] a ON ap.Approval = a.ID
		JOIN [GMGCoZone].[dbo].[Job] j ON a.Job = j.ID
		JOIN [GMGCoZone].[dbo].[ViewingCondition] vwc on vwc.ID=aa.ViewingCondition
		WHERE j.Account IN (SELECT ID FROM @Accounts)
		UNION 
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[SoftProofingSessionParams] sfse
		JOIN [GMGCoZone].[dbo].[Approval] a ON sfse.Approval = a.ID
		JOIN [GMGCoZone].[dbo].[Job] j ON a.Job = j.ID
		JOIN [GMGCoZone].[dbo].[ViewingCondition] vwc on vwc.ID=sfse.ViewingCondition
		WHERE j.Account IN (SELECT ID FROM @Accounts)
		UNION 
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[ApprovalJobViewingCondition] ajvc
		JOIN [GMGCoZone].[dbo].[Job] j ON ajvc.Job = j.ID
		JOIN [GMGCoZone].[dbo].[ViewingCondition] vwc on vwc.ID=ajvc.ViewingCondition
		WHERE j.Account IN (SELECT ID FROM @Accounts)
	) y on x.ID=y.CustomProfile
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountCustomICCProfile] OFF;

	print '[SimulationProfile]' -- SimulationProfile must be before ViewingCondition
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[SimulationProfile] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[SimulationProfile] (
			[ID]
           ,[AccountId]
           ,[Creator]
           ,[ProfileName]
           ,[FileName]
           ,[Guid]
           ,[MediaCategory]
           ,[MeasurementCondition]
           ,[L]
           ,[a]
           ,[b]
           ,[CreatedDate]
           ,[UpdatedDate]
           ,[IsValid]
           ,[IsDeleted])
	SELECT DISTINCT x.*
	FROM [GMGCoZone].[dbo].[SimulationProfile] x
	INNER JOIN (
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[ViewingCondition] vwc
		LEFT JOIN [GMGCoZone].[dbo].[PaperTint] ppt ON vwc.PaperTint = ppt.ID
		LEFT JOIN [GMGCoZone].[dbo].[SimulationProfile] sim ON vwc.SimulationProfile = sim.ID
		LEFT JOIN [GMGCoZone].[dbo].[AccountCustomICCProfile] acp ON vwc.CustomProfile = acp.ID			
		LEFT JOIN [GMGCoZone].[dbo].ApprovalEmbeddedProfile aep on vwc.EmbeddedProfile=aep.ID
		LEFT JOIN [GMGCoZone].[dbo].Approval a on a.id=aep.approval
		LEFT JOIN [GMGCoZone].[dbo].job j on j.id=a.job
		WHERE ppt.AccountId IN (SELECT ID FROM @Accounts)
			or sim.AccountId IN (SELECT ID FROM @Accounts)
			or acp.Account IN (SELECT ID FROM @Accounts)
			or j.Account IN (SELECT ID FROM @Accounts)
		UNION
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[ApprovalAnnotation] aa
		JOIN [GMGCoZone].[dbo].[ApprovalPage] ap ON aa.Page = ap.ID
		JOIN [GMGCoZone].[dbo].[Approval] a ON ap.Approval = a.ID
		JOIN [GMGCoZone].[dbo].[Job] j ON a.Job = j.ID
		JOIN [GMGCoZone].[dbo].[ViewingCondition] vwc on vwc.ID=aa.ViewingCondition
		WHERE j.Account IN (SELECT ID FROM @Accounts)
		UNION 
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[SoftProofingSessionParams] sfse
		JOIN [GMGCoZone].[dbo].[Approval] a ON sfse.Approval = a.ID
		JOIN [GMGCoZone].[dbo].[Job] j ON a.Job = j.ID
		JOIN [GMGCoZone].[dbo].[ViewingCondition] vwc on vwc.ID=sfse.ViewingCondition
		WHERE j.Account IN (SELECT ID FROM @Accounts)
		UNION 
		SELECT vwc.*
		FROM [GMGCoZone].[dbo].[ApprovalJobViewingCondition] ajvc
		JOIN [GMGCoZone].[dbo].[Job] j ON ajvc.Job = j.ID
		JOIN [GMGCoZone].[dbo].[ViewingCondition] vwc on vwc.ID=ajvc.ViewingCondition
		WHERE j.Account IN (SELECT ID FROM @Accounts)
	) y on x.ID=y.SimulationProfile

	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[SimulationProfile] OFF;

	print '[SimulationProfile]' -- ALL
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[SimulationProfile] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[SimulationProfile] (
			[ID]
           ,[AccountId]
           ,[Creator]
           ,[ProfileName]
           ,[FileName]
           ,[Guid]
           ,[MediaCategory]
           ,[MeasurementCondition]
           ,[L]
           ,[a]
           ,[b]
           ,[CreatedDate]
           ,[UpdatedDate]
           ,[IsValid]
           ,[IsDeleted])
	SELECT *
	FROM [GMGCoZone].[dbo].[SimulationProfile] x
	WHERE x.ID NOT IN (SELECT ID FROM [GMGCoZone_PartiallyMigrated3].[dbo].[SimulationProfile])
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[SimulationProfile] OFF;

	print '[BillingPlanType]' -- BillingPlanType is also nomenclature when Account is NULL  
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlanType] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BillingPlanType] (
			[ID]
           ,[Key]
           ,[Name]
           ,[MediaService]
           ,[EnablePrePressTools]
           ,[EnableMediaTools]
           ,[AppModule]
           ,[Account])
	SELECT *
	FROM [GMGCoZone].[dbo].[BillingPlanType]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlanType] OFF;

	print '[BillingPlan]' -- BillingPlan is also nomenclature when Account is NULL
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlan] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BillingPlan] (
			[ID]
           ,[Name]
           ,[Type]
           ,[Proofs]
           ,[Price]
           ,[IsFixed]
           ,[ColorProofConnections]
           ,[IsDemo]
           ,[MaxUsers]
           ,[MaxStorageInGb]
           ,[Account])
	SELECT *
	FROM [GMGCoZone].[dbo].[BillingPlan]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BillingPlan] OFF;

	print '[AttachedAccountBillingPlan]'  
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AttachedAccountBillingPlan] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AttachedAccountBillingPlan] (
			[ID]
           ,[Account]
           ,[BillingPlan]
           ,[NewPrice]
           ,[IsAppliedCurrentRate]
           ,[NewProofPrice]
           ,[IsModifiedProofPrice])
	SELECT *
	FROM [GMGCoZone].[dbo].[AttachedAccountBillingPlan]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AttachedAccountBillingPlan] OFF;

	print '[Shape]' -- Copy Shape records with not custom attribute, and for the selected Accounts (there are about 100k rows which will not be copied because they are not referenced by any Account)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Shape] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Shape] (
			[ID]
           ,[IsCustom]
           ,[FXG]
           ,[SVG])
	SELECT * 
	FROM [GMGCoZone].[dbo].[Shape] s2 WHERE s2.IsCustom=0
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Shape] OFF;	

	--------- LEVEL 2 -------------
	----------------------------------------------------------------------	

	print '[AccountStorageUsage]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountStorageUsage] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountStorageUsage] (
			[ID]
           ,[Account]
           ,[UsedStorage])
	SELECT *
	FROM [GMGCoZone].[dbo].[AccountStorageUsage]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountStorageUsage] OFF;
	
	print '[BrandingPreset]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BrandingPreset] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BrandingPreset] (
			[ID]
           ,[Account]
           ,[Guid]
           ,[Name]
           ,[HeaderLogoPath]
           ,[EmailLogoPath]
           ,[FavIconPath])
	SELECT *
	FROM [GMGCoZone].[dbo].[BrandingPreset]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BrandingPreset] OFF;

	print '[ChangedBillingPlan]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ChangedBillingPlan] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ChangedBillingPlan] (
			[ID]
           ,[Account]
           ,[BillingPlan]
           ,[AttachedAccountBillingPlan])
	SELECT cbp.*
	FROM [GMGCoZone].[dbo].[ChangedBillingPlan] cbp
	LEFT JOIN [GMGCoZone].[dbo].[AttachedAccountBillingPlan] aabp ON cbp.AttachedAccountBillingPlan = aabp.ID
	WHERE aabp.Account IN (SELECT ID FROM @Accounts)
		OR cbp.Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ChangedBillingPlan] OFF;
	
	----------------------------------------------------------------------
	-- (LEVEL 1)
	----------------------------------------------------------------------

	---------------------------------------------------------------------
	--- Administration - Approval Phasing related tables -----------
	---------------------------------------------------------------------

	print '[CurrencyRate]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[CurrencyRate] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[CurrencyRate] (
			[ID]
           ,[Currency]
           ,[Rate]
           ,[Creator]
           ,[Modifier]
           ,[CreatedDate]
           ,[ModifiedDate])
	SELECT *
	FROM [GMGCoZone].[dbo].[CurrencyRate]
	WHERE [Creator] IN (SELECT ID FROM @Users)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[CurrencyRate] OFF;	

	print '[UserRole]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UserRole] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UserRole] (
			[ID]
           ,[User]
           ,[Role])
	SELECT *
	FROM [GMGCoZone].[dbo].[UserRole]
	WHERE [User] IN (SELECT ID FROM @Users)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[UserRole] OFF;	
	
	print '[UserSetting]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[UserSetting] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[UserSetting] (
			[ID]
           ,[User]
           ,[Setting]
           ,[Value]
           ,[ValueDataType])
	SELECT *
	FROM [GMGCoZone].[dbo].[UserSetting]
	WHERE [User] IN (SELECT ID FROM @Users)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[UserSetting] OFF;	

	print '[BrandingPresetTheme]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[BrandingPresetTheme] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[BrandingPresetTheme] (
			[ID]
           ,[BrandingPreset]
           ,[Key]
           ,[ThemeName]
           ,[Active])
	SELECT bpt.*
	FROM [GMGCoZone].[dbo].[BrandingPresetTheme] bpt
	JOIN [GMGCoZone].[dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	WHERE bp.Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[BrandingPresetTheme] OFF;	

	print '[AccountTheme]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountTheme] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTheme] (
			[ID]
           ,[Account]
           ,[Key]
           ,[ThemeName]
           ,[Active])
	SELECT act.*
	FROM [GMGCoZone].[dbo].[AccountTheme] act
	WHERE act.Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountTheme] OFF;	

	print '[ThemeColorScheme]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[ThemeColorScheme] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[ThemeColorScheme] (
			[ID]
           ,[DefaultTheme]
           ,[AccountTheme]
           ,[BrandingPresetTheme]
           ,[Name]
           ,[THex]
           ,[BHex])
	SELECT tcs.*
	FROM [GMGCoZone].[dbo].[ThemeColorScheme] tcs
	LEFT JOIN [GMGCoZone].[dbo].[DefaultTheme] d ON tcs.DefaultTheme = d.ID
	LEFT JOIN [GMGCoZone].[dbo].[AccountTheme] act ON tcs.AccountTheme = act.ID
	LEFT JOIN [GMGCoZone].[dbo].[BrandingPresetTheme] bpt ON tcs.BrandingPresetTheme = bpt.ID
	LEFT JOIN [GMGCoZone].[dbo].[BrandingPreset] bp ON bpt.BrandingPreset = bp.ID
	WHERE (tcs.DefaultTheme IS NOT NULL AND tcs.AccountTheme IS NULL AND tcs.BrandingPresetTheme IS NULL)
		OR act.Account IN (SELECT ID FROM @Accounts)
		OR bp.Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[ThemeColorScheme] OFF;	

	print '[SubscriberPlanInfo]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[SubscriberPlanInfo] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[SubscriberPlanInfo] (
			[ID]
           ,[NrOfCredits]
           ,[IsQuotaAllocationEnabled]
           ,[ChargeCostPerProofIfExceeded]
           ,[SelectedBillingPlan]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
	SELECT *
	FROM [GMGCoZone].[dbo].[SubscriberPlanInfo]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[SubscriberPlanInfo] OFF;	
	
	print '[Company]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[Company] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[Company] (
			[ID]
           ,[Account]
           ,[Name]
           ,[Number]
           ,[Address1]
           ,[Address2]
           ,[City]
           ,[Postcode]
           ,[State]
           ,[Phone]
           ,[Mobile]
           ,[Email]
           ,[Country]
           ,[Guid])
	SELECT *
	FROM [GMGCoZone].[dbo].[Company]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[Company] OFF;	

	print '[AccountSubscriptionPlan]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountSubscriptionPlan] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountSubscriptionPlan] (
			[ID]
           ,[SelectedBillingPlan]
           ,[IsMonthlyBillingFrequency]
           ,[ChargeCostPerProofIfExceeded]
           ,[ContractStartDate]
           ,[Account]
           ,[AccountPlanActivationDate]
           ,[AllowOverdraw])
	SELECT *
	FROM [GMGCoZone].[dbo].[AccountSubscriptionPlan]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountSubscriptionPlan] OFF;	

	print '[AccountSetting]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountSetting] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountSetting] (
			[ID]
           ,[Account]
           ,[Name]
           ,[Value]
           ,[ValueDataType]
           ,[Description]
           ,[Creator]
           ,[CreatedDate]
           ,[Modifier]
           ,[ModifiedData])
	SELECT *
	FROM [GMGCoZone].[dbo].[AccountSetting]
	WHERE Account IN (SELECT ID FROM @Accounts)
		OR Account IS NULL
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountSetting] OFF;	

	print '[AccountHelpCentre]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountHelpCentre] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountHelpCentre] (
			[ID]
           ,[Account]
           ,[ContactEmail]
           ,[ContactPhone]
           ,[SupportDays]
           ,[SupportHours]
           ,[UserGuideLocation]
           ,[UserGuideName]
           ,[UserGuideUpdatedDate])
	SELECT *
	FROM [GMGCoZone].[dbo].[AccountHelpCentre]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountHelpCentre] OFF;	

	print '[AccountBillingTransactionHistory]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountBillingTransactionHistory] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountBillingTransactionHistory] (
			[ID]
           ,[Account]
           ,[CreatedDate]
           ,[Creator]
           ,[BaseAmount]
           ,[DiscountAmount])
	SELECT *
	FROM [GMGCoZone].[dbo].[AccountBillingTransactionHistory]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountBillingTransactionHistory] OFF;	

	print '[AccountBillingPlanChangeLog]'
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].dbo.[AccountBillingPlanChangeLog] ON;
	INSERT INTO [GMGCoZone_PartiallyMigrated3].[dbo].[AccountBillingPlanChangeLog] (
			[ID]
           ,[Account]
           ,[CreatedDate]
           ,[Creator]
           ,[LogMessage]
           ,[PreviousPlanAmount]
           ,[PreviousProofsAmount]
           ,[GPPDiscount]
           ,[PreviousPlan]
           ,[CurrentPlan])
	SELECT *
	FROM [GMGCoZone].[dbo].[AccountBillingPlanChangeLog]
	WHERE Account IN (SELECT ID FROM @Accounts)
	SET IDENTITY_INSERT [GMGCoZone_PartiallyMigrated3].[dbo].[AccountBillingPlanChangeLog] OFF;	

	print 'END [SPC_CopyTablesRelatedToAccountsAndUsers]'

END
GO
