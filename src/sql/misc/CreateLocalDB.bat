@SET COZONEFOLDER=%~dp0\..\

@ECHO "executing DropScript.sql"
@SET DROP_DB_SCRIPT="%COZONEFOLDER%\setup\GMGColor_DropsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %DROP_DB_SCRIPT%

@ECHO "executing GMGColor_EntityCreationsScript.sql"
@SET ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\setup\GMGColor_EntityCreationsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %ENTITY_CREATION_SCRIPT%

@ECHO "executing GMGColor_InsertsScript.sql"
@SET INSERTS_SCRIPT="%COZONEFOLDER%\setup\GMGColor_InsertsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %INSERTS_SCRIPT%

@ECHO "executing GMGColor_InsertsScriptDeveloper.sql"
@SET DEVELOPER_SCRIPT="%COZONEFOLDER%\setup\GMGColor_InsertsScriptDeveloper.sql"
sqlcmd -S . -d GMGCoZone -E -i %DEVELOPER_SCRIPT%

@ECHO "executing GMGColor_Update_Local.sql"
@SET LOCAL_SCRIPT="%COZONEFOLDER%\setup\GMGColor_Update_Local.sql"
sqlcmd -S . -d GMGCoZone -E -i %LOCAL_SCRIPT%

@REM v0.1 CHANGES

@ECHO "executing Changes.sql"
@SET CHANGES_SCRIPT="%COZONEFOLDER%\changes\changes v0.1\Changes.sql"
sqlcmd -S . -d GMGCoZone -E -i %CHANGES_SCRIPT%


@REM  v1.8 (DELIVER MODULE) CHANGES

@ECHO "executing GMGColor_Deliver_EntityCreationsScript.sql"
@SET DELIVER_ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\changes\changes v1.8 (Deliver)\GMGColor_Deliver_EntityCreationsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_CREATION_SCRIPT%

@ECHO "executing GMGColor_Deliver_InsertsScript.sql"
@SET DELIVER_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.8 (Deliver)\GMGColor_Deliver_InsertsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_INSERT_SCRIPT%

@ECHO "executing GMGColor_Deliver_InsertsScriptDeveloper.sql"
@SET DELIVER_DEVELOPER_SCRIPT="%COZONEFOLDER%\changes\changes v1.8 (Deliver)\GMGColor_Deliver_InsertsScriptDeveloper.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_DEVELOPER_SCRIPT%

@REM  v1.9 CHANGES

@ECHO "executing changes v1.9.sql"
@SET DELIVER_ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\changes\changes v1.9\changes v1.9.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_CREATION_SCRIPT%

@REM  v1.10 CHANGES

@ECHO "executing changes v1.10.sql"
@SET DELIVER_ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\changes\changes v1.10\changes v1.10.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_CREATION_SCRIPT%

@REM  v1.12 CHANGES

@ECHO "executing GMGColor_EntityCreationsScript.sql"
@SET DELIVER_ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\changes\changes v1.12\GMGColor_EntityCreationsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_CREATION_SCRIPT%

@ECHO "executing GMGColor_InsertsScript.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.12\GMGColor_InsertsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes.sql"
@SET DELIVER_ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\changes\changes v1.12\changes.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_CREATION_SCRIPT%

@REM @ECHO "executing remove duplicate roles from sysadmin account.sql"
@REM @SET DELIVER_ENTITY_CREATION_SCRIPT="%COZONEFOLDER%\changes\changes v1.12\remove duplicate roles from sysadmin account.sql"
@REM sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_CREATION_SCRIPT%

@REM  v1.13 CHANGES

@ECHO "executing GMGColor_InsertsScript.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.13\GMGColor_InsertsScript.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@REM  v1.14 CHANGES

@ECHO "executing changes v1.14.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.14\changes v1.14.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@REM  v1.15 CHANGES

@ECHO "executing changes v1.15.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.15\changes v1.15.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@REM  v1.16 CHANGES

@ECHO "executing changes v1.16.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.16\changes v1.16.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@REM  v1.17 CHANGES

@ECHO "executing changes v1.17.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.17 (SoftProofing)\changes v1.17.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@REM  v1.18 CHANGES - Email changes

@ECHO "executing changes v1.18.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.18\changes v1.18.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.19.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.19\changes v1.19.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.20.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.20\changes v1.20.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.21.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.21\changes v1.21.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.22.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.22\changes v1.22.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.23.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.23\changes v1.23.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.23\XML Engine changes v1.23.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes AddSVGDefinitionForPredefinedShapes.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\misc\UpdateSVGPredefinedShapes\AddSVGDefinitionForPredefinedShapes.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.24.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.24\changes v1.24.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.25.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.25 (Deliver Pairing and Sharing)\changes v1.25.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.25 (Deliver Pairing and Sharing)\Deliver Pairing and Sharing.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.26.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.26\changes v1.26.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.27.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.27\changes v1.27.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.28.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.28\changes v1.28.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.29.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.29\changes v1.29.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.30.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.30\changes v1.30.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.31.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.31\changes v1.31.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.32.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.32\changes v1.32.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.34.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.34\changes v1.34.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.35.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.35\changes v1.35.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.36.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.36\changes v1.36.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.37.sql"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.37\changes v1.37.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.38"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.38\changes v1.38.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing GMGColor_InsertsScriptDeveloper"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.38\GMGColor_InsertsScriptDeveloper.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.39"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.39\changes v1.39.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.40"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.40\changes v1.40.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing softproofing changes"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.41 (SoftProofing)\SoftProofingChanges.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.41 (SoftProofing)\SoftProofingChanges_2.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%      

@ECHO "executing changes v1.42"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.42\changes v1.42.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.42.1"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.42\changes v1.42.1.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%


@ECHO "executing changes v1.42.1 deadlocks imp"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.42\changes v1.42.1.DEADLOCK_IMP.sql"

@ECHO "executing changes v1.42.1.annotations_from_all_phases"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.42\changes v1.42.1.annotations_from_all_phases.sql"

sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.43"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.43\changes v1.43.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.43"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.43\changes v1.43_annotations_from_all_phases.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

@ECHO "executing changes v1.43.2"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.43\changes v1.43.2.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%


ECHO "executing changes v1.43.3"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.43\changes v1.43.3.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.44"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.44\changes v1.44.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.44 AddDefProfile"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.44\AddDefProfile.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%


ECHO "executing changes v1.44 DropColumnsWARNINg"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.44\DropColumnsWARNINg.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.45"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.45\changes v1.45.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.46"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.46\changes v1.46.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.47"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.47\changes v1.47.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.49"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.49\changes v1.49.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.50"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.50\changes v1.50.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.51"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.51\changes v1.51.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.52"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.52\changes v1.52.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.53"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.53\changes v1.53.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT% 

ECHO "executing changes v1.53"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.53\stored_procedures v1.53.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.54"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.54\changes v1.54.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT% 

ECHO "executing changes v1.55"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.55\changes v1.55.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v1.55"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v1.55\stored_procedures v1.55.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.0"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.0\changes v2.0.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.1"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.1\changes v2.1.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing CZ-3146 Update billing report function and stored procedure"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.1\CZ-3146 Update billing report function and stored procedure.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.2"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.2\changes v2.2.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.2 patch20180704_SPC_ReturnApprovalsPageInfo"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.2\patch20180704_SPC_ReturnApprovalsPageInfo_22062v01_B05.sql"
sqlcmd -S (local) -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.2 patch20180704_SPC_ReturnFoldersApprovalsPageInfo"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.2\patch20180704_SPC_ReturnFoldersApprovalsPageInfo_22062v01_B05.sql"
sqlcmd -S (local) -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.2_differences_on_migration\modified tables and views"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.2_differences_on_migration\modified tables and views.sql"
sqlcmd -S (local) -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

ECHO "executing changes v2.2 translations"
@SET DELIVER_ENTITY_INSERT_SCRIPT="%COZONEFOLDER%\changes\changes v2.2.0 translations\changes v2.2.0 translations.sql"
sqlcmd -S . -d GMGCoZone -E -i %DELIVER_ENTITY_INSERT_SCRIPT%

pause
