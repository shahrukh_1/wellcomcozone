DECLARE @i INT 
DECLARE @usernameprefix NVARCHAR(100)
DECLARE @username NVARCHAR(100)
DECLARE @indexStr NVARCHAR(100)

SET @i = 0
SET @usernameprefix = 'TestGroup_'

WHILE(@i < 50)
BEGIN
	SET @indexStr =  convert(varchar, @i)
	SET @username = @usernameprefix + @indexStr
	
	INSERT INTO [GMGCoZone].[dbo].[UserGroup]
           ([Name]
           ,[Account]
           ,[Creator]
           ,[CreatedDate]
           ,[Modifier]
           ,[ModifiedDate])
		 VALUES
			   (@username
			   ,4
			   ,1
			   ,GETDATE()
			   ,4
			   ,GETDATE()
			   )
			   
	DECLARE @id INT
	SET @id = SCOPE_IDENTITY();
		
	SET @i = @i + 1;
END

