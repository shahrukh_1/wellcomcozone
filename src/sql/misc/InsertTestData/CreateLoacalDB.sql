USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--
-- TABLES --
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Account]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Name] [nvarchar](128) NOT NULL,
	[SiteName] [nvarchar](128) NOT NULL,
	[Domain] [nvarchar](255) NOT NULL,	
	[AccountType] [int] NOT NULL,
	[Parent] [int] NULL,
	[Owner] [int] NULL,	
	[Guid] [nvarchar](36) NOT NULL,
	[ContactFirstName] [nvarchar](128) NOT NULL,
	[ContactLastName] [nvarchar](128) NOT NULL,
	[ContactPhone] [nvarchar](20) NULL,
	[ContactEmailAddress] [nvarchar](64) NOT NULL,
	[HeaderLogoPath] [nvarchar](256) NULL,
	[LoginLogoPath] [nvarchar](256) NULL,
	[EmailLogoPath] [nvarchar](256) NULL,
	[Theme] [int] NOT NULL,
	[CustomColor] [nvarchar](8) NULL,
	[CurrencyFormat] [int] NOT NULL,
	[Locale] [int] NOT NULL,
	[TimeZone] [nvarchar] (128) NOT NULL,
	[DateFormat] [int] NOT NULL,
	[TimeFormat] [int] NOT NULL,
	[GPPDiscount] [decimal] (4,2) NULL,
	[NeedSubscriptionPlan] [bit] NOT NULL,
	[ChargeCostPerProofIfExceeded] [bit] NOT NULL,
	[SelectedBillingPlan] [int] NULL,
	[IsMonthlyBillingFrequency] [bit] NULL,
	--[BillingAnniversaryDate] [nvarchar](32) NULL,
	--[BillingAnniversaryMonth] [int] NULL,
	[ContractPeriod] [int] NOT NULL,
	[ChildAccountsVolume] [int] NULL,	
	[CustomFromAddress] [nvarchar](256) NULL,
	[CustomFromName] [nvarchar](256) NULL,
	[SuspendReason] [nvarchar](256) NULL,
	[IsSuspendedOrDeletedByParent] [bit] NOT NULL,	
	[CustomDomain] [nvarchar](255) NULL,	
	[IsCustomDomainActive] [bit] NOT NULL,	
	[IsHideLogoInFooter] [bit] NOT NULL,
	[IsHideLogoInLoginFooter] [bit] NOT NULL,
	[IsNeedProfileManagement] [bit] NOT NULL,
	[IsNeedPDFsToBeColorManaged] [bit] NOT NULL,
	[IsRemoveAllGMGCollaborateBranding] [bit] NOT NULL,
	[Status] [int] NOT NULL,
	[IsEnabledLog] [bit] NOT NULL,		
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsHelpCentreActive] [bit] NOT NULL,
	[DealerNumber] [nvarchar](32) NULL,
	[CustomerNumber] [nvarchar](32) NULL,
	[VATNumber] [nvarchar](32) NULL,
	[IsAgreedToTermsAndConditions] [bit] NOT NULL,	
	[ContractStartDate] [datetime2](7) NULL,
	[Region] [nvarchar](128) NOT NULL,
	[IsTemporary] [bit] NOT NULL,
CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
( [ID] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The unique identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The site name of the account in while labels' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SiteName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account Domain for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Domain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Owner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A unique string that can be used to identify the account in login-less situations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'First name of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactFirstName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last name of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactLastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone # of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email address of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the header logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'HeaderLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the login logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'LoginLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the email logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'EmailLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Selected theme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Theme'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom color value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomColor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default currency type of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CurrencyFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default locale of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Locale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time zone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'TimeZone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date format of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'DateFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The time format of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'TimeFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Global Price Plan Discount for this accounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'GPPDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This account need subscription plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'NeedSubscriptionPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If monthly limit is exceeded then charge cost per proof' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ChargeCostPerProofIfExceeded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Billing plan of this accounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SelectedBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Billing will happen monthly?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsMonthlyBillingFrequency'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Billing anniversary date for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'BillingAnniversaryDate'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Billing anniversary month for this account, if frequency is yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'BillingAnniversaryMonth'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract period for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContractPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many child accounts this account can have?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ChildAccountsVolume'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom from address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomFromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom from name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomFromName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The reason this account was suspended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SuspendReason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suspended or deleted by parent?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsSuspendedOrDeletedByParent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The custom Domain for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomDomain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this account has coustom domain?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsCustomDomainActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hide the logo in the footer?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHideLogoInFooter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hide the logo in the login footer?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHideLogoInLoginFooter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is need profile management for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsNeedProfileManagement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Need PDFs to be color managed for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsNeedPDFsToBeColorManaged'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is remove all GMGCollaborate branding for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsRemoveAllGMGCollaborateBranding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Log enabled for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsEnabledLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User who created the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date the Account was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TheUser who last modified this Account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date the Account was last modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Help center is avaialble for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHelpCentreActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dealer number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'DealerNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The customer number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomerNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The VAT number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'VATNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is agreed to the terms and conditions in welcome page when activating the account?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsAgreedToTermsAndConditions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract start date of this account?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContractStartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts region' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this account created temporaryly?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsTemporary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Theme]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Theme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_Theme] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Theme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Theme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountAccountSetting]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountAccountSetting](
	[Account] [int] NOT NULL,
	[AccountSetting] [int] NOT NULL,
CONSTRAINT [PK_AccountAccountSetting] PRIMARY KEY CLUSTERED 
(
	[Account] ASC,
	[AccountSetting] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountAccountSetting', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountAccountSetting', @level2type=N'COLUMN',@level2name=N'AccountSetting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountAccountSetting'
GO **/
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountHelpCentre]    Script Date: 06/14/2012 13:55:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccountHelpCentre](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[ContactEmail] [nvarchar](64) NOT NULL,
	[ContactPhone] [nvarchar](32) NULL,
	[SupportDays] [nvarchar](128) NULL,
	[SupportHours] [nvarchar](64) NULL,
	[UserGuideLocation] [nvarchar](256) NULL,
	[UserGuideName] [nvarchar](256) NULL,
	[UserGuideUpdatedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_AccountHelpCentre] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account which this help center details are belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center email address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center contact number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center open days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'SupportDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center open hours' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'SupportHours'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide location' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide updated date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideUpdatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountPrePressFunction]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPrePressFunction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[PrePressFunction] [int] NOT NULL,
 CONSTRAINT [PK_AccountPrePressFunction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the PrePressFunction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction', @level2type=N'COLUMN',@level2name=N'PrePressFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ValueDataType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValueDataType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_ValueDataType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the DataType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the DataType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountSetting]    Script Date: 02/23/2012 15:06:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] int NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Value] [nvarchar](MAX) NOT NULL,
	[ValueDataType] int NOT NULL,
	[Description] [nvarchar](512) NULL,
CONSTRAINT [PK_AccountSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account of the account setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the account setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the value of the account setting as a string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value''s data type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'ValueDataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A description of the Account Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountStatus]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AccountStatus] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the AccountType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the AccountType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[PrePressFunction]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PrePressFunction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_PrePressFunction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the PrePress Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the PrePress Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Folder]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Parent] [int] NULL,
	[Account] [int] NOT NULL,
	--[SelectedAccessMode] int NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,	
	[ModifiedDate] [datetime2](7) NOT NULL,	
	[IsDeleted] [bit] NOT NULL,
	--[IsArchived] [bit] NOT NULL DEFAULT(0),
	[IsPrivate] [bit] NOT NULL,
CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Collaborator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The parent folder of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Account'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to selected access mode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'SelectedAccessMode'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The creator of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The created date of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The modifier of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The modified date of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder is deleted?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder is archived?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsArchived'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this private folder?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsPrivate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalDecision]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
	[Description] [nvarchar](128) NULL,
CONSTRAINT [PK_ApprovalDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[JobStatus]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JobStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_JobStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Job]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Job](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Title] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
	[Account] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Title of this Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Status of this Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account associated with the Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The status modified date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Approval]		******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Approval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorMessage] [nvarchar](512) NULL,
	[FileName] [nvarchar](128) NOT NULL,
	[FolderPath] [nvarchar](512) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Job] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[Deadline] [datetime2](7) NULL,
	--[Account] [int] NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsPageSpreads] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LockProofWhenAllDecisionsMade] [bit] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[OnlyOneDecisionRequired] [bit] NOT NULL,
	[PrimaryDecisionMaker] [int] NULL,
	[AllowDownloadOriginal] [bit] NOT NULL,
	[FileType] [int] NOT NULL,
	[Size] [decimal] (18,2) NOT NULL,
	[Owner] [int] NOT NULL,
	[IsError] [bit] NOT NULL,
CONSTRAINT [PK_Approval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Error Message of the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ErrorMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the uploaded file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file folder path' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FolderPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A uniquely identifying string that can be used to retrive the Material in login-less scenarios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Job ID of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Job'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version number of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this Approval was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who created this Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime by which this Approval should be completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Deadline'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Company associated with the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Account'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who last modified the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the Approval was last modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, Page spreads is et in Flex component' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to if this approval is deleted or not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsPageSpreads'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lock the proof when all decisions are made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'LockProofWhenAllDecisionsMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The proof is in locked state' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsLocked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Only one decision is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'OnlyOneDecisionRequired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary decision maker, if only one decion is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'PrimaryDecisionMaker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allow users to download the original' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'AllowDownloadOriginal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FileType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file size' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Size'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Owner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Proof is in Error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsError'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalPage]    Script Date: 02/19/2012 22:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalPage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NOT NULL,
	[Approval] [int] NOT NULL,
	[PageSmallThumbnailHeight] [int] NOT NULL,
	[PageSmallThumbnailWidth] [int] NOT NULL,
	[PageLargeThumbnailHeight] [int] NOT NULL,
	[PageLargeThumbnailWidth] [int] NOT NULL,
	[DPI] [int] NOT NULL,
	[Progress] [int] NOT NULL
CONSTRAINT [PK_ApprovalPage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page number', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval to which this ApprovalPage belongs', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page small thumbnail height', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageSmallThumbnailHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page small thumbnail width', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageSmallThumbnailWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page large thumbnail height', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageLargeThumbnailHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page large thumbnail width', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageLargeThumbnailWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resolution of the file', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'DPI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Progress of the file', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Progress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalUserViewInfo]    Script Date: 02/19/2012 22:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalUserViewInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[User] [int] NOT NULL,
	[ViewedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalUserViewInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval that user has been viewed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Viewed date and time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'ViewedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCommentType]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCommentType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_ApprovalCommentType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationType]    Script Date: 02/19/2012 22:41:39 ******/
/** SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_ApprovalAnnotationType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType'
GO **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FileType]    Script Date: 04/02/2012 14:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Extention] [nvarchar](8) NOT NULL,
	[Name] [nvarchar](64) NULL,
 CONSTRAINT [PK_FileType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The extention of the FileType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'Extention'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the FileType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotation]    Script Date: 02/19/2012 22:51:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[Parent] [int] NULL,
	[Page] [int] NOT NULL,
	[OrderNumber] [int] NULL,
	[Creator] [int] NULL,
	[Modifier] [int] NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Comment] [nvarchar](MAX) NOT NULL,
	[ExternalCollaborator] [int] NULL,
	[ReferenceFilepath] [nvarchar](512) NULL,
	[AnnotatedDate] [datetime2](7) NOT NULL,
	--[Type] [int] NULL,
	[CommentType] [int] NOT NULL,
	[StartIndex] [int] NULL,
	[EndIndex] [int] NULL,
	[HighlightType] [int] NULL,
	[CrosshairXCoord] [int] NULL,
	[CrosshairYCoord] [int] NULL,
	[HighlightData] [nvarchar](MAX) NULL,
	[IsPrivate] [bit] NOT NULL,
CONSTRAINT [PK_ApprovalAnnotation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Comment status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Parent Approval Comment of this Approval Comment if any' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval Page that contains this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Page'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Order number of the Annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'OrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Collaborator who made this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Collaborator who modify this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date that this comment has been modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The textual content of the Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TODO: remove this property' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ReferenceFilepath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this Approval Comment was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'AnnotatedDate'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of the Annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Type'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of the comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CommentType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start index for text metric of annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'StartIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End index for text metric of annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'EndIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'HighlightType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crosshair X Coordinate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CrosshairXCoord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crosshair Y Coordinate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CrosshairYCoord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'HighlightData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this private annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'IsPrivate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[HighlightType]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HighlightType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_HighlightType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this highlight type will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationPrivateCollaborator]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationPrivateCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalAnnotationPrivateCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationAttachment]    Script Date: 04/02/2012 14:52:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileType] [int] NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ApprovalAnnotationAttachment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The FileType id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'FileType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The display name of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationTextPrintData]    Script Date: 04/02/2012 14:52:30 ******/
/*SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationTextPrintData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Data] [varbinary](MAX) NULL,
 CONSTRAINT [PK_ApprovalAnnotationTextPrintData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData', @level2type=N'COLUMN',@level2name=N'Data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData'
GO*/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Shape]    Script Date: 04/02/2012 14:52:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Shape](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IsCustom] [bit] NOT NULL,
	[FXG] [nvarchar](MAX) NULL,
	[SVG] [nvarchar](MAX) NULL,
 CONSTRAINT [PK_Shape] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this custom shape?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The FXG of the shape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'FXG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'SVG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationMarkup]    Script Date: 04/02/2012 14:34:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationMarkup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[X] [int] NOT NULL,
	[Y] [int] NOT NULL,
	[Width] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[Color] [nvarchar](8) NULL,
	[Size] [int] NULL,
	[BackgroundColor] [nvarchar](8) NULL,
	[Rotation] [int] NULL,
	--[FXG] [nvarchar](64) NULL,
	[Shape] [int] NULL,
	--[StrokeColor] [nvarchar](8) NULL,
	--[StokeAlpha] [nvarchar](8) NULL,
	--[StrokeWeight] [int] NULL,
	--[FillColor] [nvarchar](8) NULL,
	--[FillAlpha] [nvarchar](8) NULL,
 CONSTRAINT [PK_ApprovalAnnotationTypeMarkup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the comment markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval Comment that the markup belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The X ordinate of the origin of the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'X'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Y ordinate of the origin of the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Y'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Width if the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Width'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Height if the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Height'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The color of the text control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Color'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The size of the font' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Size'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The backgroundcolor of the text control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'BackgroundColor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rotaion value of the shape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Rotation'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'FXG'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shape information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Shape'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'StrokeColor'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'StokeAlpha'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'StrokeWeight'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'FillColor'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'FillAlpha'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationStatus]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_ApprovalAnnotationStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaborator]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[ApprovalCollaboratorRole] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval collaborator role id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator assigned to this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaboratorGroup]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FolderCollaborator]    Script Date: 04/16/2012 14:36:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FolderCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Folder] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	--[CollaboratorGroup] [int] NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_FolderCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Folder id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator assigned date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FolderCollaboratorGroup]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FolderCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Folder] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_FolderCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FolderApproval]    Script Date: 04/16/2012 14:52:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FolderApproval](
	[Folder] [int] NOT NULL,
	[Approval] [int] NOT NULL,
 CONSTRAINT [PK_FolderApproval] PRIMARY KEY CLUSTERED 
(
	[Folder] ASC,
	[Approval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaboratorDecision]    Script Date: 04/16/2012 16:05:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaboratorDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Decision] [int] NULL,
	[Collaborator] [int] NULL,
	[ExternalCollaborator] [int] NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
	[OpenedDate] [datetime2](7) NULL,
	[CompletedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_ApprovalCollaboratorDecisionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval History record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval to which the History record belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Decision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The assigned user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time is assigend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time is opened' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'OpenedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time this decision has been completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'CompletedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[LogModule]    Script Date: 04/16/2012 16:19:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogModule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_LogModule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the LogModule' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[LogAction]    Script Date: 04/16/2012 16:19:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[LogModule] [int] NOT NULL,
	[Message] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_LogAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The LogModule of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'LogModule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The action message with formatted info string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[LogApproval]    Script Date: 04/16/2012 16:19:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[LogAction] [int] NOT NULL,
	[ActualMessage] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_LogApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The log action id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'LogAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'ActualMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--


/****** Object:  Table [dbo].[ApprovalHistory]    Script Date: 02/19/2012 22:57:45 ******/
/* SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Decision] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[DateCreated] [datetime2](7) NULL,
	[Action] [int] NOT NULL,
	[ActionMessage] [nvarchar](128) NULL,
	[ActionData] [nvarchar](128) NULL,
CONSTRAINT [PK_ApprovalHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Advanced Approval History record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Advanced Approval to which the History record belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the status whic hwas applied to the approval at the time this record was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Decision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who triggered the creation of this history record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time this history record was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'DateCreated'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the action that was applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The message for the action associated with this history' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'ActionMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Any further data that needs to be attached to the history record fior the action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'ActionData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory'
GO   */

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AttachedAccountBillingPlan]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttachedAccountBillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[BillingPlan] [int] NOT NULL,
	[NewPrice] [decimal](9,2) NULL,
	[IsAppliedCurrentRate] [bit] NOT NULL,
	[NewProofPrice] [decimal](9,2) NULL,
	[IsModifiedProofPrice] [bit] NOT NULL,
CONSTRAINT [PK_AttachedAccountBillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO	
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Attached Account Billing Plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account id this billing plan is assigned to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The billing plan of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The billing monthly plan new currency type value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'NewPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New value is based on the current rate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'IsAppliedCurrentRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The single proof price based on new currency type value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'NewProofPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User has been modified the proof price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'IsModifiedProofPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[BillingAnniversaryDate]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingAnniversaryDate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK_BillingAnniversaryDate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Anniversary Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the Billing Anniversary Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate'
GO  **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[InvoivePeriod]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoivePeriod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
CONSTRAINT [PK_InvoivePeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the Billing Frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod'
GO **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[BillingPlan]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Type] [int] NOT NULL,
	[Proofs] [int] NULL,
	[Price] [decimal](9,2) NOT NULL,
	[IsFixed] [bit] NOT NULL,
CONSTRAINT [PK_BillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Proof count allow by this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Proofs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The price of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this fixed plan?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'IsFixed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[BillingPlanType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingPlanType](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Key] [nvarchar](64) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,	
	[MediaService] [int] NOT NULL,
	[EnablePrePressTools] [bit] NOT NULL,
	[EnableMediaTools] [bit] NOT NULL,
CONSTRAINT [PK_BillingPlanType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The media service attached to this Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'MediaService'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enable pre press tools for this plan type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'EnablePrePressTools'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enable media tools for this Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'EnableMediaTools'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountBillingPlanChangeLog]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBillingPlanChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[LogMessage] [nvarchar](256) NOT NULL,
	[PreviousPlanAmount] [decimal](9,2) NULL,
	[PreviousProofsAmount] [int] NULL,
	[GPPDiscount] [decimal](3,2) NULL,
	[PreviousPlan] [int] NULL,
	[CurrentPlan] [int] NULL,
CONSTRAINT [PK_AccountBillingPlanChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'LogMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousPlanAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous proof amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousProofsAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GPPDiscount of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'GPPDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current plan id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'CurrentPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountBillingTransactionHistory]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBillingTransactionHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[BaseAmount] [decimal](9,2) NOT NULL,
	[DiscountAmount] [decimal](9,2) NOT NULL,
CONSTRAINT [PK_AccountBillingTransactionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'BaseAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Company]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[Number] [nvarchar](32) NULL,
	[Address1] [nvarchar](128) NOT NULL,
	[Address2] [nvarchar](128) NULL,
	[City] [nvarchar](64) NOT NULL,
	[Postcode] [nvarchar](20) NULL,
	[State] [nvarchar](20) NULL,
	[Phone] [nvarchar](20) NOT NULL,
	[Mobile] [nvarchar](20) NULL,
	[Email] [nvarchar](64) NULL,
	[Country] [int] NOT NULL,
CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account to which the Company belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ABN/ACN or similar of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address1 of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address2 of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The city of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The postcode of eh Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Postcode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The state of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The phone number of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mobile number of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Mobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The email address of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Country of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Country'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ContractPeriod]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractPeriod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ContractPeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Contract Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Contract Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod'
GO 

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ControllerAction]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControllerAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Controller] [nvarchar](128) NOT NULL,
	[Action] [nvarchar](128) NULL,
	[Parameters] [nvarchar](128) NULL,
CONSTRAINT [TBL_ControllerAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Controller' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Controller'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Controller Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Parameeters of the Controller Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Parameters'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Country]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Iso2] [nvarchar](2) NOT NULL,
	[Iso3] [nvarchar](3) NOT NULL,
	[IsoCountryNumber] [int] NOT NULL,
	[DialingPrefix] [int] NULL,
	[Name] [nvarchar](64) NOT NULL,
	[ShortName] [nvarchar](50) NOT NULL,
	[HasLocationData] [bit] NOT NULL,
CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO2 code for the country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Iso2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO3 code for the Country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Iso3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO Country number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'IsoCountryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dialing prefix for the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'DialingPrefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Country''s name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Country''s short name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'ShortName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'true if the country has location data in the Location table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'HasLocationData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Currency]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Code] [nvarchar](4) NOT NULL,
	[Symbol] [nvarchar](5) NULL,
	--[Country] [int] NULL,
CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Symbol'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The country of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Country'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[DateFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DateFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pattern] [nvarchar](16) NOT NULL,
	[Result] [nvarchar](16) NOT NULL,
CONSTRAINT [PK_DateFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The pattern of the date format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'Pattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sample result of the pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[TimeFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar] (4) NOT NULL,
	[Pattern] [nvarchar](16) NOT NULL,
	[Result] [nvarchar](16) NOT NULL,
CONSTRAINT [PK_TimeFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the time format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The pattern of the time format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Pattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sample result of the pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[CurrencyRate]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [int] NOT NULL,
	[Rate] [decimal](10,2) NOT NULL,
	[Creator] [int] NOT NULL,
	[Modifier] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_CurrencyRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Rate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Locale]    Script Date: 11/02/2010 20:42:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Locale](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](16) NULL,
	[DisplayName] [nvarchar](64) NULL,
CONSTRAINT [PK_Locale] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Locale display name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[MenuItem]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ControllerAction] [int] NOT NULL,	
	[Parent] [int] NULL,
	[Position] [int] NOT NULL,		
	[IsAdminAppOwned] [bit] NULL,
	[IsAlignedLeft] [bit] NOT NULL,
	[IsSubNav] [bit] NOT NULL,
	[IsTopNav] [bit] NOT NULL,	
	[IsVisible] [bit] NOT NULL,	
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,	
	[Title] [nvarchar](128) NOT NULL,
CONSTRAINT [PK_MenuItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'ControllerAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to weather manu item is aligned to left or right?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsAlignedLeft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent of the Menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item position' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Position'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item visible?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsVisible'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item is placed inside main or top level menu?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsTopNav'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item is visible for Admin App?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsAdminAppOwned'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is sub navigation?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsSubNav'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The title of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[MenuItemNameLocale]    Script Date: 11/02/2010 20:42:40 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[MenuItemNameLocale](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Locale] [int] NOT NULL,
--	[MenuItem] [int] NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,	
--	[Title] [nvarchar](128) NOT NULL,
--CONSTRAINT [PK_MenuItemNameLocale] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Locale Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'Locale'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Menu item Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'MenuItem'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the menu based on the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Titile of the menu based on the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'Title'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale'
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[MenuItemAccountTypeRole]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItemAccountTypeRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MenuItem] [int] NOT NULL,	
	[AccountTypeRole] [int] NOT NULL,
CONSTRAINT [PK_MenuItemAccountTypeRole] PRIMARY KEY CLUSTERED 
( [ID] ASC )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'MenuItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'AccountTypeRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[NotificationFrequency]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationFrequency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	--[IsCustom] [bit] NOT NULL,
	--[Frequency] [nvarchar](256) NULL,
CONSTRAINT [PK_NotificationFrequency] PRIMARY KEY CLUSTERED 
( [ID] ASC )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the NotificationFrequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the NotificationFrequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Name'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is custom frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'IsCustom'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The frequency, if this is a custom one' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Frequency'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[NotificationEmailQueue]    Script Date: 02/23/2012 15:06:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationEmailQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryRecipient] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Creator] [int] NULL,
	[SecondaryRecipient] [int] NULL,
	[ExternalCollaborator] [int] NULL,
	[BillingPlan1] [int] NULL,
	[BillingPlan2] [int] NULL,
	[Account] [int] NULL,
	[Approval] [int] NULL,
	[Folder] [int] NULL,
	[UserGroup] [int] NULL,
	[Header] [nvarchar](max) NOT NULL,
	[Body] [nvarchar](max) NULL,
	[IsEmailSent] [bit] NOT NULL,
	[SentDate] [datetime2](7) NULL,
	[CreatedDate] [datetime] NOT NULL,
CONSTRAINT [PK_NotificationEmailQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PrimaryRecipient of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'PrimaryRecipient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EventType of the NotificationEmailQueue as a string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'EventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SecondaryRecipient of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'SecondaryRecipient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ExternalCollaborator of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BillingPlan1 of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'BillingPlan1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BillingPlan2 of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'BillingPlan2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Approval of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Folder of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UserGroup of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'UserGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Header of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Header'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Body of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Body'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IsEmailSent or not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'IsEmailSent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SentDate of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'SentDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CreatedDate of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Role]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The order to be shown on UIs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[User]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Username] [nvarchar](128) NOT NULL,
	[Password] [varchar](64) NOT NULL,
	[GivenName] [nvarchar](64) NOT NULL,
	[FamilyName] [nvarchar](64) NOT NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[PhotoPath] [nvarchar](256) NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[MobileTelephoneNumber] [nvarchar](20) NULL,
	[HomeTelephoneNumber] [nvarchar](20) NULL,
	[OfficeTelephoneNumber] [nvarchar](20) NOT NULL,
	[NotificationFrequency] [int] NOT NULL,
	[DateLastLogin] [datetime2](7) NULL,
	--[IsActive] [bit] NOT NULL,
	--[IsDeleted] [bit] NOT NULL DEFAULT(0),
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Preset] [int] NOT NULL,
	[IncludeMyOwnActivity] [bit] NOT NULL,
CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
( [ID] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account that this user belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The username used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The password used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Photo of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'PhotoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The guid associated with this user that can be used instead of authentication under certain circumstances' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mobile telephone number number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'MobileTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The home telephone number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'HomeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The office telephone number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'OfficeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Notification frequency to send emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this user last authenticated with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'DateLastLogin'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is active and is able to authenticate with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IsActive'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is deleted and is no longer available to the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IsDeleted'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who last modified this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was last updated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preset level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Include my own activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IncludeMyOwnActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserControllerActionAccess]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserControllerActionAccess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ControllerAction] [int] NOT NULL,
	[MenuItem] [int] NOT NULL,
	[User] [int] NOT NULL,
CONSTRAINT [PK_UserControllerActionAccess] PRIMARY KEY CLUSTERED 
( [ID] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This user can be aaccessed this page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'ControllerAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item of the page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'MenuItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This page can be accessed by this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess'
GO  **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserGroup]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	--[Description] [nvarchar](256) NULL,
	[Account] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the UserGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the UserGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Description'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account id of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modifier of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modified date id of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserGroupUser]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupUser](
	[UserGroup] [int] NOT NULL,
	[User] [int] NOT NULL,
CONSTRAINT [PK_UserGroupUser] PRIMARY KEY CLUSTERED 
(
	[UserGroup] ASC,
	[User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser', @level2type=N'COLUMN',@level2name=N'UserGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserHistory]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Company] [int] NOT NULL,
	[Username] [nvarchar](32) NOT NULL,
	[Password] [varchar](255) NULL,
	[GivenName] [nvarchar](32) NULL,
	[FamilyName] [nvarchar](32) NOT NULL,
	[EmailAddress] [nvarchar](64) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
CONSTRAINT [PK_UserHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User to which this History pertains' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The company that this User belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The username used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The password used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is active and is able to authenticate with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'IsActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is deleted and is no longer available to the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserLogin]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[IpAddress] [nvarchar](16) NOT NULL,
	[Success] [bit] NOT NULL,
	[DateLogin] [datetime2](7) NOT NULL,
	[DateLogout] [datetime2](7) NULL,
	--[SessionId] [nvarchar](50) NOT NULL,
CONSTRAINT [PK_UserLogin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who created this UserLogin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The IP address that the User logged in from' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'IpAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether or not the login attempt was successful' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'Success'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user attempted to login' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'DateLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user logged out (if any)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'DateLogout'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserRole]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Role] [int] NOT NULL,
CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO	
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountTypeRole]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountTypeRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [int] NOT NULL,
	[Role] [int] NOT NULL,
CONSTRAINT [PK_AccountTypeRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserStatus]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_UserStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[StudioSettingType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudioSettingType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_StudioSettingType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Studio Setting Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Studio Setting Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[HowOftenReceiveEmails]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[HowOftenReceiveEmails](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,
--	[Description] [nvarchar](256) NULL,
--CONSTRAINT [PK_HowOftenReceiveEmails] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the How Often Receive Emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the How Often Receive Emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails', @level2type=N'COLUMN',@level2name=N'Description'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[CommentEmailSendDecision]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentEmailSendDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_CommentEmailSendDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Comment Email Send Decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Comment Email Send Decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[StudioSetting]    Script Date: 04/11/2012 21:01:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudioSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [int] NOT NULL,
	[User] [int] NULL,
	[NotificationFrequency] [int] NOT NULL,
	[IncludeMyOwnActivity] [bit] NOT NULL,
	[WhenCommentMade] [int] NOT NULL,
	[RepliesMyComment] [int] NOT NULL,
	[NewVesionAdded] [int] NOT NULL,
	[FinalDecisionMade] [int] NOT NULL,
	[EnableColorCorrection] [bit] NOT NULL,
 CONSTRAINT [PK_StudioSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the studio Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user belongs to these setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notification frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Include my own activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'IncludeMyOwnActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When a comment is made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'WhenCommentMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Replies to my comments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'RepliesMyComment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New version added' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'NewVesionAdded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Final decisions made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'FinalDecisionMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disable or enable color correction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'EnableColorCorrection'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Preset]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_Preset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Preset' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Preset' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[EventGroup]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_EventGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the EventGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the EventGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[EventType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventGroup] [int] NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The EventGroup of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'EventGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[RolePresetEventType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePresetEventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [int] NOT NULL,
	[Preset] [int] NOT NULL,
	[EventType] [int] NOT NULL,
CONSTRAINT [PK_RolePresetEventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Role of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Preset of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The EventType of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'EventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserPresetEventType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCustomPresetEventTypeValue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[RolePresetEventType] [int] NOT NULL,
	[Value] [bit] NOT NULL,
CONSTRAINT [PK_UserCustomPresetEventTypeValue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The RolePresetEventType of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'RolePresetEventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Value of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ChangedBillingPlan]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangedBillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[BillingPlan] [int] NULL,
	[AttachedAccountBillingPlan] [int] NULL,	
 CONSTRAINT [PK_ChangedBillingPlan] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'AttachedAccountBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ChangedBillingPlan]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPricePlanChangedMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedBillingPlan] [int] NOT NULL,
	[ChildAccount] [int] NOT NULL,
 CONSTRAINT [PK_AccountPricePlanChangedMessage] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ChangedBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ChildAccount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--


/****** Object:  Table [dbo].[Smoothing]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Smoothing](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_Smoothing] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Smoothing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Smoothing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Colorspace]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colorspace](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_Colorspace] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Colorspace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Colorspace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ImageFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImageFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Parent] [int] NULL,
CONSTRAINT [PK_ImageFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The parent of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[PageBox]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageBox](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_PageBox] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Page Box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Page Box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[TileImageFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TileImageFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_TileImageFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Tile Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Tile Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[MediaService]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MediaService](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[ApplySimulateOverprinting] [bit] NOT NULL,
	[Smoothing] [int] NOT NULL,
	[Resolution] [int] NOT NULL,
	[Colorspace] [int] NOT NULL,
	[ImageFormat] [int] NOT NULL,
	[PageBox] [int] NOT NULL,
	[TileImageFormat] [int] NOT NULL,
	[JPEGCompression] [int] NOT NULL,
CONSTRAINT [PK_MediaService] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apply the simulate overprinting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ApplySimulateOverprinting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The smoothing attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Smoothing'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The resolution attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Resolution'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The colorspace attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Colorspace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The image format attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page box attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'PageBox'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The tile image format attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'TileImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The jpeg compression attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'JPEGCompression'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalSeparationPlate]    Script Date: 11/08/2012 11:27:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalSeparationPlate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Page] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_ApprovalSeparationPlate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval page Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'Page'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaboratorRole]    Script Date: 11/21/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaboratorRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_ApprovalCollaboratorRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The order to be shown on UIs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ExternalCollaborator]    Script Date: 11/21/2012 10:10:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[ExternalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[GivenName] [nvarchar](64) NULL,
	[FamilyName] [nvarchar](64) NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[DateLoggedIn] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ExternalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account that this user belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The guid associated with this user that can be used instead of authentication under certain circumstances' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this user last authenticated with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'DateLoggedIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who modified this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the activeness of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[SharedApproval]    Script Date: 11/21/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SharedApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[ExternalCollaborator] [int] NOT NULL,
	[ApprovalCollaboratorRole] [int] NOT NULL,
	[SharedDate] [datetime2](7) NOT NULL,
	[IsSharedURL] [bit] NOT NULL,
	[IsSharedDownloadURL] [bit] NOT NULL,
	[IsExpired] [bit] NOT NULL,
	[ExpireDate] [datetime2](7) NOT NULL,
	[Notes] [nvarchar](512) NULL,
 CONSTRAINT [PK_SharedApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external users role for this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The shared date of this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'SharedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can share the url with external user?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsSharedURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can share the download url with external user?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsSharedDownloadURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The this share expired?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsExpired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The expire date of this share' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notes of this share' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ReservedDomainName]    Script Date: 01/24/2013 14:45:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReservedDomainName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ReservedDomainName] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the reserved domain name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[NativeDataType]    Script Date: 03/04/2013 22:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NativeDataType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar] (64) NOT NULL,
 CONSTRAINT [PK_NativeDataType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType'
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  Table [dbo].[GlobalSettings]    Script Date: 03/04/2013 22:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GlobalSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](64) NOT NULL,
	[DataType] [int] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_GlobalSettings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the global settings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dta type of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'DataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The value of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings'
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  Table [dbo].[Variation]    Script Date: 11/08/2012 11:49:06 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Variation](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Key] [nvarchar](4) NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,
-- CONSTRAINT [PK_Variations] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Variations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation', @level2type=N'COLUMN',@level2name=N'Key'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Variations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[VariationPlate]    Script Date: 11/08/2012 11:49:06 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[VariationPlate](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Variation] [int] NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,
-- CONSTRAINT [PK_VariationPlate] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Variation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate', @level2type=N'COLUMN',@level2name=N'Variation'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Variation Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAdditionalPlate]    Script Date: 11/08/2012 11:27:34 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalAdditionalPlate](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Approval] [int] NOT NULL,
--	[Name] [nvarchar](128) NOT NULL,
-- CONSTRAINT [PK_AdditionalPlate] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate', @level2type=N'COLUMN',@level2name=N'Approval'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the additional separation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalVariationPlate]    Script Date: 11/08/2012 11:27:34 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalVariationPlate](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Approval] [int] NOT NULL,
--	[VariationPlate]  [int] NOT NULL,
--	[Name] [nvarchar](128) NOT NULL,
-- CONSTRAINT [PK_ApprovalVariationPlate] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate', @level2type=N'COLUMN',@level2name=N'Approval'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Variation Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate', @level2type=N'COLUMN',@level2name=N'VariationPlate'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[AccountVolume]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[AccountVolume](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Min] [int] NOT NULL,
--	[Max] [int] NOT NULL,
--CONSTRAINT [PK_AccountVolume] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimun value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume', @level2type=N'COLUMN',@level2name=N'Min'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume', @level2type=N'COLUMN',@level2name=N'Max'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume'
--GO

--/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[AccountTypeAccountVolume]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[AccountTypeAccountVolume](
--	[AccountType] [int] NOT NULL,
--	[AccountVolume] [int] NOT NULL,
--CONSTRAINT [PK_AccountTypeAccountVolume] PRIMARY KEY CLUSTERED 
--(
--	[AccountType] ASC,
--	[AccountVolume] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeAccountVolume', @level2type=N'COLUMN',@level2name=N'AccountType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account volume' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeAccountVolume', @level2type=N'COLUMN',@level2name=N'AccountVolume'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeAccountVolume'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountUser]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[AccountUser](
--	[Account] [int] NOT NULL,
--	[User] [int] NOT NULL,
--CONSTRAINT [PK_AccountUser] PRIMARY KEY CLUSTERED 
--(
--	[Account] ASC,
--	[User] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountUser', @level2type=N'COLUMN',@level2name=N'Account'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountUser', @level2type=N'COLUMN',@level2name=N'User'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountUser'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalApprover]    Script Date: 02/19/2012 22:38:14 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalApprover](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Approval] [int] NULL,
--	[Approver] [int] NULL,
--	[DateAssigned] [datetime2](7) NULL,
--	[DateFirstViewing] [datetime2](7) NULL,
--	[Approved] [int] NULL,
--	[IsReviewed] [bit] NULL,
--	[DateReviewed] [datetime2](7) NULL,
--CONSTRAINT [PK_ApprovalApprover] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval that this Approval Approver belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'Approval'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User that is represented by this Approval Approver' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'Approver'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this ApprovalApprover was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'DateAssigned'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTIme this ApprovalApprover first viewed the associated Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'DateFirstViewing'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this ApprovalApprover has approved the associated Approval. If false, they have rejected it. If null, no action has been taken' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'Approved'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--
-- CONSTRAINS --
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

-- Account

ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsSuspendedOrDeletedByParent]  DEFAULT ((0)) FOR [IsSuspendedOrDeletedByParent]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsCustomDomainActive]  DEFAULT ((0)) FOR [IsCustomDomainActive]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsHideLogoInFooter]  DEFAULT ((0)) FOR [IsHideLogoInFooter]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsHideLogoInLoginFooter]  DEFAULT ((0)) FOR [IsHideLogoInLoginFooter]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsNeedProfileManagement]  DEFAULT ((0)) FOR [IsNeedProfileManagement]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsNeedPDFsToBeColorManaged]  DEFAULT ((0)) FOR [IsNeedPDFsToBeColorManaged]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsRemoveAllGMGCollaborateBranding]  DEFAULT ((0)) FOR [IsRemoveAllGMGCollaborateBranding]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsEnabledLog]  DEFAULT ((0)) FOR [IsEnabledLog]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsHelpCentreActive]  DEFAULT ((0)) FOR [IsHelpCentreActive]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsAgreedToTermsAndConditions]  DEFAULT ((0)) FOR [IsAgreedToTermsAndConditions]
GO

---- AccountUser

--/****** Object:  ForeignKey [FK_AccountUser_Account]    ******/
--ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD CONSTRAINT [FK_AccountUser_Account] FOREIGN KEY([Account])
--REFERENCES [dbo].[Account] ([ID])
--GO
--ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_Account]
--GO

--/****** Object:  ForeignKey [FK_AccountUser_User]    ******/
--ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD CONSTRAINT [FK_AccountUser_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_User]
--GO

-- MenuItem

/****** Object:  ForeignKey [FK_MenuItem_Page]    ******/
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD CONSTRAINT [FK_MenuItem_ControllerAction] FOREIGN KEY([ControllerAction])
REFERENCES [dbo].[ControllerAction] ([ID])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_ControllerAction]
GO

/****** Object:  Default [DF_MenuItem_IsActive]    ******/
--ALTER TABLE [dbo].[MenuItem] ADD CONSTRAINT [DF_MenuItem_IsActive]  DEFAULT ((0)) FOR [IsActive]
--GO

/****** Object:  Default [DF_MenuItem_IsFeatureEnabled]    ******/
--ALTER TABLE [dbo].[MenuItem] ADD CONSTRAINT [DF_MenuItem_IsFeatureEnabled]  DEFAULT ((0)) FOR [IsFeatureEnabled]
--GO

/****** Object:  ForeignKey [FK_MenuItem_Parent]    ******/
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD CONSTRAINT [FK_MenuItem_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[MenuItem] ([ID])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_Parent]
GO

-- MenuItemNameLocale

--ALTER TABLE [dbo].[MenuItemNameLocale]  WITH CHECK ADD CONSTRAINT [FK_MenuItemNameLocale_MenuItem] FOREIGN KEY([MenuItem])
--REFERENCES [dbo].[MenuItem] ([ID])
--GO
--ALTER TABLE [dbo].[MenuItemNameLocale] CHECK CONSTRAINT [FK_MenuItemNameLocale_MenuItem]
--GO

--ALTER TABLE [dbo].[MenuItemNameLocale]  WITH CHECK ADD CONSTRAINT [FK_MenuItemNameLocale_Locale] FOREIGN KEY([Locale])
--REFERENCES [dbo].[Locale] ([ID])
--GO
--ALTER TABLE [dbo].[MenuItemNameLocale] CHECK CONSTRAINT [FK_MenuItemNameLocale_Locale]
--GO

-- AccountTypeAccountVolume

--/****** Object:  ForeignKey [FK_AccountTypeAccountVolume_AccountType]    ******/
--ALTER TABLE [dbo].[AccountTypeAccountVolume]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeAccountVolume_AccountType] FOREIGN KEY([AccountType])
--REFERENCES [dbo].[AccountType] ([ID])
--GO
--ALTER TABLE [dbo].[AccountTypeAccountVolume] CHECK CONSTRAINT [FK_AccountTypeAccountVolume_AccountType]
--GO

--/****** Object:  ForeignKey [FK_AccountTypeAccountVolume_AccountVolume]    ******/
--ALTER TABLE [dbo].[AccountTypeAccountVolume]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeAccountVolume_AccountVolume] FOREIGN KEY([AccountVolume])
--REFERENCES [dbo].[AccountVolume] ([ID])
--GO
--ALTER TABLE [dbo].[AccountTypeAccountVolume] CHECK CONSTRAINT [FK_AccountTypeAccountVolume_AccountVolume]
--GO

---- UserCompany

--/****** Object:  ForeignKey [FK_UserCompany_User]    ******/
--ALTER TABLE [dbo].[UserCompany]  WITH CHECK ADD CONSTRAINT [FK_UserCompany_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[UserCompany] CHECK CONSTRAINT [FK_UserCompany_User]
--GO

--/****** Object:  ForeignKey [K_UserCompany_Company]    ******/
--ALTER TABLE [dbo].[UserCompany]  WITH CHECK ADD CONSTRAINT [FK_UserCompany_Company] FOREIGN KEY([Company])
--REFERENCES [dbo].[Company] ([ID])
--GO
--ALTER TABLE [dbo].[UserCompany] CHECK CONSTRAINT [FK_UserCompany_Company]
--GO

-- Account

/****** Object:  ForeignKey [FK_Account_AccountType]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountType]
GO

/****** Object:  ForeignKey [FK_Account_Locale]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Locale] FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Locale]
GO

/****** Object:  ForeignKey [FK_Account_Currency]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Currency] FOREIGN KEY([CurrencyFormat])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Currency]
GO

/****** Object:  ForeignKey [FK_Account_DateFormat]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_DateFormat] FOREIGN KEY([DateFormat])
REFERENCES [dbo].[DateFormat] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_DateFormat]
GO

/****** Object:  ForeignKey [FK_Account_TimeFormat]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_TimeFormat] FOREIGN KEY([TimeFormat])
REFERENCES [dbo].[TimeFormat] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_TimeFormat]
GO

/****** Object:  ForeignKey [FK_Account_Creator]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Creator]
GO

/****** Object:  ForeignKey [FK_Account_Parent]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Parent]
GO

/****** Object:  ForeignKey [FK_Account_Modifier]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Modifier]
GO

/****** Object:  ForeignKey [FK_Account_Owner]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Owner]
GO

/****** Object:  ForeignKey [FK_Account_BillingPlan]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_BillingPlan] FOREIGN KEY([SelectedBillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_BillingPlan]
GO

/****** Object:  ForeignKey [FK_Account_BillingAnniversaryDate]    ******/
/** ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_BillingAnniversaryDate] FOREIGN KEY([BillingAnniversaryDate])
REFERENCES [dbo].[BillingAnniversaryDate] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_BillingAnniversaryDate]
GO **/

/****** Object:  ForeignKey [FK_Account_ContractPeriod]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_ContractPeriod] FOREIGN KEY([ContractPeriod])
REFERENCES [dbo].[ContractPeriod] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_ContractPeriod]
GO

/** ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_InvoivePeriod] FOREIGN KEY([InvoivePeriod])
REFERENCES [dbo].[InvoivePeriod] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_InvoivePeriod]
GO **/

ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_AccountStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[AccountStatus] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountStatus]
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Theme] FOREIGN KEY([Theme])
REFERENCES [dbo].[Theme] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Theme]
GO

-- AccountHelpCentre

ALTER TABLE [dbo].[AccountHelpCentre]  WITH CHECK ADD  CONSTRAINT [FK_AccountHelpCentre_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountHelpCentre] CHECK CONSTRAINT [FK_AccountHelpCentre_Account]
GO

-- AccountSetting

ALTER TABLE [dbo].[AccountSetting]  WITH CHECK ADD CONSTRAINT [TBL_AccountSetting_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountSetting] CHECK CONSTRAINT [TBL_AccountSetting_Account]
GO

ALTER TABLE [dbo].[AccountSetting]  WITH CHECK ADD CONSTRAINT [TBL_AccountSetting_ValueDataType] FOREIGN KEY([ValueDataType])
REFERENCES [dbo].[ValueDataType] ([ID])
GO

ALTER TABLE [dbo].[AccountSetting] CHECK CONSTRAINT [TBL_AccountSetting_ValueDataType]
GO 

-- AccountPrePressFunction

ALTER TABLE [dbo].[AccountPrePressFunction]  WITH CHECK ADD CONSTRAINT [TBL_AccountPrePressFunction_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountPrePressFunction] CHECK CONSTRAINT [TBL_AccountPrePressFunction_Account]
GO

ALTER TABLE [dbo].[AccountPrePressFunction]  WITH CHECK ADD CONSTRAINT [TBL_AccountPrePressFunction_PrePressFunction] FOREIGN KEY([PrePressFunction])
REFERENCES [dbo].[PrePressFunction] ([ID])
GO

ALTER TABLE [dbo].[AccountPrePressFunction] CHECK CONSTRAINT [TBL_AccountPrePressFunction_PrePressFunction]
GO

-- AccountBillingPlanChangeLog

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_Account]
GO

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_Creator]
GO

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_PreviousPlan] FOREIGN KEY([PreviousPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_PreviousPlan]
GO

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_CurrentPlan] FOREIGN KEY([CurrentPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_CurrentPlan]
GO

-- AccountBillingTransactionHistory

ALTER TABLE [dbo].[AccountBillingTransactionHistory]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingTransactionHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory] CHECK CONSTRAINT [FK_AccountBillingTransactionHistory_Account]
GO

ALTER TABLE [dbo].[AccountBillingTransactionHistory]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingTransactionHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory] CHECK CONSTRAINT [FK_AccountBillingTransactionHistory_Creator]
GO

-- Company

/****** Object:  ForeignKey [FK_Company_Account]    ******/
ALTER TABLE [dbo].[Company]  WITH CHECK ADD CONSTRAINT [FK_Company_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Account]
GO

/****** Object:  ForeignKey [FK_Company_BillingCountry]    ******/
ALTER TABLE [dbo].[Company]  WITH CHECK ADD CONSTRAINT [FK_Company_Country] FOREIGN KEY([Country])
REFERENCES [dbo].[Country] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Country]
GO

-- MenuItemAccountTypeRole

/****** Object:  ForeignKey [FK_MenuItemAccountTypeRole_MenuItem]    ******/
ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_MenuItemAccountTypeRole_MenuItem] FOREIGN KEY([MenuItem])
REFERENCES [dbo].[MenuItem] ([ID])
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_MenuItem]
GO

--/****** Object:  ForeignKey [FK_MenuItemAccountTypeRole_Role]    ******/
--ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_MenuItemAccountTypeRole_Role] FOREIGN KEY([Role])
--REFERENCES [dbo].[Role] ([ID])
--GO
--ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_Role]
--GO

/****** Object:  ForeignKey [FK_MenuItemAccountTypeRole_AccountTypeRole]    ******/
ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_MenuItemAccountTypeRole_AccountTypeRole] FOREIGN KEY([AccountTypeRole])
REFERENCES [dbo].[AccountTypeRole] ([ID])
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_AccountTypeRole]
GO

-- User

/****** Object:  ForeignKey [[FK_User_UserAccount]]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserAccount]
GO

/****** Object:  ForeignKey [FK_User_UserStatus]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_UserStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[UserStatus] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserStatus]
GO

/****** Object:  ForeignKey [FK_User_Creator]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Creator]
GO

/****** Object:  ForeignKey [FK_User_Modifier]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Modifier]
GO

/****** Object:  ForeignKey [FK_User_NotificationFrequency]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_NotificationFrequency] FOREIGN KEY([NotificationFrequency])
REFERENCES [dbo].[NotificationFrequency] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_NotificationFrequency]
GO

ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_Preset] FOREIGN KEY([Preset])
REFERENCES [dbo].[Preset] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Preset]
GO

ALTER TABLE [dbo].[User] ADD CONSTRAINT [DF_User_IncludeMyOwnActivity]  DEFAULT ((0)) FOR [IncludeMyOwnActivity]
GO

---- UserGroupUser

/****** Object:  ForeignKey [FK_UserGroupUser_UserGroup]    ******/
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD CONSTRAINT [FK_UserGroupUser_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_UserGroup]
GO

/****** Object:  ForeignKey [FK_UserGroupUser_User]    ******/
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD CONSTRAINT [FK_UserGroupUser_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_User]
GO

-- UserHistory

/****** Object:  ForeignKey [FK_UserHistory_Creator]    ******/
ALTER TABLE [dbo].[UserHistory]  WITH CHECK ADD CONSTRAINT [FK_UserHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserHistory] CHECK CONSTRAINT [FK_UserHistory_Creator]
GO

/****** Object:  ForeignKey [FK_UserHistory_User]    ******/
ALTER TABLE [dbo].[UserHistory]  WITH CHECK ADD CONSTRAINT [FK_UserHistory_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserHistory] CHECK CONSTRAINT [FK_UserHistory_User]
GO

ALTER TABLE [dbo].[UserHistory] ADD CONSTRAINT [DF_UserHistory_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

-- UserLogin

/****** Object:  ForeignKey [FK_UserLogin_User]    ******/
ALTER TABLE [dbo].[UserLogin]  WITH CHECK ADD CONSTRAINT [FK_UserLogin_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserLogin] CHECK CONSTRAINT [FK_UserLogin_User]
GO

ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [DF_UserLogin_Success]  DEFAULT ((0)) FOR [Success]
GO

-- UserControllerActionAccess

--/****** Object:  ForeignKey [FK_UserControllerActionAccess_MenuItem]    ******/
--ALTER TABLE [dbo].[UserControllerActionAccess]  WITH CHECK ADD CONSTRAINT [FK_UserControllerActionAccess_MenuItem] FOREIGN KEY([MenuItem])
--REFERENCES [dbo].[MenuItem] ([ID])
--GO
--ALTER TABLE [dbo].[UserControllerActionAccess] CHECK CONSTRAINT [FK_UserControllerActionAccess_MenuItem]
--GO

--/****** Object:  ForeignKey [FK_UserControllerActionAccess_ControllerAction]    ******/
--ALTER TABLE [dbo].[UserControllerActionAccess]  WITH CHECK ADD CONSTRAINT [FK_UserControllerActionAccess_ControllerAction] FOREIGN KEY([ControllerAction])
--REFERENCES [dbo].[ControllerAction] ([ID])
--GO
--ALTER TABLE [dbo].[UserControllerActionAccess] CHECK CONSTRAINT [FK_UserControllerActionAccess_ControllerAction]
--GO

--/****** Object:  ForeignKey [FK_UserControllerActionAccess_User]    ******/
--ALTER TABLE [dbo].[UserControllerActionAccess]  WITH CHECK ADD CONSTRAINT [FK_UserControllerActionAccess_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[UserControllerActionAccess] CHECK CONSTRAINT [FK_UserControllerActionAccess_User]
--GO

-- ApprovalAnnotationTypeMarkup

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationMarkup_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup] CHECK CONSTRAINT [FK_ApprovalAnnotationMarkup_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationMarkup_Shape] FOREIGN KEY([Shape])
REFERENCES [dbo].[Shape] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup] CHECK CONSTRAINT [FK_ApprovalAnnotationMarkup_Shape]
GO

-- Shape

ALTER TABLE [dbo].[Shape] ADD CONSTRAINT [DF_Shape_IsCustom]  DEFAULT ((0)) FOR [IsCustom]
GO

-- ApprovalAnnotationTextPrintData

--ALTER TABLE [dbo].[ApprovalAnnotationTextPrintData]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationTextPrintData_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
--REFERENCES [dbo].[ApprovalAnnotation] ([ID])
--GO
--ALTER TABLE [dbo].[ApprovalAnnotationTextPrintData] CHECK CONSTRAINT [FK_ApprovalAnnotationTextPrintData_ApprovalAnnotation]
--GO

-- UserRole

/****** Object:  ForeignKey [FK_UserRole_Role]    ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO

/****** Object:  ForeignKey [FK_UserRole_User]    ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO

-- AccountTypeRole

/****** Object:  ForeignKey [FK_AccountTypeRole_Role]    ******/
ALTER TABLE [dbo].[AccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeRole_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[AccountTypeRole] CHECK CONSTRAINT [FK_AccountTypeRole_Role]
GO

/****** Object:  ForeignKey [FK_AccountTypeRole_AccountType]    ******/
ALTER TABLE [dbo].[AccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeRole_AccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[AccountTypeRole] CHECK CONSTRAINT [FK_AccountTypeRole_AccountType]
GO

---- CollaboratorGroup

--/****** Object:  ForeignKey [FK_CollaboratorGroup_Account]    ******/
--ALTER TABLE [dbo].[CollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_CollaboratorGroup_Account] FOREIGN KEY([Account])
--REFERENCES [dbo].[Account] ([ID])
--GO
--ALTER TABLE [dbo].[CollaboratorGroup] CHECK CONSTRAINT [FK_CollaboratorGroup_Account]
--GO

---- CollaboratorGroupUser

--/****** Object:  ForeignKey [FK_CollaboratorGroupUser_CollaboratorGroup]    ******/
--ALTER TABLE [dbo].[CollaboratorGroupUser]  WITH CHECK ADD CONSTRAINT [FK_CollaboratorGroupUser_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
--REFERENCES [dbo].[CollaboratorGroup] ([ID])
--GO
--ALTER TABLE [dbo].[CollaboratorGroupUser] CHECK CONSTRAINT [FK_CollaboratorGroupUser_CollaboratorGroup]
--GO

--/****** Object:  ForeignKey [FK_CollaboratorGroupUser_User]    ******/
--ALTER TABLE [dbo].[CollaboratorGroupUser]  WITH CHECK ADD CONSTRAINT [FK_CollaboratorGroupUser_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[CollaboratorGroupUser] CHECK CONSTRAINT [FK_CollaboratorGroupUser_User]
--GO

-- Job
ALTER TABLE [dbo].[Job]  WITH CHECK ADD CONSTRAINT [FK_Job_JobStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[JobStatus] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_JobStatus]
GO

ALTER TABLE [dbo].[Job]  WITH CHECK ADD CONSTRAINT [FK_Job_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Account]
GO

-- Approval

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Owner]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Creator]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Modifier]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Job]
GO

--ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Account] FOREIGN KEY([Account])
--REFERENCES [dbo].[Account] ([ID])
--GO
--ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Account]
--GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_PrimaryDecisionMaker] FOREIGN KEY([PrimaryDecisionMaker])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_PrimaryDecisionMaker]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_FileType] FOREIGN KEY([FileType])
REFERENCES [dbo].[FileType] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_FileType]
GO

ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_LockProofWhenAllDecisionsMade]  DEFAULT ((0)) FOR [LockProofWhenAllDecisionsMade]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_OnlyOneDecisionRequired]  DEFAULT ((0)) FOR [OnlyOneDecisionRequired]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_AllowDownloadOriginal]  DEFAULT ((0)) FOR [AllowDownloadOriginal]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_Size]  DEFAULT ((0)) FOR [Size]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_IsError]  DEFAULT ((0)) FOR [IsError]
GO

-- ApprovalPage

ALTER TABLE [dbo].[ApprovalPage]  WITH CHECK ADD CONSTRAINT [FK_ApprovalPage_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO

ALTER TABLE [dbo].[ApprovalPage] CHECK CONSTRAINT [FK_ApprovalPage_Approval]
GO

-- ApprovalUserViewInfo

ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD CONSTRAINT [FK_ApprovalUserViewInfo_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_Approval]
GO

ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD CONSTRAINT [FK_ApprovalUserViewInfo_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_User]
GO

-- ApprovalAnnotation

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Creator]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_ApprovalPage] FOREIGN KEY([Page])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ApprovalPage]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_HighlightType] FOREIGN KEY([HighlightType])
REFERENCES [dbo].[HighlightType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_HighlightType]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator]
GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Status]    ******/
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[ApprovalAnnotationStatus] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Status]
GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Modifier]    ******/
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Modifier]
GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Type]    ******/
--ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Type] FOREIGN KEY([Type])
--REFERENCES [dbo].[ApprovalAnnotationType] ([ID])
--GO
--ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Type]
--GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Parent]    ******/
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Parent]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_CommentType] FOREIGN KEY([CommentType])
REFERENCES [dbo].[ApprovalCommentType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_CommentType]
GO

ALTER TABLE [dbo].[ApprovalAnnotation] ADD CONSTRAINT [DF_ApprovalAnnotation_StartIndex]  DEFAULT ((0)) FOR [StartIndex]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD CONSTRAINT [DF_ApprovalAnnotation_EndIndex]  DEFAULT ((0)) FOR [EndIndex]
GO

-- ApprovalHistory

/* ALTER TABLE [dbo].[ApprovalHistory]  WITH CHECK ADD CONSTRAINT [FK_ApprovalHistory_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalHistory] CHECK CONSTRAINT [FK_ApprovalHistory_Approval]
GO

ALTER TABLE [dbo].[ApprovalHistory]  WITH CHECK ADD CONSTRAINT [FK_ApprovalHistory_Status] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalHistory] CHECK CONSTRAINT [FK_ApprovalHistory_Status]
GO

ALTER TABLE [dbo].[ApprovalHistory]  WITH CHECK ADD CONSTRAINT [FK_ApprovalHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalHistory] CHECK CONSTRAINT [FK_ApprovalHistory_Creator]
GO */

-- ApprovalAnnotationAttachment

ALTER TABLE [dbo].[ApprovalAnnotationAttachment]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationAttachment_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationAttachment] CHECK CONSTRAINT [FK_ApprovalAnnotationAttachment_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationAttachment]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationAttachment_FileType] FOREIGN KEY([FileType])
REFERENCES [dbo].[FileType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationAttachment] CHECK CONSTRAINT [FK_ApprovalAnnotationAttachment_FileType]
GO

-- BillingPlan
ALTER TABLE [dbo].[BillingPlan]  WITH CHECK ADD CONSTRAINT [FK_BillingPlan_Type] FOREIGN KEY([Type])
REFERENCES [dbo].[BillingPlanType] ([ID])
GO
ALTER TABLE [dbo].[BillingPlan] CHECK CONSTRAINT [FK_BillingPlan_Type]
GO

ALTER TABLE [dbo].[BillingPlan] ADD CONSTRAINT [DF_BillingPlan_Price]  DEFAULT ((0.0)) FOR [Price]
GO
ALTER TABLE [dbo].[BillingPlan] ADD CONSTRAINT [DF_BillingPlan_IsFixed]  DEFAULT ((0)) FOR [IsFixed]
GO

-- BillingPlanType
ALTER TABLE [dbo].[BillingPlanType]  WITH CHECK ADD CONSTRAINT [FK_BillingPlanType_MediaService] FOREIGN KEY([MediaService])
REFERENCES [dbo].[MediaService] ([ID])
GO
ALTER TABLE [dbo].[BillingPlanType] CHECK CONSTRAINT [FK_BillingPlanType_MediaService]
GO

ALTER TABLE [dbo].[BillingPlanType] ADD CONSTRAINT [DF_BillingPlanType_EnablePrePressTools]  DEFAULT ((0)) FOR [EnablePrePressTools]
GO
ALTER TABLE [dbo].[BillingPlanType] ADD CONSTRAINT [DF_BillingPlanType_EnableMediaTools]  DEFAULT ((0)) FOR [EnableMediaTools]
GO

-- AttachedAccountBillingPlan

ALTER TABLE [dbo].[AttachedAccountBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_AttachedAccountBillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] CHECK CONSTRAINT [FK_AttachedAccountBillingPlan_Account]
GO

ALTER TABLE [dbo].[AttachedAccountBillingPlan] ADD CONSTRAINT [DF_AttachedAccountBillingPlan_IsAppliedCurrentRate]  DEFAULT ((0)) FOR [IsAppliedCurrentRate]
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] ADD CONSTRAINT [DF_AttachedAccountBillingPlan_IsModifiedProofPrice]  DEFAULT ((0)) FOR [IsModifiedProofPrice]
GO

-- AttachedAccountBillingPlan

ALTER TABLE [dbo].[AttachedAccountBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_AttachedAccountBillingPlan_BillingPlan] FOREIGN KEY([BillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] CHECK CONSTRAINT [FK_AttachedAccountBillingPlan_BillingPlan]
GO

-- CurrencyRate

ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD CONSTRAINT [FK_CurrencyRate_Currency] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Currency]
GO

ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD CONSTRAINT [FK_CurrencyRate_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Creator]
GO

ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD CONSTRAINT [FK_CurrencyRate_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Modifier]
GO

-- ApprovalCollaborator

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaborator_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_Approval]
GO

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_Collaborator]
GO

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaborator_ApprovalCollaboratorRole] FOREIGN KEY([ApprovalCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_ApprovalCollaboratorRole]
GO

-- ApprovalAnnotationPrivateCollaborator

ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator] CHECK CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator] CHECK CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_Collaborator]
GO

-- ApprovalCollaboratorGroup

ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaboratorGroup_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_Approval]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_CollaboratorGroup]
GO

-- StudioSetting

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_StudioSettingType] FOREIGN KEY([Type])
REFERENCES [dbo].[StudioSettingType] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_StudioSettingType]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_User]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_NotificationFrequency] FOREIGN KEY([NotificationFrequency])
REFERENCES [dbo].[NotificationFrequency] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_NotificationFrequency]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_WhenCommentMade] FOREIGN KEY([WhenCommentMade])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_WhenCommentMade]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_RepliesMyComment] FOREIGN KEY([RepliesMyComment])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_RepliesMyComment]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_NewVesionAdded] FOREIGN KEY([NewVesionAdded])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_NewVesionAdded]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_FinalDecisionMade] FOREIGN KEY([FinalDecisionMade])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_FinalDecisionMade]
GO

ALTER TABLE [dbo].[StudioSetting] ADD CONSTRAINT [DF_StudioSetting_IncludeMyOwnActivity]  DEFAULT ((0)) FOR [IncludeMyOwnActivity]
GO
ALTER TABLE [dbo].[StudioSetting] ADD CONSTRAINT [DF_StudioSetting_EnableColorCorrection]  DEFAULT ((0)) FOR [EnableColorCorrection]
GO

-- Folder

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Creator]
GO

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Modifier]
GO

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Parent]
GO

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Account]
GO

ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [DF_Folder_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [DF_Folder_IsPrivate]  DEFAULT ((1)) FOR [IsPrivate]
GO

-- FolderCollaborator

ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_Folder]
GO

ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_Collaborator]
GO

--ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
--REFERENCES [dbo].[UserGroup] ([ID])
--GO
--ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_CollaboratorGroup]
--GO

-- FolderCollaboratorGroup

ALTER TABLE [dbo].[FolderCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_FolderCollaboratorGroup_Approval] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup] CHECK CONSTRAINT [FK_FolderCollaboratorGroup_Approval]
GO

ALTER TABLE [dbo].[FolderCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_FolderCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup] CHECK CONSTRAINT [FK_FolderCollaboratorGroup_CollaboratorGroup]
GO

-- FolderApproval

ALTER TABLE [dbo].[FolderApproval]  WITH CHECK ADD  CONSTRAINT [FK_FolderApproval_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderApproval] CHECK CONSTRAINT [FK_FolderApproval_Folder]
GO

ALTER TABLE [dbo].[FolderApproval]  WITH CHECK ADD  CONSTRAINT [FK_FolderApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[FolderApproval] CHECK CONSTRAINT [FK_FolderApproval_Approval]
GO

-- ApprovalCollaboratorDecision

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Approval]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Decision] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Decision]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Collaborator]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaboratorDecision_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_ExternalCollaborator]
GO

-- LogAction

ALTER TABLE [dbo].[LogAction]  WITH CHECK ADD  CONSTRAINT [FK_LogAction_LogModule] FOREIGN KEY([LogModule])
REFERENCES [dbo].[LogModule] ([ID])
GO
ALTER TABLE [dbo].[LogAction] CHECK CONSTRAINT [FK_LogAction_LogModule]
GO

-- LogApproval

ALTER TABLE [dbo].[LogApproval]  WITH CHECK ADD  CONSTRAINT [FK_LogApproval_LogAction] FOREIGN KEY([LogAction])
REFERENCES [dbo].[LogAction] ([ID])
GO
ALTER TABLE [dbo].[LogApproval] CHECK CONSTRAINT [FK_LogApproval_LogAction]
GO

-- UserGroup

ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Account]
GO

ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Creator]
GO

ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Modifier]
GO

-- EventType

ALTER TABLE [dbo].[EventType]  WITH CHECK ADD CONSTRAINT [FK_EventType_Group] FOREIGN KEY([EventGroup])
REFERENCES [dbo].[EventGroup] ([ID])
GO
ALTER TABLE [dbo].[EventType] CHECK CONSTRAINT [FK_EventType_Group]
GO

-- RolePresetEventType

ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD CONSTRAINT [FK_RolePresetEventType_Level] FOREIGN KEY([Preset])
REFERENCES [dbo].[Preset] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Level]
GO

ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD CONSTRAINT [FK_RolePresetEventType_Type] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Type]
GO

ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD CONSTRAINT [FK_RolePresetEventType_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Role]
GO

-- UserCustomPresetEventTypeValue

ALTER TABLE [dbo].[UserCustomPresetEventTypeValue]  WITH CHECK ADD CONSTRAINT [FK_UserCustomPresetEventTypeValue_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] CHECK CONSTRAINT [FK_UserCustomPresetEventTypeValue_User]
GO

ALTER TABLE [dbo].[UserCustomPresetEventTypeValue]  WITH CHECK ADD CONSTRAINT [FK_UserCustomPresetEventTypeValue_RolePresetEventType] FOREIGN KEY([RolePresetEventType])
REFERENCES [dbo].[RolePresetEventType] ([ID])
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] CHECK CONSTRAINT [FK_UserCustomPresetEventTypeValue_RolePresetEventType]
GO

--NotificationEmailQueue--

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_PrimaryRecipient] FOREIGN KEY([PrimaryRecipient])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_PrimaryRecipient]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_EventType]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Creator]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_SecondaryRecipient] FOREIGN KEY([SecondaryRecipient])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_SecondaryRecipient]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_ExternalCollaborator]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_BillingPlan1] FOREIGN KEY([BillingPlan1])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_BillingPlan1]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_BillingPlan2] FOREIGN KEY([BillingPlan2])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_BillingPlan2]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Account]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Approval]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Folder]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_UserGroup]
GO

ALTER TABLE [dbo].[NotificationEmailQueue] ADD CONSTRAINT [DF_NotificationEmailQueue_IsEmailSent]  DEFAULT ((0)) FOR [IsEmailSent]
GO

-- ChangedBillingPlan
ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_ChangedBillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_Account]
GO

ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_ChangedBillingPlan_BillingPlan] FOREIGN KEY([BillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_BillingPlan]
GO

ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_ChangedBillingPlan_AttachedAccountBillingPlan] FOREIGN KEY([AttachedAccountBillingPlan])
REFERENCES [dbo].[AttachedAccountBillingPlan] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_AttachedAccountBillingPlan]
GO

-- AccountPricePlanChangedMessage
ALTER TABLE [dbo].[AccountPricePlanChangedMessage]  WITH CHECK ADD CONSTRAINT [FK_AccountPricePlanChangedMessage_ChangedBillingPlan] FOREIGN KEY([ChangedBillingPlan])
REFERENCES [dbo].[ChangedBillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage] CHECK CONSTRAINT [FK_AccountPricePlanChangedMessage_ChangedBillingPlan]
GO

ALTER TABLE [dbo].[AccountPricePlanChangedMessage]  WITH CHECK ADD CONSTRAINT [FK_AccountPricePlanChangedMessage_ChildAccount] FOREIGN KEY([ChildAccount])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage] CHECK CONSTRAINT [FK_AccountPricePlanChangedMessage_ChildAccount]
GO

-- ImageFormat
ALTER TABLE [dbo].[ImageFormat]  WITH CHECK ADD CONSTRAINT [FK_ImageFormat_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ImageFormat] ([ID])
GO
ALTER TABLE [dbo].[ImageFormat] CHECK CONSTRAINT [FK_ImageFormat_Parent]
GO

-- MediaService
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_Smoothing] FOREIGN KEY([Smoothing])
REFERENCES [dbo].[Smoothing] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_Smoothing]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_Colorspace] FOREIGN KEY([Colorspace])
REFERENCES [dbo].[Colorspace] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_Colorspace]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_ImageFormat] FOREIGN KEY([ImageFormat])
REFERENCES [dbo].[ImageFormat] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_ImageFormat]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_PageBox] FOREIGN KEY([PageBox])
REFERENCES [dbo].[PageBox] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_PageBox]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_TileImageFormat] FOREIGN KEY([TileImageFormat])
REFERENCES [dbo].[TileImageFormat] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_TileImageFormat]
GO

ALTER TABLE [dbo].[MediaService] ADD CONSTRAINT [DF_MediaService_ApplySimulateOverprinting]  DEFAULT ((0)) FOR [ApplySimulateOverprinting]
GO

-- ApprovalSeparationPlate
ALTER TABLE [dbo].[ApprovalSeparationPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalSeparationPlate_Page] FOREIGN KEY([Page])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[ApprovalSeparationPlate] CHECK CONSTRAINT [FK_ApprovalSeparationPlate_Page]
GO
/*
-- VariationPlate
ALTER TABLE [dbo].[VariationPlate]  WITH CHECK ADD CONSTRAINT [FK_VariationPlate_Variation] FOREIGN KEY([Variation])
REFERENCES [dbo].[Variation] ([ID])
GO
ALTER TABLE [dbo].[VariationPlate] CHECK CONSTRAINT [FK_VariationPlate_Variation]
GO

-- ApprovalAdditionalPlate
ALTER TABLE [dbo].[ApprovalAdditionalPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAdditionalPlate_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAdditionalPlate] CHECK CONSTRAINT [FK_ApprovalAdditionalPlate_Approval]
GO

-- ApprovalVariationPlate
ALTER TABLE [dbo].[ApprovalVariationPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalVariationPlate_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalVariationPlate] CHECK CONSTRAINT [FK_ApprovalVariationPlate_Approval]
GO

ALTER TABLE [dbo].[ApprovalVariationPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalVariationPlate_VariationPlate] FOREIGN KEY([VariationPlate])
REFERENCES [dbo].[VariationPlate] ([ID])
GO
ALTER TABLE [dbo].[ApprovalVariationPlate] CHECK CONSTRAINT [FK_ApprovalVariationPlate_VariationPlate]
GO
*/

-- ExternalCollaborator
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_UserAccount]
GO

ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_Creator]
GO

ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_Modifier]
GO

ALTER TABLE [dbo].[ExternalCollaborator] ADD DEFAULT ((1)) FOR [IsDeleted]
GO

-- SharedApproval
ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_Approval]
GO

ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ExternalCollaborator]
GO

ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ApprovalCollaboratorRole] FOREIGN KEY([ApprovalCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ApprovalCollaboratorRole]
GO

ALTER TABLE [dbo].[SharedApproval] ADD DEFAULT ((1)) FOR [IsSharedURL]
GO

ALTER TABLE [dbo].[SharedApproval] ADD DEFAULT ((1)) FOR [IsSharedDownloadURL]
GO

ALTER TABLE [dbo].[SharedApproval] ADD DEFAULT ((0)) FOR [IsExpired]
GO

-- GlobalSettings

ALTER TABLE [dbo].[GlobalSettings]  WITH CHECK ADD  CONSTRAINT [FK_GlobalSettings_NativeDataType] FOREIGN KEY([DataType])
REFERENCES [dbo].[NativeDataType] ([ID])
GO
ALTER TABLE [dbo].[GlobalSettings] CHECK CONSTRAINT [FK_GlobalSettings_NativeDataType]
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[UserMenuItemRoleView]    Script Date: 10/28/2010 21:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserMenuItemRoleView]
AS

SELECT	
		TOP 100 PERCENT
		atr.[AccountType],
		atr.[Role],
		mi.[ID] AS [MenuItem],
		mi.[Key],
		mi.[Name] AS [MenuName],	
		mi.[Title],
		ISNULL(mi.[Parent], 0) AS [Parent],
		mi.[Position],
		mi.[IsAdminAppOwned],
		mi.[IsAlignedLeft],
		mi.[IsSubNav],
		mi.[IsTopNav],
		mi.[IsVisible],		
		ca.[ID] AS [ControllerAction], 	
		ca.[Controller], 
		ca.[Action],
		ca.[Parameters]
FROM	
		[dbo].[AccountTypeRole] atr
		JOIN [dbo].[MenuItemAccountTypeRole] mirat
			ON atr.[ID] = mirat.[AccountTypeRole]
		JOIN [dbo].[MenuItem] mi
			ON mirat.[MenuItem] = mi.[ID]
		LEFT OUTER JOIN [dbo].[ControllerAction] ca 
			ON mi.ControllerAction = ca.ID
ORDER BY
		atr.[AccountType] ASC, atr.[Role] ASC, mi.[ID] ASC, [Parent], mi.[Position]
GO
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[ReturnStringView] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnStringView] 
AS 
	SELECT  '' as RetVal

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_EncryptedPassword] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_EncryptedPassword
-- Description:  Select an encrypted password based on the supplied one
-- Date Created: Wednesday, 29 September 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE   PROCEDURE [dbo].[SPC_EncryptedPassword] (
	@P_Password varchar(64)
)
AS
	SELECT CONVERT(varchar(255),HashBytes('SHA1', @P_Password)) as RetVal

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserLogin] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnUserLogin
-- Description:  Select item in User by login details
-- Date Created: Wednesday, 29 September 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnUserLogin] (
	@P_Account int,
	@P_Username varchar(255),
	@P_Password varchar(255)
)
AS
BEGIN
	SELECT	DISTINCT
			u.[ID],
			u.[Account],		
			u.[Status],  
			u.[Username],
			u.[Password],
			u.[GivenName],
			u.[FamilyName],
			u.[EmailAddress],
			u.[OfficeTelephoneNumber],
			u.[MobileTelephoneNumber],
			u.[HomeTelephoneNumber],
			u.[Guid],
			u.[PhotoPath],
			us.[ID] AS [StatusId],
			us.[Key],
			us.[Name],
			u.[Creator],
			u.[CreatedDate],
			u.[Modifier],
			u.[ModifiedDate],
			u.[DateLastLogin],
			u.[IncludeMyOwnActivity]
	FROM	[dbo].[User] u
			JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID 
	WHERE 	u.[Account] = @P_Account AND
			u.[Username] = @P_Username AND 
			u.[Password] = CONVERT(varchar(255), HashBytes('SHA1', @P_Password)) AND
			us.[Key] = 'A'
END
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[UserAccountRoleView]    Script Date: 11/09/2011 17:21:28 ******/
/*****   IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UserAccountRoleView]'))
DROP VIEW [dbo].[UserAccountRoleView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserAccountRoleView]
AS

SELECT
	u.[ID], 
	u.GivenName,
	u.FamilyName,
	u.EmailAddress,
	u.Username,
	u.DateLastLogin,
	u.[Status],
	u.IsActive,
	r.Name AS RoleName,
	a.Name AS AccountName
FROM    
	[dbo].[User] u 
	JOIN [dbo].[UserRole] ur 
		ON u.[ID] = ur.[User]
	JOIN [dbo].[Role] r 
		ON ur.[Role] = r.[ID] 
	--JOIN UserCompany uc
	--	ON  uc.[User] = u.ID 
	JOIN Company c
		ON c.ID = uc.Company
	JOIN [dbo].[Account] a
		ON  a.[ID] = c.Account
GO *****/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  UserDefinedFunction [dbo].[GetAccessFolders]    Script Date: 10/26/2012 14:30:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAccessFolders]
(	
	-- Add the parameters for the function here
	@UserId int,
	@FolderId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT f.ID, f.Parent, f.Name, (0) AS Level, (CASE WHEN 
											(@UserId IN (	SELECT	fc.Collaborator 
															FROM	[dbo].FolderCollaborator fc
															WHERE	fc.Folder = f.ID 
					) ) THEN  '1' ELSE '0' end)  AS HasAccess 
		FROM	[dbo].[Folder] f
		WHERE	f.ID = @FolderId		 
		
		UNION ALL
		
		SELECT sf.ID, sf.Parent,sf.Name, (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  '1' ELSE '0' END)  AS HasAccess 
		FROM [dbo].[Folder] sf
			JOIN tableSet h
		ON h.ID	 = sf.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
    WHERE [Level] = 1
)

GO

/****** Object:  View [dbo].[AccountsView]    Script Date: 11/09/2011 17:21:28 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AccountsView]'))
DROP VIEW [dbo].[AccountsView]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[AccountsView]
AS
SELECT
	a.[ID], 
	a.Name,
	a.[Status] AS StatusId,
	s.[Name] AS StatusName,
	s.[Key],
	a.Domain,
	a.IsCustomDomainActive,
	a.CustomDomain,
	a.AccountType AS AccountTypeID,
	a.CreatedDate,
	a.IsTemporary,
	at.Name AS AccountTypeName,
	u.GivenName,
	u.FamilyName,
	co.ID AS Company,
	c.ID AS Country,
	c.ShortName AS CountryName,
	--a.IsDraft,
	(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts
FROM
	[dbo].[AccountType] at 
	JOIN [dbo].[Account] a
		ON at.[ID] = a.AccountType
	JOIN [dbo].[AccountStatus] s
		ON s.[ID] = a.[Status]
	LEFT OUTER JOIN [dbo].[User] u
		ON u.ID= a.[Owner]
	JOIN [dbo].[Company] co
		ON a.[ID] = co.[Account]
	JOIN [dbo].[Country] c
		ON c.[ID] = co.[Country]
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  View [dbo].[UserDetailsView]    Script Date: 11/09/2011 17:21:28 ******/
/*****  IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UserDetailsView]'))
DROP VIEW [dbo].[UserDetailsView]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserDetailsView]
AS
SELECT 
	u.[ID], 
	u.Username,
	u.GivenName,
	u.FamilyName,
	u.EmailAddress,
	u.OfficeTelephoneNumber,
	u.IsActive,
	u.IsDeleted,
	u.[Guid],
	u.PhotoPath,
	u.IsSupplier,
	us.ID AS UserStatusID,
	us.Name AS UserStatusName,
	r.Name AS UserRoleName,
	uc.Company,
	uc.IsOwnerCompany
FROM    
	[dbo].[User] u
	JOIN [dbo].[UserStatus] us
		ON us.[ID] = u.[Status]
	JOIN [dbo].[UserRole] ur 
		ON u.[ID] = ur.[User]
	JOIN [dbo].[Role] r 
		ON r.[ID] = ur.[Role]
	JOIN UserCompany uc
		ON  uc.[User] = u.ID 
	JOIN Company c
		ON c.ID = uc.Company
	JOIN [dbo].[Account] a
		ON  a.[ID] = c.Account 
GO ***/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  View [dbo].[AccountUsersView]    Script Date: 11/22/2011 18:08:54 ******/
/****   IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AccountUsersView]'))
DROP VIEW [dbo].[AccountUsersView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[AccountUsersView]
AS
SELECT c.[ID] AS [CompanyID]
      ,c.[Account] AS [AccountID]
      ,c.[IsSupplier]
      ,c.[Name] AS [CompanyName]
      ,u.[ID] AS [UserID]
      ,u.[GivenName] + ' ' + u.[FamilyName] AS [FullName]
      ,us.[ID] AS [StatusID] 
      ,us.[Name] AS [StatusName]
      ,us.[Key]
      ,u.[IsActive] 
      ,u.[IsDeleted] 
      ,r.[ID] AS [RoleID]
      ,r.[Name] AS [RoleName]
  FROM [dbo].[Company] c
		JOIN [dbo].[UserCompany] uc
			ON c.[ID] = uc.[Company]
		JOIN [dbo].[User] u
			ON u.[ID] = uc.[User]
		JOIN [dbo].[UserStatus] us
			ON us.[ID] = u.[Status]
		JOIN [dbo].[UserRole] ur
			ON u.[ID] = ur.[User]
		JOIN [dbo].[Role] r
			ON r.[ID] = ur.[Role]
GO   ******/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 04/18/2013 14:54:24 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnApprovalsPageView]'))
DROP VIEW [dbo].[ReturnApprovalsPageView]
GO

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 03/21/2013 15:22:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			0 AS [Status],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			'' AS Collaborators,
			0 AS Decision
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 04/18/2013 14:54:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetApprovalCollaborators]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetApprovalCollaborators]
GO

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 03/25/2013 15:10:33 ******/

CREATE FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac
			 ON  a.ID = ac.Approval
		WHERE a.ID = @P_Approval ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCollaborator IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;

GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 04/18/2013 14:55:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFolderCollaborators]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFolderCollaborators]
GO

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 03/25/2013 15:11:35 ******/

CREATE FUNCTION [dbo].[GetFolderCollaborators] (@P_Folder int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT fcg.CollaboratorGroup
		FROM Folder f 
				JOIN FolderCollaboratorGroup fcg
					ON  f.ID = fcg.Folder
		WHERE f.ID = @P_Folder ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT fc.Collaborator
		FROM Folder f 
				JOIN FolderCollaborator fc
			 ON  f.ID = fc.Folder
		WHERE f.ID = @P_Folder ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
			
	RETURN	(@GroupList + '#' + @CollaboratorList ) 

END;

GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 07/01/2013 15:29:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnFoldersApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 07/01/2013 15:29:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnFoldersApprovalsPageInfo
-- Description:  This SP returns folders and approvals
-- Date Created: Monday, 30 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC					
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,	
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]	 						
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )												
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
				ORDER BY 
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN Deadline
					--END ASC,
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN Deadline
					--END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN f.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN f.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN f.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN f.ModifiedDate
					END DESC--,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN f.[Status]
					--END ASC,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN f.[Status]
					--END DESC		 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]	 						
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )												
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END


GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 04/26/2012 23:24:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalsPageInfo
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC					
				)) AS ID, 
				a.ID AS Approval,	
				0 AS Folder,
				j.Title,							 
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,				
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )					
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)								
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)										
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )					
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)								
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)										
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END

END



GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--


/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]    Script Date: 06/12/2012 11:42:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnRecentViewedApprovalsPageInfo
-- Description:  This Sp returns recenr viwed approvals
-- Date Created: Monday, 12 June 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
			SELECT 	TOP (@P_Set * @P_MaxRows)
					CONVERT(int, ROW_NUMBER() OVER(
					ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
					END DESC					
				)) AS ID, 
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,							 
					j.[Status] AS [Status],	
					j.ID AS Job,
					a.[Version],
					100 AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					(SELECT MAX([Version])
					 FROM Approval av
					 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT 	a.ID
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	  
END


GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecycleBinPageInfo]    Script Date: 06/12/2012 11:42:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnRecycleBinPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnRecycleBinPageInfo
-- Description:  This SP returns deleted folders and approvals
-- Date Created: Monday, 30 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END


GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_ReturnArchivedApprovalInfo]    Script Date: 06/12/2012 11:42:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnArchivedApprovalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnArchivedApprovalInfo
-- Description:  This SP returns archived folders and approvals
-- Date Created: Friday, 21 September 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC					
				)) AS ID, 			
				a.ID AS Approval,	
				0 AS Folder,
				j.Title,							 
				j.[Status] AS [Status],
				j.ID AS Job,	
				a.[Version],
				100 AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				0 AS MaxVersion,				
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				'######' AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
		FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
		WHERE	j.Account = @P_Account
				AND a.IsDeleted = 0	
				AND js.[Key] = 'ARC'									
				AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
				AND
				(	SELECT MAX([Version])
					FROM Approval av 
					WHERE 
						av.Job = a.Job								
						AND av.IsDeleted = 0
						AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
				) = a.[Version]	
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	 
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM	(
			SELECT 	a.ID
			FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0	
					AND js.[Key] = 'ARC'									
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job								
							AND av.IsDeleted = 0
							AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
					) = a.[Version]		
				)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	 
END


GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_DeleteApproval]    Script Date: 10/15/2012 16:35:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_DeleteApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_DeleteApproval]
GO

/****** Object:  StoredProcedure [dbo].[SPC_DeleteApproval]    Script Date: 10/15/2012 14:29:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteApproval
-- Description:  This SP deletes the Approval and it's files
-- Date Created: Monday, 15 October 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
		
		-- Delete from NotificationEmailQueue
		DELETE FROM [dbo].[NotificationEmailQueue]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END


GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_DeleteFolder]    Script Date: 10/15/2012 16:35:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_DeleteFolder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_DeleteFolder]
GO

/****** Object:  StoredProcedure [dbo].[SPC_DeleteFolder]    Script Date: 10/15/2012 14:29:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteFolder
-- Description:  This SP deletes the Folder and it's files
-- Date Created: Monday, 15 October 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[SPC_DeleteFolder] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY
	
		-- Delete from FolderCollaborator 
		DELETE FROM	[dbo].[FolderCollaborator]
				WHERE Folder = @P_Id
		
		-- Delete from FolderCollaboratorGroup 
		DELETE FROM	[dbo].[FolderCollaboratorGroup] 
				WHERE Folder = @P_Id
		
		-- Delete from NotificationEmailQueue 
		DELETE FROM	[dbo].[NotificationEmailQueue] 
				WHERE Folder = @P_Id				
	
		DECLARE @ApprovalID INT
		DECLARE @getApprovalID CURSOR
		SET @getApprovalID = CURSOR FOR SELECT Approval FROM [dbo].[FolderApproval] WHERE Folder = @P_Id
		OPEN @getApprovalID
		
		FETCH NEXT
		FROM @getApprovalID INTO @ApprovalID
			WHILE @@FETCH_STATUS = 0
			BEGIN
			EXECUTE [dbo].[SPC_DeleteApproval] @ApprovalID
			
			FETCH NEXT
			FROM @getApprovalID INTO @ApprovalID
			END
		CLOSE @getApprovalID
		DEALLOCATE @getApprovalID	
 
	    -- Delete from Folder
		DELETE FROM [dbo].[Folder]
		WHERE ID = @P_Id	    
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END



GO

/****** Object:  View [dbo].[ReturnApprovalCountsView]    Script Date: 03/14/2013 13:11:47 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnApprovalCountsView]'))
DROP VIEW [dbo].[ReturnApprovalCountsView]
GO

/****** Object:  View [dbo].[ReturnApprovalCountsView]    Script Date: 03/14/2013 13:07:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount   

GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 03/14/2013 13:29:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnApprovalCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnApprovalCounts]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 03/14/2013 12:12:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalCounts
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;
	
	-- Get All Approval count
	SET @AllCount = (	SELECT COUNT(a.ID)
						FROM	Job j
								INNER JOIN Approval a 
									ON a.Job = j.ID
								INNER JOIN JobStatus js
									ON js.ID = j.[Status]		
						WHERE	j.Account = @P_Account
							AND a.IsDeleted = 0
							AND js.[Key] != 'ARC'
							AND	(	SELECT MAX([Version])
									FROM Approval av 
									WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND @P_User IN (	SELECT ac.Collaborator 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID
															)
								) = a.[Version] )
						
	-- Get Owned by me count		
	SET @OwnedByMeCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] = @P_User
									) = a.[Version]	)
			
	-- Get Shared with me count
	SET @SharedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] != @P_User
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version]	)
			
	-- Get Recently viewed count
	SET @RecentlyViewedCount = (	SELECT COUNT(DISTINCT j.ID)
									FROM	ApprovalUserViewInfo auvi
											INNER JOIN Approval a
													ON a.ID = auvi.Approval 
											INNER JOIN Job j
													ON j.ID = a.Job 
											INNER JOIN JobStatus js
													ON js.ID = j.[Status]		
									WHERE	auvi.[User] =  @P_User
										AND j.Account = @P_Account
										AND a.IsDeleted = 0
										AND js.[Key] != 'ARC'
										AND @P_User IN (	SELECT ac.Collaborator 
																			FROM ApprovalCollaborator ac 
																			WHERE ac.Approval = a.ID
																		)										
										)
			
	-- Get Archived count
	SET @ArchivedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] = 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version])
			
	-- Get Recycle bin count
	SET @RecycleCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (@P_User IN (	SELECT ac.Collaborator 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = a.ID
												)			
									)
						) 
						+
						(	SELECT COUNT(f.ID)
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)
									AND (@P_User IN (	SELECT fc.Collaborator 
														FROM FolderCollaborator fc 
														WHERE fc.Folder = f.ID
													)
										)													
						)
							
	SELECT 	@AllCount AS AllCount,
			@OwnedByMeCount AS OwnedByMeCount,
			@SharedCount AS SharedCount,
			@RecentlyViewedCount AS RecentlyViewedCount,
			@ArchivedCount AS ArchivedCount,
			@RecycleCount AS RecycleCount
END


GO

/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 03/26/2013 17:16:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnFolderTreeView]'))
DROP VIEW [dbo].[ReturnFolderTreeView]
GO

/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 03/21/2013 15:22:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnFolderTreeView] 
AS 
	SELECT  0 AS ID,
			0 AS Parent,
			'' AS Name,
			0 AS Creator,
			0 AS [Level],
			'' AS Collaborators

GO

/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 03/26/2013 17:16:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_GetFolderTree]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_GetFolderTree]
GO


/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 03/26/2013 17:05:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_GetFolderTree
-- Description:  This Sp returns accessed folders that needed for Approval index Page
-- Date Created: Monday, 23 April 2010
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_GetFolderTree] (
	@P_User int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH tableSet AS 
		(
			SELECT f.ID, f.Parent, f.Name, f.Creator, (0) AS Level, (CASE WHEN 
												(@P_User IN (	SELECT	fc.Collaborator 
																FROM	[dbo].FolderCollaborator fc
																WHERE	fc.Folder = f.ID 
						) ) THEN  '1' ELSE '0' end)  AS HasAccess 
			FROM	[dbo].[Folder] f
			WHERE f.IsDeleted = 0	 
			
			UNION ALL
			
			SELECT sf.ID, sf.Parent,sf.Name, sf.Creator, (CASE WHEN (@P_User IN (	SELECT fc.Collaborator 
						FROM	[dbo].FolderCollaborator fc
						WHERE	fc.Folder = sf.ID 
						 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@P_User IN (	SELECT fc.Collaborator 
						FROM	[dbo].FolderCollaborator fc
						WHERE	fc.Folder = sf.ID 
						 )) THEN  '1' ELSE '0' END)  AS HasAccess 
			FROM [dbo].[Folder] sf
				JOIN tableSet h
			ON h.ID	 = sf.Parent
			WHERE sf.IsDeleted = 0			 
		)	
	
	SELECT ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level],(SELECT dbo.GetFolderCollaborators(ID)) AS Collaborators
	FROM tableSet
	WHERE HasAccess = 1
	GROUP BY ID, Parent, Name, Creator
	
END


GO


/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 05/16/2013 11:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SplitString]
GO

/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 05/16/2013 11:04:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SplitString](
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    
    RETURN
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 05/16/2013 09:23:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBillingCycles]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetBillingCycles]
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 05/15/2013 16:59:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetBillingCycles](
	@AccountID int,
	@StartDate datetime,
	@EndDate dateTime	
) 
RETURNS nvarchar(MAX)
AS BEGIN

	DECLARE @cycleCount INT = 0
	DECLARE @AddedProofsCount INT = 0
	DECLARE @totalExceededCount INT = 0	
	DECLARE @cycleStartDate DATETIME = @StartDate
	DECLARE @cycleEndDate DATETIME
	DECLARE @cycleAllowedProofsCount INT
	DECLARE @IsMonthly bit = 1 
	DECLARE @PlanPrice decimal = 0.0
	DECLARE @ParentAccount INT
	DECLARE @SelectedBillingPLan INT
	DECLARE @BillingPlanPrice INT
	
	SET @ParentAccount = (SELECT Parent FROM Account WHERE ID = @AccountID)
	SET @SelectedBillingPLan = (SELECT SelectedBillingPLan FROM Account WHERE ID = @AccountID)
	
	SET @cycleAllowedProofsCount = (SELECT bp.Proofs 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @cycleAllowedProofsCount = (CASE WHEN (@IsMonthly = 1)
						THEN @cycleAllowedProofsCount
						ELSE (@cycleAllowedProofsCount*12)
						END
						) 										
											
	SET @BillingPlanPrice = (SELECT bp.Price 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @IsMonthly = (SELECT IsMonthlyBillingFrequency 
						FROM Account a
						WHERE a.ID = @AccountID
						)
						
	SET @PlanPrice = (CASE WHEN (@ParentAccount = 1)
						THEN @BillingPlanPrice
						ELSE (SELECT NewPrice FROM AttachedAccountBillingPlan
								WHERE Account = @ParentAccount AND BillingPlan = @SelectedBillingPLan)
						END)
						
	SET @PlanPrice = (CASE WHEN (@IsMonthly = 1)
						THEN @PlanPrice
						ELSE (@PlanPrice*12)
						END
						) 
																					
	SET @cycleStartDate = @StartDate
	
	WHILE (@cycleStartDate < @EndDate)
	BEGIN	
		DECLARE @currentCycleApprovalsCount INT = 0
				
		SET @cycleEndDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END	
	
		SET @currentCycleApprovalsCount = ( SELECT COUNT(ap.ID) FROM Job j 
														INNER JOIN Approval ap
															ON j.ID = ap.Job
														WHERE j.Account = @AccountID
														AND ap.CreatedDate >= @cycleStartDate
														AND ap.CreatedDate <= @cycleEndDate	)
														 
		SET @AddedProofsCount = @AddedProofsCount + @currentCycleApprovalsCount
													
		SET @cycleStartDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END
								
		SET @cycleCount = @cycleCount + 1;	
		SET @totalExceededCount = @totalExceededCount + CASE WHEN (@cycleAllowedProofsCount < @currentCycleApprovalsCount)  
										THEN @currentCycleApprovalsCount - @cycleAllowedProofsCount
										ELSE 0
										END													
	END
		
	RETURN  CONVERT(NVARCHAR, @cycleCount) + '|' +  CONVERT(NVARCHAR, @AddedProofsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice)
	
END

GO

/****** Object:  UserDefinedFunction [dbo].[GetChildAccounts]    Script Date: 05/16/2013 09:23:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetChildAccounts]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetChildAccounts]
GO

/****** Object:  UserDefinedFunction [dbo].[GetChildAccounts]    Script Date: 05/17/2013 17:26:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[GetChildAccounts]
(	
	-- Add the parameters for the function here
	@LoggedAccountId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT a.ID
		FROM	[dbo].[Account] a
		WHERE	a.ID = @LoggedAccountId		 
		
		UNION ALL
		
		SELECT sa.ID
		FROM [dbo].[Account] sa
			JOIN tableSet h
		ON h.ID	 = sa.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
)


GO

/****** Object:  View [dbo].[ReturnBillingReportInfoView]    Script Date: 05/16/2013 11:48:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnBillingReportInfoView]'))
DROP VIEW [dbo].[ReturnBillingReportInfoView]
GO

/****** Object:  View [dbo].[ReturnBillingReportInfoView]    Script Date: 05/16/2013 11:46:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS AccountName,
			'' AS AccountTypeName,
			'' AS BillingPlanName,
			0.0 AS GPPDiscount,
			0 AS Proofs,
			'' AS BillingCycleDetails,
			0 AS Parent,
			0 AS BillingPlan,
			CONVERT(bit, 0) AS IsFixedPlan,
			'' AS Subordinates
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 05/16/2013 10:55:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnBillingReportInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 05/15/2013 11:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnBillingReportInfo
-- Description:  This Sp returns multiple result sets that needed for Billing Report
-- Date Created: Wendsday, 15 May 2012
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnBillingReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountNameList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',
	@P_BillingPlanList nvarchar(MAX)='',
	@P_StartDate datetime ,
	@P_EndDate dateTime,
	@P_IsMonthlyBilling bit = 1,
	@P_LoggedAccount int 
)
AS
BEGIN
	-- Get the accounts	
	SET NOCOUNT ON
			
		SELECT DISTINCT					
				a.ID,
				a.Name AS AccountName,
				at.Name AS AccountTypeName,
				bp.Name AS BillingPlanName,
				a.GPPDiscount,
				bp.Proofs,
				(SELECT [dbo].[GetBillingCycles](a.ID, @P_StartDate, @P_EndDate )) AS BillingCycleDetails,
				a.Parent,
				a.SelectedBillingPlan AS BillingPlan,
				bp.IsFixed AS IsFixedPlan,				
				ISNULL((SELECT Name+',' 
					FROM Account 
					WHERE Parent= a.ID
					GROUP BY Name FOR XML PATH('')),'') AS Subordinates													
		FROM	Account a
		INNER JOIN Company c 
				ON c.Account = a.ID
		INNER JOIN AccountType at 
				ON a.AccountType = at.ID
		INNER JOIN AccountStatus ast 
				ON a.[Status] = ast.ID		
		INNER JOIN BillingPlan bp 
				ON a.SelectedBillingPlan = bp.ID				
		WHERE	a.NeedSubscriptionPlan = 1
		AND a.ContractStartDate IS NOT NULL
		AND a.IsTemporary =0
		AND (ast.[Key] = 'A')
		AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountNameList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountNameList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND (@P_BillingPlanList = '' OR a.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_BillingPlanList, ',')))
		
END

GO


/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 05/16/2013 11:48:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnUserReportInfoView]'))
DROP VIEW [dbo].[ReturnUserReportInfoView]
GO

/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 05/16/2013 11:46:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReturnUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Name,
			'' AS StatusName,
			'' AS UserName,
			'' AS EmailAddress,
			GETDATE() AS DateLastLogin
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 05/16/2013 10:55:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnUserReportInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnUserReportInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserReportInfo]    Script Date: 05/15/2013 11:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnUserReportInfo
-- Description:  This Sp returns multiple result sets that needed for User Report
-- Date Created: Wendsday, 17 May 2012
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
	-- Get the users	
	SET NOCOUNT ON
			
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID																							
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ( (at.[Key] = 'GMHQ') OR (u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END

GO

USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- AccountStatus --
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('A','Active')
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('I','Inactive')
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('R','Draft')
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('S','Suspended')               
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('D','Deleted')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalAnnotationType --
--INSERT INTO [dbo].[ApprovalAnnotationType]([Key],[Name],[Priority])
--     VALUES('CMT','Comment',1)
--INSERT INTO [dbo].[ApprovalAnnotationType]([Key],[Name],[Priority])
--     VALUES('DRW','Draw',2)
--INSERT INTO [dbo].[ApprovalAnnotationType]([Key],[Name],[Priority])
--     VALUES('TXT','Text',3)
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalCommentType --
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('MKR', 'Marker Comment', 1)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('ARA', 'Area Comment', 2)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('TXT', 'Text Comment', 3)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('DRW', 'Draw Comment', 4)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('ADT', 'Add Text Comment', 5)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('CPR', 'Colorpicker Comment', 6)     
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalAnnotationStatus
INSERT INTO [dbo].[ApprovalAnnotationStatus]([Key] ,[Name] ,[Priority])
     VALUES('A' ,'Approved' ,1)
INSERT INTO [dbo].[ApprovalAnnotationStatus]([Key] ,[Name] ,[Priority])
     VALUES('D' ,'Declined' ,2)
INSERT INTO [dbo].[ApprovalAnnotationStatus]([Key] ,[Name] ,[Priority])
     VALUES('N' ,'Neutral' ,3)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalStatus--
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('NEW', 'New', 1)     
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('INP', 'In Progress', 2)  
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('COM', 'Completed', 3)
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('ARC', 'Archived', 4)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalDecision--
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('PDG', 'Pending', 1, 'A decision is still required')
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('CHR', 'Changes Required', 2, 'You have requested changes to the approval')
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('AWC', 'Approved with Changes', 3, 'You have requested changes to the approval but do not need to see another version')  
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('APD', 'Approved', 4, 'You have approved the approval')          
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('NAP', 'Not Applicable', 5, 'You do not think that you need to make a decision on this approval')     
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalCollaboratorRole --
INSERT INTO [dbo].[ApprovalCollaboratorRole]([Key],[Name],[Priority])
     VALUES ('RDO','Read Only', 1)
INSERT INTO [dbo].[ApprovalCollaboratorRole]([Key],[Name],[Priority])
     VALUES ('RVW','Reviewer', 2)
INSERT INTO [dbo].[ApprovalCollaboratorRole]([Key],[Name],[Priority])
     VALUES ('ANR','Approver & Reviewer', 3)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- HighlightType
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('N' , 'Normal' , 1)
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('I' , 'Insert' , 2)
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('R' , 'Replace' , 3)
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('D' , 'Delete' , 4)               
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Smoothing --
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('NON' ,'None')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('ALL' ,'All')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('LNE' ,'Line')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('IMG' ,'Images')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('TXT' ,'Text')
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

--  Colorspace --
INSERT INTO [dbo].[Colorspace]([Key] ,[Name])
     VALUES('RGB' ,'RGB')
INSERT INTO [dbo].[Colorspace]([Key] ,[Name])
     VALUES('CMK' ,'CMYK')
INSERT INTO [dbo].[Colorspace]([Key] ,[Name])
     VALUES('GRY' ,'Gray')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Image Format --
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('JPG', 'JPEG', NULL)
GO
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('COM', 'Compression', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('MIN', 'JPEG_minimum', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('LOW', 'JPEG_low', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('MED', 'JPEG_medium', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('HIG', 'JPEG_high', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('MAX', 'JPEG_maximum', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('PNG', 'PNG', NULL)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('TIF', 'TIFF', NULL)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

--  Page Box --
INSERT INTO [dbo].[PageBox]([Key] ,[Name])
     VALUES('CRP', 'CROPBOX')
INSERT INTO [dbo].[PageBox]([Key] ,[Name])
     VALUES('TRM', 'TRIMBOX')
INSERT INTO [dbo].[PageBox]([Key] ,[Name])
     VALUES('BLD', 'BLEEDBOX')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

--  Tile Image Format --
INSERT INTO [dbo].[TileImageFormat]([Key] ,[Name])
     VALUES('JPG', 'JPG')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- MediaService --
INSERT INTO [dbo].[MediaService]([Name] ,[Smoothing] ,[Resolution] ,[Colorspace] ,[ImageFormat] ,[PageBox] ,[TileImageFormat] ,[JPEGCompression])
     VALUES('Basic', 2, 300, 1, 5, 1, 1 , 90)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- BillingPlanType --
INSERT INTO [dbo].[BillingPlanType]([Key], [Name], [MediaService])
     VALUES ('Essentials', 'Essentials', 1)
INSERT INTO [dbo].[BillingPlanType]([Key], [Name], [MediaService])
     VALUES ('PrePress', 'Pre Press', 1)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- BillingPlan --
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essentials Free', 1, 30, 0.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essentials 300', 1, 300, 250.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 600', 1, 600, 500.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 1000', 1, 1000, 1000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 2000', 1, 2000, 2000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 3000', 1, 3000, 3000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 4000', 1, 4000, 4000.0, 0)               
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 5000', 1, 5000, 5000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 6000', 1, 6000, 6000.0, 0)    
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress Free', 2, 20, 0.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 300', 2, 300, 325.0, 0)   
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 600', 2, 600, 650.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 1000', 2, 1000, 975.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 2000', 2, 2000, 1560.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 3000', 2, 3000, 2210.0, 0) 
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ContractPeriod --
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('ONEY', 'One Year')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('TWOY', 'Two Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('THRY', 'Three Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('FORY', 'Four Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('FIVY', 'Five Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('NOLT', 'No Limit')              
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Currency --
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('Australian Dollar', 'AUD', '$')
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('British Pound', 'GBP', '�')     
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('EURO', 'EUR', '�')
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('Japanese Yen', 'JPY', '�')
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('US Dollar', 'USD', '$')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- DateFormat --
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('MM/dd/yyyy', '04/16/2012')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('dd/MM/yyyy', '16/04/2012')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('yyyy-MM-dd', '2012-04-16')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('dd.MM.yyyy', '16.04.2012')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('yyyy.MM.dd', '2012.04.16')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('yyyy/MM/dd', '2012/04/16')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*/

-- File Types --
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('jpeg','Joint Photographic Experts Group')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('jpg','Joint Photographic Experts Group')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('pdf','Portable Document Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('png','Portable Network Graphics')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('tiff','Tagged Image File Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('tif','Tagged Image File Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('eps','Encapsulated Post Script')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mp4','Moving Pictures Experts Group Audio Layer 4(MPEG-4)')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('m4v','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('flv','Flash Live Video')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('f4v','Flash Video')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mov','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mpg','Moving Picture Experts Group')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mpg2','Moving Picture Experts Group Layer 2')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('3gp','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('wmv','Windows Medoa Video')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('avi','Audio Video Interleaved')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('asf','Advanced Systems Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('fm','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('ogv','')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('bmp','Windows Bitmap')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('doc','Microsoft Word Document')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('docx','Word 2007 XML Document')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('gif','Graphics Interchange Format')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('htm','Hypertext Markup Language File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('html','Hypertext Markup Language File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('pps','PowerPoint Slideshow')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('ppt','PowerPoint Presentation')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('pptx','PowerPoint 2007 XML Presentation')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('psd','Adobe PhotoShop Document')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('swf','Shockwave Flash')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('ttf','TrueType Font')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('txt','Plain Text File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('xls','Microsoft Excel Spreadsheet')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('xlsx','Excel 2007 XML Workbook')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('xml','XML File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('zip','Zipped File')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Locale --
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('zh-Hans' ,'Chinese (Simplified) - China')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('zh-Hant' ,'Chinese (Traditional) - China')     
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('en-US' ,'English - United States')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('fr-FR' ,'French - France')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('de-DE' ,'German - Germany')      
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('it-IT' ,'Italian - Italy')     
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('ko-KR' ,'Korean - Korea')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('ja-JP' ,'Japanese - Japan')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('pt-PT' ,'Portuguese - Portugal')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('es-ES' ,'Spanish - Spain')    
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Notification Frequency --
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('AEO','As Events Occur')
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('DLY','Daily')
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('WLY','Weekly')
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('NVR','Never')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- PrePressFunction --
INSERT INTO [dbo].[PrePressFunction]([Key], [Name])
     VALUES ('CKRB' , 'CYMK/RGB Indicator')
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('OTPW' , 'Overprint Preview')
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('DSTR' , 'Densitometer')
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('CKSP' , 'CMYK Separations')          
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('ICCP' , 'ICC Profile Preview') 
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Preset --
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('L', 'Low')
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('M', 'Medium')
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('H', 'High')
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('C', 'Custom')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Theme --
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('D', 'Default')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('S', 'Splash')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('J', 'Juicy')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('F', 'Fresh')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('M', 'Moody')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- TimeFormat --
INSERT INTO [dbo].[TimeFormat]([Key], [Pattern], [Result])
     VALUES ('12HR', '12-hour clock', '4:20 PM')
INSERT INTO [dbo].[TimeFormat]([Key], [Pattern], [Result])
     VALUES ('24HR', '24-hour clock', '16:20')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- User Status --
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('A','Active')
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('N','Invited')
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('I','Inactive')
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('D','Delete')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ValueDataType --
INSERT INTO [dbo].[ValueDataType]([Key],[Name])
     VALUES('STRG', 'System.String')
INSERT INTO [dbo].[ValueDataType]([Key],[Name])
     VALUES('IN32', 'System.Int32')
INSERT INTO [dbo].[ValueDataType]([Key],[Name])
     VALUES('BOLN', 'System.Boolean')      
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- AccountSetting --
INSERT INTO [dbo].[AccountSetting]([Account] ,[Name] ,[Value] ,[ValueDataType] ,[Description])
     VALUES (NULL, 'PDF2SWFArguments', '-s transparent -T9 -S -s jpegquality=95 -s subpixels=150 \"{0}\" \"{1}\"' , 1, 'Arguments for the pdf2swf.exe for this account')
INSERT INTO [dbo].[AccountSetting]([Account] ,[Name] ,[Value] ,[ValueDataType] ,[Description])
     VALUES (NULL, 'GraphicsMagickConvertNotRGBArguments', 'convert " + epsOptions + " \"" + inputFilePath + "\"[0] +profile icm " + srcprofile + " -intent perceptual -profile \"" + TargetRgbColorProfilePath + "\" -resize " + Convert.ToString(Convert.ToInt32(width)) + "x" + Convert.ToString(Convert.ToInt32(height)) + " +compress -colorspace RGB " + resolution + " " + flatten + " \"" + outputFilePath + "\"' , 1, 'GraphicsMagick convert aguments if source file is not RGB or extended image')
INSERT INTO [dbo].[AccountSetting]([Account] ,[Name] ,[Value] ,[ValueDataType] ,[Description])
     VALUES (NULL, 'GraphicsMagickConvertRGBArguments', 'convert " + epsOptions + " \"" + inputFilePath + "\"[0] -resize " + Convert.ToString(Convert.ToInt32(width)) + "x" + Convert.ToString(Convert.ToInt32(height)) + "  +compress -colorspace RGB " + resolution + " " + flatten + " \"" + outputFilePath + "\"' , 1, 'GraphicsMagick convert aguments if source file is RGB')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Country --
SET IDENTITY_INSERT [dbo].[Country] ON
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (1, N'AD', N'AND', 20, NULL, N'Principality of Andorra', N'Andorra', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (2, N'AE', N'ARE', 784, NULL, N'United Arab Emirates', N'United Arab Emirates', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (3, N'AF', N'AFG', 4, NULL, N'Afghanistan', N'Afghanistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (4, N'AG', N'ATG', 28, NULL, N'Antigua and Barbuda', N'Antigua and Barbuda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (5, N'AI', N'AIA', 660, NULL, N'Anguilla', N'Anguilla', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (6, N'AL', N'ALB', 8, NULL, N'People''s Socialist Republic of Albania', N'Albania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (7, N'AM', N'ARM', 51, NULL, N'Armenia', N'Armenia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (8, N'AN', N'ANT', 530, NULL, N'Netherlands Antilles', N'Netherlands Antilles', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (9, N'AO', N'AGO', 24, NULL, N'Republic of Angola', N'Angola', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (10, N'AQ', N'ATA', 10, NULL, N'Antarctica', N'Antarctica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (11, N'AR', N'ARG', 32,  NULL, N'Argentine Republic', N'Argentina', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (12, N'AS', N'ASM', 16, NULL, N'American Samoa', N'American Samoa', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (13, N'AT', N'AUT', 40, NULL, N'Republic of Austria', N'Austria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (14, N'AU', N'AUS', 36, NULL, N'Commonwealth of Australia', N'Australia', 1)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (15, N'AW', N'ABW', 533, NULL, N'Aruba', N'Aruba', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (16, N'AZ', N'AZE', 31, NULL, N'Republic of Azerbaijan', N'Azerbaijan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (17, N'BA', N'BIH', 70, NULL, N'Bosnia and Herzegovina', N'Bosnia and Herzegovina', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (18, N'BB', N'BRB', 52, NULL, N'Barbados', N'Barbados', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (19, N'BD', N'BGD', 50, NULL, N'People''s Republic of Bangladesh', N'Bangladesh', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (20, N'BE', N'BEL', 56, NULL, N'Kingdom of Belgium', N'Belgium', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (21, N'BF', N'BFA', 854, NULL, N'Burkina Faso', N'Burkina Faso', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (22, N'BG', N'BGR', 100, NULL, N'People''s Republic of Bulgaria', N'Bulgaria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (23, N'BH', N'BHR', 48, NULL, N'Kingdom of Bahrain', N'Bahrain', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (24, N'BI', N'BDI', 108, NULL, N'Republic of Burundi', N'Burundi', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (25, N'BJ', N'BEN', 204, NULL, N'People''s Republic of Benin', N'Benin', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (26, N'BM', N'BMU', 60, NULL, N'Bermuda', N'Bermuda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (27, N'BN', N'BRN', 96, NULL, N'Brunei Darussalam', N'Brunei', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (28, N'BO', N'BOL', 68, NULL, N'Republic of Bolivia', N'Bolivia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (29, N'BR', N'BRA', 76, NULL, N'Federative Republic of Brazil', N'Brazil', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (30, N'BS', N'BHS', 44, NULL, N'Commonwealth of the Bahamas', N'Bahamas', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (31, N'BT', N'BTN', 64, NULL, N'Kingdom of Bhutan', N'Bhutan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (32, N'BV', N'BVT', 74, NULL, N'Bouvet Island', N'Bouvet Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (33, N'BW', N'BWA', 72, NULL, N'Republic of Botswana', N'Botswana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (34, N'BY', N'BLR', 112, NULL, N'Belarus', N'Belarus', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (35, N'BZ', N'BLZ', 84, NULL, N'Belize', N'Belize', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (36, N'CA', N'CAN', 124, NULL, N'Canada', N'Canada', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (37, N'CC', N'CCK', 166, NULL, N'Cocos (Keeling) Islands', N'Cocos Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (38, N'CD', N'COD', 180, NULL, N'Democratic Republic of Congo', N'Congo', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (39, N'CF', N'CAF', 140, NULL, N'Central African Republic', N'Central African Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (40, N'CG', N'COG', 178, NULL, N'People''s Republic of Congo', N'Congo', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (41, N'CH', N'CHE', 756, NULL, N'Swiss Confederation Switzerland', N'Switzerland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (42, N'CI', N'CIV', 384, NULL, N'Republic of the Ivory Coast', N'Ivory Coast', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (43, N'CK', N'COK', 184, NULL, N'Cook Islands', N'Cook Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (44, N'CL', N'CHL', 152, NULL, N'Republic of Chile', N'Chile', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (45, N'CM', N'CMR', 120, NULL, N'United Republic of Cameroon', N'Cameroon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (46, N'CN', N'CHN', 156, NULL, N'People''s Republic of China', N'China', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (47, N'CO', N'COL', 170, NULL, N'Republic of Colombia', N'Colombia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (48, N'CR', N'CRI', 188, NULL, N'Republic of Costa Rica', N'Costa Rica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (49, N'CS', N'SCG', 891, NULL, N'Serbia and Montenegro', N'Serbia and Montenegro', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (50, N'CU', N'CUB', 192, NULL, N'Republic of Cuba', N'Cuba', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (51, N'CV', N'CPV', 132, NULL, N'Republic of Cape Verde', N'Cape Verde', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (52, N'CX', N'CXR', 162, NULL, N'Christmas Island', N'Christmas Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (53, N'CY', N'CYP', 196, NULL, N'Republic of Cyprus', N'Cyprus', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (54, N'CZ', N'CZE', 203, NULL, N'Czech Republic', N'Czech Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (55, N'DE', N'DEU', 276, NULL, N'Germany', N'Germany', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (56, N'DJ', N'DJI', 262, NULL, N'Republic of Djibouti', N'Djibouti', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (57, N'DK', N'DNK', 208, NULL, N'Kingdom of Denmark', N'Denmark', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (58, N'DM', N'DMA', 212, NULL, N'Commonwealth of Dominica', N'Dominica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (59, N'DO', N'DOM', 214, NULL, N'Dominican Republic', N'Dominican Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (60, N'DZ', N'DZA', 12, NULL, N'People''s Democratic Republic of Algeria', N'Algeria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (61, N'EC', N'ECU', 218, NULL, N'Republic of Ecuador', N'Ecuador', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (62, N'EE', N'EST', 233, NULL, N'Estonia', N'Estonia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (63, N'EG', N'EGY', 818, NULL, N'Arab Republic of Egypt', N'Egypt', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (64, N'EH', N'ESH', 732, NULL, N'Western Sahara', N'Western Sahara', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (65, N'ER', N'ERI', 232, NULL, N'Eritrea', N'Eritrea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (66, N'ES', N'ESP', 724, NULL, N'Spanish State Spain', N'Spain', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (67, N'ET', N'ETH', 231, NULL, N'Ethiopia', N'Ethiopia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (68, N'FI', N'FIN', 246, NULL, N'Republic of Finland', N'Finland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (69, N'FJ', N'FJI', 242, NULL, N'Republic of the Fiji Islands Fiji', N'Fiji', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (70, N'FK', N'FLK', 238, NULL, N'Falkland Islands', N'Falkland Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (71, N'FM', N'FSM', 583, NULL, N'Federated States of Micronesia', N'Micronesia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (72, N'FO', N'FRO', 234, NULL, N'Faeroe Islands', N'Faeroe Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (73, N'FR', N'FRA', 250, NULL, N'French Republic France', N'France', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (74, N'GA', N'GAB', 266, NULL, N'Gabonese Republic Gabon', N'Gabon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (75, N'GB', N'GBR', 826, NULL, N'United Kingdom of Great Britain & N. Ireland', N'United Kingdom', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (76, N'GD', N'GRD', 308, NULL, N'Grenada', N'Grenada', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (77, N'GE', N'GEO', 268, NULL, N'Georgia', N'Georgia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (78, N'GF', N'GUF', 254, NULL, N'French Guiana', N'French Guiana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (79, N'GH', N'GHA', 288, NULL, N'Republic of Ghana', N'Ghana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (80, N'GI', N'GIB', 292, NULL, N'Gibraltar', N'Gibraltar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (81, N'GL', N'GRL', 304, NULL, N'Greenland', N'Greenland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (82, N'GM', N'GMB', 270, NULL, N'Republic of the Gambia', N'Gambia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (83, N'GN', N'GIN', 324, NULL, N'Revolutionary People''s Republic of Guinea', N'Guinea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (84, N'GP', N'GLP', 312, NULL, N'Guadaloupe', N'Guadaloupe', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (85, N'GQ', N'GNQ', 226, NULL, N'Republic of Equatorial Guinea', N'Equatorial Guinea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (86, N'GR', N'GRC', 300, NULL, N'Hellenic Republic Greece', N'Greece', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (87, N'GS', N'SGS', 239, NULL, N'South Georgia and the South Sandwich Islands', N'South Georgia and the South Sandwich Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (88, N'GT', N'GTM', 320, NULL, N'Republic of Guatemala', N'Guatemala', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (89, N'GU', N'GUM', 316, NULL, N'Guam', N'Guam', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (90, N'GW', N'GNB', 624, NULL, N'Republic of Guinea-Bissau', N'Guinea-Bissau', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (91, N'GY', N'GUY', 328, NULL, N'Republic of Guyana', N'Guyana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (92, N'HK', N'HKG', 344, NULL, N'Special Administrative Region of China Hong Kong', N'Hong Kong', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (93, N'HM', N'HMD', 334, NULL, N'Heard and McDonald Islands', N'Heard and McDonald Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (94, N'HN', N'HND', 340, NULL, N'Republic of Honduras', N'Honduras', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (95, N'HR', N'HRV', 191, NULL, N'Hrvatska', N'Hrvatska', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (96, N'HT', N'HTI', 332, NULL, N'Republic of Haiti', N'Haiti', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (97, N'HU', N'HUN', 348, NULL, N'Hungarian People''s Republic Hungary', N'Hungary', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (98, N'ID', N'IDN', 360, NULL, N'Republic of Indonesia', N'Indonesia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (99, N'IE', N'IRL', 372, NULL, N'Ireland', N'Ireland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (100, N'IL', N'ISR', 376, NULL, N'State of Israel', N'Israel', 0)

INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (101, N'IN', N'IND', 356, NULL, N'Republic of India', N'India', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (102, N'IO', N'IOT', 86, NULL, N'British Indian Ocean Territory', N'British Indian Ocean Territory', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (103, N'IQ', N'IRQ', 368, NULL, N'Republic of Iraq', N'Iraq', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (104, N'IR', N'IRN', 364, NULL, N'Islamic Republic of Iran', N'Iran', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (105, N'IS', N'ISL', 352, NULL, N'Republic of Iceland', N'Iceland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (106, N'IT', N'ITA', 380, NULL, N'Italian Republic Italy', N'Italy', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (107, N'JM', N'JAM', 388, NULL, N'Jamaica', N'Jamaica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (108, N'JO', N'JOR', 400, NULL, N'Hashemite Kingdom of Jordan', N'Jordan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (109, N'JP', N'JPN', 392, NULL, N'Japan', N'Japan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (110, N'KE', N'KEN', 404, NULL, N'Republic of Kenya', N'Kenya', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (111, N'KG', N'KGZ', 417, NULL, N'Kyrgyz Republic', N'Kyrgyz Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (112, N'KH', N'KHM', 116, NULL, N'Kingdom of Cambodia', N'Cambodia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (113, N'KI', N'KIR', 296, NULL, N'Republic of Kiribati', N'Kiribati', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (114, N'KM', N'COM', 174, NULL, N'Union of the Comoros', N'Comoros', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (115, N'KN', N'KNA', 659, NULL, N'St. Kitts and Nevis', N'St. Kitts and Nevis', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (116, N'KP', N'PRK', 408, NULL, N'Democratic People''s Republic of Korea', N'North Korea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (117, N'KR', N'KOR', 410, NULL, N'Republic of Korea', N'South Korea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (118, N'KW', N'KWT', 414, NULL, N'State of Kuwait', N'Kuwait', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (119, N'KY', N'CYM', 136, NULL, N'Cayman Islands', N'Cayman Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (120, N'KZ', N'KAZ', 398, NULL, N'Republic of Kazakhstan', N'Kazakhstan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (121, N'LA', N'LAO', 418, NULL, N'Lao People''s Democratic Republic', N'Lao People''s Democratic Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (122, N'LB', N'LBN', 422, NULL, N'Lebanese Republic Lebanon', N'Lebanon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (123, N'LC', N'LCA', 662, NULL, N'St. Lucia', N'St. Lucia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (124, N'LI', N'LIE', 438, NULL, N'Principality of Liechtenstein', N'Liechtenstein', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (125, N'LK', N'LKA', 144, NULL, N'Democratic Socialist Republic of Sri Lanka', N'Sri Lanka', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (126, N'LR', N'LBR', 430, NULL, N'Republic of Liberia', N'Liberia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (127, N'LS', N'LSO', 426, NULL, N'Kingdom of Lesotho', N'Lesotho', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (128, N'LT', N'LTU', 440, NULL, N'Lithuania', N'Lithuania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (129, N'LU', N'LUX', 442, NULL, N'Grand Duchy of Luxembourg', N'Luxembourg', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (130, N'LV', N'LVA', 428, NULL, N'Latvia', N'Latvia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (131, N'LY', N'LBY', 434, NULL, N'Libyan Arab Jamahiriya', N'Libyan Arab Jamahiriya', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (132, N'MA', N'MAR', 504, NULL, N'Kingdom of Morocco', N'Morocco', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (133, N'MC', N'MCO', 492, NULL, N'Principality of Monaco', N'Monaco', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (134, N'MD', N'MDA', 498, NULL, N'Republic of Moldova', N'Moldova', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (135, N'MG', N'MDG', 450, NULL, N'Republic of Madagascar', N'Madagascar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (136, N'MH', N'MHL', 584, NULL, N'Marshall Islands', N'Marshall Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (137, N'MK', N'MKD', 807, NULL, N'the former Yugoslav Republic of Macedonia', N'Macedonia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (138, N'ML', N'MLI', 466, NULL, N'Republic of Mali', N'Mali', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (139, N'MM', N'MMR', 104, NULL, N'Myanmar', N'Myanmar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (140, N'MN', N'MNG', 496, NULL, N'Mongolian People''s Republic Mongolia', N'Mongolia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (141, N'MO', N'MAC', 446, NULL, N'Special Administrative Region of China Macao', N'Macao', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (142, N'MP', N'MNP', 580, NULL, N'Northern Mariana Islands', N'Northern Mariana Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (143, N'MQ', N'MTQ', 474, NULL, N'Martinique', N'Martinique', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (144, N'MR', N'MRT', 478, NULL, N'Islamic Republic of Mauritania', N'Mauritania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (145, N'MS', N'MSR', 500, NULL, N'Montserrat', N'Montserrat', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (146, N'MT', N'MLT', 470, NULL, N'Republic of Malta', N'Malta', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (147, N'MU', N'MUS', 480, NULL, N'Mauritius', N'Mauritius', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (148, N'MV', N'MDV', 462, NULL, N'Republic of Maldives', N'Maldives', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (149, N'MW', N'MWI', 454, NULL, N'Republic of Malawi', N'Malawi', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (150, N'MX', N'MEX', 484, NULL, N'United Mexican States Mexico', N'Mexico', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (151, N'MY', N'MYS', 458, NULL, N'Malaysia', N'Malaysia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (152, N'MZ', N'MOZ', 508, NULL, N'People''s Republic of Mozambique', N'Mozambique', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (153, N'NA', N'NAM', 516, NULL, N'Namibia', N'Namibia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (154, N'NC', N'NCL', 540, NULL, N'New Caledonia', N'New Caledonia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (155, N'NE', N'NER', 562, NULL, N'Republic of the Niger', N'Niger', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (156, N'NF', N'NFK', 574, NULL, N'Norfolk Island', N'Norfolk Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (157, N'NG', N'NGA', 566, NULL, N'Federal Republic of Nigeria', N'Nigeria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (158, N'NI', N'NIC', 558, NULL, N'Republic of Nicaragua', N'Nicaragua', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (159, N'NL', N'NLD', 528, NULL, N'Kingdom of the Netherlands', N'Netherlands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (160, N'NO', N'NOR', 578, NULL, N'Kingdom of Norway', N'Norway', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (161, N'NP', N'NPL', 524, NULL, N'Kingdom of Nepal', N'Nepal', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (162, N'NR', N'NRU', 520, NULL, N'Republic of Nauru', N'Nauru', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (163, N'NU', N'NIU', 570, NULL, N'Republic of Niue', N'Niue', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (164, N'NZ', N'NZL', 554, NULL, N'New Zealand', N'New Zealand', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (165, N'OM', N'OMN', 512, NULL, N'Sultanate of Oman', N'Oman', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (166, N'PA', N'PAN', 591, NULL, N'Republic of Panama', N'Panama', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (167, N'PE', N'PER', 604, NULL, N'Republic of Peru', N'Peru', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (168, N'PF', N'PYF', 258, NULL, N'French Polynesia', N'French Polynesia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (169, N'PG', N'PNG', 598, NULL, N'Papua New Guinea', N'Papua New Guinea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (170, N'PH', N'PHL', 608, NULL, N'Republic of the Philippines', N'Philippines', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (171, N'PK', N'PAK', 586, NULL, N'Islamic Republic of Pakistan', N'Pakistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (172, N'PL', N'POL', 616, NULL, N'Polish People''s Republic Poland', N'Poland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (173, N'PM', N'SPM', 666, NULL, N'St. Pierre and Miquelon', N'St. Pierre and Miquelon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (174, N'PN', N'PCN', 612, NULL, N'Pitcairn Island', N'Pitcairn Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (175, N'PR', N'PRI', 630, NULL, N'Puerto Rico', N'Puerto Rico', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (176, N'PS', N'PSE', 275, NULL, N'Occupied Palestinian Territory', N'Palestinian Territory', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (177, N'PT', N'PRT', 620, NULL, N'Portuguese Republic Portugal', N'Portugal', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (178, N'PW', N'PLW', 585, NULL, N'Palau', N'Palau', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (179, N'PY', N'PRY', 600, NULL, N'Republic of Paraguay', N'Paraguay', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (180, N'QA', N'QAT', 634, NULL, N'State of Qatar', N'Qatar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (181, N'RE', N'REU', 638, NULL, N'Reunion', N'Reunion', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (182, N'RO', N'ROU', 642, NULL, N'Socialist Republic of Romania', N'Romania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (183, N'RU', N'RUS', 643, NULL, N'Russian Federation', N'Russian Federation', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (184, N'RW', N'RWA', 646, NULL, N'Rwandese Republic Rwanda', N'Rwanda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (185, N'SA', N'SAU', 682, NULL, N'Kingdom of Saudi Arabia', N'Saudi Arabia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (186, N'SB', N'SLB', 90, NULL, N'Solomon Islands', N'Solomon Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (187, N'SC', N'SYC', 690, NULL, N'Republic of Seychelles', N'Seychelles', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (188, N'SD', N'SDN', 736, NULL, N'Democratic Republic of the Sudan', N'Sudan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (189, N'SE', N'SWE', 752, NULL, N'Kingdom of Sweden', N'Sweden', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (190, N'SG', N'SGP', 702, NULL, N'Republic of Singapore', N'Singapore', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (191, N'SH', N'SHN', 654, NULL, N'St. Helena', N'St. Helena', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (192, N'SI', N'SVN', 705, NULL, N'Slovenia', N'Slovenia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (193, N'SJ', N'SJM', 744, NULL, N'Svalbard & Jan Mayen Islands', N'Svalbard & Jan Mayen Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (194, N'SK', N'SVK', 703, NULL, N'Slovakia', N'Slovakia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (195, N'SL', N'SLE', 694, NULL, N'Republic of Sierra Leone', N'Sierra Leone', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (196, N'SM', N'SMR', 674, NULL, N'Republic of San Marino', N'San Marino', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (197, N'SN', N'SEN', 686, NULL, N'Republic of Senegal', N'Senegal', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (198, N'SO', N'SOM', 706, NULL, N'Somali Republic Somalia', N'Somalia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (199, N'SR', N'SUR', 740, NULL, N'Republic of Suriname', N'Suriname', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (200, N'ST', N'STP', 678, NULL, N'Democratic Republic of Sao Tome and Principe', N'Sao Tome and Principe', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (201, N'SV', N'SLV', 222, NULL, N'Republic of El Salvador', N'El Salvador', 0)

INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (202, N'SY', N'SYR', 760, NULL, N'Syrian Arab Republic', N'Syrian Arab Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (203, N'SZ', N'SWZ', 748, NULL, N'Kingdom of Swaziland', N'Swaziland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (204, N'TC', N'TCA', 796, NULL, N'Turks and Caicos Islands', N'Turks and Caicos Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (205, N'TD', N'TCD', 148, NULL, N'Republic of Chad', N'Chad', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (206, N'TF', N'ATF', 260, NULL, N'French Southern Territories', N'French Southern Territories', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (207, N'TG', N'TGO', 768, NULL, N'Togolese Republic Togo', N'Togo', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (208, N'TH', N'THA', 764, NULL, N'Kingdom of Thailand', N'Thailand', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (209, N'TJ', N'TJK', 762, NULL, N'Tajikistan', N'Tajikistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (210, N'TK', N'TKL', 772, NULL, N'Tokelau', N'Tokelau', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (211, N'TL', N'TLS', 626, NULL, N'Democratic Republic of Timor-Leste', N'Timor-Leste', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (212, N'TM', N'TKM', 795, NULL, N'Turkmenistan', N'Turkmenistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (213, N'TN', N'TUN', 788, NULL, N'Republic of Tunisia', N'Tunisia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (214, N'TO', N'TON', 776, NULL, N'Kingdom of Tonga', N'Tonga', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (215, N'TR', N'TUR', 792, NULL, N'Republic of Turkey', N'Turkey', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (216, N'TT', N'TTO', 780, NULL, N'Republic of Trinidad and Tobago', N'Trinidad and Tobago', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (217, N'TV', N'TUV', 798, NULL, N'Tuvalu', N'Tuvalu', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (218, N'TW', N'TWN', 158, NULL, N'Province of China Taiwan', N'Taiwan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (219, N'TZ', N'TZA', 834, NULL, N'United Republic of Tanzania', N'Tanzania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (220, N'UA', N'UKR', 804, NULL, N'Ukraine', N'Ukraine', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (221, N'UG', N'UGA', 800, NULL, N'Republic of Uganda', N'Uganda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (222, N'UM', N'UMI', 581, NULL, N'United States Minor Outlying Islands', N'United States Minor Outlying Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (223, N'US', N'USA', 840, NULL, N'United States of America', N'USA', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (224, N'UY', N'URY', 858, NULL, N'Eastern Republic of Uruguay', N'Uruguay', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (225, N'UZ', N'UZB', 860, NULL, N'Uzbekistan', N'Uzbekistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (226, N'VA', N'VAT', 336, NULL, N'Holy See', N'Holy See', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (227, N'VC', N'VCT', 670, NULL, N'St. Vincent and the Grenadines', N'St. Vincent and the Grenadines', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (228, N'VE', N'VEN', 862, NULL, N'Bolivarian Republic of Venezuela', N'Venezuela', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (229, N'VG', N'VGB', 92, NULL, N'British Virgin Islands', N'British Virgin Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (230, N'VI', N'VIR', 850, NULL, N'US Virgin Islands', N'US Virgin Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (231, N'VN', N'VNM', 704, NULL, N'Socialist Republic of Viet Nam', N'Viet Nam', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (232, N'VU', N'VUT', 548, NULL, N'Vanuatu', N'Vanuatu', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (233, N'WF', N'WLF', 876, NULL, N'Wallis and Futuna Islands', N'Wallis and Futuna Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (234, N'WS', N'WSM', 882, NULL, N'Independent State of Samoa', N'Samoa', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (235, N'YE', N'YEM', 887, NULL, N'Yemen', N'Yemen', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (236, N'YT', N'MYT', 175, NULL, N'Mayotte', N'Mayotte', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (237, N'ZA', N'ZAF', 710, NULL, N'Republic of South Africa', N'South Africa', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (238, N'ZM', N'ZMB', 894, NULL, N'Republic of Zambia', N'Zambia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (239, N'ZW', N'ZWE', 716, NULL, N'Zimbabwe', N'Zimbabwe', 0)
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Account --
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_Creator]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Creator]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_Modifier]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Modifier]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_Owner]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Owner]
GO

DECLARE @GMGcoZone int
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int

-- AccountType --
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('GMHQ', 'GMGcoZone')
SET @GMGcoZone = SCOPE_IDENTITY()

-- HQ Administrator Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Head Office','GMG CoZone', 'gmgcozone.com', @GMGcoZone, NULL, 1, LOWER(NEWID()),
			'Marcus', 'Wright', '+49 7071 93874-0', 'marcus.wright@gmgcolor.com',
			NULL, NULL, NULL, 1, '',
			3, 3, 'W. Europe Standard Time', 1, 1,
			0, 0, 1, NULL, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)

-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'GMG GmbH & Co. KG', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'marcus.wright@gmgcolor.com',55)

-- User (HQ Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Administrator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Marcus','Wright','marcus.wright@gmgcolor.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), '+447850844724', NULL, '+49 7071 93874-0', NULL, 4, 1)
SET @UserId = SCOPE_IDENTITY()

-- Administrator Role --
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('HQA', 'Administrator', 1)
SET @RoleId = SCOPE_IDENTITY()
  
-- AccuntType Role --
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(1, @RoleId)
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, @RoleId)
	
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Creator]
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Modifier]
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Owner]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Account Types --
DECLARE @GMGcoZone int
DECLARE @Subsidiary int
DECLARE @Dealer int
DECLARE @Client int
DECLARE @Subscriber int

-- Account Types - GMG coZone
SET @GMGcoZone = (SELECT [ID] FROM [dbo].[AccountType] WHERE [Key] = 'GMHQ')

-- Account Types - Subsidiary
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('SBSY', 'Subsidiary')
SET @Subsidiary = SCOPE_IDENTITY()
  
-- Account Types - Dealer   
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('DELR', 'Dealer')
SET @Dealer = SCOPE_IDENTITY()

-- Account Types - Client   
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('CLNT', 'Client')
SET @Client = SCOPE_IDENTITY()

-- Account Types - Subscriber
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('SBSC', 'Subscriber')
SET @Subscriber = SCOPE_IDENTITY()

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Roles --
DECLARE @GMGAdminRole int
DECLARE @GMGManagerRole int
DECLARE @AccountAdminRole int
DECLARE @AccountManagerRole int
DECLARE @AccountModeratorRole int
DECLARE @AccountContributorRole int
DECLARE @AccountViewerRole int

-- Role - GMG coZone Administrator
SET @GMGAdminRole = (SELECT [ID] FROM [dbo].[Role] WHERE [Key] = 'HQA')

-- Role - GMG coZone  Manager
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('HQM', 'Manager', 2)
SET @GMGManagerRole = SCOPE_IDENTITY()

-- Role - Account Administrator
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('ADM', 'Administrator', 1)
SET @AccountAdminRole = SCOPE_IDENTITY()

-- Role - Account Manager
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('MAN', 'Manager', 2)
SET @AccountManagerRole = SCOPE_IDENTITY()

-- Role - Moderator
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('MOD', 'Moderator', 3)
SET @AccountModeratorRole  = SCOPE_IDENTITY()

-- Role - Contributor
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('CON', 'Contributor', 4)
SET @AccountContributorRole   = SCOPE_IDENTITY() 

-- Role - Viewer
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('VIE', 'Viewer', 5)
SET @AccountViewerRole   = SCOPE_IDENTITY() 

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Account Type Role --
DECLARE @GMGCcoZoneGMGAdminRole int
DECLARE @GMGCcoZoneGMGManagerRole int

SET @GMGCcoZoneGMGAdminRole = (SELECT [ID] FROM [dbo].[AccountTypeRole] WHERE [AccountType] = @GMGcoZone AND [Role] = @GMGAdminRole)

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@GMGcoZone, @GMGManagerRole)     
SET @GMGCcoZoneGMGManagerRole   = SCOPE_IDENTITY() 


DECLARE @SubsidiaryAccountAdminRole int
DECLARE @SubsidiaryAccountManagerRole int
DECLARE @SubsidiaryAccountModeratorRole int
DECLARE @SubsidiaryAccountContributorRole int
DECLARE @SubsidiaryAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountAdminRole)
SET @SubsidiaryAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountManagerRole)
SET @SubsidiaryAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountModeratorRole)
SET @SubsidiaryAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountContributorRole)
SET @SubsidiaryAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountViewerRole)
SET @SubsidiaryAccountViewerRole   = SCOPE_IDENTITY()


DECLARE @DealerAccountAdminRole int
DECLARE @DealerAccountManagerRole int
DECLARE @DealerAccountModeratorRole int
DECLARE @DealerAccountContributorRole int
DECLARE @DealerAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountAdminRole)
SET @DealerAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer , @AccountManagerRole)
SET @DealerAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountModeratorRole)
SET @DealerAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountContributorRole)
SET @DealerAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountViewerRole)
SET @DealerAccountViewerRole   = SCOPE_IDENTITY() 
     
     
DECLARE @ClientAccountAdminRole int
DECLARE @ClientAccountManagerRole int
DECLARE @ClientAccountModeratorRole int
DECLARE @ClientAccountContributorRole int
DECLARE @ClientAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountAdminRole)
SET @ClientAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountManagerRole)
SET @ClientAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountModeratorRole)
SET @ClientAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountContributorRole)
SET @ClientAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountViewerRole)
SET @ClientAccountViewerRole   = SCOPE_IDENTITY()     
     

DECLARE @SubscriberAccountAdminRole int
DECLARE @SubscriberAccountManagerRole int
DECLARE @SubscriberAccountModeratorRole int
DECLARE @SubscriberAccountContributorRole int
DECLARE @SubscriberAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountAdminRole)
SET @SubscriberAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountManagerRole)
SET @SubscriberAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountModeratorRole)
SET @SubscriberAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountContributorRole)
SET @SubscriberAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountViewerRole)
SET @SubscriberAccountViewerRole   = SCOPE_IDENTITY()    
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

DECLARE @ControllerActionId int
DECLARE @ParentMenuItemId int
DECLARE @ChildMenuItemId int
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- ADMIN SITE MENU ITEMS -----------------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Dashboard Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index (for admin app) --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Dashboard', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 1, 1, 1, 0, 1, 1, 'DBIN', 'Dashboard','Dashboard')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole) 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)

-- Index (for proof app) --	 
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, 0, 0, 1, 1, 1, 'DBAP', 'Administrator Site','Dashboard')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole) 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
/*-- End Dashboard Menu --------------------------------------------------------------------------------------------------------------------------------------------*/	 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Accounts Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index (for admin app) --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 2, 1, 1, 0, 1, 1, 'ACIN', 'Accounts', 'Account')
SET @ParentMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)

-- Index (for proof app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 0, 0, 1, 1, 1, 'ACST', 'Accounts', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
/*-- New Account ---------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- NewAccount --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'NewAccount','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACNW', 'New Account', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- NewAccountInfo--
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountInfo','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACAI', 'Account Info', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- NewAccountPlans --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountPlans','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACAP', 'Account Plans', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- NewAccountCustomise --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountCustomise','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACAC', 'Account Customise', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- NewAccountConfirm --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountConfirm','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACCA', 'Confirm Account', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

/*-- Edit Account --------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Summary --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Summary','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, 1, 1, 0, 0, 1, 'ACSM', 'Summary', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole) 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- Profile --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Profile','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 1, 0, 0, 1, 'ACPF', 'Profile', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- Customise --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Customise','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, 1, 1, 0, 0, 1, 'ACCU', 'Customise', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
-- SiteSettings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'SiteSettings','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, 1, 1, 0, 0, 1, 'ACSS', 'Site Settings', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- WhiteLabel --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'WhiteLabel','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 5, 1, 1, 0, 0, 1, 'ACWL', 'White Labels', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- DNSAndCustomDomain --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'DNSAndCustomDomain','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 6, 1, 1, 0, 0, 1, 'ACDN', 'DNS & Custom Domain', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- Plans --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Plans','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 7, 1, 1, 0, 0, 1, 'ACPL', 'Plans', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- BillingInfo --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'BillingInfo','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 8, 1, 1, 0, 0, 1, 'ACBI', 'Billing Info', 'Account') 
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 	 
-- Users --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Users','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 9, 1, 1, 0, 0, 1, 'ACUS', 'Users', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- EditUser --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'EditUser','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 9, 1, 0, 0, 0, 0, 'ACEU', 'Edit User', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- SuspendAccount --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'SuspendAccount','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 10, 1, 1, 0, 0, 1, 'ACSA', 'Suspend Account', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
/*-- End Accounts Menu ---------------------------------------------------------------------------------------------------------------------------------------------*/	 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Plans Menu ----------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index ---
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Plans', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 3, 1, 1, 0, 1, 1, 'PLIN', 'Plans', 'Plans')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)	  
	 
-- NewPlan ---
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Plans', 'AddEditBillingPlan','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'PLBP', 'Add Edit Plan','Plans')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	
/*-- End Plans Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/	 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Reports Menu --------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 4, 1, 1, 0, 1, 1, 'RPIN', 'Reports','Reports')
SET @ParentMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)	 	 
	 	 
-- AccountReport --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'AccountReport','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 1, 0, 0, 1, 'RPAR', 'Account Reports','Reports')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
	 	 
-- UserReport --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'UserReport','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, 1, 1, 0, 0, 1, 'RPUR', 'User Reports','Reports')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
	 	 
-- BillingReport --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'BillingReport','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, 1, 1, 0, 0, 1, 'RPBR', 'Billing Reports','Reports')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
	  
-- Print --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'Print','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, 1, 1, 0, 0, 1, 'RPPR', 'Print','Reports') 
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 	 
/*-- End Reports Menu ----------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Services Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Services',  'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 5, 1, 1, 0, 1, 1, 'SVIN', 'Services','Services')
SET @ParentMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 		 	 
	 	 
-- AccountReport --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Services',  'MediaServices','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 1, 0, 0, 1, 'SVMS', 'Media Services','Services')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
/*-- End Services Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Global Settings Menu -----------------------------------------------------------------------------------------------------------------------------------------*/
	 
--  Settings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings',  'Global','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 6, 1, 1, 0, 1, 1, 'GSET', 'Settings','Settings')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	

-- Global Settings --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, NULL, 1, 0, 0, 1, 'GSEP', 'Global Settings','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	  
-- Reserved Domains --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'ReservedDomains','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, NULL, 1, 0, 0, 1, 'SERD', 'Reserved Sub Domains','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
/*-- End Settings Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- Top Right Menu Items Under The Account -----------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Settings Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 1, NULL, 1, 1, 1, 1, 'SEIN', 'Account Settings','Settings')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)

-- Summary --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, NULL, 1, 0, 0, 1, 'SESM', 'Summary','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
     
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Profile --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Profile','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, NULL, 1, 0, 0, 1,'SEPF', 'Profile','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Customise --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Customise','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, NULL, 1, 0, 0, 1, 'SECU', 'Customise','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
	 	 
-- SiteSettings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'SiteSettings','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, NULL, 1, 0, 0, 1, 'SESS', 'SiteSettings','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
	 
-- WhiteLabelSettings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'WhiteLabelSettings','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 5, NULL, 1, 0, 0, 1, 'SEWL', 'WhiteLabelSettings','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- DNSAndCustomDomain --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'DNSAndCustomDomain','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 6, NULL, 1, 0, 0, 1, 'SEDN', 'DNS & Custom Domain','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Users --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Users','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 7, NULL, 1, 0, 0, 1, 'SEUS', 'Users','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

--Edit User--
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'EditUser','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 7, NULL, 0, 0, 0, 0, 'SEEU', 'Edit User','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
	 
--UserGroups--
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'UserGroups','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 8, NULL, 1, 0, 0, 1, 'SEGR', 'User Groups','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Add Edit User Groups --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'AddEditUserGroup','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 8, NULL, 0, 0, 0, 0, 'SEEG', 'Add Edit User Group','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- ExternalUsers --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'ExternalUsers','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 9, NULL, 1, 0, 0, 1, 'SEEX', 'External Users','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
/*-- End Reports Menu ---------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- Top Right Menu Items Under The User Name ---------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Welcome Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index --
--INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
--	 VALUES ('Welcome', 'Index','')
--SET @ControllerActionId = SCOPE_IDENTITY()

--INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
--	 VALUES (@ControllerActionId, NULL, 1, NULL, 0, 1, 0, 1, 'WLIN', 'Welcome','Welcome')
--SET @ParentMenuItemId = SCOPE_IDENTITY()

--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	  
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)
/*-- End Welcome Menu ----------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Personal Settings Menu ----------------------------------------------------------------------------------------------------------------------------------------*/

-- Informaion --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 2, NULL, 0, 1, 0, 1, 'PSIN', 'Edit Profile','PersonalSettings')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)
	 
-- Information --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, NULL, 1, 0, 0, 1, 'PSPI', 'Profile Information','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()	

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)

-- Photo --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Photo','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, NULL, 1, 0, 0, 1, 'PSPH', 'Photo','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)

-- Password --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Password','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, NULL, 1, 0, 0, 1, 'PSPW', 'Password','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)
	 
-- Notifications --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Notifications','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, NULL, 1, 0, 0, 1, 'PSNT', 'Notifications','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
/*-- End Personal Settings Menu ------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Help Menu -----------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Help', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 3, NULL, 0, 1, 0, 1, 'HLIN', 'Help','Help')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)	 
/*-- End Help Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- APPLICATION SITE MENU ITEMS ----------------------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Dashboard Menu -----------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index (for proof app) --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Studio', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 1, 0, 1, 0, 1, 1, 'STIN', 'Dashboard','Studio')
SET @ParentMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)
	 
-- Index (for admin app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, 1, 0, 1, 1, 1, 'STAD', 'Application Site','Studio')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole) 
/*-- End Dashboard Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Flex Menu -----------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Flex --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Studio', 'Flex','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, 0, 1, 0, 1, 0, 'STFL', 'Flex','Studio')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)
/*-- End Flex Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Approvals Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- Index --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

-- Index (for proof app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 2, 0, 1, 0, 1, 1, 'APIN', 'Approvals','Approvals')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)

-- Index (for admin app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 0, 1, 1, 1, 'APDB', 'Approvals','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole) 

-- Populate --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Populate','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APPO', 'Populate Approvals','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)	 

-- New Approval --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'NewApproval','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APNA', 'New Approval','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)

-- ApprovalDetails --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Details','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APDE', 'Approval Details','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)
	 
-- ApprovalDetails --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Annotations','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APDE', 'Approval Annotations','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)	 	 
/*-- End Approvals Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
-- End Studio Controler Actions --
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Notification Engine  --

DECLARE @GroupId int
DECLARE @EventTypeId int

-- Group Comments --
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('C', 'Comments')
SET @GroupId = SCOPE_IDENTITY()     
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'CCM', 'When a comment is made....') -- When a comment is made (reply to someone else comment- any comment)
SET @EventTypeId = SCOPE_IDENTITY()       
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'CRC', 'Replies to my comments') 
SET @EventTypeId = SCOPE_IDENTITY()       

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'CNC', 'New comments made')  
SET @EventTypeId = SCOPE_IDENTITY()       

-- Group Approvals --
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('A', 'Approvals')
SET @GroupId = SCOPE_IDENTITY()  

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAA', 'New approvals added') 
SET @EventTypeId = SCOPE_IDENTITY()
  
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'ADW', 'Approvals was downloaded') 
SET @EventTypeId = SCOPE_IDENTITY()
  
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAU', 'Approvals was updated')
SET @EventTypeId = SCOPE_IDENTITY()
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAD', 'Approvals was deleted') 
SET @EventTypeId = SCOPE_IDENTITY()
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AVC', 'New version was created') 
SET @EventTypeId = SCOPE_IDENTITY()
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAS', 'Approval was shared') 
SET @EventTypeId = SCOPE_IDENTITY()

-- Group Users --     
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('U', 'Users')
SET @GroupId = SCOPE_IDENTITY()  

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'UUA', 'User was added') 
SET @EventTypeId = SCOPE_IDENTITY()
    
-- Group Groups --     
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('G', 'Groups')
SET @GroupId = SCOPE_IDENTITY()  

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'GGC', 'Group was created')
SET @EventTypeId = SCOPE_IDENTITY()

-- Group Groups --     
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('M', 'Manage')
SET @GroupId = SCOPE_IDENTITY() 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'MPC', 'Plan was changed') 
SET @EventTypeId = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'MAD', 'Account was deleted')
SET @EventTypeId = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'MAA', 'Account was added') 
SET @EventTypeId = SCOPE_IDENTITY()

GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- RolePresetEventType --

--Low--

--Administrator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 1, 4)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 1, 4)
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 1, 4)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 1, 4)
--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 1, 4)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 1, 2)	
-------------------------------------------------------------------------------------------------------------------------------------------------
--Medium--

--Administrator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(1, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 12)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(3, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 10)
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(2, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 9)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(4, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 9)
--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(5, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 2, 8)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(6, 2, 3)
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--High--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(1, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 14)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(3, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 14)
		
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(2, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 11)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(4, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 11)
--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(5, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 9)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(6, 3, 3)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Custom Preset --
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(1, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 14)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(3, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 14)
	
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(2, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 11)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(4, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 11)

--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(5, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 9)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(6, 4, 3)
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- Currency Rates --
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (1, '1', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (2, '0.626', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (3, '0.774', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (4, '78.566', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (5, '0.9901', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- NativeDataType --
INSERT INTO [dbo].[NativeDataType]([Type])
     VALUES('System.Boolean')
     INSERT INTO [dbo].[NativeDataType]([Type])
     VALUES('System.Int32')
INSERT INTO [dbo].[NativeDataType]([Type])
     VALUES('System.String')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- GlobalSettings --
INSERT INTO [dbo].[GlobalSettings]([Key],[DataType],[Value])
     VALUES ('STRB', 2, '30')
INSERT INTO [dbo].[GlobalSettings]([Key],[DataType],[Value])
     VALUES ('STCF', 2, '10')
INSERT INTO [dbo].[GlobalSettings]([Key],[DataType],[Value])
     VALUES ('STAF', 2, '10')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- Reserved Subdomains --
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('www')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('manage')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('assets')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('files')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('mail')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('docs')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('calendar')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('sites')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('ftp')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('git')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('ssl')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('support')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('status')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('blog')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('api')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('staging')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('lab')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('svn')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('collaborate')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('deliver')
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

INSERT INTO [dbo].[Shape]
           ([IsCustom]
           ,[FXG])
     VALUES
           (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" d:userLabel="Arrow 1" data="M99.1699 49.6357 81.5918 32.0728C81.5918 32.0728 71.7354 41.9272 62.0293 51.6328L62.0293 0.000488281 37.1602 0 37.1602 51.6328 17.5781 32.0605 0 49.6357 49.5825 99.1738 99.1699 49.6357Z">
					<fill>
					  <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M0 0 0 101.302 101.302 101.302 101.302 0 0 0ZM56.9824 75.9771 37.9878 75.9771 56.9824 56.9824 18.9941 56.9824 18.9941 44.3198 56.9824 44.3198 37.9878 25.3252 56.9824 25.3252 82.3081 50.6514 56.9824 75.9771Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M30.1045 96.335 30.1045 30.1045 48.167 30.1045 24.083 0 0 30.1045 18.0625 30.1045 18.0625 96.335 30.1045 96.335Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M89.3652 36.0716 34.7549 36.0663 55.5547 15.2611C58.9102 11.9051 58.7686 6.27475 55.2354 2.74155 51.7012 -0.786285 46.0781 -0.928864 42.7197 2.42758L12.834 32.3148 12.834 32.3148 0 45.1483 9.99805 55.1454 12.834 57.9823 12.834 57.9823 41.9883
 87.1419C45.2969 90.4383 50.875 90.2498 54.4072 86.722 57.9414 83.1893 58.1299 77.6053 54.8223 74.3089L34.7549 54.2298 89.3652 54.2245C93.4551 54.2298 96.8086 50.1414 96.8086 45.1424 96.8086 40.1497 93.4551 36.0663 89.3652 36.0716Z">
        <fill>
          <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M51.8833 0C23.231 0 0 23.231 0 51.8828 0 80.5371 23.231 103.767 51.8833 103.767 80.5371 103.767 103.768 80.5371 103.768 51.8828 103.768 23.231 80.5371 0 51.8833 0L51.8833 0Z">
        <fill>
         <SolidColor color="#000000"/>  
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M51.8828 6.48486C76.9131 6.48486 97.2822 26.8535 97.2822 51.8828 97.2822 76.9126 76.9131 97.2817 51.8828 97.2817 26.8535 97.2817 6.48438 76.9126 6.48438 51.8828 6.48438 26.8535 26.8535 6.48486 51.8828 6.48486M51.8828 0C23.2305 0 0 23.231 0 51.8828
 0 80.5366 23.2305 103.766 51.8828 103.766 80.5371 103.766 103.768 80.5366 103.768 51.8828 103.768 23.231 80.5371 0 51.8828 0L51.8828 0Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M51.8545 0C23.2197 0 0 23.2188 0 51.8535 0 80.4873 23.2197 103.707 51.8545 103.707 80.4883 103.707 103.708 80.4873 103.708 51.8535 103.708 23.2188 80.4883 0 51.8545 0ZM82.0898 68.7568 68.7578 82.0889 51.8545 65.1846 34.9502 82.0889 21.6182 68.7568
 38.5225 51.8535 21.6182 34.9492 34.9502 21.6172 51.8545 38.5215 68.7578 21.6172 82.0898 34.9492 65.1865 51.8535 82.0898 68.7568Z">
        <fill>
             <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029ZM6.78809 98.2412 98.2422 98.2412 98.2422 6.78809 6.78809 6.78809 6.78809 98.2412Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M53.0586 0C23.7588 0 0 23.751 0 53.0498 0 82.3486 23.7588 106.101 53.0586 106.101 82.3486 106.101 106.108 82.3486 106.108 53.0498 106.108 23.751 82.3486 0 53.0586 0ZM50.5566 75.9795 46.79 80.0479 43.1807 75.8428 22.7051 52.0225 29.375 44.8311
 45.9707 57.0498 82.2412 27.2588 89.0059 34.4346 50.5566 75.9795Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
        <Path y="-0.000488281" winding="nonZero" data="M57.2407 12.7202 25.1294 12.7202 25.1294 0.000488281 0.000488281 25.7622 25.1294 50.8804 25.1294 38.1655 57.2407 38.1597 57.2407 12.7202Z">
          <fill>
             <SolidColor color="#000000"/>
          </fill>
        </Path>
        <Path x="44.5195" y="44.519" winding="nonZero" data="M57.2407 25.1167 31.7993 0.000488281 31.7993 12.7202 0.000488281 12.7202 0.000488281 38.1597 31.7993 38.1597 31.7993 50.8794 57.2407 25.1167Z">
          <fill>
            <SolidColor color="#000000"/>
          </fill>
        </Path>
  <Private/>
</Graphic>'), 
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M91.8105 73.4512 64.2646 45.8984 91.8037 18.3594 73.4375 0 45.8975 27.5391 18.3594 0 0 18.3594 27.5381 45.9121 0 73.4512 18.3594 91.8105 45.8975 64.2715 73.4443 91.8105 91.8105 73.4512Z">
        <fill>
             <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M101.585 38.0947 66.3799 38.0947 84.6211 10.5039 68.7354 0 50.7988 27.1328 32.8623 0 16.9639 10.5039 35.2168 38.0947 0 38.0947 0 57.1426 30.9521 57.1426 12.6982 84.7334 28.583 95.2373 50.793 61.6553 73.002 95.2373 88.8867 84.7334 70.6445 57.1426
 101.585 57.1426 101.585 38.0947Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M93.6289 0 36.5947 46.8457 10.5088 27.6289 0 38.9492 32.208 76.4043 37.8809 83.0039 43.7979 76.6104 104.254 11.2812 93.6289 0Z">
        <fill>
         <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero"  data="M72.832 0.000488281 0 0.000488281 0 79.4536 72.832 79.4536 105.938 39.7271 72.832 0.000488281ZM63.8442 56.0396 57.4888 62.4028 41.4082 46.3218 25.3335 62.4028 18.9712 56.0396 35.0454 39.9595 18.9712 23.8853 25.3335 17.522 41.4082 33.603 57.4888
 17.522 63.8442 23.8853 47.77 39.9595 63.8442 56.0396Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'), 
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path data="M108.185 86.9669C105.212 81.6115 62.4178 7.59976 59.7469 3.0773 57.3543 -0.994965 51.6512 -1.05649 49.2567 3.0773 45.6141 9.36636 3.04184 83.0939 0.927582 86.7794 -1.70132 91.3849 1.64145 96.0402 6.15219 96.0402 9.62387 96.0402 97.9647 96.0402
 102.999 96.0402 107.951 96.0402 110.359 90.8976 108.185 86.9669ZM59.162 21.1408 57.7977 67.8019 51.1893 67.8019 49.8289 21.1408 59.162 21.1408ZM54.4969 87.7277 54.3963 87.7277C51.0956 87.7277 48.7586 85.1046 48.7586 81.7052 48.7586 78.2033 51.1893
 75.6759 54.4969 75.6759 57.994 75.6759 60.2303 78.2033 60.2303 81.7052 60.2303 85.1046 57.994 87.7277 54.4969 87.7277Z">
        <fill>
         <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>')
GO

USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Subsidiary Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Subsidiary','Subsidiary', 'local-subsidiary.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'HQ', 'User', NULL, 'noreply-subsidiary@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			10, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-subsidiary.gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-subsidiary@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Subsidiary',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-subsidiary@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 1, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Dealer Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Dealer Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Dealer','Dealer', 'local-dealer.gmgcozone.com', 3, 1, 1, LOWER(NEWID()),
			'Dealer', 'User', NULL, 'noreply-dealer@gmgcozone.com',
			NULL, NULL, NULL, 3, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			12, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-dealer@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Dealer', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-dealer@gmgcozone.com',16)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Dealer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-dealer@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 2, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Client Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Client Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Client','Client', 'local-client.gmgcozone.com', 4, 1, 1, LOWER(NEWID()),
			'Client', 'User', NULL, 'noreply-client@gmgcozone.com',
			NULL, NULL, NULL, 2, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			14, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-client@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Client', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-client@gmgcozone.com',18)
      
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Client',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-client@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 3, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Subscriber Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subscriber Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Subscriber','Subscriber', 'local-subscriber.gmgcozone.com', 5, 1, 1, LOWER(NEWID()),
			'Subscriber', 'User', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 1, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			15, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subscriber', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',20)
     
-- User (Subscriber Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Subscriber',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Administrator','administrator@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 4, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

DECLARE @AccountId int
DECLARE @UserId int

-- GMG coZone HQ Users --
SET @AccountId = 1

INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'HQ','Manager','manager@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 2)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Subsidiary Users --
SET @AccountId = 2
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Manager','manager@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Moderator','moderator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Contributor','contributor@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Viewer','viewer@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Dealer Users --
SET @AccountId = 3
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Manager','manager@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Moderator','moderator@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Contributor','contributor@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Viewer','viewer@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Client Users --
SET @AccountId = 4
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Manager','manager@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Moderator','moderator@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Contributor','contributor@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Viewer','viewer@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Subscriber Users --
SET @AccountId = 5
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Manager','manager@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Moderator','moderator@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Contributor','contributor@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Viewer','viewer@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)               
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Chinese Simplified Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Chinese Simplified Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('ChineseS','ChineseS Subsidiary', 'chineses.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 1, 'AUS Eastern Standard Time', 1, 1,
			0, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Chinese Simplified', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',48)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'ChineseS',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Chinese Simplified','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 6, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Chinese Traditional Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Chinese Traditional Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('ChineseT','ChineseT Subsidiary', 'chineset.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 2, 'AUS Eastern Standard Time', 1, 1,
			2, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Chinese Traditional', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',48)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'ChineseT',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Chinese Traditional','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 7, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- French Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary French Account --

INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('French','French Subsidiary', 'french.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 4, 'AUS Eastern Standard Time', 1, 1,
			4, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary French', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'French',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'French','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 8, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- German Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary German Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('German','German Subsidiary', 'german.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 5, 'AUS Eastern Standard Time', 1, 1,
			6, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary German', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'German',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'German','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 10, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Italian Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary German Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Italian','Italian Subsidiary', 'italian.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 6, 'AUS Eastern Standard Time', 1, 1,
			8, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Italian', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Italian',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Italian','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 11, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Korean Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Korean Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Korean','Korean Subsidiary', 'korean.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 7, 'AUS Eastern Standard Time', 1, 1,
			10, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Korean', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Korean',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Korean','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 12, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Japan Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Japan Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('DEV_Japan','Japan', 'japan.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Takeshi', 'De Silva', '03-9999-9999', 'noreply-japan@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 8, 'AUS Eastern Standard Time', 1, 1,
			12, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'japan-noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Japan (Subsidiary)', 'A9F21', 'Central 4, 1-1-1, Higashi', NULL, 'Nagoya', NULL, NULL, '03-9999-9999', NULL, 'japan-noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Administrator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','admin-user@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '03-9999-9999', NULL, 4, 1)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 13, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Portugal Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Portugal Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Portugal','Portugal Subsidiary', 'portugal.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 9, 'AUS Eastern Standard Time', 1, 1,
			14, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Portugal', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Portugal',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Portugal','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 14, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Spain Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Spain Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Spain','Spain Subsidiary', 'spain.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 10, 'AUS Eastern Standard Time', 1, 1,
			16, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Spain', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Spain',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Spain','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 15, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Approval --
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_01','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_02','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_03','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_04','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_05','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_06','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_07','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_08','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_09','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_10','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_11','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_12','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_13','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_14','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_15','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_16','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_17','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_18','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_19','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_20','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)     
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- UserGroup AND UserGroupUser --
--DECLARE @UserGroup int

--INSERT INTO [dbo].[UserGroup]([Name],[Account],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--	VALUES	('GMG Colloaborate', 2,2, GETDATE(),1,GETDATE())
--SET @UserGroup = SCOPE_IDENTITY()

--INSERT INTO [dbo].[UserGroupUser]([UserGroup],[User])
--	VALUES	(@UserGroup, 2)
--INSERT INTO [dbo].[UserGroupUser]([UserGroup],[User])
--	VALUES	(@UserGroup, 4)
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Folder AND FolderCollaborator --
--DECLARE @Folder int
--DECLARE @SubFolder int

--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('My Approvals', 'My Approvals', NULL, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @Folder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@Folder, 2, CAST(GETDATE() AS datetime2(7)))
     
--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('CoZone', 'CoZone Approvals', @Folder, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @SubFolder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@SubFolder, 2, CAST(GETDATE() AS datetime2(7)))

--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('Draft', 'Draft Approvals', @SubFolder, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @SubFolder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@SubFolder, 2, CAST(GETDATE() AS datetime2(7)))

--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('CoZone Proofs', 'CoZone Proofs', @Folder, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @SubFolder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@SubFolder, 2, CAST(GETDATE() AS datetime2(7)))   
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Currency Rates --
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (1, '1', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (2, '0.626', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (3, '0.774', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (4, '78.566', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (5, '0.9901', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--GO
--/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/    

USE [GMGCoZone]
GO

-- HQ Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local.gmgcozone.com'
 WHERE [ID] = 1
GO

-- Subsidiary Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-subsidiary.gmgcozone.com'
 WHERE [ID] = 2
GO

-- Dealer Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-dealer.gmgcozone.com'
 WHERE [ID] = 3
GO

-- Client Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-client.gmgcozone.com'
 WHERE [ID] = 4
GO

-- Subscriber Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-subscriber.gmgcozone.com'
 WHERE [ID] = 5
GO

-- Chinese Simplified Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-chineses.gmgcozone.com'
 WHERE [ID] = 6
GO

-- Chinese Traditional Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-chineset.gmgcozone.com'
 WHERE [ID] = 7
GO

-- French Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'local-french.gmgcozone.com'
 WHERE [ID] = 8
GO

-- German Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-german.gmgcozone.com'
 WHERE [ID] = 9
GO

-- Italian Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-italian.gmgcozone.com'
 WHERE [ID] = 10
GO

-- Korean Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'local-korean.gmgcozone.com'
 WHERE [ID] = 11
GO

-- Japan Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-japan.gmgcozone.com'
 WHERE [ID] = 12
GO

-- Portugal Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'local-portugal.gmgcozone.com'
 WHERE [ID] = 13
GO

-- Spain Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'local-spain.gmgcozone.com'
 WHERE [ID] = 14
GO

-- Reset Passwords -
UPDATE [dbo].[User]
   SET [Password] = CONVERT(varchar(255), HashBytes('SHA1', 'password'))
GO

USE [GMGCoZone]
GO

--**--**--**--**--**--**--**--**--**--** BillingReportError-10072013  --**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  UserDefinedFunction [dbo].[GetParentAccounts]    Script Date: 07/10/2013 16:01:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetParentAccounts]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetParentAccounts]
GO

/****** Object:  UserDefinedFunction [dbo].[GetParentAccounts]    Script Date: 07/10/2013 15:59:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetParentAccounts]
(	
	-- Add the parameters for the function here
	@AccountId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH parentAccounts AS (
		SELECT a.ID, a.Parent, a.Name, a.GPPDiscount
		FROM	[dbo].[Account] a
		WHERE	a.ID = @AccountId		 
		
		UNION ALL
		
		SELECT pa.ID, pa.Parent, pa.Name, pa.GPPDiscount
		FROM [dbo].[Account] pa
			JOIN parentAccounts pas
		ON pa.ID = pas.Parent		
	)	

	SELECT *
	FROM  parentAccounts	
)

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 07/10/2013 16:20:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBillingCycles]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetBillingCycles]
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 07/10/2013 12:55:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetBillingCycles](
	@AccountID int,
	@LoggedAccountID int,
	@StartDate datetime,
	@EndDate dateTime	
) 
RETURNS nvarchar(MAX) 
AS BEGIN

	DECLARE @cycleCount INT = 0
	DECLARE @AddedProofsCount INT = 0
	DECLARE @totalExceededCount INT = 0	
	DECLARE @cycleStartDate DATETIME = @StartDate
	DECLARE @cycleEndDate DATETIME
	DECLARE @cycleAllowedProofsCount INT
	DECLARE @IsMonthly bit = 1 
	DECLARE @PlanPrice decimal = 0.0
	DECLARE @ParentAccount INT
	DECLARE @SelectedBillingPLan INT
	DECLARE @BillingPlanPrice INT
	
	SET @ParentAccount = (SELECT Parent FROM Account WHERE ID = @AccountID)
	SET @SelectedBillingPLan = (SELECT SelectedBillingPLan FROM Account WHERE ID = @AccountID)
	
	SET @cycleAllowedProofsCount = (SELECT bp.Proofs 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @cycleAllowedProofsCount = (CASE WHEN (@IsMonthly = 1)
						THEN @cycleAllowedProofsCount
						ELSE (@cycleAllowedProofsCount*12)
						END
						) 										
											
	SET @BillingPlanPrice = (SELECT bp.Price 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @IsMonthly = (SELECT IsMonthlyBillingFrequency 
						FROM Account a
						WHERE a.ID = @AccountID
						)
						
	SET @PlanPrice = (CASE WHEN (@LoggedAccountID = 1)
						THEN @BillingPlanPrice
						ELSE (SELECT NewPrice FROM AttachedAccountBillingPlan
								WHERE Account = @LoggedAccountID AND BillingPlan = @SelectedBillingPLan)
						END)
						
	SET @PlanPrice = (CASE WHEN (@IsMonthly = 1)
						THEN @PlanPrice
						ELSE (@PlanPrice*12)
						END
						) 
																					
	SET @cycleStartDate = @StartDate
	
	WHILE (@cycleStartDate < @EndDate)
	BEGIN	
		DECLARE @currentCycleApprovalsCount INT = 0
				
		SET @cycleEndDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END	
	
		SET @currentCycleApprovalsCount = ( SELECT COUNT(ap.ID) FROM Job j 
														INNER JOIN Approval ap
															ON j.ID = ap.Job
														WHERE j.Account = @AccountID
														AND ap.CreatedDate >= @cycleStartDate
														AND ap.CreatedDate <= @cycleEndDate	)
														 
		SET @AddedProofsCount = @AddedProofsCount + @currentCycleApprovalsCount
													
		SET @cycleStartDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END
								
		SET @cycleCount = @cycleCount + 1;	
		SET @totalExceededCount = @totalExceededCount + CASE WHEN (@cycleAllowedProofsCount < @currentCycleApprovalsCount)  
										THEN @currentCycleApprovalsCount - @cycleAllowedProofsCount
										ELSE 0
										END													
	END
		
	RETURN  CONVERT(NVARCHAR, @cycleCount) + '|' +  CONVERT(NVARCHAR, @AddedProofsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice)
	
END

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[ReturnAccountParametersView]    Script Date: 07/15/2013 15:32:33 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnAccountParametersView]'))
DROP VIEW [dbo].[ReturnAccountParametersView]
GO

/****** Object:  View [dbo].[ReturnAccountParametersView]    Script Date: 07/15/2013 15:31:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnAccountParametersView] 
AS 
	SELECT  0 AS AccountID,
			'' AS AccountName,
			0 AS AccountTypeID,
			'' AS AccountTypeName,
			0 AS BillingPlanID,
			'' AS BillingPlanName,
			0 AS AccountStatusID,
			'' AS AccountStatusName,
			0 AS LocationID,
			'' AS LocationName

GO


--**--**--**--**--**--**--**--**--**--** BillingReportChanges-16072013  --**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountParameters]    Script Date: 07/16/2013 17:24:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnAccountParameters]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnAccountParameters]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnAccountParameters]    Script Date: 07/16/2013 17:23:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnAccountParameters
-- Description:  This Sp returns multiple result sets that needed for Account/Billing Report parameters
-- Date Created: Wendsday, 15 July 2013
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnAccountParameters] (		
	@P_LoggedAccount int 
)
AS
BEGIN

SET NOCOUNT ON
			
		SELECT DISTINCT					
				a.ID AS AccountID,
				a.Name AS AccountName,
				at.ID AS AccountTypeID,
				at.Name AS AccountTypeName,
				ISNULL (bp.ID, 0) AS BillingPlanID,
				ISNULL (bp.Name, '') AS BillingPlanName,
				ast.ID AS AccountStatusID,
				ast.Name AS AccountStatusName,
				cty.ID AS LocationID,
				cty.ShortName AS LocationName
		FROM	Account a		
		INNER JOIN AccountType at 
				ON a.AccountType = at.ID
		INNER JOIN AccountStatus ast 
				ON a.[Status] = ast.ID		
		LEFT OUTER JOIN BillingPlan bp 
				ON a.SelectedBillingPlan = bp.ID
		INNER JOIN Company c 
				ON c.Account = a.ID
		INNER JOIN Country cty 
				ON c.Country = cty.ID		
		WHERE a.Parent = @P_LoggedAccount
		AND a.IsSuspendedOrDeletedByParent = 0
		AND a.IsTemporary = 0
		AND (ast.[Key] = 'A')
END

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 07/16/2013 17:02:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnBillingReportInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 07/16/2013 17:01:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnBillingReportInfo
-- Description:  This Sp returns multiple result sets that needed for Billing Report
-- Date Created: Wendsday, 15 May 2012
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnBillingReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountNameList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',
	@P_BillingPlanList nvarchar(MAX)='',
	@P_StartDate datetime ,
	@P_EndDate dateTime,
	@P_IsMonthlyBilling bit = 1,
	@P_LoggedAccount int 
)
AS
BEGIN
	DECLARE @subAccounts TABLE (ID INT)
	INSERT INTO @subAccounts SELECT a.ID FROM [dbo].[Account] a
								INNER JOIN Company c 
									ON c.Account = a.ID
								WHERE Parent = @P_LoggedAccount
								--AND a.NeedSubscriptionPlan = 1
								--AND a.ContractStartDate IS NOT NULL
								AND a.IsTemporary = 0
								--AND (ast.[Key] = 'A')
								--AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
								AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
								AND (@P_AccountNameList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountNameList, ',')))
								AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
								AND (@P_BillingPlanList = '' OR a.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_BillingPlanList, ',')))

	DECLARE @childAccounts TABLE (ID INT)		
	
	DECLARE @subAccount_ID int
	DECLARE Cursor_SubAccounts Cursor
	FOR SELECT ID FROM @subAccounts	
	OPEN Cursor_SubAccounts
	FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
	WHILE @@FETCH_STATUS = 0

	BEGIN	
	INSERT INTO @childAccounts SELECT ID FROM [dbo].[GetChildAccounts](@subAccount_ID)
	FETCH NEXT FROM Cursor_SubAccounts INTO @subAccount_ID
	END

	CLOSE Cursor_SubAccounts
	DEALLOCATE Cursor_SubAccounts
		
	-- Get the accounts	
	SET NOCOUNT ON
			
		SELECT DISTINCT					
				a.ID,
				a.Name AS AccountName,
				at.Name AS AccountTypeName,
				ISNULL(bp.Name,'') AS BillingPlanName,
				ISNULL ((SELECT CASE WHEN a.Parent = 1
							THEN a.GPPDiscount
							ELSE
								(SELECT GPPDiscount FROM [GMGCoZone].[dbo].[GetParentAccounts] (a.ID)
										WHERE Parent = @P_LoggedAccount)
						END), 0.00)		 AS GPPDiscount,
				ISNULL (bp.Proofs, 0) AS Proofs,
				ISNULL((SELECT [dbo].[GetBillingCycles](a.ID,@P_LoggedAccount, @P_StartDate, @P_EndDate )), '') AS BillingCycleDetails,
				a.Parent,
				ISNULL(a.SelectedBillingPlan, 0) AS BillingPlan,
				ISNULL(bp.IsFixed, 0) AS IsFixedPlan,				
				ISNULL((SELECT sa.Name+',' 
					FROM Account sa
					INNER JOIN AccountStatus sast 
						ON sa.[Status] = sast.ID
					WHERE sa.Parent= a.ID
					AND sa.IsTemporary = 0
					AND sa.ContractStartDate IS NOT NULL
					AND (sast.[Key] = 'A')
					GROUP BY sa.Name FOR XML PATH('')),'') AS Subordinates													
		FROM	Account a		
		INNER JOIN AccountType at 
				ON a.AccountType = at.ID
		INNER JOIN AccountStatus ast 
				ON a.[Status] = ast.ID		
		LEFT OUTER JOIN BillingPlan bp 
				ON a.SelectedBillingPlan = bp.ID				
		WHERE
		(a.ID IN (SELECT * FROM @subAccounts) OR a.ID IN (SELECT * FROM @childAccounts))
		--AND a.NeedSubscriptionPlan = 1
		--AND a.ContractStartDate IS NOT NULL
		AND a.IsTemporary = 0
		AND (ast.[Key] = 'A')		
END

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

UPDATE [dbo].[Account]
	SET [IsMonthlyBillingFrequency] = 1
WHERE [Status] = (SELECT ID FROM [dbo].[AccountStatus] WHERE [Key] = 'A')  
	AND [IsMonthlyBillingFrequency] IS NULL
	AND [SelectedBillingPlan] IS NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN X decimal(22, 8) NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN Y decimal(22, 8) NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN Width decimal(22, 8) NOT NULL
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]
	ALTER  COLUMN Height decimal(22, 8) NOT NULL
GO

--**--**--**--**--**--**--**--**--**--** NewApprovalChanges-19072013  --**--**--**--**--**--**--**--**--**--**--**--**--**--**

ALTER TABLE [dbo].[Approval]
	ALTER  COLUMN Size decimal(18, 2) NOT NULL
GO