DECLARE @i INT 
DECLARE @usernameprefix NVARCHAR(100)
DECLARE @username NVARCHAR(100)
DECLARE @indexStr NVARCHAR(100)

SET @i = 0
SET @usernameprefix = 'TestUser_'

WHILE(@i < 100)
BEGIN
	SET @indexStr =  convert(varchar, @i)
	SET @username = @usernameprefix + @indexStr
	
	INSERT INTO [GMGCoZone].[dbo].[User]
			   ([Account]
			   ,[Status]
			   ,[Username]
			   ,[Password]
			   ,[GivenName]
			   ,[FamilyName]
			   ,[EmailAddress]
			   ,[PhotoPath]
			   ,[Guid]
			   ,[MobileTelephoneNumber]
			   ,[HomeTelephoneNumber]
			   ,[OfficeTelephoneNumber]
			   ,[NotificationFrequency]
			   ,[DateLastLogin]
			   ,[Creator]
			   ,[CreatedDate]
			   ,[Modifier]
			   ,[ModifiedDate]
			   ,[Preset]
			   ,[IncludeMyOwnActivity])
		 VALUES
			   (4
			   ,1
			   ,@username
			   ,'[�a�ɹ??�%l�3~��'
			   ,'TestUser'
			   ,'TestUser'
			   ,'administrator.user-client@gmgcozone.com'
			   ,NULL
			   ,NEWID()
			   ,NULL
			   ,NULL
			   ,'1300 000 000'
			   ,1
			   ,NULL
			   ,1
			   ,GETDATE()
			   ,1
			   ,GETDATE()
			   ,3
			   ,0)
			   
	DECLARE @id INT
	SET @id = SCOPE_IDENTITY();
	
	INSERT INTO [GMGCoZone].[dbo].[UserRole]
	        ( [User], [Role] )
	VALUES  ( @id, 3 ),
			( @id, 9 ),
			( @id, 14 ),
			( @id, 19 )
	
	SET @i = @i + 1;
END

