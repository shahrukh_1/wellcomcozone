DELETE dbo.DeliverJobPageVerificationResult
DELETE dbo.DeliverJobPage

DECLARE @deliverJobId AS INT

DECLARE DeliverJobs CURSOR LOCAL FOR SELECT ID FROM dbo.DeliverJob
OPEN DeliverJobs
FETCH NEXT FROM DeliverJobs INTO @deliverJobId
WHILE @@FETCH_STATUS = 0 
    BEGIN
        DECLARE @page1 AS INT
        DECLARE @page2 AS INT
        DECLARE @page3 AS INT
        DECLARE @page4 AS INT

        INSERT  [dbo].[DeliverJobPage]
                ( [UserFriendlyName] ,
                  [Status] ,
                  [ErrorMessage] ,
                  [DeliverJob]
                )
        VALUES  ( N'Page 1' ,
                  6 ,
                  N'Printer not calibrated' ,
                  @deliverJobId
                )
        SET @page1 = SCOPE_IDENTITY()
        INSERT  [dbo].[DeliverJobPage]
                ( [UserFriendlyName] ,
                  [Status] ,
                  [ErrorMessage] ,
                  [DeliverJob]
                )
        VALUES  (  N'Page 2' ,
                  2 ,
                  N'Printer not calibrated' ,
                  @deliverJobId
                )
        SET @page2 = SCOPE_IDENTITY()
		INSERT  [dbo].[DeliverJobPage]
                ( [UserFriendlyName] ,
                  [Status] ,
                  [ErrorMessage] ,
                  [DeliverJob]
                )
        VALUES  ( N'Page 3' ,
                  6 ,
                  N'Printer not calibrated' ,
                  @deliverJobId
                )
        SET @page3 = SCOPE_IDENTITY()
        INSERT  [dbo].[DeliverJobPage]
                ( [UserFriendlyName] ,
                  [Status] ,
                  [ErrorMessage] ,
                  [DeliverJob]
                )
        VALUES  (  N'Page 4' ,
                  2 ,
                  N'Printer not calibrated' ,
                  @deliverJobId
                )
        SET @page4 = SCOPE_IDENTITY()        






        INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'Ugra/Fogra Media Wedge CMYK V3.0' ,
                  2.00 ,
                  0.75 ,
                  1 ,
                  6.00 ,
                  3.00 ,
                  0 ,
                  @page1
                )
        INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'IDEAlliance Digital Control Strip 2008 3x18' ,
                  8.75 ,
                  0.93 ,
                  0 ,
                  6.50 ,
                  3.75 ,
                  0 ,
                  @page1
                )
                
 INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'IDEAlliance Digital Control Strip 2008 3x18' ,
                  5.45 ,
                  2.93 ,
                  1 ,
                  5.50 ,
                  8.75 ,
                  0 ,
                  @page1
                )                

		INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'Ugra/Fogra Media Wedge CMYK V3.0' ,
                  2.00 ,
                  0.75 ,
                  1 ,
                  6.00 ,
                  3.00 ,
                  0 ,
                  @page3
                )
        INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'IDEAlliance Digital Control Strip 2008 3x18' ,
                  8.75 ,
                  0.93 ,
                  0 ,
                  6.50 ,
                  3.75 ,
                  0 ,
                  @page3
                )


		INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'Ugra/Fogra Media Wedge CMYK V3.0!' ,
                  3.00 ,
                  0.75 ,
                  1 ,
                  6.00 ,
                  3.00 ,
                  0 ,
                  @page2
                )
        INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'GMG Spot Color Control Strip – Full Tone v1.0!' ,
                  5.75 ,
                  0.93 ,
                  1 ,
                  NULL ,
                  NULL ,
                  1 ,
                  @page2
                )

		INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'Ugra/Fogra Media Wedge CMYK V3.0!' ,
                  3.00 ,
                  0.75 ,
                  1 ,
                  6.00 ,
                  3.00 ,
                  0 ,
                  @page4
                )
        INSERT  [dbo].[DeliverJobPageVerificationResult]
                ( [StripName] ,
                  [MaxDelta] ,
                  [AvgDelta] ,
                  [ProofResult] ,
                  [AllowedTolleranceMax] ,
                  [AllowedTolleranceAvg] ,
                  [IsSpotColorVerification] ,
                  [DeliverJobPage]
                )
        VALUES  ( N'GMG Spot Color Control Strip – Full Tone v1.0!' ,
                  5.75 ,
                  0.93 ,
                  1 ,
                  NULL ,
                  NULL ,
                  1 ,
                  @page4
                )                

        FETCH NEXT FROM DeliverJobs INTO @deliverJobId
    END
CLOSE DeliverJobs
DEALLOCATE DeliverJobs