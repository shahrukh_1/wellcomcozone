DECLARE @nrOfAccounts INT = 200
DECLARE @jobCounts INT = 80

DECLARE @currentAccount INT = 0
WHILE ( @currentAccount < @nrOfAccounts ) 
    BEGIN
        DECLARE @userId INT
        DECLARE @accountId INT

        INSERT  INTO dbo.[User]
                ( Account ,
                  Status ,
                  Username ,
                  Password ,
                  GivenName ,
                  FamilyName ,
                  EmailAddress ,
                  PhotoPath ,
                  Guid ,
                  MobileTelephoneNumber ,
                  HomeTelephoneNumber ,
                  OfficeTelephoneNumber ,
                  NotificationFrequency ,
                  DateLastLogin ,
                  Creator ,
                  CreatedDate ,
                  Modifier ,
                  ModifiedDate ,
                  Preset ,
                  IncludeMyOwnActivity
                )
        VALUES  ( 1 , -- Account - int
                  1 , -- Status - int
                  'test_user_' + CONVERT(VARCHAR(10), @currentAccount) , -- Username - nvarchar(128)
                  N'[�a�ɹ??�%l�3~��' , -- Password - varchar(64)
                  'Administrator ' + CONVERT(VARCHAR(10), @currentAccount) , -- GivenName - nvarchar(64)
                  'Test' , -- FamilyName - nvarchar(64)
                  'user@netrom.ro' , -- EmailAddress - nvarchar(128)
                  NULL , -- PhotoPath - nvarchar(256)
                  NEWID() , -- Guid - nvarchar(36)
                  000 , -- MobileTelephoneNumber - nvarchar(20)
                  111 , -- HomeTelephoneNumber - nvarchar(20)
                  222 , -- OfficeTelephoneNumber - nvarchar(20)
                  1 , -- NotificationFrequency - int
                  GETDATE() , -- DateLastLogin - datetime2
                  1 , -- Creator - int
                  GETDATE() , -- CreatedDate - datetime2
                  1 , -- Modifier - int
                  GETDATE() , -- ModifiedDate - datetime2
                  3 , -- Preset - int
                  0  -- IncludeMyOwnActivity - bit
                )
        SET @userId = SCOPE_IDENTITY()
        

        INSERT  INTO dbo.UserRole
                ( [User], Role )
        VALUES  ( @userId, -- User - int
                  3  -- Role - int
                  )
        INSERT  INTO dbo.UserRole
                ( [User], Role )
        VALUES  ( @userId, -- User - int
                  9  -- Role - int
                  )
        INSERT  INTO dbo.UserRole
                ( [User], Role )
        VALUES  ( @userId, -- User - int
                  14  -- Role - int
                  )          
          
                    
        INSERT  INTO dbo.Account
                ( Name ,
                  SiteName ,
                  Domain ,
                  AccountType ,
                  Parent ,
                  Owner ,
                  Guid ,
                  ContactFirstName ,
                  ContactLastName ,
                  ContactPhone ,
                  ContactEmailAddress ,
                  HeaderLogoPath ,
                  LoginLogoPath ,
                  EmailLogoPath ,
                  Theme ,
                  CustomColor ,
                  CurrencyFormat ,
                  Locale ,
                  TimeZone ,
                  DateFormat ,
                  TimeFormat ,
                  GPPDiscount ,
                  NeedSubscriptionPlan ,
                  ContractPeriod ,
                  ChildAccountsVolume ,
                  CustomFromAddress ,
                  CustomFromName ,
                  SuspendReason ,
                  IsSuspendedOrDeletedByParent ,
                  CustomDomain ,
                  IsCustomDomainActive ,
                  IsHideLogoInFooter ,
                  IsHideLogoInLoginFooter ,
                  IsNeedProfileManagement ,
                  IsNeedPDFsToBeColorManaged ,
                  IsRemoveAllGMGCollaborateBranding ,
                  Status ,
                  IsEnabledLog ,
                  Creator ,
                  CreatedDate ,
                  Modifier ,
                  ModifiedDate ,
                  IsHelpCentreActive ,
                  DealerNumber ,
                  CustomerNumber ,
                  VATNumber ,
                  IsAgreedToTermsAndConditions ,
                  Region ,
                  IsTemporary
                )
        VALUES  ( 'test_' + CONVERT(VARCHAR(10), @currentAccount) , -- Name - nvarchar(128)
                  'test_' + CONVERT(VARCHAR(10), @currentAccount) , -- SiteName - nvarchar(128)
                  'local-test' + CONVERT(VARCHAR(10), @currentAccount) + '.gmgcozone.com' , -- Domain - nvarchar(255)
                  4 , -- AccountType - int
                  1 , -- Parent - int
                  4 , -- Owner - int
                  NEWID() , -- Guid - nvarchar(36)
                  'test first name' , -- ContactFirstName - nvarchar(128)
                  'test last name' , -- ContactLastName - nvarchar(128)
                  NULL , -- ContactPhone - nvarchar(20)
                  'cozone@netrom.ro' , -- ContactEmailAddress - nvarchar(64)
                  NULL , -- HeaderLogoPath - nvarchar(256)
                  NULL , -- LoginLogoPath - nvarchar(256)
                  NULL , -- EmailLogoPath - nvarchar(256)
                  1 , -- Theme - int
                  '' , -- CustomColor - nvarchar(8)
                  3 , -- CurrencyFormat - int
                  3 , -- Locale - int
                  'AUS Eastern Standard Time' , -- TimeZone - nvarchar(128)
                  1 , -- DateFormat - int
                  1 , -- TimeFormat - int
                  12.00 , -- GPPDiscount - decimal
                  1 , -- NeedSubscriptionPlan - bit
                  1 , -- ContractPeriod - int
                  NULL , -- ChildAccountsVolume - int
                  NULL , -- CustomFromAddress - nvarchar(256)
                  NULL , -- CustomFromName - nvarchar(256)
                  NULL , -- SuspendReason - nvarchar(256)
                  0 , -- IsSuspendedOrDeletedByParent - bit
                  NULL , -- CustomDomain - nvarchar(255)
                  0 , -- IsCustomDomainActive - bit
                  0 , -- IsHideLogoInFooter - bit
                  0 , -- IsHideLogoInLoginFooter - bit
                  1 , -- IsNeedProfileManagement - bit
                  1 , -- IsNeedPDFsToBeColorManaged - bit
                  1 , -- IsRemoveAllGMGCollaborateBranding - bit
                  1 , -- Status - int
                  1 , -- IsEnabledLog - bit
                  1 , -- Creator - int
                  GETDATE() , -- CreatedDate - datetime2
                  1 , -- Modifier - int
                  GETDATE() , -- ModifiedDate - datetime2
                  0 , -- IsHelpCentreActive - bit
                  NULL , -- DealerNumber - nvarchar(32)
                  NULL , -- CustomerNumber - nvarchar(32)
                  NULL , -- VATNumber - nvarchar(32)
                  0 , -- IsAgreedToTermsAndConditions - bit
                  'local' , -- Region - nvarchar(128)
                  0  -- IsTemporary - bit
                )
        SET @accountId = SCOPE_IDENTITY()

        DECLARE @i INT = 0
        WHILE ( @i < 50 ) 
            BEGIN
                INSERT  INTO dbo.[User]
                        ( Account ,
                          Status ,
                          Username ,
                          Password ,
                          GivenName ,
                          FamilyName ,
                          EmailAddress ,
                          PhotoPath ,
                          Guid ,
                          MobileTelephoneNumber ,
                          HomeTelephoneNumber ,
                          OfficeTelephoneNumber ,
                          NotificationFrequency ,
                          DateLastLogin ,
                          Creator ,
                          CreatedDate ,
                          Modifier ,
                          ModifiedDate ,
                          Preset ,
                          IncludeMyOwnActivity
					
                        )
                VALUES  ( @accountId , -- Account - int
                          1 , -- Status - int
                          'user_' + CONVERT(VARCHAR(10), @currentAccount)
                          + '_' + CONVERT(VARCHAR(10), @i) , -- Username - nvarchar(128)
                          N'[�a�ɹ??�%l�3~��' , -- Password - varchar(64)
                          'User ' + CONVERT(VARCHAR(10), @currentAccount)
                          + '-' + CONVERT(VARCHAR(10), @i) , -- GivenName - nvarchar(64)
                          'Test' , -- FamilyName - nvarchar(64)
                          'user@netrom.ro' , -- EmailAddress - nvarchar(128)
                          NULL , -- PhotoPath - nvarchar(256)
                          NEWID() , -- Guid - nvarchar(36)
                          000 , -- MobileTelephoneNumber - nvarchar(20)
                          111 , -- HomeTelephoneNumber - nvarchar(20)
                          222 , -- OfficeTelephoneNumber - nvarchar(20)
                          1 , -- NotificationFrequency - int
                          GETDATE() , -- DateLastLogin - datetime2
                          1 , -- Creator - int
                          GETDATE() , -- CreatedDate - datetime2
                          1 , -- Modifier - int
                          GETDATE() , -- ModifiedDate - datetime2
                          3 , -- Preset - int
                          0  -- IncludeMyOwnActivity - bit
					
                        )
                SET @i = @i + 1
            END
				
        UPDATE  dbo.[User]
        SET     Account = @accountId
        WHERE   ID = @userId

        UPDATE  dbo.Account
        SET     Owner = @userId
        WHERE   id = @accountId

        INSERT  INTO dbo.Company
                ( Account ,
                  Name ,
                  Number ,
                  Address1 ,
                  Address2 ,
                  City ,
                  Postcode ,
                  State ,
                  Phone ,
                  Mobile ,
                  Email ,
                  Country
                )
        VALUES  ( @accountId , -- Account - int
                  'TestCompany_' + CONVERT(VARCHAR(10), @currentAccount) , -- Name - nvarchar(128)
                  '001122' , -- Number - nvarchar(32)
                  '' , -- Address1 - nvarchar(128)
                  '' , -- Address2 - nvarchar(128)
                  'Craiova' , -- City - nvarchar(64)
                  '1100' , -- Postcode - nvarchar(20)
                  'Dolj' , -- State - nvarchar(20)
                  '1122' , -- Phone - nvarchar(20)
                  '3344' , -- Mobile - nvarchar(20)
                  'cozone@netrom.ro' , -- Email - nvarchar(64)
                  182  -- Country - int
                )
        INSERT  INTO dbo.AccountSubscriptionPlan
                ( SelectedBillingPlan ,
                  IsMonthlyBillingFrequency ,
                  ChargeCostPerProofIfExceeded ,
                  ContractStartDate ,
                  Account
                )
        VALUES  ( 8 , -- SelectedBillingPlan - int
                  1 , -- IsMonthlyBillingFrequency - bit
                  0 , -- ChargeCostPerProofIfExceeded - bit
                  GETDATE() , -- ContractStartDate - datetime2
                  @accountId  -- Account - int
                )
        INSERT  INTO dbo.AccountSubscriptionPlan
                ( SelectedBillingPlan ,
                  IsMonthlyBillingFrequency ,
                  ChargeCostPerProofIfExceeded ,
                  ContractStartDate ,
                  Account
                )
        VALUES  ( 17 , -- SelectedBillingPlan - int
                  1 , -- IsMonthlyBillingFrequency - bit
                  0 , -- ChargeCostPerProofIfExceeded - bit
                  GETDATE() , -- ContractStartDate - datetime2
                  @accountId  -- Account - int
                )
        INSERT  INTO dbo.AccountSubscriptionPlan
                ( SelectedBillingPlan ,
                  IsMonthlyBillingFrequency ,
                  ChargeCostPerProofIfExceeded ,
                  ContractStartDate ,
                  Account
                )
        VALUES  ( 20 , -- SelectedBillingPlan - int
                  1 , -- IsMonthlyBillingFrequency - bit
                  0 , -- ChargeCostPerProofIfExceeded - bit
                  GETDATE() , -- ContractStartDate - datetime2
                  @accountId  -- Account - int
                )                                
                
        DECLARE @jobNr AS INT = 0
        WHILE ( @jobNr < @jobCounts ) 
            BEGIN
                DECLARE @jobId AS INT
                INSERT  INTO dbo.Job
                        ( Title ,
                          Status ,
                          Account ,
                          ModifiedDate
                        )
                VALUES  ( 'filename test ' + CONVERT(VARCHAR(10), @jobNr) , -- Title - nvarchar(128)
                          1 , -- Status - int
                          @accountId , -- Account - int
                          GETDATE()  -- ModifiedDate - datetime2
                        )        
                SET @jobId = SCOPE_IDENTITY()
                DECLARE @approvalId AS INT
                INSERT  INTO dbo.Approval
                        ( ErrorMessage ,
                          FileName ,
                          FolderPath ,
                          Guid ,
                          Job ,
                          Version ,
                          CreatedDate ,
                          Creator ,
                          Deadline ,
                          Modifier ,
                          ModifiedDate ,
                          IsPageSpreads ,
                          IsDeleted ,
                          LockProofWhenAllDecisionsMade ,
                          IsLocked ,
                          OnlyOneDecisionRequired ,
                          PrimaryDecisionMaker ,
                          AllowDownloadOriginal ,
                          FileType ,
                          Size ,
                          Owner ,
                          IsError ,
                          Type
                        )
                VALUES  ( NULL , -- ErrorMessage - nvarchar(512)
                          'filename' + CONVERT(VARCHAR(10), @jobNr) , -- FileName - nvarchar(128)
                          'c:\projects\' , -- FolderPath - nvarchar(512)
                          NEWID() , -- Guid - nvarchar(36)
                          @jobId , -- Job - int
                          1 , -- Version - int
                          GETDATE() , -- CreatedDate - datetime2
                          @userId , -- Creator - int
                          GETDATE() , -- Deadline - datetime2
                          @userId , -- Modifier - int
                          GETDATE() , -- ModifiedDate - datetime2
                          0 , -- IsPageSpreads - bit
                          0 , -- IsDeleted - bit
                          1 , -- LockProofWhenAllDecisionsMade - bit
                          0 , -- IsLocked - bit
                          0 , -- OnlyOneDecisionRequired - bit
                          NULL , -- PrimaryDecisionMaker - int
                          1 , -- AllowDownloadOriginal - bit
                          2 , -- FileType - int
                          5 * @jobId , -- Size - decimal
                          @userId , -- Owner - int
                          0 , -- IsError - bit
                          1  -- Type - int
                        )
                SET @approvalId = SCOPE_IDENTITY()
                INSERT  INTO dbo.ApprovalPage
                        ( Number ,
                          Approval ,
                          PageSmallThumbnailHeight ,
                          PageSmallThumbnailWidth ,
                          PageLargeThumbnailHeight ,
                          PageLargeThumbnailWidth ,
                          DPI ,
                          Progress
                        )
                VALUES  ( 1 , -- Number - int
                          @approvalId , -- Approval - int
                          100 , -- PageSmallThumbnailHeight - int
                          100 , -- PageSmallThumbnailWidth - int
                          500 , -- PageLargeThumbnailHeight - int
                          500 , -- PageLargeThumbnailWidth - int
                          300 , -- DPI - int
                          100  -- Progress - int
                        )
                SET @jobNr = @jobNr + 1
            END                
        SET @currentAccount = @currentAccount + 1
    END
