DECLARE @userGroupId INT
DECLARE @accountId INT = 4
DECLARE @userId INT = 4
DECLARE @colorProofInstanceId INT
DECLARE @colorProofWorkflowId INT
DECLARE @colorProofCoZoneWorkflowId INT
DECLARE @nrOfJobsPerModule INT = 30000

INSERT  [dbo].[UserGroup]
        ( [Name] ,
          [Account] ,
          [Creator] ,
          [CreatedDate] ,
          [Modifier] ,
          [ModifiedDate]
        )
VALUES  ( N'group' ,
          @accountId ,
          1 ,
          CAST(0x0700BAB1077D8A370B AS DATETIME2) ,
          1 ,
          CAST(0x0700BAB1077D8A370B AS DATETIME2)
        )
SET @userGroupId = SCOPE_IDENTITY()

INSERT  [dbo].[UserGroupUser]
        SELECT  @userGroupId AS [UserGroup] ,
                ID AS [User]
        FROM    [User]
        WHERE   Account = @accountId

INSERT  [dbo].[ColorProofInstance]
        ( [Guid] ,
          [Owner] ,
          [Version] ,
          [UserName] ,
          [Password] ,
          [ComputerName] ,
          [SystemName] ,
          [SerialNumber] ,
          [IsOnline] ,
          [LastSeen] ,
          [State] ,
          [AdminName] ,
          [Company] ,
          [Address] ,
          [Email] ,
          [Phone] ,
          [Website] ,
          [WorkflowsLastModifiedTimestamp] ,
          [IsDeleted]
        )
VALUES  ( N'b340e87c-b0b6-47db-ad39-b1aeeec1f3aa' ,
          4 ,
          N'5.5.1.220' ,
          N'geo' ,
          N'geo' ,
          N'lala' ,
          N'unknown machine' ,
          N'0.027.787.715' ,
          0 ,
          1378307304 ,
          4 ,
          NULL ,
          NULL ,
          NULL ,
          NULL ,
          NULL ,
          NULL ,
          1378307309 ,
          0
        )
SET @colorProofInstanceId = SCOPE_IDENTITY()

INSERT  [dbo].[ColorProofWorkflow]
        ( [Guid] ,
          [Name] ,
          [OldName] ,
          [ProofStandard] ,
          [MaximumUsablePaperWidth] ,
          [IsActivated] ,
          [IsInlineProofVerificationSupported] ,
          [SupportedSpotColors] ,
          [ColorProofInstance]
        )
VALUES  ( N'089bef8d-0380-4667-85f0-de7952133b5c' ,
          N'wf unknown 01' ,
          N'wf unknown 01' ,
          N'ISO Coated (27L)' ,
          44 ,
          1 ,
          0 ,
          N'HKS_N
HKS_K
PANTONE® Pastels & Neons uncoated
PANTONE® PLUS coated
PANTONE® PLUS uncoated 336 New
DIC Color Guide 19th edition®
PANTONE® GoeBridge coated
PANTONE® PREMIUM METALLICS coated
PANTONE® PLUS coated 336 New
PANTONE® Pastel uncoated
PANTONE® Metallic coated
PANTONE® Pastels & Neons coated
PANTONE® GOE coated
PANTONE® solid uncoated
PANTONE® GOE uncoated
DIC Color Guide®
PANTONE® Pastel coated
PANTONE® PLUS uncoated
PANTONE® solid coated
PANTONE® solid Matte
' ,
          @colorProofInstanceId
        )

SET @colorProofWorkflowId = SCOPE_IDENTITY()

INSERT  [dbo].[ColorProofCoZoneWorkflow]
        ( [ColorProofWorkflow] ,
          [Name] ,
          [IsAvailable] ,
          [TransmissionTimeout] ,
          [IsDeleted]
        )
VALUES  ( @colorProofWorkflowId ,
          N'wf01 unknown' ,
          1 ,
          100 ,
          0
        )
SET @colorProofCoZoneWorkflowId = SCOPE_IDENTITY()

INSERT  [dbo].[ColorProofCoZoneWorkflowUserGroup]
        ( [ColorProofCoZoneWorkflow] ,
          [UserGroup]
        )
VALUES  ( @colorProofWorkflowId ,
          @userGroupId
        )


DECLARE @i AS INT = 0
WHILE ( @i < @nrOfJobsPerModule ) 
    BEGIN
        DECLARE @jobId AS INT
        INSERT  INTO dbo.Job
                ( Title ,
                  Status ,
                  Account ,
                  ModifiedDate
	          )
        VALUES  ( 'job test ' + CONVERT(VARCHAR(10), @i) , -- Title - nvarchar(128)
                  1 , -- Status - int
                  @accountId , -- Account - int
                  GETDATE()  -- ModifiedDate - datetime2
	          )
        SET @jobId = SCOPE_IDENTITY()
	    
        INSERT  INTO dbo.DeliverJob
                ( Job ,
                  Guid ,
                  Owner ,
                  FileName ,
                  CPWorkflow ,
                  Pages ,
                  CreatedDate ,
                  IncludeProofMetaInformationOnLabel ,
                  LogProofControlResults ,
                  Size ,
                  Status ,
                  IsAlertSent ,
                  IsCancelRequested ,
                  StatusInColorProof
	            
                )
        VALUES  ( @jobId , -- Job - int
                  NEWID() , -- Guid - nvarchar(36)
                  @userId , -- Owner - int
                  'filename' + CONVERT(VARCHAR(10), @i) , -- FileName - nvarchar(128)
                  @colorProofWorkflowId , -- CPWorkflow - int
                  'All' , -- Pages - nvarchar(50)
                  GETDATE() , -- CreatedDate - datetime
                  1 , -- IncludeProofMetaInformationOnLabel - bit
                  1 , -- LogProofControlResults - bit
                  @i * 256 , -- Size - decimal
                  1 , -- Status - int
                  0 , -- IsAlertSent - bit
                  0 , -- IsCancelRequested - bit
                  12  -- StatusInColorProof - int
                )
                
        INSERT  INTO dbo.Approval
                ( ErrorMessage ,
                  FileName ,
                  FolderPath ,
                  Guid ,
                  Job ,
                  Version ,
                  CreatedDate ,
                  Creator ,
                  Deadline ,
                  Modifier ,
                  ModifiedDate ,
                  IsPageSpreads ,
                  IsDeleted ,
                  LockProofWhenAllDecisionsMade ,
                  IsLocked ,
                  OnlyOneDecisionRequired ,
                  PrimaryDecisionMaker ,
                  AllowDownloadOriginal ,
                  FileType ,
                  Size ,
                  Owner ,
                  IsError ,
                  Type
                        
                )
        VALUES  ( 'error' , -- ErrorMessage - nvarchar(512)
                  'filename' + CONVERT(VARCHAR(10), @i) , -- FileName - nvarchar(128)
                  '' , -- FolderPath - nvarchar(512)
                  NEWID() , -- Guid - nvarchar(36)
                  @jobId , -- Job - int
                  1 , -- Version - int
                  GETDATE() , -- CreatedDate - datetime2
                  @userId , -- Creator - int
                  GETDATE() , -- Deadline - datetime2
                  @userId , -- Modifier - int
                  GETDATE() , -- ModifiedDate - datetime2
                  0 , -- IsPageSpreads - bit
                  0 , -- IsDeleted - bit
                  0 , -- LockProofWhenAllDecisionsMade - bit
                  0 , -- IsLocked - bit
                  0 , -- OnlyOneDecisionRequired - bit
                  NULL , -- PrimaryDecisionMaker - int
                  1 , -- AllowDownloadOriginal - bit
                  2 , -- FileType - int
                  256 * @i , -- Size - decimal
                  @userId , -- Owner - int
                  1 , -- IsError - bit
                  1  -- Type - int
                        
                )
        SET @i = @i + 1    
    END
