USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

DECLARE @i INT 
DECLARE @accountnameprefix NVARCHAR(100)
DECLARE @accountname NVARCHAR(100)
DECLARE @indexStr NVARCHAR(100)

SET @i = 0
SET @accountnameprefix = 'TestAccount_'

WHILE(@i < 500000)
BEGIN

	SET @indexStr =  convert(varchar, @i)
	SET @accountname = @accountnameprefix + @indexStr

-- Subsidiary Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [Region], [IsTemporary])
    VALUES (@accountname,'Subsidiary', 'local-subsidiary.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'HQ', 'User', NULL, 'noreply-subsidiary@gmgcozone.com',
			NULL, NULL, 4, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			10, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-subsidiary.gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-subsidiary@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset], [Locale])
     VALUES (@AccountId, 1, 'Subsidiary',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-subsidiary@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 1, 0, 1, 0, 0)

	SET @i = @i + 1;
END

GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
