USE ELMAH

CREATE TABLE [dbo].[ColorProofATPConnectorLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CoZoneManageLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[FTPIISCustomProviderLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,    
    [Level] [varchar] (50) NOT NULL,    
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[FTPServiceLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CoZoneEmailServiceLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CoZoneMediaDefineLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CoZoneMaintenanceLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CollaborateAPILog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max)  NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[SoftProofingDataLog] (
    [Id] [int] IDENTITY (1, 1) NOT NULL,
    [Date] [datetime] NOT NULL,
    [Thread] [varchar] (255) NOT NULL,
    [Level] [varchar] (50) NOT NULL,
    [Logger] [varchar] (255) NOT NULL,
    [Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CoZoneBouncedEmailTrackerServiceLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)

CREATE TABLE [dbo].[CoZoneEmailSenderServiceLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Exception] [nvarchar](max) NOT NULL
)