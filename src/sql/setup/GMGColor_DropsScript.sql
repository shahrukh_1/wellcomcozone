USE [GMGCoZone]
DECLARE @sql VARCHAR(MAX) = ''

SELECT  @sql += N'
ALTER TABLE ' + QUOTENAME(OBJECT_SCHEMA_NAME(parent_object_id)) + '.'
        + QUOTENAME(OBJECT_NAME(parent_object_id)) + ' DROP CONSTRAINT '
        + QUOTENAME(name) + ';'
FROM    sys.foreign_keys ;
EXEC (@sql) ;

DECLARE @table VARCHAR(500) = ''
DECLARE DropTablesCursor CURSOR LOCAL FOR  
SELECT sobjects.name
FROM sysobjects sobjects
WHERE sobjects.xtype = 'U'
OPEN DropTablesCursor
FETCH NEXT FROM DropTablesCursor INTO @table
WHILE @@FETCH_STATUS = 0 
    BEGIN
        SET @sql = 'IF (EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND TABLE_NAME = '''
            + @table + ''')) DROP TABLE dbo.[' + @table + '];' + CHAR(13) ;
        EXEC (@sql)
        FETCH NEXT FROM DropTablesCursor INTO @table
    END
CLOSE DropTablesCursor
DEALLOCATE DropTablesCursor

DECLARE @view VARCHAR(100)
DECLARE DropViewsCursor CURSOR LOCAL FOR SELECT  '[' + SCHEMA_NAME(schema_id) + '].[' + name + ']'
FROM    sys.views
OPEN DropViewsCursor
FETCH NEXT FROM DropViewsCursor INTO @view
WHILE @@FETCH_STATUS = 0 
    BEGIN
        SET @sql = 'DROP VIEW ' + @view + ';' + CHAR(13) ;
        EXEC (@sql)
        FETCH NEXT FROM DropViewsCursor INTO @view
    END
CLOSE DropViewsCursor
DEALLOCATE DropViewsCursor

DECLARE @procedure VARCHAR(100)
DECLARE DropProceduresCursor CURSOR LOCAL FOR SELECT  '[' + b.name + '].[' + a.Name + ']'
FROM    sys.objects a
INNER JOIN sys.schemas b ON a.schema_id = b.schema_id
WHERE   TYPE = 'P'
AND LEFT(a.name, 3) NOT IN ( 'sp_', 'xp_', 'ms_' )
OPEN DropProceduresCursor
FETCH NEXT FROM DropProceduresCursor INTO @procedure
WHILE @@FETCH_STATUS = 0 
    BEGIN
        SET @sql = ' DROP PROCEDURE ' + @procedure + ';' + CHAR(13) ;
        EXEC (@sql)
        FETCH NEXT FROM DropProceduresCursor INTO @procedure
    END
CLOSE DropProceduresCursor
DEALLOCATE DropProceduresCursor


DECLARE @function VARCHAR(100)
DECLARE DropFunctionsCursor CURSOR LOCAL FOR SELECT '[' + b.name + '].[' + a.Name + ']'
FROM sys.objects a
INNER JOIN sys.schemas b
ON a.schema_id = b.schema_id
WHERE TYPE IN ('FN', 'IF', 'TF')
OPEN DropFunctionsCursor
FETCH NEXT FROM DropFunctionsCursor INTO @function
WHILE @@FETCH_STATUS = 0 
    BEGIN
        SET @sql = ' DROP FUNCTION ' + @function + ';' + CHAR(13) ;
        EXEC (@sql)
        FETCH NEXT FROM DropFunctionsCursor INTO @function
    END
CLOSE DropFunctionsCursor
DEALLOCATE DropFunctionsCursor