USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--
-- TABLES --
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Account]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Name] [nvarchar](128) NOT NULL,
	[SiteName] [nvarchar](128) NOT NULL,
	[Domain] [nvarchar](255) NOT NULL,	
	[AccountType] [int] NOT NULL,
	[Parent] [int] NULL,
	[Owner] [int] NULL,	
	[Guid] [nvarchar](36) NOT NULL,
	[ContactFirstName] [nvarchar](128) NOT NULL,
	[ContactLastName] [nvarchar](128) NOT NULL,
	[ContactPhone] [nvarchar](20) NULL,
	[ContactEmailAddress] [nvarchar](64) NOT NULL,
	[HeaderLogoPath] [nvarchar](256) NULL,
	[LoginLogoPath] [nvarchar](256) NULL,
	[EmailLogoPath] [nvarchar](256) NULL,
	[Theme] [int] NOT NULL,
	[CustomColor] [nvarchar](8) NULL,
	[CurrencyFormat] [int] NOT NULL,
	[Locale] [int] NOT NULL,
	[TimeZone] [nvarchar] (128) NOT NULL,
	[DateFormat] [int] NOT NULL,
	[TimeFormat] [int] NOT NULL,
	[GPPDiscount] [decimal] (4,2) NULL,
	[NeedSubscriptionPlan] [bit] NOT NULL,
	[ChargeCostPerProofIfExceeded] [bit] NOT NULL,
	[SelectedBillingPlan] [int] NULL,
	[IsMonthlyBillingFrequency] [bit] NULL,
	--[BillingAnniversaryDate] [nvarchar](32) NULL,
	--[BillingAnniversaryMonth] [int] NULL,
	[ContractPeriod] [int] NOT NULL,
	[ChildAccountsVolume] [int] NULL,	
	[CustomFromAddress] [nvarchar](256) NULL,
	[CustomFromName] [nvarchar](256) NULL,
	[SuspendReason] [nvarchar](256) NULL,
	[IsSuspendedOrDeletedByParent] [bit] NOT NULL,	
	[CustomDomain] [nvarchar](255) NULL,	
	[IsCustomDomainActive] [bit] NOT NULL,	
	[IsHideLogoInFooter] [bit] NOT NULL,
	[IsHideLogoInLoginFooter] [bit] NOT NULL,
	[IsNeedProfileManagement] [bit] NOT NULL,
	[IsNeedPDFsToBeColorManaged] [bit] NOT NULL,
	[IsRemoveAllGMGCollaborateBranding] [bit] NOT NULL,
	[Status] [int] NOT NULL,
	[IsEnabledLog] [bit] NOT NULL,		
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsHelpCentreActive] [bit] NOT NULL,
	[DealerNumber] [nvarchar](32) NULL,
	[CustomerNumber] [nvarchar](32) NULL,
	[VATNumber] [nvarchar](32) NULL,
	[IsAgreedToTermsAndConditions] [bit] NOT NULL,	
	[ContractStartDate] [datetime2](7) NULL,
	[Region] [nvarchar](128) NOT NULL,
	[IsTemporary] [bit] NOT NULL,
CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
( [ID] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The unique identifier' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The site name of the account in while labels' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SiteName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account Domain for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Domain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Owner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A unique string that can be used to identify the account in login-less situations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'First name of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactFirstName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Last name of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactLastName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Phone # of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Email address of the contact' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContactEmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the header logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'HeaderLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the login logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'LoginLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The path to the email logo file of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'EmailLogoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Selected theme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Theme'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom color value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomColor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default currency type of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CurrencyFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default locale of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Locale'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time zone' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'TimeZone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date format of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'DateFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The time format of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'TimeFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Global Price Plan Discount for this accounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'GPPDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This account need subscription plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'NeedSubscriptionPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If monthly limit is exceeded then charge cost per proof' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ChargeCostPerProofIfExceeded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Billing plan of this accounts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SelectedBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Billing will happen monthly?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsMonthlyBillingFrequency'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Billing anniversary date for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'BillingAnniversaryDate'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Billing anniversary month for this account, if frequency is yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'BillingAnniversaryMonth'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract period for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContractPeriod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'How many child accounts this account can have?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ChildAccountsVolume'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom from address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomFromAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Custom from name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomFromName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The reason this account was suspended' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'SuspendReason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suspended or deleted by parent?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsSuspendedOrDeletedByParent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The custom Domain for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomDomain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this account has coustom domain?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsCustomDomainActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hide the logo in the footer?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHideLogoInFooter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Hide the logo in the login footer?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHideLogoInLoginFooter'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is need profile management for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsNeedProfileManagement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Need PDFs to be color managed for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsNeedPDFsToBeColorManaged'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is remove all GMGCollaborate branding for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsRemoveAllGMGCollaborateBranding'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is Log enabled for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsEnabledLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User who created the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Date the Account was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TheUser who last modified this Account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date the Account was last modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Help center is avaialble for this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsHelpCentreActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dealer number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'DealerNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The customer number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'CustomerNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The VAT number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'VATNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is agreed to the terms and conditions in welcome page when activating the account?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsAgreedToTermsAndConditions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contract start date of this account?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'ContractStartDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Accounts region' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'Region'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this account created temporaryly?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account', @level2type=N'COLUMN',@level2name=N'IsTemporary'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Account'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Theme]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Theme](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_Theme] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Theme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Theme' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Theme'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountAccountSetting]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountAccountSetting](
	[Account] [int] NOT NULL,
	[AccountSetting] [int] NOT NULL,
CONSTRAINT [PK_AccountAccountSetting] PRIMARY KEY CLUSTERED 
(
	[Account] ASC,
	[AccountSetting] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountAccountSetting', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountAccountSetting', @level2type=N'COLUMN',@level2name=N'AccountSetting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountAccountSetting'
GO **/
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountHelpCentre]    Script Date: 06/14/2012 13:55:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AccountHelpCentre](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[ContactEmail] [nvarchar](64) NOT NULL,
	[ContactPhone] [nvarchar](32) NULL,
	[SupportDays] [nvarchar](128) NULL,
	[SupportHours] [nvarchar](64) NULL,
	[UserGuideLocation] [nvarchar](256) NULL,
	[UserGuideName] [nvarchar](256) NULL,
	[UserGuideUpdatedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_AccountHelpCentre] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account which this help center details are belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center email address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ContactEmail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center contact number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'ContactPhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center open days' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'SupportDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The help center open hours' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'SupportHours'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide location' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideLocation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PDF user guide updated date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre', @level2type=N'COLUMN',@level2name=N'UserGuideUpdatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountHelpCentre'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountPrePressFunction]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPrePressFunction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[PrePressFunction] [int] NOT NULL,
 CONSTRAINT [PK_AccountPrePressFunction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the PrePressFunction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction', @level2type=N'COLUMN',@level2name=N'PrePressFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPrePressFunction'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ValueDataType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValueDataType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_ValueDataType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the DataType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the DataType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValueDataType'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountSetting]    Script Date: 02/23/2012 15:06:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] int NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Value] [nvarchar](MAX) NOT NULL,
	[ValueDataType] int NOT NULL,
	[Description] [nvarchar](512) NULL,
CONSTRAINT [PK_AccountSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account of the account setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the account setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the value of the account setting as a string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value''s data type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'ValueDataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A description of the Account Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountSetting'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountStatus]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_AccountStatus] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountStatus'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the AccountType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the AccountType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountType'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[PrePressFunction]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PrePressFunction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_PrePressFunction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the PrePress Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the PrePress Function' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PrePressFunction'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Folder]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Parent] [int] NULL,
	[Account] [int] NOT NULL,
	--[SelectedAccessMode] int NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,	
	[ModifiedDate] [datetime2](7) NOT NULL,	
	[IsDeleted] [bit] NOT NULL,
	--[IsArchived] [bit] NOT NULL DEFAULT(0),
	[IsPrivate] [bit] NOT NULL,
CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Collaborator' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The parent folder of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Account'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to selected access mode' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'SelectedAccessMode'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The creator of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The created date of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The modifier of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The modified date of this folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder is deleted?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder is archived?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsArchived'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this private folder?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder', @level2type=N'COLUMN',@level2name=N'IsPrivate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Folder'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalDecision]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
	[Description] [nvarchar](128) NULL,
CONSTRAINT [PK_ApprovalDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalDecision'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[JobStatus]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JobStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_JobStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'JobStatus'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Job]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Job](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Title] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
	[Account] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Title of this Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Status of this Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account associated with the Job' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The status modified date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Job'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Approval]		******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Approval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorMessage] [nvarchar](512) NULL,
	[FileName] [nvarchar](128) NOT NULL,
	[FolderPath] [nvarchar](512) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[Job] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[Deadline] [datetime2](7) NULL,
	--[Account] [int] NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsPageSpreads] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[LockProofWhenAllDecisionsMade] [bit] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[OnlyOneDecisionRequired] [bit] NOT NULL,
	[PrimaryDecisionMaker] [int] NULL,
	[AllowDownloadOriginal] [bit] NOT NULL,
	[FileType] [int] NOT NULL,
	[Size] [decimal] (18,2) NOT NULL,
	[Owner] [int] NOT NULL,
	[IsError] [bit] NOT NULL,
CONSTRAINT [PK_Approval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Error Message of the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ErrorMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the uploaded file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FileName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file folder path' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FolderPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A uniquely identifying string that can be used to retrive the Material in login-less scenarios' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Job ID of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Job'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Version number of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Version'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this Approval was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who created this Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime by which this Approval should be completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Deadline'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Company associated with the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Account'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who last modified the Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the Approval was last modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, Page spreads is et in Flex component' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to if this approval is deleted or not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsPageSpreads'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lock the proof when all decisions are made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'LockProofWhenAllDecisionsMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The proof is in locked state' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsLocked'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Only one decision is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'OnlyOneDecisionRequired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary decision maker, if only one decion is required' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'PrimaryDecisionMaker'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allow users to download the original' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'AllowDownloadOriginal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'FileType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Uploaded file size' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Size'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'Owner'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Proof is in Error' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval', @level2type=N'COLUMN',@level2name=N'IsError'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Approval'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalPage]    Script Date: 02/19/2012 22:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalPage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Number] [int] NOT NULL,
	[Approval] [int] NOT NULL,
	[PageSmallThumbnailHeight] [int] NOT NULL,
	[PageSmallThumbnailWidth] [int] NOT NULL,
	[PageLargeThumbnailHeight] [int] NOT NULL,
	[PageLargeThumbnailWidth] [int] NOT NULL,
	[DPI] [int] NOT NULL,
	[Progress] [int] NOT NULL
CONSTRAINT [PK_ApprovalPage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page number', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval to which this ApprovalPage belongs', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page small thumbnail height', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageSmallThumbnailHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page small thumbnail width', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageSmallThumbnailWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page large thumbnail height', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageLargeThumbnailHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page large thumbnail width', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'PageLargeThumbnailWidth'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resolution of the file', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'DPI'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Progress of the file', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage', @level2type=N'COLUMN',@level2name=N'Progress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'', @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalPage'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalUserViewInfo]    Script Date: 02/19/2012 22:47:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalUserViewInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[User] [int] NOT NULL,
	[ViewedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalUserViewInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval that user has been viewed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Viewed date and time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo', @level2type=N'COLUMN',@level2name=N'ViewedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalUserViewInfo'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCommentType]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCommentType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_ApprovalCommentType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCommentType'
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationType]    Script Date: 02/19/2012 22:41:39 ******/
/** SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_ApprovalAnnotationType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationType'
GO **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FileType]    Script Date: 04/02/2012 14:51:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Extention] [nvarchar](8) NOT NULL,
	[Name] [nvarchar](64) NULL,
 CONSTRAINT [PK_FileType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The extention of the FileType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'Extention'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the FileType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FileType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotation]    Script Date: 02/19/2012 22:51:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotation](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [int] NOT NULL,
	[Parent] [int] NULL,
	[Page] [int] NOT NULL,
	[OrderNumber] [int] NULL,
	[Creator] [int] NULL,
	[Modifier] [int] NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Comment] [nvarchar](MAX) NOT NULL,
	[ExternalCollaborator] [int] NULL,
	[ReferenceFilepath] [nvarchar](512) NULL,
	[AnnotatedDate] [datetime2](7) NOT NULL,
	--[Type] [int] NULL,
	[CommentType] [int] NOT NULL,
	[StartIndex] [int] NULL,
	[EndIndex] [int] NULL,
	[HighlightType] [int] NULL,
	[CrosshairXCoord] [int] NULL,
	[CrosshairYCoord] [int] NULL,
	[HighlightData] [nvarchar](MAX) NULL,
	[IsPrivate] [bit] NOT NULL,
CONSTRAINT [PK_ApprovalAnnotation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Comment status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Parent Approval Comment of this Approval Comment if any' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval Page that contains this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Page'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Order number of the Annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'OrderNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Collaborator who made this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Collaborator who modify this Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date that this comment has been modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The textual content of the Approval Comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Comment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TODO: remove this property' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'ReferenceFilepath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this Approval Comment was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'AnnotatedDate'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of the Annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'Type'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of the comment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CommentType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Start index for text metric of annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'StartIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'End index for text metric of annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'EndIndex'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'HighlightType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crosshair X Coordinate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CrosshairXCoord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crosshair Y Coordinate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'CrosshairYCoord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'HighlightData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this private annotation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation', @level2type=N'COLUMN',@level2name=N'IsPrivate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotation'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[HighlightType]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[HighlightType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_HighlightType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this highlight type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this highlight type will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HighlightType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationPrivateCollaborator]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationPrivateCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalAnnotationPrivateCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationPrivateCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationAttachment]    Script Date: 04/02/2012 14:52:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationAttachment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileType] [int] NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[DisplayName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_ApprovalAnnotationAttachment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The FileType id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'FileType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The display name of the file' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationAttachment'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationTextPrintData]    Script Date: 04/02/2012 14:52:30 ******/
/*SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationTextPrintData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[Data] [varbinary](MAX) NULL,
 CONSTRAINT [PK_ApprovalAnnotationTextPrintData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval annotation id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData', @level2type=N'COLUMN',@level2name=N'Data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationTextPrintData'
GO*/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Shape]    Script Date: 04/02/2012 14:52:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Shape](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IsCustom] [bit] NOT NULL,
	[FXG] [nvarchar](MAX) NULL,
	[SVG] [nvarchar](MAX) NULL,
 CONSTRAINT [PK_Shape] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this custom shape?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'IsCustom'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The FXG of the shape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'FXG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The data to create the image file back' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape', @level2type=N'COLUMN',@level2name=N'SVG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Shape'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationMarkup]    Script Date: 04/02/2012 14:34:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalAnnotationMarkup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalAnnotation] [int] NOT NULL,
	[X] [int] NOT NULL,
	[Y] [int] NOT NULL,
	[Width] [int] NOT NULL,
	[Height] [int] NOT NULL,
	[Color] [nvarchar](8) NULL,
	[Size] [int] NULL,
	[BackgroundColor] [nvarchar](8) NULL,
	[Rotation] [int] NULL,
	--[FXG] [nvarchar](64) NULL,
	[Shape] [int] NULL,
	--[StrokeColor] [nvarchar](8) NULL,
	--[StokeAlpha] [nvarchar](8) NULL,
	--[StrokeWeight] [int] NULL,
	--[FillColor] [nvarchar](8) NULL,
	--[FillAlpha] [nvarchar](8) NULL,
 CONSTRAINT [PK_ApprovalAnnotationTypeMarkup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the comment markup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval Comment that the markup belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'ApprovalAnnotation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The X ordinate of the origin of the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'X'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Y ordinate of the origin of the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Y'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Width if the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Width'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Height if the markup in pixels/pt' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Height'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The color of the text control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Color'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The size of the font' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Size'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The backgroundcolor of the text control' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'BackgroundColor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Rotaion value of the shape' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Rotation'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'FXG'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Shape information' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'Shape'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'StrokeColor'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'StokeAlpha'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'StrokeWeight'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'FillColor'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup', @level2type=N'COLUMN',@level2name=N'FillAlpha'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationMarkup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAnnotationStatus]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalAnnotationStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_ApprovalAnnotationStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The lower this value, the higher up this Status will appear in lists' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAnnotationStatus'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaborator]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	[ApprovalCollaboratorRole] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval collaborator role id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator assigned to this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaboratorGroup]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_ApprovalCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FolderCollaborator]    Script Date: 04/16/2012 14:36:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FolderCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Folder] [int] NOT NULL,
	[Collaborator] [int] NOT NULL,
	--[CollaboratorGroup] [int] NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_FolderCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Folder id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The collaborator assigned date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FolderCollaboratorGroup]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FolderCollaboratorGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Folder] [int] NOT NULL,
	[CollaboratorGroup] [int] NOT NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_FolderCollaboratorGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The folder id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user group id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'CollaboratorGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date of this collaborator or group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderCollaboratorGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[FolderApproval]    Script Date: 04/16/2012 14:52:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FolderApproval](
	[Folder] [int] NOT NULL,
	[Approval] [int] NOT NULL,
 CONSTRAINT [PK_FolderApproval] PRIMARY KEY CLUSTERED 
(
	[Folder] ASC,
	[Approval] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the folder' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FolderApproval'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaboratorDecision]    Script Date: 04/16/2012 16:05:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaboratorDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Decision] [int] NULL,
	[Collaborator] [int] NULL,
	[ExternalCollaborator] [int] NULL,
	[AssignedDate] [datetime2](7) NOT NULL,
	[OpenedDate] [datetime2](7) NULL,
	[CompletedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_ApprovalCollaboratorDecisionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval History record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Approval to which the History record belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Decision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The assigned user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'Collaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time is assigend' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'AssignedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time is opened' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'OpenedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time this decision has been completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision', @level2type=N'COLUMN',@level2name=N'CompletedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorDecision'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[LogModule]    Script Date: 04/16/2012 16:19:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogModule](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_LogModule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the LogModule' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogModule'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[LogAction]    Script Date: 04/16/2012 16:19:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
	[LogModule] [int] NOT NULL,
	[Message] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_LogAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The LogModule of the HistoryAction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'LogModule'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The action message with formatted info string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction', @level2type=N'COLUMN',@level2name=N'Message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogAction'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[LogApproval]    Script Date: 04/16/2012 16:19:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LogApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[LogAction] [int] NOT NULL,
	[ActualMessage] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_LogApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The log action id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'LogAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval', @level2type=N'COLUMN',@level2name=N'ActualMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogApproval'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--


/****** Object:  Table [dbo].[ApprovalHistory]    Script Date: 02/19/2012 22:57:45 ******/
/* SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[Decision] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[DateCreated] [datetime2](7) NULL,
	[Action] [int] NOT NULL,
	[ActionMessage] [nvarchar](128) NULL,
	[ActionData] [nvarchar](128) NULL,
CONSTRAINT [PK_ApprovalHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Advanced Approval History record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Advanced Approval to which the History record belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the status whic hwas applied to the approval at the time this record was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Decision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who triggered the creation of this history record' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date and time this history record was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'DateCreated'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the action that was applied' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The message for the action associated with this history' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'ActionMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Any further data that needs to be attached to the history record fior the action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory', @level2type=N'COLUMN',@level2name=N'ActionData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalHistory'
GO   */

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AttachedAccountBillingPlan]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttachedAccountBillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[BillingPlan] [int] NOT NULL,
	[NewPrice] [decimal](9,2) NULL,
	[IsAppliedCurrentRate] [bit] NOT NULL,
	[NewProofPrice] [decimal](9,2) NULL,
	[IsModifiedProofPrice] [bit] NOT NULL,
CONSTRAINT [PK_AttachedAccountBillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO	
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Attached Account Billing Plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account id this billing plan is assigned to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The billing plan of this account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The billing monthly plan new currency type value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'NewPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New value is based on the current rate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'IsAppliedCurrentRate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The single proof price based on new currency type value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'NewProofPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'User has been modified the proof price' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan', @level2type=N'COLUMN',@level2name=N'IsModifiedProofPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AttachedAccountBillingPlan'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[BillingAnniversaryDate]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingAnniversaryDate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
 CONSTRAINT [PK_BillingAnniversaryDate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Anniversary Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the Billing Anniversary Date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingAnniversaryDate'
GO  **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[InvoivePeriod]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoivePeriod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](256) NULL,
CONSTRAINT [PK_InvoivePeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the Billing Frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'InvoivePeriod'
GO **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[BillingPlan]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Type] [int] NOT NULL,
	[Proofs] [int] NULL,
	[Price] [decimal](9,2) NOT NULL,
	[IsFixed] [bit] NOT NULL,
CONSTRAINT [PK_BillingPlan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Proof count allow by this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Proofs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The price of this plan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'Price'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is this fixed plan?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan', @level2type=N'COLUMN',@level2name=N'IsFixed'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlan'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[BillingPlanType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BillingPlanType](
	[ID] [int] IDENTITY(1,1) NOT NULL,	
	[Key] [nvarchar](64) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,	
	[MediaService] [int] NOT NULL,
	[EnablePrePressTools] [bit] NOT NULL,
	[EnableMediaTools] [bit] NOT NULL,
CONSTRAINT [PK_BillingPlanType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The media service attached to this Billing Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'MediaService'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enable pre press tools for this plan type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'EnablePrePressTools'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enable media tools for this Plan Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType', @level2type=N'COLUMN',@level2name=N'EnableMediaTools'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BillingPlanType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountBillingPlanChangeLog]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBillingPlanChangeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[LogMessage] [nvarchar](256) NOT NULL,
	[PreviousPlanAmount] [decimal](9,2) NULL,
	[PreviousProofsAmount] [int] NULL,
	[GPPDiscount] [decimal](3,2) NULL,
	[PreviousPlan] [int] NULL,
	[CurrentPlan] [int] NULL,
CONSTRAINT [PK_AccountBillingPlanChangeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'LogMessage'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousPlanAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous proof amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousProofsAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GPPDiscount of the account' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'GPPDiscount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'PreviousPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Current plan id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog', @level2type=N'COLUMN',@level2name=N'CurrentPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingPlanChangeLog'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountBillingTransactionHistory]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountBillingTransactionHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Creator] [int] NOT NULL,
	[BaseAmount] [decimal](9,2) NOT NULL,
	[DiscountAmount] [decimal](9,2) NOT NULL,
CONSTRAINT [PK_AccountBillingTransactionHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this log' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Actual log message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'BaseAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Previous plan amount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory', @level2type=N'COLUMN',@level2name=N'DiscountAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountBillingTransactionHistory'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Company]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Name] [nvarchar](128) NULL,
	[Number] [nvarchar](32) NULL,
	[Address1] [nvarchar](128) NOT NULL,
	[Address2] [nvarchar](128) NULL,
	[City] [nvarchar](64) NOT NULL,
	[Postcode] [nvarchar](20) NULL,
	[State] [nvarchar](20) NULL,
	[Phone] [nvarchar](20) NOT NULL,
	[Mobile] [nvarchar](20) NULL,
	[Email] [nvarchar](64) NULL,
	[Country] [int] NOT NULL,
CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Account to which the Company belongs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ABN/ACN or similar of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address1 of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address2 of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Address2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The city of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'City'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The postcode of eh Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Postcode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The state of the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The phone number of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mobile number of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Mobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The email address of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Country of the company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Company', @level2type=N'COLUMN',@level2name=N'Country'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ContractPeriod]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractPeriod](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
 CONSTRAINT [PK_ContractPeriod] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Contract Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Contract Period' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ContractPeriod'
GO 

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ControllerAction]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControllerAction](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Controller] [nvarchar](128) NOT NULL,
	[Action] [nvarchar](128) NULL,
	[Parameters] [nvarchar](128) NULL,
CONSTRAINT [TBL_ControllerAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Controller' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Controller'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the Controller Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Action'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Parameeters of the Controller Action' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction', @level2type=N'COLUMN',@level2name=N'Parameters'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ControllerAction'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Country]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Iso2] [nvarchar](2) NOT NULL,
	[Iso3] [nvarchar](3) NOT NULL,
	[IsoCountryNumber] [int] NOT NULL,
	[DialingPrefix] [int] NULL,
	[Name] [nvarchar](64) NOT NULL,
	[ShortName] [nvarchar](50) NOT NULL,
	[HasLocationData] [bit] NOT NULL,
CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO2 code for the country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Iso2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO3 code for the Country' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Iso3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ISO Country number' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'IsoCountryNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dialing prefix for the Company' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'DialingPrefix'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Country''s name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Country''s short name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'ShortName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'true if the country has location data in the Location table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country', @level2type=N'COLUMN',@level2name=N'HasLocationData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Currency]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Code] [nvarchar](4) NOT NULL,
	[Symbol] [nvarchar](5) NULL,
	--[Country] [int] NULL,
CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Symbol'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The country of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency', @level2type=N'COLUMN',@level2name=N'Country'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Currency'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[DateFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DateFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pattern] [nvarchar](16) NOT NULL,
	[Result] [nvarchar](16) NOT NULL,
CONSTRAINT [PK_DateFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The pattern of the date format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'Pattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sample result of the pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DateFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[TimeFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar] (4) NOT NULL,
	[Pattern] [nvarchar](16) NOT NULL,
	[Result] [nvarchar](16) NOT NULL,
CONSTRAINT [PK_TimeFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the time format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The pattern of the time format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Pattern'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The sample result of the pattern' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat', @level2type=N'COLUMN',@level2name=N'Result'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimeFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[CurrencyRate]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrencyRate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [int] NOT NULL,
	[Rate] [decimal](10,2) NOT NULL,
	[Creator] [int] NOT NULL,
	[Modifier] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_CurrencyRate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Currency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Rate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The code of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The symbol of the Currency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CurrencyRate'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Locale]    Script Date: 11/02/2010 20:42:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Locale](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](16) NULL,
	[DisplayName] [nvarchar](64) NULL,
CONSTRAINT [PK_Locale] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Locale display name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale', @level2type=N'COLUMN',@level2name=N'DisplayName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Locale'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[MenuItem]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ControllerAction] [int] NOT NULL,	
	[Parent] [int] NULL,
	[Position] [int] NOT NULL,		
	[IsAdminAppOwned] [bit] NULL,
	[IsAlignedLeft] [bit] NOT NULL,
	[IsSubNav] [bit] NOT NULL,
	[IsTopNav] [bit] NOT NULL,	
	[IsVisible] [bit] NOT NULL,	
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,	
	[Title] [nvarchar](128) NOT NULL,
CONSTRAINT [PK_MenuItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'ControllerAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identify to weather manu item is aligned to left or right?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsAlignedLeft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent of the Menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item position' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Position'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item visible?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsVisible'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item is placed inside main or top level menu?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsTopNav'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item is visible for Admin App?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsAdminAppOwned'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is sub navigation?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'IsSubNav'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The title of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem', @level2type=N'COLUMN',@level2name=N'Title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItem'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[MenuItemNameLocale]    Script Date: 11/02/2010 20:42:40 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[MenuItemNameLocale](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Locale] [int] NOT NULL,
--	[MenuItem] [int] NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,	
--	[Title] [nvarchar](128) NOT NULL,
--CONSTRAINT [PK_MenuItemNameLocale] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Locale Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'Locale'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Menu item Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'MenuItem'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Name of the menu based on the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Titile of the menu based on the Locale' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale', @level2type=N'COLUMN',@level2name=N'Title'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemNameLocale'
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[MenuItemAccountTypeRole]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuItemAccountTypeRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MenuItem] [int] NOT NULL,	
	[AccountTypeRole] [int] NOT NULL,
CONSTRAINT [PK_MenuItemAccountTypeRole] PRIMARY KEY CLUSTERED 
( [ID] ASC )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the menu item' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'MenuItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole', @level2type=N'COLUMN',@level2name=N'AccountTypeRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MenuItemAccountTypeRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[NotificationFrequency]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationFrequency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	--[IsCustom] [bit] NOT NULL,
	--[Frequency] [nvarchar](256) NULL,
CONSTRAINT [PK_NotificationFrequency] PRIMARY KEY CLUSTERED 
( [ID] ASC )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the NotificationFrequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the NotificationFrequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Name'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Is custom frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'IsCustom'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The frequency, if this is a custom one' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency', @level2type=N'COLUMN',@level2name=N'Frequency'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationFrequency'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[NotificationEmailQueue]    Script Date: 02/23/2012 15:06:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationEmailQueue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PrimaryRecipient] [int] NOT NULL,
	[EventType] [int] NOT NULL,
	[Creator] [int] NULL,
	[SecondaryRecipient] [int] NULL,
	[ExternalCollaborator] [int] NULL,
	[BillingPlan1] [int] NULL,
	[BillingPlan2] [int] NULL,
	[Account] [int] NULL,
	[Approval] [int] NULL,
	[Folder] [int] NULL,
	[UserGroup] [int] NULL,
	[Header] [nvarchar](max) NOT NULL,
	[Body] [nvarchar](max) NULL,
	[IsEmailSent] [bit] NOT NULL,
	[SentDate] [datetime2](7) NULL,
	[CreatedDate] [datetime] NOT NULL,
CONSTRAINT [PK_NotificationEmailQueue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'PrimaryRecipient of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'PrimaryRecipient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'EventType of the NotificationEmailQueue as a string' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'EventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SecondaryRecipient of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'SecondaryRecipient'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ExternalCollaborator of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BillingPlan1 of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'BillingPlan1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'BillingPlan2 of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'BillingPlan2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Approval of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Folder of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Folder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UserGroup of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'UserGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Header of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Header'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Body of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'Body'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IsEmailSent or not' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'IsEmailSent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SentDate of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'SentDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CreatedDate of the NotificationEmailQueue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NotificationEmailQueue', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Role]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The order to be shown on UIs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Role'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[User]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[Username] [nvarchar](128) NOT NULL,
	[Password] [varchar](64) NOT NULL,
	[GivenName] [nvarchar](64) NOT NULL,
	[FamilyName] [nvarchar](64) NOT NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[PhotoPath] [nvarchar](256) NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[MobileTelephoneNumber] [nvarchar](20) NULL,
	[HomeTelephoneNumber] [nvarchar](20) NULL,
	[OfficeTelephoneNumber] [nvarchar](20) NOT NULL,
	[NotificationFrequency] [int] NOT NULL,
	[DateLastLogin] [datetime2](7) NULL,
	--[IsActive] [bit] NOT NULL,
	--[IsDeleted] [bit] NOT NULL DEFAULT(0),
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[Preset] [int] NOT NULL,
	[IncludeMyOwnActivity] [bit] NOT NULL,
CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
( [ID] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account that this user belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The username used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The password used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Photo of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'PhotoPath'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The guid associated with this user that can be used instead of authentication under certain circumstances' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mobile telephone number number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'MobileTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The home telephone number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'HomeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The office telephone number associated with the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'OfficeTelephoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Notification frequency to send emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this user last authenticated with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'DateLastLogin'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is active and is able to authenticate with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IsActive'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is deleted and is no longer available to the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IsDeleted'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who last modified this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was last updated' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Preset level' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Include my own activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User', @level2type=N'COLUMN',@level2name=N'IncludeMyOwnActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'User'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserControllerActionAccess]    ******/
/** SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserControllerActionAccess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ControllerAction] [int] NOT NULL,
	[MenuItem] [int] NOT NULL,
	[User] [int] NOT NULL,
CONSTRAINT [PK_UserControllerActionAccess] PRIMARY KEY CLUSTERED 
( [ID] ASC )
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY])
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This user can be aaccessed this page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'ControllerAction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Menu item of the page' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'MenuItem'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This page can be accessed by this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserControllerActionAccess'
GO  **/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserGroup]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	--[Description] [nvarchar](256) NULL,
	[Account] [int] NOT NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the UserGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the UserGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Description'
--GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Account id of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creator of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Created date of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modifier of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Modified date id of this user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserGroupUser]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroupUser](
	[UserGroup] [int] NOT NULL,
	[User] [int] NOT NULL,
CONSTRAINT [PK_UserGroupUser] PRIMARY KEY CLUSTERED 
(
	[UserGroup] ASC,
	[User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser', @level2type=N'COLUMN',@level2name=N'UserGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserGroupUser'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserHistory]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Company] [int] NOT NULL,
	[Username] [nvarchar](32) NOT NULL,
	[Password] [varchar](255) NULL,
	[GivenName] [nvarchar](32) NULL,
	[FamilyName] [nvarchar](32) NOT NULL,
	[EmailAddress] [nvarchar](64) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
CONSTRAINT [PK_UserHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User to which this History pertains' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The company that this User belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The username used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Username'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The password used for authentication' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Password'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is active and is able to authenticate with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'IsActive'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this user is deleted and is no longer available to the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserHistory'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserLogin]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[IpAddress] [nvarchar](16) NOT NULL,
	[Success] [bit] NOT NULL,
	[DateLogin] [datetime2](7) NOT NULL,
	[DateLogout] [datetime2](7) NULL,
	--[SessionId] [nvarchar](50) NOT NULL,
CONSTRAINT [PK_UserLogin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User who created this UserLogin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The IP address that the User logged in from' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'IpAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether or not the login attempt was successful' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'Success'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user attempted to login' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'DateLogin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime the user logged out (if any)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin', @level2type=N'COLUMN',@level2name=N'DateLogout'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserLogin'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserRole]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[Role] [int] NOT NULL,
CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO	
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountTypeRole]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountTypeRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [int] NOT NULL,
	[Role] [int] NOT NULL,
CONSTRAINT [PK_AccountTypeRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserStatus]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_UserStatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserStatus'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[StudioSettingType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudioSettingType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_StudioSettingType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Studio Setting Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Studio Setting Type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSettingType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[HowOftenReceiveEmails]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[HowOftenReceiveEmails](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,
--	[Description] [nvarchar](256) NULL,
--CONSTRAINT [PK_HowOftenReceiveEmails] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the How Often Receive Emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The description of the How Often Receive Emails' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails', @level2type=N'COLUMN',@level2name=N'Description'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HowOftenReceiveEmails'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[CommentEmailSendDecision]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentEmailSendDecision](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_CommentEmailSendDecision] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Comment Email Send Decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Comment Email Send Decision' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CommentEmailSendDecision'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[StudioSetting]    Script Date: 04/11/2012 21:01:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudioSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [int] NOT NULL,
	[User] [int] NULL,
	[NotificationFrequency] [int] NOT NULL,
	[IncludeMyOwnActivity] [bit] NOT NULL,
	[WhenCommentMade] [int] NOT NULL,
	[RepliesMyComment] [int] NOT NULL,
	[NewVesionAdded] [int] NOT NULL,
	[FinalDecisionMade] [int] NOT NULL,
	[EnableColorCorrection] [bit] NOT NULL,
 CONSTRAINT [PK_StudioSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the studio Setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user belongs to these setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notification frequency' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'NotificationFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Include my own activity' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'IncludeMyOwnActivity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When a comment is made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'WhenCommentMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Replies to my comments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'RepliesMyComment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'New version added' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'NewVesionAdded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Final decisions made' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'FinalDecisionMade'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Disable or enable color correction' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting', @level2type=N'COLUMN',@level2name=N'EnableColorCorrection'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'StudioSetting'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Preset]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preset](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_Preset] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Preset' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Preset' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Preset'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[EventGroup]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_EventGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the EventGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the EventGroup' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventGroup'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[EventType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventGroup] [int] NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_EventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The EventGroup of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'EventGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the EventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EventType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[RolePresetEventType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePresetEventType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Role] [int] NOT NULL,
	[Preset] [int] NOT NULL,
	[EventType] [int] NOT NULL,
CONSTRAINT [PK_RolePresetEventType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Role of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Preset of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'Preset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The EventType of the PresetEventType' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType', @level2type=N'COLUMN',@level2name=N'EventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RolePresetEventType'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[UserPresetEventType]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCustomPresetEventTypeValue](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[User] [int] NOT NULL,
	[RolePresetEventType] [int] NOT NULL,
	[Value] [bit] NOT NULL,
CONSTRAINT [PK_UserCustomPresetEventTypeValue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The User of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The RolePresetEventType of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'RolePresetEventType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Value of the UserCustomPresetEventTypeValue' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserCustomPresetEventTypeValue'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ChangedBillingPlan]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangedBillingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[BillingPlan] [int] NULL,
	[AttachedAccountBillingPlan] [int] NULL,	
 CONSTRAINT [PK_ChangedBillingPlan] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'BillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan', @level2type=N'COLUMN',@level2name=N'AttachedAccountBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ChangedBillingPlan'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ChangedBillingPlan]	******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountPricePlanChangedMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChangedBillingPlan] [int] NOT NULL,
	[ChildAccount] [int] NOT NULL,
 CONSTRAINT [PK_AccountPricePlanChangedMessage] PRIMARY KEY CLUSTERED 
(	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ChangedBillingPlan'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage', @level2type=N'COLUMN',@level2name=N'ChildAccount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountPricePlanChangedMessage'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--


/****** Object:  Table [dbo].[Smoothing]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Smoothing](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_Smoothing] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Smoothing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Smoothing' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Smoothing'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[Colorspace]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colorspace](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_Colorspace] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Colorspace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Colorspace' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Colorspace'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ImageFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImageFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Parent] [int] NULL,
CONSTRAINT [PK_ImageFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The parent of the Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat', @level2type=N'COLUMN',@level2name=N'Parent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ImageFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[PageBox]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageBox](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_PageBox] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Page Box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Page Box' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PageBox'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[TileImageFormat]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TileImageFormat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
CONSTRAINT [PK_TileImageFormat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Tile Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Tile Image Format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TileImageFormat'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[MediaService]    Script Date: 02/19/2012 22:41:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MediaService](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[ApplySimulateOverprinting] [bit] NOT NULL,
	[Smoothing] [int] NOT NULL,
	[Resolution] [int] NOT NULL,
	[Colorspace] [int] NOT NULL,
	[ImageFormat] [int] NOT NULL,
	[PageBox] [int] NOT NULL,
	[TileImageFormat] [int] NOT NULL,
	[JPEGCompression] [int] NOT NULL,
CONSTRAINT [PK_MediaService] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of this Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apply the simulate overprinting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ApplySimulateOverprinting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The smoothing attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Smoothing'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The resolution attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Resolution'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The colorspace attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'Colorspace'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The image format attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'ImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The page box attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'PageBox'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The tile image format attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'TileImageFormat'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The jpeg compression attached to Media Service' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService', @level2type=N'COLUMN',@level2name=N'JPEGCompression'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MediaService'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalSeparationPlate]    Script Date: 11/08/2012 11:27:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalSeparationPlate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Page] [int] NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_ApprovalSeparationPlate] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval page Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'Page'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalSeparationPlate'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalCollaboratorRole]    Script Date: 11/21/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalCollaboratorRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_ApprovalCollaboratorRole] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Role' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The order to be shown on UIs' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole', @level2type=N'COLUMN',@level2name=N'Priority'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalCollaboratorRole'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ExternalCollaborator]    Script Date: 11/21/2012 10:10:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

CREATE TABLE [dbo].[ExternalCollaborator](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [int] NOT NULL,
	[GivenName] [nvarchar](64) NULL,
	[FamilyName] [nvarchar](64) NULL,
	[EmailAddress] [nvarchar](128) NOT NULL,
	[Guid] [nvarchar](36) NOT NULL,
	[DateLoggedIn] [datetime2](7) NULL,
	[Creator] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Modifier] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ExternalCollaborator] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The account that this user belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Account'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user''s given name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'GivenName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the user''s family, or surname' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'FamilyName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the email address associated with this user - all correspondence is directed to this address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'EmailAddress'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The guid associated with this user that can be used instead of authentication under certain circumstances' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this user last authenticated with the system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'DateLoggedIn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who created this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Creator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'CreatedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user who modified this user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'Modifier'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the DateTime this user was modified' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'ModifiedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'the activeness of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalCollaborator'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[SharedApproval]    Script Date: 11/21/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SharedApproval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Approval] [int] NOT NULL,
	[ExternalCollaborator] [int] NOT NULL,
	[ApprovalCollaboratorRole] [int] NOT NULL,
	[SharedDate] [datetime2](7) NOT NULL,
	[IsSharedURL] [bit] NOT NULL,
	[IsSharedDownloadURL] [bit] NOT NULL,
	[IsExpired] [bit] NOT NULL,
	[ExpireDate] [datetime2](7) NOT NULL,
	[Notes] [nvarchar](512) NULL,
 CONSTRAINT [PK_SharedApproval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The approval id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'Approval'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external user id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ExternalCollaborator'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The external users role for this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ApprovalCollaboratorRole'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The shared date of this approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'SharedDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can share the url with external user?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsSharedURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Can share the download url with external user?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsSharedDownloadURL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The this share expired?' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'IsExpired'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The expire date of this share' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'ExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Notes of this share' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SharedApproval'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ReservedDomainName]    Script Date: 01/24/2013 14:45:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReservedDomainName](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ReservedDomainName] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the reserved domain name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReservedDomainName'
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[NativeDataType]    Script Date: 03/04/2013 22:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NativeDataType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar] (64) NOT NULL,
 CONSTRAINT [PK_NativeDataType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType', @level2type=N'COLUMN',@level2name=N'Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NativeDataType'
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  Table [dbo].[GlobalSettings]    Script Date: 03/04/2013 22:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GlobalSettings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](64) NOT NULL,
	[DataType] [int] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_GlobalSettings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the global settings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'Key'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The dta type of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'DataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The value of the setting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings', @level2type=N'COLUMN',@level2name=N'Value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GlobalSettings'
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  Table [dbo].[Variation]    Script Date: 11/08/2012 11:49:06 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[Variation](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Key] [nvarchar](4) NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,
-- CONSTRAINT [PK_Variations] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The key of the Variations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation', @level2type=N'COLUMN',@level2name=N'Key'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Variations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Variation'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[VariationPlate]    Script Date: 11/08/2012 11:49:06 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[VariationPlate](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Variation] [int] NOT NULL,
--	[Name] [nvarchar](64) NOT NULL,
-- CONSTRAINT [PK_VariationPlate] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Variation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate', @level2type=N'COLUMN',@level2name=N'Variation'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the Variation Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VariationPlate'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalAdditionalPlate]    Script Date: 11/08/2012 11:27:34 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalAdditionalPlate](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Approval] [int] NOT NULL,
--	[Name] [nvarchar](128) NOT NULL,
-- CONSTRAINT [PK_AdditionalPlate] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate', @level2type=N'COLUMN',@level2name=N'Approval'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the additional separation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate', @level2type=N'COLUMN',@level2name=N'Name'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalAdditionalPlate'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalVariationPlate]    Script Date: 11/08/2012 11:27:34 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalVariationPlate](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Approval] [int] NOT NULL,
--	[VariationPlate]  [int] NOT NULL,
--	[Name] [nvarchar](128) NOT NULL,
-- CONSTRAINT [PK_ApprovalVariationPlate] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Approval Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate', @level2type=N'COLUMN',@level2name=N'Approval'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The id of the Variation Plate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate', @level2type=N'COLUMN',@level2name=N'VariationPlate'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalVariationPlate'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[AccountVolume]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[AccountVolume](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Min] [int] NOT NULL,
--	[Max] [int] NOT NULL,
--CONSTRAINT [PK_AccountVolume] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimun value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume', @level2type=N'COLUMN',@level2name=N'Min'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum value' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume', @level2type=N'COLUMN',@level2name=N'Max'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountVolume'
--GO

--/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

--/****** Object:  Table [dbo].[AccountTypeAccountVolume]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[AccountTypeAccountVolume](
--	[AccountType] [int] NOT NULL,
--	[AccountVolume] [int] NOT NULL,
--CONSTRAINT [PK_AccountTypeAccountVolume] PRIMARY KEY CLUSTERED 
--(
--	[AccountType] ASC,
--	[AccountVolume] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeAccountVolume', @level2type=N'COLUMN',@level2name=N'AccountType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the account volume' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeAccountVolume', @level2type=N'COLUMN',@level2name=N'AccountVolume'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountTypeAccountVolume'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[AccountUser]    ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE TABLE [dbo].[AccountUser](
--	[Account] [int] NOT NULL,
--	[User] [int] NOT NULL,
--CONSTRAINT [PK_AccountUser] PRIMARY KEY CLUSTERED 
--(
--	[Account] ASC,
--	[User] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the acccount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountUser', @level2type=N'COLUMN',@level2name=N'Account'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountUser', @level2type=N'COLUMN',@level2name=N'User'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AccountUser'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  Table [dbo].[ApprovalApprover]    Script Date: 02/19/2012 22:38:14 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ApprovalApprover](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[Approval] [int] NULL,
--	[Approver] [int] NULL,
--	[DateAssigned] [datetime2](7) NULL,
--	[DateFirstViewing] [datetime2](7) NULL,
--	[Approved] [int] NULL,
--	[IsReviewed] [bit] NULL,
--	[DateReviewed] [datetime2](7) NULL,
--CONSTRAINT [PK_ApprovalApprover] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
--) ON [PRIMARY]

--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'ID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the AdvancedApproval that this Approval Approver belongs to' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'Approval'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the User that is represented by this Approval Approver' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'Approver'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTime this ApprovalApprover was created' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'DateAssigned'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateTIme this ApprovalApprover first viewed the associated Approval' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'DateFirstViewing'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If true, this ApprovalApprover has approved the associated Approval. If false, they have rejected it. If null, no action has been taken' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover', @level2type=N'COLUMN',@level2name=N'Approved'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ApprovalApprover'
--GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--
-- CONSTRAINS --
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

-- Account

ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsSuspendedOrDeletedByParent]  DEFAULT ((0)) FOR [IsSuspendedOrDeletedByParent]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsCustomDomainActive]  DEFAULT ((0)) FOR [IsCustomDomainActive]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsHideLogoInFooter]  DEFAULT ((0)) FOR [IsHideLogoInFooter]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsHideLogoInLoginFooter]  DEFAULT ((0)) FOR [IsHideLogoInLoginFooter]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsNeedProfileManagement]  DEFAULT ((0)) FOR [IsNeedProfileManagement]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsNeedPDFsToBeColorManaged]  DEFAULT ((0)) FOR [IsNeedPDFsToBeColorManaged]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsRemoveAllGMGCollaborateBranding]  DEFAULT ((0)) FOR [IsRemoveAllGMGCollaborateBranding]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsEnabledLog]  DEFAULT ((0)) FOR [IsEnabledLog]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsHelpCentreActive]  DEFAULT ((0)) FOR [IsHelpCentreActive]
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [DF_Account_IsAgreedToTermsAndConditions]  DEFAULT ((0)) FOR [IsAgreedToTermsAndConditions]
GO

---- AccountUser

--/****** Object:  ForeignKey [FK_AccountUser_Account]    ******/
--ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD CONSTRAINT [FK_AccountUser_Account] FOREIGN KEY([Account])
--REFERENCES [dbo].[Account] ([ID])
--GO
--ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_Account]
--GO

--/****** Object:  ForeignKey [FK_AccountUser_User]    ******/
--ALTER TABLE [dbo].[AccountUser]  WITH CHECK ADD CONSTRAINT [FK_AccountUser_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[AccountUser] CHECK CONSTRAINT [FK_AccountUser_User]
--GO

-- MenuItem

/****** Object:  ForeignKey [FK_MenuItem_Page]    ******/
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD CONSTRAINT [FK_MenuItem_ControllerAction] FOREIGN KEY([ControllerAction])
REFERENCES [dbo].[ControllerAction] ([ID])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_ControllerAction]
GO

/****** Object:  Default [DF_MenuItem_IsActive]    ******/
--ALTER TABLE [dbo].[MenuItem] ADD CONSTRAINT [DF_MenuItem_IsActive]  DEFAULT ((0)) FOR [IsActive]
--GO

/****** Object:  Default [DF_MenuItem_IsFeatureEnabled]    ******/
--ALTER TABLE [dbo].[MenuItem] ADD CONSTRAINT [DF_MenuItem_IsFeatureEnabled]  DEFAULT ((0)) FOR [IsFeatureEnabled]
--GO

/****** Object:  ForeignKey [FK_MenuItem_Parent]    ******/
ALTER TABLE [dbo].[MenuItem]  WITH CHECK ADD CONSTRAINT [FK_MenuItem_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[MenuItem] ([ID])
GO
ALTER TABLE [dbo].[MenuItem] CHECK CONSTRAINT [FK_MenuItem_Parent]
GO

-- MenuItemNameLocale

--ALTER TABLE [dbo].[MenuItemNameLocale]  WITH CHECK ADD CONSTRAINT [FK_MenuItemNameLocale_MenuItem] FOREIGN KEY([MenuItem])
--REFERENCES [dbo].[MenuItem] ([ID])
--GO
--ALTER TABLE [dbo].[MenuItemNameLocale] CHECK CONSTRAINT [FK_MenuItemNameLocale_MenuItem]
--GO

--ALTER TABLE [dbo].[MenuItemNameLocale]  WITH CHECK ADD CONSTRAINT [FK_MenuItemNameLocale_Locale] FOREIGN KEY([Locale])
--REFERENCES [dbo].[Locale] ([ID])
--GO
--ALTER TABLE [dbo].[MenuItemNameLocale] CHECK CONSTRAINT [FK_MenuItemNameLocale_Locale]
--GO

-- AccountTypeAccountVolume

--/****** Object:  ForeignKey [FK_AccountTypeAccountVolume_AccountType]    ******/
--ALTER TABLE [dbo].[AccountTypeAccountVolume]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeAccountVolume_AccountType] FOREIGN KEY([AccountType])
--REFERENCES [dbo].[AccountType] ([ID])
--GO
--ALTER TABLE [dbo].[AccountTypeAccountVolume] CHECK CONSTRAINT [FK_AccountTypeAccountVolume_AccountType]
--GO

--/****** Object:  ForeignKey [FK_AccountTypeAccountVolume_AccountVolume]    ******/
--ALTER TABLE [dbo].[AccountTypeAccountVolume]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeAccountVolume_AccountVolume] FOREIGN KEY([AccountVolume])
--REFERENCES [dbo].[AccountVolume] ([ID])
--GO
--ALTER TABLE [dbo].[AccountTypeAccountVolume] CHECK CONSTRAINT [FK_AccountTypeAccountVolume_AccountVolume]
--GO

---- UserCompany

--/****** Object:  ForeignKey [FK_UserCompany_User]    ******/
--ALTER TABLE [dbo].[UserCompany]  WITH CHECK ADD CONSTRAINT [FK_UserCompany_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[UserCompany] CHECK CONSTRAINT [FK_UserCompany_User]
--GO

--/****** Object:  ForeignKey [K_UserCompany_Company]    ******/
--ALTER TABLE [dbo].[UserCompany]  WITH CHECK ADD CONSTRAINT [FK_UserCompany_Company] FOREIGN KEY([Company])
--REFERENCES [dbo].[Company] ([ID])
--GO
--ALTER TABLE [dbo].[UserCompany] CHECK CONSTRAINT [FK_UserCompany_Company]
--GO

-- Account

/****** Object:  ForeignKey [FK_Account_AccountType]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_AccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountType]
GO

/****** Object:  ForeignKey [FK_Account_Locale]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Locale] FOREIGN KEY([Locale])
REFERENCES [dbo].[Locale] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Locale]
GO

/****** Object:  ForeignKey [FK_Account_Currency]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Currency] FOREIGN KEY([CurrencyFormat])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Currency]
GO

/****** Object:  ForeignKey [FK_Account_DateFormat]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_DateFormat] FOREIGN KEY([DateFormat])
REFERENCES [dbo].[DateFormat] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_DateFormat]
GO

/****** Object:  ForeignKey [FK_Account_TimeFormat]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_TimeFormat] FOREIGN KEY([TimeFormat])
REFERENCES [dbo].[TimeFormat] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_TimeFormat]
GO

/****** Object:  ForeignKey [FK_Account_Creator]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Creator]
GO

/****** Object:  ForeignKey [FK_Account_Parent]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Parent]
GO

/****** Object:  ForeignKey [FK_Account_Modifier]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Modifier]
GO

/****** Object:  ForeignKey [FK_Account_Owner]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Owner]
GO

/****** Object:  ForeignKey [FK_Account_BillingPlan]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_BillingPlan] FOREIGN KEY([SelectedBillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_BillingPlan]
GO

/****** Object:  ForeignKey [FK_Account_BillingAnniversaryDate]    ******/
/** ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_BillingAnniversaryDate] FOREIGN KEY([BillingAnniversaryDate])
REFERENCES [dbo].[BillingAnniversaryDate] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_BillingAnniversaryDate]
GO **/

/****** Object:  ForeignKey [FK_Account_ContractPeriod]    ******/
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_ContractPeriod] FOREIGN KEY([ContractPeriod])
REFERENCES [dbo].[ContractPeriod] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_ContractPeriod]
GO

/** ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_InvoivePeriod] FOREIGN KEY([InvoivePeriod])
REFERENCES [dbo].[InvoivePeriod] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_InvoivePeriod]
GO **/

ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_AccountStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[AccountStatus] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_AccountStatus]
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD CONSTRAINT [FK_Account_Theme] FOREIGN KEY([Theme])
REFERENCES [dbo].[Theme] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Theme]
GO

-- AccountHelpCentre

ALTER TABLE [dbo].[AccountHelpCentre]  WITH CHECK ADD  CONSTRAINT [FK_AccountHelpCentre_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountHelpCentre] CHECK CONSTRAINT [FK_AccountHelpCentre_Account]
GO

-- AccountSetting

ALTER TABLE [dbo].[AccountSetting]  WITH CHECK ADD CONSTRAINT [TBL_AccountSetting_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountSetting] CHECK CONSTRAINT [TBL_AccountSetting_Account]
GO

ALTER TABLE [dbo].[AccountSetting]  WITH CHECK ADD CONSTRAINT [TBL_AccountSetting_ValueDataType] FOREIGN KEY([ValueDataType])
REFERENCES [dbo].[ValueDataType] ([ID])
GO

ALTER TABLE [dbo].[AccountSetting] CHECK CONSTRAINT [TBL_AccountSetting_ValueDataType]
GO 

-- AccountPrePressFunction

ALTER TABLE [dbo].[AccountPrePressFunction]  WITH CHECK ADD CONSTRAINT [TBL_AccountPrePressFunction_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[AccountPrePressFunction] CHECK CONSTRAINT [TBL_AccountPrePressFunction_Account]
GO

ALTER TABLE [dbo].[AccountPrePressFunction]  WITH CHECK ADD CONSTRAINT [TBL_AccountPrePressFunction_PrePressFunction] FOREIGN KEY([PrePressFunction])
REFERENCES [dbo].[PrePressFunction] ([ID])
GO

ALTER TABLE [dbo].[AccountPrePressFunction] CHECK CONSTRAINT [TBL_AccountPrePressFunction_PrePressFunction]
GO

-- AccountBillingPlanChangeLog

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_Account]
GO

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_Creator]
GO

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_PreviousPlan] FOREIGN KEY([PreviousPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_PreviousPlan]
GO

ALTER TABLE [dbo].[AccountBillingPlanChangeLog]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingPlanChangeLog_CurrentPlan] FOREIGN KEY([CurrentPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingPlanChangeLog] CHECK CONSTRAINT [FK_AccountBillingPlanChangeLog_CurrentPlan]
GO

-- AccountBillingTransactionHistory

ALTER TABLE [dbo].[AccountBillingTransactionHistory]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingTransactionHistory_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory] CHECK CONSTRAINT [FK_AccountBillingTransactionHistory_Account]
GO

ALTER TABLE [dbo].[AccountBillingTransactionHistory]  WITH CHECK ADD CONSTRAINT [FK_AccountBillingTransactionHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[AccountBillingTransactionHistory] CHECK CONSTRAINT [FK_AccountBillingTransactionHistory_Creator]
GO

-- Company

/****** Object:  ForeignKey [FK_Company_Account]    ******/
ALTER TABLE [dbo].[Company]  WITH CHECK ADD CONSTRAINT [FK_Company_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Account]
GO

/****** Object:  ForeignKey [FK_Company_BillingCountry]    ******/
ALTER TABLE [dbo].[Company]  WITH CHECK ADD CONSTRAINT [FK_Company_Country] FOREIGN KEY([Country])
REFERENCES [dbo].[Country] ([ID])
GO
ALTER TABLE [dbo].[Company] CHECK CONSTRAINT [FK_Company_Country]
GO

-- MenuItemAccountTypeRole

/****** Object:  ForeignKey [FK_MenuItemAccountTypeRole_MenuItem]    ******/
ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_MenuItemAccountTypeRole_MenuItem] FOREIGN KEY([MenuItem])
REFERENCES [dbo].[MenuItem] ([ID])
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_MenuItem]
GO

--/****** Object:  ForeignKey [FK_MenuItemAccountTypeRole_Role]    ******/
--ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_MenuItemAccountTypeRole_Role] FOREIGN KEY([Role])
--REFERENCES [dbo].[Role] ([ID])
--GO
--ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_Role]
--GO

/****** Object:  ForeignKey [FK_MenuItemAccountTypeRole_AccountTypeRole]    ******/
ALTER TABLE [dbo].[MenuItemAccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_MenuItemAccountTypeRole_AccountTypeRole] FOREIGN KEY([AccountTypeRole])
REFERENCES [dbo].[AccountTypeRole] ([ID])
GO
ALTER TABLE [dbo].[MenuItemAccountTypeRole] CHECK CONSTRAINT [FK_MenuItemAccountTypeRole_AccountTypeRole]
GO

-- User

/****** Object:  ForeignKey [[FK_User_UserAccount]]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserAccount]
GO

/****** Object:  ForeignKey [FK_User_UserStatus]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_UserStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[UserStatus] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserStatus]
GO

/****** Object:  ForeignKey [FK_User_Creator]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Creator]
GO

/****** Object:  ForeignKey [FK_User_Modifier]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Modifier]
GO

/****** Object:  ForeignKey [FK_User_NotificationFrequency]    ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_NotificationFrequency] FOREIGN KEY([NotificationFrequency])
REFERENCES [dbo].[NotificationFrequency] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_NotificationFrequency]
GO

ALTER TABLE [dbo].[User]  WITH CHECK ADD CONSTRAINT [FK_User_Preset] FOREIGN KEY([Preset])
REFERENCES [dbo].[Preset] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Preset]
GO

ALTER TABLE [dbo].[User] ADD CONSTRAINT [DF_User_IncludeMyOwnActivity]  DEFAULT ((0)) FOR [IncludeMyOwnActivity]
GO

---- UserGroupUser

/****** Object:  ForeignKey [FK_UserGroupUser_UserGroup]    ******/
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD CONSTRAINT [FK_UserGroupUser_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_UserGroup]
GO

/****** Object:  ForeignKey [FK_UserGroupUser_User]    ******/
ALTER TABLE [dbo].[UserGroupUser]  WITH CHECK ADD CONSTRAINT [FK_UserGroupUser_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroupUser] CHECK CONSTRAINT [FK_UserGroupUser_User]
GO

-- UserHistory

/****** Object:  ForeignKey [FK_UserHistory_Creator]    ******/
ALTER TABLE [dbo].[UserHistory]  WITH CHECK ADD CONSTRAINT [FK_UserHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserHistory] CHECK CONSTRAINT [FK_UserHistory_Creator]
GO

/****** Object:  ForeignKey [FK_UserHistory_User]    ******/
ALTER TABLE [dbo].[UserHistory]  WITH CHECK ADD CONSTRAINT [FK_UserHistory_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserHistory] CHECK CONSTRAINT [FK_UserHistory_User]
GO

ALTER TABLE [dbo].[UserHistory] ADD CONSTRAINT [DF_UserHistory_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO

-- UserLogin

/****** Object:  ForeignKey [FK_UserLogin_User]    ******/
ALTER TABLE [dbo].[UserLogin]  WITH CHECK ADD CONSTRAINT [FK_UserLogin_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserLogin] CHECK CONSTRAINT [FK_UserLogin_User]
GO

ALTER TABLE [dbo].[UserLogin] ADD CONSTRAINT [DF_UserLogin_Success]  DEFAULT ((0)) FOR [Success]
GO

-- UserControllerActionAccess

--/****** Object:  ForeignKey [FK_UserControllerActionAccess_MenuItem]    ******/
--ALTER TABLE [dbo].[UserControllerActionAccess]  WITH CHECK ADD CONSTRAINT [FK_UserControllerActionAccess_MenuItem] FOREIGN KEY([MenuItem])
--REFERENCES [dbo].[MenuItem] ([ID])
--GO
--ALTER TABLE [dbo].[UserControllerActionAccess] CHECK CONSTRAINT [FK_UserControllerActionAccess_MenuItem]
--GO

--/****** Object:  ForeignKey [FK_UserControllerActionAccess_ControllerAction]    ******/
--ALTER TABLE [dbo].[UserControllerActionAccess]  WITH CHECK ADD CONSTRAINT [FK_UserControllerActionAccess_ControllerAction] FOREIGN KEY([ControllerAction])
--REFERENCES [dbo].[ControllerAction] ([ID])
--GO
--ALTER TABLE [dbo].[UserControllerActionAccess] CHECK CONSTRAINT [FK_UserControllerActionAccess_ControllerAction]
--GO

--/****** Object:  ForeignKey [FK_UserControllerActionAccess_User]    ******/
--ALTER TABLE [dbo].[UserControllerActionAccess]  WITH CHECK ADD CONSTRAINT [FK_UserControllerActionAccess_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[UserControllerActionAccess] CHECK CONSTRAINT [FK_UserControllerActionAccess_User]
--GO

-- ApprovalAnnotationTypeMarkup

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationMarkup_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup] CHECK CONSTRAINT [FK_ApprovalAnnotationMarkup_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationMarkup]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationMarkup_Shape] FOREIGN KEY([Shape])
REFERENCES [dbo].[Shape] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationMarkup] CHECK CONSTRAINT [FK_ApprovalAnnotationMarkup_Shape]
GO

-- Shape

ALTER TABLE [dbo].[Shape] ADD CONSTRAINT [DF_Shape_IsCustom]  DEFAULT ((0)) FOR [IsCustom]
GO

-- ApprovalAnnotationTextPrintData

--ALTER TABLE [dbo].[ApprovalAnnotationTextPrintData]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationTextPrintData_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
--REFERENCES [dbo].[ApprovalAnnotation] ([ID])
--GO
--ALTER TABLE [dbo].[ApprovalAnnotationTextPrintData] CHECK CONSTRAINT [FK_ApprovalAnnotationTextPrintData_ApprovalAnnotation]
--GO

-- UserRole

/****** Object:  ForeignKey [FK_UserRole_Role]    ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO

/****** Object:  ForeignKey [FK_UserRole_User]    ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD CONSTRAINT [FK_UserRole_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO

-- AccountTypeRole

/****** Object:  ForeignKey [FK_AccountTypeRole_Role]    ******/
ALTER TABLE [dbo].[AccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeRole_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[AccountTypeRole] CHECK CONSTRAINT [FK_AccountTypeRole_Role]
GO

/****** Object:  ForeignKey [FK_AccountTypeRole_AccountType]    ******/
ALTER TABLE [dbo].[AccountTypeRole]  WITH CHECK ADD CONSTRAINT [FK_AccountTypeRole_AccountType] FOREIGN KEY([AccountType])
REFERENCES [dbo].[AccountType] ([ID])
GO
ALTER TABLE [dbo].[AccountTypeRole] CHECK CONSTRAINT [FK_AccountTypeRole_AccountType]
GO

---- CollaboratorGroup

--/****** Object:  ForeignKey [FK_CollaboratorGroup_Account]    ******/
--ALTER TABLE [dbo].[CollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_CollaboratorGroup_Account] FOREIGN KEY([Account])
--REFERENCES [dbo].[Account] ([ID])
--GO
--ALTER TABLE [dbo].[CollaboratorGroup] CHECK CONSTRAINT [FK_CollaboratorGroup_Account]
--GO

---- CollaboratorGroupUser

--/****** Object:  ForeignKey [FK_CollaboratorGroupUser_CollaboratorGroup]    ******/
--ALTER TABLE [dbo].[CollaboratorGroupUser]  WITH CHECK ADD CONSTRAINT [FK_CollaboratorGroupUser_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
--REFERENCES [dbo].[CollaboratorGroup] ([ID])
--GO
--ALTER TABLE [dbo].[CollaboratorGroupUser] CHECK CONSTRAINT [FK_CollaboratorGroupUser_CollaboratorGroup]
--GO

--/****** Object:  ForeignKey [FK_CollaboratorGroupUser_User]    ******/
--ALTER TABLE [dbo].[CollaboratorGroupUser]  WITH CHECK ADD CONSTRAINT [FK_CollaboratorGroupUser_User] FOREIGN KEY([User])
--REFERENCES [dbo].[User] ([ID])
--GO
--ALTER TABLE [dbo].[CollaboratorGroupUser] CHECK CONSTRAINT [FK_CollaboratorGroupUser_User]
--GO

-- Job
ALTER TABLE [dbo].[Job]  WITH CHECK ADD CONSTRAINT [FK_Job_JobStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[JobStatus] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_JobStatus]
GO

ALTER TABLE [dbo].[Job]  WITH CHECK ADD CONSTRAINT [FK_Job_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Account]
GO

-- Approval

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Owner]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Creator]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Modifier]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Job] FOREIGN KEY([Job])
REFERENCES [dbo].[Job] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Job]
GO

--ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_Account] FOREIGN KEY([Account])
--REFERENCES [dbo].[Account] ([ID])
--GO
--ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_Account]
--GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD CONSTRAINT [FK_Approval_PrimaryDecisionMaker] FOREIGN KEY([PrimaryDecisionMaker])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_PrimaryDecisionMaker]
GO

ALTER TABLE [dbo].[Approval]  WITH CHECK ADD  CONSTRAINT [FK_Approval_FileType] FOREIGN KEY([FileType])
REFERENCES [dbo].[FileType] ([ID])
GO
ALTER TABLE [dbo].[Approval] CHECK CONSTRAINT [FK_Approval_FileType]
GO

ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_LockProofWhenAllDecisionsMade]  DEFAULT ((0)) FOR [LockProofWhenAllDecisionsMade]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_IsLocked]  DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_OnlyOneDecisionRequired]  DEFAULT ((0)) FOR [OnlyOneDecisionRequired]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_AllowDownloadOriginal]  DEFAULT ((0)) FOR [AllowDownloadOriginal]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_Size]  DEFAULT ((0)) FOR [Size]
GO
ALTER TABLE [dbo].[Approval] ADD CONSTRAINT [DF_Account_IsError]  DEFAULT ((0)) FOR [IsError]
GO

-- ApprovalPage

ALTER TABLE [dbo].[ApprovalPage]  WITH CHECK ADD CONSTRAINT [FK_ApprovalPage_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO

ALTER TABLE [dbo].[ApprovalPage] CHECK CONSTRAINT [FK_ApprovalPage_Approval]
GO

-- ApprovalUserViewInfo

ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD CONSTRAINT [FK_ApprovalUserViewInfo_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_Approval]
GO

ALTER TABLE [dbo].[ApprovalUserViewInfo]  WITH CHECK ADD CONSTRAINT [FK_ApprovalUserViewInfo_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalUserViewInfo] CHECK CONSTRAINT [FK_ApprovalUserViewInfo_User]
GO

-- ApprovalAnnotation

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Creator]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_ApprovalPage] FOREIGN KEY([Page])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ApprovalPage]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_HighlightType] FOREIGN KEY([HighlightType])
REFERENCES [dbo].[HighlightType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_HighlightType]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_ExternalCollaborator]
GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Status]    ******/
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[ApprovalAnnotationStatus] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Status]
GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Modifier]    ******/
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Modifier]
GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Type]    ******/
--ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Type] FOREIGN KEY([Type])
--REFERENCES [dbo].[ApprovalAnnotationType] ([ID])
--GO
--ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Type]
--GO

/****** Object:  ForeignKey [FK_ApprovalAnnotation_Parent]    ******/
ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_Parent]
GO

ALTER TABLE [dbo].[ApprovalAnnotation]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotation_CommentType] FOREIGN KEY([CommentType])
REFERENCES [dbo].[ApprovalCommentType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotation] CHECK CONSTRAINT [FK_ApprovalAnnotation_CommentType]
GO

ALTER TABLE [dbo].[ApprovalAnnotation] ADD CONSTRAINT [DF_ApprovalAnnotation_StartIndex]  DEFAULT ((0)) FOR [StartIndex]
GO
ALTER TABLE [dbo].[ApprovalAnnotation] ADD CONSTRAINT [DF_ApprovalAnnotation_EndIndex]  DEFAULT ((0)) FOR [EndIndex]
GO

-- ApprovalHistory

/* ALTER TABLE [dbo].[ApprovalHistory]  WITH CHECK ADD CONSTRAINT [FK_ApprovalHistory_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalHistory] CHECK CONSTRAINT [FK_ApprovalHistory_Approval]
GO

ALTER TABLE [dbo].[ApprovalHistory]  WITH CHECK ADD CONSTRAINT [FK_ApprovalHistory_Status] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalHistory] CHECK CONSTRAINT [FK_ApprovalHistory_Status]
GO

ALTER TABLE [dbo].[ApprovalHistory]  WITH CHECK ADD CONSTRAINT [FK_ApprovalHistory_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalHistory] CHECK CONSTRAINT [FK_ApprovalHistory_Creator]
GO */

-- ApprovalAnnotationAttachment

ALTER TABLE [dbo].[ApprovalAnnotationAttachment]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationAttachment_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationAttachment] CHECK CONSTRAINT [FK_ApprovalAnnotationAttachment_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationAttachment]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalAnnotationAttachment_FileType] FOREIGN KEY([FileType])
REFERENCES [dbo].[FileType] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationAttachment] CHECK CONSTRAINT [FK_ApprovalAnnotationAttachment_FileType]
GO

-- BillingPlan
ALTER TABLE [dbo].[BillingPlan]  WITH CHECK ADD CONSTRAINT [FK_BillingPlan_Type] FOREIGN KEY([Type])
REFERENCES [dbo].[BillingPlanType] ([ID])
GO
ALTER TABLE [dbo].[BillingPlan] CHECK CONSTRAINT [FK_BillingPlan_Type]
GO

ALTER TABLE [dbo].[BillingPlan] ADD CONSTRAINT [DF_BillingPlan_Price]  DEFAULT ((0.0)) FOR [Price]
GO
ALTER TABLE [dbo].[BillingPlan] ADD CONSTRAINT [DF_BillingPlan_IsFixed]  DEFAULT ((0)) FOR [IsFixed]
GO

-- BillingPlanType
ALTER TABLE [dbo].[BillingPlanType]  WITH CHECK ADD CONSTRAINT [FK_BillingPlanType_MediaService] FOREIGN KEY([MediaService])
REFERENCES [dbo].[MediaService] ([ID])
GO
ALTER TABLE [dbo].[BillingPlanType] CHECK CONSTRAINT [FK_BillingPlanType_MediaService]
GO

ALTER TABLE [dbo].[BillingPlanType] ADD CONSTRAINT [DF_BillingPlanType_EnablePrePressTools]  DEFAULT ((0)) FOR [EnablePrePressTools]
GO
ALTER TABLE [dbo].[BillingPlanType] ADD CONSTRAINT [DF_BillingPlanType_EnableMediaTools]  DEFAULT ((0)) FOR [EnableMediaTools]
GO

-- AttachedAccountBillingPlan

ALTER TABLE [dbo].[AttachedAccountBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_AttachedAccountBillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] CHECK CONSTRAINT [FK_AttachedAccountBillingPlan_Account]
GO

ALTER TABLE [dbo].[AttachedAccountBillingPlan] ADD CONSTRAINT [DF_AttachedAccountBillingPlan_IsAppliedCurrentRate]  DEFAULT ((0)) FOR [IsAppliedCurrentRate]
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] ADD CONSTRAINT [DF_AttachedAccountBillingPlan_IsModifiedProofPrice]  DEFAULT ((0)) FOR [IsModifiedProofPrice]
GO

-- AttachedAccountBillingPlan

ALTER TABLE [dbo].[AttachedAccountBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_AttachedAccountBillingPlan_BillingPlan] FOREIGN KEY([BillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AttachedAccountBillingPlan] CHECK CONSTRAINT [FK_AttachedAccountBillingPlan_BillingPlan]
GO

-- CurrencyRate

ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD CONSTRAINT [FK_CurrencyRate_Currency] FOREIGN KEY([Currency])
REFERENCES [dbo].[Currency] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Currency]
GO

ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD CONSTRAINT [FK_CurrencyRate_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Creator]
GO

ALTER TABLE [dbo].[CurrencyRate]  WITH CHECK ADD CONSTRAINT [FK_CurrencyRate_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[CurrencyRate] CHECK CONSTRAINT [FK_CurrencyRate_Modifier]
GO

-- ApprovalCollaborator

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaborator_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_Approval]
GO

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_Collaborator]
GO

ALTER TABLE [dbo].[ApprovalCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaborator_ApprovalCollaboratorRole] FOREIGN KEY([ApprovalCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaborator] CHECK CONSTRAINT [FK_ApprovalCollaborator_ApprovalCollaboratorRole]
GO

-- ApprovalAnnotationPrivateCollaborator

ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_ApprovalAnnotation] FOREIGN KEY([ApprovalAnnotation])
REFERENCES [dbo].[ApprovalAnnotation] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator] CHECK CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_ApprovalAnnotation]
GO

ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAnnotationPrivateCollaborator] CHECK CONSTRAINT [FK_ApprovalAnnotationPrivateCollaborator_Collaborator]
GO

-- ApprovalCollaboratorGroup

ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaboratorGroup_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_Approval]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorGroup] CHECK CONSTRAINT [FK_ApprovalCollaboratorGroup_CollaboratorGroup]
GO

-- StudioSetting

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_StudioSettingType] FOREIGN KEY([Type])
REFERENCES [dbo].[StudioSettingType] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_StudioSettingType]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_User]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_NotificationFrequency] FOREIGN KEY([NotificationFrequency])
REFERENCES [dbo].[NotificationFrequency] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_NotificationFrequency]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_WhenCommentMade] FOREIGN KEY([WhenCommentMade])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_WhenCommentMade]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_RepliesMyComment] FOREIGN KEY([RepliesMyComment])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_RepliesMyComment]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_NewVesionAdded] FOREIGN KEY([NewVesionAdded])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_NewVesionAdded]
GO

ALTER TABLE [dbo].[StudioSetting]  WITH CHECK ADD CONSTRAINT [FK_StudioSetting_FinalDecisionMade] FOREIGN KEY([FinalDecisionMade])
REFERENCES [dbo].[CommentEmailSendDecision] ([ID])
GO
ALTER TABLE [dbo].[StudioSetting] CHECK CONSTRAINT [FK_StudioSetting_FinalDecisionMade]
GO

ALTER TABLE [dbo].[StudioSetting] ADD CONSTRAINT [DF_StudioSetting_IncludeMyOwnActivity]  DEFAULT ((0)) FOR [IncludeMyOwnActivity]
GO
ALTER TABLE [dbo].[StudioSetting] ADD CONSTRAINT [DF_StudioSetting_EnableColorCorrection]  DEFAULT ((0)) FOR [EnableColorCorrection]
GO

-- Folder

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Creator]
GO

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Modifier]
GO

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Parent]
GO

ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_Account]
GO

ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [DF_Folder_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [DF_Folder_IsPrivate]  DEFAULT ((1)) FOR [IsPrivate]
GO

-- FolderCollaborator

ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_Folder]
GO

ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_Collaborator]
GO

--ALTER TABLE [dbo].[FolderCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_FolderCollaborator_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
--REFERENCES [dbo].[UserGroup] ([ID])
--GO
--ALTER TABLE [dbo].[FolderCollaborator] CHECK CONSTRAINT [FK_FolderCollaborator_CollaboratorGroup]
--GO

-- FolderCollaboratorGroup

ALTER TABLE [dbo].[FolderCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_FolderCollaboratorGroup_Approval] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup] CHECK CONSTRAINT [FK_FolderCollaboratorGroup_Approval]
GO

ALTER TABLE [dbo].[FolderCollaboratorGroup]  WITH CHECK ADD CONSTRAINT [FK_FolderCollaboratorGroup_CollaboratorGroup] FOREIGN KEY([CollaboratorGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[FolderCollaboratorGroup] CHECK CONSTRAINT [FK_FolderCollaboratorGroup_CollaboratorGroup]
GO

-- FolderApproval

ALTER TABLE [dbo].[FolderApproval]  WITH CHECK ADD  CONSTRAINT [FK_FolderApproval_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[FolderApproval] CHECK CONSTRAINT [FK_FolderApproval_Folder]
GO

ALTER TABLE [dbo].[FolderApproval]  WITH CHECK ADD  CONSTRAINT [FK_FolderApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[FolderApproval] CHECK CONSTRAINT [FK_FolderApproval_Approval]
GO

-- ApprovalCollaboratorDecision

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Approval]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Decision] FOREIGN KEY([Decision])
REFERENCES [dbo].[ApprovalDecision] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Decision]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalCollaboratorDecision_Collaborator] FOREIGN KEY([Collaborator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_Collaborator]
GO

ALTER TABLE [dbo].[ApprovalCollaboratorDecision]  WITH CHECK ADD CONSTRAINT [FK_ApprovalCollaboratorDecision_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[ApprovalCollaboratorDecision] CHECK CONSTRAINT [FK_ApprovalCollaboratorDecision_ExternalCollaborator]
GO

-- LogAction

ALTER TABLE [dbo].[LogAction]  WITH CHECK ADD  CONSTRAINT [FK_LogAction_LogModule] FOREIGN KEY([LogModule])
REFERENCES [dbo].[LogModule] ([ID])
GO
ALTER TABLE [dbo].[LogAction] CHECK CONSTRAINT [FK_LogAction_LogModule]
GO

-- LogApproval

ALTER TABLE [dbo].[LogApproval]  WITH CHECK ADD  CONSTRAINT [FK_LogApproval_LogAction] FOREIGN KEY([LogAction])
REFERENCES [dbo].[LogAction] ([ID])
GO
ALTER TABLE [dbo].[LogApproval] CHECK CONSTRAINT [FK_LogApproval_LogAction]
GO

-- UserGroup

ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Account]
GO

ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Creator]
GO

ALTER TABLE [dbo].[UserGroup]  WITH CHECK ADD  CONSTRAINT [FK_UserGroup_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserGroup] CHECK CONSTRAINT [FK_UserGroup_Modifier]
GO

-- EventType

ALTER TABLE [dbo].[EventType]  WITH CHECK ADD CONSTRAINT [FK_EventType_Group] FOREIGN KEY([EventGroup])
REFERENCES [dbo].[EventGroup] ([ID])
GO
ALTER TABLE [dbo].[EventType] CHECK CONSTRAINT [FK_EventType_Group]
GO

-- RolePresetEventType

ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD CONSTRAINT [FK_RolePresetEventType_Level] FOREIGN KEY([Preset])
REFERENCES [dbo].[Preset] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Level]
GO

ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD CONSTRAINT [FK_RolePresetEventType_Type] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Type]
GO

ALTER TABLE [dbo].[RolePresetEventType]  WITH CHECK ADD CONSTRAINT [FK_RolePresetEventType_Role] FOREIGN KEY([Role])
REFERENCES [dbo].[Role] ([ID])
GO
ALTER TABLE [dbo].[RolePresetEventType] CHECK CONSTRAINT [FK_RolePresetEventType_Role]
GO

-- UserCustomPresetEventTypeValue

ALTER TABLE [dbo].[UserCustomPresetEventTypeValue]  WITH CHECK ADD CONSTRAINT [FK_UserCustomPresetEventTypeValue_User] FOREIGN KEY([User])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] CHECK CONSTRAINT [FK_UserCustomPresetEventTypeValue_User]
GO

ALTER TABLE [dbo].[UserCustomPresetEventTypeValue]  WITH CHECK ADD CONSTRAINT [FK_UserCustomPresetEventTypeValue_RolePresetEventType] FOREIGN KEY([RolePresetEventType])
REFERENCES [dbo].[RolePresetEventType] ([ID])
GO
ALTER TABLE [dbo].[UserCustomPresetEventTypeValue] CHECK CONSTRAINT [FK_UserCustomPresetEventTypeValue_RolePresetEventType]
GO

--NotificationEmailQueue--

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_PrimaryRecipient] FOREIGN KEY([PrimaryRecipient])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_PrimaryRecipient]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_EventType] FOREIGN KEY([EventType])
REFERENCES [dbo].[EventType] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_EventType]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Creator]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_SecondaryRecipient] FOREIGN KEY([SecondaryRecipient])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_SecondaryRecipient]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_ExternalCollaborator]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_BillingPlan1] FOREIGN KEY([BillingPlan1])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_BillingPlan1]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_BillingPlan2] FOREIGN KEY([BillingPlan2])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_BillingPlan2]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Account]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Approval]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_Folder] FOREIGN KEY([Folder])
REFERENCES [dbo].[Folder] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_Folder]
GO

ALTER TABLE [dbo].[NotificationEmailQueue]  WITH CHECK ADD CONSTRAINT [FK_NotificationEmailQueue_UserGroup] FOREIGN KEY([UserGroup])
REFERENCES [dbo].[UserGroup] ([ID])
GO
ALTER TABLE [dbo].[NotificationEmailQueue] CHECK CONSTRAINT [FK_NotificationEmailQueue_UserGroup]
GO

ALTER TABLE [dbo].[NotificationEmailQueue] ADD CONSTRAINT [DF_NotificationEmailQueue_IsEmailSent]  DEFAULT ((0)) FOR [IsEmailSent]
GO

-- ChangedBillingPlan
ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_ChangedBillingPlan_Account] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_Account]
GO

ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_ChangedBillingPlan_BillingPlan] FOREIGN KEY([BillingPlan])
REFERENCES [dbo].[BillingPlan] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_BillingPlan]
GO

ALTER TABLE [dbo].[ChangedBillingPlan]  WITH CHECK ADD CONSTRAINT [FK_ChangedBillingPlan_AttachedAccountBillingPlan] FOREIGN KEY([AttachedAccountBillingPlan])
REFERENCES [dbo].[AttachedAccountBillingPlan] ([ID])
GO
ALTER TABLE [dbo].[ChangedBillingPlan] CHECK CONSTRAINT [FK_ChangedBillingPlan_AttachedAccountBillingPlan]
GO

-- AccountPricePlanChangedMessage
ALTER TABLE [dbo].[AccountPricePlanChangedMessage]  WITH CHECK ADD CONSTRAINT [FK_AccountPricePlanChangedMessage_ChangedBillingPlan] FOREIGN KEY([ChangedBillingPlan])
REFERENCES [dbo].[ChangedBillingPlan] ([ID])
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage] CHECK CONSTRAINT [FK_AccountPricePlanChangedMessage_ChangedBillingPlan]
GO

ALTER TABLE [dbo].[AccountPricePlanChangedMessage]  WITH CHECK ADD CONSTRAINT [FK_AccountPricePlanChangedMessage_ChildAccount] FOREIGN KEY([ChildAccount])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[AccountPricePlanChangedMessage] CHECK CONSTRAINT [FK_AccountPricePlanChangedMessage_ChildAccount]
GO

-- ImageFormat
ALTER TABLE [dbo].[ImageFormat]  WITH CHECK ADD CONSTRAINT [FK_ImageFormat_Parent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ImageFormat] ([ID])
GO
ALTER TABLE [dbo].[ImageFormat] CHECK CONSTRAINT [FK_ImageFormat_Parent]
GO

-- MediaService
ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_Smoothing] FOREIGN KEY([Smoothing])
REFERENCES [dbo].[Smoothing] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_Smoothing]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_Colorspace] FOREIGN KEY([Colorspace])
REFERENCES [dbo].[Colorspace] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_Colorspace]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_ImageFormat] FOREIGN KEY([ImageFormat])
REFERENCES [dbo].[ImageFormat] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_ImageFormat]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_PageBox] FOREIGN KEY([PageBox])
REFERENCES [dbo].[PageBox] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_PageBox]
GO

ALTER TABLE [dbo].[MediaService]  WITH CHECK ADD CONSTRAINT [FK_MediaService_TileImageFormat] FOREIGN KEY([TileImageFormat])
REFERENCES [dbo].[TileImageFormat] ([ID])
GO
ALTER TABLE [dbo].[MediaService] CHECK CONSTRAINT [FK_MediaService_TileImageFormat]
GO

ALTER TABLE [dbo].[MediaService] ADD CONSTRAINT [DF_MediaService_ApplySimulateOverprinting]  DEFAULT ((0)) FOR [ApplySimulateOverprinting]
GO

-- ApprovalSeparationPlate
ALTER TABLE [dbo].[ApprovalSeparationPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalSeparationPlate_Page] FOREIGN KEY([Page])
REFERENCES [dbo].[ApprovalPage] ([ID])
GO
ALTER TABLE [dbo].[ApprovalSeparationPlate] CHECK CONSTRAINT [FK_ApprovalSeparationPlate_Page]
GO
/*
-- VariationPlate
ALTER TABLE [dbo].[VariationPlate]  WITH CHECK ADD CONSTRAINT [FK_VariationPlate_Variation] FOREIGN KEY([Variation])
REFERENCES [dbo].[Variation] ([ID])
GO
ALTER TABLE [dbo].[VariationPlate] CHECK CONSTRAINT [FK_VariationPlate_Variation]
GO

-- ApprovalAdditionalPlate
ALTER TABLE [dbo].[ApprovalAdditionalPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalAdditionalPlate_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalAdditionalPlate] CHECK CONSTRAINT [FK_ApprovalAdditionalPlate_Approval]
GO

-- ApprovalVariationPlate
ALTER TABLE [dbo].[ApprovalVariationPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalVariationPlate_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[ApprovalVariationPlate] CHECK CONSTRAINT [FK_ApprovalVariationPlate_Approval]
GO

ALTER TABLE [dbo].[ApprovalVariationPlate]  WITH CHECK ADD CONSTRAINT [FK_ApprovalVariationPlate_VariationPlate] FOREIGN KEY([VariationPlate])
REFERENCES [dbo].[VariationPlate] ([ID])
GO
ALTER TABLE [dbo].[ApprovalVariationPlate] CHECK CONSTRAINT [FK_ApprovalVariationPlate_VariationPlate]
GO
*/

-- ExternalCollaborator
ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_UserAccount] FOREIGN KEY([Account])
REFERENCES [dbo].[Account] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_UserAccount]
GO

ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_Creator]
GO

ALTER TABLE [dbo].[ExternalCollaborator]  WITH CHECK ADD  CONSTRAINT [FK_ExternalCollaborator_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[ExternalCollaborator] CHECK CONSTRAINT [FK_ExternalCollaborator_Modifier]
GO

ALTER TABLE [dbo].[ExternalCollaborator] ADD DEFAULT ((1)) FOR [IsDeleted]
GO

-- SharedApproval
ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_Approval] FOREIGN KEY([Approval])
REFERENCES [dbo].[Approval] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_Approval]
GO

ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ExternalCollaborator] FOREIGN KEY([ExternalCollaborator])
REFERENCES [dbo].[ExternalCollaborator] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ExternalCollaborator]
GO

ALTER TABLE [dbo].[SharedApproval]  WITH CHECK ADD  CONSTRAINT [FK_SharedApproval_ApprovalCollaboratorRole] FOREIGN KEY([ApprovalCollaboratorRole])
REFERENCES [dbo].[ApprovalCollaboratorRole] ([ID])
GO
ALTER TABLE [dbo].[SharedApproval] CHECK CONSTRAINT [FK_SharedApproval_ApprovalCollaboratorRole]
GO

ALTER TABLE [dbo].[SharedApproval] ADD DEFAULT ((1)) FOR [IsSharedURL]
GO

ALTER TABLE [dbo].[SharedApproval] ADD DEFAULT ((1)) FOR [IsSharedDownloadURL]
GO

ALTER TABLE [dbo].[SharedApproval] ADD DEFAULT ((0)) FOR [IsExpired]
GO

-- GlobalSettings

ALTER TABLE [dbo].[GlobalSettings]  WITH CHECK ADD  CONSTRAINT [FK_GlobalSettings_NativeDataType] FOREIGN KEY([DataType])
REFERENCES [dbo].[NativeDataType] ([ID])
GO
ALTER TABLE [dbo].[GlobalSettings] CHECK CONSTRAINT [FK_GlobalSettings_NativeDataType]
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[UserMenuItemRoleView]    Script Date: 10/28/2010 21:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserMenuItemRoleView]
AS

SELECT	
		TOP 100 PERCENT
		atr.[AccountType],
		atr.[Role],
		mi.[ID] AS [MenuItem],
		mi.[Key],
		mi.[Name] AS [MenuName],	
		mi.[Title],
		ISNULL(mi.[Parent], 0) AS [Parent],
		mi.[Position],
		mi.[IsAdminAppOwned],
		mi.[IsAlignedLeft],
		mi.[IsSubNav],
		mi.[IsTopNav],
		mi.[IsVisible],		
		ca.[ID] AS [ControllerAction], 	
		ca.[Controller], 
		ca.[Action],
		ca.[Parameters]
FROM	
		[dbo].[AccountTypeRole] atr
		JOIN [dbo].[MenuItemAccountTypeRole] mirat
			ON atr.[ID] = mirat.[AccountTypeRole]
		JOIN [dbo].[MenuItem] mi
			ON mirat.[MenuItem] = mi.[ID]
		LEFT OUTER JOIN [dbo].[ControllerAction] ca 
			ON mi.ControllerAction = ca.ID
ORDER BY
		atr.[AccountType] ASC, atr.[Role] ASC, mi.[ID] ASC, [Parent], mi.[Position]
GO
--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[ReturnStringView] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnStringView] 
AS 
	SELECT  '' as RetVal

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_EncryptedPassword] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_EncryptedPassword
-- Description:  Select an encrypted password based on the supplied one
-- Date Created: Wednesday, 29 September 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE   PROCEDURE [dbo].[SPC_EncryptedPassword] (
	@P_Password varchar(64)
)
AS
	SELECT CONVERT(varchar(255),HashBytes('SHA1', @P_Password)) as RetVal

GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserLogin] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnUserLogin
-- Description:  Select item in User by login details
-- Date Created: Wednesday, 29 September 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnUserLogin] (
	@P_Account int,
	@P_Username varchar(255),
	@P_Password varchar(255)
)
AS
BEGIN
	SELECT	DISTINCT
			u.[ID],
			u.[Account],		
			u.[Status],  
			u.[Username],
			u.[Password],
			u.[GivenName],
			u.[FamilyName],
			u.[EmailAddress],
			u.[OfficeTelephoneNumber],
			u.[MobileTelephoneNumber],
			u.[HomeTelephoneNumber],
			u.[Guid],
			u.[PhotoPath],
			us.[ID] AS [StatusId],
			us.[Key],
			us.[Name],
			u.[Creator],
			u.[CreatedDate],
			u.[Modifier],
			u.[ModifiedDate],
			u.[DateLastLogin],
			u.[IncludeMyOwnActivity]
	FROM	[dbo].[User] u
			JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID 
	WHERE 	u.[Account] = @P_Account AND
			u.[Username] = @P_Username AND 
			u.[Password] = CONVERT(varchar(255), HashBytes('SHA1', @P_Password)) AND
			us.[Key] = 'A'
END
GO

--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**

/****** Object:  View [dbo].[UserAccountRoleView]    Script Date: 11/09/2011 17:21:28 ******/
/*****   IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UserAccountRoleView]'))
DROP VIEW [dbo].[UserAccountRoleView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserAccountRoleView]
AS

SELECT
	u.[ID], 
	u.GivenName,
	u.FamilyName,
	u.EmailAddress,
	u.Username,
	u.DateLastLogin,
	u.[Status],
	u.IsActive,
	r.Name AS RoleName,
	a.Name AS AccountName
FROM    
	[dbo].[User] u 
	JOIN [dbo].[UserRole] ur 
		ON u.[ID] = ur.[User]
	JOIN [dbo].[Role] r 
		ON ur.[Role] = r.[ID] 
	--JOIN UserCompany uc
	--	ON  uc.[User] = u.ID 
	JOIN Company c
		ON c.ID = uc.Company
	JOIN [dbo].[Account] a
		ON  a.[ID] = c.Account
GO *****/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  UserDefinedFunction [dbo].[GetAccessFolders]    Script Date: 10/26/2012 14:30:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAccessFolders]
(	
	-- Add the parameters for the function here
	@UserId int,
	@FolderId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT f.ID, f.Parent, f.Name, (0) AS Level, (CASE WHEN 
											(@UserId IN (	SELECT	fc.Collaborator 
															FROM	[dbo].FolderCollaborator fc
															WHERE	fc.Folder = f.ID 
					) ) THEN  '1' ELSE '0' end)  AS HasAccess 
		FROM	[dbo].[Folder] f
		WHERE	f.ID = @FolderId		 
		
		UNION ALL
		
		SELECT sf.ID, sf.Parent,sf.Name, (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@UserId IN (	SELECT fc.Collaborator 
					FROM	[dbo].FolderCollaborator fc
					WHERE	fc.Folder = sf.ID 
					 )) THEN  '1' ELSE '0' END)  AS HasAccess 
		FROM [dbo].[Folder] sf
			JOIN tableSet h
		ON h.ID	 = sf.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
    WHERE [Level] = 1
)

GO

/****** Object:  View [dbo].[AccountsView]    Script Date: 11/09/2011 17:21:28 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AccountsView]'))
DROP VIEW [dbo].[AccountsView]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[AccountsView]
AS
SELECT
	a.[ID], 
	a.Name,
	a.[Status] AS StatusId,
	s.[Name] AS StatusName,
	s.[Key],
	a.Domain,
	a.IsCustomDomainActive,
	a.CustomDomain,
	a.AccountType AS AccountTypeID,
	a.CreatedDate,
	a.IsTemporary,
	at.Name AS AccountTypeName,
	u.GivenName,
	u.FamilyName,
	co.ID AS Company,
	c.ID AS Country,
	c.ShortName AS CountryName,
	--a.IsDraft,
	(SELECT COUNT(ac.Parent) FROM [dbo].Account ac WHERE ac.Parent = a.ID) AS NoOfAccounts
FROM
	[dbo].[AccountType] at 
	JOIN [dbo].[Account] a
		ON at.[ID] = a.AccountType
	JOIN [dbo].[AccountStatus] s
		ON s.[ID] = a.[Status]
	LEFT OUTER JOIN [dbo].[User] u
		ON u.ID= a.[Owner]
	JOIN [dbo].[Company] co
		ON a.[ID] = co.[Account]
	JOIN [dbo].[Country] c
		ON c.[ID] = co.[Country]
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  View [dbo].[UserDetailsView]    Script Date: 11/09/2011 17:21:28 ******/
/*****  IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[UserDetailsView]'))
DROP VIEW [dbo].[UserDetailsView]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[UserDetailsView]
AS
SELECT 
	u.[ID], 
	u.Username,
	u.GivenName,
	u.FamilyName,
	u.EmailAddress,
	u.OfficeTelephoneNumber,
	u.IsActive,
	u.IsDeleted,
	u.[Guid],
	u.PhotoPath,
	u.IsSupplier,
	us.ID AS UserStatusID,
	us.Name AS UserStatusName,
	r.Name AS UserRoleName,
	uc.Company,
	uc.IsOwnerCompany
FROM    
	[dbo].[User] u
	JOIN [dbo].[UserStatus] us
		ON us.[ID] = u.[Status]
	JOIN [dbo].[UserRole] ur 
		ON u.[ID] = ur.[User]
	JOIN [dbo].[Role] r 
		ON r.[ID] = ur.[Role]
	JOIN UserCompany uc
		ON  uc.[User] = u.ID 
	JOIN Company c
		ON c.ID = uc.Company
	JOIN [dbo].[Account] a
		ON  a.[ID] = c.Account 
GO ***/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  View [dbo].[AccountUsersView]    Script Date: 11/22/2011 18:08:54 ******/
/****   IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AccountUsersView]'))
DROP VIEW [dbo].[AccountUsersView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[AccountUsersView]
AS
SELECT c.[ID] AS [CompanyID]
      ,c.[Account] AS [AccountID]
      ,c.[IsSupplier]
      ,c.[Name] AS [CompanyName]
      ,u.[ID] AS [UserID]
      ,u.[GivenName] + ' ' + u.[FamilyName] AS [FullName]
      ,us.[ID] AS [StatusID] 
      ,us.[Name] AS [StatusName]
      ,us.[Key]
      ,u.[IsActive] 
      ,u.[IsDeleted] 
      ,r.[ID] AS [RoleID]
      ,r.[Name] AS [RoleName]
  FROM [dbo].[Company] c
		JOIN [dbo].[UserCompany] uc
			ON c.[ID] = uc.[Company]
		JOIN [dbo].[User] u
			ON u.[ID] = uc.[User]
		JOIN [dbo].[UserStatus] us
			ON us.[ID] = u.[Status]
		JOIN [dbo].[UserRole] ur
			ON u.[ID] = ur.[User]
		JOIN [dbo].[Role] r
			ON r.[ID] = ur.[Role]
GO   ******/

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 04/18/2013 14:54:24 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnApprovalsPageView]'))
DROP VIEW [dbo].[ReturnApprovalsPageView]
GO

/****** Object:  View [dbo].[ReturnApprovalsPageView]    Script Date: 03/21/2013 15:22:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnApprovalsPageView] 
AS 
	SELECT  0 AS ID,
			0 AS Approval,
			0 AS Folder,
			'' AS Title,
			0 AS [Status],
			0 AS Job,
			0 AS [Version],
			0 AS Progress,
			GETDATE() AS CreatedDate,          
			GETDATE() AS ModifiedDate,
			GETDATE() AS Deadline,
			0 AS Creator,
			0 AS [Owner],
			0 AS PrimaryDecisionMaker,
			CONVERT(bit, 0) AS AllowDownloadOriginal,
			CONVERT(bit, 0) AS IsDeleted,
			0 AS MaxVersion,
			0 AS SubFoldersCount,
			0 AS ApprovalsCount, 
			'' AS Collaborators,
			0 AS Decision
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 04/18/2013 14:54:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetApprovalCollaborators]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetApprovalCollaborators]
GO

/****** Object:  UserDefinedFunction [dbo].[GetApprovalCollaborators]    Script Date: 03/25/2013 15:10:33 ******/

CREATE FUNCTION [dbo].[GetApprovalCollaborators] (@P_Approval int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT acg.CollaboratorGroup
		FROM Approval a 
				JOIN ApprovalCollaboratorGroup acg
					ON  a.ID = acg.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT ac.Collaborator, ac.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN ApprovalCollaborator ac
			 ON  a.ID = ac.Approval
		WHERE a.ID = @P_Approval ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
		
	-- Decision collaborators	
	DECLARE @DecisionMakerList nvarchar(MAX)
	SET @DecisionMakerList = ''

	SELECT @DecisionMakerList = @DecisionMakerList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT acd.Collaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.ExternalCollaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@DecisionMakerList) > 0)
			SET @DecisionMakerList = LEFT(@DecisionMakerList, LEN(@DecisionMakerList) - 1)			
			
	-- Annotated collaborators	
	DECLARE @AnnotatedList nvarchar(MAX)
	SET @AnnotatedList = ''

	SELECT @AnnotatedList = @AnnotatedList + CONVERT(nvarchar, temp.Creator) + ','
	FROM (
		SELECT DISTINCT aa.Creator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page
		WHERE a.ID = @P_Approval AND aa.ExternalCollaborator IS NULL) temp
		
		IF(LEN(@AnnotatedList) > 0)
			SET @AnnotatedList = LEFT(@AnnotatedList, LEN(@AnnotatedList) - 1)			
	
	-- Get the external collaborators
	DECLARE @ExCollaboratorList nvarchar(MAX)
	SET @ExCollaboratorList = ''

	SELECT @ExCollaboratorList = @ExCollaboratorList + CONVERT(nvarchar, temp.ExternalCollaborator) + '|' + CONVERT(nvarchar, temp.ApprovalCollaboratorRole) + ','
	FROM (
		SELECT DISTINCT sa.ExternalCollaborator, sa.ApprovalCollaboratorRole
		FROM Approval a 
				JOIN SharedApproval sa
			 ON  a.ID = sa.Approval
		WHERE a.ID = @P_Approval ) temp

	IF(LEN(@ExCollaboratorList) > 0)
		SET @ExCollaboratorList = LEFT(@ExCollaboratorList, LEN(@ExCollaboratorList) - 1)
	
	-- ExDecision collaborators	
	DECLARE @ExDecisionMakerList nvarchar(MAX)
	SET @ExDecisionMakerList = ''

	SELECT @ExDecisionMakerList = @ExDecisionMakerList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT acd.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalCollaboratorDecision acd
			 ON  a.ID = acd.Approval
		WHERE a.ID = @P_Approval AND acd.Collaborator IS NULL AND acd.CompletedDate IS NOT NULL ) temp
		
		IF(LEN(@ExDecisionMakerList) > 0)
			SET @ExDecisionMakerList = LEFT(@ExDecisionMakerList, LEN(@ExDecisionMakerList) - 1)
	
	-- ExAnnotated collaborators	
	DECLARE @ExAnnotatedList nvarchar(MAX)
	SET @ExAnnotatedList = ''

	SELECT @ExAnnotatedList = @ExAnnotatedList + CONVERT(nvarchar, temp.ExternalCollaborator) + ','
	FROM (
		SELECT DISTINCT aa.ExternalCollaborator
		FROM Approval a 
				JOIN ApprovalPage ap
					ON a.ID = ap.Approval 
				JOIN ApprovalAnnotation aa
					ON  ap.ID = aa.Page 
		WHERE a.ID = @P_Approval AND aa.Creator IS NULL) temp
		
		IF(LEN(@ExAnnotatedList) > 0)
			SET @ExAnnotatedList = LEFT(@ExAnnotatedList, LEN(@ExAnnotatedList) - 1)	
	
	RETURN	(@GroupList + '#' + @CollaboratorList  + '#' + @DecisionMakerList + '#' + @AnnotatedList + '#' + @ExCollaboratorList + '#' + @ExDecisionMakerList + '#' + @ExAnnotatedList ) 

END;

GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 04/18/2013 14:55:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetFolderCollaborators]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetFolderCollaborators]
GO

/****** Object:  UserDefinedFunction [dbo].[GetFolderCollaborators]    Script Date: 03/25/2013 15:11:35 ******/

CREATE FUNCTION [dbo].[GetFolderCollaborators] (@P_Folder int)
RETURNS nvarchar(MAX)
WITH EXECUTE AS CALLER
AS
BEGIN

-- Get the groups 
	DECLARE @GroupList nvarchar(MAX)
	SET @GroupList = ''

	SELECT @GroupList = @GroupList + CONVERT(nvarchar, temp.CollaboratorGroup) + ','
	FROM (
		SELECT DISTINCT fcg.CollaboratorGroup
		FROM Folder f 
				JOIN FolderCollaboratorGroup fcg
					ON  f.ID = fcg.Folder
		WHERE f.ID = @P_Folder ) temp

	IF(LEN(@GroupList) > 0)
		SET @GroupList = LEFT(@GroupList, LEN(@GroupList) - 1)	

	-- Get the collaborators
	DECLARE @CollaboratorList nvarchar(MAX)
	SET @CollaboratorList = ''

	SELECT @CollaboratorList = @CollaboratorList + CONVERT(nvarchar, temp.Collaborator) + ','
	FROM (
		SELECT DISTINCT fc.Collaborator
		FROM Folder f 
				JOIN FolderCollaborator fc
			 ON  f.ID = fc.Folder
		WHERE f.ID = @P_Folder ) temp
		
	IF(LEN(@CollaboratorList) > 0)
		SET @CollaboratorList = LEFT(@CollaboratorList, LEN(@CollaboratorList) - 1)		
			
	RETURN	(@GroupList + '#' + @CollaboratorList ) 

END;

GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 07/01/2013 15:29:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnFoldersApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnFoldersApprovalsPageInfo]    Script Date: 07/01/2013 15:29:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnFoldersApprovalsPageInfo
-- Description:  This SP returns folders and approvals
-- Date Created: Monday, 30 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnFoldersApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_FolderId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN result.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN result.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN result.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN result.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN result.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN result.[Status]
					END DESC					
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.[CreatedDate],
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,	
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]	 						
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC
						
				UNION
				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
                				INNER JOIN Approval av
                					ON av.ID = fa.Approval
               				WHERE fa.Folder = f.ID AND av.IsDeleted = 0
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )												
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId))
				ORDER BY 
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN Deadline
					--END ASC,
					--CASE						
					--	WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN Deadline
					--END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN f.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN f.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN f.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN f.ModifiedDate
					END DESC--,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN f.[Status]
					--END ASC,
					--CASE
					--	WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN f.[Status]
					--END DESC		 
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]	 						
						LEFT OUTER JOIN FolderApproval fa
							ON fa.Approval = a.ID		
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 0
						AND js.[Key] != 'ARC'
						AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
						AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )						
						AND	(	SELECT MAX([Version])
								FROM Approval av 
								WHERE av.Job = a.Job
									AND av.IsDeleted = 0
									AND (	@P_User IN (	SELECT ac.Collaborator 
														FROM ApprovalCollaborator ac 
														WHERE ac.Approval = av.ID
													)	
										)
							) = a.[Version]
						AND	@P_FolderId = fa.Folder 
					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND (@P_Status = 0)
						AND f.IsDeleted = 0
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )												
						AND f.ID IN ( SELECT ID FROM GetAccessFolders (@P_User, @P_FolderId)) 
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END 
	 
END


GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalsPageInfo]    Script Date: 04/26/2012 23:24:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalsPageInfo
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_TopLinkId int = 0,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC					
				)) AS ID, 
				a.ID AS Approval,	
				0 AS Folder,
				j.Title,							 
				j.[Status] AS [Status],
				j.ID AS Job,
				a.[Version],
				(SELECT CASE 
						WHEN a.IsError = 1 THEN -1
						WHEN ISNULL((SELECT SUM(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID), 0 ) = 0 THEN '0'
						WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 100 THEN '100'
						WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
								WHERE ap.Approval = a.ID ) = 0 THEN '0'
						ELSE '50'
					END) AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				(SELECT MAX([Version])
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,				
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
		FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )					
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)								
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)										
									)
								)		
					) = a.[Version]
	)	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset

	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT DISTINCT	a.ID
			FROM	Job j
				JOIN Approval a 
					ON a.Job = j.ID	
				JOIN JobStatus js
					ON js.ID = j.[Status]	
		WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)				
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )					
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job
							AND av.IsDeleted = 0
							AND (
									(@P_TopLinkId = 1 
										AND ( 
											@P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)								
											)
									)
								OR 
									(@P_TopLinkId = 2 
										AND av.[Owner] = @P_User
									)
								OR 
									(@P_TopLinkId = 3 
										AND av.[Owner] != @P_User
										AND @P_User IN (	SELECT ac.Collaborator 
															FROM ApprovalCollaborator ac 
															WHERE ac.Approval = av.ID)										
									)
								)	
					) = a.[Version]
		)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END

END



GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	--**--**--**--**--**--***--**--**--**--**--


/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]    Script Date: 06/12/2012 11:42:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnRecentViewedApprovalsPageInfo
-- Description:  This Sp returns recenr viwed approvals
-- Date Created: Monday, 12 June 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnRecentViewedApprovalsPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,
	@P_SearchText nvarchar(100) = '',
	@P_Status int = 0,
	@P_RecCount int OUTPUT
)
AS
BEGIN
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
			SELECT 	TOP (@P_Set * @P_MaxRows)
					CONVERT(int, ROW_NUMBER() OVER(
					ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN js.[Key]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN js.[Key]
					END DESC					
				)) AS ID, 
					a.ID AS Approval,	
					0 AS Folder,
					j.Title,							 
					j.[Status] AS [Status],	
					j.ID AS Job,
					a.[Version],
					100 AS Progress,
					a.CreatedDate,
					a.ModifiedDate,
					ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
					a.Creator,
					a.[Owner],
					ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
					a.AllowDownloadOriginal,
					a.IsDeleted,
					(SELECT MAX([Version])
					 FROM Approval av
					 WHERE av.Job = j.ID AND av.IsDeleted = 0) AS MaxVersion,						
					0 AS SubFoldersCount,
					(SELECT COUNT(av.ID)
					FROM Approval av
					WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
					(SELECT dbo.GetApprovalCollaborators(a.ID)) AS Collaborators,
					ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	  
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM (
			SELECT 	a.ID
			FROM	ApprovalUserViewInfo auvi
					INNER JOIN Approval a	
						ON a.ID =  auvi.Approval
					INNER JOIN Job j
						ON j.ID = a.Job
					INNER JOIN JobStatus js
						ON js.ID = j.[Status]		
			WHERE	j.Account = @P_Account					
					AND a.IsDeleted = 0
					AND js.[Key] != 'ARC'
					AND (
								(@P_Status = 0) OR
								((@P_Status = 1) AND (js.[Key] = 'NEW')) OR
								((@P_Status = 2) AND (js.[Key] = 'INP')) OR
								((@P_Status = 3) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 4) AND (js.[Key] = 'COM')) OR
								((@P_Status = 5) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM'))) OR
								((@P_Status = 6) AND ((js.[Key] = 'COM') OR (js.[Key] = 'INP'))) OR
								((@P_Status = 7) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM'))) OR
								--((@P_Status = 8) AND (js.[Key] = 'ARC')) OR
								((@P_Status = 9) AND ((js.[Key] = 'NEW') )) OR
								((@P_Status = 10) AND ((js.[Key] = 'INP') )) OR
								((@P_Status = 11) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') )) OR
								((@P_Status = 12) AND ((js.[Key] = 'COM') )) OR
								((@P_Status = 13) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 14) AND ((js.[Key] = 'INP') OR (js.[Key] = 'COM') )) OR
								((@P_Status = 15) AND ((js.[Key] = 'NEW') OR (js.[Key] = 'INP') OR (js.[Key] = 'COM') )) 
							)					
					AND (	@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND		auvi.[User] = @P_User
					AND (	SELECT MAX([ViewedDate]) 
							FROM ApprovalUserViewInfo vuvi
								INNER JOIN Approval av
									ON  av.ID = vuvi.[Approval]
								INNER JOIN Job vj
									ON vj.ID = av.Job	
							WHERE  av.Job = a.Job
								AND av.IsDeleted = 0
						) = auvi.[ViewedDate]
					AND @P_User IN (	SELECT ac.Collaborator 
										FROM ApprovalCollaborator ac 
										WHERE ac.Approval = a.ID
									)	
					)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	  
END


GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_ReturnRecycleBinPageInfo]    Script Date: 06/12/2012 11:42:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnRecycleBinPageInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnRecycleBinPageInfo
-- Description:  This SP returns deleted folders and approvals
-- Date Created: Monday, 30 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnRecycleBinPageInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 4 - ModifiedDate,
								-- 5 - FileType
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT	
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 0) THEN result.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 4 AND @P_Order = 1) THEN result.ModifiedDate
					END DESC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 0) THEN result.Approval
					END ASC,
					CASE						
						WHEN (@P_SearchField = 5 AND @P_Order = 1) THEN result.Approval
					END DESC
				)) AS ID, 
			result.Approval, 
			result.Folder,
			result.Title,
			result.[Status],
			result.[Job],
			result.[Version],
			result.[Progress],
			result.CreatedDate,
			result.ModifiedDate,
			result.Deadline,
			result.Creator,
			result.[Owner],
			result.PrimaryDecisionMaker,
			result.AllowDownloadOriginal,
			result.IsDeleted,
			result.MaxVersion,			
			result.SubFoldersCount,	
			result.ApprovalsCount,
			result.Collaborators,
			result.Decision
		FROM (				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						a.ID AS Approval,	
						0 AS Folder,
						j.Title,							 
						j.[Status] AS [Status],
						j.ID AS Job,
						a.[Version],
						(SELECT CASE 
								WHEN a.IsError = 1 THEN -1
								WHEN (SELECT MIN(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 100 THEN '100'
								WHEN (SELECT MAX(Progress) FROM ApprovalPage ap
										WHERE ap.Approval = a.ID ) = 0 THEN '0'
								ELSE '50'
							END) AS Progress,
						a.CreatedDate,
						a.ModifiedDate,
						ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
						a.Creator,
						a.[Owner],
						ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
						a.AllowDownloadOriginal,
						a.IsDeleted,
						(SELECT MAX([Version])
						 FROM Approval av
						 WHERE av.Job = j.ID) AS MaxVersion,						
						0 AS SubFoldersCount,
						(SELECT COUNT(av.ID)
						FROM Approval av
						WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
						'######' AS Collaborators,
						ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 												
				UNION				
				SELECT 	TOP (@P_Set * @P_MaxRows)
						0 AS Approval, 
						f.ID AS Folder,
						f.Name AS Title,
						0 AS [Status],
						0 AS Job,
						0 AS [Version],
						0 AS Progress,						
						f.CreatedDate,
						f.ModifiedDate,
						GetDate() AS Deadline,
						f.Creator,
						0 AS [Owner],
						0 AS PrimaryDecisionMaker,
						'false' AS AllowDownloadOriginal,
						f.IsDeleted,
						0 AS MaxVersion,
						(	SELECT COUNT(f1.ID)
                			FROM Folder f1
               				WHERE f1.Parent = f.ID
						) AS SubFoldersCount,
						(	SELECT COUNT(fa.Approval)
                			FROM FolderApproval fa
               				WHERE fa.Folder = f.ID
						) AS ApprovalsCount,				
						(SELECT dbo.GetFolderCollaborators(f.ID)) AS Collaborators,
						0 AS Decision
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) result		
			--END
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.Approval)
		FROM (				
				SELECT 	a.ID AS Approval
				FROM	Job j
						INNER JOIN Approval a 
							ON a.Job = j.ID
						INNER JOIN JobStatus js
							ON js.ID = j.[Status]		 							
				WHERE	j.Account = @P_Account
						AND a.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
						AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
						AND (
								(@P_User IN (	SELECT ac.Collaborator 
											FROM ApprovalCollaborator ac 
											WHERE ac.Approval = a.ID)
								) 							
							) 					
				UNION
				
				SELECT 	f.ID AS Approval
				FROM	Folder f							
				WHERE	f.Account = @P_Account
						AND f.IsDeleted = 1
						AND (@P_SearchText IS NULL OR @P_SearchText = '' OR f.Name LIKE '%' + @P_SearchText + '%' )		
						AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)				
						AND (	@P_User IN (	SELECT fc.Collaborator 
											FROM FolderCollaborator fc 
											WHERE fc.Folder = f.ID
											) 							
							)
			) a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	
END


GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_ReturnArchivedApprovalInfo]    Script Date: 06/12/2012 11:42:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnArchivedApprovalInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnArchivedApprovalInfo
-- Description:  This SP returns archived folders and approvals
-- Date Created: Friday, 21 September 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnArchivedApprovalInfo] (
	@P_Account int,
	@P_User int,
	@P_MaxRows int = 100,	
	@P_Set int = 1,
	@P_SearchField int = 0, 	-- 0 - Deadline,
								-- 1 - CreatedDate,
								-- 2 - ModifiedDate,
								-- 3 - Status
	@P_Order bit = 0,							
	@P_SearchText nvarchar(100) = '',
	@P_RecCount int OUTPUT
)
AS
BEGIN
		
	-- Get the approvals	
	SET NOCOUNT ON
	DECLARE @StartOffset int;
	SET @StartOffset = (@P_Set - 1) * @P_MaxRows;

	-- Get the records to the CTE
	WITH Approvals AS
	(
		SELECT
				DISTINCT	TOP (@P_Set * @P_MaxRows)
				CONVERT(int, ROW_NUMBER() OVER(
				ORDER BY 
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 0) THEN a.Deadline
					END ASC,
					CASE						
						WHEN (@P_SearchField = 0 AND @P_Order = 1) THEN a.Deadline
					END DESC,
					CASE
						WHEN (@P_SearchField = 1 AND @P_Order = 0) THEN a.CreatedDate
					END ASC,
					CASE						
						WHEN (@P_SearchField = 1 AND @P_Order = 1) THEN a.CreatedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 0) THEN a.ModifiedDate
					END ASC,
					CASE
						WHEN (@P_SearchField = 2 AND @P_Order = 1) THEN a.ModifiedDate
					END DESC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 0) THEN j.[Status]
					END ASC,
					CASE
						WHEN (@P_SearchField = 3 AND @P_Order = 1) THEN j.[Status]
					END DESC					
				)) AS ID, 			
				a.ID AS Approval,	
				0 AS Folder,
				j.Title,							 
				j.[Status] AS [Status],
				j.ID AS Job,	
				a.[Version],
				100 AS Progress,
				a.CreatedDate,
				a.ModifiedDate,
				ISNULL(a.Deadline, a.CreatedDate) AS Deadline,
				a.Creator,
				a.[Owner],
				ISNULL(a.PrimaryDecisionMaker, 0) AS PrimaryDecisionMaker,
				a.AllowDownloadOriginal,
				a.IsDeleted,
				0 AS MaxVersion,				
				0 AS SubFoldersCount,
				(SELECT COUNT(av.ID)
				 FROM Approval av
				 WHERE av.Job = j.ID AND av.[Owner] = @P_User AND av.IsDeleted = 0) AS ApprovalsCount,				
				'######' AS Collaborators,
				ISNULL((SELECT CASE WHEN (ISNULL(a.PrimaryDecisionMaker, 0) = 0) 
						THEN
							(SELECT TOP 1 appcd.Decision
								FROM (	SELECT acd.Decision, MIN([Priority]) AS [Priority] FROM ApprovalCollaboratorDecision acd
										INNER JOIN ApprovalDecision ad
										ON ad.ID = acd.Decision	
										WHERE Approval = a.ID
										GROUP BY acd.Decision 
									) appcd
							)		
						ELSE 
							(SELECT appcd.Decision
								FROM (SELECT acd.Decision FROM ApprovalCollaboratorDecision acd
									WHERE Approval = a.ID AND acd.Collaborator = a.PrimaryDecisionMaker) appcd
							)
						END	
				), 0 ) AS Decision
		FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
		WHERE	j.Account = @P_Account
				AND a.IsDeleted = 0	
				AND js.[Key] = 'ARC'									
				AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
				AND
				(	SELECT MAX([Version])
					FROM Approval av 
					WHERE 
						av.Job = a.Job								
						AND av.IsDeleted = 0
						AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
				) = a.[Version]	
	)
	
	-- Return the total effected records
	SELECT * FROM Approvals WHERE ID > @StartOffset
	 
	---- Send the total
	IF @P_Set = 1
	BEGIN	
		SELECT @P_RecCount = COUNT (a.ID)
		FROM	(
			SELECT 	a.ID
			FROM	Job j
				INNER JOIN Approval a 
					ON a.Job = j.ID
				INNER JOIN JobStatus js
					ON js.ID = j.[Status]
			WHERE	j.Account = @P_Account
					AND a.IsDeleted = 0	
					AND js.[Key] = 'ARC'									
					AND (@P_SearchText IS NULL OR @P_SearchText = '' OR j.Title LIKE '%' + @P_SearchText + '%' )
					AND
					(	SELECT MAX([Version])
						FROM Approval av 
						WHERE 
							av.Job = a.Job								
							AND av.IsDeleted = 0
							AND @P_User IN (SELECT ac.Collaborator FROM ApprovalCollaborator ac WHERE ac.Approval = av.ID)
					) = a.[Version]		
				)a
	END
	ELSE
	BEGIN
		SET @P_RecCount = 0
	END
	 
END


GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_DeleteApproval]    Script Date: 10/15/2012 16:35:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_DeleteApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_DeleteApproval]
GO

/****** Object:  StoredProcedure [dbo].[SPC_DeleteApproval]    Script Date: 10/15/2012 14:29:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteApproval
-- Description:  This SP deletes the Approval and it's files
-- Date Created: Monday, 15 October 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_DeleteApproval] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''
	DECLARE @Job_ID int;

	BEGIN TRY
	-- Delete from ApprovalAnnotationPrivateCollaborator 
		DELETE [dbo].[ApprovalAnnotationPrivateCollaborator] 
		FROM	[dbo].[ApprovalAnnotationPrivateCollaborator] apc
				JOIN [dbo].[ApprovalAnnotation] aa
					ON apc.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationMarkup 
		DELETE [dbo].[ApprovalAnnotationMarkup] 
		FROM	[dbo].[ApprovalAnnotationMarkup] am
				JOIN [dbo].[ApprovalAnnotation] aa
					ON am.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from ApprovalAnnotationAttachment 
		DELETE [dbo].[ApprovalAnnotationAttachment] 
		FROM	[dbo].[ApprovalAnnotationAttachment] at
				JOIN [dbo].[ApprovalAnnotation] aa
					ON at.ApprovalAnnotation = aa.ID
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalAnnotation 
		DELETE [dbo].[ApprovalAnnotation] 
		FROM	[dbo].[ApprovalAnnotation] aa
				JOIN [dbo].[ApprovalPage] ap
					ON aa.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
	    
		-- Delete from ApprovalCollaboratorDecision
		DELETE FROM [dbo].[ApprovalCollaboratorDecision]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaboratorGroup
		DELETE FROM [dbo].[ApprovalCollaboratorGroup]				
			WHERE Approval = @P_Id	
		
		-- Delete from ApprovalCollaborator
		DELETE FROM [dbo].[ApprovalCollaborator] 		
				WHERE Approval = @P_Id
		
		-- Delete from SharedApproval
		DELETE FROM [dbo].[SharedApproval] 
				WHERE Approval = @P_Id
		
		-- Delete from ApprovalSeparationPlate 
		DELETE [dbo].[ApprovalSeparationPlate] 
		FROM	[dbo].[ApprovalSeparationPlate] asp
				JOIN [dbo].[ApprovalPage] ap
					ON asp.Page = ap.ID
				JOIN [dbo].[Approval] a
					ON a.ID = ap.Approval 
		WHERE a.ID = @P_Id
		
		-- Delete from approval pages
		DELETE FROM [dbo].[ApprovalPage] 		 
			WHERE Approval = @P_Id
		
		-- Delete from FolderApproval
		DELETE FROM [dbo].[FolderApproval]
		WHERE Approval = @P_Id
		
		--Delete from ApprovalUserViewInfo
		DELETE FROM [dbo].[ApprovalUserViewInfo]
		WHERE Approval = @P_Id
		
		-- Delete from NotificationEmailQueue
		DELETE FROM [dbo].[NotificationEmailQueue]
		WHERE Approval = @P_Id
		
		-- Set Job ID
		SET @Job_ID = (SELECT Job From [dbo].[Approval] WHERE ID = @P_Id)
		
		-- Delete from approval
		DELETE FROM [dbo].[Approval]
		WHERE ID = @P_Id
	    
	    -- Delete Job, if no approvals left
	    IF ( (SELECT COUNT(ID) FROM [dbo].[Approval] WHERE Job = @Job_ID) = 0)
			BEGIN
				DELETE FROM [dbo].[Job]
				WHERE ID = @Job_ID
			END
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END


GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

/****** Object:  StoredProcedure [dbo].[SPC_DeleteFolder]    Script Date: 10/15/2012 16:35:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_DeleteFolder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_DeleteFolder]
GO

/****** Object:  StoredProcedure [dbo].[SPC_DeleteFolder]    Script Date: 10/15/2012 14:29:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_DeleteFolder
-- Description:  This SP deletes the Folder and it's files
-- Date Created: Monday, 15 October 2012
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[SPC_DeleteFolder] (
	@P_Id int
)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @P_Success nvarchar(512) = ''

	BEGIN TRY
	
		-- Delete from FolderCollaborator 
		DELETE FROM	[dbo].[FolderCollaborator]
				WHERE Folder = @P_Id
		
		-- Delete from FolderCollaboratorGroup 
		DELETE FROM	[dbo].[FolderCollaboratorGroup] 
				WHERE Folder = @P_Id
		
		-- Delete from NotificationEmailQueue 
		DELETE FROM	[dbo].[NotificationEmailQueue] 
				WHERE Folder = @P_Id				
	
		DECLARE @ApprovalID INT
		DECLARE @getApprovalID CURSOR
		SET @getApprovalID = CURSOR FOR SELECT Approval FROM [dbo].[FolderApproval] WHERE Folder = @P_Id
		OPEN @getApprovalID
		
		FETCH NEXT
		FROM @getApprovalID INTO @ApprovalID
			WHILE @@FETCH_STATUS = 0
			BEGIN
			EXECUTE [dbo].[SPC_DeleteApproval] @ApprovalID
			
			FETCH NEXT
			FROM @getApprovalID INTO @ApprovalID
			END
		CLOSE @getApprovalID
		DEALLOCATE @getApprovalID	
 
	    -- Delete from Folder
		DELETE FROM [dbo].[Folder]
		WHERE ID = @P_Id	    
	    
		SET @P_Success = ''
    
    END TRY
	BEGIN CATCH
		SET @P_Success = ERROR_MESSAGE()
	END CATCH;
	
	SELECT @P_Success AS RetVal
	  
END



GO

/****** Object:  View [dbo].[ReturnApprovalCountsView]    Script Date: 03/14/2013 13:11:47 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnApprovalCountsView]'))
DROP VIEW [dbo].[ReturnApprovalCountsView]
GO

/****** Object:  View [dbo].[ReturnApprovalCountsView]    Script Date: 03/14/2013 13:07:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnApprovalCountsView]
AS
	 SELECT	0 AS AllCount, 
			0 AS OwnedByMeCount, 
			0 AS SharedCount, 
			0 AS RecentlyViewedCount, 
			0 AS ArchivedCount,
			0 AS RecycleCount   

GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 03/14/2013 13:29:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnApprovalCounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnApprovalCounts]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnApprovalCounts]    Script Date: 03/14/2013 12:12:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnApprovalCounts
-- Description:  This Sp returns multiple result sets that needed for Approval page
-- Date Created: Monday, 23 April 2010
-- Created By:   Siwanka De Silva
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnApprovalCounts] (
	@P_Account int,
	@P_User int
)
AS
BEGIN
	-- Get the approval counts	
	SET NOCOUNT ON
	
	DECLARE @AllCount int;
	DECLARE @OwnedByMeCount int;
	DECLARE @SharedCount int;
	DECLARE @RecentlyViewedCount int;
	DECLARE @ArchivedCount int;
	DECLARE @RecycleCount int;
	
	-- Get All Approval count
	SET @AllCount = (	SELECT COUNT(a.ID)
						FROM	Job j
								INNER JOIN Approval a 
									ON a.Job = j.ID
								INNER JOIN JobStatus js
									ON js.ID = j.[Status]		
						WHERE	j.Account = @P_Account
							AND a.IsDeleted = 0
							AND js.[Key] != 'ARC'
							AND	(	SELECT MAX([Version])
									FROM Approval av 
									WHERE	av.Job = a.Job
											AND av.IsDeleted = 0
											AND @P_User IN (	SELECT ac.Collaborator 
																FROM ApprovalCollaborator ac 
																WHERE ac.Approval = av.ID
															)
								) = a.[Version] )
						
	-- Get Owned by me count		
	SET @OwnedByMeCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] = @P_User
									) = a.[Version]	)
			
	-- Get Shared with me count
	SET @SharedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] != 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND av.[Owner] != @P_User
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version]	)
			
	-- Get Recently viewed count
	SET @RecentlyViewedCount = (	SELECT COUNT(DISTINCT j.ID)
									FROM	ApprovalUserViewInfo auvi
											INNER JOIN Approval a
													ON a.ID = auvi.Approval 
											INNER JOIN Job j
													ON j.ID = a.Job 
											INNER JOIN JobStatus js
													ON js.ID = j.[Status]		
									WHERE	auvi.[User] =  @P_User
										AND j.Account = @P_Account
										AND a.IsDeleted = 0
										AND js.[Key] != 'ARC'
										AND @P_User IN (	SELECT ac.Collaborator 
																			FROM ApprovalCollaborator ac 
																			WHERE ac.Approval = a.ID
																		)										
										)
			
	-- Get Archived count
	SET @ArchivedCount = (	SELECT COUNT(a.ID)
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]		
							WHERE	j.Account = @P_Account
								AND a.IsDeleted = 0
								AND js.[Key] = 'ARC'
								AND	(	SELECT MAX([Version])
										FROM Approval av 
										WHERE	av.Job = a.Job
												AND av.IsDeleted = 0
												AND @P_User IN (	SELECT ac.Collaborator 
																	FROM ApprovalCollaborator ac 
																	WHERE ac.Approval = av.ID
																)															
									) = a.[Version])
			
	-- Get Recycle bin count
	SET @RecycleCount = (	SELECT COUNT(a.ID) 
							FROM	Job j
									INNER JOIN Approval a 
										ON a.Job = j.ID
									INNER JOIN JobStatus js
										ON js.ID = j.[Status]
							WHERE j.Account = @P_Account
								AND a.IsDeleted = 1
								AND ((SELECT COUNT(f.ID) FROM Folder f INNER JOIN FolderApproval fa ON fa.Folder = f.ID WHERE fa.Approval = a.ID AND f.IsDeleted = 1) = 0)
								AND (@P_User IN (	SELECT ac.Collaborator 
													FROM ApprovalCollaborator ac 
													WHERE ac.Approval = a.ID
												)			
									)
						) 
						+
						(	SELECT COUNT(f.ID)
							FROM	Folder f 
							WHERE	f.Account = @P_Account
									AND f.IsDeleted = 1
									AND ((SELECT COUNT(pf.ID) FROM Folder pf WHERE pf.ID = f.Parent AND pf.IsDeleted = 1) = 0)
									AND (@P_User IN (	SELECT fc.Collaborator 
														FROM FolderCollaborator fc 
														WHERE fc.Folder = f.ID
													)
										)													
						)
							
	SELECT 	@AllCount AS AllCount,
			@OwnedByMeCount AS OwnedByMeCount,
			@SharedCount AS SharedCount,
			@RecentlyViewedCount AS RecentlyViewedCount,
			@ArchivedCount AS ArchivedCount,
			@RecycleCount AS RecycleCount
END


GO

/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 03/26/2013 17:16:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnFolderTreeView]'))
DROP VIEW [dbo].[ReturnFolderTreeView]
GO

/****** Object:  View [dbo].[ReturnFolderTreeView]    Script Date: 03/21/2013 15:22:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ReturnFolderTreeView] 
AS 
	SELECT  0 AS ID,
			0 AS Parent,
			'' AS Name,
			0 AS Creator,
			0 AS [Level],
			'' AS Collaborators

GO

/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 03/26/2013 17:16:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_GetFolderTree]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_GetFolderTree]
GO


/****** Object:  StoredProcedure [dbo].[SPC_GetFolderTree]    Script Date: 03/26/2013 17:05:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_GetFolderTree
-- Description:  This Sp returns accessed folders that needed for Approval index Page
-- Date Created: Monday, 23 April 2010
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_GetFolderTree] (
	@P_User int
)
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH tableSet AS 
		(
			SELECT f.ID, f.Parent, f.Name, f.Creator, (0) AS Level, (CASE WHEN 
												(@P_User IN (	SELECT	fc.Collaborator 
																FROM	[dbo].FolderCollaborator fc
																WHERE	fc.Folder = f.ID 
						) ) THEN  '1' ELSE '0' end)  AS HasAccess 
			FROM	[dbo].[Folder] f
			WHERE f.IsDeleted = 0	 
			
			UNION ALL
			
			SELECT sf.ID, sf.Parent,sf.Name, sf.Creator, (CASE WHEN (@P_User IN (	SELECT fc.Collaborator 
						FROM	[dbo].FolderCollaborator fc
						WHERE	fc.Folder = sf.ID 
						 )) THEN  (h.[Level] + 1) ELSE (0) END)  AS [Level], (CASE WHEN (@P_User IN (	SELECT fc.Collaborator 
						FROM	[dbo].FolderCollaborator fc
						WHERE	fc.Folder = sf.ID 
						 )) THEN  '1' ELSE '0' END)  AS HasAccess 
			FROM [dbo].[Folder] sf
				JOIN tableSet h
			ON h.ID	 = sf.Parent
			WHERE sf.IsDeleted = 0			 
		)	
	
	SELECT ID, ISNULL(Parent, 0) AS Parent, Name, Creator, MAX([Level]) AS [Level],(SELECT dbo.GetFolderCollaborators(ID)) AS Collaborators
	FROM tableSet
	WHERE HasAccess = 1
	GROUP BY ID, Parent, Name, Creator
	
END


GO


/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 05/16/2013 11:05:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SplitString]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SplitString]
GO

/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 05/16/2013 11:04:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SplitString](
    @delimited NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
    DECLARE @xml XML
    SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'
    INSERT INTO @t(val)
    
    SELECT  r.value('.','varchar(MAX)') as item
    FROM  @xml.nodes('/t') as records(r)
    
    RETURN
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 05/16/2013 09:23:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBillingCycles]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetBillingCycles]
GO

/****** Object:  UserDefinedFunction [dbo].[GetBillingCycles]    Script Date: 05/15/2013 16:59:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetBillingCycles](
	@AccountID int,
	@StartDate datetime,
	@EndDate dateTime	
) 
RETURNS nvarchar(MAX)
AS BEGIN

	DECLARE @cycleCount INT = 0
	DECLARE @AddedProofsCount INT = 0
	DECLARE @totalExceededCount INT = 0	
	DECLARE @cycleStartDate DATETIME = @StartDate
	DECLARE @cycleEndDate DATETIME
	DECLARE @cycleAllowedProofsCount INT
	DECLARE @IsMonthly bit = 1 
	DECLARE @PlanPrice decimal = 0.0
	DECLARE @ParentAccount INT
	DECLARE @SelectedBillingPLan INT
	DECLARE @BillingPlanPrice INT
	
	SET @ParentAccount = (SELECT Parent FROM Account WHERE ID = @AccountID)
	SET @SelectedBillingPLan = (SELECT SelectedBillingPLan FROM Account WHERE ID = @AccountID)
	
	SET @cycleAllowedProofsCount = (SELECT bp.Proofs 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @cycleAllowedProofsCount = (CASE WHEN (@IsMonthly = 1)
						THEN @cycleAllowedProofsCount
						ELSE (@cycleAllowedProofsCount*12)
						END
						) 										
											
	SET @BillingPlanPrice = (SELECT bp.Price 
											FROM Account a
												INNER JOIN BillingPlan bp
													ON bp.ID = a.SelectedBillingPlan
											WHERE a.ID = @AccountID	)
											
	SET @IsMonthly = (SELECT IsMonthlyBillingFrequency 
						FROM Account a
						WHERE a.ID = @AccountID
						)
						
	SET @PlanPrice = (CASE WHEN (@ParentAccount = 1)
						THEN @BillingPlanPrice
						ELSE (SELECT NewPrice FROM AttachedAccountBillingPlan
								WHERE Account = @ParentAccount AND BillingPlan = @SelectedBillingPLan)
						END)
						
	SET @PlanPrice = (CASE WHEN (@IsMonthly = 1)
						THEN @PlanPrice
						ELSE (@PlanPrice*12)
						END
						) 
																					
	SET @cycleStartDate = @StartDate
	
	WHILE (@cycleStartDate < @EndDate)
	BEGIN	
		DECLARE @currentCycleApprovalsCount INT = 0
				
		SET @cycleEndDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END	
	
		SET @currentCycleApprovalsCount = ( SELECT COUNT(ap.ID) FROM Job j 
														INNER JOIN Approval ap
															ON j.ID = ap.Job
														WHERE j.Account = @AccountID
														AND ap.CreatedDate >= @cycleStartDate
														AND ap.CreatedDate <= @cycleEndDate	)
														 
		SET @AddedProofsCount = @AddedProofsCount + @currentCycleApprovalsCount
													
		SET @cycleStartDate = CASE WHEN (@IsMonthly = 1)
								THEN DATEADD(MM,1,@cycleStartDate)
								ELSE
									DATEADD(YYYY,1,@cycleStartDate)
								END
								
		SET @cycleCount = @cycleCount + 1;	
		SET @totalExceededCount = @totalExceededCount + CASE WHEN (@cycleAllowedProofsCount < @currentCycleApprovalsCount)  
										THEN @currentCycleApprovalsCount - @cycleAllowedProofsCount
										ELSE 0
										END													
	END
		
	RETURN  CONVERT(NVARCHAR, @cycleCount) + '|' +  CONVERT(NVARCHAR, @AddedProofsCount) + '|' +  CONVERT(NVARCHAR, @totalExceededCount) + '|' +  CONVERT(NVARCHAR, @PlanPrice)
	
END

GO

/****** Object:  UserDefinedFunction [dbo].[GetChildAccounts]    Script Date: 05/16/2013 09:23:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetChildAccounts]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetChildAccounts]
GO

/****** Object:  UserDefinedFunction [dbo].[GetChildAccounts]    Script Date: 05/17/2013 17:26:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[GetChildAccounts]
(	
	-- Add the parameters for the function here
	@LoggedAccountId int
)
RETURNS TABLE 
AS
RETURN 
(
	WITH tableSet AS (
		SELECT a.ID
		FROM	[dbo].[Account] a
		WHERE	a.ID = @LoggedAccountId		 
		
		UNION ALL
		
		SELECT sa.ID
		FROM [dbo].[Account] sa
			JOIN tableSet h
		ON h.ID	 = sa.Parent			 
	)	
	-- Add the SELECT statement with parameter references here
	SELECT *
	FROM  tableSet
)


GO

/****** Object:  View [dbo].[ReturnBillingReportInfoView]    Script Date: 05/16/2013 11:48:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnBillingReportInfoView]'))
DROP VIEW [dbo].[ReturnBillingReportInfoView]
GO

/****** Object:  View [dbo].[ReturnBillingReportInfoView]    Script Date: 05/16/2013 11:46:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReturnBillingReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS AccountName,
			'' AS AccountTypeName,
			'' AS BillingPlanName,
			0.0 AS GPPDiscount,
			0 AS Proofs,
			'' AS BillingCycleDetails,
			0 AS Parent,
			0 AS BillingPlan,
			CONVERT(bit, 0) AS IsFixedPlan,
			'' AS Subordinates
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 05/16/2013 10:55:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnBillingReportInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnBillingReportInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 05/15/2013 11:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnBillingReportInfo
-- Description:  This Sp returns multiple result sets that needed for Billing Report
-- Date Created: Wendsday, 15 May 2012
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnBillingReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountNameList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',
	@P_BillingPlanList nvarchar(MAX)='',
	@P_StartDate datetime ,
	@P_EndDate dateTime,
	@P_IsMonthlyBilling bit = 1,
	@P_LoggedAccount int 
)
AS
BEGIN
	-- Get the accounts	
	SET NOCOUNT ON
			
		SELECT DISTINCT					
				a.ID,
				a.Name AS AccountName,
				at.Name AS AccountTypeName,
				bp.Name AS BillingPlanName,
				a.GPPDiscount,
				bp.Proofs,
				(SELECT [dbo].[GetBillingCycles](a.ID, @P_StartDate, @P_EndDate )) AS BillingCycleDetails,
				a.Parent,
				a.SelectedBillingPlan AS BillingPlan,
				bp.IsFixed AS IsFixedPlan,				
				ISNULL((SELECT Name+',' 
					FROM Account 
					WHERE Parent= a.ID
					GROUP BY Name FOR XML PATH('')),'') AS Subordinates													
		FROM	Account a
		INNER JOIN Company c 
				ON c.Account = a.ID
		INNER JOIN AccountType at 
				ON a.AccountType = at.ID
		INNER JOIN AccountStatus ast 
				ON a.[Status] = ast.ID		
		INNER JOIN BillingPlan bp 
				ON a.SelectedBillingPlan = bp.ID				
		WHERE	a.NeedSubscriptionPlan = 1
		AND a.ContractStartDate IS NOT NULL
		AND a.IsTemporary =0
		AND (ast.[Key] = 'A')
		AND a.IsMonthlyBillingFrequency = @P_IsMonthlyBilling
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountNameList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountNameList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND (@P_BillingPlanList = '' OR a.SelectedBillingPlan IN (Select val FROM dbo.splitString(@P_BillingPlanList, ',')))
		
END

GO


/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 05/16/2013 11:48:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[ReturnUserReportInfoView]'))
DROP VIEW [dbo].[ReturnUserReportInfoView]
GO

/****** Object:  View [dbo].[ReturnUserReportInfoView]    Script Date: 05/16/2013 11:46:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ReturnUserReportInfoView] 
AS 
	SELECT  0 AS ID,
			'' AS Name,
			'' AS StatusName,
			'' AS UserName,
			'' AS EmailAddress,
			GETDATE() AS DateLastLogin
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnBillingReportInfo]    Script Date: 05/16/2013 10:55:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SPC_ReturnUserReportInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SPC_ReturnUserReportInfo]
GO

/****** Object:  StoredProcedure [dbo].[SPC_ReturnUserReportInfo]    Script Date: 05/15/2013 11:10:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Procedure: 	 SPC_ReturnUserReportInfo
-- Description:  This Sp returns multiple result sets that needed for User Report
-- Date Created: Wendsday, 17 May 2012
-- Created By:   Danesh Uthuranga
------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[SPC_ReturnUserReportInfo] (
		
	@P_AccountTypeList nvarchar(MAX) = '',
	@P_AccountIDList nvarchar(MAX)='',
	@P_LocationList nvarchar(MAX)='',	
	@P_UserStatusList nvarchar(MAX)='',
	@P_LoggedAccount int
)
AS
BEGIN
	-- Get the users	
	SET NOCOUNT ON
			
		SELECT	DISTINCT
			u.[ID],
			(u.[GivenName] + ' ' + u.[FamilyName]) AS Name,					
			us.[Name] AS StatusName,  
			u.[Username],
			u.[EmailAddress],
			ISNULL(u.[DateLastLogin], '1900/01/01') AS DateLastLogin
		FROM	[dbo].[User] u
			INNER JOIN [dbo].[UserStatus] us
				ON u.[Status] = us.ID
			INNER JOIN [dbo].[Account] a
				ON u.[Account] = a.ID
			INNER JOIN AccountType at 
				ON a.AccountType = at.ID
			INNER JOIN AccountStatus ast
				ON a.[Status] = ast.ID		
			INNER JOIN Company c 
				ON c.Account = a.ID																							
		WHERE
		a.IsTemporary =0
		AND (ast.[Key] = 'A') 
		AND (@P_AccountTypeList = '' OR a.AccountType IN (Select val FROM dbo.splitString(@P_AccountTypeList, ',')))
		AND (@P_AccountIDList = '' OR a.ID IN (Select val FROM dbo.splitString(@P_AccountIDList, ',')))
		AND (@P_LocationList = '' OR c.Country IN (Select val FROM dbo.splitString(@P_LocationList, ',')))
		AND ((@P_UserStatusList = '' AND ((us.[Key] != 'I') AND (us.[Key] != 'D') )) OR u.[Status] IN (Select val FROM dbo.splitString(@P_UserStatusList, ',')))
		AND ( (at.[Key] = 'GMHQ') OR (u.Account = @P_LoggedAccount) OR (a.Parent = @P_LoggedAccount))
		
END

GO

