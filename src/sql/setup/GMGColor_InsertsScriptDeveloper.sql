USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Subsidiary Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Subsidiary','Subsidiary', 'local-subsidiary.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'HQ', 'User', NULL, 'noreply-subsidiary@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			10, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-subsidiary.gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-subsidiary@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Subsidiary',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-subsidiary@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 1, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Dealer Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Dealer Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Dealer','Dealer', 'local-dealer.gmgcozone.com', 3, 1, 1, LOWER(NEWID()),
			'Dealer', 'User', NULL, 'noreply-dealer@gmgcozone.com',
			NULL, NULL, NULL, 3, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			12, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-dealer@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Dealer', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-dealer@gmgcozone.com',16)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Dealer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-dealer@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 2, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Client Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Client Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Client','Client', 'local-client.gmgcozone.com', 4, 1, 1, LOWER(NEWID()),
			'Client', 'User', NULL, 'noreply-client@gmgcozone.com',
			NULL, NULL, NULL, 2, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			14, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply-client@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Client', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'noreply-client@gmgcozone.com',18)
      
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Client',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','administrator.user-client@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300 000 000', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 3, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Subscriber Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subscriber Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Subscriber','Subscriber', 'local-subscriber.gmgcozone.com', 5, 1, 1, LOWER(NEWID()),
			'Subscriber', 'User', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 1, '',
			3, 3, 'AUS Eastern Standard Time', 1, 1,
			15, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subscriber', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',20)
     
-- User (Subscriber Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Subscriber',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Administrator','administrator@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
 WHERE [ID] = @AccountId
 
INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 4, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

DECLARE @AccountId int
DECLARE @UserId int

-- GMG coZone HQ Users --
SET @AccountId = 1

INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'HQ','Manager','manager@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 2)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Subsidiary Users --
SET @AccountId = 2
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Manager','manager@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Moderator','moderator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Contributor','contributor@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'S_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subsidiary','Viewer','viewer@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Dealer Users --
SET @AccountId = 3
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Manager','manager@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Moderator','moderator@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Contributor','contributor@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'D_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Dealer','Viewer','viewer@dealer.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Client Users --
SET @AccountId = 4
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Manager','manager@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Moderator','moderator@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Contributor','contributor@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'C_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Client','Viewer','viewer@client.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
-- Subscriber Users --
SET @AccountId = 5
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Manager',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Manager','manager@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 4)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Moderator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Moderator','moderator@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1,
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 5)
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Contributor',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Contributor','contributor@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 6)               
     
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'U_Viewer',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Subscriber','Viewer','viewer@subscriber.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()

INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 7)               
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Chinese Simplified Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Chinese Simplified Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('ChineseS','ChineseS Subsidiary', 'chineses.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 1, 'AUS Eastern Standard Time', 1, 1,
			0, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Chinese Simplified', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',48)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'ChineseS',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Chinese Simplified','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 6, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Chinese Traditional Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Chinese Traditional Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('ChineseT','ChineseT Subsidiary', 'chineset.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 2, 'AUS Eastern Standard Time', 1, 1,
			2, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Chinese Traditional', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',48)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'ChineseT',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Chinese Traditional','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 7, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- French Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary French Account --

INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('French','French Subsidiary', 'french.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 4, 'AUS Eastern Standard Time', 1, 1,
			4, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary French', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'French',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'French','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 8, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- German Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary German Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('German','German Subsidiary', 'german.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 5, 'AUS Eastern Standard Time', 1, 1,
			6, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary German', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'German',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'German','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 10, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Italian Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary German Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Italian','Italian Subsidiary', 'italian.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 6, 'AUS Eastern Standard Time', 1, 1,
			8, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Italian', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Italian',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Italian','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 11, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Korean Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Korean Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Korean','Korean Subsidiary', 'korean.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 7, 'AUS Eastern Standard Time', 1, 1,
			10, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Korean', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Korean',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Korean','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 12, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Japan Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Japan Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('DEV_Japan','Japan', 'japan.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Takeshi', 'De Silva', '03-9999-9999', 'noreply-japan@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 8, 'AUS Eastern Standard Time', 1, 1,
			12, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'japan-noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Japan (Subsidiary)', 'A9F21', 'Central 4, 1-1-1, Higashi', NULL, 'Nagoya', NULL, NULL, '03-9999-9999', NULL, 'japan-noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Administrator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Administrator','User','admin-user@gmgcozone.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '03-9999-9999', NULL, 4, 1)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 13, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Portugal Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Portugal Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Portugal','Portugal Subsidiary', 'portugal.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 9, 'AUS Eastern Standard Time', 1, 1,
			14, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Portugal', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Portugal',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Portugal','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 14, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Spain Account --
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int
     
-- Subsidiary Spain Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Spain','Spain Subsidiary', 'spain.gmgcozone.com', 2, 1, 1, LOWER(NEWID()),
			'Siwanka', 'De Silva', NULL, 'noreply@gmgcozone.com',
			NULL, NULL, NULL, 4, '',
			3, 10, 'AUS Eastern Standard Time', 1, 1,
			16, 1, 1, 1, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)
     
-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'Subsidiary Spain', NULL, 'Raglan Street', NULL, 'Melbourne', NULL, NULL, '1300405813', NULL, 'noreply@gmgcozone.com',14)
     
-- User (Subsidiary Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Spain',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Spain','Administrator','administrator@subsidiary.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), NULL, NULL, '1300405813', NULL, 1, 3)
SET @UserId = SCOPE_IDENTITY()
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, 3)
 
UPDATE [dbo].[Account]
   SET [Owner]  = @UserId
WHERE [ID] = @AccountId

INSERT INTO [dbo].[AttachedAccountBillingPlan]
            ([Account],[BillingPlan],[NewPrice],[IsAppliedCurrentRate],[NewProofPrice],[IsModifiedProofPrice])
     VALUES (@AccountId, 15, 0, 1, 0, 0)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Approval --
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_01','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_02','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_03','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_04','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_05','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_06','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_07','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_08','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_09','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_10','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_11','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_12','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_13','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_14','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_15','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_16','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_17','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_18','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_19','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)
--INSERT INTO [dbo].[Approval]([Decision],[Title],[Description],[DocumentPath],[Guid],[Parent],[Version],[CreatedDate],[Creator],[Deadline],[Account],[Modifier],[ModifiedDate],[IsPageSpreads],[IsDeleted],[SelectedAccessMode])
--     VALUES(1,'Test Approval_20','','noaproval-png-256px-256px.png','a7bdba21-h4be-3de8-sb73-3e75b1a56bdf',NULL,0,CAST(GETDATE() AS datetime2(7)),2,CAST(GETDATE() AS datetime2(7)),2,1,CAST(GETDATE() AS datetime2(7)),0,0,1)     
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- UserGroup AND UserGroupUser --
--DECLARE @UserGroup int

--INSERT INTO [dbo].[UserGroup]([Name],[Account],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--	VALUES	('GMG Colloaborate', 2,2, GETDATE(),1,GETDATE())
--SET @UserGroup = SCOPE_IDENTITY()

--INSERT INTO [dbo].[UserGroupUser]([UserGroup],[User])
--	VALUES	(@UserGroup, 2)
--INSERT INTO [dbo].[UserGroupUser]([UserGroup],[User])
--	VALUES	(@UserGroup, 4)
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Folder AND FolderCollaborator --
--DECLARE @Folder int
--DECLARE @SubFolder int

--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('My Approvals', 'My Approvals', NULL, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @Folder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@Folder, 2, CAST(GETDATE() AS datetime2(7)))
     
--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('CoZone', 'CoZone Approvals', @Folder, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @SubFolder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@SubFolder, 2, CAST(GETDATE() AS datetime2(7)))

--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('Draft', 'Draft Approvals', @SubFolder, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @SubFolder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@SubFolder, 2, CAST(GETDATE() AS datetime2(7)))

--INSERT INTO [dbo].[Folder]([Name],[Description],[Parent],[SelectedAccessMode],[Creator],[CreatedDate],[Modifier],[ModifiedDate])
--     VALUES	('CoZone Proofs', 'CoZone Proofs', @Folder, 1, 2, CAST(GETDATE() AS datetime2(7)), 2, CAST(GETDATE() AS datetime2(7)))
--SET @SubFolder = SCOPE_IDENTITY()
--INSERT INTO [dbo].[FolderCollaborator]([Folder],[Collaborator],[AssignedDate])
--     VALUES	(@SubFolder, 2, CAST(GETDATE() AS datetime2(7)))   
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Currency Rates --
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (1, '1', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (2, '0.626', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (3, '0.774', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (4, '78.566', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
--	VALUES (5, '0.9901', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
--GO
--/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/    