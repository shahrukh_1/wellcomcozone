USE [GMGCoZone]
GO

-- HQ Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'debug.gmgcozone.com',
	   [Region] = 'us-east-1'
 WHERE [ID] = 1
GO

-- Subsidiary Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'subsidiary.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 2
GO

-- Dealer Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'dealer.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 3
GO

-- Client Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'client.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 4
GO

-- Subscriber Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'subscriber.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 5
GO

-- Chinese Simplified Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'chineses.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 6
GO

-- Chinese Traditional Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'chineset.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 7
GO

-- French Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'french.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 8
GO

-- German Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'german.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 9
GO

-- Italian Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'italian.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 10
GO

-- Korean Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'korean.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 11
GO

-- Japan Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'japan.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 12
GO

-- Portugal Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'portugal.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 13
GO

-- Spain Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'spain.debug.gmgcozone.com',
   	   [Region] = 'us-east-1'
 WHERE [ID] = 14
GO

-- Reset Passwords -
UPDATE [dbo].[User]
   SET [Password] = CONVERT(varchar(255), HashBytes('SHA1', 'TOFILLDBPASSWORD'))
GO