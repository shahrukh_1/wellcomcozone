USE [GMGCoZone]
GO

-- HQ Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'gmgcozonelocal.com'
 WHERE [ID] = 1
GO

-- Subsidiary Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'subsidiary.gmgcozonelocal.com'
 WHERE [ID] = 2
GO

-- Dealer Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'dealer.gmgcozonelocal.com'
 WHERE [ID] = 3
GO

-- Client Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'client.gmgcozonelocal.com'
 WHERE [ID] = 4
GO

-- Subscriber Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'subscriber.gmgcozonelocal.com'
 WHERE [ID] = 5
GO

-- Chinese Simplified Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'chineses.gmgcozonelocal.com'
 WHERE [ID] = 6
GO

-- Chinese Traditional Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'chineset.gmgcozonelocal.com'
 WHERE [ID] = 7
GO

-- French Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'french.gmgcozonelocal.com'
 WHERE [ID] = 8
GO

-- German Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'german.gmgcozonelocal.com'
 WHERE [ID] = 9
GO

-- Italian Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'italian.gmgcozonelocal.com'
 WHERE [ID] = 10
GO

-- Korean Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'korean.gmgcozonelocal.com'
 WHERE [ID] = 11
GO

-- Japan Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'japan.gmgcozonelocal.com'
 WHERE [ID] = 12
GO

-- Portugal Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'portugal.gmgcozonelocal.com'
 WHERE [ID] = 13
GO

-- Spain Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'spain.gmgcozonelocal.com'
 WHERE [ID] = 14
GO

-- Reset Passwords -
UPDATE [dbo].[User]
   SET [Password] = CONVERT(varchar(255), HashBytes('SHA1', 'password'))
GO

-- Reset Contact Email Address email for all accounts
UPDATE [dbo].[Account]
   SET [ContactEmailAddress] = 'cozonetest1@netrom.ro'
GO