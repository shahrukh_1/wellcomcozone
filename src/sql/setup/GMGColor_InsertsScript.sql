USE [GMGCoZone]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- AccountStatus --
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('A','Active')
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('I','Inactive')
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('R','Draft')
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('S','Suspended')               
INSERT INTO [dbo].[AccountStatus]([Key],[Name])
     VALUES ('D','Deleted')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalAnnotationType --
--INSERT INTO [dbo].[ApprovalAnnotationType]([Key],[Name],[Priority])
--     VALUES('CMT','Comment',1)
--INSERT INTO [dbo].[ApprovalAnnotationType]([Key],[Name],[Priority])
--     VALUES('DRW','Draw',2)
--INSERT INTO [dbo].[ApprovalAnnotationType]([Key],[Name],[Priority])
--     VALUES('TXT','Text',3)
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalCommentType --
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('MKR', 'Marker Comment', 1)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('ARA', 'Area Comment', 2)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('TXT', 'Text Comment', 3)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('DRW', 'Draw Comment', 4)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('ADT', 'Add Text Comment', 5)
INSERT INTO [dbo].[ApprovalCommentType]([Key],[Name],[Priority])
     VALUES('CPR', 'Colorpicker Comment', 6)     
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalAnnotationStatus
INSERT INTO [dbo].[ApprovalAnnotationStatus]([Key] ,[Name] ,[Priority])
     VALUES('A' ,'Approved' ,1)
INSERT INTO [dbo].[ApprovalAnnotationStatus]([Key] ,[Name] ,[Priority])
     VALUES('D' ,'Declined' ,2)
INSERT INTO [dbo].[ApprovalAnnotationStatus]([Key] ,[Name] ,[Priority])
     VALUES('N' ,'Neutral' ,3)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalStatus--
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('NEW', 'New', 1)     
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('INP', 'In Progress', 2)  
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('COM', 'Completed', 3)
INSERT INTO [dbo].[JobStatus]([Key], [Name], [Priority])
     VALUES('ARC', 'Archived', 4)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalDecision--
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('PDG', 'Pending', 1, 'A decision is still required')
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('CHR', 'Changes Required', 2, 'You have requested changes to the approval')
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('AWC', 'Approved with Changes', 3, 'You have requested changes to the approval but do not need to see another version')  
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('APD', 'Approved', 4, 'You have approved the approval')          
INSERT INTO [dbo].[ApprovalDecision]([Key], [Name], [Priority], [Description])
     VALUES('NAP', 'Not Applicable', 5, 'You do not think that you need to make a decision on this approval')     
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ApprovalCollaboratorRole --
INSERT INTO [dbo].[ApprovalCollaboratorRole]([Key],[Name],[Priority])
     VALUES ('RDO','Read Only', 1)
INSERT INTO [dbo].[ApprovalCollaboratorRole]([Key],[Name],[Priority])
     VALUES ('RVW','Reviewer', 2)
INSERT INTO [dbo].[ApprovalCollaboratorRole]([Key],[Name],[Priority])
     VALUES ('ANR','Approver & Reviewer', 3)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- HighlightType
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('N' , 'Normal' , 1)
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('I' , 'Insert' , 2)
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('R' , 'Replace' , 3)
INSERT INTO [dbo].[HighlightType]([Key] ,[Name] ,[Priority])
     VALUES('D' , 'Delete' , 4)               
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Smoothing --
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('NON' ,'None')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('ALL' ,'All')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('LNE' ,'Line')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('IMG' ,'Images')
INSERT INTO [dbo].[Smoothing]([Key] ,[Name])
     VALUES('TXT' ,'Text')
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

--  Colorspace --
INSERT INTO [dbo].[Colorspace]([Key] ,[Name])
     VALUES('RGB' ,'RGB')
INSERT INTO [dbo].[Colorspace]([Key] ,[Name])
     VALUES('CMK' ,'CMYK')
INSERT INTO [dbo].[Colorspace]([Key] ,[Name])
     VALUES('GRY' ,'Gray')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Image Format --
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('JPG', 'JPEG', NULL)
GO
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('COM', 'Compression', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('MIN', 'JPEG_minimum', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('LOW', 'JPEG_low', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('MED', 'JPEG_medium', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('HIG', 'JPEG_high', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('MAX', 'JPEG_maximum', 1)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('PNG', 'PNG', NULL)
INSERT INTO [dbo].[ImageFormat]([Key] ,[Name] ,[Parent])
     VALUES('TIF', 'TIFF', NULL)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

--  Page Box --
INSERT INTO [dbo].[PageBox]([Key] ,[Name])
     VALUES('CRP', 'CROPBOX')
INSERT INTO [dbo].[PageBox]([Key] ,[Name])
     VALUES('TRM', 'TRIMBOX')
INSERT INTO [dbo].[PageBox]([Key] ,[Name])
     VALUES('BLD', 'BLEEDBOX')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

--  Tile Image Format --
INSERT INTO [dbo].[TileImageFormat]([Key] ,[Name])
     VALUES('JPG', 'JPG')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- MediaService --
INSERT INTO [dbo].[MediaService]([Name] ,[Smoothing] ,[Resolution] ,[Colorspace] ,[ImageFormat] ,[PageBox] ,[TileImageFormat] ,[JPEGCompression])
     VALUES('Basic', 2, 300, 1, 5, 1, 1 , 90)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- BillingPlanType --
INSERT INTO [dbo].[BillingPlanType]([Key], [Name], [MediaService])
     VALUES ('Essentials', 'Essentials', 1)
INSERT INTO [dbo].[BillingPlanType]([Key], [Name], [MediaService])
     VALUES ('PrePress', 'Pre Press', 1)
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- BillingPlan --
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essentials Free', 1, 30, 0.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essentials 300', 1, 300, 250.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 600', 1, 600, 500.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 1000', 1, 1000, 1000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 2000', 1, 2000, 2000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 3000', 1, 3000, 3000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 4000', 1, 4000, 4000.0, 0)               
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 5000', 1, 5000, 5000.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('Essential 6000', 1, 6000, 6000.0, 0)    
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress Free', 2, 20, 0.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 300', 2, 300, 325.0, 0)   
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 600', 2, 600, 650.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 1000', 2, 1000, 975.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 2000', 2, 2000, 1560.0, 0)
INSERT INTO [dbo].[BillingPlan]([Name], [Type], [Proofs] ,[Price], [IsFixed])
     VALUES ('PrePress 3000', 2, 3000, 2210.0, 0) 
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ContractPeriod --
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('ONEY', 'One Year')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('TWOY', 'Two Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('THRY', 'Three Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('FORY', 'Four Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('FIVY', 'Five Years')
INSERT INTO [dbo].[ContractPeriod]([Key], [Name])
     VALUES ('NOLT', 'No Limit')              
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Currency --
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('Australian Dollar', 'AUD', '$')
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('British Pound', 'GBP', '�')     
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('EURO', 'EUR', '�')
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('Japanese Yen', 'JPY', '�')
INSERT INTO [dbo].[Currency]([Name], [Code], [Symbol])
     VALUES ('US Dollar', 'USD', '$')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- DateFormat --
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('MM/dd/yyyy', '04/16/2012')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('dd/MM/yyyy', '16/04/2012')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('yyyy-MM-dd', '2012-04-16')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('dd.MM.yyyy', '16.04.2012')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('yyyy.MM.dd', '2012.04.16')
INSERT INTO [dbo].[DateFormat]([Pattern] ,[Result])
     VALUES ('yyyy/MM/dd', '2012/04/16')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*/

-- File Types --
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('jpeg','Joint Photographic Experts Group')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('jpg','Joint Photographic Experts Group')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('pdf','Portable Document Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('png','Portable Network Graphics')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('tiff','Tagged Image File Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('tif','Tagged Image File Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('eps','Encapsulated Post Script')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mp4','Moving Pictures Experts Group Audio Layer 4(MPEG-4)')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('m4v','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('flv','Flash Live Video')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('f4v','Flash Video')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mov','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mpg','Moving Picture Experts Group')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('mpg2','Moving Picture Experts Group Layer 2')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('3gp','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('wmv','Windows Medoa Video')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('avi','Audio Video Interleaved')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('asf','Advanced Systems Format')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('fm','')
INSERT INTO [dbo].[FileType]([Extention],[Name])
     VALUES ('ogv','')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('bmp','Windows Bitmap')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('doc','Microsoft Word Document')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('docx','Word 2007 XML Document')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('gif','Graphics Interchange Format')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('htm','Hypertext Markup Language File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('html','Hypertext Markup Language File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('pps','PowerPoint Slideshow')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('ppt','PowerPoint Presentation')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('pptx','PowerPoint 2007 XML Presentation')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('psd','Adobe PhotoShop Document')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('swf','Shockwave Flash')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('ttf','TrueType Font')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('txt','Plain Text File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('xls','Microsoft Excel Spreadsheet')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('xlsx','Excel 2007 XML Workbook')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('xml','XML File')
--INSERT INTO [dbo].[FileType]([Extention],[Name])
--     VALUES ('zip','Zipped File')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Locale --
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('zh-Hans' ,'Chinese (Simplified) - China')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('zh-Hant' ,'Chinese (Traditional) - China')     
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('en-US' ,'English - United States')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('fr-FR' ,'French - France')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('de-DE' ,'German - Germany')      
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('it-IT' ,'Italian - Italy')     
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('ko-KR' ,'Korean - Korea')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('ja-JP' ,'Japanese - Japan')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('pt-PT' ,'Portuguese - Portugal')
INSERT INTO [dbo].[Locale] ([Name] ,[DisplayName])
     VALUES ('es-ES' ,'Spanish - Spain')    
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Notification Frequency --
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('AEO','As Events Occur')
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('DLY','Daily')
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('WLY','Weekly')
INSERT INTO [dbo].[NotificationFrequency]([Key], [Name])
     VALUES ('NVR','Never')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- PrePressFunction --
INSERT INTO [dbo].[PrePressFunction]([Key], [Name])
     VALUES ('CKRB' , 'CYMK/RGB Indicator')
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('OTPW' , 'Overprint Preview')
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('DSTR' , 'Densitometer')
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('CKSP' , 'CMYK Separations')          
--INSERT INTO [dbo].[PrePressFunction]([Name], [Description])
--     VALUES ('ICCP' , 'ICC Profile Preview') 
--GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Preset --
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('L', 'Low')
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('M', 'Medium')
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('H', 'High')
INSERT INTO [dbo].[Preset]([Key],[Name])
     VALUES('C', 'Custom')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Theme --
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('D', 'Default')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('S', 'Splash')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('J', 'Juicy')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('F', 'Fresh')
INSERT INTO [dbo].[Theme]([Key] ,[Name])
     VALUES('M', 'Moody')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- TimeFormat --
INSERT INTO [dbo].[TimeFormat]([Key], [Pattern], [Result])
     VALUES ('12HR', '12-hour clock', '4:20 PM')
INSERT INTO [dbo].[TimeFormat]([Key], [Pattern], [Result])
     VALUES ('24HR', '24-hour clock', '16:20')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- User Status --
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('A','Active')
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('N','Invited')
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('I','Inactive')
INSERT INTO [dbo].[UserStatus]([Key],[Name])
     VALUES ('D','Delete')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- ValueDataType --
INSERT INTO [dbo].[ValueDataType]([Key],[Name])
     VALUES('STRG', 'System.String')
INSERT INTO [dbo].[ValueDataType]([Key],[Name])
     VALUES('IN32', 'System.Int32')
INSERT INTO [dbo].[ValueDataType]([Key],[Name])
     VALUES('BOLN', 'System.Boolean')      
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- AccountSetting --
INSERT INTO [dbo].[AccountSetting]([Account] ,[Name] ,[Value] ,[ValueDataType] ,[Description])
     VALUES (NULL, 'PDF2SWFArguments', '-s transparent -T9 -S -s jpegquality=95 -s subpixels=150 \"{0}\" \"{1}\"' , 1, 'Arguments for the pdf2swf.exe for this account')
INSERT INTO [dbo].[AccountSetting]([Account] ,[Name] ,[Value] ,[ValueDataType] ,[Description])
     VALUES (NULL, 'GraphicsMagickConvertNotRGBArguments', 'convert " + epsOptions + " \"" + inputFilePath + "\"[0] +profile icm " + srcprofile + " -intent perceptual -profile \"" + TargetRgbColorProfilePath + "\" -resize " + Convert.ToString(Convert.ToInt32(width)) + "x" + Convert.ToString(Convert.ToInt32(height)) + " +compress -colorspace RGB " + resolution + " " + flatten + " \"" + outputFilePath + "\"' , 1, 'GraphicsMagick convert aguments if source file is not RGB or extended image')
INSERT INTO [dbo].[AccountSetting]([Account] ,[Name] ,[Value] ,[ValueDataType] ,[Description])
     VALUES (NULL, 'GraphicsMagickConvertRGBArguments', 'convert " + epsOptions + " \"" + inputFilePath + "\"[0] -resize " + Convert.ToString(Convert.ToInt32(width)) + "x" + Convert.ToString(Convert.ToInt32(height)) + "  +compress -colorspace RGB " + resolution + " " + flatten + " \"" + outputFilePath + "\"' , 1, 'GraphicsMagick convert aguments if source file is RGB')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Country --
SET IDENTITY_INSERT [dbo].[Country] ON
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (1, N'AD', N'AND', 20, NULL, N'Principality of Andorra', N'Andorra', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (2, N'AE', N'ARE', 784, NULL, N'United Arab Emirates', N'United Arab Emirates', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (3, N'AF', N'AFG', 4, NULL, N'Afghanistan', N'Afghanistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (4, N'AG', N'ATG', 28, NULL, N'Antigua and Barbuda', N'Antigua and Barbuda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (5, N'AI', N'AIA', 660, NULL, N'Anguilla', N'Anguilla', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (6, N'AL', N'ALB', 8, NULL, N'People''s Socialist Republic of Albania', N'Albania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (7, N'AM', N'ARM', 51, NULL, N'Armenia', N'Armenia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (8, N'AN', N'ANT', 530, NULL, N'Netherlands Antilles', N'Netherlands Antilles', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (9, N'AO', N'AGO', 24, NULL, N'Republic of Angola', N'Angola', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (10, N'AQ', N'ATA', 10, NULL, N'Antarctica', N'Antarctica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (11, N'AR', N'ARG', 32,  NULL, N'Argentine Republic', N'Argentina', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (12, N'AS', N'ASM', 16, NULL, N'American Samoa', N'American Samoa', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (13, N'AT', N'AUT', 40, NULL, N'Republic of Austria', N'Austria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (14, N'AU', N'AUS', 36, NULL, N'Commonwealth of Australia', N'Australia', 1)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (15, N'AW', N'ABW', 533, NULL, N'Aruba', N'Aruba', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (16, N'AZ', N'AZE', 31, NULL, N'Republic of Azerbaijan', N'Azerbaijan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (17, N'BA', N'BIH', 70, NULL, N'Bosnia and Herzegovina', N'Bosnia and Herzegovina', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (18, N'BB', N'BRB', 52, NULL, N'Barbados', N'Barbados', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (19, N'BD', N'BGD', 50, NULL, N'People''s Republic of Bangladesh', N'Bangladesh', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (20, N'BE', N'BEL', 56, NULL, N'Kingdom of Belgium', N'Belgium', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (21, N'BF', N'BFA', 854, NULL, N'Burkina Faso', N'Burkina Faso', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (22, N'BG', N'BGR', 100, NULL, N'People''s Republic of Bulgaria', N'Bulgaria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (23, N'BH', N'BHR', 48, NULL, N'Kingdom of Bahrain', N'Bahrain', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (24, N'BI', N'BDI', 108, NULL, N'Republic of Burundi', N'Burundi', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (25, N'BJ', N'BEN', 204, NULL, N'People''s Republic of Benin', N'Benin', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (26, N'BM', N'BMU', 60, NULL, N'Bermuda', N'Bermuda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (27, N'BN', N'BRN', 96, NULL, N'Brunei Darussalam', N'Brunei', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (28, N'BO', N'BOL', 68, NULL, N'Republic of Bolivia', N'Bolivia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (29, N'BR', N'BRA', 76, NULL, N'Federative Republic of Brazil', N'Brazil', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (30, N'BS', N'BHS', 44, NULL, N'Commonwealth of the Bahamas', N'Bahamas', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (31, N'BT', N'BTN', 64, NULL, N'Kingdom of Bhutan', N'Bhutan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (32, N'BV', N'BVT', 74, NULL, N'Bouvet Island', N'Bouvet Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (33, N'BW', N'BWA', 72, NULL, N'Republic of Botswana', N'Botswana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (34, N'BY', N'BLR', 112, NULL, N'Belarus', N'Belarus', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (35, N'BZ', N'BLZ', 84, NULL, N'Belize', N'Belize', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (36, N'CA', N'CAN', 124, NULL, N'Canada', N'Canada', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (37, N'CC', N'CCK', 166, NULL, N'Cocos (Keeling) Islands', N'Cocos Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (38, N'CD', N'COD', 180, NULL, N'Democratic Republic of Congo', N'Congo', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (39, N'CF', N'CAF', 140, NULL, N'Central African Republic', N'Central African Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (40, N'CG', N'COG', 178, NULL, N'People''s Republic of Congo', N'Congo', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (41, N'CH', N'CHE', 756, NULL, N'Swiss Confederation Switzerland', N'Switzerland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (42, N'CI', N'CIV', 384, NULL, N'Republic of the Ivory Coast', N'Ivory Coast', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (43, N'CK', N'COK', 184, NULL, N'Cook Islands', N'Cook Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (44, N'CL', N'CHL', 152, NULL, N'Republic of Chile', N'Chile', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (45, N'CM', N'CMR', 120, NULL, N'United Republic of Cameroon', N'Cameroon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (46, N'CN', N'CHN', 156, NULL, N'People''s Republic of China', N'China', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (47, N'CO', N'COL', 170, NULL, N'Republic of Colombia', N'Colombia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (48, N'CR', N'CRI', 188, NULL, N'Republic of Costa Rica', N'Costa Rica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (49, N'CS', N'SCG', 891, NULL, N'Serbia and Montenegro', N'Serbia and Montenegro', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (50, N'CU', N'CUB', 192, NULL, N'Republic of Cuba', N'Cuba', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (51, N'CV', N'CPV', 132, NULL, N'Republic of Cape Verde', N'Cape Verde', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (52, N'CX', N'CXR', 162, NULL, N'Christmas Island', N'Christmas Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (53, N'CY', N'CYP', 196, NULL, N'Republic of Cyprus', N'Cyprus', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (54, N'CZ', N'CZE', 203, NULL, N'Czech Republic', N'Czech Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (55, N'DE', N'DEU', 276, NULL, N'Germany', N'Germany', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (56, N'DJ', N'DJI', 262, NULL, N'Republic of Djibouti', N'Djibouti', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (57, N'DK', N'DNK', 208, NULL, N'Kingdom of Denmark', N'Denmark', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (58, N'DM', N'DMA', 212, NULL, N'Commonwealth of Dominica', N'Dominica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (59, N'DO', N'DOM', 214, NULL, N'Dominican Republic', N'Dominican Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (60, N'DZ', N'DZA', 12, NULL, N'People''s Democratic Republic of Algeria', N'Algeria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (61, N'EC', N'ECU', 218, NULL, N'Republic of Ecuador', N'Ecuador', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (62, N'EE', N'EST', 233, NULL, N'Estonia', N'Estonia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (63, N'EG', N'EGY', 818, NULL, N'Arab Republic of Egypt', N'Egypt', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (64, N'EH', N'ESH', 732, NULL, N'Western Sahara', N'Western Sahara', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (65, N'ER', N'ERI', 232, NULL, N'Eritrea', N'Eritrea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (66, N'ES', N'ESP', 724, NULL, N'Spanish State Spain', N'Spain', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (67, N'ET', N'ETH', 231, NULL, N'Ethiopia', N'Ethiopia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (68, N'FI', N'FIN', 246, NULL, N'Republic of Finland', N'Finland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (69, N'FJ', N'FJI', 242, NULL, N'Republic of the Fiji Islands Fiji', N'Fiji', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (70, N'FK', N'FLK', 238, NULL, N'Falkland Islands', N'Falkland Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (71, N'FM', N'FSM', 583, NULL, N'Federated States of Micronesia', N'Micronesia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (72, N'FO', N'FRO', 234, NULL, N'Faeroe Islands', N'Faeroe Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (73, N'FR', N'FRA', 250, NULL, N'French Republic France', N'France', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (74, N'GA', N'GAB', 266, NULL, N'Gabonese Republic Gabon', N'Gabon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (75, N'GB', N'GBR', 826, NULL, N'United Kingdom of Great Britain & N. Ireland', N'United Kingdom', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (76, N'GD', N'GRD', 308, NULL, N'Grenada', N'Grenada', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (77, N'GE', N'GEO', 268, NULL, N'Georgia', N'Georgia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (78, N'GF', N'GUF', 254, NULL, N'French Guiana', N'French Guiana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (79, N'GH', N'GHA', 288, NULL, N'Republic of Ghana', N'Ghana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (80, N'GI', N'GIB', 292, NULL, N'Gibraltar', N'Gibraltar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (81, N'GL', N'GRL', 304, NULL, N'Greenland', N'Greenland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (82, N'GM', N'GMB', 270, NULL, N'Republic of the Gambia', N'Gambia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (83, N'GN', N'GIN', 324, NULL, N'Revolutionary People''s Republic of Guinea', N'Guinea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (84, N'GP', N'GLP', 312, NULL, N'Guadaloupe', N'Guadaloupe', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (85, N'GQ', N'GNQ', 226, NULL, N'Republic of Equatorial Guinea', N'Equatorial Guinea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (86, N'GR', N'GRC', 300, NULL, N'Hellenic Republic Greece', N'Greece', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (87, N'GS', N'SGS', 239, NULL, N'South Georgia and the South Sandwich Islands', N'South Georgia and the South Sandwich Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (88, N'GT', N'GTM', 320, NULL, N'Republic of Guatemala', N'Guatemala', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (89, N'GU', N'GUM', 316, NULL, N'Guam', N'Guam', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (90, N'GW', N'GNB', 624, NULL, N'Republic of Guinea-Bissau', N'Guinea-Bissau', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (91, N'GY', N'GUY', 328, NULL, N'Republic of Guyana', N'Guyana', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (92, N'HK', N'HKG', 344, NULL, N'Special Administrative Region of China Hong Kong', N'Hong Kong', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (93, N'HM', N'HMD', 334, NULL, N'Heard and McDonald Islands', N'Heard and McDonald Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (94, N'HN', N'HND', 340, NULL, N'Republic of Honduras', N'Honduras', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (95, N'HR', N'HRV', 191, NULL, N'Hrvatska', N'Hrvatska', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (96, N'HT', N'HTI', 332, NULL, N'Republic of Haiti', N'Haiti', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (97, N'HU', N'HUN', 348, NULL, N'Hungarian People''s Republic Hungary', N'Hungary', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (98, N'ID', N'IDN', 360, NULL, N'Republic of Indonesia', N'Indonesia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (99, N'IE', N'IRL', 372, NULL, N'Ireland', N'Ireland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (100, N'IL', N'ISR', 376, NULL, N'State of Israel', N'Israel', 0)

INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (101, N'IN', N'IND', 356, NULL, N'Republic of India', N'India', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (102, N'IO', N'IOT', 86, NULL, N'British Indian Ocean Territory', N'British Indian Ocean Territory', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (103, N'IQ', N'IRQ', 368, NULL, N'Republic of Iraq', N'Iraq', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (104, N'IR', N'IRN', 364, NULL, N'Islamic Republic of Iran', N'Iran', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (105, N'IS', N'ISL', 352, NULL, N'Republic of Iceland', N'Iceland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (106, N'IT', N'ITA', 380, NULL, N'Italian Republic Italy', N'Italy', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (107, N'JM', N'JAM', 388, NULL, N'Jamaica', N'Jamaica', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (108, N'JO', N'JOR', 400, NULL, N'Hashemite Kingdom of Jordan', N'Jordan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (109, N'JP', N'JPN', 392, NULL, N'Japan', N'Japan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (110, N'KE', N'KEN', 404, NULL, N'Republic of Kenya', N'Kenya', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (111, N'KG', N'KGZ', 417, NULL, N'Kyrgyz Republic', N'Kyrgyz Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (112, N'KH', N'KHM', 116, NULL, N'Kingdom of Cambodia', N'Cambodia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (113, N'KI', N'KIR', 296, NULL, N'Republic of Kiribati', N'Kiribati', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (114, N'KM', N'COM', 174, NULL, N'Union of the Comoros', N'Comoros', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (115, N'KN', N'KNA', 659, NULL, N'St. Kitts and Nevis', N'St. Kitts and Nevis', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (116, N'KP', N'PRK', 408, NULL, N'Democratic People''s Republic of Korea', N'North Korea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (117, N'KR', N'KOR', 410, NULL, N'Republic of Korea', N'South Korea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (118, N'KW', N'KWT', 414, NULL, N'State of Kuwait', N'Kuwait', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (119, N'KY', N'CYM', 136, NULL, N'Cayman Islands', N'Cayman Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (120, N'KZ', N'KAZ', 398, NULL, N'Republic of Kazakhstan', N'Kazakhstan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (121, N'LA', N'LAO', 418, NULL, N'Lao People''s Democratic Republic', N'Lao People''s Democratic Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (122, N'LB', N'LBN', 422, NULL, N'Lebanese Republic Lebanon', N'Lebanon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (123, N'LC', N'LCA', 662, NULL, N'St. Lucia', N'St. Lucia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (124, N'LI', N'LIE', 438, NULL, N'Principality of Liechtenstein', N'Liechtenstein', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (125, N'LK', N'LKA', 144, NULL, N'Democratic Socialist Republic of Sri Lanka', N'Sri Lanka', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (126, N'LR', N'LBR', 430, NULL, N'Republic of Liberia', N'Liberia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (127, N'LS', N'LSO', 426, NULL, N'Kingdom of Lesotho', N'Lesotho', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (128, N'LT', N'LTU', 440, NULL, N'Lithuania', N'Lithuania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (129, N'LU', N'LUX', 442, NULL, N'Grand Duchy of Luxembourg', N'Luxembourg', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (130, N'LV', N'LVA', 428, NULL, N'Latvia', N'Latvia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (131, N'LY', N'LBY', 434, NULL, N'Libyan Arab Jamahiriya', N'Libyan Arab Jamahiriya', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (132, N'MA', N'MAR', 504, NULL, N'Kingdom of Morocco', N'Morocco', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (133, N'MC', N'MCO', 492, NULL, N'Principality of Monaco', N'Monaco', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (134, N'MD', N'MDA', 498, NULL, N'Republic of Moldova', N'Moldova', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (135, N'MG', N'MDG', 450, NULL, N'Republic of Madagascar', N'Madagascar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (136, N'MH', N'MHL', 584, NULL, N'Marshall Islands', N'Marshall Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (137, N'MK', N'MKD', 807, NULL, N'the former Yugoslav Republic of Macedonia', N'Macedonia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (138, N'ML', N'MLI', 466, NULL, N'Republic of Mali', N'Mali', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (139, N'MM', N'MMR', 104, NULL, N'Myanmar', N'Myanmar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (140, N'MN', N'MNG', 496, NULL, N'Mongolian People''s Republic Mongolia', N'Mongolia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (141, N'MO', N'MAC', 446, NULL, N'Special Administrative Region of China Macao', N'Macao', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (142, N'MP', N'MNP', 580, NULL, N'Northern Mariana Islands', N'Northern Mariana Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (143, N'MQ', N'MTQ', 474, NULL, N'Martinique', N'Martinique', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (144, N'MR', N'MRT', 478, NULL, N'Islamic Republic of Mauritania', N'Mauritania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (145, N'MS', N'MSR', 500, NULL, N'Montserrat', N'Montserrat', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (146, N'MT', N'MLT', 470, NULL, N'Republic of Malta', N'Malta', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (147, N'MU', N'MUS', 480, NULL, N'Mauritius', N'Mauritius', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (148, N'MV', N'MDV', 462, NULL, N'Republic of Maldives', N'Maldives', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (149, N'MW', N'MWI', 454, NULL, N'Republic of Malawi', N'Malawi', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (150, N'MX', N'MEX', 484, NULL, N'United Mexican States Mexico', N'Mexico', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (151, N'MY', N'MYS', 458, NULL, N'Malaysia', N'Malaysia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (152, N'MZ', N'MOZ', 508, NULL, N'People''s Republic of Mozambique', N'Mozambique', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (153, N'NA', N'NAM', 516, NULL, N'Namibia', N'Namibia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (154, N'NC', N'NCL', 540, NULL, N'New Caledonia', N'New Caledonia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (155, N'NE', N'NER', 562, NULL, N'Republic of the Niger', N'Niger', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (156, N'NF', N'NFK', 574, NULL, N'Norfolk Island', N'Norfolk Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (157, N'NG', N'NGA', 566, NULL, N'Federal Republic of Nigeria', N'Nigeria', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (158, N'NI', N'NIC', 558, NULL, N'Republic of Nicaragua', N'Nicaragua', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (159, N'NL', N'NLD', 528, NULL, N'Kingdom of the Netherlands', N'Netherlands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (160, N'NO', N'NOR', 578, NULL, N'Kingdom of Norway', N'Norway', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (161, N'NP', N'NPL', 524, NULL, N'Kingdom of Nepal', N'Nepal', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (162, N'NR', N'NRU', 520, NULL, N'Republic of Nauru', N'Nauru', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (163, N'NU', N'NIU', 570, NULL, N'Republic of Niue', N'Niue', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (164, N'NZ', N'NZL', 554, NULL, N'New Zealand', N'New Zealand', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (165, N'OM', N'OMN', 512, NULL, N'Sultanate of Oman', N'Oman', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (166, N'PA', N'PAN', 591, NULL, N'Republic of Panama', N'Panama', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (167, N'PE', N'PER', 604, NULL, N'Republic of Peru', N'Peru', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (168, N'PF', N'PYF', 258, NULL, N'French Polynesia', N'French Polynesia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (169, N'PG', N'PNG', 598, NULL, N'Papua New Guinea', N'Papua New Guinea', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (170, N'PH', N'PHL', 608, NULL, N'Republic of the Philippines', N'Philippines', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (171, N'PK', N'PAK', 586, NULL, N'Islamic Republic of Pakistan', N'Pakistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (172, N'PL', N'POL', 616, NULL, N'Polish People''s Republic Poland', N'Poland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (173, N'PM', N'SPM', 666, NULL, N'St. Pierre and Miquelon', N'St. Pierre and Miquelon', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (174, N'PN', N'PCN', 612, NULL, N'Pitcairn Island', N'Pitcairn Island', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (175, N'PR', N'PRI', 630, NULL, N'Puerto Rico', N'Puerto Rico', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (176, N'PS', N'PSE', 275, NULL, N'Occupied Palestinian Territory', N'Palestinian Territory', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (177, N'PT', N'PRT', 620, NULL, N'Portuguese Republic Portugal', N'Portugal', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (178, N'PW', N'PLW', 585, NULL, N'Palau', N'Palau', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (179, N'PY', N'PRY', 600, NULL, N'Republic of Paraguay', N'Paraguay', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (180, N'QA', N'QAT', 634, NULL, N'State of Qatar', N'Qatar', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (181, N'RE', N'REU', 638, NULL, N'Reunion', N'Reunion', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (182, N'RO', N'ROU', 642, NULL, N'Socialist Republic of Romania', N'Romania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (183, N'RU', N'RUS', 643, NULL, N'Russian Federation', N'Russian Federation', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (184, N'RW', N'RWA', 646, NULL, N'Rwandese Republic Rwanda', N'Rwanda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (185, N'SA', N'SAU', 682, NULL, N'Kingdom of Saudi Arabia', N'Saudi Arabia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (186, N'SB', N'SLB', 90, NULL, N'Solomon Islands', N'Solomon Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (187, N'SC', N'SYC', 690, NULL, N'Republic of Seychelles', N'Seychelles', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (188, N'SD', N'SDN', 736, NULL, N'Democratic Republic of the Sudan', N'Sudan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (189, N'SE', N'SWE', 752, NULL, N'Kingdom of Sweden', N'Sweden', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (190, N'SG', N'SGP', 702, NULL, N'Republic of Singapore', N'Singapore', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (191, N'SH', N'SHN', 654, NULL, N'St. Helena', N'St. Helena', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (192, N'SI', N'SVN', 705, NULL, N'Slovenia', N'Slovenia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (193, N'SJ', N'SJM', 744, NULL, N'Svalbard & Jan Mayen Islands', N'Svalbard & Jan Mayen Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (194, N'SK', N'SVK', 703, NULL, N'Slovakia', N'Slovakia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (195, N'SL', N'SLE', 694, NULL, N'Republic of Sierra Leone', N'Sierra Leone', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (196, N'SM', N'SMR', 674, NULL, N'Republic of San Marino', N'San Marino', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (197, N'SN', N'SEN', 686, NULL, N'Republic of Senegal', N'Senegal', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (198, N'SO', N'SOM', 706, NULL, N'Somali Republic Somalia', N'Somalia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (199, N'SR', N'SUR', 740, NULL, N'Republic of Suriname', N'Suriname', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (200, N'ST', N'STP', 678, NULL, N'Democratic Republic of Sao Tome and Principe', N'Sao Tome and Principe', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (201, N'SV', N'SLV', 222, NULL, N'Republic of El Salvador', N'El Salvador', 0)

INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (202, N'SY', N'SYR', 760, NULL, N'Syrian Arab Republic', N'Syrian Arab Republic', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (203, N'SZ', N'SWZ', 748, NULL, N'Kingdom of Swaziland', N'Swaziland', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (204, N'TC', N'TCA', 796, NULL, N'Turks and Caicos Islands', N'Turks and Caicos Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (205, N'TD', N'TCD', 148, NULL, N'Republic of Chad', N'Chad', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (206, N'TF', N'ATF', 260, NULL, N'French Southern Territories', N'French Southern Territories', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (207, N'TG', N'TGO', 768, NULL, N'Togolese Republic Togo', N'Togo', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (208, N'TH', N'THA', 764, NULL, N'Kingdom of Thailand', N'Thailand', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (209, N'TJ', N'TJK', 762, NULL, N'Tajikistan', N'Tajikistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (210, N'TK', N'TKL', 772, NULL, N'Tokelau', N'Tokelau', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (211, N'TL', N'TLS', 626, NULL, N'Democratic Republic of Timor-Leste', N'Timor-Leste', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (212, N'TM', N'TKM', 795, NULL, N'Turkmenistan', N'Turkmenistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (213, N'TN', N'TUN', 788, NULL, N'Republic of Tunisia', N'Tunisia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (214, N'TO', N'TON', 776, NULL, N'Kingdom of Tonga', N'Tonga', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (215, N'TR', N'TUR', 792, NULL, N'Republic of Turkey', N'Turkey', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (216, N'TT', N'TTO', 780, NULL, N'Republic of Trinidad and Tobago', N'Trinidad and Tobago', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (217, N'TV', N'TUV', 798, NULL, N'Tuvalu', N'Tuvalu', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (218, N'TW', N'TWN', 158, NULL, N'Province of China Taiwan', N'Taiwan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (219, N'TZ', N'TZA', 834, NULL, N'United Republic of Tanzania', N'Tanzania', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (220, N'UA', N'UKR', 804, NULL, N'Ukraine', N'Ukraine', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (221, N'UG', N'UGA', 800, NULL, N'Republic of Uganda', N'Uganda', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (222, N'UM', N'UMI', 581, NULL, N'United States Minor Outlying Islands', N'United States Minor Outlying Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (223, N'US', N'USA', 840, NULL, N'United States of America', N'USA', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (224, N'UY', N'URY', 858, NULL, N'Eastern Republic of Uruguay', N'Uruguay', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (225, N'UZ', N'UZB', 860, NULL, N'Uzbekistan', N'Uzbekistan', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (226, N'VA', N'VAT', 336, NULL, N'Holy See', N'Holy See', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (227, N'VC', N'VCT', 670, NULL, N'St. Vincent and the Grenadines', N'St. Vincent and the Grenadines', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (228, N'VE', N'VEN', 862, NULL, N'Bolivarian Republic of Venezuela', N'Venezuela', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (229, N'VG', N'VGB', 92, NULL, N'British Virgin Islands', N'British Virgin Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (230, N'VI', N'VIR', 850, NULL, N'US Virgin Islands', N'US Virgin Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (231, N'VN', N'VNM', 704, NULL, N'Socialist Republic of Viet Nam', N'Viet Nam', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (232, N'VU', N'VUT', 548, NULL, N'Vanuatu', N'Vanuatu', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (233, N'WF', N'WLF', 876, NULL, N'Wallis and Futuna Islands', N'Wallis and Futuna Islands', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (234, N'WS', N'WSM', 882, NULL, N'Independent State of Samoa', N'Samoa', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (235, N'YE', N'YEM', 887, NULL, N'Yemen', N'Yemen', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (236, N'YT', N'MYT', 175, NULL, N'Mayotte', N'Mayotte', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (237, N'ZA', N'ZAF', 710, NULL, N'Republic of South Africa', N'South Africa', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (238, N'ZM', N'ZMB', 894, NULL, N'Republic of Zambia', N'Zambia', 0)
INSERT [dbo].[Country] ([ID], [Iso2], [Iso3], [IsoCountryNumber], [DialingPrefix], [Name], [ShortName], [HasLocationData]) VALUES (239, N'ZW', N'ZWE', 716, NULL, N'Zimbabwe', N'Zimbabwe', 0)
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Account --
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_Creator]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Creator]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_Modifier]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Modifier]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_Owner]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Owner]
GO

DECLARE @GMGcoZone int
DECLARE @AccountId int
DECLARE @UserId int
DECLARE @RoleId int

-- AccountType --
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('GMHQ', 'GMGcoZone')
SET @GMGcoZone = SCOPE_IDENTITY()

-- HQ Administrator Account --
INSERT INTO [dbo].[Account]
           ([Name],[SiteName],[Domain],[AccountType],[Parent],[Owner],[Guid]
           ,[ContactFirstName],[ContactLastName],[ContactPhone],[ContactEmailAddress]
           ,[HeaderLogoPath], [LoginLogoPath], [EmailLogoPath], [Theme], [CustomColor]
           ,[CurrencyFormat],[Locale],[TimeZone],[DateFormat],[TimeFormat]
           ,[GPPDiscount],[NeedSubscriptionPlan],[ChargeCostPerProofIfExceeded],[SelectedBillingPlan],[IsMonthlyBillingFrequency],[ContractPeriod]
           ,[ChildAccountsVolume],[CustomFromAddress],[SuspendReason]
           ,[CustomDomain],[IsCustomDomainActive],[IsHideLogoInFooter],[IsHideLogoInLoginFooter],[IsNeedProfileManagement],[IsNeedPDFsToBeColorManaged],[IsRemoveAllGMGCollaborateBranding]
           ,[Status],[IsEnabledLog],[Creator],[CreatedDate],[Modifier],[ModifiedDate], [IsHelpCentreActive], [ContractStartDate], [Region], [IsTemporary])
    VALUES ('Head Office','GMG CoZone', 'gmgcozone.com', @GMGcoZone, NULL, 1, LOWER(NEWID()),
			'Marcus', 'Wright', '+49 7071 93874-0', 'marcus.wright@gmgcolor.com',
			NULL, NULL, NULL, 1, '',
			3, 3, 'W. Europe Standard Time', 1, 1,
			0, 0, 1, NULL, 1, 1,
			NULL, NULL, NULL,
			NULL,0, 0, 0, 1, 1, 1,
			1, 1, 1, CAST(GETUTCDATE() AS datetime2(7)), 1, CAST(GETUTCDATE() AS datetime2(7)), 0, CAST(GETUTCDATE() AS datetime2(7)), 'local', 0)
SET @AccountId = SCOPE_IDENTITY()

-- AccountHelpCentre
INSERT INTO [dbo].[AccountHelpCentre]
           ([Account],[ContactEmail],[ContactPhone],[SupportDays],[SupportHours],[UserGuideLocation],[UserGuideName],[UserGuideUpdatedDate])
     VALUES(@AccountId,'noreply@gmgcozone.com',NULL, NULL, NULL, NULL,NULL, NULL)

-- Company --
INSERT INTO [dbo].[Company]
           ([Account], [Name], [Number], [Address1], [Address2], [City], [Postcode], [State], [Phone], [Mobile], [Email], [Country])
     VALUES (@AccountId, 'GMG GmbH & Co. KG', 'A9F21', 'Moempelgarder Weg 10', NULL, 'Tuebingen', '72072', NULL, '+49 7071 93874-0', NULL, 'marcus.wright@gmgcolor.com',55)

-- User (HQ Administrator) --
INSERT INTO [dbo].[User]
           ([Account], [Status], [Username], [Password], [GivenName], [FamilyName], [EmailAddress], [PhotoPath]
           ,[ModifiedDate], [CreatedDate], [Creator], [Modifier]
           ,[Guid], [MobileTelephoneNumber], [HomeTelephoneNumber], [OfficeTelephoneNumber], [DateLastLogin], [NotificationFrequency], [Preset])
     VALUES (@AccountId, 1, 'Administrator',CONVERT(varchar(255), HashBytes('SHA1', 'password')),'Marcus','Wright','marcus.wright@gmgcolor.com',NULL,
			CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)), 1, 1, 
			LOWER(NEWID()), '+447850844724', NULL, '+49 7071 93874-0', NULL, 4, 1)
SET @UserId = SCOPE_IDENTITY()

-- Administrator Role --
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('HQA', 'Administrator', 1)
SET @RoleId = SCOPE_IDENTITY()
  
-- AccuntType Role --
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(1, @RoleId)
        
-- User Role --
INSERT INTO [dbo].[UserRole]([User],[Role])
     VALUES (@UserId, @RoleId)
	
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Creator] FOREIGN KEY([Creator])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Creator]
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Modifier] FOREIGN KEY([Modifier])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Modifier]
GO

ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Owner] FOREIGN KEY([Owner])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Owner]
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Account Types --
DECLARE @GMGcoZone int
DECLARE @Subsidiary int
DECLARE @Dealer int
DECLARE @Client int
DECLARE @Subscriber int

-- Account Types - GMG coZone
SET @GMGcoZone = (SELECT [ID] FROM [dbo].[AccountType] WHERE [Key] = 'GMHQ')

-- Account Types - Subsidiary
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('SBSY', 'Subsidiary')
SET @Subsidiary = SCOPE_IDENTITY()
  
-- Account Types - Dealer   
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('DELR', 'Dealer')
SET @Dealer = SCOPE_IDENTITY()

-- Account Types - Client   
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('CLNT', 'Client')
SET @Client = SCOPE_IDENTITY()

-- Account Types - Subscriber
INSERT INTO [dbo].[AccountType]([Key] ,[Name])
     VALUES('SBSC', 'Subscriber')
SET @Subscriber = SCOPE_IDENTITY()

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Roles --
DECLARE @GMGAdminRole int
DECLARE @GMGManagerRole int
DECLARE @AccountAdminRole int
DECLARE @AccountManagerRole int
DECLARE @AccountModeratorRole int
DECLARE @AccountContributorRole int
DECLARE @AccountViewerRole int

-- Role - GMG coZone Administrator
SET @GMGAdminRole = (SELECT [ID] FROM [dbo].[Role] WHERE [Key] = 'HQA')

-- Role - GMG coZone  Manager
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('HQM', 'Manager', 2)
SET @GMGManagerRole = SCOPE_IDENTITY()

-- Role - Account Administrator
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('ADM', 'Administrator', 1)
SET @AccountAdminRole = SCOPE_IDENTITY()

-- Role - Account Manager
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('MAN', 'Manager', 2)
SET @AccountManagerRole = SCOPE_IDENTITY()

-- Role - Moderator
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('MOD', 'Moderator', 3)
SET @AccountModeratorRole  = SCOPE_IDENTITY()

-- Role - Contributor
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('CON', 'Contributor', 4)
SET @AccountContributorRole   = SCOPE_IDENTITY() 

-- Role - Viewer
INSERT INTO [dbo].[Role] ([Key], [Name], [Priority])
     VALUES ('VIE', 'Viewer', 5)
SET @AccountViewerRole   = SCOPE_IDENTITY() 

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Account Type Role --
DECLARE @GMGCcoZoneGMGAdminRole int
DECLARE @GMGCcoZoneGMGManagerRole int

SET @GMGCcoZoneGMGAdminRole = (SELECT [ID] FROM [dbo].[AccountTypeRole] WHERE [AccountType] = @GMGcoZone AND [Role] = @GMGAdminRole)

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@GMGcoZone, @GMGManagerRole)     
SET @GMGCcoZoneGMGManagerRole   = SCOPE_IDENTITY() 


DECLARE @SubsidiaryAccountAdminRole int
DECLARE @SubsidiaryAccountManagerRole int
DECLARE @SubsidiaryAccountModeratorRole int
DECLARE @SubsidiaryAccountContributorRole int
DECLARE @SubsidiaryAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountAdminRole)
SET @SubsidiaryAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountManagerRole)
SET @SubsidiaryAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountModeratorRole)
SET @SubsidiaryAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountContributorRole)
SET @SubsidiaryAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subsidiary, @AccountViewerRole)
SET @SubsidiaryAccountViewerRole   = SCOPE_IDENTITY()


DECLARE @DealerAccountAdminRole int
DECLARE @DealerAccountManagerRole int
DECLARE @DealerAccountModeratorRole int
DECLARE @DealerAccountContributorRole int
DECLARE @DealerAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountAdminRole)
SET @DealerAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer , @AccountManagerRole)
SET @DealerAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountModeratorRole)
SET @DealerAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountContributorRole)
SET @DealerAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Dealer, @AccountViewerRole)
SET @DealerAccountViewerRole   = SCOPE_IDENTITY() 
     
     
DECLARE @ClientAccountAdminRole int
DECLARE @ClientAccountManagerRole int
DECLARE @ClientAccountModeratorRole int
DECLARE @ClientAccountContributorRole int
DECLARE @ClientAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountAdminRole)
SET @ClientAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountManagerRole)
SET @ClientAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountModeratorRole)
SET @ClientAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountContributorRole)
SET @ClientAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Client, @AccountViewerRole)
SET @ClientAccountViewerRole   = SCOPE_IDENTITY()     
     

DECLARE @SubscriberAccountAdminRole int
DECLARE @SubscriberAccountManagerRole int
DECLARE @SubscriberAccountModeratorRole int
DECLARE @SubscriberAccountContributorRole int
DECLARE @SubscriberAccountViewerRole int

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountAdminRole)
SET @SubscriberAccountAdminRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountManagerRole)
SET @SubscriberAccountManagerRole   = SCOPE_IDENTITY() 

INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountModeratorRole)
SET @SubscriberAccountModeratorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountContributorRole)
SET @SubscriberAccountContributorRole   = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[AccountTypeRole]([AccountType] ,[Role])
     VALUES(@Subscriber, @AccountViewerRole)
SET @SubscriberAccountViewerRole   = SCOPE_IDENTITY()    
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

DECLARE @ControllerActionId int
DECLARE @ParentMenuItemId int
DECLARE @ChildMenuItemId int
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- ADMIN SITE MENU ITEMS -----------------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Dashboard Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index (for admin app) --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Dashboard', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 1, 1, 1, 0, 1, 1, 'DBIN', 'Dashboard','Dashboard')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole) 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)

-- Index (for proof app) --	 
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, 0, 0, 1, 1, 1, 'DBAP', 'Administrator Site','Dashboard')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole) 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
/*-- End Dashboard Menu --------------------------------------------------------------------------------------------------------------------------------------------*/	 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Accounts Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index (for admin app) --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 2, 1, 1, 0, 1, 1, 'ACIN', 'Accounts', 'Account')
SET @ParentMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)

-- Index (for proof app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 0, 0, 1, 1, 1, 'ACST', 'Accounts', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
/*-- New Account ---------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- NewAccount --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'NewAccount','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACNW', 'New Account', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- NewAccountInfo--
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountInfo','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACAI', 'Account Info', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- NewAccountPlans --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountPlans','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACAP', 'Account Plans', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- NewAccountCustomise --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountCustomise','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACAC', 'Account Customise', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- NewAccountConfirm --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'AccountConfirm','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'ACCA', 'Confirm Account', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

/*-- Edit Account --------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Summary --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Summary','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, 1, 1, 0, 0, 1, 'ACSM', 'Summary', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole) 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- Profile --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Profile','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 1, 0, 0, 1, 'ACPF', 'Profile', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- Customise --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Customise','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, 1, 1, 0, 0, 1, 'ACCU', 'Customise', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
-- SiteSettings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'SiteSettings','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, 1, 1, 0, 0, 1, 'ACSS', 'Site Settings', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- WhiteLabel --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'WhiteLabel','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 5, 1, 1, 0, 0, 1, 'ACWL', 'White Labels', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- DNSAndCustomDomain --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'DNSAndCustomDomain','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 6, 1, 1, 0, 0, 1, 'ACDN', 'DNS & Custom Domain', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- Plans --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Plans','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 7, 1, 1, 0, 0, 1, 'ACPL', 'Plans', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)

-- BillingInfo --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'BillingInfo','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 8, 1, 1, 0, 0, 1, 'ACBI', 'Billing Info', 'Account') 
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 	 
-- Users --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'Users','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 9, 1, 1, 0, 0, 1, 'ACUS', 'Users', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- EditUser --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'EditUser','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 9, 1, 0, 0, 0, 0, 'ACEU', 'Edit User', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 
-- SuspendAccount --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Account', 'SuspendAccount','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 10, 1, 1, 0, 0, 1, 'ACSA', 'Suspend Account', 'Account')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
/*-- End Accounts Menu ---------------------------------------------------------------------------------------------------------------------------------------------*/	 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Plans Menu ----------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index ---
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Plans', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 3, 1, 1, 0, 1, 1, 'PLIN', 'Plans', 'Plans')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)	  
	 
-- NewPlan ---
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Plans', 'AddEditBillingPlan','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 1, 0, 0, 0, 0, 'PLBP', 'Add Edit Plan','Plans')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	
/*-- End Plans Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/	 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Reports Menu --------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 4, 1, 1, 0, 1, 1, 'RPIN', 'Reports','Reports')
SET @ParentMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)	 	 
	 	 
-- AccountReport --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'AccountReport','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 1, 0, 0, 1, 'RPAR', 'Account Reports','Reports')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
	 	 
-- UserReport --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'UserReport','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, 1, 1, 0, 0, 1, 'RPUR', 'User Reports','Reports')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
	 	 
-- BillingReport --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'BillingReport','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, 1, 1, 0, 0, 1, 'RPBR', 'Billing Reports','Reports')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
	  
-- Print --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Reports',  'Print','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, 1, 1, 0, 0, 1, 'RPPR', 'Print','Reports') 
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 	 
/*-- End Reports Menu ----------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Services Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Services',  'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 5, 1, 1, 0, 1, 1, 'SVIN', 'Services','Services')
SET @ParentMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 		 	 
	 	 
-- AccountReport --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Services',  'MediaServices','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 1, 0, 0, 1, 'SVMS', 'Media Services','Services')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
/*-- End Services Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Global Settings Menu -----------------------------------------------------------------------------------------------------------------------------------------*/
	 
--  Settings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings',  'Global','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 6, 1, 1, 0, 1, 1, 'GSET', 'Settings','Settings')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	

-- Global Settings --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, NULL, 1, 0, 0, 1, 'GSEP', 'Global Settings','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)
	  
-- Reserved Domains --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'ReservedDomains','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, NULL, 1, 0, 0, 1, 'SERD', 'Reserved Sub Domains','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
/*-- End Settings Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- Top Right Menu Items Under The Account -----------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Settings Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 1, NULL, 1, 1, 1, 1, 'SEIN', 'Account Settings','Settings')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)

-- Summary --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, NULL, 1, 0, 0, 1, 'SESM', 'Summary','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
     
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Profile --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Profile','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, NULL, 1, 0, 0, 1,'SEPF', 'Profile','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Customise --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Customise','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, NULL, 1, 0, 0, 1, 'SECU', 'Customise','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
	 	 
-- SiteSettings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'SiteSettings','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, NULL, 1, 0, 0, 1, 'SESS', 'SiteSettings','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
	 
-- WhiteLabelSettings --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'WhiteLabelSettings','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 5, NULL, 1, 0, 0, 1, 'SEWL', 'WhiteLabelSettings','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- DNSAndCustomDomain --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'DNSAndCustomDomain','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 6, NULL, 1, 0, 0, 1, 'SEDN', 'DNS & Custom Domain','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Users --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'Users','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 7, NULL, 1, 0, 0, 1, 'SEUS', 'Users','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

--Edit User--
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'EditUser','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 7, NULL, 0, 0, 0, 0, 'SEEU', 'Edit User','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
	 
--UserGroups--
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'UserGroups','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 8, NULL, 1, 0, 0, 1, 'SEGR', 'User Groups','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- Add Edit User Groups --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'AddEditUserGroup','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 8, NULL, 0, 0, 0, 0, 'SEEG', 'Add Edit User Group','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)

-- ExternalUsers --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Settings', 'ExternalUsers','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 9, NULL, 1, 0, 0, 1, 'SEEX', 'External Users','Settings')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
/*-- End Reports Menu ---------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- Top Right Menu Items Under The User Name ---------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Welcome Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index --
--INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
--	 VALUES ('Welcome', 'Index','')
--SET @ControllerActionId = SCOPE_IDENTITY()

--INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
--	 VALUES (@ControllerActionId, NULL, 1, NULL, 0, 1, 0, 1, 'WLIN', 'Welcome','Welcome')
--SET @ParentMenuItemId = SCOPE_IDENTITY()

--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	  
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
--INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
--	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)
/*-- End Welcome Menu ----------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Personal Settings Menu ----------------------------------------------------------------------------------------------------------------------------------------*/

-- Informaion --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 2, NULL, 0, 1, 0, 1, 'PSIN', 'Edit Profile','PersonalSettings')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)
	 
-- Information --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, NULL, 1, 0, 0, 1, 'PSPI', 'Profile Information','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()	

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)

-- Photo --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Photo','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, NULL, 1, 0, 0, 1, 'PSPH', 'Photo','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)

-- Password --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Password','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, NULL, 1, 0, 0, 1, 'PSPW', 'Password','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)
	 
-- Notifications --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('PersonalSettings', 'Notifications','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 4, NULL, 1, 0, 0, 1, 'PSNT', 'Notifications','PersonalSettings')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
/*-- End Personal Settings Menu ------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Help Menu -----------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Help', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 3, NULL, 0, 1, 0, 1, 'HLIN', 'Help','Help')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	  VALUES (@ParentMenuItemId, @GMGCcoZoneGMGAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @GMGCcoZoneGMGManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)	 
/*-- End Help Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*-- APPLICATION SITE MENU ITEMS ----------------------------------------------------------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Dashboard Menu -----------------------------------------------------------------------------------------------------------------------------------------------*/

-- Index (for proof app) --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Studio', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 1, 0, 1, 0, 1, 1, 'STIN', 'Dashboard','Studio')
SET @ParentMenuItemId = SCOPE_IDENTITY()
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)
	 
-- Index (for admin app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 1, 1, 0, 1, 1, 1, 'STAD', 'Application Site','Studio')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole) 
/*-- End Dashboard Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Flex Menu -----------------------------------------------------------------------------------------------------------------------------------------------------*/

-- Flex --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Studio', 'Flex','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 3, 0, 1, 0, 1, 0, 'STFL', 'Flex','Studio')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)
/*-- End Flex Menu -------------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
/*-- Approvals Menu ------------------------------------------------------------------------------------------------------------------------------------------------*/
	 
-- Index --	 
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Index','')
SET @ControllerActionId = SCOPE_IDENTITY()

-- Index (for proof app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, NULL, 2, 0, 1, 0, 1, 1, 'APIN', 'Approvals','Approvals')
SET @ParentMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubsidiaryAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @ClientAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ParentMenuItemId, @SubscriberAccountViewerRole)

-- Index (for admin app) --
INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 2, 1, 0, 1, 1, 1, 'APDB', 'Approvals','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole) 

-- Populate --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Populate','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APPO', 'Populate Approvals','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()
	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)	 

-- New Approval --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'NewApproval','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APNA', 'New Approval','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)

-- ApprovalDetails --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Details','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APDE', 'Approval Details','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)
	 
-- ApprovalDetails --
INSERT INTO [dbo].[ControllerAction] ([Controller], [Action], [Parameters])
	 VALUES ('Approvals', 'Annotations','')
SET @ControllerActionId = SCOPE_IDENTITY()

INSERT INTO [dbo].[MenuItem]([ControllerAction],[Parent],[Position],[IsAdminAppOwned],[IsAlignedLeft],[IsSubNav],[IsTopNav],[IsVisible],[Key],[Name],[Title])
	 VALUES (@ControllerActionId, @ParentMenuItemId, 0, 0, 0, 0, 0, 0, 'APDE', 'Approval Annotations','Approvals')
SET @ChildMenuItemId = SCOPE_IDENTITY()	 

INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubsidiaryAccountViewerRole)
	  
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @DealerAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountAdminRole)	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @ClientAccountViewerRole)
	 	 
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountAdminRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountManagerRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountModeratorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountContributorRole)
INSERT INTO [dbo].[MenuItemAccountTypeRole] ([MenuItem],[AccountTypeRole])
	 VALUES (@ChildMenuItemId, @SubscriberAccountViewerRole)	 	 
/*-- End Approvals Menu --------------------------------------------------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
-- End Studio Controler Actions --
/*------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- Notification Engine  --

DECLARE @GroupId int
DECLARE @EventTypeId int

-- Group Comments --
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('C', 'Comments')
SET @GroupId = SCOPE_IDENTITY()     
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'CCM', 'When a comment is made....') -- When a comment is made (reply to someone else comment- any comment)
SET @EventTypeId = SCOPE_IDENTITY()       
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'CRC', 'Replies to my comments') 
SET @EventTypeId = SCOPE_IDENTITY()       

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'CNC', 'New comments made')  
SET @EventTypeId = SCOPE_IDENTITY()       

-- Group Approvals --
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('A', 'Approvals')
SET @GroupId = SCOPE_IDENTITY()  

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAA', 'New approvals added') 
SET @EventTypeId = SCOPE_IDENTITY()
  
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'ADW', 'Approvals was downloaded') 
SET @EventTypeId = SCOPE_IDENTITY()
  
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAU', 'Approvals was updated')
SET @EventTypeId = SCOPE_IDENTITY()
 
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAD', 'Approvals was deleted') 
SET @EventTypeId = SCOPE_IDENTITY()
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AVC', 'New version was created') 
SET @EventTypeId = SCOPE_IDENTITY()
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'AAS', 'Approval was shared') 
SET @EventTypeId = SCOPE_IDENTITY()

-- Group Users --     
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('U', 'Users')
SET @GroupId = SCOPE_IDENTITY()  

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'UUA', 'User was added') 
SET @EventTypeId = SCOPE_IDENTITY()
    
-- Group Groups --     
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('G', 'Groups')
SET @GroupId = SCOPE_IDENTITY()  

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'GGC', 'Group was created')
SET @EventTypeId = SCOPE_IDENTITY()

-- Group Groups --     
INSERT INTO [dbo].[EventGroup]([Key],[Name])
     VALUES('M', 'Manage')
SET @GroupId = SCOPE_IDENTITY() 

INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'MPC', 'Plan was changed') 
SET @EventTypeId = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'MAD', 'Account was deleted')
SET @EventTypeId = SCOPE_IDENTITY() 
     
INSERT INTO [dbo].[EventType]([EventGroup],[Key],[Name])
     VALUES(@GroupId, 'MAA', 'Account was added') 
SET @EventTypeId = SCOPE_IDENTITY()

GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

-- RolePresetEventType --

--Low--

--Administrator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 1, 4)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 1, 4)
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 1, 4)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 1, 4)
--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 1, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 1, 4)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 1, 2)	
-------------------------------------------------------------------------------------------------------------------------------------------------
--Medium--

--Administrator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(1, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 2, 12)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(3, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 2, 10)
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(2, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 2, 9)
	
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(4, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 2, 9)
--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(5, 2, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 2, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 2, 8)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 2, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(6, 2, 3)
-------------------------------------------------------------------------------------------------------------------------------------------------------------
--High--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(1, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 3, 14)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(3, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 3, 14)
		
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(2, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 3, 11)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(4, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 3, 11)
--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(5, 3, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 3, 9)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 3, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 3, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(6, 3, 3)
------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Custom Preset --
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(1, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(1, 4, 14)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(3, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 11)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 12)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 13)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(3, 4, 14)
	
--Manager--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(2, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(2, 4, 11)

INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(4, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 5)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 6)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 7)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 9)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 10)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(4, 4, 11)

--Moderator--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(5, 4, 3)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 4)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 8)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(5, 4, 9)
--Contributor--
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 4, 1)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])
	VALUES(6, 4, 2)
INSERT INTO [dbo].[RolePresetEventType] ([Role], [Preset], [EventType])	
	VALUES(6, 4, 3)
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- Currency Rates --
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (1, '1', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (2, '0.626', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (3, '0.774', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (4, '78.566', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
INSERT INTO [dbo].[CurrencyRate] ([Currency], [Rate], [Creator], [Modifier], [CreatedDate], [ModifiedDate])
	VALUES (5, '0.9901', 1, 1,CAST(GETDATE() AS datetime2(7)),CAST(GETDATE() AS datetime2(7)))
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- NativeDataType --
INSERT INTO [dbo].[NativeDataType]([Type])
     VALUES('System.Boolean')
     INSERT INTO [dbo].[NativeDataType]([Type])
     VALUES('System.Int32')
INSERT INTO [dbo].[NativeDataType]([Type])
     VALUES('System.String')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- GlobalSettings --
INSERT INTO [dbo].[GlobalSettings]([Key],[DataType],[Value])
     VALUES ('STRB', 2, '30')
INSERT INTO [dbo].[GlobalSettings]([Key],[DataType],[Value])
     VALUES ('STCF', 2, '10')
INSERT INTO [dbo].[GlobalSettings]([Key],[DataType],[Value])
     VALUES ('STAF', 2, '10')
GO
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/	

-- Reserved Subdomains --
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('www')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('manage')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('assets')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('files')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('mail')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('docs')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('calendar')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('sites')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('ftp')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('git')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('ssl')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('support')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('status')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('blog')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('api')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('staging')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('lab')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('svn')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('collaborate')
INSERT INTO [dbo].[ReservedDomainName]([Name])
     VALUES('deliver')
GO

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/

INSERT INTO [dbo].[Shape]
           ([IsCustom]
           ,[FXG])
     VALUES
           (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
			<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
			  <Library/>
				  <Path winding="nonZero" d:userLabel="Arrow 1" data="M99.1699 49.6357 81.5918 32.0728C81.5918 32.0728 71.7354 41.9272 62.0293 51.6328L62.0293 0.000488281 37.1602 0 37.1602 51.6328 17.5781 32.0605 0 49.6357 49.5825 99.1738 99.1699 49.6357Z">
					<fill>
					  <SolidColor color="#000000"/>
					</fill>
				  </Path>
			  <Private/>
			</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M0 0 0 101.302 101.302 101.302 101.302 0 0 0ZM56.9824 75.9771 37.9878 75.9771 56.9824 56.9824 18.9941 56.9824 18.9941 44.3198 56.9824 44.3198 37.9878 25.3252 56.9824 25.3252 82.3081 50.6514 56.9824 75.9771Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M30.1045 96.335 30.1045 30.1045 48.167 30.1045 24.083 0 0 30.1045 18.0625 30.1045 18.0625 96.335 30.1045 96.335Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M89.3652 36.0716 34.7549 36.0663 55.5547 15.2611C58.9102 11.9051 58.7686 6.27475 55.2354 2.74155 51.7012 -0.786285 46.0781 -0.928864 42.7197 2.42758L12.834 32.3148 12.834 32.3148 0 45.1483 9.99805 55.1454 12.834 57.9823 12.834 57.9823 41.9883
 87.1419C45.2969 90.4383 50.875 90.2498 54.4072 86.722 57.9414 83.1893 58.1299 77.6053 54.8223 74.3089L34.7549 54.2298 89.3652 54.2245C93.4551 54.2298 96.8086 50.1414 96.8086 45.1424 96.8086 40.1497 93.4551 36.0663 89.3652 36.0716Z">
        <fill>
          <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M51.8833 0C23.231 0 0 23.231 0 51.8828 0 80.5371 23.231 103.767 51.8833 103.767 80.5371 103.767 103.768 80.5371 103.768 51.8828 103.768 23.231 80.5371 0 51.8833 0L51.8833 0Z">
        <fill>
         <SolidColor color="#000000"/>  
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M51.8828 6.48486C76.9131 6.48486 97.2822 26.8535 97.2822 51.8828 97.2822 76.9126 76.9131 97.2817 51.8828 97.2817 26.8535 97.2817 6.48438 76.9126 6.48438 51.8828 6.48438 26.8535 26.8535 6.48486 51.8828 6.48486M51.8828 0C23.2305 0 0 23.231 0 51.8828
 0 80.5366 23.2305 103.766 51.8828 103.766 80.5371 103.766 103.768 80.5366 103.768 51.8828 103.768 23.231 80.5371 0 51.8828 0L51.8828 0Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M51.8545 0C23.2197 0 0 23.2188 0 51.8535 0 80.4873 23.2197 103.707 51.8545 103.707 80.4883 103.707 103.708 80.4873 103.708 51.8535 103.708 23.2188 80.4883 0 51.8545 0ZM82.0898 68.7568 68.7578 82.0889 51.8545 65.1846 34.9502 82.0889 21.6182 68.7568
 38.5225 51.8535 21.6182 34.9492 34.9502 21.6172 51.8545 38.5215 68.7578 21.6172 82.0898 34.9492 65.1865 51.8535 82.0898 68.7568Z">
        <fill>
             <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029Z">
        <fill>
           <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M105.031 105.029 0 105.029 0 0 105.031 0 105.031 105.029ZM6.78809 98.2412 98.2422 98.2412 98.2422 6.78809 6.78809 6.78809 6.78809 98.2412Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M53.0586 0C23.7588 0 0 23.751 0 53.0498 0 82.3486 23.7588 106.101 53.0586 106.101 82.3486 106.101 106.108 82.3486 106.108 53.0498 106.108 23.751 82.3486 0 53.0586 0ZM50.5566 75.9795 46.79 80.0479 43.1807 75.8428 22.7051 52.0225 29.375 44.8311
 45.9707 57.0498 82.2412 27.2588 89.0059 34.4346 50.5566 75.9795Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
        <Path y="-0.000488281" winding="nonZero" data="M57.2407 12.7202 25.1294 12.7202 25.1294 0.000488281 0.000488281 25.7622 25.1294 50.8804 25.1294 38.1655 57.2407 38.1597 57.2407 12.7202Z">
          <fill>
             <SolidColor color="#000000"/>
          </fill>
        </Path>
        <Path x="44.5195" y="44.519" winding="nonZero" data="M57.2407 25.1167 31.7993 0.000488281 31.7993 12.7202 0.000488281 12.7202 0.000488281 38.1597 31.7993 38.1597 31.7993 50.8794 57.2407 25.1167Z">
          <fill>
            <SolidColor color="#000000"/>
          </fill>
        </Path>
  <Private/>
</Graphic>'), 
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M91.8105 73.4512 64.2646 45.8984 91.8037 18.3594 73.4375 0 45.8975 27.5391 18.3594 0 0 18.3594 27.5381 45.9121 0 73.4512 18.3594 91.8105 45.8975 64.2715 73.4443 91.8105 91.8105 73.4512Z">
        <fill>
             <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M101.585 38.0947 66.3799 38.0947 84.6211 10.5039 68.7354 0 50.7988 27.1328 32.8623 0 16.9639 10.5039 35.2168 38.0947 0 38.0947 0 57.1426 30.9521 57.1426 12.6982 84.7334 28.583 95.2373 50.793 61.6553 73.002 95.2373 88.8867 84.7334 70.6445 57.1426
 101.585 57.1426 101.585 38.0947Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		    (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero" data="M93.6289 0 36.5947 46.8457 10.5088 27.6289 0 38.9492 32.208 76.4043 37.8809 83.0039 43.7979 76.6104 104.254 11.2812 93.6289 0Z">
        <fill>
         <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'),
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path winding="nonZero"  data="M72.832 0.000488281 0 0.000488281 0 79.4536 72.832 79.4536 105.938 39.7271 72.832 0.000488281ZM63.8442 56.0396 57.4888 62.4028 41.4082 46.3218 25.3335 62.4028 18.9712 56.0396 35.0454 39.9595 18.9712 23.8853 25.3335 17.522 41.4082 33.603 57.4888
 17.522 63.8442 23.8853 47.77 39.9595 63.8442 56.0396Z">
        <fill>
            <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>'), 
		   (0
           ,'<?xml version="1.0" encoding="utf-8" ?>
<Graphic version="2.0" xmlns="http://ns.adobe.com/fxg/2008" xmlns:d="http://ns.adobe.com/fxg/2008/dt">
  <Library/>
      <Path data="M108.185 86.9669C105.212 81.6115 62.4178 7.59976 59.7469 3.0773 57.3543 -0.994965 51.6512 -1.05649 49.2567 3.0773 45.6141 9.36636 3.04184 83.0939 0.927582 86.7794 -1.70132 91.3849 1.64145 96.0402 6.15219 96.0402 9.62387 96.0402 97.9647 96.0402
 102.999 96.0402 107.951 96.0402 110.359 90.8976 108.185 86.9669ZM59.162 21.1408 57.7977 67.8019 51.1893 67.8019 49.8289 21.1408 59.162 21.1408ZM54.4969 87.7277 54.3963 87.7277C51.0956 87.7277 48.7586 85.1046 48.7586 81.7052 48.7586 78.2033 51.1893
 75.6759 54.4969 75.6759 57.994 75.6759 60.2303 78.2033 60.2303 81.7052 60.2303 85.1046 57.994 87.7277 54.4969 87.7277Z">
        <fill>
         <SolidColor color="#000000"/>
        </fill>
      </Path>
  <Private/>
</Graphic>')
GO
