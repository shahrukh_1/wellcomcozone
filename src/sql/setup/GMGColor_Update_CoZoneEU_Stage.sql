USE [GMGCoZone]
GO

-- HQ Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'release.gmgcozoneeu.com'
 WHERE [ID] = 1
GO

-- HQ Account --
UPDATE [dbo].[Account]
   SET [Region] = 'eu-west-1'
 WHERE [ID] = 1
GO

-- Subsidiary Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'subsidiary.release.gmgcozoneeu.com'
 WHERE [ID] = 2
GO

-- Dealer Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'dealer.release.gmgcozoneeu.com'
 WHERE [ID] = 3
GO

-- Client Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'client.release.gmgcozoneeu.com'
 WHERE [ID] = 4
GO

-- Subscriber Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'subscriber.release.gmgcozoneeu.com'
 WHERE [ID] = 5
GO

-- Chinese Simplified Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'chineses.release.gmgcozoneeu.com'
 WHERE [ID] = 6
GO

-- Chinese Traditional Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'chineset.release.gmgcozoneeu.com'
 WHERE [ID] = 7
GO

-- French Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'french.release.gmgcozoneeu.com'
 WHERE [ID] = 8
GO

-- German Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'german.release.gmgcozoneeu.com'
 WHERE [ID] = 9
GO

-- Italian Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'italian.release.gmgcozoneeu.com'
 WHERE [ID] = 10
GO

-- Korean Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'korean.release.gmgcozoneeu.com'
 WHERE [ID] = 11
GO

-- Japan Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'japan.release.gmgcozoneeu.com'
 WHERE [ID] = 12
GO

-- Portugal Account --

UPDATE [dbo].[Account]
   SET [Domain] = 'portugal.release.gmgcozoneeu.com'
 WHERE [ID] = 13
GO

-- Spain Account --
UPDATE [dbo].[Account]
   SET [Domain] = 'spain.release.gmgcozoneeu.com'
 WHERE [ID] = 14
GO

-- Reset Passwords -
UPDATE [dbo].[User]
   SET [Password] = CONVERT(varchar(255), HashBytes('SHA1', 'password'))
GO