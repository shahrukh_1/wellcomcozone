﻿using GMG.CoZone.Common.Module.Enums;
using GMG.CoZone.SSO.Interfaces.Auth0Interfaces;
using GMG.CoZone.SSO.Interfaces.SamlInterfaces;

namespace GMG.CoZone.SSO.Interfaces
{
    /// <summary>
    /// SSO core interface
    /// </summary>
    public interface ISsoCore
    {
        /// <summary>
        /// SSO Saml Plugin
        /// </summary>
        ISamlPlugin SamlPlugin { get; set; }

        /// <summary>
        /// Auth0 Plugin
        /// </summary>
        IAuth0Plugin Auth0Plugin { get; set; }

        /// <summary>
        /// Check if account has SSO enabled
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>True or False based on account settings</returns>
        bool IsSsoAccountEnabled(int accountId);

        /// <summary>
        /// Check if user is SSO user
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="userEmail">User Email</param>
        /// <returns>True or False bases on user</returns>
        bool IsSsoUser(int accountId, string userEmail);

        /// <summary>
        /// Returns the active configuration type for an account
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>Active configuration type</returns>
        SsoOptionsEnum ActiveSsoConfiguration(int accountId);
    }
}
