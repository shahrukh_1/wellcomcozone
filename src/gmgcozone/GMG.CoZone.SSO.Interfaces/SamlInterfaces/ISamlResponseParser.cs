﻿using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Interfaces.SamlInterfaces
{
    /// <summary>
    /// SSO Saml response parser interface
    /// </summary>
    public interface ISamlResponseParser
    {
        /// <summary>
        /// Parse IdP response
        /// </summary>
        /// <param name="response">IdP response</param>
        /// <param name="ssoSettings">Account sso settings</param>
        /// <returns>Response model</returns>
        SsoSamlResponseModel ParseResponse(string response, SsoSamlConfigurationModel ssoSettings);
    }
}
