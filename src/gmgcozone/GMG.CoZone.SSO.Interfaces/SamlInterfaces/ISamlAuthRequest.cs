﻿using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Interfaces.SamlInterfaces
{
    /// <summary>
    /// SSO Saml request interface
    /// </summary>
    public interface ISamlAuthRequest
    {
        /// <summary>
        /// Creates the request to be sent to the identity provider
        /// </summary>
        /// <param name="settings">Account SSO settings</param>
        /// <returns>The request as a string</returns>
        string GetRequest(SsoSamlConfigurationModel settings);
    }
}
