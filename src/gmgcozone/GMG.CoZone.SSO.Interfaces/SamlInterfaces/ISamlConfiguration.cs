﻿using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Interfaces.SamlInterfaces
{
    /// <summary>
    /// SSO Saml configuration interface
    /// </summary>
    public interface ISamlConfiguration
    {
        /// <summary>
        /// Load the account SSO settings
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="accountGuid">Account Guid</param>
        /// <param name="accountRegion">Account Region</param>
        /// <returns></returns>
        SsoSamlConfigurationModel LoadProprieties(int accountId, string accountGuid, string accountRegion);
    }
}
