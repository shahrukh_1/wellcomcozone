﻿using System.Security.Cryptography.X509Certificates;
using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Interfaces.SamlInterfaces
{
    /// <summary>
    /// SSO Saml certificate interface
    /// </summary>
    public interface ISamlCertificate
    {
        /// <summary>
        /// Load the account certificate
        /// </summary>
        /// <param name="ssoSettings">Account settings object</param>
        /// <returns></returns>
        X509Certificate2 Load(SsoSamlConfigurationModel ssoSettings);
    }
}
