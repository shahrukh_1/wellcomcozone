﻿using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Interfaces.SamlInterfaces
{
    /// <summary>
    /// SSO Saml plugin interface
    /// </summary>
    public interface ISamlPlugin
    {
        /// <summary>
        /// Get the identity providers url
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="accountGuid">Account Guid</param>
        /// <param name="accountRegion">Account Region</param>
        /// <returns>Idp url string</returns>
        string GetIdpUrl(int accountId, string accountGuid, string accountRegion);

        /// <summary>
        /// Parse the identity providers saml response
        /// </summary>
        /// <param name="samlResponse">Saml response string</param>
        /// <param name="accountId">Account Id</param>
        /// <param name="accountGuid">Account Guid</param>
        /// <param name="accountRegion">Account Region</param>
        /// <returns>Sso response model</returns>
        SsoSamlResponseModel ParseResponse(string samlResponse, int accountId, string accountGuid, string accountRegion);
    }
}
