﻿using GMG.CoZone.SSO.DomainModels;

namespace GMG.CoZone.SSO.Interfaces.Auth0Interfaces
{
    /// <summary>
    /// SSO Auth0 plugin interface
    /// </summary>
    public interface IAuth0Plugin
    {
        SsoAuth0SettingsModel GetSettings(string hostName);
    }
}
