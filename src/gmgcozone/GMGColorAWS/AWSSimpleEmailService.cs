﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;

namespace GMGColor.AWS
{
    public static class AWSSimpleEmailService
    {
        private static AmazonSESService amazonSESService;

        private static AmazonSESService ServiceClient(string AWSAccessKey, string AWSSecretKey)
        {
            if (amazonSESService == null)
            {
                amazonSESService = new AmazonSESService(AWSAccessKey, AWSSecretKey);
            }
            return amazonSESService;

        }

        /// <summary>
        /// Verification email will be send to the given email address.
        /// Click link in email to verify
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static bool VerifyEmailAddress(string emailAddress, string AWSAccessKey, string AWSSecretKey)
        {
            try
            {
                bool verified = ServiceClient(AWSAccessKey, AWSSecretKey).VerifyEmailAddress(emailAddress);
                return verified;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SentEmailResult SendEmail(Email email, string AWSAccessKey, string AWSSecretKey)
        {
            return ServiceClient(AWSAccessKey, AWSSecretKey).SendEmail(email);
        }

        public static List<string> GetVerifiedEmailAddresses()
        {
            return amazonSESService.ListVerifiedEmailAddresses();
        }

        public static SendQuota GetSendQuotaInformation(string AWSAccessKey, string AWSSecretKey)
        {
            return ServiceClient(AWSAccessKey, AWSSecretKey).GetSendQuotaInformation();
        }
    }

    public class Email
    {
        public List<string> ToAddresseList { get; set; }
        public List<string> CCAddresseList { get; set; }
        public List<string> BCCAdressList { get; set; }
        public string SenderEmailAddress { get; set; }
        public string ReplyToEmailAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Text { get; set; }
    }

    public class SentEmailResult
    {
        public Exception ErrorException { get; set; }
        public string MessageId { get; set; }
        public bool HasError { get; set; }

        public SentEmailResult()
        {
            this.HasError = false;
            this.ErrorException = null;
            this.MessageId = string.Empty;
        }
    }

    /// <summary>
    /// Send Quota Response 
    /// </summary>
    public class SendQuota
    {
        public double Max24HourSend { get; set; }
        public double MaxSendRate { get; set; }
        public double SentLast24Hours { get; set; }
    }

    internal class AmazonSESService
    {
        private AmazonSimpleEmailServiceClient client;

        private string AccessKey { get; set; }
        private string SecretKey { get; set; }
        private string IdentityNotificationTopic { get; set; }

        internal AmazonSESService()
        {
        }

        internal AmazonSESService(string accessKey, string secretKey)
        {
            this.AccessKey = accessKey;
            this.SecretKey = secretKey;
            client = new Amazon.SimpleEmail.AmazonSimpleEmailServiceClient(accessKey, secretKey);
        }

        /// <summary>
        /// Send Email with specified email object
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        internal SentEmailResult SendEmail(Email email)
        {
            return SendEmail(email.ToAddresseList, email.CCAddresseList, email.BCCAdressList, email.SenderEmailAddress,
                email.ReplyToEmailAddress, email.Subject, email.Body);
        }

        /// <summary>
        /// Send Email Via Amazon SES
        /// </summary>
        /// <param name="awsAccessKey"></param>
        /// <param name="awsSecretKey"></param>
        /// <param name="to">List of strings TO address collection</param>
        /// <param name="cc">List of strings CCC address collection</param>
        /// <param name="bcc">List of strings BCC address collection</param>
        /// <param name="senderEmailAddress">Sender email. Must be verified before sending.</param>
        /// <param name="replyToEmailAddress">Reply to email.</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="body">Mail Body</param>
        /// <returns></returns>
        private SentEmailResult SendEmail(List<string> to, List<string> cc, List<string> bcc, string senderEmailAddress, string replyToEmailAddress, string subject, string body)
        {
            SentEmailResult result = new SentEmailResult();

            try
            {
                List<string> listColTo = new List<string>();
                listColTo.AddRange(to);

                Destination destinationObj = new Destination();
                destinationObj.ToAddresses.AddRange(listColTo);

                if (cc != null)
                {
                    List<string> listColCc = new List<string>();
                    listColCc.AddRange(cc);
                    destinationObj.BccAddresses.AddRange(listColCc);
                }

                if (bcc != null)
                {
                    List<string> listColBcc = new List<string>();
                    listColBcc.AddRange(bcc);
                    destinationObj.CcAddresses.AddRange(listColBcc);
                }

                SendEmailRequest mailObj = new SendEmailRequest();
                mailObj.Destination = destinationObj;
                mailObj.Source = senderEmailAddress;
                mailObj.ReturnPath = replyToEmailAddress;

                ////Create Message
                Content emailSubjectObj = new Content(subject);
                Content emailBodyContentObj = new Content(body);

                //Create email body object
                Body emailBodyObj = new Body();
                emailBodyObj.Html = emailBodyContentObj;
                emailBodyObj.Text = emailBodyContentObj;

                //Create message
                Message emailMessageObj = new Message(emailSubjectObj, emailBodyObj);
                                mailObj.Message = emailMessageObj;

                //Send Message
                SendEmailResponse response = client.SendEmail(mailObj);
                result.MessageId = response.MessageId;
            }
            catch (Exception ex)
            {
                //If any error occurs, HasError flag will be set to true.
                result.HasError = true;
                result.ErrorException = ex;
            }

            return result;
        }

        /// <summary>
        /// Send a verification email to specified email. Amazon SES needs to a verified email in order to use it as a sender email.
        /// When this function calls, a verification email will be sent to specified email. You need to click the verification link on the upcoming email.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        internal bool VerifyEmailAddress(string email)
        {
            bool result = false;

            VerifyEmailAddressRequest request = new VerifyEmailAddressRequest();
            VerifyEmailAddressResponse response = new VerifyEmailAddressResponse();

            if (client != null)
            {

                request.EmailAddress = email.Trim();
                response = client.VerifyEmailAddress(request);

                if (!string.IsNullOrEmpty(response.ResponseMetadata.RequestId))
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Lists the verified sender emails 
        /// </summary>
        /// 
        internal List<string> ListVerifiedEmailAddresses()
        {
            //client = new Amazon.SimpleEmail.AmazonSimpleEmailServiceClient(secretKey, secretKey);
            ListVerifiedEmailAddressesRequest request = new ListVerifiedEmailAddressesRequest();
            ListVerifiedEmailAddressesResponse response = new ListVerifiedEmailAddressesResponse();

            List<string> verifiedEmailList = new List<string>();
            response = client.ListVerifiedEmailAddresses(request);
            if (client != null)
            {
                if (response != null)
                {
                    if (response.VerifiedEmailAddresses != null)
                    {
                        verifiedEmailList.AddRange(response.VerifiedEmailAddresses);
                    }
                }
            }

            return verifiedEmailList;
        }

        /// <summary>
        /// Get Send Qouta information from Amazon
        /// </summary>
        /// 
        internal SendQuota GetSendQuotaInformation()
        {
            SendQuota sendQuota = new SendQuota();
            GetSendQuotaRequest request = new GetSendQuotaRequest();
            GetSendQuotaResponse response = new GetSendQuotaResponse();

            if (client != null)
            {
                response = client.GetSendQuota(request);

                if (!string.IsNullOrEmpty(response.ResponseMetadata.RequestId))
                {
                    if (response != null)
                    {
                        sendQuota.Max24HourSend = response.Max24HourSend;

                        sendQuota.MaxSendRate = response.MaxSendRate;

                        sendQuota.SentLast24Hours = response.SentLast24Hours;
                    }
                }
            }

            return sendQuota;
        }
    }
}
