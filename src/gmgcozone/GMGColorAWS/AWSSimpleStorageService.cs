﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3.IO;

namespace GMGColor.AWS
{
    public class AWSSimpleStorageService
    {
        #region Fields

        private static AmazonS3Client amazoneS3Client;

        #endregion

        #region Private Constants

        private const string ExpiresKey = "Expires";
        #endregion

        #region Events

        #endregion

        #region Private Methods

        private static AmazonS3Client S3Client(string AWSAccessKey, string AWSSecretKey)
        {
            return amazoneS3Client ?? (amazoneS3Client = new AmazonS3Client(AWSAccessKey, AWSSecretKey));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        private static IEnumerable<string> GetFiles(string Path)
        {
            Queue<string> queue = new Queue<string>();
            queue.Enqueue(Path);

            while (queue.Count > 0)
            {
                Path = queue.Dequeue();
                try
                {
                    foreach (string subDir in Directory.GetDirectories(Path))
                    {
                        queue.Enqueue(subDir);
                    }
                }
                catch (Exception)
                {

                }

                string[] files = null;
                try
                {
                    files = Directory.GetFiles(Path);
                }
                catch (Exception)
                {

                }

                if (files != null)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        yield return files[i];
                    }
                }
            }
        }

        #endregion

        #region Get Methods

        /// <summary>
        /// Gets all files.
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <param name="awsAccessKey">The AWS access key.</param>
        /// <param name="awsSecretKey">The AWS secret key.</param>
        /// <param name="awsPrefix">The aws prefix.</param>
        /// <returns></returns>
        public static List<string> GetAllFiles(string bucketName, string awsAccessKey, string awsSecretKey, string awsPrefix)
        {
            List<string> files = new List<string>();
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            request.Prefix = awsPrefix;

            do
            {
                ListObjectsResponse response = S3Client(awsAccessKey, awsSecretKey).ListObjects(request);
                files =
                    response.S3Objects.Where(
                        o => Path.GetExtension(o.Key) != null && Path.GetExtension(o.Key) != "").Select(
                            o => o.Key).ToList();
                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);

            return files;
        }

        public static List<S3Object> GetObjects(string bucketName, string awsAccessKey, string awsSecretKey, string awsPrefix)
        {
            List<S3Object> files = new List<S3Object>();
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            request.Prefix = awsPrefix;

            do
            {
                ListObjectsResponse response = S3Client(awsAccessKey, awsSecretKey).ListObjects(request);
                files.AddRange(response.S3Objects.Where(
                    o => Path.GetExtension(o.Key) != null && Path.GetExtension(o.Key) != "").ToList());

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);

            return files;
        }

        public static List<string> GetListObjects(string bucketName, string awsAccessKey, string awsSecretKey, string awsPrefix)
        {
            List<string> files = new List<string>();
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            request.Prefix = awsPrefix;

            do
            {
                ListObjectsResponse response = S3Client(awsAccessKey, awsSecretKey).ListObjects(request);
                files.AddRange(response.S3Objects.Where(
                    o => Path.GetExtension(o.Key) != null && Path.GetExtension(o.Key) != "").Select(o=>o.Key.ToString()).ToList());

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);

            return files;
        }


        /// <summary>
        /// Get root folders
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="awsAccessKey"></param>
        /// <param name="awsSecretKey"></param>
        /// <returns></returns>
        public static List<string> GetAllFolders(string bucketName, string awsAccessKey, string awsSecretKey)
        {
            List<string> lstFolders = new List<string>();

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;

            ListObjectsResponse response = S3Client(awsAccessKey, awsSecretKey).ListObjects(request);

            foreach (S3Object entry in response.S3Objects)
            {
                string folder = Path.GetDirectoryName(entry.Key);
                if (!String.IsNullOrEmpty(folder) && !lstFolders.Contains(folder))
                {
                    lstFolders.Add(folder);
                }
            }

            return lstFolders;
        }


        /// <summary>
        /// Gets all folders.
        /// </summary>
        /// <param name="bucketName">Name of the bucket.</param>
        /// <param name="awsAccessKey">The aws access key.</param>
        /// <param name="awsSecretKey">The aws secret key.</param>
        /// <param name="awsFolderPath">The aws folder path.</param>
        /// <param name="oneLevel"> </param>
        /// <returns></returns>
        public static List<string> GetAllFolders(string bucketName, string awsAccessKey, string awsSecretKey, string awsFolderPath, bool oneLevel = true)
        {
            List<string> lstFolders = new List<string>();

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = bucketName;
            request.Prefix = awsFolderPath;
            if (oneLevel)
            {
                request.Delimiter = "/";
            }

            // get all pages of responses
            do
            {
                ListObjectsResponse response = S3Client(awsAccessKey, awsSecretKey).ListObjects(request);

                lstFolders.AddRange(response.CommonPrefixes);

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);


            return lstFolders;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static MemoryStream GetFileStream(string BucketName, string FileName, string AWSAccessKey, string AWSSecretKey)
        {
            MemoryStream file = new MemoryStream();

            GetObjectRequest request = new GetObjectRequest();
            request.BucketName = BucketName;
            request.Key = FileName.Replace("\\", "/");

            using (GetObjectResponse response = S3Client(AWSAccessKey, AWSSecretKey).GetObject(request))
            {
                using (BufferedStream stream2 = new BufferedStream(response.ResponseStream))
                {
                    byte[] buffer = new byte[0x2000];
                    int count = 0;
                    while ((count = stream2.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        file.Write(buffer, 0, count);
                    }
                    file.Seek(0, SeekOrigin.Begin);
                    return file;
                }
            }
        }

        /// <summary>
        /// Gets a chunk in the requester range from the file in the specified Bucket
        /// </summary>
        /// <param name="BucketName">The Amazon Bucket Name</param>
        /// <param name="FileName">File Name from the specified Bucket</param>
        /// <param name="AWSAccessKey">AWS Access Key</param>
        /// <param name="AWSSecretKey">AWS Secret Key</param>
        /// <returns>The byte array containing the requested chunk</returns>
        public static byte[] GetFileStreamByteRange(string BucketName, string FileName, string AWSAccessKey, string AWSSecretKey, long start, long stop)
        {
            GetObjectRequest request = new GetObjectRequest();
            request.BucketName = BucketName;
            request.Key = FileName.Replace("\\", "/");
            request.ByteRange = new ByteRange(start, stop);

            using (GetObjectResponse response = S3Client(AWSAccessKey, AWSSecretKey).GetObject(request))
            {
                using (Stream responseStream = response.ResponseStream)
                {
                    byte[] buffer = new byte[16 * 1024];
                    using (MemoryStream ms = new MemoryStream())
                    {
                        int read;
                        while ((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            ms.Write(buffer, 0, read);
                        }
                        return ms.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Get folders that contatins the given folder name
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FolderPath"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static List<string> GetSubFolders(string BucketName, string FolderPath, string AWSAccessKey, string AWSSecretKey)
        {
            List<string> lstSubFolders = new List<string>();

            foreach (string directoryPath in AWSSimpleStorageService.GetAllFolders(BucketName, AWSAccessKey, AWSSecretKey))
            {
                if (directoryPath.Contains(FolderPath.ToLower()))
                {
                    string folder = Path.GetDirectoryName(directoryPath.Replace(FolderPath.ToLower(), string.Empty));
                    if (!String.IsNullOrEmpty(folder) && !lstSubFolders.Contains(folder))
                    {
                        lstSubFolders.Add(folder);
                    }
                }
            }

            return lstSubFolders;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static DateTime GetS3ObjectModifiedDate(string BucketName, string FileName, string AWSAccessKey, string AWSSecretKey)
        {
            DateTime lastModified = DateTime.Now;

            if (AWSSimpleStorageService.FileExistsInBucket(BucketName, FileName, AWSAccessKey, AWSSecretKey))
            {
                GetObjectMetadataRequest metadataReq = new GetObjectMetadataRequest()
                {
                    BucketName = BucketName,
                    Key = FileName
                };

                GetObjectMetadataResponse response = S3Client(AWSAccessKey, AWSSecretKey).GetObjectMetadata(metadataReq);
                lastModified = response.LastModified;
            }
            return lastModified;
        }

        #endregion

        #region IO Methods

        /// <summary>
        /// Create a new folder inside the S3 bucket
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="DirectoryPath"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <param name="expireDate"></param>
        /// <returns></returns>
        public static bool CreateNewFolder(string BucketName, string DirectoryPath, string AWSAccessKey, string AWSSecretKey, DateTime? expireDate = null)
        {
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BucketName;
            request.Key = DirectoryPath.Replace("\\", "/");
            request.ContentBody = string.Empty;

            if (expireDate.HasValue)
            {
                request.Headers[ExpiresKey] =  expireDate.Value.ToString("R");// TODO - check for alternatives (the AddHeader method seems to have no effect)
            }

            var responce = S3Client(AWSAccessKey, AWSSecretKey).PutObject(request);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="Prefix"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        public static void DeleteAllFiles(string BucketName, string Prefix, string AWSAccessKey, string AWSSecretKey)
        {
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = BucketName;
            request.Prefix = Prefix;

            do
            {
                ListObjectsResponse response = S3Client(AWSAccessKey, AWSSecretKey).ListObjects(request);
                Parallel.ForEach(response.S3Objects, entry => AWSSimpleStorageService.FileExistsInBucketThenDelete(BucketName, entry.Key, AWSAccessKey, AWSSecretKey));

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }

            } while (request != null);

            AWSSimpleStorageService.FolderExistsInBucketThenDelete(BucketName, Prefix, AWSAccessKey, AWSSecretKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool DeleteAnObjectFromBucket(string BucketName, string FileName, string AWSAccessKey, string AWSSecretKey)
        {
            DeleteObjectRequest request = new DeleteObjectRequest();
            request.BucketName = BucketName;
            request.Key = FileName;

            DeleteObjectResponse response = S3Client(AWSAccessKey, AWSSecretKey).DeleteObject(request);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool FileExistsInBucket(string BucketName, string FileName, string AWSAccessKey, string AWSSecretKey)
        {
            try
            {
                FileName = FileName.Replace("\\", "/");

                GetObjectMetadataRequest metadataReq = new GetObjectMetadataRequest()
                {
                    BucketName = BucketName,
                    Key = FileName
                };

                GetObjectMetadataResponse response = S3Client(AWSAccessKey, AWSSecretKey).GetObjectMetadata(metadataReq);
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;

                // Status wasn't not found, so throw the exception
                throw;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        public static bool FileExistsInBucketThenDelete(string BucketName, string FileName, string AWSAccessKey, string AWSSecretKey)
        {
            bool success = false;

            if (AWSSimpleStorageService.FileExistsInBucket(BucketName, FileName, AWSAccessKey, AWSSecretKey))
            {
                success = AWSSimpleStorageService.DeleteAnObjectFromBucket(BucketName, FileName, AWSAccessKey, AWSSecretKey);
            }

            return success;
        }

        /// <summary>
        /// Folder Exists in bucket
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FolderName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool FolderExistsInBucket(string BucketName, string FolderName, string AWSAccessKey, string AWSSecretKey)
        {
            try
            {   
                FolderName = FolderName.Replace("\\", "/");
                var request = new ListObjectsRequest
                {
                    BucketName = BucketName,
                    Delimiter = "/",
                    Prefix = FolderName.EndsWith("/") ? FolderName.Remove(FolderName.Length - 1) : FolderName
                };

                return S3Client(AWSAccessKey, AWSSecretKey).ListObjects(request).CommonPrefixes.Any();
            }

            catch (Amazon.S3.AmazonS3Exception ex)
            {
                if (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return false;
                
                // Status wasn't not found, so throw the exception
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="FolderName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool FolderExistsInBucketThenDelete(string BucketName, string FolderName, string AWSAccessKey, string AWSSecretKey)
        {
            bool IsDeleted = false;

            if (AWSSimpleStorageService.FolderExistsInBucket(BucketName, FolderName, AWSAccessKey, AWSSecretKey))
            {

                DeleteObjectRequest request = new DeleteObjectRequest();
                request.BucketName = BucketName;
                request.Key = FolderName;

                DeleteObjectResponse response = S3Client(AWSAccessKey, AWSSecretKey).DeleteObject(request);

                IsDeleted = true;
            }
            return IsDeleted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="SourceFilePath"></param>
        /// <param name="DestinationFilePath"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool MoveFileInsideBucket(string BucketName, string SourceFilePath, string DestinationFilePath, string AWSAccessKey, string AWSSecretKey)
        {
            CopyObjectRequest copyRequest = new CopyObjectRequest();
            copyRequest.SourceBucket = BucketName;
            copyRequest.DestinationBucket = BucketName;
            copyRequest.SourceKey = SourceFilePath;
            copyRequest.DestinationKey = DestinationFilePath;
            copyRequest.CannedACL = S3CannedACL.PublicReadWrite;

            CopyObjectResponse responseWithMetadata = S3Client(AWSAccessKey, AWSSecretKey).CopyObject(copyRequest);

            AWSSimpleStorageService.FileExistsInBucketThenDelete(BucketName, SourceFilePath, AWSAccessKey, AWSSecretKey);

            return true;
        }

        public static bool MoveFolderInsideBucket(string BucketName, string SourceFilePath, string DestinationFilePath, string AWSAccessKey, string AWSSecretKey)
        {
            AmazonS3Client s3Client1 = new AmazonS3Client(AWSAccessKey, AWSSecretKey, Amazon.RegionEndpoint.APSoutheast1);
            var listObjectRequest = new ListObjectsRequest();
            listObjectRequest.BucketName = BucketName;
            listObjectRequest.Prefix = SourceFilePath;

            var listObjectsResult = s3Client1.ListObjects(listObjectRequest);
            List<System.Threading.Tasks.Task> AsyncList = new List<System.Threading.Tasks.Task>();
            // copy each file
            foreach (var file in listObjectsResult.S3Objects)
            {
                AsyncList.Add(System.Threading.Tasks.Task.Factory.StartNew(() => CopyingObjectAsync(BucketName, file.Key, BucketName, file.Key.Replace(DestinationFilePath,"approvals/"), s3Client1)));
            }
            System.Threading.Tasks.Task.WhenAll(AsyncList);

            listObjectRequest.Prefix = DestinationFilePath;
            var recheck_DestinationFile = s3Client1.ListObjects(listObjectRequest);

            AsyncList.Clear();
            if (listObjectsResult.S3Objects.Count != recheck_DestinationFile.S3Objects.Count)
            {
                // copy each file
                foreach (var file in listObjectsResult.S3Objects)
                {
                    AsyncList.Add(System.Threading.Tasks.Task.Factory.StartNew(() => CopyingObjectAsync(BucketName, file.Key, BucketName, file.Key.Replace(DestinationFilePath, "approvals/"), s3Client1)));
                }
                System.Threading.Tasks.Task.WhenAll(AsyncList);
            }
            System.Threading.Thread.Sleep(1500);

            //Amazon.S3.IO.S3DirectoryInfo sourcePath = new Amazon.S3.IO.S3DirectoryInfo(s3Client1, BucketName, SourceFilePath);
            //sourcePath.Delete(true);

            return true;
        }

        private static async Task CopyingObjectAsync(string sourceBucket, string objectKey,string destinationBucket, string destObjectKey, AmazonS3Client s3Client)
        {
            try
            {
                CopyObjectRequest request = new CopyObjectRequest
                {
                    SourceBucket = sourceBucket,
                    SourceKey = objectKey,
                    DestinationBucket = destinationBucket,
                    DestinationKey = destObjectKey,
                    CannedACL= S3CannedACL.PublicReadWrite
            };
                await s3Client.CopyObjectAsync(request);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="SourceFilePath"></param>
        /// <param name="DestinationFilePath"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <param name="expireDate"></param>
        /// <returns></returns>
        public static void CopyFileInsideBucket(string BucketName, string SourceFilePath, string DestinationFilePath, string AWSAccessKey, string AWSSecretKey, DateTime? expireDate = null)
        {
            CopyObjectRequest copyRequest = new CopyObjectRequest();
            copyRequest.SourceBucket = BucketName;
            copyRequest.DestinationBucket = BucketName;
            copyRequest.SourceKey = SourceFilePath;
            copyRequest.DestinationKey = DestinationFilePath;
            copyRequest.CannedACL = S3CannedACL.PublicReadWrite;
            copyRequest.Timeout = TimeSpan.FromHours(2);

            if (expireDate.HasValue)
            {
                copyRequest.Metadata.Add(ExpiresKey, expireDate.Value.ToString("R"));
            }
            var response = S3Client(AWSAccessKey, AWSSecretKey).CopyObject(copyRequest);

            if(response.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception(String.Format("Failed to copy file: {0}. Status code: {1}", SourceFilePath, response.HttpStatusCode));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="Prefix"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        public static void CopyAllFilesInsideBucket(string BucketName, string Prefix, string destinationFolder, string AWSAccessKey, string AWSSecretKey)
        {
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = BucketName;
            request.Prefix = Prefix;

            do
            {
                ListObjectsResponse response = S3Client(AWSAccessKey, AWSSecretKey).ListObjects(request);

                Parallel.ForEach(response.S3Objects, 
                    entry => 
                        CopyFileInsideBucket(BucketName, entry.Key, destinationFolder + entry.Key, AWSAccessKey, AWSSecretKey)
                    );

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }

            } while (request != null);
        }

        /// <summary>
        /// Reads the given file. ex-: .txt,.html etc...
        /// <b>targetFilePath shold be in the format 'folder/file.txt'</b>
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <param name="targetPath">should be in the format 'folder/file.txt'</param>
        /// <returns>file content</returns>
        public static string ReadFile(string BucketName, string TargetFilePath, string AWSAccessKey, string AWSSecretKey)
        {
            String content = string.Empty;

            TargetFilePath = (TargetFilePath.StartsWith("/")) ? TargetFilePath.Remove(0, 1) : TargetFilePath;

            AmazonS3Client client = new AmazonS3Client(AWSAccessKey, AWSSecretKey);
            GetObjectRequest request = new GetObjectRequest();

            request.BucketName = BucketName;
            request.Key = TargetFilePath;

            GetObjectResponse response = client.GetObject(request);
            StreamReader reader = new StreamReader(response.ResponseStream);

            content = reader.ReadToEnd();
            reader.Close();

            return content;
        }

        #endregion

        #region Upload Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="SourceFilePath"></param>
        /// <param name="DestinationFilePath"></param>
        /// <param name="DestinationFileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool UploadFile(string BucketName, string SourceFilePath, string DestinationFilePath, string AWSAccessKey, string AWSSecretKey)
        {
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BucketName;
            request.FilePath = SourceFilePath;
            request.Key = DestinationFilePath.Replace("\\", "/");
            request.CannedACL = S3CannedACL.PublicReadWrite;
            request.Timeout = new TimeSpan(1,0,0); // One Hour

            PutObjectResponse responseWithMetadata = S3Client(AWSAccessKey, AWSSecretKey).PutObject(request);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>        
        /// /// <param name="DestinationFilePath"></param>
        /// <param name="InputStreem"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool UploadFile(string BucketName, string DestinationFilePath, Stream InputStreem, long contentLengh, string AWSAccessKey, string AWSSecretKey)
        {
            PutObjectRequest request = new PutObjectRequest();

            request.BucketName = BucketName;
            request.CannedACL = S3CannedACL.PublicReadWrite;
            request.Key = DestinationFilePath.Replace("\\", "/");
            request.InputStream = InputStreem;
            request.ReadWriteTimeout = new TimeSpan(1, 0, 0); // 1 Hour
            request.Timeout = new TimeSpan(24, 0, 0); //24 h

            PutObjectResponse response = S3Client(AWSAccessKey, AWSSecretKey).PutObject(request);

            return true;
        }

        public static async Task<bool> UploadFileAync(string BucketName, string DestinationFilePath, Stream InputStreem, long contentLengh, string AWSAccessKey, string AWSSecretKey)
        {
            PutObjectRequest request = new PutObjectRequest();

            request.BucketName = BucketName;
            request.CannedACL = S3CannedACL.PublicReadWrite;
            request.Key = DestinationFilePath.Replace("\\", "/");
            request.InputStream = InputStreem;
            request.ReadWriteTimeout = new TimeSpan(1, 0, 0); // 1 Hour
            request.Timeout = new TimeSpan(24, 0, 0); //24 h

            await S3Client(AWSAccessKey, AWSSecretKey).PutObjectAsync(request);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="DestinationFilePath"></param>
        /// <param name="Content"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool UploadFileWithContent(string BucketName, string DestinationFilePath, string Content, string AWSAccessKey, string AWSSecretKey)
        {
            PutObjectRequest request = new PutObjectRequest();
            request.BucketName = BucketName;
            request.ContentBody = Content;
            request.Key = DestinationFilePath;
            request.CannedACL = S3CannedACL.PublicReadWrite;

            PutObjectResponse responseWithMetadata = S3Client(AWSAccessKey, AWSSecretKey).PutObject(request);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="SourceFolderPath"></param>
        /// <param name="DestinationFolderPath"></param>
        /// <param name="DestinationFileName"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool UploadFolder(string BucketName, string SourceFolderPath, string DestinationFolderPath, string AWSAccessKey, string AWSSecretKey)
        {
            TransferUtility utility = new TransferUtility(AWSAccessKey, AWSSecretKey);

            TransferUtilityUploadDirectoryRequest request = new TransferUtilityUploadDirectoryRequest();
            request.BucketName = BucketName;
            request.Directory = SourceFolderPath;
            request.KeyPrefix = DestinationFolderPath;
            request.CannedACL = S3CannedACL.PublicRead;

            utility.UploadDirectory(request);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="SourceFolderPath"></param>
        /// <param name="ProgressText"></param>
        /// <param name="ProgressFilePath"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static bool UploadFolderByFile(string BucketName, string SourceFolderPath, string destinationFolderRelativePath, string AWSAccessKey, string AWSSecretKey)
        {
            foreach (string sourceFilePath in AWSSimpleStorageService.GetFiles(SourceFolderPath))
            {
                string filePath = sourceFilePath.Replace(SourceFolderPath, string.Empty);
                string relativeFileURL = destinationFolderRelativePath + filePath.Replace(@"\", "/");

                AWSSimpleStorageService.UploadFile(BucketName, sourceFilePath, relativeFileURL, AWSAccessKey, AWSSecretKey);
            }

            return true;
        }

        // returns the uploaded file names
        // upload the files that match regular expresions except the uploaded files
        public static List<string> UploadSpecificFilesInFolderByFileInParallel(string BucketName, string SourceFolderPath, string destinationFolderRelativePath, string AWSAccessKey, string AWSSecretKey, List<string> fileNameContainsRegExp, List<string> uploadedFiles, ConcurrentQueue<Exception> exceptions)
        {
            var uploadedFilesNames = new ConcurrentQueue<string>();
            Parallel.ForEach(Directory.GetFiles(SourceFolderPath), filePath =>
            {
                try
                {               
                    string currentFile = filePath.Replace(SourceFolderPath, string.Empty);
                    if (fileNameContainsRegExp.Any(regExp => Regex.Match(currentFile, regExp).Success) && // current file matches regular expression
                        (uploadedFiles.All(fn => !currentFile.Equals(fn.ToString()))))                    // do not upload files already uploaded
                    {
                        string relativeFileURL = destinationFolderRelativePath + currentFile.Replace(@"\", "/");
                        AWSSimpleStorageService.UploadFile(BucketName, filePath, relativeFileURL, AWSAccessKey,
                            AWSSecretKey);
                        uploadedFilesNames.Enqueue(currentFile);
                    }
                }
                catch (Exception ex)
                {
                    exceptions.Enqueue(ex);
                }
            });
           
            foreach (var uploadFile in UploadSubFoldersInParallelRecursiveThatContains(BucketName, SourceFolderPath, SourceFolderPath, destinationFolderRelativePath, AWSAccessKey, AWSSecretKey, fileNameContainsRegExp, uploadedFiles, exceptions))
            {
                uploadedFilesNames.Enqueue(uploadFile);
            }

            return uploadedFilesNames.ToList();
        }

        // returns the uploaded file names
        private static IEnumerable<string> UploadSubFoldersInParallelRecursiveThatContains(string BucketName, string SourceFolderPath, string currentSourceSubFolder, string destinationFolderRelativePath, string AWSAccessKey, string AWSSecretKey, List<string> fileNameContainsRegExp, List<string> uploadedFiles, ConcurrentQueue<Exception> exceptions )
        {
            var uploadedFilesNames = new ConcurrentQueue<string>();
            // Use ConcurrentQueue to enable safe enqueueing from multiple threads.
            Parallel.ForEach(Directory.GetDirectories(currentSourceSubFolder), dirPath =>
            {            
                Parallel.ForEach(Directory.GetFiles(dirPath), filePath =>
                {
                    try
                    {
                        string currentFile = filePath.Replace(SourceFolderPath, string.Empty);
                        if (fileNameContainsRegExp.Any(regExp => Regex.Match(currentFile, regExp).Success) && // current file matches regular expression
                            (uploadedFiles.All(fn => !currentFile.Equals(fn.ToString()))))                    // do not upload files already uploaded
                        {
                            string relativeFileURL = destinationFolderRelativePath + currentFile.Replace(@"\", "/");
                            AWSSimpleStorageService.UploadFile(BucketName, filePath, relativeFileURL, AWSAccessKey,
                                AWSSecretKey);
                            uploadedFilesNames.Enqueue(currentFile);
                        }
                    }
                    catch (Exception ex)
                    {
                        exceptions.Enqueue(ex);
                    }
                });

                foreach (var uploadedFile in UploadSubFoldersInParallelRecursiveThatContains(BucketName, SourceFolderPath, dirPath, destinationFolderRelativePath, AWSAccessKey, AWSSecretKey, fileNameContainsRegExp, uploadedFiles, exceptions))
                {
                    uploadedFilesNames.Enqueue(uploadedFile);
                }

            });

            return uploadedFilesNames;
        }

        public static bool UploadFolderByFileInParallel(string BucketName, string SourceFolderPath, string destinationFolderRelativePath, string AWSAccessKey, string AWSSecretKey)
        {
            Parallel.ForEach(Directory.GetFiles(SourceFolderPath), filePath =>
            {
                string currentFile = filePath.Replace(SourceFolderPath, string.Empty);
                string relativeFileURL = destinationFolderRelativePath + currentFile.Replace(@"\", "/");
                AWSSimpleStorageService.UploadFile(BucketName, filePath, relativeFileURL, AWSAccessKey,
                    AWSSecretKey);
            });

            UploadSubFoldersInParallelRecursive(BucketName, SourceFolderPath, SourceFolderPath, destinationFolderRelativePath, AWSAccessKey, AWSSecretKey);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BucketName"></param>
        /// <param name="SourceFolderPath"></param>
        /// <param name="ProgressText"></param>
        /// <param name="ProgressFilePath"></param>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        private static bool UploadSubFoldersInParallelRecursive(string BucketName, string SourceFolderPath, string currentSourceSubFolder, string destinationFolderRelativePath, string AWSAccessKey, string AWSSecretKey)
        {
            Parallel.ForEach(Directory.GetDirectories(currentSourceSubFolder), dirPath =>
            {
                Parallel.ForEach(Directory.GetFiles(dirPath), filePath =>
                {
                    string currentFile = filePath.Replace(SourceFolderPath, string.Empty);
                    string relativeFileURL = destinationFolderRelativePath + currentFile.Replace(@"\", "/");
                    AWSSimpleStorageService.UploadFile(BucketName, filePath, relativeFileURL, AWSAccessKey,
                        AWSSecretKey);
                });

                UploadSubFoldersInParallelRecursive(BucketName, SourceFolderPath, dirPath, destinationFolderRelativePath, AWSAccessKey, AWSSecretKey);
            });

            return true;
        }

        /// <summary>
        /// Start a multi-part upload to S3
        /// </summary>
        /// <param name="BucketName">The name of the bucket where the new object(file) will be created, or existing object updated. </param>
        /// <param name="DestinationFilePath">The path of the object(file) to create or update</param>
        /// <param name="AWSAccessKey">AWS key</param>
        /// <param name="AWSSecretKey">AWS secret Key</param>
        /// <returns>The multipart upload session ID needed for the subsequent calls. The method returns empty string if fails</returns>
        public static string InitMultipartUploadRequest(string BucketName, string DestinationFilePath, string AWSAccessKey, string AWSSecretKey)
        {
            string uploadID = String.Empty;

            AmazonS3Client client = new AmazonS3Client(AWSAccessKey, AWSSecretKey);

            InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest
            {
                BucketName = BucketName,
                Key = DestinationFilePath
            };

            InitiateMultipartUploadResponse initResponse = client.InitiateMultipartUpload(initRequest);
            uploadID = initResponse.UploadId;

            return uploadID;
        }

        /// <summary>
        /// Request upload of a part in a multi-part upload operation
        /// The method must be preceded by a InitMultipartUploadRequest call to obtain the upload session ID
        /// </summary>
        /// <param name="BucketName">The name of the bucket containing the object(file) to receive the part</param>
        /// <param name="DestinationFilePath">The relative destination file path on S3</param>
        /// <param name="uploadID">The upload id for the multipart upload in progress (comming from a InitMultipartUploadRequest call)</param>
        /// <param name="partNumber">The index of the part to be uploaded</param>
        /// <param name="partSize">The size of the part to be uploaded</param>
        /// <param name="inputStream">Teh input stream for the upload request</param>
        /// <param name="AWSAccessKey">AWS key</param>
        /// <param name="AWSSecretKey">AWS secret Key</param>
        /// <returns>Returns part upload etag in case the request succeeded otherwise null</returns>
        public static AWSPartETag UploadFilePartStream(string BucketName, string DestinationFilePath, string uploadID, int partNumber, long partSize, Stream inputStream, string AWSAccessKey, string AWSSecretKey)
        {
            AWSPartETag eTag = null;

            AmazonS3Client client = new AmazonS3Client(AWSAccessKey, AWSSecretKey);

            UploadPartRequest uploadRequest = new UploadPartRequest
            {
                BucketName = BucketName,
                Key = DestinationFilePath,
                UploadId = uploadID,
                PartNumber = partNumber,
                PartSize = partSize,
                InputStream = inputStream
            };

            UploadPartResponse upResponse = client.UploadPart(uploadRequest);

            eTag = new AWSPartETag(partNumber, upResponse.ETag);

            return eTag;
        }

        /// <summary>
        /// Request completion of a multi-part upload by assembling previously uploaded parts
        /// </summary>
        /// <param name="BucketName">The name of the bucket containing the S3 object(file) that was being uploaded in parts</param>
        /// <param name="DestinationFilePath">The key of the S3 object(file) that was being uploaded</param>
        /// <param name="uploadID">The upload id for the in-progress multipart upload that should be completed</param>
        /// <param name="partETags">Uploaded parts numbers and corresponding etags</param>
        /// <param name="AWSAccessKey">AWS key</param>
        /// <param name="AWSSecretKey">AWS secret Key</param>
        /// <returns></returns>
        public static bool CompleteMultipartUploadRequest(string BucketName, string DestinationFilePath, string uploadID, IEnumerable<AWSPartETag> partETags, string AWSAccessKey, string AWSSecretKey)
        {
            AmazonS3Client client = new AmazonS3Client(AWSAccessKey, AWSSecretKey);

            var partETagLst = partETags.Select(x => new PartETag() { PartNumber = x.PartNumber, ETag = x.ETag }).ToList();

            CompleteMultipartUploadRequest completeMultipartUploadRequest = new CompleteMultipartUploadRequest
            {
                BucketName = BucketName,
                Key = DestinationFilePath,
                UploadId = uploadID,
                PartETags = partETagLst
            };

            CompleteMultipartUploadResponse completeResponse = client.CompleteMultipartUpload(completeMultipartUploadRequest);

            return true;
        }

        /// <summary>
        /// Lists the parts that have been uploaded for a specific multipart upload
        /// </summary>
        /// <param name="BucketName">The name of the bucket receiving the multipart upload </param>
        /// <param name="DestinationFilePath">The key of the S3 object(file) that was being uploaded</param>
        /// <param name="uploadID">The upload ID identifying the multipart upload whose parts are being listed</param>
        /// <param name="AWSAccessKey">AWS key</param>
        /// <param name="AWSSecretKey">AWS secret Key</param>
        public static IEnumerable<AWSPartETag> ListPartsUpload(string BucketName, string DestinationFilePath, string uploadID, string AWSAccessKey, string AWSSecretKey)
        {
            List<AWSPartETag> parts = new List<AWSPartETag>();

            AmazonS3Client client = new AmazonS3Client(AWSAccessKey, AWSSecretKey);

            ListPartsRequest listPartsRequest = new ListPartsRequest()
            {
                BucketName = BucketName,
                Key = DestinationFilePath,
                UploadId = uploadID
            };

            ListPartsResponse listPartsResponse = client.ListParts(listPartsRequest);

            if (listPartsResponse != null && listPartsResponse.Parts != null)
            {
                parts.AddRange(listPartsResponse.Parts.Select(partDetail => new AWSPartETag(partDetail.PartNumber, partDetail.ETag)));
            }

            return parts;
        }

        /// <summary>
        /// Request an abort of a multi-part upload
        /// </summary>
        /// <param name="BucketName">The name of the bucket containing the S3 object that was being uploaded in parts</param>
        /// <param name="DestinationFilePath">The key of the S3 object(file) that was being uploaded</param>
        /// <param name="uploadID">The upload id for the in-progress multipart upload that should be aborted</param>
        /// <param name="AWSAccessKey">AWS key</param>
        /// <param name="AWSSecretKey">AWS secret Key</param>
        /// <returns></returns>
        public static bool AbortMultipartUploadRequest(string BucketName, string DestinationFilePath, string uploadID, string AWSAccessKey, string AWSSecretKey)
        {
            AbortMultipartUploadRequest abortMultipartUploadRequest = new AbortMultipartUploadRequest()
            {
                BucketName = BucketName,
                Key = DestinationFilePath,
                UploadId = uploadID
            };

            AbortMultipartUploadResponse completeResponse = S3Client(AWSAccessKey, AWSSecretKey).AbortMultipartUpload(abortMultipartUploadRequest);

            return true;
        }



        #endregion
    }
}