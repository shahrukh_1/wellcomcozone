﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.S3.Model;

namespace GMGColor.AWS
{
    public class AWSPartETag
    {
        #region Properties
        /// <summary>
        /// The entity tag associated with the part
        /// </summary>
        public string ETag { get; private set;}
        
        /// <summary>
        /// The part number identifying the part
        /// </summary>
        public int PartNumber {get; private set;}

        #endregion

        #region CTor
        public AWSPartETag(int partNumber, string eTag)
        {
            ETag = eTag;
            PartNumber = partNumber;
        }
        #endregion
    }
}
