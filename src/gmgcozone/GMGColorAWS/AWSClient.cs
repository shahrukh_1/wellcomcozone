﻿using System;
using Amazon;

namespace GMGColor.AWS
{
    public class AWSSettings
    {
        public string Region { get; set; }

        public bool UseV4Signature { get; set; }
    }

    public static class AWSClient
    {
        public static void Init(AWSSettings settings)
        {
            AWSConfigs.AWSRegion = settings.Region;
            AWSConfigs.S3Config.UseSignatureVersion4 = settings.UseV4Signature;
        }
    }
}
