﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using System.Net;
using System.Web;
using System.IO;

namespace GMGColor.AWS
{
    public static class AWSSimpleNotificationService
    {
        private static AmazonSimpleNotificationServiceClient snsclient;

        private static AmazonSimpleNotificationServiceClient SNSClient(string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            if (snsclient == null)
            {   
                Amazon.RegionEndpoint region = Amazon.RegionEndpoint.GetBySystemName(AWSRegion);
                snsclient = new AmazonSimpleNotificationServiceClient(AWSAccessKey, AWSSecretKey, region);
            }
            return snsclient;
        }

        public static string GetTopicTran(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string TopicName)
        {
            try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey, AWSRegion);

                 //Create topic
                string topicArn = client.CreateTopic(new CreateTopicRequest
                {
                    Name = TopicName                   
                }).TopicArn;

                return topicArn;
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }
        }

        public static bool TopicExists(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string TopicARN)
        {
            try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey, AWSRegion);

                 //Check if topic exists
                bool topicFound = client.ListTopics(new ListTopicsRequest()
                ).Topics.Any(t => t.TopicArn == TopicARN);

                return topicFound;
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex; 
            }
        }

        public static void ConfirmSubscription(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string topicARN, string token)
        {
            try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey, AWSRegion);

                client.ConfirmSubscription(
                    new ConfirmSubscriptionRequest()
                    {
                        Token = token,
                        TopicArn = topicARN
                    });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }

        }

        public static void CreateTopic(string AWSAccessKey, string AWSSecretKey, string TopicName)
        {
            /*PRW try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey);
                //string topicArn = AWSSimpleNotificationService.GetTopicTran(AWSAccessKey, AWSSecretKey, TopicName);

                // Delete topic
                client.CreateTopic(new CreateTopicRequest
                {
                    Name = TopicName
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }*/
        }

        public static void CreateSubscriber(string AWSAccessKey, string AWSSecretKey, string TopicName, string Email)
        {
            /*PRW try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey);
                string topicArn = AWSSimpleNotificationService.GetTopicTran(AWSAccessKey, AWSSecretKey, TopicName);

                // Set display name to a friendly value
                client.SetTopicAttributes(new SetTopicAttributesRequest
                {
                    TopicArn = topicArn,
                    AttributeName = "DisplayName",
                    AttributeValue = "AWS Simple Notification Service Subscription Request"
                });

                // Subscribe an endpoint - in this case, an email address
                client.Subscribe(new SubscribeRequest
                {
                    TopicArn = topicArn,
                    Protocol = "email",
                    Endpoint = Email
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }*/
        }

        public static string CreateHttpSubscriber(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string topicName, string domain, string protocol)
        {
            try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey, AWSRegion);
                string topicArn = AWSSimpleNotificationService.GetTopicTran(AWSAccessKey, AWSSecretKey, AWSRegion, topicName);


                client.SetTopicAttributes(
                    new SetTopicAttributesRequest
                        {
                            TopicArn = topicArn,
                            AttributeName = "DisplayName",
                            AttributeValue = "Notifications for Transcoded videos"
                        }
                    );

                 //Subscribe an endpoint - in this case, an http url
                client.Subscribe(
                    new SubscribeRequest
                    {
                        TopicArn = topicArn,
                        Protocol = protocol,
                        Endpoint = domain 
                    });

                return topicArn;
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }
        }


        public static string CreateSubscriber(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string topicName, string domain, string protocol, string attributeName, string attributeValue)
        {
            try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey, AWSRegion);
                string topicArn = AWSSimpleNotificationService.GetTopicTran(AWSAccessKey, AWSSecretKey, AWSRegion, topicName);

                client.SetTopicAttributes(
                    new SetTopicAttributesRequest
                    {
                        TopicArn = topicArn,
                        AttributeName = attributeName,
                        AttributeValue = attributeValue
                    });

                client.Subscribe(
                    new SubscribeRequest
                    {
                        TopicArn = topicArn,
                        Protocol = protocol,
                        Endpoint = domain
                    });
                return topicArn;
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                
                throw ex;
            }
        }

        public static void WriteMessage(string AWSAccessKey, string AWSSecretKey, string TopicName, string Message)
        {
            /*PRW try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey);
                string topicArn = AWSSimpleNotificationService.GetTopicTran(AWSAccessKey, AWSSecretKey, TopicName);

                // Publish message
                client.Publish(new PublishRequest
                {
                    Subject = "GMGCoZone Error",
                    Message = Message,
                    TopicArn = topicArn
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }*/
        }

        public static void WriteMessage(string AWSAccessKey, string AWSSecretKey, string TopicName, AWSSNSData MessageData)
        {
            /*PRW try
            {
                AmazonSimpleNotificationServiceClient client = SNSClient(AWSAccessKey, AWSSecretKey);
                string topicArn = AWSSimpleNotificationService.GetTopicTran(AWSAccessKey, AWSSecretKey, TopicName);

                // Publish message
                client.Publish(new PublishRequest
                {
                    Subject = "GMGCoZone Error",
                    Message = MessageData.BuildMessage(MessageData),
                    TopicArn = topicArn
                });
            }
            catch (AmazonSimpleNotificationServiceException ex)
            {
                throw ex;
            }*/
        }
    }

    public class AWSSNSData
    {
        public int Account { get; set; }
        public string AccountRegion { get; set; }
        public string Message { get; set; }
        public string Project { get; set; }
        public string InnerProcessException { get; set; }
        public Exception Exception { get; set; }
        public string AMI_ID { get; set; }
        public string AWSReagion { get; set; }

        private string StackTrace { get; set; }

        public AWSSNSData()
        {
        }

        public AWSSNSData(int account, string accountRegion, string message, Exception exception, string currentProject)
        {
            this.Account = account;
            this.AccountRegion = accountRegion;
            this.Message = message;
            this.Exception = exception;
            this.StackTrace = (exception != null && exception.StackTrace != null) ? exception.StackTrace : string.Empty;
            this.Project = currentProject;
        }

        public string BuildMessage(AWSSNSData MessageData)
        {
            StringBuilder message = new StringBuilder();

            if (!string.IsNullOrEmpty(MessageData.Project))
                message.Append("Project\t: " + MessageData.Project + "\n\n");
            if (MessageData.Account > 0)
                message.Append("Account\t: " + MessageData.Account.ToString() + "\n\n");
            if (!string.IsNullOrEmpty(MessageData.AccountRegion))
                message.Append("Account Region\t: " + MessageData.AccountRegion + "\n\n");
            if (!string.IsNullOrEmpty(MessageData.Message))
                message.Append("Message\t: " + MessageData.Message + "\n\n");
            if (MessageData.Exception != null)
                message.Append(MessageData.Exception.ToString() + "\n\n");
            if (!string.IsNullOrEmpty(MessageData.StackTrace))
                message.Append("Stack Trace\t: " + MessageData.StackTrace + "\n\n");

            return message.ToString();
        }
    }
}
