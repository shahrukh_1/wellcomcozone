﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Amazon.SQS;
using Amazon.SQS.Model;

namespace GMGColor.AWS
{
    public class AWSSimpleQueueService
    {
        #region Enum

        public enum SQSAttribute
        {
            All,
            ApproximateNumberofMessages,
            CreatedTimestamp,
            LastModifiedTimestamp,
            Policy,
            VisibilityTimeout,
            QueueArn
        }

        #endregion

        #region Fields

        private static AmazonSQSClient amazoneSQSClient;

        #endregion

        #region Methods

        public static AmazonSQSClient SQSClient(string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            if (amazoneSQSClient == null)
            {
                amazoneSQSClient = new AmazonSQSClient(AWSAccessKey, AWSSecretKey, Amazon.RegionEndpoint.GetBySystemName(AWSRegion));
            }
            return amazoneSQSClient;
        }

        public static bool CreateMessage(string message, string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            SendMessageRequest sendMessageRequest = new SendMessageRequest();
            sendMessageRequest.MessageBody = message;
            sendMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);

            SendMessageResponse messageResponce = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).SendMessage(sendMessageRequest);

            return true;
        }

        public static string GetQueueUrl(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            string queueUrl = string.Empty;

            ListQueuesRequest listQueuesRequest = new ListQueuesRequest();
            listQueuesRequest.QueueNamePrefix = queueName;
            ListQueuesResponse listQueuesResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ListQueues(listQueuesRequest);

            if (listQueuesResponse.QueueUrls.Count > 0)
            {
                queueUrl = listQueuesResponse.QueueUrls[0].ToString();
            }
            else
            {
                throw (new Exception("The specified Queue was not found on AWS Queue server."));
            }

            return queueUrl;
        }

        public static GetQueueAttributesResponse GetQueueAttribute(string queueName, SQSAttribute sqsAttribute, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            GetQueueAttributesRequest req = new GetQueueAttributesRequest();
            req.AttributeNames.Add(sqsAttribute.ToString());  // All, ApproximateNumberofMessages, Visibility Timeout, CreatedTimestamp, LastModifiedTimestamp, Policy
            req.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);

            GetQueueAttributesResponse res = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).GetQueueAttributes(req);
            return res;
        }

        public static string GetSQSArn(string queueName, SQSAttribute queueArn, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            var res = GetQueueAttribute(queueName, queueArn, AWSAccessKey, AWSSecretKey, AWSRegion);
            return res.QueueARN;
        }

        public static void CreateQueue(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            CreateQueueRequest sqsRequest = new CreateQueueRequest();
            sqsRequest.QueueName = queueName;
            CreateQueueResponse createQueueResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).CreateQueue(sqsRequest);
        }

        public static List<string> GetAllQueues(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            List<string> lstQueueUrls = new List<string>();

            ListQueuesRequest listQueuesRequest = new ListQueuesRequest();
            ListQueuesResponse listQueuesResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ListQueues(listQueuesRequest);

            foreach (String queueUrl in listQueuesResponse.QueueUrls)
            {
                lstQueueUrls.Add(queueUrl);
            }

            return lstQueueUrls;
        }

        public static bool CheckIfQueueExists(string queueToCheck, string QueueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            return GetAllQueues(QueueName, AWSAccessKey, AWSSecretKey, AWSRegion).Any(t=> t.Contains(queueToCheck));
        }

        public static string ReadMessage(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            string message = string.Empty;

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            ReceiveMessageResponse receiveMessageResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ReceiveMessage(receiveMessageRequest);

            if (receiveMessageResponse.Messages.Count > 0)
            {
                message = receiveMessageResponse.Messages[0].Body;
            }

            return message;
        }

        public static ReceiveMessageResponse ReadMessages(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion) 
        {
            var receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.QueueUrl = AWSSimpleQueueService.GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            
            return SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ReceiveMessage(receiveMessageRequest);
        }

        public static ReceiveMessageResponse CheckQueueForMessages(string queueName, int maxNoOfMessages, int visibilityTimePeriod, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            ReceiveMessageResponse objSqsResponse = new ReceiveMessageResponse();

            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.AttributeNames.Add("All");
            receiveMessageRequest.MaxNumberOfMessages = maxNoOfMessages;
            receiveMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            receiveMessageRequest.VisibilityTimeout = visibilityTimePeriod;

            ReceiveMessageResponse receiveMessageResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ReceiveMessage(receiveMessageRequest);

            if (receiveMessageResponse.Messages.Count > 0)
            {
                objSqsResponse = receiveMessageResponse;
            }

            return objSqsResponse;
        }

        public static bool ChangeVisibilityTimeOut(string queueName, string receiptHandle, int visibilityTimePeriod, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            ChangeMessageVisibilityRequest changeTimeOutRequest = new ChangeMessageVisibilityRequest();
            changeTimeOutRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            changeTimeOutRequest.ReceiptHandle = receiptHandle;
            changeTimeOutRequest.VisibilityTimeout = visibilityTimePeriod;

            ChangeMessageVisibilityResponse changeTimeOutResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ChangeMessageVisibility(changeTimeOutRequest);

            return true;
        }

        public static void CleanQueue(string queueName, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest();
            receiveMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            ReceiveMessageResponse receiveMessageResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).ReceiveMessage(receiveMessageRequest);

            if (receiveMessageResponse.Messages.Count < 1)
            {
                return;
            }

            foreach (Message message in receiveMessageResponse.Messages)
            {
                string messageReceiptHandle = message.ReceiptHandle;
                DeleteMessageRequest delMsg = new DeleteMessageRequest()
                {
                    QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion),
                    ReceiptHandle = messageReceiptHandle
                };
                SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).DeleteMessage(delMsg);
            }
        }

        public static bool DeleteMessage(string queueName, Message message, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            bool deleteSuccess = false;

            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest();
            deleteMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            deleteMessageRequest.ReceiptHandle = message.ReceiptHandle;
        
            DeleteMessageResponse objDeleteMessageResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).DeleteMessage(deleteMessageRequest);

            deleteSuccess = true;

            return deleteSuccess;
        }

        public static bool DeleteMessage(string queueName, string receipt, string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            DeleteMessageRequest deleteMessageRequest = new DeleteMessageRequest();
            deleteMessageRequest.QueueUrl = GetQueueUrl(queueName, AWSAccessKey, AWSSecretKey, AWSRegion);
            deleteMessageRequest.ReceiptHandle = receipt;

            DeleteMessageResponse objDeleteMessageResponse = SQSClient(AWSAccessKey, AWSSecretKey, AWSRegion).DeleteMessage(deleteMessageRequest);

            return true;
        }
    }
        #endregion
}
