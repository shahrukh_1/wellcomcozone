﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.EC2.Model;
using Amazon.EC2;
using Amazon.SQS;

namespace GMGColor.AWS
{
    #region Enums

    /// <summary>
    /// AWS EC2 instance state enum
    /// </summary>
    public enum AWSEC2InstanceState
    {
        Pending,
        Running,
        ShuttingDown,
        Terminated,
        Stopping,
        Stopped,
        Unknown
    }

    #endregion

    /// <summary>
    /// Utils for EC2 instance management
    /// </summary>
    public static class AWSEC2InstanceManagement
    {

        #region Methods
        
        /// <summary>
        /// Checks the status of the EC2 instance identified by instance ID
        /// </summary>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <param name="AWSRegion"></param>
        /// <param name="instanceID"></param>        
        public static AWSEC2InstanceState GetEC2InstanceStatus(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string instanceID)
        {
            var request = new DescribeInstanceStatusRequest();
            request.InstanceIds = new List<System.String>();
            request.InstanceIds.Add(instanceID);

            Amazon.RegionEndpoint region = Amazon.RegionEndpoint.GetBySystemName(AWSRegion);
            AmazonEC2Client ec2Client = new AmazonEC2Client(AWSAccessKey, AWSSecretKey, region);
            DescribeInstanceStatusResponse response = ec2Client.DescribeInstanceStatus(request);

            if (response == null ||
                response.InstanceStatuses == null ||
                response.InstanceStatuses.Count == 0)
            {
                throw (new Exception( String.Format("Retrieving the status of the EC2 {0} instance on {1} region failed", instanceID, AWSRegion)));
            }

            AWSEC2InstanceState instanceState = GetEC2InstanceStatusFromString(response.InstanceStatuses[0].InstanceState.Name);
            if (instanceState == AWSEC2InstanceState.Unknown)
            {
                throw (new Exception(String.Format("Error converting {0} instance state to AWSEC2InstanceState enum", response.InstanceStatuses[0].InstanceState.Name)));
            }
            return instanceState;
        }

        /// <summary>
        /// Restart or start the AWS EC2 instance given by instance ID if the instance is stopped or is running
        /// </summary>
        /// <returns></returns>
        public static bool RebootAWSEC2Instance(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string instanceID)
        {
            AWSEC2InstanceState instanceState = GetEC2InstanceStatus(AWSAccessKey, AWSSecretKey, AWSRegion, instanceID);
            if (instanceState == AWSEC2InstanceState.Stopped || instanceState == AWSEC2InstanceState.Running)
            {
                Amazon.RegionEndpoint region = Amazon.RegionEndpoint.GetBySystemName(AWSRegion);
                AmazonEC2Client ec2Client = new AmazonEC2Client(AWSAccessKey, AWSSecretKey, region);
                    
                if (instanceState == AWSEC2InstanceState.Stopped)
                {
                    StartInstancesRequest startRequest = new StartInstancesRequest();
                    startRequest.InstanceIds.Add(instanceID);
                    ec2Client.StartInstances(startRequest);
                }
                else
                {
                    RebootInstancesRequest rebootRequest = new RebootInstancesRequest();
                    rebootRequest.InstanceIds.Add(instanceID);
                    ec2Client.RebootInstances(rebootRequest);
                }
            }
            return true;
        }

        /// <summary>
        /// Maps the given string to the corresponding enum value
        /// </summary>
        /// <param name="instanceState"></param>
        /// <returns></returns>
        private static AWSEC2InstanceState GetEC2InstanceStatusFromString(string instanceState)
        {
            switch(instanceState)
            {
                case "pending":
                    return AWSEC2InstanceState.Pending;
                case "running":
                    return AWSEC2InstanceState.Running;
                case "shutting-down":
                    return AWSEC2InstanceState.ShuttingDown;
                case "terminated":
                    return AWSEC2InstanceState.Terminated;
                case "stopping":
                    return AWSEC2InstanceState.Stopping;
                case "stopped":
                    return AWSEC2InstanceState.Stopped;
                default:
                    return AWSEC2InstanceState.Unknown;
            }
        }

        #endregion
    }
}
