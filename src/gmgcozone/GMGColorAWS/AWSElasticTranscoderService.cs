﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Amazon.ElasticTranscoder;
using Amazon.ElasticTranscoder.Model;
using System.IO;

namespace GMGColor.AWS
{
    public class TranscodedJob
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public static class AWSElasticTranscoderService
    {
        private static AmazonElasticTranscoderClient etsclient;

        private static AmazonElasticTranscoderClient ETSClient(string AWSAccessKey, string AWSSecretKey, string AWSRegion)
        {
            if (etsclient == null)
            {
                Amazon.RegionEndpoint region = Amazon.RegionEndpoint.GetBySystemName(AWSRegion);
                etsclient = new AmazonElasticTranscoderClient(AWSAccessKey, AWSSecretKey, region);
            }
            return etsclient;
        }

        /// <summary>
        /// Creates the Pipeline in which the videos that need conversion will be queued
        /// </summary>
        /// <param name="AWSAccessKey">AWS Access Key</param>
        /// <param name="AWSSecretKey">AWS Secret Key</param>
        /// <param name="TopicARN">The Topic ARN in which job notifications will be published</param>
        /// <param name="AWSEtsRoleARN">The Role that The Transcoder service will have for permissions in SNS ans S3 bucket</param>
        /// <param name="BucketName">The name of the input bucket where the video is found and where it will be placed after conversion finishes</param>
        public static string CreatePipeline(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string topicARN,
            string AWSEtsRoleARN, string pipelineName, string bucketName)
        {
            try
            {
                AmazonElasticTranscoderClient client = ETSClient(AWSAccessKey, AWSSecretKey, AWSRegion);

                List<Pipeline> pipelines = client.ListPipelines().Pipelines;

                if (pipelines.Any(t => t.Name == pipelineName))
                {
                    string pipelineID = pipelines.SingleOrDefault(t => t.Name == pipelineName).Id;
                    return pipelineID;
                }
                else
                {
                    Notifications notifications = new Notifications()
                    {
                        Completed = topicARN,
                        Error = topicARN,
                        Progressing = topicARN,
                        Warning = topicARN
                    };

                PipelineOutputConfig contentConfig = new PipelineOutputConfig();

                    List<string> accesses = new List<string>();
                    accesses.Add("Read");
                    accesses.Add("ReadAcp");
                    List<Permission> permissions = new List<Permission>();
                    Permission permission = new Permission()
                    {
                         Access = accesses,
                         GranteeType = "Group",
                         Grantee = "AllUsers"
                    };
                    permissions.Add(permission);
                    contentConfig.Permissions = permissions;
                    contentConfig.Bucket = bucketName;
                    contentConfig.StorageClass = "Standard";

                    Pipeline pipeline = client.CreatePipeline(new CreatePipelineRequest()
                    {
                        Name = pipelineName,
                        InputBucket = bucketName,
                        Notifications = notifications,
                        Role = AWSEtsRoleARN,
                        ContentConfig = contentConfig,
                        ThumbnailConfig = new PipelineOutputConfig()
                        {
                            Bucket = bucketName,                            
                            StorageClass = "Standard",
                            Permissions = permissions
                        }
                    }).Pipeline;

                    if (pipeline != null)
                    {
                        return pipeline.Id;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch(AmazonElasticTranscoderException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Creates transcoding job
        /// </summary>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <param name="videoPath"></param>
        /// <param name="pipelineID"></param>
        /// <param name="presetID"></param>
        public static string TranscodeVideo(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string videoPath, string outputVideoPath, string outputThumbnailPath, string pipelineID, string presetID)
        {
            try
            {
                AmazonElasticTranscoderClient client = ETSClient(AWSAccessKey, AWSSecretKey, AWSRegion);              

                Job job = client.CreateJob(new CreateJobRequest()
                                        { 
                                            PipelineId = pipelineID,
                                            Input = new JobInput()
                                            {
                                                AspectRatio = "auto",
                                                Container = "auto",
                                                FrameRate = "auto",
                                                Interlaced = "auto",
                                                Resolution = "auto",
                                                Key = videoPath
                                            },
                                     
                                            Output = new CreateJobOutput()
                                            {
                                                ThumbnailPattern = "",
                                                Rotate = "0",
                                                PresetId = presetID,
                                                Key = outputVideoPath
                                            }
                                        }).Job;


                if (job != null)
                {
                    return job.Id;
                }
                else
                {
                    return null;
                }


            }
            catch(AmazonElasticTranscoderException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Creates Custom Preset for transcoding videos
        /// </summary>
        /// <param name="AWSAccessKey"></param>
        /// <param name="AWSSecretKey"></param>
        /// <returns></returns>
        public static string CreateCustomPreset(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string presetName)
        {
            try
            {
                AmazonElasticTranscoderClient client = ETSClient(AWSAccessKey, AWSSecretKey, AWSRegion);

                List<Preset> presets = client.ListPresets().Presets;

                if (presets.Any(t => t.Name == presetName))
                {
                    string presetId = presets.SingleOrDefault(t => t.Name == presetName).Id;
                    return presetId;
                }
                else
                {
                    Dictionary<string, string> codecOpt = new Dictionary<string, string>();
                    codecOpt.Add("Profile", "baseline");
                    codecOpt.Add("Level", "3.1");
                    codecOpt.Add("MaxReferenceFrames", "5");

                    Preset preset = client.CreatePreset(new CreatePresetRequest()
                    {
                        Name = presetName,
                        Description = "Preset for transcoding CoZone videos",
                        Video = new VideoParameters()
                        {
                            BitRate = "auto",
                            Codec = "H.264",
                            CodecOptions = codecOpt,
                            MaxHeight = "720",
                            MaxWidth = "auto",
                            FrameRate = "24",
                            FixedGOP = "true",
                            DisplayAspectRatio = "auto",
                            KeyframesMaxDist = "24",
                            PaddingPolicy = "NoPad",
                            SizingPolicy = "ShrinkToFit",
                        },
                        Audio = new AudioParameters()
                        {
                            Channels = "auto",
                            Codec = "AAC",
                            SampleRate = "auto",
                            BitRate = "319"
                        },
                        Thumbnails = new Thumbnails()
                        {
                            Format = "jpg",
                            Interval = "2",
                            MaxHeight = "128",
                            MaxWidth = "128",
                            PaddingPolicy = "NoPad",
                            SizingPolicy = "ShrinkToFit"
                        },
                        Container = "mp4"
                    }).Preset;

                    if (preset != null)
                    {
                        return preset.Id;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (AmazonElasticTranscoderException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Read the specified Transcoded Job
        /// </summary>
        /// <param name="jobId">The id of the Transcoded Job</param>
        public static TranscodedJob ReadTranscodedJob(string AWSAccessKey, string AWSSecretKey, string AWSRegion, string jobId)
        {
            try
            {
                AmazonElasticTranscoderClient client = ETSClient(AWSAccessKey, AWSSecretKey, AWSRegion);

                JobOutput job = client.ReadJob(new ReadJobRequest()
                                {
                                    Id = jobId
                                }).Job.Outputs.FirstOrDefault();

                if (job != null)
                {
                    TranscodedJob outputJob = new TranscodedJob()
                                            {
                                                Width = job.Width,
                                                Height = job.Height
                                            };

                    return outputJob;
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
