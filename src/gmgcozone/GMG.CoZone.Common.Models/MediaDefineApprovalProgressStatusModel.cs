﻿namespace GMG.CoZone.Common.Models
{
    public class MediaDefineApprovalProgressStatusModel
    {
        public int ApprovalId { get; set; }
        public string Thumbnail { get; set; }
        public string  Status { get; set; }
    }
}
