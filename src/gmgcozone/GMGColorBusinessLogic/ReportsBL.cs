﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System.Text;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMGColorBusinessLogic
{
    public class ReportsBL : BaseBL
    {
        public static AccountReportDetails GetAccountReportDetails(int accountId, DbContextBL context)
        {
            AccountReportDetails accountReportDetails = new AccountReportDetails();
            
            Account objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();
            accountReportDetails.AccountID = objAccount.ID;
            accountReportDetails.AccountName = objAccount.Name;
            accountReportDetails.AccountURL = objAccount.Domain;
            accountReportDetails.OwnerName = objAccount.OwnerUser.GivenName + " " + objAccount.OwnerUser.FamilyName;
            accountReportDetails.OwnerEmail = objAccount.OwnerUser.EmailAddress;

            if(AccountType.GetAccountType(objAccount.AccountType1.Key) != AccountType.Type.Subscriber)
            {
                AccountSubscriptionPlan collaborateSubscriptionPlan = objAccount.CollaborateSubscriptionPlan(context);              
                AccountSubscriptionPlan deliverSubscriptionPlan = objAccount.DeliverSubscriptionPlan(context);

                if (collaborateSubscriptionPlan != null)
                accountReportDetails.ListBillingPlans.Add(new KeyValuePair<int, string>(collaborateSubscriptionPlan.BillingPlan.BillingPlanType.AppModule1.Key, collaborateSubscriptionPlan.BillingPlan.Name));               
                if (deliverSubscriptionPlan != null)
                    accountReportDetails.ListBillingPlans.Add(new KeyValuePair<int, string>(deliverSubscriptionPlan.BillingPlan.BillingPlanType.AppModule1.Key, deliverSubscriptionPlan.BillingPlan.Name));               
            }
            else
            {
                SubscriberPlanInfo collaborateSubscriptionPlan = objAccount.CollaborateSubscriberPlan(context);               
                SubscriberPlanInfo deliverSubscriptionPlan = objAccount.DeliverSubscriberPlan(context);               

                if (collaborateSubscriptionPlan != null)
                accountReportDetails.ListBillingPlans.Add(new KeyValuePair<int, string>(collaborateSubscriptionPlan.BillingPlan.BillingPlanType.AppModule1.Key, collaborateSubscriptionPlan.BillingPlan.Name));               
                if (deliverSubscriptionPlan != null)
                    accountReportDetails.ListBillingPlans.Add(new KeyValuePair<int, string>(deliverSubscriptionPlan.BillingPlan.BillingPlanType.AppModule1.Key, deliverSubscriptionPlan.BillingPlan.Name));              
            }

            return accountReportDetails;
        }

        public static DateTime? GetValidBillingPeriod(ReportBillingData model, DbContextBL context)
        {
            try
            {
                List<int> accountIds = model.SelectedAccounts.Where(t => t != "").Select(int.Parse).ToList();
                List<int> accountTypeIds = model.SelectedAccountTypes.Where(t => t != "").Select(int.Parse).ToList();
                List<int> locationIds = model.SelectedLocations.Where(t => t != "").Select(int.Parse).ToList();
                List<int> billingPlanIds = model.SelectedBillingPlans.Where(t => t != "").Select(int.Parse).ToList();

                return ((from ac in context.Accounts
                                     join co in context.Companies on ac.ID equals co.Account
                                    join at in context.AccountTypes on ac.AccountType equals at.ID
                                    join acsp in context.AccountSubscriptionPlans on ac.ID equals acsp.Account
                                    where (!accountIds.Any() || accountIds.Contains(ac.ID)) &&
                                    (!accountTypeIds.Any() || accountTypeIds.Contains(at.ID)) &&
                                    (!locationIds.Any() || locationIds.Contains(co.Country)) &&
                                    (!billingPlanIds.Any() || billingPlanIds.Contains(acsp.SelectedBillingPlan))
                                     select acsp.ContractStartDate).Union(from ac in context.Accounts
                                                                         join co in context.Companies on ac.ID equals co.Account
                                                                        join at in context.AccountTypes on ac.AccountType equals at.ID
                                                                        join spif in context.SubscriberPlanInfoes on ac.ID equals spif.Account
                                                                        where (!accountIds.Any() || accountIds.Contains(ac.ID)) &&
                                                                        (!accountTypeIds.Any() || accountTypeIds.Contains(at.ID)) &&
                                                                        (!locationIds.Any() || locationIds.Contains(co.Country)) &&
                                                                        (!billingPlanIds.Any() || billingPlanIds.Contains(spif.SelectedBillingPlan))
                                                                        select spif.ContractStartDate)).Min();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetBillingPeriodValidStartDate, ex);
            }
        }
    }
}
