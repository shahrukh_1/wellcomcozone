﻿using System;
using System.Reflection;

namespace GMGColorBusinessLogic
{
    public class ExceptionBL: Exception
    {
        public new string Message { get; private set; }

        public new Exception InnerException { get; private set; }

        public ExceptionBL(string message) : base(message)
        {
            InternalPreserveStackTrace(this);
            Message = message;
        }

        public ExceptionBL(string message, Exception innerException): base(message, innerException)
        {
            InternalPreserveStackTrace(innerException);
            Message = message;
            InnerException = innerException;
        }

        private static readonly Action<Exception> InternalPreserveStackTrace =
            (Action<Exception>) Delegate.CreateDelegate(
                typeof (Action<Exception>),
                typeof (Exception).GetMethod(
                    "InternalPreserveStackTrace",
                    BindingFlags.Instance | BindingFlags.NonPublic));
    }
}
