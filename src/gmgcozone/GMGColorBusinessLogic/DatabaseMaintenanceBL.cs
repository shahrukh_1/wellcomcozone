﻿using System;
using System.Activities.Statements;
using System.Threading;
using System.Threading.Tasks;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class DatabaseMaintenanceBL : BaseBL
    {
        private DBMaintenanceBL _maintenanceBL;

        public DatabaseMaintenanceBL()
        {
            _maintenanceBL = new DBMaintenanceBL();
        }

        /// <summary>
        /// Recreate database indexes routine
        /// </summary>
        public void RecreateDBIndexes(DbContextBL context)
        {
            _maintenanceBL.RecreateIndexes(context);
        }

        /// <summary>
        /// Clear stored procedure cache routine
        /// </summary>
        public void ClearStoredProceduresCache(DbContextBL context)
        {
            _maintenanceBL.ClearStoredProceduresCache(context);
        }

        /// <summary>
        /// Update statistics routine
        /// </summary>
        public void UpdateStatistics(DbContextBL context)
        {
            _maintenanceBL.UpdateStatistics(context);
        }
    }
}
