﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorNotificationService;
using GMGColorDAL.CustomModels;
using GMGColorBusinessLogic.Common;

namespace GMGColorBusinessLogic
{
    public class ThemeBL : BaseBL
    {
        /// <summary>
        /// Get the correct theme based on the account or user group settings
        /// </summary>
        /// <param name="loggedUserId">The logged user id</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ThemeColorSchema> GetLoggedUserThemeColorSchemeToDisplay(int? loggedUserId, DbContextBL context)
        {
            List<ThemeColorSchema> colorSchema = new List<ThemeColorSchema>();
            if (loggedUserId != null)
            {
                colorSchema = (from us in context.Users
                               join ac in context.Accounts on us.Account equals ac.ID
                               join ath in context.AccountThemes on ac.ID equals ath.Account
                               let colors = (from tcs in context.ThemeColorSchemes
                                             where ath.ID == tcs.AccountTheme
                                             select new ThemeColors
                                             {
                                                 ID = tcs.ID,
                                                 Name = tcs.Name,
                                                 TopColor = tcs.THex,
                                                 BottomColor = tcs.BHex
                                             }).ToList()
                               where us.ID == loggedUserId && !ac.IsTemporary
                               select new ThemeColorSchema
                               {
                                   ID = ath.ID,
                                   Key = ath.Key,
                                   Active = ath.Active,
                                   ThemeName = ath.ThemeName,
                                   Colors = colors
                               }).ToList();

                if (colorSchema.Count() > 0)
                {
                    //check if logged user has PrimaryGroup with BrandingPreset and update theme schema
                    var userBrandingPresetColorSchema = (from ugu in context.UserGroupUsers
                                                         join bprug in context.BrandingPresetUserGroups on ugu.UserGroup equals bprug.UserGroup
                                                         join bpr in context.BrandingPresets on bprug.BrandingPreset equals bpr.ID
                                                         join bprt in context.BrandingPresetThemes on bpr.ID equals bprt.BrandingPreset
                                                         let colors = (from thc in context.ThemeColorSchemes
                                                                       where bprt.ID == thc.BrandingPresetTheme
                                                                       select new ThemeColors
                                                                       {
                                                                           ID = thc.ID,
                                                                           Name = thc.Name,
                                                                           TopColor = thc.THex,
                                                                           BottomColor = thc.BHex
                                                                       }).ToList()
                                                         where ugu.IsPrimary && ugu.User == loggedUserId.Value
                                                         select new ThemeColorSchema
                                                         {
                                                             ID = bprt.ID,
                                                             Key = bprt.Key,
                                                             ThemeName = bprt.ThemeName,
                                                             Active = bprt.Active,
                                                             Colors = colors
                                                         }).ToList();

                    if (userBrandingPresetColorSchema.Count > 0)
                    {
                        colorSchema = userBrandingPresetColorSchema;
                    }
                }
            }
            else
            {
                var accColorSchema = (from ac in context.Accounts
                                      join act in context.AccountThemes on ac.ID equals act.Account
                                      let colors = (from thc in context.ThemeColorSchemes
                                                    where thc.AccountTheme == act.ID
                                                    select new ThemeColors
                                                    {
                                                        ID = thc.ID,
                                                        Name = thc.Name,
                                                        TopColor = thc.THex,
                                                        BottomColor = thc.BHex
                                                    }).ToList()
                                      where !ac.IsTemporary
                                      select new ThemeColorSchema
                                      {
                                          ID = act.ID,
                                          Key = act.Key,
                                          ThemeName = act.ThemeName,
                                          Active = act.Active,
                                          Colors = colors
                                      }).ToList();

                if (accColorSchema.Count > 0)
                {
                    colorSchema = accColorSchema;
                }
            }
            return colorSchema;
        }

        /// <summary>
        /// Get the account global theme color schema
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ThemeColorSchema> GetAccountGlobalColorScheme(int accountId, DbContextBL context)
        {
            var accountGlobalThemeSchema = (from acc in context.Accounts
                                            join acth in context.AccountThemes on acc.ID equals acth.Account
                                            let themeColors = (from tcs in context.ThemeColorSchemes
                                                               where tcs.AccountTheme == acth.ID
                                                               select new ThemeColors
                                                               {
                                                                   ID = tcs.ID,
                                                                   Name = tcs.Name,
                                                                   TopColor = tcs.THex,
                                                                   BottomColor = tcs.BHex
                                                               }).ToList()
                                            where acc.ID == accountId
                                            select new ThemeColorSchema
                                            {
                                                ID = acth.ID,
                                                Key = acth.Key,
                                                ThemeName = acth.ThemeName,
                                                Active = acth.Active,
                                                Colors = themeColors
                                            }).ToList();
            var themeSchemaWithRgbValues = ConvertThemesColors(accountGlobalThemeSchema, ConversionEnum.Convert.HexToRbg);
            return themeSchemaWithRgbValues;
        }

        /// <summary>
        /// Get the user/account global color schema
        /// </summary>
        /// <param name="loggedUserId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ThemeColorSchema> GetUserGlobalColorScheme(int loggedUserId, DbContextBL context)
        {
            var userGlobalSchema = (from us in context.Users
                                    join ac in context.Accounts on us.Account equals ac.ID
                                    join ath in context.AccountThemes on ac.ID equals ath.Account
                                    let colors = (from tcs in context.ThemeColorSchemes
                                                  where ath.ID == tcs.AccountTheme
                                                  select new ThemeColors
                                                  {
                                                      ID = tcs.ID,
                                                      Name = tcs.Name,
                                                      TopColor = tcs.THex,
                                                      BottomColor = tcs.BHex
                                                  }).ToList()
                                    where us.ID == loggedUserId && !ac.IsTemporary
                                    select new ThemeColorSchema
                                    {
                                        ID = ath.ID,
                                        Key = ath.Key,
                                        Active = ath.Active,
                                        ThemeName = ath.ThemeName,
                                        Colors = colors
                                    }).ToList();

            var themeSchemaWithRgbValues = ConvertThemesColors(userGlobalSchema, ConversionEnum.Convert.HexToRbg);
            return themeSchemaWithRgbValues;
        }

        public static List<ApprovalTypeModel> GetApprovalTypeColorScheme(DbContextBL context)
        {
            var ApprovalTypeColour = (from atc in context.ApprovalTypeColours select atc).ToList();
            List<ApprovalTypeModel> a = (from atc in context.ApprovalTypeColours
                     join ft in context.FileTypes on atc.ID equals ft.ApprovalTypeColour
                     select new ApprovalTypeModel
                     {
                         ID = atc.ID,
                         Name = atc.Name,
                         FileTypes = ft.Extention,
                         Colour = atc.Colour
                     }).ToList();
           
            var ApprovalTypeModel = new List<ApprovalTypeModel>(); 
            for(int i =0; i < 5; i++)
            {  
                ApprovalTypeModel.Add(new ApprovalTypeModel()
                {
                    ID = ApprovalTypeColour[i].ID,
                    Name = ApprovalTypeColour[i].Name,
                    FileTypes = string.Join(",", a.Where(atm => atm.ID == ApprovalTypeColour[i].ID).Select(atm => atm.FileTypes).ToList()),
                    Colour = ApprovalTypeColour[i].Colour
                });  
            }
            return ConvertApprovalTypeThemesColors(ApprovalTypeModel, GMGColorBusinessLogic.Common.ConversionEnum.Convert.HexToRbg);
        }

        public static void UpdateApprovalTypeColorScheme(List<ApprovalTypeModel> model_, DbContextBL context)
        {
            int i = 1;
            var model = ConvertApprovalTypeThemesColors(model_, GMGColorBusinessLogic.Common.ConversionEnum.Convert.RgbToHex);

            foreach (var approvalType in model)
            {
                if (approvalType.FileTypes != null)
                {
                    var filetype_extention = approvalType.FileTypes.Split(',').ToList();
                    if (filetype_extention != null)
                    {
                        foreach (var fileType in filetype_extention)
                        {
                            var color = (from ft in context.FileTypes where ft.Extention == fileType select ft).FirstOrDefault();
                            if (color != null)
                            {
                                color.ApprovalTypeColour = i;
                            }
                        }
                    }

                    var objapprovalType = (from atc in context.ApprovalTypeColours where atc.ID == i select atc).FirstOrDefault();
                    objapprovalType.Colour = approvalType.Colour;
                }
                i++;
            }
           context.SaveChanges();
        }

        /// <summary>
        /// Convert hex value stored in db to rgb for user display
        /// </summary>
        /// <param name="themeList"></param>
        /// <returns></returns>
        public static List<ThemeColorSchema> ConvertThemesColors(List<ThemeColorSchema> themeList, ConversionEnum.Convert convertTo)
        {
            themeList.ForEach(l => l.Colors.ForEach(c =>
            {
                if (convertTo == ConversionEnum.Convert.HexToRbg)
                {
                    c.TopColor = GMG.CoZone.Common.Utils.ConvertHexToRGB(c.TopColor);

                    if (c.BottomColor != null)
                    {
                        c.BottomColor = GMG.CoZone.Common.Utils.ConvertHexToRGB(c.BottomColor);
                    }
                }
                else
                {
                    c.TopColor = GMG.CoZone.Common.Utils.ConvertRgbToHex(c.TopColor);
                    
                    if (c.BottomColor != null)
                    {
                        c.BottomColor = GMG.CoZone.Common.Utils.ConvertRgbToHex(c.BottomColor);
                    }
                }
            }));
            return themeList;
        }

        public static List<ApprovalTypeModel> ConvertApprovalTypeThemesColors(List<ApprovalTypeModel> approvalTypeList, ConversionEnum.Convert convertTo)
        {
            approvalTypeList.ForEach(l => 
            {
                if (convertTo == ConversionEnum.Convert.HexToRbg)
                {
                    l.Colour = GMG.CoZone.Common.Utils.ConvertHexToRGB(l.Colour);
                }
                else
                {
                    l.Colour = GMG.CoZone.Common.Utils.ConvertRgbToHex(l.Colour);
                }
            }
            );
            return approvalTypeList;
        }

        /// <summary>
        /// Update Account or User group schema based on user changes
        /// </summary>
        /// <param name="list">The list of themes and custom colors</param>
        /// <param name="Context"></param>
        public static void UpdateThemeSchema(int accountID, List<ThemeColorSchema> themesList, DbContextBL context)
        {
            var accountTheme = (from acc in context.Accounts
                                join act in context.AccountThemes on acc.ID equals act.Account
                                join ats in context.ThemeColorSchemes on act.ID equals ats.AccountTheme
                                where acc.ID == accountID
                                select act).Distinct().ToList();
            
            var convertedColorsTheme =  ConvertThemesColors(themesList, ConversionEnum.Convert.RgbToHex);

            convertedColorsTheme.ForEach(theme => 
            {
                accountTheme.ForEach(th =>
                {
                    if (theme.Key == th.Key)
                    {
                        th.Active = theme.Active;
                        th.ThemeName = theme.ThemeName;
                        th.ThemeColorSchemes.ToList().ForEach(c => 
                        {
                            theme.Colors.ForEach(cl => {
                                if (c.ID == cl.ID)
                                {
                                    c.THex = cl.TopColor;
                                    c.BHex = cl.BottomColor; 
                                }
                            });
                        });
                    }
                });
            });
        }

        /// <summary>
        /// Update account database themes
        /// </summary>
        /// <param name="themeListToUpdate"></param>
        /// <param name="themeListWithUpdateValues"></param>
        public static void UpdateAccountDbThemes(ICollection<AccountTheme> themeListToUpdate, List<ThemeColorSchema> themeListWithUpdateValues)
        {
            var convertedColorsTheme =  ConvertThemesColors(themeListWithUpdateValues, ConversionEnum.Convert.RgbToHex);
            foreach (var theme in themeListToUpdate)
	        {
                theme.ThemeName = convertedColorsTheme.Where(th=>th.Key == theme.Key).Select(th=>th.ThemeName).FirstOrDefault();
                theme.Active = convertedColorsTheme.Where(th=>th.Key == theme.Key).Select(th=>th.Active).FirstOrDefault();

                foreach (var color in theme.ThemeColorSchemes)
	            {
                    color.BHex = convertedColorsTheme.Where(th => th.Key == theme.Key).Select(th => th.Colors).ToList().Select(cl => cl.Where(ci => ci.ID == color.ID).Select(ci => ci.BottomColor).FirstOrDefault()).FirstOrDefault();
                    color.THex = convertedColorsTheme.Where(th => th.Key == theme.Key).Select(th => th.Colors).ToList().Select(cl => cl.Where(ci => ci.ID == color.ID).Select(ci => ci.TopColor).FirstOrDefault()).FirstOrDefault();
	            }
	        }
        }

        public static List<AccountTheme> CreateAccountThemeSchema(List<ThemeColorSchema> selectedThemeSchema, int accountID)
        {
            var convertedColorsSchema = ThemeBL.ConvertThemesColors(selectedThemeSchema, ConversionEnum.Convert.RgbToHex);
            var newThemeSchema = new List<AccountTheme>();

            return newThemeSchema = convertedColorsSchema.Select(th => new AccountTheme()
            {
                Key = th.Key,
                Active = th.Active,
                ThemeName = th.ThemeName,
                Account = accountID,
                ThemeColorSchemes = th.Colors.Select(cl => new ThemeColorScheme()
                {
                    Name = cl.Name,
                    THex = cl.TopColor,
                    BHex = cl.BottomColor,
                    AccountTheme = th.ID
                }).ToList()
            }).ToList();
        }

        /// <summary>
        /// Get default theme values for creating new brading presents and accounts
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ThemeColorSchema> GetDefaultSchema(DbContextBL context)
        {
            return (from df in context.DefaultThemes
                    let colors = (from tcs in context.ThemeColorSchemes
                                 where tcs.DefaultTheme == df.ID
                                 select new ThemeColors 
                                 { 
                                     ID = tcs.ID,
                                     Name = tcs.Name,
                                     TopColor = tcs.THex,
                                     BottomColor = tcs.BHex
                                 }).ToList()
                    select new ThemeColorSchema
                    {
                        ID = df.ID,
                        Key = df.Key,
                        ThemeName = df.ThemeName,
                        Colors = colors
                    }).ToList();
        }
    }
}
