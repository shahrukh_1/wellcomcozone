﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class DateTimeFormatBL: BaseBL
    {
        /// <summary>
        /// Gets the date format pattern by userId.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        internal static string GetDateFormatPattern(int userId, GMGColorDAL.GMGColorContext context)
        {
            try
            {
                return (from u in context.Users
                        join a in context.Accounts on u.Account equals a.ID
                        join df in context.DateFormats on a.DateFormat equals df.ID
                        where u.ID == userId
                        select df.Pattern).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetDateFormatPattern, ex);
            }
        }

        /// <summary>
        /// Gets the date format pattern.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static string GetDateFormatPattern(int userId)
        {
            try
            {
                using (GMGColorDAL.GMGColorContext context = new GMGColorContext())
                {
                    return GetDateFormatPattern(userId, context);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetDateFormatPattern, ex);
            }
        }

        /// <summary>
        /// Gets the time format pattern by userId.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        internal static int GetTimeFormatPattern(int userId, GMGColorDAL.GMGColorContext context)
        {
            try
            {
                return (from u in context.Users
                        join a in context.Accounts on u.Account equals a.ID
                        join tf in context.TimeFormats on a.TimeFormat equals tf.ID
                        where u.ID == userId
                        select tf.ID).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetTimeFormatPattern, ex);
            }
        }
    }
}
