﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using GMGColorDAL.Common;
using GMG.CoZone.Common;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
namespace GMGColorBusinessLogic
{
    public class FileTransferBL : BaseBL
    {
        #region Constants

        public static double OnePointInCm = 0.03527777777778;
        public const int ProofStudioPenWidthDefaultValue = 2;
        #endregion



        public static void ZipTheFileTransfers(List<FileTransferIndividualItem> items, string fileGuid, string fileName, Account loggedAccount, GMGColorDAL.User loggedUser)
        {
            GMGColorLogging.log.Info($" enetering the method:");
            using (DbContextBL context = new DbContextBL())
            {
                //make the folder where the zip will be stored

                string accountRegion = AccountBL.GetAccountRegion(loggedUser.ID, context);

                string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(loggedUser.ID, loggedAccount.ID, accountRegion);



                // string localTempFolder = "C:\\Temp\\";
                string localTempFolder = GMGColorConfiguration.AppConfiguration.PathToFileTranferTempFolder;
                GMGColorLogging.log.Info($" the path for the local temp file is : {localTempFolder}");

                //check if the Temp file exists
                bool folderExists = System.IO.Directory.Exists(localTempFolder);
                GMGColorLogging.log.Info($" the path for the local temp file exists at the moment : {folderExists}");

                //if the tenp folder does not exists, create it
                if (!folderExists)
                {
                    try
                    {
                        Directory.CreateDirectory(localTempFolder);
                        GMGColorLogging.log.Info($" try creating the directory : {localTempFolder}");
                        folderExists = true;
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.ErrorFormat(ex, " Creating the local temp file failed: exception {0} , temp local path {1}", ex.Message, localTempFolder);
                    }

                }

                string localZipPath = localTempFolder + fileGuid + ".zip";
                GMGColorLogging.log.Info($" the local  zip path is : {localZipPath}");

                try
                {
                    GMGColorLogging.log.Info($" START:");
                    var x = System.IO.File.Create(localZipPath);
                    GMGColorLogging.log.Info($" DONE: local zip path {localZipPath}");

                    using (var zipStream = new ZipOutputStream(x))
                    {
                        zipStream.UseZip64 = UseZip64.Off;
                        foreach (var item in items)
                        {
                            string itemFilePath = string.Concat(loggedUserTempFolderPath, item.FileGuid, "/", item.FileName);
                            GMGColorLogging.log.Info($" DONE: get itemFilePath  {itemFilePath}");

                            using (Stream fileStream = GMGColorIO.GetMemoryStreamOfAFile(itemFilePath, accountRegion))
                            {

                                var fileEntry = new ZipEntry(Path.GetFileName(itemFilePath));
                                GMGColorLogging.log.Info($" DONE: fileEntry  {itemFilePath}");

                                zipStream.PutNextEntry(fileEntry);
                                GMGColorLogging.log.Info($" DONE: zip stream put fileEntry  {itemFilePath}");

                                StreamUtils.Copy(fileStream, zipStream, new byte[4096]);
                                zipStream.CloseEntry();
                                fileStream.Close();
                            }
                        }

                        zipStream.IsStreamOwner = false;
                        zipStream.Flush();
                        zipStream.Close();
                        x.Close();

                    }
                }
                catch (Exception ex)
                {

                    GMGColorLogging.log.ErrorFormat(ex, " Creating the zip failed: exception {0} , local zip path {1}", ex.Message, localZipPath);
                }


                //move the zip to file transfer
                GMGColorLogging.log.Info($"Start moving the zip to temp");
                string dataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
                string destinationFolderPath = GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath + "/" + fileGuid + "/";
                string destinationFilePath = string.Concat(destinationFolderPath, fileName);
                //string destinationPath = string.Concat(dataFolder, zipPath);

                try
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {

                        GMGColor.AWS.AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), localZipPath, destinationFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                        GMGColorLogging.log.Info($" Uploaded zip from {localZipPath} to {destinationFilePath}");
                        System.IO.File.Delete(localZipPath);
                        GMGColorLogging.log.Info($" Deleteed file:{localZipPath}");

                    }
                    else
                    {
                        //create the destination folder
                        string localtemp = string.Concat(dataFolder, GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath, "\\", fileGuid);
                        Directory.CreateDirectory(localtemp);
                        string destination = string.Concat(dataFolder, GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath, "\\", fileGuid, "\\", fileName);
                        try
                        {

                            File.Move(localZipPath, destination);
                        }

                        catch (Exception ex)
                        {

                            GMGColorLogging.log.ErrorFormat(ex, " Moving the zip to local file transfer failed: exception {0} , local zip path {1},destination path {2}", ex.Message, localZipPath, destination);
                        }
                    }

                    //delete the rest of your temp files

                    foreach (var item in items)
                    {
                        string itemFilePath = string.Concat(loggedUserTempFolderPath, '/', item.FileGuid);
                        GMGColorIO.TryDeleteFolder(itemFilePath, accountRegion);
                    }
                }

                catch (Exception ex)
                {

                    GMGColorLogging.log.ErrorFormat(ex, " Moving the zip to temp failed: exception {0} , local zip path {1},destination path {2}", ex.Message, localZipPath, destinationFilePath);
                }


            }

        }

        public static void ProcessTheFileTransfer(FileTransfer fileTransfer, string fileName, GMGColorDAL.User loggedUser)
        {
            GMGColorLogging.log.Info($" enter ProcessTheFileTransfer function for fileName {fileName}");
            using (DbContextBL context = new DbContextBL())
            {
                string accountRegion = AccountBL.GetAccountRegion(loggedUser.ID, context);

                string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(fileTransfer.Creator, fileTransfer.AccountID, accountRegion);

                #region Move folder with file from temp to FileTransfer

                string relativeFolderPath = loggedUserTempFolderPath + fileTransfer.Guid + "/";
                if (GMGColorIO.FolderExists(relativeFolderPath, accountRegion))
                {
                    string sourceRelativeFilePath = relativeFolderPath + fileName;
                    string destinationFolderPath = GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath + "/" + fileTransfer.Guid + "/";
                    string destinationFilePath = destinationFolderPath + fileName;
                    GMGColorIO.FolderExists(destinationFolderPath, accountRegion, true);
                    GMGColorIO.MoveFile(sourceRelativeFilePath, destinationFilePath, accountRegion);
                    GMGColorIO.TryDeleteFolder(relativeFolderPath, accountRegion);
                }
            }
            #endregion
        }


        public static void SendNewFileTransferNotifications(FileTransfer newApproval, List<int> internalCollaborators, List<int> externalCollaborators, Account loggedAccount, GMGColorDAL.User loggedUser, string optionalMessage, DbContextBL context)
        {
            //public static void SendNewFileTransferNotifications(FileTransfer newApproval, List<int> internalCollaborators, Account loggedAccount, GMGColorDAL.User loggedUser, string optionalMessage, DbContextBL context)
            //{

            List<int> internalScheduledUsers = new List<int>();
            List<int> externalScheduledUsers = new List<int>();
            int loggedUserId = loggedUser?.ID ?? -1;
            int loggedAccountId = loggedAccount?.ID ?? -1;
            int newApprovalId = newApproval.ID;

            //List<int> externalUsers = sharedApprovals.Where(sa =>sa.ID == newApprovalId).Select(a => a.ExternalCollaborator.ID).ToList();


            using (var ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, GMGColorConfiguration.AppConfiguration.NewApprovalTimeOutInMinutes, 0)))
            {
                try
                {
                    context.Configuration.AutoDetectChangesEnabled = false;

                    // GMGColorLogging.log.InfoFormat(loggedAccountId, "AddNewApproval.SendNewApprovalNotifications LoggedAccountId: {0} |  LoggedUserId : {1} |InternalCollaborators: {2} | ExternalCollaborators: {3} | ApprovalID: {4} "
                    // , new object[] { loggedAccountId, loggedUserId, internalCollaborators.ToArray(), sharedApprovals.ToArray(), newApprovalId });

                    if (loggedUser != null)
                    {
                        internalScheduledUsers = GlobalNotificationBL
                            .GetScheduleUsersForEventType(loggedUser.Account, NotificationTypeParser.GetKey(NotificationType.NewFileTransferAdded), context)
                            .ToList();
                        externalScheduledUsers = internalScheduledUsers;
                    }
                    else
                    {
                        externalScheduledUsers = GlobalNotificationBL
                            .GetScheduleUsersForEventType(0, NotificationTypeParser.GetKey(NotificationType.NewFileTransferAdded), context)
                            .ToList();
                    }

                    //internal users
                    foreach (int internalCollaborator in internalCollaborators)
                    {
                        if (loggedUser == null)
                        {
                            int accId = UserBL.GetAccountId(internalCollaborator, context);
                            internalScheduledUsers = GlobalNotificationBL.GetScheduleUsersForEventType(accId, NotificationTypeParser.GetKey(NotificationType.NewFileTransferAdded), context).ToList();
                        }


                        var notification = CreateNewFileTransferNotification(newApprovalId, internalCollaborator, null, loggedUserId, optionalMessage);
                        NotificationServiceBL.CreateNotificationFileTransfer(notification, loggedUser, internalCollaborator, context, saveChanges: false);
                    }




                    //external users
                    foreach (var externalUserId in externalCollaborators)
                    {

                        var notification = CreateNewFileTransferNotification(newApprovalId, null, externalUserId, loggedUserId, optionalMessage);
                        NotificationServiceBL.CreateNotificationFileTransfer(notification, loggedUser, null, context, saveChanges: false);
                    }

                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    object message = $"AddNewApproval.SendNewFileTransferNotifications Failed LoggedAccountId: {loggedAccountId} | LoggedUserId: {loggedUserId} | InternalCollaborators: {internalCollaborators.ToArray()} | ApprovalID: {newApprovalId}";
                    // object message = $"AddNewApproval.SendNewFileTransferNotifications Failed LoggedAccountId: {loggedAccountId} | LoggedUserId: {loggedUserId} | InternalCollaborators: {internalCollaborators.ToArray()} | ExternalCollaborators {sharedApprovals.ToArray()}  | ApprovalID: {newApprovalId}";
                    GMGColorLogging.log.Error(loggedUserId, message, ex);
                    throw;
                }
                finally
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                }

                ts.Complete();
            }

        }





        private static NewFileTransfer CreateNewFileTransferNotification(int newApprovalId, int? internalCollaborator, int? externalCollaborator, int loggedUserId, string optionalMessage)
        {
            var notification = new NewFileTransfer
            {
                EventCreator = loggedUserId,
                InternalRecipient = internalCollaborator,
                ExternalRecipient = externalCollaborator,
                OptionalMessage = optionalMessage,
                ApprovalsIds = new int[] { newApprovalId }
            };

            return notification;
        }



        public static void PopulateFileTransferFolderModel(NewFileTransferFolderModel model, int loggedUserId, DbContextBL context)
        {
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(model.objLoggedAccount.ID, ProofStudioPenWidthDefaultValue, false, context);



            var objFolder = FolderBL.GetFolderByID(model.Folder, loggedUserId, context);

            model.Title = objFolder.Name;

            model.OriginalFileName = objFolder.Name;


            model.Creator = objFolder.Creator;
            model.Owner = objFolder.Creator;

            model.ApprovalVersionCollaboratorInfo = new FolderCollaboratorInfo();
            model.ApprovalVersionCollaboratorInfo.LoadCollaborats(objFolder.ID, context);
            
            model.IsDueDateSelected = !collaborateSettings.DisableFileDueDateinUploader;
            
        }




        public static void ProcessTheFolderTransfer(FileTransfer fileTransfer, Dictionary<int, string> approvalsDetails, GMGColorDAL.User loggedUser, Account loggedAccount)
        {
            GMGColorLogging.log.Info($" enter ProcessTheFolderTransfer function for collaborate folder with name {fileTransfer.FileName}");

            //zip the approvals on local temp
            using (DbContextBL context = new DbContextBL())
            {
                //make the folder where the zip will be stored

                string accountRegion = AccountBL.GetAccountRegion(loggedUser.ID, context);

                string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(loggedUser.ID, loggedAccount.ID, accountRegion);



                // string localTempFolder = "C:\\Temp\\";
                string localTempFolder = GMGColorConfiguration.AppConfiguration.PathToFileTranferTempFolder;
                GMGColorLogging.log.Info($" the path for the local temp file is : {localTempFolder}");

                //check if the Temp file exists
                bool folderExists = System.IO.Directory.Exists(localTempFolder);
                GMGColorLogging.log.Info($" the path for the local temp file exists at the moment : {folderExists}");

                //if the tenp folder does not exists, create it
                if (!folderExists)
                {
                    try
                    {
                        Directory.CreateDirectory(localTempFolder);
                        GMGColorLogging.log.Info($" try creating the directory : {localTempFolder}");
                        folderExists = true;
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.ErrorFormat(ex, " Creating the local temp file failed: exception {0} , temp local path {1}", ex.Message, localTempFolder);
                    }

                }

                string localZipPath = localTempFolder + fileTransfer.FileName;
                GMGColorLogging.log.Info($" the local  zip path is : {localZipPath}");

                try
                {
                    GMGColorLogging.log.Info($" START:");
                    var x = System.IO.File.Create(localZipPath);
                    GMGColorLogging.log.Info($" DONE: local zip path {localZipPath}");

                    using (var zipStream = new ZipOutputStream(x))
                    {
                        zipStream.UseZip64 = UseZip64.Off;
                        foreach (KeyValuePair<int, string> pair in approvalsDetails)
                        {
                            string itemFilePath = pair.Value;
                            GMGColorLogging.log.Info($" DONE: get itemFilePath  {itemFilePath}");

                            using (Stream fileStream = GMGColorIO.GetMemoryStreamOfAFile(itemFilePath, accountRegion))
                            {

                                var fileEntry = new ZipEntry(Path.GetFileName(itemFilePath));
                                GMGColorLogging.log.Info($" DONE: fileEntry  {itemFilePath}");

                                zipStream.PutNextEntry(fileEntry);
                                GMGColorLogging.log.Info($" DONE: zip stream put fileEntry  {itemFilePath}");

                                StreamUtils.Copy(fileStream, zipStream, new byte[4096]);
                                zipStream.CloseEntry();
                                fileStream.Close();
                            }
                        }

                        zipStream.IsStreamOwner = false;
                        zipStream.Flush();
                        zipStream.Close();
                        x.Close();

                    }
                }
                catch (Exception ex)
                {

                    GMGColorLogging.log.ErrorFormat(ex, " Creating the zip failed: exception {0} , local zip path {1}", ex.Message, localZipPath);
                }


                //move the zip to file transfer
                GMGColorLogging.log.Info($"Start moving the zip to temp");
                string dataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
                string destinationFolderPath = GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath + "/" + fileTransfer.Guid + "/";
                string destinationFilePath = string.Concat(destinationFolderPath, fileTransfer.FileName);
                //string destinationPath = string.Concat(dataFolder, zipPath);

                try
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {

                        GMGColor.AWS.AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), localZipPath, destinationFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                        GMGColorLogging.log.Info($" Uploaded zip from {localZipPath} to {destinationFilePath}");
                        System.IO.File.Delete(localZipPath);
                        GMGColorLogging.log.Info($" Deleteed file:{localZipPath}");

                    }
                    else
                    {
                        //create the destination folder
                        string localtemp = string.Concat(dataFolder, GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath, "\\", fileTransfer.Guid);
                        Directory.CreateDirectory(localtemp);
                        string destination = string.Concat(dataFolder, GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath, "\\", fileTransfer.Guid, "\\", fileTransfer.FileName);
                        try
                        {

                            File.Move(localZipPath, destination);
                        }

                        catch (Exception ex)
                        {

                            GMGColorLogging.log.ErrorFormat(ex, " Moving the zip to local file transfer failed: exception {0} , local zip path {1},destination path {2}", ex.Message, localZipPath, destination);
                        }
                    }


                }

                catch (Exception ex)
                {

                    GMGColorLogging.log.ErrorFormat(ex, " Moving the zip to temp failed: exception {0} , local zip path {1},destination path {2}", ex.Message, localZipPath, destinationFilePath);
                }


            }

            
        }



        public static void PopulateFileTransferApprovalModel(NewFileTransferApprovalModel model, int loggedUserId, DbContextBL context)
        {
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(model.objLoggedAccount.ID, ProofStudioPenWidthDefaultValue, false, context);



            var approval = ApprovalBL.GetApproval(model.Approval, context);

            model.Title = approval.FileName;

            model.OriginalFileName = approval.FileName;


            model.Creator = approval.Creator;
            model.Owner = approval.Owner;


            model.ApprovalVersionCollaboratorInfo = new ApprovalVersionCollaboratorInfo();
            model.ApprovalVersionCollaboratorInfo.LoadNewVersionCollaborats(approval.ID, context);


            model.ApprovalVersionCollaboratorInfo.LoadNewVersionExternalCollaborators(approval.ID, context);



            model.IsDueDateSelected = !collaborateSettings.DisableFileDueDateinUploader;


        }


        public static void ProcessTheApprovalTransfer(FileTransfer fileTransfer, string approvalGuid, GMGColorDAL.User loggedUser)
        {
            GMGColorLogging.log.Info($" enter ProcessTheApprovalTransfer function for approval with fileName {fileTransfer.FileName}");
            using (DbContextBL context = new DbContextBL())
            {
                string accountRegion = AccountBL.GetAccountRegion(loggedUser.ID, context);

                string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(fileTransfer.Creator, fileTransfer.AccountID, accountRegion);

                #region Move copy file from approvals to FileTransfer


                string sourceRelativeFilePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + approvalGuid + '/' + fileTransfer.FileName;
                string destinationFolderPath = GMGColorConfiguration.AppConfiguration.FileTransferFolderRelativePath + "/" + fileTransfer.Guid + "/";
                string destinationFilePath = destinationFolderPath + fileTransfer.FileName;
                GMGColorIO.FolderExists(destinationFolderPath, accountRegion, true);
                GMGColorIO.CopyFile(sourceRelativeFilePath, destinationFilePath, accountRegion);

                #endregion

            }

        }




        public static FileTransferHistory.FileTransferDetailsModel PopulateFileTransferDetailsModel(int approvalVersionId, Account loggedAccount, int userLocale, GMGColorContext context, bool isFromRestAPI = false)
        {
            var versionDetailsModel = new FileTransferHistory.FileTransferDetailsModel();

            var collaboratorsInfo = (from apv in context.FileTransfers

                                     where apv.ID == approvalVersionId
                                     select new
                                     {
                                         objApprovalVersion = apv,

                                     }).FirstOrDefault();

            var objFileTransfer = collaboratorsInfo.objApprovalVersion;

            versionDetailsModel.ID = objFileTransfer.ID;

            versionDetailsModel.Title = objFileTransfer.FileName;
            versionDetailsModel.objLoggedAccount = loggedAccount;


            versionDetailsModel.ColloaboratorActivities = GetFileTransferDetailsCollaboratorActivities(objFileTransfer, loggedAccount, userLocale, context);
            versionDetailsModel.ExternalColloaboratorActivities = GetFileTransferExternalCollaboratorActivities(objFileTransfer, loggedAccount, context);



            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccount.ID, ProofStudioPenWidthDefaultValue, false, context);


            return versionDetailsModel;
        }

        public static List<FileTransferHistory.FileTransferDetailsCollaboratorActivity> GetFileTransferDetailsCollaboratorActivities(FileTransfer objApproval, Account loggedAccount, int userLocale, GMGColorContext context)
        {


            List<FileTransferHistory.FileTransferDetailsCollaboratorActivity> activityList = new List<FileTransferHistory.FileTransferDetailsCollaboratorActivity>();

            //Select collaborators that have not been deleted or that have made a decision or an annotation for the specified approval

            var collaboratorsList = (from ft in context.FileTransfers
                                     join fti in context.FileTransferUserInternals on ft.ID equals fti.FileTransferID
                                     join u in context.Users on fti.UserInternalID equals u.ID
                                     join ac in context.Accounts on u.Account equals ac.ID
                                     where ft.ID == objApproval.ID
                                     select new
                                     {
                                         u.GivenName,
                                         u.FamilyName,
                                         u.EmailAddress,
                                         u.ID,
                                         fti.DownloadDate,
                                         ac.TimeZone,
                                         u.Username,
                                         u.Status
                                     }).ToList();



            foreach (var objAppCollaborator in collaboratorsList)
            {


                string completedHtml = string.Empty;

                DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAppCollaborator.TimeZone);


                activityList.Add(new FileTransferHistory.FileTransferDetailsCollaboratorActivity
                {
                    UserID = objAppCollaborator.ID,
                    UserName = objAppCollaborator.Username,
                    Name = objAppCollaborator.GivenName + " " + objAppCollaborator.FamilyName,
                    EmailAddress = objAppCollaborator.EmailAddress,
                    Downloaded = objAppCollaborator.DownloadDate,
                    UserStatus = objAppCollaborator.Status
                });

            }

            return activityList;
        }


        public static List<FileTransferHistory.FileTransferDetailsCollaboratorActivity> GetFileTransferExternalCollaboratorActivities(FileTransfer objApproval, Account loggedAccount, GMGColorContext context)
        {


            List<FileTransferHistory.FileTransferDetailsCollaboratorActivity> activityList = new List<FileTransferHistory.FileTransferDetailsCollaboratorActivity>();

            //Select collaborators that have not been deleted or that have made a decision or an annotation for the specified approval

            var collaboratorsList = (from ft in context.FileTransfers
                                     join fti in context.FileTransferUserExternals on ft.ID equals fti.FileTransferID
                                     join u in context.ExternalCollaborators on fti.UserExternalID equals u.ID
                                     join ac in context.Accounts on u.Account equals ac.ID
                                     where ft.ID == objApproval.ID
                                     select new
                                     {
                                         u.GivenName,
                                         u.FamilyName,
                                         u.EmailAddress,
                                         u.ID,
                                         fti.DownloadDate,
                                         ac.TimeZone,

                                     }).ToList();

            string showExternalUsersEmailId = (from asn in context.AccountSettings where asn.Account == loggedAccount.ID && asn.Name == "DisableExternalUsersEmailIndicator" select asn.Value).FirstOrDefault();


            foreach (var objAppCollaborator in collaboratorsList)
            {


                string completedHtml = string.Empty;

                DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAppCollaborator.TimeZone);


                activityList.Add(new FileTransferHistory.FileTransferDetailsCollaboratorActivity
                {
                    UserID = objAppCollaborator.ID,
                    UserName = objAppCollaborator.EmailAddress,
                    Name = objAppCollaborator.GivenName + " " + objAppCollaborator.FamilyName,
                    EmailAddress = showExternalUsersEmailId == "True" ? "" : objAppCollaborator.EmailAddress,
                    Downloaded = objAppCollaborator.DownloadDate



                });

            }

            return activityList;
        }







    }
}
