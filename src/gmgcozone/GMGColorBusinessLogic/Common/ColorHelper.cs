﻿
using System.Collections.Generic;
using System.Drawing;
using System;

namespace GMGColorBusinessLogic.Common
{
    public static class ColorHelper
    {

        /// <summary>
        /// Get the next available color in grayscale
        /// </summary>
        /// <param name="grayScaleColors"></param>
        /// <returns></returns>
        public static double GetNextAvailableColorInGrayscale(List<float> grayScaleColors)
        {
            double availableColor = 0;
            double maxDistance = 0;

            for (int i = 0; i < grayScaleColors.Count - 1; i++)
            {
                if (grayScaleColors[i + 1] - grayScaleColors[i] > maxDistance)
                {
                    maxDistance = grayScaleColors[i + 1] - grayScaleColors[i];
                    availableColor = grayScaleColors[i] + (grayScaleColors[i + 1] - grayScaleColors[i]) / 2;
                }
            }
            return availableColor;
        }
        
        /// <summary>
        /// Coverts the given grayscale color to RGB
        /// </summary>
        /// <param name="greyscaleValue"></param>
        /// <returns></returns>
        public static Color ColorFromHSV(double hue, double saturation, double value)
        {
            int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
            double f = hue / 60 - Math.Floor(hue / 60);

            value = value * 255;
            int v = Convert.ToInt32(value);
            int p = Convert.ToInt32(value * (1 - saturation));
            int q = Convert.ToInt32(value * (1 - f * saturation));
            int t = Convert.ToInt32(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return Color.FromArgb(255, t, p, v);
            else
                return Color.FromArgb(255, v, p, q);
        }

        /// <summary>
        /// REturns the color hex string representation 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string GetColorAsHex(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
