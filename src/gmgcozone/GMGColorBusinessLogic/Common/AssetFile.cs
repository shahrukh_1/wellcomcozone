﻿using System;
using System.IO;

namespace GMGColorBusinessLogic.Common
{
    public class AssetFile
    {
        public string Name { get; set; }
        public Stream Stream { get; set; }
        public int Size { get; set; }
        public string fileUUID { get; set; }
        public Int64 uploadFileSize { get; set; }
        public int chunkCount { get; set; }
        public string contentRange { get; set; }

        public AssetFile()
        {
            Stream = new MemoryStream();
            Name = String.Empty;
            Size = 0;
            fileUUID = String.Empty;
            uploadFileSize = 0;
            chunkCount = 0;
            contentRange = String.Empty;
        }
    }
}
