﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;

namespace GMGColorBusinessLogic.Common
{
    public static class Utils
    {
        /// <summary>
        /// Converts an array of string into an array of nullable int.
        /// </summary>
        /// <param name="obj">The string array</param>
        /// <returns></returns>
        public static int?[] ToIntArray(this string[] obj)
        {
            return obj.Select(o => o.ToInt()).ToArray();
        }

        /// <summary>
        /// Converts a string into a nullable int.
        /// </summary>
        /// <param name="obj">The string.</param>
        /// <returns></returns>
        public static int? ToInt(this string obj)
        {
            int retValue = 0;
            return Int32.TryParse(obj, out retValue) ? (int?)retValue : null;
        }

        public static string PathCombine(string path1, string path2)
        {
            return path1 + "/" + path2 + "/";
        }
        public static string PathCombine(string path1, string path2, string path3)
        {
            return path1 + "/" + path2 + "/" + path3 + "/";
        }
        public static string PathCombine(string path1, string path2, string path3, string path4)
        {
            return path1 + "/" + path2 + "/" + path3 + "/" + path4 + "/";
        }

        public static string PathCombine(string path1, string path2, string path3, string path4, string path5)
        {
            return path1 + "/" + path2 + "/" + path3 + "/" + path4 + "/" + path5 + "/";
        }

        /// <summary>
        /// Get number of days until next specified day of week
        /// </summary>
        /// <param name="from"></param>
        /// <param name="dayOfWeek"></param>
        /// <returns></returns>
        public static int DaysTillWeekDay(this DateTime from, DayOfWeek dayOfWeek)
        {
            int start = (int)from.DayOfWeek;
            int target = (int)dayOfWeek;
            if (target <= start)
                target += 7;
            return target - start;
        }

        public static List<AssetFile> UnzipStreamRootFolder(Stream stream, string rootFolderName)
        {
            stream.Position = 0;
            List<AssetFile> files = new List<AssetFile>();
            ZipInputStream zipInStream = new ZipInputStream(stream);
            ZipEntry entry;
            while ((entry = zipInStream.GetNextEntry()) != null)
            {
                string extension = Path.GetExtension(entry.Name);
                if ((String.IsNullOrEmpty(extension)) || entry.Name.Contains("__MACOSX") || extension == ".plist" || extension == ".DS_Store" || extension == ".zip")// skip folders and hidden files created with Mac OS (__MACOSX folder, plist)
                    continue;

                AssetFile file = new AssetFile();
                file.Name = rootFolderName + "/" + entry.Name;
                int size;
                byte[] buffer = new byte[2048];
                do
                {
                    size = zipInStream.Read(buffer, 0, buffer.Length);
                    file.Stream.Write(buffer, 0, size);
                    file.Size += size;
                } while (size > 0);

                files.Add(file);
            }
            return files;
        }

        /// <summary>
        /// Extract files from archive. The 
        /// </summary>
        /// 
        public static List<AssetFile> UnzipStreamSubFolders(Stream stream, string rootFolderName)
        {
            stream.Position = 0;
            List<AssetFile> files = new List<AssetFile>();
            ICSharpCode.SharpZipLib.Zip.ZipFile zipArchive = new ICSharpCode.SharpZipLib.Zip.ZipFile(stream);
            
            foreach (ZipEntry elementInsideZip in zipArchive)
            {
                String ZipArchiveName = elementInsideZip.Name;
                if (ZipArchiveName.Contains(".zip"))
                {
                    Stream zipStream = zipArchive.GetInputStream(elementInsideZip);
                    ZipInputStream zipInStream = new ZipInputStream(zipStream);
                    ZipEntry entry;
                    while ((entry = zipInStream.GetNextEntry()) != null)
                    {
                        string extension = Path.GetExtension(entry.Name);
                        if ((String.IsNullOrEmpty(extension)) || entry.Name.Contains("__MACOSX") || extension == ".plist" || extension == ".DS_Store" || extension == ".zip")// skip folders and hidden files created with Mac OS (__MACOSX folder, plist)
                            continue;

                        AssetFile file = new AssetFile();
                        file.Name = rootFolderName + "/" + elementInsideZip.ToString().Replace(".zip","") + "/" + entry.Name;

                        int size;
                        byte[] buffer = new byte[2048];
                        do
                        {
                            size = zipInStream.Read(buffer, 0, buffer.Length);
                            file.Stream.Write(buffer, 0, size);
                            file.Size += size;
                        } while (size > 0);

                        files.Add(file);
                    }

                }
            }

            return files;
        }
       
        public static string GetSHA256Base64(string message, bool ascii)
        {
            using (var hasher = new SHA256Managed())
            {
                if (ascii)
                {
                    return Convert.ToBase64String(hasher.ComputeHash(Encoding.ASCII.GetBytes(message)));
                }
                else
                {
                    return Convert.ToBase64String(hasher.ComputeHash(Encoding.UTF8.GetBytes(message)));
                }
            }
        }

    }
}
