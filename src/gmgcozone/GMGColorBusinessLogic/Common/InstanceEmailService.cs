﻿//using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorNotificationService;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GMGColorBusinessLogic.Common
{
    public class InstanceEmailService
    {
        public bool SendForgotPasswordEmail(DbContextBL context, int eventCreator, int internalRecipient, string newPassword)
        {
            try
            {
                List<Email> emails = new List<Email>();
                string emailSenderAddress = string.Empty;

                Notification userNotificationsToSend = null;
                userNotificationsToSend = new LostPassword()
                {
                    CreatedDate = DateTime.UtcNow,
                    EventCreator = eventCreator,
                    InternalRecipient = internalRecipient,
                    NewPassword = newPassword
                };

                //set current notification thread culture
                Thread.CurrentThread.CurrentCulture =
                Thread.CurrentThread.CurrentUICulture =
                CultureInfo.CreateSpecificCulture(userNotificationsToSend.GetLocale(context));

                // as event occur
                string template = GMGColorCommon.ReadServerFile(GetTemplatePath("LostPassword.html"));
                string DigestAsEventOccur = GMGColorCommon.ReadServerFile(GetTemplatePath("digest_AsEventOccur.html"));
                string CommonOuterTemplate = GMGColorCommon.ReadServerFile(GetTemplatePath("CommonOuterTemplate.html"));

                Email email = userNotificationsToSend.GetNotificationEmail(template, DigestAsEventOccur, CommonOuterTemplate, context, false);

                Receiver receiverData = new Receiver();
                if (email != null)
                {
                    var Jsondata = new JObject();
                    email.subject = email.subject;
                    Jsondata.Add("subject", email.subject);
                    Jsondata.Add("htmlBody", email.bodyHtml);
                    receiverData.JSONTemplateData = Jsondata.ToString();
                    receiverData.ReceiverEmail = email.toEmail;
                }
                emailSenderAddress = email.fromEmail;
                SendBulkEmailUsingTemplate(receiverData, emailSenderAddress);
                return true;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("SendForgotPasswordEmail: Create Email exception: ", ex);
            }
            return false;
        }

        public void SendBulkEmailUsingTemplate(Receiver receiversAddress, string senderAddress)
        {
            string TemplateName = "SampleTemplate20";
            string TemplateDefaultData = "{ \"subject\":\"sample subject\", \"htmlBody\": \"sample html body\" }";
            List<BulkEmailDestination> destinations = new List<BulkEmailDestination>();

            destinations.Add(new BulkEmailDestination
            {
                Destination = new Amazon.SimpleEmail.Model.Destination
                {
                    ToAddresses = new List<string> { receiversAddress.ReceiverEmail }
                },
                ReplacementTemplateData = receiversAddress.JSONTemplateData
            });

            using (var client = new Amazon.SimpleEmail.AmazonSimpleEmailServiceClient(new Amazon.Runtime.BasicAWSCredentials("AKIAR6KWPSIL75ELFDFO", "Y4H+zpWRnUfLWZCNGPT/AExRy43B22QMprjxFBMW"), Amazon.RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendBulkTemplatedEmailRequest
                {
                    Source = senderAddress,
                    Template = TemplateName,
                    Destinations = destinations,
                    DefaultTemplateData = TemplateDefaultData
                };

                var response = client.SendBulkTemplatedEmailAsync(sendRequest);
                response.Wait();

                JObject jsonData = JObject.Parse(receiversAddress.JSONTemplateData);
                string subject = jsonData.GetValue("subject").ToString();

                string message = String.Format("InstanceEmailService: Sending the email to user {0} with subject {1}", receiversAddress.ReceiverEmail, subject);
                GMGColorLogging.log.Warn(message);
            }
        }

        private string GetTemplatePath(string templateRelativePath)
        {
            string fileServerPath = System.Web.HttpContext.Current == null ? System.Web.Hosting.HostingEnvironment.MapPath("/") : System.Web.HttpContext.Current.Server.MapPath("/");
            string profileAbsPath = Path.Combine(fileServerPath, "Views/NotificationTemplates", templateRelativePath);

            if (File.Exists(profileAbsPath))
            {
                return profileAbsPath;
            }

            return null;
        }

    }

    public class Receiver
    {
        public string ReceiverEmail { get; set; }
        public string JSONTemplateData { get; set; }
    }
}
