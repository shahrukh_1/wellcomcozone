﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Transactions;
using GMG.CoZone.InteropWrapper;
using GMGColor.Resources;
using GMGColorBusinessLogic.SoftProofingJobs;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.Api;
using GMGColorNotificationService;
using ICSharpCode.SharpZipLib.Zip;
using Workstation = GMGColorDAL.Workstation;
using GMGColorBusinessLogic.Common;

namespace GMGColorBusinessLogic
{
    public class SoftProofingBL : BaseBL
    {
        #region Constants

        public const int ConnectedTimeoutInMinutes = 5;
        public const string ProfileFolder = "Profile";

        #endregion

        #region Classes
        public class SoftProofingSwitch
        {
            public SoftProofingSwitchEnum WorkstationType { get; set; }
        }

        public class SettingsParam
        {
            public SoftProofingSwitchEnum SettingsType { get; set; }
            public int LoggedUserId { get; set; }
            public int LoggedAccountId { get; set; }
            public int CurrentPage { get; set; }
            public string SearchText { get; set; }
            public string SortField { get; set; }
            public string SortDirection { get; set; }
            public int RowsPerPage { get; set; }

            public List<int> SelectedUsers { get; set; }
            public List<string> SelectedWorkstations { get; set; }
            public DateTime? CalibrationStartDate { get; set; }
            public DateTime? CalibrationEndDate { get; set; }
        }
        #endregion

        #region Internal/Private Methods
        
        /// <summary>
        /// Saves the workstation to database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="workstationCalibrationDataGuid">The workstation calibration data GUID.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        private static string SaveWorkstationToDatabase(SoftProofing model, string workstationCalibrationDataGuid, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            string workstationGuid = Guid.NewGuid().ToString();
            try
            {
                bool sendNotificationEmail = false;

                // the following line will validate the authkey against db
                int userId = UserBL.GetUserId(model.AuthKey, context);

                DateTime calibrationTime = DateTime.Parse(model.CalibrationDateTimeUTC, null, System.Globalization.DateTimeStyles.RoundtripKind);
                using (TransactionScope tscope = new TransactionScope())
                {
                    Workstation workstation =
                        (from w in context.Workstations
                         where w.MachineId == model.MachineId && w.User == userId && w.IsDeleted == false
                         select w).FirstOrDefault();

                    if (workstation == null)
                    {
                        sendNotificationEmail = true;
                        workstation = new Workstation
                        {
                            Guid = workstationGuid,
                            Location = String.Empty,
                            MachineId = model.MachineId,
                            User = userId,
                            WorkstationName = model.WorkstationName
                        };
                        context.Workstations.Add(workstation);
                    }

                    WorkstationCalibrationData workstationCalibrationData = new WorkstationCalibrationData()
                    {
                        Guid = workstationCalibrationDataGuid,
                        Filename = model.FileName,
                        Workstation1 = workstation,
                        Processed = false,
                        UploadedDate = DateTime.UtcNow,
                        LastErrorMessage = String.Empty,
                        CalibrationSoftware = model.CalibrationSoftware,
                        DisplayIdentifier = model.DisplayIdentifier,
                        DisplayName = model.DisplayName,
                        CalibrationUploadStatus = true,
                        CalibrationDate = calibrationTime,
                        DisplayProfileName = model.DisplayProfileName,
                        DisplayHeight = model.DisplayHeight,
                        DisplayWidth = model.DisplayWidth
                    };

                    context.WorkstationCalibrationDatas.Add(workstationCalibrationData);

                    context.SaveChanges();
                    tscope.Complete();
                }
                if (sendNotificationEmail)
                {
                    SendNotificationToAllAdmins(workstationCalibrationDataGuid, context, loggedUser);
                }
                return workstationGuid;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSaveWorkstation, ex);
            }
        }

        /// <summary>
        /// Uploads the workstation data to s3.
        /// </summary>
        /// <param name="model">EF model.</param>
        /// <param name="fileGuid">The file GUID.</param>
        /// <param name="accountRegion">The account region.</param>
        private static void UploadWorkstationDataToS3(SoftProofing model, string fileGuid, string accountRegion)
        {
            try
            {
                string relativeDestinationFilePath = GMGColorConfiguration.AppConfiguration.SoftProofDataFolderRelativePath + "/" + fileGuid + "/";
                GMGColorIO.FolderExists(relativeDestinationFilePath, accountRegion, true);

                byte[] dataArray = null;
                try
                {
                    dataArray = Convert.FromBase64String(model.FileData);
                }
                catch (Exception ex)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotExtractByteArrayFromBase64String, ex);
                }

                // copy original sent file
                using (MemoryStream data = new MemoryStream(dataArray))
                {
                    GMGColorIO.MoveStream(data, relativeDestinationFilePath + model.FileName, accountRegion);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSaveWorkstationDataToS3Bucket, ex);
            }
        }

        /// <summary>
        /// Deletes the workstation folder from s3.
        /// </summary>
        /// <param name="fileGuid">The file GUID.</param>
        /// <param name="accountRegion">The account region.</param>
        private static void DeleteWorkstationFolderFromS3(string fileGuid, string accountRegion)
        {
            try
            {
                string relativeDestinationFilePath = GMGColorConfiguration.AppConfiguration.SoftProofDataFolderRelativePath + "/" + fileGuid + "/";

                if (GMGColorIO.FolderExists(relativeDestinationFilePath, accountRegion))
                {
                    GMGColorIO.DeleteFolder(relativeDestinationFilePath, accountRegion);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteWorkstationFolderFromS3, ex);
            }
        }

        /// <summary>
        /// Deletes the workstation calibration data from database.
        /// </summary>
        /// <param name="workstationCalibrationDataGuid">The file GUID.</param>
        /// <param name="context">EF context.</param>
        private static void DeleteWorkstationCalibrationDataFromDatabase(string workstationCalibrationDataGuid, GMGColorContext context)
        {
            try
            {
                int workstationId = 0;
                WorkstationCalibrationData workstationCalibrationData = (from wcd in context.WorkstationCalibrationDatas
                                                                         where wcd.Guid == workstationCalibrationDataGuid
                                                                         select wcd).FirstOrDefault();
                if (workstationCalibrationData != null)
                {
                    workstationId = workstationCalibrationData.Workstation;
                    DALUtils.Delete<WorkstationCalibrationData>(context, workstationCalibrationData);
                }

                if (workstationId > 0)
                {
                    bool anyOtherCalibrationData =
                        (from wcd in context.WorkstationCalibrationDatas
                         where wcd.Workstation == workstationId && wcd.Guid != workstationCalibrationDataGuid
                         select wcd.ID).Any();

                    if (!anyOtherCalibrationData)
                    {
                        Workstation workstation =
                            (from w in context.Workstations where w.ID == workstationId select w).FirstOrDefault();
                        if (workstation != null)
                        {
                            DALUtils.Delete<Workstation>(context, workstation);
                        }
                    }
                }
                context.SaveChanges();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteWorkstationCalibrationDataFromDatabase, ex);
            }
        }

        /// <summary>
        /// Deletes the workstation from database.
        /// </summary>
        /// <param name="workstationGuid">The workstation GUID.</param>
        /// <param name="context">EF context.</param>
        private static void DeleteWorkstationFromDatabase(string workstationGuid, GMGColorContext context)
        {
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    var workstationValidationResults = (from ws in context.Workstations
                                                        join wcd in context.WorkstationCalibrationDatas on ws.ID equals wcd.Workstation
                                                        join wvr in context.WorkstationValidationResults on wcd.ID equals wvr.WorkstationCalibrationData
                                                        where ws.Guid == workstationGuid
                                                        select wvr).ToList();

                    for (int j = workstationValidationResults.Count() - 1; j >= 0; j--)
                    {
                        DALUtils.Delete<WorkstationValidationResult>(context, workstationValidationResults[j]);
                    }

                    var workstationCalibrationDatas = (from ws in context.Workstations
                                                       join wcd in context.WorkstationCalibrationDatas on ws.ID equals wcd.Workstation
                                                       where ws.Guid == workstationGuid
                                                       select wcd).ToList();

                    for (int j = workstationCalibrationDatas.Count() - 1; j >= 0; j--)
                    {
                        DALUtils.Delete<WorkstationCalibrationData>(context, workstationCalibrationDatas[j]);
                    }

                    Workstation workstation = (from ws in context.Workstations where ws.Guid == workstationGuid select ws).FirstOrDefault();
                    if (workstation != null)
                    {
                        DALUtils.Delete<Workstation>(context, workstation);
                    }

                    context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CoultNotDeleteWorkstationFromDatabase, ex);
            }
        }

        /// <summary>
        /// Processes the deleted jobs.
        /// </summary>
        /// <param name="context">EF context.</param>
        /// <param name="nrOfJobs">The nr of jobs to be deleted (default 50 jobs).</param>
        private static void ProcessDeletedJobs(GMGColorContext context, int nrOfJobs = 50)
        {
            try
            {
                var deletedJobs = (from ws in context.Workstations
                                   where ws.IsDeleted == true
                                   orderby ws.ID ascending
                                   select new { ws.User, ws.Guid }).
                        Take(nrOfJobs).ToList();

                string exceptionMessage = null;
                for (int i = deletedJobs.Count() - 1; i >= 0; i--)
                {
                    try
                    {
                        var deletedJob = deletedJobs.ElementAt(i);

                        string accountRegion = AccountBL.GetAccountRegion(deletedJob.User, context);

                        var deletedFolderGuids = (from ws in context.Workstations
                                                  join wcd in context.WorkstationCalibrationDatas on ws.ID equals
                                                      wcd.Workstation
                                                  where ws.Guid == deletedJob.Guid
                                                  select wcd.Guid).ToList();
                        foreach (string deletedFolderGuid in deletedFolderGuids)
                        {
                            DeleteWorkstationFolderFromS3(deletedFolderGuid, accountRegion);
                        }
                        DeleteWorkstationFromDatabase(deletedJob.Guid, context);
                    }
                    catch (ExceptionBL exbl)
                    {
                        if (exceptionMessage == null)
                        {
                            exceptionMessage = exbl.Message;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (exceptionMessage == null)
                        {
                            exceptionMessage = ex.Message;
                        }
                    }
                    finally
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
                if (exceptionMessage != null)
                {
                    throw new Exception(exceptionMessage);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessDeletedJobs, ex);
            }
        }

        /// <summary>
        /// Unzips the file.
        /// </summary>
        /// <param name="workstationCalibrationDataGuid">The workstation calibration data GUID.</param>
        /// <param name="jobFilename">The job filename.</param>
        /// <param name="accountRegion">The account region.</param>
        /// <param name="context">The context.</param>
        private static void UnzipFile(string workstationCalibrationDataGuid, string jobFilename, string accountRegion, GMGColorContext context)
        {
            try
            {
                string relativeDestinationFilePath =
                        GMGColorConfiguration.AppConfiguration.SoftProofDataFolderRelativePath + "/" +
                        workstationCalibrationDataGuid + "/";

                string submitedFile = relativeDestinationFilePath + jobFilename;
                using (Stream data = GMGColorIO.GetMemoryStreamOfAFile(submitedFile, accountRegion))
                {
                    ZipFile zipFile = new ZipFile(data);
                    foreach (ZipEntry zipFileEntry in zipFile)
                    {
                        if (zipFileEntry.IsDirectory && zipFileEntry.Name != "/")
                        {
                            string directory = zipFileEntry.Name.Substring(0, zipFileEntry.Name.LastIndexOf("/"));
                            GMGColorIO.FolderExists(relativeDestinationFilePath + directory, accountRegion, true);
                        }

                        if (zipFileEntry.IsFile)
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                zipFile.GetInputStream(zipFileEntry).CopyTo(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                GMGColorIO.MoveStream(ms, relativeDestinationFilePath + zipFileEntry.Name, accountRegion);
                            }
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (ZipException zex)
            {
                SetProcessedJobToError(workstationCalibrationDataGuid, Resources.lblSoftProofingErrInvalidCalibrationData, context);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotExtraFilesFromInvalidFile, zex);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotExtractFilesFromSubmitedFile, ex);
            }
        }

        /// <summary>
        /// Processes the new jobs.
        /// </summary>
        /// <param name="context">EF context.</param>
        /// <param name="nrOfJobs">The nr of jobs to be processed (default 50 jobs).</param>
        private static void ProcessNewJobs(GMGColorContext context, string workstationCalibrationDataGuid)
        {

            string key = Guid.NewGuid().ToString().ToLower();

            WorkstationCalibrationData workstationCalibrationData = context.WorkstationCalibrationDatas
                                                    .Where(wcd => wcd.Guid == workstationCalibrationDataGuid && wcd.Processed == false)
                                                    .FirstOrDefault();

            if (workstationCalibrationData == null)
            {
                //no jobs found for processing
                return;
            }

            workstationCalibrationData.ProcessingInProgress = key;
            context.SaveChanges();

            Workstation workstation = context.Workstations.Where(ws => ws.ID == workstationCalibrationData.Workstation).FirstOrDefault();
            if (workstation == null)
            {
                //no workstation found for workstationCalibrationData
                return;
            }

            string exceptionMessage = null;
            Exception exception = null;

            try
            {
                string accountRegion = AccountBL.GetAccountRegion(workstation.User, context);
                string relativeDestinationFilePath = GMGColorConfiguration.AppConfiguration.SoftProofDataFolderRelativePath + "/" + workstationCalibrationDataGuid + "/";

                UnzipFile(workstationCalibrationDataGuid, workstationCalibrationData.Filename, accountRegion, context);

                ISoftProofingJob jobFile = null;
                CalibrationSoftwareEnum calibrationSoftwareEnum;
                if (Enum.TryParse((workstationCalibrationData.CalibrationSoftware ?? String.Empty).Replace(" ", String.Empty), true, out calibrationSoftwareEnum))
                {
                    Stream fileStream = null;
                    try
                    {
                        switch (calibrationSoftwareEnum)
                        {
                            case CalibrationSoftwareEnum.BasicColorDisplay:
                            {
                                string file = relativeDestinationFilePath + BasicColorDisplay.Filename;
                                if (GMGColorIO.FileExists(file, accountRegion))
                                {
                                    fileStream = GMGColorIO.GetMemoryStreamOfAFile(file, accountRegion);
                                    jobFile = new BasicColorDisplay();
                                }
                                else
                                {
                                    SetProcessedJobToError(workstationCalibrationDataGuid,
                                        Resources.lblSoftProofingErrMissingCalibrationData, context);
                                }
                                break;
                            }
                            case CalibrationSoftwareEnum.Other:
                                string profile = relativeDestinationFilePath + ProfileFolder + "/" +
                                                    workstationCalibrationData.DisplayProfileName;
                                if (GMGColorIO.FileExists(profile, accountRegion))
                                {
                                    fileStream = GMGColorIO.GetMemoryStreamOfAFile(profile, accountRegion);
                                    jobFile = new OtherCalibrationTool();
                                }
                                else
                                {
                                    SetProcessedJobToError(workstationCalibrationDataGuid,
                                        Resources.lblSoftProofingErrMissingCalibrationData, context);
                                }
                                break;
                            default:
                            {
                                break;
                            }
                        }
                        if (jobFile != null)
                        {
                            int displayIndex;
                            if (!int.TryParse(workstationCalibrationData.DisplayIdentifier, out displayIndex))
                            {
                                displayIndex = 0;
                            }
                            jobFile.ProcessJob(fileStream, workstation.User, null, displayIndex);
                            AddProcessedJobToDatabase(jobFile, workstationCalibrationDataGuid, context);
                        }
                    }
                    catch (DbEntityValidationException valEx)
                    {
                        string valErr = DALUtils.GetDbEntityValidationException(valEx);
                        throw  new Exception("Validation Exception: " + valErr, valEx);
                    }
                    catch (SoftProofingJobProcessingError spex)
                    {
                        SetProcessedJobToError(workstationCalibrationDataGuid, spex.Message, context);
                        throw;
                    }
                    catch (Exception ex)
                    {
                        SetProcessedJobToError(workstationCalibrationDataGuid, Resources.lblSoftProofingErrReceivingCalibrationData, context);
                        throw new ExceptionBL(ExceptionBLMessages.CouldNotAddedProcessedJobToDatabase, ex);
                    }
                    finally
                    {
                        if (fileStream != null)
                        {
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                }
                else
                {
                    SetProcessedJobToError(workstationCalibrationDataGuid, Resources.lblSoftProofingErrNotSupportedCalibrationSoftware, context);
                }
            }
            catch (ExceptionBL exbl)
            {
                if (exceptionMessage == null)
                {
                    exception = exbl;
                    exceptionMessage = exbl.Message;
                }
            }
            catch (Exception ex)
            {
                if (exceptionMessage == null)
                {
                    exception = ex;
                    exceptionMessage = ex.Message;
                }
            }

            if (exceptionMessage != null)
            {
                throw new Exception(exceptionMessage, exception);
            }
        }

        /// <summary>
        /// Adds the processed job to database.
        /// </summary>
        /// <param name="job">The job.</param>
        /// <param name="workstationCalibrationDataGuid">The workstation calibration data GUID.</param>
        /// <param name="context">EF context.</param>
        private static void AddProcessedJobToDatabase(ISoftProofingJob job, string workstationCalibrationDataGuid, GMGColorContext context)
        {
            try
            {
                WorkstationCalibrationData workstationCalibrationData =
                    (from wcd in context.WorkstationCalibrationDatas
                        where wcd.Guid == workstationCalibrationDataGuid
                        select wcd).FirstOrDefault();

                if (workstationCalibrationData == null)
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotFindCalibrationData);

                List<WorkstationValidationResult> previousResults = (from wrs in context.WorkstationValidationResults
                    join wcd in context.WorkstationCalibrationDatas on
                        wrs.WorkstationCalibrationData equals
                        wcd.ID
                    where wcd.Guid == workstationCalibrationDataGuid
                    select wrs).ToList();
                workstationCalibrationData.Processed = true;
                workstationCalibrationData.LastErrorMessage = String.Empty;
                workstationCalibrationData.ProcessingInProgress = null;

                if (job.CalibratedDate != default(DateTime))
                {
                    workstationCalibrationData.CalibrationDate = job.CalibratedDate;
                }

                if (!String.IsNullOrEmpty(job.CalibrationSoftwareSignature))
                {
                    var existingVendor = (from iccsig in context.ICCProfileCreatorSignatures
                        where job.CalibrationSoftwareSignature.ToLower() == iccsig.Signature.ToLower()
                        select iccsig.Name).FirstOrDefault();

                    workstationCalibrationData.CalibrationSoftware = existingVendor ?? job.CalibrationSoftwareSignature;
                }

                Workstation workstation = (from ws in context.Workstations
                    where ws.ID == workstationCalibrationData.Workstation
                    select ws).FirstOrDefault();

                if (workstation != null)
                {
                    workstation.IsCalibrated = job.IsValidated;
                }

                // clear the previous results
                for (int i = previousResults.Count() - 1; i >= 0; i--)
                {
                    DALUtils.Delete<WorkstationValidationResult>(context, previousResults[i]);
                }

                List<WorkstationValidationResult> validationResults =
                    job.Fields.Select(
                        o =>
                            new WorkstationValidationResult()
                            {
                                Name = o.Name,
                                Value =
                                    o.CurrentValue +
                                    (!String.IsNullOrEmpty(o.MaxValueAllowed)
                                        ? String.Format("(<{0})", o.MaxValueAllowed)
                                        : String.Empty),
                                WorkstationCalibrationData = workstationCalibrationData.ID
                            }).ToList();

                foreach (WorkstationValidationResult workstationValidationResult in validationResults)
                {
                    context.WorkstationValidationResults.Add(workstationValidationResult);
                }

                context.SaveChanges();
            }
            catch (DbEntityValidationException)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddedProcessedJobToDatabase, ex);
            }
        }

        /// <summary>
        /// Sets the processed job to error.
        /// </summary>
        /// <param name="workstationCalibrationDataGuid">The workstation calibration data GUID.</param>
        /// <param name="errorMessage">The error message.</param>
        /// <param name="context">EF context.</param>
        private static void SetProcessedJobToError(string workstationCalibrationDataGuid, string errorMessage, GMGColorContext context)
        {
            try
            {
                var workstationCalibrationData =
                    (from wcd in context.WorkstationCalibrationDatas
                        where wcd.Guid == workstationCalibrationDataGuid
                        select wcd).FirstOrDefault();

                if (workstationCalibrationData != null)
                {
                    workstationCalibrationData.Processed = true;
                    workstationCalibrationData.LastErrorMessage = errorMessage;
                    workstationCalibrationData.CalibrationUploadStatus = false;
                    workstationCalibrationData.ProcessingInProgress = null;
                }

                context.SaveChanges();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetProofingJobToError, ex);
            }
        }

        /// <summary>
        /// Sends the notification to all admins.
        /// </summary>
        /// <param name="workstationCalibrationDataGuid">The workstation GUID.</param>
        /// <param name="context">EF context.</param>
        internal static void SendNotificationToAllAdmins(string workstationCalibrationDataGuid, DbContextBL context, GMGColorDAL.User loggedUser)
        {
            try
            {
                var job = (from ws in context.Workstations
                           join wcd in context.WorkstationCalibrationDatas on ws.ID equals wcd.Workstation
                           join u in context.Users on ws.User equals u.ID
                           where wcd.Guid == workstationCalibrationDataGuid
                           orderby wcd.CalibrationDate descending
                           select new {ws.WorkstationName, ws.User, u.Account, ws.Guid}).FirstOrDefault();

                if (job != null)
                {
                    bool sentNotification = (from ws in context.Workstations
                                             join wcd in context.WorkstationCalibrationDatas on ws.ID equals
                                                 wcd.Workstation
                                             where ws.Guid == job.Guid && wcd.Processed == true && ws.IsDeleted == false
                                             select wcd.ID).Count() > 1;
                   
                    List<int> admins = (from u in context.Users
                                        join ur in context.UserRoles on u.ID equals ur.User
                                        join r in context.Roles on ur.Role equals r.ID
                                        join am in context.AppModules on r.AppModule equals am.ID
                                        join us in context.UserStatus on u.Status equals us.ID
                                        where
                                            u.Account == job.Account && r.Key == "ADM" &&
                                            am.Key == (int)GMG.CoZone.Common.AppModule.Admin && us.Key == "A"
                                        select u.ID).Distinct().ToList();

                    if (!sentNotification)
                    {
                        NotificationServiceBL.CreateNotification(new WorkstationRegistered()
                                                                        {
                                                                            EventCreator = job.User,
                                                                            Account = job.Account,
                                                                            WorkstationName = job.WorkstationName
                                                                        },
                                                                        loggedUser,
                                                                        context
                                                                    );
                        
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSendNotificationsToAllAdmins, ex);
            }
        }

        /// <summary>
        /// Moves user uploaded simulation profile from the temporary location to the storage location based on account region
        /// </summary>
        /// <param name="accountGuid"></param>
        /// <param name="accountRegion"></param>
        /// <param name="profileGuid"></param>
        /// <param name="loggedUserId"></param>
        /// <param name="accountId"></param>
        /// <param name="simulationProfileName"></param>
        private static void UploadSimulationProfile(bool isSysAdmin, string accountGuid, string accountRegion, string profileGuid, int loggedUserId, int accountId, string simulationProfileName)
        {
            // get simualtion profile temp location
            string simulationProfileTempLocation = GMGColorDAL.User.GetUserTempFolderPath(loggedUserId, accountId, accountRegion) + simulationProfileName;

            if (GMGColorIO.FileExists(simulationProfileTempLocation, accountRegion))
            {
                string relativeDestinationPath;

                if (isSysAdmin)
                {
                    relativeDestinationPath = GMGColorConfiguration.AppConfiguration.SoftProofingProfilesRepo + "/";
                }
                else
                {
                    //get destination profile path
                    relativeDestinationPath = GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath + "/" +
                                              accountGuid + "/" +
                                              GMGColorConfiguration.AppConfiguration.SimulationProfilesRelativePath + "/" +
                                              profileGuid + "/";

                    //create destination folder
                    GMGColorIO.FolderExists(relativeDestinationPath, accountRegion, true);
                }
                //move file to destination folder
                if (!GMGColorIO.FileExists(relativeDestinationPath + simulationProfileName, accountRegion))
                {
                    GMGColorIO.MoveFile(simulationProfileTempLocation, relativeDestinationPath + simulationProfileName, accountRegion);   
                }
            }
        }

        /// <summary>
        /// Create a queeue message when a Simulation Profile is uploaded/created
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="softProofinAction"></param>
        private static void CreateQueueMessageAfterSimulationProfileUpload(int profileId, GMG.CoZone.Common.SoftProofingAction softProofinAction)
        {
            try
            {
                //Create queue message to be sent to SoftProofing Data Service
                var dict = new GMG.CoZone.Common.CustomDictionary<string, string>
                {
                    {GMG.CoZone.Common.Constants.SimulationProfileMsgProfilelID, profileId.ToString()},
                    {GMG.CoZone.Common.Constants.Action, softProofinAction.ToString()}
                };
                GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.SoftProofing);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateSimulationProflieQueuMessage, ex);
            }
        }

        /// <summary>
        /// Search paper tints. User can type a paper tint name or media category
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        private static List<PaperTintDashBoardViewModel> SearchPaperTints(SettingsParam param)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    return (from pt in context.PaperTints
                            join ac in context.Accounts on pt.AccountId equals ac.ID
                            join mct in context.MediaCategories on pt.MediaCategory equals mct.ID
                            let measurementConditions = (from pptm in context.PaperTintMeasurements
                                                         join mc in context.MeasurementConditions on pptm.MeasurementCondition equals mc.ID
                                                         where pptm.PaperTint == pt.ID
                                                         select new DashboardMeasurementConditions() 
                                                         {
                                                             ID = mc.ID,
                                                             Name = mc.Name,
                                                             L = pptm.L,
                                                             a = pptm.a,
                                                             b = pptm.b
                                                         }).ToList()
                            where pt.AccountId == param.LoggedAccountId && (pt.Name.Contains(param.SearchText) || mct.Name.Contains(param.SearchText))
                            select new PaperTintDashBoardViewModel
                            {
                                ID = pt.ID,
                                Name = pt.Name,
                                MediaCategory = mct.Name,
                                MeasurementConditions = measurementConditions
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrievePaperTints, ex);
            }
        }
              
        /// <summary>
        /// Search for uploaded simulation profile file and if found delete the folder
        /// </summary>
        /// <param name="account"></param>
        /// <param name="profileGuid"></param>
        /// <param name="fileName"></param>
        private static void DeleteSimulationProfileUploadedFile(bool isSysAdmin, string region, string accGuid, string profileGuid, string fileName)
        {
            try
            {
                //get destination profile path
                string relativeDestinationPath;
                if (isSysAdmin)
                {
                    relativeDestinationPath = GMGColorConfiguration.AppConfiguration.SoftProofingProfilesRepo + "/";
                    GMGColorIO.DeleteFile(relativeDestinationPath + fileName, region);
                }
                else
                {
                    relativeDestinationPath = GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath + "/" +
                                              accGuid + "/" +
                                              GMGColorConfiguration.AppConfiguration.SimulationProfilesRelativePath + "/" +
                                              profileGuid + "/";
                    GMGColorIO.DeleteFolder(relativeDestinationPath, region);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteSimulationProflieUploadedFile, ex);
            }
        }

        /// <summary>
        /// Check if a simulation profile should be deleted or marked as exluded for a perticula account
        /// </summary>
        /// <param name="isSysAdmin"></param>
        /// <param name="context"></param>
        /// <param name="profileId"></param>
        /// <param name="profileInfo"></param>
        /// <param name="loggedAccountRegion"></param>
        /// <param name="loggedAccountGuid"></param>
        private static void CheckSimulationInUseStatusAndRemove(bool isSysAdmin, DbContextBL context, int? profileId, SimulationProfile profileInfo, string loggedAccountRegion, string loggedAccountGuid)
        {
            // check if simulation profile is being used a a viewing condition
            var isInUse = (from vw in context.ViewingConditions
                           where vw.SimulationProfile == profileId
                           select vw.ApprovalJobViewingConditions).ToList();
            var isExcludedInfo = (from eadp in context.ExcludedAccountDefaultProfiles
                                  where eadp.SimulationProfile == profileId
                                  select eadp).ToList();
            if (isInUse.Count > 0)
            {
                profileInfo.IsDeleted = true;
                context.ApprovalJobViewingConditions.RemoveRange(isInUse.SelectMany(v => v));

            }
            else
            {
                //remove profile if not used
                context.SimulationProfiles.Remove(profileInfo);

                //remove exclusion entry if the user is Sys Admin
                if (isSysAdmin)
                {
                    foreach (var item in isExcludedInfo)
                    {
                        context.ExcludedAccountDefaultProfiles.Remove(item);
                    }
                }

                // delete the simulation profile uploaded file
                DeleteSimulationProfileUploadedFile(isSysAdmin, loggedAccountRegion, loggedAccountGuid, profileInfo.Guid, profileInfo.FileName);
            }
        }


        #endregion

        #region Public Methods

        /// <summary>
        /// Saves the workstation data (update existing or add a new one).
        /// </summary>
        /// <param name="model">EF model.</param>
        /// <returns></returns>
        public static string SaveWorkstationData(SoftProofing model, GMGColorDAL.User loggedUser)
        {
            try
            {
                string workstationCalibrationDataGuid = Guid.NewGuid().ToString();

                using (DbContextBL context = new DbContextBL())
                {
                    //Save workstation to database
                    string workstationGuid = SaveWorkstationToDatabase(model, workstationCalibrationDataGuid, loggedUser, context);

                    //Upload file to S3
                    string accountRegion = AccountBL.GetAccountRegion(model.AuthKey, context);
                    try
                    {
                        UploadWorkstationDataToS3(model, workstationCalibrationDataGuid, accountRegion);
                    }
                    catch (ExceptionBL)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        // delete folder from S3 bucket if exists
                        DeleteWorkstationFolderFromS3(workstationCalibrationDataGuid, accountRegion);

                        // if any error then clear the database
                        DeleteWorkstationCalibrationDataFromDatabase(workstationGuid, context);
                        throw new ExceptionBL(ExceptionBLMessages.CouldNotUploadWorkstationDataToS3, ex);
                    }
                }
                return workstationCalibrationDataGuid;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessWorkstation, ex);
            }
        }

        /// <summary>
        /// Gets the settings model for SoftProofing setting page.
        /// </summary>
        /// <param name="param">The parameters.</param>
        /// <returns></returns>
        public static SoftProofingSettings GetSettings(SettingsParam param, AccountType.Type loggedAccountType)
        {
            try
            {
                SoftProofingSettings result = new SoftProofingSettings()
                                      {
                                          Page = param.CurrentPage,
                                          SearchText = param.SearchText,
                                          SortField = param.SortField,
                                          SortDir = param.SortDirection,
                                          Type = param.SettingsType,
                                          RowsPerPage = param.RowsPerPage > 0 ? param.RowsPerPage : 10,
                                          Filters =
                                              new ReportFilters()
                                                  {
                                                      SelectedUsers = param.SelectedUsers,
                                                      SelectedWorkstations = param.SelectedWorkstations,
                                                  }
                                      };
                if (param.SettingsType == SoftProofingSwitchEnum.Workstations)
                {
                    using (var context = new GMGColorContext())
                    {
                        var workstations = (
                                               from ws in context.Workstations
                                               let wcd =
                                                   context.WorkstationCalibrationDatas.Where(
                                                       o => o.Workstation == ws.ID && o.Processed == true)
                                                   .OrderByDescending(cl => cl.UploadedDate).Take(1)
                                                   .FirstOrDefault()
                                               join u in context.Users on ws.User equals u.ID
                                               where wcd != null && ws.IsDeleted == false && u.Account == param.LoggedAccountId
                                               orderby ws.ID descending
                                               select new GMGColorDAL.CustomModels.Workstation()
                                                          {
                                                              ID = ws.ID,
                                                              MachineId = ws.MachineId,
                                                              IsCalibrated =
                                                                  ws.IsCalibrated.HasValue &&
                                                                  ws.IsCalibrated.Value,
                                                              Username = u.Username,
                                                              CalibrationDate = wcd.CalibrationDate,
                                                              Name = ws.WorkstationName,
                                                              LastErrorMessage = wcd.LastErrorMessage,
                                                              DisplayProfileName = wcd.DisplayProfileName,
                                                              CalibrationSoftware = wcd.CalibrationSoftware
                                                          }
                                           );

                        result.Workstations = (string.IsNullOrEmpty(param.SearchText)
                                                   ? workstations.ToList()
                                                   : workstations.Where(o => o.Name.ToLower().Contains(param.SearchText.ToLower()) || o.Username.ToLower().Contains(param.SearchText.ToLower()))
                                                         .ToList());
                        result.TotalRowsNr = result.Workstations.Count();


                        string timeZoneName = AccountBL.GetTimeZoneName(param.LoggedAccountId, context);
                        TimeZoneInfo timeZone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(o => o.Id == timeZoneName);

                        string dateFormatPattern = DateTimeFormatBL.GetDateFormatPattern(param.LoggedUserId, context);
                        int timeFormat = DateTimeFormatBL.GetTimeFormatPattern(param.LoggedUserId, context);

                        foreach (GMGColorDAL.CustomModels.Workstation item in result.Workstations)
                        {
                            // converts datetime using the current timezone
                            if (timeZone != null && item.CalibrationDate.HasValue)
                            {
                                item.CalibrationDate = (DateTime?)TimeZoneInfo.ConvertTimeFromUtc(item.CalibrationDate.Value, timeZone);
                            }
                            item.LastVerificationDate = item.CalibrationDate.HasValue
                                                            ? String.Format("{0} {1}",
                                                                            GMGColorFormatData.GetFormattedDate(item.CalibrationDate.Value, dateFormatPattern),
                                                                            GMGColorFormatData.GetFormattedTime(item.CalibrationDate.Value, timeFormat))
                                                            : String.Empty;
                        }

                        var setting = AccountSettingsBL.GetSoftproofingCalibrationTimeoutDays(context, param.LoggedAccountId);
                        result.CalibrationTimeoutDays = setting.Value.ToInt().GetValueOrDefault();
                    }
                }
                else if (param.SettingsType == SoftProofingSwitchEnum.Reports)
                {
                    using (GMGColorContext context = new GMGColorContext())
                    {
                        string timeZoneName = AccountBL.GetTimeZoneName(param.LoggedAccountId, context);
                        TimeZoneInfo timeZone = TimeZoneInfo.GetSystemTimeZones().FirstOrDefault(o => o.Id == timeZoneName);

                        result.Filters.DateTimeFormat = DateTimeFormatBL.GetDateFormatPattern(param.LoggedUserId, context);

                        var reports = (from ws in context.Workstations
                                       join wcd in context.WorkstationCalibrationDatas on ws.ID equals
                                           wcd.Workstation
                                       join u in context.Users on ws.User equals u.ID
                                       let wrs =
                                           context.WorkstationValidationResults.Where(
                                               o => o.WorkstationCalibrationData == wcd.ID)
                                       where wcd != null && ws.IsDeleted == false && u.Account == param.LoggedAccountId && wcd.Processed == true
                                       orderby ws.ID descending, wcd.ID descending
                                       select new GMGColorDAL.CustomModels.WorkstationReport()
                                                  {
                                                      ID = ws.ID,
                                                      UserId = u.ID,
                                                      Username = u.Username,
                                                      CalibrationDate = wcd.CalibrationDate,
                                                      Name = ws.WorkstationName,
                                                      Results =
                                                          wrs.Select(o => o.Name + " " + o.Value).AsEnumerable(),
                                                      FileGuid = wcd.Guid
                                                  }
                                         ).OrderBy(GetSortingFieldAndDirection(param.SortField, param.SortDirection, SoftProofingSwitchEnum.Reports));

                        result.Filters.ListWorkstations = reports.Select(o => o.Name).Distinct().ToList();

                        result.Filters.ListUsers =
                            (from r in reports select new { r.UserId, r.Username }).ToList().Distinct().Select(
                                o => new KeyValuePair<int, string>(o.UserId, o.Username)).ToList();

                        if (param.SelectedUsers != null && param.SelectedUsers.Any())
                        {
                            reports = reports.Where(o => param.SelectedUsers.Contains(o.UserId));
                        }
                        if (param.SelectedWorkstations != null && param.SelectedWorkstations.Any())
                        {
                            reports = reports.Where(o => param.SelectedWorkstations.Contains(o.Name));
                        }
                        if (param.CalibrationStartDate.HasValue && timeZone != null)
                        {
                            DateTime calibrationDateUtc = TimeZoneInfo.ConvertTimeToUtc(param.CalibrationStartDate.Value, timeZone);
                            reports = reports.Where(o => o.CalibrationDate.HasValue && DbFunctions.DiffDays(calibrationDateUtc, o.CalibrationDate.Value) >= 0);
                        }
                        if (param.CalibrationEndDate.HasValue && timeZone != null)
                        {
                            DateTime calibrationDateUtc = TimeZoneInfo.ConvertTimeToUtc(param.CalibrationEndDate.Value, timeZone);
                            reports = reports.Where(o => o.CalibrationDate.HasValue && DbFunctions.DiffDays(o.CalibrationDate.Value, calibrationDateUtc) >= 0);
                        }

                        result.TotalRowsNr = reports.Count();

                        DateTime[] arrMinMax = (from r in reports where r.CalibrationDate.HasValue select r.CalibrationDate.Value).ToArray();
                        if (arrMinMax.Any() && timeZone != null)
                        {
                            result.Filters.MinCalibrationDate = (DateTime?)TimeZoneInfo.ConvertTimeFromUtc(arrMinMax.Min(), timeZone);
                            result.Filters.MaxCalibrationDate = (DateTime?)TimeZoneInfo.ConvertTimeFromUtc(arrMinMax.Max(), timeZone);
                        }
                        if (!param.CalibrationStartDate.HasValue && !param.CalibrationEndDate.HasValue)
                        {
                            result.Filters.CalibrationStartString = result.Filters.MinCalibrationDate.GetValueOrDefault().ToString(result.Filters.DateTimeFormat);
                            result.Filters.CalibrationEndString = result.Filters.MaxCalibrationDate.GetValueOrDefault().ToString(result.Filters.DateTimeFormat);
                        }
                        else
                        {
                            result.Filters.CalibrationStartString = param.CalibrationStartDate.HasValue
                                                                  ? param.CalibrationStartDate.Value
                                                                        .ToString(
                                                                            result.Filters.DateTimeFormat)
                                                                  : null;
                            result.Filters.CalibrationEndString = param.CalibrationEndDate.HasValue
                                                                           ? param.CalibrationEndDate.Value.ToString(
                                                                                     result.Filters.DateTimeFormat)
                                                                           : null;
                        }

                        result.Reports = param.CurrentPage > -1
                                             ? reports.Skip((param.CurrentPage > 1 ? param.CurrentPage - 1 : 0)*
                                                            param.RowsPerPage).Take(param.RowsPerPage).ToList()
                                             : reports.ToList();

                        string dateFormatPattern = DateTimeFormatBL.GetDateFormatPattern(param.LoggedUserId, context);
                        int timeFormat = DateTimeFormatBL.GetTimeFormatPattern(param.LoggedUserId, context);

                        foreach (WorkstationReport report in result.Reports)
                        {
                            // converts datetime using the current timezone
                            if (timeZone != null && report.CalibrationDate.HasValue)
                            {
                                report.CalibrationDate = (DateTime?)TimeZoneInfo.ConvertTimeFromUtc(report.CalibrationDate.Value, timeZone);
                            }
                            // converts datetime using the current timezone
                            report.LastCalibrationDate = report.CalibrationDate.HasValue
                                                            ? String.Format("{0} {1}",
                                                                            GMGColorFormatData.GetFormattedDate(report.CalibrationDate.Value, dateFormatPattern),
                                                                            GMGColorFormatData.GetFormattedTime(report.CalibrationDate.Value, timeFormat))
                                                            : String.Empty;

                            report.ResultsString = string.Join("<br/>", report.Results);
                        }
                    }
                }
                else if (param.SettingsType == SoftProofingSwitchEnum.PaperTint)
                {
                    result.PaperTints = SearchPaperTints(param); 
                }
                else if (param.SettingsType == SoftProofingSwitchEnum.SimulationProfile)
                {
                    result.SimulationProfiles = SearchSimulationProfiles(param, loggedAccountType);
                }
                return result;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveWorkstations, ex);
            }
        }

     
        /// <summary>
        /// Deletes the workstation.
        /// </summary>
        /// <param name="model">EF model.</param>
        public static void DeleteWorkstation(SoftProofingSettings model)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    Workstation workstation = (from ws in context.Workstations where ws.ID == model.SelectedInstance select ws).FirstOrDefault();
                    if (workstation != null)
                    {
                        workstation.IsDeleted = true;
                    }
                    context.SaveChanges();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteWorkstation, ex);
            }
        }

        /// <summary>
        /// Updates the calibration timeout days value.
        /// </summary>
        /// <param name="model">EF model.</param>
        /// <param name="accountId">The account id.</param>
        public static void UpdateCalibrationTimeout(SoftProofingSettings model, int accountId)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    //add new value
                    AccountSettingsBL.SaveSetting(
                        AccountSettingsBL.AccountSettingsKeyEnum.SoftproofingCalibrationTimeoutDays,
                        model.CalibrationTimeoutDays.ToString(), AccountSettingsBL.AccountSettingValueType.IN32,
                        accountId,
                        context);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateCalibrationTimeout, ex);
            }
        }

        /// <summary>
        /// Processes new soft proofing jobs.
        /// </summary>
        public static void ProcessNewSoftProofingJobs(string workstationCalibrationDataGuid)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    ProcessNewJobs(context, workstationCalibrationDataGuid);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ProcessNewSoftProofingJobs method failed", ex);
            }
        }

        /// <summary>
        /// Processes deleted soft proofing jobs.
        /// </summary>
        public static void ProcessDeletedSoftProofingJobs()
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    ProcessDeletedJobs(context);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ProcessDeletedSoftProofingJobs method failed", ex);
            }
        }

        /// <summary>
        /// Sets the workstation connected.
        /// </summary>
        /// <param name="cookieValue">The cookie value.</param>
        /// <param name="logout">if set to <c>true</c> [logout].</param>
        public static void SetWorkstationConnected(string cookieValue, DbContextBL context, bool logout = false)
        {
            try
            {
                StartSession session = null;
                if (StartSession.TryParse(cookieValue, out session))
                {
                    SetWorkstationConnected(session.UserGuid, session.MachineId, context, logout);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetWorkstationToConnected, ex);
            }
        }

        /// <summary>
        /// Sets the workstation connected.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="machineId">The machine id.</param>
        /// <param name="context">EF context.</param>
        /// <param name="logout"> </param>
        public static void SetWorkstationConnected(string userGuid, string machineId, GMGColorContext context, bool logout = false)
        {
            //TODO this method should create the context inside it
            try
            {
                var workstation = (from ws in context.Workstations
                                   join u in context.Users on ws.User equals u.ID
                                   where ws.MachineId == machineId && u.Guid == userGuid && ws.IsDeleted == false
                                   select ws).FirstOrDefault();
                if (workstation != null)
                {
                    workstation.LastActivityDate = logout ? null : (DateTime?)DateTime.UtcNow;
                    context.SaveChanges();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetWorkstationToConnected, ex);
            }
        }

        /// <summary>
        /// Workstations the is processed.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="machineId">The machine id.</param>
        public static SoftProofingSessionState GetSessionState(string userGuid, string machineId)
        {
            SoftProofingSessionState session = SoftProofingSessionState.NotActive;
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    var workstationCalibrationDataIsProcessed = (from ws in context.Workstations
                             let wcd =
                                 context.WorkstationCalibrationDatas.Where(
                                     o => o.Workstation == ws.ID)
                                 .OrderByDescending(cl => cl.UploadedDate)
                                 .FirstOrDefault()
                             join u in context.Users on ws.User equals u.ID
                             where
                                 u.Guid == userGuid &&
                                 ws.MachineId == machineId &&
                                 ws.IsDeleted == false
                             select wcd.Processed).FirstOrDefault();


                    session = workstationCalibrationDataIsProcessed ? SoftProofingSessionState.Ok : SoftProofingSessionState.InProgress;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateSession, ex);
            }

            return session;
        }

        /// <summary>
        /// Gets the workstation.
        /// </summary>
        /// <param name="workstationId">The workstation id.</param>
        /// <param name="accountId">The account id where the workstation was created.</param>
        /// <returns></returns>
        public static WorkstationEdit GetWorkstation(int workstationId, int accountId)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return (from ws in context.Workstations
                            let wcd =
                                context.WorkstationCalibrationDatas.Where(
                                    o => o.Workstation == ws.ID && o.Processed == true)
                                .OrderByDescending(cl => cl.CalibrationDate)
                                .FirstOrDefault()
                            join u in context.Users on ws.User equals u.ID
                            where ws.ID == workstationId && u.Account == accountId && ws.IsDeleted == false
                            orderby ws.ID descending
                            select new WorkstationEdit()
                                       {
                                           ID = ws.ID,
                                           MachineId = ws.MachineId,
                                           Name = ws.WorkstationName,
                                           SelectedUserId = u.ID
                                       }
                           ).FirstOrDefault();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetWorkstationById, ex);
            }
        }

        /// <summary>
        /// Gets all active users having a workstation excluding the user of current workstation.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="workstationId"></param>
        /// <returns></returns>
        public static List<KeyValuePair<int, string>> GetUsersWithWorkstation(int accountId, int workstationId)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return (from u in context.Users
                            join uss in context.UserStatus on u.Status equals uss.ID
                            join ws in context.Workstations on u.ID equals ws.User
                            where u.Account == accountId && uss.Key == "A" && ws.ID != workstationId
                            select new { u.ID, u.Username}).ToList().Select(
                                o => new KeyValuePair<int, string>(o.ID, o.Username)).ToList();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUsersByAccountId, ex);
            }
        }

        /// <summary>
        /// Updates the workstation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="accountId">The account id.</param>
        /// <returns></returns>
        public static bool UpdateWorkstation(WorkstationEdit model, int accountId)
        {
            try
            {
                bool result = false;
                using (GMGColorContext context = new GMGColorContext())
                {
                    bool workstationAlreadyExists = (from ws in context.Workstations
                                                     where
                                                         ws.MachineId.ToLower() == model.MachineId.ToLower() &&
                                                         ws.User == model.SelectedUserId &&
                                                         ws.ID != model.ID
                                                     select ws.ID).Any();

                    Workstation workstation =
                        (from ws in context.Workstations
                         join u in context.Users on ws.User equals u.ID
                         where ws.ID == model.ID &&
                               ws.IsDeleted == false &&
                               u.Account == accountId
                         select ws).FirstOrDefault();

                    if (workstation != null)
                    {
                        workstation.User = model.SelectedUserId;
                        workstation.WorkstationName = model.Name;
                        context.SaveChanges();
                        result = true;
                    }
                }
                return result;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateWorkstation, ex);
            }
        }

        /// <summary>
        /// Checks if workstation already exists.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="machineId">The machine id.</param>
        /// <param name="workstationId">The workstation id.</param>
        /// <returns></returns>
        public static bool CheckIfWorkstationAlreadyExists(int userId, string machineId, int? workstationId = null)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return workstationId.HasValue
                               ? (from ws in context.Workstations
                                  where
                                      ws.MachineId.ToLower() == machineId.ToLower() &&
                                      ws.User == userId &&
                                      ws.ID != workstationId &&
                                      ws.IsDeleted == false
                                  select ws.ID).Any()
                               : (from ws in context.Workstations
                                  where
                                      ws.MachineId.ToLower() == machineId.ToLower() &&
                                      ws.User == userId &&
                                      ws.IsDeleted == false
                                  select ws.ID).Any();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfWorkstationAlreadyExists, ex);
            }
        }

        /// <summary>
        /// Converts the reports to CSV.
        /// </summary>
        /// <param name="reports">The reports.</param>
        /// <returns></returns>
        public static byte[] ConvertReportsToCsv(List<WorkstationReport> reports)
        {
            byte[] results = null;
            const string separator = ",";
            string fieldsSeparator = string.Format("\"{0}\"", separator);

            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (StreamWriter sw = new StreamWriter(ms))
                    {
                      sw.WriteLine(string.Join(",",
                                                 new string[]
                                                     {
                                                         Resources.lblSoftProofingWorkstationName,
                                                         Resources.lblSoftProofingWorkstationUsername,
                                                         Resources.lblSoftProofingWorkstationLastVerificationDate,
                                                         Resources.lblSoftProofingReportsResults
                                                     }));
                        foreach (WorkstationReport report in reports)
                        {
                            sw.Write("\"");
                            sw.Write(string.Join(fieldsSeparator, new string[]
                                                                      {
                                                                          report.Name,
                                                                          report.Username,
                                                                          report.LastCalibrationDate,
                                                                          string.Join("; ", report.Results).Replace(
                                                                              "Δ", "delta")
                                                                      }));
                            sw.WriteLine("\"");
                        }

                        sw.Flush();
                        ms.Seek(0, SeekOrigin.Begin);
                        results = GetBytes(ms);
                    }
                    
                }
                return results;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotConvertReportsToCsv, ex);
            }
        }

        /// <summary>
        /// Gets the sorting field and direction.
        /// </summary>
        /// <param name="sort">The sort.</param>
        /// <param name="sortdir">The sortdir.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private static string GetSortingFieldAndDirection(string sort, string sortdir, SoftProofingSwitchEnum type)
        {
            string columnForSearch = "wcd.ProcessedDate desc";

            if (sortdir.ToLower() != "asc" && sortdir.ToLower() != "desc")
            {
                sortdir = "desc";
            }

            if (type == SoftProofingSwitchEnum.Reports)
            {
                switch (sort.ToLower())
                {
                    case "name":
                        columnForSearch = "Name " + sortdir;
                        break;
                    case "username":
                        columnForSearch = "Username " + sortdir;
                        break;
                    case "software":
                        columnForSearch = "Software " + sortdir;
                        break;
                    case "machineid":
                        columnForSearch = "MachineId " + sortdir;
                        break;
                    case "calibrationdate":
                        columnForSearch = "CalibrationDate " + sortdir;
                        break;
                    case "uploadeddate":
                        columnForSearch = "UploadedDate " + sortdir;
                        break;
                    case "status":
                        columnForSearch = "Status " + sortdir;
                        break;
                    case "calibrationuploadstatus":
                    case "resultsstring":
                        columnForSearch = "CalibrationUploadStatus " + sortdir;
                        break;
                }
            }
            return columnForSearch;
        }

        public static SoftProofingSession GetSoftProofingStatus(StartSession session)
        {
            SoftProofingSession result = new SoftProofingSession()
            {
                Enabled = false,
                Level = SoftProofingLevel.NotCalibrated,
                MonitorName = string.Empty
            };

            try
            {
                using (DbContextBL context = new DbContextBL())
                        {
                            var softproofingStatus = (from ws in context.Workstations
                                                      join wcd in context.WorkstationCalibrationDatas on ws.ID equals wcd.Workstation
                                                      join u in context.Users on ws.User equals u.ID
                                                      where
                                                          u.Guid == session.UserGuid &&
                                                          ws.MachineId == session.MachineId &&
                                                          wcd.Processed == true
                                                      orderby wcd.UploadedDate descending
                                                      select
                                                          new
                                                              {
                                                                  wcd.DisplayName,
                                                                  ws.IsCalibrated,
                                                                  wcd.CalibrationDate,
                                                                  u.Account,
                                                                  wcd.DisplayWidth,
                                                                  wcd.DisplayHeight
                                                              }).FirstOrDefault();

                            if (softproofingStatus != null)
                            {
                                result.Enabled = true;
                                result.MonitorName = softproofingStatus.DisplayName;
                                result.Level = softproofingStatus.IsCalibrated.HasValue &&
                                               softproofingStatus.IsCalibrated.Value
                                                   ? SoftProofingLevel.Calibrated
                                                   : SoftProofingLevel.NotCalibrated;

                                result.DisplayHeight = softproofingStatus.DisplayHeight;
                                result.DisplayWidth = softproofingStatus.DisplayWidth;

                                int calibrationTimeoutDays =
                                    AccountSettingsBL.GetSoftproofingCalibrationTimeoutDays(context, softproofingStatus.Account).
                                        Value.ToInt().GetValueOrDefault();
                                if (result.Level == SoftProofingLevel.Calibrated &&
                                    (DateTime.UtcNow - softproofingStatus.CalibrationDate.Value).TotalDays >
                                    calibrationTimeoutDays)
                                {
                                    result.Level = SoftProofingLevel.Expired;
                                }
                            }
                        }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetSoftProofingStatus, ex);
            }
            return result;
        }

        /// <summary>
        /// Gets the soft proofing status.
        /// </summary>
        /// <param name="cookieValue">The cookie value.</param>
        /// <returns></returns>
        public static SoftProofingSession GetSoftProofingStatus(string cookieValue)
        {
            SoftProofingSession result = new SoftProofingSession()
            {
                Enabled = false,
                Level = SoftProofingLevel.NotCalibrated,
                MonitorName = string.Empty
            };
            try
            {
                if (!string.IsNullOrEmpty(cookieValue))
                {
                    StartSession softProofingSession = null;
                    if (StartSession.TryParse(cookieValue, out softProofingSession))
                    {
                        return GetSoftProofingStatus(softProofingSession);
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetSoftProofingStatus, ex);
            }
            return result;
        }

        /// <summary>
        /// Remove all the expired session. Sessions that have not been accessed in the specificed time.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="daysLastUsed"></param>
        public static void DeleteProofStudioExpiredSessions(DbContextBL context, int daysLastUsed)
        { 
            //Get expired sessions from the database
            var nowTime = DateTime.UtcNow;

            var expiredSessionsList = (from s in context.SoftProofingSessionParams
                                       join a in context.Approvals on s.Approval equals a.ID
                                       join j in context.Jobs on a.Job equals j.ID
                                       join ac in context.Accounts on j.Account equals ac.ID
                                       where DbFunctions.DiffDays(s.LastAccessedDate, nowTime) >= daysLastUsed && !s.DefaultSession
                                       select new 
                                       {
                                           s.ID,
                                           softproofingparams = s,
                                           approvalGuid = a.Guid,
                                           ac.Region,
                                       }).ToList();

            //delete entries from related table
            var sessionID = expiredSessionsList.Select(o => o.ID);
            context.SoftProofingSessionErrors.RemoveRange(context.SoftProofingSessionErrors.Where(t => sessionID.Contains(t.SoftProofingSessionParams)));

            //Delete entities from database and S3
            foreach (var expiredSession in expiredSessionsList)
            {
                try
                {
                    context.SoftProofingSessionParams.Remove(expiredSession.softproofingparams);
                    var approvalFolder = Path.Combine(GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath, expiredSession.approvalGuid, expiredSession.softproofingparams.SessionGuid);
                    GMGColorIO.DeleteFolderIfExists(approvalFolder, expiredSession.Region);
                }
                catch (Exception ex)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotRemoveSoftProofingSession, ex);
                }
            }
            context.SaveChanges();
        }
        
        /// <summary>
        /// Add a new paper tint. 
        /// </summary>
        /// <param name="paperTint"></param>
        /// <param name="accountId">Account id for which the paper tint is added</param>
        /// <param name="context"></param>
        public static void AddNewPaperTint(PaperTintViewModel paperTint, int accountId, int creatorId, DbContextBL context)
        {
            try
            {
                // create new paper tint 
                PaperTint newPaperTint = new PaperTint()
                {
                    Name = paperTint.Name,
                    AccountId = accountId,
                    MediaCategory = paperTint.SelectedMediaCategory,
                    Creator = creatorId,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                };
                
                context.PaperTints.Add(newPaperTint);
                
                // create new paper tint measurements
                PaperTintMeasurement newPaperTintMeasurements = null;
                foreach (var condition in paperTint.MeasurementConditionValues)
                {
                    newPaperTintMeasurements = new PaperTintMeasurement()
                    {
                        MeasurementCondition = condition.MeasurementCondition,
                        L = condition.L,
                        a = condition.a,
                        b = condition.b
                    };
                    newPaperTintMeasurements.PaperTint1 = newPaperTint;
                    context.PaperTintMeasurements.Add(newPaperTintMeasurements);
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddNewPaperTint, ex);
            }
        }

        /// <summary>
        /// Update an existing paper tint
        /// </summary>
        /// <param name="model"></param>
        /// <param name="accountId">Account id for which the paper tint is updated</param>
        /// <param name="context"></param>
        public static void UpdatePaperTint(PaperTintViewModel model, int accountId, DbContextBL context)
        {
            try
            {
                // get the data from the database
                var pptToBeUpdated = (from ppt in context.PaperTints
                                      join a in context.Accounts on ppt.AccountId equals a.ID
                                      where ppt.ID == model.ID && ppt.AccountId == accountId
                                      select ppt).FirstOrDefault();

                var mcToBeUpdated = (from pptm in context.PaperTintMeasurements 
                                     join mc in context.MeasurementConditions on pptm.MeasurementCondition equals mc.ID
                                     where pptm.PaperTint == pptToBeUpdated.ID
                                     select pptm).ToList();

                // update paper tint values
                pptToBeUpdated.Name = model.Name;
                pptToBeUpdated.MediaCategory = model.SelectedMediaCategory;
                pptToBeUpdated.UpdatedDate = DateTime.UtcNow;
                
                //update paper tint measurement conditions
                foreach (var condition in model.MeasurementConditionValues)
                {
                    var existingDbPptm = mcToBeUpdated.FirstOrDefault(c => c.MeasurementCondition == condition.MeasurementCondition);
                    if (existingDbPptm == null)
                    {
                        existingDbPptm = new PaperTintMeasurement()
                        {
                            PaperTint = model.ID,
                            MeasurementCondition = condition.MeasurementCondition,
                            L = condition.L,
                            a = condition.a,
                            b = condition.b
                        };
                        context.PaperTintMeasurements.Add(existingDbPptm);
                    }
                    else
                    {
                        existingDbPptm.L = condition.L;
                        existingDbPptm.a = condition.a;
                        existingDbPptm.b = condition.b;
                    }
                }

                var deletedMeasurements = mcToBeUpdated.Where(c => !model.MeasurementConditionValues.Select(t => t.MeasurementCondition).Contains(c.MeasurementCondition));
                context.PaperTintMeasurements.RemoveRange(deletedMeasurements);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdatedPaperTint, ex);
            }
        }

        /// <summary>
        /// Get paper tint by id to be used for update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accountId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static void PopulatePaperTintModel(PaperTintViewModel model, DbContextBL context)
        {
            var modelInfo = (from ppt in context.PaperTints
                            let measurementConditions = (from mc in context.MeasurementConditions
                                                        join pptm in context.PaperTintMeasurements on mc.ID equals pptm.MeasurementCondition
                                                        where pptm.PaperTint == model.ID
                                                        select new MeasurementConditionValuesViewModel
                                                        {
                                                            L = pptm.L,
                                                            a = pptm.a,
                                                            b = pptm.b,
                                                            MeasurementCondition = mc.ID
                                                        }).ToList()
                            where ppt.ID == model.ID
                            select new
                            {
                                Name = ppt.Name,
                                SelectedMediaCategory = ppt.MediaCategory,
                                MeasurementConditions = measurementConditions
                            }).FirstOrDefault();

            model.Name = modelInfo.Name;
            model.MeasurementConditionValues = modelInfo.MeasurementConditions;
            model.SelectedMediaCategory = modelInfo.SelectedMediaCategory;
            if (model.MeasurementConditionValues.Any())
            {
                model.SelectedMeasurementCondition = model.MeasurementConditionValues.FirstOrDefault().MeasurementCondition;
            }
        }

        /// <summary>
        /// Delete paper tint selected from the webgrid
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accountId"></param>
        /// <param name="context"></param>
        public static void DeletePaperTint(int? id, int accountId, DbContextBL context)
        {
            try
            {
                var pptToBeDeleted = (from ppt in context.PaperTints
                                      join a in context.Accounts on ppt.AccountId equals a.ID
                                      where ppt.AccountId == accountId && ppt.ID == id
                                      select ppt).FirstOrDefault();
                var viewingConditionsToBeUpdated = (from vwc in context.ViewingConditions
                                                  join ppt in context.PaperTints on vwc.PaperTint equals ppt.ID
                                                  where ppt.ID == id
                                                  select vwc).ToList();
                if (viewingConditionsToBeUpdated != null)
                {
                    foreach (var viewingCondition in viewingConditionsToBeUpdated)
                    {
                        viewingCondition.PaperTint = null;
                    }
                }
                context.PaperTintMeasurements.RemoveRange(pptToBeDeleted.PaperTintMeasurements);
                context.PaperTints.Remove(pptToBeDeleted);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeletePaperTint, ex);
            }
        }

        /// <summary>
        /// Delete paper tint selected from the webgrid
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accountId"></param>
        /// <param name="context"></param>
        public static bool DeletePrintSubstrate(int? id, DbContextBL context)
        {
            try
            {
                if (context.SimulationProfiles.Any(t => t.MediaCategory == id) ||
                    context.PaperTints.Any(t => t.MediaCategory == id) ||
                    context.AccountCustomICCProfiles.Any(t => t.PrintSubstrate == id))
                {
                    return false;
                }

                context.MediaCategories.Remove(context.MediaCategories.FirstOrDefault(t => t.ID == id));
                context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("Could not delete print substrate", ex);
            }
        }

        /// <summary>
        /// Add New Print SUbstrates or updates existing one
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="context"></param>
        public static void SavePrintSubstrateChanges(MediaCategoryViewModel model, DbContextBL context)
        {
            MediaCategory modelBO;
            if (model.ID > 0)
            {
                modelBO = context.MediaCategories.FirstOrDefault(t => t.ID == model.ID);
            }
            else
            {
                modelBO = new MediaCategory();
                context.MediaCategories.Add(modelBO);
            }

            modelBO.Name = model.MediaCategoryName;

            context.SaveChanges();
        }

        /// <summary>
        /// Extract White Point from Profile and saves it in DB
        /// </summary>
        /// <param name="profileId"></param>
        public static void SetProfileWhitePointLab(int profileId)
        {
            using (var context = new DbContextBL())
            {
                var profileData = (from smp in context.SimulationProfiles
                                   let accountInfo = (from a in context.Accounts 
                                                    where a.ID == smp.AccountId
                                                    select new
                                                    {
                                                        a.Guid,
                                                        a.Region
                                                    }).FirstOrDefault()
                                   where smp.ID == profileId
                                   select new
                                   {
                                       accountInfo,
                                       Profile = smp   
                                   }).FirstOrDefault();

                if (profileData != null)
                {
                    string profilePath;
                    string region = profileData.accountInfo != null
                        ? profileData.accountInfo.Region
                        : GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                            ? GMGColorConfiguration.AppConfiguration.AWSRegion
                            : String.Empty;

                    if (profileData.accountInfo == null)
                    {
                        profilePath = GMGColorConfiguration.AppConfiguration.SoftProofingProfilesRepo + "/" + profileData.Profile.FileName;
                    }
                    else
                    {
                        profilePath = GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath + "/" +
                                      profileData.accountInfo.Guid + "/" +
                                      GMGColorConfiguration.AppConfiguration.SimulationProfilesRelativePath + "/" +
                                      profileData.Profile.Guid + "/" +
                                      profileData.Profile.FileName;
                    }
                    if (GMGColorIO.FileExists(profilePath, region))
                    {
                        var profileBytes = GMGColorIO.GetByteArrayOfAFile(profilePath, region);

                        var profileManager = new ICCProfileManager(profileBytes);

                        if (profileManager.IsValid() && profileManager.IsValidAsSimulationProfile())
                        {
                            profileData.Profile.IsValid = true;
                            LabValue whiteLab = profileManager.GetWhitePointLab();
                            profileData.Profile.L = whiteLab.L;
                            profileData.Profile.a = whiteLab.a;
                            profileData.Profile.b = whiteLab.b;
                        }
                        else
                        {
                            profileData.Profile.IsValid = false;
                        }

                        context.SaveChanges();
                        profileManager.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Extract White Point from Profile and saves it in DB
        /// </summary>
        /// <param name="profileId"></param>
        public static void ValidateCustomICCProfile(int profileId)
        {
            using (var context = new DbContextBL())
            {
                var profileData = (from smp in context.AccountCustomICCProfiles
                                   join a in context.Accounts on smp.Account equals a.ID
                                   where smp.ID == profileId
                                   select new
                                   {
                                       AccountGuid = a.Guid,
                                       a.Region,
                                       Profile = smp
                                   }).FirstOrDefault();

                if (profileData != null)
                {
                    var profilePath = SettingsBL.GetCustomProfileRelativeVirtualPath(profileData.AccountGuid, profileData.Profile.Guid) +
                                      profileData.Profile.Name;

                    if (GMGColorIO.FileExists(profilePath, profileData.Region))
                    {
                        var profileBytes = GMGColorIO.GetByteArrayOfAFile(profilePath, profileData.Region);

                        var profileManager = new ICCProfileManager(profileBytes);

                        bool isRGB = false;
                        var whiteLab = new LabValue();

                        if (profileManager.IsValid() && profileManager.IsCMYKorRGB(ref isRGB, ref whiteLab))
                        {
                            profileData.Profile.IsValid = true;
                            profileData.Profile.IsRGB = isRGB;
                            if (!isRGB)
                            {
                                profileData.Profile.L = whiteLab.L;
                                profileData.Profile.a = whiteLab.a;
                                profileData.Profile.b = whiteLab.b;
                            }
                        }
                        else
                        {
                            profileData.Profile.IsValid = false;
                        }

                        context.SaveChanges();
                        profileManager.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Add a new simulation profile and call the upload and create message queue methods
        /// </summary>
        /// <param name="model"></param>
        /// <param name="accountId"></param>
        /// <param name="accountGuid"></param>
        /// <param name="loggedUserId"></param>
        /// <param name="accountRegion"></param>
        /// <param name="context"></param>
        public static void AddNewSimulationProfile(SimulationProfileViewModel model, bool isSysAdmin, int accountId, string accountGuid, int loggedUserId, string accountRegion, DbContextBL context)
        {
            try
            {
                // generate profile GUID
                Guid simulationProfileGuid = Guid.NewGuid();

                // populate db object
                SimulationProfile simulationProfileData = new SimulationProfile()
                {
                    AccountId = isSysAdmin ? null : (int?)accountId,
                    Creator = isSysAdmin ? null : (int?)loggedUserId,
                    ProfileName = model.ProfileName,
                    FileName = model.FileName,
                    Guid = simulationProfileGuid.ToString(),
                    MediaCategory = model.SelectedMediaCategory,
                    MeasurementCondition = model.SelectedMeasurementCondition,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow
                };

                // add and save to the db
                context.SimulationProfiles.Add(simulationProfileData);
                context.SaveChanges();

                // upload profile
                UploadSimulationProfile(isSysAdmin,accountGuid, accountRegion, simulationProfileGuid.ToString(), loggedUserId, accountId, simulationProfileData.FileName);

                // create message queue for setting profile white point Lab
                CreateQueueMessageAfterSimulationProfileUpload(simulationProfileData.ID, GMG.CoZone.Common.SoftProofingAction.ReadProfileWhitePoint);

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateSimulationProflie, ex);
            }
        }

        /// <summary>
        /// Populate model info for editing Simulation Profile
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        public static void PopulateSimulationProfileModel(bool isAdmin, SimulationProfileViewModel model, DbContextBL context)
        {
            SimulationProfileViewModel modelInfo;
            if (isAdmin)
            {
                modelInfo = (from sp in context.SimulationProfiles
                             where sp.ID == model.ID
                             select new SimulationProfileViewModel
                             {
                                 FileName = sp.FileName,
                                 ProfileName = sp.ProfileName,
                                 SelectedMeasurementCondition = sp.MeasurementCondition,
                                 SelectedMediaCategory = sp.MediaCategory
                             }).FirstOrDefault();
            }
            else
            {
                modelInfo = (from sp in context.SimulationProfiles
                             join a in context.Accounts on sp.AccountId equals a.ID
                             where sp.ID == model.ID
                             select new SimulationProfileViewModel
                             {
                                 FileName = sp.FileName,
                                 ProfileName = sp.ProfileName,
                                 SelectedMeasurementCondition = sp.MeasurementCondition,
                                 SelectedMediaCategory = sp.MediaCategory
                             }).FirstOrDefault();
            }
            model.FileName = modelInfo.FileName;
            model.ProfileName = modelInfo.ProfileName;
            model.SelectedMeasurementCondition = modelInfo.SelectedMeasurementCondition;
            model.SelectedMediaCategory = modelInfo.SelectedMediaCategory;
        }

        /// <summary>
        /// Update Simulation Profile
        /// </summary>
        /// <param name="model"></param>
        /// <param name="accoundId"></param>
        /// <param name="context"></param>
        public static void UpdateSimulationProfile(SimulationProfileViewModel model, bool isSysAdmin, Account loggedAccount, DbContextBL context)
        {
            try
            {
                // get object to be updated from database
                var smpToBeUpdated = (from smp in context.SimulationProfiles
                                      where smp.ID == model.ID
                                      select smp).FirstOrDefault();

                var dbProfile = smpToBeUpdated.FileName;
                
                // find old uploaded icc profile and delete it
                if (model.FileName != dbProfile)
                {
                    // delete existing simulation profile
                    DeleteSimulationProfileUploadedFile(isSysAdmin, loggedAccount.Region, loggedAccount.Guid, smpToBeUpdated.Guid, smpToBeUpdated.FileName); // Update
                    smpToBeUpdated.IsValid = null;
                    smpToBeUpdated.L = null;
                    smpToBeUpdated.a = null;
                    smpToBeUpdated.b = null;
                }

                // update object with model data
                smpToBeUpdated.ProfileName = model.ProfileName;
                smpToBeUpdated.FileName = model.FileName;
                smpToBeUpdated.MediaCategory = model.SelectedMediaCategory;
                smpToBeUpdated.MeasurementCondition = model.SelectedMeasurementCondition;
                smpToBeUpdated.UpdatedDate = DateTime.UtcNow;

                // save changes to database
                context.SaveChanges();

                // upload profile only if a new profile has been selected
                if (model.FileName != dbProfile)
                {
                    var creatorID = int.MinValue;
                    var accoundID = int.MinValue;
                    if (isSysAdmin)
                    {
                        creatorID = loggedAccount.ID;
                        accoundID = loggedAccount.ID;
                    }
                    else
                    {
                        creatorID = smpToBeUpdated.Creator.Value;
                        accoundID = smpToBeUpdated.AccountId.Value;
                    }
                    UploadSimulationProfile(isSysAdmin, loggedAccount.Guid, loggedAccount.Region, smpToBeUpdated.Guid, creatorID, accoundID, smpToBeUpdated.FileName);
                    
                    // create message queue for setting profile white point Lab
                    CreateQueueMessageAfterSimulationProfileUpload(smpToBeUpdated.ID, GMG.CoZone.Common.SoftProofingAction.ReadProfileWhitePoint);
                }

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateSimulationProfile, ex);
            }
        }

        /// <summary>
        /// Delete simulation profile
        /// </summary>
        /// <param name="profileId">The id of the selected profile</param>
        /// <param name="accountId">The id of the account that owns the profile</param>
        /// <param name="Context">The database context</param>
        public static void DeleteSimulationProfile(AccountType.Type loggedAccountType,bool isSysAdmin, int? profileId, string loggedAccountRegion, string loggedAccountGuid, int accId, DbContextBL context)
        {
            try
            {
                // get the object from database to be deleted
                var profileInfo = (from smp in context.SimulationProfiles
                                   where smp.ID == profileId
                                   select smp).FirstOrDefault();

                if (profileInfo.Account != null)
                {
                    CheckSimulationInUseStatusAndRemove(isSysAdmin, context, profileId, profileInfo, loggedAccountRegion, loggedAccountGuid);
                }
                else
                {
                    if (isSysAdmin && loggedAccountType == AccountType.Type.GMGColor)
                    {
                        CheckSimulationInUseStatusAndRemove(isSysAdmin, context, profileId, profileInfo, loggedAccountRegion, loggedAccountRegion);
                    }
                    else
                    {
                        //add to exclude table
                        var newExcludedProfile = new ExcludedAccountDefaultProfile
                        {
                            Account = accId,
                            SimulationProfile = profileInfo.ID
                        };

                        context.ExcludedAccountDefaultProfiles.Add(newExcludedProfile);
                    }
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteSimulationProfile, ex);
            }
        }
        
        /// <summary>
        /// Saves the default viewing conditions for the specified job
        /// </summary>
        /// <param name="job"></param>
        /// <param name="profileId"></param>
        /// <param name="paperTint"></param>
        /// <param name="context"></param>
        /// <returns>Wheter viewing conditions changed or not</returns>
        public static bool SaveViewingConditions(int job, int? profileId, int? paperTint, DbContextBL context)
        {
            bool conditionsChanged = false;
            var jobViewCond = (from apvwcnd in context.ApprovalJobViewingConditions
                                        where apvwcnd.Job == job
                                        select apvwcnd).FirstOrDefault();
            if (profileId.HasValue || paperTint.HasValue)
            {
                var viewingCondition = (from vwcnd in context.ViewingConditions
                                        where vwcnd.SimulationProfile == profileId && vwcnd.PaperTint == paperTint
                                        select vwcnd).FirstOrDefault();

                //add
                if (viewingCondition == null)
                {
                    viewingCondition = new ViewingCondition
                                                {
                                                    SimulationProfile = profileId,
                                                    PaperTint = paperTint
                                                };

                    context.ViewingConditions.Add(viewingCondition);
                }

                if (jobViewCond == null)
                {
                    jobViewCond = new ApprovalJobViewingCondition
                    {
                        Job = job
                    };

                    context.ApprovalJobViewingConditions.Add(jobViewCond);

                    conditionsChanged = true;
                }
                else if (jobViewCond.ViewingCondition1.SimulationProfile != profileId ||
                         jobViewCond.ViewingCondition1.PaperTint != paperTint)
                {
                    conditionsChanged = true;
                }


                jobViewCond.ViewingCondition1 = viewingCondition;
            }
            else
            {
                //possible remove
                if (jobViewCond != null)
                {
                    context.ApprovalJobViewingConditions.Remove(jobViewCond);
                    conditionsChanged = true;
                }
            }

            context.SaveChanges();

            return conditionsChanged;
        }

        
        /// <summary>
        /// Get the media category list from the static Media Category table
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<MediaCategoryViewModel> GetMediaCategoryList(DbContextBL context)
        {
            return (from m in context.MediaCategories
                    select new MediaCategoryViewModel
                    {
                        ID = m.ID,
                        MediaCategoryName = m.Name
                    }).ToList();
        }

        /// <summary>
        /// Get Model for View Print Substrate Page
        /// </summary>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static MediaCategoryViewModel GetMediaCategoryViewModel(int id, DbContextBL context)
        {
            return (from m in context.MediaCategories
                    where m.ID == id
                    select new MediaCategoryViewModel
                    {
                        ID = m.ID,
                        MediaCategoryName = m.Name
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Determines if the currently Print Substrate Name is already being used
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsPrintSubstrateNameInUse(MediaCategoryViewModel model, DbContextBL context)
        {
            return (from mc in context.MediaCategories
                where mc.ID != model.ID && mc.Name.ToLower() == model.MediaCategoryName.ToLower().Trim()
                select mc.ID).Any();
        }

        /// <summary>
        /// Get the media category list from the static Measurement Conditions table 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<MeasurementConditionViewModel> GetMeasurementConditions(DbContextBL context)
        {
            return (from mc in context.MeasurementConditions
                    select new MeasurementConditionViewModel
                    {
                        ID = mc.ID,
                        Name = mc.Name
                    }).ToList();
        }

        /// <summary>
        /// Check if entity name is unique in the SoftProofing settings
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <param name="type">The type of the entity Simulation Profile\ Paper Tints</param>
        /// <param name="entitName">The entity name </param>
        /// <param name="accountId">The account id that owns the entity</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsNameUnique(int? id, SoftProofingSwitchEnum type, string entitName, int accountId, DbContextBL context)
        {
            if (type == SoftProofingSwitchEnum.PaperTint)
            {
                if (id != null)
                {
                    return !(from ppt in context.PaperTints
                             where ppt.ID != id && ppt.AccountId == accountId && ppt.Name.ToLower() == entitName.ToLower()
                             select ppt.ID).Any();
                }
                return !(from ppt in context.PaperTints
                         where ppt.AccountId == accountId && ppt.Name.ToLower() == entitName.ToLower()
                         select ppt.ID).Any();
            }
            if (id != null)
            {
                return !(from smp in context.SimulationProfiles
                         where smp.ID != id && smp.AccountId == accountId && smp.ProfileName.ToLower() == entitName.ToLower() && smp.IsDeleted != true
                         select smp.ID).Any();
            }
            return !(from smp in context.SimulationProfiles
                     where smp.AccountId == accountId && smp.FileName.ToLower() == entitName.ToLower() && smp.IsDeleted != true
                     select smp.ID).Any();
        }

        /// <summary>
        /// Get a list of simulation profile ids for which the Lab values have not been calculated
        /// </summary>
        /// <param name="accounId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetSimulationProfileIdList(List<SimulationProfileDashBoardViewModel> profiles)
        {
            var inProgressIds = profiles.Where(t => t.IsValid == null).Select(t => t.ID).ToList();
            var result = string.Join(",", inProgressIds.ToArray());

            return result;
        }

        public static List<SimulationProfileLabCalculations> GetSimulationProfileCalculatedLabValues(int[] simProfileIdsList, DbContextBL context)
        {
            return (from smp in context.SimulationProfiles
                    where simProfileIdsList.Contains(smp.ID)
                    select new SimulationProfileLabCalculations()
                    {
                        ID = smp.ID,
                        L = smp.L,
                        a = smp.a,
                        b = smp.b,
                        IsValid = smp.IsValid
                    }).ToList();
        }

        public static GMGColorDAL.ViewingCondition GetJobViewingCondition(int jobId, DbContextBL context)
        {
            return (from jvwcnd in context.ApprovalJobViewingConditions
                    join vw in context.ViewingConditions on jvwcnd.ViewingCondition equals vw.ID
                    where jvwcnd.Job == jobId
                    select vw).FirstOrDefault();
        }

        public static GMGColorDAL.ViewingCondition GetJobViewingConditionBySimProfile(int simId, DbContextBL context)
        {
            return (from vw in context.ViewingConditions
                    where vw.SimulationProfile == simId && vw.CustomProfile == null && vw.EmbeddedProfile == null & vw.PaperTint == null
                    select vw).FirstOrDefault();
        }

        public static string GetSimulationProfileRelativeVirtualPath(string accGuid, string profileGuid)
        {
            return
                    GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath + "/" + accGuid +
                    "/" + GMGColorConfiguration.AppConfiguration.SimulationProfilesRelativePath + "/" +
                    profileGuid + "/";
        }

        /// <summary>
        /// Search Simulation Profile list by profileName, uploadedFileName or mediaCategory
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static List<SimulationProfileDashBoardViewModel> SearchSimulationProfiles(SettingsParam param, AccountType.Type loggedAccountType)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    return (from smp in context.SimulationProfiles
                            let profileMeasurementCondition = (from mc in context.MeasurementConditions
                                                               where smp.MeasurementCondition == mc.ID
                                                               select new DashboardMeasurementConditions()
                                                               {
                                                                   ID = mc.ID,
                                                                   Name = mc.Name,
                                                                   L = smp.L.Value,
                                                                   a = smp.a.Value,
                                                                   b = smp.b.Value
                                                               }).ToList()
                            where (smp.AccountId == param.LoggedAccountId || smp.Account == null && !context.ExcludedAccountDefaultProfiles.Any(t => t.Account == param.LoggedAccountId && t.SimulationProfile == smp.ID) && ( loggedAccountType == AccountType.Type.GMGColor || smp.IsValid.HasValue && smp.IsValid.Value))
                                   && !smp.IsDeleted 
                                   && (smp.FileName.Contains(param.SearchText) || smp.ProfileName.Contains(param.SearchText)
                                   )
                            select new SimulationProfileDashBoardViewModel
                            {
                                ID = smp.ID,
                                IsDefaultProfile = smp.Account == null,
                                ProfileName = smp.ProfileName,
                                FileName = smp.FileName,
                                MediaCategory = smp.MediaCategory.HasValue ? (from mct in context.MediaCategories
                                                                              where smp.MediaCategory == mct.ID
                                                                              select mct.Name).FirstOrDefault()
                                                                             : Resources.lblUndefined,
                                IsValid = smp.IsValid,
                                MeasurementConditions = profileMeasurementCondition
                            }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveSimulationProflie, ex);
            }
        }
        #endregion
    }
}
