﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GL = GMGColorDAL.GMGColorLogging;

namespace GMGColorBusinessLogic
{
    public class TrialAccountBL: BaseBL
    {
        private const int DaysForTrial = 15;

        public static void SuspendTrialAccounts()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    DateTime dateNow = DateTime.UtcNow;
                    IEnumerable<Account> list =
                        (from a in context.Accounts
                         join accs in context.AccountStatus on a.Status equals accs.ID
                         where
                            a.IsTrial == true &&
                            DbFunctions.DiffDays(a.CreatedDate, dateNow) >= DaysForTrial &&
                            accs.Key == TrialAccountMessages.DefaultAccountStatusKey
                         select a);

                    foreach (Account account in list)
                    {
                        account.IsSuspendedOrDeletedByParent = true;
                        account.SuspendReason = TrialAccountMessages.AccountSuspendedReason;
                        account.Status = (from accs in context.AccountStatus
                                          where accs.Key == TrialAccountMessages.DefaultAccountSuspendedStatusKey
                                          select accs.ID).FirstOrDefault();
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("TrialAccounts.SuspendTrialAccounts() - Error occurred, Message: {0}", ex);
            }
        }
    }
}
