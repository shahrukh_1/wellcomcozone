﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using GMGColorNotificationService;
using GL = GMGColorDAL.GMGColorLogging;
using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using User = GMGColorDAL.User;

namespace GMGColorBusinessLogic
{
    public class DeliverBL: BaseBL
    {
        /// <summary>
        /// Send timeout notifications for jobs which timed out
        /// </summary>
        public static void CheckDeliverJobs()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    List<DeliverJob> jobList = GetDeliverJobsForNotification(context);

                    if (jobList.Count > 0)
                    {
                        DeliverJobStatu deliverJobStatu = DeliverJobStatu.GetObjectsByKey(DeliverJobStatus.DownloadToCPFailed, context);
                        foreach (var job in jobList)
                        {
                            NotificationServiceBL.CreateNotification(new DeliverJobTimeout
                            {
                                CreatedDate = DateTime.UtcNow,
                                EventCreator = job.Owner,
                                InternalRecipient = job.Owner,
                                JobsGuids = new []{ job.Guid}
                            },
                            job.User,
                            context);

                            var deliverExternalUsers = DeliverExternalCollaborator.GetDeliverCollaboratorsByJobGuids(new[] { job.Guid }, EventType.GetDeliverEventTypeKey(EventType.TypeEnum.DeliverJob_Timeout), context);

                            //send deliver job timeout emails for deliver external users
                            foreach (var user in deliverExternalUsers)
                            {
                                NotificationServiceBL.CreateNotification(new DeliverJobTimeout
                                                                        {
                                                                            EventCreator = job.Owner,
                                                                            ExternalRecipient = user,
                                                                            CreatedDate = DateTime.UtcNow,
                                                                            JobsGuids = new[] { job.Guid }
                                                                        },
                                                                       job.User,
                                                                       context  
                                                                       );
                            }

                            job.IsAlertSent = true;

                            if (job.DeliverJobStatu.Key == (int)DeliverJobStatus.Submitting)
                            {
                                if (deliverJobStatu != null)
                                    job.Status = deliverJobStatu.ID;
                            }
                        }
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("NotificationDeliverJobsTimeout.CheckDeliverJobsTimeout() - Error occurred, Message: {0}", ex);
            }
        }

        /// <summary>
        /// Gets list of jobs from database which timed out
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<DeliverJob> GetDeliverJobsForNotification(GMGColorContext context)
        {
            return (from deliverjob in context.DeliverJobs
                    join djs in context.DeliverJobStatus on deliverjob.DeliverJobStatu.ID equals djs.ID
                    join cpcz in context.ColorProofCoZoneWorkflows on deliverjob.CPWorkflow equals cpcz.ID
                    where
                        djs.Key != (int)DeliverJobStatus.Completed &&
                        djs.Key != (int)DeliverJobStatus.Cancelled &&
                        djs.Key != (int)DeliverJobStatus.ProcessingError &&
                        djs.Key != (int)DeliverJobStatus.CompletedDeleted &&
                        !deliverjob.IsAlertSent &&
                        DbFunctions.DiffSeconds(deliverjob.CreatedDate, DateTime.UtcNow) / 3600 >= deliverjob.ColorProofCoZoneWorkflow.TransmissionTimeout.Value
                    select deliverjob).ToList();
        }

        /// <summary>
        /// Determines if a colorproof cozone workflow with the specified name exists for the specified color proof instance
        /// </summary>
        /// <param name="workflowName"></param>
        /// <param name="instanceName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool CoZoneColorProofWorkflowExists(string workflowName, string instanceName, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofCoZoneWorkflow.CoZoneColorProofWorkflowExists(workflowName, instanceName, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotVerifyIfCoZoneCPWorkflowExists, ex);
            }
            
        }

        public static bool IsCoZoneWorkFlowInSecurityGroup(string workflowName, string instanceName, int uploaderUserId, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofCoZoneWorkflow.IsCoZoneWorkFlowInSecurityGroup(workflowName, instanceName, uploaderUserId, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotVerifyIfColorProofCZWorkflowIsInSecurityGroup, ex);
            }
        }

        /// <summary>
        /// Determines if a ColorProofInstance with the specified name exists
        /// </summary>
        /// <param name="instanceName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool RunningColorProofInstanceExists(string instanceName, DbContextBL context)
        {
            try
            {
                return ColorProofInstance.ColorProofInstanceExists(instanceName, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotVerifyIfColorProofInstanceExists, ex);
            }
           
        }

        public static bool IsColorProofInstanceRunning(string instanceName, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofInstance.IsColorProofInstanceRunning(instanceName, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotVerifyIfColorProofInstanceIsRunning, ex);
            }
        }

        /// <summary>
        /// Gets deliver job status ID
        /// </summary>
        /// <param name="status"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetDeliverJobStatusID(GMG.CoZone.Common.DeliverJobStatus status, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.DeliverJobStatu.GetDeliverJobStatusID(status, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetDeliverJobStatusID, ex);
            }
        }

        /// <summary>
        /// Gets Deliver Job Status In colorproof object ID
        /// </summary>
        /// <param name="status"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetDeliverJobStatusInColorProofID(GMG.CoZone.Common.ColorProofJobStatus status, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofJobStatu.GetDeliverJobStatusInColorProofID(status, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetDeliverStatusInColorProofID, ex);
            }
        }

        /// <summary>
        /// gets id of colorproof cozone workflow with the specified name exists for the specified color proof instance
        /// </summary>
        /// <param name="workflowName"></param>
        /// <param name="instanceName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetCoZoneColorProofWorkflowByNameAndInstanceName(string workflowName, string instanceName, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofCoZoneWorkflow.GetCoZoneColorProofWorkflowByNameAndInstanceName(workflowName, instanceName, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetCoZoneWorkflowIDByNameAndCPInstanceName, ex);
            }
        }

        /// <summary>
        /// Get ColorProofInstace Guid by colorproof cozone workflow ID
        /// </summary>
        /// <param name="coZoneWorkflowID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetColorProofInstanceGuidByWorkflow(int coZoneWorkflowID, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofInstance.GetColorProofInstanceGuidByWorkflow(coZoneWorkflowID, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetColorProofInstanceGuidByWorkflowID, ex);
            }
            
        }

        /// <summary>
        /// Get color proof workflow name by colorproof cozone workflow ID
        /// </summary>
        /// <param name="czwfID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetColorProofWorkflowName(int czwfID, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofWorkflow.GetColorProofWorkflowName(czwfID, context);
            }
            catch (System.Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetColorProofWorkflowNameByCZWorkflowID, ex);
            }
            
        }

        /// <summary>
        /// Get color proof workflow name by colorproof cozone workflow ID
        /// </summary>
        /// <param name="czwfID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetColorProofCoZoneWorkflowName(int czwfID, DbContextBL context)
        {
            try
            {
                return GMGColorDAL.ColorProofWorkflow.GetColorProofCoZoneWorkflowName(czwfID, context);
            }
            catch (System.Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetColorProofWorkflowNameByCZWorkflowID, ex);
            }

        }

        /// <summary>
        /// Gets queue message for specified deliver job
        /// </summary>
        /// <param name="item"></param>
        /// <param name="backLinkUrl"></param>
        /// <param name="loggedAccountName"></param>
        /// <param name="instanceName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static CustomDictionary<string, string> GetDictionaryForDeliverQueue(GMGColorDAL.DeliverJob item, string backLinkUrl, string loggedAccountName, string instanceName = null, DbContextBL context = null)
        {
            CustomDictionary<string, string> dict = new CustomDictionary<string, string>();

            dict.Add(Constants.DeliverQueueActionType, ActionType.AddNewJob.ToString());
            dict.Add(Constants.DeliverQueueJobTitle, item.Job1.Title);

            if (context == null)
            {
                dict.Add(Constants.DeliverQueueCPServerName, item.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.SystemName);

                dict.Add(Constants.DeliverQueueCPServerGuid, item.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.Guid);

                dict.Add(Constants.DeliverQueueCPWorkflowName, item.ColorProofCoZoneWorkflow.ColorProofWorkflow1.Name);
            }
            else
            {
                dict.Add(Constants.DeliverQueueCPServerName, instanceName);

                dict.Add(Constants.DeliverQueueCPServerGuid, GetColorProofInstanceGuidByWorkflow(item.CPWorkflow, context));

                dict.Add(Constants.DeliverQueueCPWorkflowName, GetColorProofWorkflowName(item.CPWorkflow, context));
            }
            
            dict.Add(Constants.DeliverQueueCoZoneUsername, loggedAccountName);

            dict.Add(Constants.DeliverQueueJobBackLinkURL, backLinkUrl);

            item.BackLinkUrl = backLinkUrl;

            dict.Add(Constants.DeliverQueueJobGuid, item.Guid);
            dict.Add(Constants.DeliverQueueJobFilename, item.FileName);
            dict.Add(Constants.DeliverQueueJobFileSize, item.Size.ToString());
            dict.Add(Constants.DeliverQueueJobPages, item.Pages);
            dict.Add(Constants.DeliverQueueJobIncludeMetaInfo, item.IncludeProofMetaInformationOnLabel.ToString().ToLower());
            dict.Add(Constants.DeliverQueueJobLogProofControlResults, item.LogProofControlResults.ToString().ToLower());

            return dict;
        }

        /// <summary>
        /// Gets queue message for deliver job preview
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static CustomDictionary<string, string> GetDictionaryForDefineQueue(GMGColorDAL.DeliverJob item)
        {
            CustomDictionary<string, string> dict = new CustomDictionary<string, string>();

            dict.Add(Constants.DeliverQueueJobId, item.ID.ToString());
            dict.Add(Constants.JobMessageType, GMGColorCommon.JobType.DeliverJob.ToString());
            dict.Add(Constants.ApprovalMsgApprovalType, GMGColorDAL.Approval.ApprovalTypeEnum.Image.ToString());

            return dict;
        }

        /// <summary>
        /// Validates deliver file type
        /// </summary>
        /// <param name="type"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsValidDeliverType(GMGColorDAL.FileType.FileExtention type, DbContextBL context)
        {

            if(type == GMGColorDAL.FileType.FileExtention.JPEG || type == GMGColorDAL.FileType.FileExtention.JPG || type == GMGColorDAL.FileType.FileExtention.EPS || type == GMGColorDAL.FileType.FileExtention.PDF || type == GMGColorDAL.FileType.FileExtention.TIFF || type == GMGColorDAL.FileType.FileExtention.TIF)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Try to Upload Deliver job to profile output preset if needed
        /// </summary>
        /// <param name="objDeliver"></param>
        /// <param name="context"></param>
        public static void UploadDeliverJobToFTPIfNeeded(GMGColorDAL.DeliverJob objDeliver, DbContextBL context)
        {
            try
            {
                //Get Job Upload Engine Profile Folder 
                string profileFolder = null;
                GMG.CoZone.Common.Utils.TryGetProfileFolder(objDeliver.FTPSourceFolder, out profileFolder);

                if (profileFolder != null)
                {
                    //check if the upload engine profile has an output preset set
                    int? outputProfilePreset = XMLEngineBL.GetProfileOutputPresetIfSet(profileFolder, objDeliver.Owner, context);

                    if (outputProfilePreset.GetValueOrDefault() == 0)
                    {
                        //No output preset set for this Profile
                        return;
                    }

                    GMGColorDAL.FTPPresetJobDownload conversionOutputJob = new GMGColorDAL.FTPPresetJobDownload();
                    conversionOutputJob.DeliverJob = objDeliver.ID;
                    conversionOutputJob.Preset = outputProfilePreset.Value;

                    context.FTPPresetJobDownloads.Add(conversionOutputJob);

                    context.SaveChanges();

                    //Create queue message to be sent to FTP Service
                    Dictionary<string, string> dict = Utils.GetDictionaryForFTPQueue(conversionOutputJob.ID, GMG.CoZone.Common.AppModule.Deliver, FTPAction.UploadAction);

                    GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.FTPJob);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUploadDeliverJobToFTP, ex);
            }
        }

        /// <summary>
        /// Gets the number of remaining color proof connections not used for the current deliver billing plan
        /// </summary>
        public static int GetAvailableColorProofConnnection(Account account, out int totalConnectionsAvailable, DbContextBL context)
        {
            try
            {
                BillingPlan deliverPlan = account.DeliverBillingPlan(context);

                int usedColorProofConnections = ColorProofInstance.GetNumberOfCPInstances(account.ID, context);

                if (deliverPlan != null)
                {
                    totalConnectionsAvailable = deliverPlan.ColorProofConnections.GetValueOrDefault();
                    int remainingConnections = totalConnectionsAvailable - usedColorProofConnections;
                    return remainingConnections >= 0 ? remainingConnections : 0;
                }
                else
                {
                    totalConnectionsAvailable = 0;
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAvailableColorProofConnnection, ex);
            }
        }

        /// <summary>
        /// Returns the CozoneColorProofWorkflows that are contained in a security group in which the loggedUser is part of
        /// </summary>
        /// <param name="cpInstanceID">Selected ColorProofInstance ID</param>
        /// <param name="loggedUserID">Current Logged User ID</param>
        /// <param name="context">Database Context</param>
        /// <returns>List of secured CoZone Workflows</returns>
        public static List<ColorProofCoZoneWorkflow> GetWfSecuredList(int cpInstanceID, int loggedUserID, DbContextBL context)
        {
            try
            {
                return (from cpwf in context.ColorProofWorkflows
                        join czwf in context.ColorProofCoZoneWorkflows on cpwf.ID equals czwf.ColorProofWorkflow
                        join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                        let isInSecurityGroup = (from cpwfug in context.ColorProofCoZoneWorkflowUserGroups 
                                                join ug in context.UserGroups on cpwfug.UserGroup equals ug.ID
                                                join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                                where cpwfug.ColorProofCoZoneWorkflow == czwf.ID && ugu.User == loggedUserID
                                                select cpwfug.ID).Any()
                        where cpi.ID == cpInstanceID && czwf.IsDeleted == false && czwf.IsAvailable == true
                                && isInSecurityGroup
                        select czwf).ToList();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetWorkflowSecuredList, ex);
            }
        }

        /// <summary>
        /// Gets ColorProof Instance State ID for the specified state
        /// </summary>
        /// <param name="state"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetColorProofInstanceStateID(GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum state,
            DbContextBL context)
        {
            return (from cpinst in context.ColorProofInstanceStates
                where cpinst.Key == (int) state
                select cpinst.ID).FirstOrDefault();

        }

        /// <summary>
        /// Get ColorProofInstace by instance ID
        /// </summary>
        /// <param name="instanceID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static ColorProofInstance GetColorProofInstanceByID(int instanceID, DbContextBL context)
        {
            try
            {
                return (from cpi in context.ColorProofInstances 
                        where cpi.ID == instanceID
                        select cpi).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetColorProofInstanceGuidByWorkflowID, ex);
            }

        }

        public static void ProcessSharedWorkflow(int workFlowID, User loggedUser, int userId, string workflowName, string companyName, string mailTitle, DbContextBL context)
        {
            try
            {
                if (
                    !context.SharedColorProofWorkflows.Any(
                        t => t.ColorProofCoZoneWorkflow == workFlowID && t.User == userId))
                {
                    SharedColorProofWorkflow sharedWorkflow = new SharedColorProofWorkflow();
                    sharedWorkflow.ColorProofCoZoneWorkflow = workFlowID;
                    sharedWorkflow.User = userId;
                    sharedWorkflow.IsEnabled = true;
                    sharedWorkflow.Token = Guid.NewGuid().ToString();

                    var userInfo = (from u in context.Users
                        join ac in context.Accounts on u.Account equals ac.ID
                        join com in context.Companies on ac.ID equals com.Account
                        where u.ID == userId
                        select new
                                {
                                    User = u,
                                    Domain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain
                                }).FirstOrDefault();

                    NotificationServiceBL.CreateNotification(new GMGColorNotificationService.ShareWorkflowInvitation
                                                                {
                                                                    EventCreator = loggedUser.ID,
                                                                    InternalRecipient = userInfo.User.ID,
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    WorkflowName = workflowName,
                                                                    CompanyName = companyName,
                                                                    LoggedUserName = loggedUser.GivenName + " " + loggedUser.FamilyName,
                                                                    AcceptUrl = GetShareWorkflowDecisionUrl(sharedWorkflow.Token, userInfo.Domain, true),
                                                                    RejectUrl = GetShareWorkflowDecisionUrl(sharedWorkflow.Token, userInfo.Domain, false)
                                                                },
                                                                null,
                                                                context
                                                                );

                    context.SharedColorProofWorkflows.Add(sharedWorkflow);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessSharedWorkflow, ex);
            }
        }

        /// <summary>
        /// Computes the url for accepting/rejecting shared workflow invitations received by email
        /// </summary>
        /// <param name="token"></param>
        /// <param name="domain"></param>
        /// <param name="isAccept"></param>
        /// <returns></returns>
        public static string GetShareWorkflowDecisionUrl(string token, string domain, bool isAccept)
        {
            return GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain +
            "/Settings/AcceptRejectColorProofWorkflow" + "?token=" + token + "&isAccept=" +
            (isAccept ? true.ToString() : false.ToString());
        }

        /// <summary>
        /// Tries to get shared workflow based on security token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static SharedColorProofWorkflow GetSharedWorkflowByToken(string token, DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                where shw.Token == token
                select shw).FirstOrDefault();
        }

        /// <summary>
        /// Gets workflowName by id
        /// </summary>
        /// <param name="wfId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetWorkflowNameById(int wfId, DbContextBL context)
        {
            return (from cpw in context.ColorProofCoZoneWorkflows
                    where cpw.ID == wfId
                    select cpw.Name).FirstOrDefault();
        }

        /// <summary>
        /// Gets the model for shared workflow details
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ColorProofSettings.SharedColorProofWorkflowDetails> GetSharedWorkflowDetails(int workflowID,
            DbContextBL context)
        {
            try
            {
                var details = (from shw in context.SharedColorProofWorkflows
                    join u in context.Users on shw.User equals u.ID
                    join ac in context.Accounts on u.Account equals ac.ID
                    join com in context.Companies on ac.ID equals com.Account
                    join acs in context.AccountStatus on ac.Status equals acs.ID
                    where shw.ColorProofCoZoneWorkflow == workflowID && acs.Key != "D"
                    select new ColorProofSettings.SharedColorProofWorkflowDetails
                           {
                               ID = shw.ID,
                               UserName = u.GivenName + " " + u.FamilyName,
                               CompanyName = com.Name,
                               Email = u.EmailAddress,
                               IsEnabled = shw.IsEnabled,
                               InviteStatus =
                                   shw.IsAccepted != null
                                       ? shw.IsAccepted.Value
                                           ? GMGColor.Resources.Resources.lblColorProofInstancesConnectionStatusShared
                                           : GMGColor.Resources.Resources.lblRejectedStatus
                                       : GMGColor.Resources.Resources.lblPending,
                                IsAccepted = shw.IsAccepted,
                                AccountName = ac.Name,
                                AccountUrl = (!ac.IsCustomDomainActive ? ac.Domain : ac.CustomDomain)
                           }).ToList();

                return details;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetSharedWorkflowDetails, ex);
            }
        }

        /// <summary>
        /// Determines if the specified workflow is shared or not
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsWorkflowShared(int workflowID, DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                where shw.ColorProofCoZoneWorkflow == workflowID
                select shw.ID).Any();
        }

        /// <summary>
        /// gets SharedWorkflow by ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static SharedColorProofWorkflow GetSharedColorProofWorkflowById(int id, DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                where shw.ID == id
                select shw).FirstOrDefault();
        }

        /// <summary>
        /// Determines if the specified workflow is shared with the specified account
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="accountId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsWorkflowSharedWithAccount(int workflowID, int accountId, DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                    join cpczw in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals cpczw.ID
                    join u in context.Users on shw.User equals u.ID
                    where u.Account == accountId && cpczw.ID == workflowID
                    select cpczw.ID
                    ).Any();
        }

        /// <summary>
        /// Gets the shared colorproof workflow owner details
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetSharedWorkflowOwnerInfo(int workflowID, int accountId, DbContextBL context)
        {
            var shwOwnerInfo = (from shw in context.SharedColorProofWorkflows
                                join cpczw in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals cpczw.ID
                                join cpwf in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpwf.ID
                                join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                                join owner in context.Users on cpi.Owner equals owner.ID
                                join ac in context.Accounts on owner.Account equals ac.ID
                                join com in context.Companies on ac.ID equals com.Account
                                join u in context.Users on shw.User equals u.ID
                                where cpczw.ID == workflowID && u.Account == accountId
                                select new
                                       {
                                           Name = owner.GivenName + " " + owner.FamilyName,
                                           CompanyName = com.Name
                                       }).FirstOrDefault();

            if (shwOwnerInfo != null)
            {
                return string.Format(GMGColor.Resources.Resources.lblSharedWorkflowOwnerInfo, shwOwnerInfo.Name, shwOwnerInfo.CompanyName);
                
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets workflow Security usergroups based on current account
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="accId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<UserGroup> GetUserGroupsListByWorkflowAndAccount(int workflowID, int accId, DbContextBL context)
        {
            return (from czwf in context.ColorProofCoZoneWorkflows
                join czwfug in context.ColorProofCoZoneWorkflowUserGroups on czwf.ID equals
                    czwfug.ColorProofCoZoneWorkflow
                join ug in context.UserGroups on czwfug.UserGroup equals ug.ID
                where ug.Account == accId && czwf.ID == workflowID
                select ug).ToList();
        }

        /// <summary>
        /// Determines if the owner belonges to the specified account
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="accId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsWorkflowOwner(int workflowID, int accId, DbContextBL context)
        {
            return (from cpczw in context.ColorProofCoZoneWorkflows
                join cpwf in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpwf.ID
                join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                join u in context.Users on cpi.Owner equals u.ID
                join ac in context.Accounts on u.Account equals ac.ID
                where ac.ID == accId && cpczw.ID == workflowID
                select ac.ID).Any();
        }

        /// <summary>
        /// Gets workflow Security usergroups based on current account
        /// </summary>
        /// <param name="workflowID"></param>
        /// <param name="accId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ColorProofCoZoneWorkflowUserGroup> GetWfUserGroupsListByWorkflowAndAccount(int workflowID, int accId, DbContextBL context)
        {
            return (from czwf in context.ColorProofCoZoneWorkflows
                    join czwfug in context.ColorProofCoZoneWorkflowUserGroups on czwf.ID equals
                        czwfug.ColorProofCoZoneWorkflow
                    join ug in context.UserGroups on czwfug.UserGroup equals ug.ID
                    where ug.Account == accId && czwf.ID == workflowID
                    select czwfug).ToList();
        }

        /// <summary>
        /// Gets Shared workflow by colorproof workflow id and by shared account id
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="accId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<SharedColorProofWorkflow> GetSharedWorkflowByWfAndAccount(int workflowId, int accId,
            DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                    join cpczw in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals cpczw.ID
                    join u in context.Users on shw.User equals u.ID
                    where cpczw.ID == workflowId && u.Account == accId
                    select shw).ToList();
        }

        /// <summary>
        /// Determines if a valid shared colorproof workflow instance exists for the specified user
        /// </summary>
        /// <param name="instanceName"></param>
        /// <param name="accID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool RunningSharedColorProofInstanceExist(string instanceName, int accID, DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                    join cpczw in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals cpczw.ID
                    join cpwf in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpwf.ID
                    join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                    join cpis in context.ColorProofInstanceStates on cpi.State equals cpis.ID
                    join u in context.Users on shw.User equals u.ID
                    join ac in context.Accounts on u.Account equals ac.ID
                    where
                    cpi.SystemName.ToLower() == instanceName.ToLower() && cpi.IsDeleted == false
                    && ac.ID == accID
                    && cpis.Key == (int)ColorProofInstance.ColorProofInstanceStateEnum.Running
                    select shw).Any();
        }

        /// <summary>
        /// Determines if a valid shared ColorProof workflow exists for the specified user
        /// </summary>
        /// <param name="instanceName"></param>
        /// <param name="workflowName"></param>
        /// <param name="accID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool SharedColorProofWorkflowExists(string instanceName, string workflowName, int accID,
            DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                    join cpczwf in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals  cpczwf.ID
                    join cpwf in context.ColorProofWorkflows on cpczwf.ColorProofWorkflow equals cpwf.ID
                    join cpins in context.ColorProofInstances on cpwf.ColorProofInstance equals cpins.ID
                    join u in context.Users on shw.User equals  u.ID
                    join ac in context.Accounts on u.Account equals ac.ID
                    where ac.ID == accID && cpins.SystemName.ToLower() == instanceName.ToLower() && cpczwf.Name.ToLower() == workflowName.ToLower() && cpczwf.IsDeleted == false
                    && shw.IsEnabled && shw.IsAccepted.HasValue
                    select cpczwf.ID).Any();
        }

        /// <summary>
        /// Returns the creator of the ColorProof Workflow
        /// </summary>
        /// <param name="colorProofInstanceId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static User GetUserByWorkflowID(int? workflowId, DbContextBL context)
        {
            return (from cpcw in context.ColorProofCoZoneWorkflows
                    join cpw in context.ColorProofWorkflows on cpcw.ColorProofWorkflow equals cpw.ID
                    join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals  cpi.ID
                    join u in context.Users on cpi.Owner equals u.ID
                    where cpcw.ID == workflowId
                    select u).FirstOrDefault();
        }

        public static bool IsSharedWorkflow(int? workflowId, DbContextBL context)
        {
            return (from shw in context.SharedColorProofWorkflows
                    where shw.ColorProofCoZoneWorkflow == workflowId
                    select shw.ID).Any();
        }

        /// <summary>
        /// Returns the user that has shared the workflow 
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetCPWorkflowOwner(int workflowId, GMGColorContext context)
        {
            try
            {
                return (
                        from cpczw in context.ColorProofCoZoneWorkflows
                        join cpwf in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpwf.ID
                        join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                        where cpczw.ID == workflowId
                        select cpi.Owner).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetExternalUserBySharedWorkflowId()", ex);
            }
        }

        public static void SendSharedWorkflowInvitation(User loggeUser, SharedColorProofWorkflow sharedWorkflow,
            DbContextBL context)
        {
            try
            {
                var mailInfo = (from czwf in context.ColorProofCoZoneWorkflows
                join cpwf in context.ColorProofWorkflows on czwf.ColorProofWorkflow equals cpwf.ID
                join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                join u in context.Users on cpi.Owner equals u.ID
                join ac in context.Accounts on u.Account equals ac.ID
                join com in context.Companies on ac.ID equals com.Account
                join shw in context.SharedColorProofWorkflows on czwf.ID equals shw.ColorProofCoZoneWorkflow
                join receiver in context.Accounts on shw.User1.Account equals  receiver.ID
                where u.ID == loggeUser.ID && czwf.ID == sharedWorkflow.ColorProofCoZoneWorkflow && shw.ID == sharedWorkflow.ID
                select new
                       {
                           reiceiverDomain = receiver.Domain,
                           workflowName = czwf.Name,
                           companyName = com.Name
                       }
                ).FirstOrDefault();

                NotificationServiceBL.CreateNotification(new GMGColorNotificationService.ShareWorkflowInvitation
                                                                {
                                                                    EventCreator = loggeUser.ID,
                                                                    InternalRecipient = sharedWorkflow.User,
                                                                    CreatedDate = DateTime.UtcNow,
                                                                    WorkflowName = mailInfo.workflowName,
                                                                    CompanyName = mailInfo.companyName,
                                                                    LoggedUserName = loggeUser.GivenName + " " + loggeUser.FamilyName,
                                                                    AcceptUrl = GetShareWorkflowDecisionUrl(sharedWorkflow.Token, mailInfo.reiceiverDomain, true),
                                                                    RejectUrl = GetShareWorkflowDecisionUrl(sharedWorkflow.Token, mailInfo.reiceiverDomain, false)
                                                                },
                                                               null,
                                                               context
                                                               );
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSendWorkflowShare, ex);
            }
        }

        public static void DeleteSharedColorProofWorkflow(SharedColorProofWorkflow sharedWorkflow, DbContextBL context)
        {
            try
            {
                bool accountIsActive;
                //Delete usergroups from the shared workflow account
                List<ColorProofCoZoneWorkflowUserGroup> sharedWfGroups =
                    GetWfUserGroupsListByWorkflowAndAccount(sharedWorkflow.ColorProofCoZoneWorkflow,
                        UserBL.GetAccountIdByUser(sharedWorkflow.User, out accountIsActive, context), context);

                for (int i = 0; i < sharedWfGroups.Count; i++)
                {
                    context.ColorProofCoZoneWorkflowUserGroups.Remove(sharedWfGroups[i]);
                }

                context.SharedColorProofWorkflows.Remove(sharedWorkflow);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteShareWorkflow, ex);
            }
        }

        public static void DeleteGarbageDeliverFolders(DbContextBL context)
        {
            try
            {
                List<string> deliverFolders = GMGColor.AWS.AWSSimpleStorageService.GetAllFolders(
                        GMGColorConfiguration.AppConfiguration.DataFolderName(GMGColorConfiguration.AppConfiguration.AWSRegion),
                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey, "deliver/");

                foreach (string deliverFolder in deliverFolders)
                {
                    string deliverGuid = Path.GetFileName(deliverFolder.Remove(deliverFolder.Length - 1));
                    if (!context.DeliverJobs.Any(t => t.Guid == deliverGuid))
                    {
                        GMGColorIO.DeleteFolder(deliverFolder, GMGColorConfiguration.AppConfiguration.AWSRegion);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static List<NotificationPreset> GetDeliverNotificationPresets(int accId, DbContextBL context)
        {
            try
            {
                return (from p in context.AccountNotificationPresets
                        join npe in context.AccountNotificationPresetEventTypes on p.ID equals npe.NotificationPreset
                        join evt in context.EventTypes on npe.EventType equals evt.ID
                        join eg in context.EventGroups on evt.EventGroup equals eg.ID
                        where eg.Key == "DL" && p.Account == accId
                        select new NotificationPreset
                        {
                            ID = p.ID,
                            Name = p.Name
                        }).Distinct().ToList();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Could not get presets, error: " + ex.Message, ex);
                return null;
            }
        }

        /// <summary>
        /// Get all external users related with the selected job
        /// </summary>
        /// <param name="deliverJobId">Job Id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static List<AccountSettings.DeliverExternalUser> GetExternalUsersByJobId(int deliverJobId, DbContextBL context)
        {
            return (from ec in context.DeliverExternalCollaborators
                    join dsj in context.DeliverSharedJobs on ec.ID equals dsj.DeliverExternalCollaborator
                    join ac in context.Accounts on ec.Account equals ac.ID
                    join acs in context.AccountStatus on ac.Status equals acs.ID
                    join com in context.Companies on ac.ID equals com.Account
                    where dsj.DeliverJob == deliverJobId
                    select new AccountSettings.DeliverExternalUser
                    {
                        ID = ec.ID,
                        Name = ec.GivenName + " " + ec.FamilyName,
                        Email = ec.EmailAddress,
                        Company = com.Name
                    }).Distinct().ToList();
        }

        /// <summary>
        /// Returns a list of deliver external users selected for current job
        /// </summary>
        /// <param name="loggedAccountId">Id of the logged account</param>
        /// <param name="loggedUserId">Id of the logged user</param>
        /// <param name="usersAndEmails">A string with user's id and their selected preset (this string contains details only for existing users)</param>
        /// <param name="usersAndPresets">A string with email, selected preset, first and last name (this string contains details only for new added users)</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static List<DeliverExternalCollaboratorPreset> GetDeliverExternalCollaborators(int loggedAccountId, int loggedUserId, string usersAndEmails, string usersAndPresets, DbContextBL context)
        {
            string strUsersAndPresets = !String.IsNullOrEmpty(usersAndPresets) ? usersAndPresets.Replace("[", string.Empty).Replace("]", string.Empty).Replace("\"", string.Empty).Trim() : string.Empty;
            string strUsersAndEmails = !String.IsNullOrEmpty(usersAndEmails) ? usersAndEmails.Replace("[", string.Empty).Replace("]", string.Empty).Replace("\"", string.Empty).Trim() : string.Empty;

            List<string> selectedEmailsWithPreset = (String.IsNullOrEmpty(strUsersAndEmails) ? new List<string>() : strUsersAndEmails.Split(',').ToList());
            List<string> selectedUsersWithPreset = (String.IsNullOrEmpty(strUsersAndPresets) ? new List<string>() : strUsersAndPresets.Split(',').ToList());

            //create a list with new added deliver external users if any
            var deliverExternalCollaboratorPreset = selectedEmailsWithPreset.Select(emailWithPreset => emailWithPreset.Split('|')).Select(emailPreset => new DeliverExternalCollaboratorPreset
            {
                PresetId = int.Parse(emailPreset[1]),
                DeliverExternalCollaborator = new DeliverExternalCollaborator
                {
                    Account = loggedAccountId,
                    Creator = loggedUserId,
                    EmailAddress = emailPreset[0],
                    GivenName = emailPreset[2].Split(' ')[0],
                    FamilyName = emailPreset[2].Split(' ')[1],
                    Guid = Guid.NewGuid().ToString(),
                    Locale = Common.Utils.ToInt(emailPreset[3]).GetValueOrDefault()
                }
            }).ToList();

            //merge list with existing deliver external users if any
            deliverExternalCollaboratorPreset.AddRange(selectedUsersWithPreset.Select(userWithPreset => userWithPreset.Split('|')).Select(userPreset => new DeliverExternalCollaboratorPreset
            {
                PresetId = int.Parse(userPreset[1]),
                DeliverExternalCollaborator = DeliverExternalCollaborator.GetDeliverExternalCollaboratorById(int.Parse(userPreset[0]), context)
            }));

            return deliverExternalCollaboratorPreset;
        }

        public static List<int> GetDeliverEventsIds(DbContextBL context)
        {
            return (from e in context.EventTypes
                    join eg in context.EventGroups on e.EventGroup equals eg.ID
                    where eg.Key == "DL"
                    select e.ID).ToList();
        }

        /// <summary>
        /// Returns the relations between current preset and deliver job, if any
        /// </summary>
        /// <param name="presetId">Preset id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static List<DeliverSharedJob> GetDeliverJobSharesByPresetId(int presetId, DbContextBL context)
        {
            return (from ds in context.DeliverSharedJobs
                    where ds.NotificationPreset == presetId
                    select ds).ToList();
        }

        public static bool ExtUserHasAccessToJob(string userGuid, int jobId, DbContextBL context)
        {
            return (from dsj in context.DeliverSharedJobs
                join dex in context.DeliverExternalCollaborators on dsj.DeliverExternalCollaborator equals dex.ID
                where dsj.DeliverJob == jobId && dex.Guid == userGuid
                select dsj.ID).Any();
        }

        public static void DeleteDeliverJob(int manageJobId, DbContextBL context)
        {
            try
            {
                 DeliverJob deliverJob = (from dj in context.DeliverJobs
                                                      where dj.ID == manageJobId
                                                      select dj).SingleOrDefault();

                if (deliverJob != null)
                {
                    //Delete the relation with DeliverSharedJobs for this job
                    context.DeliverSharedJobs.RemoveRange(deliverJob.DeliverSharedJobs);

                    deliverJob.IsDeleted = true;

                    Dictionary<string, string> dict = Utils.CreateCommandQueueMessage(deliverJob.Guid, deliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.UserName, 
                                                                                        deliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.Password,
                                                                                        deliverJob.ColorProofCoZoneWorkflow.ColorProofWorkflow1.ColorProofInstance1.Guid,
                                                                                        CommandType.Delete);
                    GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.DeliverJob);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteDeliverJob, ex);
            }
        }

        /// <summary>
        /// Checks if the thumbnails for the specified jobs exist
        /// </summary>
        /// <param name="jobGuids"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool AreThumbnailsProcessed(string[] jobGuids, DbContextBL context)
        {
            string firstGuid = jobGuids[0];
            string region = (from dj in context.DeliverJobs
                join j in context.Jobs on dj.Job equals j.ID
                join ac in context.Accounts on j.Account equals ac.ID
                where dj.Guid == firstGuid
                select ac.Region).FirstOrDefault();

            if (!String.IsNullOrEmpty(region))
            {
                foreach (string jobGuid in jobGuids)
                {
                    string thumbnailName = "Thumb-" + DeliverJob.SmallThumbnailDimension + "px.jpg";
                    string fileRelativePath = Path.Combine(GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath, jobGuid, "1", thumbnailName).Replace("\\", "/");
                    if (!GMGColorIO.FileExists(fileRelativePath, region))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static List<string> GetDeliverJobDownloadPath(string deliverIDs, int loggedUserAccount, DbContextBL context, Role.RoleName loggedUserDeliverRole)
        {
            try
            {
                var relativePaths = new List<string>();
                
                foreach (string id in deliverIDs.Trim().Split(','))
                {
                    if (!String.IsNullOrEmpty(id.Trim()))
                    {
                        DeliverJob objDeliverJob = GetJobById(Int32.Parse(id), context);

                        if (objDeliverJob != null && (objDeliverJob.User.Account == loggedUserAccount || loggedUserDeliverRole == Role.RoleName.AccountAdministrator))
                        {
                            var relativeFolderPath = "deliver/" + objDeliverJob.Guid + "/" + objDeliverJob.FileName;
                            relativePaths.Add(relativeFolderPath);
                        }
                    }
                }

                return relativePaths;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, loggedUserAccount, "Error in Base Control -> DeliverBL -> GetDeliverJobDownloadPath Method. Exception : {0} ", ex.Message);
                throw new Exception(GMGColor.Resources.Resources.txtFileDownloadError);
            }
        }

        private static DeliverJob GetJobById(int jobId, DbContextBL context)
        {
            return (from dj in context.DeliverJobs
                    where dj.ID == jobId
                    select dj).FirstOrDefault();
        }
    }
}
