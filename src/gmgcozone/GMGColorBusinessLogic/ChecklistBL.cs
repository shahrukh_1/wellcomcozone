﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.Checklists;
using GMGColorNotificationService;


namespace GMGColorBusinessLogic
{
    public class ChecklistBL
    {
        public static List<ChecklistModel> GetCheckLists(int accountId, DbContextBL context, string searchText = null)
        {
            bool searchCheckListName = !String.IsNullOrEmpty(searchText);
            return (from cl in context.CheckList
                    let hasItemes = (from cli in context.CheckListItem
                                     where cli.CheckList == cl.ID && cli.IsDeleted == false
                                     select cli.ID).Any()
                    where cl.Account == accountId && (!searchCheckListName || cl.Name.Contains(searchText)) && cl.IsDeleted == false
                    select new ChecklistModel()
                    {
                        ID = cl.ID,
                        Account = cl.Account,
                        Name = cl.Name,
                        HasItems = hasItemes

                    }).ToList();

        }

        public static int AddOrUpdateCheckList(ChecklistModel model, int accountId, int loggedUserId, DbContextBL context)
        {
            try
            {
                var checkList = new CheckList();
                if (model != null)
                {
                    if (model.ID > 0)
                    {
                        checkList = context.CheckList.FirstOrDefault(cl => cl.ID == model.ID);

                        if (checkList != null)
                        {
                            checkList.Name = model.Name;
                            checkList.ModifiedDate = DateTime.UtcNow;
                            checkList.ModifiedBy = loggedUserId;

                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Name))
                        {
                            checkList.Name = model.Name;
                            checkList.Account = accountId;
                            checkList.Guid = Guid.NewGuid().ToString();
                            checkList.CreatedDate = checkList.ModifiedDate = DateTime.UtcNow;
                            checkList.CreatedBy = checkList.ModifiedBy = loggedUserId;
                            context.CheckList.Add(checkList);
                        }
                    }
                    checkList.IsDeleted = false;
                    context.SaveChanges();
                }
                return checkList.ID;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddOrUpdateChecklist, ex);
            }
        }


        public static ChecklistItemViewModel GetCheckListItemDetails(Account loggedAccount, int? selectedCheckList, DbContextBL context)
        {
            var iteme = (from cli in context.CheckListItem
                         where cli.ID == selectedCheckList.Value
                         select new
                         {
                             cli.Name,
                             cli.Position,
                         }).FirstOrDefault();
            if (iteme != null)
            {
                var itemeModel = new ChecklistItemViewModel
                {
                    ItemName = iteme.Name,
                    Position = iteme.Position,

                };
                return itemeModel;
            }
            return new ChecklistItemViewModel();
        }

        public static void PopulateStaticData(ChecklistItemViewModel model, Account loggedAccount, DbContextBL context)
        {

        }

        // add or update CheckListItem

        /// <summary>
        /// Adds or updates current CheckList Item
        /// </summary>
        /// <param name="model">Checklist Item model</param>
        /// <param name="context">Database context</param>
        public static int AddOrUpdateChecklistItem(ChecklistItemViewModel model, Account loggedAccount, int loggedUserId, DbContextBL context)
        {
            try
            {
                var checkListItem = new CheckListItem();

                if (model != null)
                {
                    if (model.ID > 0)
                    {
                        checkListItem = context.CheckListItem.FirstOrDefault(cli => cli.ID == model.ID);

                        if (checkListItem != null)
                        {
                            checkListItem.Name = model.ItemName;
                            checkListItem.ModifiedDate = DateTime.UtcNow;
                            checkListItem.ModifiedBy = loggedUserId;
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.ItemName))
                        {
                            int[] itemPositions = context.CheckListItem.Where(aw => aw.CheckList == model.Checklist).Select(t => t.Position).ToArray();
                            checkListItem.Position = itemPositions.Any() ? itemPositions.Max() + 1 : 1;

                            checkListItem.CheckList = model.Checklist;
                            checkListItem.Name = model.ItemName;
                            checkListItem.Guid = Guid.NewGuid().ToString();
                            checkListItem.CreatedDate = checkListItem.ModifiedDate = DateTime.UtcNow;
                            checkListItem.CreatedBy = checkListItem.ModifiedBy = loggedUserId;
                            context.CheckListItem.Add(checkListItem);
                        }
                    }
                    checkListItem.IsDeleted = false;
                    context.SaveChanges();
                }
                return checkListItem.ID;

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddOrUpdateChecklistItem, ex);
            }
        }



        //Delete CheckList
        public static void DeleteCheckList(int checkList, DbContextBL context)
        {
            try
            {
                var Check_List = (from cl in context.CheckList where cl.ID == checkList select cl).FirstOrDefault();
                if (Check_List != null)
                {
                   
                    // Delete in CheckList Item
                    context.CheckList.Remove(Check_List);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteChecklist, ex);
            }
        }


        //Delete CheckListItem
        public static void DeleteCheckListItem(int checkListItem, DbContextBL context)
        {
            try
            {
                var CheckListItem = (from cli in context.CheckListItem where cli.ID == checkListItem select cli).FirstOrDefault();
                if (CheckListItem != null)
                {
                    ////Delete in Approval annotation checklistItem 
                    //context.ApprovalAnnotationCheckListItems.RemoveRange(CheckListItem.ApprovalAnnotationCheckListItem);

                    //// Delete in CheckList Item
                    //context.CheckListItem.Remove(CheckListItem);
                    CheckListItem.IsDeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteChecklistItem, ex);
            }
        }


        //Get checklist itemes based on checklist
        public static ChecklistItemsDashboardModel GetChecklistsItemesByID(int checklist, int accountId, DbContextBL context)
        {
            var model = new ChecklistItemsDashboardModel { Checklist = checklist };

            var checklistItemInfo = (from cli in context.CheckListItem
                                     join cl in context.CheckList on cli.CheckList equals cl.ID
                                     where cl.Account == accountId && cl.ID == checklist && cli.IsDeleted == false
                                     orderby cli.Position
                                     select new
                                     {
                                         ID = cli.ID,
                                         Order = cli.Position,
                                         Name = cli.Name,
                                     }).ToList();

            foreach (var item in checklistItemInfo)
            {
                var checklistDetailes = new ItemDetails
                {
                    ID = item.ID,
                    Name = item.Name,
                    Order = item.Order,
                };

                model.ChecklistItemDetails.Add(checklistDetailes);
            }

            return model;
        }

        public static void ChangeChecklistItemPosition(int sourceChecklistId, int destinationChecklistId, DbContextBL context)
        {
            try
            {
                var objChangingItem = (from ap in context.CheckListItem where ap.ID == sourceChecklistId select ap).FirstOrDefault();
                var objChangedPhase = (from ap in context.CheckListItem where ap.ID == destinationChecklistId select ap).FirstOrDefault();

                if (objChangingItem != null && objChangedPhase != null)
                {
                    var changingPosition = objChangingItem.Position;
                    var changedPosition = objChangedPhase.Position;

                    objChangedPhase.Position = changingPosition;
                    objChangingItem.Position = changedPosition;
                }

                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotChnageChecklistItemPosition, ex);
            }
        }

    }
}
