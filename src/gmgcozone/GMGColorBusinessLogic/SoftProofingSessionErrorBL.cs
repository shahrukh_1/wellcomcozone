﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class SoftProofingSessionErrorBL : BaseBL
    {
        /// <summary>
        /// Cleanup previous softproofing session errors
        /// </summary>
        /// <param name="approvalPageId"></param>
        /// <param name="softProofingGuid"></param>
        public static void CleanupSoftProofingSessionError(int approvalPageId, string softProofingGuid)
        {
            using (DbContextBL context = new DbContextBL())
            {
                List<SoftProofingSessionError> softProofingErrorsToDelete = (from spe in context.SoftProofingSessionErrors
                                                                                join sp in context.SoftProofingSessionParams on spe.SoftProofingSessionParams equals sp.ID
                                                                                where spe.ApprovalPage == approvalPageId && sp.SessionGuid == softProofingGuid
                                                                                select spe).ToList();
                if (softProofingErrorsToDelete.Count > 0)
                {
                    context.SoftProofingSessionErrors.RemoveRange(softProofingErrorsToDelete);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Log softproofing session error
        /// </summary>
        /// <param name="approvalPageId"></param>
        /// <param name="softProofingGuid"></param>
        public static void LogSoftProofingSessionError(int approvalPageId, string softProofingGuid, string errorMessage)
        {
            using (DbContextBL context = new DbContextBL())
            {
                SoftProofingSessionError softProofingError = new SoftProofingSessionError();
                softProofingError.ApprovalPage = approvalPageId;
                softProofingError.SoftProofingSessionParams = context.SoftProofingSessionParams.Single(sp => sp.SessionGuid == softProofingGuid).ID;
                if (errorMessage.Length > 500)
                {
                    errorMessage = errorMessage.Substring(0, 500);
                }
                softProofingError.Error = errorMessage;
                softProofingError.LogDate = DateTime.UtcNow;
                context.SoftProofingSessionErrors.Add(softProofingError);

                context.SaveChanges();
            }
        }
    }
}
