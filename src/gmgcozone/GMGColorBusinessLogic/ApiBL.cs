﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using GMGColor.Resources;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.Api;
using GMGColorNotificationService;
using ApprovalPage = GMGColorDAL.CustomModels.Api.ApprovalPage;
using Role = GMGColorDAL.CustomModels.Api.Role;
using Shape = GMGColorDAL.CustomModels.Api.Shape;
using System.Transactions;
using System.Xml.Linq;
using System.IO;
using GMGColor.AWS;
using AppModule = GMG.CoZone.Common.AppModule;
using ApprovalAnnotationAttachment = GMGColorDAL.CustomModels.Api.ApprovalAnnotationAttachment;
using ApprovalChecklistItem = GMGColorDAL.CustomModels.Api.ApprovalChecklistItem;
using UserGroup = GMGColorDAL.CustomModels.Api.UserGroup;
using Constants = GMG.CoZone.Common.Constants;
using System.Data.Entity;
using GMG.WebHelpers.Job;
using System.Diagnostics;
using ApprovalAnnotationStatusChangeLog = GMGColorDAL.ApprovalAnnotationStatusChangeLog;
using User = GMGColorDAL.User;
using GMG.CoZone.Proofstudio.DomainModels.Requests;
using GMG.CoZone.Collaborate.Interfaces;
using GMG.CoZone.Proofstudio.Interfaces;
using GMG.CoZone.Common;
using System.Web;
using System.Net;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Repositories.Interfaces;

namespace GMGColorBusinessLogic
{
    public class ApiBL : BaseBL
    {
        public const int ProofStudioPenWidthDefaultValue = 2;
        private const string ShowAllFilesToAdminSetting = "ShowAllFilesToAdmins";

        [ObsoleteAttribute("This enum is obsolete. Call ApprovalType from collaborate module instead.", false)]
        private enum ApprovalType
        {
            Image = 0,
            Video = 1,
            Swf = 2
        }
        private const string DateFormat = "yyyy-MM-dd HH:mm:ss"; //2014-04-30 13:09:40

        [ObsoleteAttribute("This method is obsolete. Call DateTime2String from collaborate module instead.", false)]
        private static string DateTime2String(DateTime dt, string timezone)
        {
            return GMGColorFormatData.GetUserTimeFromUTC(dt, timezone).ToString(DateFormat);
        }

        private static DateTime String2DateTime(string dt)
        {
            IFormatProvider cultureInfo = new System.Globalization.CultureInfo("en-US", true);
            return DateTime.ParseExact(dt, DateFormat, cultureInfo);
        }

        [ObsoleteAttribute("This method is obsolete. Call IsShowAllFilesToAdminsChecked from collaborate module instead.", false)]
        public static bool IsShowAllFilesToAdminsChecked(DbContextBL context, int accountId)
        {
            return ((from acs in context.AccountSettings
                     where acs.Account == accountId && acs.Name == ShowAllFilesToAdminSetting
                     select acs.Value).FirstOrDefault() ?? string.Empty).ToLower() == Boolean.TrueString.ToLower();
        }

        public static bool UserIsManager(DbContextBL context, string userKey)
        {
            string showAllFilesToManagers = (from acs in context.AccountSettings
                                           join u in context.Users on acs.Account equals u.Account
                                           where u.Guid == userKey.ToLower() && acs.Name == "ShowAllFilesToManagers"
                                           select acs.Value).FirstOrDefault();

            if(showAllFilesToManagers == "True")
            {
                return (from u in context.Users
                        join us in context.UserStatus on u.Status equals us.ID
                        join ur in context.UserRoles on u.ID equals ur.User
                        join r in context.Roles on ur.Role equals r.ID
                        join am in context.AppModules on r.AppModule equals am.ID
                        where u.Guid.ToLower() == userKey.ToLower() &&
                        us.Key.ToUpper() == "A" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate &&
                        r.Key.ToUpper() == "MAN"
                        select true).FirstOrDefault();
            }
            else
            {
                return false;
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call UserIsAdministrator from collaborate module instead.", false)]
        public static bool UserIsAdministrator(DbContextBL context, string userKey)
        {
            return (from u in context.Users
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where
                        u.Guid.ToLower() == userKey.ToLower() &&
                        us.Key.ToUpper() == "A" &&
                        am.Key == (int) GMG.CoZone.Common.AppModule.Collaborate &&
                        r.Key.ToUpper() == "ADM"
                    select true).FirstOrDefault();
        }

        public static List<ApprovalPagesHeightAndWidth> GetPagesHeightAndWidth(DbContextBL context, int approvalID)
        {
            try
            {
                return (from ap in context.ApprovalPages
                        join a in context.Approvals on ap.Approval equals a.ID
                        where a.ID == approvalID
                        select new ApprovalPagesHeightAndWidth
                        {
                            OriginalImageHeight = ap.OriginalImageHeight,
                            OriginalImageWidth = ap.OriginalImageWidth
                        }).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
        }

        public static UserResponse GetUser(string userGuid, int approvalId, bool isExternalUser,
                                           string proofStudioShowAnnotationsForExternalUsersSettingsName)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    UserResponse result = null;

                    var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
                    var userInfo = !isExternalUser ? (from u in context.Users
                                                      join acc in context.Accounts on u.Account equals acc.ID
                                                      join ur in context.UserRoles on u.ID equals ur.User
                                                      join r in context.Roles on ur.Role equals r.ID
                                                      join l in context.Locales on u.Locale equals l.ID
                                                      join am in context.AppModules on r.AppModule equals am.ID
                                                      join c in context.ProofStudioUserColors on u.ProofStudioColor equals c.ID
                                                      let brandingPresetHeaderLogo = (from bp in context.BrandingPresets
                                                                                      join bpug in context.BrandingPresetUserGroups on bp.ID
                                                                                          equals bpug.BrandingPreset
                                                                                      join ug in context.UserGroups on bpug.UserGroup equals ug.ID
                                                                                      join ugu in context.UserGroupUsers on ug.ID equals
                                                                                          ugu.UserGroup
                                                                                      where ugu.User == u.ID && ugu.IsPrimary && bp.HeaderLogoPath != null
                                                                                      select
                                                                                    "usergroupbranding/" + bp.Guid + @"/" +
                                                                                    bp.HeaderLogoPath).FirstOrDefault()
                                                      let accountHeaderLogo = (from acc in context.Accounts
                                                                               join u in context.Users on acc.ID equals u.Account
                                                                               where u.Guid == userGuid
                                                                               select "accounts/" + acc.Guid + "/" + acc.HeaderLogoPath).FirstOrDefault()
                                                      let showAllPhases = (from ust in context.UserSettings
                                                                           where ust.User == u.ID && ust.Setting == showAnnotationsFromAllPhases

                                                                           select ust.Value == "True").FirstOrDefault()
                                                      where
                                                          u.Guid == userGuid &&
                                                          r.Key != "NON" &&
                                                          am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate
                                                      select new
                                                      {
                                                          ID = u.ID,
                                                          Username = u.Username,
                                                          UserLocaleName = l.Name,
                                                          UserPhotoPath = u.PhotoPath,
                                                          UserRole = (int?)r.ID,
                                                          UserAvatar = string.Empty,
                                                          UserIsExternal = isExternalUser,
                                                          UserColor = c.HEXValue,
                                                          Domain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                                          BrandingHeaderLogo = string.IsNullOrEmpty(brandingPresetHeaderLogo) ? accountHeaderLogo : "accounts/" + acc.Guid + "/" + brandingPresetHeaderLogo,
                                                          CanSeeAllAnnotations = true,
                                                          ShowAnnotationsOfAllPhases = showAllPhases,
                                                          acc.Region,
                                                          PrivateAnnotations = u.PrivateAnnotations
                                                      }).FirstOrDefault()
                                                :
                                                    (from ec in context.ExternalCollaborators
                                                     join acc in context.Accounts on ec.Account equals acc.ID
                                                     join l in context.Locales on ec.Locale equals l.ID
                                                     let role = (from r in context.Roles
                                                                 join am in context.AppModules on r.AppModule equals am.ID
                                                                 where r.Key == "NON" && am.Key == (int) AppModule.Collaborate
                                                                 select r.ID).FirstOrDefault()
                                                     let canSeeAllAnnotations = (from accs in context.AccountSettings
                                                                                 where
                                                                                     accs.Account == ec.Account &&
                                                                                     accs.Name.ToLower() ==
                                                                                     proofStudioShowAnnotationsForExternalUsersSettingsName
                                                                                         .ToLower() &&
                                                                                     accs.Value.ToLower() == "true"
                                                                                 select true).Any()
                                                     where
                                                         ec.Guid == userGuid &&
                                                         ec.IsDeleted == false
                                                     select new
                                                     {
                                                         ID = ec.ID,
                                                         Username = ec.EmailAddress,
                                                         UserLocaleName = l.Name,
                                                         UserPhotoPath = string.Empty,
                                                         UserRole = (role > 0) ? (int?) role : null,
                                                         UserAvatar = string.Empty,
                                                         UserIsExternal = true,
                                                         UserColor = ec.ProofStudioColor,
                                                         Domain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                                         BrandingHeaderLogo = string.Empty,
                                                         CanSeeAllAnnotations = canSeeAllAnnotations,
                                                         ShowAnnotationsOfAllPhases = ec.ShowAnnotationsOfAllPhases,
                                                         acc.Region,
                                                         PrivateAnnotations = false
                                                     }
                                                ).FirstOrDefault();

                    if (userInfo != null)
                    {
                        result = new UserResponse
                        {
                            ID = userInfo.ID,
                            Username = userInfo.Username,
                            UserLocaleName = userInfo.UserLocaleName,
                            UserPhotoPath = userInfo.UserPhotoPath,
                            UserRole = userInfo.UserRole,
                            UserAvatar = userInfo.UserAvatar,
                            UserIsExternal = isExternalUser,
                            UserColor = userInfo.UserColor,
                            BrandingHeaderLogo = userInfo.BrandingHeaderLogo,
                            CanSeeAllAnnotations = userInfo.CanSeeAllAnnotations,
                            ShowAnnotationsOfAllPhases = userInfo.ShowAnnotationsOfAllPhases,
                            PrivateAnnotations = userInfo.PrivateAnnotations
                        };

                        if (!isExternalUser)
                        {
                            result.UserAvatar = User.GetImagePath(userInfo.Region, userInfo.Domain,
                                userInfo.UserPhotoPath, userGuid, true);
                        }
                        else
                        {
                            result.UserAvatar = User.GetDefaultImagePath(userInfo.Domain);

                        }

                        if (!string.IsNullOrEmpty(result.BrandingHeaderLogo))
                        {
                            result.BrandingHeaderLogo = GMGColorIO.GetAbsoluteFilePath(result.BrandingHeaderLogo, userInfo.Region, userInfo.Domain);
                        }
                    }

                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetUser, ex);
            }
        }

        public static string GetUserLocaleName(string userGuid, bool isExternalUser)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    var userLocaleName = !isExternalUser ? (from u in context.Users
                                                            join l in context.Locales on u.Locale equals l.ID
                                                            where
                                                                u.Guid == userGuid
                                                            select l.Name).FirstOrDefault()
                                                :
                                                    (from ec in context.ExternalCollaborators
                                                     join l in context.Locales on ec.Locale equals l.ID
                                                     where
                                                         ec.Guid == userGuid &&
                                                         ec.IsDeleted == false
                                                     select l.Name).FirstOrDefault();

                    return userLocaleName;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetUserLocale, ex);
            }
        }

        public static string GetValueOfShowAllFilesToManagers(int loggedAccountId, string showAllFilesToManagers, DbContextBL context)
        {
                return (from acs in context.AccountSettings
                        where acs.Account == loggedAccountId && acs.Name == showAllFilesToManagers
                        select acs.Value).FirstOrDefault();
        }

        public static AccountResponse GetAccount(string userGuid, bool isExternalUser)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    AccountResponse account = !isExternalUser
                                                  ? ((from a in context.Accounts
                                                      join accs in context.AccountStatus on a.Status equals accs.ID
                                                      join acct in context.AccountTypes on a.AccountType equals acct.ID
                                                      join u in context.Users on a.ID equals u.Account
                                                      join df in context.DateFormats on a.DateFormat equals df.ID
                                                      join tf in context.TimeFormats on a.TimeFormat equals tf.ID
                                                      let userBrandingThemeName = (from ugu in context.UserGroupUsers
                                                                                   join user in context.Users on
                                                                                       ugu.User equals user.ID
                                                                                   join bprug in
                                                                                       context.BrandingPresetUserGroups
                                                                                       on ugu.UserGroup equals
                                                                                       bprug.UserGroup
                                                                                   join bpr in context.BrandingPresets
                                                                                       on bprug.BrandingPreset equals
                                                                                       bpr.ID
                                                                                   join thm in context.BrandingPresetThemes on
                                                                                       bpr.ID equals thm.BrandingPreset
                                                                                   where
                                                                                       ugu.IsPrimary &&
                                                                                       user.Guid == userGuid
                                                                                   select thm.ThemeName).FirstOrDefault()

                                                      let hasSubscription = acct.Key != "SBSC" ? (
                                                                                                from asp in
                                                                                                    context.AccountSubscriptionPlans
                                                                                                join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                                                                                                join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                                                                                join am in context.AppModules on bpt.AppModule equals am.ID
                                                                                                where
                                                                                                    asp.Account == a.ID &&
                                                                                                    am.Key == (int) GMG.CoZone.Common.AppModule .Collaborate &&
                                                                                                    (!bp.IsDemo ||
                                                                                                    (bp.IsDemo &&
                                                                                                        DbFunctions.DiffDays(
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                asp
                                                                                                                    .AccountPlanActivationDate),
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                DateTime.UtcNow)) <
                                                                                                        GMGColorConfiguration
                                                                                                            .DEMOBILLINGPLANACTIVEDAYS))
                                                                                                select asp.ID).Take(1).Any()
                                                                                                : (
                                                                                                    from spi in context.SubscriberPlanInfoes
                                                                                                    join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                                                                                                    join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                                                                                    join am in context.AppModules on bpt.AppModule equals am.ID
                                                                                                    where spi.Account == a.ID &&
                                                                                                        am.Key == (int)
                                                                                                        GMG.CoZone.Common.AppModule.Collaborate
                                                                                                        &&
                                                                                                        (!bp.IsDemo ||
                                                                                                        (bp.IsDemo &&
                                                                                                        DbFunctions.DiffDays(
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                spi
                                                                                                                    .AccountPlanActivationDate),
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                DateTime.UtcNow)) <
                                                                                                        GMGColorConfiguration
                                                                                                            .DEMOBILLINGPLANACTIVEDAYS))
                                                                                                    select spi.ID).Take(1).Any()

                                                      where
                                                          u.Guid == userGuid && a.IsSuspendedOrDeletedByParent == false && hasSubscription && accs.Key == "A"
                                                      select new AccountResponse
                                                      {
                                                          ID = a.ID,
                                                          AccountDateFormat = df.Pattern,
                                                          AccountTimeFormat = tf.Pattern,
                                                          ProofStudioBackgroundColor = null,
                                                          ProofStudioPenWidth = ProofStudioPenWidthDefaultValue
                                                      }).FirstOrDefault())
                                                  : ((from a in context.Accounts
                                                      join accs in context.AccountStatus on a.Status equals accs.ID
                                                      join acct in context.AccountTypes on a.AccountType equals acct.ID
                                                      join ec in context.ExternalCollaborators on a.ID equals ec.Account
                                                      join df in context.DateFormats on a.DateFormat equals df.ID
                                                      join tf in context.TimeFormats on a.TimeFormat equals tf.ID
                                                      let hasSubscription = acct.Key != "SBSC" ? (
                                                                                                from asp in
                                                                                                    context.AccountSubscriptionPlans
                                                                                                join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                                                                                                join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                                                                                join am in context.AppModules on bpt.AppModule equals am.ID
                                                                                                where
                                                                                                    asp.Account == a.ID &&
                                                                                                    am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate &&
                                                                                                    (!bp.IsDemo ||
                                                                                                    (bp.IsDemo &&
                                                                                                        DbFunctions.DiffDays(
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                asp
                                                                                                                    .AccountPlanActivationDate),
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                DateTime.UtcNow)) <
                                                                                                        GMGColorConfiguration
                                                                                                            .DEMOBILLINGPLANACTIVEDAYS))
                                                                                                select asp.ID).Take(1).Any()
                                                                                                : (
                                                                                                    from spi in context.SubscriberPlanInfoes
                                                                                                    join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                                                                                                    join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                                                                                    join am in context.AppModules on bpt.AppModule equals am.ID
                                                                                                    where spi.Account == a.ID &&
                                                                                                        am.Key == (int)
                                                                                                        GMG.CoZone.Common.AppModule.Collaborate
                                                                                                        &&
                                                                                                        (!bp.IsDemo ||
                                                                                                        (bp.IsDemo &&
                                                                                                        DbFunctions.DiffDays(
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                spi
                                                                                                                    .AccountPlanActivationDate),
                                                                                                            DbFunctions.TruncateTime(
                                                                                                                DateTime.UtcNow)) <
                                                                                                        GMGColorConfiguration
                                                                                                            .DEMOBILLINGPLANACTIVEDAYS))
                                                                                                    select spi.ID).Take(1).Any()
                                                      where
                                                          ec.Guid == userGuid && a.IsSuspendedOrDeletedByParent == false && hasSubscription && accs.Key == "A"
                                                      select new AccountResponse
                                                      {
                                                          ID = a.ID,
                                                          AccountDateFormat = df.Pattern,
                                                          AccountTimeFormat = tf.Pattern,
                                                          ProofStudioBackgroundColor = null,
                                                          ProofStudioPenWidth = ProofStudioPenWidthDefaultValue
                                                      }).FirstOrDefault());

                    if (account != null)
                    {
                        var setting =
                            AccountSettingsBL.GetSetting(
                                AccountSettingsBL.AccountSettingsKeyEnum.ProofStudioBackgroundColor, context, account.ID);
                        if (setting != null)
                        {
                            account.ProofStudioBackgroundColor = setting.Value;
                        }

                        var penSetting =
                            AccountSettingsBL.GetSetting(AccountSettingsBL.AccountSettingsKeyEnum.ProofStudioPenWidth,
                                                         context, account.ID);
                        if (penSetting != null)
                        {
                            account.ProofStudioPenWidth = Int32.Parse(penSetting.Value);
                        }

                        account.AvailableFeatures = ApprovalBL.GetPlanTypeOptions(account.ID, AppModule.Collaborate,
                                                                                  context);
                    }

                    return account;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAccount, ex);
            }
        }

        public static CollaboratorRolesResponse GetCollaboratorRoles()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    CollaboratorRolesResponse result = new CollaboratorRolesResponse();
                    result.collaborator_roles = (from cr in context.ApprovalCollaboratorRoles
                                                 select
                                                     new CollaboratorRole()
                                                     {
                                                         ID = cr.ID,
                                                         Key = cr.Key,
                                                         Name = cr.Name,
                                                         Priority = cr.Priority.HasValue ? cr.Priority.Value : 0
                                                     }).ToList();

                    foreach (CollaboratorRole role in result.collaborator_roles)
                    {
                        role.Name =
                            ApprovalCollaboratorRole.GetLocalizedRoleName(ApprovalCollaboratorRole.GetRole(role.Key));
                    }
                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetCollaboratorRoles, ex);
            }
        }

        public static AnnotationStatusResponse GetAnnotationStatuses()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    AnnotationStatusResponse result = new AnnotationStatusResponse();
                    result.annotation_statuses = (from cr in context.ApprovalAnnotationStatus
                                                  select
                                                      new AnnotationStatus()
                                                      {
                                                          ID = cr.ID,
                                                          Key = cr.Key,
                                                          Name = cr.Name,
                                                          Priority = cr.Priority.HasValue ? cr.Priority.Value : 0
                                                      }).ToList();
                    foreach (AnnotationStatus status in result.annotation_statuses)
                    {
                        status.Name =
                            GMGColorDAL.ApprovalAnnotationStatu.GetLocalizedApprovalAnnotationStatus(status.Key);
                    }
                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAnnotationStatuses, ex);
            }
        }

        public static DecisionsResponse GetDecisions(string user_key, bool is_external_user)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    DecisionsResponse result = new DecisionsResponse();
                    result.decisions = (from ad in context.ApprovalDecisions
                                        let isInactive = is_external_user ?
                                            (from ias in context.InactiveApprovalStatuses
                                             join ac in context.Accounts on ias.Account equals ac.ID
                                             join ec in context.ExternalCollaborators on ac.ID equals ec.Account
                                             where ad.ID == ias.ApprovalDecision && ec.Guid == user_key
                                             select ias.ID).Any()
                                        :
                                                          (from ias in context.InactiveApprovalStatuses
                                                           join ac in context.Accounts on ias.Account equals  ac.ID
                                                           join u in context.Users on ac.ID equals u.Account
                                                           where ad.ID == ias.ApprovalDecision && u.Guid == user_key
                                                           select ias.ID).Any()
                                        where !isInactive
                                        select
                                            new Decision()
                                            {
                                                ID = ad.ID,
                                                Key = ad.Key,
                                                Name = ad.Name,
                                                Priority = ad.Priority.HasValue ? ad.Priority.Value : 0
                                            }).ToList();

                    var customDecisions = is_external_user
                                              ? (from acd in context.ApprovalCustomDecisions
                                                 join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                 join ec in context.ExternalCollaborators on acd.Account equals ec.Account
                                                 where ec.Guid.ToLower() == user_key && acd.Locale == ec.Locale
                                                 select new
                                                 {
                                                     acd.Name,
                                                     ad.Key
                                                 }).ToList()
                                              : (from acd in context.ApprovalCustomDecisions
                                                 join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                 join u in context.Users on acd.Account equals u.Account
                                                 where u.Guid.ToLower() == user_key && acd.Locale == u.Locale
                                                 select new
                                                 {
                                                     acd.Name,
                                                     ad.Key
                                                 }).ToList();


                    foreach (Decision decision in result.decisions)
                    {
                        decision.Name = customDecisions.Any(d => d.Key == decision.Key && !string.IsNullOrEmpty(d.Name))
                                            ? customDecisions.Where(d => d.Key == decision.Key)
                                                             .Select(d => d.Name)
                                                             .FirstOrDefault()
                                            : ApprovalDecision.GetLocalizedApprovalDecision(
                                                ApprovalDecision.GetApprovalDecision(decision.Key));
                    }

                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetDecisions, ex);
            }
        }

        public static Decision GetDecisionById(int decisionId)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    Decision result = new Decision();
                    result = (from ad in context.ApprovalDecisions
                              where ad.ID == decisionId
                              select
                                  new Decision()
                                  {
                                      ID = ad.ID,
                                      Key = ad.Key,
                                      Name = ad.Name,
                                      Priority = ad.Priority.HasValue ? ad.Priority.Value : 0
                                  }).FirstOrDefault();
                    if (result != null)
                    {
                        result.Name = ApprovalDecision.GetLocalizedApprovalDecision(
                            ApprovalDecision.GetApprovalDecision(result.Key));
                    }
                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetDecisionById, ex);
            }
        }

        public static List<ProjectFolderDetails> GetProjectFolderDetails(string user_key, bool is_external_user)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    var domain = (from a in context.Accounts
                                       join u in context.Users on a.ID equals u.Account
                                       where u.Guid == user_key
                                       select a.Domain ).FirstOrDefault();

                    var projectIds = (from pf in context.ProjectFolders
                                      join j in context.Jobs on pf.ID equals j.ProjectFolder
                             join pc in context.ProjectFolderCollaborators on pf.ID equals pc.ProjectFolder
                             join u in context.Users on pc.Collaborator equals u.ID
                             join ac in context.Accounts on pf.Account equals ac.ID
                             where u.Guid == user_key && j.ProjectFolder != null
                             select pf.ID).ToList();

                    var ProjectFolderslist = (from pf in context.ProjectFolders
                                              let projectFolderApprovals = (from j in context.Jobs
                                                                            join a in context.Approvals on j.ID equals a.Job
                                                                            join ac in context.Accounts on j.Account equals ac.ID
                                                                            join ft in context.FileTypes on a.FileType equals ft.ID
                                                                            where j.ProjectFolder == pf.ID && a.IsDeleted == false && a.DeletePermanently == false
                                                                            select new ProjectFolderApprovalDetails
                                                                            {
                                                                                ID = a.ID,
                                                                                ApprovalName = a.FileName,
                                                                                ApprovalThumbnailPath = string.Empty
                                                                            }).ToList()
                                                                            where projectIds.Contains(pf.ID)
                                                                            select new ProjectFolderDetails
                                                                            {
                                                                                ID = pf.ID,
                                                                                ProjectFolderName = pf.Name,
                                                                                projectfolderapproval = projectFolderApprovals
                                                                            }).ToList();


                    foreach(var p in ProjectFolderslist)
                    {
                        foreach(var approval in p.projectfolderapproval )
                        { 
                            var filetype = (from a in context.Approvals
                                                join ft in context.FileTypes on a.FileType equals ft.ID
                                                join atc in context.ApprovalTypeColours on ft.ApprovalTypeColour equals atc.ID
                                                where a.ID == approval.ID
                                            select atc.Name).FirstOrDefault();
                            if(filetype == "Video")
                            {
                                approval.ApprovalThumbnailPath = GMGColorDAL.Approval.GetVideoImagePath(approval.ID, domain , context);
                            }
                            else
                            {
                                if (filetype == "HTML")
                                {
                                    approval.ApprovalThumbnailPath = GMGColorDAL.Approval.GetThumbnailZipPath(approval.ID, domain, context);
                                }
                                else
                                {
                                    approval.ApprovalThumbnailPath = GMGColorDAL.Approval.GetThumbnailImagePath(approval.ID, context);
                                }
                            }
                            approval.ApprovalType = filetype;        
                        }
                    }

                    return ProjectFolderslist;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetProjectFolders, ex);
            }
        }

        public static ApprovalsFilterModel GetTagwordsBasedOnSelectedApprovalType(string selectedApprovalTypes, string selectedTagwords, int projectID)
        {
            try
            {
                var model = new ApprovalsFilterModel();
                var ApprovalTypes = selectedApprovalTypes.Split(',').ToList();
                var Tagwords = selectedTagwords.Split(',').ToList();
                if (Tagwords.Count == 1 && Tagwords[0] == "")
                {
                    Tagwords.RemoveAt(0);
                }
                if (ApprovalTypes.Count == 1 && ApprovalTypes[0] == "")
                {
                    ApprovalTypes.RemoveAt(0);
                }
                var count =  PopulateApprovalsCount(Tagwords, projectID);
                model.NoOfImageApprovals = count.NoOfImageApprovals;
                model.NoOfVideoApprovals = count.NoOfVideoApprovals;
                model.NoOfDocumentApprovals = count.NoOfDocumentApprovals;
                model.NoOfHTMLApprovals = count.NoOfHTMLApprovals;

                if (ApprovalTypes.Count == 1 && ApprovalTypes[0] == "")
                {
                    ApprovalTypes.RemoveAt(0);
                }
                    model.tagwords = GetTagwordsForProjectFolder(projectID);
                return model;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetProjectFolderTagwordsAndCount, ex);
            }
        }


        public static List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount> GetTagwordsForProjectFolder(int projectID)
        {
            try
            {
                var model = new ApprovalsTypeCount();
                using (DbContextBL context = new DbContextBL())
                {
                    var a1 = (from ptw in context.PredefinedTagwords
                              join pfs in context.ProjectFolderTagwords on ptw.ID equals pfs.PredefinedTagwords
                              let count = (from pf in context.ProjectFolders
                                           join pft in context.ProjectFolderTagwords on pf.ID equals pft.ProjectFolder
                                           join pdt in context.PredefinedTagwords on pft.PredefinedTagwords equals pdt.ID
                                           where pf.IsDeleted == false
                                           select pf.ID).Count()
                              where pfs.ProjectFolder == projectID && ptw.ID == pfs.PredefinedTagwords
                              select new GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount
                              {
                                  Id = ptw.ID,
                                  tagword = ptw.Name,
                                  count = count
                              }).ToList();

                    if (a1.Count() > 20)
                    {
                        a1 = a1.GetRange(0, 20);
                    }
                    return a1;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetProjectFolderTagwordsAndCount, ex);
            }
        }
        public static ApprovalsTypeCount PopulateApprovalsCount(List<string> selectedTagwords, int projectID)
        {
            try
            {
                var model = new ApprovalsTypeCount();
                using (DbContextBL context = new DbContextBL())
                {
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join pf in context.ProjectFolders on j.ProjectFolder equals pf.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && pf.ID == projectID && //j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) //&&
                                                                    select j.ID).Distinct().Count()
                                              select new GMGColorDAL.Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                            var approvalCounts = (from atc in context.ApprovalTypeColours
                                                  let approval_Count = (from app in context.ApprovalTypeColours
                                                                        join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                        join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                        join j in context.Jobs on aprl.Job equals j.ID
                                                                        join pf in context.ProjectFolders on j.ProjectFolder equals pf.ID
                                                                        join js in context.JobStatus on j.Status equals js.ID
                                                                        where aprl.ApprovalTypeColour == atc.ID && pf.ID == projectID &&//j.Account == loggedAccountId &&
                                                                        aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" //&&
                                                                        select j.ID).Distinct().Count()
                                                  select new GMGColorDAL.Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                }
                return model;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetProjectFolderTagwordsAndCount, ex);
            }
        }

        public static ShapesResponse GetShapes()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    ShapesResponse result = new ShapesResponse();
                    result.shapes = (from sh in context.Shapes
                                     where sh.IsCustom == false
                                     select
                                         new Shape()
                                         {
                                             ID = sh.ID,
                                             IsCustom = sh.IsCustom,
                                             SVG = sh.SVG
                                         }).ToList();
                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetShapes, ex);
            }
        }

        public static RolesResponse GetRoles()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    RolesResponse result = new RolesResponse();
                    result.roles = (from r in context.Roles
                                    join am in context.AppModules on r.AppModule equals am.ID
                                    where am.Key == (int) GMG.CoZone.Common.AppModule.Collaborate
                                    select
                                        new Role()
                                        {
                                            ID = r.ID,
                                            Name = r.Name,
                                            Key = r.Key,
                                            Priority = r.Priority.HasValue ? r.Priority.Value : 0
                                        }).ToList();
                    foreach (Role role in result.roles)
                    {
                        role.Name = GMGColorDAL.Role.GetLocalizedRoleName(role.Key);
                    }
                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetRoles, ex);
            }
        }

        public static CommentsResponse GetComments()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    CommentsResponse result = new CommentsResponse();
                    result.comment_types = (from r in context.ApprovalCommentTypes
                                            select
                                                new Comment()
                                                {
                                                    ID = r.ID,
                                                    Name = r.Name,
                                                    Key = r.Key,
                                                    Priority = r.Priority.HasValue ? r.Priority.Value : 0
                                                }).ToList();
                    foreach (Comment comment in result.comment_types)
                    {
                        comment.Name =
                            ApprovalCommentType.GetLocalizedApprovalCommentType(
                                ApprovalCommentType.GetApprovalCommentType(comment.Key));
                    }
                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetCommentsType, ex);
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call GetApproval from Proofstudio service instead.", false)]
        public static ApprovalResponse GetApproval(int? approvalId, string userKey, bool isExternalUser,
                                                   string allCollReqSettingName)
        {
            try
            {
                if (!approvalId.HasValue)
                {
                    return null;
                }

                using (DbContextBL context = new DbContextBL())
                {
                    bool userIsAdmin = ApiBL.UserIsAdministrator(context, userKey);

                    var watch1 = System.Diagnostics.Stopwatch.StartNew();

                    string timezone = (from aj in context.Approvals
                                       join j in context.Jobs on aj.Job equals j.ID
                                       join a in context.Accounts on j.Account equals a.ID
                                       where aj.ID == approvalId && aj.IsDeleted == false
                                       select a.TimeZone).Take(1).FirstOrDefault();
                    watch1.Stop();
                    GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load timeZone Execution Time: " + watch1.ElapsedMilliseconds);

                    watch1 = System.Diagnostics.Stopwatch.StartNew();

                    #region Get Approval


                    var approval = !isExternalUser
                                       ? (from a in context.Approvals
                                          join at in context.ApprovalTypes on a.Type equals at.ID
                                          join j in context.Jobs on a.Job equals j.ID
                                          join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                          join u in context.Users on ac.Collaborator equals u.ID
                                          join us in context.UserStatus on u.Status equals us.ID
                                          where
                                              a.ID == approvalId.Value &&
                                              a.IsDeleted == false &&
                                              ((u.Guid.ToLower() == userKey.ToLower() && us.Key == "A") || userIsAdmin)
                                          orderby a.Version ascending
                                          select new ApprovalInternalModel
                                          {
                                              ID = a.ID,
                                              ErrorMessage = a.ErrorMessage,
                                              Creator = a.Creator,
                                              ApprovalTypeName = at.Name,
                                              Title = j.Title,
                                              CurrentPhase = a.CurrentPhase,
                                              Account = j.Account,
                                              Job = j.ID,
                                          }).Take(1).FirstOrDefault()
                                       : (from a in context.Approvals
                                          join at in context.ApprovalTypes on a.Type equals at.ID
                                          join j in context.Jobs on a.Job equals j.ID
                                          join sa in context.SharedApprovals on a.ID equals sa.Approval
                                          join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                          where
                                              a.ID == approvalId.Value &&
                                              a.IsDeleted == false &&
                                              ec.Guid.ToLower() == userKey.ToLower() && !ec.IsDeleted && !sa.IsExpired
                                          orderby a.Version ascending
                                          select new ApprovalInternalModel
                                          {
                                              ID = a.ID,
                                              ErrorMessage = a.ErrorMessage,
                                              Creator = a.Creator,
                                              ApprovalTypeName = at.Name,
                                              Title = j.Title,
                                              CurrentPhase = a.CurrentPhase,
                                              Account = j.Account,
                                              Job = j.ID,
                                          }).Take(1).FirstOrDefault();

                    watch1.Stop();
                    GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load approval Execution Time: " + watch1.ElapsedMilliseconds);

                    watch1 = System.Diagnostics.Stopwatch.StartNew();

                    if (approval != null)
                    {
                        ApprovalResponse result = new ApprovalResponse();
                        result.approvals.Add(new GMGColorDAL.CustomModels.Api.Approval
                        {
                            ID = approval.ID,
                            ErrorMessage = approval.ErrorMessage,
                            Title = approval.Title,
                            ApprovalType = approval.ApprovalTypeName,
                            Creator = approval.Creator,
                        });

                        #region Get Versions

                        result.versions = GetVersions(approval, userKey, isExternalUser, userIsAdmin, context);
                        result.approvals[0].Versions = result.versions.Select(v => v.ID).ToArray();

                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load versions Execution Time: " + watch1.ElapsedMilliseconds);

                        watch1 = System.Diagnostics.Stopwatch.StartNew();

                        var maxVersion = new []{approval.ID};
                        #endregion

                        result.user_groups = GetJobPrimayGroups(context, approval.Job, userKey, isExternalUser);
                        result.user_group_links = GetJobPrimaryGroupDecisions(context, userKey, approval.Job);

                        var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
                        var userCanViewAllPhasesAnnotations = approval.CurrentPhase != null && (!isExternalUser ? (from us in context.UserSettings
                                                                                                                   join u in context.Users on us.User equals u.ID
                                                                                                                   where (u.Guid.ToLower() == userKey.ToLower()) && us.Setting == showAnnotationsFromAllPhases
                                                                                                                   select us.Value.ToLower() == "true").FirstOrDefault()
                                                                                                                          :
                                                                                                                          (from ec in context.ExternalCollaborators
                                                                                                                           where (ec.Guid.ToLower() == userKey.ToLower())
                                                                                                                           select ec.ShowAnnotationsOfAllPhases).FirstOrDefault());
                        var visiblePhases = new List<int>();
                        if (approval.CurrentPhase != null)
                        {
                            visiblePhases = !isExternalUser
                                                ? (from aph in context.ApprovalJobPhases
                                                   join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                   where ajpa.Approval == approval.ID && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == approval.CurrentPhase)
                                                   select aph.ID).ToList()
                                                : (from aph in context.ApprovalJobPhases
                                                   join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                   where ajpa.Approval == approval.ID && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == approval.CurrentPhase)
                                                   select aph.ID).ToList();
                        }

                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load groups and visible phases Execution Time: " + watch1.ElapsedMilliseconds);

                        watch1 = System.Diagnostics.Stopwatch.StartNew();

                        result.internal_collaborators = GetCollaborators(context, maxVersion, false, timezone, userCanViewAllPhasesAnnotations, visiblePhases);
                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load internal colaborators Execution Time: " + watch1.ElapsedMilliseconds);
                        watch1 = System.Diagnostics.Stopwatch.StartNew();

                        result.internal_collaborators_links = GetCollaboratorLinks(context, maxVersion, false, timezone, userCanViewAllPhasesAnnotations, visiblePhases);
                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load internal collaborators links Execution Time: " + watch1.ElapsedMilliseconds);
                        watch1 = System.Diagnostics.Stopwatch.StartNew();

                        result.external_collaborators = GetCollaborators(context, maxVersion, true, timezone, userCanViewAllPhasesAnnotations, visiblePhases);
                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load external collaborators Execution Time: " + watch1.ElapsedMilliseconds);
                        watch1 = System.Diagnostics.Stopwatch.StartNew();
                        result.external_collaborators_links = GetCollaboratorLinks(context, maxVersion, true, timezone, userCanViewAllPhasesAnnotations, visiblePhases);
                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load external collaborators links Execution Time: " + watch1.ElapsedMilliseconds);
                        watch1 = System.Diagnostics.Stopwatch.StartNew();

                        result.softProofingParams = GetDefaultSoftProofingParams(context, maxVersion[0], approval.Job, !result.versions.FirstOrDefault(t => t.ID == approval.ID).IsRGBColorSpace);

                        watch1.Stop();
                        GMGColorLogging.log.InfoFormat("ApiBL.GetApproval load default soft proofing params Execution Time: " + watch1.ElapsedMilliseconds);

                        return result;
                    }

                    #endregion

                    return null;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetApproval, ex);
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call GetApprovals from ProofstudioAPIService instead.", false)]
        public static ApprovalResponse GetApprovals(ApprovalRequest model, string allCollReqSettingName)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            string[] approvalIds = model.approvalsIds.Split(',');
            ApprovalResponse approval = new ApprovalResponse();
            int noOfVersions = 0;

            foreach (var approvalId in approvalIds)
            {
                var watch1 = System.Diagnostics.Stopwatch.StartNew();
                ApprovalResponse approval1 = ApiBL.GetApproval(Convert.ToInt32(approvalId), model.user_key, model.is_external_user, allCollReqSettingName);
                approval.approvals.Add(approval1.approvals[0]);
                watch1.Stop();
                GMGColorLogging.log.InfoFormat("ApiBL.GetApprovals load approval Execution Time: " + watch1.ElapsedMilliseconds);

                var watch2 = System.Diagnostics.Stopwatch.StartNew();
                foreach (var version in approval1.versions)
                {
                    // version parent is not loaded all the times and it is required in ProofStudion
                    version.Parent = approval1.approvals[0].ID;
                }
                approval.versions.AddRange(approval1.versions);
                approval.external_collaborators.AddRange(approval1.external_collaborators.Where(x => approval.external_collaborators.All(y => y.ID != x.ID)));
                approval.external_collaborators_links.AddRange(approval1.external_collaborators_links.Where(x => approval.external_collaborators_links.All(y => y.ID != x.ID)));
                approval.internal_collaborators.AddRange(approval1.internal_collaborators.Where(x => approval.internal_collaborators.All(y => y.ID != x.ID)));
                approval.internal_collaborators_links.AddRange(approval1.internal_collaborators_links.Where(x => approval.internal_collaborators_links.All(y => y.ID != x.ID)));
                approval.user_groups.AddRange(approval1.user_groups.Where(x => approval.user_groups.All(y => y.ID != x.ID)));
                approval.user_group_links.AddRange(approval1.user_group_links.Where(x => approval.user_group_links.All(y => y.ID != x.ID)));
                approval.softProofingParams.AddRange(approval1.softProofingParams.Where(x => approval.softProofingParams.All(y => y.ID != x.ID)));

                watch2.Stop();
                GMGColorLogging.log.InfoFormat("ApiBL.GetApprovals load versions after approval Execution Time: " + watch2.ElapsedMilliseconds);

            }

            // set the order in which the versions are seen in dropdown list (the versions are displayed in ascending order)
            for (int i = 0; i < approvalIds.Length; i++)
            {
                var versions =
                    approval.versions.Where(v => v.Parent == approval.approvals[i].ID)
                        .OrderByDescending(v => v.VersionNumber)
                        .ToList();
                for (int j = 0; j < versions.Count(); j++)
                {
                    versions[j].OrderInDropDownMenu = noOfVersions++;
                }
            }

            watch.Stop();

            GMGColorLogging.log.InfoFormat("ApiBL.GetApprovals Execution Time: " + watch.ElapsedMilliseconds);

            return approval;
        }

        [ObsoleteAttribute("This method is obsolete. Call GetVersions from collaborate module instead.", false)]
        private static List<ApprovalVersion> GetVersions(ApprovalInternalModel approval, string userKey, bool isExternalUser, bool userIsAdmin, DbContextBL context)
        {
            var versionsList = new List<ApprovalVersion>();
            try
            {
                if (isExternalUser)
                {
                    // return only current version
                    var versions = (from aap in context.Approvals
                                    join at in context.ApprovalTypes on aap.Type equals at.ID
                                    join sa in context.SharedApprovals on aap.ID equals sa.Approval
                                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                    join acc in context.Accounts on ec.Account equals acc.ID

                                    where
                                        aap.Job == approval.Job && aap.IsDeleted == false && !aap.DeletePermanently &&
                                        ec.Guid.ToLower() == userKey.ToLower() && sa.IsExpired == false &&
                                        ec.IsDeleted == false && aap.CurrentPhase == approval.CurrentPhase && aap.ID != approval.ID
                                    select new
                                    {
                                        ID = aap.ID,
                                        Title = aap.FileName,
                                        aap.Guid,
                                        AccountRegion = acc.Region,
                                        AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                        VersionNumber = aap.Version,
                                        IsVideo = at.Key == (int)GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                                        VersionLabel = aap.VersionSufix,
                                    }).GroupBy(x => x.ID, (key, g) => g.FirstOrDefault()).ToList();

                    versionsList.AddRange(versions.Select(version => new ApprovalVersion()
                    {
                        ID = version.ID,
                        Title = version.Title,
                        VersionNumber = version.VersionNumber,
                        VersionLabel = ConstructVersionLabel(version.VersionLabel, version.VersionNumber),
                        PageVersionLabel = ConstructPageVersionLabel(version.Title, version.VersionNumber),
                        VersionThumbnail = version.IsVideo ? GMGColorDAL.Approval.GetVideoImagePath(version.Guid, version.AccountRegion, version.AccountDomain) : GMGColorDAL.Approval.GetThumbnailImagePath(version.AccountDomain, version.AccountRegion, version.Guid, "1"),
                    }));
                }
                else
                {
                    bool isAdministrator = userIsAdmin && IsShowAllFilesToAdminsChecked(context, approval.Account);

                    var versions = (from aap in context.Approvals
                                    join at in context.ApprovalTypes on aap.Type equals at.ID
                                    join ac in context.ApprovalCollaborators on aap.ID equals ac.Approval
                                    join u in context.Users on ac.Collaborator equals u.ID
                                    join acc in context.Accounts on u.Account equals acc.ID

                                    where
                                        aap.Job == approval.Job && aap.IsDeleted == false && !aap.DeletePermanently &&
                                        (isAdministrator || u.Guid.ToLower() == userKey.ToLower()) &&
                                        aap.CurrentPhase == approval.CurrentPhase && aap.ID != approval.ID
                                    select new
                                    {
                                        ID = aap.ID,
                                        Title = aap.FileName,
                                        aap.Guid,
                                        AccountRegion = acc.Region,
                                        AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                        VersionNumber = aap.Version,
                                        IsVideo = at.Key == (int) GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                                        VersionLabel = aap.VersionSufix
                                    }).GroupBy(x => x.ID, (key, g) => g.FirstOrDefault()).ToList();

                    versionsList.AddRange(versions.Select(version => new ApprovalVersion()
                    {
                        ID = version.ID,
                        Title = version.Title,
                        VersionNumber = version.VersionNumber,
                        VersionLabel = ConstructVersionLabel(version.VersionLabel, version.VersionNumber),
                        PageVersionLabel = ConstructPageVersionLabel(version.Title, version.VersionNumber),
                        VersionThumbnail = version.IsVideo ? GMGColorDAL.Approval.GetVideoImagePath(version.Guid, version.AccountRegion, version.AccountDomain) : GMGColorDAL.Approval.GetThumbnailImagePath(version.AccountDomain, version.AccountRegion, version.Guid, "1")
                    }));

                }

                versionsList.Add(GetVersion(context, approval.ID, userKey, isExternalUser));
            }
            catch (Exception)
            {
                throw;
            }
            return versionsList;
        }

        [ObsoleteAttribute("This method is obsolete. Call ConstructVersionLabel from collaborate module instead.", false)]
        private static string ConstructVersionLabel(string versionLabel, int versionNumber)
        {
            return versionLabel != string.Empty && versionLabel != null
                ? versionLabel
                : "v" + versionNumber;
        }

        [ObsoleteAttribute("This method is obsolete. Call ConstructPageVersionLabel from collaborate module instead.", false)]
        private static string ConstructPageVersionLabel(string title, int versionNumber)
        {
            return "<span class='versionstyle'>v" + versionNumber + "</span>" + "<span class='versiontitlestyle'>" + title + "</span>";
        }

        private static List<ApprovalAnnotationAttachment> GetAttachments(DbContextBL context, IEnumerable<int> attachmentsIds)
        {
            try
            {
                return (from aaa in context.ApprovalAnnotationAttachments
                        where attachmentsIds.Contains(aaa.ID)
                        select new ApprovalAnnotationAttachment
                        {
                            DisplayName = aaa.DisplayName,
                            Name = aaa.Name,
                            Guid = aaa.Guid,
                            ID = aaa.ID,
                            ApprovalAnnotation = aaa.ApprovalAnnotation,
                        }).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAttachments, ex);
            }
        }


        private static List<ApprovalAnnotationChecklistItemDetail> GetApprovalAnnotationChecklistItemsHistory(DbContextBL context, IEnumerable<int> checklistItemHistory, IEnumerable<int> checklistitems, string pattern, string timezone)
        {
            try
            {
          var checklistItemHistories = (from anch in context.ApprovalAnnotationChecklistItemHistories
                        join ci in context.CheckListItem on anch.ChecklistItem equals ci.ID
                let creator = (from u in context.Users where u.ID == anch.Creator select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                let externalCreator = (from ec in context.ExternalCollaborators where ec.ID == anch.ExternalCreator select ec.GivenName +  " " + ec.FamilyName).FirstOrDefault()
                        where checklistItemHistory.Contains(anch.ID)
                        select new ApprovalAnnotationChecklistItemDetail
                        { 
                            ID = anch.ID,
                            ChecklistItemID = anch.ChecklistItem,
                            ChecklistItemName = ci.Name,
                            ChecklistItemValue = anch.TextValue,
                            CreatedDate = anch.CreatedDate.ToString(),
                            Creator = creator == null ? externalCreator : creator
                        }).ToList();

                var checklistItems = (from anci in context.ApprovalAnnotationCheckListItems
                                      join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                      join an in context.ApprovalAnnotations on anci.ApprovalAnnotation equals an.ID
                                      let creator = (from u in context.Users where u.ID == anci.Creator select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                      let externalCreator = (from ec in context.ExternalCollaborators where ec.ID == anci.ExternalCreator select ec.GivenName + " " + ec.FamilyName).FirstOrDefault()
                                      where checklistitems.Contains(anci.ID)
                                      select new ApprovalAnnotationChecklistItemDetail
                                      {
                                          ID = anci.ID,
                                          ChecklistItemID = anci.CheckListItem,
                                          ChecklistItemName = ci.Name,
                                          ChecklistItemValue = anci.TextValue,
                                          CreatedDate = anci.CreatedDate == null ? an.AnnotatedDate.ToString() : anci.CreatedDate.ToString(),
                                          Creator = creator == null ? externalCreator : creator
                                      }).ToList();
               
                checklistItemHistories.AddRange(checklistItems);
               
                foreach (var item in checklistItemHistories)
                {
                    var date = Convert.ToDateTime(item.CreatedDate);
                    item.CreatedDate = GMGColorFormatData.GetFormattedDate(date, pattern) + " " +GMGColorFormatData.GetUserTimeFromUTC(date, timezone).ToString("h:mm tt");
                }
                return checklistItemHistories;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAttachments, ex);
            }
        }

        private static List<ApprovalChecklistItem> GetChecklistItems(DbContextBL context, IEnumerable<int> checklitsitemIds)
        {
            try
            {
                return(from aci in context.ApprovalAnnotationCheckListItems
                        join ci in context.CheckListItem on aci.CheckListItem equals ci.ID
                        where checklitsitemIds.Contains(aci.ID)
                        select new ApprovalChecklistItem
                        {
                            Id = aci.CheckListItem,
                            Item = ci.Name,
                            IsItemChecked = true,
                            ItemValue = aci.TextValue,
                            ID = aci.ID,
                            TotalChanges = aci.TotalChanges == null ? 1 : aci.TotalChanges,
                            ShowNoOfChanges = true
                        }).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAttachments, ex);
            }
        }

        private static List<ViewingConditionApiModel> GetViewingConditions(DbContextBL context, IEnumerable<int> viewingConditionsIds)
        {
            try
            {
                return (from vwc in context.ViewingConditions
                        where viewingConditionsIds.Contains(vwc.ID)
                        select new ViewingConditionApiModel
                        {
                            SimulationProfileId = vwc.SimulationProfile,
                            EmbeddedProfileId = vwc.EmbeddedProfile,
                            CustomProfileId = vwc.CustomProfile,
                            ID = vwc.ID,
                            PaperTintId = vwc.PaperTint
                        }).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAttachments, ex);
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call GetJobPrimaryGroupDecisions from collaborate module instead.", false)]
        private static List<UserGroupDecision> GetJobPrimaryGroupDecisions(DbContextBL context, string userGuid, int jobId)
        {
            try
            {
                var links = (
                                from a in context.Approvals
                                join acg in context.ApprovalCollaboratorGroups on a.ID equals acg.Approval
                                let isPrimary =
                                    (from ugu in context.UserGroupUsers
                                     where ugu.UserGroup == acg.CollaboratorGroup && ugu.IsPrimary
                                     select ugu.IsPrimary).FirstOrDefault()
                                let decision = (
                                                   from acd in context.ApprovalCollaboratorDecisions
                                                   join ac in context.ApprovalCollaborators on acd.Collaborator equals
                                                       ac.Collaborator
                                                   join u in context.Users on ac.Collaborator equals u.ID
                                                   join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                   join dec in context.ApprovalDecisions on acd.Decision equals dec.ID
                                                   orderby dec.Priority ascending
                                                   where
                                                       acd.Approval == a.ID && ugu.UserGroup == acg.CollaboratorGroup &&
                                                       ugu.IsPrimary && acd.Phase == a.CurrentPhase
                                                   select dec.ID
                                               ).Take(1)
                                let hasAccess = (
                                                    from ac in context.ApprovalCollaborators
                                                    join u in context.Users on ac.Collaborator equals u.ID
                                                    join us in context.UserStatus on u.Status equals us.ID
                                                    where
                                                        a.ID == ac.Approval && u.Guid == userGuid && us.Key == "A" &&
                                                        a.CurrentPhase == ac.Phase
                                                    select ac.ID
                                                ).Any() || (from sa in context.SharedApprovals
                                                            join ec in context.ExternalCollaborators on
                                                                sa.ExternalCollaborator equals ec.ID
                                                            where
                                                                sa.Approval == a.ID && ec.Guid == userGuid &&
                                                                ec.IsDeleted == false && sa.Phase == a.CurrentPhase
                                                            select sa.ID).Any()
                                where a.Job == jobId && isPrimary && hasAccess && acg.Phase == a.CurrentPhase
                                select new UserGroupDecision()
                                {
                                    approval_version = a.ID,
                                    user_group = acg.UserGroup.ID,
                                    decision = decision.Any() ? (int?) decision.FirstOrDefault() : null
                                }
                            ).ToList();

                int index = 1;
                foreach (var link in links)
                {
                    link.ID =(jobId * 10000) + index++;
                }
                return links;

            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserGroupLinks, ex);
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call GetJobPrimayGroups from collaborate module instead.", false)]
        private static List<UserGroup> GetJobPrimayGroups(DbContextBL context, int jobId, string userKey, bool isExternalUser)
        {
            try
            {
                var groups = (from acg in context.ApprovalCollaboratorGroups
                              join ug in context.UserGroups on acg.CollaboratorGroup equals ug.ID
                              join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                              join a in context.Approvals on acg.Approval equals a.ID

                              let ShowAnnotationsOfAllPhases = !isExternalUser ? (from u in context.Users
                                                                                  join uus in context.UserSettings on u.ID equals uus.User
                                                                                  where u.Guid == userKey && uus.Setting == "ShowAnnotationsOfAllPhases"
                                                                                  select uus.Value).FirstOrDefault() : (from ec in context.ExternalCollaborators
                                                                                                                        join uus in context.UserSettings on ec.ID equals uus.User
                                                                                                                        where ec.Guid == userKey && uus.Setting == "ShowAnnotationsOfAllPhases"
                                                                                                                        select uus.Value).FirstOrDefault()

                              where ugu.IsPrimary && a.Job == jobId && (a.CurrentPhase == acg.Phase || ShowAnnotationsOfAllPhases == "True")
                              select new UserGroup
                              {
                                  ID = ug.ID,
                                  Name = ug.Name
                              }).ToList();

                return groups.GroupBy(o => o.ID).Select(o => o.FirstOrDefault()).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserGroups, ex);
            }
        }

        private static ViewingCondition GetViewingConditionIdByParams(TilesPageRequest model, DbContextBL context)
        {
            return (from vwcn in context.ViewingConditions
                    where vwcn.SimulationProfile == model.SimulationProfileId &&
                          vwcn.EmbeddedProfile == model.EmbeddedProfileId &&
                          vwcn.CustomProfile == model.CustomProfileId &&
                          vwcn.PaperTint == model.PaperTintId
                    select vwcn).FirstOrDefault();
        }

        [ObsoleteAttribute("This method is obsolete. Call GetVersion from collaborate module instead.", false)]
        private static ApprovalVersion GetVersion(DbContextBL context, int versionId, string userKey, bool isExternalUser)
        {
            try
            {
                int? parent = (from pa in context.Approvals
                               join j in context.Jobs on pa.Job equals j.ID
                               where pa.ID == versionId && pa.Version == 1
                               select pa.ID).Take
                                (1).FirstOrDefault();

                if (parent == 0)
                {
                    parent = null;
                }

                var approvalVersion = new ApprovalVersion();
                var version =
                            (from a in context.Approvals
                             join at in context.ApprovalTypes on a.Type equals at.ID
                             join j in context.Jobs on a.Job equals j.ID
                             join js in context.JobStatus on j.Status equals js.ID
                             join acc in context.Accounts on j.Account equals acc.ID
                             join ft in context.FileTypes on a.FileType equals ft.ID

                             let totalPagesCount = (from ap in context.ApprovalPages
                                                    where ap.Approval == a.ID
                                                    select ap.ID).Count()

                             let docEmbeddedProfile = (from apep in context.ApprovalEmbeddedProfiles where apep.Approval == a.ID select apep).FirstOrDefault()
                             let firstPageId = (from ap in context.ApprovalPages where ap.Approval == a.ID select ap.ID).Take(1).FirstOrDefault()

                             let phaseName = (from ajp in context.ApprovalJobPhases
                                              where ajp.ID == a.CurrentPhase
                                              select ajp.Name).FirstOrDefault()
                             let phaseCompleted = ( from apja in context.ApprovalJobPhaseApprovals
                                                   where apja.Phase == a.CurrentPhase
                                                   orderby apja.ID descending
                                                   select apja.PhaseMarkedAsCompleted).FirstOrDefault()
                             let shouldBeViewedByAll = (from ajp in context.ApprovalJobPhases
                                                        join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
                                                        where ajp.ID == a.CurrentPhase
                                                        select aph.ApprovalShouldBeViewedByAll).FirstOrDefault()
                             where versionId == a.ID
                             select new
                             {
                                 a.ID,
                                 a.Guid,
                                 a.ErrorMessage,
                                 a.IsDeleted,
                                 IsLocked = a.IsLocked,
                                 a.Creator,
                                 ApprovalTypeName = at.Name,
                                 a.AllowDownloadOriginal,
                                 VersionNumber = a.Version,
                                 a.FileName,
                                 Extention = ft.Extention,
                                 AccountRegion = acc.Region,
                                 ApprovalType = at.Key,
                                 AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                 isVideo = at.Key == (int) GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                                 FirstPageNumber = 1,
                                 TotalPagesCount = totalPagesCount,
                                 JobStatus = js.Key,
                                 IsRGBColorSpace = (docEmbeddedProfile != null ? docEmbeddedProfile.IsRGB : (from asp in context.ApprovalSeparationPlates
                                                                                                             where
                                                                                                                 asp.Page == firstPageId &&
                                                                                                                 (asp.Name.ToLower() == Constants.REDPLATE ||
                                                                                                                  asp.Name.ToLower() == Constants.GREENPLATE ||
                                                                                                                  asp.Name.ToLower() == Constants.BLUEPLATE)
                                                                                                             select asp.ID).Count() == 3),
                                 a.CurrentPhase,
                                 PhaseName = phaseName,
                                 VersionLabel = a.VersionSufix,
                                 IsPhaseCompleted = phaseCompleted,
                                 shouldBeViewedByAll
                             }).FirstOrDefault();

                if(version != null)
                {
                    var decision = ApprovalBL.GetApprovalDecision(version.ID, userKey, isExternalUser, context);
                    ApprovalVersion v = new ApprovalVersion()
                    {
                        ID = version.ID,
                        Creator = version.Creator,
                        ErrorMessage = version.ErrorMessage,
                        Guid = version.Guid,
                        IsDeleted = version.IsDeleted,
                        Parent = parent,
                        Title = version.FileName,
                        VersionNumber = version.VersionNumber,
                        IsLocked = version.IsLocked || (version.CurrentPhase != null && !ApprovalBL.IsLastVersion(versionId, context)),
                        AllowDownloadOriginal = version.AllowDownloadOriginal,
                        Decision = decision,
                        ApprovalType = version.ApprovalTypeName,
                        VersionThumbnail = version.isVideo
                                                            ? GMGColorDAL.Approval.GetVideoImagePath(version.Guid, version.AccountRegion,
                                                                                                     version.AccountDomain)
                                                            : GMGColorDAL.Approval.GetThumbnailImagePath(version.AccountDomain,
                                                                                                         version.AccountRegion, version.Guid,
                                                                                                         version.FirstPageNumber.ToString()),
                        PagesCount = version.TotalPagesCount,
                        JobStatus = version.JobStatus,
                        IsRGBColorSpace = version.IsRGBColorSpace,
                        CurrentPhase = version.CurrentPhase,
                        CurrentPhaseName = version.PhaseName,
                        IsCompleted = version.IsPhaseCompleted,
                        VersionLabel = ConstructVersionLabel(version.VersionLabel, version.VersionNumber),
                        PageVersionLabel = ConstructPageVersionLabel(version.FileName, version.VersionNumber),
                        CurrentPhaseShouldBeViewedByAll = version.shouldBeViewedByAll
                    };

                    var movieApproval = version.ApprovalType == (int) ApprovalType.Video;

                    if (movieApproval)
                    {
                        if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                        {
                            string result1 = Path.ChangeExtension(version.FileName, ".mp4");
                            v.MovieFilePath =
                                GMGColorCommon.GetDataFolderHttpPath(version.AccountRegion, version.AccountDomain) +
                                "approvals/" + version.Guid + "/" + GMGColorConfiguration.CONVERTED + "/" + result1;
                        }
                        else
                        {
                            v.MovieFilePath = GMGColorDAL.Approval.GetOriginalFilePath(version.AccountDomain, version.AccountRegion,
                                                                             version.Guid, version.FileName);
                        }
                    }
                    approvalVersion = v;
                }
                return approvalVersion;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetVersions, ex);
            }
        }
        public static GMG.CoZone.Proofstudio.DomainModels.Responses.ApprovalInitialVersionsResponseModel GetApprovalInitialVersions(List<ApprovalBasicModel> approvals, string userKey, bool isExternalUser)
        {
            var response = new GMG.CoZone.Proofstudio.DomainModels.Responses.ApprovalInitialVersionsResponseModel();
            int noOfVersions = 0;
            using (DbContextBL context = new DbContextBL())
            {
                
                bool userIsAdmin = UserIsAdministrator(context, userKey);
                bool userIsManager = UserIsManager(context, userKey);

                foreach (var approvalBasic in approvals)
                {
                    if (isExternalUser)
                    {
                        var approval = (from a in context.Approvals
                                  join at in context.ApprovalTypes on a.Type equals at.ID
                                  join j in context.Jobs on a.Job equals j.ID
                                  join sa in context.SharedApprovals on a.ID equals sa.Approval
                                  join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                  where
                                      a.ID == approvalBasic.ApprovalId &&
                                      a.IsDeleted == false &&
                                      ec.Guid.ToLower() == userKey.ToLower() && !ec.IsDeleted && !sa.IsExpired
                                  orderby a.Version ascending
                                  select new GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalModel
                                  {
                                      ID = a.ID,
                                      ErrorMessage = a.ErrorMessage,
                                      Creator = a.Creator,
                                      ApprovalType = at.Name,
                                      Title = j.Title,
                                  }).Take(1).FirstOrDefault();

                        response.approvals.Add(approval);

                       var verson =  (from aap in context.Approvals
                         join at in context.ApprovalTypes on aap.Type equals at.ID
                         join sa in context.SharedApprovals on aap.ID equals sa.Approval
                         join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                         join acc in context.Accounts on ec.Account equals acc.ID

                         where
                             aap.Job == approvalBasic.JobId && aap.IsDeleted == false && !aap.DeletePermanently &&
                             ec.Guid.ToLower() == userKey.ToLower() && sa.IsExpired == false &&
                             ec.IsDeleted == false && aap.ID != approvalBasic.ApprovalId
                         select new ApprovalVersionModel
                         {
                             ID = aap.ID,
                             Title = aap.FileName,
                             Guid = aap.Guid,
                             AccountRegion = acc.Region,
                             AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                             VersionNumber = aap.Version,
                             ApprovalType = at.Name,
                             VersionLabel = aap.VersionSufix,
                         }).GroupBy(x => x.ID, (key, g) => g.FirstOrDefault()).ToList();

                        response.versions.AddRange(verson);
                    }
                    else
                    {
                        var loggedUserId = UserBL.GetUserId(userKey, context, false);
                        List<int> collaborators = (from ac in context.ApprovalCollaborators
                                                   where ac.Approval == approvalBasic.ApprovalId
                                                   select ac.Collaborator).ToList();
                        if (userIsAdmin)
                        {
                            collaborators.Add(loggedUserId);
                        }
                        if (userIsManager)
                        {
                            collaborators.Add(loggedUserId);
                        }

                        var approval = (from a in context.Approvals
                                  join at in context.ApprovalTypes on a.Type equals at.ID
                                  join j in context.Jobs on a.Job equals j.ID
                                  where
                                      a.ID == approvalBasic.ApprovalId &&
                                      a.IsDeleted == false &&
                                      (collaborators.Contains(loggedUserId) || j.JobOwner == loggedUserId)
                                  orderby a.Version ascending
                                  select new GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalModel
                                  {
                                      ID = a.ID,
                                      ErrorMessage = a.ErrorMessage,
                                      Creator = a.Creator,
                                      ApprovalType = at.Name,
                                      Title = j.Title,
                                  }).FirstOrDefault();
                        response.approvals.Add(approval);

                      var verson =  (from aap in context.Approvals
                         join at in context.ApprovalTypes on aap.Type equals at.ID
                         join j in context.Jobs on aap.Job equals j.ID
                         join acc in context.Accounts on j.Account equals acc.ID

                         where
                             aap.Job == approvalBasic.JobId && aap.IsDeleted == false && !aap.DeletePermanently &&
                              aap.ID != approvalBasic.ApprovalId
                                 select new ApprovalVersionModel
                                 {
                             ID = aap.ID,
                             Title = aap.FileName,
                             Guid = aap.Guid,
                             AccountRegion = acc.Region,
                             AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                             VersionNumber = aap.Version,
                             ApprovalType = at.Name,
                             VersionLabel = aap.VersionSufix
                         }).GroupBy(x => x.ID, (key, g) => g.FirstOrDefault()).ToList();

                        response.versions.AddRange(verson);

                    }

                    int? parent = (from pa in context.Approvals
                                   join j in context.Jobs on pa.Job equals j.ID
                                   where pa.ID == approvalBasic.ApprovalId && pa.Version == 1
                                   select pa.ID).Take(1).FirstOrDefault();

                    if (parent == 0)
                        parent = null;

                    var decision = isExternalUser ? (from apcd in context.ApprovalCollaboratorDecisions
                                                     join ec in context.ExternalCollaborators on apcd.ExternalCollaborator equals ec.ID
                                                     join a in context.Approvals on apcd.Approval equals a.ID
                                                     where apcd.Approval == approvalBasic.ApprovalId && ec.Guid == userKey && apcd.Phase == a.CurrentPhase
                                                     select apcd.Decision).FirstOrDefault()
                                                        : (from apcd in context.ApprovalCollaboratorDecisions
                                                           join col in context.Users on apcd.Collaborator equals col.ID
                                                           join a in context.Approvals on apcd.Approval equals a.ID
                                                           where apcd.Approval == approvalBasic.ApprovalId && col.Guid == userKey && apcd.Phase == a.CurrentPhase
                                                           select apcd.Decision).FirstOrDefault();

                    bool isLastVersion = IsLastVersion(approvalBasic.ApprovalId);
                    var totalPagesCount = (from ap in context.ApprovalPages
                                           where ap.Approval == approvalBasic.ApprovalId
                                           select ap.ID).Count();
                    var docEmbeddedProfile = (from apep in context.ApprovalEmbeddedProfiles where apep.Approval == approvalBasic.ApprovalId select apep).FirstOrDefault();
                    var firstPageId = (from ap in context.ApprovalPages where ap.Approval == approvalBasic.ApprovalId select ap.ID).Take(1).FirstOrDefault();
                    List<string> color = new List<string>();
                    color.Add(Constants.REDPLATE);
                    color.Add(Constants.GREENPLATE);
                    color.Add(Constants.BLUEPLATE);
                   var docEmbeddedProfilet = (docEmbeddedProfile != null ? docEmbeddedProfile.IsRGB : (from asp in context.ApprovalSeparationPlates
                                                                                                   where
                                                                                                       asp.Page == firstPageId && color.Contains(asp.Name.ToLower())
                                                                                                   select asp.ID).Count() == 3);
                    var phaseName = (from ajp in context.ApprovalJobPhases
                                     join a in context.Approvals on ajp.ID equals a.CurrentPhase
                                     where a.ID == approvalBasic.ApprovalId
                                     select ajp.Name).FirstOrDefault();
                    var phaseCompleted = (from apja in context.ApprovalJobPhaseApprovals
                                          join a in context.Approvals on apja.ID equals a.CurrentPhase
                                          where a.ID == approvalBasic.ApprovalId
                                          orderby apja.ID descending
                                          select apja.PhaseMarkedAsCompleted).FirstOrDefault();
                    var shouldBeViewedByAll = (from ajp in context.ApprovalJobPhases
                                               join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
                                               join a in context.Approvals on ajp.ID equals a.CurrentPhase
                                               where a.ID == approvalBasic.ApprovalId
                                               select aph.ApprovalShouldBeViewedByAll).FirstOrDefault();

                    var version = (from a in context.Approvals
                                   join at in context.ApprovalTypes on a.Type equals at.ID
                                   join j in context.Jobs on a.Job equals j.ID
                                   join js in context.JobStatus on j.Status equals js.ID
                                   join acc in context.Accounts on j.Account equals acc.ID
                                   where a.ID == approvalBasic.ApprovalId
                                   select new GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalVersionModel
                                   {
                                       ID = a.ID,
                                       Creator = a.Creator,
                                       Guid = a.Guid,
                                       ErrorMessage = a.ErrorMessage,
                                       IsDeleted = a.IsDeleted,
                                       Parent = parent,
                                       Title = a.FileName,
                                       VersionNumber = a.Version,
                                       IsLocked = a.IsLocked || (a.CurrentPhase != null && !isLastVersion),
                                       AllowDownloadOriginal = a.AllowDownloadOriginal,
                                       Decision = decision,
                                       ApprovalType = at.Name,
                                       PagesCount = totalPagesCount,
                                       JobStatus = js.Key,
                                       IsRGBColorSpace = docEmbeddedProfilet,
                                       CurrentPhase = a.CurrentPhase,
                                       CurrentPhaseName = phaseName,
                                       IsCompleted = phaseCompleted,
                                       VersionSufix = a.VersionSufix,
                                       CurrentPhaseShouldBeViewedByAll = shouldBeViewedByAll,
                                       RestrictedZoom = a.RestrictedZoom,
                                       AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                       AccountRegion = acc.Region
                                   }).FirstOrDefault();

                    
                    version.VersionLabel = ConstructVersionLabel(version.VersionSufix, version.VersionNumber);
                    version.PageVersionLabel = ConstructPageVersionLabel(version.Title, version.VersionNumber);
                        if (version.ApprovalType == GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalType.Video.ToString())
                        {
                            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                string result1 = Path.ChangeExtension(version.Title, ".mp4");
                            version.MovieFilePath =
                                    GMGColorCommon.GetDataFolderHttpPath(version.AccountRegion, version.AccountDomain) +
                                    "approvals/" + version.Guid + "/" + GMGColorConfiguration.CONVERTED + "/" + result1;
                            }
                            else
                            {
                            version.MovieFilePath = GMGColorDAL.Approval.GetOriginalFilePath(version.AccountDomain, version.AccountRegion,
                                                                                    version.Guid, version.Title);
                            }
                        version.VersionThumbnail = GMGColorDAL.Approval.GetVideoImagePath(version.Guid, version.AccountRegion, version.AccountDomain);
                        }
                        else
                        {
                            int firstPageNumber = 1;
                        version.VersionThumbnail = GMGColorDAL.Approval.GetThumbnailImagePath(version.AccountDomain, version.AccountRegion, version.Guid, firstPageNumber.ToString());
                        }

                    foreach(var v in response.versions)
                    {
                        v.VersionLabel = ConstructVersionLabel(v.VersionLabel, v.VersionNumber);
                        v.PageVersionLabel = ConstructPageVersionLabel(v.Title, v.VersionNumber);
                        v.VersionThumbnail = v.ApprovalType == GMGColorDAL.Approval.ApprovalTypeEnum.Movie.ToString() ? GMGColorDAL.Approval.GetVideoImagePath(v.Guid, v.AccountRegion, v.AccountDomain) : GMGColorDAL.Approval.GetThumbnailImagePath(v.AccountDomain, v.AccountRegion, v.Guid, "1");
                    }
                    response.versions.Add(version);
                    response.approvals[0].Versions = response.versions.Select(v => v.ID).ToArray();

                    foreach (var ve in response.versions)
                    {
                        ve.Parent = response.approvals[0].ID;
                    }

                    var isRGB = response.versions.FirstOrDefault(t => t.ID == approvalBasic.ApprovalId).IsRGBColorSpace;
                    var softProofingParam = GetSoftProofingParamsDefaultOrCreate(approvalBasic.ApprovalId, approvalBasic.JobId, isRGB, context);
                    if (softProofingParam == null)
                    {
                        response.softProofingParams = new List<SoftProofingSessionParamsModel>();
                    }
                    else
                    {
                        response.softProofingParams = new List<SoftProofingSessionParamsModel> { softProofingParam };
                    }
                }
            }
            for (int i = 0; i < approvals.Count; i++)
            {
                var versions =
                    response.versions.Where(v => v.Parent == response.approvals[i].ID)
                        .OrderByDescending(v => v.VersionNumber)
                        .ToList();
                for (int j = 0; j < versions.Count(); j++)
                {
                    versions[j].OrderInDropDownMenu = noOfVersions++;
                }
            }
            return response;
        }

        private static SoftProofingSessionParamsModel GetSoftProofingParamsDefaultOrCreate(int approval, int job, bool isRGB, DbContextBL context)
        {
            var defaultSessionParams = context.SoftProofingSessionParams.Where(sp => sp.Approval == approval && sp.DefaultSession).FirstOrDefault();
            var firstPage = context.ApprovalPages.Where(ap => ap.Approval == approval && ap.Number == 1).FirstOrDefault();
            if (isRGB) 
            { 
             
            if (defaultSessionParams == null || !defaultSessionParams.HighResolutionInProgress || firstPage == null || firstPage.Progress == 100)
            {
                return (from sp in context.SoftProofingSessionParams
                        where
                            sp.Approval == approval &&
                            sp.ActiveChannels == null &&
                            sp.DefaultSession
                        select new SoftProofingSessionParamsModel
                        {
                            ID = sp.ID,
                            ActiveChannels = string.Empty,
                            OutputRgbProfileName = sp.OutputRGBProfileName,
                            SessionGuid = sp.SessionGuid,
                            Version = sp.Approval,
                            DefaultSession = true,
                            HighResolution = sp.HighResolution
                        }).Take(1).FirstOrDefault();
            }
            else
            {
                return (from sp in context.SoftProofingSessionParams
                        where
                            sp.Approval == approval &&
                            sp.ActiveChannels == null &&
                            sp.ViewingCondition == defaultSessionParams.ViewingCondition &&
                            !sp.DefaultSession
                        select new SoftProofingSessionParamsModel
                        {
                            ID = sp.ID,
                            ActiveChannels = string.Empty,
                            OutputRgbProfileName = sp.OutputRGBProfileName,
                            SessionGuid = sp.SessionGuid,
                            Version = sp.Approval,
                            DefaultSession = true,
                            HighResolution = sp.HighResolution,
                            HighResolutionInProgress = true
                        }).Take(1).FirstOrDefault();
            }
        }
                
            var jobViewingConditions = (from vwcn in context.ViewingConditions
                                        join jvwcn in context.ApprovalJobViewingConditions on vwcn.ID equals jvwcn.ViewingCondition
                                        where jvwcn.Job == job
                                        select new InternalUseJobViewingConditionsModel
                                        {
                                            ID = vwcn.ID,
                                            SimulationProfile = vwcn.SimulationProfile,
                                            PaperTint = vwcn.PaperTint
                                        }).FirstOrDefault();

            if (jobViewingConditions != null && jobViewingConditions.SimulationProfile.HasValue)
            {
                var softProofingSessions = (from sp in context.SoftProofingSessionParams
                                            join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                            where
                                                sp.Approval == approval &&
                                                sp.ActiveChannels == null && vwcnd.PaperTint == jobViewingConditions.PaperTint &&
                                                vwcnd.SimulationProfile == jobViewingConditions.SimulationProfile
                                            select new SoftProofingSessionParamsModel
                                            {
                                                ID = sp.ID,
                                                ActiveChannels = string.Empty,
                                                OutputRgbProfileName = sp.OutputRGBProfileName,
                                                SessionGuid = sp.SessionGuid,
                                                Version = sp.Approval,
                                                SimulationProfileId = vwcnd.SimulationProfile,
                                                EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                                CustomProfileId = vwcnd.CustomProfile,
                                                PaperTintId = vwcnd.PaperTint,
                                                DefaultSession = true,
                                                HighResolution = sp.HighResolution
                                            }).Where(s => s.HighResolution == true).FirstOrDefault();
               
                var jobExistingSession = softProofingSessions;

                if (jobExistingSession != null)
                    return jobExistingSession;

                var defaulttSessionParam = context.SoftProofingSessionParams.FirstOrDefault(t => t.Approval == approval && t.DefaultSession == true);

                var newSession = new SoftProofingSessionParam
                {
                    Approval = approval,
                    LastAccessedDate = DateTime.UtcNow,
                    SessionGuid = Guid.NewGuid().ToString(),
                    ViewingCondition = jobViewingConditions.ID,
                    SimulatePaperTint = true,
                    HighResolution = defaulttSessionParam == null ? false : defaulttSessionParam.HighResolution
                };
                context.SoftProofingSessionParams.Add(newSession);
                SoftProofingSessionParamsModel jobSpspModel = new SoftProofingSessionParamsModel
                {
                    ID = newSession.ID,
                    ActiveChannels = string.Empty,
                    OutputRgbProfileName = newSession.OutputRGBProfileName,
                    SessionGuid = newSession.SessionGuid,
                    Version = newSession.Approval,
                    SimulationProfileId = jobViewingConditions.SimulationProfile,
                    PaperTintId = jobViewingConditions.PaperTint,
                    DefaultSession = true,
                    HighResolution = defaulttSessionParam == null ? false : defaulttSessionParam.HighResolution
                };

                return jobSpspModel;
            }

            int? paperTintId = jobViewingConditions != null ? jobViewingConditions.PaperTint : null;

            if (!paperTintId.HasValue)
                if (defaultSessionParams == null || !defaultSessionParams.HighResolutionInProgress || firstPage == null || firstPage.Progress == 100)
                {
                    return (from sp in context.SoftProofingSessionParams
                            join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                            where
                                sp.Approval == approval &&
                                sp.ActiveChannels == null &&
                                sp.DefaultSession
                            select new SoftProofingSessionParamsModel
                            {
                                ID = sp.ID,
                                ActiveChannels = string.Empty,
                                OutputRgbProfileName = sp.OutputRGBProfileName,
                                SessionGuid = sp.SessionGuid,
                                Version = sp.Approval,
                                SimulationProfileId = vwcnd.SimulationProfile,
                                CustomProfileId = vwcnd.CustomProfile,
                                EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                PaperTintId = vwcnd.PaperTint,
                                DefaultSession = true,
                                HighResolution = sp.HighResolution
                            }).Take(1).FirstOrDefault();
                }
                else
                {
                    return (from sp in context.SoftProofingSessionParams
                            join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                            where
                                sp.Approval == approval &&
                                sp.ActiveChannels == null &&
                                sp.ViewingCondition == defaultSessionParams.ViewingCondition &&
                                !sp.DefaultSession
                            select new SoftProofingSessionParamsModel
                            {
                                ID = sp.ID,
                                ActiveChannels = string.Empty,
                                OutputRgbProfileName = sp.OutputRGBProfileName,
                                SessionGuid = sp.SessionGuid,
                                Version = sp.Approval,
                                SimulationProfileId = vwcnd.SimulationProfile,
                                CustomProfileId = vwcnd.CustomProfile,
                                EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                PaperTintId = vwcnd.PaperTint,
                                DefaultSession = true,
                                HighResolution = sp.HighResolution,
                                HighResolutionInProgress = true
                            }).Take(1).FirstOrDefault();
                }


            var defaultConditions = (from sp in context.SoftProofingSessionParams
                                     join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                     where
                                         sp.Approval == approval &&
                                         sp.DefaultSession
                                     select new InternalUseApprovalViewingConditionsModel
                                     {
                                         CustomProfile = vwcnd.CustomProfile,
                                         EmbeddedProfile = vwcnd.EmbeddedProfile,
                                         SimulationProfile = vwcnd.SimulationProfile
                                     }).FirstOrDefault();

            var existingSession = (from sp in context.SoftProofingSessionParams
                                   join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                   where
                                       sp.Approval == approval &&
                                       sp.ActiveChannels == null && vwcnd.PaperTint == paperTintId &&
                                       vwcnd.SimulationProfile == defaultConditions.SimulationProfile && vwcnd.CustomProfile == defaultConditions.CustomProfile && vwcnd.EmbeddedProfile == defaultConditions.EmbeddedProfile
                                   select new SoftProofingSessionParamsModel
                                   {
                                       ID = sp.ID,
                                       ActiveChannels = string.Empty,
                                       OutputRgbProfileName = sp.OutputRGBProfileName,
                                       SessionGuid = sp.SessionGuid,
                                       Version = sp.Approval,
                                       SimulationProfileId = vwcnd.SimulationProfile,
                                       EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                       CustomProfileId = vwcnd.CustomProfile,
                                       PaperTintId = vwcnd.PaperTint,
                                       DefaultSession = true,
                                       HighResolution = sp.HighResolution
                                   }).Take(1).FirstOrDefault();

            if (existingSession != null)
                return existingSession;

            var defaultwithPaperTintCond = new ViewingCondition()
            {
                SimulationProfile = defaultConditions.SimulationProfile,
                EmbeddedProfile = defaultConditions.EmbeddedProfile,
                CustomProfile = defaultConditions.CustomProfile,
                PaperTint = paperTintId,
            };

            var newSessionWithPaperTint = new SoftProofingSessionParam
            {
                Approval = approval,
                LastAccessedDate = DateTime.UtcNow,
                SessionGuid = Guid.NewGuid().ToString(),
                ViewingCondition1 = defaultwithPaperTintCond,
                SimulatePaperTint = true
            };

            context.ViewingConditions.Add(defaultwithPaperTintCond);
            context.SoftProofingSessionParams.Add(newSessionWithPaperTint);

            SoftProofingSessionParamsModel spspModel = new SoftProofingSessionParamsModel
            {
                ID = newSessionWithPaperTint.ID,
                ActiveChannels = string.Empty,
                OutputRgbProfileName = newSessionWithPaperTint.OutputRGBProfileName,
                SessionGuid = newSessionWithPaperTint.SessionGuid,
                Version = newSessionWithPaperTint.Approval,
                SimulationProfileId = defaultwithPaperTintCond.SimulationProfile,
                EmbeddedProfileId = defaultwithPaperTintCond.EmbeddedProfile,
                CustomProfileId = defaultwithPaperTintCond.CustomProfile,
                PaperTintId = defaultwithPaperTintCond.PaperTint,
                DefaultSession = true
            };
          
            spspModel.HighResolution = false;
            

            return spspModel;
        }

        public static bool IsLastVersion(int approvalId)
        {
            using (DbContextBL context = new DbContextBL())
            {
                var jobId = context.Approvals.Where(a => a.ID == approvalId).Select(a => a.Job).FirstOrDefault();
                var maxVersion = context.Approvals.Where(a => a.Job == jobId).OrderByDescending(v => v.Version).FirstOrDefault();
                return maxVersion != null && approvalId == maxVersion.ID;
            }
        }
        public static List<ApprovalPage> GetPages(DbContextBL context, int approvalId,
                                                                                string userKey, bool isExternal)
        {
            try
            {
                bool userIsAdmin = ApiBL.UserIsAdministrator(context, userKey);
                bool userIsManager = ApiBL.UserIsManager(context, userKey);

                //TODO: the query below should be replaced with call from _unitOfWork.UserRepository.GetUserIdByGuid(userKey);
                var loggedUserId = (from u in context.Users
                                    join us in context.UserStatus on u.Status equals us.ID
                                    where u.Guid.ToLower() == userKey.ToLower() && us.Key == "A"
                                    select u.ID).FirstOrDefault();

                List<GMGColorDAL.CustomModels.Api.ApprovalPage> list = new List<ApprovalPage>();
                var data = !isExternal
                               ? (from app in context.Approvals
                                  join at in context.ApprovalTypes on app.Type equals at.ID
                                  join ap in context.ApprovalPages on app.ID equals ap.Approval
                                  join j in context.Jobs on app.Job equals j.ID
                                  join acc in context.Accounts on j.Account equals acc.ID

                                  let collaborators = (from ac in context.ApprovalCollaborators
                                                       where ap.Approval == ac.Approval
                                                       select ac.Collaborator).ToList()
                                  let docEmbeddedProfile =
                                      (from apep in context.ApprovalEmbeddedProfiles
                                       where apep.Approval == app.ID
                                       select apep.Name).FirstOrDefault()
                                  where
                                      ap.Approval == approvalId &&
                                     (collaborators.Contains(loggedUserId) || userIsAdmin || userIsManager || j.JobOwner == loggedUserId)
                                  select new
                                  {
                                      accountRegion = acc.Region,
                                      accountId = acc.ID,
                                      accountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                      approvalId = app.ID,
                                      isVideoFile = at.Key == (int) GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                                      jobGUID = app.Guid,
                                      jobFilename = app.FileName,
                                      documentEmbeddedProfile = docEmbeddedProfile
                                  }).FirstOrDefault()
                               : ((from app in context.Approvals
                                   join at in context.ApprovalTypes on app.Type equals at.ID
                                   join ap in context.ApprovalPages on app.ID equals ap.Approval
                                   join j in context.Jobs on app.Job equals j.ID
                                   join acc in context.Accounts on j.Account equals acc.ID
                                   join sa in context.SharedApprovals on ap.Approval equals sa.Approval
                                   join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                   let docEmbeddedProfile =
                                       (from apep in context.ApprovalEmbeddedProfiles
                                        where apep.Approval == app.ID
                                        select apep.Name).FirstOrDefault()
                                   where
                                       ap.Approval == approvalId && ec.Guid.ToLower() == userKey.ToLower() &&
                                       !ec.IsDeleted
                                   select new
                                   {
                                       accountRegion = acc.Region,
                                       accountId = acc.ID,
                                       accountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                       approvalId = app.ID,
                                       isVideoFile = at.Key == (int) GMGColorDAL.Approval.ApprovalTypeEnum.Movie,
                                       jobGUID = app.Guid,
                                       jobFilename = app.FileName,
                                       documentEmbeddedProfile = docEmbeddedProfile
                                   }).FirstOrDefault());

                if (data == null)
                {
                    // if no access then return null
                    return null;
                }

                string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(data.accountRegion);

                string urlToApprovals = GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                                            ? GMGColorConfiguration.AppConfiguration.PathToDataFolder(data.accountRegion)
                                            : GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +
                                              data.accountDomain + "/" +
                                              GMGColorConfiguration.AppConfiguration.DataFolderName(data.accountRegion) +
                                              "/";

                GMG.CoZone.Common.ApprovalJobFolder approvalJobFolder = new GMG.CoZone.Common.ApprovalJobFolder(
                    data.jobGUID,
                    data.jobFilename,
                    GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                        ? GMGColorConfiguration.AppConfiguration.PathToProcessingFolder
                        : GMGColorConfiguration.AppConfiguration.FileSystemPathToDataFolder,
                    urlToApprovals);

                var pages = (from ap in context.ApprovalPages
                             join a in context.Approvals on ap.Approval equals a.ID
                             where a.ID == data.approvalId && (ap.Progress == 100 || (ap.Number == 1 && ap.Progress > 0))
                             orderby ap.Number ascending
                             select new
                             {
                                 ap.ID,
                                 ap.DPI,
                                 a.Type,
                                 LargeThumbnailHeight = ap.PageLargeThumbnailHeight,
                                 LargeThumbnailWidth = ap.PageLargeThumbnailWidth,
                                 SmallThumbnailHeight = ap.PageSmallThumbnailHeight,
                                 SmallThumbnailWidth = ap.PageSmallThumbnailWidth,
                                 PageNumber = ap.Number,
                                 a.TranscodingJobId,
                                 ap.OriginalImageHeight,
                                 ap.OriginalImageWidth,
                                 ap.OutputRenderHeight,
                                 ap.OutputRenderWidth,
                                 ap.HTMLFilePath
                             }).ToArray();

                if (pages.Any())
                {
                    string approvalFolderRelativePath =
                            GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + data.jobGUID + "/";

                    foreach (var page in pages)
                    {
                        var p = new ApprovalPage
                        {
                            ID = page.ID,
                            Approval = approvalId,
                            DPI = page.DPI,
                            LargeThumbnailWidth = page.LargeThumbnailWidth,
                            LargeThumbnailHeight = page.LargeThumbnailHeight,
                            SmallThumbnailHeight = page.SmallThumbnailHeight,
                            SmallThumbnailWidth = page.SmallThumbnailWidth,
                            PageNumber = page.PageNumber,
                            PageFolder = approvalJobFolder.GetApprovalRemoteFolderPath(),
                            OriginalImageHeight = (int)page.OriginalImageHeight.GetValueOrDefault(),
                            OriginalImageWidth = (int)page.OriginalImageWidth.GetValueOrDefault(),

                            SmallThumbnailPath = data.isVideoFile
                                                         ? GMGColorDAL.Approval.GetVideoImagePath(data.jobGUID,
                                                                                                  data.accountRegion,
                                                                                                  data.accountDomain
                                                                                                  )
                                                         : GMGColorDAL.Approval.GetThumbnailImagePath(
                                                             data.accountDomain, data.accountRegion,
                                                             data.jobGUID, data.jobFilename, page.PageNumber, false),

                            LargeThumbnailPath =
                                    GMGColorDAL.Approval.GetProcessedImagePath(data.accountDomain, data.accountRegion,
                                                                               data.jobGUID, data.jobFilename, page.PageNumber,
                                                                               false),

                            TranscodingJobId = page.TranscodingJobId,
                            EmbeddedProfile = data.documentEmbeddedProfile
                        };

                     
                        if (page.Type == (int)GMGColorDAL.Approval.ApprovalTypeEnum.Zip + 1)
                        {
                            string[] s = page.HTMLFilePath.Replace('\\','/').Split('/');
                            p.HTMLFileName = s[s.Length - 2] + "/" + s[s.Length - 1];
                            p.HTMLFilePath = GMGColorCommon.GetZipHttpPath(data.accountRegion, data.accountDomain) + page.HTMLFilePath;
                        }

                        if (!data.isVideoFile)
                        {
                            if (page.OriginalImageHeight == null || page.OriginalImageWidth == null)
                            {
                                string pageFolderRelativePath = approvalFolderRelativePath + p.PageNumber + "/" +
                                                                "ImageProperties.xml";
                                if (GMGColorIO.FileExists(pageFolderRelativePath, data.accountRegion))
                                {
                                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                                    {
                                        MemoryStream xmlStream =
                                            AWSSimpleStorageService.GetFileStream(
                                                GMGColorConfiguration.AppConfiguration.DataFolderName(data.accountRegion),
                                                pageFolderRelativePath,
                                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                                        XDocument xmlFile = XDocument.Load(xmlStream);
                                        p.OriginalImageHeight =
                                            xmlFile.Root.Attribute("HEIGHT").Value.ToInt().GetValueOrDefault();
                                        p.OriginalImageWidth =
                                            xmlFile.Root.Attribute("WIDTH").Value.ToInt().GetValueOrDefault();
                                        xmlStream.Dispose();
                                    }
                                    else
                                    {
                                        XDocument xmlProperties =
                                            XDocument.Load(pathtoDataFolder + pageFolderRelativePath);
                                        if (xmlProperties != null && xmlProperties.Root != null)
                                        {
                                            p.OriginalImageHeight =
                                                xmlProperties.Root.Attribute("HEIGHT").Value.ToInt().GetValueOrDefault();
                                            p.OriginalImageWidth =
                                                xmlProperties.Root.Attribute("WIDTH").Value.ToInt().GetValueOrDefault();
                                        }
                                        else
                                        {
                                            //TODO Log Error
                                        }
                                    }
                                }

                                //Save the fields into database to avoid making another s3 requests in the future
                                GMGColorDAL.ApprovalPage pageDAL = new GMGColorDAL.ApprovalPage {ID = page.ID};
                                context.ApprovalPages.Attach(pageDAL);
                                context.Entry(pageDAL).Property(u => u.OriginalImageHeight).IsModified = true;
                                context.Entry(pageDAL).Property(u => u.OriginalImageWidth).IsModified = true;
                                pageDAL.OriginalImageHeight = Convert.ToInt32(p.OriginalImageHeight);
                                pageDAL.OriginalImageWidth = Convert.ToInt32(p.OriginalImageWidth);
                                context.SaveChanges();
                            }
                            else
                            {
                                p.OriginalImageHeight = page.OriginalImageHeight.GetValueOrDefault();
                                p.OriginalImageWidth = page.OriginalImageWidth.GetValueOrDefault();
                                p.OutputImageHeight = page.OutputRenderHeight.GetValueOrDefault();
                                p.OutputImageWidth = page.OutputRenderWidth.GetValueOrDefault();
                            }
                        }
                        else
                        {
                            if (p.TranscodingJobId != null && GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                if (p.LargeThumbnailHeight == 0 && p.LargeThumbnailWidth == 0)
                                {
                                    TranscodedJob outputJob =
                                        AWSElasticTranscoderService.ReadTranscodedJob(
                                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                            GMGColorConfiguration.AppConfiguration.VideoTranscoderAWSRegion,
                                            p.TranscodingJobId);

                                    if (outputJob != null)
                                    {
                                        p.LargeThumbnailWidth = outputJob.Width;
                                        p.LargeThumbnailHeight = outputJob.Height;
                                        context.SaveChanges();
                                    }
                                }
                            }
                        }

                        list.Add(p);
                    }

                    // add nextPage and prevPage
                    for (var i = 0; i < list.Count; i++)
                    {
                        list[i].PrevPage = (i >= 1) ? (int?) list[i - 1].ID : null;
                        list[i].NextPage = (i < list.Count - 1) ? (int?) list[i + 1].ID : null;
                    }
                }
                return list;
            }
            catch (ExceptionBL)
            {
                GMGColorDAL.GMGColorLogging.log.Info("Exception in GetPages");
                throw;
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Info("Exception in GetPages");
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetPages, ex);
            }
        }

        public static List<GMGColorDAL.CustomModels.Api.AnnotationResponse> GetAnnotationWithReplies(
            DbContextBL context, int annotationId, string user_key)
        {
            List<GMGColorDAL.CustomModels.Api.AnnotationResponse> result = new List<AnnotationResponse>();
            try
            {
                result.Add(GetAnnotation(context, annotationId, null, user_key));

                List<int> list = (from aa in context.ApprovalAnnotations
                                  where aa.Parent == annotationId
                                  select aa.ID).ToList();

                result.AddRange(list.Select(reply => GetAnnotation(context, reply, null, user_key)));
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAnnotationWithReplies, ex);
            }
            return result;
        }

        public static AnnotationResponse GetAnnotation(DbContextBL context, int annotationId, int? parentAnnotationId, string user_key)
        {
            try
            {
                bool isExternalUser = false;
                AccountTimeZoneandPattern accountDetails = new AccountTimeZoneandPattern();
                accountDetails = (from u in context.Users
                                  join a in context.Accounts on u.Account equals a.ID
                                  join df in context.DateFormats on a.DateFormat equals df.ID
                                  where u.Guid == user_key
                                  select new AccountTimeZoneandPattern
                                  {
                                      timeZone = a.TimeZone,
                                      pattern = df.Pattern
                                  }).FirstOrDefault();
                if (accountDetails == null || accountDetails.timeZone == "")
                {
                    accountDetails = (from ex in context.ExternalCollaborators
                                      join a in context.Accounts on ex.Account equals a.ID
                                      join df in context.DateFormats on a.DateFormat equals df.ID
                                      where ex.Guid == user_key
                                      select new AccountTimeZoneandPattern
                                      {
                                          timeZone = a.TimeZone,
                                          pattern = df.Pattern
                                      }).FirstOrDefault();
                    isExternalUser = true;
                }

                var CanPassAnnotation = isExternalUser ? (from u in context.ExternalCollaborators
                                                          join aa in context.ApprovalAnnotations on u.ID equals aa.ExternalCreator
                                                          join ap in context.ApprovalPages on aa.Page equals ap.ID
                                                          join a in context.Approvals on ap.Approval equals a.ID
                                                          where aa.ID == annotationId && (u.ID == a.PrimaryDecisionMaker || aa.ExternalCreator == u.ID || !aa.IsPrivateAnnotation || !a.PrivateAnnotations)
                                                          select aa.ID).Any() : (from u in context.Users
                                                                                 join aa in context.ApprovalAnnotations on u.ID equals aa.Creator
                                                                                 join ap in context.ApprovalPages on aa.Page equals ap.ID
                                                                                 join a in context.Approvals on ap.Approval equals a.ID
                                                                                 where aa.ID == annotationId && (u.ID == a.PrimaryDecisionMaker || aa.Creator == u.ID || !aa.IsPrivateAnnotation || !a.PrivateAnnotations)
                                                                                 select aa.ID).Any();
                
                var phaseInfo = (from ajp in context.ApprovalJobPhases
                                 join aa in context.ApprovalAnnotations on ajp.ID equals aa.Phase
                                 where aa.ID == annotationId
                                 select new
                                 {
                                     ajp.Name,
                                     ajp.ID
                                 }).FirstOrDefault();
                var annotationResponse = new GMGColorDAL.CustomModels.Api.AnnotationResponse();
                 annotationResponse.annotation = (from aa in context.ApprovalAnnotations
                                  join ap in context.ApprovalPages on aa.Page equals ap.ID
                                  where aa.ID == annotationId && CanPassAnnotation
                                  select new ApprovalAnnotationGet
                                  {

                                      ID = aa.ID,
                                      Status = aa.Status,
                                      PageNumber = ap.Number,
                                      StartIndex = aa.StartIndex,
                                      EndIndex = aa.EndIndex,
                                      Page = ap.ID,
                                      OrderNumber = aa.OrderNumber,
                                      Parent = aa.Parent,
                                      ModifiedDate = aa.ModifiedDate.ToString(),
                                      Creator = aa.Creator,//.HasValue ? aa.Creator : null,
                                      ExternalCreator = aa.ExternalCreator,//.HasValue ? aa.ExternalCreator : null,
                                      Modifier = aa.Modifier,//.HasValue ? aa.Modifier : null,
                                      ExternalModifier = aa.ExternalModifier,//.HasValue ? aa.ExternalModifier : null,
                                      AnnotatedDate = aa.AnnotatedDate.ToString(),
                                      Comment = aa.Comment,
                                      IsPrivate = aa.IsPrivate,
                                      TimeFrame = aa.TimeFrame,
                                      CommentType = aa.CommentType,
                                      CrosshairXCoord = aa.CrosshairXCoord,
                                      CrosshairYCoord = aa.CrosshairYCoord,
                                      ReferenceFilePath = aa.ReferenceFilepath,
                                      Version = ap.Approval,
                                      SoftProofingLevel = (SoftProofingLevel)aa.CalibrationStatus,
                                      ViewingCondition = aa.ViewingCondition,
                                      AnnotatedOnImageWidth = aa.AnnotatedOnImageWidth,
                                      OriginalVideoWidth = aa.OriginalVideoWidth == null ? 0 : aa.OriginalVideoWidth,
                                      OriginalVideoHeight = aa.OriginalVideoHeight == null ? 0 : aa.OriginalVideoHeight,
                                      IsPrivateAnnotation = aa.IsPrivateAnnotation,
                                      ShowVideoReport = aa.ShowVideoReport
                                  }).FirstOrDefault();

                if (annotationResponse.annotation == null)
                {
                    return null;
                }
                else
                {
                    annotationResponse.annotation.Phase = phaseInfo != null ? (int?)phaseInfo.ID : null;
                    annotationResponse.annotation.PhaseName = phaseInfo != null ? phaseInfo.Name : null;
                    annotationResponse.annotation.ModifiedDate = DateTime2String(Convert.ToDateTime(annotationResponse.annotation.ModifiedDate), accountDetails.timeZone);
                    annotationResponse.annotation.AnnotatedDate = DateTime2String(Convert.ToDateTime(annotationResponse.annotation.AnnotatedDate), accountDetails.timeZone);
                    List<int> commentTypes = new List<int>();
                    commentTypes.Add((int)ApprovalCommentType.CommentType.TextComment);
                    commentTypes.Add((int)ApprovalCommentType.CommentType.DrawComment);
                    if (commentTypes.Contains(annotationResponse.annotation.CommentType))
                    {
                        annotationResponse.custom_shapes = (from aam in context.ApprovalAnnotationMarkups
                                                            join sh in context.Shapes on aam.Shape equals sh.ID
                                                            where
                                                                aam.ApprovalAnnotation ==
                                                                annotationResponse.annotation.ID
                                                            select new ApprovalAnnotationShape
                                                            {

                                                                ID = sh.ID,
                                                                FXG = sh.FXG,
                                                                SVG = sh.SVG,
                                                                IsCustom = sh.IsCustom,
                                                                BackgroundColor = aam.BackgroundColor,
                                                                Color = aam.Color,
                                                                Rotation = aam.Rotation,
                                                                ScaleX = (double)aam.Width,
                                                                ScaleY = (double)aam.Height,
                                                                ApprovalAnnotation = aam.ApprovalAnnotation,
                                                                Size = aam.Size,
                                                                X = (double)aam.X,
                                                                Y = (double)aam.Y,
                                                                HighlighType = (int?)null
                                                            }).Union((from aa in context.ApprovalAnnotations
                                                                      where
                                                                          aa.ID == annotationId &&
                                                                          aa.CommentType == (int)ApprovalCommentType.CommentType.TextComment
                                                                      select new ApprovalAnnotationShape
                                                                      {
                                                                          ID = 0,
                                                                          FXG = "",
                                                                          SVG = aa.HighlightData,
                                                                          IsCustom = true,
                                                                          BackgroundColor = "#ff0000",
                                                                          Color = "#ff0000",
                                                                          Rotation = (int?)1,
                                                                          ScaleX = (double)1,
                                                                          ScaleY = (double)1,
                                                                          ApprovalAnnotation = aa.ID,
                                                                          Size = (int?)1,
                                                                          X = (double)1,
                                                                          Y = (double)1,
                                                                          HighlighType = aa.HighlightType
                                                                      })).ToArray();


                        foreach (var shape in annotationResponse.custom_shapes)
                        {
                            if (shape.ID == 0)
                            {
                                shape.ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
                            }
                        }
                    }
                    annotationResponse.AnnotationAttachments = (from aaa in context.ApprovalAnnotationAttachments
                                                                join aa in context.ApprovalAnnotations on
                                                                    aaa.ApprovalAnnotation equals aa.ID
                                                                where aa.ID == annotationId
                                                                select new ApprovalAnnotationAttachment
                                                                {
                                                                    DisplayName = aaa.DisplayName,
                                                                    Guid = aaa.Guid,
                                                                    Name = aaa.Name,
                                                                    ID = aaa.ID
                                                                }).ToArray();

                    var isCheckList = (from a in context.Approvals where a.ID == annotationResponse.annotation.Version select a.Checklist).FirstOrDefault();
                    if (isCheckList != null)
                    { 
                    if (parentAnnotationId > 0)
                    {

                        annotationResponse.ApprovalannotationChecklistitems = (from aci in context.ApprovalAnnotationCheckListItems
                                                                               join aa in context.ApprovalAnnotations on aci.ApprovalAnnotation equals aa.ID
                                                                               join ci in context.CheckListItem on aci.CheckListItem equals ci.ID
                                                                               where aa.ID == parentAnnotationId
                                                                               select new ApprovalChecklistItem
                                                                               {
                                                                                   Id = aci.CheckListItem,
                                                                                   Item = ci.Name,
                                                                                   IsItemChecked = true,
                                                                                   ItemValue = aci.TextValue,
                                                                                   ID = aci.ID,
                                                                                   TotalChanges = aci.TotalChanges == null ? 0 : aci.TotalChanges,
                                                                                   ShowNoOfChanges = true
                                                                               }).ToArray();

                        annotationResponse.AnnotationAllchecklistitems = (from anch in context.ApprovalAnnotationChecklistItemHistories
                                                                          join aa in context.ApprovalAnnotations on anch.ApprovalAnnotation equals aa.ID
                                                                          join ci in context.CheckListItem on anch.ChecklistItem equals ci.ID
                                                                          let creator = (from u in context.Users where u.ID == anch.Creator select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                                          let externalCreator = (from ec in context.ExternalCollaborators where ec.ID == anch.ExternalCreator select ec.GivenName + " " + ec.FamilyName).FirstOrDefault()
                                                                          where aa.ID == parentAnnotationId
                                                                          select new ApprovalAnnotationChecklistItemDetail
                                                                          {
                                                                              ID = anch.ID,
                                                                              ChecklistItemID = anch.ChecklistItem,
                                                                              ChecklistItemName = ci.Name,
                                                                              ChecklistItemValue = anch.TextValue,
                                                                              CreatedDate = anch.CreatedDate.ToString(),
                                                                              Creator = creator == null ? externalCreator : creator
                                                                          }).ToArray();

                        var AnnotationChecklistItems = (from aci in context.ApprovalAnnotationCheckListItems
                                                        join aa in context.ApprovalAnnotations on aci.ApprovalAnnotation equals aa.ID
                                                        join ci in context.CheckListItem on aci.CheckListItem equals ci.ID
                                                        let creator = (from u in context.Users where u.ID == aci.Creator select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                        let externalCreator = (from ec in context.ExternalCollaborators where ec.ID == aci.ExternalCreator select ec.GivenName + " " + ec.FamilyName).FirstOrDefault()
                                                        where aa.ID == parentAnnotationId
                                                        select new ApprovalAnnotationChecklistItemDetail
                                                        {
                                                            ID = aci.ID,
                                                            ChecklistItemID = aci.CheckListItem,
                                                            ChecklistItemName = ci.Name,
                                                            ChecklistItemValue = aci.TextValue,
                                                            CreatedDate = aci.CreatedDate.ToString(),
                                                            Creator = creator == null ? externalCreator : creator
                                                        }).ToArray();
                        annotationResponse.AnnotationAllchecklistitems = annotationResponse.AnnotationAllchecklistitems.Concat(AnnotationChecklistItems).ToArray();
                    }
                    else
                    {
                        annotationResponse.ApprovalannotationChecklistitems = (from aci in context.ApprovalAnnotationCheckListItems
                                                                               join aa in context.ApprovalAnnotations on aci.ApprovalAnnotation equals aa.ID
                                                                               join ci in context.CheckListItem on aci.CheckListItem equals ci.ID
                                                                               where aa.ID == annotationId
                                                                               select new ApprovalChecklistItem
                                                                               {
                                                                                   Id = aci.CheckListItem,
                                                                                   Item = ci.Name,
                                                                                   IsItemChecked = true,
                                                                                   ItemValue = aci.TextValue,
                                                                                   ID = aci.ID,
                                                                                   TotalChanges = aci.TotalChanges == null ? 0 : aci.TotalChanges,
                                                                                   ShowNoOfChanges = true
                                                                               }).ToArray();

                        annotationResponse.AnnotationAllchecklistitems = (from anch in context.ApprovalAnnotationChecklistItemHistories
                                                                          join aa in context.ApprovalAnnotations on anch.ApprovalAnnotation equals aa.ID
                                                                          join ci in context.CheckListItem on anch.ChecklistItem equals ci.ID
                                                                          let creator = (from u in context.Users where u.ID == anch.Creator select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                                          let externalCreator = (from ec in context.ExternalCollaborators where ec.ID == anch.ExternalCreator select ec.GivenName + " " + ec.FamilyName).FirstOrDefault()
                                                                          where aa.ID == annotationId
                                                                          select new ApprovalAnnotationChecklistItemDetail
                                                                          {
                                                                              ID = anch.ID,
                                                                              ChecklistItemID = anch.ChecklistItem,
                                                                              ChecklistItemName = ci.Name,
                                                                              ChecklistItemValue = anch.TextValue,
                                                                              CreatedDate = anch.CreatedDate.ToString(),
                                                                              Creator = creator == null ? externalCreator : creator
                                                                          }).ToArray();

                        var AnnotationChecklistItems = (from aci in context.ApprovalAnnotationCheckListItems
                                                        join aa in context.ApprovalAnnotations on aci.ApprovalAnnotation equals aa.ID
                                                        join ci in context.CheckListItem on aci.CheckListItem equals ci.ID
                                                        let creator = (from u in context.Users where u.ID == aci.Creator select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                        let externalCreator = (from ec in context.ExternalCollaborators where ec.ID == aci.ExternalCreator select ec.GivenName + " " + ec.FamilyName).FirstOrDefault()
                                                        where aa.ID == annotationId
                                                        select new ApprovalAnnotationChecklistItemDetail
                                                        {
                                                            ID = aci.ID,
                                                            ChecklistItemID = aci.CheckListItem,
                                                            ChecklistItemName = ci.Name,
                                                            ChecklistItemValue = aci.TextValue,
                                                            CreatedDate = aci.CreatedDate.ToString(),
                                                            Creator = creator == null ? externalCreator : creator
                                                        }).ToArray();

                        annotationResponse.AnnotationAllchecklistitems = annotationResponse.AnnotationAllchecklistitems.Concat(AnnotationChecklistItems).ToArray();
                    }
                }
                    if (annotationResponse.AnnotationAttachments != null)
                    {
                        annotationResponse.annotation.AnnotationAttachments =
                            annotationResponse.AnnotationAttachments.Select(o => o.ID).ToArray();
                    }

                    if (annotationResponse.ApprovalannotationChecklistitems != null)
                    {
                        annotationResponse.annotation.ApprovalannotationChecklistitems =
                            annotationResponse.ApprovalannotationChecklistitems.Select(o => o.ID).ToArray();
                    }

                    if (annotationResponse.AnnotationAllchecklistitems != null)
                    {
                        annotationResponse.annotation.AnnotationAllchecklistitems =
                            annotationResponse.AnnotationAllchecklistitems.Select(o => o.ID).ToArray();

                        foreach (var item in annotationResponse.AnnotationAllchecklistitems)
                        {
                            var date = Convert.ToDateTime(item.CreatedDate);
                            item.CreatedDate = GMGColorFormatData.GetFormattedDate(date, accountDetails.pattern) + " " + GMGColorFormatData.GetUserTimeFromUTC(date, accountDetails.timeZone).ToString("h:mm tt");
                        }
                    }

                    if (annotationResponse.annotation.ViewingCondition != null)
                    {
                        annotationResponse.viewing_conditions = GetViewingConditions(context,
                            new[] { annotationResponse.annotation.ViewingCondition.Value });
                    }

                    return annotationResponse;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAnnotation, ex);
            }
        }

        private static List<GMGColorDAL.CustomModels.Api.ApprovalCollaboratorLink> GetVersionCollaboratorLinks(
            DbContextBL context, IEnumerable<int> versionIds, bool retrieveExternalUsers, string timezone)
        {
            try
            {
                var items = !retrieveExternalUsers
                                ? (from ac in context.ApprovalCollaborators
                                   join acr in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole equals
                                       acr.ID
                                   join u in context.Users on ac.Collaborator equals u.ID
                                   join us in context.UserStatus on u.Status equals us.ID
                                   join a in context.Approvals on ac.Approval equals a.ID
                                   let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                   where
                                                       acd.Collaborator == u.ID && acd.Approval == a.ID &&
                                                       acd.Phase == a.CurrentPhase
                                                   select acd.Decision).Take(1).FirstOrDefault()
                                   let primaryGroup = (from ug in context.UserGroups
                                                       join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                                       join acg in context.ApprovalCollaboratorGroups on ug.ID equals
                                                           acg.CollaboratorGroup
                                                       where ugu.User == u.ID && ugu.IsPrimary && acg.Approval == a.ID
                                                       select ug.ID).FirstOrDefault()
                                                                                      
                                   let jobOwner = (from j in context.Jobs where j.ID == a.Job select j.JobOwner).FirstOrDefault()

                                   where
                                       versionIds.Contains(a.ID) 
                                   select new
                                   {
                                       ID = ac.Collaborator,
                                       ExpireDate = u.CreatedDate,
                                       IsExpired = false,
                                       approval_role = ac.ApprovalCollaboratorRole,
                                       approval_version = ac.Approval,
                                       PrimaryGroup = primaryGroup,
                                       Decision = decision,
                                       Permission = acr.Key,
                                       a.IsLocked,
                                       a.CurrentPhase,
                                       ac.Phase,
                                       a.Version,
                                       a.Owner,
                                       a.Job,
                                       JobOwner = jobOwner,
                                       ApprovalId = a.ID
                                   }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new { e.Version, e.Phase }).FirstOrDefault()).ToList()
                                : (from sa in context.SharedApprovals
                                   join acr in context.ApprovalCollaboratorRoles on sa.ApprovalCollaboratorRole equals
                                       acr.ID
                                   join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                   join a in context.Approvals on sa.Approval equals a.ID
                                   let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                   where
                                                       acd.ExternalCollaborator == ec.ID && acd.Approval == a.ID &&
                                                       acd.Decision != null && acd.Phase == a.CurrentPhase
                                                   select acd.Decision).Take(1).FirstOrDefault()

                                   let jobOwner = (from j in context.Jobs where j.ID == a.Job select j.JobOwner).FirstOrDefault()

                                   where
                                       versionIds.Contains(a.ID) 
                                   select new
                                   {
                                       ID = sa.ExternalCollaborator,
                                       ExpireDate = sa.ExpireDate,
                                       IsExpired = sa.IsExpired,
                                       approval_role = sa.ApprovalCollaboratorRole,
                                       approval_version = sa.Approval,
                                       PrimaryGroup = 0,
                                       Decision = decision,
                                       Permission = acr.Key,
                                       a.IsLocked,
                                       a.CurrentPhase,
                                       sa.Phase,
                                       a.Version,
                                       a.Owner,
                                       a.Job,
                                       JobOwner = jobOwner,
                                       ApprovalId = a.ID
                                   }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new { e.Version, e.Phase }).FirstOrDefault()).ToList();

                List<ApprovalCollaboratorLink> list =
                    items.Select(o => new GMGColorDAL.CustomModels.Api.ApprovalCollaboratorLink
                    {
                        collaborator_id = o.ID,
                        ExpireDate = DateTime2String(o.ExpireDate, timezone),
                        IsExpired = o.IsExpired,
                        approval_role = o.approval_role,
                        approval_version = o.approval_version,
                        UserGroup = o.PrimaryGroup > 0 ? (int?)o.PrimaryGroup : null,
                        Decision = o.Decision.HasValue ? (int?)o.Decision.Value : null,
                        CanAnnotate = ((o.CurrentPhase == null && (!o.IsLocked || o.ID == o.Owner)) || (o.CurrentPhase != null && ApprovalBL.IsLastVersion(o.ApprovalId, context) && (!o.IsLocked || (o.IsLocked && o.ID == o.JobOwner)))) && GMG.CoZone.Common.Permissions.CanAnnotate(o.Permission),
                        CanApprove = ((o.CurrentPhase == null && (!o.IsLocked || o.ID == o.Owner)) || (o.CurrentPhase != null && ApprovalBL.IsLastVersion(o.ApprovalId, context) && (!o.IsLocked || (o.IsLocked && o.ID == o.JobOwner)))) && GMG.CoZone.Common.Permissions.CanApprove(o.Permission)
                    }).ToList();

                for (int i = 0; i < list.Count(); i++)
                {
                    list[i].ID = (versionIds.First() * 10000) + i + 1;
                }
                return list;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetInternalCollaboratorLinks, ex);
            }
        }


        [ObsoleteAttribute("This method is obsolete. Call GetCollaboratorLinks from GetProofstudioApproval instead.", false)]
        private static List<GMGColorDAL.CustomModels.Api.ApprovalCollaboratorLink> GetCollaboratorLinks(
            DbContextBL context, IEnumerable<int> versionIds, bool retrieveExternalUsers, string timezone, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            try
            {
                var items = !retrieveExternalUsers
                                ? (from ac in context.ApprovalCollaborators
                                   join acr in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole equals
                                       acr.ID
                                   join u in context.Users on ac.Collaborator equals u.ID
                                   join us in context.UserStatus on u.Status equals us.ID
                                   join a in context.Approvals on ac.Approval equals a.ID
                                   let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                   where
                                                       acd.Collaborator == u.ID && acd.Approval == a.ID &&
                                                       acd.Phase == a.CurrentPhase
                                                   select acd.Decision).Take(1).FirstOrDefault()
                                   let primaryGroup = (from ug in context.UserGroups
                                                       join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                                       join acg in context.ApprovalCollaboratorGroups on ug.ID equals
                                                           acg.CollaboratorGroup
                                                       where ugu.User == u.ID && ugu.IsPrimary && acg.Approval == a.ID
                                                       select ug.ID).FirstOrDefault()

                                   let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                                join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                                where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                                select aan.ID).Any()

                                   let jobOwner = (from j in context.Jobs where j.ID == a.Job select j.JobOwner).FirstOrDefault()

                                   where
                                       versionIds.Contains(a.ID) && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                                   select new
                                   {
                                       ID = ac.Collaborator,
                                       ExpireDate = u.CreatedDate,
                                       IsExpired = false,
                                       approval_role = ac.ApprovalCollaboratorRole,
                                       approval_version = ac.Approval,
                                       PrimaryGroup = primaryGroup,
                                       Decision = decision,
                                       Permission = acr.Key,
                                       a.IsLocked,
                                       a.CurrentPhase,
                                       ac.Phase,
                                       a.Version,
                                       a.Owner,
                                       a.Job,
                                       JobOwner = jobOwner,
                                       ApprovalId = a.ID
                                   }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new { e.Version, e.Phase }).FirstOrDefault()).ToList()
                                : (from sa in context.SharedApprovals
                                   join acr in context.ApprovalCollaboratorRoles on sa.ApprovalCollaboratorRole equals
                                       acr.ID
                                   join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                   join a in context.Approvals on sa.Approval equals a.ID
                                   let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                   where
                                                       acd.ExternalCollaborator == ec.ID && acd.Approval == a.ID &&
                                                       acd.Decision != null && acd.Phase == a.CurrentPhase
                                                   select acd.Decision).Take(1).FirstOrDefault()

                                   let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                                join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                                where ap.Approval == a.ID && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                                select aan.ID).Any()

                                   let jobOwner = (from j in context.Jobs where j.ID == a.Job select j.JobOwner).FirstOrDefault()

                                   where
                                       versionIds.Contains(a.ID) && ((userCanViewAllPhasesAnnotations && userHasAnnotations &&
                                         visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                                   select new
                                   {
                                       ID = sa.ExternalCollaborator,
                                       ExpireDate = sa.ExpireDate,
                                       IsExpired = sa.IsExpired,
                                       approval_role = sa.ApprovalCollaboratorRole,
                                       approval_version = sa.Approval,
                                       PrimaryGroup = 0,
                                       Decision = decision,
                                       Permission = acr.Key,
                                       a.IsLocked,
                                       a.CurrentPhase,
                                       sa.Phase,
                                       a.Version,
                                       a.Owner,
                                       a.Job,
                                       JobOwner = jobOwner,
                                       ApprovalId = a.ID
                                   }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new { e.Version, e.Phase }).FirstOrDefault()).ToList();

                List<ApprovalCollaboratorLink> list =
                    items.Select(o => new GMGColorDAL.CustomModels.Api.ApprovalCollaboratorLink
                    {
                        collaborator_id = o.ID,
                        ExpireDate = DateTime2String(o.ExpireDate, timezone),
                        IsExpired = o.IsExpired,
                        approval_role = o.approval_role,
                        approval_version = o.approval_version,
                        UserGroup = o.PrimaryGroup > 0 ? (int?) o.PrimaryGroup : null,
                        Decision = o.Decision.HasValue ? (int?) o.Decision.Value : null,
                        CanAnnotate = ((o.CurrentPhase == null && (!o.IsLocked || o.ID == o.Owner)) || (o.CurrentPhase != null && ApprovalBL.IsLastVersion(o.ApprovalId, context) && (!o.IsLocked || (o.IsLocked && o.ID == o.JobOwner)))) && GMG.CoZone.Common.Permissions.CanAnnotate(o.Permission),
                        CanApprove = ((o.CurrentPhase == null && (!o.IsLocked || o.ID == o.Owner)) || (o.CurrentPhase != null && ApprovalBL.IsLastVersion(o.ApprovalId, context) && (!o.IsLocked || (o.IsLocked && o.ID == o.JobOwner)))) && GMG.CoZone.Common.Permissions.CanApprove(o.Permission)
                    }).ToList();

                for (int i = 0; i < list.Count(); i++)
                {
                    list[i].ID = (versionIds.First() * 10000) + i + 1;
                }
                return list;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetInternalCollaboratorLinks, ex);
            }
        }

        public static List<GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape> GetAnnotationShapes(DbContextBL context, List<int> annotationIds)
        {
            return (from aam in context.ApprovalAnnotationMarkups
                    join aa in context.ApprovalAnnotations on aam.ApprovalAnnotation equals aa.ID
                    join s in context.Shapes on aam.Shape equals s.ID
                    where annotationIds.Contains(aa.ID) 
                    select new GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape
                    {
                        ID = s.ID,
                        IsCustom = s.IsCustom,
                        SVG = s.SVG,
                        ApprovalAnnotation = aa.ID,
                        Color = aam.Color,
                        BackgroundColor = aam.BackgroundColor,
                        Rotation = aam.Rotation,
                        X = (double)aam.X,
                        Y = (double)aam.Y,
                        HighlighType = null
                    }
                      ).Union((from aa in context.ApprovalAnnotations where annotationIds.Contains(aa.ID) && aa.CommentType == (int)ApprovalCommentType.CommentType.TextComment
                               select new GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape
                               {
                                   ID = 0,
                                   IsCustom = true,
                                   SVG = aa.HighlightData,
                                   ApprovalAnnotation = aa.ID,
                                   Color = "#ff0000",
                                   BackgroundColor = "#ff0000",
                                   Rotation = 0,
                                   X = (double)0,
                                   Y = (double)0,
                                   HighlighType = aa.HighlightType.Value
                               })).ToList();
        }

        public static List<GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape> GetShapes(DbContextBL context,
                                                                                           int pageId, string user_key,
                                                                                           bool is_external_user,
                                                                                           string
                                                                                               proofStudioShowAnnotationsForExternalUsers)
        {
            try
            {

                List<ApprovalAnnotationShape> shapes = new List<ApprovalAnnotationShape>();

                var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
                var userCanViewAllPhasesAnnotations = !is_external_user ?
                                                       (from us in context.UserSettings
                                                        join u in context.Users on us.User equals u.ID
                                                        where
                                                            (u.Guid.ToLower() == user_key.ToLower()) &&
                                                            us.Setting == showAnnotationsFromAllPhases
                                                        select us.Value.ToLower() == "true").FirstOrDefault()
                                                       : (from ec in context.ExternalCollaborators
                                                          where (ec.Guid.ToLower() == user_key.ToLower())
                                                          select ec.ShowAnnotationsOfAllPhases).FirstOrDefault();
                var parents = (from a in context.Approvals
                               join ap in context.ApprovalPages on a.ID equals ap.Approval
                               join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                               where ap.ID == pageId && aa.Parent == null && aa.Phase == a.CurrentPhase
                               select aa.ID).ToList();

                if (is_external_user)
                {
                    bool showAllAnnotationsForExternalUsers =
                                                               (from accs in context.AccountSettings
                                                                join ec in context.ExternalCollaborators on accs.Account equals ec.Account
                                                                where
                                                                    ec.Guid.ToLower() == user_key.ToLower() &&
                                                                    accs.Name.ToLower() == proofStudioShowAnnotationsForExternalUsers.ToLower() &&
                                                                    accs.Value.ToLower() == "true"
                                                                select true).Take(1).FirstOrDefault();

                    shapes = (from aam in context.ApprovalAnnotationMarkups
                              join aa in context.ApprovalAnnotations on aam.ApprovalAnnotation equals aa.ID
                              join s in context.Shapes on aam.Shape equals s.ID
                              join ap in context.ApprovalPages on aa.Page equals ap.ID
                              join sa in context.SharedApprovals on ap.Approval equals sa.Approval
                              join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                              join a in context.Approvals on sa.Approval equals a.ID

                              let visiblePhases = (from aph in context.ApprovalJobPhases
                                                   join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                   where ajpa.Approval == a.ID && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == a.CurrentPhase)
                                                   select aph.ID).ToList()
                              where
                                  ap.ID == pageId && ec.Guid.ToLower() == user_key.ToLower() && ec.IsDeleted == false && ((a.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (
                                  (aa.ExternalCreator == ec.ID || showAllAnnotationsForExternalUsers) && a.CurrentPhase == aa.Phase && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                              select new GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape
                              {
                                  ID = s.ID,
                                  IsCustom = s.IsCustom,
                                  SVG = s.SVG,
                                  ApprovalAnnotation = aa.ID,
                                  Color = aam.Color,
                                  BackgroundColor = aam.BackgroundColor,
                                  Rotation = aam.Rotation,
                                  X = (double) aam.X,
                                  Y = (double) aam.Y,
                                  HighlighType = null
                              }).Union(
                                      (from aa in context.ApprovalAnnotations
                                       join ap in context.ApprovalPages on aa.Page equals ap.ID
                                       join sa in context.SharedApprovals on ap.Approval equals sa.Approval
                                       join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                       join a in context.Approvals on sa.Approval equals a.ID

                                       let visiblePhases = (from aph in context.ApprovalJobPhases
                                                            join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                            where ajpa.Approval == a.ID && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == a.CurrentPhase)
                                                            select aph.ID).ToList()
                                       where
                                           aa.Page == pageId &&
                                           aa.CommentType == (int) ApprovalCommentType.CommentType.TextComment &&
                                           ec.Guid.ToLower() == user_key.ToLower()  && ((a.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (
                                           (aa.ExternalCreator == ec.ID || showAllAnnotationsForExternalUsers) && a.CurrentPhase == aa.Phase  && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                                       select new GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape
                                       {
                                           ID = 0,
                                           IsCustom = true,
                                           SVG = aa.HighlightData,
                                           ApprovalAnnotation = aa.ID,
                                           Color = "#ff0000",
                                           BackgroundColor = "#ff0000",
                                           Rotation = 0,
                                           X = (double) 0,
                                           Y = (double) 0,
                                           HighlighType = aa.HighlightType.Value
                                       })).ToList();
                }
                else
                {
                    shapes = (from aam in context.ApprovalAnnotationMarkups
                              join aa in context.ApprovalAnnotations on aam.ApprovalAnnotation equals aa.ID
                              join s in context.Shapes on aam.Shape equals s.ID
                              join ap in context.ApprovalPages on aa.Page equals ap.ID
                              join a in context.Approvals on ap.Approval equals a.ID
                              let creatorIsCollaborator = (from ac2 in context.ApprovalCollaborators
                                                           where
                                                               ac2.Approval == ap.Approval &&
                                                               ac2.Collaborator == aa.Creator
                                                           select true).Take(1).FirstOrDefault()
                              let externalCreatorIsCollaborator = (from sa2 in context.SharedApprovals
                                                                   where
                                                                       sa2.Approval == ap.Approval &&
                                                                       sa2.ExternalCollaborator == aa.ExternalCreator
                                                                   select true).Take(1).FirstOrDefault()
                              let modifierIsCollaborator = (from ac2 in context.ApprovalCollaborators
                                                            where
                                                                ac2.Approval == ap.Approval &&
                                                                ac2.Collaborator == aa.Modifier
                                                            select true).Take(1).FirstOrDefault()
                              let externalModifierIsCollaborator = (from sa2 in context.SharedApprovals
                                                                    where
                                                                        sa2.Approval == ap.Approval &&
                                                                        sa2.ExternalCollaborator == aa.ExternalModifier
                                                                    select true).Take(1).FirstOrDefault()

                              let visiblePhases = (from aph in context.ApprovalJobPhases
                                                   join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                   where ajpa.Approval == a.ID && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == a.CurrentPhase)
                                                   select aph.ID).ToList()

                              where ap.ID == pageId  && ((a.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (
                                    (!aa.Creator.HasValue || creatorIsCollaborator) &&
                                    (!aa.ExternalCreator.HasValue || externalCreatorIsCollaborator) &&
                                    (!aa.Modifier.HasValue || modifierIsCollaborator) &&
                                    (!aa.ExternalModifier.HasValue || externalModifierIsCollaborator) && a.CurrentPhase == aa.Phase  && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                              select new GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape
                              {
                                  ID = s.ID,
                                  IsCustom = s.IsCustom,
                                  SVG = s.SVG,
                                  ApprovalAnnotation = aa.ID,
                                  Color = aam.Color,
                                  BackgroundColor = aam.BackgroundColor,
                                  Rotation = aam.Rotation,
                                  X = (double) aam.X,
                                  Y = (double) aam.Y,
                                  HighlighType = null
                              }
                             ).Union((from aa in context.ApprovalAnnotations
                                      join ap in context.ApprovalPages on aa.Page equals ap.ID
                                      join a in context.Approvals on ap.Approval equals a.ID

                                      let creatorIsCollaborator = (from ac2 in context.ApprovalCollaborators
                                                                   where
                                                                       ac2.Approval == ap.Approval &&
                                                                       ac2.Collaborator == aa.Creator
                                                                   select true).Take(1).FirstOrDefault()
                                      let externalCreatorIsCollaborator = (from sa2 in context.SharedApprovals
                                                                           where
                                                                               sa2.Approval == ap.Approval &&
                                                                               sa2.ExternalCollaborator ==
                                                                               aa.ExternalCreator
                                                                           select true).Take(1).FirstOrDefault()
                                      let modifierIsCollaborator = (from ac2 in context.ApprovalCollaborators
                                                                    where
                                                                        ac2.Approval == ap.Approval &&
                                                                        ac2.Collaborator == aa.Modifier
                                                                    select true).Take(1).FirstOrDefault()
                                      let externalModifierIsCollaborator = (from sa2 in context.SharedApprovals
                                                                            where
                                                                                sa2.Approval == ap.Approval &&
                                                                                sa2.ExternalCollaborator ==
                                                                                aa.ExternalModifier
                                                                            select true).Take(1).FirstOrDefault()
                                      let visiblePhases = (from aph in context.ApprovalJobPhases
                                                           join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                           where ajpa.Approval == a.ID && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == a.CurrentPhase)
                                                           select aph.ID).ToList()
                                      where
                                          aa.Page == pageId &&
                                          aa.CommentType == (int) ApprovalCommentType.CommentType.TextComment && ((a.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (

                                          (!aa.Creator.HasValue || creatorIsCollaborator) &&
                                          (!aa.ExternalCreator.HasValue || externalCreatorIsCollaborator) &&
                                          (!aa.Modifier.HasValue || modifierIsCollaborator) &&
                                          (!aa.ExternalModifier.HasValue || externalModifierIsCollaborator) && a.CurrentPhase == aa.Phase && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                                      select new GMGColorDAL.CustomModels.Api.ApprovalAnnotationShape
                                      {
                                          ID = 0,
                                          IsCustom = true,
                                          SVG = aa.HighlightData,
                                          ApprovalAnnotation = aa.ID,
                                          Color = "#ff0000",
                                          BackgroundColor = "#ff0000",
                                          Rotation = 0,
                                          X = (double) 0,
                                          Y = (double) 0,
                                          HighlighType = aa.HighlightType.Value
                                      })).ToList();
                }

                foreach (var shape in shapes)
                {
                    if (shape.ID == 0)
                    {
                        shape.ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
                    }
                }
                return shapes;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetShapesPerApproval, ex);
            }
        }

        public static List<int> GetAnnotationChilds(DbContextBL context, int annotationId)
        {
            return (from aa in context.ApprovalAnnotations where aa.Parent == annotationId select aa.ID).ToList();
        }

        public static bool DeleteAnnotation(DbContextBL context, int? annotationID, string userkey,
                                            bool is_external_user, bool saveChanges = true)
        {
            try
            {
                if (!annotationID.HasValue)
                {
                    return false;
                }

                var annotation = (from an in context.ApprovalAnnotations
                                  where an.ID == annotationID
                                  select an).Take(1).SingleOrDefault();

                if (annotation == null)
                {
                    return false;
                }

                if (!HasAccessToDeleteAnnotation(is_external_user, annotationID.Value, userkey, context))
                {
                    return false;
                }

                // Delete attachment
                //objApprovalAnnotation.ApprovalAnnotationAttachments.Clear();
                List<GMGColorDAL.ApprovalAnnotationAttachment> lstAnnotationAttachments =
                    (from anat in context.ApprovalAnnotationAttachments
                     where anat.ApprovalAnnotation == annotation.ID
                     select anat).ToList();

                foreach (
                    GMGColorDAL.ApprovalAnnotationAttachment item in
                        lstAnnotationAttachments.GetRange(0, lstAnnotationAttachments.Count))
                {
                    DALUtils.Delete<GMGColorDAL.ApprovalAnnotationAttachment>(context, item);
                }

                //Delete ChecklistItems
                //objApprovalAnnotation.ApprovalAnnotationChecklistItems.Clear();
                List<GMGColorDAL.ApprovalAnnotationCheckListItem> lstAnnotationChecklists =
                   (from anci in context.ApprovalAnnotationCheckListItems
                    where anci.ApprovalAnnotation == annotation.ID
                    select anci).ToList();

                foreach (
                    GMGColorDAL.ApprovalAnnotationCheckListItem item in
                        lstAnnotationChecklists.GetRange(0, lstAnnotationChecklists.Count))
                {
                    DALUtils.Delete<GMGColorDAL.ApprovalAnnotationCheckListItem>(context, item);
                }

                List<GMGColorDAL.ApprovalAnnotationChecklistItemHistory> lstAnnotationChecklistsHistory =
                   (from anch in context.ApprovalAnnotationChecklistItemHistories
                    where anch.ApprovalAnnotation == annotation.ID
                    select anch).ToList();

                foreach (
                   GMGColorDAL.ApprovalAnnotationChecklistItemHistory item in
                                 lstAnnotationChecklistsHistory.GetRange(0, lstAnnotationChecklistsHistory.Count))
                {
                   DALUtils.Delete<GMGColorDAL.ApprovalAnnotationChecklistItemHistory>(context, item);
                }  

                // Delete Markup
                //objApprovalAnnotation.ApprovalAnnotationMarkups.Clear();
                List<GMGColorDAL.ApprovalAnnotationMarkup> lstAnnotationMarkups =
                    (from anmk in context.ApprovalAnnotationMarkups
                     where anmk.ApprovalAnnotation == annotation.ID
                     select anmk).ToList();

                List<int?> shapesID = lstAnnotationMarkups.Select(o => o.Shape).ToList();

                List<GMGColorDAL.Shape> lstShapes = (from shp in context.Shapes
                                                     where shapesID.Contains(shp.ID) && shp.IsCustom == true
                                                     select shp).ToList();

                foreach (
                    GMGColorDAL.ApprovalAnnotationMarkup item in
                        lstAnnotationMarkups.GetRange(0, lstAnnotationMarkups.Count))
                {
                    DALUtils.Delete<GMGColorDAL.ApprovalAnnotationMarkup>(context, item);
                }

                // Delete Collaborators
                //  objApprovalAnnotation.ApprovalAnnotationPrivateCollaborators.Clear();
                List<GMGColorDAL.ApprovalAnnotationPrivateCollaborator> lstAnnotationPrivateColloabs =
                    (from anpc in context.ApprovalAnnotationPrivateCollaborators
                     where anpc.ApprovalAnnotation == annotation.ID
                     select anpc).ToList();
                foreach (
                    GMGColorDAL.ApprovalAnnotationPrivateCollaborator item in
                        lstAnnotationPrivateColloabs.GetRange(0, lstAnnotationPrivateColloabs.Count))
                {
                    DALUtils.Delete<GMGColorDAL.ApprovalAnnotationPrivateCollaborator>(context, item);
                }

                //Delete Shapes
                foreach (GMGColorDAL.Shape item in lstShapes.GetRange(0, lstShapes.Count))
                {
                    DALUtils.Delete<GMGColorDAL.Shape>(context, item);
                }

                //Delete ApprovalAnnotationStatusChangeLog
                foreach (
                    GMGColorDAL.ApprovalAnnotationStatusChangeLog item in
                        annotation.ApprovalAnnotationStatusChangeLogs.ToList())
                {
                    context.ApprovalAnnotationStatusChangeLogs.Remove(item);
                }

                DALUtils.Delete<GMGColorDAL.ApprovalAnnotation>(context, annotation);

                if (saveChanges)
                {
                    context.SaveChanges();
                }
                return true;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteAnnotation, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteAnnotation, ex);
            }
        }

        public static string GetVideoFilePath(int pageId, string userKey, bool isExternaluser, string accountRegion, string approvalGuid, string approvalFileName, DbContextBL context)
        {
            string pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + approvalGuid;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                pageRelativePath = Path.Combine(pageRelativePath, approvalFileName);
                if(GMGColorIO.FileExists(pageRelativePath, accountRegion))
                {
                    string domain = isExternaluser ? (from ec in context.ExternalCollaborators
                                                      join a in context.Accounts on ec.Account equals a.ID
                                                      where ec.Guid == userKey
                                                      select a.Domain).FirstOrDefault()
                        :(from u in context.Users
                                     join a in context.Accounts on u.Account equals a.ID
                                     where u.Guid == userKey
                                     select a.Domain).FirstOrDefault();

                    return GMGColorCommon.GetDataFolderHttpPath(accountRegion, domain);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var videoApprovalPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + pageRelativePath;

                var videoFilePath = Path.Combine(videoApprovalPath, approvalFileName);

                if (Directory.Exists(videoApprovalPath) && File.Exists(videoFilePath))
                {
                    return videoFilePath;
                }
                else
                {
                    return null;
                }

            }
        }

        public static string GetHTMLFilePath(int pageId, string userKey, bool isExternaluser, string accountRegion, string approvalGuid, string approvalFileName, DbContextBL context)
        {
            string pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + approvalGuid;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                pageRelativePath = Path.Combine(pageRelativePath, approvalFileName);
                if (GMGColorIO.FileExists(pageRelativePath, accountRegion))
                {
                    string domain = isExternaluser ? (from ec in context.ExternalCollaborators
                                                      join a in context.Accounts on ec.Account equals a.ID
                                                      where ec.Guid == userKey
                                                      select a.Domain).FirstOrDefault()
                        : (from u in context.Users
                           join a in context.Accounts on u.Account equals a.ID
                           where u.Guid == userKey
                           select a.Domain).FirstOrDefault();

                    return GMGColorCommon.GetDataFolderHttpPath(accountRegion, domain);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var videoApprovalPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + pageRelativePath;

                var videoFilePath = Path.Combine(videoApprovalPath, approvalFileName);

                if (Directory.Exists(videoApprovalPath) && File.Exists(videoFilePath))
                {
                    return videoFilePath;
                }
                else
                {
                    return null;
                }

            }
        }

        
        public static string GetVideoAnnotationOutputPath(int pageId, string userKey, bool isExternaluser, string accountRegion, string approvalGuid, DbContextBL context)
        {
            string pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + approvalGuid + @"\" + "1";
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
               string outputPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content\\VideoAnnotationReportFiles");
                if(!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }
                return outputPath;
            }
            else
            {
                var videoAnnotationFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + pageRelativePath;
                
                if (Directory.Exists(videoAnnotationFolder))
                {
                    return videoAnnotationFolder;
                }
                else
                {
                    Directory.CreateDirectory(videoAnnotationFolder);
                    return Directory.Exists(videoAnnotationFolder) ? videoAnnotationFolder : null;
                }

            }
        }

        public static string GetHTMLAnnotationOutputPath(int pageNumber ,int pageId, string userKey, bool isExternaluser, string accountRegion, string approvalGuid, DbContextBL context)
        {
            string pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + approvalGuid + @"\" + pageNumber;
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                string outputPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content\\VideoAnnotationReportFiles");
                if (!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }
                return outputPath;
            }
            else
            {
                var videoAnnotationFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + pageRelativePath;

                if (Directory.Exists(videoAnnotationFolder))
                {
                    return videoAnnotationFolder;
                }
                else
                {
                    Directory.CreateDirectory(videoAnnotationFolder);
                    return Directory.Exists(videoAnnotationFolder) ? videoAnnotationFolder : null;
                }

            }
        }

        public static bool PushVideoFrameToS3Bucket(string LocaleFilePath, string accountRegion, string approvalGuid, string fileName, DbContextBL context)
        {
            bool success = false;

            string outputPath =  "approvals" + "/" + approvalGuid + "/" + "1/";

            bool folderExists = GMGColorIO.FolderExists(outputPath, accountRegion, true);

            using (FileStream stream = new FileStream(LocaleFilePath, FileMode.Open, FileAccess.Read))
            {
                success = GMGColorIO.UploadFile(stream, outputPath + fileName, accountRegion, false);
            }
                               
            return success;
        }

        public static void RemoveVideoFrameFromLocale(string outputFilePath)
        {
            if(File.Exists(outputFilePath))
            {
                File.Delete(outputFilePath);
            }
        }

        public static void GrabInstanceOfComment(DbContextBL context, AnnotationRequest model)
        {
            try
            {
                int timeFrame = (int)model.annotation.TimeFrame - 85000 > 0  ? (int)model.annotation.TimeFrame - 85000 : (int)model.annotation.TimeFrame;

                int ss = timeFrame / 1000000;
                long ms = timeFrame % 1000000;
                long oness = (ms / 100000);
                long onems = ms % 100000;

                long twoss = onems / 10000;
                long twoms = onems % 10000;

                long threess = twoms / 1000;
                long threems = twoms % 1000;

                long fourss = threems / 100;
                long fourms = threems % 100;

                long fivess = fourms / 10;
                long fivems = (fourms % 10);

                string accountRegion = !model.is_external_user ? (from u in context.Users
                                                                  join a in context.Accounts on u.Account equals a.ID
                                                                  where u.Guid == model.user_key
                                                                  select a.Region).FirstOrDefault()
                                                  : (from ex in context.ExternalCollaborators
                                                     join a in context.Accounts on ex.Account equals a.ID
                                                     where ex.Guid == model.user_key
                                                     select a.Region).FirstOrDefault();

                var approvalDetails = (from a in context.Approvals
                                       join ap in context.ApprovalPages on a.ID equals ap.Approval
                                       where ap.ID == model.annotation.Page
                                       select new { a.Guid, a.FileName }).FirstOrDefault();
                               
                string approvalPath = GetVideoFilePath(model.annotation.Page, model.user_key, model.is_external_user, accountRegion, approvalDetails.Guid, approvalDetails.FileName, context);
                string outputFolderPath = GetVideoAnnotationOutputPath(model.annotation.Page, model.user_key, model.is_external_user, accountRegion, approvalDetails.Guid, context);

                if (approvalPath != null && outputFolderPath != null)
                {
                    string outputFileName = model.annotation.Page.ToString() + "_" + model.annotation.TimeFrame.ToString() + ".jpg";
                    string outputPath = Path.Combine(outputFolderPath, outputFileName);
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        string outputPathInS3Bucket = "approvals" + "/" + approvalDetails.Guid + "/1/" + outputFileName;
                        if (!GMGColorIO.FileExists(outputPathInS3Bucket, accountRegion))
                        {
                            string DataFolderHttpPath = approvalPath;
                            approvalPath = DataFolderHttpPath + "approvals" + "/" + approvalDetails.Guid + "/Converted/" + HttpUtility.UrlEncode(Path.ChangeExtension(approvalDetails.FileName, "mp4"));

                            using (var process = new Process())
                            {
                                var startInfo = process.StartInfo;
                                startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                                // Add ffmpeg.exe file in the applicataion folder
                                startInfo.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Tools\\ffmpeg\\ffmpeg.exe");

                                //ffmpeg arguments callings to grab the JPEG file from video file
                                startInfo.Arguments = "-i \"" + approvalPath + "\" -ss " + ss + "." + oness + "" + twoss + "" + threess + "" + fourss + "" + fivess + "" + fivems + " -vframes 1 \"" + outputPath + "\"";
                                startInfo.UseShellExecute = false;
                                process.Start();
                                process.WaitForExit();
                                process.Close();
                            }
                            if (File.Exists(outputPath))
                            {
                                bool success = PushVideoFrameToS3Bucket(outputPath, accountRegion, approvalDetails.Guid, outputFileName, context);
                                if (success)
                                {
                                    RemoveVideoFrameFromLocale(outputPath);
                                }
                            }
                        }
                    }
                    else
                    { 
                      if (!File.Exists(outputPath))
                      {
                        using (var process = new Process())
                        {
                            var startInfo = process.StartInfo;
                            startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                            // Add ffmpeg.exe file in the applicataion folder
                            startInfo.FileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Tools\\ffmpeg\\ffmpeg.exe");

                            //ffmpeg arguments callings to grab the JPEG file from video file
                            startInfo.Arguments = "-i " + approvalPath + " -ss " + ss + "." + oness + "" + twoss + "" + threess + "" + fourss + "" + fivess + "" + fivems + " -vframes 1 " + outputPath + "";
                            startInfo.UseShellExecute = false;
                            process.Start();
                            process.WaitForExit();
                            process.Close();
                        }
                     }
                   }
                } else
                    {
                      throw new ExceptionBL(ExceptionBLMessages.CouldNotGrabVideoInstance);
                    }
               }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGrabVideoInstance, ex);
            }
        }

        public static  void ConvertBase64ToJPG(DbContextBL context, AnnotationRequest model, AnnotationsResponse annotationsResponseData)
        {
            try
            {
                model.annotation.base64 = model.annotation.base64.Substring(22);
                string accountRegion = !model.is_external_user ? (from u in context.Users
                                                                  join a in context.Accounts on u.Account equals a.ID
                                                                  where u.Guid == model.user_key
                                                                  select a.Region).FirstOrDefault()
                                                  : (from ex in context.ExternalCollaborators
                                                     join a in context.Accounts on ex.Account equals a.ID
                                                     where ex.Guid == model.user_key
                                                     select a.Region).FirstOrDefault();



                var approvalDetails = (from a in context.Approvals
                                       join ap in context.ApprovalPages on a.ID equals ap.Approval
                                       where ap.ID == model.annotation.Page
                                       select new { a.Guid, a.FileName, ap.Number }).FirstOrDefault();

                string approvalPath = GetHTMLFilePath(model.annotation.Page, model.user_key, model.is_external_user, accountRegion, approvalDetails.Guid, approvalDetails.FileName, context);
                string outputFolderPath = GetHTMLAnnotationOutputPath(approvalDetails.Number, model.annotation.Page, model.user_key, model.is_external_user, accountRegion, approvalDetails.Guid, context);

                    string outputFileName= "";
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        outputFileName = annotationsResponseData.AnnotationId + ".jpeg";
                            }
                    else {
                        outputFileName = annotationsResponseData.AnnotationId + ".jpg";
                    }


                    string outputPath = Path.Combine(outputFolderPath, outputFileName);
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                    string pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + approvalDetails.Guid + @"\" + approvalDetails.Number;
                    var bytes = Convert.FromBase64String(model.annotation.base64);
                    System.Drawing.Image image;
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        image = System.Drawing.Image.FromStream(ms);

                    }
                    var stream = new System.IO.MemoryStream();
                    image.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    stream.Position = 0;
                    GMGColorIO.UploadFile(stream, pageRelativePath + @"\" + annotationsResponseData.AnnotationId + ".jpeg" , accountRegion, false);

                }

                else if(outputFolderPath != null && outputPath != null)
                    {
                        if (!File.Exists(outputFolderPath))
                        {

                            //Check if directory exist
                            if (!System.IO.Directory.Exists(outputFolderPath))
                            {
                                System.IO.Directory.CreateDirectory(outputFolderPath); //Create directory if it doesn't exist
                            }
                            byte[] imageBytes = Convert.FromBase64String(model.annotation.base64);

                            File.WriteAllBytes(outputPath, imageBytes);
                        }
                    }
                else
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotGrabVideoInstance);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGrabVideoInstance, ex);
            }
        }

        public static int GetHtmlPageId(int approvalId)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    var pageIds = (from ap in context.ApprovalPages
                             where ap.Approval == approvalId
                             select ap.ID).ToList();
                    if (pageIds.Count == 1)
                    {
                        return pageIds[0];
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotLoadApprovalFirstPageID, ex);
            }
        }

        public static void SaveSceenshot(DbContextBL context, AnnotationRequest model, AnnotationsResponse annotationdetails)
        {
            
            string path = "c:"  + @"\Projects" + @"\GMGCoZone"  + @"\src" + @"\gmgcozonedata" + @"\" + GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\4cb98a13-2b74-463d-a698-40eabc1b1027" + @"\1";

            //Check if directory exist
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
            }

            string imageName = "test.jpg";

            //set the image path
            string imgPath = Path.Combine(path, imageName);

            byte[] imageBytes = Convert.FromBase64String(model.annotation.base64);

            File.WriteAllBytes(imgPath, imageBytes);
        }

        public static void UpdateSelectedHtmlPageHeightWidth(HtmlPageRequest model, DbContextBL context)
        {
            try
            {
                var approvalPage = (from a in context.Approvals
                                    join ap in context.ApprovalPages on a.ID equals ap.Approval
                                    where a.ID == model.ApprovalId && ap.ID == model.Page
                                    select ap).FirstOrDefault();
                approvalPage.OriginalImageHeight = model.Height;
                approvalPage.OriginalImageWidth = model.Width;
                approvalPage.OutputRenderHeight = model.Height;
                approvalPage.OutputRenderWidth = model.Width;

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.UpdateSelectedHtmlPageHeightWidth, ex);
            }
        }
        public static AnnotationsResponse AddOrEditAnnotation(DbContextBL context, AnnotationRequest model)
        {
            try
            {
                var annotationDetails = (from aj in context.Approvals
                                         join j in context.Jobs on aj.Job equals j.ID
                                         join a in context.Accounts on j.Account equals a.ID
                                         join ap in context.ApprovalPages on aj.ID equals ap.Approval
                                         where ap.ID == model.annotation.Page && aj.IsDeleted == false
                                         select new
                                         {
                                             Approval = aj.ID,
                                             a.TimeZone,
                                             aj.CurrentPhase,
                                             Job = model.Id == null ? j : null
                                         }).FirstOrDefault();

                if(model.annotation.Phase != annotationDetails.CurrentPhase && model.annotation.IsSamePage)
                {
                    AnnotationsResponse response = new AnnotationsResponse();
                    response.IsDifferentPhase = true;
                    return response;
                }

            if(model.annotation.OriginalVideoHeight != null && model.annotation.OriginalVideoWidth != null)
                {
                    var approvalPageInfo = (from ap in context.ApprovalPages where ap.ID == model.annotation.Page select ap).FirstOrDefault();
                    if(approvalPageInfo.OutputRenderHeight == null && approvalPageInfo.OutputRenderWidth == null)
                    {
                        approvalPageInfo.OutputRenderHeight = model.annotation.OriginalVideoHeight;
                        approvalPageInfo.OutputRenderWidth = model.annotation.OriginalVideoWidth;
                    }
                }

                if (model.annotation.ID == 0)
                {
                    var annotationModel = model.annotation;
                    var machineId = model.machineId ?? string.Empty;

                    if (!HasAccessToAnnotations(model.is_external_user, annotationModel.Page, model.user_key, context))
                    {
                        return null;
                    }

                    var annotationsResponse = new AnnotationsResponse();
                    if (
                        !ValidateUser(context, annotationModel.ExternalModifier.HasValue,
                                      annotationModel.ExternalCreator.HasValue
                                          ? annotationModel.ExternalCreator.Value
                                          : annotationModel.Creator.GetValueOrDefault()) ||
                        !ValidateUser(context, annotationModel.ExternalModifier.HasValue,
                                      annotationModel.ExternalModifier.HasValue
                                          ? annotationModel.ExternalModifier.Value
                                          : annotationModel.Modifier.GetValueOrDefault()))
                    {
                        return null;
                    }

                    ApprovalAnnotation annotation = null;
                    DateTime annotationDate = DateTime.UtcNow;


                    var level = 0;


                    //check if the annotation was added on a softproofing session
                    if (!String.IsNullOrEmpty(machineId))
                    {
                        level = model.annotation.CalibrationStatus;
                    }

                    var loggedUserInternal = model.is_external_user ? null: context.Users.FirstOrDefault(u => u.Guid == model.user_key);
                    var loggedUserExternal = model.is_external_user ? context.ExternalCollaborators.FirstOrDefault(u => u.Guid == model.user_key) : null;

                    var loggedUser = new UserDetails();
                    if (model.is_external_user)
                    {
                        loggedUser.Id = loggedUserExternal.ID;
                        loggedUser.Email = loggedUserExternal.EmailAddress;
                    }
                    else
                    {
                        loggedUser.Id = loggedUserInternal.ID;
                        loggedUser.Email = loggedUserInternal.EmailAddress;
                        loggedUser.Username = loggedUserInternal.Username;
                    }

                    if (model.Id == null)
                    {
                        #region Create Annotation

                        annotation = new ApprovalAnnotation();
                        //TODO check if user has access to approval page
                        //TODO check if approval page number exists
                        //TODO check why status should be NEW

                        if (annotationDetails.CurrentPhase != null)
                        {
                            try
                            {

                                var jobDetails = new JobStatusDetails()
                                {
                                    approvalGuid = annotationDetails.Approval.ToString(),
                                    phaseName = annotationDetails.CurrentPhase.ToString(),
                                    username = loggedUser.Email
                                };
                                ApprovalBL.PushCallBackTrigger(jobDetails, annotationDetails.Job.Account, context, (int)CallBackType.OnAnnotation);

                            }
                            catch (Exception exc)
                            {
                                GMGColorDAL.GMGColorLogging.log.Error(exc.Message, exc);
                            }
                        }


                        if (annotationModel.Parent > 0)
                        {
                            annotation.Parent = annotationModel.Parent;

                            GMGColorDAL.ApprovalAnnotation parent = (from ap in context.ApprovalAnnotations
                                                                     where ap.ID == annotationModel.Parent.Value
                                                                     select ap).Take(1).SingleOrDefault();
                            //TODO check if parent is null and handle this situation
                            List<ApprovalAnnotation> childs = parent.GetChilds(context);
                            if (childs.Any())
                            {
                                annotation.OrderNumber = (from o in childs select o.OrderNumber).Max() + 1;
                            }
                            else
                            {
                                annotation.OrderNumber = 1;
                            }
                        }
                        else
                        {
                            GMGColorDAL.ApprovalPage page = new GMGColorDAL.ApprovalPage();
                            page.ID = annotationModel.Page;

                            if (page.ApprovalAnnotations.Count > 0)
                            {
                                annotation.OrderNumber = (from o in page.ApprovalAnnotations
                                                          where o.Parent == 0
                                                          select o.OrderNumber).Max() + 1;
                            }
                            else
                            {
                                annotation.OrderNumber = 1;
                            }
                        }

                        if (!string.IsNullOrEmpty(annotationModel.SPPSessionGUID))
                        {
                            var viewingCond = (from sp in context.SoftProofingSessionParams
                                               where sp.SessionGuid == annotationModel.SPPSessionGUID
                                               select sp.ViewingCondition).FirstOrDefault();

                            annotation.ViewingCondition = viewingCond;
                        }

                        if (string.IsNullOrEmpty(annotation.Guid))
                        {
                            annotation.Guid = Guid.NewGuid().ToString();
                        }
                        annotation.Creator = annotationModel.Creator;
                        annotation.Modifier = annotationModel.Modifier;
                        annotation.ExternalCreator = annotationModel.ExternalCreator;
                        annotation.ExternalModifier = annotationModel.ExternalModifier;

                        //TODO check if annotation status exists
                        annotation.Status = annotationModel.Status;
                        annotation.Page = annotationModel.Page;
                        annotation.ModifiedDate = DateTime.UtcNow;

                        if (System.Text.RegularExpressions.Regex.Replace(annotationModel.Comment, @"<(.|\n)*?>", "") != string.Empty)
                        {
                            annotation.Comment = annotationModel.Comment;
                        }
                        else
                        {
                            annotation.Comment = "<p> </p>";
                        }


                        //TODO check if comment type exists
                        annotation.CommentType = annotationModel.CommentType;
                        annotation.ReferenceFilepath = annotationModel.ReferenceFilePath;
                        annotation.AnnotatedDate = DateTime.UtcNow;
                        annotation.Phase = annotationDetails.CurrentPhase;
                        if (annotationModel.Parent > 0)
                        {
                            var parentAnnotation = (from ann in context.ApprovalAnnotations where ann.ID == annotationModel.Parent select ann.IsPrivateAnnotation).FirstOrDefault();
                            annotation.IsPrivateAnnotation = parentAnnotation;
                        }
                        else
                        {
                            annotation.IsPrivateAnnotation = annotationModel.IsPrivateAnnotation;
                        }

                        if (annotationModel.HighlightType.HasValue && annotationModel.HighlightType.Value > 0)
                        {
                            //TODO check if HightlightType exists
                            annotation.HighlightType = annotationModel.HighlightType;
                        }
                        if (annotationModel.CrosshairXCoord != null)
                        {
                            annotation.CrosshairXCoord = annotationModel.CrosshairXCoord;
                        }
                        if (annotationModel.CrosshairYCoord != null)
                        {
                            annotation.CrosshairYCoord = annotationModel.CrosshairYCoord;
                        }
                        if (annotationModel.StartIndex != null)
                        {
                            annotation.StartIndex = annotationModel.StartIndex;
                        }
                        if (annotationModel.EndIndex != null)
                        {
                            annotation.EndIndex = annotationModel.EndIndex;
                        }
                        if (annotationModel.OriginalVideoWidth != null)
                        {
                            annotation.OriginalVideoWidth = annotationModel.OriginalVideoWidth;
                        }
                        if (annotationModel.OriginalVideoHeight != null)
                        {
                            annotation.OriginalVideoHeight = annotationModel.OriginalVideoHeight;
                        }

                        annotation.HighlightData = annotationModel.HighlightData;
                        annotation.IsPrivate = annotationModel.IsPrivate;
                        annotation.TimeFrame = annotationModel.TimeFrame;
                        annotation.machineId = machineId;
                        annotation.CalibrationStatus = level;
                        if(annotationModel.IsVideo)
                        {
                            annotation.ShowVideoReport = true;
                        }
                        if (annotationModel.OriginalVideoHeight != null && annotationModel.OriginalVideoWidth != null)
                        {
                            annotation.AnnotatedOnImageWidth = (int)annotationModel.OriginalVideoWidth;
                        }
                        else
                        {
                        annotation.AnnotatedOnImageWidth = annotationModel.AnnotatedOnImageWidth;
                        }

                        #region Add Markup

                        if (annotationModel.Shapes != null)
                        {
                            foreach (var obj in annotationModel.Shapes)
                            {
                                GMGColorDAL.ApprovalAnnotationMarkup markup = new GMGColorDAL.ApprovalAnnotationMarkup();
                                markup.X = Convert.ToDecimal(obj.X);
                                markup.Y = Convert.ToDecimal(obj.Y);
                                markup.Height = Convert.ToDecimal(obj.ScaleY);
                                markup.Width = Convert.ToDecimal(obj.ScaleX);
                                markup.Color = obj.Color;

                                if (obj.Size != null)
                                    markup.Size = obj.Size;

                                markup.BackgroundColor = obj.BackgroundColor;
                                markup.Rotation = obj.Rotation;


                                GMGColorDAL.Shape shape = new GMGColorDAL.Shape();
                                shape.IsCustom = true;
                                shape.FXG = obj.FXG;
                                shape.SVG = obj.SVG;

                                shape.ApprovalAnnotationMarkups.Add(markup);
                                context.Shapes.Add(shape);

                                annotation.ApprovalAnnotationMarkups.Add(markup);
                                context.ApprovalAnnotationMarkups.Add(markup);
                            }
                        }

                        #endregion

                        #region Add Private Collaborators

                        if (annotationModel.ApprovalAnnotationPrivateCollaborators != null)
                        {
                            foreach (
                                GMGColorDAL.CustomModels.Api.ApprovalAnnotationPrivateCollaborator collaborator in
                                    annotationModel.ApprovalAnnotationPrivateCollaborators)
                            {
                                GMGColorDAL.ApprovalAnnotationPrivateCollaborator pCollab =
                                    new GMGColorDAL.ApprovalAnnotationPrivateCollaborator();
                                pCollab.Collaborator = collaborator.Collaborator;
                                pCollab.AssignedDate = String2DateTime(collaborator.AssignedDate);

                                annotation.ApprovalAnnotationPrivateCollaborators.Add(pCollab);
                                context.ApprovalAnnotationPrivateCollaborators.Add(pCollab);
                            }
                        }

                        #endregion

                        #region AnnotationAttachments

                        if (annotationModel.AnnotationAttachments != null)
                        {
                            foreach (ApprovalAnnotationAttachment attachment in annotationModel.AnnotationAttachments)
                            {
                                var attachmentdb = new GMGColorDAL.ApprovalAnnotationAttachment()
                                {
                                    DisplayName = attachment.DisplayName ?? attachment.Name,
                                    Name = attachment.Name,
                                    Guid = attachment.Guid
                                };
                                annotation.ApprovalAnnotationAttachments.Add(attachmentdb);
                                context.ApprovalAnnotationAttachments.Add(attachmentdb);
                            }
                        }

                        #endregion

                        context.ApprovalAnnotations.Add(annotation);

                        #endregion
                    }
                    else
                    {
                        #region Update Annotation

                        annotation = (from an in context.ApprovalAnnotations
                                      where an.ID == model.Id.Value
                                      select an).Take(1).FirstOrDefault();

                        if (annotation == null)
                            return null;

                        if (
                            !ValidateUser(context, annotationModel.ExternalModifier.HasValue,
                                          annotationModel.ExternalModifier.HasValue
                                              ? annotationModel.ExternalModifier.Value
                                              : annotationModel.Modifier.GetValueOrDefault()) ||
                            !ValidateUser(context, annotationModel.ExternalCreator.HasValue,
                                          annotationModel.ExternalCreator.HasValue
                                              ? annotationModel.ExternalCreator.Value
                                              : annotationModel.Creator.GetValueOrDefault())
                            )
                        {
                            return null;
                        }

                        //TODO check if status exists
                        annotation.Status = annotationModel.Status;
                        if (annotationModel.Parent.HasValue && annotationModel.Parent > 0)
                        {
                            //TODO check if annotation parent exists and current user has right to use it
                            annotation.Parent = annotationModel.Parent;
                        }
                        annotation.Page = annotationModel.Page;
                        annotation.ModifiedDate = DateTime.UtcNow;
                        annotation.Comment = annotationModel.Comment;
                        annotation.ReferenceFilepath = annotationModel.ReferenceFilePath;
                        //TODO check if comment type exists
                        annotation.CommentType = annotationModel.CommentType;
                        annotation.StartIndex = annotationModel.StartIndex;
                        annotation.EndIndex = annotationModel.EndIndex;
                        annotation.Phase = annotationDetails.CurrentPhase;

                        if (annotationModel.ExternalModifier.HasValue)
                        {
                            annotation.Modifier = null;
                            annotation.ExternalModifier = annotationModel.ExternalModifier;
                        }
                        else
                        {
                            annotation.ExternalModifier = null;
                            annotation.Modifier = annotationModel.Modifier;
                        }

                        if (annotationModel.HighlightType.HasValue && annotationModel.HighlightType.Value > 0)
                        {
                            //TODO check if HightlightType exists
                            annotation.HighlightType = annotationModel.HighlightType;
                        }

                        annotation.CrosshairXCoord = annotationModel.CrosshairXCoord;
                        annotation.CrosshairYCoord = annotationModel.CrosshairYCoord;
                        annotation.IsPrivate = annotationModel.IsPrivate;
                        annotation.TimeFrame = annotationModel.TimeFrame;
                        annotation.machineId = machineId;
                        annotation.CalibrationStatus = (int) level; //TODO dddd
                        annotation.AnnotatedOnImageWidth = annotationModel.AnnotatedOnImageWidth;
                        annotation.IsPrivateAnnotation = annotationModel.IsPrivateAnnotation;

                        #region Update Private Collaborators

                        // Identify the deleted  private collaborators
                        List<GMGColorDAL.ApprovalAnnotationPrivateCollaborator> deletedPrivateList =
                            new List<GMGColorDAL.ApprovalAnnotationPrivateCollaborator>();
                        foreach (
                            GMGColorDAL.ApprovalAnnotationPrivateCollaborator obj in
                                annotation.ApprovalAnnotationPrivateCollaborators)
                        {
                            if (!(from o in annotationModel.ApprovalAnnotationPrivateCollaborators
                                  where o.ID == obj.ID
                                  select o).Any())
                            {
                                deletedPrivateList.Add(obj);
                            }
                        }

                        // Add Private Collaborators
                        if (annotationModel.ApprovalAnnotationPrivateCollaborators != null)
                        {
                            foreach (
                                GMGColorDAL.CustomModels.Api.ApprovalAnnotationPrivateCollaborator obj in
                                    annotationModel.ApprovalAnnotationPrivateCollaborators)
                            {
                                GMGColorDAL.ApprovalAnnotationPrivateCollaborator pCollab =
                                    (from anpc in context.ApprovalAnnotationPrivateCollaborators
                                     where anpc.ID == obj.ID
                                     select anpc).Take(1).FirstOrDefault();
                                if (pCollab == null)
                                {
                                    pCollab = new GMGColorDAL.ApprovalAnnotationPrivateCollaborator();
                                }

                                pCollab.Collaborator = pCollab.Collaborator; //TODO ?!
                                pCollab.AssignedDate = pCollab.AssignedDate; //TODO ?!

                                if (obj.ID == 0)
                                {
                                    annotation.ApprovalAnnotationPrivateCollaborators.Add(pCollab);
                                    context.ApprovalAnnotationPrivateCollaborators.Add(pCollab);
                                }
                            }
                        }

                        // Now delete the identified ones
                        for (int i = deletedPrivateList.Count - 1; i >= 0; i--)
                        {
                            GMGColorDAL.ApprovalAnnotationPrivateCollaborator obj = deletedPrivateList[i];
                            DALUtils.Delete<GMGColorDAL.ApprovalAnnotationPrivateCollaborator>(context, obj);
                        }

                        #endregion

                        #endregion
                    }
                    context.SaveChanges();
                    annotationModel.ID = annotation.ID;
                    annotationModel.AnnotatedDate = DateTime2String(annotation.AnnotatedDate, annotationDetails.TimeZone);
                    annotationModel.ModifiedDate = DateTime2String(annotation.ModifiedDate, annotationDetails.TimeZone);

                    int annotationId = 0;

                    List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel> checklistItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel>>(annotationModel.Checklist.ToString());
                    if(checklistItems.Count > 0)
                    { 
                    if (annotation.Parent.HasValue)
                    {
                        annotationId = (int)annotation.Parent;

                        List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel> newChecklistItems = new List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel>();
                        var checkChecklistItemsExists = (from anci in context.ApprovalAnnotationCheckListItems where anci.ApprovalAnnotation == annotationId select anci.ID).Any();
                        
                        if (checkChecklistItemsExists)
                        {
                            var existingChecklistItems = (from anci in context.ApprovalAnnotationCheckListItems where anci.ApprovalAnnotation == annotationId select anci.CheckListItem).ToList();

                            List<int> newChecklistItemIDs = new List<int>();
                            foreach (var newChecklistItem in checklistItems)
                            {
                                newChecklistItemIDs.Add(newChecklistItem.Id);
                            }

                            List<int> checkAnyChecklistItemHasDeleted = new List<int>();
                            foreach (var item in existingChecklistItems)
                            {
                                if (!newChecklistItemIDs.Contains(item))
                                {
                                    checkAnyChecklistItemHasDeleted.Add(item);
                                }
                            }

                            if (checkAnyChecklistItemHasDeleted.Count > 0)
                            {
                                var previousChecklistData = (from anci in context.ApprovalAnnotationCheckListItems
                                                             let creator = (from an in context.ApprovalAnnotations
                                                                            where anci.ApprovalAnnotation == an.ID
                                                                            select an.Creator).FirstOrDefault()
                                                             let externalCreator = (from an in context.ApprovalAnnotationCheckListItems
                                                                                    where anci.ApprovalAnnotation == an.ID
                                                                                    select an.ExternalCreator).FirstOrDefault()
                                                             join an in context.ApprovalAnnotations on anci.ApprovalAnnotation equals an.ID
                                                             where anci.ApprovalAnnotation == annotationId && checkAnyChecklistItemHasDeleted.Contains(anci.CheckListItem)
                                                             select new
                                                             {
                                                                 ApprovalAnnotation = anci.ApprovalAnnotation,
                                                                 CheckListItem = anci.CheckListItem,
                                                                 TextValue = anci.TextValue,
                                                                 Creator = anci.Creator == null ? creator : anci.Creator,
                                                                 CreatedDate = anci.CreatedDate == null ? an.AnnotatedDate : anci.CreatedDate,
                                                                 ExternalCreator = anci.ExternalCreator == null ? externalCreator : anci.ExternalCreator,
                                                                 TotalChanges = anci.TotalChanges
                                                             }).ToList();

                                for (int i = 0; i < previousChecklistData.Count(); i++)
                                {
                                    var attachmentdb = new GMGColorDAL.ApprovalAnnotationChecklistItemHistory();
                                    {
                                        attachmentdb.ApprovalAnnotation = previousChecklistData[i].ApprovalAnnotation;
                                        attachmentdb.ChecklistItem = previousChecklistData[i].CheckListItem;
                                        attachmentdb.TextValue = previousChecklistData[i].TextValue;
                                        attachmentdb.Guid = Guid.NewGuid().ToString();
                                        attachmentdb.Creator = previousChecklistData[i].Creator;
                                        attachmentdb.ExternalCreator = previousChecklistData[i].ExternalCreator;
                                        attachmentdb.CreatedDate = (DateTime)previousChecklistData[i].CreatedDate;
                                    };
                                    context.ApprovalAnnotationChecklistItemHistories.Add(attachmentdb);
                                }

                                List<GMGColorDAL.ApprovalAnnotationCheckListItem> lstAnnotationChecklists =
                           (from anch in context.ApprovalAnnotationCheckListItems
                            where anch.ApprovalAnnotation == annotationId && checkAnyChecklistItemHasDeleted.Contains(anch.CheckListItem)
                            select anch).ToList();

                                foreach (var a in lstAnnotationChecklists)
                                {
                                    var attachmentdb = new GMGColorDAL.ApprovalAnnotationCheckListItem();
                                    {
                                        attachmentdb.ApprovalAnnotation = annotationId;
                                        attachmentdb.CheckListItem = a.CheckListItem;
                                        attachmentdb.TextValue = string.Empty;
                                        attachmentdb.Guid = Guid.NewGuid().ToString();
                                        attachmentdb.TotalChanges = a.TotalChanges  + 1;
                                        attachmentdb.Creator = model.annotation.Creator;
                                        attachmentdb.ExternalCreator = model.annotation.ExternalCreator;
                                        attachmentdb.CreatedDate = DateTime.UtcNow;
                                    };
                                    if(a.TextValue != string.Empty)
                                    { 
                                        annotation.ApprovalAnnotationCheckListItem.Add(attachmentdb);
                                        context.ApprovalAnnotationCheckListItems.Add(attachmentdb);
                                    }
                                }

                                foreach (
                                   GMGColorDAL.ApprovalAnnotationCheckListItem item in
                                                 lstAnnotationChecklists.GetRange(0, lstAnnotationChecklists.Count))
                                {
                                    if (item.TextValue != string.Empty)
                                    {
                                        DALUtils.Delete<GMGColorDAL.ApprovalAnnotationCheckListItem>(context, item);
                                    }
                                }
                            }


                        }
                        
                        foreach (var checklistItemTableId in checklistItems)
                        {
                            var chechklistItemNotEdited = (from anci in context.ApprovalAnnotationCheckListItems where anci.ID == checklistItemTableId.ID && anci.TextValue == checklistItemTableId.ItemValue select anci).Any();
                            if (!chechklistItemNotEdited)
                            {
                                newChecklistItems.Add(checklistItemTableId);
                            }
                        }

                        List<int> checklistItemIDs = new List<int>();
                        foreach (var checklistItem in newChecklistItems)
                        {
                            checklistItemIDs.Add(checklistItem.Id);
                        }

                        if (checkChecklistItemsExists)
                        {
                            var previousChecklistData = (from anci in context.ApprovalAnnotationCheckListItems
                                                         let creator = (from an in context.ApprovalAnnotations
                                                                        where anci.ApprovalAnnotation == an.ID
                                                                        select an.Creator).FirstOrDefault()
                                                         let externalCreator = (from an in context.ApprovalAnnotations
                                                                                where anci.ApprovalAnnotation == an.ID
                                                                                select an.ExternalCreator).FirstOrDefault()
                                                         join an in context.ApprovalAnnotations on anci.ApprovalAnnotation equals an.ID
                                                         where anci.ApprovalAnnotation == annotationId && checklistItemIDs.Contains(anci.CheckListItem)
                                                         select new
                                                         {
                                                             ApprovalAnnotation = anci.ApprovalAnnotation,
                                                             CheckListItem = anci.CheckListItem,
                                                             TextValue = anci.TextValue,
                                                             Creator = anci.Creator == null ? creator : anci.Creator,
                                                             ExternalCreator = anci.ExternalCreator == null ? externalCreator : anci.ExternalCreator,
                                                             CreatedDate = anci.CreatedDate == null ? an.AnnotatedDate : anci.CreatedDate,
                                                             TotalChanges = anci.TotalChanges
                                                         }).ToList();
                            for (int i = 0; i < previousChecklistData.Count(); i++)
                            {
                                var attachmentdb = new GMGColorDAL.ApprovalAnnotationChecklistItemHistory();
                                {
                                    attachmentdb.ApprovalAnnotation = previousChecklistData[i].ApprovalAnnotation;
                                    attachmentdb.ChecklistItem = previousChecklistData[i].CheckListItem;
                                    attachmentdb.TextValue = previousChecklistData[i].TextValue;
                                    attachmentdb.Guid = Guid.NewGuid().ToString();
                                    attachmentdb.Creator = previousChecklistData[i].Creator;
                                    attachmentdb.ExternalCreator = previousChecklistData[i].ExternalCreator;
                                    attachmentdb.CreatedDate = (DateTime)previousChecklistData[i].CreatedDate;
                                };
                                context.ApprovalAnnotationChecklistItemHistories.Add(attachmentdb);
                            }

                            List<GMGColorDAL.ApprovalAnnotationCheckListItem> lstAnnotationChecklists =
                            (from anch in context.ApprovalAnnotationCheckListItems
                             where anch.ApprovalAnnotation == annotationId && checklistItemIDs.Contains(anch.CheckListItem)
                             select anch).ToList();

                            foreach (
                               GMGColorDAL.ApprovalAnnotationCheckListItem item in
                                             lstAnnotationChecklists.GetRange(0, lstAnnotationChecklists.Count))
                            {
                                DALUtils.Delete<GMGColorDAL.ApprovalAnnotationCheckListItem>(context, item);
                            }


                            for (int i = 0; i < newChecklistItems.Count(); i++)
                            {
                                var attachmentdb = new GMGColorDAL.ApprovalAnnotationCheckListItem();
                                {
                                    attachmentdb.ApprovalAnnotation = annotationId;
                                    attachmentdb.CheckListItem = newChecklistItems[i].Id;
                                    attachmentdb.TextValue = newChecklistItems[i].ItemValue;
                                    attachmentdb.Guid = Guid.NewGuid().ToString();
                                    attachmentdb.TotalChanges = newChecklistItems[i].TotalChanges == null ? 1 : newChecklistItems[i].TotalChanges + 1;
                                    attachmentdb.Creator = model.annotation.Creator;
                                    attachmentdb.ExternalCreator = model.annotation.ExternalCreator;
                                    attachmentdb.CreatedDate = DateTime.UtcNow;
                                };
                                annotation.ApprovalAnnotationCheckListItem.Add(attachmentdb);
                                context.ApprovalAnnotationCheckListItems.Add(attachmentdb);
                            }

                        }
                        else
                        {
                            for (int i = 0; i < newChecklistItems.Count(); i++)
                            {
                                var attachmentdb = new GMGColorDAL.ApprovalAnnotationCheckListItem();
                                {
                                    attachmentdb.ApprovalAnnotation = annotationId;
                                    attachmentdb.CheckListItem = newChecklistItems[i].Id;
                                    attachmentdb.TextValue = newChecklistItems[i].ItemValue;
                                    attachmentdb.Guid = Guid.NewGuid().ToString();
                                    attachmentdb.TotalChanges = newChecklistItems[i].TotalChanges + 1;
                                    attachmentdb.Creator = model.annotation.Creator;
                                    attachmentdb.ExternalCreator = model.annotation.ExternalCreator;
                                    attachmentdb.CreatedDate = DateTime.UtcNow;
                                };
                                annotation.ApprovalAnnotationCheckListItem.Add(attachmentdb);
                                context.ApprovalAnnotationCheckListItems.Add(attachmentdb);
                            }
                        }
                        context.SaveChanges();
                    }
                    else
                    {
                        annotationId = annotation.ID;


                        for (int i = 0; i < checklistItems.Count(); i++)
                        {
                            var attachmentdb = new GMGColorDAL.ApprovalAnnotationCheckListItem();
                            {
                                attachmentdb.ApprovalAnnotation = annotationId;
                                attachmentdb.CheckListItem = checklistItems[i].Id;
                                attachmentdb.TextValue = checklistItems[i].ItemValue;
                                attachmentdb.Guid = Guid.NewGuid().ToString();
                                attachmentdb.TotalChanges = checklistItems[i].TotalChanges + 1;
                                attachmentdb.ExternalCreator = model.annotation.ExternalCreator;
                                attachmentdb.Creator = model.annotation.Creator;
                                attachmentdb.CreatedDate = DateTime.UtcNow;
                            };
                            annotation.ApprovalAnnotationCheckListItem.Add(attachmentdb);
                            context.ApprovalAnnotationCheckListItems.Add(attachmentdb);
                        }
                        context.SaveChanges();
                    }

                    if(annotation.Parent.HasValue)
                    {
                        List<GMGColorDAL.ApprovalAnnotationCheckListItem> AnnotationChecklistsWithChildId =
                            (from aci in context.ApprovalAnnotationCheckListItems where aci.ApprovalAnnotation == annotation.ID select aci).ToList();

                        if(AnnotationChecklistsWithChildId.Count() > 0)
                        {
                            for(int i = 0; i < AnnotationChecklistsWithChildId.Count(); i++)
                            {

                                AnnotationChecklistsWithChildId[i].ApprovalAnnotation = (int)annotation.Parent;
                            }
                            context.SaveChanges();
                        }
                    }
                }
                    annotationsResponse.InstantNotification = new DashboardInstantNotification()
                    {
                        ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                        Version = annotationDetails.Approval.ToString(),
                        EntityType = InstantNotificationEntityType.Decision,
                        ExternalCreator = model.is_external_user ? loggedUser.Id: (int?)null,
                        Creator = !model.is_external_user ? loggedUser.Id : (int?)null,
                    };

                    #region Move attachments

                    if (!annotationModel.Parent.HasValue && annotationModel.AnnotationAttachments != null &&
                        annotationModel.AnnotationAttachments.Length > 0)
                    {
                        foreach (ApprovalAnnotationAttachment attachment in annotationModel.AnnotationAttachments)
                        {
                            AnnotationBL.MoveAnnotationAttachmentFile(context, model.user_key, model.is_external_user, annotationModel.Page,
                                                                      attachment.Guid, attachment.Name);
                        }
                    }

                    #endregion

                    #region If Substitute User

                    GMGColorDAL.ApprovalCollaborator updateSubstituecollaborator = new GMGColorDAL.ApprovalCollaborator();
                    if (model.substituteLoggedUser != null && model.substituteLoggedUser > 0)
                    {
                        updateSubstituecollaborator = (from an in context.ApprovalCollaborators
                                                       where an.Approval == annotationDetails.Approval && an.Collaborator == loggedUserInternal.ID && an.Phase == annotationDetails.CurrentPhase
                                                       select an).FirstOrDefault();

                        if (updateSubstituecollaborator != null && !updateSubstituecollaborator.SubstituteUser.HasValue)
                        {
                            updateSubstituecollaborator.SubstituteUser = model.substituteLoggedUser;
                            context.SaveChanges();
                        }
                    }

                    #endregion

                    //Send Mails when adding new annotations
                    if (model.Id == null)
                    {
                        int? approvalId = annotationDetails.Approval;

                        if (approvalId != null)
                        {
                            if (annotation.Parent == null || annotation.Parent == 0)
                            {
                                int? psSessionId = model.is_external_user
                                                   ? null
                                                   : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
                                                       NotificationTypeParser.GetKey(NotificationType.NewAnnotationAdded),
                                                       loggedUserInternal.ID,
                                                       context)
                                                       ? GlobalNotificationBL.GetPSSessionId(model.session_key, context)
                                                       : null;
                                if (model.annotation.SelectedUserDetails != "[]" && model.annotation.SelectedUserDetails != null)
                                {
                                    NotificationServiceBL.CreateNotification(new NewAnnotationAtUserInComment()
                                    {
                                        EventCreator = model.is_external_user ? (int?)null : annotation.Creator.Value,
                                        Comment = annotation.Comment,
                                        ApprovalId = approvalId.Value,
                                        ApprovalAnnotationId = annotation.ID,
                                        ExternalCreator = model.is_external_user ? loggedUserExternal.ID : (int?)null,

                                    },
                                                                             loggedUserInternal,
                                                                             context,
                                                                             true,
                                                                             psSessionId,
                                                                             model.annotation.SelectedUserDetails

                                        );
                                } else
                                {
                                    NotificationServiceBL.CreateNotification(new NewAnnotation()
                                    {
                                        EventCreator = model.is_external_user ? (int?)null : annotation.Creator.Value,
                                        Comment = annotation.Comment,
                                        ApprovalId = approvalId.Value,
                                        ApprovalAnnotationId = annotation.ID,
                                        ExternalCreator = model.is_external_user ? loggedUserExternal.ID : (int?)null,

                                    },
                                                                             loggedUserInternal,
                                                                             context,
                                                                             true,
                                                                             psSessionId,
                                                                             model.annotation.SelectedUserDetails

                                        );
                                }
                            }
                            else
                            {
                                int? psSessionId = model.is_external_user
                                                   ? null
                                                   : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
                                                       NotificationTypeParser.GetKey(NotificationType.ACommentIsMade),
                                                       loggedUserInternal.ID,
                                                       context)
                                                       ? GlobalNotificationBL.GetPSSessionId(model.session_key, context)
                                                       : null;

                                NotificationServiceBL.CreateNotification(new NewAnnotationComment()
                                {
                                    EventCreator = model.is_external_user ? (int?) null : annotation.Creator.Value,
                                    Comment = annotation.Comment,
                                    ApprovalId = approvalId.Value,
                                    ApprovalAnnotationId = annotation.ID,
                                    ExternalCreator = model.is_external_user ? loggedUserExternal.ID : (int?) null
                                },
                                                                         loggedUserInternal,
                                                                         context,
                                                                         true,
                                                                         psSessionId,
                                                                         model.annotation.SelectedUserDetails


                                    );

                                //Send reply to my comment email for creator
                                List<AnnoatationReplayToMyCommentUser> PreviousCommentUsers =
                                    AnnotationBL.GetPreviousCommentUsers(annotation.Parent.Value, annotation.Creator.HasValue ? annotation.Creator.Value : annotation.ExternalCreator.Value, annotation.ExternalCreator.HasValue, context);
                                List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel> selectedusers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel>>(model.annotation.SelectedUserDetails.ToString());

                                if (selectedusers.Count == 0)
                                {
                                    // Adding Notification item to all user related to comment.(Replay To my Comment is checked)
                                    foreach (AnnoatationReplayToMyCommentUser user in PreviousCommentUsers)
                                    {
                                        //>> FOR INTERNALRECIPIENT
                                        if (!user.IsExternal)
                                        {
                                            int? psSessionIdforReply = model.is_external_user
                                                          ? null
                                                          : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
                                                              NotificationTypeParser.GetKey(NotificationType.RepliesToMyComments),
                                                              loggedUserInternal.ID,
                                                              context)
                                                              ? GlobalNotificationBL.GetPSSessionId(model.session_key, context)
                                                              : null;

                                            NotificationServiceBL.CreateNotification(new NewReplyToMyComment
                                            {
                                                EventCreator = model.is_external_user ? (int?)null : annotation.Creator.Value,
                                                Comment = annotation.Comment,
                                                ApprovalId = approvalId.Value,
                                                ApprovalAnnotationId = annotation.ID,
                                                InternalRecipient = user.UserId.Value,
                                                ExternalCreator = model.is_external_user ? loggedUserExternal.ID : (int?)null
                                            },
                                                                                     loggedUserInternal,
                                                                                     context,
                                                                                     true,
                                                                                     psSessionIdforReply,
                                                                                     model.annotation.SelectedUserDetails
                                                );
                                        }

                                        //>> FOR EXTERNAL  RECIPIENT
                                        else
                                        {
                                            int? psSessionIdforReply = model.is_external_user ? null
                                            : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
                                                NotificationTypeParser.GetKey(NotificationType.RepliesToMyComments),
                                                loggedUserInternal.ID,
                                                context)
                                                ? GlobalNotificationBL.GetPSSessionId(model.session_key, context)
                                                : null;

                                            // UnBlock External User URL from Shared Approval Table

                                            var objSharedApproval = (from sa in context.SharedApprovals
                                                                     where sa.Approval == approvalId && sa.ExternalCollaborator == user.UserId.Value
                                                                     select sa).FirstOrDefault();

                                            if (objSharedApproval != null)
                                            {
                                                objSharedApproval.IsBlockedURL = false;
                                                objSharedApproval.ExpireDate = DateTime.UtcNow.AddDays(7);
                                                context.SaveChanges();
                                            }

                                            NotificationServiceBL.CreateNotificationReplayComment(new NewReplyToMyComment
                                            {
                                                EventCreator = model.is_external_user ? (int?)null : annotation.Creator.Value,
                                                Comment = annotation.Comment,
                                                ApprovalId = approvalId.Value,
                                                ApprovalAnnotationId = annotation.ID,
                                                ExternalRecipient = user.UserId.Value,
                                                ExternalCreator = model.is_external_user ? loggedUserExternal.ID : (int?)null
                                            },
                                                                                                        loggedUserInternal,
                                                                                                        context,
                                                                                                        true,
                                                                                                        psSessionIdforReply
                                           );
                                        }
                                    }
                                }
                            }
                        }
                    }
                    annotationsResponse.AnnotationId = annotationModel.ID;
                    if (annotation.Parent.HasValue)
                    {
                        annotationsResponse.ParentAnnotationId = annotation.Parent.Value;
                    }
                    return annotationsResponse;

                }
                else
                {
                  
                    var loggedUserInternal = model.is_external_user ? null : context.Users.FirstOrDefault(u => u.Guid == model.user_key);
                    var loggedUserExternal = model.is_external_user ? context.ExternalCollaborators.FirstOrDefault(u => u.Guid == model.user_key) : null;

                    List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel> checklistItems =
                    Newtonsoft.Json.JsonConvert.DeserializeObject<List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel>>(model.annotation.Checklist.ToString());

                    var checkChecklistItemsExists = (from anci in context.ApprovalAnnotationCheckListItems where anci.ApprovalAnnotation == model.annotation.ID select anci.ID).Any();
                    if(checkChecklistItemsExists)
                    {
                        var existingChecklistItems = (from anci in context.ApprovalAnnotationCheckListItems where anci.ApprovalAnnotation == model.annotation.ID select anci.CheckListItem).ToList();
                       
                        List<int> newChecklistItemIDs = new List<int>();
                        foreach(var newChecklistItem in checklistItems)
                        {
                            newChecklistItemIDs.Add(newChecklistItem.Id);
                        }

                        List<int> checkAnyChecklistItemHasDeleted = new List<int>();
                        foreach(var item in existingChecklistItems)
                        {
                            if(!newChecklistItemIDs.Contains(item))
                            {
                                checkAnyChecklistItemHasDeleted.Add(item);
                            }
                        }

                        if(checkAnyChecklistItemHasDeleted.Count > 0)
                        {
                            var previousChecklistData = (from anci in context.ApprovalAnnotationCheckListItems
                                                         let creator = (from an in context.ApprovalAnnotations
                                                                        where anci.ApprovalAnnotation == an.ID
                                                                        select an.Creator).FirstOrDefault()
                                                         let externalCreator = (from an in context.ApprovalAnnotationCheckListItems
                                                                                where anci.ApprovalAnnotation == an.ID
                                                                                select an.ExternalCreator).FirstOrDefault()
                                                         join an in context.ApprovalAnnotations on anci.ApprovalAnnotation equals an.ID
                                                         where anci.ApprovalAnnotation == model.annotation.ID && checkAnyChecklistItemHasDeleted.Contains(anci.CheckListItem)
                                                         select new
                                                         {
                                                             ApprovalAnnotation = anci.ApprovalAnnotation,
                                                             CheckListItem = anci.CheckListItem,
                                                             TextValue = anci.TextValue,
                                                             Creator = anci.Creator == null ? creator : anci.Creator,
                                                             CreatedDate = anci.CreatedDate == null ? an.AnnotatedDate : anci.CreatedDate,
                                                             ExternalCreator = anci.ExternalCreator == null ? externalCreator : anci.ExternalCreator,
                                                             TotalChanges = anci.TotalChanges
                                                         }).ToList();

                            for (int i = 0; i < previousChecklistData.Count(); i++)
                            {
                                var attachmentdb = new GMGColorDAL.ApprovalAnnotationChecklistItemHistory();
                                {
                                    attachmentdb.ApprovalAnnotation = previousChecklistData[i].ApprovalAnnotation;
                                    attachmentdb.ChecklistItem = previousChecklistData[i].CheckListItem;
                                    attachmentdb.TextValue = previousChecklistData[i].TextValue;
                                    attachmentdb.Guid = Guid.NewGuid().ToString();
                                    attachmentdb.Creator = previousChecklistData[i].Creator;
                                    attachmentdb.ExternalCreator = previousChecklistData[i].ExternalCreator;
                                    attachmentdb.CreatedDate = (DateTime)previousChecklistData[i].CreatedDate;
                                };
                                context.ApprovalAnnotationChecklistItemHistories.Add(attachmentdb);
                            }

                            List<GMGColorDAL.ApprovalAnnotationCheckListItem> lstAnnotationChecklists =
                       (from anch in context.ApprovalAnnotationCheckListItems
                        where anch.ApprovalAnnotation == model.annotation.ID && checkAnyChecklistItemHasDeleted.Contains(anch.CheckListItem)
                        select anch).ToList();

                            foreach (
                               GMGColorDAL.ApprovalAnnotationCheckListItem item in
                                             lstAnnotationChecklists.GetRange(0, lstAnnotationChecklists.Count))
                            {
                                DALUtils.Delete<GMGColorDAL.ApprovalAnnotationCheckListItem>(context, item);
                            }
                        }


                    }
                   
                    List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel> newChecklistItems = new List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.ApprovalChecklistItemModel>();
                    foreach(var checklistItemTableId in checklistItems)
                    {
                        var chechklistItemNotEdited = (from anci in context.ApprovalAnnotationCheckListItems where anci.ID == checklistItemTableId.ID && anci.TextValue == checklistItemTableId.ItemValue select anci).Any();
                        if(!chechklistItemNotEdited)
                        {
                            newChecklistItems.Add(checklistItemTableId);
                        }
                    }
                                        
                    List<int> checklistItemIDs = new List<int>();
                    foreach(var checklistItem in newChecklistItems)
                    {
                        checklistItemIDs.Add(checklistItem.Id);  
                    }

                    ApprovalAnnotation annotation = new ApprovalAnnotation();

                    if (checkChecklistItemsExists)
                    {
                        var previousChecklistData = (from anci in context.ApprovalAnnotationCheckListItems
                                                     let creator = (from an in context.ApprovalAnnotations
                                                                    where anci.ApprovalAnnotation == an.ID 
                                                                    select an.Creator).FirstOrDefault()
                                                     let externalCreator = (from an in context.ApprovalAnnotationCheckListItems
                                                                            where anci.ApprovalAnnotation == an.ID
                                                                            select an.ExternalCreator).FirstOrDefault()
                                                     join an in context.ApprovalAnnotations on anci.ApprovalAnnotation equals an.ID
                                                     where anci.ApprovalAnnotation == model.annotation.ID && checklistItemIDs.Contains(anci.CheckListItem)
                                                     select new
                                                     {
                                                         ApprovalAnnotation = anci.ApprovalAnnotation,
                                                         CheckListItem = anci.CheckListItem,
                                                         TextValue = anci.TextValue,
                                                         Creator = anci.Creator == null ? creator : anci.Creator,
                                                         CreatedDate = anci.CreatedDate == null ? an.AnnotatedDate : anci.CreatedDate,
                                                         ExternalCreator = anci.ExternalCreator == null ? externalCreator : anci.ExternalCreator,
                                                         TotalChanges = anci.TotalChanges
                                                     }).ToList();
                        for (int i = 0; i < previousChecklistData.Count(); i++)
                        {
                            var attachmentdb = new GMGColorDAL.ApprovalAnnotationChecklistItemHistory();
                            {
                                attachmentdb.ApprovalAnnotation = previousChecklistData[i].ApprovalAnnotation;
                                attachmentdb.ChecklistItem = previousChecklistData[i].CheckListItem;
                                attachmentdb.TextValue = previousChecklistData[i].TextValue;
                                attachmentdb.Guid = Guid.NewGuid().ToString();
                                attachmentdb.Creator = previousChecklistData[i].Creator;
                                attachmentdb.ExternalCreator = previousChecklistData[i].ExternalCreator;
                                attachmentdb.CreatedDate = (DateTime)previousChecklistData[i].CreatedDate;
                            };
                            context.ApprovalAnnotationChecklistItemHistories.Add(attachmentdb);
                        }

                        List<GMGColorDAL.ApprovalAnnotationCheckListItem> lstAnnotationChecklists =
                        (from anch in context.ApprovalAnnotationCheckListItems
                         where anch.ApprovalAnnotation == model.annotation.ID && checklistItemIDs.Contains(anch.CheckListItem)
                         select anch).ToList();

                        foreach (
                           GMGColorDAL.ApprovalAnnotationCheckListItem item in
                                         lstAnnotationChecklists.GetRange(0, lstAnnotationChecklists.Count))
                        {
                            DALUtils.Delete<GMGColorDAL.ApprovalAnnotationCheckListItem>(context, item);
                        }


                        for (int i = 0; i < newChecklistItems.Count(); i++)
                        {
                            var attachmentdb = new GMGColorDAL.ApprovalAnnotationCheckListItem();
                            {
                                attachmentdb.ApprovalAnnotation = model.annotation.ID;
                                attachmentdb.CheckListItem = newChecklistItems[i].Id;
                                attachmentdb.TextValue = newChecklistItems[i].ItemValue;
                                attachmentdb.Guid = Guid.NewGuid().ToString();
                                attachmentdb.TotalChanges = newChecklistItems[i].TotalChanges == null ? 1 : newChecklistItems[i].TotalChanges + 1;
                                attachmentdb.Creator = model.annotation.Creator;
                                attachmentdb.ExternalCreator = model.annotation.ExternalCreator;
                                attachmentdb.CreatedDate = DateTime.UtcNow;
                            };
                            annotation.ApprovalAnnotationCheckListItem.Add(attachmentdb);
                            context.ApprovalAnnotationCheckListItems.Add(attachmentdb);
                        }

                    }
                    else
                    {
                        for (int i = 0; i < newChecklistItems.Count(); i++)
                        {
                            var attachmentdb = new GMGColorDAL.ApprovalAnnotationCheckListItem();
                            {
                                attachmentdb.ApprovalAnnotation = model.annotation.ID;
                                attachmentdb.CheckListItem = newChecklistItems[i].Id;
                                attachmentdb.TextValue = newChecklistItems[i].ItemValue;
                                attachmentdb.Guid = Guid.NewGuid().ToString();
                                attachmentdb.TotalChanges = newChecklistItems[i].TotalChanges + 1;
                                attachmentdb.Creator = model.annotation.Creator;
                                attachmentdb.ExternalCreator = model.annotation.ExternalCreator;
                                attachmentdb.CreatedDate = DateTime.UtcNow;
                            };
                            annotation.ApprovalAnnotationCheckListItem.Add(attachmentdb);
                            context.ApprovalAnnotationCheckListItems.Add(attachmentdb);
                        }
                    }
                    context.SaveChanges();


                    var annotationsResponse = new AnnotationsResponse();
                    annotationsResponse.AnnotationId = model.annotation.ID;
                    int? approvalId = annotationDetails.Approval;
                    annotationsResponse.InstantNotification = new DashboardInstantNotification()
                    {
                        ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                        Version = annotationDetails.Approval.ToString(),
                        EntityType = InstantNotificationEntityType.Decision,
                        ExternalCreator = model.is_external_user ? model.annotation.ExternalCreator : (int?)null,
                        Creator = !model.is_external_user ? model.annotation.Creator : (int?)null,
                    };




                    int? psSessionId = model.is_external_user
                                                  ? null
                                                  : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
                                                      NotificationTypeParser.GetKey(NotificationType.ChecklistItemEdited),
                                                      loggedUserInternal.ID,
                                                      context)
                                                      ? GlobalNotificationBL.GetPSSessionId(model.session_key, context)
                                                      : null;

                    NotificationServiceBL.CreateNotification(new NewChecklistItemComment()
                    {
                        EventCreator = model.is_external_user ? (int?)null : model.annotation.Creator.Value,
                        Comment = "",
                        ApprovalId = approvalId.Value,
                        ApprovalAnnotationId = model.annotation.ID,
                        ExternalCreator = model.is_external_user ? loggedUserExternal.ID : (int?)null
                    },
                                                             loggedUserInternal,
                                                             context,
                                                             true,
                                                             psSessionId,
                                                             model.annotation.SelectedUserDetails


                        );



                    return annotationsResponse;
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateOrEditAnnotation, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateOrEditAnnotation, ex);
            }
        }

        public static UpdateDecision UpdateApprovalDecision(UpdateApprovalDecision model)
        {
            try
            {
                if (!model.collaboratorId.HasValue || !model.decisionId.HasValue)
                {
                    return null;
                }

                var response = new UpdateDecision();
                using (var context = new DbContextBL())
                {
                    if (!HasRightToMakeDecision(model.isExternal, model.collaboratorId.Value, model.Id.Value, model.user_key, context))
                        return null;

                    User loggedUserInternal = null;
                    ExternalCollaborator loggedUserExternal = null;
                    if (model.isExternal)
                    {
                        loggedUserExternal = context.ExternalCollaborators.FirstOrDefault(u => u.Guid == model.user_key);
                    }
                    else
                    {
                        loggedUserInternal = context.Users.FirstOrDefault(u => u.Guid == model.user_key);
                    }

                    var approval = (from ap in context.Approvals
                                    where ap.ID == model.Id
                                    select ap).SingleOrDefault();

                    var decision = AddOrUpdateApprovalDecision(model, approval.ID, approval.CurrentPhase, context);

                    //check to see if the version was uploaded via ftp
                    if (approval != null && !string.IsNullOrWhiteSpace(approval.FTPSourceFolder))
                    {
                        ApprovalBL.CreateQueueMessageForUpdateApprovalJobStatus(approval.ID);
                    }

                    LockApprovalIfMeetsConditions(ref approval, model.collaboratorId);
                    context.SaveChanges();

                    if (approval != null)
                    {
                        ApprovalBL.JobStatusAutoUpdate(approval, model.collaboratorId, context);
                        if (model.substituteLoggedUser != null && model.substituteLoggedUser > 0)
                        {
                            UpdateCollaborateSubstituteUser(model, approval.ID, approval.CurrentPhase, context);
                        }
                        PushApprovalStatus(model, decision, loggedUserInternal, loggedUserExternal, approval, context);
                        SendApprovalStatusChangedNotification(model, approval,  context);
                        LockApproval lockApproval = null;
                        bool moveNext = false;
                        if (approval.CurrentPhase != null)
                        {
                            int loggedUserId = loggedUserInternal != null ? loggedUserInternal.ID : loggedUserExternal.ID;
                            lockApproval = ApprovalWorkflowBL.CheckApprovalShouldMoveToNextPhase(new List<int> { approval.ID }, loggedUserId, model.isExternal, model.decisionId);
                            moveNext = lockApproval != null && lockApproval.MoveNext;
                        }

                        if (lockApproval != null && lockApproval.IsRejected)
                        {
                            ApprovalBL.NotifyUsers(model.isExternal, model.collaboratorId, approval.ID, context, approval.Owner);
                            moveNext = lockApproval.MoveNext;
                        }

                        // Block URL for external users once they made Decision
                        if (model.isExternal)
                        {
                            var objSharedApproval = (from sa in context.SharedApprovals
                                                     where sa.Approval == model.Id && sa.ExternalCollaborator == loggedUserExternal.ID
                                                     select sa).FirstOrDefault();

                            if (objSharedApproval != null)
                            {
                                objSharedApproval.ExpireDate = DateTime.UtcNow.AddDays(-1);
                                context.SaveChanges();
                            }
                        }

                        response.UpdateApprovalDecisionResponse = GetApprovalDecisionResponse(model, approval, moveNext, context);
                        //response.UpdateApprovalDecisionResponse = new UpdateApprovalDecisionResponse();
                        response.UpdateApprovalDecisionResponse.decision = GetDecisionById(model.decisionId.GetValueOrDefault());
                        response.UpdateApprovalDecisionResponse.decision.JobStatus = GetApprovalJobStatus(approval.ID, context);

                        var approvalHasPdm = approval.PrimaryDecisionMaker != null || approval.ExternalPrimaryDecisionMaker != null;

                        response.ApprovalWorkflowInstantNotification = GetApprovalWorkflowInstantNotification(lockApproval, approval.ID, loggedUserInternal, loggedUserExternal ,response.UpdateApprovalDecisionResponse.decision.Key, approvalHasPdm, moveNext, context);
                        if (moveNext) //save the changes related to the next phase of an approval workflow
                        {
                            approval.SOADState = 0;
                            context.SaveChanges();

                            if (!lockApproval.IsLastPhaseCompleted)
                            {
                                ApprovalBL.SendEmailsToUsersFromCurrentPhase(approval.ID, loggedUserInternal, context);
                            }
                        }
                    }
                }
                return response;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalDecision, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalDecision, ex);
            }
        }

        public static void UpdateMultiApprovalDecision(UpdateApprovalDecision model)
        {
            try
            {
                if (model.collaboratorId.HasValue && model.decisionId.HasValue)
                {
                    using (var context = new DbContextBL())
                    {
                        if (HasRightToMakeDecision(model.isExternal, model.collaboratorId.Value, model.Id.Value, model.user_key, context))
                        {
                            User loggedUserInternal = null;
                            ExternalCollaborator loggedUserExternal = null;
                            loggedUserInternal = context.Users.FirstOrDefault(u => u.Guid == model.user_key);
                           
                            var approval = (from ap in context.Approvals
                                            where ap.ID == model.Id
                                            select ap).SingleOrDefault();

                            var decision = AddOrUpdateApprovalDecision(model, approval.ID, approval.CurrentPhase, context);

                            //check to see if the version was uploaded via ftp
                            if (approval != null && !string.IsNullOrWhiteSpace(approval.FTPSourceFolder))
                            {
                                ApprovalBL.CreateQueueMessageForUpdateApprovalJobStatus(approval.ID);
                            }

                            LockApprovalIfMeetsConditions(ref approval, model.collaboratorId);
                            context.SaveChanges();

                            if (approval != null)
                            {
                                ApprovalBL.JobStatusAutoUpdate(approval, model.collaboratorId, context);
                                if (model.substituteLoggedUser != null && model.substituteLoggedUser > 0)
                                {
                                    UpdateCollaborateSubstituteUser(model, approval.ID, approval.CurrentPhase, context);
                                }
                                PushApprovalStatus(model, decision, loggedUserInternal, loggedUserExternal, approval, context);
                                SendApprovalStatusChangedNotification(model, approval, context);
                                
                                LockApproval lockApproval = null;
                                bool moveNext = false;
                                if (approval.CurrentPhase != null)
                                {
                                    int loggedUserId =  loggedUserInternal.ID;
                                    lockApproval = ApprovalWorkflowBL.CheckApprovalShouldMoveToNextPhase(new List<int> { approval.ID }, loggedUserId, model.isExternal, model.decisionId);
                                    moveNext = lockApproval != null && lockApproval.MoveNext;
                                }
                                
                                if (lockApproval != null && lockApproval.IsRejected)
                                {
                                    ApprovalBL.NotifyUsers(model.isExternal, model.collaboratorId, approval.ID, context, approval.Owner);
                                    moveNext = lockApproval.MoveNext;
                                }

                                if (moveNext) //save the changes related to the next phase of an approval workflow
                                {
                                    approval.SOADState = 0;
                                    context.SaveChanges();

                                    if (!lockApproval.IsLastPhaseCompleted)
                                    {
                                        ApprovalBL.SendEmailsToUsersFromCurrentPhase(approval.ID, loggedUserInternal, context);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalDecision, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalDecision, ex);
            }
        }

        public static void UpdateApprovalZoomLevel(int approvalId , int zoomLevel)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    var objapproval = (from a in context.Approvals
                                             where a.ID == approvalId
                                    select a).FirstOrDefault();

                    if (objapproval != null)
                    {
                        objapproval.ProofStudioZoomLevel = zoomLevel;
                        context.SaveChanges();
                    }

                }

                }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalZoomLevel, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalZoomLevel, ex);
            }
        }

        private static string GetApprovalJobStatus(int approvalID, DbContextBL context)
        {
            return (from a in context.Approvals
                    join j in context.Jobs on a.Job equals j.ID
                    join js in context.JobStatus on j.Status equals js.ID
                    where a.ID == approvalID
                    select js.Key).FirstOrDefault();
        }
        private static ApprovalWorkflowInstantNotification GetApprovalWorkflowInstantNotification(LockApproval lockApproval, int approvalId, User loggedUser, ExternalCollaborator externalLoggedUser, string decision, bool approvalHasPdm, bool moveNext, DbContextBL context)
        {
            var selectedDecision = ApprovalDecision.GetApprovalDecision(decision);
            var approvalWorkflowInstantNotification = new ApprovalWorkflowInstantNotification
            {
                ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                Version = approvalId.ToString(),
                EntityType = InstantNotificationEntityType.Decision,
                ExternalCreator = externalLoggedUser != null ? externalLoggedUser.ID :(int?) null,
                Creator = loggedUser != null ? loggedUser.ID : (int?)null,
                DecisionMaker = approvalHasPdm ? loggedUser != null ? String.Format("{0} {1}", loggedUser.GivenName, loggedUser.FamilyName) : String.Format("{0} {1}", externalLoggedUser.GivenName, externalLoggedUser.FamilyName) : string.Empty,
                Decision = decision
            };

            if (lockApproval != null && lockApproval.PhaseDetails != null)
            {
                var accId = (loggedUser != null ? loggedUser.Account : externalLoggedUser.Account);
                var loggedAccount = context.Accounts.Where(ac => ac.ID == accId).FirstOrDefault();

                approvalWorkflowInstantNotification.MoveNext = lockApproval.MoveNext;
                approvalWorkflowInstantNotification.PhaseName = lockApproval.PhaseName;
                ApprovalBL.GetPhaseDetails(lockApproval.PhaseDetails, lockApproval.IsLastPhaseCompleted, context, loggedAccount, ref approvalWorkflowInstantNotification, lockApproval.IsRejected, moveNext);
            }

            return approvalWorkflowInstantNotification;
        }

        private static void LockApprovalIfMeetsConditions(ref GMGColorDAL.Approval approval, int? collaboratorId)
        {
            //check if approval meets conditions to be locked
            if (approval != null &&
                (approval.LockProofWhenFirstDecisionsMade ||
                 approval.OnlyOneDecisionRequired &&
                 (approval.PrimaryDecisionMaker == collaboratorId ||
                  approval.ExternalPrimaryDecisionMaker == collaboratorId)))
            {
                approval.IsLocked = true;
            }
            else if (approval != null && approval.LockProofWhenAllDecisionsMade)
            {
                if (approval.ApprovalCollaboratorDecisions.All(t => t.Decision != null))
                {
                    approval.IsLocked = true;
                }
            }
        }

        private static UpdateApprovalDecisionResponse GetApprovalDecisionResponse(UpdateApprovalDecision model, GMGColorDAL.Approval approval, bool moveNext, DbContextBL context)
        {
            var result = new UpdateApprovalDecisionResponse();
            var approvalRequest = new ApprovalRequest()
            {
                approvalId = model.Id.GetValueOrDefault(),
                is_external_user = model.isExternal,
                user_key = model.user_key,
                Id = model.Id.GetValueOrDefault(),
                key = model.key
            };

            var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
            var userCanViewAllPhasesAnnotations = approval.CurrentPhase != null && (!model.isExternal ? (from us in context.UserSettings
                                                                                                         join u in context.Users on us.User equals u.ID
                                                                                                         where (u.Guid.ToLower() == model.user_key.ToLower()) && us.Setting == showAnnotationsFromAllPhases
                                                                                                         select us.Value.ToLower() == "true").FirstOrDefault()
                                                                                                               :
                                                                                                               (from ec in context.ExternalCollaborators
                                                                                                                where (ec.Guid.ToLower() == model.user_key.ToLower())
                                                                                                                select ec.ShowAnnotationsOfAllPhases).FirstOrDefault());
            var visiblePhases = !model.isExternal
                                   ? (from aph in context.ApprovalJobPhases
                                      join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                      where ajpa.Approval == approval.ID && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == approval.CurrentPhase)
                                      select aph.ID).ToList()
                                   : (from aph in context.ApprovalJobPhases
                                      join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                      where ajpa.Approval == approval.ID && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == approval.CurrentPhase)
                                      select aph.ID).ToList();

            var data = (from aj in context.Approvals
                        join j in context.Jobs on aj.Job equals j.ID
                        join a in context.Accounts on j.Account equals a.ID
                        where aj.ID == approval.ID && aj.IsDeleted == false
                        select new { a.TimeZone, jobId = j.ID }).FirstOrDefault();

            result.decision = GetDecisionById(model.decisionId.GetValueOrDefault());
            result.internal_collaborators_links = GetCollaboratorLinks(context, approvalRequest, false,
                                                                       data.TimeZone, moveNext, userCanViewAllPhasesAnnotations, visiblePhases);          
            result.external_collaborators_links = GetCollaboratorLinks(context, approvalRequest, true,
                                                                       data.TimeZone, moveNext, userCanViewAllPhasesAnnotations, visiblePhases);
            result.user_groups = GetJobPrimayGroups(context, data.jobId, model.user_key, model.isExternal);
            result.user_group_links = GetJobPrimaryGroupDecisions(context, model.user_key, data.jobId);

            return result;
        }

        private static void SendApprovalStatusChangedNotification(UpdateApprovalDecision model, GMGColorDAL.Approval approval, DbContextBL context)
        {
            var eventCreator = model.isExternal ? null : model.collaboratorId;

            var isDecidedByPDM = ApprovalBL.IsApprovalDecidedByPDM(model.Id.GetValueOrDefault(), eventCreator, context);

            int? psSessionId = model.isExternal
                       ? null
                       : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
                       NotificationTypeParser.GetKey(isDecidedByPDM ? NotificationType.ApprovalStatusChangedByPdm : NotificationType.ApprovalStatusChanged),
                           model.collaboratorId.GetValueOrDefault(),
                           context)
                           ? GlobalNotificationBL.GetPSSessionId(model.session_key, context)
                           : null;

            var approvalStatusChanged = isDecidedByPDM ? new ApprovalStatusChangedByPDM() : new ApprovalStatusChanged();

            approvalStatusChanged.EventCreator = eventCreator;
            approvalStatusChanged.ExternalCreator = model.isExternal ? model.collaboratorId : null;
            approvalStatusChanged.InternalRecipient = approval.Owner;
            approvalStatusChanged.CreatedDate = DateTime.UtcNow;
            approvalStatusChanged.ApprovalsIds = new[] { model.Id.GetValueOrDefault() };
            approvalStatusChanged.NewDecisionId = model.decisionId.GetValueOrDefault();

            GMGColorLogging.log.InfoFormat("Creating notification from UpdateApprovalDecision. ApprovalID: {0} | Date: {1} | DecisionID: {2} | IsExternal: {3} | InternalRecipient: {4} | collaboratorId: {5} "
                , new object[] { approval.ID, DateTime.UtcNow, approvalStatusChanged.NewDecisionId, model.isExternal, approval.Owner, model.collaboratorId });

            NotificationServiceBL.CreateNotification(approvalStatusChanged, null, context, true, psSessionId);
        
            //create notification for approval job completed
            var approvalJobCompleted = new GMGColorNotificationService.Notifications.Approvals.ApprovalJobCompleted();

            if (approval.CurrentPhase == null)
            {

                if (approval.ApprovalCollaboratorDecisions.All(t => t.ApprovalDecision != null ) || (model.isExternal ? approval.ExternalPrimaryDecisionMaker != null ? approval.ExternalPrimaryDecisionMaker == model.collaboratorId : false : approval.PrimaryDecisionMaker != null ? approval.PrimaryDecisionMaker == model.collaboratorId : false))
                {
                    approvalJobCompleted.EventCreator = eventCreator;
                    approvalJobCompleted.ExternalCreator = model.isExternal ? model.collaboratorId : null;
                    approvalJobCompleted.InternalRecipient = approval.Owner;
                    approvalJobCompleted.CreatedDate = DateTime.UtcNow;
                    approvalJobCompleted.ApprovalsIds = new[] { model.Id.GetValueOrDefault() };
                    approvalJobCompleted.NewDecisionId = model.decisionId.GetValueOrDefault();

                    NotificationServiceBL.CreateNotification(approvalJobCompleted, null, context, true, psSessionId);
                }
            }
            else
            {
                //get the max phase of the approval
                var maxPhase = (from j in context.Jobs
                                join a in context.Approvals on j.ID equals a.Job
                                join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
                                join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow
                                where a.ID == approval.ID
                                select new
                                {
                                    ID = ajp.ID,
                                    InternalPrimaryDecisionMaker = ajp.PrimaryDecisionMaker,
                                    ExternalPrimaryDecisionMaker = ajp.ExternalPrimaryDecisionMaker,
                                    Position = ajp.Position
                                }).Distinct().OrderByDescending(t => t.Position).FirstOrDefault();

                //check if current phase is Max Phase
                if (maxPhase.ID == approval.CurrentPhase)
                {
                    if (approval.ApprovalCollaboratorDecisions.Where(t => t.Phase == maxPhase.ID).All(p => p.Decision != null ) || approval.CurrentPhase == maxPhase.ID
                                                  && (model.isExternal ? maxPhase.ExternalPrimaryDecisionMaker != null ? maxPhase.ExternalPrimaryDecisionMaker == model.collaboratorId : false : maxPhase.InternalPrimaryDecisionMaker != null ? maxPhase.InternalPrimaryDecisionMaker == model.collaboratorId : false)
                        )
                    {
                        approvalJobCompleted.EventCreator = eventCreator;
                        approvalJobCompleted.ExternalCreator = model.isExternal ? model.collaboratorId : null;
                        approvalJobCompleted.InternalRecipient = approval.Owner;
                        approvalJobCompleted.CreatedDate = DateTime.UtcNow;
                        approvalJobCompleted.ApprovalsIds = new[] { model.Id.GetValueOrDefault() };
                        approvalJobCompleted.NewDecisionId = model.decisionId.GetValueOrDefault();

                        NotificationServiceBL.CreateNotification(approvalJobCompleted, null, context, true, psSessionId);

                    }
                }
            }

            }

        private static void PushApprovalStatus(UpdateApprovalDecision model, ApprovalCollaboratorDecision decision, User loggedUserInternal, ExternalCollaborator loggedUserExternal, GMGColorDAL.Approval approval, DbContextBL context)
        {
            var loggedUser = new UserDetails();
            var account = 0;
            if (model.isExternal)
            {
                if (loggedUserExternal != null)
                {
                    loggedUser.Id = loggedUserExternal.ID;
                    loggedUser.Email = loggedUserExternal.EmailAddress;
                    account = loggedUserExternal.Account;
                }
            }
            else
            {
                if (loggedUserInternal != null)
                {
                    loggedUser.Id = loggedUserInternal.ID;
                    loggedUser.Email = loggedUserInternal.EmailAddress;
                    loggedUser.Username = loggedUserInternal.Username;
                    account = loggedUserInternal.Account;
                }
            }

            try
            {
                //Push the job status to the external service
                var approvalDetails = new JobStatusDetails()
                {
                    approvalGuid = approval.Guid,
                    approvalStatus = decision.ApprovalDecision.Name,
                    approvalStatusKey = decision.ApprovalDecision.Key
                };

                ApprovalBL.PushStatus(loggedUser, approvalDetails, account, context, approval.CurrentPhase);

            }
            catch (Exception exc)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exc.Message, exc);
            }
        }

        private static ApprovalCollaboratorDecision AddOrUpdateApprovalDecision(UpdateApprovalDecision model, int approvalId, int? currentPhase, DbContextBL context)
        {
            ApprovalCollaboratorDecision decision;

            if (model.isExternal)
            {
                decision = (from acd in context.ApprovalCollaboratorDecisions
                            where
                                acd.Approval == approvalId && acd.ExternalCollaborator == model.collaboratorId &&
                                acd.Phase == currentPhase
                            select acd).Take(1).FirstOrDefault();
            }
            else
            {
                decision = (from acd in context.ApprovalCollaboratorDecisions
                            where
                                acd.Approval == approvalId && acd.Collaborator == model.collaboratorId &&
                                acd.Phase == currentPhase
                            select acd).Take(1).FirstOrDefault();
            }

            if (decision != null)
            {
                decision.Decision = model.decisionId;
                decision.CompletedDate = DateTime.UtcNow;
            }
            else
            {
                decision = new ApprovalCollaboratorDecision
                {
                    Approval = model.Id.Value,
                    Decision = model.decisionId,
                    CompletedDate = DateTime.UtcNow,
                    OpenedDate = DateTime.UtcNow,
                    AssignedDate = DateTime.UtcNow,
                    Phase = currentPhase
                };

                if (model.isExternal)
                {
                    decision.ExternalCollaborator = model.collaboratorId;
                }
                else
                {
                    decision.Collaborator = model.collaboratorId;
                }

                context.ApprovalCollaboratorDecisions.Add(decision);
            }
            return decision;
        }

        public static bool UpdateVersionLocking(UpdateVersionLocking model)
        {
            try
            {
                if (model.data == null)
                {
                    return false;
                }

                using (TransactionScope ts = new TransactionScope())
                {
                    using (DbContextBL context = new DbContextBL())
                    {
                        foreach (var version in model.data)
                        {
                            GMGColorDAL.Approval approval = (from ap in context.Approvals
                                                             where ap.ID == version.Id
                                                             select ap).SingleOrDefault();
                            if (approval != null)
                            {
                                approval.IsLocked = version.isLocked;
                                context.SaveChanges();
                            }
                        }
                    }
                    ts.Complete();
                }
                return true;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotNotUpdateVersionLock, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotNotUpdateVersionLock, ex);
            }
        }

        public static CopyAnnotationResponse CopyAnnotation(DbContextBL context, CopyAnnotationRequest model)
        {
            CopyAnnotationResponse result = new CopyAnnotationResponse();
            try
            {
                ApprovalAnnotation annotation = (from an in context.ApprovalAnnotations
                                                     //let page = (from app in context.ApprovalPages
                                                     //            where app.ID == model.destinationPageId
                                                     //            select app.ID).Any()
                                                 where an.ID == model.Id
                                                 select an).FirstOrDefault();

                if (annotation == null)
                {
                    result.ErrorMessage = GMGColor.Resources.Resources.errAnnotationOrDestinationPageNotFound;
                    result.annotation = null;
                    //if annotation or destination page not found in database return
                    return result;
                }

                //check if annotation source and destination approval versions belong to the same job
                bool sameJobSource = (from app in context.ApprovalPages
                                      join a in context.Approvals on app.Approval equals a.ID
                                      join j in context.Jobs on a.Job equals j.ID
                                      where app.ID == annotation.Page
                                      select j.ID).FirstOrDefault()
                                                  .Equals((from app in context.ApprovalPages
                                                           join a in context.Approvals on app.Approval equals a.ID
                                                           join j in context.Jobs on a.Job equals j.ID
                                                           where app.ID == model.destinationPageId
                                                           select j.ID).FirstOrDefault());

                if (!sameJobSource)
                {
                    result.ErrorMessage = GMGColor.Resources.Resources.errApprovalVersionsDiffJob;
                    result.annotation = null;
                    // approval versions belong to different jobs
                    return result;
                }

                //Check if annotation creator is collaborator on destination approval

                var annotationCreator = model.Id.HasValue
                                            ? (from aa in context.ApprovalAnnotations
                                               where aa.ID == model.Id.Value
                                               select new {internalUser = aa.Creator, externalUser = aa.ExternalCreator})
                                                  .FirstOrDefault()
                                            : null;

                int creatorId = annotationCreator != null
                                    ? (
                                          from ap in context.ApprovalPages
                                          join a in context.Approvals on ap.Approval equals a.ID
                                          join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                          join u in context.Users on ac.Collaborator equals u.ID
                                          join us in context.UserStatus on u.Status equals us.ID
                                          where
                                              ap.ID == model.destinationPageId &&
                                              ac.Collaborator == annotationCreator.internalUser &&
                                              us.Key == "A"
                                          select ac.Collaborator
                                      ).FirstOrDefault()
                                    : 0;

                int externalCreatorId = annotationCreator != null
                                            ? (
                                                  from ap in context.ApprovalPages
                                                  join a in context.Approvals on ap.Approval equals a.ID
                                                  join sa in context.SharedApprovals on a.ID equals sa.Approval
                                                  join ec in context.ExternalCollaborators on sa.ExternalCollaborator
                                                      equals ec.ID
                                                  where
                                                      ap.ID == model.destinationPageId &&
                                                      sa.ExternalCollaborator == annotationCreator.externalUser &&
                                                      ec.IsDeleted == false
                                                  select sa.ExternalCollaborator
                                              ).FirstOrDefault()
                                            : 0;

                bool annotationCreatorIsCollaborator = true;

                // check if current logged user is collaborator on destination approval
                if (creatorId == 0 && externalCreatorId == 0)
                {
                    annotationCreatorIsCollaborator = false;

                    if (model.is_external_user)
                    {
                        externalCreatorId = (from ex in context.ExternalCollaborators
                                             join sa in context.SharedApprovals on ex.ID equals sa.ExternalCollaborator
                                             join ap in context.ApprovalPages on sa.Approval equals ap.Approval
                                             where ex.Guid == model.user_key && ap.ID == model.destinationPageId
                                             select ex.ID).FirstOrDefault();
                    }
                    else
                    {
                        creatorId = (from u in context.Users
                                     join us in context.UserStatus on u.Status equals us.ID
                                     join ac in context.ApprovalCollaborators on u.ID equals ac.Collaborator
                                     join ap in context.ApprovalPages on ac.Approval equals ap.Approval
                                     where us.Key == "A" && u.Guid == model.user_key && ap.ID == model.destinationPageId
                                     select u.ID).FirstOrDefault();
                    }
                }


                // if userId not found then load the approval owner
                if (creatorId == 0 && externalCreatorId == 0)
                {
                    annotationCreatorIsCollaborator = false;

                    creatorId = (from ap in context.ApprovalPages
                                 join a in context.Approvals on ap.Approval equals a.ID
                                 where ap.ID == model.destinationPageId && !a.IsDeleted
                                 select a.Creator).FirstOrDefault();
                }

                var destionationPageDimenssons = (from ap in context.ApprovalPages
                                                  where ap.ID == model.destinationPageId
                                                  select new
                                                  {
                                                      ap.OriginalImageWidth,
                                                      ap.OriginalImageHeight,
                                                      ap.OutputRenderWidth,
                                                      ap.OutputRenderHeight,
                                                  }).FirstOrDefault();

                var sourcePageDimenssons =
                    (from ap in context.ApprovalPages
                     where ap.ID == annotation.Page
                     select new
                     {
                         ap.OriginalImageWidth,
                         ap.OriginalImageHeight,
                         ap.OutputRenderWidth,
                         ap.OutputRenderHeight,
                     }).FirstOrDefault();

                bool copiedAnnotationDoesNotFit = false;

                // Check to see if the new annotation fits in the destination page dimenssions
                if (destionationPageDimenssons != null && sourcePageDimenssons != null &&
                    ((destionationPageDimenssons.OriginalImageHeight < sourcePageDimenssons.OriginalImageHeight ||
                     destionationPageDimenssons.OriginalImageWidth < sourcePageDimenssons.OriginalImageWidth) ||
                     // Take into account DPI changes
                     (destionationPageDimenssons.OutputRenderHeight < sourcePageDimenssons.OutputRenderHeight ||
                     destionationPageDimenssons.OutputRenderWidth < sourcePageDimenssons.OutputRenderWidth)))
                {
                    if ((annotation.CrosshairXCoord > destionationPageDimenssons.OriginalImageWidth ||
                        annotation.CrosshairYCoord > destionationPageDimenssons.OriginalImageHeight) ||
                        // Take into account DPI changes
                        (annotation.CrosshairXCoord > destionationPageDimenssons.OutputRenderWidth ||
                        annotation.CrosshairYCoord > destionationPageDimenssons.OutputRenderHeight))
                    {
                        //Annotation won't fit on the destination page
                        copiedAnnotationDoesNotFit = true;
                    }
                }

                ApprovalAnnotation copyAnnotation = new ApprovalAnnotation();

                if (creatorId > 0)
                {
                    copyAnnotation.Modifier = copyAnnotation.Creator = creatorId;
                }
                else if (externalCreatorId > 0)
                {
                    copyAnnotation.ExternalModifier = copyAnnotation.ExternalCreator = externalCreatorId;
                }

                if (annotationCreatorIsCollaborator)
                {
                    copyAnnotation.AnnotatedDate = annotation.AnnotatedDate;
                    copyAnnotation.ModifiedDate = DateTime.UtcNow;
                }
                else
                {
                    copyAnnotation.AnnotatedDate = copyAnnotation.ModifiedDate = DateTime.UtcNow;
                }

                copyAnnotation.Page = model.destinationPageId.GetValueOrDefault();
                copyAnnotation.Guid = Guid.NewGuid().ToString();
                copyAnnotation.Status = annotation.Status;
                copyAnnotation.HighlightData = annotation.HighlightData;
                copyAnnotation.HighlightType = annotation.HighlightType;
                copyAnnotation.IsPrivate = annotation.IsPrivate;
                copyAnnotation.Comment = annotation.Comment;
                copyAnnotation.CommentType = annotation.CommentType;

                if (copiedAnnotationDoesNotFit)
                {
                    // If rendered page dimenssions are different than original ones, use render dimenssions 
                    if (destionationPageDimenssons.OutputRenderHeight != destionationPageDimenssons.OriginalImageHeight ||
                        destionationPageDimenssons.OutputRenderWidth != destionationPageDimenssons.OriginalImageWidth)
                    {
                        // CZ-2433 If the copied annotation does not fit on the destination page, 
                        // place it in the middle of the rendered page dimenssions 
                        copyAnnotation.CrosshairXCoord = destionationPageDimenssons.OutputRenderWidth / 2;
                        copyAnnotation.CrosshairYCoord = destionationPageDimenssons.OutputRenderHeight / 2;
                    }
                    else
                    {
                        // CZ-2433 If the copied annotation does not fit on the destination page, 
                        // place it in the middle of the original image width
                        copyAnnotation.CrosshairXCoord = destionationPageDimenssons.OriginalImageWidth / 2;
                        copyAnnotation.CrosshairYCoord = destionationPageDimenssons.OriginalImageHeight / 2;
                    }
                }
                else
                {
                    copyAnnotation.CrosshairXCoord = annotation.CrosshairXCoord;
                    copyAnnotation.CrosshairYCoord = annotation.CrosshairYCoord;
                }

                copyAnnotation.EndIndex = annotation.EndIndex;
                copyAnnotation.StartIndex = annotation.StartIndex;
                copyAnnotation.ReferenceFilepath = annotation.ReferenceFilepath;
                copyAnnotation.OrderNumber = annotation.OrderNumber;
                copyAnnotation.TimeFrame = annotation.TimeFrame;

                copyAnnotation.machineId = annotation.machineId;
                copyAnnotation.CalibrationStatus = annotation.CalibrationStatus;

                //copy markups
                if (annotation.ApprovalAnnotationMarkups != null)
                {
                    var markups = annotation.ApprovalAnnotationMarkups.Where(m => m.ApprovalAnnotation == model.Id);
                    foreach (var obj in markups)
                    {
                        ApprovalAnnotationMarkup markup = new ApprovalAnnotationMarkup();
                        markup.X = Convert.ToDecimal(obj.X);
                        markup.Y = Convert.ToDecimal(obj.Y);
                        markup.Height = Convert.ToDecimal(obj.Height);
                        markup.Width = Convert.ToDecimal(obj.Width);
                        markup.Color = obj.Color;
                        markup.ApprovalAnnotation1 = copyAnnotation;

                        if (obj.Size != null)
                            markup.Size = obj.Size;

                        markup.BackgroundColor = obj.BackgroundColor;
                        markup.Rotation = obj.Rotation;

                        //copy shape
                        GMGColorDAL.Shape newShape = new GMGColorDAL.Shape();
                        newShape.IsCustom = true;
                        newShape.FXG = obj.Shape1.FXG;
                        newShape.SVG = obj.Shape1.SVG;

                        markup.Shape1 = newShape;

                        context.ApprovalAnnotationMarkups.Add(markup);
                    }
                }

                //Copy replies
                foreach (ApprovalAnnotation reply in annotation.GetChilds(context))
                {
                    ApprovalAnnotation copyReply = new ApprovalAnnotation();
                    copyReply.Comment = reply.Comment;
                    if (creatorId > 0)
                    {
                        copyReply.Creator = copyReply.Modifier = creatorId;
                    }
                    else if (externalCreatorId > 0)
                    {
                        copyReply.ExternalCreator = copyReply.ExternalModifier = externalCreatorId;
                    }

                    if (annotationCreatorIsCollaborator)
                    {
                        copyReply.AnnotatedDate = reply.AnnotatedDate;
                        copyReply.ModifiedDate = reply.ModifiedDate;
                    }
                    else
                    {
                        copyReply.AnnotatedDate = copyReply.ModifiedDate = DateTime.UtcNow;
                    }

                    copyReply.OrderNumber = reply.OrderNumber;
                    copyReply.Status = reply.Status;
                    copyReply.Guid = Guid.NewGuid().ToString();
                    copyReply.Page = model.destinationPageId.GetValueOrDefault();
                    copyReply.CommentType = reply.CommentType;
                    copyReply.ParentApprovalAnnotation = copyAnnotation;
                    copyReply.StartIndex = reply.StartIndex;
                    copyReply.EndIndex = reply.EndIndex;
                    copyReply.machineId = reply.machineId;

                    context.ApprovalAnnotations.Add(copyReply);
                }

                context.ApprovalAnnotations.Add(copyAnnotation);
                context.SaveChanges();

                AnnotationBL.CopyAnnotationAttachments(context, model, annotation, copyAnnotation);

                result.annotation.AddRange(GetAnnotationWithReplies(context, copyAnnotation.ID, model.user_key));

                result.ErrorMessage = string.Empty;
                return result;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCopyAnnotations, ex);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCopyAnnotations, ex);
            }
        }

        public static bool HasRightToMakeDecision(bool isExternal, int collaboratorId, int approvalId, string userKey,
                                                  DbContextBL context)
        {
            if (isExternal)
            {
                var hasAccess = (from shapp in context.SharedApprovals
                                 join ap in context.Approvals on shapp.Approval equals ap.ID
                                 join exc in context.ExternalCollaborators on shapp.ExternalCollaborator equals exc.ID
                                 join ac in context.Accounts on exc.Account equals ac.ID
                                 join acstatus in context.AccountStatus on ac.Status equals acstatus.ID
                                 join apcrole in context.ApprovalCollaboratorRoles on shapp.ApprovalCollaboratorRole
                                     equals apcrole.ID
                                 where exc.Guid == userKey && apcrole.Key == "ANR" && shapp.IsExpired == false
                                       && shapp.Approval == approvalId && exc.IsDeleted == false &&
                                       ap.IsDeleted == false
                                       && acstatus.Key == "A"
                                 select shapp).Take(1).SingleOrDefault();

                if (hasAccess == null)
                    return false;

            }
            else
            {
                var hasAccess = (from apc in context.ApprovalCollaborators
                                 join ap in context.Approvals on apc.Approval equals ap.ID
                                 join user in context.Users on apc.Collaborator equals user.ID
                                 join userStatus in context.UserStatus on user.Status equals userStatus.ID
                                 join ac in context.Accounts on user.Account equals ac.ID
                                 join acstatus in context.AccountStatus on ac.Status equals acstatus.ID
                                 join apcrole in context.ApprovalCollaboratorRoles on apc.ApprovalCollaboratorRole
                                     equals apcrole.ID
                                 where
                                     user.Guid == userKey && apcrole.Key == "ANR" && apc.Approval == approvalId &&
                                     userStatus.Key == "A" && ap.IsDeleted == false && acstatus.Key == "A"
                                 select apc).Take(1).SingleOrDefault();

                if (hasAccess == null)
                    return false;
            }

            return true;
        }

        public static bool HasAccessToAnnotations(bool isExternal, int approvalpageId, string userKey,
                                                  DbContextBL context)
        {
            if (isExternal)
            {
                return (from shapp in context.SharedApprovals
                        join ap in context.Approvals on shapp.Approval equals ap.ID
                        join appage in context.ApprovalPages on ap.ID equals appage.Approval
                        join exc in context.ExternalCollaborators on shapp.ExternalCollaborator equals exc.ID
                        join ac in context.Accounts on exc.Account equals ac.ID
                        join acstatus in context.AccountStatus on ac.Status equals acstatus.ID
                        join apcrole in context.ApprovalCollaboratorRoles on shapp.ApprovalCollaboratorRole equals
                            apcrole.ID
                        where
                            exc.Guid == userKey && (apcrole.Key == "ANR" || apcrole.Key == "RVW") &&
                            shapp.IsExpired == false
                            && appage.ID == approvalpageId && exc.IsDeleted == false && ap.IsDeleted == false
                            && acstatus.Key == "A"
                        select shapp.ID).Take(1).Any();
            }
            else
            {
                return (from apc in context.ApprovalCollaborators
                        join ap in context.Approvals on apc.Approval equals ap.ID
                        join appage in context.ApprovalPages on ap.ID equals appage.Approval
                        join user in context.Users on apc.Collaborator equals user.ID
                        join userStatus in context.UserStatus on user.Status equals userStatus.ID
                        join ac in context.Accounts on user.Account equals ac.ID
                        join acstatus in context.AccountStatus on ac.Status equals acstatus.ID
                        join apcrole in context.ApprovalCollaboratorRoles on apc.ApprovalCollaboratorRole equals
                            apcrole.ID
                        where
                            user.Guid == userKey && (apcrole.Key == "ANR" || apcrole.Key == "RVW") &&
                            appage.ID == approvalpageId && userStatus.Key == "A" && ap.IsDeleted == false &&
                            acstatus.Key == "A"
                        select apc.ID).Take(1).Any();
            }
        }

        public static bool HasAccessToDeleteAnnotation(bool isExternal, int annotationId, string userKey,
                                                       DbContextBL context)
        {
            bool hasAccess = isExternal
                                 ? (from shapp in context.SharedApprovals
                                    join ap in context.Approvals on shapp.Approval equals ap.ID
                                    join exc in context.ExternalCollaborators on shapp.ExternalCollaborator equals
                                        exc.ID
                                    join apan in context.ApprovalAnnotations on exc.ID equals apan.ExternalCreator
                                    join ac in context.Accounts on exc.Account equals ac.ID
                                    join acstatus in context.AccountStatus on ac.Status equals acstatus.ID
                                    join apcrole in context.ApprovalCollaboratorRoles on shapp.ApprovalCollaboratorRole
                                        equals
                                        apcrole.ID
                                    where
                                        exc.Guid == userKey && (apcrole.Key == "ANR" || apcrole.Key == "RVW") &&
                                        shapp.IsExpired == false
                                        && apan.ID == annotationId && exc.IsDeleted == false && ap.IsDeleted == false
                                        && acstatus.Key == "A"
                                    select shapp.ID).Any()
                                 : (from ac in context.ApprovalCollaborators
                                    join ap in context.Approvals on ac.Approval equals ap.ID
                                    join u in context.Users on ac.Collaborator equals u.ID
                                    join us in context.UserStatus on u.Status equals us.ID
                                    join apan in context.ApprovalAnnotations on u.ID equals apan.Creator
                                    join acc in context.Accounts on u.Account equals acc.ID
                                    join acstatus in context.AccountStatus on acc.Status equals acstatus.ID
                                    join apcrole in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole
                                        equals
                                        apcrole.ID
                                    where
                                        u.Guid == userKey && us.Key == "A"
                                        && (apcrole.Key == "ANR" || apcrole.Key == "RVW")
                                        && apan.ID == annotationId && ap.IsDeleted == false
                                        && acstatus.Key == "A"
                                    select ac.ID).Any();

            if (!hasAccess)
            {
                // if user does not have access to delete the annotation but the annotation has a parent id (this means that it is a comment) then we should check the access for the parent annotation id
                // this is the situation when another user put a comment on a annotation that it is not belong to it and the annotation owner wants to remove that comment from his annotation
                int parentAnnotationId = (from aa in context.ApprovalAnnotations
                                          where aa.ID == annotationId
                                          select aa.Parent).Take(1)
                                                           .FirstOrDefault().GetValueOrDefault();

                if (parentAnnotationId > 0)
                {
                    hasAccess = HasAccessToDeleteAnnotation(isExternal, parentAnnotationId, userKey, context);
                }
            }

            if (!hasAccess)
            {
                // if approval owner wants to delete an annotation that was made by an collaborator
                hasAccess =
                    (from aa in context.ApprovalAnnotations
                     join ap in context.ApprovalPages on aa.Page equals ap.ID
                     join a in context.Approvals on ap.Approval equals a.ID
                     join u in context.Users on a.Owner equals u.ID
                     let collaborator =
                         (from ac in context.ApprovalCollaborators
                          where ac.Approval == a.ID
                          select ac.ID).Take(
                              1).FirstOrDefault()
                     where u.Guid == userKey && aa.ID == annotationId
                     select collaborator
                    ).Take(1).Any();
            }

            return hasAccess;
        }

        public static bool ValidateUser(DbContextBL context, bool isExternalUser, int userId)
        {
            return isExternalUser
                       ? (from ec in context.ExternalCollaborators
                          where ec.ID == userId && ec.IsDeleted == false
                          select ec.ID).Any()
                       : (from u in context.Users
                          join us in context.UserStatus on u.Status equals us.ID
                          where u.ID == userId && us.Key == "A"
                          select u.ID).Any();
        }

        public static List<GMGColorDAL.CustomModels.Api.ApprovalSeparationPlate> GetSeparations(DbContextBL context,
                                                                                                IEnumerable<int> pageIds)
        {
            return ((from asp in context.ApprovalSeparationPlates
                     join ap in context.ApprovalPages on asp.Page equals ap.ID
                     where pageIds.Contains(ap.ID)
                     select new GMGColorDAL.CustomModels.Api.ApprovalSeparationPlate
                     {
                         ID = asp.ID,
                         Name = asp.Name,
                         Page = asp.Page,
                         ChannelIndex = asp.ChannelIndex,
                         RGBHex = asp.RGBHex
                     }).ToList());
        }

        public static AccountSettingsResponse UpdateAccountSettings(AccountSettingsRequest model)
        {
            AccountSettingsResponse response = new AccountSettingsResponse();
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    response.AccountSetting.AccountId = (model.is_external_user == false)
                                                            ? (from a in context.Accounts
                                                               join u in context.Users on a.ID equals u.Account
                                                               where u.Guid.ToLower() == model.user_key.ToLower()
                                                               select a.ID).FirstOrDefault()
                                                            : (from a in context.Accounts
                                                               join ec in context.ExternalCollaborators on a.ID equals
                                                                   ec.Account
                                                               where
                                                                   ec.Guid.ToLower() == model.user_key.ToLower() &&
                                                                   ec.IsDeleted == false
                                                               select a.ID).FirstOrDefault();

                    if (response.AccountSetting.AccountId > 0)
                    {
                        AccountSettingsBL.SaveSetting(
                            AccountSettingsBL.AccountSettingsKeyEnum.ProofStudioBackgroundColor,
                            model.AccountSetting.ProofStudioBackgroundColor ?? string.Empty,
                            AccountSettingsBL.AccountSettingValueType.STRG,
                            response.AccountSetting.AccountId,
                            context);
                        response.AccountSetting.ProofStudioBackgroundColor =
                            model.AccountSetting.ProofStudioBackgroundColor;
                    }


                }
                return response;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateAccounntSettings, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateAccounntSettings, ex);
            }

        }

        public static AnnotationResponse UpdateAnnotationStatus(UpdateAnnotationStatusRequest model)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    int annotationId = model.Id.GetValueOrDefault();
                    ApprovalAnnotation annotation =
                        (from aa in context.ApprovalAnnotations where aa.ID == annotationId select aa).FirstOrDefault();

                    if (annotation == null)
                        throw new Exception("Annotation with Id " + annotationId + " does not exist");

                    annotation.Status = model.status;
                    annotation.ModifiedDate = DateTime.UtcNow;

                    if (model.is_external_user)
                    {
                        annotation.ExternalModifier =
                            (from ec in context.ExternalCollaborators
                             where ec.Guid.ToLower() == model.user_key.ToLower() && ec.IsDeleted == false
                             select ec.ID).FirstOrDefault();
                        annotation.Modifier = null;
                    }
                    else
                    {
                        annotation.ExternalModifier = null;
                        annotation.Modifier =
                            (from u in context.Users
                             join us in context.UserStatus on u.Status equals us.ID
                             where u.Guid.ToLower() == model.user_key.ToLower() && us.Key == "A"
                             select u.ID)
                                .FirstOrDefault();
                    }

                    //Create annotation status change log entry
                    ApprovalAnnotationStatusChangeLog changeStatusLog = new ApprovalAnnotationStatusChangeLog();
                    changeStatusLog.Annotation = annotationId;
                    changeStatusLog.Status = model.status;
                    changeStatusLog.ModifiedDate = DateTime.UtcNow;
                    if (model.is_external_user)
                    {
                        changeStatusLog.ExternalModifier = (from exc in context.ExternalCollaborators
                                                            where exc.Guid == model.user_key
                                                            select exc.ID).FirstOrDefault();
                    }
                    else
                    {
                        changeStatusLog.Modifier = (from u in context.Users
                                                    where u.Guid == model.user_key
                                                    select u.ID).FirstOrDefault();
                    }
                    context.ApprovalAnnotationStatusChangeLogs.Add(changeStatusLog);
                    context.SaveChanges();

                    AnnotationResponse response = GetAnnotation(context, annotationId, null, model.user_key);
                    response.DisplayChangesCompletePopup =
                        ApprovalBL.IsAnnotationChangesComplete(annotation.ApprovalPage.Approval, model.user_key, context);
                    return response;
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateAnnotationStatus, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateAnnotationStatus, ex);
            }
        }

        public static AnnotationResponse UpdateAnnotationComment(DbContextBL context,
                                                                 UpdateAnnotationCommentRequest model)
        {
            try
            {

                int annotationId = model.Id.GetValueOrDefault();
                ApprovalAnnotation annotation =
                    (from aa in context.ApprovalAnnotations where aa.ID == annotationId select aa).FirstOrDefault();

                if (annotation == null)
                    throw new Exception("Annotation with Id " + annotationId + " does not exist");

                annotation.Comment = model.comment;
                if (model.is_external_user)
                {
                    int userId =
                        (from ec in context.ExternalCollaborators
                         where ec.Guid == model.user_key && ec.IsDeleted == false
                         select ec.ID).FirstOrDefault();

                    annotation.ExternalModifier = userId;
                    annotation.Modifier = null;
                    annotation.ModifiedDate = DateTime.UtcNow;
                }
                else
                {
                    int userId =
                        (from u in context.Users
                         join us in context.UserStatus on u.Status equals us.ID
                         where u.Guid == model.user_key && us.Key == "A"
                         select u.ID).FirstOrDefault();

                    annotation.Modifier = userId;
                    annotation.ExternalModifier = null;
                    annotation.ModifiedDate = DateTime.UtcNow;
                }

                context.SaveChanges();

                return ApiBL.GetAnnotation(context, annotationId, null, model.user_key);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateAnnotationStatus, ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateAnnotationStatus, ex);
            }
        }


        public static List<ApprovalPage> GetPages(DbContextBL context, ApprovalRequest model)
        {
            return GetPages(context, model.approvalId, model.user_key, model.is_external_user);
        }



        public static List<GMGColorDAL.CustomModels.Api.ApprovalAnnotationGet> GetAnnotations(DbContextBL context,
                                                                                              ApprovalRequest model,
                                                                                              string timezone)
        {
            try
            {
                var list = new List<GMGColorDAL.CustomModels.Api.ApprovalAnnotationGet>();

                bool showAllAnnotationsForExternalUsers = false;
                var approvalJobID = (from a in context.Approvals where a.ID == model.approvalId select a.Job).FirstOrDefault();

                var currentVersionCurrentPhaseID = (from a in context.Approvals where a.Job == approvalJobID select a).OrderByDescending(x => x.ID).FirstOrDefault();
                bool userCanViewAllPhasesAnnotations = false;
                if (currentVersionCurrentPhaseID.CurrentPhase != null)
                {
                    var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
                     userCanViewAllPhasesAnnotations = !model.is_external_user ?
                                                       (from us in context.UserSettings
                                                        join u in context.Users on us.User equals u.ID
                                                        where
                                                            (u.Guid.ToLower() == model.user_key.ToLower()) &&
                                                            us.Setting == showAnnotationsFromAllPhases
                                                        select us.Value.ToLower() == "true").FirstOrDefault()
                                                       :(from ec in context.ExternalCollaborators
                                                          where (ec.Guid.ToLower() == model.user_key.ToLower())
                                                          select ec.ShowAnnotationsOfAllPhases).FirstOrDefault();
                }

                var parents = (from a in context.Approvals
                               join ap in context.ApprovalPages on a.ID equals ap.Approval
                               join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                               where a.ID == model.approvalId && aa.Parent == null && aa.Phase == currentVersionCurrentPhaseID.CurrentPhase
                               select aa.ID).ToList();

                int selectedHtmlPageId = model.selectedHtmlPageId == null ? 0 : (int)model.selectedHtmlPageId;

                List<ApprovalAnnotationGet> annotations = new List<ApprovalAnnotationGet>();

                if (!model.is_external_user)
                {
                    List<int> visiblePhases = new List<int>();
                    if (currentVersionCurrentPhaseID.CurrentPhase != null)
                    {
                        visiblePhases = (from aph in context.ApprovalJobPhases
                                         join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                         where ajpa.Approval == model.approvalId && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == currentVersionCurrentPhaseID.CurrentPhase)
                                         select aph.ID).ToList();
                    }
                    var userDetails = (from u in context.Users
                                       where u.Guid == model.user_key
                                       select u).FirstOrDefault();

                    var publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == model.approvalId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any();

                    annotations = !publicPrivateAnnotations ? (from a in context.Approvals
                                                               join ap in context.ApprovalPages on a.ID equals ap.Approval
                                                               join aa in context.ApprovalAnnotations on ap.ID equals aa.Page

                                                               let phaseInfo = (from aph in context.ApprovalJobPhases
                                                                                where aph.ID == aa.Phase
                                                                                select new
                                                                                {
                                                                                    aph.Name,
                                                                                    aph.ID
                                                                                }).FirstOrDefault()
                                                               where
                                                                    a.ID == model.approvalId && (selectedHtmlPageId == 0 || ap.ID == selectedHtmlPageId)
                                                               &&
                                                               ((currentVersionCurrentPhaseID.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (
                                                               currentVersionCurrentPhaseID.CurrentPhase == aa.Phase && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                                                               select new ApprovalAnnotationGet
                                                               {
                                                                   ID = aa.ID,
                                                                   Status = aa.Status,
                                                                   PageNumber = ap.Number,
                                                                   StartIndex = aa.StartIndex,
                                                                   EndIndex = aa.EndIndex,
                                                                   OriginalVideoHeight = aa.OriginalVideoHeight,
                                                                   OriginalVideoWidth = aa.OriginalVideoWidth,
                                                                   Page = ap.ID,
                                                                   OrderNumber = aa.OrderNumber,
                                                                   Parent = aa.Parent,
                                                                   ModifiedDate = aa.ModifiedDate.ToString(),
                                                                   Creator = aa.Creator.HasValue ? aa.Creator.Value : 0,
                                                                   ExternalCreator = aa.ExternalCreator.HasValue ? aa.ExternalCreator.Value : 0,
                                                                   Modifier = aa.Modifier.HasValue ? aa.Modifier.Value : 0,
                                                                   ExternalModifier = aa.ExternalModifier.HasValue ? aa.ExternalModifier.Value : 0,
                                                                   AnnotatedDate = aa.AnnotatedDate.ToString(),
                                                                   Comment = aa.Comment,
                                                                   IsPrivate = aa.IsPrivate,
                                                                   TimeFrame = aa.TimeFrame,
                                                                   CommentType = aa.CommentType,
                                                                   CrosshairXCoord = aa.CrosshairXCoord,
                                                                   CrosshairYCoord = aa.CrosshairYCoord,
                                                                   ReferenceFilePath = aa.ReferenceFilepath,
                                                                   AnnotationNr = 0,
                                                                   Version = ap.Approval,
                                                                   SoftProofingLevel = (SoftProofingLevel)aa.CalibrationStatus,
                                                                   PhaseName = phaseInfo != null ? phaseInfo.Name : null,
                                                                   Phase = phaseInfo != null ? (int?)phaseInfo.ID : null,
                                                                   ViewingCondition = aa.ViewingCondition,
                                                                   AnnotatedOnImageWidth = aa.AnnotatedOnImageWidth,
                                                                   IsPrivateAnnotation = aa.IsPrivateAnnotation,
                                                                   ShowVideoReport = aa.ShowVideoReport
                                                               }).ToList()
                                       : (from a in context.Approvals
                                          join ap in context.ApprovalPages on a.ID equals ap.Approval
                                          join aa in context.ApprovalAnnotations on ap.ID equals aa.Page

                                          let phaseInfo = (from aph in context.ApprovalJobPhases
                                                           where aph.ID == aa.Phase
                                                           select new
                                                           {
                                                               aph.Name,
                                                               aph.ID
                                                           }).FirstOrDefault()
                                          where
                                               a.ID == model.approvalId && (selectedHtmlPageId == 0 || ap.ID == selectedHtmlPageId)
                                          &&
                                          ((currentVersionCurrentPhaseID.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (
                                          currentVersionCurrentPhaseID.CurrentPhase == aa.Phase && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                                         &&
                                         (a.PrimaryDecisionMaker == userDetails.ID || userDetails.PrivateAnnotations == false || aa.IsPrivateAnnotation == false || aa.Creator == userDetails.ID)
                                          select new ApprovalAnnotationGet
                                          {
                                              ID = aa.ID,
                                              Status = aa.Status,
                                              PageNumber = ap.Number,
                                              StartIndex = aa.StartIndex,
                                              EndIndex = aa.EndIndex,
                                              OriginalVideoHeight = aa.OriginalVideoHeight,
                                              OriginalVideoWidth = aa.OriginalVideoWidth,
                                              Page = ap.ID,
                                              OrderNumber = aa.OrderNumber,
                                              Parent = aa.Parent,
                                              ModifiedDate = aa.ModifiedDate.ToString(),
                                              Creator = aa.Creator.HasValue ? aa.Creator.Value : 0,
                                              ExternalCreator = aa.ExternalCreator.HasValue ? aa.ExternalCreator.Value : 0,
                                              Modifier = aa.Modifier.HasValue ? aa.Modifier.Value : 0,
                                              ExternalModifier = aa.ExternalModifier.HasValue ? aa.ExternalModifier.Value : 0,
                                              AnnotatedDate = aa.AnnotatedDate.ToString(),
                                              Comment = aa.Comment,
                                              IsPrivate = aa.IsPrivate,
                                              TimeFrame = aa.TimeFrame,
                                              CommentType = aa.CommentType,
                                              CrosshairXCoord = aa.CrosshairXCoord,
                                              CrosshairYCoord = aa.CrosshairYCoord,
                                              ReferenceFilePath = aa.ReferenceFilepath,
                                              AnnotationNr = 0,
                                              Version = ap.Approval,
                                              SoftProofingLevel = (SoftProofingLevel)aa.CalibrationStatus,
                                              PhaseName = phaseInfo != null ? phaseInfo.Name : null,
                                              Phase = phaseInfo != null ? (int?)phaseInfo.ID : null,
                                              ViewingCondition = aa.ViewingCondition,
                                              AnnotatedOnImageWidth = aa.AnnotatedOnImageWidth,
                                              IsPrivateAnnotation = aa.IsPrivateAnnotation,
                                              ShowVideoReport = aa.ShowVideoReport
                                           }).ToList();
                }
                else
                {
                    var proofStudioShowAnnotationsForExternalUsersSettingsName = AccountSettingsBL.AccountSettingsKeyEnum.ProofStudioShowAnnotationsForExternalUsers.ToString();
                    showAllAnnotationsForExternalUsers = (from accs in context.AccountSettings
                                                          join ec in context.ExternalCollaborators on accs.Account
                                                              equals ec.Account
                                                          where
                                                              ec.Guid.ToLower() == model.user_key.ToLower() &&
                                                              accs.Name.ToLower() ==
                                                              proofStudioShowAnnotationsForExternalUsersSettingsName
                                                                  .ToLower() &&
                                                              accs.Value.ToLower() == "true"
                                                          select accs.ID).Any();

                    annotations = ((from a in context.Approvals
                                    join ap in context.ApprovalPages on a.ID equals ap.Approval
                                    join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                                    let creatorIsCollaborator = (from ac2 in context.ApprovalCollaborators
                                                                 where
                                                                     ac2.Approval == ap.Approval &&
                                                                     ac2.Collaborator == aa.Creator
                                                                 select true).Take(1).FirstOrDefault()
                                    let externalCreatorIsCollaborator = (from sa2 in context.SharedApprovals
                                                                         where
                                                                             sa2.Approval == ap.Approval &&
                                                                             sa2.ExternalCollaborator ==
                                                                             aa.ExternalCreator
                                                                         select true).Take(1).FirstOrDefault()
                                    let modifierIsCollaborator = (from ac2 in context.ApprovalCollaborators
                                                                  where
                                                                      ac2.Approval == ap.Approval &&
                                                                      ac2.Collaborator == aa.Modifier
                                                                  select true).Take(1).FirstOrDefault()
                                    let externalModifierIsCollaborator = (from sa2 in context.SharedApprovals
                                                                          where
                                                                              sa2.Approval == ap.Approval &&
                                                                              sa2.ExternalCollaborator ==
                                                                              aa.ExternalModifier
                                                                          select true).Take(1).FirstOrDefault()
                                    let visiblePhases = (from aph in context.ApprovalJobPhases
                                                         join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                                         where ajpa.Approval == a.ID && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == currentVersionCurrentPhaseID.CurrentPhase)
                                                         select aph.ID).ToList()
                                    let phaseInfo = (from aph in context.ApprovalJobPhases
                                                     where aph.ID == aa.Phase
                                                     select new
                                                     {
                                                         aph.Name,
                                                         aph.ID
                                                     }).FirstOrDefault()

                                    let externalUserGuid =
                                        (from exc in context.ExternalCollaborators
                                         where exc.ID == aa.ExternalCreator
                                         select exc.Guid.ToLower()).FirstOrDefault()

                                    where
                                        a.ID == model.approvalId && ((currentVersionCurrentPhaseID.CurrentPhase != null && userCanViewAllPhasesAnnotations && visiblePhases.Contains((int)aa.Phase)) || (
                                        ((showAllAnnotationsForExternalUsers || externalUserGuid == model.user_key) &&
                                         (!aa.Creator.HasValue || creatorIsCollaborator) &&
                                         (!aa.ExternalCreator.HasValue || externalCreatorIsCollaborator) &&
                                         (!aa.Modifier.HasValue || modifierIsCollaborator) &&
                                         (!aa.ExternalModifier.HasValue || externalModifierIsCollaborator)) &&
                                        currentVersionCurrentPhaseID.CurrentPhase == aa.Phase && (parents.Contains(aa.ID) || parents.Contains((int)aa.Parent))))
                                         && (selectedHtmlPageId == 0 || ap.ID == selectedHtmlPageId)
                                    orderby aa.ID ascending
                                    select new ApprovalAnnotationGet
                                    {

                                        ID = aa.ID,
                                        Status = aa.Status,
                                        PageNumber = ap.Number,
                                        StartIndex = aa.StartIndex,
                                        EndIndex = aa.EndIndex,
                                        OriginalVideoHeight = aa.OriginalVideoHeight,
                                        OriginalVideoWidth = aa.OriginalVideoWidth,
                                        Page = ap.ID,
                                        OrderNumber = aa.OrderNumber,
                                        Parent = aa.Parent,
                                        ModifiedDate = aa.ModifiedDate.ToString(),
                                        Creator = aa.Creator.HasValue ? aa.Creator.Value : 0,
                                        ExternalCreator = aa.ExternalCreator.HasValue ? aa.ExternalCreator.Value : 0,
                                        Modifier = aa.Modifier.HasValue ? aa.Modifier.Value : 0,
                                        ExternalModifier = aa.ExternalModifier.HasValue ? aa.ExternalModifier.Value : 0,
                                        AnnotatedDate = aa.AnnotatedDate.ToString(),
                                        Comment = aa.Comment,
                                        IsPrivate = aa.IsPrivate,
                                        TimeFrame = aa.TimeFrame,
                                        CommentType = aa.CommentType,
                                        CrosshairXCoord = aa.CrosshairXCoord,
                                        CrosshairYCoord = aa.CrosshairYCoord,
                                        ReferenceFilePath = aa.ReferenceFilepath,
                                        AnnotationNr = 0,
                                        Version = ap.Approval,
                                        SoftProofingLevel = (SoftProofingLevel)aa.CalibrationStatus,
                                        PhaseName = phaseInfo != null ? phaseInfo.Name : null,
                                        Phase = phaseInfo != null ? (int?)phaseInfo.ID : null,
                                        ViewingCondition = aa.ViewingCondition,
                                        AnnotatedOnImageWidth = aa.AnnotatedOnImageWidth,
                                        IsPrivateAnnotation = aa.IsPrivateAnnotation,
                                        ShowVideoReport = aa.ShowVideoReport
                                    })).ToList();
                }
              
                foreach (var annotation in annotations)
                {
                    annotation.Creator = annotation.Creator == 0 ? null : annotation.Creator;
                    annotation.Modifier = annotation.Modifier == 0 ? null : annotation.Modifier;
                    annotation.ModifiedDate = DateTime2String(Convert.ToDateTime(annotation.ModifiedDate), timezone);
                    annotation.AnnotatedDate = DateTime2String(Convert.ToDateTime(annotation.AnnotatedDate), timezone);
                    annotation.AnnotationAttachments =
                        (from aaa in context.ApprovalAnnotationAttachments
                         where aaa.ApprovalAnnotation == annotation.ID
                         select aaa.ID).ToArray();
                    annotation.ApprovalannotationChecklistitems = (from aci in context.ApprovalAnnotationCheckListItems
                                                                   where aci.ApprovalAnnotation == annotation.ID
                                                                   select aci.ID).ToArray();
                    annotation.AnnotationChecklistItemsHistory = (from anch in context.ApprovalAnnotationChecklistItemHistories
                                                                  where anch.ApprovalAnnotation == annotation.ID
                                                                  select anch.ID).ToArray();
                    annotation.AnnotationAllchecklistitems = annotation.AnnotationChecklistItemsHistory;
                    annotation.AnnotationAllchecklistitems = annotation.AnnotationAllchecklistitems.Concat(annotation.ApprovalannotationChecklistitems).ToArray();

                    list.Add(annotation);
                }
                return list;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetAnnotations, ex);
            }
        }

        public static List<GMGColorDAL.CustomModels.Api.ApprovalChecklistApi> GetApprovalChecklists(DbContextBL context, ApprovalRequest model)
        {

            List<ApprovalChecklistApi> checklist = new  List<ApprovalChecklistApi>();

            var ApprovalChecklistId = (from a in context.Approvals where a.ID == model.approvalId select a.Checklist).FirstOrDefault();

            if (ApprovalChecklistId > 0)
            {
                var ApprovalChecklist = (from c in context.CheckList
                                         join a in context.Approvals on c.ID equals a.Checklist
                                         where a.ID == model.approvalId
                                         select new
                                         {
                                             c.ID,
                                             c.Name
                                         }).FirstOrDefault();

                var ChecklistItems = (from c in context.CheckList
                                      join ci in context.CheckListItem on c.ID equals ci.CheckList
                                      where c.ID == ApprovalChecklist.ID 
                                      select new ApprovalChecklistItem
                                      {
                                          Id = ci.ID,
                                          Item = ci.Name,
                                          IsItemChecked = false,
                                          ItemValue = null,
                                          TotalChanges = 0,
                                          ShowNoOfChanges = false
                                      }).Distinct().ToList();


                ApprovalChecklistApi Checklist1 = new ApprovalChecklistApi();

                Checklist1.ID = ApprovalChecklist.ID;
                Checklist1.Name = ApprovalChecklist.Name;
                Checklist1.ChecklistItem.AddRange(ChecklistItems);
                checklist.Add(Checklist1);
                return checklist;
            }
            else {
                return checklist;
            }
        }

        public static GMGColorDAL.CustomModels.Api.ApprovalAnnotationApi GetApprovalAnnotations(DbContextBL context,
                                                                                                ApprovalRequest model)
        {
            var annotation = new GMGColorDAL.CustomModels.Api.ApprovalAnnotationApi();
          
            AccountTimeZoneandPattern accountDetails = new AccountTimeZoneandPattern();
         
            if (!model.is_external_user)
            {
                accountDetails = (from u in context.Users
                                   join a in context.Accounts on u.Account equals a.ID
                                   join df in context.DateFormats on a.DateFormat equals df.ID
                                   where u.Guid == model.user_key
                                   select new AccountTimeZoneandPattern
                                   {
                                       timeZone = a.TimeZone,
                                       pattern = df.Pattern
                                   }).FirstOrDefault();
            }
            else
            {
                accountDetails = (from ex in context.ExternalCollaborators
                            join a in context.Accounts on ex.Account equals a.ID
                            join df in context.DateFormats on a.DateFormat equals df.ID
                            where ex.Guid == model.user_key
                            select new AccountTimeZoneandPattern
                            {
                                timeZone = a.TimeZone,
                                pattern = df.Pattern
                            }).FirstOrDefault();
            }
            annotation.annotations = GetAnnotations(context, model, accountDetails.timeZone);

            List<int> commentTypes = new List<int>();
            commentTypes.Add((int)ApprovalCommentType.CommentType.TextComment);
            commentTypes.Add((int)ApprovalCommentType.CommentType.DrawComment);
            List<int> drawAnnotationIds = annotation.annotations.Where(o => commentTypes.Contains(o.CommentType)).Select(t => t.ID).ToList();

            if(drawAnnotationIds.Count > 0)
            {
                annotation.custom_shapes = GetAnnotationShapes(context, drawAnnotationIds);
            }
                        
            List<int> attachments = annotation.annotations.SelectMany(o => o.AnnotationAttachments).ToList();
            if (attachments.Count > 0)
            {
                annotation.AnnotationAttachments = GetAttachments(context, attachments);
            }

            List<int> checklistitems = annotation.annotations.SelectMany(o => o.ApprovalannotationChecklistitems).ToList();
            if (checklistitems.Count > 0)
            {
                annotation.ApprovalannotationChecklistitems = GetChecklistItems(context, checklistitems);
            }
            List<int> checklistItemHistory = annotation.annotations.SelectMany(o => o.AnnotationChecklistItemsHistory).ToList();
            if (checklistItemHistory.Count > 0)
            {
                annotation.AnnotationAllchecklistitems = GetApprovalAnnotationChecklistItemsHistory(context, checklistItemHistory, checklistitems, accountDetails.pattern, accountDetails.timeZone);
            }
           

            annotation.viewing_conditions = GetViewingConditions(context, annotation.annotations.Select(t => t.ViewingCondition.GetValueOrDefault()));

            return annotation;
        }

        private static List<GMGColorDAL.CustomModels.Api.ApprovalCollaborator> GetVersionCollaborators(DbContextBL context,
                                                                                                IEnumerable<int>
                                                                                                    versions,
                                                                                                bool isExternalUser,
                                                                                                string timezone)
        {
            try
            {

                List<GMGColorDAL.CustomModels.Api.ApprovalCollaborator> list = new List<GMGColorDAL.CustomModels.Api.ApprovalCollaborator>();

                var collaborators = !isExternalUser
                                        ? (from a in context.Approvals
                                           join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                           join u in context.Users on ac.Collaborator equals u.ID
                                           join acc in context.Accounts on u.Account equals acc.ID
                                           join us in context.UserStatus on u.Status equals us.ID
                                           join ur in context.UserRoles on u.ID equals ur.User
                                           join r in context.Roles on ur.Role equals r.ID
                                           join am in context.AppModules on r.AppModule equals am.ID
                                           join c in context.ProofStudioUserColors on u.ProofStudioColor equals c.ID
                                                                                    
                                           where
                                               versions.Contains(a.ID) &&
                                               am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate 
                                           select new
                                           {
                                               ID = ac.ID,
                                               u.FamilyName,
                                               u.GivenName,
                                               IsExternal = false,
                                               CollaboratorAvatar = "",
                                               CollaboratorRole = r.ID,
                                               Collaborator = u.ID,
                                               AssignedDate = u.CreatedDate,
                                               UserColor = c.HEXValue,
                                               AccountID = u.Account,
                                               AccountRegion = acc.Region,
                                               AccountDomain =
                                                   acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                               UserPhotoPath = u.PhotoPath,
                                               UserGuid = u.Guid
                                           }).Distinct().ToList()
                                        : (from sa in context.SharedApprovals
                                           join a in context.Approvals on sa.Approval equals a.ID
                                           join appp in context.ApprovalPages on a.ID equals appp.Approval
                                           join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals
                                               ec.ID
                                           join j in context.Jobs on a.Job equals j.ID
                                           join ac in context.Accounts on j.Account equals ac.ID
                                                                                    
                                           where
                                               versions.Contains(a.ID) && a.IsDeleted == false 
                                           select new
                                           {
                                               ID = ec.ID,
                                               ec.FamilyName,
                                               ec.GivenName,
                                               IsExternal = true,
                                               CollaboratorAvatar = "",
                                               CollaboratorRole = sa.ApprovalCollaboratorRole,
                                               Collaborator = ec.ID,
                                               AssignedDate = ec.CreatedDate,
                                               UserColor = ec.ProofStudioColor,
                                               AccountID = ac.ID,
                                               AccountRegion = ac.Region,
                                               AccountDomain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                               UserPhotoPath = string.Empty,
                                               UserGuid = ec.Guid
                                           }).Distinct().ToList();

                List<string> avoidedColors = new List<string>();
                bool saveChanges = false;

                foreach (var collaborator in collaborators)
                {
                    var coll = new GMGColorDAL.CustomModels.Api.ApprovalCollaborator
                    {
                        ID = collaborator.Collaborator,
                        CollaboratorRole = collaborator.CollaboratorRole,
                        FamilyName = collaborator.FamilyName,
                        GivenName = collaborator.GivenName,
                        AssignedDate = DateTime2String(collaborator.AssignedDate, timezone),
                        CollaboratorAvatar = GMGColorDAL.User.GetImagePath(collaborator.AccountRegion, collaborator.AccountDomain,
                                                              collaborator.UserPhotoPath, collaborator.UserGuid, false,
                                                              collaborator.IsExternal),
                        UserColor = collaborator.UserColor
                    };
                    // In case the user doesn't have a color assigned then generate a random and save it to database
                    // - for external users added before the user color future was added
                    if (isExternalUser && String.IsNullOrEmpty(collaborator.UserColor))
                    {
                        coll.UserColor =
                            ExternalCollaboratorBL.AttachColorToExternalCollaborator(collaborator.ID,
                                                                                     collaborator.AccountID,
                                                                                     avoidedColors, context);
                        saveChanges = true;
                        avoidedColors.Add(collaborator.UserColor);
                    }
                    list.Add(coll);
                }

                if (saveChanges)
                {
                    context.SaveChanges();
                }

                return list.GroupBy(i => i.ID).Select(i => i.First()).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetInternalCollaborators, ex);
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call GetCollaborators from collaborate module instead.", false)]
        private static List<GMGColorDAL.CustomModels.Api.ApprovalCollaborator> GetCollaborators(DbContextBL context,
                                                                                                IEnumerable<int>
                                                                                                    versions,
                                                                                                bool isExternalUser,
                                                                                                string timezone, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            try
            {

                List<GMGColorDAL.CustomModels.Api.ApprovalCollaborator> list = new List<GMGColorDAL.CustomModels.Api.ApprovalCollaborator>();

                var collaborators = !isExternalUser
                                        ? (from a in context.Approvals
                                           join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                           join u in context.Users on ac.Collaborator equals u.ID
                                           join acc in context.Accounts on u.Account equals acc.ID
                                           join us in context.UserStatus on u.Status equals us.ID
                                           join ur in context.UserRoles on u.ID equals ur.User
                                           join r in context.Roles on ur.Role equals r.ID
                                           join am in context.AppModules on r.AppModule equals am.ID
                                           join c in context.ProofStudioUserColors on u.ProofStudioColor equals c.ID

                                           let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                                        join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                                        where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                                        select aan.ID).Any()
                                           where
                                               versions.Contains(a.ID) &&
                                               am.Key == (int) GMG.CoZone.Common.AppModule.Collaborate &&
                                               ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                                           select new
                                           {
                                               ID = ac.ID,
                                               u.FamilyName,
                                               u.GivenName,
                                               IsExternal = false,
                                               CollaboratorAvatar = "",
                                               CollaboratorRole = r.ID,
                                               Collaborator = u.ID,
                                               AssignedDate = u.CreatedDate,
                                               UserColor = c.HEXValue,
                                               AccountID = u.Account,
                                               AccountRegion = acc.Region,
                                               AccountDomain =
                                                   acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                               UserPhotoPath = u.PhotoPath,
                                               UserGuid = u.Guid
                                           }).Distinct().ToList()
                                        : (from sa in context.SharedApprovals
                                           join a in context.Approvals on sa.Approval equals a.ID
                                           join appp in context.ApprovalPages on a.ID equals appp.Approval
                                           join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals
                                               ec.ID
                                           join j in context.Jobs on a.Job equals j.ID
                                           join ac in context.Accounts on j.Account equals ac.ID

                                           let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                                        where appp.ID == aan.Page && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                                        select aan.ID).Any()
                                           where
                                               versions.Contains(a.ID) && a.IsDeleted == false && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                                           select new
                                           {
                                               ID = ec.ID,
                                               ec.FamilyName,
                                               ec.GivenName,
                                               IsExternal = true,
                                               CollaboratorAvatar = "",
                                               CollaboratorRole = sa.ApprovalCollaboratorRole,
                                               Collaborator = ec.ID,
                                               AssignedDate = ec.CreatedDate,
                                               UserColor = ec.ProofStudioColor,
                                               AccountID = ac.ID,
                                               AccountRegion = ac.Region,
                                               AccountDomain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                               UserPhotoPath = string.Empty,
                                               UserGuid = ec.Guid
                                           }).Distinct().ToList();

                List<string> avoidedColors = new List<string>();
                bool saveChanges = false;

                foreach (var collaborator in collaborators)
                {
                    var coll = new GMGColorDAL.CustomModels.Api.ApprovalCollaborator
                    {
                        ID = collaborator.Collaborator,
                        CollaboratorRole = collaborator.CollaboratorRole,
                        FamilyName = collaborator.FamilyName,
                        GivenName = collaborator.GivenName,
                        AssignedDate = DateTime2String(collaborator.AssignedDate, timezone),
                        CollaboratorAvatar = GMGColorDAL.User.GetImagePath(collaborator.AccountRegion, collaborator.AccountDomain,
                                                              collaborator.UserPhotoPath, collaborator.UserGuid, false,
                                                              collaborator.IsExternal),
                        UserColor = collaborator.UserColor
                    };
                    // In case the user doesn't have a color assigned then generate a random and save it to database
                    // - for external users added before the user color future was added
                    if (isExternalUser && String.IsNullOrEmpty(collaborator.UserColor))
                    {
                        coll.UserColor =
                            ExternalCollaboratorBL.AttachColorToExternalCollaborator(collaborator.ID,
                                                                                     collaborator.AccountID,
                                                                                     avoidedColors, context);
                        saveChanges = true;
                        avoidedColors.Add(collaborator.UserColor);
                    }
                    list.Add(coll);
                }

                if (saveChanges)
                {
                    context.SaveChanges();
                }

                return list.GroupBy(i => i.ID).Select(i => i.First()).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetInternalCollaborators, ex);
            }
        }

        [ObsoleteAttribute("This method is obsolete. Call GetCollaboratorLinksForUpdateApprovalDecision from GetProofstudioApproval module instead.", false)]
        public static List<GMGColorDAL.CustomModels.Api.ApprovalCollaboratorLink> GetCollaboratorLinks(
            DbContextBL context, ApprovalRequest model, bool isExternalUser, string timezone, bool moveNext, bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            try
            {
                var jobId = (
                                from a in context.Approvals
                                where a.ID == model.approvalId
                                select a.Job).Take(1).FirstOrDefault();

                var items = !isExternalUser
                                 ? (from ac in context.ApprovalCollaborators
                                    join u in context.Users on ac.Collaborator equals u.ID
                                    join a in context.Approvals on ac.Approval equals a.ID
                                    let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                    where
                                                        acd.Collaborator == u.ID && acd.Approval == a.ID &&
                                                        acd.Phase == a.CurrentPhase
                                                    select acd.Decision).FirstOrDefault()
                                    let primaryGroup = (from ug in context.UserGroups
                                                        join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                                        join acg in context.ApprovalCollaboratorGroups on ug.ID equals
                                                            acg.CollaboratorGroup
                                                        where
                                                            ugu.User == u.ID && ugu.IsPrimary && acg.Approval == a.ID &&
                                                            acg.Phase == a.CurrentPhase
                                                        select ug.ID).FirstOrDefault()

                                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                                 where ap.Approval == a.ID && aan.Creator == ac.Collaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                                 select aan.ID).Any()

                                    where
                                        jobId == a.Job && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)ac.Phase)) || a.CurrentPhase == ac.Phase)
                                    select new
                                    {
                                        ID = ac.Collaborator,
                                        ExpireDate = u.CreatedDate,
                                        IsExpired = false,
                                        approval_role = ac.ApprovalCollaboratorRole,
                                        approval_version = ac.Approval,
                                        PrimaryGroup = primaryGroup,
                                        Decision = decision,
                                        a.CurrentPhase,
                                        a.IsLocked,
                                        ac.Phase,
                                        a.Version
                                    }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new { e.Version, e.Phase}).FirstOrDefault()).ToList()
                                 : (from sa in context.SharedApprovals
                                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                    join a in context.Approvals on sa.Approval equals a.ID
                                    let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                    where
                                                        acd.ExternalCollaborator == ec.ID && acd.Approval == a.ID &&
                                                        acd.Decision != null && acd.Phase == a.CurrentPhase
                                                    select acd.Decision).Take(1).FirstOrDefault()

                                    let userHasAnnotations = userCanViewAllPhasesAnnotations && (from aan in context.ApprovalAnnotations
                                                                                                 join ap in context.ApprovalPages on aan.Page equals ap.ID
                                                                                                 where ap.Approval == a.ID && aan.ExternalCreator == sa.ExternalCollaborator && visiblePhases.Contains((int)aan.Phase)
                                                                                                 select aan.ID).Any()
                                    where
                                        jobId == a.Job && ((userCanViewAllPhasesAnnotations && userHasAnnotations && visiblePhases.Contains((int)sa.Phase)) || a.CurrentPhase == sa.Phase)
                                    select new
                                    {
                                        ID = sa.ExternalCollaborator,
                                        ExpireDate = sa.ExpireDate,
                                        IsExpired = sa.IsExpired,
                                        approval_role = sa.ApprovalCollaboratorRole,
                                        approval_version = sa.Approval,
                                        PrimaryGroup = 0,
                                        Decision = decision,
                                        a.CurrentPhase,
                                        a.IsLocked,
                                        sa.Phase,
                                        a.Version
                                    }).GroupBy(x => new { x.ID, x.Version }, (key, g) => g.OrderByDescending(e => new { e.Version, e.Phase}).FirstOrDefault()).ToList();
                
                List<ApprovalCollaboratorLink> list =
                    items.Select(o => new GMGColorDAL.CustomModels.Api.ApprovalCollaboratorLink
                    {
                        collaborator_id = o.ID,
                        ExpireDate = DateTime2String(o.ExpireDate, timezone),
                        IsExpired = o.IsExpired,
                        approval_role = o.approval_role,
                        approval_version = o.approval_version,
                        UserGroup = o.PrimaryGroup > 0 ? (int?) o.PrimaryGroup : null,
                        Decision = o.Decision.HasValue ? (int?) o.Decision.Value : null
                    }).ToList();
                for (int i = 0; i < list.Count(); i++)
                {
                    list[i].ID = (model.approvalId * 10000) + i + 1;
                }
                return list;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetInternalCollaboratorLinks, ex);
            }
        }

        public static List<UserGroup> GetUserGroups(DbContextBL context, ApprovalRequest model)
        {
            try
            {
                var jobId = (
                                from a in context.Approvals
                                where a.ID == model.approvalId
                                select a.Job).Take(1).FirstOrDefault();

                var groups = (from acg in context.ApprovalCollaboratorGroups
                              join ug in context.UserGroups on acg.CollaboratorGroup equals ug.ID
                              join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                              join a in context.Approvals on acg.Approval equals a.ID
                              join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                              join u in context.Users on ac.Collaborator equals u.ID
                              join us in context.UserStatus on u.Status equals us.ID
                              where
                                  ugu.IsPrimary &&
                                  a.Job == jobId &&
                                  u.Guid == model.user_key &&
                                  us.Key == "A" &&
                                  model.is_external_user == false &&
                                  acg.Phase == a.CurrentPhase
                              select new UserGroup
                              {
                                  ID = ug.ID,
                                  Name = ug.Name
                              }).Union((from acg in context.ApprovalCollaboratorGroups
                                        join ug in context.UserGroups on acg.CollaboratorGroup equals ug.ID
                                        join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                        join a in context.Approvals on acg.Approval equals a.ID
                                        join sa in context.SharedApprovals on a.ID equals sa.Approval
                                        join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals
                                            ec.ID
                                        where
                                            ugu.IsPrimary &&
                                            a.Job == jobId &&
                                            ec.Guid == model.user_key &&
                                            ec.IsDeleted == false &&
                                            model.is_external_user == true &&
                                            acg.Phase == a.CurrentPhase
                                        select new UserGroup
                                        {
                                            ID = ug.ID,
                                            Name = ug.Name
                                        })).ToList();

                return groups.GroupBy(o => o.ID).Select(o => o.FirstOrDefault()).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserGroups, ex);
            }
        }

        public static List<UserGroupDecision> GetUserGroupLinks(DbContextBL context, ApprovalRequest model)
        {
            try
            {
                var jobId = (
                                from a in context.Approvals
                                where a.ID == model.approvalId
                                select a.Job).Take(1).FirstOrDefault();

                var links = (
                                from a in context.Approvals
                                join acg in context.ApprovalCollaboratorGroups on a.ID equals acg.Approval
                                let isPrimary =
                                    (from ugu in context.UserGroupUsers
                                     where ugu.UserGroup == acg.CollaboratorGroup && ugu.IsPrimary
                                     select ugu.IsPrimary).FirstOrDefault()
                                let decision = (
                                                   from acd in context.ApprovalCollaboratorDecisions
                                                   join ac in context.ApprovalCollaborators on acd.Collaborator equals
                                                       ac.Collaborator
                                                   join u in context.Users on ac.Collaborator equals u.ID
                                                   join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                   join dec in context.ApprovalDecisions on acd.Decision equals dec.ID
                                                   orderby dec.Priority ascending
                                                   where
                                                       acd.Approval == a.ID && ugu.UserGroup == acg.CollaboratorGroup &&
                                                       ugu.IsPrimary
                                                   select dec.ID
                                               ).Take(1)
                                let hasAccess = (
                                                    from ac in context.ApprovalCollaborators
                                                    join u in context.Users on ac.Collaborator equals u.ID
                                                    join us in context.UserStatus on u.Status equals us.ID
                                                    where
                                                        a.ID == ac.Approval && u.Guid == model.user_key && us.Key == "A" &&
                                                        !model.is_external_user
                                                    select ac.ID
                                                ).Any() || (from sa in context.SharedApprovals
                                                            join ec in context.ExternalCollaborators on
                                                                sa.ExternalCollaborator equals ec.ID
                                                            where
                                                                sa.Approval == a.ID && ec.Guid == model.user_key &&
                                                                ec.IsDeleted == false && model.is_external_user
                                                            select sa.ID).Any()
                                where a.Job == jobId && isPrimary && hasAccess
                                select new UserGroupDecision()
                                {
                                    approval_version = a.ID,
                                    user_group = acg.UserGroup.ID,
                                    decision = decision.Any() ? (int?) decision.FirstOrDefault() : null
                                }
                            ).ToList();

                int index = 1;
                foreach (var link in links)
                {
                    link.ID = index++;
                }
                return links;

            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserGroupLinks, ex);
            }
        }

        public static List<PageBand> GetBands(ApprovalPageRequest model)
        {
            List<PageBand> files = new List<PageBand>();

            using (var context = new DbContextBL())
            {
                bool userIsAdmin = ApiBL.UserIsAdministrator(context, model.user_key);
                var data = model.is_external_user
                               ? (from a in context.Approvals
                                  join ap in context.ApprovalPages on a.ID equals ap.Approval
                                  join sa in context.SharedApprovals on a.ID equals sa.Approval
                                  join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                  join j in context.Jobs on a.Job equals j.ID
                                  join acc in context.Accounts on j.Account equals acc.ID
                                  where ap.ID == model.pageId.Value &&
                                        ec.Guid.ToLower() == model.user_key.ToLower() &&
                                        !ec.IsDeleted && !sa.IsExpired
                                  select
                                      new
                                      {
                                          ApprovalGuid = a.Guid,
                                          Width = ap.OriginalImageWidth,
                                          Height = ap.OriginalImageHeight,
                                          AccountRegion = acc.Region,
                                          AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                          PageNumber = ap.Number,
                                          FileName = a.FileName
                                      })
                                     .FirstOrDefault()
                               : (from a in context.Approvals
                                  join ap in context.ApprovalPages on a.ID equals ap.Approval
                                  join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                  join u in context.Users on ac.Collaborator equals u.ID
                                  join us in context.UserStatus on u.Status equals us.ID
                                  join j in context.Jobs on a.Job equals j.ID
                                  join acc in context.Accounts on j.Account equals acc.ID
                                  where ap.ID == model.pageId.Value &&
                                        ((u.Guid.ToLower() == model.user_key.ToLower() || userIsAdmin) && us.Key == "A")
                                  select
                                      new
                                      {
                                          ApprovalGuid = a.Guid,
                                          Width = ap.OriginalImageWidth,
                                          Height = ap.OriginalImageHeight,
                                          AccountRegion = acc.Region,
                                          AccountDomain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                          PageNumber = ap.Number,
                                          FileName = a.FileName
                                      })
                                     .FirstOrDefault();

                if (data != null)
                {
                    string urlToApprovals = GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                                                ? GMGColorConfiguration.AppConfiguration.PathToDataFolder(
                                                    data.AccountRegion)
                                                : GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +
                                                  data.AccountDomain + "/" +
                                                  GMGColorConfiguration.AppConfiguration.DataFolderName(
                                                      data.AccountRegion) + "/";

                    GMG.CoZone.Common.ApprovalJobFolder approvalJobFolder = new GMG.CoZone.Common.ApprovalJobFolder(
                        data.ApprovalGuid,
                        data.FileName,
                        GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                            ? GMGColorConfiguration.AppConfiguration.PathToProcessingFolder
                            : GMGColorConfiguration.AppConfiguration.FileSystemPathToDataFolder,
                        urlToApprovals);

                    string path = approvalJobFolder.GetPageBandsFolder((uint) data.PageNumber).Replace("\\", "/");
                    List<AWSFileObject> awsFiles = GMGColorIO.ListFiles(path, data.AccountRegion, data.AccountDomain);


                    int startBandId = Convert.ToInt32(model.pageId)*10000;
                    files.AddRange(awsFiles.Select(item => new PageBand()
                    {
                        FileName = item.FileName,
                        Page = Convert.ToInt32(model.pageId),
                        Url = item.Url,
                        Size = item.Size,
                        ID = startBandId++
                    }));
                }

                return files;
            }
        }

        public static List<int> GetVersionsIds(List<int> approvalsIds)
        {
            using (var context = new DbContextBL())
            {

                try
                {
                    return
                        context.Approvals.Where(
                            a =>
                                context.Approvals.Where(a1 => approvalsIds.Contains(a1.ID))
                                    .Select(ap => ap.Job)
                                    .Contains(a.Job))
                            .Select(ap => ap.ID).ToList();
                }
                catch
                {
                    var versions = new List<int>();
                    versions.AddRange(approvalsIds);
                    return versions;
                }
            }
        }

        public static List<SimulationProfileResponseModel> GetProfilesData(CustomProfilesRequest model)
        {
            if (model.nextProfileId > 0)
            {
                var newProfiles = CustomOrEmbededProfiles(model);
                var newId = model.nextProfileId;
                newProfiles.ForEach(p => p.ID = newId++);
                return newProfiles;
            }
            else
            {
                return GetCustomProfilesData(model);
            }
        }

        public static List<SimulationProfileResponseModel> CustomOrEmbededProfiles(CustomProfilesRequest model)
        {
            var profiles = new List<SimulationProfileResponseModel>();
            using (var context = new DbContextBL())
            {
                //check custom profile or embedded for all versions
                foreach (var version in model.versionsIds.Split(',').Select(int.Parse))
                {
                    var embeddedProfile = (from emb in context.ApprovalEmbeddedProfiles
                                           let printSubstrate = (from mc in context.MediaCategories
                                                                 where mc.ID == emb.PrintSubstrate
                                                                 select mc.Name).FirstOrDefault()
                                           where emb.Approval == version
                                           select new
                                           {
                                               emb.ID,
                                               emb.Name,
                                               PrintSubstrate = printSubstrate,
                                               emb.L,
                                               emb.a,
                                               emb.b,
                                           }).FirstOrDefault();

                    if (embeddedProfile == null)
                    {
                        var customProfile = (from apcp in context.ApprovalCustomICCProfiles
                                             join acp in context.AccountCustomICCProfiles on
                                                 apcp.CustomICCProfile equals acp.ID
                                             let printSubstrate = (from mc in context.MediaCategories
                                                                   where mc.ID == acp.PrintSubstrate
                                                                   select mc.Name).FirstOrDefault()
                                             where apcp.Approval == version
                                             select new
                                             {
                                                 acp.Name,
                                                 acp.ID,
                                                 PrintSubstrate = printSubstrate,
                                                 acp.L,
                                                 acp.a,
                                                 acp.b
                                             }).FirstOrDefault();

                        if (customProfile != null)
                        {
                            profiles.Add(new SimulationProfileResponseModel
                            {
                                DisplayName = customProfile.Name + " (" + GMGColor.Resources.Resources.lblCustom + ")",
                                CustomProfileId = customProfile.ID,
                                MediaCategory = customProfile.PrintSubstrate,
                                L = customProfile.L,
                                a = customProfile.a,
                                b = customProfile.b,
                                VersionId = version
                            });
                        }
                    }
                    else
                    {
                        profiles.Add(new SimulationProfileResponseModel
                        {
                            DisplayName = embeddedProfile.Name + " " + GMGColor.Resources.Resources.lblEmbedded,
                            EmbeddedProfileId = embeddedProfile.ID,
                            MediaCategory = embeddedProfile.PrintSubstrate,
                            L = embeddedProfile.L,
                            a = embeddedProfile.a,
                            b = embeddedProfile.b,
                            VersionId = version
                        });
                    }
                }
            }
            return profiles;
        }

        public static List<SimulationProfileResponseModel> GetCustomProfilesData(CustomProfilesRequest model)
        {
            var profiles = new List<SimulationProfileResponseModel>();
            using (var context = new DbContextBL())
            {
                var data = model.is_external_user
                               ? (from  ec in context.ExternalCollaborators
                                  join acc in context.Accounts on ec.Account equals acc.ID
                                  let simulationProfiles = (from sp in context.SimulationProfiles
                                                            join msmntc in context.MeasurementConditions on sp.MeasurementCondition equals msmntc.ID
                                                            where (sp.AccountId == acc.ID || sp.AccountId == null && !context.ExcludedAccountDefaultProfiles.Any(t => t.Account == acc.ID && t.SimulationProfile == sp.ID))
                                                                    && !sp.IsDeleted && sp.IsValid.HasValue && sp.IsValid.Value
                                                            select new
                                                            {
                                                                sp.ID,
                                                                sp.ProfileName,
                                                                MediaCategory = sp.MediaCategory.HasValue ? (from mct in context.MediaCategories
                                                                                                             where sp.MediaCategory == mct.ID
                                                                                                             select mct.Name).FirstOrDefault()
                                                                                : null,
                                                                MeasurementCondition = msmntc.Name,
                                                                sp.L,
                                                                sp.a,
                                                                sp.b
                                                            }).ToList()
                                  where
                                        ec.Guid.ToLower() == model.user_key.ToLower() &&
                                        !ec.IsDeleted
                                  select
                                      new
                                      {
                                          SimulationProfiles = simulationProfiles
                                      })
                                     .FirstOrDefault()
                               : (from u in context.Users
                                  join acc in context.Accounts on u.Account equals acc.ID
                                  let simulationProfiles = (from sp in context.SimulationProfiles
                                                            join msmntc in context.MeasurementConditions on sp.MeasurementCondition equals msmntc.ID
                                                            where (sp.AccountId == acc.ID || sp.AccountId == null && !context.ExcludedAccountDefaultProfiles.Any(t => t.Account == acc.ID && t.SimulationProfile == sp.ID))
                                                            && !sp.IsDeleted && sp.IsValid.HasValue && sp.IsValid.Value
                                                            select new
                                                            {
                                                                sp.ID,
                                                                sp.ProfileName,
                                                                MediaCategory = sp.MediaCategory.HasValue ? (from mct in context.MediaCategories
                                                                                                             where sp.MediaCategory == mct.ID
                                                                                                             select mct.Name).FirstOrDefault()
                                                                                : null,
                                                                MeasurementCondition = msmntc.Name,
                                                                sp.L,
                                                                sp.a,
                                                                sp.b
                                                            }).ToList()
                                  where u.Guid.ToLower() == model.user_key.ToLower()
                                  select
                                      new
                                      {
                                          SimulationProfiles = simulationProfiles
                                      })
                                     .FirstOrDefault();

                profiles = CustomOrEmbededProfiles(model);

                if (data != null)
                {
                    profiles.AddRange(data.SimulationProfiles.Select(profile => new SimulationProfileResponseModel
                    {
                        DisplayName = profile.ProfileName,
                        SimulationProfileId = profile.ID,
                        MediaCategory = profile.MediaCategory,
                        MeasurementCondition = profile.MeasurementCondition,
                        L = profile.L,
                        a = profile.a,
                        b = profile.b
                    }));

                }

                var dummyId = 1;

                profiles.ForEach(t =>
                {
                    t.ID = dummyId++;
                });
            }
            return profiles;
        }

        public static SoftProofingPaperTintModel GetPaperTintsModel(string user_key, bool isExternal)
        {
            var model = new SoftProofingPaperTintModel();

            using (var context = new DbContextBL())
            {
                model.PaperTints = isExternal?
                                     (from ppt in context.PaperTints
                                      join mc in context.MediaCategories on ppt.MediaCategory equals mc.ID
                                      join ac in context.Accounts on ppt.AccountId equals ac.ID
                                      join ec in context.ExternalCollaborators on ac.ID equals ec.Account
                                      where ec.Guid == user_key
                                      select new SoftProofingPapertint
                                      {
                                          ID = ppt.ID,
                                          MediaCategory = mc.Name,
                                          PaperTintName = ppt.Name
                                      }).ToList()
                                    :
                                    (from ppt in context.PaperTints
                                     join mc in context.MediaCategories on ppt.MediaCategory equals mc.ID
                                     join ac in context.Accounts on ppt.AccountId equals ac.ID
                                     join u in context.Users on ac.ID equals u.Account
                                     where u.Guid == user_key
                                     select new SoftProofingPapertint
                                     {
                                         ID = ppt.ID,
                                         MediaCategory = mc.Name,
                                         PaperTintName = ppt.Name
                                     }).ToList();

                model.PaperTints.Add(new SoftProofingPapertint
                {
                    ID = 0,
                    PaperTintName = GMGColor.Resources.Resources.lblNoPaperTint
                });

                List<int> paperTintsIds = model.PaperTints.Select(t => t.ID).ToList();

                model.PaperTintsMeasurements = (from pptmes in context.PaperTintMeasurements
                                                join measco in context.MeasurementConditions on pptmes.MeasurementCondition equals measco.ID
                                                where paperTintsIds.Contains(pptmes.PaperTint)
                                                select new SoftProofingPapertintMeasurement
                                                {
                                                    ID = pptmes.ID,
                                                    Measurement = measco.Name,
                                                    PaperTint = pptmes.PaperTint,
                                                    L = pptmes.L,
                                                    a = pptmes.a,
                                                    b = pptmes.b
                                                }).ToList();
            }

            return model;
        }

        [ObsoleteAttribute("This method is obsolete. Call GetSoftProofingParamsDefaultOrCreate from collaborate module instead.", false)]
        private static List<SoftProofingSessionParams> GetDefaultSoftProofingParams(DbContextBL context, int approval, int job, bool isCMYK)
        {
            if (isCMYK)
            {
                var jobViewingCond = (from vwcn in context.ViewingConditions
                                      join jvwcn in context.ApprovalJobViewingConditions on vwcn.ID equals jvwcn.ViewingCondition
                                      where jvwcn.Job == job
                                      select new
                                      {
                                          vwcn.ID,
                                          vwcn.SimulationProfile,
                                          vwcn.PaperTint
                                      }).FirstOrDefault();

                //check if job viewing cond are set in job details
                if (jobViewingCond != null && jobViewingCond.SimulationProfile.HasValue)
                {
                    var existingSession = (from sp in context.SoftProofingSessionParams
                                           join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                           where
                                               sp.Approval == approval &&
                                               sp.ActiveChannels == null && vwcnd.PaperTint == jobViewingCond.PaperTint &&
                                               vwcnd.SimulationProfile == jobViewingCond.SimulationProfile
                                           select new SoftProofingSessionParams
                                           {
                                               ID = sp.ID,
                                               ActiveChannels = string.Empty,
                                               OutputRgbProfileName = sp.OutputRGBProfileName,
                                               SessionGuid = sp.SessionGuid,
                                               Version = sp.Approval,
                                               SimulationProfileId = vwcnd.SimulationProfile,
                                               EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                               CustomProfileId = vwcnd.CustomProfile,
                                               PaperTintId = vwcnd.PaperTint,
                                               DefaultSession = true,
                                               HighResolution = sp.HighResolution,
                                               HighResolutionInProgress = sp.HighResolutionInProgress
                                           }).Take(1).FirstOrDefault();

                    if (existingSession != null)
                    {
                        return new List<SoftProofingSessionParams> {existingSession};
                    }
                    else
                    {
                        //create new session if not existging and also send generate tiles
                        var newSession = new SoftProofingSessionParam
                        {
                            Approval = approval,
                            LastAccessedDate = DateTime.UtcNow,
                            SessionGuid = Guid.NewGuid().ToString(),
                            ViewingCondition = jobViewingCond.ID,
                            SimulatePaperTint = true
                        };
                        context.SoftProofingSessionParams.Add(newSession);
                        context.SaveChanges();

                        return new List<SoftProofingSessionParams>
                        {
                            new SoftProofingSessionParams
                            {
                                ID = newSession.ID,
                                ActiveChannels = string.Empty,
                                OutputRgbProfileName = newSession.OutputRGBProfileName,
                                SessionGuid = newSession.SessionGuid,
                                Version = newSession.Approval,
                                SimulationProfileId = jobViewingCond.SimulationProfile,
                                PaperTintId = jobViewingCond.PaperTint,
                                DefaultSession = true
                            }
                        };
                    }
                }
                else
                {
                    int? paperTintId = jobViewingCond != null ? jobViewingCond.PaperTint : null;

                    //check if in job detail only paper tint is selected
                    if (paperTintId.HasValue)
                    {
                        //get default session profile
                        var defaultConditions = (from sp in context.SoftProofingSessionParams
                                                 join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                                 where
                                                     sp.Approval == approval &&
                                                     sp.DefaultSession
                                                 select new
                                                 {
                                                     vwcnd.CustomProfile,
                                                     vwcnd.EmbeddedProfile,
                                                     vwcnd.SimulationProfile
                                                 }).FirstOrDefault();

                        //check if already rendered
                        var existingSession = (from sp in context.SoftProofingSessionParams
                                               join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                               where
                                                   sp.Approval == approval &&
                                                   sp.ActiveChannels == null && vwcnd.PaperTint == paperTintId &&
                                                   vwcnd.SimulationProfile == defaultConditions.SimulationProfile && vwcnd.CustomProfile == defaultConditions.CustomProfile && vwcnd.EmbeddedProfile == defaultConditions.EmbeddedProfile
                                               select new SoftProofingSessionParams
                                               {
                                                   ID = sp.ID,
                                                   ActiveChannels = string.Empty,
                                                   OutputRgbProfileName = sp.OutputRGBProfileName,
                                                   SessionGuid = sp.SessionGuid,
                                                   Version = sp.Approval,
                                                   SimulationProfileId = vwcnd.SimulationProfile,
                                                   EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                                   CustomProfileId = vwcnd.CustomProfile,
                                                   PaperTintId = vwcnd.PaperTint,
                                                   DefaultSession = true,
                                                   HighResolution = sp.HighResolution,
                                                   HighResolutionInProgress = sp.HighResolutionInProgress
                                               }).Take(1).FirstOrDefault();

                        if (existingSession != null)
                        {
                            return new List<SoftProofingSessionParams> { existingSession };
                        }
                        else
                        {
                            var defaultwithPaperTintCond = new ViewingCondition()
                            {
                                SimulationProfile = defaultConditions.SimulationProfile,
                                EmbeddedProfile = defaultConditions.EmbeddedProfile,
                                CustomProfile = defaultConditions.CustomProfile,
                                PaperTint = paperTintId,
                            };

                            //create new session if not existging and also send generate tiles
                            var newSessionWithPaperTint = new SoftProofingSessionParam
                            {
                                Approval = approval,
                                LastAccessedDate = DateTime.UtcNow,
                                SessionGuid = Guid.NewGuid().ToString(),
                                ViewingCondition1 = defaultwithPaperTintCond,
                                SimulatePaperTint = true
                            };

                            context.ViewingConditions.Add(defaultwithPaperTintCond);
                            context.SoftProofingSessionParams.Add(newSessionWithPaperTint);
                            context.SaveChanges();

                            return new List<SoftProofingSessionParams>
                        {
                            new SoftProofingSessionParams
                            {
                                ID = newSessionWithPaperTint.ID,
                                ActiveChannels = string.Empty,
                                OutputRgbProfileName = newSessionWithPaperTint.OutputRGBProfileName,
                                SessionGuid = newSessionWithPaperTint.SessionGuid,
                                Version = newSessionWithPaperTint.Approval,
                                SimulationProfileId = defaultwithPaperTintCond.SimulationProfile,
                                EmbeddedProfileId = defaultwithPaperTintCond.EmbeddedProfile,
                                CustomProfileId = defaultwithPaperTintCond.CustomProfile,
                                PaperTintId = defaultwithPaperTintCond.PaperTint,
                                DefaultSession = true
                            }
                        };
                        }
                    }
                    else
                    {
                        //if no job details sim profile is set return default session
                        return (from sp in context.SoftProofingSessionParams
                                join vwcnd in context.ViewingConditions on sp.ViewingCondition equals vwcnd.ID
                                where
                                    sp.Approval == approval &&
                                    sp.ActiveChannels == null &&
                                    sp.DefaultSession
                                select new SoftProofingSessionParams
                                {
                                    ID = sp.ID,
                                    ActiveChannels = string.Empty,
                                    OutputRgbProfileName = sp.OutputRGBProfileName,
                                    SessionGuid = sp.SessionGuid,
                                    Version = sp.Approval,
                                    SimulationProfileId = vwcnd.SimulationProfile,
                                    CustomProfileId = vwcnd.CustomProfile,
                                    EmbeddedProfileId = vwcnd.EmbeddedProfile,
                                    PaperTintId = vwcnd.PaperTint,
                                    DefaultSession = true,
                                    HighResolution = sp.HighResolution,
                                    HighResolutionInProgress = sp.HighResolutionInProgress
                                }).Take(1).ToList();
                    }
                }
            }

            //for rgb documents just return def session
            return (from sp in context.SoftProofingSessionParams
                    where
                        sp.Approval == approval &&
                        sp.ActiveChannels == null &&
                        sp.DefaultSession
                    select new SoftProofingSessionParams
                    {
                        ID = sp.ID,
                        ActiveChannels = string.Empty,
                        OutputRgbProfileName = sp.OutputRGBProfileName,
                        SessionGuid = sp.SessionGuid,
                        Version = sp.Approval,
                        DefaultSession = true
                    }).Take(1).ToList();
        }

        public static List<SoftProofingSessionParams> GetSoftProofingParams(TilesPageRequest model, ISoftProofingService softProofinService)
        {
            var highResViewConditionSessionGuid = string.Empty;
            SoftProofingSessionParam highResViewConditionSession = null;
            var list = new List<SoftProofingSessionParams>();
            int pageId = model.pageId.GetValueOrDefault();

            using (var context = new DbContextBL())
            {
                int approval = model.Id ?? (from ap in context.ApprovalPages
                                            where ap.ID == pageId
                                            select ap.Approval).FirstOrDefault();

                if (model.PaperTintId == 0)
                {
                    model.PaperTintId = null;
                }

                SoftProofingSessionParam existingSession = ApprovalBL.GetSoftProofingSessionByParams(model, context);

                if (existingSession == null)
                {
                    //check if for the requestered parameters the viewing cond already exists
                    var existingViewingCondition = GetViewingConditionIdByParams(model, context);
                    ViewingCondition newViewingCondition = null;
                    if (existingViewingCondition == null)
                    {
                        //create new vwcn
                        newViewingCondition = new ViewingCondition
                        {
                            SimulationProfile = model.SimulationProfileId,
                            EmbeddedProfile = model.EmbeddedProfileId,
                            CustomProfile = model.CustomProfileId,
                            PaperTint = model.PaperTintId,
                        };
                        context.ViewingConditions.Add(newViewingCondition);
                    }
                    // create new session

                    //create new session item
                    SoftProofingSessionParam newSession;
                    var defaultSession = ApprovalBL.GetDefaultSessionParamsByApprovalId(approval, context);
                    if (model.HighResolution && (defaultSession == null || !defaultSession.HighResolution))
                    {
                        newSession = new SoftProofingSessionParam
                        {
                            ActiveChannels = defaultSession.ActiveChannels,
                            Approval = approval,
                            SessionGuid = Guid.NewGuid().ToString(),
                            ZoomLevel = 0,
                            LastAccessedDate = DateTime.UtcNow,
                            ViewingCondition = defaultSession.ViewingCondition,
                            ViewingCondition1 = defaultSession.ViewingCondition1,
                            SimulatePaperTint = true,
                            HighResolution = true,
                            DefaultSession = true,
                            OutputRGBProfileName = defaultSession.OutputRGBProfileName
                        };

                        var approvalJob = (from a in context.Approvals
                                           join j in context.Jobs on a.Job equals j.ID
                                           where a.ID == approval
                                           select j.ID).FirstOrDefault();
                        var viewingCondition = softProofinService.GetViewingConditionByJobId(approvalJob);
                        var prevSoftProofingParams = softProofinService.GetJobSoftProofingParams(approval, approvalJob);

                        if (viewingCondition != null && prevSoftProofingParams != null)
                        {
                            highResViewConditionSessionGuid = Guid.NewGuid().ToString();
                            highResViewConditionSession = new SoftProofingSessionParam
                            {
                                ActiveChannels = !string.IsNullOrEmpty(prevSoftProofingParams.ActiveChannels) ? prevSoftProofingParams.ActiveChannels : null,
                                Approval = approval,
                                SessionGuid = highResViewConditionSessionGuid,
                                ZoomLevel = 0,
                                LastAccessedDate = DateTime.UtcNow,
                                ViewingCondition = viewingCondition.ID,
                                ViewingCondition1 = viewingCondition,
                                SimulatePaperTint = true,
                                HighResolution = true,
                                OutputRGBProfileName = prevSoftProofingParams.OutputRgbProfileName,
                                HighResolutionInProgress = false
                            };

                            list.Add(GetSPSParamsAPIModel(highResViewConditionSession));
                        }
                    }
                    else
                    {
                        newSession = new SoftProofingSessionParam
                        {
                            ActiveChannels = model.ActiveChannels,
                            Approval = approval,
                            SessionGuid = Guid.NewGuid().ToString(),
                            ZoomLevel = model.ZoomLevel,
                            ViewTiles = model.ViewTiles,
                            LastAccessedDate = DateTime.UtcNow,
                            ViewingCondition1 = newViewingCondition ?? existingViewingCondition,
                            SimulatePaperTint = true,
                            HighResolution = defaultSession.HighResolution,
                            HighResolutionInProgress = defaultSession.HighResolutionInProgress
                        };

                        // in case highres processing is in progress do not save the new session (non highres) in the database
                        // just return the session to the client (ProofStudio) to display a message
                        if(defaultSession.HighResolutionInProgress)
                        {
                            list.Add(GetSPSParamsAPIModel(newSession));
                            return list;
                        }
                    }

                    existingSession = ApprovalBL.GetSoftProofingSessionByParams(model, context);
                    // check again if the session already exits in case two users trigger highres in the same time
                    if (existingSession == null)
                    {
                        context.SoftProofingSessionParams.Add(newSession);
                        list.Add(GetSPSParamsAPIModel(newSession));
                    }
                    else
                    {
                        newSession.HighResolutionInProgress = existingSession.HighResolutionInProgress;
                        existingSession.LastAccessedDate = DateTime.UtcNow;
                        list.Add(GetSPSParamsAPIModel(existingSession));
                    }
                }
                else
                {
                    existingSession.LastAccessedDate = DateTime.UtcNow;
                    list.Add(GetSPSParamsAPIModel(existingSession));
                }
                context.SaveChanges();
            }

            if (!string.IsNullOrEmpty(highResViewConditionSessionGuid))
            {
                list.RemoveAll(item => item.SessionGuid != highResViewConditionSessionGuid);
                softProofinService.AddNewSoftProofinSession(highResViewConditionSession);
            }
            return list;
        }

        // get all tree folders for the logged user
        public static List<TreeFoldersModel> GetTreeFolders(BaseGetEntityRequest model)
        {
            var folders = new List<TreeFoldersModel>();
            using (var context = new DbContextBL())
            {
                if (!model.is_external_user)
                {
                    var loggedUser = context.Users.FirstOrDefault(u => u.Guid == model.user_key);
                    if (loggedUser != null)
                    {
                        List<ReturnFolderTreeView> treefolders = GMGColorDAL.Folder.GetFolderTree(loggedUser.ID, context);
                        folders.AddRange(treefolders.Select(folder => new TreeFoldersModel()
                        {
                            ID = folder.ID,
                            Parent = folder.Parent,
                            Creator = folder.Creator,
                            Level = folder.Level,
                            Name = folder.Name,
                            HasApprovals = folder.HasApprovals
                        }));
                    }
                }
            }
            return folders;
        }

        //// get all approvals linked to a folder or not
        //public static List<ApprovalInTree> GetApprovalsInTree(BaseGetEntityRequest model, int folderId = 0)
        //{
        //    List<ApprovalInTree> approvals = new List<ApprovalInTree>();
        //    using (var context = new DbContextBL())
        //    {
        //        if (!model.is_external_user)
        //        {
        //            var loggedUser = context.Users.FirstOrDefault(u => u.Guid == model.user_key);
        //            if (loggedUser != null)
        //            {
        //                int recCount = 0;
        //                List<ReturnApprovalsPageView> approvalsFromDB;
        //                if (folderId == 0)
        //                {
        //                    approvalsFromDB = GMGColorDAL.Approval.GetApprovals(loggedUser.Account,
        //                        loggedUser.ID, 1, 10, 1, 0, true, "", 0, true, out recCount, context);
        //                }
        //                else
        //                {
        //                    approvalsFromDB = GMGColorDAL.Approval.GetFoldersApprovals(loggedUser.Account,
        //                    loggedUser.ID, folderId, 100, 1, 0, false, "", 0, out recCount, context);
        //                }
        //                approvals.AddRange(approvalsFromDB.Where(a => a.Approval > 0).Select(a => new ApprovalInTree()
        //                {
        //                    ID = a.Approval,
        //                    TreeFolderId = a.Folder,
        //                    Name = a.Title
        //                }));
        //            }
        //        }
        //    }
        //    return approvals;
        //}

        public static SoftProofingSessionParams GetSPSParamsAPIModel(SoftProofingSessionParam sessionParamsBO)
        {
            var sessionModel = new SoftProofingSessionParams()
            {
                ID = sessionParamsBO.ID,
                ActiveChannels = sessionParamsBO.ActiveChannels,
                OutputRgbProfileName = sessionParamsBO.OutputRGBProfileName,
                SessionGuid = sessionParamsBO.SessionGuid,
                Version = sessionParamsBO.Approval,
                SimulationProfileId = sessionParamsBO.ViewingCondition1.SimulationProfile,
                EmbeddedProfileId = sessionParamsBO.ViewingCondition1.EmbeddedProfile,
                CustomProfileId = sessionParamsBO.ViewingCondition1.CustomProfile,
                PaperTintId = sessionParamsBO.ViewingCondition1.PaperTint,
                HighResolution = sessionParamsBO.HighResolution,
                HighResolutionInProgress = sessionParamsBO.HighResolutionInProgress
            };

            return sessionModel;
        }

        public static void GenerateTiles(GenerateTilesRequest model, IApprovalService approvalService, IProofstudioApiService proofstudioApiService)
        {
            SoftProofingSessionParam existingSession;
            SoftProofingSessionParam defaultSession = null;

            using (var context = new DbContextBL())
            {
                existingSession = ApprovalBL.GetSessionParamsBySessionGuid(model.SessionGuid, context);
                if (existingSession == null)
                {
                    return;
                }
                if (existingSession.HighResolution)
                {
                    defaultSession = ApprovalBL.GetDefaultSessionParamsByApprovalId(existingSession.Approval, context);
                }

                if (existingSession.HighResolution && (defaultSession == null || (!defaultSession.HighResolution && !defaultSession.HighResolutionInProgress)))
                {
                    proofstudioApiService.ResetDefaultForNonHighResSessionParams(existingSession.Approval);
                    ApprovalBL.SetHighResSessionInProgress(context, existingSession.Approval, true);

                    var approvalID = existingSession.Approval;
                    var approvalProcessingData = approvalService.GetApprovalProcessingData(approvalID);
                    approvalProcessingData.JobIsHighRes = true;
                    approvalProcessingData.SessionGuid = model.SessionGuid;


                    proofstudioApiService.ResetApprovalPagesProgress(approvalID);
                    approvalService.ResetApprovalProcessingDate(approvalID);

                    ApprovalBL.CreateMessageForMediaDefineQueue(approvalProcessingData, context);
                }
                else if(defaultSession == null || !defaultSession.HighResolutionInProgress)
                {
                    int pageId = model.pageId.GetValueOrDefault();

                    var dictJobParameters = new Dictionary<string, string>
                    {
                        {Constants.PAGEID, Convert.ToString(pageId)},
                        {Constants.SESSIONGUID, model.SessionGuid}
                    };

                    SendMessage(dictJobParameters);
                    //cleanup previous log errors for current page and session
                    SoftProofingSessionErrorBL.CleanupSoftProofingSessionError(pageId, model.SessionGuid);
                }
            }
        }

        private static void SendMessage(Dictionary<string, string> dict)
        {
            GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.TileServerJob);
        }

        public static bool IsPageSoftProofingSessionInError(PageSoftProofingSession pageSession)
        {
            using (var context = new DbContextBL())
            {
                return
                    (from spe in context.SoftProofingSessionErrors
                     join sps in context.SoftProofingSessionParams on spe.SoftProofingSessionParams equals sps.ID
                     where
                         sps.SessionGuid == pageSession.SoftProofingSessionGUID &&
                         spe.ApprovalPage == pageSession.PageID
                     select spe.ID).Any();
            }
        }

        public static int GetCollaboratorId(DbContextBL context, bool isExternal, string guid)
        {
            return isExternal
                       ? (from ec in context.ExternalCollaborators
                          where ec.Guid.ToLower() == guid.ToLower()
                          select ec.ID).FirstOrDefault()
                       : (from u in context.Users
                          where u.Guid.ToLower() == guid.ToLower()
                          select u.ID).FirstOrDefault();
        }

        public static int GetVersionIdByAnnotationId(DbContextBL context, int annotationId)
        {
            return (from aa in context.ApprovalAnnotations
                    join ap in context.ApprovalPages on aa.Page equals ap.ID
                    where aa.ID == annotationId
                    select ap.Approval).FirstOrDefault();
        }

        public static void SetJobStatusChangesComplete(int pageId, string userGuid)
        {
            using (DbContextBL context = new DbContextBL())
            {
                var job = (from j in context.Jobs
                           join ap in context.Approvals on j.ID equals ap.Job
                           let changesCompleteStatus = (from js in context.JobStatus
                                                        where js.Key == "CCM"
                                                        select js.ID).FirstOrDefault()
                           join app in context.ApprovalPages on ap.ID equals app.Approval
                           where app.ID == pageId
                           select new
                           {
                               Job = j,
                               changesCompleteStatus = changesCompleteStatus
                           }).FirstOrDefault();

                var user = (from u in context.Users
                            where u.Guid == userGuid
                            select new UserDetails()
                            {
                                Username = u.Username,
                                Id = u.ID,
                                Email = u.EmailAddress
                            }).FirstOrDefault();

                job.Job.Status = job.changesCompleteStatus;

                JobBL.LogJobStatusChange(job.Job.ID, job.changesCompleteStatus, user.Id, context);

                context.SaveChanges();

                try
                {
                    //Push the job status to the external service
                    var jobDetails = new JobStatusDetails()
                    {
                        jobGuid = job.Job.Guid,
                        jobStatus = job.Job.JobStatu.Name,
                        jobStatusKey = job.Job.JobStatu.Key
                    };
                    ApprovalBL.PushStatus(user, jobDetails, job.Job.Account, context);
                }
                catch (Exception exc)
                {
                    GMGColorDAL.GMGColorLogging.log.Error(exc.Message, exc);
                }
            }
        }

        public static void MarkPsSessionComplete(string sessionGuid)
        {
            using (DbContextBL context = new DbContextBL())
            {
                var session = (from pss in context.ProofStudioSessions
                               where pss.Guid == sessionGuid
                               select  pss).FirstOrDefault();

                if (session != null)
                {
                    session.IsCompleted = true;
                    context.SaveChanges();
                }
            }
        }

        public static void BlockExternalUrl(int approval_id, string user_guid)
        {
            using (DbContextBL context = new DbContextBL())
            {
                var blockUrl = (from sa in context.SharedApprovals
                                join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                where sa.Approval == approval_id && ec.Guid == user_guid
                                select sa).FirstOrDefault();

                var ExpireURLDecisionMadeExit = (from acd in context.ApprovalCollaboratorDecisions
                                                 where acd.Approval == approval_id && acd.ExternalCollaborator == blockUrl.ExternalCollaborator
                                                 select acd.Decision).FirstOrDefault();

                if (blockUrl != null && ExpireURLDecisionMadeExit != null)
                {
                    blockUrl.ExpireDate = blockUrl.ExpireDate.AddDays(-2);
                    context.SaveChanges();

                }
            }
        }

        public static List<AvailableFeature> GetAvailableFeatureList(List<int> featureIds)
        {
            List<AvailableFeature> featureList = new List<AvailableFeature>();

            if (featureIds != null)
            {
                featureIds.ForEach(
                    t =>
                    featureList.Add(new AvailableFeature
                    {
                        ID = t,
                        Title = ((GMG.CoZone.Common.PlanFeatures) t).ToString()
                    }));
            }

            return featureList;
        }

        public static PhaseCompleteResponse IsPhaseCompletedOrVersionRejected(PhaseCompleteGetRequest model)
        {
            PhaseCompleteResponse phaseComplete = null;
            using (var context = new DbContextBL())
            {
                var approvalPhase = (from ajp in context.ApprovalJobPhases
                                     join ad in context.ApprovalDecisions on ajp.ApprovalTrigger equals ad.ID
                                     join a in context.Approvals on ajp.ID equals a.CurrentPhase

                                     let nextPhase = (from ajpw in context.ApprovalJobPhases
                                                      let phase = (from ajw in context.ApprovalJobPhases
                                                                   where ajw.ID == model.currentPhase
                                                                   select new
                                                                   {
                                                                       ajw.ApprovalJobWorkflow,
                                                                       ajw.Position
                                                                   }).FirstOrDefault()
                                                      where
                                                          ajpw.Position > phase.Position &&
                                                          ajpw.ApprovalJobWorkflow == phase.ApprovalJobWorkflow
                                                      orderby ajpw.Position
                                                      select
                                                          new
                                                          {
                                                              ajpw.ID,
                                                              ajpw.PrimaryDecisionMaker,
                                                              ajpw.ExternalPrimaryDecisionMaker
                                                          }).FirstOrDefault()
                                     let approvalJobPhaseTrigger = (from ap in context.ApprovalPhases
                                                                    join apt in context.ApprovalPhaseTrigger on ap.ID equals apt.ApprovalPhase
                                                                    where ap.ID == ajp.PhaseTemplateID
                                                                    select apt.ApprovalDecision.ToString()).ToList()

                                     where ajp.ID == model.currentPhase && a.ID == model.Id
                                     select new
                                     {
                                         approvalJobPhaseTrigger= approvalJobPhaseTrigger,
                                         ApprovalTrigger = ajp.ApprovalTrigger,
                                         ApprovalTriggerPriority = ad.Priority,
                                         DecisionType = ajp.DecisionType,
                                         PrimaryDecisionMaker =  a.PrimaryDecisionMaker,
                                         ExternalPrimaryDecisionMaker = a.ExternalPrimaryDecisionMaker
                                     }).FirstOrDefault();

                if (approvalPhase != null)
                {
                    //var approvalJobPhaseTrigger = (from ap in context.ApprovalPhases
                    //                               join apt in context.ApprovalPhaseTrigger on ap.ID equals apt.ApprovalPhase
                    //                               join apd in context.ApprovalDecisions on apt.ApprovalDecision equals apd.ID
                    //                               where ap.ID == model.currentPhase
                    //                               select apt.ApprovalDecision.ToString()).ToList()

                    phaseComplete = new PhaseCompleteResponse();
                    var decisionPriority = (from ad in context.ApprovalDecisions where ad.ID == model.decisionId select ad.Priority).FirstOrDefault();

                    //check the decision type needed in order to mark the approval as completed
                    if (approvalPhase.DecisionType == (int) ApprovalPhase.ApprovalDecisionType.ApprovalGroup)
                    {
                        var approvalGroupCollaborators = (from acd in context.ApprovalCollaboratorDecisions
                                                          join ac in context.ApprovalCollaborators on acd.Collaborator
                                                              equals ac.Collaborator
                                                          join acr in context.ApprovalCollaboratorRoles on
                                                              ac.ApprovalCollaboratorRole equals acr.ID
                                                          where
                                                              acd.Approval == model.Id &&
                                                              acd.Phase == model.currentPhase &&
                                                              acr.Key == "ANR"
                                                          select new
                                                          {
                                                              Decision = acd.Decision,
                                                              Collaborator = acd.Collaborator,
                                                              IsExternal = false
                                                          }
                                                         ).Distinct().ToList();

                        var approvalGroupExternalCollaborators = (from acd in context.ApprovalCollaboratorDecisions
                                                                  join sa in context.SharedApprovals on acd.ExternalCollaborator equals sa.ExternalCollaborator
                                                                  join acr in context.ApprovalCollaboratorRoles on sa.ApprovalCollaboratorRole equals acr.ID
                                                                  where
                                                                      acd.Approval == model.Id &&
                                                                      acd.Phase == model.currentPhase &&
                                                                      acr.Key == "ANR"
                                                                  select new
                                                                  {
                                                                      Decision = acd.Decision,
                                                                      Collaborator = acd.ExternalCollaborator,
                                                                      IsExternal = true
                                                                  }
                                                           ).Distinct().ToList();

                        approvalGroupCollaborators.AddRange(approvalGroupExternalCollaborators);

                        var allExceptCurrent = approvalGroupCollaborators.Where(ag => !(ag.Collaborator == model.collaboratorId && ag.IsExternal == model.isExternal));
                        if (approvalGroupCollaborators.Any())
                        {
                            if (allExceptCurrent.All(ad => approvalPhase.approvalJobPhaseTrigger.Contains(ad.Decision.ToString())) && approvalPhase.approvalJobPhaseTrigger.Contains(model.decisionId.ToString()))
                            {
                                phaseComplete.isCompleted = true;
                            }

                            if (approvalGroupCollaborators.Any(ad => (ad.Collaborator == model.collaboratorId && ad.IsExternal == model.isExternal) && decisionPriority < (approvalPhase.approvalJobPhaseTrigger.Select(v => int.Parse(v)).Min())))
                            {
                                phaseComplete.isRejected = true;
                            }
                        }
                    }
                    else //if decision type is "Primary Decision Maker"
                    {

                        if ((model.collaboratorId == approvalPhase.PrimaryDecisionMaker ||
                             model.collaboratorId == approvalPhase.ExternalPrimaryDecisionMaker) &&
                            approvalPhase.ApprovalTrigger == model.decisionId)
                        {
                            phaseComplete.isCompleted = true;
                        }

                        if ((model.collaboratorId == approvalPhase.PrimaryDecisionMaker ||
                             model.collaboratorId == approvalPhase.ExternalPrimaryDecisionMaker) &&
                            approvalPhase.ApprovalTriggerPriority > decisionPriority)
                        {
                            phaseComplete.isRejected = true;
                        }
                    }
                }
            }
            return phaseComplete;
        }

        public static void UpdateAnnotationsToPublic(UpdatePublicAnnotationsRequest model)
        {
            using (var context = new DbContextBL())
            {
                var annotations = (from aa in context.ApprovalAnnotations
                                   where model.Id.Contains(aa.ID)
                                   select aa).ToList();

                var annotationReplies = (from aa in context.ApprovalAnnotations
                                         where model.Id.Contains((int)aa.Parent)
                                         select aa).ToList();

                if(annotationReplies.Count() > 0)
                {
                    annotations.AddRange(annotationReplies);
                }


                foreach(var annotation in annotations)
                {
                    annotation.IsPrivateAnnotation = !annotation.IsPrivateAnnotation;
                }

                context.SaveChanges();
            }
        }

        public static void AddEditUserSettings(UserRequest model)
        {
            using (var context = new DbContextBL())
            {
                if (!model.is_external_user)
                {
                    var userId =
                        (from u in context.Users where u.Guid.ToLower() == model.user_key select u.ID).FirstOrDefault();
                    if (userId > 0)
                    {
                        UserBL.AddOrUpdateUserSetting(userId, model.showAnnotationsOfAllPhases, UserSettings.ShowAnnotationsOfAllPhases.ToString(), context);
                    }
                }
                else
                {
                    var externalUser = (from ec in context.ExternalCollaborators
                                        where ec.Guid.ToLower() == model.user_key.ToLower()
                                        select ec).FirstOrDefault();

                    if (externalUser != null)
                    {
                        externalUser.ShowAnnotationsOfAllPhases = model.showAnnotationsOfAllPhases;
                    }
                }

                context.SaveChanges();
            }
        }

        public static ApprovalUsersAndGroupsResponse GetCollaboratorsAndGroups(ApprovalUsersAndGroupsRequest model)
        {
            try
            {
                if (model.approval_id == 0)
                {
                    return null;
                }

                using (DbContextBL context = new DbContextBL())
                {
                    #region Get Versions

                    string timezone = (from aj in context.Approvals
                                       join j in context.Jobs on aj.Job equals j.ID
                                       join a in context.Accounts on j.Account equals a.ID
                                       where aj.ID == model.approval_id && aj.IsDeleted == false
                                       select a.TimeZone).Take(1).FirstOrDefault();

                    var approval = (from a in context.Approvals
                                    join j in context.Jobs on a.Job equals j.ID
                                    where a.ID == model.approval_id
                                    select new
                                    {
                                        a.CurrentPhase,
                                        j.Account,
                                        a.Job,
                                        a.ID
                                    }).FirstOrDefault();

                    var versions = new List<int>();
                    if (model.is_external_user)
                    {
                        // return only current version
                        versions = (from aap in context.Approvals
                                    join sa in context.SharedApprovals on aap.ID equals sa.Approval
                                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                    where
                                        aap.Job == approval.Job && aap.IsDeleted == false &&
                                        ec.Guid.ToLower() == model.user_key.ToLower() && sa.IsExpired == false &&
                                        ec.IsDeleted == false && aap.CurrentPhase == approval.CurrentPhase
                                    select aap.ID).Distinct().ToList();
                    }
                    else
                    {
                        bool isAdministrator = UserIsAdministrator(context, model.user_key) &&
                                               IsShowAllFilesToAdminsChecked(context, approval.Account);

                        versions  = (from aap in context.Approvals
                                    join ac in context.ApprovalCollaborators on aap.ID equals ac.Approval
                                    join u in context.Users on ac.Collaborator equals u.ID
                                    where
                                        aap.Job == approval.Job && aap.IsDeleted == false &&
                                        (isAdministrator || u.Guid.ToLower() == model.user_key.ToLower()) &&
                                        aap.CurrentPhase == approval.CurrentPhase
                                    select aap.ID).Distinct().ToList();
                    }

                    #endregion

                    var result = new ApprovalUsersAndGroupsResponse();
                    result.collaborators_and_group = new CollaboratorsAndGroups() {ID = approval.ID};
                    result.user_groups = GetJobPrimayGroups(context, approval.Job, model.user_key, model.is_external_user);
                    result.user_group_links = GetJobPrimaryGroupDecisions(context, model.user_key, approval.Job);

                    var showAnnotationsFromAllPhases = UserSettings.ShowAnnotationsOfAllPhases.ToString();
                    var userCanViewAllPhasesAnnotations = approval.CurrentPhase != null && (!model.is_external_user ? (from us in context.UserSettings
                                                                                                                       join u in context.Users on us.User equals u.ID
                                                                                                                       where (u.Guid.ToLower() == model.user_key.ToLower()) && us.Setting == showAnnotationsFromAllPhases
                                                                                                                       select us.Value.ToLower() == "true").FirstOrDefault()
                                                                                                                      :
                                                                                                                      (from ec in context.ExternalCollaborators
                                                                                                                       where (ec.Guid.ToLower() == model.user_key.ToLower())
                                                                                                                       select ec.ShowAnnotationsOfAllPhases).FirstOrDefault());
                    var visiblePhases = !model.is_external_user
                                            ? (from aph in context.ApprovalJobPhases
                                               join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                               where ajpa.Approval == model.approval_id && (aph.ShowAnnotationsToUsersOfOtherPhases || aph.ID == approval.CurrentPhase)
                                               select aph.ID).ToList()
                                            : (from aph in context.ApprovalJobPhases
                                               join ajpa in context.ApprovalJobPhaseApprovals on aph.ID equals ajpa.Phase
                                               where ajpa.Approval == model.approval_id && (aph.ShowAnnotationsOfOtherPhasesToExternalUsers || aph.ID == approval.CurrentPhase)
                                               select aph.ID).ToList();

                    result.internal_collaborators = GetCollaborators(context, versions, false,
                                                                     timezone, userCanViewAllPhasesAnnotations, visiblePhases);
                    result.internal_collaborators_links = GetCollaboratorLinks(context, versions,
                                                                               false, timezone, userCanViewAllPhasesAnnotations, visiblePhases);

                    result.external_collaborators = GetCollaborators(context, versions, true,
                                                                     timezone, userCanViewAllPhasesAnnotations, visiblePhases);
                    result.external_collaborators_links = GetCollaboratorLinks(context, versions,
                                                                               true, timezone, userCanViewAllPhasesAnnotations, visiblePhases);

                    return result;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetApproval, ex);
            }
        }

        public static VersionResponse GetVersion(int? versionId, string userKey, bool isExternalUser)
        {
            try
            {
                if (!versionId.HasValue)
                {
                    return null;
                }

                using (DbContextBL context = new DbContextBL())
                {
                    string timezone = (from aj in context.Approvals
                                       join j in context.Jobs on aj.Job equals j.ID
                                       join a in context.Accounts on j.Account equals a.ID
                                       where aj.ID == versionId && aj.IsDeleted == false
                                       select a.TimeZone).Take(1).FirstOrDefault();

                    var version = (from a in context.Approvals
                                   where a.ID == versionId
                                   select new
                                   {
                                       a.CurrentPhase,
                                       a.Job
                                   }).FirstOrDefault();

                    #region Get Version

                    VersionResponse versionResponse = new VersionResponse();

                    versionResponse.version = GetVersion(context, versionId.GetValueOrDefault(), userKey, isExternalUser);

                    var maxVersion = new[] { versionId.GetValueOrDefault() };

                    #endregion

                    versionResponse.user_groups = GetJobPrimayGroups(context, version.Job, userKey, isExternalUser);
                    versionResponse.user_group_links = GetJobPrimaryGroupDecisions(context, userKey, version.Job);

                    versionResponse.internal_collaborators = GetVersionCollaborators(context, maxVersion, false, timezone);
                    versionResponse.internal_collaborators_links = GetVersionCollaboratorLinks(context, maxVersion, false, timezone);

                    versionResponse.external_collaborators = GetVersionCollaborators(context, maxVersion, true, timezone);
                    versionResponse.external_collaborators_links = GetVersionCollaboratorLinks(context, maxVersion, true, timezone);

                    versionResponse.softProofingParams = GetDefaultSoftProofingParams(context, maxVersion[0], version.Job, !versionResponse.version.IsRGBColorSpace);

                    return versionResponse;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApiGetApproval, ex);
            }
        }

        public static bool validateUserAccessToAppModule(string userGuid, GMG.CoZone.Common.AppModule appModule, DbContextBL context)
        {
            var deliverAccess = (from u in context.Users
                                 join acc in context.Accounts on u.Account equals acc.ID
                                 join ur in context.UserRoles on u.ID equals ur.User
                                 join r in context.Roles on ur.Role equals r.ID
                                 join am in context.AppModules on r.AppModule equals am.ID
                                 let hasDeliverSubscriptionPlan = (from acsp in context.AccountSubscriptionPlans
                                                                   join bp in context.BillingPlans on acsp.SelectedBillingPlan equals bp.ID
                                                                   join btp in context.BillingPlanTypes on bp.Type equals btp.ID
                                                                   join appm in context.AppModules on btp.AppModule equals appm.ID
                                                                   where
                                                                     acsp.Account == acc.ID &&
                                                                     appm.Key == (int)appModule
                                                                   select btp.ID).Any()
                                 where
                                         u.Guid == userGuid &&
                                         r.Key != "NON" &&
                                         am.Key == (int)appModule
                                 select new
                                 {
                                     HasDeliverSubscriptionPlan = hasDeliverSubscriptionPlan,
                                     UserID = u.ID
                                 }).FirstOrDefault();
            if (deliverAccess != null && deliverAccess.HasDeliverSubscriptionPlan)
            {
                return GMGColorDAL.ColorProofInstance.AnyAvailableValidInstances(deliverAccess.UserID, context);
            }
            return false;
        }

        public static List<ApprovalInTree> GetPsApprovalsInTree(ApprovalsInTreeRequest model)
        {
            var approvalList = new List<ApprovalInTree>();
            using (DbContextBL context = new DbContextBL())
            {
                List<ReturnApprovalInTreeView> approalsInDb = GMGColorDAL.Approval.GetApprovalsInTree(model.user_key, model.TreeFolderId, model.Page, (int)GMGColorDAL.Approval.ApprovalTypeEnum.Movie, context);

                approvalList.AddRange(approalsInDb.Select(approvalInDb => new ApprovalInTree()
                {
                    ID = approvalInDb.ApprovalID,
                    Name = approvalInDb.FileName,
                    TreeFolderId = approvalInDb.FolderID,
                    Thumbnail = GMGColorDAL.Approval.GetApprovalImage(approvalInDb.Domain, approvalInDb.Region, approvalInDb.Guid, approvalInDb.FileName, GMGColorDAL.Approval.ImageType.Thumbnail),
                    Owner = approvalInDb.ApprovalOwner,
                }).OrderByDescending(t => t.ID));
            }
            return approvalList;
        }

        public static bool CheckApprovalShouldBeLockedOnFirstDecission(LockApprovalRequest model)
        {

            using (DbContextBL context  = new DbContextBL())
            {

                var approval = (from a in context.Approvals
                                where a.ID == model.approvalId
                                select new
                                {
                                    a.ID,
                                    a.LockProofWhenFirstDecisionsMade,
                                    a.OnlyOneDecisionRequired,
                                    a.PrimaryDecisionMaker,
                                    a.ExternalPrimaryDecisionMaker,
                                    a.IsLocked,
                                    a.LockProofWhenAllDecisionsMade,
                                    a.ApprovalCollaboratorDecisions
                                }).FirstOrDefault();


                //check if approval meets conditions to be locked
                if (!approval.IsLocked)
                {
                    if (approval != null &&
                        (approval.LockProofWhenFirstDecisionsMade ||
                         approval.OnlyOneDecisionRequired &&
                         (approval.PrimaryDecisionMaker == model.collaboratorId ||
                          approval.ExternalPrimaryDecisionMaker == model.collaboratorId)))
                    {
                        return true;
                    }
                    else if (approval != null && approval.LockProofWhenAllDecisionsMade)
                    {
                        if (!approval.ApprovalCollaboratorDecisions.Where(t => t.Collaborator != model.collaboratorId).Any(t => t.Decision == null))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static Dictionary<int, string> GetAllGroupsForAccount(Account loggedAccount, GMGColorDAL.User loggedUser, GMGColorDAL.Role.RoleName loggedUserCollaborateRole)
        {
            using (DbContextBL context = new DbContextBL())
            {
                //return context.UserGroups.Where(u => u.Account == accountId).ToDictionary(u => u.ID, u => u.Name);
                return PermissionsBL.GetCollaboratorsAndGroups(loggedAccount, loggedUser, loggedUserCollaborateRole, null, context).Where(ug => ug.IsGroup == true).ToDictionary(g => g.ID, g => g.Name);
            }
        }

        public static Dictionary<int, string> GetAllUsersForAccount(Account loggedAccount, GMGColorDAL.User loggedUser, GMGColorDAL.Role.RoleName loggedUserCollaborateRole)
        {
            using (DbContextBL context = new DbContextBL())
            {
                var internalUsers = PermissionsBL.GetCollaboratorsAndGroups(loggedAccount, loggedUser, loggedUserCollaborateRole, null, context).Where(ug => ug.IsGroup == false && ug.IsExternal == false).ToDictionary(g => g.ID, g => g.Name);
                var externalUsers = PermissionsBL.GetCollaboratorsAndGroups(loggedAccount, loggedUser, loggedUserCollaborateRole, null, context).Where(ug => ug.IsGroup == false && ug.IsExternal == true).ToDictionary(g => -g.ID, g => g.Name);
                return internalUsers.Concat(externalUsers).ToDictionary(x => x.Key, x => x.Value);
            }
        }
        public static Dictionary<int, string> GetAllPhasesForAccount(int accountId)
        {
            using (DbContextBL context = new DbContextBL())
            {
                return (from ajp in context.ApprovalJobPhases
                        join aw in context.ApprovalJobWorkflows on ajp.ApprovalJobWorkflow equals aw.ID
                        where aw.Account == accountId && ajp.PhaseTemplateID != 0
                        group ajp by ajp.PhaseTemplateID into phases
                        select new
                        {
                            pId = phases.Select(t => t.ID).FirstOrDefault(),
                            pName = phases.Select(t => t.Name).FirstOrDefault()
                        }).ToDictionary(ap => ap.pId, ap => ap.pName);
            }
        }
        public static Dictionary<string,string> GetAllWorkflowsForAccount(int accountId)
        {
            using (DbContextBL context = new DbContextBL())
            {
                return context.ApprovalJobWorkflows.Where(u => u.Account == accountId).GroupBy(w => w.Name).Select(ajw => ajw.Key).ToDictionary(w => w, w => w);
            }
        }
        public static bool CheckApprovalIsPDFFile(int approvalID)
        {
            List<int> filetypes = new List<int>() { 3, 30, 31, 32, 33 }; // file types pdf, doc, docx, ppt, pptx
            using (DbContextBL context = new DbContextBL())
            {
                return (from a in context.Approvals
                            //join ft in context.FileTypes on a.FileType equals ft.ID
                        where a.ID == approvalID && filetypes.Contains(a.FileType)
                        select a).Any();
            }
        }
        public static DashboardInstantNotification GetInstantNotificationModel(string userKey, string approvalsIds)
        {
            using (DbContextBL context = new DbContextBL())
            {
                var loggedUserId = context.Users.Where(u => u.Guid.ToLower() == userKey.ToLower()).Select(u=> u.ID).FirstOrDefault();
                return new DashboardInstantNotification
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = loggedUserId,
                    Version = approvalsIds,
                    EntityType = InstantNotificationEntityType.Decision
                };
            }
        }

        private static void UpdateCollaborateSubstituteUser(UpdateApprovalDecision model, int approvalId, int? phaseId, DbContextBL context)
        {
            var updateSubstituecollaborator = (from an in context.ApprovalCollaborators
                                                where an.Approval == approvalId && an.Collaborator == model.collaboratorId && an.Phase == phaseId
                                                select an).FirstOrDefault();

            if (updateSubstituecollaborator != null && !updateSubstituecollaborator.SubstituteUser.HasValue)
            {
                updateSubstituecollaborator.SubstituteUser = model.substituteLoggedUser;
                context.SaveChanges();
            }
        }
    }
}
