﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorBusinessLogic
{
    public class UserDetails
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
