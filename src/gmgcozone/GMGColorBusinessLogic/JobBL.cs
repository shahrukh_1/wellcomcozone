﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GMGColorDAL;
using GMGColorDAL.Common;

namespace GMGColorBusinessLogic
{
    public class JobBL
    {
        public static int GetJobSourceByKey(GMG.CoZone.Common.JobSource jobSource, DbContextBL context)
        {
            return (from js in context.JobSources
                    where js.Key == (int)jobSource
                    select js.ID).SingleOrDefault();
        }

        public static Job GetJobByID(int jobID, DbContextBL context)
        {
            return (from j in context.Jobs
                    where j.ID == jobID
                    select j).FirstOrDefault();
        }

        /// <summary>
        /// Get number of row per page for current user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rowsPerPage"></param>
        /// <param name="settingName"></param>
        /// <param name="defaultValue"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetNumberOfRowsPerPage(int userId, int? rowsPerPage, string settingName, int defaultValue, DbContextBL context)
        {
            var nrOfRows = defaultValue;
            var rowsPerPageSetting = UserBL.GetUserSetting(userId, settingName, context);

            var hasChanges = false;
            //Add the default value for this user if he never set a value for this setting 
            if (rowsPerPageSetting == null)
            {
                UserBL.AddOrUpdateUserSetting(userId, null, settingName, defaultValue, context);
                hasChanges = true;
            }
            else
            {
                //Update the setting value if it was changed
                if (rowsPerPage != null)
                {
                    UserBL.AddOrUpdateUserSetting(userId, rowsPerPageSetting, settingName,
                                                  rowsPerPage.GetValueOrDefault(), context);
                    nrOfRows = rowsPerPage.GetValueOrDefault();
                    hasChanges = true;
                }
                else
                {
                    nrOfRows = Int32.Parse(rowsPerPageSetting.Value);
                }
            }

            if (hasChanges)
            {
                context.SaveChanges();
            }

            return nrOfRows;
        }

        /// <summary>
        /// removes from DB jobs marked as deleted
        /// </summary>
        /// <param name="context"></param>
        public static void RemoveDeletedJobs(DbContextBL context)
        {
            ExceptionBL exceptionBl = null;            

            exceptionBl = RemoveDeliverJobs(context, exceptionBl);

            exceptionBl = RemoveCollaborateJob(context, exceptionBl);

            //if any errors where find during processing
            if (exceptionBl != null)
            {
                throw exceptionBl;
            }
        }

        private static ExceptionBL RemoveCollaborateJob(DbContextBL context, ExceptionBL exceptionBl)
        {
            var approvalJobs = (from ap in context.Approvals
                                join j in context.Jobs on ap.Job equals j.ID
                                join acc in context.Accounts on j.Account equals acc.ID
                                where ap.DeletePermanently
                                select new
                                {
                                    ap.ID,
                                    ap.Guid,
                                    acc.Region,
                                    Account = acc.ID,
                                    ap.CreatedDate,
                                    JobName = j.Title,
                                    JobId = j.ID
                                }).ToList();

            foreach (var ap in approvalJobs)
            {
                string errorMessage;
                if ((errorMessage = Approval.DeleteApproval(ap.ID, context)) == String.Empty)
                {
                    //log job for billing history
                    ApprovalJobDeleteHistory jobdeleteHistory = new ApprovalJobDeleteHistory();
                    jobdeleteHistory.Account = ap.Account;
                    jobdeleteHistory.CreatedDate = ap.CreatedDate;
                    jobdeleteHistory.JobName = ap.JobName;
                    jobdeleteHistory.Job = ap.JobId;
                    context.ApprovalJobDeleteHistories.Add(jobdeleteHistory);

                    //delete job assets
                    string approvalFolder = Path.Combine(GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath,
                        ap.Guid);
                    GMGColorIO.DeleteFolderIfExists(approvalFolder, ap.Region);
                }
                else
                {
                    if (exceptionBl != null)
                    {
                        exceptionBl = new ExceptionBL(ExceptionBLMessages.CouldNotRemoveDeletedJobs, new Exception(errorMessage));
                    }
                }
            }
            context.SaveChanges();
            return exceptionBl;
        }

        private static ExceptionBL RemoveDeliverJobs(DbContextBL context, ExceptionBL exceptionBl)
        {
            var deletedDeliverJobs = (from dj in context.DeliverJobs
                                      join j in context.Jobs on dj.Job equals j.ID
                                      join acc in context.Accounts on j.Account equals acc.ID
                                      where dj.IsDeleted
                                      select new
                                      {
                                          deliverJob = dj,
                                          acc.Region
                                      }).ToList();

            foreach (var dj in deletedDeliverJobs)
            {
                try
                {
                    //delete entities where job is FK
                    context.FTPPresetJobDownloads.RemoveRange(dj.deliverJob.FTPPresetJobDownloads);
                    context.DeliverJobDatas.RemoveRange(dj.deliverJob.DeliverJobDatas);
                    dj.deliverJob.DeliverJobPages.ToList().ForEach(t => context.DeliverJobPageVerificationResults.RemoveRange(t.DeliverJobPageVerificationResults));
                    context.DeliverJobPages.RemoveRange(dj.deliverJob.DeliverJobPages);

                    Job jobBO = dj.deliverJob.Job1;

                    //log job for billing history
                    DeliverJobDeleteHistory jobdeleteHistory = new DeliverJobDeleteHistory();
                    jobdeleteHistory.Account = jobBO.Account;
                    jobdeleteHistory.CreatedDate = dj.deliverJob.CreatedDate;
                    jobdeleteHistory.JobName = jobBO.Title;
                    context.DeliverJobDeleteHistories.Add(jobdeleteHistory);

                    context.DeliverJobs.Remove(dj.deliverJob);
                    context.Jobs.Remove(jobBO);

                    //save changes to make sure we remove assets from s3 only if delete succeeds
                    context.SaveChanges();

                    //delete job assets
                    string deliverFolder = Path.Combine(GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath,
                        dj.deliverJob.Guid);
                    GMGColorIO.DeleteFolderIfExists(deliverFolder, dj.Region);
                }
                catch (Exception ex)
                {
                    if (exceptionBl == null)
                    {
                        exceptionBl = new ExceptionBL(ExceptionBLMessages.CouldNotRemoveDeletedJobs, ex);
                    }
                }
            }
            return exceptionBl;
        }       

        public static void LogJobStatusChange(int job, int jobStatus, int modifier, DbContextBL context)
        {
            JobStatusChangeLog jobstatusChange = new JobStatusChangeLog();
            jobstatusChange.Job = job;
            jobstatusChange.Modifier = modifier;
            jobstatusChange.Status = jobStatus;
            jobstatusChange.ModifiedDate = DateTime.UtcNow;
            context.JobStatusChangeLogs.Add(jobstatusChange);
        }

        public static void DeleteJobVersions(string jobGuid, int userId, DbContextBL context)
        {}

        public static string GetJobTitleByID(int jobId, DbContextBL context)
        {
            return (from j in context.Jobs
                    where j.ID == jobId
                    select j.Title).FirstOrDefault();
        }

        public static int GetJobIdByAppGuid(string guid, DbContextBL context)
        {
            return (from j in context.Approvals
                    where j.Guid == guid
                    select j.Job).FirstOrDefault();
        }
    }
}
