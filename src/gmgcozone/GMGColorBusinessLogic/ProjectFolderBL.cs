﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using System.Text;
using GMGColorDAL.CustomModels;
using System.Threading.Tasks;
using static GMGColorDAL.CustomModels.ProjectFolderModel;
using GMG.CoZone.Common;
using GMGColorNotificationService;

namespace GMGColorBusinessLogic
{
    public class ProjectFolderBL
    {
        public static List<ProjectFolderCollaboratorRole> GetProjectFolderCollaboratorRoles()
        {
            using (DbContextBL context = new DbContextBL())
            {
                return (from pfcr in context.ProjectFolderCollaboratorRoles where pfcr.Key != "OWN" select pfcr).ToList();
            }
        }

        public static bool CheckIfAnyActiveProjectFolderForTagword(int tagwordID, int loggedAccountID, DbContextBL context)
        {
            bool projectFolderExists = (from pf in context.ProjectFolders
                                   join pft in context.ProjectFolderTagwords on pf.ID equals pft.PredefinedTagwords
                                   join pt in context.PredefinedTagwords on pft.PredefinedTagwords equals pt.ID
                                   where pf.Account == loggedAccountID && pt.ID == tagwordID && pf.IsDeleted == false
                                   select pf.ID).Any();
            if (projectFolderExists)
            {
                return false;
            }
            else
            {
                var projectFolderTagwordsToDelete = (from pft in context.ProjectFolderTagwords where pft.PredefinedTagwords == tagwordID select pft).ToList();
                context.ProjectFolderTagwords.RemoveRange(projectFolderTagwordsToDelete);
                context.SaveChanges();
                return true;
            }
        }
        public static string GetProjectFolderApprovalIDs(List<int> selectedProjectIDs, int loggedAccountID)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    var result = (from pf in context.ProjectFolders
                            join j in context.Jobs on pf.ID equals j.ProjectFolder
                            join a in context.Approvals on j.ID equals a.Job
                            where pf.Account == loggedAccountID && j.Account == loggedAccountID && selectedProjectIDs.Contains(pf.ID)
                            select a.ID.ToString()).ToList();
                    string returnResult = string.Empty;
                    foreach(var id in result)
                    {
                        if(returnResult == "")
                        {
                            returnResult = id;
                        } else
                        {
                            returnResult = returnResult + "," + id;
                        }
                    }

                   return returnResult;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetProjectFolderApprovals, ex);
            }
        }
        public static int AddProjectFolder(ProjectFolder model)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    ProjectFolder ProjectModel = new ProjectFolder();
                    ProjectModel.Name = model.Name;
                    ProjectModel.Description = model.Description != null ? model.Description : String.Empty;
                    ProjectModel.Account = model.Account;
                    ProjectModel.Guid = Guid.NewGuid().ToString();
                    ProjectModel.Creator = model.Creator;
                    ProjectModel.CreatedDate = DateTime.UtcNow;
                    ProjectModel.PageGrid = model.PageGrid;
                    ProjectModel.Deadline = model.Deadline;
                    ProjectModel.Status = 1;
                    context.ProjectFolders.Add(ProjectModel);
                    context.SaveChanges();
                    return ProjectModel.ID;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddNewProjectFolder, ex);
            }
        }

        public static void EditProjectFolderDetails(ProjectFolder projectModel, int loggedUserID)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {

                    ProjectFolder projectDetails = (from pf in context.ProjectFolders where pf.ID == projectModel.ID select pf).FirstOrDefault();
                    projectDetails.Name = projectModel.Name;
                    projectDetails.Description = projectModel.Description != null ? projectModel.Description : string.Empty;
                    projectDetails.Modifier = loggedUserID;
                    projectDetails.ModifiedDate = DateTime.UtcNow;
                    projectDetails.PageGrid = projectModel.PageGrid;
                    projectDetails.Deadline = projectModel.Deadline;

                    context.SaveChanges();
                }
                
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotEditProjectFolderDeatils, ex);
            }
        }

        public static void SendNotificationAddNewProjectFolder(GMGColorDAL.User LoggedUser ,int ProjectID, DbContextBL context)
        {
            NotificationServiceBL.CreateNotification(new NewProjectFolderAdded()
            {
                EventCreator = LoggedUser.ID,
                ProjectFolderID = ProjectID,
                Account = LoggedUser.Account
            },LoggedUser,context 
            );
        }

        public static void SendNotificationProjectFolderWasUpdated(GMGColorDAL.User LoggedUser, int ProjectID, DbContextBL context)
        {
            NotificationServiceBL.CreateNotification(new ProjectFolderWasUpdated()
            {
                EventCreator = LoggedUser.ID,
                ProjectFolderID = ProjectID,
                Account = LoggedUser.Account
            }, LoggedUser, context
            );
        }

        public static ProjectFolder GetProjectFolderDetails(int selectedProjectFolderId)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    return (from pf in context.ProjectFolders where pf.ID == selectedProjectFolderId select pf).FirstOrDefault();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveProjectFolderDeatils, ex);
            }
        }

        public static void EditProjectFolderCollaborators(List<ProjectFolderSelectedCollaboratorsModel> selectedCollaborators, int selectedProjectFolderId, int loggedUserID, int loggedAccountId)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    List<int> projectCollaboratorsDetails = (from pf in context.ProjectFolders
                                                             join pc in context.ProjectFolderCollaborators on pf.ID equals pc.ProjectFolder
                                                             join pfcr in context.ProjectFolderCollaboratorRoles on pc.ProjectFolderCollaboratorRole equals pfcr.ID
                                                             join u in context.Users on pc.Collaborator equals u.ID
                                                             where pf.ID == selectedProjectFolderId
                                                             select pc.Collaborator).ToList();

                    List<int> selectedCollabortorsID = selectedCollaborators.Select(x => x.collaboratorID).ToList();

                    List<int> collaboratorsIdToRemove = projectCollaboratorsDetails.Where(x => !selectedCollabortorsID.Contains(x)).Select(x => x).ToList();

                    List<ProjectFolderCollaborator> CollaboratorsToRemove = (from pf in context.ProjectFolders
                                                                             join pc in context.ProjectFolderCollaborators on pf.ID equals pc.ProjectFolder
                                                                             join pfcr in context.ProjectFolderCollaboratorRoles on pc.ProjectFolderCollaboratorRole equals pfcr.ID
                                                                             join u in context.Users on pc.Collaborator equals u.ID
                                                                             where pf.ID == selectedProjectFolderId && collaboratorsIdToRemove.Contains(pc.Collaborator)
                                                                             select pc).ToList();

                    List<ProjectFolderSelectedCollaboratorsModel> collaboratorsToAdd = selectedCollaborators.Where(x => !projectCollaboratorsDetails.Contains(x.collaboratorID)).Select(x => x).ToList();

                    context.ProjectFolderCollaborators.RemoveRange(CollaboratorsToRemove);
                    context.SaveChanges();
                    List<string> tagwords = new List<string>();
                    AddProjectFolderCollaboratorsandTagwords(loggedUserID, selectedProjectFolderId, collaboratorsToAdd, tagwords, loggedAccountId, false);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotEditProjectFolderDeatils, ex);
            }
        }
        public static void EditProjectFolderTagwordsandCollaborators(int selectedProjectFolderId, List<string> selectedTagwords, List<ProjectFolderSelectedCollaboratorsModel> selectedCollaborators, int projectCreatorID, int loggedUserID , int loggedAccountID)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    // removing deleted tagwords
                    List<ProjectFolderTagwordDetails> projectTagwordsDetails = GetProjectFolderTagwords(selectedProjectFolderId, loggedAccountID);

                    List<int> tagwordIdsToRemove = projectTagwordsDetails.Where(x => !selectedTagwords.Contains(x.Tagword)).Select(x => x.ID).ToList();

                    List<ProjectFolderTagword> tagwordsToRemove = (from pf in context.ProjectFolders
                                                                   join pft in context.ProjectFolderTagwords on pf.ID equals pft.ProjectFolder
                                                                   join pt in context.PredefinedTagwords on pft.PredefinedTagwords equals pt.ID
                                                                   where pf.ID == selectedProjectFolderId && tagwordIdsToRemove.Contains(pft.PredefinedTagwords)
                                                                   select pft).ToList();

                    List<string> projectTagwords = (from pf in context.ProjectFolders
                                                                         join pft in context.ProjectFolderTagwords on pf.ID equals pft.ProjectFolder
                                                                         join pt in context.PredefinedTagwords on pft.PredefinedTagwords equals pt.ID
                                                                         where pf.ID == selectedProjectFolderId
                                                                         select pt.Name ).ToList();

                    List<string> tagwordsToAdd = selectedTagwords.Where(x => !projectTagwords.Contains(x)).Select(x => x).ToList();

                    // removing deleted collaborators

                    List<int> projectCollaboratorsDetails = (from pf in context.ProjectFolders
                                                             join pc in context.ProjectFolderCollaborators on pf.ID equals pc.ProjectFolder
                                                             join pfcr in context.ProjectFolderCollaboratorRoles on pc.ProjectFolderCollaboratorRole equals pfcr.ID
                                                             join u in context.Users on pc.Collaborator equals u.ID
                                                             where pf.ID == selectedProjectFolderId
                                                             select pc.Collaborator).ToList();

                    List<int> selectedCollabortorsID = selectedCollaborators.Select(x => x.collaboratorID).ToList();

                    List<int> collaboratorsIdToRemove = projectCollaboratorsDetails.Where(x => !selectedCollabortorsID.Contains(x)).Select(x => x).ToList();

                    List<ProjectFolderCollaborator> CollaboratorsToRemove = (from pf in context.ProjectFolders
                                                             join pc in context.ProjectFolderCollaborators on pf.ID equals pc.ProjectFolder
                                                             join pfcr in context.ProjectFolderCollaboratorRoles on pc.ProjectFolderCollaboratorRole equals pfcr.ID
                                                             join u in context.Users on pc.Collaborator equals u.ID
                                                             where pf.ID == selectedProjectFolderId && collaboratorsIdToRemove.Contains(pc.Collaborator)
                                                             select pc).ToList();

                    List<ProjectFolderSelectedCollaboratorsModel> collaboratorsToAdd = selectedCollaborators.Where(x => !projectCollaboratorsDetails.Contains(x.collaboratorID)).Select(x => x).ToList();

                    context.ProjectFolderTagwords.RemoveRange(tagwordsToRemove);
                    context.ProjectFolderCollaborators.RemoveRange(CollaboratorsToRemove);
                    context.SaveChanges();
                    AddProjectFolderCollaboratorsandTagwords(loggedUserID, selectedProjectFolderId, collaboratorsToAdd, tagwordsToAdd, loggedAccountID, false);

                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotEditProjectFolderDeatils, ex);
            }
        }
        public static int GetProjectFolderIdFromApproval(int approvalId)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    return (from a in context.Approvals
                            join j in context.Jobs on a.Job equals j.ID
                            where a.ID == approvalId
                            select (int)j.ProjectFolder).FirstOrDefault();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveProjectFolderDeatils, ex);
            }
        }
        public static List<ProjectFolderTagwordDetails> GetProjectFolderTagwords(int selectedProjectFolderId, int loggedAccountID)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    return (from pf in context.ProjectFolders
                            join pft in context.ProjectFolderTagwords on pf.ID equals pft.ProjectFolder
                            join pt in context.PredefinedTagwords on pft.PredefinedTagwords equals pt.ID
                            where pf.ID == selectedProjectFolderId && pf.Account == loggedAccountID
                            select new ProjectFolderTagwordDetails { ID = pt.ID, Tagword = pt.Name }).Distinct().ToList();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveProjectFolderDeatils, ex);
            }
        }
        public static int GetProjectFolderCreatorID(int projectFolderID)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    return (from pf in context.ProjectFolders where pf.ID == projectFolderID select pf.Creator).FirstOrDefault();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveProjectFolderDeatils, ex);
            }

        }
        public static List<ProjectFolderCollaboratorsDetails> GetProjectFolderCollaboratorDetails(int selectedProjectFolderId, int projectCreatorID)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    return (from pf in context.ProjectFolders
                            join pc in context.ProjectFolderCollaborators on pf.ID equals pc.ProjectFolder
                            join pfcr in context.ProjectFolderCollaboratorRoles on pc.ProjectFolderCollaboratorRole equals pfcr.ID
                            join u in context.Users on pc.Collaborator equals u.ID
                            where pf.ID == selectedProjectFolderId
                            select new ProjectFolderCollaboratorsDetails
                            {
                                CollaboratorID = u.ID,
                                CollaboratorName = u.GivenName +" "+ u.FamilyName,
                                CollaboratorEmailAddress = u.EmailAddress,
                                CollaboratorIsPDM = pc.IsPrimaryDecisionMaker,
                                CollaboratorRole = pc.ProjectFolderCollaboratorRole,
                                CollaboratorRoleName = pfcr.Name,
                                IsCollaboratorCreator = (u.ID == projectCreatorID ? true : false)
                            }).ToList();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveProjectFolderDeatils, ex);
            }
        }
        public static int AddProjectFolderCollaboratorsandTagwords(int loggedUserID, int projectFolderID, List<ProjectFolderSelectedCollaboratorsModel> selectedCollaborators, List<string> selectedTagwords, int loggedAccountId , bool IsNewProject)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    List<ProjectFolderCollaborator> collaborators = new List<ProjectFolderCollaborator>();
                    foreach(var collaborator in selectedCollaborators)
                    {
                        ProjectFolderCollaborator model = new ProjectFolderCollaborator();
                        model.ProjectFolder = projectFolderID;
                        model.Collaborator = collaborator.collaboratorID;
                        model.ProjectFolderCollaboratorRole = collaborator.roleID;
                        model.AssignedDate = DateTime.UtcNow;
                        model.IsPrimaryDecisionMaker = collaborator.isPDM;
                        collaborators.Add(model);
                    }
                    context.ProjectFolderCollaborators.AddRange(collaborators);

                    List<int> SelectedtagwordIds = (from pt in context.PredefinedTagwords where selectedTagwords.Contains(pt.Name) && pt.Account == loggedAccountId select pt.ID).ToList();
                    List<ProjectFolderTagword> projectFolderTagwords = new List<ProjectFolderTagword>();
                    foreach(var tagword in SelectedtagwordIds)
                    {
                        ProjectFolderTagword model = new ProjectFolderTagword();
                        model.ProjectFolder = projectFolderID;
                        model.Creator = loggedUserID;
                        model.CreatedDate = DateTime.UtcNow;
                        model.PredefinedTagwords = tagword;
                        projectFolderTagwords.Add(model);
                    }
                    context.ProjectFolderTagwords.AddRange(projectFolderTagwords);
                    context.SaveChanges();

                    //Send Notification to New Users
                    if(selectedCollaborators.Count > 0 && !IsNewProject)
                    { 
                        var notificationItem = new ProjectFolderWasShared();
                        notificationItem.EventCreator = loggedUserID;
                        notificationItem.ProjectFolderID = projectFolderID;
                        notificationItem.Account = loggedAccountId;
                        foreach (var recipient in selectedCollaborators)
                        {
                            NotificationItem notification = new NotificationItem()
                            {
                                NotificationType = (int)NotificationType.ProjectFolderWasShared + 7,
                                CreatedDate = DateTime.UtcNow,
                                Creator = notificationItem.EventCreator,
                                CustomFields = notificationItem.CustomFieldsToXML(),
                                InternalRecipient = recipient.collaboratorID,
                                PSSessionID = null
                            };
                            context.NotificationItems.Add(notification);
                        }
                        context.SaveChanges();
                    }

                    return 1;
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddNewProjectFolder, ex);
            }
        }

        public static void ArchiveProjectFolder(int projectFolderID, int userId, DbContextBL context)
        {
            var objProjectFolder = (from a in context.ProjectFolders where a.ID == projectFolderID select a).FirstOrDefault();

            if (objProjectFolder != null)
            {
                //Check is the status is Notassigned
                if (objProjectFolder.Status == 1)
                    objProjectFolder.IsArchived = true;
                objProjectFolder.Status = (int)ProjectFolderStatusID.Archived;
                objProjectFolder.Modifier = userId;
                objProjectFolder.ModifiedDate = DateTime.UtcNow;
            }
        }

        public static void RestoreProjectFolder(int projectFolderID, int userId, DbContextBL context)
        {
            var objProjectFolder = (from a in context.ProjectFolders where a.ID == projectFolderID select a).FirstOrDefault();

            if (objProjectFolder != null)
            {
                //Check is the status is Notassigned and restore to same
                if ((bool)objProjectFolder.IsArchived)
                {
                    objProjectFolder.IsArchived = false;
                    objProjectFolder.Status = (int)ProjectFolderStatusID.NotAssigned;
                }
                else
                {
                    objProjectFolder.Status = (int)ProjectFolderStatusID.Ongoing;
                }
                objProjectFolder.Modifier = userId;
                objProjectFolder.ModifiedDate = DateTime.UtcNow;
            }  
        }

        public static DateTime GetFormatedProjectFolderDeadline(string deadlineDate, string dateFormat)
        {
            DateTime deadline = DateTime.ParseExact(deadlineDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
            return deadline;
        }

    }
}
