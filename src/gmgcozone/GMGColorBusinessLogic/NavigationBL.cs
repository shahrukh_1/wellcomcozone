﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMGColorBusinessLogic
{
    public class NavigationBL : BaseBL
    {
        private const string cacheLoggedUserNavigationItems = "LUNI_"; // logged user navigation items

        public static bool IsApprovalDashboard(int menuItemId, DbContextBL context)
        {
            try
            {
                return (from mi in context.MenuItems
                        where mi.ID == menuItemId && (mi.Key == "APDB" || mi.Key == "MPAI")
                        select mi.ID).Any();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfApprovalDashboard, ex);
            }
        }

        public static bool IsApprovalDetailsDashboard(int menuItemId, DbContextBL context)
        {
            try
            {
                return (from mi in context.MenuItems
                        where mi.ID == menuItemId && mi.Key == "APDE"
                        select mi.ID).Any();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfApprovalDetailsDashboard, ex);
            }
        }

        public static Dictionary<string, List<UserMenuItemRoleView>> GetNavigationItems(DbContextBL context, int userId)
        {
            try
            {
                var menus = (from umirv in context.UserMenuItemRoleViews
                             join r in context.Roles on umirv.Role equals r.ID
                             join ur in context.UserRoles on r.ID equals ur.Role
                             join u in context.Users on ur.User equals u.ID
                             join us in context.UserStatus on u.Status equals us.ID
                             where
                                 u.ID == userId && us.Key == "A" && umirv.IsVisible == true
                             select new
                                        {
                                            umirv.Position,
                                            umirv.MenuItem,
                                            umirv.Parent,
                                            umirv.Key,
                                            umirv.Controller,
                                            umirv.Action,
                                            umirv.IsSubNav,
                                            umirv.IsTopNav,
                                            IsModule =
                                 umirv.Key.ToUpper() == "APIN" || umirv.Key.ToUpper() == "DLIN" ||
                                 umirv.Key.ToUpper() == "MNIN" || umirv.Key.ToUpper() == "MYPF",
                                            IsAccountSettings = umirv.Key.ToUpper() == "SEIN",
                                            IsEditProfile = umirv.Key.ToUpper() == "PSIN",
                                            IsAdministration =
                                 umirv.Key.ToUpper() == "ACIN" || umirv.Key.ToUpper() == "PLIN" ||
                                 umirv.Key.ToUpper() == "RPIN" || umirv.Key.ToUpper() == "APST", 
                                        }).Distinct().ToList();

                var modulesMenus =
                    menus.Where(o => o.IsModule).Select(o => new UserMenuItemRoleView()
                                                                 {
                                                                     Position = o.Position,
                                                                     MenuItem = o.MenuItem,
                                                                     Parent = o.Parent,
                                                                     Key = o.Key,
                                                                     Controller = o.Controller,
                                                                     Action = o.Action
                                                                 }).ToList();

                var accountSettingMenus = menus.Where(o => o.IsAccountSettings).Select(
                        o => new UserMenuItemRoleView()
                                 {
                                     Position = o.Position,
                                     MenuItem = o.MenuItem,
                                     Parent = o.Parent,
                                     Key = o.Key,
                                     Controller = o.Controller,
                                     Action = o.Action
                                 }).ToList();

                var editProfileMenus = menus.Where(o => o.IsEditProfile).Select(
                    o => new UserMenuItemRoleView()
                             {
                                 Position = o.Position,
                                 MenuItem = o.MenuItem,
                                 Parent = o.Parent,
                                 Key = o.Key,
                                 Controller = o.Controller,
                                 Action = o.Action
                             }).ToList();

                var adminProfileMenus = menus.Where(o => o.IsAdministration).Select(
                   o => new UserMenuItemRoleView()
                   {
                       Position = o.Position,
                       MenuItem = o.MenuItem,
                       Parent = o.Parent,
                       Key = o.Key,
                       Controller = o.Controller,
                       Action = o.Action
                   }).ToList();

                return new Dictionary<string, List<UserMenuItemRoleView>>
                           {
                               {"", modulesMenus},
                               {"SETS", accountSettingMenus},
                               {"ACST", adminProfileMenus},
                               {"PSIN", editProfileMenus}
                           };
            }
            catch (ExceptionBL exbl)
            {
                GMGColorDAL.GMGColorLogging.log.Error(exbl.Message, exbl);
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
            }

            return new Dictionary<string, List<UserMenuItemRoleView>>();
        }

        public static List<NavigationItem> GetSecondaryNavigations(DbContextBL context, int userId, Account account, SecondaryNavigationTypeEnum navigationType, int? selectedAccountToEdit)
        {
            List<NavigationItem> list = new List<NavigationItem>();
            try
            {
                string menuKey = string.Empty;
                switch (navigationType)
                {
                    case SecondaryNavigationTypeEnum.AccountSettings:
                        menuKey = "SEIN";
                        break;
                    case SecondaryNavigationTypeEnum.AdminSettings:
                        menuKey = "GSET";
                        break;
                    case SecondaryNavigationTypeEnum.EditAccountSettings:
                        menuKey = "ACIN";
                        break;
                    case SecondaryNavigationTypeEnum.EditAccountReports:
                        menuKey = "RPIN";
                        break;
                }

                int parentMenuItemId = (from mi in context.MenuItems where mi.Key == menuKey && mi.Parent == null select mi.ID).Take(1).FirstOrDefault();

                List<SecondaryNavigationMenuItem> navItems = context.GetSecondaryNavigationMenuItems(userId, account.ID, parentMenuItemId).ToList();

                // Special case for HQ(gmgcozone), remove Account Settings Summary and Groups
                if (AccountType.GetAccountType(account, context) == GMGColorDAL.AccountType.Type.GMGColor)
                {
                    if (navigationType == SecondaryNavigationTypeEnum.EditAccountReports)
                    {
                        navItems.RemoveAll(o => o.MenuKey != "RPAD" && o.MenuKey != "RPPL" && o.MenuKey != "RPBI");

                        //special case for SysAdmin, remove Plans and Billing Info menu items from Reports -> Account Report page if selected account is not direct child of the SysAdmin
                        if (selectedAccountToEdit != 0 && !account.GetChilds(context).Select(ac => ac.ID).Contains(selectedAccountToEdit.GetValueOrDefault()))
                        {
                            navItems.RemoveAll(o => o.MenuKey != "RPAD");
                        }
                    }
                    
                    navItems.RemoveAll(o => o.MenuKey == "SESM" || o.MenuKey == "SEGR");
                }

                List<NavigationItem> items = navItems.Select(o => new NavigationItem
                                                                      {
                                                                          Action = o.Action,
                                                                          Parent = o.Parent,
                                                                          Active = o.Active.HasValue && o.Active.Value,
                                                                          Controller = o.Controller,
                                                                          DisplayName = o.DisplayName,
                                                                          Level = o.Level,
                                                                          MenuId = o.MenuId,
                                                                          MenuKey = o.MenuKey,
                                                                          Position = o.Position
                                                                      }).ToList();

                foreach (var navigationItem in items)
                {
                    navigationItem.DisplayName = NavigationModel.GetLocalizedMenuName(navigationItem.MenuKey).Trim();
                }

                list.AddRange(FillRecursive(items, parentMenuItemId));

                return list;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetSecondaryNavigations, ex);
            }
        }

        private static List<NavigationItem> FillRecursive(List<NavigationItem> flatObjects, int parentId = 0)
        {
            List<NavigationItem> recursiveObjects = new List<NavigationItem>();

            foreach (var item in flatObjects.Where(x => x.Parent == parentId).OrderBy(x=>x.Position))
            {
                item.Childs = FillRecursive(flatObjects, item.MenuId);
                recursiveObjects.Add(item);
            }

            return recursiveObjects;
        }

        public static MenuItem.MenuItemPair GetParentMenuId(DbContextBL context, int menuId)
        {
            return GMGColorDAL.MenuItem.GetParentMenuId(menuId, context);
        }

        public static string GetNavigationCacheKeyForSelectedAccount(SecondaryNavigationTypeEnum type, int loggedAccount, int? selectedAccount = null)
        {
            return selectedAccount == null ? String.Format("{0}{1}_{2}_", cacheLoggedUserNavigationItems, type, loggedAccount) 
                                           : String.Format("{0}{1}_{2}_{3}", cacheLoggedUserNavigationItems, type, loggedAccount, selectedAccount);
        }

        /// <summary>
        /// Cache key for navigation item
        /// </summary>
        /// <returns></returns>
        public static string GetNavigationCacheKey(SecondaryNavigationTypeEnum type, int loggedAccount)
        {
            return String.Format("{0}{1}_{2}", cacheLoggedUserNavigationItems, type.ToString(), loggedAccount);
        }
    }

}
