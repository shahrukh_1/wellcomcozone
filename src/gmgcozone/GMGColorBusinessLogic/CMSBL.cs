﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;

namespace GMGColorBusinessLogic
{
    public class CMSBL : BaseBL
    {

        #region Methods

        /// <summary>
        ///  Repair filename of the uploaded files
        /// </summary>
        /// <param name="newManuals"></param>
        public static void RepairFileName(List<CMSItemModel> newCMSItems)
        {
            try
            {
                foreach (CMSItemModel itemModel in newCMSItems)
                {
                    if (itemModel.IsDeleted)
                        continue;

                    // check if filename contains more than one file and keep only the first file
                    string filename = (itemModel.FileName ?? string.Empty).Split(new string[] { "|" },
                        StringSplitOptions.
                            RemoveEmptyEntries).
                        FirstOrDefault() ?? string.Empty;

                    itemModel.FileName = filename;
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRepairFileName, ex);
            }
        }

        public static List<CMSItemModel> GetItems(GMGColorContext context, Enums.CMSItemsType type)
        {
            try
            {
                List<CMSItemModel> result = new List<CMSItemModel>();
                switch (type)
                {
                    case Enums.CMSItemsType.Manuals:
                        {
                            List<CMSManual> lstCMSManualsBo = CMSManual.GetAllCMSManuals(context);
                            lstCMSManualsBo.ForEach(t => result.Add(new CMSItemModel
                            {
                                FileGuid = t.FileGuid,
                                Name = t.Name,
                                FileName = t.FileName,
                                Description = t.Description,
                                IsDeleted = t.IsDeleted,
                                Url = BuildCMSHomeUrls(context, t.Account, t.FileGuid, t.FileName, Enums.CMSItemsType.Manuals),
                                Account = t.Account
                            }));
                            break;
                        }
                    case Enums.CMSItemsType.Downloads:
                        {
                            List<CMSDownload> lstCMSDownloadssBo = CMSDownload.GetAllCMSDownloads(context);
                            List<FileType> lstFileType = context.FileTypes.ToList();
                            foreach (var obj in lstCMSDownloadssBo)
                            {
                                CMSItemModel objCMSItemModel = new CMSItemModel();

                                objCMSItemModel.FileGuid = obj.FileGuid;
                                objCMSItemModel.Name = obj.Name;
                                objCMSItemModel.FileName = obj.FileName;
                                objCMSItemModel.Description = obj.Description;
                                objCMSItemModel.IsDeleted = obj.IsDeleted;
                                objCMSItemModel.Url = BuildCMSHomeUrls(context, obj.Account, obj.FileGuid, obj.FileName, Enums.CMSItemsType.Downloads);
                                objCMSItemModel.Account = obj.Account;
                                objCMSItemModel.mimetype = GetFileMimeType(obj.FileName, lstFileType);
                                result.Add(objCMSItemModel);
                            }
                            
                            break;
                        }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetCMSItems, ex);
            }
        }

        public static bool AddNewFiles(AccountSettings.CMSItems model, List<CMSItemModel> newItems, GMGColorContext context, Account loggedAccount)
        {
            try
            {
                // Add new Files - first try to save them to S3
                bool addSuccess = true;
                foreach (CMSItemModel itemModel in newItems)
                {
                    int indexOfFile = newItems.IndexOf(itemModel);
                    bool isUnique = true;

                    for (int i = indexOfFile + 1; i < newItems.Count; i++)
                    {
                        if (newItems[i].FileName == itemModel.FileName)
                        {
                            isUnique = false;
                            break;
                        }
                    }

                    string sourceAttachedFilePath = model.UserPathToTempFolder + itemModel.FileName;
                    itemModel.FileGuid = Guid.NewGuid().ToString();

                    string relativeFolderPath = string.Empty;
                    switch (model.Type)
                    {
                        case Enums.CMSItemsType.Manuals:
                            if (!isSysAdmin(loggedAccount.AccountType))
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath, loggedAccount.Guid, GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath, itemModel.FileGuid);
                            }
                            else
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath, itemModel.FileGuid);
                            }
                            break;
                        case Enums.CMSItemsType.Downloads:
                            if (!isSysAdmin(loggedAccount.AccountType))
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath, loggedAccount.Guid, GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSDownloadsFolderRelativePath, itemModel.FileGuid);
                            }
                            else
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSDownloadsFolderRelativePath, itemModel.FileGuid);
                            }
                            break;
                    }

                    string destinationFilePath = relativeFolderPath + itemModel.FileName;
                    if (GMGColorIO.FolderExists(relativeFolderPath, loggedAccount.Region, true) && GMGColorIO.FileExists(sourceAttachedFilePath, loggedAccount.Region))
                    {
                        if (!isUnique)
                        {
                            if (!GMGColorIO.CopyFile(sourceAttachedFilePath, destinationFilePath, loggedAccount.Region))
                            {
                                addSuccess = false;
                            }
                        }
                        else
                        {
                            if (!GMGColorIO.MoveFile(sourceAttachedFilePath, destinationFilePath, loggedAccount.Region))
                            {
                                addSuccess = false;
                            }
                        }
                    }
                    else
                    {
                        addSuccess = false;
                    }
                }
                if (addSuccess)
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        foreach (CMSItemModel manualModel in newItems)
                        {
                            switch (model.Type)
                            {
                                case Enums.CMSItemsType.Manuals:
                                {
                                    GMGColorDAL.CMSManual newManual = new GMGColorDAL.CMSManual()
                                    {
                                        FileGuid = manualModel.FileGuid,
                                        FileName = manualModel.FileName,
                                        Name = manualModel.Name,
                                        Description = manualModel.Description ?? string.Empty,
                                        IsDeleted = manualModel.IsDeleted,
                                    };
                                    if (!isSysAdmin(loggedAccount.AccountType))
                                    {
                                        newManual.Account = loggedAccount.ID;
                                    }
                                    context.CMSManuals.Add(newManual);
                                    break;
                                }
                                case Enums.CMSItemsType.Downloads:
                                {
                                    GMGColorDAL.CMSDownload newDownload = new GMGColorDAL.CMSDownload()
                                    {
                                        FileGuid = manualModel.FileGuid,
                                        FileName = manualModel.FileName,
                                        Name = manualModel.Name,
                                        Description = manualModel.Description ?? string.Empty,
                                        IsDeleted = manualModel.IsDeleted
                                    };
                                    if (!isSysAdmin(loggedAccount.AccountType))
                                    {
                                        newDownload.Account = loggedAccount.ID;
                                    }
                                    context.CMSDownloads.Add(newDownload);
                                    break;
                                }
                            }
                        }
                        context.SaveChanges();
                        ts.Complete();
                    }
                }
                return addSuccess;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new DbEntityValidationException(ExceptionBLMessages.CouldNotAddNewFiles,ex);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddNewFiles, ex);
            }
        }

        public static void UpdateExistingManualsFiles(AccountSettings.CMSItems model, List<CMSItemModel> updatedItems, GMGColorContext context)
        {
            try
            {
                // Update Name and Description or delete Profile
                List<string> fileGuids = updatedItems.Select(t => t.FileGuid).ToList();

                List<GMGColorDAL.CMSManual> existingManuals = (new GMGColorDAL.CMSManual()).GetManuals(fileGuids, context);
                foreach (CMSItemModel updatedManual in updatedItems)
                {
                    GMGColorDAL.CMSManual existingManual = existingManuals.SingleOrDefault(t => t.FileGuid == updatedManual.FileGuid);
                    if (existingManual != null)
                    {
                        if (updatedManual.IsDeleted)
                        {
                            DALUtils.Delete<GMGColorDAL.CMSManual>(context, existingManual);
                            var relativeFolderPath = string.Empty; 
                            var onwerAccount = (from cmsm in context.CMSManuals
                                               join acc in context.Accounts on cmsm.Account equals acc.ID
                                               where cmsm.FileGuid == updatedManual.FileGuid
                                               select new
                                               {
                                                   acc.Guid,
                                                   acc.AccountType
                                               }).FirstOrDefault();
                            if (onwerAccount == null)
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath, updatedManual.FileGuid);   
                            }
                            else
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath, onwerAccount.Guid, GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath, updatedManual.FileGuid);
                            }
                            GMGColorIO.DeleteFolderIfExists(relativeFolderPath, model.Region);
                        }
                        else
                        {
                            existingManual.Name = updatedManual.Name;
                            existingManual.Description = updatedManual.Description ?? string.Empty;
                        }
                    }
                }
                context.SaveChanges();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateExistingFiles, ex);
            }
        }

        public static void UpdateExistingDownloadsFiles(AccountSettings.CMSItems model, List<CMSItemModel> updatedItems, GMGColorContext context)
        {
            try
            {
                // Update Name and Description or delete Profile
                List<string> fileGuids = updatedItems.Select(t => t.FileGuid).ToList();

                List<GMGColorDAL.CMSDownload> existingDownloads = (new GMGColorDAL.CMSDownload()).GetDownloads(fileGuids, context);
                foreach (CMSItemModel updatedDownload in updatedItems)
                {
                    GMGColorDAL.CMSDownload existingDownload = existingDownloads.SingleOrDefault(t => t.FileGuid == updatedDownload.FileGuid);
                    if (existingDownload != null)
                    {
                        if (updatedDownload.IsDeleted)
                        {
                            DALUtils.Delete<GMGColorDAL.CMSDownload>(context, existingDownload);
                            var relativeFolderPath = string.Empty;
                            var onwerAccount = (from cmsm in context.CMSManuals
                                                join acc in context.Accounts on cmsm.Account equals acc.ID
                                                where cmsm.FileGuid == updatedDownload.FileGuid
                                                select new
                                                {
                                                    acc.Guid,
                                                    acc.AccountType
                                                }).FirstOrDefault();
                            if (onwerAccount == null)
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath, updatedDownload.FileGuid);
                            }
                            else
                            {
                                relativeFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath, onwerAccount.Guid, GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath, GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath, updatedDownload.FileGuid);
                            }
                            GMGColorIO.DeleteFolderIfExists(relativeFolderPath, model.Region);
                        }
                        else
                        {
                            existingDownload.Name = updatedDownload.Name;
                            existingDownload.Description = updatedDownload.Description ?? string.Empty;
                        }
                    }
                }
                context.SaveChanges();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateExistingFiles, ex);
            }
        }

        public static bool isSysAdmin(int accountType)
        {
            if (accountType == (int)AccountType.Type.GMGColor)
            {
                return true;
            }
            return false;
        }

        public static string BuildCMSHomeUrls(GMGColorContext context, int? accountID, string fileGuid, string fileName, Enums.CMSItemsType itemType)
        {
            var folderRelativePath = GMGColorConfiguration.AppConfiguration.CMSManualsFolderRelativePath;
            if (itemType == Enums.CMSItemsType.Downloads)
            {
                folderRelativePath = GMGColorConfiguration.AppConfiguration.CMSDownloadsFolderRelativePath;
            }

            if (accountID != null)
            {
                var fileUploaderGuid = (from u in context.Accounts
                                        where u.ID == accountID
                                        select u.Guid).FirstOrDefault();
                return Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath,
                                            fileUploaderGuid,
                                            GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath,
                                            folderRelativePath, 
                                            fileGuid) + fileName;
            }
            else
            {
                return Utils.PathCombine(GMGColorConfiguration.AppConfiguration.CMSFolderRelativePath,
                                           folderRelativePath,
                                           fileGuid) + fileName;
            }
        }

        public static string GetFileMimeType(string fileName, List<FileType> lstFileType)
        {
            FileInfo fi = new FileInfo(fileName);
            string filePath = "";

            string filetype = lstFileType.Where(x => x.Extention == fi.Extension.Remove(0, 1)).Select(x => x.Type).FirstOrDefault();

            if(filetype == "video")
            {
                filePath = "~/Content/img/video.png";
            }
            else
            {
                filePath = "~/Content/img/"+ fi.Extension.Remove(0, 1) + ".gif";
            }
            return filePath;
        }

        #endregion
    }
}
