﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GMG.CoZone.InteropWrapper;

namespace GMGColorBusinessLogic.SoftProofingJobs
{
    public class OtherCalibrationTool : ISoftProofingJob
    {

        #region Constructor

        public OtherCalibrationTool()
        {
            Fields = new List<SoftProofingJobField>();
        }

        #endregion

        public bool IsValidated
        {
            get { return true; }
        }

        public List<SoftProofingJobField> Fields { get; private set; }

        public void ProcessJob(string filename, int? ownerUserId = null, int? ownerAccountId = null, int? selectedDisplay = null)
        {
            using (var fs = new FileStream(filename, FileMode.Open))
            {
                ProcessJob(fs, ownerUserId, ownerAccountId, selectedDisplay);
            }
        }

        public void ProcessJob(System.IO.Stream fileStream, int? ownerUserId, int? ownerAccountId = null, int? selectedDisplay = null)
        {
            byte[] profileByteArray;

            if (fileStream is MemoryStream)
            {
                //AWS
                profileByteArray = (fileStream as MemoryStream).ToArray();
            }
            else
            {
                //local
                using (var memoryStream = new MemoryStream())
                {
                    fileStream.CopyTo(memoryStream);
                    profileByteArray = memoryStream.ToArray();
                }
            }

            var profileManager = new ICCProfileManager(profileByteArray);
            CalibratedDate = profileManager.ReadCalibrationTimeTag();
            CalibrationSoftwareSignature = profileManager.GetCreator();
            profileManager.Dispose();

            OwnerUserId = ownerUserId;
            OwnerAccountId = ownerAccountId;
            DisplayIndex = selectedDisplay;
        }

        public int? OwnerUserId
        {
            get;
            private set;
        }

        public int? OwnerAccountId
        {
            get;
            private set;
        }

        public int? DisplayIndex { get; set; }


        public DateTime CalibratedDate { get; set; }


        public string CalibrationSoftwareSignature { get; private set; }
    }
}
