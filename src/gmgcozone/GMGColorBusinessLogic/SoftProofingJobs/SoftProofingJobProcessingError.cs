﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorBusinessLogic.SoftProofingJobs
{
    public class SoftProofingJobProcessingError: Exception
    {
        public SoftProofingJobProcessingError(string message):base(message)
        {
        }
    }
}
