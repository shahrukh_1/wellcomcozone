﻿using System;
using System.Collections.Generic;
using System.IO;

namespace GMGColorBusinessLogic.SoftProofingJobs
{
    public class SoftProofingJobField
    {
        public string Name { get; set; }

        public string CurrentValue { get; set; }

        public string MaxValueAllowed { get; set; }
    }

    public interface ISoftProofingJob
    {
        bool IsValidated { get; }

        DateTime CalibratedDate { get; }

        string CalibrationSoftwareSignature { get; }

        List<SoftProofingJobField> Fields { get;}

        void ProcessJob(string filename, int? ownerUserId = null, int? ownerAccountId = null, int? selectedDisplay = null);

        void ProcessJob(Stream fileStream, int? ownerUserId, int? ownerAccountId = null, int? selectedDisplay = null);

        int? OwnerUserId { get; }

        int? OwnerAccountId { get; }

        int? DisplayIndex { get; set; }
    }
}
