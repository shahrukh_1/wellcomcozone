﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using GMGColor.Resources;

namespace GMGColorBusinessLogic.SoftProofingJobs
{
    class BasicColorDisplay: ISoftProofingJob
    {
        #region Constants

        public const string Filename = "basICColor_display_status.xml";

        private const string DisplayNodeName = "display_{0}";
        private const string De94AverageFieldName = "ΔE 94 Average";
        private const string De94AverageXpath = "dEaverage";

        private const string De94MaxGrayScale = "ΔE 94 Max Greyscale";
        private const string De94MaxGrayScaleXpath = "dEmaxGB";

        private const string De94MaxChromaticColors = "ΔE 94 Max Chromatic Colors";
        private const string De94MaxChromaticColorsXpath = "dEmaxN";

        private const string WhitePointTarget = "White Point Target";
        private const string WhitePointTargetXpath = "Target_Whitepoint";

        private const string WhiteLuminanceTarget = "Luminance Target";
        private const string WhiteLuminanceTargetXpath = "WhiteLuminance";

        private const string JobValidatedSuccesValue = "GREEN";
        private const string JobValidatedSuccessXpath = "Status_Overall";

        #endregion

        public BasicColorDisplay()
        {
            Fields = new List<SoftProofingJobField>();
        }

        public bool IsValidated { get; private set; }

        public List<SoftProofingJobField> Fields { get; private set; }

        public void ProcessJob(string filename, int? ownerUserId = null, int? ownerAccountId = null, int? selectedDisplay = null)
        {
            try
            {
                using (FileStream fs = new FileStream(filename, FileMode.Open))
                {
                    ProcessJob(fs, ownerUserId, ownerAccountId, selectedDisplay);
                }
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotReadBasicColorDisplayJobFile, ex);
            }
        }

        public void ProcessJob(Stream fileStream, int? ownerUserId = null, int? ownerAccountId = null, int? selectedDisplay = null)
        {
            try
            {
                fileStream.Seek(0, SeekOrigin.Begin);

                XDocument doc = XDocument.Load(fileStream);

                XElement displayNode = doc.Descendants(String.Format(DisplayNodeName, selectedDisplay)).SingleOrDefault();

                if (displayNode == null)
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errDisplayNodeNotFound);
                }

                #region Status overall
                XElement validateNod = displayNode.Element(JobValidatedSuccessXpath);
                if (validateNod != null)
                {
                    IsValidated = (validateNod.Value.ToLower() == JobValidatedSuccesValue.ToLower());
                }
                else
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errOverallStatusMissing);
                }
                #endregion

                #region dE94 Average
                XElement de94Average = displayNode.Element(De94AverageXpath);
                if (de94Average != null)
                {
                    Fields.Add(new SoftProofingJobField() { CurrentValue = de94Average.Value, Name = De94AverageFieldName });
                }
                else
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errDe94AverageMissing);
                }
                #endregion

                #region dE94 Max Grayscale

                XElement de94Grayscale = displayNode.Element(De94MaxGrayScaleXpath);
                if (de94Grayscale != null)
                {
                    Fields.Add(new SoftProofingJobField() { CurrentValue = de94Grayscale.Value, Name = De94MaxGrayScale });
                }
                else
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errDe94GrayscaleMissing);
                }

                #endregion

                #region dE94 Max Chromatic Colors

                XElement de94ChromaticColors = displayNode.Element(De94MaxChromaticColorsXpath);
                if (de94ChromaticColors != null)
                {
                    Fields.Add(new SoftProofingJobField() { CurrentValue = de94ChromaticColors.Value, Name = De94MaxChromaticColors });
                }
                else
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errDe94ChromaticColorsMissing);
                }

                #endregion

                #region White Point

                XElement whitePoint = displayNode.Element(WhitePointTargetXpath);
                if (whitePoint != null)
                {
                    Fields.Add(new SoftProofingJobField() { CurrentValue = whitePoint.Value, Name = WhitePointTarget });
                }
                else
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errWhitePointTargetMissing);
                }

                #endregion

                #region White Luminance

                XElement whiteLuminance = displayNode.Element(WhiteLuminanceTargetXpath);
                if (whiteLuminance != null)
                {
                    Fields.Add(new SoftProofingJobField() { CurrentValue = whiteLuminance.Value, Name = WhiteLuminanceTarget });
                }
                else
                {
                    throw new SoftProofingJobProcessingError(GMGColor.Resources.Resources.errWhiteLuminanceTargetMissing);
                }

                #endregion

                OwnerUserId = ownerUserId;
                OwnerAccountId = ownerAccountId;
                DisplayIndex = selectedDisplay;
            }
            catch(SoftProofingJobProcessingError)
            {
                throw;
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotReadBasicColorDisplayJobFile, ex);
            }
        }

        public int? OwnerUserId { get; private set; }
        public int? OwnerAccountId { get; private set; }



        public int? DisplayIndex
        {
            get;set;
        }


        public DateTime CalibratedDate { get; private set; }


        public string CalibrationSoftwareSignature { get; private set; }
        
    }
}
