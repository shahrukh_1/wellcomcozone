﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL;
using System.Transactions;
using GMGColorNotificationService;
using GMG.CoZone.Collaborate.UI;
using GMGColor.Resources;

namespace GMGColorBusinessLogic
{
    public class BaseBL
    {
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static byte[] GetBytes(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static GMGColorDAL.User CreateNewUser(UserInfo model, int loggedUserId, int loggedAccountID, DbContextBL context, BillingPlan collaboratePlan = null) 
        {
            GMGColorDAL.User objUser = null;
            bool success = true;

            try
            {
                GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.AccountID, context);

                using (TransactionScope ts = new TransactionScope())
                {
                    objUser = new GMGColorDAL.User
                    {
                        Username = model.objUser.EmailAddress,
                        EmailAddress = model.objUser.EmailAddress,
                        Account = model.AccountID,
                        Preset = GMGColorDAL.Preset.GetPreset(GMGColorDAL.Preset.PresetType.Low, context).ID,
                        Creator = loggedUserId,
                        CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
                        Modifier = loggedUserId,
                        ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
                        GivenName = model.objUser.IsSsoUser && string.IsNullOrEmpty(model.objUser.FirstName) ? model.objUser.EmailAddress : model.objUser.FirstName,
                        FamilyName = model.objUser.IsSsoUser && string.IsNullOrEmpty(model.objUser.LastName) ? "." : model.objUser.LastName,
                        OfficeTelephoneNumber = string.Empty,
                        NotificationFrequency =
                            GMGColorDAL.NotificationFrequency.GetNotificationFrequency(
                                GMGColorDAL.NotificationFrequency.Frequency.Never, context).ID,
                        Guid = Guid.NewGuid().ToString(),
                        ProofStudioColor = model.ProofStudioColorID,
                        PrinterCompany = model.objUser.PrintCompany,
                        Locale = objAccount.Locale,
                        IsSsoUser = model.objUser.IsSsoUser,
                        PrivateAnnotations = model.objUser.PrivateAnnotations
                    };


                    if (model.objUser.HasPasswordEnabled)
                    {
                        objUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(model.objUser.Password, context);
                        objUser.Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Active, context).ID;
                    }
                    else
                    {
                        objUser.Password = GMGColorDAL.User.GetNewEncryptedRandomPassword(context);
                        if (model.objUser.IsSsoUser)
                        {
                            objUser.Status = (from us in context.UserStatus
                                              where us.Key == "A"
                                              select us.ID).FirstOrDefault();
                        }
                        else
                        {
                            objUser.Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Invited, context).ID;
                        }
                    }

                    //objUser.IsActive = false;
                    //objUser.IsDeleted = false;

                    context.Users.Add(objUser);

                    foreach (var perm in model.ModulePermisions)
                    {
                        GMGColorDAL.UserRole objUserRole = new UserRole();
                        objUserRole.Role = perm.SelectedRole;
                        objUserRole.User1 = objUser;
                        objUser.UserRoles.Add(objUserRole);                       
                    }

                    if (model.UserGroupsInUserList != null)
                    {
                        foreach (var item in model.UserGroupsInUserList.Where(o => o.IsUserGroupInUser == true).ToList())
                        {
                            GMGColorDAL.UserGroup objGoup = DALUtils.GetObject<GMGColorDAL.UserGroup>(item.objUserGroup.ID, context);

                            UserGroupUser ugu = new UserGroupUser { User1 = objUser, UserGroup1 = objGoup };

                            //Set user Primary Group
                            if (model.SelectedPrimaryGroup == objGoup.ID)
                            {
                                ugu.IsPrimary = true;
                            }

                            context.UserGroupUsers.Add(ugu);
                        }
                    }
                    context.SaveChanges();

                    if (collaboratePlan == null)
                    {
                        collaboratePlan = objAccount.CollaborateBillingPlan(context);
                    }

                    //check if the max users limit for the collaborate plan has been reached
                    if (collaboratePlan != null && PlansBL.IsNewCollaborateBillingPlan(collaboratePlan))
                    {
                        if (PlansBL.GetActiveUsersNr(model.AccountID, GMG.CoZone.Common.AppModule.Collaborate, context) > collaboratePlan.MaxUsers)
                        {
                            success = false;
                        }
                    }

                    if (success)
                    {
                        ts.Complete();
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, loggedAccountID, "BaseController.CreateNewUser : error occured while sending invite to user. {0}", ex.Message);
            }
            return objUser;
        }

        public static void SendInviteToUser(int userId, int loggerUserId, int loggedAccountId, DbContextBL context)
        {
            try
            {
                NotificationServiceBL.CreateNotification(new UserWelcome
                {
                    EventCreator = loggerUserId,
                    InternalRecipient = userId,
                    CreatedDate = DateTime.UtcNow
                },
                                                                null,
                                                                context
                                                                );
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, loggedAccountId, "BaseController.SendInvite : Create a user and send invitaion failed. {0}", ex.Message);
            }
        }

        public static FileDownloadViewModel PopulateFileDownloadModel(int loggedAccountId, int loggedUserId, GMG.CoZone.Common.AppModule appModule, DbContextBL context, bool showAllFilesToAdmins = false)
        {
            UploadEngineAccountSetting accountSettings = UploadEngineAccountSetting.GetAccountUploadEngineSettings(loggedAccountId, context);

            List<KeyValuePair<int, string>> presets = new List<KeyValuePair<int, string>>();
            presets.Add(new KeyValuePair<int, string>(0, Resources.lblDownloadDestination));
           
            presets.AddRange(XMLEngineBL.GetAccountPresets(loggedAccountId, appModule, context));
            
            var fileDownload = new FileDownloadViewModel()
            {
                AccountId = loggedAccountId,
                ApplicationModule = appModule,
                AccountPressets = presets,
                ShowAllFilesToAdmins = showAllFilesToAdmins,
                IsZipFileWhenDownloadingEnabled = accountSettings != null ? accountSettings.ZipFilesWhenDownloading : false
            };

            

            var module = (appModule == GMG.CoZone.Common.AppModule.Collaborate)
                            ? UploadEnginePermissionEnum.DownloadFromCollaborate
                            : UploadEnginePermissionEnum.DownloadFromDeliver;

            fileDownload.HasAccessToFtp = UserBL.HasAccessToFTP(loggedUserId, module, context);

            return fileDownload;
        }
    }
}
