﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.Extensions;
using GMGColorNotificationService;
using System.Globalization;
using GMGColorNotificationService.Notifications.Approvals;
using User = GMGColorDAL.User;
using GMG.CoZone.Common;
using Newtonsoft.Json;
using System.Messaging;
using System.Diagnostics;
using System.Xml.Linq;
using System.Reflection;

namespace GMGColorBusinessLogic
{
	public static class NotificationServiceBL
	{
        #region Constants
        public static string ProofStudioShowAnnotationsForExternalUsers = "ProofStudioShowAnnotationsForExternalUsers";
        #endregion
        /// <summary>
        /// Adds a new notification item to database
        /// </summary>
        /// <param name="notificationItem"></param>
        /// <param name="context"></param>
        /// <param name="saveChanges"></param>
        [Obsolete("To not block database context use PutTempNotificationsToTempNotificationQueue")]
        public static void CreateNotification(Notification notificationItem, User loggedUser, DbContextBL context, bool saveChanges = true, int? psSessionId = null,
             string selectedUsers = null)
		{
            List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel> selectedusers = new List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel>();

            if (!(selectedUsers == null))
            {
                 selectedusers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel>>(selectedUsers.ToString());
            }
           
            List<int> internalRecipients = GetInternalRecipients(notificationItem, loggedUser, context, selectedusers);

            string eventKey = NotificationTypeParser.GetKey(notificationItem.NotificationType);
           
			int eventTypeId = (from e in context.EventTypes
							   where e.Key == eventKey
							   select e.ID).FirstOrDefault(); // TODO - cache event types !?

            //Add email for internal users
            if (selectedusers.Count > 0)
            {
                foreach (int recipient in internalRecipients.Distinct())
                {
                    NotificationItem notification = new NotificationItem()
                    {
                       
                        NotificationType = eventTypeId == 1? (int)NotificationType.AtUserInReply + 7: (int)NotificationType.AtUserInComment + 7,
                        CreatedDate = DateTime.UtcNow,
                        Creator = notificationItem.EventCreator,
                        CustomFields = notificationItem.CustomFieldsToXML(),
                        InternalRecipient = recipient,
                        PSSessionID = psSessionId
                    };

                    context.NotificationItems.Add(notification);
                }

                List<int> externalRecipients = GetExternalRecipients(notificationItem, selectedusers, context);
                bool ShowAnnotationsForExternalUsers = false;

                if(loggedUser != null)
                { 
                ShowAnnotationsForExternalUsers = (from accs in context.AccountSettings
                                                       where accs.Account == loggedUser.Account && accs.Name == ProofStudioShowAnnotationsForExternalUsers
                                                       && accs.Value == "True"
                                                       select accs).Any();
                }
                else
                {
                    var user = (from s in selectedusers select s).FirstOrDefault();
                    int account_ID = GetAccountFromSelectedUser(user.Id, user.IsExternal, context);

                    ShowAnnotationsForExternalUsers = (from accs in context.AccountSettings
                                                       where accs.Account == account_ID && accs.Name == ProofStudioShowAnnotationsForExternalUsers
                                                       && accs.Value == "True"
                                                       select accs).Any();
                }

                if (externalRecipients.Count() > 0 && ShowAnnotationsForExternalUsers)
                { 
                    foreach(int recipient in externalRecipients.Distinct())
                    {
                        AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;
                        annotationNotification.ExternalRecipient = recipient;
                        NotificationItem notification = new NotificationItem()
                        {
                            NotificationType = eventTypeId == 1 ? (int)NotificationType.AtUserInReply + 7 : (int)NotificationType.AtUserInComment + 7,
                            CreatedDate = DateTime.UtcNow,
                            Creator = notificationItem.EventCreator,
                            CustomFields = notificationItem.CustomFieldsToXML(),
                            InternalRecipient = null
                        };
                        context.NotificationItems.Add(notification);
                    }
                }
            }
            else
            {
                foreach (int recipient in internalRecipients)
                {
                    NotificationItem notification = new NotificationItem()
                    {
                        NotificationType = eventTypeId,
                        CreatedDate = DateTime.UtcNow,
                        Creator = notificationItem.EventCreator,
                        CustomFields = notificationItem.CustomFieldsToXML(),
                        InternalRecipient = recipient,
                        PSSessionID = psSessionId
                    };

                    context.NotificationItems.Add(notification);
                }
            }

            //Add emails for external users(dont have a CoZone login) or for contact support if any
            if (HasExternalRecipients(notificationItem) )
			{
				NotificationItem notification = new NotificationItem()
				{
					NotificationType = eventTypeId,
					CreatedDate = DateTime.UtcNow,
					Creator = notificationItem.EventCreator,
					CustomFields = notificationItem.CustomFieldsToXML(),
					InternalRecipient = null
				};

				context.NotificationItems.Add(notification);
			}

			if (saveChanges)
				context.SaveChanges();
		}

        public static void CreateNotificationReplayComment(Notification notificationItem, User loggedUser, DbContextBL context, bool saveChanges = true, int? psSessionId = null)
        {
            string eventKey = NotificationTypeParser.GetKey(notificationItem.NotificationType);
            int eventTypeId = (from e in context.EventTypes
                               where e.Key == eventKey
                               select e.ID).FirstOrDefault(); // TODO - cache event types !?

            NotificationItem notification = new NotificationItem()
            {
                NotificationType = eventTypeId,
                CreatedDate = DateTime.UtcNow,
                Creator = notificationItem.EventCreator,
                CustomFields = notificationItem.CustomFieldsToXML(),
                InternalRecipient = null
            };
            context.NotificationItems.Add(notification);
            context.SaveChanges();
        }

        public static void CreateNotificationSubstituteuser(Notification notificationItem, User loggedUser,int SubstituteUserId, DbContextBL context, bool saveChanges = true)
        {
            string eventKey = NotificationTypeParser.GetKey(notificationItem.NotificationType);
            int eventTypeId = (from e in context.EventTypes
                               where e.Key == eventKey
                               select e.ID).FirstOrDefault();

            NotificationItem notification = new NotificationItem()
            {
                NotificationType = eventTypeId,
                CreatedDate = DateTime.UtcNow,
                Creator = loggedUser.ID,
                CustomFields = notificationItem.CustomFieldsToXML(),
                InternalRecipient = SubstituteUserId
            };
            context.NotificationItems.Add(notification);
            context.SaveChanges();
        }

        public static List<NotificationItem> CreateNotificationPerf(Notification notificationItem, User loggedUser, List<int> scheduledUsers, DbContextBL context, int? psSessionId = null)
		{
			List<int> internalRecipients = GetInternalRecipientsPerf(notificationItem, loggedUser, scheduledUsers, context);

			string eventKey = NotificationTypeParser.GetKey(notificationItem.NotificationType);
			int eventTypeId = (from e in context.EventTypes
							   where e.Key == eventKey
							   select e.ID).FirstOrDefault(); // TODO - cache event types !?

            List<NotificationItem> notificationItems = new List<NotificationItem>();

            //Add email for internal users
            foreach (int recipient in internalRecipients)
			{
				NotificationItem notification = new NotificationItem()
				{
					NotificationType = eventTypeId,
					CreatedDate = DateTime.UtcNow,
					Creator = notificationItem.EventCreator,
					CustomFields = notificationItem.CustomFieldsToXML(),
					InternalRecipient = recipient,
					PSSessionID = psSessionId
				};

                notificationItems.Add(notification);
			}

			//Add emails for external users(dont have a CoZone login) or for contact support if any
			if (HasExternalRecipients(notificationItem))
			{
				NotificationItem notification = new NotificationItem()
				{
					NotificationType = eventTypeId,
					CreatedDate = DateTime.UtcNow,
					Creator = notificationItem.EventCreator,
					CustomFields = notificationItem.CustomFieldsToXML(),
					InternalRecipient = null
				};

                notificationItems.Add(notification);
			}

            return notificationItems;


        }

        public static void CreateNotificationFileTransfer(Notification notificationItem, User loggedUser, int? internalRecipient, DbContextBL context, bool saveChanges = true, int? psSessionId = null)
        {
            string eventKey = NotificationTypeParser.GetKey(notificationItem.NotificationType);
            int eventTypeId = (from e in context.EventTypes
                               where e.Key == eventKey
                               select e.ID).FirstOrDefault(); // TODO - cache event types !?

            //Add email for internal users

            if (internalRecipient != null)
            {
                NotificationItem notification = new NotificationItem()
                {
                    NotificationType = eventTypeId,
                    CreatedDate = DateTime.UtcNow,
                    Creator = notificationItem.EventCreator,
                    CustomFields = notificationItem.CustomFieldsToXML(),
                    InternalRecipient = internalRecipient,
                    PSSessionID = psSessionId
                };

                context.NotificationItems.Add(notification);

            }
            //Add emails for external users(dont have a CoZone login) or for contact support if any
            if (HasExternalRecipients(notificationItem))
            {
                NotificationItem notificationext = new NotificationItem()
                {
                    NotificationType = eventTypeId,
                    CreatedDate = DateTime.UtcNow,
                    Creator = notificationItem.EventCreator,
                    CustomFields = notificationItem.CustomFieldsToXML(),
                    InternalRecipient = null
                };

                context.NotificationItems.Add(notificationext);
            }

            if (saveChanges)
                context.SaveChanges();
        }

        public static void CreateNotificationFileTransferDownloaded(Notification notificationItem, User loggedUser, int? internalRecipient, DbContextBL context, bool saveChanges = true, int? psSessionId = null)
        {
            string eventKey = NotificationTypeParser.GetKey(notificationItem.NotificationType);
            int eventTypeId = (from e in context.EventTypes
                               where e.Key == eventKey
                               select e.ID).FirstOrDefault(); // TODO - cache event types !?

            //Add email for internal users

            if (internalRecipient != null)
            {
                NotificationItem notification = new NotificationItem()
                {
                    NotificationType = eventTypeId,
                    CreatedDate = DateTime.UtcNow,
                    Creator = notificationItem.EventCreator,
                    CustomFields = notificationItem.CustomFieldsToXML(),
                    InternalRecipient = internalRecipient,
                    PSSessionID = psSessionId
                };

                context.NotificationItems.Add(notification);

            }
            //Add emails for external users(dont have a CoZone login) or for contact support if any
            if (HasExternalRecipients(notificationItem))
            {
                NotificationItem notificationext = new NotificationItem()
                {
                    NotificationType = eventTypeId,
                    CreatedDate = DateTime.UtcNow,
                    Creator = notificationItem.EventCreator,
                    CustomFields = notificationItem.CustomFieldsToXML(),
                    InternalRecipient = null
                };

                context.NotificationItems.Add(notificationext);
            }

            if (saveChanges)
                context.SaveChanges();
        }

        private static List<int> GetExternalRecipients(Notification notificationItem, List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel> selectedUsers, DbContextBL context)
        {
            List<int> recipients = new List<int>();

                switch (notificationItem.NotificationType)
                {
                    case NotificationType.NewAnnotationAdded:
                    case NotificationType.NewAnnotationAtUserInComment:
                        {
                            AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

                        if (annotationNotification.ApprovalAnnotationId > 0)
                        {
                            foreach (var i in selectedUsers)
                            {
                                if (i.IsExternal == true)
                                {
                                    recipients.Add(i.Id);
                                }
                            }
                        }
                            break;
                        }

                case NotificationType.ACommentIsMade:
                    {
                        AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

                        if (annotationNotification.ApprovalAnnotationId > 0)
                        {
                            foreach (var i in selectedUsers)
                            {
                                if (i.IsExternal == true)
                                {
                                    recipients.Add(i.Id);
                                }
                            }
                        }
                        break;
                    }
            }
                return recipients;
                
            
        }

        private static int GetAccountFromSelectedUser(int UserID , bool IsExternal, DbContextBL context)
        {
            if(IsExternal)
            {
                return (from e in context.ExternalCollaborators
                        where e.ID == UserID
                        select e.Account).FirstOrDefault();
            }
            else
            {
                return (from u in context.Users
                        where u.ID == UserID
                        select u.Account).FirstOrDefault();
            }
            
        }


        /// <summary>
        /// Get recipients list
        /// </summary>
        /// <param name="notificationItem"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private static List<int> GetInternalRecipients(Notification notificationItem, User loggedUser, DbContextBL context,
          List<GMG.CoZone.Proofstudio.DomainModels.ServiceOutput.SelectedUserDetailsModel> selectedUsers)
		{
			List<int> recipients = new List<int>();

            switch (notificationItem.NotificationType)
			{
                case NotificationType.NewAnnotationAdded:
                case NotificationType.NewAnnotationAtUserInComment:
					{
						AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

						if (annotationNotification.ApprovalAnnotationId > 0)
						{
                            
                            if (selectedUsers.Count > 0)
                            {
                                foreach(var i in selectedUsers)
                                {
                                    if(i.IsExternal == false)
                                    {
                                        recipients.Add(i.Id);
                                    }
                                }
                            }
                            else
                            {
                                List<int> users = ApprovalCollaborator.GetCollaboratorsByAnnotationId(annotationNotification.ApprovalAnnotationId, context);
                                foreach (int userId in users)
                                {
                                    if (recipients.All(o => o != userId))
                                    {
                                        recipients.Add(userId);
                                    }
                                }
                            }
						}
						break;
					}
				case NotificationType.ACommentIsMade:
					{
						AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

						if (annotationNotification.ApprovalAnnotationId > 0)
						{
                           
                            if (selectedUsers.Count > 0)
                            {
                                foreach (var i in selectedUsers)
                                {
                                    if (i.IsExternal == false)
                                    {
                                        recipients.Add(i.Id);
                                    }
                                }
                            }
                            else
                            {
                                List<int> users = ApprovalCollaborator.GetCollaboratorsByAnnotationId(annotationNotification.ApprovalAnnotationId, context);
                                int annotationParentCreator =
                                    AnnotationBL.GetParentAnnotationCreator(annotationNotification.ApprovalAnnotationId, context);
                                foreach (int userId in users)
                                {
                                    if (recipients.All(o => o != userId) && userId != annotationParentCreator)
                                    {
                                        recipients.Add(userId);
                                    }
                                }
                            }
						}
						break;
					}
                case NotificationType.ChecklistItemEdited:
                    {
                        AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

                        if (annotationNotification.ApprovalAnnotationId > 0)
                        {
                            List<int> users = ApprovalCollaborator.GetCollaboratorsByAnnotationId(annotationNotification.ApprovalAnnotationId, context);
                                int annotationParentCreator =
                                    AnnotationBL.GetParentAnnotationCreator(annotationNotification.ApprovalAnnotationId, context);
                                foreach (int userId in users)
                                {
                                    if (recipients.All(o => o != userId) && userId != annotationParentCreator)
                                    {
                                        recipients.Add(userId);
                                    }
                                }
                            
                        }
                        break;
                    }
                case NotificationType.ApprovalJobCompleted:
                    {
                        var approval = notificationItem as ApprovalNotification;

                        //Check to send only emails for Owner of  Approval
                        if (approval.ApprovalsIds.Count() > 0)
                        {
                            IEnumerable<int> users = ApprovalCollaborator.GetOwnerByApprovalIdAndEventType(approval.ApprovalsIds[0], context);

                            recipients.AddRange(users);
                        }
                        break;
                    }
                case NotificationType.ApprovalWasDownloaded:
				case NotificationType.ApprovalWasDeleted:
				case NotificationType.ApprovalWasUpdated:
				case NotificationType.ApprovalChangesCompleted:
				case NotificationType.ApprovalWasRejected:
					{
						var approval = notificationItem as ApprovalNotification;
						//Check to send only emails for internal users
						if (approval.ApprovalsIds.Count() > 0)
						{
							IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(notificationItem.NotificationType),
									approval.ApprovalsIds[0], context);

							recipients.AddRange(users);
						}
						break;
					}
				case NotificationType.ApprovalOverdue:
					{
						var notification = notificationItem as ApprovalOverdue;
						//Check to send only emails for internal users

						if (notification != null)
						{
							//check if is phased approval
							if (notification.PhaseId != null)
							{
								IEnumerable<int> users =
									ApprovalCollaborator.GetCollaboratorsByApprovalIdAndPhaseIdOverdueEventType(
										NotificationTypeParser.GetKey(NotificationType.ApprovalOverdue),
										notification.ApprovalsIds[0], notification.PhaseId.Value, context);

								recipients.AddRange(users);
							}
							else if (notification.ApprovalsIds.Count() > 0)
							{
								IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndOverdueEventType(NotificationTypeParser.GetKey(NotificationType.ApprovalOverdue),
									notification.ApprovalsIds[0], context);

								recipients.AddRange(users);
							}
						}

						break;
					}
				case NotificationType.ApprovalStatusChanged:
					{
                        
                        var approval = notificationItem as ApprovalStatusChanged;


                        //Check to send only emails for internal users
                        if (approval.ApprovalsIds.Count() > 0)
						{
                            IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(NotificationType.ApprovalStatusChanged),
									approval.ApprovalsIds[0], context);

                            recipients.AddRange(users);
                         
                        }
                        break;
					}
				case NotificationType.ApprovalStatusChangedByPdm:
					{
						var approval = notificationItem as ApprovalStatusChangedByPDM;
						//Check to send only emails for internal users
						if (approval.ApprovalsIds.Count() > 0)
						{
							IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(NotificationType.ApprovalStatusChangedByPdm),
									approval.ApprovalsIds[0], context);

							recipients.AddRange(users);
						}
						break;
					}


				case NotificationType.AccountWasDeleted:
				case NotificationType.AccountWasAdded:
					{
						IEnumerable<int> users = User.GetUsersListByEventTypeAndAccount(NotificationTypeParser.GetKey(notificationItem.NotificationType),
								loggedUser.Account, context);
						recipients.AddRange(users);
						break;
					}
				//SoftProofing
				case NotificationType.WorkstationRegistered:
					{
						var workstation = notificationItem as WorkstationRegistered;
						IEnumerable<int> admins = AccountBL.GetAdminIdsByAccount(workstation.Account, context);
						recipients.AddRange(admins);
						break;
					}
				//Administration
				case NotificationType.UserWasAdded:
				case NotificationType.GroupWasCreated:
				case NotificationType.PlanWasChanged:
					{
						var administrationNotification = notificationItem as AdministrationNotification;

						IEnumerable<int> users = User.GetUsersListByEventTypeAndAccount(NotificationTypeParser.GetKey(notificationItem.NotificationType),
																					administrationNotification.Account, context);
						recipients.AddRange(users);
						break;
					}
				//Workflow
				case NotificationType.PhaseWasDeactivated:
				case NotificationType.PhaseWasActivated:
				case NotificationType.WorkflowPhaseRerun:
					{
						var workflowNotification = notificationItem as WorkflowNotification;

						IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdPhaseAndEventType(NotificationTypeParser.GetKey(notificationItem.NotificationType), workflowNotification.Phase, workflowNotification.ApprovalId, context);
						recipients.AddRange(users);
						break;
					}			
				//Deliver
				case NotificationType.DeliverFileTimeOut:
				case NotificationType.DeliverFileWasSubmitted:
				case NotificationType.DeliverFileErrored:
				case NotificationType.DeliverFileCompleted:
				//Shared Workflows
				case NotificationType.SharedWorkflowAccessRevoked:
				case NotificationType.InvitedUserDeleted:
				case NotificationType.SharedWorkflowDeleted:
				case NotificationType.ShareRequestAccepted:
				case NotificationType.HostAdminUserFileSubmitted:
				case NotificationType.InvitedUserFileSubmitted:
				case NotificationType.HostAdminUserDeleted:
					{
						//Get account of the recipient to check who else should receive the email
						if (notificationItem.InternalRecipient.GetValueOrDefault() > 0)
						{
							bool accountIsActive;
							int recipientAccount = UserBL.GetAccountIdByUser(notificationItem.InternalRecipient.Value, out accountIsActive, context);

							if (accountIsActive)
							{
								IEnumerable<int> users = User.GetUsersListByEventTypeAndAccount(
										NotificationTypeParser.GetKey(notificationItem.NotificationType),
										recipientAccount, context);

								recipients.AddRange(users);
							}
							else
							{
								return new List<int>();
							}
						}
					}
					break;
				case NotificationType.BillingReport:
				case NotificationType.ApprovalReminder:
				case NotificationType.LostPassword:
				case NotificationType.ResetPassword:
				case NotificationType.SharedWorkflowInvitation:
				case NotificationType.UserWelcome:
				case NotificationType.DemoPlanExpired:
				case NotificationType.PlanLimitReached:
				case NotificationType.PlanLimitWarning:
                case NotificationType.NewFileTransferAdded:
					{
						if (notificationItem.InternalRecipient.GetValueOrDefault() > 0)
						{
							recipients.Add(notificationItem.InternalRecipient.Value);
						}

						//Instant emails shoudn't go through authorizations
						return recipients;
					}                
                    //Project Folder
                case NotificationType.NewProjectFolder:
                    {
                        var NewProjectFolderAdded = notificationItem as NewProjectFolderAdded;

                        IEnumerable<int> users = User.GetProjectFolderUsers(NewProjectFolderAdded.Account, NewProjectFolderAdded.ProjectFolderID, context);
                        recipients.AddRange(users);
                        break;
                    }
                case NotificationType.ProjectFolderWasUpdated:
                    {
                        var projectFolderWasUpdated = notificationItem as ProjectFolderWasUpdated;
                        IEnumerable<int> users = User.GetProjectFolderUsersForUpdate(projectFolderWasUpdated.Account, projectFolderWasUpdated.ProjectFolderID, context);
                        recipients.AddRange(users);
                        break;
                    }
            }
            // return the recipient even if the users are not in schedules has the (specific) event type selected in the notifications area
            if (selectedUsers.Count > 0)
            {
                return recipients;
            }
            // add the primary recipient user if not exists already in the list
            if (notificationItem.InternalRecipient.HasValue && recipients.All(o => o != notificationItem.InternalRecipient))
			{
				recipients.Add(notificationItem.InternalRecipient.Value);
			}

            
            //get notification account
            int accId = loggedUser != null ? loggedUser.Account : notificationItem.InternalRecipient.HasValue ? UserBL.GetAccountId(notificationItem.InternalRecipient.Value, context) :
                notificationItem.EventCreator.HasValue ? UserBL.GetAccountId(notificationItem.EventCreator.Value, context) : 0;
            var notificationType = notificationItem.NotificationType == NotificationType.ChecklistItemEdited ? NotificationType.RepliesToMyComments : notificationItem.NotificationType;
			List<int> scheduledUsers = GlobalNotificationBL.GetScheduleUsersForEventType(accId,
				NotificationTypeParser.GetKey(notificationType), context).ToList();

			//remove current logged user if he does not have "Include my own activity" option activated and is not included in a global schedule
			if (loggedUser != null && scheduledUsers.All(t => t != loggedUser.ID) && !loggedUser.IncludeMyOwnActivity)
			{
				recipients.RemoveAll(o => o == loggedUser.ID);
			}

            // remove internal recipients who dont have in scheduledUsers
            foreach (var recipient in recipients.ToList())
            {
                if (!scheduledUsers.Contains(recipient))
                {
                    recipients.RemoveAll(o => o == recipient);
                }
            }

            // check if internal users that are not in schedules has the (speicific) event type selected in the notifications area
            foreach (var recipient in recipients.Except(scheduledUsers).ToList())
                {
                    if (!EventType.HasRightToEventType(NotificationTypeParser.GetKey(notificationItem.NotificationType), recipient, context))
                    {
                        recipients.RemoveAll(o => o == recipient);
                    }
                }

            if (notificationItem.NotificationType == NotificationType.ChecklistItemEdited)
            {
                AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;
                recipients.RemoveAll(o => o == loggedUser.ID);
                var usersRelatedToAnnotation = (from an in context.ApprovalAnnotations
                                                where (an.ID == annotationNotification.ApprovalAnnotationId ||
                                                an.Parent == annotationNotification.ApprovalAnnotationId) && an.Creator != loggedUser.ID && scheduledUsers.Contains((int)an.Creator) && an.Creator != null
                                                select (int)an.Creator).ToList();
                recipients = usersRelatedToAnnotation;
            }
			//check users with bounce emails and remove them
			var bouncedEmails = (from u in context.Users
								 where u.BouncedEmailID != null && recipients.Contains(u.ID)
								 select u.ID).ToList();
			if (bouncedEmails.Any())
			{
				recipients = recipients.Except(bouncedEmails).ToList();
			}

			return recipients;
		}

        private static List<int> GetInternalRecipientsPerf(Notification notificationItem, User loggedUser, List<int> scheduledUsers, DbContextBL context)
		{
			List<int> recipients = new List<int>();

			switch (notificationItem.NotificationType)
			{
				case NotificationType.NewAnnotationAdded:
					{
						AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

						if (annotationNotification.ApprovalAnnotationId > 0)
						{
							List<int> users = ApprovalCollaborator.GetCollaboratorsByAnnotationId(annotationNotification.ApprovalAnnotationId, context);
							foreach (int userId in users)
							{
								if (recipients.All(o => o != userId))
								{
									recipients.Add(userId);
								}
							}
						}
						break;
					}
				case NotificationType.ACommentIsMade:
					{
						AnnotationNotification annotationNotification = notificationItem as AnnotationNotification;

						if (annotationNotification.ApprovalAnnotationId > 0)
						{
							List<int> users = ApprovalCollaborator.GetCollaboratorsByAnnotationId(annotationNotification.ApprovalAnnotationId, context);
							int annotationParentCreator =
								AnnotationBL.GetParentAnnotationCreator(annotationNotification.ApprovalAnnotationId, context);
							foreach (int userId in users)
							{
								if (recipients.All(o => o != userId) && userId != annotationParentCreator)
								{
									recipients.Add(userId);
								}
							}
						}
						break;
					}
				case NotificationType.ApprovalWasDownloaded:
				case NotificationType.ApprovalWasDeleted:
				case NotificationType.ApprovalWasUpdated:
				case NotificationType.ApprovalChangesCompleted:
				case NotificationType.ApprovalWasRejected:
					{
						var approval = notificationItem as ApprovalNotification;
						//Check to send only emails for internal users
						if (approval.ApprovalsIds.Count() > 0)
						{
							IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(notificationItem.NotificationType),
								approval.ApprovalsIds[0], context);

							recipients.AddRange(users);
						}
						break;
					}
				case NotificationType.ApprovalOverdue:
					{
						var notification = notificationItem as ApprovalOverdue;
						//Check to send only emails for internal users

						if (notification != null)
						{
							//check if is phased approval
							if (notification.PhaseId != null)
							{
								IEnumerable<int> users =
									ApprovalCollaborator.GetCollaboratorsByApprovalIdAndPhaseIdOverdueEventType(
										NotificationTypeParser.GetKey(NotificationType.ApprovalOverdue),
										notification.ApprovalsIds[0], notification.PhaseId.Value, context);

								recipients.AddRange(users);
							}
							else if (notification.ApprovalsIds.Count() > 0)
							{
								IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndOverdueEventType(NotificationTypeParser.GetKey(NotificationType.ApprovalOverdue),
									notification.ApprovalsIds[0], context);

								recipients.AddRange(users);
							}
						}

						break;
					}
				case NotificationType.ApprovalStatusChanged:
					{
						var approval = notificationItem as ApprovalStatusChanged;
						//Check to send only emails for internal users
						if (approval.ApprovalsIds.Count() > 0)
						{
							IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(NotificationType.ApprovalStatusChanged),
								approval.ApprovalsIds[0], context);

							recipients.AddRange(users);
						}
						break;
					}
				case NotificationType.ApprovalStatusChangedByPdm:
					{
						var approval = notificationItem as ApprovalStatusChangedByPDM;
						//Check to send only emails for internal users
						if (approval.ApprovalsIds.Count() > 0)
						{
							IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdAndEventType(NotificationTypeParser.GetKey(NotificationType.ApprovalStatusChangedByPdm),
								approval.ApprovalsIds[0], context);

							recipients.AddRange(users);
						}
						break;
					}


				case NotificationType.AccountWasDeleted:
				case NotificationType.AccountWasAdded:
					{
						IEnumerable<int> users = User.GetUsersListByEventTypeAndAccount(NotificationTypeParser.GetKey(notificationItem.NotificationType),
							loggedUser.Account, context);
						recipients.AddRange(users);
						break;
					}
				//SoftProofing
				case NotificationType.WorkstationRegistered:
					{
						var workstation = notificationItem as WorkstationRegistered;
						IEnumerable<int> admins = AccountBL.GetAdminIdsByAccount(workstation.Account, context);
						recipients.AddRange(admins);
						break;
					}
				//Administration
				case NotificationType.UserWasAdded:
				case NotificationType.GroupWasCreated:
				case NotificationType.PlanWasChanged:
					{
						var administrationNotification = notificationItem as AdministrationNotification;

						IEnumerable<int> users = User.GetUsersListByEventTypeAndAccount(NotificationTypeParser.GetKey(notificationItem.NotificationType),
							administrationNotification.Account, context);
						recipients.AddRange(users);
						break;
					}
				//Workflow
				case NotificationType.PhaseWasDeactivated:
				case NotificationType.PhaseWasActivated:
				case NotificationType.WorkflowPhaseRerun:
					{
						var workflowNotification = notificationItem as WorkflowNotification;

						IEnumerable<int> users = ApprovalCollaborator.GetCollaboratorsByApprovalIdPhaseAndEventType(NotificationTypeParser.GetKey(notificationItem.NotificationType), workflowNotification.Phase, workflowNotification.ApprovalId, context);
						recipients.AddRange(users);
						break;
					}				
				//Deliver
				case NotificationType.DeliverFileTimeOut:
				case NotificationType.DeliverFileWasSubmitted:
				case NotificationType.DeliverFileErrored:
				case NotificationType.DeliverFileCompleted:
				//Shared Workflows
				case NotificationType.SharedWorkflowAccessRevoked:
				case NotificationType.InvitedUserDeleted:
				case NotificationType.SharedWorkflowDeleted:
				case NotificationType.ShareRequestAccepted:
				case NotificationType.HostAdminUserFileSubmitted:
				case NotificationType.InvitedUserFileSubmitted:
				case NotificationType.HostAdminUserDeleted:
					{
						//Get account of the recipient to check who else should receive the email
						if (notificationItem.InternalRecipient.GetValueOrDefault() > 0)
						{
							bool accountIsActive;
							int recipientAccount = UserBL.GetAccountIdByUser(notificationItem.InternalRecipient.Value, out accountIsActive, context);

							if (accountIsActive)
							{
								IEnumerable<int> users = User.GetUsersListByEventTypeAndAccount(
									NotificationTypeParser.GetKey(notificationItem.NotificationType),
									recipientAccount, context);

								recipients.AddRange(users);
							}
							else
							{
								return new List<int>();
							}
						}
					}
					break;
				case NotificationType.BillingReport:
				case NotificationType.ApprovalReminder:
				case NotificationType.LostPassword:
				case NotificationType.ResetPassword:
				case NotificationType.SharedWorkflowInvitation:
				case NotificationType.UserWelcome:
				case NotificationType.DemoPlanExpired:
				case NotificationType.PlanLimitReached:
				case NotificationType.PlanLimitWarning:
                case NotificationType.NewFileTransferAdded:
					{
						if (notificationItem.InternalRecipient.GetValueOrDefault() > 0)
						{
							recipients.Add(notificationItem.InternalRecipient.Value);
						}

						//Instant emails shoudn't go through authorizations
						return recipients;
					}				
			}

			// add the primary recipient user if not exists already in the list
			if (notificationItem.InternalRecipient.HasValue && recipients.All(o => o != notificationItem.InternalRecipient))
			{
				recipients.Add(notificationItem.InternalRecipient.Value);
			}

			//remove current logged user if he does not have "Include my own activity" option activated and is not included in a global schedule
			if (loggedUser != null && scheduledUsers.All(t => t != loggedUser.ID) && !loggedUser.IncludeMyOwnActivity)
			{
				recipients.RemoveAll(o => o == loggedUser.ID);
			}

			// check if internal users that are not in schedules has the (speicific) event type selected in the notifications area
			foreach (var recipient in recipients.Except(scheduledUsers).ToList())
			{
				if (!EventType.HasRightToEventType(NotificationTypeParser.GetKey(notificationItem.NotificationType), recipient, context))
				{
					recipients.RemoveAll(o => o == recipient);
				}
			}

			//check users with bounce emails and remove them
			var bouncedEmails = (from u in context.Users
								 where u.BouncedEmailID != null && recipients.Contains(u.ID)
								 select u.ID).ToList();
			if (bouncedEmails.Any())
			{
				recipients = recipients.Except(bouncedEmails).ToList();
			}

			return recipients;
		}

        /// <summary>
        /// Creates a Notification item for the given database notification item
        /// </summary>
        /// <param name="notificationItem"></param>
        /// <param name="notificationType"></param>
        /// <returns></returns>
        public static Notification CreateNotification(NotificationItem notificationItem, NotificationType notificationType)
		{

			Notification notification = null;

			switch (notificationType)
			{
                case NotificationType.ChecklistItemEdited:
                    notification = new NewChecklistItemComment();
                    break;
                case NotificationType.OutOfOffice:
                    notification = new GMGColorNotificationService.OutOfOffice();
                    break;
				case NotificationType.NewAnnotationAdded:
					notification = new NewAnnotation();
					break;
				case NotificationType.ACommentIsMade:
					notification = new NewAnnotationComment();
					break;
                case NotificationType.AtUserInComment:
                    //notification = new NewAnnotation();
                    notification = new NewAnnotationAtUserInComment();
                    break;
                case NotificationType.AtUserInReply:
                    notification = new NewAnnotationComment();
                    break;
                case NotificationType.RepliesToMyComments:
					notification = new NewReplyToMyComment();
					break;
				case NotificationType.WorkstationRegistered:
					notification = new WorkstationRegistered();
					break;
				case NotificationType.ApprovalWasDownloaded:
					notification = new ApprovalWasDownloaded();
					break;
				case NotificationType.ApprovalWasDeleted:
					notification = new ApprovalWasDeleted();
					break;
				case NotificationType.ApprovalStatusChanged:
					notification = new ApprovalStatusChanged();
					break;
				case NotificationType.ApprovalStatusChangedByPdm:
					notification = new ApprovalStatusChangedByPDM();
					break;
				case NotificationType.ApprovalWasShared:
					notification = new ApprovalWasShared();
					break;
				case NotificationType.ApprovalWasUpdated:
					notification = new ApprovalWasUpdated();
					break;
				case NotificationType.NewApprovalAdded:
					notification = new NewApprovalAdded();
					break;
				case NotificationType.NewVersionWasCreated:
					notification = new NewVersionWasCreated();
					break;
				case NotificationType.GroupWasCreated:
					notification = new GroupWasCreated();
					break;
				case NotificationType.DeliverFileCompleted:
					notification = new DeliverJobCompleted();
					break;
				case NotificationType.DeliverFileErrored:
					notification = new DeliverJobError();
					break;
				case NotificationType.DeliverFileTimeOut:
					notification = new DeliverJobTimeout();
					break;
				case NotificationType.DeliverFileWasSubmitted:
					notification = new NewDeliverJob();
					break;
				case NotificationType.UserWasAdded:
					notification = new UserWasAdded();
					break;
				case NotificationType.PlanWasChanged:
					notification = new PlanWasChanged();
					break;				
				case NotificationType.AccountWasAdded:
					notification = new AccountWasAdded();
					break;
				case NotificationType.AccountWasDeleted:
					notification = new AccountWasDeleted();
					break;
				case NotificationType.UserWelcome:
					notification = new UserWelcome();
					break;
				case NotificationType.LostPassword:
					notification = new LostPassword();
					break;
				case NotificationType.ResetPassword:
					notification = new ResetPassword();
					break;
				case NotificationType.BillingReport:
					notification = new BillingReport();
					break;
				case NotificationType.SharedWorkflowInvitation:
					notification = new ShareWorkflowInvitation();
					break;
				case NotificationType.DeliverNewPairingCode:
					notification = new NewDeliverPairingCode();
					break;
				case NotificationType.ApprovalReminder:
					notification = new ApprovalReminder();
                    break;
				case NotificationType.HostAdminUserDeleted:
					notification = new HostAdminUserDeleted();
					break;
				case NotificationType.InvitedUserDeleted:
					notification = new InvitedUserDeleted();
					break;
				case NotificationType.HostAdminUserFileSubmitted:
					notification = new HostAdminUserFileSubmitted();
					break;
				case NotificationType.InvitedUserFileSubmitted:
					notification = new InvitedUserFileSubmitted();
					break;
				case NotificationType.SharedWorkflowDeleted:
					notification = new SharedWorkflowDeleted();
					break;
				case NotificationType.SharedWorkflowAccessRevoked:
					notification = new SharedWorkflowAccessRevoked();
					break;
				case NotificationType.ShareRequestAccepted:
					notification = new ShareRequestAccepted();
					break;
				case NotificationType.AccountSupport:
					notification = new GMGColorNotificationService.AccountSupport();
					break;
				case NotificationType.DemoPlanExpired:
					notification = new DemoPlanExpired();
					break;
				case NotificationType.ApprovalOverdue:
					notification = new ApprovalOverdue();
					break;				
				case NotificationType.PlanLimitReached:
					notification = new PlanLimitReached();
					break;
				case NotificationType.PlanLimitWarning:
					notification = new PlanLimitWarning();
					break;
				case NotificationType.ApprovalChangesCompleted:
					notification = new ApprovalChangesCompleted();
					break;
                case NotificationType.ApprovalJobCompleted:
                    notification = new ApprovalJobCompleted();
                    break;
                case NotificationType.ApprovalWasRejected:
					notification = new ApprovalWasRejected();
					break;
				case NotificationType.PhaseWasDeactivated:
					notification = new PhaseWasDeactivated();
					break;
				case NotificationType.PhaseWasActivated:
					notification = new PhaseWasActivated();
					break;
				case NotificationType.WorkflowPhaseRerun:
					notification = new WorkflowPhaseRerun();
					break;
                case NotificationType.NewFileTransferAdded:
                    notification = new NewFileTransfer();
                    break;
                case NotificationType.FileTransferDownloaded:
                    notification = new FileTransferDownloaded();
                    break;
                case NotificationType.NewProjectFolder:
                    notification = new NewProjectFolderAdded();
                    break;
                case NotificationType.ProjectFolderWasUpdated:
                    notification = new ProjectFolderWasUpdated();
                    break;
                case NotificationType.ProjectFolderWasShared:
                    notification = new ProjectFolderWasShared();
                    break;

            }

			// Load custom fields
			notification.Init(notificationItem.InternalRecipient, notificationItem.CreatedDate, notificationItem.Creator, notificationItem.CustomFields);
            if (notificationType == NotificationType.ApprovalReminder)
            {
                int pFrom = notificationItem.CustomFields.IndexOf("<ApprovalId>") + "<ApprovalId>".Length;
                int pTo = notificationItem.CustomFields.LastIndexOf("</ApprovalId>");
                if (pFrom > 0 && pTo > 0)
                {
                    notification.ApprovalID = int.Parse(notificationItem.CustomFields.Substring(pFrom, pTo - pFrom));
                }

                if (notificationItem.InternalRecipient == null)
                {
                    int eFrom = notificationItem.CustomFields.IndexOf("<ExternalRecipient>") + "<ExternalRecipient>".Length;
                    int eTo = notificationItem.CustomFields.LastIndexOf("</ExternalRecipient>");
                    if (eFrom > 0 && eTo > 0)
                    {
                        notification.ExternalRecipient = int.Parse(notificationItem.CustomFields.Substring(eFrom, eTo - eFrom));
                    }
                }
            }
            return notification;
		}

		public static bool HasPersonalNotifications(int userId, DbContextBL context)
		{
			List<int> eventGrpIds = NotificationsModel.GetGroups(userId, context);
			return RolePresetEventType.HasNotifications(userId, eventGrpIds, context);
		}

		#region Create Emails

		/// <summary>
		/// Gets the notifications that sould be sent instant
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<UserNotifications> GetInstantNotificationsToPprocess(DbContextBL context)
		{
            
            int[] arr = { 1, 3 };
			// Get all database notifications (exclude events from global notification schedules for internal users and ProofStudio session) 
			List<UserNotifications> notifications =
				(from n in context.NotificationItems
				 join evt in context.EventTypes on n.NotificationType equals evt.ID
				 join egr in context.EventGroups on evt.EventGroup equals egr.ID
				 let internalUserInstant = (from u in context.Users
											join nf in context.NotificationFrequencies on u.NotificationFrequency equals nf.ID
											join us in context.UserStatus on u.Status equals us.ID
                                            where n.InternalRecipient == u.ID && u.BouncedEmailID == null && !n.IsDeleted
                                            select new
											{
												NotificationFrequencyKey = nf.Key,
												UserStatusKey = us.Key
											}).FirstOrDefault()
				 where (n.InternalRecipient == null || internalUserInstant != null) && n.PSSessionID == null
				 select new UserNotifications
				 {
					 InternalUserRecipient = (internalUserInstant == null || n.InternalRecipient == null ? null : new InternalUserRecipient()
					 {
						 UserId = n.InternalRecipient.Value,
						 NotificationFrequencyKey = internalUserInstant.NotificationFrequencyKey,
						 UserStatusKey = internalUserInstant.UserStatusKey
					 }),
					 Notifications = new List<UserNotification>()
											{
												new UserNotification()
												{
													InternalUserCreator = (n.Creator == null ? null : new InternalUserCreator()
													{
														UserId = n.Creator.Value
													}),
													NotificationItem = n,
													EventGroupKey = egr.Key
												}
											}
				 }).ToList();

			// group notifications per user
			var userNotifications = new List<UserNotifications>();
			foreach (var item in notifications)
			{
				if (item.InternalUserRecipient == null)
				{
					userNotifications.Add(item);
				}
				else
				{
					var user = userNotifications.FirstOrDefault(u =>
									u.InternalUserRecipient != null && u.InternalUserRecipient.UserId == item.InternalUserRecipient.UserId);
					if (user != null)
					{
						user.Notifications.AddRange(item.Notifications);
					}
					else
					{
						userNotifications.Add(item);
					}
				}
			}

			//filter internal users notification based on global notifications settings
			foreach (var userNotif in userNotifications.Where(t => t.InternalUserRecipient != null))
			{
				bool isPersonalNotificationsDisabled;
				//get scheduled notification for current user from account global notifications
				List<UserAccountNotificationSetting> userScheduledNotifications = GlobalNotificationBL.GetUserAccountNotificationSettings(userNotif.InternalUserRecipient.UserId, context, out isPersonalNotificationsDisabled);

				for (int i = userNotif.Notifications.Count - 1; i >= 0; i--)
				{
					UserNotification currentNotification = userNotif.Notifications[i];
					//instant emails should always be sent immediatly
					if (currentNotification.EventGroupKey == "IE")
					{
						continue;
					}

                    int currentNotifEvType = currentNotification.NotificationItem.EventType.ID;

                    //get account user notification for current event type
                    UserAccountNotificationSetting currentEvTypeUserNotif = userScheduledNotifications.FirstOrDefault(t => t.EventType == currentNotifEvType);
					if (isPersonalNotificationsDisabled)
					{
						//set frecvency for current notification
						if (currentEvTypeUserNotif != null && currentEvTypeUserNotif.NotificationFrecvencyKey == (int)GMG.CoZone.Common.NotificationPresetFrequency.AsEventOccur)
						{
							currentNotification.IsFromGlobalSchedule = true;
						}
						else
						{
							userNotif.Notifications.Remove(currentNotification);
						}
					}
					else
					{
						//check if for this current event type notification there is any schedule that has as event occur frequency, if there is not this event type should be handled later
						//by the global schedule notification task (also remove it if there is no schedule for the event and user has daily or weekly)
						if ((currentEvTypeUserNotif != null &&
							 currentEvTypeUserNotif.NotificationFrecvencyKey != (int)GMG.CoZone.Common.NotificationPresetFrequency.AsEventOccur) || (currentEvTypeUserNotif == null &&
							 (userNotif.InternalUserRecipient.NotificationFrequencyKey == "DLY" ||
							  userNotif.InternalUserRecipient.NotificationFrequencyKey == "WLY")))
						{
							userNotif.Notifications.Remove(currentNotification);
						}
						else if (currentEvTypeUserNotif != null)
						{
							//mark this notification that comes from schedule and not from personal settings
							currentNotification.IsFromGlobalSchedule = true;
						}
					}
				}
			}

			//remove empty notifications
			userNotifications.RemoveAll(t => t.Notifications.Count == 0);

			return userNotifications;
		}

		public static List<UserNotifications> GetDailyWeeklyNotificationsToProcess(DbContextBL context)
		{
			// Get all database notifications for internal recipients
			List<UserNotifications> notifications =
				(from n in context.NotificationItems
				 join iu in context.Users on n.InternalRecipient equals iu.ID
				 join ac in context.Accounts on iu.Account equals ac.ID
				 join acs in context.AccountStatus on ac.Status equals acs.ID
				 join nf in context.NotificationFrequencies on iu.NotificationFrequency equals nf.ID
				 join evt in context.EventTypes on n.NotificationType equals evt.ID
				 join egr in context.EventGroups on evt.EventGroup equals egr.ID
				 join us in context.UserStatus on iu.Status equals us.ID
				 where egr.Key != "IE" && iu.BouncedEmailID == null && !n.IsDeleted
				 select new UserNotifications
				 {
					 InternalUserRecipient = new InternalUserRecipient
					 {
						 UserId = iu.ID,
						 NotificationFrequencyKey = nf.Key,
						 UserStatusKey = us.Key,
						 AccountStatusKey = acs.Key
					 },
					 Notifications = new List<UserNotification>
												{
													new UserNotification()
													{
														NotificationItem = n,
														EventGroupKey = egr.Key
													}
												}
				 }).ToList();

			// group notifications per user
			List<UserNotifications> userNotifications = new List<UserNotifications>();
			foreach (var item in notifications)
			{
				var user = userNotifications.FirstOrDefault(u => u.InternalUserRecipient.UserId == item.InternalUserRecipient.UserId);
				if (user != null)
				{
					user.Notifications.AddRange(item.Notifications);
				}
				else
				{
					userNotifications.Add(item);
				}
			}

			//filter internal users notification based on global notifications settings
			foreach (var userNotif in userNotifications)
			{
				bool isPersonalNotificationsDisabled;
				//get scheduled notification for current user from account global notifications
				List<UserAccountNotificationSetting> userScheduledNotifications = GlobalNotificationBL.GetUserAccountNotificationSettings(userNotif.InternalUserRecipient.UserId, context, out isPersonalNotificationsDisabled);


				for (int i = userNotif.Notifications.Count - 1; i >= 0; i--)
				{
					UserNotification currentNotification = userNotif.Notifications[i];

					int currentNotifEvType = currentNotification.NotificationItem.EventType.ID;

					UserAccountNotificationSetting currentEvTypeUserNotif = userScheduledNotifications.FirstOrDefault(t => t.EventType == currentNotifEvType);

					//if personal notifications are disabled that send only global notifications if any
					if (isPersonalNotificationsDisabled)
					{
						//check whether there are any global notifications selected for daily/weekly emails
						if (currentEvTypeUserNotif != null && (currentEvTypeUserNotif.NotificationFrecvencyKey == (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryDay ||
							currentEvTypeUserNotif.NotificationFrecvencyKey == (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryWeek))
						{
							currentNotification.NotificationPresetFrecvencyKey = currentEvTypeUserNotif.NotificationFrecvencyKey;
						}
						else
						{
							userNotif.Notifications.Remove(currentNotification);
						}
					}
					else
					{
						//check if for this current event type notification there is any schedule that has a frecvency different from daily and weekly, and if so remove it
						//by the global schedule notification task (also remove it if there is no schedule for the event and user hasn't daily or weekly set in preferences)
						if ((currentEvTypeUserNotif != null &&
							 currentEvTypeUserNotif.NotificationFrecvencyKey != (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryDay &&
							 currentEvTypeUserNotif.NotificationFrecvencyKey != (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryWeek) ||
							(currentEvTypeUserNotif == null && userNotif.InternalUserRecipient.NotificationFrequencyKey != "DLY" && userNotif.InternalUserRecipient.NotificationFrequencyKey != "WLY"))
						{
							userNotif.Notifications.Remove(currentNotification);
						}
						else
						{
							//set each notification frequency
							if (currentEvTypeUserNotif != null)
							{
								currentNotification.NotificationPresetFrecvencyKey = currentEvTypeUserNotif.NotificationFrecvencyKey;
							}
							else
							{
								if (userNotif.InternalUserRecipient.NotificationFrequencyKey == "DLY")
								{
									currentNotification.NotificationPresetFrecvencyKey =
										(int)GMG.CoZone.Common.NotificationPresetFrequency.EveryDay;

								}
								else
								{
									currentNotification.NotificationPresetFrecvencyKey =
										(int)GMG.CoZone.Common.NotificationPresetFrequency.EveryWeek;
								}
							}
						}
					}
				}
			}

			//remove empty notifications
			userNotifications.RemoveAll(t => t.Notifications.Count == 0);

			return userNotifications;
		}

		/// <summary>
		/// Get List of Notifications for Global Notifications Schedules different than AEO
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<UserNotifications> GetScheduledNotificationsToProcess(DbContextBL context)
		{
			// Get all database notifications for internal recipients
			List<UserNotifications> notifications =
				(from n in context.NotificationItems
				 join iu in context.Users on n.InternalRecipient equals iu.ID
				 join ac in context.Accounts on iu.Account equals ac.ID
				 join acs in context.AccountStatus on ac.Status equals acs.ID
				 join nf in context.NotificationFrequencies on iu.NotificationFrequency equals nf.ID
				 join evt in context.EventTypes on n.NotificationType equals evt.ID
				 join egr in context.EventGroups on evt.EventGroup equals egr.ID
				 join us in context.UserStatus on iu.Status equals us.ID
				 where egr.Key != "IE" && iu.BouncedEmailID == null && !n.IsDeleted
				 select new UserNotifications
				 {
					 InternalUserRecipient = new InternalUserRecipient
					 {
						 UserId = iu.ID,
						 NotificationFrequencyKey = nf.Key,
						 UserStatusKey = us.Key,
						 AccountStatusKey = acs.Key
					 },
					 Notifications = new List<UserNotification>
												{
													new UserNotification()
													{
														NotificationItem = n,
														EventGroupKey = egr.Key
													}
												}
				 }).ToList();

			// group notifications per user
			List<UserNotifications> userNotifications = new List<UserNotifications>();
			foreach (var item in notifications)
			{
				var user = userNotifications.FirstOrDefault(u => u.InternalUserRecipient.UserId == item.InternalUserRecipient.UserId);
				if (user != null)
				{
					user.Notifications.AddRange(item.Notifications);
				}
				else
				{
					userNotifications.Add(item);
				}
			}

			//filter internal users notification based on global notifications settings
			foreach (var userNotif in userNotifications)
			{
				bool isPersonalNotificationsDisabled;
				//get scheduled notification for current user from account global notifications
				List<UserAccountNotificationSetting> userScheduledNotifications = GlobalNotificationBL.GetUserAccountNotificationSettings(userNotif.InternalUserRecipient.UserId, context, out isPersonalNotificationsDisabled);

				var isCollaborateModuleActive = GlobalNotificationBL.HasSubscriptionPlan(GMG.CoZone.Common.AppModule.Collaborate, userNotif.InternalUserRecipient.UserId, context);				
				var isDeliverModuleActive = GlobalNotificationBL.HasSubscriptionPlan(GMG.CoZone.Common.AppModule.Deliver, userNotif.InternalUserRecipient.UserId, context);

				for (int i = userNotif.Notifications.Count - 1; i >= 0; i--)
				{
					UserNotification currentNotification = userNotif.Notifications[i];

					//remove notification if notification group is from an expired module
					if ((!isCollaborateModuleActive && (currentNotification.EventGroupKey == GMGColorDAL.EventGroup.GetKey(GMGColorDAL.EventGroup.GroupName.Approvals) || currentNotification.EventGroupKey == GMGColorDAL.EventGroup.GetKey(GMGColorDAL.EventGroup.GroupName.Comments))) ||
					   (!isDeliverModuleActive && currentNotification.EventGroupKey == GMGColorDAL.EventGroup.GetKey(GMGColorDAL.EventGroup.GroupName.Deliver)))
					{
						userNotif.Notifications.Remove(currentNotification);
						continue;
					}

					int currentNotifEvType = currentNotification.NotificationItem.EventType.ID;

					UserAccountNotificationSetting currentEvTypeUserNotif = userScheduledNotifications.FirstOrDefault(t => t.EventType == currentNotifEvType);

					//if personal notifications are disabled then send only global notifications if any
					if (isPersonalNotificationsDisabled)
					{
						//check whether there are any global notifications selected for the lowest frecvency diffrent than AEO, DLY or WLY
						if (currentEvTypeUserNotif != null && currentEvTypeUserNotif.NotificationFrecvencyKey != (int)GMG.CoZone.Common.NotificationPresetFrequency.AsEventOccur &&
							currentEvTypeUserNotif.NotificationFrecvencyKey != (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryWeek &&
							currentEvTypeUserNotif.NotificationFrecvencyKey != (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryDay)
						{
							currentNotification.NotificationPresetFrecvencyKey =
								currentEvTypeUserNotif.NotificationFrecvencyKey;
						}
						else
						{
							userNotif.Notifications.Remove(currentNotification);
						}
					}
					else
					{
						//if there is no schedule for this event or if the lowest frecvency is AEO, DLY or WLY remove it from scheduled items
						if (currentEvTypeUserNotif == null || currentEvTypeUserNotif.NotificationFrecvencyKey == (int)GMG.CoZone.Common.NotificationPresetFrequency.AsEventOccur ||
							currentEvTypeUserNotif.NotificationFrecvencyKey == (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryWeek ||
							currentEvTypeUserNotif.NotificationFrecvencyKey == (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryDay)
						{
							userNotif.Notifications.Remove(currentNotification);
						}
						else
						{
							//set frecvency for current notification
							currentNotification.NotificationPresetFrecvencyKey =
								currentEvTypeUserNotif.NotificationFrecvencyKey;
						}
					}
				}
			}

			//remove empty notifications
			userNotifications.RemoveAll(t => t.Notifications.Count == 0);

			return userNotifications;
		}

		/// <summary>
		/// Check if the scheduled email should be sent now or not
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="frecvencyKey"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static bool IsScheduledNotificationTimeToSend(int userId, int frecvencyKey, DbContextBL context)
		{
			//returns a list of event type ids for the given frequency, except those who have a lower frequency than the current one
			var eventTypeIds = (from ns in context.NotificationSchedules
								join anp in context.AccountNotificationPresets on ns.NotificationPreset equals anp.ID
								join anpet in context.AccountNotificationPresetEventTypes on anp.ID equals anpet.NotificationPreset
								join npf in context.NotificationPresetFrequencies on ns.NotificatonFrequency equals npf.ID
								join acc in context.Accounts on anp.Account equals acc.ID
								join u in context.Users on acc.ID equals u.Account

								let eventsFrequencies = (from npfd in context.NotificationPresetFrequencies
														 join nsd in context.NotificationSchedules on npfd.ID equals nsd.NotificatonFrequency
														 join anpd in context.AccountNotificationPresets on nsd.NotificationPreset equals anpd.ID
														 join anpetd in context.AccountNotificationPresetEventTypes on anpd.ID equals anpetd.NotificationPreset
														 join accd in context.Accounts on anpd.Account equals acc.ID
														 join us in context.Users on accd.ID equals us.Account
														 where us.ID == userId
														 group new { anpetd.EventType, NotificatonFrequency = npfd.Key } by new { anpetd.EventType }
															 into g
														 select new
														 {
															 g.Key.EventType,
															 Frequency = g.Min(f => f.NotificatonFrequency)
														 }).ToList()
								where u.ID == userId && npf.Key == frecvencyKey && eventsFrequencies.Any(g => g.EventType == anpet.EventType && g.Frequency >= frecvencyKey)
								select anpet.EventType).Distinct().ToList();

			//retrieve the max last sent date for current frequency
			DateTime lastSentEmailDate = (from uetlog in context.NotificationScheduleUserEventTypeEmailLogs
										  where uetlog.User == userId && eventTypeIds.Contains(uetlog.EventType)
										  group uetlog by uetlog.User into grp
										  let maxDate = grp.Max(g => g.LastSentDate)
										  select grp.FirstOrDefault(g => g.LastSentDate == maxDate).LastSentDate).FirstOrDefault();


			//if not entry found send email
			if (lastSentEmailDate == default(DateTime))
			{
				return true;
			}

			//get time difference from the last sending date
			double intervalInHours = (DateTime.UtcNow - lastSentEmailDate).TotalHours;

			//take an error interval of 0.1 hours (6 minutes) to cover the time that it takes for the thread to run
			switch (frecvencyKey)
			{
				case (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryHour:
					if (intervalInHours >= 0.9)
					{
						return true;
					}
					break;
				case (int)GMG.CoZone.Common.NotificationPresetFrequency.Every2Hours:
					if (intervalInHours >= 1.9)
					{
						return true;
					}
					break;
				case (int)GMG.CoZone.Common.NotificationPresetFrequency.Every4Hours:
					if (intervalInHours >= 3.9)
					{
						return true;
					}
					break;
			}

			return false;
		}

		/// <summary>
		/// Update Log table after succesfuly sending an scheduled email for an user
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="eventTypeId"></param>
		/// <param name="context"></param>
		public static void MarkUserScheduleEventAsSent(int userId, int eventTypeId, DbContextBL context)
		{
			NotificationScheduleUserEventTypeEmailLog userEventLog = (from uetlog in context.NotificationScheduleUserEventTypeEmailLogs
																	  where uetlog.User == userId && uetlog.EventType == eventTypeId
																	  select uetlog).FirstOrDefault();

			//if no log found
			if (userEventLog == null)
			{
				userEventLog = new NotificationScheduleUserEventTypeEmailLog();
				userEventLog.User = userId;
				userEventLog.EventType = eventTypeId;

				context.NotificationScheduleUserEventTypeEmailLogs.Add(userEventLog);
			}

			userEventLog.LastSentDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Hour, 0, 0);
		}

		/// <summary>
		/// Returns a list of Approval Reminders for Global Notifications Schedules different than AEO
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<UserCollaborateReminders> GetCollaborateRemindersNotifications(DbContextBL context)
		{
			var userCollaborateReminders = (from u in context.Users
											join us in context.UserStatus on u.Status equals us.ID
											join acc in context.Accounts on u.Account equals acc.ID
											join acs in context.AccountStatus on acc.Status equals acs.ID
											join ac in context.ApprovalCollaborators on u.ID equals ac.Collaborator
											join a in context.Approvals on ac.Approval equals a.ID
											join j in context.Jobs on a.Job equals j.ID
											join js in context.JobStatus on j.Status equals js.ID
											join acr in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole
												equals acr.ID
											join acd in context.ApprovalCollaboratorDecisions on u.ID equals
												acd.Collaborator
											let schedulesUser = (from nsu in context.NotificationScheduleUsers
																 join ns in context.NotificationSchedules on nsu.NotificationSchedule equals ns.ID
																 join npu in context.NotificationPresetCollaborateUnits on ns.NotificationCollaborateUnit equals npu.ID
																 join npf in context.NotificationPresetFrequencies on ns.NotificatonFrequency equals npf.ID
																 join npt in context.NotificationPresetCollaborateTriggers on ns.NotificationCollaborateTrigger equals npt.ID
																 where
																	 nsu.User == u.ID &&
																	 ns.NotificationCollaborateUnit != null
																 select new UserNotificationSchedule
																 {
																	 Frequency = npf.Key,
																	 Unit = npu.Key,
																	 Trigger = npt.Key,
																	 UnitNumber =
																			 ns.NotificationCollaborateUnitNumber
																 }).ToList()
											let scheduleUserGroup = (from ugu in context.UserGroupUsers
																	 join nsug in context.NotificationScheduleUserGroups on ugu.UserGroup equals nsug.UserGroup
																	 join ns in context.NotificationSchedules on nsug.NotificationSchedule equals ns.ID
																	 join npu in context.NotificationPresetCollaborateUnits on ns.NotificationCollaborateUnit equals npu.ID
																	 join npf in context.NotificationPresetFrequencies on ns.NotificatonFrequency equals npf.ID
																	 join npt in context.NotificationPresetCollaborateTriggers on ns.NotificationCollaborateTrigger equals npt.ID
																	 where
																		 ugu.User == u.ID &&
																		 ns.NotificationCollaborateUnit != null
																	 select new UserNotificationSchedule
																	 {
																		 Frequency = npf.Key,
																		 Unit = npu.Key,
																		 Trigger = npt.Key,
																		 UnitNumber =
																				 ns.NotificationCollaborateUnitNumber
																	 }).ToList()
											let hasCollaborateModuleActive = (from asp in context.AccountSubscriptionPlans
																			  join account in context.Accounts on asp.Account equals account.ID
																			  join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
																			  join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
																			  join am in context.AppModules on bpt.AppModule equals am.ID
																			  where
																			  am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && account.ID == acc.ID &&
																			  (!bp.IsDemo || (bp.IsDemo && DbFunctions.DiffDays(DbFunctions.TruncateTime(asp.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS))
																			  select asp.ID).Any()
																				||
																			(from spi in context.SubscriberPlanInfoes
																			 join account in context.Accounts on spi.Account equals account.ID
																			 join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
																			 join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
																			 join am in context.AppModules on bpt.AppModule equals am.ID
																			 where
																	 am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && account.ID == acc.ID &&
																	 (!bp.IsDemo || (bp.IsDemo && DbFunctions.DiffDays(DbFunctions.TruncateTime(spi.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS))
																			 select spi.ID).Any()
											where acd.Approval == a.ID &&
												  hasCollaborateModuleActive &&
												  us.Key == "A" &&
												  acr.Key == "ANR" &&
												  js.Key != "COM" &&
												  js.Key != "ARC" &&
												  (schedulesUser.Any() ||
												  scheduleUserGroup.Any()) &&
												  a.Deadline > DateTime.UtcNow &&
												  acd.Decision == null &&
												  acs.Key != "D" &&
												  u.BouncedEmailID == null
											select new UserCollaborateReminders
											{
												InternalUserRecipient = u.ID,
												Approvals = new List<NotificationApproval>
												  {
													  new NotificationApproval()
														  {
															  ID = a.ID,
															  Deadline = a.Deadline,
															  CreatedDate = a.CreatedDate
														  }
												  }.ToList(),
												NotificationsSchedules = schedulesUser.Concat(scheduleUserGroup).ToList()

											}).ToList();


			//group reminders per user
			var reminders = new List<UserCollaborateReminders>();
			foreach (var item in userCollaborateReminders)
			{
				var user = reminders.FirstOrDefault(u => u.InternalUserRecipient == item.InternalUserRecipient);
				if (user != null)
				{
					//remove approvals duplicates 
					user.Approvals = user.Approvals.Union(item.Approvals).Distinct(new Compare()).ToList();
				}
				else
				{
					reminders.Add(item);
				}
			}

			return reminders;
		}

		/// <summary>
		/// Update Log table after succesfuly sending an approval reminder email for an user
		/// </summary>
		/// <param name="userId">User recipient id</param>
		/// <param name="approvalId">Approval id</param>
		/// <param name="time"></param>
		/// <param name="context"></param>
		public static void MarkUserApprovalRemindersAsSent(int userId, int approvalId, DateTime sendTime, DbContextBL context)
		{
			UserApprovalReminderEmailLog userReminderLog = (from uarel in context.UserApprovalReminderEmailLogs
															where uarel.User == userId && uarel.Approval == approvalId
															select uarel).FirstOrDefault();
			//if no log found
			if (userReminderLog == null)
			{
				userReminderLog = new UserApprovalReminderEmailLog
				{
					User = userId,
					Approval = approvalId
				};

				context.UserApprovalReminderEmailLogs.Add(userReminderLog);
			}

			userReminderLog.LastSentDate = sendTime;
		}

		/// <summary>
		/// Check if the approval reminder email should be sent now or not
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="approval"></param>
		/// <param name="notificationSchedule"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static bool IsApprovalReminderTimeToSend(int userId, NotificationApproval approval, UserNotificationSchedule notificationSchedule, DbContextBL context)
		{
			DateTime lastSentEmailDate = (from uarel in context.UserApprovalReminderEmailLogs
										  where uarel.User == userId && uarel.Approval == approval.ID
										  select uarel.LastSentDate).FirstOrDefault();

			if (notificationSchedule.Trigger == (int)GMG.CoZone.Common.NotificationPresetCollaborateTrigger.AfterStart)
			{
				var duration = (DateTime.UtcNow - approval.CreatedDate);

				switch (notificationSchedule.Unit)
				{
					case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Hours:
						{
							double hours = duration.TotalHours;
							if (hours >= notificationSchedule.UnitNumber)
							{
								return GetTimeDiffrenceFromTheLastSendingDate(notificationSchedule.Frequency, lastSentEmailDate);
							}
							break;
						}
					case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Days:
						{
							double days = duration.TotalDays;
							if (days >= notificationSchedule.UnitNumber)
							{
								return GetTimeDiffrenceFromTheLastSendingDate(notificationSchedule.Frequency, lastSentEmailDate);
							}
							break;
						}
					case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Weeks:
						{
							double weeksCount = duration.TotalDays / 7;
							var weeks = (int)Math.Floor(weeksCount);
							if (weeks >= notificationSchedule.UnitNumber)
							{
								return GetTimeDiffrenceFromTheLastSendingDate(notificationSchedule.Frequency, lastSentEmailDate);
							}
							break;
						}
				}

			}
			else
			{
				var duration = (TimeSpan)(approval.Deadline - DateTime.UtcNow);

				switch (notificationSchedule.Unit)
				{
					case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Hours:
						{
							double hours = duration.TotalHours;
							if (hours <= notificationSchedule.UnitNumber)
							{
								return GetTimeDiffrenceFromTheLastSendingDate(notificationSchedule.Frequency, lastSentEmailDate);
							}
							break;
						}
					case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Days:
						{
							double days = duration.TotalDays;
							if (days <= notificationSchedule.UnitNumber)
							{
								return GetTimeDiffrenceFromTheLastSendingDate(notificationSchedule.Frequency, lastSentEmailDate);
							}
							break;
						}
					case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Weeks:
						{
							double weeksCount = duration.TotalDays / 7;
							var weeks = (int)Math.Floor(weeksCount);
							if (weeks <= notificationSchedule.UnitNumber)
							{
								return GetTimeDiffrenceFromTheLastSendingDate(notificationSchedule.Frequency, lastSentEmailDate);
							}
							break;
						}
				}
			}

			return false;
		}
		#endregion

		#region Private Methods

		/// <summary>
		/// Get external recipients for event types that can have this type of user
		/// </summary>
		/// <param name="notificationItem"></param>
		/// <returns></returns>
		private static bool HasExternalRecipients(Notification notificationItem)
		{
			switch (notificationItem.NotificationType)
			{
                case NotificationType.NewFileTransferAdded:
                    NewFileTransfer transfer = notificationItem as NewFileTransfer;

                    //If this field is set than the receiver is a collaborate external collaborator
                    if (transfer.ExternalRecipient.GetValueOrDefault() > 0)
                    {
                        return true;
                    }
                    break;

				case NotificationType.ApprovalReminder:
					ApprovalReminder approvalReminder = notificationItem as ApprovalReminder;

					//If this field is set than the receiver is a collaborate external collaborator
					if (approvalReminder.ExternalRecipient.GetValueOrDefault() > 0)
					{
						return true;
					}
					break;
				case NotificationType.DeliverNewPairingCode:
				case NotificationType.AccountSupport:
					return true;
				case NotificationType.ApprovalWasDownloaded:
				case NotificationType.ApprovalWasDeleted:
				case NotificationType.ApprovalWasUpdated:
				case NotificationType.NewApprovalAdded:
				case NotificationType.NewVersionWasCreated:
				case NotificationType.ApprovalWasShared:
				case NotificationType.ApprovalWasRejected:
				case NotificationType.ApprovalOverdue:
					{
						ApprovalNotification appNotif = notificationItem as ApprovalNotification;
						if (appNotif.ExternalRecipient.GetValueOrDefault() > 0)
						{
							return true;
						}
						break;
					}
				case NotificationType.DeliverFileTimeOut:
				case NotificationType.DeliverFileWasSubmitted:
				case NotificationType.DeliverFileErrored:
				case NotificationType.DeliverFileCompleted:
					{
						DeliverNotification deliverNotif = notificationItem as DeliverNotification;
						if (deliverNotif.ExternalRecipient.GetValueOrDefault() > 0)
						{
							return true;
						}
						break;
					}
				//TODO This code is related with CZ-2548. Should be removed if the netzwerk don't want externals users to recieve activate/deactivate emails
				//case NotificationType.PhaseWasActivated:
				//case NotificationType.PhaseWasDeactivated:
				case NotificationType.WorkflowPhaseRerun:
					{
						WorkflowNotification workflowNotif = notificationItem as WorkflowNotification;
						if (workflowNotif.ExternalRecipient.GetValueOrDefault() > 0)
						{
							return true;
						}
						break;
					}
			}

			return false;
		}

		/// <summary>
		/// Get time difference from the last sending date
		/// </summary>
		/// <param name="frecvencyKey"></param>
		/// <param name="lastSentEmailDate"></param>
		/// <returns></returns>
		private static bool GetTimeDiffrenceFromTheLastSendingDate(int frecvencyKey, DateTime lastSentEmailDate)
		{
			//if not entry found send email
			if (lastSentEmailDate == default(DateTime))
			{
				return true;
			}

			double intervalInMinutes = (DateTime.UtcNow - lastSentEmailDate).TotalMinutes;

			switch (frecvencyKey)
			{
				case (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryHour:
					if (intervalInMinutes >= 56) // minutes
					{
						return true;
					}
					break;
				case (int)GMG.CoZone.Common.NotificationPresetFrequency.Every2Hours:
					if (intervalInMinutes >= 2 * 58) // minutes
					{
						return true;
					}
					break;
				case (int)GMG.CoZone.Common.NotificationPresetFrequency.Every4Hours:
					if (intervalInMinutes >= 4 * 59) // minutes
					{
						return true;
					}
					break;
			}

			return false;
		}

		#endregion


		/// <summary>
		/// Gets emails that should be sent as part of an ProofStudio Session Email (groupped by user per each session)
		/// </summary>
		/// <param name="context">Database Context</param>
		/// <param name="sessionTimeoutDays">Timeout until PS Session should be considered expired and emails should be sent</param>
		/// <returns>List of emails that should be processed in this moment</returns>
		public static List<UserNotifications> GetNotificationsPerProofStudioSession(DbContextBL context, int sessionTimeoutDays)
		{
			var hours = sessionTimeoutDays * 24;
			// Get all database notifications for ProofStudioSession
			var databaseNotificationsList = (from n in context.NotificationItems
											 join evt in context.EventTypes on n.NotificationType equals evt.ID
											 join egr in context.EventGroups on evt.EventGroup equals egr.ID
											 join ps in context.ProofStudioSessions on n.PSSessionID equals ps.ID
											 join u in context.Users on n.InternalRecipient equals u.ID
											 join us in context.UserStatus on u.Status equals us.ID
											 where ps.IsCompleted || DbFunctions.DiffHours(ps.CreatedDate, DateTime.UtcNow) > hours && u.BouncedEmailID == null && !n.IsDeleted
											 select new UserNotifications
											 {
												 InternalUserRecipient =
													  new InternalUserRecipient
													  {
														  UserId = n.InternalRecipient.Value,
														  UserStatusKey = us.Key
													  },
												 Notifications = new List<UserNotification>
					{
						new UserNotification
						{
							InternalUserCreator = (n.Creator == null
								? null
								: new InternalUserCreator
								{
									UserId = n.Creator.Value
								}),
							NotificationItem = n,
							EventGroupKey = egr.Key,
							SessionId = ps.ID
						}
					}
											 }).ToList();


			// group notifications per user
			var userNotifications = new List<UserNotifications>();
			foreach (var item in databaseNotificationsList)
			{
				var user = userNotifications.FirstOrDefault(u => u.InternalUserRecipient.UserId == item.InternalUserRecipient.UserId);
				if (user != null)
				{
					user.Notifications.AddRange(item.Notifications);
				}
				else
				{
					userNotifications.Add(item);
				}
			}

			return userNotifications;
		}

		public static void MarkAsDeletedCompletedPSSessions(List<UserNotifications> userNotifications, DbContextBL context)
		{
			// Enable changes as the source method call had disabled it
			context.Configuration.AutoDetectChangesEnabled = true;

			List<int> sessionIds = userNotifications.SelectMany(t => t.Notifications).
				Select(t => t.SessionId).ToList();

			if (sessionIds != null && sessionIds.Count > 0)
			{
				List<ProofStudioSession> sessionsList = (from sessions in context.ProofStudioSessions
														 where sessionIds.Contains(sessions.ID)
														 select sessions).ToList();

				if (sessionsList != null && sessionsList.Count > 0)
				{
					for (int i = 0; i < sessionsList.Count; i++)
					{
						sessionsList[i].IsDeleted = true;
					}

					context.SaveChanges();
				}
			}

			context.Configuration.AutoDetectChangesEnabled = false;
		}

		private static List<int> AddLoggedUserWithOwnActivity(GMGColorDAL.User loggedUser, List<int> internalCollaborators)
		{
			if (loggedUser != null)
			{
				var loggedUserId = loggedUser.ID;
				if (loggedUser.IncludeMyOwnActivity && (internalCollaborators == null || !internalCollaborators.Contains(loggedUserId)))
				{
					internalCollaborators.Add(loggedUser.ID);
				}
			}

			return internalCollaborators;
		}

		public static List<TempNotificationItem> CreateTmpNotifForExternals(int approvalID, List<int> externalCollaborators, GMGColorDAL.User loggedUser, NotificationType emailType, string optionalMessage = "")
		{
			int loggedUserId = (loggedUser != null) ? loggedUser.ID : 0;
			List<int> approvalsIds = new List<int>() { approvalID };
			List<TempNotificationItem> tempNotificationItems = new List<TempNotificationItem>();
			foreach (var externalCollaborator in externalCollaborators)
			{
				tempNotificationItems.Add(TempNotificationItem.Create(approvalsIds, loggedUserId, externalCollaborator, emailType, true, optionalMessage));
			}

			return tempNotificationItems;
		}

		public static List<TempNotificationItem> CreateTmpNotifForOwnAndInternals(int approvalID, List<int> internalCollaborators, User loggedUser, NotificationType emailType, string optionalMessage = "")
		{
            if (internalCollaborators == null)
            {
                internalCollaborators = new List<int>();
            }
            var internalCollaboratorsAndOwn = AddLoggedUserWithOwnActivity(loggedUser, internalCollaborators);
			return CreateTmpNotifForInternals(approvalID, internalCollaborators, loggedUser, emailType, optionalMessage = "");
		}

		public static List<TempNotificationItem> CreateTmpNotifForInternals(int approvalID, List<int> internalCollaborators, GMGColorDAL.User loggedUser, NotificationType emailType, string optionalMessage = "")
		{
			int loggedUserId = (loggedUser != null) ? loggedUser.ID : 0;

			List<int> approvalsIds = new List<int>() { approvalID };
			List<TempNotificationItem> tempNotificationItems = new List<TempNotificationItem>();
			foreach (var internalCollaborator in internalCollaborators)
			{
				tempNotificationItems.Add(TempNotificationItem.Create(approvalsIds, loggedUserId, internalCollaborator, emailType, false, optionalMessage));
			}

			return tempNotificationItems;
		}

		public static List<TempNotificationItem> CreateTempNotificationItems(List<int> approvalsIds, List<int> internalCollaborators, List<int> externalCollaborators, GMGColorDAL.User loggedUser, string optionalMessage, NotificationType emailType)
		{
			int loggedUserId = (loggedUser != null) ? loggedUser.ID : 0;
			internalCollaborators = AddLoggedUserWithOwnActivity(loggedUser, internalCollaborators);

			List<TempNotificationItem> tempNotificationItems = new List<TempNotificationItem>();
			foreach (var internalCollaborator in internalCollaborators)
			{
				tempNotificationItems.Add(TempNotificationItem.Create(approvalsIds, loggedUserId, emailType, internalCollaborator, optionalMessage));
			}
			foreach (var externalCollaborator in externalCollaborators)
			{
				tempNotificationItems.Add(TempNotificationItem.Create(approvalsIds, loggedUserId, emailType, externalCollaborator, optionalMessage, true));
			}

			return tempNotificationItems;
		}

		public static void PutTmpNotifInQueue(List<TempNotificationItem> tempNotificationItems)
		{
			var queueName = GMGColorConfiguration.AppConfiguration.TempNotificationQueueName;
			if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
			{
				try
				{
					PutTempNotificationsToAWSQueue(tempNotificationItems, queueName);
				}
				catch (Exception ex)
				{
					throw new ExceptionBL(String.Format("Failed to write message to AWS SQS Queue with exception message: {0}", ex.Message));
				}
			}
			else
			{
				if (GMGColorDAL.Common.GMGColorCommon.IsQueueAvailable(queueName))
				{
					PutTempNotificationToLocalQueue(tempNotificationItems, queueName);
				}
				else
				{
					throw new ExceptionBL("The specified Queue (" + queueName + ") was not found on this server.");
				}
			}
		}

		private static void PutTempNotificationsToAWSQueue(List<TempNotificationItem> tempNotificationItems, string queueName)
		{
			foreach (var tempNotificationItem in tempNotificationItems)
			{
				var tempNotificationXML = JsonConvert.SerializeObject(tempNotificationItem);

				GMGColor.AWS.AWSSimpleQueueService.CreateMessage(tempNotificationXML,
																 queueName,
																 GMGColorConfiguration.AppConfiguration.AWSAccessKey,
																 GMGColorConfiguration.AppConfiguration.AWSSecretKey,
																 GMGColorConfiguration.AppConfiguration.AWSRegion);

				GMGColorLogging.log.Info(String.Format("WritenTempNotificationsToAWS {0} : {1}, message to send {2}", tempNotificationItem.InternalRecipient, tempNotificationItem.ExternalRecipient, tempNotificationXML));
			}
		}

		private static void PutTempNotificationToLocalQueue(List<TempNotificationItem> tempNotificationItems, string queueName)
		{
			var mq = new MessageQueue(queueName);

			if (mq != null)
			{
				Parallel.ForEach(tempNotificationItems, (tempNotificationItem) =>
				{
					var mm = new Message();
					mm.Body = tempNotificationItem;
					mq.Send(mm);
				});
			}
		}
	}
}
