﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class FileProcessorBL : BaseBL
    {
        #region enum

        #endregion

        #region Methods

        public static Dictionary<string, string> ConstructPdfToolboxParametersForSQSMessage(int mediaServiceId, GMGColorContext context, bool isWebPageScreenShot = false, bool restrictedZoom = false)
        {
            Dictionary<string, string> para = new Dictionary<string, string>();

            string colorSpace = string.Empty;
            string imageFormat = string.Empty;
            string pagebox = string.Empty;
            int resolution = 0;

            var mediaServiceData = (from ms in context.MediaServices
                                    join cs in context.Colorspaces on ms.Colorspace equals cs.ID
                                    where ms.ID == mediaServiceId
                                    select new
                                               {
                                                   ColorSpaceName = cs.Name,
                                                   ImageFormat = ms.ImageFormat,
                                                   ImageFormatObj = ms.ImageFormat1,
                                                   ParentImageFormat = ms.ImageFormat1 != null ? ms.ImageFormat1.Parent : null,
                                                   PageBox = ms.PageBox,
                                                   Resolution = ms.Resolution
                                               }).FirstOrDefault();
            
            if (mediaServiceData != null)
            {
                switch (mediaServiceData.ColorSpaceName.ToUpper())
                {
                    case "CMYK": colorSpace = "0";
                        break;
                    case "RGB": colorSpace = "1";
                        break;
                    case "GRAY": colorSpace = "2";
                        break;
                }

                if ((mediaServiceData.ParentImageFormat != 0) && (GMGColorDAL.ImageFormat.GetImageFormat(mediaServiceData.ParentImageFormat.GetValueOrDefault(), context) == GMGColorDAL.ImageFormat.Format.JPEG) && !isWebPageScreenShot)
                {
                    string compression = string.Empty;

                    switch (GMGColorDAL.ImageFormat.GetImageFormat(mediaServiceData.ImageFormat, context))
                    {
                        case GMGColorDAL.ImageFormat.Format.JPEG_low: compression = "80";
                            break;
                        case GMGColorDAL.ImageFormat.Format.JPEG_medium: compression = "60";
                            break;
                        case GMGColorDAL.ImageFormat.Format.JPEG_high: compression = "40";
                            break;
                        case GMGColorDAL.ImageFormat.Format.JPEG_minimum: compression = "100";
                            break;
                        case GMGColorDAL.ImageFormat.Format.JPEG_maximum: compression = "20";
                            break;
                        default:
                            break;
                    }

                    para.Add("Compression", compression);
                }

                imageFormat = isWebPageScreenShot ? GMGColorDAL.ImageFormat.Format.PNG.ToString() : ((mediaServiceData.ParentImageFormat > 0) ? mediaServiceData.ImageFormatObj.ParentImageFormat(context) : mediaServiceData.ImageFormatObj).Key;
                switch (GMGColorDAL.PageBox.GetPageBox(mediaServiceData.PageBox, context))
                {
                    case GMGColorDAL.PageBox.PageBoxName.CROPBOX: pagebox = "0";
                        break;
                    case GMGColorDAL.PageBox.PageBoxName.TRIMBOX: pagebox = "1";
                        break;
                    case GMGColorDAL.PageBox.PageBoxName.BLEEDBOX: pagebox = "2";
                        break;
                    default:
                        break;
                }

                resolution = mediaServiceData.Resolution;

            }
            else
            {
                // default values in case billing plan is null (eg: job submitted from Subscriber account - the subscribed plan doesn't include media service)
                colorSpace = "1";
                imageFormat = "JPG";
                pagebox = "0";
                resolution = 300;
            }

            if (restrictedZoom)
            {
                resolution = GMGColorConfiguration.AppConfiguration.RestrictedZoomDPI;
            }

            para.Add("Pagebox", pagebox);
            para.Add("ImageFormat", imageFormat);
            para.Add("ColorSpace", colorSpace);
            para.Add("Resolution", Convert.ToString(resolution));

            return para;
        }

        #endregion
    }
}