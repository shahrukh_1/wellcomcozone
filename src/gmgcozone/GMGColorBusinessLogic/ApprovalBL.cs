﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using GMG.WebHelpers.Job;
using GMGColor.AWS;
using GMGColorDAL.Common;
using GMG.CoZone.Common;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.Api;
using GMGColorDAL.CustomModels.ApprovalBL;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using GMGColorDAL.CustomModels.Checklists;
using GMGColorNotificationService;
using AdvancedSearch = GMGColorDAL.CustomModels.AdvancedSearch;
using Approval = GMGColorDAL.Approval;
using ApprovalAnnotationStatusChangeLog = GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog;
using ApprovalCollaborator = GMGColorDAL.ApprovalCollaborator;
using GMG.CoZone.Common.DomainModels;
using System.Data.SqlClient;

namespace GMGColorBusinessLogic
{
	public class ApprovalBL : BaseBL
	{
		#region Constants

		public static double OnePointInCm = 0.03527777777778;
		public const int ProofStudioPenWidthDefaultValue = 2;
		#endregion

		private static Dictionary<int, int> FileTypes;

		#region Methods

		/// <summary>
		/// Process new approvals
		/// </summary>
		/// <param name="numberOfApprovals">The number of approvals.</param>
		public static void ProcessNewApprovals(int numberOfApprovals = 10)
		{
			string processingKey = Guid.NewGuid().ToString();
			using (DbContextBL context = new DbContextBL())
			{
				List<Approval> approvals = (from a in context.Approvals where a.ProcessingInProgress == null select a).Take(numberOfApprovals).ToList();
				foreach (Approval approval in approvals)
				{
					if (approval.ProcessingInProgress == null)
					{
						approval.ProcessingInProgress = processingKey;
					}
				}
                if (approvals.Count > 0)
                {
                    context.SaveChanges();
                }

				//approvals = (from a in context.Approvals where a.ProcessingInProgress == processingKey select a).Take(numberOfApprovals).ToList();
				foreach (Approval approval in approvals)
				{
					ProcessNewApproval(approval, context);
				}
			}
		}

		/// <summary>
		/// Processes the new approval.
		/// </summary>
		/// <param name="approval">The approval.</param>
		/// <param name="context">The context.</param>
		private static void ProcessNewApproval(Approval approval, DbContextBL context)
		{
			try
			{
				string accountRegion = AccountBL.GetAccountRegion(approval.Owner, context);
                #region Move folder with file from temp to approvals

                string destinationFolderPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + approval.Guid + "/";
                string loggedUserTempFolderPath = "";
                string relativeFolderPath = "";

                if (approval.FileType == 28)
                {
                    loggedUserTempFolderPath = GMGColorDAL.User.GetUserHtmlTempFolderPath(approval.Creator, approval.Job1.Account, accountRegion);
                    relativeFolderPath = loggedUserTempFolderPath + approval.Guid + "/";
                    GMGColorIO.MoveDirectory(relativeFolderPath.TrimEnd('/').Replace("/", "\\"), destinationFolderPath.TrimEnd('/').Replace("/", "\\"), loggedUserTempFolderPath, accountRegion);
                }
                else
                {
                    loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(approval.Creator, approval.Job1.Account, accountRegion);
                    relativeFolderPath = loggedUserTempFolderPath + approval.Guid + "/";
                    if (GMGColorIO.FolderExists(relativeFolderPath, accountRegion))
                    {
                        string sourceRelativeFilePath = relativeFolderPath + approval.FileName;
                        string destinationFilePath = destinationFolderPath + approval.FileName;
                        GMGColorIO.FolderExists(destinationFolderPath, accountRegion, true);
                        GMGColorIO.MoveFile(sourceRelativeFilePath, destinationFilePath, accountRegion);
                        if (approval.FileType == 30)
                        {
                            GMGColorIO.MoveFile(sourceRelativeFilePath.Replace(".pdf", ".doc"), destinationFilePath.Replace(".pdf", ".doc"), accountRegion);
                        }
                        if (approval.FileType == 31)
                        {
                            GMGColorIO.MoveFile(sourceRelativeFilePath.Replace(".pdf", ".docx"), destinationFilePath.Replace(".pdf", ".docx"), accountRegion);
                        }
                        if (approval.FileType == 32)
                        {
                            GMGColorIO.MoveFile(sourceRelativeFilePath.Replace(".pdf", ".ppt"), destinationFilePath.Replace(".pdf", ".ppt"), accountRegion);
                        }
                        if (approval.FileType == 33)
                        {
                            GMGColorIO.MoveFile(sourceRelativeFilePath.Replace(".pdf", ".pptx"), destinationFilePath.Replace(".pdf", ".pptx"), accountRegion);
                        }
                        GMGColorIO.TryDeleteFolder(relativeFolderPath, accountRegion);
                    }

                    #endregion

                    CreateMessageForMediaDefineQueue(new ApprovalProcessingModel
                    {
                        ID = approval.ID,
                        RestrictedZoom = approval.RestrictedZoom,
                        ScreenshotUrl = approval.ScreenshotUrl,
                        WebPageSnapshotDelay = approval.WebPageSnapshotDelay,
                        ProcessingInProgress = approval.ProcessingInProgress
                    }, context);
                }
			}
			catch (ExceptionBL exbl)
			{
				GMGColorLogging.log.Error(exbl.Message, exbl);
			}
			catch (Exception ex)
			{
				GMGColorLogging.log.Error(ex.Message, ex);
			}
		}


		public static void CreateMessageForMediaDefineQueue(ApprovalProcessingModel approval, DbContextBL context)
		{
			int mediaServiceId = (from app in context.Approvals
								  join j in context.Jobs on app.Job equals j.ID
								  join acc in context.Accounts on j.Account equals acc.ID
								  join asp in context.AccountSubscriptionPlans on acc.ID equals asp.Account
								  join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
								  join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
								  join ms in context.MediaServices on bpt.MediaService equals ms.ID
								  select ms.ID).FirstOrDefault();
			if (mediaServiceId == 0)
			{
				mediaServiceId = (from app in context.Approvals
								  join j in context.Jobs on app.Job equals j.ID
								  join acc in context.Accounts on j.Account equals acc.ID
								  join spi in context.SubscriberPlanInfoes on acc.ID equals spi.Account
								  join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
								  join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
								  join ms in context.MediaServices on bpt.MediaService equals ms.ID
								  select ms.ID).FirstOrDefault();
			}

			var approvalType = (Approval.ApprovalTypeEnum)((from at in context.ApprovalTypes
															join a in context.Approvals on at.ID equals a.Type
															where
																a.ID == approval.ID
															select at.Key).FirstOrDefault());

            bool isUploadApproval = approvalType == Approval.ApprovalTypeEnum.Image ||
                                    approvalType == Approval.ApprovalTypeEnum.Movie;


            Dictionary<string, string> dictJobParameters = new Dictionary<string, string>();
			dictJobParameters.Add(Constants.ApprovalMsgApprovalType, Convert.ToString(approvalType));
			dictJobParameters.Add(Constants.ApprovalMsgApprovalID, Convert.ToString(approval.ID));
			dictJobParameters.Add(Constants.ApprovalMsgUrl, approval.ScreenshotUrl);
			dictJobParameters.Add(Constants.JobMessageType, GMGColorCommon.JobType.ApprovalJob.ToString());
			dictJobParameters.Add(Constants.IsJobHighRes, Convert.ToString(approval.JobIsHighRes));
			dictJobParameters.Add(Constants.HighResSessiobGuid, Convert.ToString(approval.SessionGuid));
			if (approvalType == Approval.ApprovalTypeEnum.WebPage && approval.WebPageSnapshotDelay.HasValue)
			{
				dictJobParameters.Add(Constants.ApprovalMsgWebCaptureDelay, Convert.ToString(approval.WebPageSnapshotDelay.Value));
			}
			else
			{
				dictJobParameters.Add(Constants.ApprovalMsgWebCaptureDelay, Convert.ToString(0));
			}

			foreach (KeyValuePair<string, string> item in FileProcessorBL.ConstructPdfToolboxParametersForSQSMessage(mediaServiceId, context, !isUploadApproval, approval.RestrictedZoom))
			{
				dictJobParameters.Add(item.Key, item.Value);
			}

			GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.ApprovalJob);

			approval.ProcessingInProgress = "completed";
			context.SaveChanges();
		}

		/// <summary>
		/// Determines wheter the specified user is the owner of the specified approval
		/// </summary>
		/// <param name="approvalId">Approval ID</param>
		/// <param name="userId">User ID</param>
		/// <param name="context">DB Context</param>
		/// <returns>Whether the user is the owner or not</returns>
		public static bool IsApprovalOwner(int approvalId, int userId, DbContextBL context)
		{
			return (from ap in context.Approvals
					where ap.ID == approvalId
					select ap).Any(t => t.Owner == userId);
		}

        public static bool IsHasHtmlApproval(List<int> approvals, DbContextBL context)
        {
            return (from ap in context.Approvals
                    where approvals.Contains(ap.ID)
                    select ap).Any(t => t.FileType == 28);
        }

        /// <summary>
        /// Determines whether the specified user is the owner of the specified folder
        /// </summary>
        /// <param name="approvalId">Approval ID</param>
        /// <param name="userId">User ID</param>
        /// <param name="context">DB Context</param>
        /// <returns>Whether the user is the owner or not</returns>
        public static bool IsFolderOwner(int folderId, int userId, DbContextBL context)
		{
			return (from f in context.Folders
					where f.ID == folderId
					select f).Any(t => t.Creator == userId);
		}

		/// <summary>
		/// Determins if the specified user is collaborator for the specified approval
		/// </summary>
		/// <param name="approvalId">Approval ID</param>
		/// <param name="userId">User ID</param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<int> ValidApprovalList(List<int> approvalId, int Collaborator, DbContextBL context)
		{
            return (from ac in context.ApprovalCollaborators
                    where approvalId.Contains(ac.Approval) && ac.Collaborator == Collaborator
                    select ac.Approval).ToList();
        }

        public static List<int> ValidFolderList(List<int> folderId, int Collaborator, DbContextBL context)
        {
            return (from fc in context.FolderCollaborators
                    where folderId.Contains(fc.Folder) && fc.Collaborator == Collaborator
                    select fc.Folder).ToList();
        }

        /// <summary>
        /// Determins if the specified user is collaborator for the specified folder
        /// </summary>
        /// <param name="approvalId">Approval ID</param>
        /// <param name="userId">User ID</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool ExistsFolderCollborator(int folderId, int userId, DbContextBL context)
		{
			return (from f in context.FolderCollaborators
					where f.Folder == folderId && f.Collaborator == userId
					select f).Any();
		}

		/// <summary>
		/// Gets approval versions
		/// </summary>
		/// <returns></returns>
		public static int[] GetVersions(int jobID, DbContextBL context)
		{
			return (from a in context.Approvals
					where a.Job == jobID
					select a.Version).ToArray();
		}

		/// <summary>
		/// Gets approval versions
		/// </summary>
		/// <returns></returns>
		public static int[] GetVersionIds(int jobID, DbContextBL context)
		{
			return (from a in context.Approvals
					where a.Job == jobID
					select a.ID).ToArray();
		}

		/// <summary>
		/// Gets approval type ID
		/// </summary>
		/// <param name="type"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static int GetApprovalTypeID(Approval.ApprovalTypeEnum type, DbContextBL context)
		{
			return (from apt in context.ApprovalTypes
					where apt.Key == (int)type
					select apt.ID).FirstOrDefault();
		}

		public static ApprovalTypeGuid GetApprovalTypeGuid(int appId, DbContextBL context)
		{
			return (from ap in context.Approvals
					join apt in context.ApprovalTypes on ap.Type equals apt.ID
					where ap.ID == appId
					select new ApprovalTypeGuid
					{
						ApprovalTypeKey = apt.Key,
						Guid = ap.Guid,
						FileName = ap.FileName
					}).FirstOrDefault();
		}

		/// <summary>
		/// Gets Approval ROle ID
		/// </summary>
		/// <param name="roleName"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static int GetApprovalRoleID(GMGColorDAL.ApprovalCollaboratorRole.ApprovalRoleName roleName, DbContextBL context)
		{
			return ApprovalCollaboratorRole.GetRole(roleName, context).ID;
		}

		/// <summary>
		/// Verifies is approval processing is completed
		/// </summary>
		/// <param name="approvalID"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static bool IsApprovalProcessingCompleted(int approvalID, DbContextBL context)
		{
			try
			{
				bool approvalExists = (from a in context.Approvals
									   where a.ID == approvalID
									   select a.ID).FirstOrDefault() > 0;

				if (!approvalExists)
				{
					return true;
				}

				var progressArray = (from ap in context.ApprovalPages where ap.Approval == approvalID select ap.Progress);
				int progress = (progressArray.Any() ? progressArray.ToArray() : new int[] { 0 }).Min();
				if (progress != 100)
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotVerifyIfApprovalProcessingIsCompleted, ex);
			}
		}

		public static List<int> GetExternalCollaborators(int approvalID, DbContextBL context)
		{
			try
			{
				return Approval.GetExternalCollaborators(approvalID, context);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalExternalCollaborators, ex);
			}
		}

		public static Approval GetApproval(int approvalID, GMGColorContext context)
		{
			try
			{
				return Approval.GetApproval(approvalID, context);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalCreator, ex);
			}
		}

		public static bool IsFirstVersion(int approvalID, GMGColorContext context)
		{
			return (from ap in context.Approvals
					where ap.ID == approvalID
					select ap.Version).FirstOrDefault() == 1;
		}

        public static bool HasWorkflow(int approvalID, GMGColorContext context)
        {
            return (from ap in context.Approvals
                    where ap.ID == approvalID && ap.CurrentPhase > 0
                    select ap.ID).Take(1).Any();
        }

        public static string GetJobName(int JobID, GMGColorContext context)
        {
            return (from j in context.Jobs
                    where j.ID == JobID
                    select j.Title).FirstOrDefault();
        }

        public static void UploadApprovalToFTPIfNeeded(Approval objApproval, DbContextBL context)
		{
			try
			{
				//Get Job Upload Engine Profile Folder 
				string profileFolder = null;
				GMG.CoZone.Common.Utils.TryGetProfileFolder(objApproval.FTPSourceFolder, out profileFolder);

				if (profileFolder != null)
				{
					//check if the upload engine profile has an output preset set
					int? outputProfilePreset = XMLEngineBL.GetProfileOutputPresetIfSet(profileFolder, objApproval.Creator, context);

					if (outputProfilePreset.GetValueOrDefault() == 0)
					{
						//No output preset set for this Profile
						return;
					}

					FTPPresetJobDownload conversionOutputJob = new FTPPresetJobDownload();
					conversionOutputJob.ApprovalJob = objApproval.ID;
					conversionOutputJob.Preset = outputProfilePreset.Value;

					context.FTPPresetJobDownloads.Add(conversionOutputJob);

					context.SaveChanges();

					//Create queue message to be sent to FTP Service
					Dictionary<string, string> dict = Utils.GetDictionaryForFTPQueue(conversionOutputJob.ID, GMG.CoZone.Common.AppModule.Collaborate, FTPAction.UploadAction);

					GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.FTPJob);
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotUploadApprovalToFTP, ex);
			}
		}

		public static Approval GetApprovalById(int approvalId, DbContextBL context)
		{
			try
			{
				return (from ap in context.Approvals
						where ap.ID == approvalId
						select ap).FirstOrDefault();
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalById, ex);
			}
		}

        public static int GetApprovalProofStudioZoomLevel(int approvalId, int accountId , DbContextBL context)
        {
            try
            {
                    var proofStudioZoomLevel = (from ap in context.Approvals
                            where ap.ID == approvalId
                            select ap.ProofStudioZoomLevel).FirstOrDefault();
                    if (proofStudioZoomLevel.HasValue)
                    {
                        return proofStudioZoomLevel.Value;
                    }
                return 0;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalProofStudioZoomLevel, ex);
            }
        }

        public static int IsProofStudioZoomLevelEnabled( int accountId, DbContextBL context)
        {
            try
            {
                var EnableProofStudioZoomlevelFitToPageOnload = (from acc in context.AccountSettings
                                                                 where acc.Account == accountId && acc.Name == Constants.EnableProofStudioZoomlevelFitToPageOnload
                                                                 select acc.Value).FirstOrDefault();
                return EnableProofStudioZoomlevelFitToPageOnload != null ? EnableProofStudioZoomlevelFitToPageOnload == "True" ? 1 : 0 : 0;
                
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalProofStudioZoomLevel, ex);
            }
        }

        public static bool IsShowSubstrateFromICCProfileInProofStudio(int accountId, DbContextBL context)
        {
            try
            {
                var ShowSubstrateFromICCProfileInProofStudio = (from acc in context.AccountSettings
                                                                 where acc.Account == accountId && acc.Name == Constants.ShowSubstrateFromICCProfileInProofStudio
                                                                 select acc.Value).FirstOrDefault();
                return ShowSubstrateFromICCProfileInProofStudio != null ? ShowSubstrateFromICCProfileInProofStudio == "True" ? true : false : false;

            }
            catch (Exception ex)
            {
                throw new ExceptionBL("Error: ApprovalBL.IsSubstrateFromICCProfileInPrrofStudioEnabled", ex);
            }
        }


        public static SharedApproval GetSharedApprovalByApprovalAndColalborator(int approvalID, int colalboratorID,
			DbContextBL context)
		{
			try
			{
				return (from sa in context.SharedApprovals
						where sa.Approval == approvalID && sa.ExternalCollaborator == colalboratorID
						select sa).FirstOrDefault();
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetSharedApproval, ex);
			}
		}

		public static DateTime GetApprovalCreatedDate(int approvalId, DbContextBL context)
		{
			return (from ap in context.Approvals
					where ap.ID == approvalId
					select ap.CreatedDate).FirstOrDefault();
		}

		/// <summary>
		/// Calculate Approval Decision based on approval folder/ account settings
		/// </summary>
		/// <param name="approvalId">Approval Id</param>
		/// <param name="allcolDecReqSettingName">Name of the setting</param>
		/// <param name="context">Db Context</param>
		/// <returns>ID of the decision in case it exists</returns>
		/* not used
        public static int? GetApprovalDecision(int approvalId, string allcolDecReqSettingName, DbContextBL context)
        {
            try
            {
                int? approvalDecision = null;

                var approvalInfo = (from a in context.Approvals
                                    where a.ID == approvalId
                                    select new
                                    {
                                        FolderId = a.Folders.Count > 0 ? a.Folders.FirstOrDefault().ID : 0,
                                        a.PrimaryDecisionMaker,
                                        a.ExternalPrimaryDecisionMaker
                                    }).FirstOrDefault();

                if (approvalInfo != null)
                {
                    if (approvalInfo.PrimaryDecisionMaker.GetValueOrDefault() != 0)
                    {
                        //get primary decision maker decision
                        approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
                                            where
                                                apcd.Approval == approvalId &&
                                                apcd.Collaborator == approvalInfo.PrimaryDecisionMaker.Value
                                            select apcd.Decision).FirstOrDefault();
                    }
                    else if (approvalInfo.ExternalPrimaryDecisionMaker.GetValueOrDefault() != 0)
                    {
                        //get external primay decision maker decision
                        approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
                                            where
                                                apcd.Approval == approvalId &&
                                                apcd.ExternalCollaborator == approvalInfo.ExternalPrimaryDecisionMaker.Value
                                            select apcd.Decision).FirstOrDefault();
                    }
                    else
                    {
                        bool allColDecRequired = false;
                        if (approvalInfo.FolderId > 0)
                        {
                            //approval is in folder
                            allColDecRequired = (from f in context.Folders
                                                 where f.ID == approvalInfo.FolderId
                                                 select f.AllCollaboratorsDecisionRequired).FirstOrDefault();
                        }
                        else
                        {
                            //get account setting value
                            string accSetting = (from a in context.Approvals
                                                 join j in context.Jobs on a.Job equals j.ID
                                                 join ac in context.Accounts on j.Account equals ac.ID
                                                 join acs in context.AccountSettings on ac.ID equals acs.Account
                                                 where a.ID == approvalId && acs.Name == allcolDecReqSettingName
                                                 select acs.Value).FirstOrDefault();

                            if (accSetting != null)
                            {
                                bool.TryParse(accSetting, out allColDecRequired);
                            }
                        }

                        // if not all decisions are required or all decisions have been taken get the lowest by priority
                        if (!allColDecRequired || !((from apcd in context.ApprovalCollaboratorDecisions
                                                     where
                                                         apcd.Approval == approvalId && apcd.Decision == null
                                                     select apcd.ID).Any()))
                        {
                            approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
                                                join dec in context.ApprovalDecisions on apcd.Decision equals dec.ID
                                                where
                                                    apcd.Approval == approvalId
                                                orderby dec.Priority ascending
                                                select apcd.Decision).FirstOrDefault();
                        }
                    }
                }

                return approvalDecision.GetValueOrDefault() > 0 ? approvalDecision : null;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalDecision, ex);
            }
        }
        */
		/// <summary>
		/// Calculate Approval Decision based on current user
		/// </summary>
		/// <param name="approvalId">Approval Id</param>
		/// <param name="allcolDecReqSettingName">Name of the setting</param>
		/// <param name="context">Db Context</param>
		/// <returns>ID of the decision in case it exists</returns>
		[ObsoleteAttribute("This method is obsolete. Call GetInternalUserApprovalDecision or GetExternalUserApprovalDecision from collaborate module instead.", false)]
		public static int? GetApprovalDecision(int approvalId, string userGuid, bool isExternal, DbContextBL context)
		{
			try
			{
				int? approvalDecision = null;

				if (isExternal)
				{
					approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
										join ec in context.ExternalCollaborators on apcd.ExternalCollaborator equals ec.ID
										join a in context.Approvals on apcd.Approval equals a.ID
										where apcd.Approval == approvalId && ec.Guid == userGuid && apcd.Phase == a.CurrentPhase
										select apcd.Decision).FirstOrDefault();
				}
				else
				{
					approvalDecision = (from apcd in context.ApprovalCollaboratorDecisions
										join col in context.Users on apcd.Collaborator equals col.ID
										join a in context.Approvals on apcd.Approval equals a.ID
										where apcd.Approval == approvalId && col.Guid == userGuid && apcd.Phase == a.CurrentPhase
										select apcd.Decision).FirstOrDefault();
				}

				return approvalDecision.GetValueOrDefault() > 0 ? approvalDecision : null;
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalDecision, ex);
			}
		}

		/// <summary>
		/// Checks if there is already a 
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="folderId"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static Job JobWithSameFileNameExistsInFolder(string fileName, int folderId, int loggedAccountId, DbContextBL context)
		{
			Job job = (from a in context.Approvals
					   let folder = (from f in context.Folders
									 where f.ID == folderId && f.Account == loggedAccountId
									 select f).FirstOrDefault()
					   join j in context.Jobs on a.Job equals j.ID
					   join js in context.JobStatus on j.Status equals js.ID
					   where a.Folders.Contains(folder) && a.FileName.Equals(fileName, StringComparison.OrdinalIgnoreCase) && js.Key != "ARC" && js.Key != "COM" && a.DeletePermanently == false && a.CurrentPhase == null
					   select j).FirstOrDefault();
			return job;
		}

		/// <summary>
		/// Retrieve approval decisions
		/// </summary>
		/// <param name="approvals"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<Approval.ApprovalDecisions> GetApprovalsDecisions(List<int> approvals, int accId, int userLocale, DbContextBL context)
		{
			List<Approval.ApprovalDecisions> approvalsDecisions = new List<Approval.ApprovalDecisions>();
			List<AccountSettings.ApprovalCustomDecision> lstCustomDecisions = ApprovalCustomDecision.GetCustomDecisionsByAccountId(accId, userLocale, context);

			var approvalDetails = GetApprovalDashboardPhaseDetails(approvals, accId, context);

			foreach (var apd in approvalDetails)
			{
				foreach (var collab in apd.Collaborators)
				{
					Approval.CollaboratorDecision item = new Approval.CollaboratorDecision()
					{
						CollaboratorName = String.Format("{0} {1}", collab.GivenName, collab.FamilyName),
						ApprovalDecision = string.IsNullOrEmpty(collab.Decision) ?
											string.Empty
											:
											lstCustomDecisions.Any(d => d.Key == collab.Decision && !string.IsNullOrEmpty(d.Name)) ?
											lstCustomDecisions.FirstOrDefault(d => d.Key == collab.Decision).Name
											:
											ApprovalDecision.GetLocalizedApprovalDecision(collab.Decision),
					};

					if (!string.IsNullOrEmpty(apd.WorkflowName))
					{
						Approval.ApprovalDecisions approvalDecisions =
							approvalsDecisions.FirstOrDefault(x => x.ApprovalId == apd.ApprovalID);
						if (approvalDecisions != null)
						{
							approvalDecisions.Decisions.Add(item);
						}
						else
						{
							Approval.ApprovalDecisions dec = new Approval.ApprovalDecisions();
							dec.ApprovalId = apd.ApprovalID;
							dec.PhaseName = apd.PhaseName;
							dec.WorkflowName = apd.WorkflowName;
							dec.Decisions = new List<Approval.CollaboratorDecision>() { item };

							//check if approval phase has the option enabled should be viewed by all
							if (apd.PhaseShouldBeViewedByAll)
							{
								//check if PDM has approved the phase and the phase is viewed by all
								if (apd.Collaborators.Any(c => c.IsPDM == true && c.Decision == apd.PhaseTrigger) &&
									apd.Collaborators.All(c => c.HasViewed) || apd.PhaseManuallyCompleted)
								{
									dec.PhaseStatus = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
								}
								else if (apd.Collaborators.Any(c => c.IsPDM == true && !string.IsNullOrEmpty(c.Decision) && c.Decision != apd.PhaseTrigger))
								{
									dec.PhaseStatus = GMGColor.Resources.Resources.lblPhaseStatusRejected;
								}
								else
								{
									dec.PhaseStatus = GMGColor.Resources.Resources.lblStatusInProgress;
								}
							}
							else
							{
								if (apd.Collaborators.Any(c => c.IsPDM == true))
								{
									if (apd.Collaborators.Any(c => c.IsPDM == true && !string.IsNullOrEmpty(c.Decision) && c.Decision == apd.PhaseTrigger) || apd.PhaseManuallyCompleted)
									{
										dec.PhaseStatus = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
									}
									else if (apd.Collaborators.Any(c => c.IsPDM == true && !string.IsNullOrEmpty(c.Decision) && c.Decision != apd.PhaseTrigger))
									{
										dec.PhaseStatus = GMGColor.Resources.Resources.lblPhaseStatusRejected;
									}
									else
									{
										dec.PhaseStatus = GMGColor.Resources.Resources.lblStatusInProgress;
									}
								}
								else
								{
									if (apd.Collaborators.All(c => c.Decision == apd.PhaseTrigger) || apd.PhaseManuallyCompleted)
									{
										dec.PhaseStatus = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
									}
									else if (apd.Collaborators.Any(c => !string.IsNullOrEmpty(c.Decision) && c.Decision != apd.PhaseTrigger))
									{
										dec.PhaseStatus = GMGColor.Resources.Resources.lblPhaseStatusRejected;
									}
									else
									{
										dec.PhaseStatus = GMGColor.Resources.Resources.lblStatusInProgress;
									}
								}

							}
							approvalsDecisions.Add(dec);
						}
					}
				}
			}

			//in case retoucher workflow is active and  job status is changes complete add user to the tooltip that changes the status
			string retoucherSettingName = CollaborateGlobalSetting.EnableRetoucherWorkflow.ToString();
			string isRetoucherEnabledSetting = (from acs in context.AccountSettings
												where acs.Account == accId && acs.Name == retoucherSettingName
												select acs.Value).FirstOrDefault();

			bool isRetoucherEnabled;
			bool.TryParse(isRetoucherEnabledSetting, out isRetoucherEnabled);
			if (isRetoucherEnabled)
			{
				var jobChangesComplete = (from ap in context.Approvals
										  join j in context.Jobs on ap.Job equals j.ID
										  join js in context.JobStatus on j.Status equals js.ID
										  where approvals.Contains(ap.ID) && js.Key == "CCM"
										  select new
										  {
											  ApprovalId = ap.ID,
											  CollaboratorName = (from jslg in context.JobStatusChangeLogs
																  join u in context.Users on jslg.Modifier equals u.ID
																  where jslg.Job == j.ID
																  orderby jslg.ModifiedDate descending
																  select u.GivenName + " " + u.FamilyName).FirstOrDefault()
										  }).ToList();

				foreach (var jobChangeStatus in jobChangesComplete)
				{
					Approval.CollaboratorDecision item = new Approval.CollaboratorDecision()
					{
						CollaboratorName = jobChangeStatus.CollaboratorName,
						ApprovalDecision = GMGColor.Resources.Resources.lblChangesComplete
					};

					Approval.ApprovalDecisions approvalDecisions =
						approvalsDecisions.FirstOrDefault(x => x.ApprovalId == jobChangeStatus.ApprovalId);

					if (approvalDecisions != null)
					{
						approvalDecisions.Decisions.Add(item);
					}
					else
					{
						approvalsDecisions.Add(new Approval.ApprovalDecisions()
						{
							ApprovalId = jobChangeStatus.ApprovalId,
							Decisions = new List<Approval.CollaboratorDecision>() { item }
						});
					}
				}
			}

			foreach (var app in approvalsDecisions)
			{
				app.Decisions = app.Decisions.Where(t => t.ApprovalDecision != string.Empty).ToList();
			}

			return approvalsDecisions;
		}


		public static List<GMGColorDAL.CustomModels.ApprovalBL.ApprovalDashboardPhaseDetails> GetApprovalDashboardPhaseDetails(List<int> approvals, int loggedAccount, DbContextBL context)
		{
			return (from ap in context.Approvals
					join ajph in context.ApprovalJobPhases on ap.CurrentPhase equals ajph.ID
					let intCollaborators = (from apc in context.ApprovalCollaborators
											join u in context.Users on apc.Collaborator equals u.ID
											let hasViewed = (from auvi in context.ApprovalUserViewInfoes
															 where auvi.Approval == ap.ID && auvi.Phase == ap.CurrentPhase && auvi.User == u.ID
															 select auvi.ID).FirstOrDefault()
											let decission = (from acd in context.ApprovalCollaboratorDecisions
															 join apd in context.ApprovalDecisions on acd.Decision equals apd.ID
															 where acd.Approval == ap.ID && acd.Collaborator == u.ID && acd.Phase == ap.CurrentPhase && acd.Decision != null
															 select apd.Key).FirstOrDefault()
											where apc.Approval == ap.ID && apc.Phase == ap.CurrentPhase
											select new GMGColorDAL.CustomModels.ApprovalBL.ApprovalDashboardCollaboratorsDetails()
											{
												CollaboratorID = u.ID,
												HasViewed = hasViewed != 0 ? true : false,
												IsPDM = ap.PrimaryDecisionMaker == u.ID ? true : false,
												GivenName = u.GivenName,
												FamilyName = u.FamilyName,
												Decision = decission
											}).ToList()
					let extCollaborators = (from sa in context.SharedApprovals
											join exc in context.ExternalCollaborators on sa.ExternalCollaborator equals exc.ID
											let hasViewed = (from auvi in context.ApprovalUserViewInfoes
															 where auvi.Approval == ap.ID && auvi.Phase == ap.CurrentPhase && auvi.User == exc.ID
															 select auvi.ID).FirstOrDefault()
											let decission = (from acd in context.ApprovalCollaboratorDecisions
															 join apd in context.ApprovalDecisions on acd.Decision equals apd.ID
															 where acd.Approval == ap.ID && acd.ExternalCollaborator == exc.ID && acd.Phase == ap.CurrentPhase && acd.Decision != null
															 select apd.Key).FirstOrDefault()
											where sa.Approval == ap.ID
											select new GMGColorDAL.CustomModels.ApprovalBL.ApprovalDashboardCollaboratorsDetails()
											{
												CollaboratorID = exc.ID,
												HasViewed = hasViewed != 0 ? true : false,
												IsPDM = ap.PrimaryDecisionMaker == exc.ID ? true : false,
												GivenName = exc.GivenName,
												FamilyName = exc.FamilyName,
												Decision = decission
											}).ToList()
					let phaseTrigger = (from ajp in context.ApprovalJobPhases
										join apd in context.ApprovalDecisions on ajp.ApprovalTrigger equals apd.ID
										where ajp.ID == ap.CurrentPhase
										select apd.Key).FirstOrDefault()
					let shouldBeViewedByAll = (from ajp in context.ApprovalJobPhases
											   join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
											   where ajp.ID == ap.CurrentPhase
											   select aph.ApprovalShouldBeViewedByAll).FirstOrDefault()
					let workflowName = (from a in context.Approvals
										join j in context.Jobs on a.Job equals j.ID
										join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
										where ajw.Account == loggedAccount && a.ID == ap.ID
										select ajw.Name).FirstOrDefault()
					let manualPhaseComplete = (from ajpa in context.ApprovalJobPhaseApprovals
											   where ajpa.Approval == ap.ID && ajpa.Phase == ap.CurrentPhase
											   select ajpa.PhaseMarkedAsCompleted).FirstOrDefault()
					where approvals.Contains(ap.ID)
					select new GMGColorDAL.CustomModels.ApprovalBL.ApprovalDashboardPhaseDetails()
					{
						ApprovalID = ap.ID,
						PhaseShouldBeViewedByAll = shouldBeViewedByAll,
						CurrentPhase = ap.CurrentPhase,
						PhaseName = ajph.Name,
						PhaseTrigger = phaseTrigger,
						WorkflowName = workflowName,
						PhaseManuallyCompleted = manualPhaseComplete,
						Collaborators = intCollaborators.Concat(extCollaborators).ToList()
					}).ToList();
		}

        /// <summary>
        /// Gets the data from the DB needed for the visual display indicator
        /// </summary>
        /// <param name="approvals"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<Approval.ApprovalsVisualIndicatorData> GetApprovalsVisualIndicatorData(List<int> approvals, DbContextBL context)
        {
            var approvalsVisualIndicatorData = new List<Approval.ApprovalsVisualIndicatorData>();

            var collaborators = (from a in context.Approvals
                                 join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                 join u in context.Users on ac.Collaborator equals u.ID
                                 join us in context.UserStatus on u.Status equals us.ID
                                 join acr in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole equals acr.ID
                                 where approvals.Contains(a.ID) && ac.Phase == a.CurrentPhase && acr.Key != "RDO" && acr.Key != "RET" && us.Key != "D"
                                 let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                 join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                 where acd.Approval == a.ID && acd.Collaborator == u.ID && acd.Decision != null && acd.Phase == a.CurrentPhase
                                                 select ad.Key).FirstOrDefault()
                                 let hasOpenedFile = (from apuvi in context.ApprovalUserViewInfoes
                                                      where apuvi.Approval == a.ID && apuvi.User == u.ID && apuvi.Phase == a.CurrentPhase
                                                      select apuvi.ID).Any()

                                 let substituteuser = (from u in context.Users
                                                       where u.ID == ac.SubstituteUser
                                                       select " (To " + u.GivenName + " " + u.FamilyName + ")").FirstOrDefault()

                                 select new
                                 {
                                     ApprovalId = a.ID,
                                     u.GivenName,
                                     u.FamilyName,
                                     u.Status,
                                     Decision = decision,
                                     hasOpenedFile,
                                     substituteuser = substituteuser == null ? "" : substituteuser
                                 }
                            ).Union(from a in context.Approvals
                                    join sa in context.SharedApprovals on a.ID equals sa.Approval
                                    join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                                    join acr in context.ApprovalCollaboratorRoles on sa.ApprovalCollaboratorRole equals acr.ID
                                    where approvals.Contains(a.ID) && sa.Phase == a.CurrentPhase && acr.Key != "RDO" && acr.Key != "RET" && !sa.IsExpired && !ec.IsDeleted
                                    let decision = (from acd in context.ApprovalCollaboratorDecisions
                                                    join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                    where acd.Approval == a.ID && acd.ExternalCollaborator == ec.ID && acd.Decision != null && acd.Phase == a.CurrentPhase
                                                    select ad.Key).FirstOrDefault()
                                    let hasOpenedFile = (from apuvi in context.ApprovalUserViewInfoes
                                                         where apuvi.Approval == a.ID && apuvi.ExternalUser == ec.ID && apuvi.Phase == a.CurrentPhase
                                                         select apuvi.ID).Any()
                                    select new
                                    {
                                        ApprovalId = a.ID,
                                        ec.GivenName,
                                        ec.FamilyName,
                                        Status = 1,
                                        Decision = decision,
                                        hasOpenedFile,
                                        substituteuser = ""

                                    }).ToList();

            if (collaborators.Count > 0)
            {
                foreach (var collab in collaborators)
                {
                    Approval.ApprovalsVisualIndicatorData approvalvisualData =
                        approvalsVisualIndicatorData.FirstOrDefault(x => x.ApprovalId == collab.ApprovalId);

                    if (approvalvisualData == null)
                    {
                        approvalvisualData = new Approval.ApprovalsVisualIndicatorData { ApprovalId = collab.ApprovalId };
                        approvalsVisualIndicatorData.Add(approvalvisualData);
                    }

                    if (collab.Decision != null)
                    {
                        if (collab.Decision == "APD" || collab.Decision == "AWC")
                        {
                            if (collab.Status == (int)UserStatus.Inactive)
                            {
                                approvalvisualData.greenUsers.Add(collab.GivenName + " " + collab.FamilyName +" "+ "(INACTIVE)");
                            }
                            else
                            {
                            approvalvisualData.greenUsers.Add(collab.GivenName + " " + collab.FamilyName + collab.substituteuser);
                        }
                        }

                        else if (collab.Decision == "NAP")
                        {
                           approvalvisualData.orangeUsers.Add(collab.GivenName + " " + collab.FamilyName);
                        }
                        
                        else
                        {
                            if (collab.Status == (int)UserStatus.Inactive)
                            {
                                approvalvisualData.redUsers.Add(collab.GivenName + " " + collab.FamilyName + " " + "(INACTIVE)");
                            }
                        else
                        {
                            approvalvisualData.redUsers.Add(collab.GivenName + " " + collab.FamilyName + collab.substituteuser);
                        }
                    }
                    

                        }
                    else if (collab.hasOpenedFile)
                    {
                        if (collab.Status == (int)UserStatus.Inactive)
                        {
                            approvalvisualData.greyUsers.Add(collab.GivenName + " " + collab.FamilyName + " "+ "(INACTIVE)");
                        }
                        else
                        {
                        approvalvisualData.greyUsers.Add(collab.GivenName + " " + collab.FamilyName + collab.substituteuser);
                    }
                    }
                    else
                    {
                        if (collab.Status == (int)UserStatus.Inactive)
                        {
                            approvalvisualData.whiteUsers.Add(collab.GivenName + " " + collab.FamilyName + " " + "(INACTIVE)");
                        }
                        else
                        {
                        approvalvisualData.whiteUsers.Add(collab.GivenName + " " + collab.FamilyName + collab.substituteuser);
                    }
                }
            }
            }

            return approvalsVisualIndicatorData;
        }

		/// <summary>
		/// Gets the list of annotations not not viewed for the current approvals
		/// </summary>
		/// <param name="approvalIds"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<Approval.ApprovalNotViewedAnnotations> GetApprovalsNotViewedAnnotations(List<int> approvalIds, int loggedUserId, DbContextBL context)
		{
			return (from app in context.ApprovalPages
					let viewdDate = (from appUsrViewInfo in context.ApprovalUserViewInfoes
									 where appUsrViewInfo.Approval == app.Approval && appUsrViewInfo.User == loggedUserId
									 orderby appUsrViewInfo.ViewedDate descending
									 select appUsrViewInfo.ViewedDate).FirstOrDefault()

                    let userDetails = (from u in context.Users
                                       where u.ID == loggedUserId select u).FirstOrDefault()
                    let publicPrivateAnnotations = (from ca in context.Approvals where approvalIds.Contains(ca.ID) && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

					where approvalIds.Contains(app.Approval)
					select new Approval.ApprovalNotViewedAnnotations
					{
						ApprovalId = app.Approval,
						NotViewed = (from aan in context.ApprovalAnnotations
                                     let approvalsPDM = (from a in context.Approvals where a.ID == app.Approval select a.PrimaryDecisionMaker).FirstOrDefault()
									 where app.ID == aan.Page && (!publicPrivateAnnotations || approvalsPDM == loggedUserId || userDetails.PrivateAnnotations == false || aan.IsPrivateAnnotation == false) && aan.Modifier != loggedUserId && (viewdDate == default(DateTime) || (viewdDate != default(DateTime) && aan.ModifiedDate >= viewdDate))
									 select aan.ID).Distinct().Count()
					}).GroupBy(x => x.ApprovalId, (key, g) => g.OrderByDescending(e => new { e.NotViewed }).FirstOrDefault()).ToList();
		}

		/// <summary>
		/// Gets the list of Retouchers who have worked on the current approvals
		/// </summary>
		/// <param name="approvalIds"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<Approval.ApprovalRetoucher> GetApprovalRetouchers(List<int> approvalIds, DbContextBL context)
		{
			List<Approval.ApprovalRetoucher> retouchers = (from ap in context.Approvals
														   join app in context.ApprovalPages on ap.ID equals app.Approval
														   let ret = (from apc in context.ApprovalCollaborators
																	  join u in context.Users on apc.Collaborator equals u.ID
																	  join us in context.UserStatus on u.Status equals us.ID
																	  let hasAnnotations = (from an in context.ApprovalAnnotations
																							where an.Page == app.ID && an.Creator == u.ID
																							select an.ID).Any()
																	  let hasActivityAnnotations = (from ascl in context.ApprovalAnnotationStatusChangeLogs
																									join an in context.ApprovalAnnotations on ascl.Annotation equals an.ID
																									join ast in context.ApprovalAnnotationStatus on ascl.Status equals ast.ID
																									where ascl.Modifier == u.ID && an.Page == app.ID
																									select ascl.ID).Any()
																	  join ur in context.UserRoles on u.ID equals ur.User
																	  join r in context.Roles on ur.Role equals r.ID
																	  where apc.Approval == ap.ID && us.Key != "D" && r.Key == "RET" && (hasAnnotations || hasActivityAnnotations)
																	  select u.GivenName + " " + u.FamilyName).Distinct().ToList()
														   where approvalIds.Contains(ap.ID)
														   select new Approval.ApprovalRetoucher
														   {
															   ApprovalId = ap.ID,
															   Retouchers = ret
														   }).ToList();

			return retouchers;
		}

		/// <summary>
		/// Gets Approval Info for the annotation report model
		/// </summary>
		/// <param name="approvalId"></param>
		/// <param name="loggedAccount"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static ApprovalAnnotationsModel GetApprovalInfo(int approvalId, GMGColorDAL.User LoggedUser, Account loggedAccount, DbContextBL context)
		{
			try
			{
				var approvalInfo = (from a in context.Approvals
									join u in context.Users on a.Creator equals u.ID
									join j in context.Jobs on a.Job equals j.ID
									join js in context.JobStatus on j.Status equals js.ID
									let pagesCount = (from ap in context.ApprovalPages
													  where ap.Approval == a.ID
													  select ap.ID).Count()
									let comments = (from an in context.ApprovalAnnotations
													join page in context.ApprovalPages on an.Page equals page.ID
													where page.Approval == a.ID && an.Parent == null && a.CurrentPhase == an.Phase
                                                    && (LoggedUser.PrivateAnnotations == false || an.IsPrivateAnnotation == false || an.Creator == LoggedUser.ID)
													select an.ID).Count()
									where a.ID == approvalId
									select new
									{
										JobTitle = j.Title,
										a.Version,
										CreatorName = u.GivenName + " " + u.FamilyName,
										a.CreatedDate,
										JobStatusKey = js.Key,
										Pages = pagesCount,
										Comments = comments,
										Name = a.FileName
									}).FirstOrDefault();

				DateTime createdUserTime = GMGColorFormatData.GetUserTimeFromUTC(approvalInfo.CreatedDate, loggedAccount.TimeZone);

                ApprovalAnnotationsModel approvalModel = new ApprovalAnnotationsModel
                {
					JobTitle = approvalInfo.JobTitle,
					Version = approvalInfo.Version,
					Creator = approvalInfo.CreatorName,
					JobStatus = JobStatu.GetLocalizedJobStatus(JobStatu.GetJobStatus(approvalInfo.JobStatusKey)),
					CreatedDate = GMGColorFormatData.GetFormattedDate(createdUserTime, loggedAccount.DateFormat1.Pattern) + " " + createdUserTime.ToString("h:mm tt"),
					NrOfPages = approvalInfo.Pages,
					Comments = approvalInfo.Comments,
					ApprovalName = approvalInfo.Name,
					ApprovalID = approvalId
				};

				return approvalModel;

			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalInfo, ex);
			}
		}

        /// <summary>
		/// Gets Approval Info for the annotation report model pdf
		/// </summary>
		/// <param name="approvalId"></param>
		/// <param name="loggedAccount"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static ApprovalAnnotationsModelPDF GetApprovalInfoPDF(int approvalId, Account loggedAccount, DbContextBL context)
        {
            try
            {
                var approvalInfo = (from a in context.Approvals
                                    join u in context.Users on a.Creator equals u.ID
                                    join j in context.Jobs on a.Job equals j.ID
                                    join js in context.JobStatus on j.Status equals js.ID
                                    let pagesCount = (from ap in context.ApprovalPages
                                                      where ap.Approval == a.ID
                                                      select ap.ID).Count()
                                    let comments = (from an in context.ApprovalAnnotations
                                                    join page in context.ApprovalPages on an.Page equals page.ID
                                                    where page.Approval == a.ID && an.Parent == null && a.CurrentPhase == an.Phase
                                                    select an.ID).Count()
                                    where a.ID == approvalId
                                    select new
                                    {
                                        JobTitle = j.Title,
                                        a.Version,
                                        CreatorName = u.GivenName + " " + u.FamilyName,
                                        a.CreatedDate,
                                        JobStatusKey = js.Key,
                                        Pages = pagesCount,
                                        Comments = comments,
                                        Name = a.FileName
                                    }).FirstOrDefault();

                DateTime createdUserTime = GMGColorFormatData.GetUserTimeFromUTC(approvalInfo.CreatedDate, loggedAccount.TimeZone);

                ApprovalAnnotationsModelPDF approvalModel = new ApprovalAnnotationsModelPDF
                {
                    JobTitle = approvalInfo.JobTitle,
                    Version = approvalInfo.Version,
                    Creator = approvalInfo.CreatorName,
                    JobStatus = JobStatu.GetLocalizedJobStatus(JobStatu.GetJobStatus(approvalInfo.JobStatusKey)),
                    CreatedDate = GMGColorFormatData.GetFormattedDate(createdUserTime, loggedAccount.DateFormat1.Pattern) + " " + createdUserTime.ToString("h:mm tt"),
                    NrOfPages = approvalInfo.Pages,
                    Comments = approvalInfo.Comments,
                    ApprovalName = approvalInfo.Name,
                    ApprovalID = approvalId
                };

                return approvalModel;

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalInfo, ex);
            }
        }

        public static int GetApprovalIdByGuid(string guid, DbContextBL context)
		{
			return (from ap in context.Approvals
					where ap.Guid == guid && !ap.DeletePermanently
					select ap.ID).FirstOrDefault();
		}

        public static string GetApprovalGuidById(int id, DbContextBL context)
        {
            return (from ap in context.Approvals
                    where ap.ID == id && !ap.DeletePermanently
                    select ap.Guid).FirstOrDefault();
        }

        public static List<string> GetImagesToBase64(List<int> approvalIDs, int loggedUser, DbContextBL context)
        {
            return (from anr in context.ApprovalAnnotationReports
                    where approvalIDs.Contains(anr.Approval) && anr.User == loggedUser
                    orderby anr.SerialNumber
                    select anr.Base64String).ToList();
        }

        public static void DeleteImagesToBase64(List<int> approvalIDs, int loggedUser, DbContextBL context)
        {
            var imagesToBase64 = (from anr in context.ApprovalAnnotationReports
                                   where approvalIDs.Contains(anr.Approval) && anr.User == loggedUser
                                   orderby anr.SerialNumber
                                   select anr).ToList();

            context.ApprovalAnnotationReports.RemoveRange(imagesToBase64);
            context.SaveChanges();
        }

		public static int GetJobId(DbContextBL context, int approvalId)
		{
			try
			{
				return (from ap in context.Approvals
						where ap.ID == approvalId
						select ap.Job).FirstOrDefault();
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalJob, ex);
			}
		}

		public static string GetJobGuid(int approvalId)
		{
			try
			{
				using (DbContextBL context = new DbContextBL())
				{
					return (from ap in context.Approvals
							where ap.ID == approvalId && ap.IsDeleted == false
							select ap.Guid).FirstOrDefault();
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalJob, ex);
			}
		}

		public static bool HasAccessToDownloadFile(string fileGuid, string userKey, bool isExternal, string apiKey)
		{
			try
			{
				if (apiKey.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
				{
					return false;
				}

				using (DbContextBL context = new DbContextBL())
				{
					if (isExternal)
					{
						return (from a in context.Approvals
								join sa in context.SharedApprovals on a.ID equals sa.Approval
								join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
								join j in context.Jobs on a.Job equals j.ID
								join acc in context.Accounts on j.Account equals acc.ID
								join accs in context.AccountStatus on acc.Status equals accs.ID
								where
									ec.Guid.ToLower() == userKey.ToLower() &&
									a.Guid.ToLower() == fileGuid.ToLower() &&
									a.IsDeleted == false &&
									ec.IsDeleted == false &&
									sa.IsExpired == false &&
									accs.Key == "A"
								select a.ID).Take(1).Any();
					}
					else
					{
						bool userIsAdmin = (from u in context.Users
											join ur in context.UserRoles on u.ID equals ur.User
											join r in context.Roles on ur.Role equals r.ID
											join am in context.AppModules on r.AppModule equals am.ID
											where
												u.Guid.ToLower() == userKey.ToLower() &&
												am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && r.Key.ToUpper() == "ADM"
											select true).FirstOrDefault();

						return (from a in context.Approvals
								join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
								join u in context.Users on ac.Collaborator equals u.ID
								join us in context.UserStatus on u.Status equals us.ID
								join j in context.Jobs on a.Job equals j.ID
								join acc in context.Accounts on j.Account equals acc.ID
								join accs in context.AccountStatus on acc.Status equals accs.ID

								join ur in context.UserRoles on u.ID equals ur.User
								join r in context.Roles on ur.Role equals r.ID
								join am in context.AppModules on r.AppModule equals am.ID

								where
									am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && (r.Key.ToUpper() != "NON" && r.Key.ToUpper() != "VIE") &&
									a.IsDeleted == false &&
									(u.Guid.ToLower() == userKey.ToLower() || userIsAdmin) &&
									a.Guid.ToLower() == fileGuid.ToLower() &&
									accs.Key == "A"
								select a.ID).Take(1).Any();
					}
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfUserHasAccessToDownloadFile, ex);
			}
		}

		public static ApprovalDownloadDetails DownloadFile(string fileGuid)
		{
			try
			{
				using (DbContextBL context = new DbContextBL())
				{
					var Data =   (
						from a in context.Approvals
						join j in context.Jobs on a.Job equals j.ID
						join acc in context.Accounts on j.Account equals acc.ID
						join accs in context.AccountStatus on acc.Status equals accs.ID
						where a.Guid.ToLower() == fileGuid.ToLower() && accs.Key == "A" && a.IsDeleted == false
						select new ApprovalDownloadDetails
                        {
							Region = acc.Region,
							Domain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
							FileName = (a.FileType == 30) ? a.FileName.Replace(".pdf", ".doc") : (a.FileType == 31) ? a.FileName.Replace(".pdf", ".docx") : (a.FileType == 32) ? a.FileName.Replace(".pdf", ".ppt") : (a.FileType == 33) ? a.FileName.Replace(".pdf", ".pptx") : a.FileName
                })
						.FirstOrDefault();

                    if (Data != null)
                    {
                        return Data;
                        }
                    }
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotDownloadFile, ex);
			}
			return null;
		}

		public static string GetFileName(string fileGuid)
		{
			try
			{
				using (DbContextBL context = new DbContextBL())
				{
					return (from ap in context.Approvals
							where ap.Guid == fileGuid && ap.IsDeleted == false
                            select (ap.FileType == 30) ? ap.FileName.Replace(".pdf", ".doc") : (ap.FileType == 31) ? ap.FileName.Replace(".pdf", ".docx") : (ap.FileType == 32) ? ap.FileName.Replace(".pdf", ".ppt") : (ap.FileType == 33) ? ap.FileName.Replace(".pdf", ".pptx") : ap.FileName).FirstOrDefault();
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetApprovalFileName, ex);
			}
		}

		public static List<Approval.DuplicatedFileNamesJobs> JobWithVersionSuffixAndSameFileNameExistsInFolder(string fileName, int folderId, int loggedAccountId, DbContextBL context)
		{
			return (from a in context.Approvals
					let folder = (from f in context.Folders
								  where f.ID == folderId && f.Account == loggedAccountId
								  select f).FirstOrDefault()
					join j in context.Jobs on a.Job equals j.ID
					join js in context.JobStatus on j.Status equals js.ID
					where a.Folders.Contains(folder) && a.FileName.StartsWith(fileName) && js.Key != "ARC" && js.Key != "COM" && a.DeletePermanently == false && a.CurrentPhase == null
					select new Approval.DuplicatedFileNamesJobs
					{
						FileName = a.FileName,
						JobID = j.ID
					}).ToList();
		}

		public static void SendDownloadEmail(string fileKey, string userKey, string sessionKey, bool isExternalCollaborator)
		{
			using (DbContextBL context = new DbContextBL())
			{
				int creatorId = isExternalCollaborator
					? (from exc in context.ExternalCollaborators
					   where exc.Guid == userKey
					   select exc.ID).FirstOrDefault()
					: (from u in context.Users
					   where u.Guid == userKey
					   select u.ID).FirstOrDefault();

				var approvalInfo = (from ap in context.Approvals
									where ap.Guid == fileKey
									select new
									{
										ap.ID,
										ap.OwnerUser
									}).FirstOrDefault();

				int? psSessionId = isExternalCollaborator
											  ? null
											  : GlobalNotificationBL.IsUserPerSessionEmailsEnabled(
												  NotificationTypeParser.GetKey(NotificationType.ApprovalWasDownloaded),
												  creatorId,
												  context)
												  ? GlobalNotificationBL.GetPSSessionId(sessionKey, context)
												  : null;

				NotificationServiceBL.CreateNotification(new ApprovalWasDownloaded
				{
					EventCreator = isExternalCollaborator ? null : (int?)creatorId,
					ExternalCreator = isExternalCollaborator ? (int?)creatorId : null,
					InternalRecipient = approvalInfo.OwnerUser.ID,
					CreatedDate = DateTime.UtcNow,
					ApprovalsIds = new[] { approvalInfo.ID }
				},
														isExternalCollaborator ? null : UserBL.GetUserById(creatorId, context),
														context,
														true,
														psSessionId
														);
			}
		}

		/// <summary>
		/// Retrieves account upload settings based on user Id
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static Approval.NewApprovalUploadInfo GetUEngineAccountSettingByUserId(int userId, DbContextBL context)
		{
			return (from a in context.Accounts
					join u in context.Users on a.ID equals u.Account
					join s in context.UploadEngineAccountSettings on a.UploadEngineSetings equals s.ID
					join ud in context.UploadEngineDuplicateFileNames on s.DuplicateFileName equals ud.ID
					where u.ID == userId
					select new Approval.NewApprovalUploadInfo
					{
						AccountId = a.ID,
						DuplicateFileKey = ud.Key,
						CanApplySettings = s.ApplyUploadSettingsForNewApproval
					}).FirstOrDefault();
		}

		/// <summary>
		/// Retrieves the status of the new uploaded file- new job, new version, ignored
		/// </summary>
		/// <param name="accountId"></param>
		/// <param name="fileName"></param>
		/// <param name="folderId"></param>
		/// <param name="duplicateFileKey"></param>
		/// <param name="job"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static ApprovalFileStatus NewFileStatus(int accountId, string fileName, int folderId, FileUploadDuplicateName duplicateFileKey, out Job job, DbContextBL context)
		{
			job = null;
			ApprovalFileStatus fileStatus = ApprovalFileStatus.NewJob;

			switch (duplicateFileKey)
			{
				case FileUploadDuplicateName.FileWithSuffix_vX:
					{
						var fName = GetFileNameWithoutExtension(fileName).ToLower();
						var vIndex = fName.LastIndexOf("_v", StringComparison.Ordinal);

						//Get filename without _vX suffix
						if (vIndex > 0)
						{
							fName = fileName.Substring(0, vIndex).ToLower();
						}

						//Checks whether a duplicate file with (_vX) suffix exists in the current folder
						var objJobs = ApprovalBL.JobWithVersionSuffixAndSameFileNameExistsInFolder(fName, folderId, accountId, context);

						//Checks whether exists in the current folder at least one file with same name and (_vX) suffix
						var duplicateHasSuffix = (from j in objJobs
												  let vIdx = j.FileName.ToLower().LastIndexOf("_v", StringComparison.Ordinal)
												  where vIdx > 0 && j.FileName.Substring(0, vIdx).ToLower() == fName
												  select j.JobID).Any();

						//Remove _vX suffix from all filenames and compare their content
						foreach (Approval.DuplicatedFileNamesJobs j in objJobs)
						{
							string checkedFileName = j.FileName.ToLower();

							var idx = checkedFileName.LastIndexOf("_v", StringComparison.Ordinal);
							if (idx > 0 && checkedFileName.Substring(0, idx) == fName || GetFileNameWithoutExtension(checkedFileName) == fName)
							{
								job = JobBL.GetJobByID(j.JobID, context);
								break;
							}
						}

						//ignore file that has no (_vX) suffix
						if (job == null)
						{
							fileStatus = ApprovalFileStatus.NewJob;
						}
						else if (vIndex <= 0 && duplicateHasSuffix)
						{
							fileStatus = ApprovalFileStatus.DuplicatedFile;
						}
						else
						{
							fileStatus = ApprovalFileStatus.NewVersion;
						}
					}
					break;
				case FileUploadDuplicateName.MatchFileNameExactly:
					{
						//Checks whether a duplicate file with exact filename exists in the current folder and add it as a version
						var duplicateJob = ApprovalBL.JobWithSameFileNameExistsInFolder(fileName, folderId, accountId, context);
						job = duplicateJob;
						fileStatus = job != null ? ApprovalFileStatus.NewVersion : ApprovalFileStatus.NewJob;

						break;
					}
				case FileUploadDuplicateName.Ignore:
					{
						//Checks whether a duplicate file with exact filename exists in the current folder and add it as a version
						var duplicateJob = ApprovalBL.JobWithSameFileNameExistsInFolder(fileName, folderId, accountId, context);
						if (duplicateJob != null && duplicateJob.ID > 0)
						{
							fileStatus = ApprovalFileStatus.IgnoredFile;
						}
						break;
					}
			}

			return fileStatus;
		}

		/// <summary>
		/// Sets newVersion Settings to settings from previous version
		/// </summary>
		/// <param name="newVersion">new approval version to be created</param>
		/// <param name="objJob">Existing Job object</param>
		/// <param name="enableVersionMirroring">Option to read version number from filename</param>
		/// <param name="context">Database Context</param>
		public static void SetSettingsFromPreviousVersion(Approval newVersion, Job objJob, GMGColorDAL.User loggedUser, bool enableVersionMirroring,
														int approverAndReviewerRoleID, int jobSource, List<ApprovalNotificationsHelper> approvalNotifications, DbContextBL context)
		{
			try
			{
				newVersion.Job = objJob.ID;
				newVersion.JobSource = jobSource;

				int[] versions = objJob.Approvals.Select(t => t.Version).ToArray();
				int previousVersionNumber = versions.Max();
				newVersion.Version = enableVersionMirroring ? GetFileVersion(newVersion.FileName, versions) : versions.Any() ? previousVersionNumber + 1 : 1;

				Approval previousVersion = objJob.Approvals.FirstOrDefault(t => t.Version == previousVersionNumber);

				newVersion.Owner = previousVersion.Owner;
				newVersion.IsPageSpreads = previousVersion.IsPageSpreads;
				newVersion.AllowDownloadOriginal = previousVersion.AllowDownloadOriginal;
				newVersion.LockProofWhenAllDecisionsMade = previousVersion.LockProofWhenAllDecisionsMade;
				newVersion.LockProofWhenFirstDecisionsMade = previousVersion.LockProofWhenFirstDecisionsMade;
				newVersion.PrimaryDecisionMaker = previousVersion.PrimaryDecisionMaker;
				newVersion.ExternalPrimaryDecisionMaker = previousVersion.ExternalPrimaryDecisionMaker;
				newVersion.OnlyOneDecisionRequired = previousVersion.OnlyOneDecisionRequired;
				newVersion.CurrentPhase = previousVersion.CurrentPhase;

				foreach (Approval version in objJob.Approvals)
				{
					version.IsLocked = true;
				}

				if (previousVersion.CurrentPhase.HasValue)
				{
					//add relations between each workflow phase and current approval
					var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
					{
						Approval1 = newVersion,
						Phase = previousVersion.CurrentPhase.Value,
						CreatedDate = DateTime.UtcNow,
						ModifiedDate = DateTime.UtcNow,
						CreatedBy = loggedUser.ID,
						ModifiedBy = loggedUser.ID
					};

					newVersion.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);
				}

				List<int> internalCollaborators = new List<int>();
				//set internal users permissions
				if (previousVersion.ApprovalCollaborators.Count > 0)
				{
					foreach (ApprovalCollaborator acl in previousVersion.ApprovalCollaborators)
					{
						if (acl.Phase < previousVersion.CurrentPhase)
						{
							continue;
						}

						ApprovalCollaborator collaborator = new ApprovalCollaborator();
						collaborator.Approval1 = newVersion;
						collaborator.Collaborator = acl.Collaborator;
						collaborator.ApprovalCollaboratorRole = acl.ApprovalCollaboratorRole;
						collaborator.AssignedDate = DateTime.UtcNow;
						collaborator.Phase = acl.Phase;
						context.ApprovalCollaborators.Add(collaborator);

						if (approverAndReviewerRoleID == acl.ApprovalCollaboratorRole)
						{
							GMGColorDAL.ApprovalCollaboratorDecision objDecision = new GMGColorDAL.ApprovalCollaboratorDecision();
							objDecision.Approval1 = newVersion;
							objDecision.Collaborator = acl.Collaborator;
							objDecision.AssignedDate = DateTime.UtcNow;
							objDecision.Phase = acl.Phase;
							context.ApprovalCollaboratorDecisions.Add(objDecision);
						}

						internalCollaborators.Add(acl.Collaborator);
					}
				}

				//set user groups permissions
				if (previousVersion.ApprovalCollaboratorGroups.Count > 0)
				{
					foreach (ApprovalCollaboratorGroup acg in previousVersion.ApprovalCollaboratorGroups)
					{
						if (acg.Phase < previousVersion.CurrentPhase)
						{
							continue;
						}

						ApprovalCollaboratorGroup collaboratorGroup = new ApprovalCollaboratorGroup();
						collaboratorGroup.Approval1 = newVersion;
						collaboratorGroup.CollaboratorGroup = acg.CollaboratorGroup;
						collaboratorGroup.AssignedDate = DateTime.UtcNow;
						collaboratorGroup.Phase = acg.Phase;
						context.ApprovalCollaboratorGroups.Add(collaboratorGroup);
					}
				}

				//make sure logged user is added as Collaborator
				if (previousVersion.ApprovalCollaborators.All(t => t.Collaborator != loggedUser.ID))
				{
					ApprovalCollaborator collaborator = new ApprovalCollaborator();
					collaborator.Approval1 = newVersion;
					collaborator.Collaborator = loggedUser.ID;
					collaborator.ApprovalCollaboratorRole = approverAndReviewerRoleID;
					collaborator.AssignedDate = DateTime.UtcNow;
					context.ApprovalCollaborators.Add(collaborator);

					GMGColorDAL.ApprovalCollaboratorDecision objDecision =
						new GMGColorDAL.ApprovalCollaboratorDecision();
					objDecision.Approval1 = newVersion;
					objDecision.Collaborator = loggedUser.ID;
					objDecision.AssignedDate = DateTime.UtcNow;
					context.ApprovalCollaboratorDecisions.Add(objDecision);

				}

				List<int> externalCollaborators = new List<int>();
				//set external users permission
				foreach (SharedApproval sha in previousVersion.SharedApprovals)
				{
					if (sha.Phase < previousVersion.CurrentPhase)
					{
						continue;
					}

					SharedApproval sharedApproval = new SharedApproval();
					sharedApproval.Approval1 = newVersion;
					sharedApproval.ExternalCollaborator = sha.ExternalCollaborator;
					sharedApproval.ApprovalCollaboratorRole = sha.ApprovalCollaboratorRole;
					sharedApproval.IsSharedDownloadURL = sha.IsSharedDownloadURL;
					sharedApproval.IsSharedURL = sha.IsSharedURL;
					sharedApproval.Phase = sha.Phase;
					context.SharedApprovals.Add(sharedApproval);

					if (approverAndReviewerRoleID == sha.ApprovalCollaboratorRole)
					{
						GMGColorDAL.ApprovalCollaboratorDecision objDecision = new GMGColorDAL.ApprovalCollaboratorDecision();
						objDecision.Approval1 = newVersion;
						objDecision.AssignedDate = DateTime.UtcNow;
						objDecision.ExternalCollaborator = sha.ExternalCollaborator;
						objDecision.Phase = sha.Phase;
						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}

					//use same rendering options
					var cstmProfile = (from apcsmProf in context.ApprovalCustomICCProfiles
									   where apcsmProf.Approval == previousVersion.ID
									   select apcsmProf.CustomICCProfile).FirstOrDefault();
					if (cstmProfile > 0)
					{
						ApprovalCustomICCProfile newVersCstmProf = new ApprovalCustomICCProfile();
						newVersCstmProf.Approval1 = newVersion;
						newVersCstmProf.CustomICCProfile = cstmProfile;
						context.ApprovalCustomICCProfiles.Add(newVersCstmProf);
					}

					externalCollaborators.Add(sha.ExternalCollaborator);
				}

				ApprovalNotificationsHelper approvalNotificationHelper = new ApprovalNotificationsHelper
				{
					Approval = newVersion,
					InternalCollaborators = internalCollaborators,
					ExternalCollaborators = externalCollaborators
				};
				approvalNotifications.Add(approvalNotificationHelper);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetSettingsFromPreviousVersion, ex);
			}
		}

		/// <summary>
		/// Gets Approval's Approved Date based on user's account Time settings
		/// </summary>
		/// <param name="approvedDate"></param>
		/// <param name="userCurTime"></param>
		/// <param name="timeZone"></param>
		/// <param name="dateFormat"></param>
		/// <returns></returns>
		public static string GetFormattedApprovedDate(DateTime approvedDate, DateTime userCurTime, string timeZone, string dateFormat)
		{
			StringBuilder html = new StringBuilder();

			approvedDate = GMGColorFormatData.GetUserTimeFromUTC(approvedDate, timeZone);
			int days = (approvedDate - userCurTime).Days;
			int hours = Math.Abs((approvedDate.Hour - userCurTime.Hour));

			html.Append("<span class=\"deadline \">" +
						  GMGColor.Resources.Resources.lblApprovedOnDate +
						 "<i rel=\"tooltip\" class=\"icon-time\" data-original-title=\"" + approvedDate.ToString("h:mm tt") + " (" + Math.Abs(days) + " " + GMGColor.Resources.Resources.lblDays + ", " + hours + " " + GMGColor.Resources.Resources.lblHours + ")" + "\"></i>" +
						  GMGColorFormatData.GetFormattedDate(approvedDate, dateFormat) +
						"</span>");

			return html.ToString();
		}

		public static List<Approval.ApprovalPageCountInfo> GetApprovalPagesCount(IEnumerable<int> approvalIds, DbContextBL context)
		{
			return (from ap in context.Approvals
					let pageCount = (from app in context.ApprovalPages
									 where app.Approval == ap.ID
									 select app.ID).Count()
					where approvalIds.Contains(ap.ID)
					select new Approval.ApprovalPageCountInfo
					{
						ID = ap.ID,
						PageCount = pageCount
					}).ToList();
		}

		public static ApprovalVersionCollaboratorInfo GetApprovalVersionCollaboratorInfo(int approvalVersionID, DbContextBL context)
		{
			ApprovalVersionCollaboratorInfo approvalVersionCollaboratorInfo = new ApprovalVersionCollaboratorInfo();
			approvalVersionCollaboratorInfo.LoadCollaborats(approvalVersionID, context);
			return approvalVersionCollaboratorInfo;
		}

		/// <summary>
		/// Returns a list of ids and roles of external collaborators
		/// </summary>
		/// <param name="approvalVersionID"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<string> GetApprovalVersionExternalCollaboratorInfo(int approvalVersionID, DbContextBL context)
		{
			return (from sha in context.SharedApprovals
					where sha.Approval == approvalVersionID
					select
						System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)sha.ExternalCollaborator).Trim() + "|" +
						System.Data.Entity.SqlServer.SqlFunctions.StringConvert((double)sha.ApprovalCollaboratorRole).Trim()
					).ToList();
		}

		/// <summary>
		/// Returns a list of collaborators and groups that have access permissions to the current folder
		/// </summary>
		/// <param name="folderID"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static ApprovalFolderCollaboratorInfo GetApprovalFolderCollaboratorInfo(int folderID, int loggedUserID, GMGColorDAL.Role.RoleName loggedUserCollaborateRole, DbContextBL context)
		{
			var approvalFolderCollaboratorInfo = new ApprovalFolderCollaboratorInfo();

			//check if logged user is Moderator or not
			if (loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountModerator)
			{
				approvalFolderCollaboratorInfo.LoadGroupCollaborators(folderID, loggedUserID, context);
			}
			else
			{
				approvalFolderCollaboratorInfo.LoadFolderCollaborators(folderID, context);
			}

			return approvalFolderCollaboratorInfo;
		}

		public static FolderCollaborator GetFolderCollaborator(int fodlerId, int userId, DbContextBL context)
		{
			return (from fc in context.FolderCollaborators
					where fc.Folder == fodlerId && fc.Collaborator == userId
					select fc).FirstOrDefault();
		}


        /// <summary>
        /// Determines if the approval file is valid based on upload engine settings
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static bool IsValidApprovalFileType(AccountFileTypeFilter filter, string extension)
		{
            bool isImage = GMGColorCommon.IsImageFileExtention(extension),
                isVideo = GMGColorCommon.IsVideoFile(extension),
                isPdf = extension == ".pdf",
                isSwf = extension == ".swf",
                isZip = extension == ".zip",
                isDoc = GMGColorCommon.IsDocumentFile(extension);


            bool isValidFile = isImage || isPdf || isSwf || isZip || isDoc;

			if (!filter.HasUploadSettings || !filter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval)
			{
				if (isValidFile || (isVideo && filter.PlanIncludesVideo))
				{
					return true;
				}
			}

			if (isSwf && !filter.UploadSettingsFilter.AllowSWF)
			{
				return false;
			}

			if (isVideo && !filter.UploadSettingsFilter.AllowVideo)
			{
				return false;
			}

			if (isPdf && !filter.UploadSettingsFilter.AllowPDF)
			{
				return false;
			}

			if (isImage && !filter.UploadSettingsFilter.AllowImage)
			{
				return false;
			}

			if (isValidFile || (isVideo && filter.PlanIncludesVideo))
			{
				return true;
			}

            if(isDoc && filter.UploadSettingsFilter.AllowDoc)
            {
                return true;
            }
			return false;
		}

		/// <summary>
		/// get max version from specific folder by job id
		/// </summary>
		/// <param name="jobId"></param>
		/// <param name="folderId"></param>
		/// <param name="loggedAccountId"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static bool? GetLastVersionSettingByJobId(int jobId, int folderId, DbContextBL context)
		{
			var approvalSettings = (from aps in context.ApprovalSettings
									let maxVersionId = (from ap in context.Approvals
														join j in context.Jobs on ap.Job equals j.ID

														where j.ID == jobId && ap.Folders.Select(f => f.ID).ToList().Contains(folderId) && !ap.IsDeleted
														orderby ap.Version descending
														select ap.ID
													   ).FirstOrDefault()
									where aps.Approval == maxVersionId
									select aps).FirstOrDefault();

			return approvalSettings != null && approvalSettings.UpdateJobName.GetValueOrDefault();
		}


		/// <summary>
		/// Gets SoftProofing Session guid for current parameters in case it exists
		/// </summary>
		/// <param name="pageId"></param>
		/// <param name="profileName"></param>
		/// <param name="activeChannels"></param>
		/// <param name="simulatePaperTint"></param>
		/// <param name="defaultProfile"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static SoftProofingSessionParam GetSoftProofingSessionByParams(TilesPageRequest model, DbContextBL context)
		{
			try
			{
				//TODO this method should not be called for RGB documents

				return (from sfses in context.SoftProofingSessionParams
						join ap in context.ApprovalPages on sfses.Approval equals ap.Approval
						join vwcn in context.ViewingConditions on sfses.ViewingCondition equals vwcn.ID
						where ap.ID == model.pageId && model.ActiveChannels == sfses.ActiveChannels &&
							   vwcn.SimulationProfile == model.SimulationProfileId &&
							   vwcn.EmbeddedProfile == model.EmbeddedProfileId &&
							   vwcn.CustomProfile == model.CustomProfileId &&
							   vwcn.PaperTint == model.PaperTintId &&
							   sfses.HighResolution == model.HighResolution
						select sfses).FirstOrDefault();
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGetSoftProofingSessionByParams, ex);
			}
		}

		/// <summary>
		/// Returns session aparams object from database by Session Guid
		/// </summary>
		/// <param name="sessionGuid"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static SoftProofingSessionParam GetSessionParamsBySessionGuid(string sessionGuid, DbContextBL context)
		{
			return context.SoftProofingSessionParams.FirstOrDefault(t => t.SessionGuid == sessionGuid);
		}

		public static void SetHighResSessionInProgress(DbContextBL context, int approvalId, bool highResInProgress)
		{
			var defaultSessionParam = context.SoftProofingSessionParams.FirstOrDefault(p => p.Approval == approvalId && p.DefaultSession);
			if (defaultSessionParam != null)
			{
				defaultSessionParam.HighResolutionInProgress = highResInProgress;
				context.SaveChanges();
			}
		}

		public static SoftProofingSessionParam GetDefaultSessionParamsByApprovalId(int approvalId, DbContextBL context)
		{
			return context.SoftProofingSessionParams.FirstOrDefault(t => t.Approval == approvalId && t.DefaultSession == true);
		}

		public static bool GetDefaultPaperTintSimulationParam()
		{
			return false;
		}

		public static string GetDefaultOutputIntentProfileName()
		{
			return GMGColorConfiguration.AppConfiguration.InputProfile;
		}

		/// <summary>
		/// Gets approval processing details for Tile Server
		/// </summary>
		/// <param name="pageId">Approval Page ID</param>
		/// <param name="context">Db context</param>
		/// <returns></returns>
		public static ApprovalPageProcessingDetails GetPageProcessingDetails(int pageId, int? embeddedProfileId, int? customProfileId, int? simulationProfileId, int? paperTintId, DbContextBL context)
		{
			var processingDetails =
						(from a in context.Approvals
						 join ap in context.ApprovalPages on a.ID equals ap.Approval
						 join j in context.Jobs on a.Job equals j.ID
						 join ac in context.Accounts on j.Account equals ac.ID
						 where ap.ID == pageId
						 select new ApprovalPageProcessingDetails
						 {
							 JobGuid = a.Guid,
							 ApprovalId = a.ID,
							 FileName = a.FileName,
							 Region = ac.Region,
							 PageNumber = ap.Number,
							 Resolution = ap.DPI,
							 PageWidth = ap.OutputRenderWidth.HasValue ? ap.OutputRenderWidth.Value : 0,
							 PageHeight = ap.OutputRenderHeight.HasValue ? ap.OutputRenderHeight.Value : 0,
							 PageChannels = ap.ApprovalSeparationPlates.ToList(),
							 DocumentEmbeddedProfile = embeddedProfileId.HasValue ? (from apep in context.ApprovalEmbeddedProfiles where apep.ID == embeddedProfileId select apep.Name).FirstOrDefault() : null,
							 CustomProfileDetails = customProfileId.HasValue ? (from acp in context.AccountCustomICCProfiles
																				where acp.ID == customProfileId
																				select new ApprovalPageProcessingDetails.CustomICCProfileDetails()
																				{
																					IsGlobal = acp.Account == null,
																					Name = acp.Name,
																					Guid = acp.Guid,
																					AccGuid = ac.Guid
																				}).FirstOrDefault()
																			 : new ApprovalPageProcessingDetails.CustomICCProfileDetails
																			 {
																				 IsGlobal = false,
																				 Name = "",
																				 Guid = "",
																				 AccGuid = ""
																			 },
							 SimulationProfileDetails = simulationProfileId.HasValue ? (from acp in context.SimulationProfiles
																						where acp.ID == simulationProfileId
																						select new ApprovalPageProcessingDetails.SimulationICCProfileDetails()
																						{
																							IsGlobal = acp.Account == null,
																							Name = acp.FileName,
																							Guid = acp.Guid,
																							AccGuid = ac.Guid,
																							MeasurementCond = acp.MeasurementCondition
																						}).FirstOrDefault()
																						 : new ApprovalPageProcessingDetails.SimulationICCProfileDetails
																						 {
																							 IsGlobal = false,
																							 Name = "",
																							 Guid = "",
																							 AccGuid = "",
																							 MeasurementCond = 0
																						 }

						 }
						).SingleOrDefault();

			//add paper tint value
			if (paperTintId.HasValue)
			{
				var paperTintMeasurements = (from pptm in context.PaperTintMeasurements
											 where pptm.PaperTint == paperTintId
											 select new
											 {
												 pptm.MeasurementCondition,
												 pptm.L,
												 pptm.a,
												 pptm.b
											 }).ToList();

				if (paperTintMeasurements.Count > 1 && simulationProfileId.HasValue)
				{
					var profileMatchingExtingCond =
						paperTintMeasurements.FirstOrDefault(
							t => t.MeasurementCondition == processingDetails.SimulationProfileDetails.MeasurementCond);

					if (profileMatchingExtingCond != null)
					{
						processingDetails.PaperTintWhite = new ApprovalPageProcessingDetails.PaperTintValues
						{
							L = profileMatchingExtingCond.L,
							a = profileMatchingExtingCond.a,
							b = profileMatchingExtingCond.b
						};
					}
				}

				//if no profile match found get first
				if (processingDetails.PaperTintWhite == null)
				{
					var firstPPt = paperTintMeasurements.FirstOrDefault();
					processingDetails.PaperTintWhite = new ApprovalPageProcessingDetails.PaperTintValues
					{
						L = firstPPt.L,
						a = firstPPt.a,
						b = firstPPt.b
					};
				}
			}

			return processingDetails;
		}

		/// <summary>
		/// Determines if the Current Document was rendered with rgb Profile
		/// </summary>
		/// <returns></returns>
		public static bool IsRGBDocument(List<GMGColorDAL.ApprovalSeparationPlate> pageChannels, DbContextBL context)
		{
			//check if current page is RGB (because all pages of the document are rendered with either CMYK or RGB)
			return pageChannels.Count == 3 &&
				   pageChannels.Any(t => t.Name.ToLower() == Constants.REDPLATE) &&
				   pageChannels.Any(t => t.Name.ToLower() == Constants.GREENPLATE) &&
				   pageChannels.Any(t => t.Name.ToLower() == Constants.BLUEPLATE);
		}

		/// <summary>
		/// Move selected folder/folders to specified location
		/// </summary>
		/// <param name="foldersIds">Ids of the selected folders to be moved</param>
		/// <param name="folderId">The id of the folder where selected folders should be moved</param>
		/// <param name="context">Database context</param>
		public static void MoveFoldersToFolder(int[] foldersIds, int folderId, int loggedUserId, GMGColorDAL.Role.RoleName loggedUserCollaborateRole, DbContextBL context)
		{
			try
			{
				using (var tscope = new TransactionScope())
				{
					foreach (var id in foldersIds)
					{
						var folder = (from f in context.Folders where f.ID == id select f).FirstOrDefault();

						//move fodler(s) only if logged user is owner of the folder or logged user has admin or manage collaborate roles
						if (folder != null && ((folder.Creator == loggedUserId && (loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountContributor || loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountModerator))
							 || loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountAdministrator || loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountManager))
						{
							folder.Parent = folderId;
						}
					}

					context.SaveChanges();
					tscope.Complete();
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotMoveFoldersToFolder, ex);
			}
		}

        /// <summary>
        /// Move selected jobs to the new location
        /// </summary>
        /// <param name="approvalsIds">Ids of the selected jobs to be moved</param>
        /// <param name="folderId">The id of the folder where selected jobs should be moved</param>
        /// <param name="context">Database context</param>
        public static void MoveApprovalsToFolder(int[] approvalsIds, int folderId, int loggedUserId, GMGColorDAL.Role.RoleName loggedUserCollaborateRole, int currentTab, DbContextBL context)
		{
            //TODO change the logic when fix will be made on CZ-904 
            try
            {
                StringBuilder query = new StringBuilder();

                query.Append(@"SELECT [Extent1].[ID] AS[ID], 
                                  [Extent1].[Name] AS[Name], 
                                  [Extent1].[Parent] AS[Parent], 
                                  [Extent1].[Account] AS[Account], 
                                  [Extent1].[Creator] AS[Creator], 
                                  [Extent1].[CreatedDate] AS[CreatedDate], 
                                  [Extent1].[Modifier] AS[Modifier], 
                                  [Extent1].[ModifiedDate] AS[ModifiedDate], 
                                  [Extent1].[IsDeleted] AS[IsDeleted], 
                                  [Extent1].[IsPrivate] AS[IsPrivate], 
                                  [Extent1].[AllCollaboratorsDecisionRequired]
                                        AS[AllCollaboratorsDecisionRequired], 
                                  [Extent1].[Guid] AS[Guid]
                                FROM [dbo].[Folder] AS[Extent1]
                                WHERE ([Extent1].[ID] = @folderId
                                AND[Extent1].[IsDeleted] <> 1)");

                var newFolder1 = context.Database.SqlQuery<Folder>(query.ToString(), new SqlParameter("folderId", folderId)).FirstOrDefault();

                var newFolder = (from f in context.Folders where f.ID == folderId && !f.IsDeleted select f).FirstOrDefault();
                foreach (var approvalId in approvalsIds)
                {
                    var approval = (from a in context.Approvals where a.ID == approvalId select a).FirstOrDefault();
                    //Get all approvals from this job
                    var approvals = (from a in context.Approvals where a.Job == approval.Job && a.IsDeleted == false select a);

                    var isNotJobOwner = approvals.Any(o => o.Owner != loggedUserId);

                    //move approval only if logged user is owner of the approval or logged user has admin or manage collaborate roles
                    if (currentTab != -2 ||
                        (!isNotJobOwner && currentTab == -2 && (loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountContributor || loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountModerator)) ||
                        loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountAdministrator || loggedUserCollaborateRole == GMGColorDAL.Role.RoleName.AccountManager)
                    {
                        foreach (var app in approvals)
                        {

                            if (app.Folders.Count > 0)
                            {
                                foreach (var folder in app.Folders.ToList())
                                {
                                    app.Folders.Remove(folder);
                                }
                            }
                            app.Folders.Add(newFolder);
                        }
                    }
                }
                using (var tscope = new TransactionScope())
                {
                    context.SaveChanges();
                    tscope.Complete();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotMoveJobsToFolder, ex);
            }
		}

		public static void DeleteGarbageApprovalFolders(DbContextBL context)
		{
			try
			{
				List<string> approvalFolders = AWSSimpleStorageService.GetAllFolders(
						GMGColorConfiguration.AppConfiguration.DataFolderName(GMGColorConfiguration.AppConfiguration.AWSRegion),
						GMGColorConfiguration.AppConfiguration.AWSAccessKey,
						GMGColorConfiguration.AppConfiguration.AWSSecretKey, "approvals/");

				foreach (string approvalFolder in approvalFolders)
				{
					string approvalGuid = Path.GetFileName(approvalFolder.Remove(approvalFolder.Length - 1));
					if (!context.Approvals.Any(t => t.Guid == approvalGuid))
					{
						GMGColorIO.DeleteFolder(approvalFolder, GMGColorConfiguration.AppConfiguration.AWSRegion);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		/// <summary>
		/// Returns a list of ids only for approvals that has annotations within current folder or childs folders
		/// </summary>
		/// <param name="fodlersIds">Folder(s) id(s)</param>
		/// <param name="loggedUser">The logged user's id</param>
		/// <param name="context">Database context</param>
		/// <param name="approvalsIds">A list of approvals ids that are added at each folder level, if any</param>
		/// <param name="isTopFolder">This is "true" only for the folders that were selected from interface</param>
		/// <returns></returns>
		public static void GetAllApprovalsIdsFromFolder(int[] fodlersIds, ref List<int> approvalsIds, int loggedUser, DbContextBL context, bool isTopFolder = false)
		{
			foreach (var folder in fodlersIds)
			{
				//Get approvals from current folder
				var foldersApprovals = (from f in context.Folders
										where f.ID == folder && (f.FolderCollaborators.Select(m => m.Collaborator).Contains(loggedUser) || f.FolderCollaboratorGroups.Select(m => m.CollaboratorGroup).Contains(loggedUser))
										select f.Approvals.Where(a => a.ApprovalCollaborators.Any(ac => ac.Collaborator == loggedUser) && !a.IsDeleted && a.ApprovalType.Key != (int)Approval.ApprovalTypeEnum.Movie)
														  .GroupBy(a => a.Job)
														  .Select(o => new { Approval = o.OrderByDescending(i => i.Version).FirstOrDefault() }))
														  .ToList();
				//Add to list only ids of approvals that have annotations on last version
				foreach (var folderApprovals in foldersApprovals)
				{
					approvalsIds.AddRange(from f in folderApprovals where f.Approval.ApprovalPages.Any(ap => ap.ApprovalAnnotations.Count > 0) select f.Approval.ID);
				}

				var parent = (from f in context.Folders where f.ID == folder select f).FirstOrDefault();

				if (parent != null)
				{
					//check whether the logged user has access to the sub-folders
					var userHasAccessToChildsFolders = parent.Folder1.Any(child => child.FolderCollaborators.Select(m => m.Collaborator).Contains(loggedUser) || child.FolderCollaboratorGroups.Select(m => m.CollaboratorGroup).Contains(loggedUser));

					//check whether the selected folder is empty (no approvals or sub-folders) 
					//if so, then stop searching in child folders
					if (isTopFolder && !userHasAccessToChildsFolders && !approvalsIds.Any())
					{
						continue;
					}

					//Get approvals from child folders of the current folder
					foreach (var folderChild in parent.Folder1)
					{
						GetAllApprovalsIdsFromFolder(new int[] { folderChild.ID }, ref approvalsIds, loggedUser, context);
					}
				}
			}
		}

		public static Folder GetFolderById(int selectedFolder, DbContextBL context)
		{
			return (from f in context.Folders
					where f.ID == selectedFolder
					select f).FirstOrDefault();
		}

		/// <summary>
		/// Updates the ApprovalDeleteHistory for the sepecified approval by adding or removing the coresponding entry
		/// </summary>
		/// <param name="approvalId"></param>
		/// <param name="userId"></param>
		/// <param name="action"></param>
		/// <param name="context"></param>
		public static void ApprovalUserRecycleBinHistory(Approval approval, int userId, bool deleteAction, DbContextBL context)
		{
			if (deleteAction)
			{
				ApprovalUserRecycleBinHistory appDelHistory = new ApprovalUserRecycleBinHistory();
				appDelHistory.Approval = approval.ID;
				appDelHistory.User = userId;
				context.ApprovalUserRecycleBinHistories.Add(appDelHistory);
			}
			else // Restore
			{
				List<ApprovalUserRecycleBinHistory> appDeleteHistory = context.ApprovalUserRecycleBinHistories.Where(ah => ah.Approval == approval.ID).ToList();
				context.ApprovalUserRecycleBinHistories.RemoveRange(appDeleteHistory);
			}
		}

		public static ApprovalCollaborator GetApprovalCollaborator(int approvalId, int collaboratorId, DbContextBL context)
		{
			return (from ac in context.ApprovalCollaborators
					where ac.Approval == approvalId && ac.Collaborator == collaboratorId
					select ac).FirstOrDefault();
		}

        public static List<ApprovalDetailsModel.ApprovalCollaborateSendReminderInfo> GetApprovalCollaborators(int approvalId, Account LoggedAccount ,   DbContextBL context)
        {
            var res = new List<ApprovalDetailsModel.ApprovalCollaborateSendReminderInfo>();


            var approvalColaborators = (from ac in context.ApprovalCollaborators
                               where ac.Approval == approvalId
                               select ac).ToList();

            var sharedApprovalCollaborators = (from sa in context.SharedApprovals
                                        where sa.Approval == approvalId
                                        select sa).ToList();

            foreach (var col in approvalColaborators)
            {
                DateTime UserDateTime = DateTime.UtcNow;
                if (col.MailSentDate != null)
                {
                    UserDateTime = GMGColorFormatData.GetUserTimeFromUTC(col.MailSentDate.Value, LoggedAccount.TimeZone);
                }
                var approvalCollaborater = new ApprovalDetailsModel.ApprovalCollaborateSendReminderInfo();
                approvalCollaborater.Collaborator = col.Collaborator;
                approvalCollaborater.IsReminderMailSent = col.IsReminderMailSent;
                approvalCollaborater.MailSentDate =col.MailSentDate != null ? GMGColorFormatData.GetUserFormattedDate(UserDateTime, LoggedAccount.DateFormat1.Pattern)+ " " + GMGColorFormatData.GetUserFormattedTime(UserDateTime, LoggedAccount.TimeFormat) : string.Empty;
                approvalCollaborater.IsExternalUser = false;
                approvalCollaborater.PhaseId = col.Phase != null ? col.Phase : 0;
                res.Add(approvalCollaborater);
            }

            if (sharedApprovalCollaborators != null)
            {
                foreach (var scol in sharedApprovalCollaborators)
                {
                    DateTime UserDateTime = DateTime.UtcNow;
                    if (scol.MailSentDate != null)
                    {
                        UserDateTime = GMGColorFormatData.GetUserTimeFromUTC(scol.MailSentDate.Value, LoggedAccount.TimeZone);
                    }
                    var sharedApprovalCollaborator = new ApprovalDetailsModel.ApprovalCollaborateSendReminderInfo();
                    sharedApprovalCollaborator.Collaborator = scol.ExternalCollaborator;
                    sharedApprovalCollaborator.IsReminderMailSent = scol.IsReminderMailSent;
                    sharedApprovalCollaborator.MailSentDate = scol.MailSentDate != null ? GMGColorFormatData.GetUserFormattedDate(UserDateTime, LoggedAccount.DateFormat1.Pattern) + " " + GMGColorFormatData.GetUserFormattedTime(UserDateTime, LoggedAccount.TimeFormat) : string.Empty;
                    sharedApprovalCollaborator.IsExternalUser = true;
                    sharedApprovalCollaborator.PhaseId = scol.Phase != null ? scol.Phase : 0;
                    res.Add(sharedApprovalCollaborator);
                }
                
            }
            return res;
        }


        public static List<ApprovalCollaborator> GetListOfApprovalCollaborator(List<int> approvalId, int collaboratorId, DbContextBL context)
       {
          return (from ac in context.ApprovalCollaborators
                  join a in context.Approvals on ac.Approval equals a.ID
                   where approvalId.Contains(ac.Approval) && ac.Collaborator == collaboratorId
                   && a.Owner != collaboratorId
                  select ac).ToList();
       }


        // <summary>
        /// Determines if the current annotation is ready for job changes complete status, when a retucher makes a decision on annotation status
        /// </summary>
        /// <param name="annotationId">Annotation DB Id</param>
        /// <param name="userGuid">Retoucher Guid</param>
        /// <param name="context">Database Context</param>
        /// <returns>True if the conditions are met, false otherwise</returns>
        public static bool IsAnnotationChangesComplete(int approvalId, string userGuid, DbContextBL context)
		{
			bool isuserRetoucher = (from ac in context.Accounts
									join u in context.Users on ac.ID equals u.Account
									join ur in context.UserRoles on u.ID equals ur.User
									join r in context.Roles on ur.Role equals r.ID
									where u.Guid == userGuid && r.Key == "RET"
									select r.ID).Any();

			//if current user is not a retoucher return false
			if (!isuserRetoucher)
			{
				return false;
			}

			//check if all annotations that belong to current approval have approved status
			return (from a in context.Approvals
					join ap in context.ApprovalPages on a.ID equals ap.Approval
					join an in context.ApprovalAnnotations on ap.ID equals an.Page
					join ans in context.ApprovalAnnotationStatus on an.Status equals ans.ID
					where a.ID == approvalId && an.Parent == null
					select ans.Key).All(t => t == "A");
		}

		public static int GetFileVersion(string fileName, int[] versions)
		{
			var fileNameWithoutExtension = GetFileNameWithoutExtension(fileName);
			if (fileNameWithoutExtension != null)
			{
				var fName = fileNameWithoutExtension.ToLower();
				var vIndex = fName.LastIndexOf("_v", StringComparison.Ordinal);

				//Get filename version number
				if (vIndex > 0)
				{
					int vNumber;
					Int32.TryParse(fName.Substring(vIndex + 2), out vNumber);

					if (versions.All(t => t < vNumber))
					{
						return vNumber;
					}
				}
			}
			return versions.Any() ? (versions.Max() + 1) : 1;
		}

		/// <summary>
		/// Returns upload file types allowed
		/// When "Apply upload settings for Collaborate batch New Approval" option from Upload Engine -> Settings page is checked, returns  only the selected file types 
		/// otherwise, returns all file types  allowed 
		/// </summary>
		/// <param name="approvalBatchUploadSettings"></param>
		/// <returns></returns>
		public static string GetAllowedUploadFileTypes(AccountFileTypeFilter approvalBatchUploadSettings)
		{
			var typesAllowed = GMGColor.Resources.Resources.lblYouCanUploadFileTypes;
			bool applyForApproval = approvalBatchUploadSettings.HasUploadSettings &&
									approvalBatchUploadSettings.UploadSettingsFilter.ApplyUploadSettingsForNewApproval;

			bool anyFileAllowed = false;
			if (!applyForApproval || approvalBatchUploadSettings.UploadSettingsFilter.AllowPDF)
			{
				typesAllowed += " " + GMGColor.Resources.Resources.lblUploadSettingsPDFFileType + ",";
				anyFileAllowed = true;
			}
            if(!applyForApproval || approvalBatchUploadSettings.UploadSettingsFilter.AllowDoc)
            {
                typesAllowed += " " + GMGColor.Resources.Resources.lblUploadSettingsDocumentFileTypes + ",";
                anyFileAllowed = true;
            }

			if (!applyForApproval || approvalBatchUploadSettings.UploadSettingsFilter.AllowImage)
			{
				typesAllowed += " " + GMGColor.Resources.Resources.lblUploadSettingsImageFileType + ",";
				anyFileAllowed = true;
			}

            if (approvalBatchUploadSettings.PlanIncludesVideo && (!applyForApproval || approvalBatchUploadSettings.UploadSettingsFilter.AllowVideo))
			{
				typesAllowed += " " + GMGColor.Resources.Resources.lblUploadSettingsVideoFileType + ",";
				anyFileAllowed = true;
			}
            if (!applyForApproval || approvalBatchUploadSettings.UploadSettingsFilter.AllowZip)
            {
                typesAllowed += " " + GMGColor.Resources.Resources.lblUploadSettingsZipFileType + "(Html)" + ",";
                anyFileAllowed = true;
            }

            //remove last ',' 
            var idx = typesAllowed.LastIndexOf(",", System.StringComparison.Ordinal);
			if (idx > 0)
			{
				typesAllowed = typesAllowed.Remove(idx, 1);
			}

			//if any, get index of the last ',' in the string and replace it with 'or' word
			var index = typesAllowed.LastIndexOf(",", System.StringComparison.Ordinal);
			if (index > 0)
			{
				typesAllowed = typesAllowed.Remove(index, 1).Insert(index, " " + GMGColor.Resources.Resources.lblOr);
			}

			return anyFileAllowed ? typesAllowed : GMGColor.Resources.Resources.lblCannotUploadFileType;
		}

        public static bool IsDisableTagwordsInDashbord(int Account , DbContextBL context)
        {
            var value = (from acs in context.AccountSettings
                                where acs.Account == Account && acs.Name == "DisableTagwordsInDashbord"
                                select acs.Value).FirstOrDefault();
            if(value != null )
            {
                if(value == "True")
                {
                    return true;
                }
            }
            return false;
        }

        public static ApprovalWorkflowPhasesInfo GetApprovalWorkflowCollaborators(int workflowId, DbContextBL context)
		{
			var aprovalPhaseCollaboratorInfo = new ApprovalWorkflowPhasesInfo();

			if (workflowId > 0)
			{
				var phases = (from aw in context.ApprovalWorkflows
							  join ap in context.ApprovalPhases on aw.ID equals ap.ApprovalWorkflow
							  where aw.ID == workflowId
							  orderby ap.Position
							  select ap).ToList();
				foreach (var phase in phases)
				{
					var collaboratorsInfo = new ApprovalPhaseCollaboratorInfo();
					collaboratorsInfo.LoadPhaseCollaborats(phase.ID, context);

					int? pdm = null;
					bool isExternal = false;
					if (phase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
					{
						pdm = phase.PrimaryDecisionMaker ?? phase.ExternalPrimaryDecisionMaker;
						isExternal = phase.ExternalPrimaryDecisionMaker != null;
					}

					aprovalPhaseCollaboratorInfo.ApprovalWorkflowPhases.Add(new ApprovalWorkflowPhase
					{
						Phase = phase.ID,
						PDMId = pdm,
						DecisionType = phase.DecisionType,
						IsExternal = isExternal,
						PhaseCollaborators = collaboratorsInfo
					});
				}
			}

			return aprovalPhaseCollaboratorInfo;
		}

		public static List<ApprovalWorkflowDetails> GetApprovalWorkflowPhases(int workflowId, DbContextBL context)
		{
			if (workflowId > 0)
			{
				return (from aw in context.ApprovalWorkflows
						join ap in context.ApprovalPhases on aw.ID equals ap.ApprovalWorkflow
						where aw.ID == workflowId
						orderby ap.Position
						select new ApprovalWorkflowDetails()
						{
							WorkflowId = workflowId,
							PhaseId = ap.ID,
							PhaseName = ap.Name
						}).ToList();
			}

			return null;
		}

		public static List<ApprovalWorkflowDetails> GetApprovalJobPhases(int workflowId, int currentPhasePosition, DbContextBL context)
		{
			if (workflowId > 0)
			{
				return (from aw in context.ApprovalJobWorkflows
						join ap in context.ApprovalJobPhases on aw.ID equals ap.ApprovalJobWorkflow
						where aw.ID == workflowId && ap.Position >= currentPhasePosition
						orderby ap.Position
						select new ApprovalWorkflowDetails()
						{
							WorkflowId = workflowId,
							PhaseId = ap.ID,
							PhaseName = ap.Name
						}).ToList();
			}

			return null;
		}

		public static int AddNewApproval(BillingPlan collaboratePlan, Account loggedAccount, GMGColorDAL.User loggedUser, NewApprovalModel model, List<ApprovalJob> files, int? webPageSnapshotDelay, ApprovalWorkflowPhasesInfo phasesCollaborators, DbContextBL context, out NewApprovalResponse newApprovalResponse, bool isCollaborateApi = false, List<GMGColorDAL.ApprovalPage> approvalPageList = null)
		{
                //new approvals added in this upload sessions
                var newApprovals = new List<Approval>();
                newApprovalResponse = new NewApprovalResponse();
                DateTime deadlineDate = default(DateTime);
                var duplicatedFiles = string.Empty;
                var ignoreList = string.Empty;
                var newPhases = new Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>>();

                int folderId = 0;
                string submitGroupKey = files.Count() >= 2 ? Guid.NewGuid().ToString() : null;

                if (model.IsDueDateSelected)
                {
                    deadlineDate = GetFormatedApprovalDeadline(model.ApprovalDeadlineDate, model.DeadlineTime,
                        model.DeadlineTimeMeridiem, loggedAccount.DateFormat1.Pattern, loggedAccount.TimeFormat);
                }

                DateTime deadLineDate = model.IsDueDateSelected
                    ? GMGColorFormatData.SetUserTimeToUTC(deadlineDate, loggedAccount.TimeZone)
                    : deadlineDate;

                int newJobStatusID = JobStatu.GetJobStatusId(JobStatu.Status.New, context);
                var duplicateFileKey = loggedAccount.UploadEngineAccountSetting != null
                    ? loggedAccount.UploadEngineAccountSetting.UploadEngineDuplicateFileName.Key
                    : -1;
                var canApplySettings = loggedAccount.UploadEngineAccountSetting != null &&
                                       loggedAccount.UploadEngineAccountSetting.ApplyUploadSettingsForNewApproval;
                var usePrevVersionSettings = canApplySettings && loggedAccount.UploadEngineAccountSetting
                                                 .UseUploadSettingsFromPreviousVersion;
                var enableVersionMirroring = false;

                if (canApplySettings && loggedAccount.UploadEngineAccountSetting.EnableVersionMirroring)
                {
                    files = files.OrderBy(f => f.FileName).ToList();
                }

                var internalCollaborators = new List<int>(); //internal collaborators set for this upload session
                var sharedApprovals = new List<SharedApproval>(); //external collaborators set for this upload session

                AccountFileTypeFilter fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(loggedUser.ID, context);
                int approverAndReviewerRoleID = GetApprovalRoleID(ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer,
                    context);
                var jobSource = isCollaborateApi
                    ? JobBL.GetJobSourceByKey(GMG.CoZone.Common.JobSource.WebService, context)
                    : JobBL.GetJobSourceByKey(GMG.CoZone.Common.JobSource.WebUI, context);

                Folder objFolder = null;
                if (model.Folder > 0)
                {
                    objFolder = context.Folders.FirstOrDefault(f => f.ID == model.Folder);
                }

                var fileTypeIdsAndExtensions = FileType.GetFileTypeIdsAndExtensions(context);

                List<ApprovalNotificationsHelper> approvalNotifications = new List<ApprovalNotificationsHelper>();


                try
                {
                    context.Configuration.AutoDetectChangesEnabled = false;

                    for (int index = 0; index < files.Count(); index++)
                    {
                        ApprovalJob file = files[index];

                        Job objJob = null;

                        string filenameWithoutExtension = GetFileNameWithoutExtension(file.FileName);

                        // first check if the file is valid
                        var fileExtension = FileType.GetFileExtention(file.FileName).ToString().ToLower();
                        var fileType = fileTypeIdsAndExtensions.FirstOrDefault(kvp => kvp.Extension == fileExtension);

                        if (file.ApprovalType == 0 && fileExtension == "zip")
                        {
                            file.ApprovalType = 5;
                        }

                        string extension = fileType.Extension.StartsWith(".")
                            ? fileType.Extension
                            : "." + fileType.Extension;

                        if (!IsValidApprovalFileType(fileTypeFilter, extension))
                        {
                            continue;
                        }

                    if (model.Job > 0)
                    {
                       objJob = DALUtils.GetObject<Job>(model.Job, context);
                        if (model.KeepOriginalFileName)
                        {
                            if (file.FileTitle != null)
                            {
                                objJob.Title = file.FileTitle;
                            }
                            else
                            {
                                objJob.Title = filenameWithoutExtension;
                            }
                        }
                        else
                        {
                            objJob.Title = file.FileTitle;
                        }
                    }
                    //check whether the folder is selected and 
                    // "Apply upload settings for Collaborate batch New Approval" option from Upload Setting is selected
                    else if (model.Folder > 0 && canApplySettings)
                        {
                            ApprovalFileStatus approvalFileStatus = NewFileStatus(loggedAccount.ID, file.FileName,
                                model.Folder,
                                (FileUploadDuplicateName)duplicateFileKey,
                                out objJob, context);
                            switch (approvalFileStatus)
                            {
                                case ApprovalFileStatus.NewVersion:
                                    {
                                        enableVersionMirroring =
                                            loggedAccount.UploadEngineAccountSetting.EnableVersionMirroring;
                                        if (usePrevVersionSettings)
                                        {
                                            //create versions using settings from previous version
                                            Approval newVersion = new Approval();
                                            newVersion.Deadline = deadLineDate;
                                            newVersion.Guid = file.FileGuid;
                                            newVersion.Creator = loggedUser.ID;
                                            newVersion.CreatedDate = DateTime.UtcNow;
                                            newVersion.Modifier = loggedUser.ID;
                                            newVersion.ModifiedDate = DateTime.UtcNow;
                                            newVersion.IsDeleted = false;
                                            newVersion.FileName = Path.GetFileName(file.FileName);
                                            newVersion.FileType = fileType.ID;
                                            newVersion.Size = file.FileSize;
                                            newVersion.FolderPath = String.Empty;
                                            newVersion.VersionSufix = String.Empty;


                                            newVersion.Folders.Add(objFolder);
                                            folderId = objFolder.ID;

                                            newVersion.Type = GetFileType(model.IsUploadApproval, fileType.MediaType, context);

                                            context.Approvals.Add(newVersion);

                                            bool? keepJobName = null;
                                            if (loggedAccount.ID == objJob.Account)
                                            {
                                                keepJobName = GetLastVersionSettingByJobId(objJob.ID, model.Folder, context);
                                            }

                                        if (!keepJobName.GetValueOrDefault())
                                        {
                                            objJob.Title = filenameWithoutExtension;
                                        }

                                        var newVersionSetting = new ApprovalSetting()
                                            {
                                                Approval1 = newVersion,
                                                UpdateJobName = keepJobName ?? !model.KeepOriginalFileName
                                            };

                                            objJob.Status = newJobStatusID;
                                            context.ApprovalSettings.Add(newVersionSetting);

                                            SetSettingsFromPreviousVersion(newVersion, objJob, loggedUser, enableVersionMirroring,
                                                approverAndReviewerRoleID, jobSource,
                                                approvalNotifications, context);

                                            continue;
                                        }
                                    }
                                    break;
                                case ApprovalFileStatus.DuplicatedFile:
                                    {
                                        duplicatedFiles += duplicatedFiles.Length > 0
                                            ? ", " + file.FileName
                                            : file.FileName;
                                        continue;
                                    }
                                case ApprovalFileStatus.IgnoredFile:
                                    {
                                        ignoreList += ignoreList.Length > 0
                                            ? ", " + file.FileName
                                            : file.FileName;
                                        continue;
                                    }
                            }
                        }

                        bool? keepOriginalJobName = null;
                        if (objJob == null)
                        {
                            objJob = new Job();
                            objJob.Title = model.IsUploadApproval && model.KeepOriginalFileName ? filenameWithoutExtension : file.FileTitle;
                            objJob.Account = loggedAccount.ID;
                            objJob.ModifiedDate = DateTime.UtcNow;
                            objJob.Guid = Guid.NewGuid().ToString();
                        }
                        else
                        {
                            if (model.Folder > 0 && canApplySettings)
                            {
                                keepOriginalJobName = GetLastVersionSettingByJobId(objJob.ID, model.Folder, context);

                            if (!keepOriginalJobName.GetValueOrDefault())
                            {
                                objJob.Title = filenameWithoutExtension;
                            }
                        }
                        }
                        Approval objApproval = new Approval();
                        objApproval.SubmitGroupKey = submitGroupKey;

                        objApproval.ScreenshotUrl = model.Url;
                        objApproval.WebPageSnapshotDelay = webPageSnapshotDelay;

                        objApproval.Deadline = deadLineDate;
                        objApproval.Guid = file.FileGuid;
                        objApproval.Creator = loggedUser.ID;
                        objApproval.CreatedDate = DateTime.UtcNow;
                        objApproval.Modifier = loggedUser.ID;
                        objApproval.ModifiedDate = DateTime.UtcNow;
                        objApproval.IsDeleted = false;
                        int[] versions = objJob.Approvals.Select(t => t.Version).ToArray();
                        objApproval.Version = enableVersionMirroring
                            ? GetFileVersion(file.FileName, versions)
                            : versions.Any()
                                ? (versions.Max() + 1)
                                : 1;
                        if (isCollaborateApi)
                        {
                            newApprovalResponse.JobGuid = objJob.Guid;
                            newApprovalResponse.Version = objApproval.Version;
                            objApproval.ProcessingInProgress = "completed";
                        }

                        objApproval.AllowDownloadOriginal = model.AllowUsersToDownload;
                        objApproval.OnlyOneDecisionRequired = model.OnlyOneDecisionRequired;
                        objApproval.PrivateAnnotations = model.OnlyOneDecisionRequired ? model.PrivateAnnotations : false;
                        objApproval.LockProofWhenAllDecisionsMade = model.LockProofWhenAllDecisionsMade;
                        objApproval.LockProofWhenFirstDecisionsMade = model.LockProofWhenFirstDecisionsMade;
                        objApproval.IsPageSpreads = model.IsPageSpreads;
                        objApproval.JobSource = jobSource;
                        objApproval.AllowOtherUsersToBeAdded = model.AllowOtherUsersToBeAdded;
                        objApproval.VersionSufix = string.Empty;
                        objApproval.RestrictedZoom = model.RestrictedZoom;

                        if (model.ChecklistId > 0)
                        {
                            objApproval.Checklist = model.ChecklistId;
                        }
                        else
                        {
                            objApproval.Checklist = null;
                        }
                        if (file.ApprovalType > 0)
                        {
                            objApproval.ApprovalTypeColour = file.ApprovalType;
                        }
                        else
                        {
                            objApproval.ApprovalTypeColour = 1;
                        }

                        objApproval.Owner = model.Owner;
                        if (objApproval.OnlyOneDecisionRequired)
                        {
                            int pdm = 0;
                            if (Int32.TryParse(model.PrimaryDecisionMaker, out pdm))
                            {
                                objApproval.PrimaryDecisionMaker = pdm;
                            }
                        }

                        Folder folder = null;
                        if (model.Job > 0)
                        {
                            if (objJob.Approvals.FirstOrDefault().Folders.Count == 1)
                            {
                                var id = objJob.Approvals.FirstOrDefault().Folders.FirstOrDefault().ID;
                                folder = context.Folders.FirstOrDefault(f => f.ID == id);
                            }
                        }
                        else
                        {
                            folder = objFolder;
                        }

                        if (folder != null)
                        {
                            objApproval.Folders.Add(folder);
                            folderId = folder.ID;
                        }

                        objApproval.FileName = Path.GetFileName(file.FileName).Replace(".docx", ".pdf").Replace(".doc", ".pdf").Replace(".pptx", ".pdf").Replace(".ppt", ".pdf");
                        objApproval.FileType = fileType.ID;

                        PlansBL.LogApprovalSizeUsage(collaboratePlan, loggedAccount, file.FileSize, context);

                        objApproval.Size = file.FileSize;

                        objApproval.Type = GetFileType(model.IsUploadApproval, fileType.MediaType, context);

                        //lock previous versions
                        if (objApproval.Version > 1 && objJob != null)
                        {
                            foreach (Approval version in objJob.Approvals)
                            {
                                version.IsLocked = true;
                            }
                        }

                        objApproval.FolderPath = string.Empty;

                        //save owner if approval has adhoc workflow
                        if (model.HasAdHocWorkflow)
                        {
                            objApproval.Owner = loggedUser.ID;
                        }

                        objJob.Approvals.Add(objApproval);
                        objJob.Status = newJobStatusID;

                        if (objJob.ID == 0)
                        {
                            context.Jobs.Add(objJob);
                        }
                        //objApproval.ProcessingInProgress = "completed";
                        context.Approvals.Add(objApproval);

                        if (model.ApprovalWorkflowId > 0)
                        {
                            var deadlineTriggers = (from fdt in context.PhaseDeadlineTriggers select fdt).ToList();
                            var deadlineUnits = (from fdu in context.PhaseDeadlineUnits select fdu).ToList();

                            if (objJob.ID == 0)
                            {
                                var selectedWorkflow = context.ApprovalWorkflows.Where(w => w.ID == model.ApprovalWorkflowId)
                                    .Select(n => new { n.Name, n.RerunWorkflow }).FirstOrDefault();
                                objJob.JobOwner = loggedUser.ID;
                                var approvalJobWorkflow = new ApprovalJobWorkflow()
                                {
                                    Name = selectedWorkflow.Name,
                                    Account = loggedAccount.ID,
                                    Job1 = objJob,
                                    CreatedDate = DateTime.UtcNow,
                                    ModifiedDate = DateTime.UtcNow,
                                    CreatedBy = loggedUser.ID,
                                    ModifiedBy = loggedUser.ID,
                                    RerunWorkflow = selectedWorkflow.RerunWorkflow
                                };

                                context.ApprovalJobWorkflows.Add(approvalJobWorkflow);
                                var phases = new List<ApprovalJobPhase>();

                                bool isfirstPhase = true;
                                foreach (var phase in phasesCollaborators.ApprovalWorkflowPhases)
                                {
                                    //add default phase template details except collaborators
                                    var phaseTemplate =
                                        (from ap in context.ApprovalPhases where ap.ID == phase.Phase select ap)
                                        .FirstOrDefault();

                                    if (phaseTemplate != null)
                                    {
                                        var approvalJobPhase = new ApprovalJobPhase()
                                        {
                                            Name = phaseTemplate.Name,
                                            ApprovalJobWorkflow1 = approvalJobWorkflow,
                                            Position = phaseTemplate.Position,
                                            DecisionType = phaseTemplate.DecisionType,
                                            ApprovalTrigger = phaseTemplate.ApprovalTrigger,
                                            ShowAnnotationsToUsersOfOtherPhases = phaseTemplate.ShowAnnotationsToUsersOfOtherPhases,
                                            ShowAnnotationsOfOtherPhasesToExternalUsers = phaseTemplate.ShowAnnotationsOfOtherPhasesToExternalUsers,
                                            PhaseTemplateID = phaseTemplate.ID,
                                            Deadline = phaseTemplate.Deadline,
                                            DeadlineTrigger = phaseTemplate.DeadlineTrigger,
                                            DeadlineUnitNumber = phaseTemplate.DeadlineUnitNumber,
                                            DeadlineUnit = phaseTemplate.DeadlineUnit,
                                            IsActive = true,
                                            CreatedDate = DateTime.UtcNow,
                                            ModifiedDate = DateTime.UtcNow,
                                            ModifiedBy = loggedUser.ID,
                                            CreatedBy = loggedUser.ID
                                        };

                                        if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                        {
                                            if (phase.IsExternal)
                                            {
                                                approvalJobPhase.PrimaryDecisionMaker = null;
                                                approvalJobPhase.ExternalPrimaryDecisionMaker = phase.PDMId ??
                                                                                                phaseTemplate
                                                                                                    .ExternalPrimaryDecisionMaker;
                                            }
                                            else
                                            {
                                                approvalJobPhase.ExternalPrimaryDecisionMaker = null;
                                                approvalJobPhase.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                                            }
                                        }

                                        //set automatic phase deadline  
                                        if (phaseTemplate.DeadlineTrigger != null && phaseTemplate.DeadlineUnitNumber != null &&
                                            phaseTemplate.DeadlineUnit != null)
                                        {
                                            var deadlineTrigger = deadlineTriggers.Where(t => phaseTemplate.DeadlineTrigger == t.ID).Select(t => t.Key)
                                                .FirstOrDefault();
                                            var deadlineUnit = deadlineUnits.Where(u => phaseTemplate.DeadlineUnit == u.ID).Select(u => u.Key)
                                                .FirstOrDefault();

                                            if (isfirstPhase || deadlineTrigger != (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
                                            {
                                                approvalJobPhase.Deadline = GetAutomaticPhaseDeadline(deadLineDate, deadlineTrigger,
                                                    phaseTemplate.DeadlineUnitNumber, deadlineUnit);
                                            }
                                        }

                                        context.ApprovalJobPhases.Add(approvalJobPhase);

                                        if (isfirstPhase)
                                        {
                                            if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                            {
                                                if (phase.IsExternal)
                                                {
                                                    objApproval.ExternalPrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.ExternalPrimaryDecisionMaker;
                                                    objApproval.PrimaryDecisionMaker = null;
                                                }
                                                else
                                                {
                                                    objApproval.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                                                    objApproval.ExternalPrimaryDecisionMaker = null;
                                                }
                                            }
                                            objApproval.ApprovalJobPhase = approvalJobPhase;

                                            //add relations between each workflow phase and current approval
                                            var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                                            {
                                                Approval1 = objApproval,
                                                ApprovalJobPhase = approvalJobPhase,
                                                PhaseMarkedAsCompleted = false,
                                                CreatedBy = loggedUser.ID,
                                                ModifiedBy = loggedUser.ID,
                                                CreatedDate = DateTime.UtcNow,
                                                ModifiedDate = DateTime.UtcNow
                                            };
                                            objApproval.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);
                                            isfirstPhase = false;
                                        }

                                        phases.Add(approvalJobPhase);
                                    }
                                }

                                newPhases.Add(approvalJobWorkflow, phases);
                            }
                            else
                            {
                                var phase = phasesCollaborators.ApprovalWorkflowPhases[0];
                                var phases = (from ajp in context.ApprovalJobPhases
                                              join ajw in context.ApprovalJobWorkflows on ajp.ApprovalJobWorkflow equals ajw.ID
                                              let currentPhasePosition = (from ajpp in context.ApprovalJobPhases
                                                                          where ajpp.ID == phase.Phase
                                                                          select ajpp.Position).FirstOrDefault()

                                              where ajw.Job == objJob.ID && ajp.Position >= currentPhasePosition
                                              select ajp).ToList();

                                var existingWorkflow = context.ApprovalJobWorkflows.FirstOrDefault(w => w.Job == objJob.ID);
                                newPhases.Add(existingWorkflow, phases);
                                foreach (var approvalJobPhase in phases)
                                {
                                    //if phase decision type is PDM, then each phase should be updated with the new changes if any
                                    if (approvalJobPhase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                    {
                                        var phaseInfo =
                                            phasesCollaborators.ApprovalWorkflowPhases.FirstOrDefault(p => p.Phase == approvalJobPhase.ID);
                                        if (phaseInfo.IsExternal)
                                        {
                                            approvalJobPhase.ExternalPrimaryDecisionMaker = phaseInfo.PDMId;
                                            approvalJobPhase.PrimaryDecisionMaker = null;
                                        }
                                        else
                                        {
                                            approvalJobPhase.PrimaryDecisionMaker = phaseInfo.PDMId;
                                            approvalJobPhase.ExternalPrimaryDecisionMaker = null;
                                        }
                                    }

                                    //update deadline, if needed, only for current phase and phases with status "Pendding"
                                    var deadlineTrigger = deadlineTriggers.Where(t => approvalJobPhase.DeadlineTrigger == t.ID).Select(t => t.Key)
                                        .FirstOrDefault();
                                    var deadlineUnit = deadlineUnits.Where(u => approvalJobPhase.DeadlineUnit == u.ID).Select(u => u.Key)
                                        .FirstOrDefault();

                                    if (approvalJobPhase.Deadline == null && approvalJobPhase.DeadlineUnitNumber != null && deadlineTrigger !=
                                        (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
                                    {
                                        approvalJobPhase.Deadline = GetAutomaticPhaseDeadline(deadLineDate, deadlineTrigger,
                                            approvalJobPhase.DeadlineUnitNumber, deadlineUnit);
                                    }
                                }

                                var currentPhase = phases.FirstOrDefault(p => p.ID == phase.Phase);

                                //if phase decision type is PDM, then the approval should be updated with the new changes if any
                                if (currentPhase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                {
                                    if (phase.IsExternal)
                                    {
                                        objApproval.ExternalPrimaryDecisionMaker = phase.PDMId ?? currentPhase.ExternalPrimaryDecisionMaker;
                                        objApproval.PrimaryDecisionMaker = null;
                                    }
                                    else
                                    {
                                        objApproval.PrimaryDecisionMaker = phase.PDMId ?? currentPhase.PrimaryDecisionMaker;
                                        objApproval.ExternalPrimaryDecisionMaker = null;
                                    }
                                }
                                objApproval.CurrentPhase = phase.Phase;

                                //add relations between each workflow phase and current approval
                                var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                                {
                                    Approval1 = objApproval,
                                    Phase = phasesCollaborators.ApprovalWorkflowPhases[0].Phase,
                                    PhaseMarkedAsCompleted = false,
                                    CreatedDate = DateTime.UtcNow,
                                    ModifiedDate = DateTime.UtcNow,
                                    CreatedBy = loggedUser.ID,
                                    ModifiedBy = loggedUser.ID
                                };
                                objApproval.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);
                            }
                        }

                        var approvalSetting = new ApprovalSetting()
                        {
                            Approval1 = objApproval,
                            UpdateJobName = keepOriginalJobName ?? !model.KeepOriginalFileName
                        };

                        //check if custom profile rendering option was selected
                        if (model.RenderOption.HasValue &&
                            model.RenderOption.Value == NewApprovalModel.RenderOptions.CustomProfile)
                        {
                            ApprovalCustomICCProfile appCsmProf = new ApprovalCustomICCProfile();
                            appCsmProf.Approval1 = objApproval;
                            appCsmProf.CustomICCProfile = model.SelectedCustomProfile;
                            context.ApprovalCustomICCProfiles.Add(appCsmProf);
                        }

                        context.ApprovalSettings.Add(approvalSetting);
                        newApprovals.Add(objApproval);

                        // Add ApprovalJobViewingCondition ISOcoated v2 eci for pdf file
                        if (objApproval.FileType == (int)GMGColorDAL.FileType.FileExtention.PDF)
                        {
                            var objApprovalJobViewingCondition = (from ajvc in context.ApprovalJobViewingConditions
                                                                  where ajvc.Job == objApproval.Job
                                                                  select ajvc.ID).FirstOrDefault();
                            if (objApprovalJobViewingCondition != 0)
                            {
                                var jobViewCond = new ApprovalJobViewingCondition
                                {
                                    Job = objApproval.Job,
                                    ViewingCondition = 6
                                };
                                context.ApprovalJobViewingConditions.Add(jobViewCond);
                            }
                        }

                    }

                    internalCollaborators = SetPermissions(newApprovals, phasesCollaborators, model, newPhases, loggedAccount, loggedUser, context, ref sharedApprovals);
                    InsertApprovalTagwords(newApprovals, model, loggedUser.ID, context);

                    using (var ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, GMGColorConfiguration.AppConfiguration.NewApprovalTimeOutInMinutes, 0)))
                    {
                        context.Configuration.AutoDetectChangesEnabled = true;
                        context.SaveChanges();
                        ts.Complete();
                    }
                    if (isCollaborateApi)
                    {
                        newApprovalResponse.ApprovalID = newApprovals.Select(a => a.ID).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    int loggedUserId = loggedUser?.ID ?? -1;
                    int loggedAccountId = loggedAccount?.ID ?? -1;
                    var fileNames = files.Select(f => f.FileName).ToArray();

                    object message = $"AddNewApproval Failed LoggedAccountId: {loggedAccountId} | LoggedUserId: {loggedUserId} | InternalCollaborators: {internalCollaborators.ToArray()} | ExternalCollaborators {sharedApprovals.ToArray()} | Job: {model.Job} | FileNames: {fileNames}";
                    GMGColorLogging.log.Error(loggedUserId, message, ex);
                    throw;
                }
                finally
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                }


                if (approvalPageList != null && approvalPageList.Count > 0)
                {
                    AddNewHtmlApprovalPage(newApprovals, approvalPageList, context);
                }
                SendNewApprovalNotifications(newApprovals, internalCollaborators, sharedApprovals, loggedAccount, loggedUser, model.OptionalMessage, model.Job, context);
                SendNewVersionNotifications(approvalNotifications, loggedAccount, loggedUser, context);

                if (!string.IsNullOrEmpty(duplicatedFiles))
                {
                    model.ErrorMessage = String.Format(GMGColor.Resources.Resources.errPreviousFileExist, duplicatedFiles);
                }
                else if (!string.IsNullOrEmpty(ignoreList))
                {
                    model.ErrorMessage = String.Format(GMGColor.Resources.Resources.errIgnoreDuplicatedFiles, ignoreList);
                }

                return folderId;
           
        }

        public static void SendNewApprovalNotifications(List<Approval> newApprovals, List<int> internalCollaborators, List<SharedApproval> sharedApprovals, Account loggedAccount, GMGColorDAL.User loggedUser, string optionalMessage, int jobId, DbContextBL context)
        {
            if (newApprovals.Count > 0)
            {
                List<int> internalScheduledUsers = new List<int>();
                List<int> externalScheduledUsers = new List<int>();
                int loggedUserId = loggedUser?.ID ?? -1;
                int loggedAccountId = loggedAccount?.ID ?? -1;
                int[] newApprovalsIds = newApprovals.Select(a => a.ID).ToArray();

                List<int?> currentPhases = newApprovals.Select(a => a.CurrentPhase).Distinct().ToList();
                List<int> externalUsers = sharedApprovals.Where(sa => currentPhases.Contains(sa.Phase)).Select(sa => sa.ExternalCollaborator).ToList();

                GMGColorLogging.log.InfoFormat(loggedAccountId, "AddNewApproval.SendNewApprovalNotifications LoggedAccountId: {0} |  LoggedUserId : {1} |InternalCollaborators: {2} | ExternalCollaborators: {3} | JobID: {4} | ApprovalIDs: {5}"
                    , new object[] { loggedAccountId, loggedUserId, internalCollaborators.ToArray(), sharedApprovals.ToArray(), jobId, newApprovalsIds });

                if (loggedUser != null)
                {
                    internalScheduledUsers = GlobalNotificationBL
                        .GetScheduleUsersForEventType(loggedUser.Account, NotificationTypeParser.GetKey(NotificationType.NewApprovalAdded), context)
                        .ToList();
                    externalScheduledUsers = internalScheduledUsers;
                }
                else
                {
                    externalScheduledUsers = GlobalNotificationBL
                        .GetScheduleUsersForEventType(0, NotificationTypeParser.GetKey(NotificationType.NewApprovalAdded), context)
                        .ToList();
                }
                try
                {
                    List<NotificationItem> notificationItems = new List<NotificationItem>();

                    //internal users
                    foreach (int internalCollaborator in internalCollaborators)
                    {
                        if (loggedUser == null)
                        {
                            int accId = UserBL.GetAccountId(internalCollaborator, context);
                            internalScheduledUsers = GlobalNotificationBL.GetScheduleUsersForEventType(accId, NotificationTypeParser.GetKey(NotificationType.NewApprovalAdded), context).ToList();
                        }

                        // Create Notification for valid internal scheduledUser
                        if (internalScheduledUsers.Contains(internalCollaborator))
                        {
                            if (jobId <= 0)
                            {
                                var notification = CreateNewApprovalNotification(newApprovalsIds, internalCollaborator, null, loggedUserId, optionalMessage);
                                List<NotificationItem> listnotificationItems = NotificationServiceBL.CreateNotificationPerf(notification, loggedUser, internalScheduledUsers, context);
                                if (listnotificationItems != null)
                                {
                                    notificationItems.AddRange(listnotificationItems);
                                }
                            }
                            else
                            {
                                var notification = CreateNewVersionNotification(newApprovalsIds, internalCollaborator, null, loggedUserId, optionalMessage);
                                List<NotificationItem> listnotificationItems = NotificationServiceBL.CreateNotificationPerf(notification, loggedUser, internalScheduledUsers, context);
                                if (listnotificationItems != null)
                                {
                                    notificationItems.AddRange(listnotificationItems);
                                }
                            }
                        }
                    }

                    //external users
                    foreach (var externalUserId in externalUsers)
                    {
                        if (jobId <= 0)
                        {
                            var notification = CreateNewApprovalNotification(newApprovalsIds, null, externalUserId, loggedUserId, optionalMessage);
                            List<NotificationItem> listnotificationItems = NotificationServiceBL.CreateNotificationPerf(notification, loggedUser, externalScheduledUsers, context);
                            if (listnotificationItems != null)
                            {
                                notificationItems.AddRange(listnotificationItems);
                            }
                        }
                        else
                        {
                            var notification = CreateNewVersionNotification(newApprovalsIds, null, externalUserId, loggedUserId, optionalMessage);
                            List<NotificationItem> listnotificationItems = NotificationServiceBL.CreateNotificationPerf(notification, loggedUser, externalScheduledUsers, context);
                            if (listnotificationItems != null)
                            {
                                notificationItems.AddRange(listnotificationItems);
                            }
                        }
                    }
                    context.NotificationItems.AddRange(notificationItems);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    object message = $"AddNewApproval.SendNewApprovalNotifications Failed LoggedAccountId: {loggedAccountId} | LoggedUserId: {loggedUserId} | InternalCollaborators: {internalCollaborators.ToArray()} | ExternalCollaborators {sharedApprovals.ToArray()} | Job: {jobId} | ApprovalIDs: {newApprovalsIds}";
                    GMGColorLogging.log.Error(loggedUserId, message, ex);
                    throw;
                }
            }
        }



        public static void SendFileTransferDownloadedNotifications(int fileTransferId, int internalrecipient,int eventCreatorUserId, int loggedAccountId, GMGColorDAL.User loggedUser, string optionalMessage,bool isExternal, DbContextBL context)
        {
           
            List<int> internalScheduledUsers = new List<int>();
            List<int> externalScheduledUsers = new List<int>();
            

          
            using (var ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, GMGColorConfiguration.AppConfiguration.NewApprovalTimeOutInMinutes, 0)))
            {
                try
                {
                    context.Configuration.AutoDetectChangesEnabled = false;
                   
                    if (loggedUser != null)
                    {
                        internalScheduledUsers = GlobalNotificationBL
                            .GetScheduleUsersForEventType(loggedUser.Account, NotificationTypeParser.GetKey(NotificationType.FileTransferDownloaded), context)
                            .ToList();
                        externalScheduledUsers = internalScheduledUsers;
                    }
                    else
                    {
                        externalScheduledUsers = GlobalNotificationBL
                            .GetScheduleUsersForEventType(0, NotificationTypeParser.GetKey(NotificationType.FileTransferDownloaded), context)
                            .ToList();
                    }

                    //internal user recipient
                    
                        if (loggedUser == null)
                        {
                            int accId = UserBL.GetAccountId(internalrecipient, context);
                            internalScheduledUsers = GlobalNotificationBL.GetScheduleUsersForEventType(accId, NotificationTypeParser.GetKey(NotificationType.FileTransferDownloaded), context).ToList();
                        }


                        var notification = CreateFileTransferDownloadedNotification(fileTransferId, internalrecipient, eventCreatorUserId, optionalMessage,isExternal);
                        NotificationServiceBL.CreateNotificationFileTransferDownloaded(notification, loggedUser, internalrecipient, context, saveChanges: false);
                    
                    
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    object message = $"AddNewApproval.FileTransferDownloadedNotifications Failed LoggedAccountId: {loggedAccountId} | EventCreatorUserId: {eventCreatorUserId} | Internalrecipient: {internalrecipient} | ApprovalID: {fileTransferId}";
                    // object message = $"AddNewApproval.SendNewFileTransferNotifications Failed LoggedAccountId: {loggedAccountId} | LoggedUserId: {loggedUserId} | InternalCollaborators: {internalCollaborators.ToArray()} | ExternalCollaborators {sharedApprovals.ToArray()}  | ApprovalID: {newApprovalId}";
                    GMGColorLogging.log.Error(eventCreatorUserId, message, ex);
                    throw;
                }
                finally
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                }

                ts.Complete();
            }

        }

        private static NewApprovalAdded CreateNewApprovalNotification(int[] newApprovalsIds, int? internalCollaborator, int? externalCollaborator, int loggedUserId, string optionalMessage)
		{
			var notification = new NewApprovalAdded
			{
				EventCreator = loggedUserId,
				InternalRecipient = internalCollaborator,
				ExternalRecipient = externalCollaborator,
				OptionalMessage = optionalMessage,
				ApprovalsIds = newApprovalsIds
			};

			return notification;
		}


       
        private static FileTransferDownloaded CreateFileTransferDownloadedNotification(int newApprovalId, int internalRecipient, int eventCreatorUserId, string optionalMessage,bool isExternal)
        {
            int? creatorId = eventCreatorUserId;
            int? externalCreatorId = null;
            if (isExternal)
            {
                creatorId = null;
                externalCreatorId = eventCreatorUserId;
            }
            var notification = new FileTransferDownloaded
            {
                EventCreator = creatorId,
                InternalRecipient = internalRecipient,
                OptionalMessage = optionalMessage,
                ApprovalsIds = new int[] { newApprovalId },
                ExternalCreator=externalCreatorId
            };

            return notification;
        }

        private static NewVersionWasCreated CreateNewVersionNotification(int[] newApprovalsIds, int? internalCollaborator, int? externalCollaborator, int loggedUserId, string optionalMessage)
		{
			var notification = new NewVersionWasCreated
			{
				EventCreator = loggedUserId,
				InternalRecipient = internalCollaborator,
				ExternalRecipient = externalCollaborator,
				OptionalMessage = optionalMessage,
				ApprovalsIds = newApprovalsIds
			};

			return notification;
		}

        private static void SendNewVersionNotifications(List<ApprovalNotificationsHelper> approvalNotifications, Account loggedAccount, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            if (approvalNotifications.Count > 0)
            {
                List<int> internalScheduledUsers = new List<int>();
                List<int> externalScheduledUsers = new List<int>();
                int loggedUserId = loggedUser?.ID ?? -1;
                int loggedAccountId = loggedAccount?.ID ?? -1;
                int[] newApprovalsIds = approvalNotifications.Select(a => a.Approval.ID).ToArray();

                GMGColorLogging.log.InfoFormat(loggedAccountId, "AddNewApproval.SendNewVersionNotifications LoggedAccountId: {0} | LoggedUserId: {1} | ApprovalIDs: {2}"
                    , new object[] { loggedAccountId, loggedUserId, newApprovalsIds });

                if (loggedUser != null)
                {
                    internalScheduledUsers = GlobalNotificationBL
                        .GetScheduleUsersForEventType(loggedUser.Account, NotificationTypeParser.GetKey(NotificationType.NewVersionWasCreated), context)
                        .ToList();
                    externalScheduledUsers = internalScheduledUsers;
                }
                else
                {
                    externalScheduledUsers = GlobalNotificationBL
                        .GetScheduleUsersForEventType(0, NotificationTypeParser.GetKey(NotificationType.NewVersionWasCreated), context)
                        .ToList();
                }
                try
                {
                    List<NotificationItem> notificationItems = new List<NotificationItem>();
                    foreach (var approvalNotificationHelper in approvalNotifications)
                    {
                        foreach (var internalCollaborator in approvalNotificationHelper.InternalCollaborators)
                        {
                            if (loggedUser == null)
                            {
                                int accountId = UserBL.GetAccountId(internalCollaborator, context);
                                internalScheduledUsers = GlobalNotificationBL.GetScheduleUsersForEventType(accountId, NotificationTypeParser.GetKey(NotificationType.NewVersionWasCreated), context).ToList();
                            }

                            List<NotificationItem> listnotificationItems = NotificationServiceBL.CreateNotificationPerf(new NewVersionWasCreated
                            {
                                EventCreator = loggedUser.ID,
                                InternalRecipient = internalCollaborator,
                                ApprovalsIds = new[] { approvalNotificationHelper.Approval.ID }
                            },
                            loggedUser,
                            internalScheduledUsers,
                            context
                            );
                            if (listnotificationItems != null)
                            {
                                notificationItems.AddRange(listnotificationItems);
                            }
                        }

                        foreach (var externalCollaborator in approvalNotificationHelper.ExternalCollaborators)
                        {
                            List<NotificationItem> listnotificationItems = NotificationServiceBL.CreateNotificationPerf(new NewVersionWasCreated
                            {
                                EventCreator = loggedUser.ID,
                                ExternalRecipient = externalCollaborator,
                                ApprovalsIds = new[] { approvalNotificationHelper.Approval.ID }
                            },
                            loggedUser,
                            externalScheduledUsers,
                            context
                            );
                            if (listnotificationItems != null)
                            {
                                notificationItems.AddRange(listnotificationItems);
                            }
                        }
                    }
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    object message = $"AddNewApproval.SendNewVersionNotifications Failed LoggedAccountId: {loggedAccountId} |  LoggedUserId: {loggedUserId} | ApprovalIDs: {newApprovalsIds}";
                    GMGColorLogging.log.Error(loggedUserId, message, ex);
                    throw;
                }
            }
        }

        private static List<int> SetPermissions(List<Approval> newApprovals, ApprovalWorkflowPhasesInfo phasesCollaborators, NewApprovalModel model,
			Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>> newPhases, Account loggedAccount, GMGColorDAL.User loggedUser, DbContextBL context,
			ref List<SharedApproval> sharedApprovals)
		{
			var internalCollaborators = new List<int>();
			if (newApprovals.Any())
			{
				var workflowPhases = GetApprovalPhasesCollaboratorsPerf(phasesCollaborators, newPhases);
				var collaboratorRoles = ApprovalCollaboratorRole.GetRoles(context);

				//set internal users permissions
				var permissionsModel = new PermissionsModel
				{
					SelectedUsersAndGroups = model.SelectedUsersAndGroups
				};

				var currPhase = model.ApprovalWorkflowId > 0 ? phasesCollaborators.ApprovalWorkflowPhases[0].Phase : (int?)null;

				PermissionsUsersAndRolesModelPerf permissionsUsersAndRolesFromModel = null;
				PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles = null;
				var index = 0;
				var idx = 0;

				try
				{
					string externalEmails = PermissionsBL.GetFormatedExternalUsersDetails(model.ExternalEmails);

					foreach (var approval in newApprovals)
					{
						ApprovalWorkflowPhasesInfoPerf approvalWorkflowPhases = (workflowPhases != null) ? workflowPhases[idx] : null;
						if (workflowPhases != null)
						{
							var approvalWorkflow = workflowPhases[index];
							index++;

							permissionsUsersAndRoles = PermissionsBL.GetPermissionsPerf(approvalWorkflow);
						}
						else
						{
							if (permissionsUsersAndRolesFromModel == null)
							{
								permissionsUsersAndRolesFromModel = PermissionsBL.GetPermissionsPerf(permissionsModel);
							}
							permissionsUsersAndRoles = permissionsUsersAndRolesFromModel;
						}

						PermissionsBL.SetApprovalPermission(approval, permissionsUsersAndRoles, context);

						string externalPdmEmail = string.Empty;
						if (model.OnlyOneDecisionRequired && !string.IsNullOrEmpty(model.PrimaryDecisionMaker) && model.PrimaryDecisionMaker.Contains("@"))
						{
							externalPdmEmail = model.PrimaryDecisionMaker;
						}

						sharedApprovals = PermissionsBL.SetExternalCollaboratorsPermissions(model.ExternalUsers, externalEmails, approvalWorkflowPhases, loggedAccount.ID, collaboratorRoles, approval, model.AllowUsersToDownload, externalPdmEmail, context);

						idx++;
					}

					internalCollaborators = permissionsUsersAndRoles?.Users.Where(p => (p.Phase?.ID == 0 ? p.Phase?.PhaseTemplateID : p.Phase?.ID) == currPhase).Select(u => u.ID).ToList() ?? new List<int>();
				}
				catch (ExceptionBL)
				{
					throw;
				}
				catch (DbEntityValidationException ex)
				{
					DALUtils.LogDbEntityValidationException(ex);
					throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
				}
				catch (Exception ex)
				{
					throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
				}
			}

			return internalCollaborators;
		}

        private static string GetFileNameWithoutExtension(string fileName)
		{
			int dotIndex = fileName.LastIndexOf(".");
			if (dotIndex > 0)
			{
				return fileName.Substring(0, dotIndex);
			}
			return fileName;
		}

		/// <summary>
		/// Get automatic phase deadline
		/// </summary>
		/// <param name="date">Approval deadline</param>
		/// <param name="trigger">Phase deadline trigger [After Upload | Last Phase Completed | Approval Deadline]</param>
		/// <param name="unitNumber">Number of [Hours | Days]</param>
		/// <param name="unit">[Hours | Days]</param>
		/// <returns></returns>
		public static DateTime? GetAutomaticPhaseDeadline(DateTime? date, int? trigger, int? unitNumber, int? unit)
		{
			DateTime deadline = DateTime.UtcNow;

			//set deadline for "Approval Deadline" trigger
			if (trigger == (int)GMG.CoZone.Common.PhaseDeadlineTrigger.ApprovalDeadline)
			{
				deadline = date.Value;
			}

			if (deadline > default(DateTime))
			{
				switch (unit)
				{
					case (int)GMG.CoZone.Common.PhaseDeadlineUnit.Hours:
						{
							deadline = deadline.AddHours(unitNumber.GetValueOrDefault());
							break;
						}
					case (int)GMG.CoZone.Common.PhaseDeadlineUnit.Days:
						{
							deadline = deadline.AddDays(unitNumber.GetValueOrDefault());
							break;
						}
				}
			}

			return deadline;
		}

		private static List<ApprovalWorkflowPhasesInfo> GetApprovalPhasesCollaborators(ApprovalWorkflowPhasesInfo phasesCollaborators, Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>> newPhases)
		{
			List<ApprovalWorkflowPhasesInfo> workflowPhases = null;
			if (phasesCollaborators != null && phasesCollaborators.ApprovalWorkflowPhases.Count > 0)
			{
				workflowPhases = new List<ApprovalWorkflowPhasesInfo>();
				if (newPhases.Count > 0)
				{
					foreach (var workflow in newPhases)
					{
						var approvalPhases = new List<ApprovalWorkflowPhase>();
						var newPhasesIds = workflow.Value.Select(p => p.ID).ToList();

						if (newPhasesIds.Count > 0)
						{
							//update template phases ids with the new added ones or existing job phases ids
							for (var i = 0; i < phasesCollaborators.ApprovalWorkflowPhases.Count; i++)
							{
								phasesCollaborators.ApprovalWorkflowPhases[i].Phase = newPhasesIds[i];
								approvalPhases.Add(new ApprovalWorkflowPhase()
								{
									Phase = phasesCollaborators.ApprovalWorkflowPhases[i].Phase,
									PhaseCollaborators =
										phasesCollaborators.ApprovalWorkflowPhases[i].PhaseCollaborators
								});
							}
						}

						workflowPhases.Add(new ApprovalWorkflowPhasesInfo()
						{
							ApprovalWorkflowPhases = approvalPhases
						});
					}
				}
				else
				{
					workflowPhases.Add(phasesCollaborators);
				}
			}

			return workflowPhases;
		}

		private static List<ApprovalWorkflowPhasesInfoPerf> GetApprovalPhasesCollaboratorsPerf(ApprovalWorkflowPhasesInfo phasesCollaborators, Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>> newPhases)
		{
			List<ApprovalWorkflowPhasesInfoPerf> workflowPhases = null;
			if (phasesCollaborators != null && phasesCollaborators.ApprovalWorkflowPhases.Count > 0)
			{
				workflowPhases = new List<ApprovalWorkflowPhasesInfoPerf>();
				if (newPhases.Count > 0)
					foreach (var workflow in newPhases)
					{
						var approvalPhases = new List<ApprovalWorkflowPhasePerf>();
						var newPhasesIds = workflow.Value.ToList();

						if (newPhasesIds.Count > 0)
						{
							//update template phases ids with the new added ones or existing job phases ids
							for (var i = 0; i < phasesCollaborators.ApprovalWorkflowPhases.Count; i++)
							{
								//phasesCollaborators.ApprovalWorkflowPhases[i].Phase = newPhasesIds[i];
								approvalPhases.Add(new ApprovalWorkflowPhasePerf()
								{
									Phase = newPhasesIds[i],
									PhaseCollaborators =
										phasesCollaborators.ApprovalWorkflowPhases[i].PhaseCollaborators
								});
							}
						}

						workflowPhases.Add(new ApprovalWorkflowPhasesInfoPerf()
						{
							ApprovalWorkflowPhases = approvalPhases
						});
					}
			}

			return workflowPhases;
		}

		/// <summary>
		/// returns the id of the current file
		/// </summary>
		/// <param name="isUploadApproval">Is uploaded file or is from link</param>
		/// <param name="fileType">The file type</param>
		/// <param name="context">Database context</param>
		/// <returns></returns>
		private static int GetFileTypeId(bool isUploadApproval, FileType fileType, DbContextBL context)
		{
			return isUploadApproval
					   ? (fileType != null && fileType.Type == "video") ||
						 (fileType != null && fileType.Type == "swf")
							 ? (from at in context.ApprovalTypes
								where at.Key == (int)Approval.ApprovalTypeEnum.Movie
								select at.ID).FirstOrDefault()
							 : (from at in context.ApprovalTypes
								where at.Key == (int)Approval.ApprovalTypeEnum.Image
								select at.ID).FirstOrDefault()
					   : (from at in context.ApprovalTypes
						  where at.Key == (int)Approval.ApprovalTypeEnum.WebPage
						  select at.ID).FirstOrDefault();
		}

        /// <summary>
        /// returns the id of the current file
        /// </summary>
        /// <param name="isUploadApproval">Is uploaded file or is from link</param>
        /// <param name="fileType">The file type</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        private static int GetFileType(bool isUploadApproval, string fileType, DbContextBL context)
        {
            if (FileTypes == null)
            {
                FileTypes = context.ApprovalTypes.Select(t => new { Key = t.Key, Value = t.ID }).ToDictionary(t => t.Key, t => t.Value);
            }
            if (isUploadApproval == true)
            {
                if (fileType == "video" || fileType == "swf")
                {
                    return FileTypes[(int)Approval.ApprovalTypeEnum.Movie];
                }
                else if (fileType == "zip")
                {
                    return FileTypes[(int)Approval.ApprovalTypeEnum.Zip];
                }
                else
                {
                    return FileTypes[(int)Approval.ApprovalTypeEnum.Image];
                }
            }
            else
            {
                return FileTypes[(int)Approval.ApprovalTypeEnum.WebPage];
            }
        }

        /// <summary>
        /// Returns formated deadline date selected for uploaded files
        /// </summary>
        /// <param name="deadlineDate">The date selected from "New Approval" page</param>
        /// <param name="deadlineTime">The time selected from "New Approval" page</param>
        /// <param name="deadlineTimeMeridiem">The time meridiem selected from "New Approval" page</param>
        /// <param name="dateFormat">The date format of the logged account</param>
        /// <param name="timeFormat">The time format of the logged account</param>
        /// <returns></returns>
        public static DateTime GetFormatedApprovalDeadline(string deadlineDate, string deadlineTime, string deadlineTimeMeridiem, string dateFormat, int timeFormat)
		{
                DateTime deadline = DateTime.ParseExact(deadlineDate, dateFormat, System.Globalization.CultureInfo.InvariantCulture);
                //check if hour is set else leave default
                if (deadlineTime != "hh:mm")
                {
                    if (timeFormat == 1)
                    {
                        //if timeformat is 12 hours format
                        int hour = Convert.ToInt32(deadlineTime.Split(':')[0]);
                        int minutes = Convert.ToInt32(deadlineTime.Split(':')[1]);

                        if (deadlineTimeMeridiem == "PM")
                        {
                            //for pm hour should be between 12 and 23
                            if (hour != 12)
                            {
                                hour += 12;
                            }
                        }
                        else
                        {
                            //for am hour should be between 0 and 11
                            if (hour == 12)
                            {
                                hour = 0;
                            }
                        }

                        deadline = new DateTime(deadline.Year, deadline.Month, deadline.Day, hour, minutes, 0);
                    }
                    else
                    {
                        //if timeformat is 24 hours format
                        TimeSpan tspan = new TimeSpan(0, 0, 0);
                        tspan = new TimeSpan(int.Parse(deadlineTime.Split(':')[0]), int.Parse(deadlineTime.Split(':')[1]), 0);
                        deadline = deadline.Date + tspan;
                    }
                }
                else
                {
                    deadline = deadline.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                return deadline;
        }

		public static void PutEmailsNotificationsInQueue(List<int> newApprovalsIds, List<int> internalCollaborators, List<int> externalCollaborators, GMGColorDAL.User loggedUser, string optionalMessage, int jobId)
		{
			try
			{
				bool newVersionWasCreated = 0 < jobId;
				var emailType = newVersionWasCreated ? NotificationType.NewVersionWasCreated : NotificationType.NewApprovalAdded;
				var tempNotificationItems = NotificationServiceBL.CreateTempNotificationItems(newApprovalsIds, internalCollaborators, externalCollaborators, loggedUser, optionalMessage, emailType);

				NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);
			}
			catch (ExceptionBL exbl)
			{
				GMGColorLogging.log.Error(exbl.Message, exbl);
			}
			catch (Exception ex)
			{
				var exbl = new ExceptionBL(String.Format("Failed to put messages in Queue with exception message: {0}", ex.Message));
				GMGColorLogging.log.Error(exbl.Message, exbl);
			}
		}

		public static void PutEmailsNotificationsInQueueUpdated(List<int> newApprovalsIds, List<int> internalCollaborators, List<int> externalCollaborators, int jobId, GMGColorDAL.User loggedUser, string optionalMessage, NotificationType emailType)
		{
			int loggedUserId = loggedUser?.ID ?? -1;

			try
			{
				GMGColorLogging.log.InfoFormat(loggedUserId,
					"AddNewApproval. PutEmailsNotificationsInQueueUpdated. LoggedAccountId: {0} | InternalCollaborators count: {1} | ExternalCollaboratorsCount {2} | Job: {3} | ApprovalID: {4}"
					, new object[] { loggedUserId, internalCollaborators.ToArray(), externalCollaborators.ToArray(), jobId, newApprovalsIds.FirstOrDefault() });

				var tempNotificationItems = NotificationServiceBL.CreateTempNotificationItems(newApprovalsIds, internalCollaborators, externalCollaborators, loggedUser, optionalMessage, emailType);
				NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);
			}
			catch (Exception ex)
			{
				object message = $"AddNewApproval Failed to put messages in Queue LoggedAccountId: {loggedUserId} | InternalCollaborators count: {internalCollaborators.ToArray()} | ExternalCollaboratorsCount {externalCollaborators.ToArray()} | Job: {jobId} | ApprovalID: {newApprovalsIds.ToArray()}";
				GMGColorLogging.log.Error(loggedUserId, message, ex);
			}
		}

		public static int ApprovalJobWorkflowID(string selectedWorkflowName, int accountId, DbContextBL context)
		{
			return (from awj in context.ApprovalJobWorkflows
					where awj.Name == selectedWorkflowName.ToLower().Trim() && awj.Account == accountId
					select awj.ID).FirstOrDefault();
		}

		public static List<int> GetApprovalPhasesByWorkflowName(string selectedWorkflowName, int accountId, DbContextBL context)
		{
			return (from ajw in context.ApprovalJobWorkflows
					join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow
					where ajw.Name.ToLower() == selectedWorkflowName.ToLower() && ajw.Account == accountId
					orderby ajp.Position
					select ajp.ID).ToList();
		}

		public static ApprovalWorkflowPhasesInfo GetVersionExistingPhasesCollaborators(int maxVersion, int workflowId, int currentPhasePosition, DbContextBL context)
		{
			var aprovalPhaseCollaboratorInfo = new ApprovalWorkflowPhasesInfo();

			if (workflowId > 0)
			{
				var phases = (from aw in context.ApprovalJobWorkflows
							  join ap in context.ApprovalJobPhases on aw.ID equals ap.ApprovalJobWorkflow
							  where aw.ID == workflowId && ap.Position >= currentPhasePosition
							  orderby ap.Position
							  select ap).ToList();

				foreach (var phase in phases)
				{
					var collaboratorsInfo = new ApprovalPhaseCollaboratorInfo();
					collaboratorsInfo.LoadJobPhasesCollaborators(phase.ID, maxVersion, context);

					int? pdm = null;
					bool isExternal = false;
					if (phase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
					{
						pdm = phase.PrimaryDecisionMaker ?? phase.ExternalPrimaryDecisionMaker;
						isExternal = phase.ExternalPrimaryDecisionMaker != null;
					}

					aprovalPhaseCollaboratorInfo.ApprovalWorkflowPhases.Add(new ApprovalWorkflowPhase
					{
						Phase = phase.ID,
						PDMId = pdm,
						DecisionType = phase.DecisionType,
						IsExternal = isExternal,
						PhaseCollaborators = collaboratorsInfo
					});
				}
			}

			return aprovalPhaseCollaboratorInfo;
		}

		public static int GetCurrentJobPhasePosition(int maxVersion, DbContextBL context)
		{
			return (from a in context.Approvals
					join ajp in context.ApprovalJobPhases on a.CurrentPhase equals ajp.ID
					where a.ID == maxVersion
					select ajp.Position).FirstOrDefault();
		}

		public static Approval GetApprovalIdByJob(int jobID, int loggeduser, DbContextBL context)
		{
			return (from a in context.Approvals
					join j in context.Jobs on a.Job equals j.ID
					where
						j.ID == jobID &&
						j.JobStatu.Key != "ARC" &&
						a.IsDeleted == false &&
						a.ApprovalCollaborators.Select(x => x.Collaborator).Contains(loggeduser)
					select a).OrderByDescending(m => m.Version).FirstOrDefault();
		}

		public static ApprovalWorkflowModel GetVersionSelectedApprovalJobWorkflows(int jobId, DbContextBL context)
		{
			return (from ajw in context.ApprovalJobWorkflows
					where ajw.Job == jobId
					select new ApprovalWorkflowModel
					{
						ID = ajw.ID,
						Name = ajw.Name,
						RerunWorkflow = ajw.RerunWorkflow
					}).FirstOrDefault();
		}

        // Selecting Checklist for new version 
        public static ChecklistModel GetVersionSelectedChecklist(int jobId, DbContextBL context)
        {
            return (from a in context.Approvals
                    join cl in context.CheckList on a.Checklist equals cl.ID 
                    where a.Job == jobId
                    select new ChecklistModel
                    {
                        ID = cl.ID,
                        Name = cl.Name,
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Checks if each phase has at least one  approver and reviewer
        /// </summary>
        /// <param name="phasesCollaborators">All phases collaborators</param>
        /// <returns></returns>
        public static bool AllPhasesHaveApproverAndReviewer(ApprovalWorkflowPhasesInfo phasesCollaborators)
		{
			foreach (var phase in phasesCollaborators.ApprovalWorkflowPhases)
			{
				var usersAndRoles = PermissionsBL.GetUsersAndRoles(phase.PhaseCollaborators.CollaboratorsWithRole);
				var externalsAndRoles = PermissionsBL.GetExternalUsersAndRoles(phase.PhaseCollaborators.ExternalCollaboratorsWithRole);

				if (usersAndRoles.All(u => u.Role != (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer) &&
					externalsAndRoles.All(ex => ex.Role != (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Returns list of plan types features
		/// </summary>
		/// <param name="accountId"></param>
		/// <param name="module"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<int> GetPlanTypeOptions(int accountId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
		{
			return Account.GetPlanTypeOptions(accountId, module, context);
		}

		/// <summary>
		/// Return details for the current selected version
		/// </summary>
		/// <param name="approvalId"></param>
		/// <param name="loggedAccount"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static ApprovalVersionWorkflowDetails GetApprovalCurrentVersion(int versionId, Account loggedAccount, int userId, int userLocale, GMGColorDAL.Role.RoleName roleName, DbContextBL context)
		{
			List<WorkflowPhase> phases;
			var approvalVersion = new ApprovalVersionWorkflowDetails();
			List<WorkflowPhase> displayList = new List<WorkflowPhase>();
			var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccount.ID, 1, false, context);

			var currentVersion = (from j in context.Jobs
								  join a in context.Approvals on j.ID equals a.Job
								  join ajpa in context.ApprovalJobPhaseApprovals on a.ID equals ajpa.Approval
								  join ajp in context.ApprovalJobPhases on ajpa.Phase equals ajp.ID
								  let isMaxVersion = (from v in context.Approvals
													  where v.Job == j.ID
													  select v.Version).Max() == a.Version
								  where a.ID == versionId
								  select new
								  {
									  isMaxVersion,
									  Position = ajp.Position,
									  IsLocked = a.IsLocked
								  }
							  ).FirstOrDefault();

			var currentPhase = (from a in context.Approvals
								where a.ID == versionId
								select a.CurrentPhase).FirstOrDefault();

			var phaseHistory = (from j in context.Jobs
								join a in context.Approvals on j.ID equals a.Job

								let phasesAndApproval = (from ajw in context.ApprovalJobWorkflows
														 join ajpa in context.ApprovalJobPhaseApprovals on a.ID equals ajpa.Approval
														 where j.ID == ajw.Job
														 select new
														 {
															 ApprovalID = ajpa.Approval,
															 PhaseID = ajpa.Phase
														 }).ToList()
								where a.ID == versionId
								select new
								{
									JobOwner = j.JobOwner,
									a.IsLocked,
									Phases = phasesAndApproval
								}).FirstOrDefault();

			approvalVersion.Phases = (from j in context.Jobs
									  join a in context.Approvals on j.ID equals a.Job
									  join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
									  join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow

									  let phaseTemplateGuid = (from ap in context.ApprovalPhases
															   where ajp.PhaseTemplateID == ap.ID
															   select ap.Guid).FirstOrDefault()
									  let collaborators = (from ac in context.ApprovalCollaborators
														   where ajp.ID == ac.Phase && ac.Approval == versionId
														   select ac.Collaborator).ToList()

									  join acd in context.ApprovalCollaboratorDecisions on ajp.ID equals acd.Phase
									  where a.ID == versionId && (collaborators.Contains(userId) || userId == j.JobOwner || (collaborateSettings.ShowAllFilesToAdmins && roleName == GMGColorDAL.Role.RoleName.AccountAdministrator))
									  let wasForced = (from ajpa in context.ApprovalJobPhaseApprovals
													   where ajpa.Approval == a.ID && ajp.ID == ajpa.Phase
													   select ajpa.PhaseMarkedAsCompleted).FirstOrDefault()
									  select new WorkflowPhase()
									  {
										  ID = ajp.ID,
										  Name = ajp.Name,
										  Position = ajp.Position,
										  Status = null,
										  Deadline = ajp.Deadline,
										  ForceApproved = wasForced,
										  Guid = phaseTemplateGuid,
										  IsActive = ajp.IsActive
									  }).Distinct().ToList();

			var loggedUserIsJobOwner = phaseHistory.JobOwner == userId;
			approvalVersion.objLoggedAccount = loggedAccount;
			approvalVersion.Id = versionId;
			approvalVersion.CanEditPhaseDeadline = (loggedUserIsJobOwner || (roleName == GMGColorDAL.Role.RoleName.AccountAdministrator) || roleName == GMGColorDAL.Role.RoleName.AccountManager);
			approvalVersion.CurrentPhase = currentPhase;
			approvalVersion.LoggedUserIsJobOwner = loggedUserIsJobOwner;

			//get all phasese except for the pending ones 
			var completedOrInProgressPhases = approvalVersion.Phases.Where(p => phaseHistory.Phases.Select(i => i.PhaseID).ToList().Contains(p.ID)).ToList();

			var activePhases = approvalVersion.Phases.Where(p => p.IsActive).Select(p => p.ID).ToList();
			approvalVersion.IsMaxPhaseCompleted = activePhases.All(p => phaseHistory.Phases.Select(i => i.PhaseID).ToList().Contains(p)) && currentVersion.IsLocked;
			approvalVersion.CurrentPhasePosition = approvalVersion.Phases.Where(ap => ap.ID == currentPhase).Select(ap => ap.Position).FirstOrDefault();
			approvalVersion.IsMaxVersion = currentVersion.isMaxVersion;

			if (currentVersion != null && currentVersion.isMaxVersion)
			{
				var pendingPhases = approvalVersion.Phases.Where(p => !phaseHistory.Phases.Select(i => i.PhaseID).ToList().Contains(p.ID) && p.Position > currentVersion.Position).ToList();

				var maxPhase = completedOrInProgressPhases.OrderByDescending(m => m.Position).FirstOrDefault();

				bool isCompleted = false;
				if (maxPhase != null)
				{
					isCompleted = IsPhaseCompleted(maxPhase.ID, versionId, context);
				}

				foreach (var phase in completedOrInProgressPhases)
				{
					if (phase.ID == currentPhase && !isCompleted)
					{
						phase.Status = currentVersion.IsLocked && IsApprovalRejected(versionId, context) ? GMGColor.Resources.Resources.lblPhaseStatusRejected : GMGColor.Resources.Resources.lblPhaseStatusInProgress;
					}
					else
					{
						phase.Status = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
					}

					displayList.Add(new WorkflowPhase()
					{
						ID = phase.ID,
						Name = phase.Name,
						Position = phase.Position,
						Status = phase.Status,
						Deadline = phase.Deadline,
						PhaseVersionDetails = phase.PhaseVersionDetails,
						ForceApproved = phase.ForceApproved,
						Guid = phase.Guid,
						IsActive = phase.IsActive
					});
				}

				foreach (var pendingPhase in pendingPhases)
				{
					displayList.Add(new WorkflowPhase()
					{
						ID = pendingPhase.ID,
						Name = pendingPhase.Name,
						Position = pendingPhase.Position,
						Deadline = pendingPhase.Deadline,
						Status = pendingPhase.IsActive ? GMGColor.Resources.Resources.lblPhaseStatusPending : GMGColor.Resources.Resources.lblInactivePhase,
						Guid = pendingPhase.Guid,
						IsActive = pendingPhase.IsActive,
						InPending = true
					});
				}

				phases = displayList.OrderBy(d => d.Position).ToList();
				PopulateApprovalPhases(ref phases, versionId, loggedAccount, userId, userLocale, roleName, context);
				approvalVersion.Phases = phases;
				return approvalVersion;
			}

			foreach (var phase in completedOrInProgressPhases)
			{
				if (phase.ID == currentPhase)
				{
					phase.Status = GMGColor.Resources.Resources.lblPhaseStatusRejected;
				}
				else if (phase.ID < currentPhase)
				{
					phase.Status = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
				}
				displayList.Add(new WorkflowPhase()
				{
					ID = phase.ID,
					Name = phase.Name,
					Position = phase.Position,
					Deadline = phase.Deadline,
					Status = phase.Status,
					ForceApproved = phase.ForceApproved,
					Guid = phase.Guid,
					IsActive = phase.IsActive
				});
			}
			phases = displayList.OrderBy(d => d.Position).ToList();

			// retreive collaborators for each phase
			PopulateApprovalPhases(ref phases, versionId, loggedAccount, userId, userLocale, roleName, context);
			approvalVersion.Phases = phases;

			return approvalVersion;
		}

        public static int GetJobID(int approvalID, DbContextBL context)
        {
            return   (from a in context.Approvals
                                where a.ID == approvalID
                                select a.Job).FirstOrDefault();

        }
        public static int[] GetAllVersionIDs(int jobID, DbContextBL context)
        {
            return (from a in context.Approvals
                    where a.Job == jobID && a.Version > 0 && a.IsDeleted == false && a.DeletePermanently == false
                    select a.ID).ToArray();

        }

        public static List<int> GetAllSubAccountIds(int accountID, DbContextBL context)
        {
            return (from a in context.Accounts
                    where a.Parent == accountID && a.Status == 1 || (a.ID == accountID)
                    select a.ID).ToList();

        }

        public static int[] GetAllNonPhaseVersionIDBetweenTime(int AccountID, List<int> SelectedAccountIds , DateTime StartDate, DateTime EndDate, DbContextBL context)
        {
            EndDate = EndDate.AddDays(1);
            return (from a in context.Approvals
                    join j in context.Jobs on a.Job equals j.ID
                    where SelectedAccountIds.Contains(j.Account) && a.Version > 0 && a.IsDeleted == false && a.DeletePermanently == false
                     && a.CurrentPhase == null &&
                    a.CreatedDate > StartDate && a.CreatedDate < EndDate
                    select a.ID).ToArray();

        }

        public static int[] GetAllPhaseVersionIDBetweenTime(int AccountID, List<int> SelectedAccountIds, DateTime StartDate, DateTime EndDate, DbContextBL context)
        {
            EndDate = EndDate.AddDays(1);
            return (from a in context.Approvals
                    join j in context.Jobs on a.Job equals j.ID
                    where SelectedAccountIds.Contains(j.Account) && a.Version > 0 && a.IsDeleted == false && a.DeletePermanently == false
                    && a.CurrentPhase != null &&
                    a.CreatedDate > StartDate && a.CreatedDate < EndDate
                    select a.ID).ToArray();

        }

        public static List<ApprovalUserActivityReportViewData> GetUserActivityReport(int[] nonPhaseApprovals, int[] phasedApprovas, List<int> selectedGroup , string DateFormat_Pattern , DbContextBL context)
        {
            List<UserActivity_Report> approvalUserActivityReportViewData = new List<UserActivity_Report>();


            if (nonPhaseApprovals.Count() > 0)
            {
                if (selectedGroup.Count > 0)
                {
                    var internalUserActivityReport = (from a in context.Approvals
                                                      join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                                      join u in context.Users on ac.Collaborator equals u.ID
                                                      join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                      join ug in context.UserGroups on ugu.UserGroup equals ug.ID
                                                      join acd in context.ApprovalCollaboratorDecisions on ac.Collaborator equals acd.Collaborator
                                                      where
                                                      selectedGroup.Contains(ug.ID) &&
                                                      ac.Approval == a.ID && ac.Collaborator == u.ID && acd.Collaborator == u.ID
                                                      && a.CurrentPhase == null && ac.Phase == null
                                                      && nonPhaseApprovals.Contains(a.ID)
                                                      && acd.Approval == a.ID && acd.Collaborator == ac.Collaborator
                                                      let hasOpenedFile = (from apuvi in context.ApprovalUserViewInfoes
                                                                           where apuvi.Approval == a.ID && apuvi.User == u.ID && apuvi.Phase == null
                                                                           select apuvi).FirstOrDefault()

                                                     select new UserActivity_Report()
                                                      {
                                                          FullName = u.GivenName + " " + u.FamilyName,
                                                          UserName = u.Username,
                                                          FileName = a.FileName,
                                                           DecisionId = (acd.Decision != null ? acd.Decision : (hasOpenedFile != null ? 6 : 0) ) ,
                                                           date = acd.CompletedDate != null ? acd.CompletedDate : hasOpenedFile != null ? hasOpenedFile.ViewedDate : ac.AssignedDate ,
                                                          Version = a.Version,
                                                          Phase = string.Empty,
                                                          Annotation = (from aa in context.ApprovalAnnotations
                                                                        join u1 in context.Users on aa.Creator equals u1.ID
                                                                        join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                                        join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                              where a1.ID == a.ID && aa.Parent == null && aa.Creator == u.ID && aa.Phase == null
                                                                        select aa.ID).ToList()
                                                                                                    ,
                                                          AnnotationReplay = (from aa in context.ApprovalAnnotations
                                                                              join u1 in context.Users on aa.Creator equals u1.ID
                                                                              join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                                              join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                                    where a1.ID == a.ID && aa.Parent != null && aa.Creator == u.ID && aa.Phase == null
                                                                               select aa.ID).ToList()
                                                      }).ToList();
                    approvalUserActivityReportViewData.AddRange(internalUserActivityReport);
                }
                else
                {
                    var internalUserActivityReport = (from a in context.Approvals
                                                      join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                                      join u in context.Users on ac.Collaborator equals u.ID
                                                      join acd in context.ApprovalCollaboratorDecisions on ac.Collaborator equals acd.Collaborator
                                                      where
                                                      ac.Approval == a.ID && ac.Collaborator == u.ID && acd.Collaborator == u.ID
                                                      && a.CurrentPhase == null && ac.Phase == null
                                                      && nonPhaseApprovals.Contains(a.ID)
                                                      && acd.Approval == a.ID && acd.Collaborator == ac.Collaborator
                                                      let hasOpenedFile = (from apuvi in context.ApprovalUserViewInfoes
                                                                           where apuvi.Approval == a.ID && apuvi.User == u.ID && apuvi.Phase == null
                                                                           select apuvi).FirstOrDefault()
                                                      select new UserActivity_Report()
                                                      {
                                                          FullName = u.GivenName + " " + u.FamilyName,
                                                          UserName = u.Username,
                                                          FileName = a.FileName,
                                                           DecisionId = (acd.Decision != null ? acd.Decision : (hasOpenedFile != null ? 6 : 0)),
                                                           date = acd.CompletedDate != null ? acd.CompletedDate : hasOpenedFile != null ? hasOpenedFile.ViewedDate : ac.AssignedDate,
                                                          Version = a.Version,
                                                          Phase = string.Empty,
                                                          Annotation = (from aa in context.ApprovalAnnotations
                                                                        join u1 in context.Users on aa.Creator equals u1.ID
                                                                        join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                                        join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                              where a1.ID == a.ID && aa.Parent == null && aa.Creator == u.ID && aa.Phase == null
                                                                         select aa.ID).ToList()
                                                                                                   ,
                                                          AnnotationReplay = (from aa in context.ApprovalAnnotations
                                                                              join u1 in context.Users on aa.Creator equals u1.ID
                                                                              join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                                              join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                                    where a1.ID == a.ID && aa.Parent != null && aa.Creator == u.ID && aa.Phase == null
                                                                               select aa.ID).ToList()
                                                      }).ToList();
                    approvalUserActivityReportViewData.AddRange(internalUserActivityReport);
                }
            }



            if (phasedApprovas.Count() > 0)
            {
                if (selectedGroup.Count > 0)
                {
                    var result = (from a in context.Approvals
                                  join j in context.Jobs on a.Job equals j.ID
                                  join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                  join u in context.Users on ac.Collaborator equals u.ID
                                  join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                  join ug in context.UserGroups on ugu.UserGroup equals ug.ID
                                  join acd in context.ApprovalCollaboratorDecisions on ac.Collaborator equals acd.Collaborator
                                  join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
                                  join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow
                                  where
                                  selectedGroup.Contains(ug.ID) &&
                                  ac.Approval == a.ID && ac.Collaborator == u.ID
                                   && phasedApprovas.Contains(a.ID)
                                   && acd.Approval == a.ID && acd.Collaborator == ac.Collaborator
                                   && ac.Phase == ajp.ID && acd.Phase == ac.Phase && ajw.Job == j.ID && acd.Collaborator == u.ID
                                  let hasOpenedFile = (from apuvi in context.ApprovalUserViewInfoes
                                                       where apuvi.Approval == a.ID && apuvi.User == u.ID && apuvi.Phase == ajp.ID
                                                       select apuvi).FirstOrDefault()
                                  select new UserActivity_Report()
                                  {
                                      FullName = u.GivenName + " " + u.FamilyName,
                                      UserName = u.Username,
                                      FileName = a.FileName,
                                                          DecisionId = (acd.Decision != null ? acd.Decision : (hasOpenedFile != null ? 6 : 0)),
                                                          date = acd.CompletedDate != null ? acd.CompletedDate : hasOpenedFile != null ? hasOpenedFile.ViewedDate : ac.AssignedDate ,
                                      Version = a.Version,
                                      Phase = ajp.Name,
                                      Annotation = (from aa in context.ApprovalAnnotations
                                                    join u1 in context.Users on aa.Creator equals u1.ID
                                                    join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                    join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                             where a1.ID == a.ID && aa.Parent == null && aa.Creator == u.ID && aa.Phase == ajp.ID
                                                                        select aa.ID).ToList()
                                                                                                   ,
                                      AnnotationReplay = (from aa in context.ApprovalAnnotations
                                                          join u1 in context.Users on aa.Creator equals u1.ID
                                                          join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                          join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                                   where a1.ID == a.ID && aa.Parent != null && aa.Creator == u.ID && aa.Phase == ajp.ID
                                                                              select aa.ID).ToList()
                                  }).ToList();

                    approvalUserActivityReportViewData.AddRange(result);


                }
                else
                {
                    var result_ = (from a in context.Approvals
                                   join j in context.Jobs on a.Job equals j.ID
                                   join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                   join u in context.Users on ac.Collaborator equals u.ID
                                   join acd in context.ApprovalCollaboratorDecisions on ac.Collaborator equals acd.Collaborator
                                   join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
                                   join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow
                                   where
                                   ac.Approval == a.ID && ac.Collaborator == u.ID
                                    && phasedApprovas.Contains(a.ID)
                                    && acd.Approval == a.ID && acd.Collaborator == ac.Collaborator
                                    && ac.Phase == ajp.ID && acd.Phase == ac.Phase && ajw.Job == j.ID && acd.Collaborator == u.ID
                                   let hasOpenedFile = (from apuvi in context.ApprovalUserViewInfoes
                                                        where apuvi.Approval == a.ID && apuvi.User == u.ID && apuvi.Phase == ajp.ID
                                                        select apuvi).FirstOrDefault()

                                   select new UserActivity_Report()
                                   {
                                       FullName = u.GivenName + " " + u.FamilyName,
                                       UserName = u.Username,
                                       FileName = a.FileName,
                                                          DecisionId = (acd.Decision != null ? acd.Decision : (hasOpenedFile != null ? 6 : 0)),
                                                          date = acd.CompletedDate != null ? acd.CompletedDate : hasOpenedFile != null ? hasOpenedFile.ViewedDate : ac.AssignedDate,
                                       Version = a.Version,
                                       Phase = ajp.Name,
                                       Annotation = (from aa in context.ApprovalAnnotations
                                                     join u1 in context.Users on aa.Creator equals u1.ID
                                                     join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                     join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                              where a1.ID == a.ID && aa.Parent == null && aa.Creator == u.ID && aa.Phase == ajp.ID
                                                                        select aa.ID).ToList() ,

                                       AnnotationReplay = (from aa in context.ApprovalAnnotations
                                                           join u1 in context.Users on aa.Creator equals u1.ID
                                                           join ap2 in context.ApprovalPages on aa.Page equals ap2.ID
                                                           join a1 in context.Approvals on ap2.Approval equals a1.ID
                                                                             where a1.ID == a.ID && aa.Parent != null && aa.Creator == u.ID && aa.Phase == ajp.ID
                                                                              select aa.ID).ToList()
                                   }).ToList();
                    approvalUserActivityReportViewData.AddRange(result_);
                }
            }

            var res = (from a in approvalUserActivityReportViewData
                       select new ApprovalUserActivityReportViewData()
                       {
                           FullName = a.FullName,
                           UserName = a.UserName,
                           FileName = a.FileName,
                           DecisionId = a.DecisionId,
                           date = a.date ,
                           Version = a.Version,
                           Phase = a.Phase,
                           AnnotationCount = a.Annotation.Count() ,
                           AnnotationReplayCount = a.AnnotationReplay.Count() ,
                       }).ToList();

                       

            return res;
        }


        public static ApprovalVersionWorkflowDetails GetApprovalVersionUserActivityReport(int versionId, Account loggedAccount, int userId, int userLocale, GMGColorDAL.Role.RoleName roleName, DbContextBL context)
        {
            List<WorkflowPhase> phases;
            var approvalVersion = new ApprovalVersionWorkflowDetails();
            List<WorkflowPhase> displayList = new List<WorkflowPhase>();
            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccount.ID, 1, false, context);

            var currentVersion = (from j in context.Jobs
                                  join a in context.Approvals on j.ID equals a.Job
                                  join ajpa in context.ApprovalJobPhaseApprovals on a.ID equals ajpa.Approval
                                  join ajp in context.ApprovalJobPhases on ajpa.Phase equals ajp.ID
                                  let isMaxVersion = (from v in context.Approvals
                                                      where v.Job == j.ID
                                                      select v.Version).Max() == a.Version
                                  where a.ID == versionId
                                  select new
                                  {
                                      isMaxVersion,
                                      Position = ajp.Position,
                                      IsLocked = a.IsLocked
                                  }
                              ).FirstOrDefault();

            var currentPhase = (from a in context.Approvals
                                where a.ID == versionId
                                select a.CurrentPhase).FirstOrDefault();

            var phaseHistory = (from j in context.Jobs
                                join a in context.Approvals on j.ID equals a.Job

                                let phasesAndApproval = (from ajw in context.ApprovalJobWorkflows
                                                         join ajpa in context.ApprovalJobPhaseApprovals on a.ID equals ajpa.Approval
                                                         where j.ID == ajw.Job
                                                         select new
                                                         {
                                                             ApprovalID = ajpa.Approval,
                                                             PhaseID = ajpa.Phase
                                                         }).ToList()
                                where a.ID == versionId
                                select new
                                {
                                    JobOwner = j.JobOwner,
                                    a.IsLocked,
                                    Phases = phasesAndApproval
                                }).FirstOrDefault();

            approvalVersion.Phases = (from j in context.Jobs
                                      join a in context.Approvals on j.ID equals a.Job
                                      join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
                                      join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow

                                      let phaseTemplateGuid = (from ap in context.ApprovalPhases
                                                               where ajp.PhaseTemplateID == ap.ID
                                                               select ap.Guid).FirstOrDefault()
                                      let collaborators = (from ac in context.ApprovalCollaborators
                                                           where ajp.ID == ac.Phase && ac.Approval == versionId
                                                           select ac.Collaborator).ToList()

                                      join acd in context.ApprovalCollaboratorDecisions on ajp.ID equals acd.Phase
                                      where a.ID == versionId && (collaborators.Contains(userId) || userId == j.JobOwner || (collaborateSettings.ShowAllFilesToAdmins && roleName == GMGColorDAL.Role.RoleName.AccountAdministrator))
                                      let wasForced = (from ajpa in context.ApprovalJobPhaseApprovals
                                                       where ajpa.Approval == a.ID && ajp.ID == ajpa.Phase
                                                       select ajpa.PhaseMarkedAsCompleted).FirstOrDefault()
                                      select new WorkflowPhase()
                                      {
                                          ID = ajp.ID,
                                          Name = ajp.Name,
                                          Position = ajp.Position,
                                          Status = null,
                                          Deadline = ajp.Deadline,
                                          ForceApproved = wasForced,
                                          Guid = phaseTemplateGuid,
                                          IsActive = ajp.IsActive
                                      }).Distinct().ToList();

            var approval = (from a in context.Approvals
                            where a.ID == versionId
                            select new { a.FileName, a.Version }).FirstOrDefault();


            var loggedUserIsJobOwner = phaseHistory.JobOwner == userId;
            approvalVersion.objLoggedAccount = loggedAccount;
            approvalVersion.Id = versionId;
            approvalVersion.CanEditPhaseDeadline = (loggedUserIsJobOwner || (roleName == GMGColorDAL.Role.RoleName.AccountAdministrator) || roleName == GMGColorDAL.Role.RoleName.AccountManager);
            approvalVersion.FileName = approval.FileName;
            approvalVersion.VersionNumber = approval.Version;
            approvalVersion.LoggedUserIsJobOwner = loggedUserIsJobOwner;


            //get all phasese except for the pending ones 
            var completedOrInProgressPhases = approvalVersion.Phases.Where(p => phaseHistory.Phases.Select(i => i.PhaseID).ToList().Contains(p.ID)).ToList();

            var activePhases = approvalVersion.Phases.Where(p => p.IsActive).Select(p => p.ID).ToList();
            approvalVersion.IsMaxPhaseCompleted = activePhases.All(p => phaseHistory.Phases.Select(i => i.PhaseID).ToList().Contains(p)) && currentVersion.IsLocked;
            approvalVersion.CurrentPhasePosition = approvalVersion.Phases.Where(ap => ap.ID == currentPhase).Select(ap => ap.Position).FirstOrDefault();
            approvalVersion.IsMaxVersion = currentVersion.isMaxVersion;

            if (currentVersion != null && currentVersion.isMaxVersion)
            {
                var pendingPhases = approvalVersion.Phases.Where(p => !phaseHistory.Phases.Select(i => i.PhaseID).ToList().Contains(p.ID) && p.Position > currentVersion.Position).ToList();

                var maxPhase = completedOrInProgressPhases.OrderByDescending(m => m.Position).FirstOrDefault();

                bool isCompleted = false;
                if (maxPhase != null)
                {
                    isCompleted = IsPhaseCompleted(maxPhase.ID, versionId, context);
                }

                foreach (var phase in completedOrInProgressPhases)
                {
                    if (phase.ID == currentPhase && !isCompleted)
                    {
                        phase.Status = currentVersion.IsLocked && IsApprovalRejected(versionId, context) ? GMGColor.Resources.Resources.lblPhaseStatusRejected : GMGColor.Resources.Resources.lblPhaseStatusInProgress;
                    }
                    else
                    {
                        phase.Status = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
                    }

                    displayList.Add(new WorkflowPhase()
                    {
                        ID = phase.ID,
                        Name = phase.Name,
                        Position = phase.Position,
                        Status = phase.Status,
                        Deadline = phase.Deadline,
                        PhaseVersionDetails = phase.PhaseVersionDetails,
                        ForceApproved = phase.ForceApproved,
                        Guid = phase.Guid,
                        IsActive = phase.IsActive
                    });
                }

                foreach (var pendingPhase in pendingPhases)
                {
                    displayList.Add(new WorkflowPhase()
                    {
                        ID = pendingPhase.ID,
                        Name = pendingPhase.Name,
                        Position = pendingPhase.Position,
                        Deadline = pendingPhase.Deadline,
                        Status = pendingPhase.IsActive ? GMGColor.Resources.Resources.lblPhaseStatusPending : GMGColor.Resources.Resources.lblInactivePhase,
                        Guid = pendingPhase.Guid,
                        IsActive = pendingPhase.IsActive,
                        InPending = true
                    });
                }

                phases = displayList.OrderBy(d => d.Position).ToList();
                PopulateApprovalPhases(ref phases, versionId, loggedAccount, userId, userLocale, roleName, context);
                approvalVersion.Phases = phases;
                return approvalVersion;
            }

            foreach (var phase in completedOrInProgressPhases)
            {
                if (phase.ID == currentPhase)
                {
                    phase.Status = GMGColor.Resources.Resources.lblPhaseStatusRejected;
                }
                else if (phase.ID < currentPhase)
                {
                    phase.Status = GMGColor.Resources.Resources.lblPhaseStatusCompleted;
                }
                displayList.Add(new WorkflowPhase()
                {
                    ID = phase.ID,
                    Name = phase.Name,
                    Position = phase.Position,
                    Deadline = phase.Deadline,
                    Status = phase.Status,
                    ForceApproved = phase.ForceApproved,
                    Guid = phase.Guid,
                    IsActive = phase.IsActive
                });
            }
            phases = displayList.OrderBy(d => d.Position).ToList();

            // retreive collaborators for each phase
            PopulateApprovalPhases(ref phases, versionId, loggedAccount, userId, userLocale, roleName, context);
            approvalVersion.Phases = phases;

            return approvalVersion;
        }


        public static List<GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog> GetApprovalWorkflowAnnotationToDosDetails(int versionId, Account loggedAccount, GMGColorDAL.User userDetails, GMGColorDAL.Role.RoleName roleName, DbContextBL context)
		{
			var phaseAnnotationsDetailsList = new List<GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog>();

			var internalCollaborators = (from ap in context.Approvals
										 join acl in context.ApprovalCollaborators on ap.ID equals acl.Approval
										 join aclRole in context.ApprovalCollaboratorRoles on acl.ApprovalCollaboratorRole equals aclRole.ID
										 join u in context.Users on acl.Collaborator equals u.ID
										 join j in context.Jobs on ap.Job equals j.ID
										 let annotationAndReplies = (from ann in context.ApprovalAnnotations
																	 join app in context.ApprovalPages on ap.ID equals app.Approval
																	 join aas in context.ApprovalAnnotationStatus on ann.Status equals aas.ID
                                                                     let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == versionId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()
																	 where
																		 ann.Page == app.ID &&
																		 ann.Creator == acl.Collaborator &&
																		 ann.Phase == ap.CurrentPhase &&
                                                                         (!publicPrivateAnnotations || userDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == userDetails.ID)
																	 select new
																	 {
																		 aas.Key,
																		 ann.Comment,
																		 ann.ModifiedDate,
																	 }

																	 )
										 where ap.ID == versionId && acl.Phase == ap.CurrentPhase && annotationAndReplies.Any()
										 select new
										 {
											 Annotations = annotationAndReplies,
											 Name = u.GivenName + " " + u.FamilyName,
                                             Userstatus = u.Status,


										 }
											).ToList();

			var extrenalCollaborator = (from sh in context.SharedApprovals
										join ap in context.Approvals on sh.Approval equals ap.ID
										join exc in context.ExternalCollaborators on sh.ExternalCollaborator equals exc.ID
										let annotationsAndReplies = (from ann in context.ApprovalAnnotations
																	 join app in context.ApprovalPages on ap.ID equals app.Approval
																	 join aas in context.ApprovalAnnotationStatus on ann.Status equals aas.ID
																	 where
																		 ann.Page == app.ID &&
																		 ann.ExternalCreator == sh.ExternalCollaborator &&
																		 ann.Phase == ap.CurrentPhase
																	 select new
																	 {
																		 aas.Key,
																		 ann.Comment,
																		 ann.ModifiedDate,
																	 })
										where
											ap.ID == versionId && sh.Phase == ap.CurrentPhase &&
											annotationsAndReplies.Any()
										select new
										{
											Annotations = annotationsAndReplies,
											Name = exc.GivenName + " " + exc.FamilyName,
                                            Userstatus = (int)UserStatus.Active,
										}).ToList();


			var AnnotationsList = new List<GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog>();
			foreach (var collaborator in internalCollaborators.Concat(extrenalCollaborator))
			{
				foreach (var annotation in collaborator.Annotations)
				{
					var statusChangeViewModel = new GMGColorDAL.CustomModels.ApprovalAnnotationStatusChangeLog();

					statusChangeViewModel.CollaboratorName = collaborator.Name;
					statusChangeViewModel.AnnotationComment = annotation.Comment;
                    statusChangeViewModel.UserStatus = collaborator.Userstatus;
					DateTime dateInUserTime = GMGColorFormatData.GetUserTimeFromUTC(annotation.ModifiedDate, loggedAccount.TimeZone);
					statusChangeViewModel.ModifiedDate = GMGColorFormatData.GetFormattedDate(dateInUserTime, loggedAccount.DateFormat1.Pattern);
					statusChangeViewModel.ModifiedDateHour = dateInUserTime.ToString("h:mm tt");
					statusChangeViewModel.Status = ApprovalAnnotationStatu.GetLocalizedApprovalAnnotationStatus(annotation.Key);
					AnnotationsList.Add(statusChangeViewModel);
				}
				phaseAnnotationsDetailsList = AnnotationsList;
			}
			return phaseAnnotationsDetailsList;
		}

		public static void PopulateApprovalPhases(ref List<WorkflowPhase> phases, int versionId, Account loggedAccount, int userId, int userLocale, GMGColorDAL.Role.RoleName roleName, DbContextBL context)
		{

			var collaboratorsInfo = (from apv in context.Approvals
									 let workflowInfo = (from apw in context.ApprovalJobPhases
														 join apjw in context.ApprovalJobWorkflows on apw.ApprovalJobWorkflow equals apjw.ID
														 where apv.CurrentPhase == apw.ID
														 select new
														 {
															 appPhaseName = apw.Name,
															 approvalWorkflowName = apjw.Name,
															 appId = apv.CurrentPhase
														 }).FirstOrDefault()
									 where apv.ID == versionId
									 select new
									 {
										 objApprovalVersion = apv,
										 workflowInfo
									 }).FirstOrDefault();

            var jobStatus = (from j in context.Jobs
                             join js in context.JobStatus on j.Status equals js.ID
                             join a in context.Approvals on j.ID equals a.Job
                             where a.ID == versionId
                             select js.Name).FirstOrDefault();

            JobStatu.Status status  = (JobStatu.Status)Enum.Parse(typeof(JobStatu.Status), jobStatus.Replace(" ", ""), true);

			var objeApprovalVersion = collaboratorsInfo.objApprovalVersion;

			var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccount.ID, 1, false, context);

			// retreive collaborators for each phase
			foreach (var approvalPhase in phases)
			{
				approvalPhase.PhaseVersionDetails = new ApprovalDetailsModel.VersionDetailsModel
				{
					ColloaboratorActivities = new List<ApprovalDetailsCollaboratorActivity>(),
                    DeletedColloaboratorActivitiesWhoAnnotated = new List<ApprovalDetailsCollaboratorActivity>(),
					ExternalColloaboratorActivities = new List<ApprovalDetailsCollaboratorActivity>(),
                    DeletedExternalColloaboratorActivitiesWhoAnnotated = new List<ApprovalDetailsCollaboratorActivity>(),
					objLoggedAccount = loggedAccount,
					LoggedUserRole = roleName,
					LoggedUserID = userId,
					DisableSOADIndicator = collaborateSettings.DisableSOADIndicator,
                    DisableTagwordsInDashbord = collaborateSettings.DisableTagwordsInDashbord,
                    JobStatus = status,
                    ID = versionId
                };

                var loggedUserDetails = (from u in context.Users where u.ID == userId && u.Account == loggedAccount.ID select u).FirstOrDefault();
				approvalPhase.PhaseVersionDetails.ColloaboratorActivities =
					GetApprovalPhaseCollaboratorActivities(objeApprovalVersion, loggedUserDetails, approvalPhase.ID, versionId, userLocale, loggedAccount, context);
                approvalPhase.PhaseVersionDetails.DeletedColloaboratorActivitiesWhoAnnotated =
                    GetApprovalPhaseDeletedCollaboratorActivities(objeApprovalVersion, approvalPhase.ID, versionId, userLocale, loggedAccount, context);
                approvalPhase.PhaseVersionDetails.ExternalColloaboratorActivities =
					GetApprovalPhaseExternalCollaboratorActivities(objeApprovalVersion, loggedUserDetails, approvalPhase.ID, versionId, loggedAccount, context);
                approvalPhase.PhaseVersionDetails.DeletedExternalColloaboratorActivitiesWhoAnnotated =
                    GetApprovalPhaseExternalDeletedCollaboratorActivities(objeApprovalVersion, approvalPhase.ID, versionId, loggedAccount, context);
                if (collaboratorsInfo != null)
				{
					approvalPhase.PhaseVersionDetails.objApprovalVersion = collaboratorsInfo.objApprovalVersion;
				}
			}
		}

        public static List<ApprovalDetailsCollaboratorActivity> GetApprovalPhaseDeletedCollaboratorActivities(Approval objApproval, int? phaseId, int versionId, int userLocale, Account loggedAccount, GMGColorContext context)
        {
            List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();

            var Role = (from acl in context.ApprovalCollaboratorRoles
                        select acl.ID).FirstOrDefault();

            var listOfUsersInApprovalCollaborator = (from ac in context.ApprovalCollaborators
                                                     join a in context.Approvals on ac.Approval equals a.ID
                                                     where a.ID == objApproval.ID && ac.Phase == phaseId
                                                     select ac.Collaborator).Distinct().ToList();

            var deletedInternalUserIdsWhoMadeAnnotations = (from aa in context.ApprovalAnnotations
                                                            join ap in context.ApprovalPages on aa.Page equals ap.ID
                                                            join a in context.Approvals on ap.Approval equals a.ID
                                                            where a.ID == objApproval.ID && !listOfUsersInApprovalCollaborator.Contains((int)aa.Creator) && !(aa.Creator == null) && (aa.Phase == phaseId)
                                                            select aa.Creator).Distinct().ToList();

            List<int> annotationIdsOfDeletedInternalUsers = new List<int>();
            foreach (var u in deletedInternalUserIdsWhoMadeAnnotations)
            {
                var annotations = (from aa in context.ApprovalAnnotations
                                   join ap in context.ApprovalPages on aa.Page equals ap.ID
                                   join a in context.Approvals on ap.Approval equals a.ID
                                   where a.ID == objApproval.ID && aa.Creator == u && aa.Phase == phaseId
                                   select aa.ID).FirstOrDefault();

                annotationIdsOfDeletedInternalUsers.Add(annotations);
            }

            var collaboratorsList = (from aa in context.ApprovalAnnotations
                                     join ap in context.ApprovalPages on aa.Page equals ap.ID
                                     join a in context.Approvals on ap.Approval equals a.ID
                                     join u in context.Users on aa.Creator equals u.ID
                                     join us in context.UserStatus on u.Status equals us.ID
                                     join j in context.Jobs on a.Job equals j.ID
                                     join ac in context.Accounts on j.Account equals ac.ID
                                     join ur in context.UserRoles on u.ID equals ur.User
                                     join r in context.Roles on ur.Role equals r.ID

                                     let decisions = (from acd in context.ApprovalCollaboratorDecisions
                                                      where acd.Approval == versionId && acd.Collaborator == aa.Creator && acd.Phase == a.CurrentPhase
                                                      select acd).ToList()
                                     let annotationAndReplies = (from ann in context.ApprovalAnnotations
                                                                 join app in context.ApprovalPages on a.ID equals app.Approval
                                                                 where ann.Page == app.ID && ann.Creator == aa.Creator && ann.Phase == a.CurrentPhase
                                                                 select ann.Parent).ToList()
                                     let isPDM = (from ajp in context.ApprovalJobPhases
                                                  where ajp.ID == phaseId && a.Creator == ajp.PrimaryDecisionMaker
                                                  select ajp.ID).Any()
                                     let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                     where a.ID == objApproval.ID && (us.Key != "D" || decisions.Any(t => t.Decision != null)) && (r.Key != "NON" || decisions.Any(t => t.Decision != null)) 
                                     && r.AppModule == appModuleID && aa.Phase == phaseId && annotationIdsOfDeletedInternalUsers.Contains(aa.ID)
                                     select new
                                     {
                                         GivenName = u.GivenName,
                                         FamilyName = u.FamilyName,
                                         Username = u.Username,
                                         EmailAddress = u.EmailAddress,
                                         Collaborator = aa.Creator,
                                         AssignedDate = aa.AnnotatedDate,
                                         ApprovalCollaboratorRole = Role,
                                         TimeZone = ac.TimeZone,
                                         roleKey = "RDO",
                                         annotationAndReplies,
                                         decisions,
                                         PDM = isPDM,
                                         Phase = aa.Phase
                                     }).ToList();

            foreach (var objAppCollaborator in collaboratorsList)
            {
                List<ApprovalCollaboratorDecision> lstAppColDesHistory = objAppCollaborator.decisions;

                string completedHtml = string.Empty;
                string openedHtml = string.Empty;
                DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAppCollaborator.TimeZone);
                DateTimeSpan dateSpan;
                openedHtml = "-";
               
                if (lstAppColDesHistory.Count > 0 && lstAppColDesHistory[0].CompletedDate != null)
                {
                    DateTime dtCompleted = lstAppColDesHistory[0].CompletedDate.Value;
                    dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, objAppCollaborator.TimeZone);
                    dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
                    completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
                }
                else
                {
                    completedHtml = "-";
                }

                var approvalCustomDecision = ApprovalCustomDecision.GetCustomDecisionsByAccountId(loggedAccount.ID, userLocale, context);

                activityList.Add(new ApprovalDetailsCollaboratorActivity
                {
                    UserID = (int)objAppCollaborator.Collaborator,
                    Name = objAppCollaborator.GivenName + " " + objAppCollaborator.FamilyName,
                    Role = ApprovalCollaboratorRole.GetLocalizedRoleName(objAppCollaborator.roleKey),
                    Sent = objAppCollaborator.AssignedDate,
                    Opened = openedHtml,
                    Annotations = objAppCollaborator.annotationAndReplies.Count(t => t == null),
                    Replies = objAppCollaborator.annotationAndReplies.Count(t => t != null),
                    DecisionMade = (lstAppColDesHistory.Count > 0)
                                                ? (lstAppColDesHistory[0].Decision != null)
                                                              ? (approvalCustomDecision.Any(d => d.DecisionId == lstAppColDesHistory[0].Decision && !string.IsNullOrEmpty(d.Name)))
                                                                             ? approvalCustomDecision.Where(d => d.DecisionId == lstAppColDesHistory[0].Decision).Select(d => d.Name).FirstOrDefault()
                                                                             : ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDesHistory[0].ApprovalDecision)
                                                              : GMGColor.Resources.Resources.lblNone
                                                : GMGColor.Resources.Resources.lblNone,
                    Completed = completedHtml,
                    IsAccessRevoked = true,
                    IsPrimaryDecisionMaker = objAppCollaborator.PDM,
                    CollaboratorRoleKey = objAppCollaborator.roleKey,
                    UserName = objAppCollaborator.Username,
                    EmailAddress = objAppCollaborator.EmailAddress,
                    PhaseId = objAppCollaborator.Phase
                });
            }
            return activityList;
        }
        
        public static List<ApprovalDetailsCollaboratorActivity> GetApprovalPhaseCollaboratorActivities(Approval objApproval, GMGColorDAL.User loggedUserDetails, int? phaseId, int versionId, int userLocale, Account loggedAccount, GMGColorContext context)
		{
			List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();

            //Select collaborators that have not been deleted or that have made a decision or an annotation for the specified approval
            var collaboratorsList = (from ap in context.Approvals
                                     join acl in context.ApprovalCollaborators on ap.ID equals acl.Approval
                                     join aclRole in context.ApprovalCollaboratorRoles on acl.ApprovalCollaboratorRole equals aclRole.ID
                                     join u in context.Users on acl.Collaborator equals u.ID
                                     join j in context.Jobs on ap.Job equals j.ID
                                     join ac in context.Accounts on j.Account equals ac.ID
                                     join us in context.UserStatus on u.Status equals us.ID
                                     join ur in context.UserRoles on u.ID equals ur.User
                                     join r in context.Roles on ur.Role equals r.ID

                                     let lastOpenedDate = (from auv in context.ApprovalUserViewInfoes
                                                           where auv.Approval == objApproval.ID && auv.User == acl.Collaborator && auv.Phase == acl.Phase
                                                           orderby auv.ViewedDate descending
                                                           select auv.ViewedDate).FirstOrDefault()

                                     //join ajp in context.ApprovalJobPhaseApprovals on ap.ID equals ajp.Approval
                                     let decisions = (from acd in context.ApprovalCollaboratorDecisions
                                                      where acd.Approval == versionId && acd.Collaborator == acl.Collaborator && acd.Phase == acl.Phase
                                                      select acd).ToList()
                                     let annotationAndReplies = (from ann in context.ApprovalAnnotations
                                                                 join app in context.ApprovalPages on ap.ID equals app.Approval

                                                                 let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == versionId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

                                                                 where ann.Page == app.ID && ann.Creator == acl.Collaborator && ann.Phase == acl.Phase &&
                                                                 (!publicPrivateAnnotations || loggedUserDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUserDetails.ID)
                                                                 select new { ann.Parent, ann.IsPhaseRerun }).ToList()
                                     let isPDM = (from ajp in context.ApprovalJobPhases
                                                  where ajp.ID == phaseId && acl.Collaborator == ajp.PrimaryDecisionMaker
                                                  select ajp.ID).Any()
                                     let selectedForPublicAnnotations = (from ca in context.Approvals where ca.ID == versionId && ca.PrimaryDecisionMaker == u.ID select ca.PrivateAnnotations).FirstOrDefault()
                                     let substituteuser = (from u in context.Users
                                                           where u.ID == acl.SubstituteUser
                                                           select " (To " + u.GivenName + " " + u.FamilyName + ")").FirstOrDefault()
                                     let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                     where ap.ID == versionId && (us.Key != "D" || decisions.Any(t => t.Decision != null)) && (r.Key != "NON" || decisions.Any(t => t.Decision != null)) && r.AppModule == appModuleID && acl.Phase == phaseId
                                     select new
                                     {
                                         u.GivenName,
                                         u.FamilyName,
                                         u.Username,
                                         u.EmailAddress,
                                         acl.Collaborator,
                                         acl.AssignedDate,
                                         acl.ApprovalCollaboratorRole,
                                         ac.TimeZone,
                                         UserStatus = u.Status,
                                         roleKey = aclRole.Key,
                                         annotationAndReplies,
                                         decisions,
                                         PDM = isPDM,
                                         lastOpenedDate,
                                         acl.Phase,
                                         substituteuser = substituteuser == null ? "" : substituteuser,
                                         selectedForPublicAnnotations,
                                         acl.IsReminderMailSent
                                     }).ToList();
            bool isApprovalTypeVideo = (from a in context.Approvals
                                        join ft in context.FileTypes on a.FileType equals ft.ID
                                        where a.ID == objApproval.ID && ft.Type == "video"
                                        select ft.ID).Any();
          
            int VideoAnnotationReport = !isApprovalTypeVideo ? 1 : (from ann in context.ApprovalAnnotations
                                                                                          join app in context.ApprovalPages on ann.Page equals app.ID
                                                                                          join ap in context.Approvals on app.Approval equals ap.ID
                                                                                          join apf in context.ApprovalJobPhaseApprovals on ap.ID equals apf.Approval into tempSt
                                                                                          from subPhase in tempSt.DefaultIfEmpty()
                                                                                          let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

                                                                                          where ap.Job == objApproval.Job && ann.ShowVideoReport == true && ann.Parent == null
                                                                                          && (!publicPrivateAnnotations || loggedUserDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUserDetails.ID)
                                                                                          select ann.ID).Distinct().Count();
            foreach (var objAppCollaborator in collaboratorsList)
			{
				List<ApprovalCollaboratorDecision> lstAppColDesHistory = objAppCollaborator.decisions;

				string completedHtml = string.Empty;
				string openedHtml = string.Empty;
				DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAppCollaborator.TimeZone);
				DateTimeSpan dateSpan;

				if (objAppCollaborator.lastOpenedDate != default(DateTime))
				{
					DateTime dtOpened = objAppCollaborator.lastOpenedDate;
					dtOpened = GMGColorFormatData.GetUserTimeFromUTC(dtOpened, objAppCollaborator.TimeZone);
					dateSpan = DateTimeSpan.CompareDates(dtOpened, userCurTime);
					openedHtml = GMGColorFormatData.GetFormattedDate(dtOpened, loggedAccount, context) + "<br /><small>" + GMGColorFormatData.GetFormattedTime(dtOpened, loggedAccount) + "</small>";
				}
				else
				{
					openedHtml = "-";
				}

				if (lstAppColDesHistory.Count > 0 && lstAppColDesHistory[0].CompletedDate != null)
				{
					DateTime dtCompleted = lstAppColDesHistory[0].CompletedDate.Value;
					dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, objAppCollaborator.TimeZone);
					dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
					completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
				}
				else
				{
					completedHtml = "-";
				}

				var approvalCustomDecision = ApprovalCustomDecision.GetCustomDecisionsByAccountId(loggedAccount.ID, userLocale, context);

				activityList.Add(new ApprovalDetailsCollaboratorActivity
				{
					UserID = objAppCollaborator.Collaborator,
					Name = objAppCollaborator.GivenName + " " + objAppCollaborator.FamilyName + objAppCollaborator.substituteuser,
					Role = ApprovalCollaboratorRole.GetLocalizedRoleName(objAppCollaborator.roleKey),
					Sent = objAppCollaborator.AssignedDate,
					Opened = openedHtml,
                    Annotations = objAppCollaborator.annotationAndReplies.Select(x => x.Parent).Count(t => t == null),
                    Replies = objAppCollaborator.annotationAndReplies.Select(x => x.Parent).Count(t => t != null),
                    AnnotationsPhaseRerun = objAppCollaborator.annotationAndReplies.Select(x => x.IsPhaseRerun).Count(t => t != null && t == true),
                    DecisionMade = (lstAppColDesHistory.Count > 0)
												? (lstAppColDesHistory[0].Decision != null)
															  ? (approvalCustomDecision.Any(d => d.DecisionId == lstAppColDesHistory[0].Decision && !string.IsNullOrEmpty(d.Name)))
																			 ? approvalCustomDecision.Where(d => d.DecisionId == lstAppColDesHistory[0].Decision).Select(d => d.Name).FirstOrDefault()
																			 : ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDesHistory[0].ApprovalDecision)
															  : GMGColor.Resources.Resources.lblNone
												: GMGColor.Resources.Resources.lblNone,
					Completed = completedHtml,
					IsAccessRevoked = false,
					IsPrimaryDecisionMaker = objAppCollaborator.PDM,
					CollaboratorRoleKey = objAppCollaborator.roleKey,
					UserName = objAppCollaborator.Username,
					EmailAddress = objAppCollaborator.EmailAddress,
					PhaseId = objAppCollaborator.Phase,
                    selectedForPublicAnnotations = objAppCollaborator.selectedForPublicAnnotations,
                    TotalVideoAnnotationsReport = VideoAnnotationReport,
                    userStatus = objAppCollaborator.UserStatus,
                    IsReminderMailSent = objAppCollaborator.IsReminderMailSent
                });
			}
			return activityList;
		}

        public static List<ApprovalDetailsCollaboratorActivity> GetApprovalPhaseExternalDeletedCollaboratorActivities(Approval objApproval, int? phaseId, int versionId, Account loggedAccount, GMGColorContext context)
        {
            List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();


            var listOfUsersInSharedApprovals = (from sa in context.SharedApprovals
                                                join a in context.Approvals on sa.Approval equals a.ID
                                                where a.ID == objApproval.ID && sa.Phase == phaseId
                                                select sa.ExternalCollaborator).Distinct().ToList();

            var deletedExternalUserIdsWhoMadeAnnotations = (from aa in context.ApprovalAnnotations
                                                            join ap in context.ApprovalPages on aa.Page equals ap.ID
                                                            join a in context.Approvals on ap.Approval equals a.ID
                                                            where a.ID == objApproval.ID && !listOfUsersInSharedApprovals.Contains((int)aa.ExternalCreator) && !(aa.ExternalCreator == null) && (aa.Phase == phaseId)
                                                            select aa.ExternalCreator).Distinct().ToList();

            List<int> annotationIdsOfDeletedExternalUsers = new List<int>();
            foreach (var u in deletedExternalUserIdsWhoMadeAnnotations)
            {
                var annotations = (from aa in context.ApprovalAnnotations
                                   join ap in context.ApprovalPages on aa.Page equals ap.ID
                                   join a in context.Approvals on ap.Approval equals a.ID
                                   where a.ID == objApproval.ID && aa.ExternalCreator == u && aa.Phase == phaseId
                                   select aa.ID).FirstOrDefault();


                annotationIdsOfDeletedExternalUsers.Add(annotations);
            }
            var externalCollaborators = (from aa in context.ApprovalAnnotations
                                         join ap in context.ApprovalPages on aa.Page equals ap.ID
                                         join a in context.Approvals on ap.Approval equals a.ID
                                         join exc in context.ExternalCollaborators on aa.ExternalCreator equals exc.ID
                                         join j in context.Jobs on a.Job equals j.ID
                                         join ac in context.Accounts on j.Account equals ac.ID

                                         let lastOpenedDate = (from auv in context.ApprovalUserViewInfoes
                                                               where auv.Approval == objApproval.ID && auv.ExternalUser == aa.ExternalCreator && auv.Phase == aa.Phase
                                                               orderby auv.ViewedDate descending
                                                               select auv.ViewedDate).FirstOrDefault()

                                         let colDecisions = (from o in context.ApprovalCollaboratorDecisions
                                                             where o.ExternalCollaborator == aa.ExternalCreator && o.Approval == versionId && o.Phase == aa.Phase
                                                             select o).ToList()
                                         let annotationsAndReplies = (from ann in context.ApprovalAnnotations
                                                                      join app in context.ApprovalPages on a.ID equals app.Approval
                                                                      where ann.Page == app.ID && ann.ExternalCreator == aa.ExternalCreator && ann.Phase == aa.Phase
                                                                      select ann.Parent).ToList()
                                         let isPDM = (from ajp in context.ApprovalJobPhases
                                                      where ajp.ID == phaseId && aa.ExternalCreator == ajp.ExternalPrimaryDecisionMaker
                                                      select ajp.ID).Any()
                                         where a.ID == versionId && (exc.IsDeleted == false || colDecisions.Any(t => t.Decision != null)) && aa.Phase == phaseId
                                         && annotationIdsOfDeletedExternalUsers.Contains(aa.ID)
                                         select new
                                         {
                                             ExternalCollaborator = aa.ExternalCreator,
                                             colDecisions,
                                             Name = exc.GivenName + " " + exc.FamilyName,
                                             SharedDate = aa.AnnotatedDate,
                                             annotationsAndReplies,
                                             IsExpired = true,
                                             PDM = isPDM,
                                             EmailAddress = exc.EmailAddress,
                                             RoleKey = "RDO",
                                             lastOpenedDate,
                                             Phase = aa.Phase
                                         }).ToList();

            foreach (var objSharedApproval in externalCollaborators)
            {
                List<ApprovalCollaboratorDecision> lstAppColDecision = objSharedApproval.colDecisions;

                string completedHtml = "-";
                string openedHtml = "-";
                DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, loggedAccount.TimeZone);

                if (objSharedApproval.lastOpenedDate != default(DateTime))
                {
                    DateTime dtOpened = objSharedApproval.lastOpenedDate;
                    dtOpened = GMGColorFormatData.GetUserTimeFromUTC(dtOpened, loggedAccount.TimeZone);
                    openedHtml = GMGColorFormatData.GetFormattedDate(dtOpened, loggedAccount, context) + "<br /><small>" + GMGColorFormatData.GetFormattedTime(dtOpened, loggedAccount) + "</small>";
                }

                if (lstAppColDecision.Count > 0 && lstAppColDecision[0].CompletedDate != null)
                {
                    DateTime dtCompleted = lstAppColDecision[0].CompletedDate.Value;
                    dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, loggedAccount.TimeZone);
                    DateTimeSpan dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
                    completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
                }

                activityList.Add(new ApprovalDetailsCollaboratorActivity
                {
                    UserID = (int)objSharedApproval.ExternalCollaborator,
                    Name = objSharedApproval.Name,
                    Role = ApprovalCollaboratorRole.GetLocalizedRoleName(objSharedApproval.RoleKey),
                    Sent = objSharedApproval.SharedDate,
                    Opened = openedHtml,
                    Annotations = objSharedApproval.annotationsAndReplies.Count(t => t == null),
                    Replies = objSharedApproval.annotationsAndReplies.Count(t => t != null),
                    DecisionMade = (lstAppColDecision.Count > 0 && lstAppColDecision[0].ApprovalDecision != null) ? ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDecision[0].ApprovalDecision) : GMGColor.Resources.Resources.lblNone,
                    Completed = completedHtml,
                    IsPrimaryDecisionMaker = objSharedApproval.PDM,
                    IsAccessRevoked = true,
                    EmailAddress = objSharedApproval.EmailAddress,
                    PhaseId = objSharedApproval.Phase,
                    CollaboratorRoleKey = objSharedApproval.RoleKey
                });
            }
            return activityList;

        }
        
        public static List<ApprovalDetailsCollaboratorActivity> GetApprovalPhaseExternalCollaboratorActivities(Approval objApproval, GMGColorDAL.User loggedUserDetails, int? phaseId, int versionId, Account loggedAccount, GMGColorContext context)
		{
			var activityList = new List<ApprovalDetailsCollaboratorActivity>();

			var externalCollaborators = (from sh in context.SharedApprovals
										 join ap in context.Approvals on sh.Approval equals ap.ID
										 join exc in context.ExternalCollaborators on sh.ExternalCollaborator equals exc.ID
										 join aclRole in context.ApprovalCollaboratorRoles on sh.ApprovalCollaboratorRole equals aclRole.ID

										 let lastOpenedDate = (from auv in context.ApprovalUserViewInfoes
															   where auv.Approval == objApproval.ID && auv.ExternalUser == sh.ExternalCollaborator && auv.Phase == sh.Phase
															   orderby auv.ViewedDate descending
															   select auv.ViewedDate).FirstOrDefault()

										 let colDecisions = (from o in context.ApprovalCollaboratorDecisions
															 where o.ExternalCollaborator == sh.ExternalCollaborator && o.Approval == versionId && o.Phase == sh.Phase
															 select o).ToList()
										 let annotationsAndReplies = (from ann in context.ApprovalAnnotations
																	  join app in context.ApprovalPages on ap.ID equals app.Approval

                                                                      let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == versionId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

																	  where ann.Page == app.ID && ann.ExternalCreator == sh.ExternalCollaborator && ann.Phase == sh.Phase
                                                                      && (!publicPrivateAnnotations || loggedUserDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUserDetails.ID)
                                                                      select ann.Parent).ToList()
										 let isPDM = (from ajp in context.ApprovalJobPhases
													  where ajp.ID == phaseId && sh.ExternalCollaborator == ajp.ExternalPrimaryDecisionMaker
													  select ajp.ID).Any()
										 where ap.ID == versionId && (exc.IsDeleted == false || colDecisions.Any(t => t.Decision != null)) && sh.Phase == phaseId
										 select new
										 {
											 sh.ExternalCollaborator,
											 colDecisions,
											 Name = exc.GivenName + " " + exc.FamilyName,
											 sh.SharedDate,
											 annotationsAndReplies,
											 sh.IsExpired,
											 PDM = isPDM,
											 exc.EmailAddress,
											 RoleKey = aclRole.Key,
											 lastOpenedDate,
											 sh.Phase
										 }).ToList();

			foreach (var objSharedApproval in externalCollaborators)
			{
				List<ApprovalCollaboratorDecision> lstAppColDecision = objSharedApproval.colDecisions;

				string completedHtml = "-";
				string openedHtml = "-";
				DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, loggedAccount.TimeZone);

				if (objSharedApproval.lastOpenedDate != default(DateTime))
				{
					DateTime dtOpened = objSharedApproval.lastOpenedDate;
					dtOpened = GMGColorFormatData.GetUserTimeFromUTC(dtOpened, loggedAccount.TimeZone);
					openedHtml = GMGColorFormatData.GetFormattedDate(dtOpened, loggedAccount, context) + "<br /><small>" + GMGColorFormatData.GetFormattedTime(dtOpened, loggedAccount) + "</small>";
				}

				if (lstAppColDecision.Count > 0 && lstAppColDecision[0].CompletedDate != null)
				{
					DateTime dtCompleted = lstAppColDecision[0].CompletedDate.Value;
					dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, loggedAccount.TimeZone);
					DateTimeSpan dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
					completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
				}

				activityList.Add(new ApprovalDetailsCollaboratorActivity
				{
					UserID = objSharedApproval.ExternalCollaborator,
					Name = objSharedApproval.Name,
					Role = ApprovalCollaboratorRole.GetLocalizedRoleName(objSharedApproval.RoleKey),
					Sent = objSharedApproval.SharedDate,
					Opened = openedHtml,
					Annotations = objSharedApproval.annotationsAndReplies.Count(t => t == null),
					Replies = objSharedApproval.annotationsAndReplies.Count(t => t != null),
					DecisionMade = (lstAppColDecision.Count > 0 && lstAppColDecision[0].ApprovalDecision != null) ? ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDecision[0].ApprovalDecision) : GMGColor.Resources.Resources.lblNone,
					Completed = completedHtml,
					IsPrimaryDecisionMaker = objSharedApproval.PDM,
					IsAccessRevoked = objSharedApproval.IsExpired,
					EmailAddress = objSharedApproval.EmailAddress,
					PhaseId = objSharedApproval.Phase,
					CollaboratorRoleKey = objSharedApproval.RoleKey
				});
			}
			return activityList;
		}

        public static bool CheckApprovalDecisionsMade(int approvalId, int loggedUserId, DbContextBL context)
        {
            var jobStatus = (from a in context.Approvals
                             join j in context.Jobs on a.Job equals j.ID
                             where a.ID == approvalId
                             select j.Status).FirstOrDefault();
            if (jobStatus != 3)
            { 
            bool checkCollaboratorIsPresent = (from acd in context.ApprovalCollaboratorDecisions
                                               join a in context.Approvals on acd.Approval equals a.ID
                                               where acd.Approval == approvalId && acd.Phase == a.CurrentPhase && acd.Collaborator == loggedUserId
                                               select acd.Decision).Any();

            bool checkCollaboratorMadeDecision = (from acd in context.ApprovalCollaboratorDecisions
                                                  join a in context.Approvals on acd.Approval equals a.ID
                                                  where acd.Approval == approvalId && acd.Phase == a.CurrentPhase && acd.Decision != null && acd.Collaborator == loggedUserId
                                                  select acd.Decision).Any();
            if (checkCollaboratorIsPresent)
            {
                return !checkCollaboratorMadeDecision;
            }
            else
            {
                return checkCollaboratorMadeDecision;
            }
        }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Marks the current phase as completed and moves the approval to the next phase
        /// </summary>
        /// <param name="selectedApproval">Selected approval</param>
        /// <param name="context">Database context</param>
        public static ApprovalWorkflowInstantNotification MarkPhaseAsCompleted(int selectedApproval, int loggedUser, Account loggedAccount, DbContextBL context, out bool isLastPhaseCompleted)
		{
			isLastPhaseCompleted = false;
			var instantNotification = new ApprovalWorkflowInstantNotification();

			var approval = GetInfoAboutCurrentAndNextPhases(selectedApproval, context);
			if (approval != null)
			{
				approval.ApprovalJobPhaseApproval.PhaseMarkedAsCompleted = true;
				var nextPhaseId = approval.NextPhase != null ? approval.NextPhase.ID : 0;

				GMGColorLogging.log.InfoFormat(loggedAccount.ID, "MarkPhaseAsCompleted. Phase completed. Approval: {0} | NextPhaseID: {1}"
					, new object[] { selectedApproval, nextPhaseId });

				if (approval.NextPhase != null)
				{
					var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
					{
						Approval = selectedApproval,
						Phase = approval.NextPhase.ID,
						CreatedBy = loggedUser,
						CreatedDate = DateTime.UtcNow,
						ModifiedDate = DateTime.UtcNow,
						ModifiedBy = loggedUser
					};

					context.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);

					//move the approval to the next phase
					approval.Approval.CurrentPhase = approval.NextPhase.ID;
					approval.Approval.PrimaryDecisionMaker = approval.NextPhase.PrimaryDecisionMaker;
					approval.Approval.ExternalPrimaryDecisionMaker = approval.NextPhase.ExternalPrimaryDecisionMaker;
					approval.Approval.IsLocked = false;
					approval.Approval.SOADState = 0;

					var deadlineTrigger = (from fdt in context.PhaseDeadlineTriggers where fdt.ID == approval.NextPhase.DeadlineTrigger select fdt.Key).FirstOrDefault();
					var deadlineUnit = (from fdu in context.PhaseDeadlineUnits where fdu.ID == approval.NextPhase.DeadlineUnit select fdu.Key).FirstOrDefault();

					if (approval.NextPhase.Deadline == null && deadlineTrigger == (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
					{
						approval.NextPhase.Deadline = GetAutomaticPhaseDeadline(null, deadlineTrigger, approval.NextPhase.DeadlineUnitNumber, deadlineUnit);
					}
                   instantNotification.PhaseName = approval.NextPhase.Name;
				}
				else
				{
					approval.Approval.IsLocked = true;
					isLastPhaseCompleted = true;
					instantNotification.PhaseName = approval.PhaseName;
                    var job = (from j in context.Jobs
                               join a in context.Approvals on j.ID equals a.Job
                               where a.ID == selectedApproval
                               select j).FirstOrDefault();

                    job.Status = (int)JobStatu.Status.Completed - 5;

                    var approvalJobCompleted = new GMGColorNotificationService.Notifications.Approvals.ApprovalJobCompleted();

                    approvalJobCompleted.EventCreator = loggedUser;
                    approvalJobCompleted.ExternalCreator =  null;
                    approvalJobCompleted.InternalRecipient = approval.Approval.Owner;
                    approvalJobCompleted.CreatedDate = DateTime.UtcNow;
                    approvalJobCompleted.ApprovalsIds = new[] { approval.Approval.ID };
                    approvalJobCompleted.NewDecisionId = (int)JobStatu.ApprovalDecision.Approved;

                    NotificationServiceBL.CreateNotification(approvalJobCompleted, null, context, true, null);
                }
                var approvalCollaborationDecision = approval.Approval.ApprovalCollaboratorDecisions.Where(x => x.Collaborator == loggedUser).Select(x => x).FirstOrDefault();
                if (approvalCollaborationDecision != null)
                {
                    approvalCollaborationDecision.CompletedDate = DateTime.UtcNow;
                }

                instantNotification.ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
				instantNotification.Version = selectedApproval.ToString();
				instantNotification.Creator = loggedUser;
				instantNotification.MoveNext = true;
				instantNotification.Decision = string.Empty;

				GetPhaseDetails(approval, isLastPhaseCompleted, context, loggedAccount, ref instantNotification, false, true);
				context.SaveChanges();
			}
			return instantNotification;
		}

		public static ApprovalPhaseDetails GetInfoAboutCurrentAndNextPhases(int selectedApproval, DbContextBL context)
		{
			return (from a in context.Approvals
					join ajp in context.ApprovalJobPhases on a.CurrentPhase equals ajp.ID

					let workflowName = (from w in context.ApprovalJobWorkflows
										where w.ID == ajp.ApprovalJobWorkflow
										select w.Name).FirstOrDefault()

					let nextPhase = (from ajpw in context.ApprovalJobPhases
									 let phase = (from ajw in context.ApprovalJobPhases
												  where a.CurrentPhase == ajw.ID && ajw.IsActive
												  select new
												  {
													  ajw.ApprovalJobWorkflow,
													  ajw.Position
												  }).FirstOrDefault()
									 where ajpw.Position > phase.Position && ajpw.ApprovalJobWorkflow == phase.ApprovalJobWorkflow && ajpw.IsActive
									 orderby ajpw.Position
									 select ajpw).FirstOrDefault()

					let approvalJobPhase = (from ajpa in context.ApprovalJobPhaseApprovals
											where a.CurrentPhase == ajpa.Phase && a.ID == ajpa.Approval
											select ajpa).FirstOrDefault()

                    let approvalJobPhaseTrigger = (from ap in context.ApprovalPhases
                                                   join apt in context.ApprovalPhaseTrigger on ap.ID equals apt.ApprovalPhase
                                                   where ap.ID == ajp.PhaseTemplateID
                                                   select apt.ApprovalDecision.ToString()).ToList()

                    where a.ID == selectedApproval
					select new ApprovalPhaseDetails()
					{
						ApprovalTrigger = approvalJobPhaseTrigger,
						DecisionType = ajp.DecisionType,
						PrimaryDecisionMaker = a.PrimaryDecisionMaker.HasValue ? a.PrimaryDecisionMaker : a.ExternalPrimaryDecisionMaker,
						NextPhase = nextPhase,
						Approval = a,
						ApprovalJobPhaseApproval = approvalJobPhase,
						WorkflowName = workflowName,
						PhaseName = ajp.Name
					}).FirstOrDefault();
		}

		public static void GetPhaseDetails(ApprovalPhaseDetails approval, bool isLastPhaseCompleted, DbContextBL context, Account loggedAccount, ref ApprovalWorkflowInstantNotification instantNotification, bool isRejected = false, bool moveNext = false)
		{
			if (string.IsNullOrEmpty(instantNotification.PhaseName))
			{
				instantNotification.PhaseName = approval.PhaseName;
			}

			var dataOriginalTitle = String.Format("{0}: {1} <br>{2}: {3} <br> {4}: {5}", GMGColor.Resources.Resources.lblWorkflow, approval.WorkflowName, GMGColor.Resources.Resources.lblPhase, instantNotification.PhaseName, GMGColor.Resources.Resources.lblStatus, isLastPhaseCompleted ? GMGColor.Resources.Resources.lblCompleted : GMGColor.Resources.Resources.lblInProgress);
			instantNotification.WorkflowTooltip = "<img rel=\"tooltip\" src=\"/Content/img/green-workflow.png\" class=\"approval-icon phase-icon\" data-html=\"true\" data-title=\"" + dataOriginalTitle + "\" data-original-title=\"\" title=\"\">";

			string decisionMakers;
			if (approval.NextPhase != null && moveNext && !isRejected)
			{
				instantNotification.NextPhase = GetNextPhaseName(approval.NextPhase.ID, context);
				decisionMakers = GetAndPopulateDecisionMakers(approval.NextPhase.ID, approval.Approval.ID, approval.NextPhase.PrimaryDecisionMaker, approval.NextPhase.ExternalPrimaryDecisionMaker, instantNotification, context);

				var deadline = approval.NextPhase.Deadline.HasValue ? GMGColorFormatData.GetUserTimeFromUTC(approval.NextPhase.Deadline.Value, loggedAccount.TimeZone) : (DateTime?)null;

				instantNotification.DeadLineDate = deadline.HasValue ? GMGColorFormatData.GetFormattedDate(deadline.Value, loggedAccount.DateFormat1.Pattern) : "-";
				instantNotification.DeadLineTime = deadline.HasValue ? GMGColorFormatData.GetFormattedTime(deadline.Value, loggedAccount) : string.Empty;
				instantNotification.IsPhaseDeadline = true;
			}
			else
			{
				decisionMakers = GetAndPopulateDecisionMakers(approval.Approval.CurrentPhase.GetValueOrDefault(), approval.Approval.ID, approval.Approval.PrimaryDecisionMaker, approval.Approval.ExternalPrimaryDecisionMaker, instantNotification, context);
			}
			instantNotification.DecisionMaker = string.IsNullOrEmpty(decisionMakers) ? string.Empty : decisionMakers;
			instantNotification.IsPhaseComplete = isLastPhaseCompleted;
		}

		private static string GetAndPopulateDecisionMakers(int phaseId, int approvalId, int? primaryDecisionMaker, int? externalPrimaryDecisionMaker, ApprovalWorkflowInstantNotification instantNotification, DbContextBL context)
		{
			if (primaryDecisionMaker.HasValue || externalPrimaryDecisionMaker.HasValue)
			{
				var primaryDecisionMakerName = GetPrimaryDecisionMakerName(primaryDecisionMaker, externalPrimaryDecisionMaker, context);
				instantNotification.PrimaryDecisionMaker = primaryDecisionMakerName ?? "-";
				instantNotification.DecisionType = (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker;
				return primaryDecisionMakerName;
			}

			instantNotification.DecisionType = (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup;
			instantNotification.PrimaryDecisionMaker = "-";
			return GetGroupPrimaryDecisionMakers(phaseId, approvalId, context);
		}

		private static string GetGroupPrimaryDecisionMakers(int nextPhaseId, int approvalId, DbContextBL context)
		{
			var internalPdm = (from u in context.Users
							   join acl in context.ApprovalCollaborators on u.ID equals acl.Collaborator
							   join acr in context.ApprovalCollaboratorRoles on acl.ApprovalCollaboratorRole equals acr.ID
							   where acr.Key == "ANR" && acl.Phase == nextPhaseId && acl.Approval == approvalId
							   select u.GivenName.Trim() + " " + u.FamilyName.Trim()).ToList();

			var externalPdm = (from ex in context.ExternalCollaborators
							   join sha in context.SharedApprovals on ex.ID equals sha.ExternalCollaborator
							   join acr in context.ApprovalCollaboratorRoles on sha.ApprovalCollaboratorRole equals acr.ID
							   where acr.Key == "ANR" && sha.Phase == nextPhaseId && sha.Approval == approvalId
							   select ex.GivenName.Trim() + " " + ex.FamilyName.Trim()).ToList();

			return String.Join(",", internalPdm.Concat(externalPdm));
		}

		public static ApprovalPhaseDetails GetInfoAboutCurrentAndNextPhases(int selectedApproval, int? currentPhase, DbContextBL context)
		{
			return (from ajp in context.ApprovalJobPhases
					join ad in context.ApprovalDecisions on ajp.ApprovalTrigger equals ad.ID
					join a in context.Approvals on ajp.ID equals a.CurrentPhase

					let workflowName = (from w in context.ApprovalJobWorkflows
										where w.ID == ajp.ApprovalJobWorkflow
										select w.Name).FirstOrDefault()

					let nextPhase = (from ajpw in context.ApprovalJobPhases
									 let phase = (from ajw in context.ApprovalJobPhases
												  where currentPhase == ajw.ID
												  select new
												  {
													  ajw.ApprovalJobWorkflow,
													  ajw.Position
												  }).FirstOrDefault()
									 where ajpw.Position > phase.Position && ajpw.ApprovalJobWorkflow == phase.ApprovalJobWorkflow && ajpw.IsActive
									 orderby ajpw.Position
									 select ajpw).FirstOrDefault()

					let approvalJobPhase = (from ajpa in context.ApprovalJobPhaseApprovals
											where a.CurrentPhase == ajpa.Phase && a.ID == ajpa.Approval
											select ajpa).FirstOrDefault()

                    let approvalJobPhaseTrigger = (from ap in context.ApprovalPhases
                    join apt in context.ApprovalPhaseTrigger on ap.ID equals apt.ApprovalPhase
                    where ap.ID == ajp.PhaseTemplateID select apt.ApprovalDecision.ToString()).ToList()

					where ajp.ID == currentPhase && a.ID == selectedApproval
					select new ApprovalPhaseDetails()
					{
						ApprovalTrigger = approvalJobPhaseTrigger,
						ApprovalTriggerPriority = ad.Priority,
						DecisionType = ajp.DecisionType,
						NextPhase = nextPhase,
						Approval = a,
						PrimaryDecisionMaker = a.PrimaryDecisionMaker.HasValue ? a.PrimaryDecisionMaker : a.ExternalPrimaryDecisionMaker,
						ApprovalJobPhaseApproval = approvalJobPhase,
						WorkflowName = workflowName,
						PhaseName = ajp.Name
					}).FirstOrDefault();
		}

		private static string GetPrimaryDecisionMakerName(int? primaryDecisionMaker, int? externalPrimaryDecisionMaker, DbContextBL context)
		{
			if (primaryDecisionMaker.HasValue)
			{
				return (from u in context.Users
						where u.ID == primaryDecisionMaker.Value
						select u.GivenName.Trim() + " " + u.FamilyName.Trim()).FirstOrDefault();
			}

			return (from ex in context.ExternalCollaborators
					where ex.ID == externalPrimaryDecisionMaker.Value
					select ex.GivenName.Trim() + " " + ex.FamilyName.Trim()).FirstOrDefault();
		}

		private static string GetNextPhaseName(int currentPhaseId, DbContextBL context)
		{
			var nextPhaseName = (from ajpw in context.ApprovalJobPhases
								 let phase = (from ajw in context.ApprovalJobPhases
											  where currentPhaseId == ajw.ID
											  select new
											  {
												  ajw.ApprovalJobWorkflow,
												  ajw.Position
											  }).FirstOrDefault()
								 where ajpw.Position > phase.Position && ajpw.ApprovalJobWorkflow == phase.ApprovalJobWorkflow
								 orderby ajpw.Position
								 select ajpw.Name).FirstOrDefault();

			return string.IsNullOrEmpty(nextPhaseName) ? "-" : nextPhaseName;
		}

		public static bool IsPhaseCompleted(int maxPhaseId, int versionId, DbContextBL context)
		{
			var maxPhaseDetails = (from ajp in context.ApprovalJobPhases
								   join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
								   join ajpa in context.ApprovalJobPhaseApprovals on ajp.ID equals ajpa.Phase
								   let decisions = (from acd in context.ApprovalCollaboratorDecisions
													where acd.Phase == ajp.ID && acd.Approval == versionId
													select acd).ToList()
								   where ajp.ID == maxPhaseId && ajpa.Approval == versionId
								   select new
								   {
									   Decisions = decisions,
									   ApprovalTrigger = ajp.ApprovalTrigger,
									   DecisionType = ajp.DecisionType,
									   ajp.PrimaryDecisionMaker,
									   ajp.ExternalPrimaryDecisionMaker,
									   ajpa.PhaseMarkedAsCompleted,
									   aph.ApprovalShouldBeViewedByAll,
								   }).FirstOrDefault();

			if (maxPhaseDetails != null)
			{
				if (maxPhaseDetails.PhaseMarkedAsCompleted != false)
				{
					return true;
				}
				else
				{
					//check for the max phase the decision type
					if (maxPhaseDetails.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup)
					{
						//check if all of the phase collaborators have made a decission of the required phase decision
						if (maxPhaseDetails.Decisions.Any() && maxPhaseDetails.Decisions.All(ad => ad.Decision == maxPhaseDetails.ApprovalTrigger))
						{
							return true;
						}
					}
					else
					{
						//check if approval PDM has made a decision of the required phase decision
						if (
							maxPhaseDetails.Decisions.Any(u => ((maxPhaseDetails.PrimaryDecisionMaker != null && u.Collaborator == maxPhaseDetails.PrimaryDecisionMaker) ||
																(maxPhaseDetails.ExternalPrimaryDecisionMaker != null && u.ExternalCollaborator == maxPhaseDetails.ExternalPrimaryDecisionMaker)) &&
																u.Decision == maxPhaseDetails.ApprovalTrigger) && maxPhaseDetails.ApprovalShouldBeViewedByAll == false)
						{
							return true;
						}
						else
						{
							//convert id to list with one item
							var listId = new List<int>() { versionId };
							var apwfDetails = ApprovalWorkflowBL.GetApprovalWorkflowPhaseDetails(listId, context);
							foreach (var item in apwfDetails)
							{
								if (item.Collaborators.All(c => c.HasViewed == true) && item.Collaborators.Any(c => c.IsPDM == true && item.PhaseTrigger.Contains(c.Decision)))
								{
									return true;
								}
							}
						}
					}
				}
			}
			return false;
		}
		#endregion

		public static LockApproval LockApprovalOnPhase(int? currentPhase, int approvalId, int? decisionId, int? loggedUserId, DbContextBL context, bool? isViewedByAll, bool? phaseApproved)
		{
			var lockApproval = new LockApproval();

			if (currentPhase != null)
			{
				var approvalPhase = GetInfoAboutCurrentAndNextPhases(approvalId, currentPhase, context);

                if (approvalPhase != null)
				{
                    var decisionShoulBeMadeByAll = (from a in context.Approvals
                                                    join ajp in context.ApprovalJobPhases on a.CurrentPhase equals ajp.ID
                                                    join ap in context.ApprovalPhases on ajp.PhaseTemplateID equals ap.ID
                                                    where a.ID == approvalId && a.CurrentPhase == currentPhase
                                                    select ap.DecisionsShouldBeMadeByAll).FirstOrDefault();

					lockApproval.PhaseDetails = approvalPhase;

					var decisionPriority = (from ad in context.ApprovalDecisions where ad.ID == decisionId select ad.Priority).FirstOrDefault();

                    var approvalGroupCollaborators = (from acd in context.ApprovalCollaboratorDecisions
                                                      where
                                                          acd.Approval == approvalId &&
                                                          acd.Phase == currentPhase &&
                                                          acd.Collaborator != null
                                                      select new
                                                      {
                                                          acd.Decision,
                                                          acd.Collaborator,
                                                          IsExternal = false
                                                      }
                                                      ).Distinct().ToList();

                     var approvalGroupExternalCollaborators = (from acd in context.ApprovalCollaboratorDecisions
                                                                  where
                                                                    acd.Approval == approvalId &&
                                                                    acd.Phase == currentPhase &&
                                                                    acd.ExternalCollaborator != null
                                                                   select new
                                                                  {
                                                                      Decision = acd.Decision,
                                                                      Collaborator = acd.ExternalCollaborator,
                                                                      IsExternal = true
                                                                  }
                                                               ).Distinct().ToList();
                        approvalGroupCollaborators.AddRange(approvalGroupExternalCollaborators);

                    if (approvalPhase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup)
                    {
                        if (approvalGroupCollaborators.Any())
                        {
                            var allCollaboratorsApprovedTheVersion = approvalGroupCollaborators.All(ad => approvalPhase.ApprovalTrigger.Contains(ad.Decision.HasValue ? ad.Decision.Value.ToString() : ""));
                            if (allCollaboratorsApprovedTheVersion)
                            {
                                lockApproval.MoveNext = true;
                            }

                            var atLeastOneCollaboratorDisagree = approvalGroupCollaborators.Any(ad => ad.Decision == decisionId && decisionPriority < approvalPhase.ApprovalTrigger.Select(v => int.Parse(v)).Min());
                            //if (approvalGroupCollaborators.All(ad => ad.Decision == (int)ApprovalPhase.ApprovalDecisionSatusType.NotApplicable))
                            //{
                            //    atLeastOneCollaboratorDisagree = true;
                            //}

                            if (decisionShoulBeMadeByAll == true)
                            {
                                lockApproval.IsRejected = decisionPriority < approvalPhase.ApprovalTriggerPriority;
                                var allCollaboratorsMadeDecision = approvalGroupCollaborators.All(ad => ad.Decision != null);
                                if (allCollaboratorsMadeDecision)
                                {
                                    approvalPhase.Approval.IsLocked = true;
                                }

                            }
                            else
                            {
                                if (atLeastOneCollaboratorDisagree)
                                {
                                    lockApproval.IsRejected = true;
                                    approvalPhase.Approval.IsLocked = true;
                                }
                            }
                        }
                    }
                    else //if decision type is "Primary Decision Maker"
                    {
                        var approvalCollaboratorDecision = (from acd in context.ApprovalCollaboratorDecisions
                                                            join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
                                                            where
                                                                acd.Approval == approvalId &&
                                                                acd.Phase == currentPhase
                                                            select acd.Decision).FirstOrDefault();

                        if (isViewedByAll == null)
                        {
                            var isApproved = phaseApproved != null && phaseApproved.Value;
                            if (isApproved)
                            {
                                lockApproval.MoveNext = true;
                            }
                        }
                        else if (isViewedByAll != null && isViewedByAll.Value)
                        {
                            lockApproval.MoveNext = true;
                        }
                        else
                        {
                            if (decisionShoulBeMadeByAll == true)
                            {
                                var allCollaboratorsMadeDecision = approvalGroupCollaborators.All(ad => ad.Decision != null);
                                if (allCollaboratorsMadeDecision)
                                {
                                    approvalPhase.Approval.IsLocked = true;
                                }

                            }
                            else
                            {
                                approvalPhase.Approval.IsLocked = true;
                            }
                        }

                        var isDecisionLowerThanTrigger = approvalPhase.ApprovalTriggerPriority > decisionPriority && approvalCollaboratorDecision == decisionId;
                        if (isDecisionLowerThanTrigger)
                        {
                            lockApproval.IsRejected = true;
                            if (decisionShoulBeMadeByAll == true)
                            {
                                var allCollaboratorsMadeDecision = approvalGroupCollaborators.All(ad => ad.Decision != null);
                                if (allCollaboratorsMadeDecision)
                                {
                                    approvalPhase.Approval.IsLocked = true;
                                }
                            }
                            else
                            {
                                approvalPhase.Approval.IsLocked = true;
                            }
                        }
                    }

					if (lockApproval.MoveNext)
					{
						MoveToNextPhaseOrMarkAsCompleted(ref lockApproval, approvalPhase, loggedUserId, context);
					}
				}
			}
			return lockApproval;
		}

		private static void MoveToNextPhaseOrMarkAsCompleted(ref LockApproval lockApproval, ApprovalPhaseDetails approvalPhase, int? loggedUserId, DbContextBL context)
		{
			if (approvalPhase.NextPhase != null && approvalPhase.NextPhase.ID != 0)
			{
				var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
				{
					Approval = approvalPhase.Approval.ID,
					Phase = approvalPhase.NextPhase.ID
				};

				if (loggedUserId.HasValue)
				{
					approvalJobPhaseApproval.CreatedBy = approvalJobPhaseApproval.ModifiedBy = loggedUserId.Value;
					approvalJobPhaseApproval.CreatedDate = approvalJobPhaseApproval.ModifiedDate = DateTime.UtcNow;
				}

				context.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);

				//move the approval to the next phase
				approvalPhase.Approval.CurrentPhase = approvalPhase.NextPhase.ID;
				approvalPhase.Approval.PrimaryDecisionMaker = approvalPhase.NextPhase.PrimaryDecisionMaker;
				approvalPhase.Approval.ExternalPrimaryDecisionMaker = approvalPhase.NextPhase.ExternalPrimaryDecisionMaker;

				var deadlineTrigger = (from fdt in context.PhaseDeadlineTriggers where fdt.ID == approvalPhase.NextPhase.DeadlineTrigger select fdt.Key).FirstOrDefault();
				var deadlineUnit = (from fdu in context.PhaseDeadlineUnits where fdu.ID == approvalPhase.NextPhase.DeadlineUnit select fdu.Key).FirstOrDefault();

				if (approvalPhase.NextPhase.Deadline == null && deadlineTrigger == (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
				{
					approvalPhase.NextPhase.Deadline = GetAutomaticPhaseDeadline(null, deadlineTrigger, approvalPhase.NextPhase.DeadlineUnitNumber, deadlineUnit);
				}
				approvalPhase.Approval.IsLocked = false;
				lockApproval.PhaseName = approvalPhase.NextPhase.Name;
			}
			else
			{
				approvalPhase.Approval.IsLocked = true;
				lockApproval.IsLastPhaseCompleted = true;
				lockApproval.PhaseName = approvalPhase.PhaseName;
			}
		}

		public static void NotifyUsers(bool? isExternal, int? collaboratorId, int approvalId, DbContextBL context, int approvalOwner)
		{
			var userIsExternal = (isExternal != null ? isExternal.Value : false);
			NotificationServiceBL.CreateNotification(new ApprovalWasRejected
			{
				EventCreator = userIsExternal ? null : collaboratorId,
				ExternalCreator = userIsExternal ? collaboratorId : null,
				InternalRecipient = approvalOwner, //change to jobowner
				CreatedDate = DateTime.UtcNow,
				ApprovalsIds = new[] { approvalId }
			},
													null,
													context
													);
		}

		public static bool IsApprovalRejected(int approvalId, GMGColorContext context)
		{
			var phaseDetails = (from ajp in context.ApprovalJobPhases
								join adj in context.ApprovalDecisions on ajp.ApprovalTrigger equals adj.ID
								join ajpa in context.ApprovalJobPhaseApprovals on ajp.ID equals ajpa.Phase
								join a in context.Approvals on ajpa.Approval equals a.ID
								let decisions = (from acd in context.ApprovalCollaboratorDecisions
												 join ad in context.ApprovalDecisions on acd.Decision equals ad.ID
												 where acd.Phase == ajp.ID && acd.Approval == approvalId
												 select new
												 {
													 ad.Priority,
													 acd.Collaborator,
													 acd.ExternalCollaborator
												 }).ToList()
								where ajpa.Approval == approvalId && a.CurrentPhase == ajp.ID
								select new
								{
									Decisions = decisions,
									ApprovalTriggerPriority = adj.Priority,
									DecisionType = ajp.DecisionType,
									ajp.PrimaryDecisionMaker,
									ajp.ExternalPrimaryDecisionMaker,
									a.CurrentPhase
								}).FirstOrDefault();

			if (phaseDetails != null)
			{
				//check the decision type for the max phase 
				if (phaseDetails.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup)
				{
					//check if anyone in the Approval Group takes a decision lower than the Lowest Approval Trigger
					if (phaseDetails.Decisions.Any() && phaseDetails.Decisions.Any(ad => ad.Priority < phaseDetails.ApprovalTriggerPriority))
					{
						return true;
					}
				}
				else
				{
					//check if approval PDM has made a decision lower than the required phase decision
					if (phaseDetails.Decisions.Any(u => ((phaseDetails.PrimaryDecisionMaker != null && u.Collaborator == phaseDetails.PrimaryDecisionMaker) ||
															(phaseDetails.ExternalPrimaryDecisionMaker != null && u.ExternalCollaborator == phaseDetails.ExternalPrimaryDecisionMaker)) &&
															u.Priority < phaseDetails.ApprovalTriggerPriority))
					{
						return true;
					}
				}
			}
			return false;
		}

		public static void UpdateApprovalDeadLine(int approvalId, DateTime dateTime, DbContextBL context, int accountId, string loggedUseremail)
		{
			try
			{
				var tbu = (from d in context.Approvals
						   where d.ID == approvalId
						   select d).Single();
				tbu.Deadline = dateTime;

				context.SaveChanges();

                if (tbu.CurrentPhase != null)
                {
                    try
                    {
                        var jobDetails = new JobStatusDetails()
                        {
                            approvalGuid = tbu.Guid,
                            email = loggedUseremail,
                            approvalDueDate = dateTime.ToString()
                        };

                        ApprovalBL.PushCallBackTrigger(jobDetails, accountId, context, (int)CallBackType.ApprovalDueDate);
                    }
                    catch (Exception exc)
                    {
                        GMGColorDAL.GMGColorLogging.log.Error(exc.Message, exc);
                    }
                }
            }
			catch (DbEntityValidationException ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateApprovalDeadLine, ex);
			}
		}

		[ObsoleteAttribute("This method is obsolete. Call IsLastVersion from collaborate module instead.", false)]
		public static bool IsLastVersion(int approvalId, GMGColorContext context)
		{
			var jobId = context.Approvals.Where(a => a.ID == approvalId).Select(a => a.Job).FirstOrDefault();
			var maxVersion = context.Approvals.Where(a => a.Job == jobId).OrderByDescending(v => v.Version).FirstOrDefault();
			return maxVersion != null && approvalId == maxVersion.ID;
		}

		public static void UpdatePhaseDeadline(int phaseId, DateTime dateTime,int approvalId, DbContextBL context, int accountId, string loggedUseremail)
		{
			try
			{
				var phase = (from ajp in context.ApprovalJobPhases
							 where ajp.ID == phaseId
							 select ajp).Single();
				phase.Deadline = dateTime;
				phase.DeadlineUnit = null;
				phase.DeadlineUnitNumber = null;
				phase.DeadlineTrigger = null;

				if (dateTime > DateTime.UtcNow)
				{
					phase.OverdueNotificationSent = false;
				}

				context.SaveChanges();

                var jobDetails = new JobStatusDetails()
                {
                    approvalGuid = approvalId.ToString(),
                    phaseName = phaseId.ToString(),
                    email = loggedUseremail,
                    approvalDueDate = dateTime.ToString()
                };

                ApprovalBL.PushCallBackTrigger(jobDetails, accountId, context, (int)CallBackType.PhaseDueDate);

            }
            catch (DbEntityValidationException ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdatePhaseDeadline, ex);
			}
		}
		public static ApprovalDetailsModel.VersionDetailsModel PopulateVersionDetailsModel(int approvalVersionId, GMGColorDAL.User LoggedUser,Account loggedAccount, int userLocale, GMGColorContext context, bool isFromRestAPI = false)
		{
			var versionDetailsModel = new ApprovalDetailsModel.VersionDetailsModel();
			//Get collaborators info details needed for Access Permission Pop up
			var collaboratorsInfo = (from apv in context.Approvals
									 let workflowInfo = (from apw in context.ApprovalJobPhases
														 join apjw in context.ApprovalJobWorkflows on apw.ApprovalJobWorkflow equals apjw.ID

														 let isPhaseCompleted = (from apja in context.ApprovalJobPhaseApprovals
																				 where apja.Phase == apw.ID && apja.Approval == approvalVersionId
																				 select apja.PhaseMarkedAsCompleted).FirstOrDefault()



														 where apv.CurrentPhase == apw.ID
														 select new
														 {
															 appPhaseName = apw.Name,
															 approvalWorkflowName = apjw.Name,
															 appId = apv.CurrentPhase,
															 isPhaseCompleted
														 }).FirstOrDefault()

									 where apv.ID == approvalVersionId
									 select new
									 {
										 objApprovalVersion = apv,
										 workflowInfo,
										 IsExternalPrimaryDecisionMaker = apv.ExternalPrimaryDecisionMaker
									 }).FirstOrDefault();
            versionDetailsModel.objApprovalVersion = collaboratorsInfo.objApprovalVersion;
			versionDetailsModel.ApprovalVersionCollaboratorInfo = new ApprovalVersionCollaboratorInfo();
			versionDetailsModel.ApprovalVersionCollaboratorInfo.LoadCollaborats(approvalVersionId, context);
			versionDetailsModel.JobOwner = (from j in context.Jobs
											join a in context.Approvals on j.ID equals a.Job
											where a.ID == approvalVersionId
											select j.JobOwner).FirstOrDefault();
			if (versionDetailsModel.objApprovalVersion.OnlyOneDecisionRequired && versionDetailsModel.objApprovalVersion.PrimaryDecisionMaker != null && (versionDetailsModel.objApprovalVersion.PrimaryDecisionMaker > 0))
			{
				versionDetailsModel.ApprovalVersionCollaboratorInfo.CollaboratorsWithDecision.Add(versionDetailsModel.objApprovalVersion.PrimaryDecisionMaker.Value);
			}

			if (collaboratorsInfo.workflowInfo != null)
			{
				versionDetailsModel.ApprovalPhaseName = collaboratorsInfo.workflowInfo.appPhaseName;
				versionDetailsModel.ApprovalWorkflowName = collaboratorsInfo.workflowInfo.approvalWorkflowName;
				versionDetailsModel.PhaseId = collaboratorsInfo.workflowInfo.appId;
				versionDetailsModel.IsPhaseCompleted = collaboratorsInfo.workflowInfo.isPhaseCompleted;
			}

			versionDetailsModel.ID = versionDetailsModel.objApprovalVersion.ID;
			versionDetailsModel.Version = versionDetailsModel.objApprovalVersion.Version;
            versionDetailsModel.Title = GetFileNameWithoutExtension(versionDetailsModel.objApprovalVersion.FileName);
			versionDetailsModel.objLoggedAccount = loggedAccount;
			var approvalCustomDecision = ApprovalCustomDecision.GetCustomDecisionsByAccountId(loggedAccount.ID, userLocale, context);
			versionDetailsModel.ColloaboratorActivities = GetApprovalDetailsCollaboratorActivities(versionDetailsModel.objApprovalVersion, loggedAccount, LoggedUser, userLocale, approvalCustomDecision, context);
            versionDetailsModel.DeletedColloaboratorActivitiesWhoAnnotated = GetDeletedApprovalDetailsCollaboratorActivities(versionDetailsModel.objApprovalVersion,loggedAccount, userLocale, approvalCustomDecision, context);
            versionDetailsModel.ExternalColloaboratorActivities = GetApprovalDetailsExternalCollaboratorActivities(versionDetailsModel.objApprovalVersion, LoggedUser, loggedAccount, approvalCustomDecision,
				context);
            versionDetailsModel.DeletedExternalColloaboratorActivitiesWhoAnnotated = GetDeletedApprovalDetailsExternalCollaboratorActivities(versionDetailsModel.objApprovalVersion, loggedAccount, approvalCustomDecision,
                context);

            if (isFromRestAPI)
			{
				versionDetailsModel.AnnotationsStatusLogs =
					GetApprovalAnnotationsStatusChangeLog(versionDetailsModel.objApprovalVersion.ID, loggedAccount,
						context);

				versionDetailsModel.VersionCanBeDeleted = CanDeleteJobVersion(approvalVersionId,
					versionDetailsModel.ColloaboratorActivities, versionDetailsModel.ExternalColloaboratorActivities,
					context);
			}

			versionDetailsModel.IsLastVersion = IsLastVersion(versionDetailsModel.ID, context);

			var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccount.ID, ProofStudioPenWidthDefaultValue, false, context);
			versionDetailsModel.IsDueDateEditDisabled = collaborateSettings.DisableFileDueDateinUploader;
			versionDetailsModel.IsExternalPrimaryDecisionMaker = !collaboratorsInfo.IsExternalPrimaryDecisionMaker.HasValue ? false : Convert.ToBoolean(collaboratorsInfo.IsExternalPrimaryDecisionMaker.Value);
            versionDetailsModel.Checklist = (from cl in context.CheckList
                                             join a in context.Approvals on cl.ID equals a.Checklist
                                             where a.ID == approvalVersionId
                                             select cl.Name).FirstOrDefault();
            versionDetailsModel.FileName = (from a in context.Approvals
                                            where a.ID == approvalVersionId
                                            select a.FileName).FirstOrDefault();

            return versionDetailsModel;
		}


        private static bool CanDeleteJobVersion(int approvalVersionId, List<ApprovalDetailsCollaboratorActivity> colloaboratorActivities, List<ApprovalDetailsCollaboratorActivity> externalColloaboratorActivities, GMGColorContext context)
		{
			var phaseHistory = (from a in context.Approvals
								join ajpa in context.ApprovalJobPhaseApprovals on a.ID equals ajpa.Approval
								join ajp in context.ApprovalJobPhases on ajpa.Phase equals ajp.ID
								where approvalVersionId == a.ID
								select new
								{
									ajpa.ID,
									ajp.ApprovalTrigger,
									ajp.DecisionType,
									ajp.PrimaryDecisionMaker,
									ajp.ExternalPrimaryDecisionMaker
								}).ToList();

			if (phaseHistory.Count <= 1)
			{
				var maxPhase = phaseHistory.OrderByDescending(h => h.ID).FirstOrDefault();
				if (maxPhase != null)
				{
					if (maxPhase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup)
					{
						var internalUsersDecision = colloaboratorActivities.Where(ac => ac.CollaboratorRoleKey == "ANR")
																   .Select(d => d.DecisionId)
																   .ToList();
						var externalUsersDecision = externalColloaboratorActivities.Where(ac => ac.CollaboratorRoleKey == "ANR")
														   .Select(d => d.DecisionId)
														   .ToList();
						if ((internalUsersDecision.Any() && internalUsersDecision.All(d => d == maxPhase.ApprovalTrigger)) || (externalUsersDecision.Any() &&
													  externalUsersDecision.All(d => d == maxPhase.ApprovalTrigger)))
						{
							return false;
						}
						return true;
					}
					return !colloaboratorActivities.Any(ac => ac.IsPrimaryDecisionMaker && ac.DecisionId == maxPhase.ApprovalTrigger) && !externalColloaboratorActivities.Any(ac => ac.IsPrimaryDecisionMaker && ac.DecisionId == maxPhase.ApprovalTrigger);
				}
			}
			return false;
		}

		private static List<ApprovalAnnotationStatusChangeLog> GetApprovalAnnotationsStatusChangeLog(
				int approvalId, Account loggedAccount, GMGColorContext context)
		{
			List<ApprovalAnnotationStatusChangeLog> statusChangeList = new List<ApprovalAnnotationStatusChangeLog>();

			var statusChangeLogs = (from ascl in context.ApprovalAnnotationStatusChangeLogs
									join ann in context.ApprovalAnnotations on ascl.Annotation equals ann.ID
									join aps in context.ApprovalAnnotationStatus on ascl.Status equals aps.ID
									join ap in context.ApprovalPages on ann.Page equals ap.ID
									join a in context.Approvals on ap.Approval equals a.ID
									let lastDec = (from asclm in context.ApprovalAnnotationStatusChangeLogs
												   where asclm.Annotation == ann.ID
												   select asclm.ModifiedDate).Max()
									where a.ID == approvalId && ascl.ModifiedDate == lastDec
									orderby ascl.ModifiedDate descending
									select new
									{
										collaboratorName = ascl.Modifier.HasValue
											? ascl.User.GivenName + " " + ascl.User.FamilyName
											: ascl.ExternalCollaborator.GivenName + " " + ascl.ExternalCollaborator.FamilyName,
										comment = ann.Comment,
										statusKey = aps.Key,
										modifiedDate = ascl.ModifiedDate
									});

			foreach (var statusChange in statusChangeLogs)
			{
				ApprovalAnnotationStatusChangeLog statusChangeViewModel = new ApprovalAnnotationStatusChangeLog();
				statusChangeViewModel.CollaboratorName = statusChange.collaboratorName;
				statusChangeViewModel.AnnotationComment = statusChange.comment;

				DateTime dateInUserTime = GMGColorFormatData.GetUserTimeFromUTC(statusChange.modifiedDate, loggedAccount.TimeZone);

				statusChangeViewModel.ModifiedDate =
					GMGColorFormatData.GetFormattedDate(dateInUserTime, loggedAccount.DateFormat1.Pattern);
				statusChangeViewModel.ModifiedDateHour = dateInUserTime.ToString("h:mm tt");

				statusChangeViewModel.Status =
					ApprovalAnnotationStatu.GetLocalizedApprovalAnnotationStatus(statusChange.statusKey);

				statusChangeList.Add(statusChangeViewModel);
			}

			return statusChangeList;
		}

        public static List<ApprovalDetailsCollaboratorActivity> GetDeletedApprovalDetailsCollaboratorActivities(Approval objApproval, Account loggedAccount, int userLocale, List<AccountSettings.ApprovalCustomDecision> approvalCustomDecision, GMGColorContext context)
        {
            List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();

            var Role = (from acl in context.ApprovalCollaboratorRoles
                        select acl.ID).FirstOrDefault();

            var listOfUsersInApprovalCollaborator = (from ac in context.ApprovalCollaborators
                                                     join a in context.Approvals on ac.Approval equals a.ID
                                                     where a.ID == objApproval.ID
                                                     select ac.Collaborator).Distinct().ToList();

            var deletedInternalUserIdsWhoMadeAnnotations = (from aa in context.ApprovalAnnotations
                                                            join ap in context.ApprovalPages on aa.Page equals ap.ID
                                                            join a in context.Approvals on ap.Approval equals a.ID
                                                            where a.ID == objApproval.ID && !listOfUsersInApprovalCollaborator.Contains((int)aa.Creator) && !(aa.Creator == null)
                                                            select aa.Creator).Distinct().ToList();

            List<int> annotationIdsOfDeletedInternalUsers = new List<int>();
            foreach (var u in deletedInternalUserIdsWhoMadeAnnotations)
            {
                var annotations = (from aa in context.ApprovalAnnotations
                                   join ap in context.ApprovalPages on aa.Page equals ap.ID
                                   join a in context.Approvals on ap.Approval equals a.ID
                                   where a.ID == objApproval.ID && aa.Creator == u
                                   select aa.ID).FirstOrDefault();

                annotationIdsOfDeletedInternalUsers.Add(annotations);
            }

            var collaboratorsList = (from aa in context.ApprovalAnnotations
                                     join ap in context.ApprovalPages on aa.Page equals ap.ID
                                     join a in context.Approvals on ap.Approval equals a.ID
                                     join u in context.Users on aa.Creator equals u.ID
                                     join j in context.Jobs on a.Job equals j.ID
                                     join ac in context.Accounts on j.Account equals ac.ID
                                     let decisions = (from acd in context.ApprovalCollaboratorDecisions
                                                      where acd.Approval == objApproval.ID && acd.Collaborator == aa.Creator && acd.Phase == a.CurrentPhase
                                                      select acd).ToList()
                                     let annotationAndReplies = (from ann in context.ApprovalAnnotations
                                                                 join app in context.ApprovalPages on a.ID equals app.Approval
                                                                 where ann.Page == app.ID && ann.Creator == aa.Creator && ann.Phase == a.CurrentPhase
                                                                 select ann.Parent).ToList()
                                     where a.ID == objApproval.ID && !listOfUsersInApprovalCollaborator.Contains((int)aa.Creator) && annotationIdsOfDeletedInternalUsers.Contains(aa.ID)
                                     select new
                                     {
                                         GivenName = u.GivenName,
                                         FamilyName = u.FamilyName,
                                         EmailAddress = u.EmailAddress,
                                         Collaborator = aa.Creator,
                                         AssignedDate = aa.AnnotatedDate,
                                         ApprovalCollaboratorRole = Role,
                                         TimeZone = ac.TimeZone,
                                         roleKey = "RDO",
                                         annotationAndReplies,
                                         decisions,
                                         Username = u.Username,
                                         Phase = aa.Phase
                                     }).ToList();

            var annotationCommentList = (from ann in context.ApprovalAnnotations
                                         join app in context.ApprovalPages on ann.Page equals app.ID
                                         join ap in context.Approvals on app.Approval equals ap.ID
                                         join apf in context.ApprovalJobPhaseApprovals on ap.ID equals apf.Approval into tempSt
                                         from subPhase in tempSt.DefaultIfEmpty()
                                         where ap.Job == objApproval.Job
                                         select ann.ID).Distinct().Count();

            var approvalAnnotationCount = (from ann in context.ApprovalAnnotations
                                           join app in context.ApprovalPages on ann.Page equals app.ID
                                           join ap in context.Approvals on app.Approval equals ap.ID
                                           join apf in context.ApprovalJobPhaseApprovals on ap.ID equals apf.Approval into tempSt
                                           from subPhase in tempSt.DefaultIfEmpty()
                                           where ap.ID == objApproval.ID
                                           select ann.ID).Distinct().Count();

            foreach (var objAppCollaborator in collaboratorsList)
            {
                List<ApprovalCollaboratorDecision> lstAppColDesHistory = objAppCollaborator.decisions;
                string completedHtml = string.Empty;
                string openedHtml = string.Empty;
                DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAppCollaborator.TimeZone);
                DateTimeSpan dateSpan;
                openedHtml = "-";
                if (lstAppColDesHistory.Count > 0 && lstAppColDesHistory[0].CompletedDate != null)
                {
                    DateTime dtCompleted = lstAppColDesHistory[0].CompletedDate.Value;
                    dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, objAppCollaborator.TimeZone);
                    dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
                    completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
                }
                else
                {
                    completedHtml = "-";
                }

                activityList.Add(new ApprovalDetailsCollaboratorActivity
                {
                    UserID = (int)objAppCollaborator.Collaborator,
                    UserName = objAppCollaborator.Username,
                    Name = objAppCollaborator.GivenName + " " + objAppCollaborator.FamilyName,
                    EmailAddress = objAppCollaborator.EmailAddress,
                    Sent = objAppCollaborator.AssignedDate,
                    Opened = openedHtml,
                    Annotations = objAppCollaborator.annotationAndReplies.Count(t => t == null),
                    Replies = objAppCollaborator.annotationAndReplies.Count(t => t != null),
                    TotalApprovalAnnotations = approvalAnnotationCount,
                    TotalAnnotations = annotationCommentList,
                    DecisionMade = (lstAppColDesHistory.Count > 0)
                                                ? (lstAppColDesHistory[0].Decision != null)
                                                              ? (approvalCustomDecision.Any(d => d.DecisionId == lstAppColDesHistory[0].Decision && !string.IsNullOrEmpty(d.Name)))
                                                                             ? approvalCustomDecision.Where(d => d.DecisionId == lstAppColDesHistory[0].Decision).Select(d => d.Name).FirstOrDefault()
                                                                             : ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDesHistory[0].ApprovalDecision)
                                                              : GMGColor.Resources.Resources.lblNone
                                                : GMGColor.Resources.Resources.lblNone,
                    DecisionId = lstAppColDesHistory.Count > 0 ? lstAppColDesHistory[0].Decision.GetValueOrDefault() : 0,
                    Completed = completedHtml,
                    IsPrimaryDecisionMaker = (objApproval.PrimaryDecisionMaker != null && objAppCollaborator.Collaborator == objApproval.PrimaryDecisionMaker),
                    CollaboratorRoleKey = objAppCollaborator.roleKey,
                    PhaseId = objAppCollaborator.Phase.GetValueOrDefault()
                });
            }
            if (activityList.Count > 0)
            {
                activityList[0].TotalApprovalAnnotations = approvalAnnotationCount;
                activityList[0].TotalAnnotations = annotationCommentList;
            }
            return activityList;
        }

        public static List<ApprovalDetailsCollaboratorActivity> GetApprovalDetailsCollaboratorActivities(Approval objApproval, Account loggedAccount, GMGColorDAL.User loggedUser,int userLocale, List<AccountSettings.ApprovalCustomDecision> approvalCustomDecision, GMGColorContext context)
		{
			List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();

            //Select collaborators that have not been deleted or that have made a decision or an annotation for the specified approval
            var collaboratorsList = (from ap in context.Approvals
                                     join acl in context.ApprovalCollaborators on ap.ID equals acl.Approval
                                     join aclRole in context.ApprovalCollaboratorRoles on acl.ApprovalCollaboratorRole equals aclRole.ID
                                     join u in context.Users on acl.Collaborator equals u.ID
                                     join j in context.Jobs on ap.Job equals j.ID
                                     join ac in context.Accounts on j.Account equals ac.ID
                                     join us in context.UserStatus on u.Status equals us.ID
                                     join ur in context.UserRoles on u.ID equals ur.User
                                     join r in context.Roles on ur.Role equals r.ID
                                     let lastOpenedDate = (from auv in context.ApprovalUserViewInfoes
                                                           where auv.Approval == objApproval.ID && auv.User == acl.Collaborator
                                                           orderby auv.ViewedDate descending
                                                           select auv.ViewedDate).FirstOrDefault()
                                     let decisions = (from acd in context.ApprovalCollaboratorDecisions
                                                      where acd.Approval == objApproval.ID && acd.Collaborator == acl.Collaborator && acd.Phase == ap.CurrentPhase
                                                      select acd).ToList()
                                     let annotationAndReplies = (from ann in context.ApprovalAnnotations
                                                                 join app in context.ApprovalPages on ap.ID equals app.Approval

                                                                 let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

                                                                 where ann.Page == app.ID && ann.Creator == acl.Collaborator && ann.Phase == ap.CurrentPhase
                                                                 && (!publicPrivateAnnotations || loggedUser.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUser.ID)
                                                                 select new { ann.Parent, ann.IsPhaseRerun }).ToList()
                                     let selectedForPublicAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker == u.ID select ca.PrivateAnnotations).FirstOrDefault()

                                     let substituteuser = (from u in context.Users
                                                           where u.ID == acl.SubstituteUser
                                                           select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                    ).FirstOrDefault()

                                     let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                     where ap.ID == objApproval.ID && ap.CurrentPhase == acl.Phase && (r.Key != "NON" || decisions.Any(t => t.Decision != null)) && r.AppModule == appModuleID
                                     select new
                                     {
                                         u.GivenName,
                                         u.FamilyName,
                                         u.EmailAddress,
                                         acl.Collaborator,
                                         acl.AssignedDate,
                                         acl.ApprovalCollaboratorRole,
                                         ac.TimeZone,
                                         roleKey = aclRole.Key,
                                         annotationAndReplies,
                                         decisions,
                                         lastOpenedDate,
                                         u.Username,
                                         acl.Phase,
                                         substituteuser,
                                         selectedForPublicAnnotations,
                                         UserStatus = u.Status,
                                         acl.IsReminderMailSent
                                         //annotationIsPhaseRerun
                                     }).ToList();

            bool isApprovalTypeVideo = (from a in context.Approvals
                                        join ft in context.FileTypes on a.FileType equals ft.ID
                                        where a.ID == objApproval.ID && ft.Type == "video"
                                        select ft.ID).Any();
                       
            var annotationCommentList = (from ann in context.ApprovalAnnotations
                                         join app in context.ApprovalPages on ann.Page equals app.ID
                                         join ap in context.Approvals on app.Approval equals ap.ID
                                         join apf in context.ApprovalJobPhaseApprovals on ap.ID equals apf.Approval into tempSt
                                         from subPhase in tempSt.DefaultIfEmpty()
                                         let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

                                         where ap.Job == objApproval.Job 
                                         && (!publicPrivateAnnotations || loggedUser.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUser.ID)
                                         select ann.ID).Distinct().Count();
            
            var approvalAnnotationCount = (from ann in context.ApprovalAnnotations
                                         join app in context.ApprovalPages on ann.Page equals app.ID
                                         join ap in context.Approvals on app.Approval equals ap.ID
                                         join apf in context.ApprovalJobPhaseApprovals on ap.ID equals apf.Approval into tempSt
                                         from subPhase in tempSt.DefaultIfEmpty()
                                         let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

                                         where ap.ID == objApproval.ID
                                         && (!publicPrivateAnnotations || loggedUser.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUser.ID)
                                         select ann.ID).Distinct().Count();

            int VideoAnnotationReport = !isApprovalTypeVideo ? approvalAnnotationCount : (from ann in context.ApprovalAnnotations
                                                                                        join app in context.ApprovalPages on ann.Page equals app.ID
                                                                                        join ap in context.Approvals on app.Approval equals ap.ID
                                                                                        join apf in context.ApprovalJobPhaseApprovals on ap.ID equals apf.Approval into tempSt
                                                                                        from subPhase in tempSt.DefaultIfEmpty()
                                                                                        let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()

                                                                                        where ap.Job == objApproval.Job && ann.ShowVideoReport == true && ann.Parent == null
                                                                                        && (!publicPrivateAnnotations || loggedUser.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUser.ID)
                                                                                        select ann.ID).Distinct().Count();


            foreach (var objAppCollaborator in collaboratorsList)
			{
				List<ApprovalCollaboratorDecision> lstAppColDesHistory = objAppCollaborator.decisions;
				//if (lstAppColDesHistory.Count > 0)
				//{
				string completedHtml = string.Empty;
				string openedHtml = string.Empty;
				DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAppCollaborator.TimeZone);
				DateTimeSpan dateSpan;

				if (objAppCollaborator.lastOpenedDate != default(DateTime))
				{
					DateTime dtOpened = objAppCollaborator.lastOpenedDate;
					dtOpened = GMGColorFormatData.GetUserTimeFromUTC(dtOpened, objAppCollaborator.TimeZone);
					openedHtml = GMGColorFormatData.GetFormattedDate(dtOpened, loggedAccount, context) + "<br /><small>" + GMGColorFormatData.GetFormattedTime(dtOpened, loggedAccount) + "</small>";
				}
				else
				{
					openedHtml = "-";
				}

				if (lstAppColDesHistory.Count > 0 && lstAppColDesHistory[0].CompletedDate != null)
				{
					DateTime dtCompleted = lstAppColDesHistory[0].CompletedDate.Value;
					dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, objAppCollaborator.TimeZone);
					dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
					completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
				}
				else
				{
					completedHtml = "-";
				}

                activityList.Add(new ApprovalDetailsCollaboratorActivity
                {
                    UserID = objAppCollaborator.Collaborator,
                    UserName = objAppCollaborator.Username,
                    OpenedDate = objAppCollaborator.lastOpenedDate,
                    Name = objAppCollaborator.GivenName + " " + objAppCollaborator.FamilyName + (objAppCollaborator.substituteuser == null ? "" : objAppCollaborator.substituteuser),
                    EmailAddress = objAppCollaborator.EmailAddress,
                    Role = ApprovalCollaboratorRole.GetLocalizedRoleName(objAppCollaborator.roleKey),
                    Sent = objAppCollaborator.AssignedDate,
                    Opened = openedHtml,
                    Annotations = objAppCollaborator.annotationAndReplies.Select(x => x.Parent).Count(t => t == null),
                    Replies = objAppCollaborator.annotationAndReplies.Select(x => x.Parent).Count(t => t != null),
                    TotalApprovalAnnotations = approvalAnnotationCount,
                    TotalAnnotations = annotationCommentList,
                    AnnotationsPhaseRerun = objAppCollaborator.annotationAndReplies.Select(x => x.IsPhaseRerun).Count(t => t != null && t == true),
                    DecisionMade = (lstAppColDesHistory.Count > 0)
                                                ? (lstAppColDesHistory[0].Decision != null)
                                                              ? (approvalCustomDecision.Any(d => d.DecisionId == lstAppColDesHistory[0].Decision && !string.IsNullOrEmpty(d.Name)))
                                                                             ? approvalCustomDecision.Where(d => d.DecisionId == lstAppColDesHistory[0].Decision).Select(d => d.Name).FirstOrDefault()
                                                                             : ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDesHistory[0].ApprovalDecision)
                                                              : GMGColor.Resources.Resources.lblNone
                                                : GMGColor.Resources.Resources.lblNone,
                    DecisionId = lstAppColDesHistory.Count > 0 ? lstAppColDesHistory[0].Decision.GetValueOrDefault() : 0,
                    Completed = completedHtml,
                    IsPrimaryDecisionMaker = (objApproval.PrimaryDecisionMaker != null && objAppCollaborator.Collaborator == objApproval.PrimaryDecisionMaker),
                    CollaboratorRoleKey = objAppCollaborator.roleKey,
                    PhaseId = objAppCollaborator.Phase.GetValueOrDefault(),
                    selectedForPublicAnnotations = objAppCollaborator.selectedForPublicAnnotations,
                    TotalVideoAnnotationsReport = VideoAnnotationReport,
                    userStatus = objAppCollaborator.UserStatus,
                    IsReminderMailSent = objAppCollaborator.IsReminderMailSent
                });
				//}
			}
            if(activityList.Count> 0)
            {
                activityList[0].TotalApprovalAnnotations = approvalAnnotationCount;
                activityList[0].TotalAnnotations = annotationCommentList;
            }
			return activityList;
		}


		public static List<ApprovalDetailsCollaboratorActivity> GetApprovalDetailsExternalCollaboratorActivities(Approval objApproval, GMGColorDAL.User loggedUser, Account loggedAccount, List<AccountSettings.ApprovalCustomDecision> approvalCustomDecision, GMGColorContext context)
		{
			List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();

			var externalCollaborators = (from sh in context.SharedApprovals
										 join ap in context.Approvals on sh.Approval equals ap.ID
										 join exc in context.ExternalCollaborators on sh.ExternalCollaborator equals exc.ID
										 join aclRole in context.ApprovalCollaboratorRoles on sh.ApprovalCollaboratorRole equals aclRole.ID

										 let lastOpenedDate = (from auv in context.ApprovalUserViewInfoes
															   where auv.Approval == objApproval.ID && auv.ExternalUser == sh.ExternalCollaborator && auv.Phase == ap.CurrentPhase
															   orderby auv.ViewedDate descending
															   select auv.ViewedDate).FirstOrDefault()

										 let colDecisions = (from o in context.ApprovalCollaboratorDecisions
															 where o.ExternalCollaborator == sh.ExternalCollaborator && o.Approval == objApproval.ID && o.Phase == ap.CurrentPhase
															 select o).ToList()
										 let annotationsAndReplies = (from ann in context.ApprovalAnnotations
																	  join app in context.ApprovalPages on ap.ID equals app.Approval
                                                                      let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == objApproval.ID && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()
																	  where ann.Page == app.ID && ann.ExternalCreator == sh.ExternalCollaborator
                                                                      && (!publicPrivateAnnotations || loggedUser.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == loggedUser.ID)
                                                                      select ann.Parent).ToList()
										 where ap.ID == objApproval.ID && ap.CurrentPhase == sh.Phase //colDecisions.Count > 0
										 select new
										 {
											 sh.ExternalCollaborator,
											 colDecisions,
											 Name = exc.GivenName + " " + exc.FamilyName,
											 EmailAddress = exc.EmailAddress,
											 sh.SharedDate,
											 annotationsAndReplies,
											 sh.IsExpired,
											 RoleKey = aclRole.Key,
											 lastOpenedDate,
											 sh.Phase
										 }).ToList();
                       
            foreach (var objSharedApproval in externalCollaborators)
			{
				var lstAppColDecision = objSharedApproval.colDecisions;

				string completedHtml = "-";
				string openedHtml = "-";
				DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, loggedAccount.TimeZone);

				if (objSharedApproval.lastOpenedDate != default(DateTime))
				{
					DateTime dtOpened = objSharedApproval.lastOpenedDate;
					dtOpened = GMGColorFormatData.GetUserTimeFromUTC(dtOpened, loggedAccount.TimeZone);
					openedHtml = GMGColorFormatData.GetFormattedDate(dtOpened, loggedAccount, context) + "<br /><small>" + GMGColorFormatData.GetFormattedTime(dtOpened, loggedAccount) + "</small>";
				}

				if (lstAppColDecision.Count > 0 && lstAppColDecision[0].CompletedDate != null)
				{
					DateTime dtCompleted = lstAppColDecision[0].CompletedDate.Value;
					dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, loggedAccount.TimeZone);
					DateTimeSpan dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
					completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
				}

				activityList.Add(new ApprovalDetailsCollaboratorActivity
				{
					UserID = objSharedApproval.ExternalCollaborator,
					OpenedDate = lstAppColDecision.Count > 0 ? lstAppColDecision[0].OpenedDate.GetValueOrDefault() : default(DateTime),
					Name = objSharedApproval.Name,
					EmailAddress = objSharedApproval.EmailAddress,
					Role = ApprovalCollaboratorRole.GetLocalizedRoleName(objSharedApproval.RoleKey),
					Sent = objSharedApproval.SharedDate,
					Opened = openedHtml,
					Annotations = objSharedApproval.annotationsAndReplies.Count(t => t == null),
					Replies = objSharedApproval.annotationsAndReplies.Count(t => t != null),
					DecisionMade = (lstAppColDecision.Count > 0 && lstAppColDecision[0].Decision != null)
																  ? (approvalCustomDecision.Any(d => d.DecisionId == lstAppColDecision[0].Decision && !string.IsNullOrEmpty(d.Name)))
																				 ? approvalCustomDecision.Where(d => d.DecisionId == lstAppColDecision[0].Decision).Select(d => d.Name).FirstOrDefault()
																				 : ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDecision[0].ApprovalDecision)
																  : GMGColor.Resources.Resources.lblNone,
					DecisionId = lstAppColDecision.Count > 0 ? lstAppColDecision[0].Decision.GetValueOrDefault() : 0,
					Completed = completedHtml,
					IsPrimaryDecisionMaker = (objApproval.ExternalPrimaryDecisionMaker != null && objSharedApproval.ExternalCollaborator == objApproval.ExternalPrimaryDecisionMaker),
					IsAccessRevoked = objSharedApproval.IsExpired,
					PhaseId = objSharedApproval.Phase.GetValueOrDefault(),
					CollaboratorRoleKey = objSharedApproval.RoleKey
				});
			}
			return activityList;
		}

        public static List<ApprovalDetailsCollaboratorActivity> GetDeletedApprovalDetailsExternalCollaboratorActivities(Approval objApproval, Account loggedAccount, List<AccountSettings.ApprovalCustomDecision> approvalCustomDecision, GMGColorContext context)
        {
            List<ApprovalDetailsCollaboratorActivity> activityList = new List<ApprovalDetailsCollaboratorActivity>();


            var listOfUsersInSharedApprovals = (from sa in context.SharedApprovals
                                                join a in context.Approvals on sa.Approval equals a.ID
                                                where a.ID == objApproval.ID
                                                select sa.ExternalCollaborator).Distinct().ToList();

            var deletedExternalUserIdsWhoMadeAnnotations = (from aa in context.ApprovalAnnotations
                                                            join ap in context.ApprovalPages on aa.Page equals ap.ID
                                                            join a in context.Approvals on ap.Approval equals a.ID
                                                            where a.ID == objApproval.ID && !listOfUsersInSharedApprovals.Contains((int)aa.ExternalCreator) && !(aa.ExternalCreator == null)
                                                            select aa.ExternalCreator).Distinct().ToList();

            List<int> annotationIdsOfDeletedExternalUsers = new List<int>();
            foreach (var u in deletedExternalUserIdsWhoMadeAnnotations)
            {
                var annotations = (from aa in context.ApprovalAnnotations
                                   join ap in context.ApprovalPages on aa.Page equals ap.ID
                                   join a in context.Approvals on ap.Approval equals a.ID
                                   where a.ID == objApproval.ID && aa.ExternalCreator == u
                                   select aa.ID).FirstOrDefault();


                annotationIdsOfDeletedExternalUsers.Add(annotations);
            }

            var externalCollaborators = (from aa in context.ApprovalAnnotations
                                         join ap in context.ApprovalPages on aa.Page equals ap.ID
                                         join a in context.Approvals on ap.Approval equals a.ID
                                         join exc in context.ExternalCollaborators on aa.ExternalCreator equals exc.ID
                                         join j in context.Jobs on a.Job equals j.ID
                                         join ac in context.Accounts on j.Account equals ac.ID

                                         let lastOpenedDate = (from auv in context.ApprovalUserViewInfoes
                                                               where auv.Approval == objApproval.ID && auv.User == aa.ExternalCreator
                                                               orderby auv.ViewedDate descending
                                                               select auv.ViewedDate).FirstOrDefault()

                                         let colDecisions = (from acd in context.ApprovalCollaboratorDecisions
                                                             where acd.Approval == objApproval.ID && acd.ExternalCollaborator == aa.ExternalCreator && acd.Phase == a.CurrentPhase
                                                             select acd).ToList()

                                         let annotationsAndReplies = (from ann in context.ApprovalAnnotations
                                                                      join app in context.ApprovalPages on a.ID equals app.Approval
                                                                      where ann.Page == app.ID && ann.ExternalCreator == aa.ExternalCreator
                                                                      select ann.Parent).ToList()
                                         where a.ID == objApproval.ID && !listOfUsersInSharedApprovals.Contains((int)aa.ExternalCreator) && a.CurrentPhase == aa.Phase && annotationIdsOfDeletedExternalUsers.Contains(aa.ID)
                                         select new
                                         {
                                             ExternalCollaborator = aa.ExternalCreator,
                                             colDecisions,
                                             Name = exc.GivenName + " " + exc.FamilyName,
                                             EmailAddress = exc.EmailAddress,
                                             SharedDate = aa.AnnotatedDate,
                                             annotationsAndReplies,
                                             IsExpired = true,
                                             RoleKey = "RDO",
                                             lastOpenedDate,
                                             aa.Phase
                                         }).ToList();

            foreach (var objSharedApproval in externalCollaborators)
            {
                var lstAppColDecision = objSharedApproval.colDecisions;

                string completedHtml = "-";
                string openedHtml = "-";
                DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, loggedAccount.TimeZone);

                if (objSharedApproval.lastOpenedDate != default(DateTime))
                {
                    DateTime dtOpened = objSharedApproval.lastOpenedDate;
                    dtOpened = GMGColorFormatData.GetUserTimeFromUTC(dtOpened, loggedAccount.TimeZone);
                    openedHtml = GMGColorFormatData.GetFormattedDate(dtOpened, loggedAccount, context) + "<br /><small>" + GMGColorFormatData.GetFormattedTime(dtOpened, loggedAccount) + "</small>";
                }

                if (lstAppColDecision.Count > 0 && lstAppColDecision[0].CompletedDate != null)
                {
                    DateTime dtCompleted = lstAppColDecision[0].CompletedDate.Value;
                    dtCompleted = GMGColorFormatData.GetUserTimeFromUTC(dtCompleted, loggedAccount.TimeZone);
                    DateTimeSpan dateSpan = DateTimeSpan.CompareDates(dtCompleted, userCurTime);
                    completedHtml = "<small rel=\"tooltip\" data-original-title=\" (" + dateSpan.Days + " " + GMGColor.Resources.Resources.lblDays + ", " + dateSpan.Hours + " " + GMGColor.Resources.Resources.lblHours + ") \">" + GMGColorFormatData.GetFormattedDate(dtCompleted, objApproval.Job1.Account1, context) + " " + GMGColorFormatData.GetFormattedTime(dtCompleted, objApproval.Job1.Account1) + "</small>";
                }

                activityList.Add(new ApprovalDetailsCollaboratorActivity
                {
                    UserID = (int)objSharedApproval.ExternalCollaborator,
                    OpenedDate = lstAppColDecision.Count > 0 ? lstAppColDecision[0].OpenedDate.GetValueOrDefault() : default(DateTime),
                    Name = objSharedApproval.Name,
                    EmailAddress = objSharedApproval.EmailAddress,
                    Sent = objSharedApproval.SharedDate,
                    Opened = openedHtml,
                    Annotations = objSharedApproval.annotationsAndReplies.Count(t => t == null),
                    Replies = objSharedApproval.annotationsAndReplies.Count(t => t != null),
                    DecisionMade = (lstAppColDecision.Count > 0 && lstAppColDecision[0].Decision != null)
                                                                  ? (approvalCustomDecision.Any(d => d.DecisionId == lstAppColDecision[0].Decision && !string.IsNullOrEmpty(d.Name)))
                                                                                 ? approvalCustomDecision.Where(d => d.DecisionId == lstAppColDecision[0].Decision).Select(d => d.Name).FirstOrDefault()
                                                                                 : ApprovalDecision.GetLocalizedApprovalDecisionName(lstAppColDecision[0].ApprovalDecision)
                                                                  : GMGColor.Resources.Resources.lblNone,
                    DecisionId = lstAppColDecision.Count > 0 ? lstAppColDecision[0].Decision.GetValueOrDefault() : 0,
                    Completed = completedHtml,
                    IsPrimaryDecisionMaker = (objApproval.ExternalPrimaryDecisionMaker != null && objSharedApproval.ExternalCollaborator == objApproval.ExternalPrimaryDecisionMaker),
                    IsAccessRevoked = objSharedApproval.IsExpired,
                    PhaseId = objSharedApproval.Phase.GetValueOrDefault(),
                    CollaboratorRoleKey = objSharedApproval.RoleKey
                });
            }
            return activityList;
        }

        public static void RestoreApproval(int approvalId, int userId, DbContextBL context)
		{
			var approval = (from a in context.Approvals where a.ID == approvalId select a).FirstOrDefault();
			if (approval != null)
			{
				approval.IsDeleted = false;
				approval.ModifiedDate = DateTime.UtcNow;
				approval.Modifier = userId;
			}

			// remove entry from ApprovalDeleteHistory
			ApprovalUserRecycleBinHistory(approval, userId, false, context);
		}

		/// <summary>
		/// Removes the expired CleanupExpiredCollaborateAPISessions
		/// </summary>
		/// <param name="context"></param>
		/// <param name="maxAgeHours"></param>
		public static void CleanupExpiredCollaborateAPISessions(DbContextBL context, int maxAgeHours)
		{
			var expiredSessions = (from cas in context.CollaborateAPISessions
								   where DbFunctions.DiffHours(cas.TimeStamp, DateTime.UtcNow) > maxAgeHours
								   select cas).ToList();

			if (expiredSessions.Count > 0)
			{
				context.CollaborateAPISessions.RemoveRange(expiredSessions);
				context.SaveChanges();
			}
		}

        public static void RemoveExpiredCollaborateAPISessions()
        {
            using (DbContextBL context = new DbContextBL())
            {
                var dt = DateTime.UtcNow.AddDays(-2);
                var expiredSessions = (from cas in context.CollaborateAPISessions
                                       where cas.TimeStamp < dt
                                       select cas).ToList();

                if (expiredSessions.Count > 0)
                {
                    context.CollaborateAPISessions.RemoveRange(expiredSessions);
                    context.SaveChanges();
                }
            }

        }


        /// <summary>
        /// Removes expired job medatadatas that did not result into a CoZone job from the REST API
        /// </summary>
        /// <param name="context"></param>
        /// <param name="maxAgeHours"></param>
        public static void CleanupExpiredJobMetadata(DbContextBL context)
		{
			var expiredjobs = (from jmt in context.CollaborateAPIJobMetadatas
							   where DbFunctions.DiffHours(jmt.CreatedDate, DateTime.UtcNow) > 24
							   select jmt).ToList();

			if (expiredjobs.Count > 0)
			{
				context.CollaborateAPIJobMetadatas.RemoveRange(expiredjobs);
				context.SaveChanges();
			}
		}

		/// <summary>
		/// Sends the new job/approval status to an external service
		/// </summary>
		/// <param name="details"></param>
		/// <param name="accountId"></param>
		/// <param name="context"></param>
		public static void PushJobStatus(JobStatusDetails details, int accountId, DbContextBL context)
		{
			var jobClient = new JobClient();
            details.callBackKey = "STS";
            var url = (from ueas in context.UploadEngineAccountSettings
					   join a in context.Accounts on ueas.ID equals a.UploadEngineSetings
					   where a.ID == accountId
					   select new { ueas.PostStatusUpdatesURL, ueas.AllowApiStatusUpdate }).FirstOrDefault();
			if (!string.IsNullOrEmpty(url.PostStatusUpdatesURL))
			{
                if (url.AllowApiStatusUpdate)
                    jobClient.PushStatus(details, url.PostStatusUpdatesURL);
			}
		}

        public static void PushCallBackTrigger(JobStatusDetails details, int accountId, DbContextBL context, int callBackType)
        {
            var jobClient = new JobClient();
            var callbackSettings = (from ueas in context.UploadEngineAccountSettings
                       join a in context.Accounts on ueas.ID equals a.UploadEngineSetings
                       where a.ID == accountId
                       select new {
                           ueas.PostStatusUpdatesURL,
                           ueas.AllowApiApprovalDueDate,
                       ueas.AllowApiOnAnnotation,
                       ueas.AllowApiPhaseDueDate
                       }).FirstOrDefault();

            if (!string.IsNullOrEmpty(callbackSettings.PostStatusUpdatesURL))
            {
                if (callBackType == (int)CallBackType.ApprovalDueDate && callbackSettings.AllowApiApprovalDueDate)
                {
                    details.callBackKey = "ADD";
                    jobClient.PushStatus(details, callbackSettings.PostStatusUpdatesURL);

                }
                else if (callBackType == (int)CallBackType.PhaseDueDate && callbackSettings.AllowApiPhaseDueDate) {
                    details.callBackKey = "PDD";
                    if (details.phaseName != null)
                    {
                        int phaseId = Convert.ToInt32(details.phaseName);
                        int approvalId = Convert.ToInt32(details.approvalGuid);
                        var phaseDetails = (from apjp in context.ApprovalJobPhases
                                            join ap in context.ApprovalPhases on apjp.PhaseTemplateID equals ap.ID
                                            join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                                            join apjw in context.ApprovalJobWorkflows on apjp.ApprovalJobWorkflow equals apjw.ID
                         let approvalkey = (from ap in context.Approvals where ap.ID == approvalId select ap.Guid).FirstOrDefault()

                                            where apjp.ID == phaseId
                                            select new
                                            {
                                                ApprovalGuid = approvalkey,
                                                PhaseName = apjp.Name,
                                                WorkflName = apjw.Name,
                                                PhaseGuid = ap.Guid,
                                                WorkflowGuid = aw.Guid
                                            }).FirstOrDefault();

                        if (phaseDetails != null)
                        {
                            details.approvalGuid = phaseDetails.ApprovalGuid;
                            details.phaseName = phaseDetails.PhaseName;
                            details.workflowName = phaseDetails.WorkflName;
                            details.phaseGuid = phaseDetails.PhaseGuid;
                            details.workflowGuid = phaseDetails.WorkflowGuid;
                        }
                    }

                    jobClient.PushStatus(details, callbackSettings.PostStatusUpdatesURL);
                }
                else if (callBackType == (int)CallBackType.LastPhaseDueDate && callbackSettings.AllowApiPhaseDueDate)
                {
                    details.callBackKey = "LPD";
                    if (details.phaseName != null)
                    {
                        int phaseId = Convert.ToInt32(details.phaseName);
                        var phaseDetails = (from apjp in context.ApprovalJobPhases
                                            join ap in context.ApprovalPhases on apjp.PhaseTemplateID equals ap.ID
                                            join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                                            join apjw in context.ApprovalJobWorkflows on apjp.ApprovalJobWorkflow equals apjw.ID
                                            where apjp.ID == phaseId
                                            select new
                                            {
                                                PhaseName = apjp.Name,
                                                WorkflName = apjw.Name,
                                                PhaseGuid = ap.Guid,
                                                WorkflowGuid = aw.Guid
                                            }).FirstOrDefault();

                        if (phaseDetails != null)
                        {
                            details.phaseName = phaseDetails.PhaseName;
                            details.workflowName = phaseDetails.WorkflName;
                            details.phaseGuid = phaseDetails.PhaseGuid;
                            details.workflowGuid = phaseDetails.WorkflowGuid;
                        }
                    }
                    jobClient.PushStatus(details, callbackSettings.PostStatusUpdatesURL);
                }
                else if (callBackType == (int)CallBackType.OnAnnotation && callbackSettings.AllowApiOnAnnotation) {
                    details.callBackKey = "ANN";
                    if (details.phaseName != null)
                    {
                        int phaseId = Convert.ToInt32(details.phaseName);
                        int approvalId = Convert.ToInt32(details.approvalGuid);
                        var phaseDetails = (from apjp in context.ApprovalJobPhases
                                            join ap in context.ApprovalPhases on apjp.PhaseTemplateID equals ap.ID
                                            join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                                            join apjw in context.ApprovalJobWorkflows on apjp.ApprovalJobWorkflow equals apjw.ID

                         let approvalkey = (from ap in context.Approvals where ap.ID == approvalId select ap.Guid).FirstOrDefault()
                                            where apjp.ID == phaseId
                                            select new
                                            {
                                                ApprovalGuid = approvalkey,
                                                PhaseName = apjp.Name,
                                                WorkflName = apjw.Name,
                                                PhaseGuid = ap.Guid,
                                                WorkflowGuid = aw.Guid
                                            }).FirstOrDefault();

                        if (phaseDetails != null)
                        {
                            details.approvalGuid = phaseDetails.ApprovalGuid;
                            details.phaseName = phaseDetails.PhaseName;
                            details.workflowName = phaseDetails.WorkflName;
                            details.phaseGuid = phaseDetails.PhaseGuid;
                            details.workflowGuid = phaseDetails.WorkflowGuid;
                        }
                    }

                    details.annotationMade = "True";
                    jobClient.PushStatus(details, callbackSettings.PostStatusUpdatesURL);
                }
            }
        }

        public static List<AnnotationReportPhase> GetApprovalPhasesForAnnotationReport(int appId, DbContextBL context)
		{
			return (from ap in context.Approvals
					join apjp in context.ApprovalJobPhaseApprovals on ap.ID equals apjp.Approval
					join apphase in context.ApprovalJobPhases on apjp.Phase equals apphase.ID
					let hasAnnotation = (from annot in context.ApprovalAnnotations
										 join app in context.ApprovalPages on annot.Page equals app.ID
										 where app.Approval == ap.ID && annot.Parent == null && annot.Phase == apphase.ID
										 select annot.ID).Any()
					where ap.ID == appId && apjp.Phase <= ap.CurrentPhase && apphase.ShowAnnotationsToUsersOfOtherPhases && hasAnnotation
					select new AnnotationReportPhase
					{
						ID = apphase.ID,
						Name = apphase.Name,
						IsSelected = apphase.ID == ap.CurrentPhase
					}).ToList();
		}

        public static int? GetApprovalLastVersion(int appId, DbContextBL context)
        {
            return (from ap in context.Approvals
                    where ap.Job == (from app in context.Approvals where app.ID == appId select app.Job).FirstOrDefault()  && ap.IsDeleted == false 
                    && ap.DeletePermanently == false orderby ap.Version descending
                    select ap.Version ).FirstOrDefault();                   
        }

        public static int? GetApprovalSelectedVersion(int appId, DbContextBL context)
        {
            return (from ap in context.Approvals
                    where ap.ID == appId && ap.IsDeleted == false
                    && ap.DeletePermanently == false
                    select ap.Version).FirstOrDefault();
        }

        /// <summary>
        /// Creates a queue message when job/approval status is changed
        /// </summary>
        /// <param name="approvalId"></param>
        public static void CreateQueueMessageForUpdateApprovalJobStatus(int approvalId)
		{
			//Create queue message to be sent to FTP Service
			CustomDictionary<string, string> dict = new CustomDictionary<string, string>();

			dict.Add(Constants.ApprovalMsgApprovalID, approvalId.ToString());
			dict.Add(Constants.FTPAction, FTPAction.UploadStatus.ToString());

			GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.FTPJob);
		}

		public static string GetApprovapStatus(int approvalId, int account, GMGColorContext context)
		{
			var approvlaStatus = (from d in context.GetApprovalDecission(approvalId, account) select d).FirstOrDefault();
			return approvlaStatus != null ? approvlaStatus.RetVal : string.Empty;
		}

		public static List<int> GetApprovalsIdsForVersionsIds(DbContextBL context, List<int> versionsid)
		{
			List<int> jobIds = context.Approvals.Where(a => versionsid.Contains(a.ID)).Select(av => av.Job).Distinct().ToList();
			return jobIds.Select(jobId => context.Approvals.Where(a => a.Job == jobId).Select(ap => ap.ID).Max()).ToList();
		}

		public static void PopulateNewApprovalModel(NewApprovalModel model, int loggedUserId, DbContextBL context)
		{
			var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(model.objLoggedAccount.ID, ProofStudioPenWidthDefaultValue, false, context);
			model.ApprovalWorkflows = new List<ApprovalWorkflowModel>();
            model.Checklists = new List<ChecklistModel>();
			if (model.Job > 0)
			{
				model.Title = JobBL.GetJobTitleByID(model.Job, context);

				var versions = (from a in context.Approvals
								where a.Job == model.Job
								orderby a.Version descending
								select new
								{
									a.Version,
									a.FileName
								}).ToList();


				model.Version = versions.Any() ? (versions.First().Version + 1) : 1;
				model.OriginalFileName = versions.Any() ? versions.Last().FileName : String.Empty;

				var selectedWorkflow = GetVersionSelectedApprovalJobWorkflows(model.Job, context);
				if (selectedWorkflow != null)
				{
					model.ApprovalWorkflowId = selectedWorkflow.ID;
				}

                var selectedChecklist = GetVersionSelectedChecklist(model.Job, context);
                if (selectedChecklist != null)
                {
                    model.ChecklistId = selectedChecklist.ID;
                }

                var approvalInfo = (from a in context.Approvals
									join j in context.Jobs on a.Job equals j.ID

									let collaborators = (from ac in context.ApprovalCollaborators
														 where ac.Approval == a.ID
														 select ac.Collaborator).ToList()
									where
										(collaborators.Contains(loggedUserId) || j.JobOwner == loggedUserId) &&
										j.ID == model.Job &&
										j.JobStatu.Key != "ARC" &&
										a.IsDeleted == false
									orderby a.Version descending
									select new
									{
										a.ID,
										a.Creator,
										a.Owner,
										a.AllowOtherUsersToBeAdded
									}).FirstOrDefault();

				model.Creator = approvalInfo.Creator;
				model.Owner = approvalInfo.Owner;

				if (model.ApprovalWorkflowId > 0)
				{
					var currentPhasePosition = selectedWorkflow.RerunWorkflow ? 1 : GetCurrentJobPhasePosition(approvalInfo.ID, context);
					if (string.IsNullOrEmpty(model.PhasesCollaborators))
					{
						model.PhaseCollaboratorsModel = GetVersionExistingPhasesCollaborators(approvalInfo.ID, model.ApprovalWorkflowId, currentPhasePosition, context);
					}
					model.WorkflowPhases = GetApprovalJobPhases(model.ApprovalWorkflowId, currentPhasePosition, context);
					model.AllowOtherUsersToBeAdded = approvalInfo.AllowOtherUsersToBeAdded;

					//get selected approval job workflow
					model.ApprovalWorkflows = new List<ApprovalWorkflowModel> { selectedWorkflow };
				}
				else
				{
					model.ApprovalVersionCollaboratorInfo = new ApprovalVersionCollaboratorInfo();
					model.ApprovalVersionCollaboratorInfo.LoadNewVersionCollaborats(approvalInfo.ID, context);


					if (collaborateSettings != null && collaborateSettings.KeepExternalUsersWhenUploadingNewVersion)
					{
						model.ApprovalVersionCollaboratorInfo.LoadNewVersionExternalCollaborators(approvalInfo.ID, context);
					}
				}
			}
			else
			{
				model.Title = string.Empty;
				model.Version = 0;
				model.Creator = loggedUserId;
				model.Owner = loggedUserId;
				//get approval workflows templates
				var approvalWorkflows = ApprovalWorkflowBL.GetApprovalWorkflows(model.objLoggedAccount.ID, context);
				model.ApprovalWorkflows = approvalWorkflows.Where(aw => aw.HasPhases).ToList();
				if (model.ApprovalWorkflowId > 0)
				{
					model.WorkflowPhases = GetApprovalWorkflowPhases(model.ApprovalWorkflowId, context);
				}
			}

            var checklists = ChecklistBL.GetCheckLists(model.objLoggedAccount.ID, context);
            model.Checklists = checklists.Where(cl => cl.HasItems).ToList();
            model.IsDueDateSelected = !collaborateSettings.DisableFileDueDateinUploader;

			model.ListDecisionMakers = context.Users.Where(m => m.Account == model.objLoggedAccount.ID).ToDictionary(m => m.ID, m => m.GivenName + " " + m.FamilyName);
		}


      
       

        public static string GetVersionSufix(string fileName)
		{
            // CHECKING THE LENGTH OF SUFIX, TO GET MAX LENGTH OF 36 CHARACTER ONLY
            int totalChar = fileName.LastIndexOf("_v", StringComparison.InvariantCultureIgnoreCase);
            if (totalChar > 0)
            {
                return fileName.Substring(totalChar + 1, ((fileName.Length - totalChar) > 36 ? 36 : (fileName.Length - totalChar) - 1));
            }
            return string.Empty;
		}

		public static List<AdvancedSearchName> GetAdvancedSearchNames(int accountID, DbContextBL context)
		{
			return (from advs in context.AdvancedSearches
					where advs.AccountId == accountID
					orderby advs.Name ascending
					select new AdvancedSearchName
					{
						ID = advs.AdvancedSearchId,
						Name = advs.Name,
					}).ToList();
		}
		public static int GetAdvancedSearchesCount(int accountID, DbContextBL context)
		{
			return (from advs in context.AdvancedSearches
					where advs.AccountId == accountID
					select advs.AdvancedSearchId).Count();
		}

		public static AdvancedSearch GetAdvancedSearch(int advancedSearchId, DbContextBL context, string datePattern)
		{
			var adsGeneric = context.AdvancedSearches.Where(advs => advs.AdvancedSearchId == advancedSearchId)
					.Select(s => new
					{
						ID = s.AdvancedSearchId,
						AdvSName = s.Name,
						AdvSIsOverdue = s.OverdueFiles != null && (bool)s.OverdueFiles,
						AdvSInProgress = s.OverdueFiles != null && !(bool)s.OverdueFiles,
						AdvSWithinATimeframe = s.WithinATimeframe != null && (bool)s.WithinATimeframe,
						AdvSTimeFrameMin = s.MinTimeDeadline,
						AdvSTimeFrameMax = s.MaxTimeDeadline,
						AdvSSearchByTitle = s.JobTitle
					}).FirstOrDefault();
			if (adsGeneric == null)
				return null;
			return new AdvancedSearch
			{
				ID = adsGeneric.ID,
				AdvSSearchByTitle = adsGeneric.AdvSSearchByTitle,
				AdvSName = adsGeneric.AdvSName,
				AdvSIsOverdue = adsGeneric.AdvSIsOverdue,
				AdvSInProgress = adsGeneric.AdvSInProgress,
				AdvSWithinATimeframe = adsGeneric.AdvSWithinATimeframe,
				AdvSTimeFrameMin = adsGeneric.AdvSTimeFrameMin.ToString(datePattern),
				AdvSTimeFrameMax = adsGeneric.AdvSTimeFrameMax.ToString(datePattern),
				AdvSSelectedUsers = context.AdvancedSearchUsers.Where(advs => advs.AdvancedSearchId == advancedSearchId)
					.Select(u => u.UserId)
					.ToList(),
				AdvSSelectedGroups =
					context.AdvancedSearchGroups.Where(advs => advs.AdvancedSearchId == advancedSearchId)
						.Select(g => g.GroupId)
						.ToList(),
				AdvSSelectedWorkflows =
					context.AdvancedSearchWorkflows.Where(advs => advs.AdvancedSearchId == advancedSearchId)
						.Select(w => w.Workflow)
						.ToList(),
				AdvSSelectedPhases =
					context.AdvancedSearchApprovalPhases.Where(advs => advs.AdvancedSearchId == advancedSearchId)
						.Select(p => p.ApprovalPhaseId)
						.ToList(),
			};
		}

		public static bool AdvancedSearchNameExist(AdvancedSearch model, int LoggedAccountId, DbContextBL context)
		{
			if (model.ID == 0)
			{
				return context.AdvancedSearches.Any(g => g.Name == model.AdvSName);
			}
			else
			{
				return context.AdvancedSearches.Any(g => (g.AdvancedSearchId != model.ID) && (g.Name == model.AdvSName));
			}
		}

		/// <summary>
		/// Adds or updates and advanced search
		/// </summary>
		/// <param name="model">The advanced search data</param>
		/// <param name="LoggedAccountId">The logged account for wich the search is saved/updated</param>
		/// <param name="context">The database context</param>
		public static void AddOrUpdateAdvancedSearch(AdvancedSearch model, int LoggedAccountId, DbContextBL context, string datePattern)
		{
			if (model.ID == 0)
			{
				GMGColorDAL.AdvancedSearch advs = model.ToDALObj(datePattern);
				advs.AccountId = LoggedAccountId;
				AddAdvancedSearch(advs, context);
			}
			else
			{
				UpdateAdvancedSearch(model, context, datePattern);
			}
		}

		public static void AddAdvancedSearch(GMGColorDAL.AdvancedSearch advs, DbContextBL context)
		{
			context.AdvancedSearches.Add(advs);
			context.SaveChanges();

		}
		public static void UpdateAdvancedSearch(AdvancedSearch model, DbContextBL context, string datePattern)
		{
			// remove related and add modified
			var advsGroups = context.AdvancedSearchGroups.Where(g => g.AdvancedSearchId == model.ID);
			context.AdvancedSearchGroups.RemoveRange(advsGroups);

			var advSUesers = context.AdvancedSearchUsers.Where(u => u.AdvancedSearchId == model.ID);
			context.AdvancedSearchUsers.RemoveRange(advSUesers);

			var advSWorkflows = context.AdvancedSearchWorkflows.Where(w => w.AdvancedSearchId == model.ID);
			context.AdvancedSearchWorkflows.RemoveRange(advSWorkflows);

			var advSPhases = context.AdvancedSearchApprovalPhases.Where(p => p.AdvancedSearchId == model.ID);
			context.AdvancedSearchApprovalPhases.RemoveRange(advSPhases);


			var advSfromModel = context.AdvancedSearches.Find(model.ID);
			GMGColorDAL.AdvancedSearch advs = model.ToDALObj(datePattern, advSfromModel);

			context.AdvancedSearches.Attach(advs);
			context.Entry(advs).State = EntityState.Modified;

			context.SaveChanges();
		}

		public static void DeleteAdvancedSearch(int advSId, DbContextBL context)
		{
			var advS = context.AdvancedSearches.Find(advSId);
			context.AdvancedSearches.Remove(advS);
			context.SaveChanges();
		}

		public static void JobStatusAutoUpdate(List<int> approvalIds, DbContextBL context)
		{
			var jobsIdToUpdate = (from a in context.Approvals
								  join j in context.Jobs on a.Job equals j.ID
								  join js in context.JobStatus on j.Status equals js.ID
								  where js.Key == "NEW" && approvalIds.Contains(a.ID)
								  select j.ID);

			if (jobsIdToUpdate.Any())
			{
				foreach (var jobID in jobsIdToUpdate)
				{
					Job updateJob = new Job()
					{
						ID = jobID
					};
					context.Jobs.Attach(updateJob);
					updateJob.Status = (int)JobStatu.Status.ChangesRequired;
					context.Entry(updateJob).Property(p => p.Status).IsModified = true;
				}
				context.Configuration.ValidateOnSaveEnabled = false;
				context.SaveChanges();
				context.Configuration.ValidateOnSaveEnabled = true;
			}
		}
		public static void JobStatusAutoUpdate(GMGColorDAL.Approval approval, int? decisionUser, DbContextBL context)
		{
			//check if approval is on a workflow
			if (approval.CurrentPhase == null)
			{
				//if not check if all the collaborators have approved it and set the job status to 'Completed'
				if (approval.ApprovalCollaboratorDecisions.All(t => t.ApprovalDecision != null ) || approval.PrimaryDecisionMaker == decisionUser)
				{
					approval.Job1.Status = context.JobStatus.FirstOrDefault(t => t.Key == "COM").ID;
				}
			}
			else
			{
				//get the max phase of the approval
				var maxPhase = (from j in context.Jobs
								join a in context.Approvals on j.ID equals a.Job
								join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
								join ajp in context.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow
								where a.ID == approval.ID
								select new
								{
									ID = ajp.ID,
									PrimaryDecisionMaker = ajp.PrimaryDecisionMaker,
									Position = ajp.Position
								}).Distinct().OrderByDescending(t => t.Position).FirstOrDefault();

				//check if current phase is Max Phase
				if (maxPhase.ID == approval.CurrentPhase)
				{
					if (approval.ApprovalCollaboratorDecisions.Where(t => t.Phase == maxPhase.ID).All(p => p.Decision != null) ||
						approval.CurrentPhase == maxPhase.ID &&
												 maxPhase.PrimaryDecisionMaker == decisionUser
						)
					{
						approval.Job1.Status = context.JobStatus.FirstOrDefault(t => t.Key == "COM").ID;
					}
				}
			}
		}

		public static int GetUserIdByCollaborateAPISessionKey(string sessionKey, DbContextBL context)
		{
			return (from u in context.Users
					where u.Guid == sessionKey
					select u.ID).FirstOrDefault();
		}

		/// <summary>
		/// create notifications for approvals that are overdue
		/// </summary>
		public static void CheckApprovalsOverdue(int inLastNoOfDays = 1)
		{
			try
			{
				using (DbContextBL context = new DbContextBL())
				{
					List<ApprovalOverdueM> overdueApprovalsList = GetApprovalsOverdue(context, inLastNoOfDays);

					if (overdueApprovalsList.Count > 0)
					{
						foreach (var overdueApproval in overdueApprovalsList)
						{
							NotificationServiceBL.CreateNotification(new ApprovalOverdue()
							{
								CreatedDate = DateTime.UtcNow,
								ApprovalsIds = new int[] { overdueApproval.ApprovalId },
								EventCreator = (int?)overdueApproval.CreatorUserId
							},
								null,
								context);
							var approval = context.Approvals.Find(overdueApproval.ApprovalId);
							approval.OverdueNotificationSent = true;
						}

						context.SaveChanges();
					}
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL("ApprovalBL.CheckApprovalsOverdue() - Error occurred, Message: {0}", ex);
			}
		}

		/// <summary>
		/// Returns a list with approvals that have deadline less or equal that inLastDaysOverdue
		/// </summary>
		/// <param name="context">Database context</param>
		/// <returns></returns>
		public static List<ApprovalOverdueM> GetApprovalsOverdue(DbContextBL context, int inLastNoOfDays)
		{
			int completedStatusId = JobStatu.GetJobStatus(JobStatu.Status.Completed, context).ID;
			int arhivedStatusId = JobStatu.GetJobStatus(JobStatu.Status.Archived, context).ID;
			int changesCompleteStatusId = JobStatu.GetJobStatus(JobStatu.Status.ChangesComplete, context).ID;

			// get jobs that have maximum positions
			var maxPositions = context.ApprovalJobPhases
										.GroupBy(p => p.ApprovalJobWorkflow)
										.Select(g => new
										{
											workflow = g.Key,
											maxPosition = g.Max(x => x.Position)
										});
			// get approvals that are in the latest phase
			var latestPhasesIds = (from p in context.ApprovalJobPhases
								   join maxP in maxPositions on p.ApprovalJobWorkflow equals maxP.workflow
								   where p.Position == maxP.maxPosition
								   select p.ID);
			// get approvals in the latest phase and completed phase
			var completedPhaseIds = (from p in context.ApprovalJobPhases
									 join cp in context.ApprovalJobPhaseApprovals on p.ID equals cp.Phase
									 where cp.PhaseMarkedAsCompleted == true && latestPhasesIds.Contains(p.ID)
									 select p.ID).Distinct();

			return (from ap in context.Approvals
					join j in context.Jobs on ap.Job equals j.ID
					where ap.IsDeleted == false && j.Status != completedStatusId && j.Status != arhivedStatusId && j.Status != changesCompleteStatusId &&
						(ap.OverdueNotificationSent == null || ap.OverdueNotificationSent == false) &&
						(DbFunctions.DiffDays(DbFunctions.TruncateTime(ap.Deadline), DbFunctions.TruncateTime(DateTime.UtcNow)) <= inLastNoOfDays) && // load only for the last no of days
						(ap.CurrentPhase == null || !completedPhaseIds.Contains((int)ap.CurrentPhase)) &&
						(ap.Deadline < DateTime.UtcNow) && ap.DeletePermanently == false
                    select new ApprovalOverdueM
					{
						ApprovalId = ap.ID,
						CreatorUserId = ap.CreatorUser.ID
					}).ToList();
		}

        /// <summary>
        /// Move Approvals to Recycle Bin
        /// </summary>
        /// 
        public static string DeleteArchivedApprovalsAfterSixMonths()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    List<int> accounttypes = new List<int> { 4, 5 };
                    var accountDetails = context.Accounts.Where(x => accounttypes.Contains(x.AccountType) && x.IsSuspendedOrDeletedByParent == false).Select(x => x.ID).ToList();
                    string _approval = string.Empty;
                    DateTime dt = new DateTime();

                    foreach (var accnt in accountDetails)
                    {

                        var accountSettings = context.AccountSettings.Where(x => x.Account == accnt && x.Name.Contains("ArchiveTimeLimit")).Select(x => x).ToList();

                        if (accountSettings.Count == 0)
                        {
                            dt = DateTime.Now.AddMonths(-15);
                        }
                        else if (accountSettings != null)
                        {
                            if (!Convert.ToBoolean(accountSettings[0].Value))
                            {
                                dt = DateTime.Now.AddMonths(-15);
                            }
                            else
                            {
                                if (Convert.ToInt32(accountSettings[2].Value) == 1)
                                {
                                    dt = DateTime.Now.AddDays(-(Convert.ToInt32(accountSettings[1].Value)));
                                }
                                else if (Convert.ToInt32(accountSettings[2].Value) == 2)
                                {
                                    dt = DateTime.Now.AddMonths(-(Convert.ToInt32(accountSettings[1].Value)));
                                }
                                else if (Convert.ToInt32(accountSettings[2].Value) == 3)
                                {
                                    dt = DateTime.Now.AddYears(-(Convert.ToInt32(accountSettings[1].Value)));
                                }
                            }
                        }

                        var Approvals = (from j in context.Jobs
                                         join a in context.Approvals on j.ID equals a.Job
                                         where j.Account == accnt && j.Status == ((int)JobStatu.Status.Archived - 12) && a.IsDeleted == false && a.DeletePermanently == false && a.ModifiedDate < dt
                                         select a).ToList();

                        if (Approvals.Count > 0)
                        {
                            foreach (var a in Approvals)
                            {
                                a.DeletePermanently = true;
                                a.ModifiedDate = DateTime.UtcNow;

                                _approval = _approval + a.ID + " , ";
                            }
                            context.SaveChanges();
                        }
                    }
                    return _approval;
                }
                
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("ApprovalBL.DeleteArchivedApprovalsAfterSixMonths() - Error occurred, Message: {0}", ex);
            }


        }

        /// <summary>
        /// Delete Approvals Permanently
        /// </summary>
        /// 
        public static string DeleteRecycleBinApprovalsAfterOneMonth()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    var dt = DateTime.UtcNow.AddMonths(-1);

                    var Approvals = (from a in context.Approvals
                                     where a.IsDeleted == true && a.DeletePermanently == false && a.ModifiedDate < dt
                                     select a).ToList();
                    string _approvals = string.Empty; 

                    if (Approvals.Count > 0)
                    {
                        foreach (var a in Approvals)
                        { 
                            a.DeletePermanently = true;
                            a.ModifiedDate = DateTime.UtcNow;
                            _approvals = _approvals + a.ID + ", ";
                        }
                        context.SaveChanges();
                    }
                    return _approvals;
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("ApprovalBL.DeleteRecycleBinApprovalsAfterOneMonth() - Error occurred, Message: {0}", ex);
            }


        }



        /// <summary>
        /// Returns true if the decision was taken by PDM
        /// </summary>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static bool IsApprovalDecidedByPDM(int? approvalId, int? decidedbyUserId, DbContextBL context)
		{
			if (approvalId == null || decidedbyUserId == null)
				return false;
			int? pdmId = (from app in context.Approvals
						  where app.ID == approvalId
						  select app.PrimaryDecisionMaker).FirstOrDefault();

			return (pdmId != null && pdmId == decidedbyUserId);
		}

		public static string PhasesWithPdmDecisionHasPdm(ApprovalWorkflowPhasesInfo phasesCollaborators, DbContextBL context)
		{
			string phases = null;
			List<int> phasesIds = (from phase in phasesCollaborators.ApprovalWorkflowPhases where phase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker && (phase.PDMId == null || phase.PDMId == 0) select phase.Phase).ToList();
			if (phasesIds.Count > 0)
			{
				var names = context.ApprovalPhases.Where(ap => phasesIds.Contains(ap.ID)).Select(ap => ap.Name).ToArray();
				phases = String.Join(", ", names);
			}

			return phases;
		}

		/// <summary>
		/// Send email to users that have access to the current phase and weren't in previous phases
		/// </summary>
		/// <param name="selectedApproval">Approval id</param>
		/// <param name="loggedUser"></param>
		/// <param name="context"></param>
		public static void SendEmailsToUsersFromCurrentPhase(int selectedApproval, GMGColorDAL.User loggedUser, DbContextBL context)
		{
			var currentPhase = (from a in context.Approvals
								join ajp in context.ApprovalJobPhases on a.CurrentPhase equals ajp.ID
								where a.ID == selectedApproval
								select new
								{
									ajp.ID,
									ajp.Position
								}).FirstOrDefault();

            // Email is Sending only for Current phase user( Except previous phase users)
			//var internalCollaborators = (from ac in context.ApprovalCollaborators
			//							 let collaboratorsFromCompletedPhases = (from acl in context.ApprovalCollaborators
			//																	 join ajp in context.ApprovalJobPhases on acl.Phase equals ajp.ID
			//																	 where ajp.Position < currentPhase.Position && acl.Approval == selectedApproval
			//																	 select acl.Collaborator).ToList()
			//							 where ac.Approval == selectedApproval && !collaboratorsFromCompletedPhases.Contains(ac.Collaborator) && ac.Phase == currentPhase.ID
			//							 select ac.Collaborator).ToList();

			//var externalCollaborators = (from sa in context.SharedApprovals
			//							 let collaboratorsFromCompletedPhases = (from acl in context.SharedApprovals
			//																	 join ajp in context.ApprovalJobPhases on acl.Phase equals ajp.ID
			//																	 where ajp.Position < currentPhase.Position && acl.Approval == selectedApproval
			//																	 select acl.ExternalCollaborator).ToList()
			//							 where sa.Approval == selectedApproval && !collaboratorsFromCompletedPhases.Contains(sa.ExternalCollaborator) && sa.Phase == currentPhase.ID
			//							 select sa.ExternalCollaborator).ToList();


            // Now we are sending "New Approval added" mail to all internal and user presnt in current phase 
            var internalCollaborators = (from ac in context.ApprovalCollaborators
                                         where ac.Approval == selectedApproval && ac.Phase == currentPhase.ID
                                         select ac.Collaborator).ToList();

            var externalCollaborators = (from sa in context.SharedApprovals
                                         where sa.Approval == selectedApproval && sa.Phase == currentPhase.ID
                                         select sa.ExternalCollaborator).ToList();

            if (internalCollaborators.Count > 0)
			{
				GMGColorLogging.log.InfoFormat("SendEmailsToUsersFromCurrentPhase: Sending internal collaborators messages. Emails to be sent: {0} | Approval: {1} | Logged user email: {2}"
					, new object[] { internalCollaborators.Count, selectedApproval, loggedUser == null ? "" : loggedUser.EmailAddress });
			}

			//internal collaborators
			foreach (int collaborator in internalCollaborators)
			{
				NotificationServiceBL.CreateNotification(new NewApprovalAdded
				{
					EventCreator = loggedUser != null ? loggedUser.ID : (int?)null,
					OptionalMessage = string.Empty,
					InternalRecipient = collaborator,
					ApprovalsIds = new[] { selectedApproval }
				},
															loggedUser,
															context
															);

			}

			if (externalCollaborators.Count > 0)
			{
				GMGColorLogging.log.InfoFormat("SendEmailsToUsersFromCurrentPhase: Sending external collaborators messages. Emails to be sent: {0} | Approval: {1} | Logged user email: {2}"
					, new object[] { externalCollaborators.Count, selectedApproval, loggedUser == null ? "" : loggedUser.EmailAddress });
			}

			//external users
			foreach (var collaborator in externalCollaborators)
			{
                var objSharedApproval = (from sa in context.SharedApprovals
                                         where sa.Approval == selectedApproval && sa.ExternalCollaborator == collaborator 
                                           && sa.Phase == currentPhase.ID
                                         select sa).FirstOrDefault();

                if (objSharedApproval != null)
                {
                    objSharedApproval.IsBlockedURL = false;
                    objSharedApproval.ExpireDate = DateTime.UtcNow.AddDays(1);
                    context.SaveChanges();
                }

                NotificationServiceBL.CreateNotification(new NewApprovalAdded
				{
					EventCreator = loggedUser != null ? loggedUser.ID : (int?)null,
					ExternalRecipient = collaborator,
					OptionalMessage = string.Empty,
					ApprovalsIds = new[] { selectedApproval }
				},
															loggedUser,
															context
															);
			}
		}

		/// <summary>
		/// Check if an advanced search name already exists for that account
		/// </summary>
		/// <param name="advsName">The search name</param>
		/// <param name="account">The account for the search </param>
		/// <param name="id">The search id</param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static bool isAdvsNameUnique(string advsName, int account, int ID, DbContextBL context)
		{
			if (advsName != null)
			{
				return !(from advs in context.AdvancedSearches
						 where advs.Name.ToLower() == advsName.ToLower().Trim() && advs.AccountId == account && advs.AdvancedSearchId != ID
						 select advs.AdvancedSearchId).Any();
			}
			return true;
		}

		public static void PushStatus(UserDetails loggedUser, JobStatusDetails approvalDetails, int loggedAccount, DbContextBL context, int? currentPhase = null)
		{
			approvalDetails.email = loggedUser.Email;
			approvalDetails.username = loggedUser.Username;

			if (currentPhase != null)
			{
				var phaseDetails = (from apjp in context.ApprovalJobPhases
									join ap in context.ApprovalPhases on apjp.PhaseTemplateID equals ap.ID
									join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
									join apjw in context.ApprovalJobWorkflows on apjp.ApprovalJobWorkflow equals apjw.ID
									where apjp.ID == currentPhase
									select new
									{
										PhaseName = apjp.Name,
										WorkflName = apjw.Name,
										PhaseGuid = ap.Guid,
										WorkflowGuid = aw.Guid
									}).FirstOrDefault();

				if (phaseDetails != null)
				{
					approvalDetails.phaseName = phaseDetails.PhaseName;
					approvalDetails.workflowName = phaseDetails.WorkflName;
					approvalDetails.phaseGuid = phaseDetails.PhaseGuid;
					approvalDetails.workflowGuid = phaseDetails.WorkflowGuid;
				}
			}

			PushJobStatus(approvalDetails, loggedAccount, context);
		}

		public static string GetApprovalIDs(string approvalIDs, string folderIDs, int loggedUserId, DbContextBL context)
		{
			if (approvalIDs == null)
			{
				approvalIDs = String.Empty;
			}

			if (!String.IsNullOrEmpty(folderIDs))
			{
				approvalIDs += ",";
				folderIDs = ((folderIDs.Length > 0) && (folderIDs.LastIndexOf(',') == (folderIDs.Length - 1)))
					? folderIDs.Remove((folderIDs.Length - 1), 1)
					: folderIDs;

				foreach (string folder in folderIDs.Trim().Split(','))
				{
					if (!String.IsNullOrEmpty(folder.Trim()))
					{
						approvalIDs = RetunApprovalIDs(int.Parse(folder), loggedUserId, context).Aggregate(approvalIDs, (current, id) => current + (id + ","));
					}
				}
			}

			approvalIDs = ((approvalIDs.Length > 0) && (approvalIDs.LastIndexOf(',') == (approvalIDs.Length - 1)))
					? approvalIDs.Remove((approvalIDs.Length - 1), 1)
					: approvalIDs;

			return approvalIDs;
		}

		private static IEnumerable<int> RetunApprovalIDs(int parentFolderId, int loggedUserId, DbContextBL context)
		{
			var lstApprovalIDs = new List<int>();

			var objFolder = FolderBL.GetFolderByID(parentFolderId, loggedUserId, context);

			if (objFolder != null)
			{
				if (objFolder.FolderCollaborators.Select(m => m.Collaborator).Contains(loggedUserId))
				{
					lstApprovalIDs = FolderBL.ReturnFoldersApprovalsIDs(objFolder, loggedUserId, context);

					foreach (var oFolder in objFolder.GetChilds(context))
					{
						lstApprovalIDs.AddRange(RetunApprovalIDs(oFolder.ID, loggedUserId, context));
					}
				}
			}

			return lstApprovalIDs;
		}

		public static void SendApprovalWasDownloadedNotification(List<int> approvalIds, GMGColorDAL.User loggedUser, DbContextBL context)
		{
			foreach (var approvalId in approvalIds)
			{
				NotificationServiceBL.CreateNotification(new ApprovalWasDownloaded
				{
					EventCreator = loggedUser.ID,
					InternalRecipient = loggedUser.ID,
					CreatedDate = DateTime.UtcNow,
					ApprovalsIds = new int[] { approvalId }
				},
																 loggedUser,
																 context
																 );
			}
		}

		public static DashboardInstantNotification CreateDashboardInstantNotificationDeadLineItem(int approvalId, int creator, DateTime deadLine, string datePattern, int timeFormat, bool isPhaseDeadline)
		{
			var instantNotificationItem = new DashboardInstantNotification
			{
				ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
				Creator = creator,
				DeadLineDate = GMGColorFormatData.GetFormattedDate(deadLine, datePattern),
				DeadLineTime = GMGColorFormatData.GetFormattedTime(deadLine, timeFormat),
				Version = approvalId.ToString(),
				IsOverdue = DateTime.Compare(deadLine, DateTime.Now) <= 0 ? true : false,
				OverdueTimeDifference = new OverdueTimeDifference(),
				IsPhaseDeadline = isPhaseDeadline
			};

			var overdueDiff = instantNotificationItem.IsOverdue ? DateTime.UtcNow - deadLine : deadLine - DateTime.UtcNow;

			instantNotificationItem.OverdueTimeDifference.Days = overdueDiff.Days;
			instantNotificationItem.OverdueTimeDifference.Hours = overdueDiff.Hours;

			return instantNotificationItem;
		}

		public static DashboardInstantNotification CreateDashboardInstantNotificationChangePDM(int loggedUserId, bool isExternal, string approvalId, string userId, DbContextBL context)
		{
			var instantNotificationItem = new DashboardInstantNotification
			{
				ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
				Creator = loggedUserId,
				Version = approvalId,
			};

			var userID = ConvertStringIdToInt(userId);
			if (isExternal)
			{
				instantNotificationItem.PDM = GetApprovalExternalPdmNameById(context, userID);
				return instantNotificationItem;
			}

			instantNotificationItem.PDM = GetApprovalInternalPdmNameById(context, userID);
			return instantNotificationItem;
		}

		private static string GetApprovalExternalPdmNameById(DbContextBL context, int userId)
		{
			var PDM = (from ec in context.ExternalCollaborators
					   where ec.ID == userId
					   select new
					   {
						   ec.GivenName,
						   ec.FamilyName
					   }).FirstOrDefault();
			return PDM.GivenName + " " + PDM.FamilyName;
		}

		private static string GetApprovalInternalPdmNameById(DbContextBL context, int userId)
		{
			var PDM = (from u in context.Users
					   where u.ID == userId
					   select new
					   {
						   u.GivenName,
						   u.FamilyName
					   }).FirstOrDefault();
			return PDM.GivenName + " " + PDM.FamilyName;
		}

		private static int ConvertStringIdToInt(string id)
		{
			int convertedId;
			Int32.TryParse(id, out convertedId);
			return convertedId;
		}

		public static string GetDecisionDashboardLabel(string decisionKey, string decisionMaker, DbContextBL context, GMGColorDAL.User loggedUser)
		{
			var response = string.Empty;

			ApprovalDecision.Decision decision = ApprovalDecision.GetApprovalDecision(decisionKey);
			var lstCustomDecisions = ApprovalCustomDecision.GetCustomDecisionsByAccountId(loggedUser.Account, loggedUser.Locale, context);

			string approvalDecisionText = lstCustomDecisions.Any(d => d.Key == decisionKey && !string.IsNullOrEmpty(d.Name))
											? lstCustomDecisions.FirstOrDefault(d => d.Key == decisionKey).Name
											: ApprovalDecision.GetLocalizedApprovalDecision(decisionKey);

			bool showDecisionText = (decision != ApprovalDecision.Decision.NotApplicable && decision != ApprovalDecision.Decision.Pending);

			if (!String.IsNullOrEmpty(decisionKey) && showDecisionText)
			{
				if (string.IsNullOrEmpty(decisionMaker))
				{
					response = "<span class=\"label label-" + decision.ToString().ToLower() + " approval-decision\"" +
							" data-html=\"true\" data-original-title=\"\">" + approvalDecisionText + "</span>";
				}
				else
				{
					response = "<span class=\"label label-" + decision.ToString().ToLower() + " approval-decision\" rel=\"tooltip\" data-toggle=\"tooltip\" data-trigger=\"hover\"" +
							" data-placement=\"bottom\" data-html=\"true\" data-original-title=\"\" data-title=\"" + decisionMaker + "&nbsp;-&nbsp;" + approvalDecisionText + " <br>\">" + approvalDecisionText + "</span>";
				}
			}

			return response;
		}

		public static string GetApprovalStatus(int approvalId, int decisionType, bool isPhaseComplete, string decisionKey, string decisionMakers)
		{
			string status = "<span class=\"status-col-" + approvalId + "\"><span class=\"await-pdm-col-" + approvalId + "\">" + (decisionType > 0 ? isPhaseComplete ? "<span class=\"redColor\">" + GMGColor.Resources.Resources.lblCompleted + "</span>" : (decisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker ? GMGColor.Resources.Resources.lblAwaitingPDMDecision : GMGColor.Resources.Resources.lblAwaitingGroupApproval) :
							(string.IsNullOrEmpty(decisionMakers) ? "-" : ((!string.IsNullOrEmpty(decisionKey) && ApprovalDecision.GetApprovalDecision(decisionKey) != ApprovalDecision.Decision.Pending) ? "<span style=\"color:red\">" + GMGColor.Resources.Resources.lblCompleted + "</span>" : GMGColor.Resources.Resources.lblAwaitingPDM))) + "</span> <br />" +
						   "<small class=\"medium-gray-color pdm-col-" + approvalId + "\">" + decisionMakers + "</small></span>";
			return status;
		}

		public static bool UpdatedDeadlineIsFromCurrentPhase(int phaseID, int approvalId, DbContextBL context)
		{
			return (from a in context.Approvals
					where a.CurrentPhase == phaseID && a.ID == approvalId
					select a.ID).Any();
		}

		public static List<ApprovalPhaseDeadline> GetApprovalPhasesDeadline(List<int> ids, DbContextBL context, Account loggedAccount)
		{
			var phasesDeadlines = (from a in context.Approvals
								   join ajp in context.ApprovalJobPhases on a.CurrentPhase equals ajp.ID
								   where ids.Contains(a.ID) && a.CurrentPhase != null && ajp.IsActive
								   select new
								   {
									   ApprovalId = a.ID,
									   PhaseDeadline = ajp.Deadline
								   }).ToList();

			var approvalPhaseDeadlines = new List<ApprovalPhaseDeadline>();
			foreach (var phase in phasesDeadlines)
			{
				var deadline = phase.PhaseDeadline.HasValue ? GMGColorFormatData.GetUserTimeFromUTC(phase.PhaseDeadline.Value, loggedAccount.TimeZone) : (DateTime?)null;
				approvalPhaseDeadlines.Add(new ApprovalPhaseDeadline
				{
					Approval = phase.ApprovalId,
					DeadlineDate = deadline.HasValue ? GMGColorFormatData.GetFormattedDate(deadline.Value, loggedAccount.DateFormat1.Pattern) : "-",
					DeadlineTime = deadline.HasValue ? GMGColorFormatData.GetFormattedTime(deadline.Value, loggedAccount) : string.Empty
				});
			}

			return approvalPhaseDeadlines;
		}

		public static List<ApprovalJobPhase> GetJobAllAvailablePhasesWithAnnotations(int approvalID, int loggedUserId, DbContextBL context)
		{
			List<ApprovalJobPhase> phases = new List<ApprovalJobPhase>();

			int jobID = ApprovalBL.GetJobId(context, approvalID);
			Job objJob = context.Jobs.Where(j => j.ID == jobID).First();

			phases = (from ajp in context.ApprovalJobPhases
					  join ajpa in context.ApprovalJobPhaseApprovals on ajp.ID equals ajpa.Phase
					  join apps in context.Approvals on ajpa.Approval equals apps.ID
					  join jobs in context.Jobs on apps.Job equals jobID

					  let userIsCollaborator = (from acl in context.ApprovalCollaborators
												where acl.Approval == approvalID || acl.Collaborator == loggedUserId && acl.Phase == ajp.ID
												select acl.ID).Any()

					  let hasAnnotation = (from annot in context.ApprovalAnnotations
										   join app in context.ApprovalPages on annot.Page equals app.ID
										   join approvals in context.Approvals on app.Approval equals approvals.ID
										   join job in context.Jobs on approvals.Job equals job.ID
										   where job.ID == jobID && annot.Parent == null && annot.Phase == ajp.ID
										   select annot.ID).Any()

					  where jobs.ID == jobID && hasAnnotation && (userIsCollaborator || loggedUserId == objJob.JobOwner)
					  select ajp).ToList();

			return phases;
		}

		public static void CheckPhaseOverdue(int inLastNoOfDays)
		{
			try
			{
				using (DbContextBL context = new DbContextBL())
				{
					List<PhaseOverdueModel> overduePhases = GetPhasesOverdue(context, inLastNoOfDays);

					if (overduePhases.Count > 0)
					{
						foreach (var overduePhase in overduePhases)
						{
							NotificationServiceBL.CreateNotification(new ApprovalOverdue()
							{
								CreatedDate = DateTime.UtcNow,
								PhaseId = overduePhase.PhaseId,
								ApprovalsIds = new int[] { overduePhase.ApprovalId }
							},
																		null,
																		context);

							//Mark overdue notification as sent for this phase
							var approvalJobPhase = context.ApprovalJobPhases.Find(overduePhase.PhaseId);
							if (approvalJobPhase != null)
								approvalJobPhase.OverdueNotificationSent = true;
						}

						List<PhaseOverdueModel> overduePhaseForExternalCollaborators = GetPhaseExternalCollaborators(overduePhases, context);

						foreach (var overduePhaseForExternalCollaborator in overduePhaseForExternalCollaborators)
						{
							NotificationServiceBL.CreateNotification(new ApprovalOverdue()
							{
								CreatedDate = DateTime.UtcNow,
								PhaseId = overduePhaseForExternalCollaborator.PhaseId,
								ApprovalsIds = new int[] { overduePhaseForExternalCollaborator.ApprovalId },
								ExternalRecipient = overduePhaseForExternalCollaborator.ExternalCollaborator
							},
																	  null,
																	  context);
						}

						context.SaveChanges();
					}
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL("ApprovalBL -> CheckPhaseOverdue() - Error occurred, Message: {0}", ex);
			}
		}

		private static List<PhaseOverdueModel> GetPhaseExternalCollaborators(List<PhaseOverdueModel> overduePhases, DbContextBL context)
		{
			var overduePhaseIds = overduePhases.Select(op => op.PhaseId);
			var overduePhaseApprovalIds = overduePhases.Select(op => op.ApprovalId);

			var externalCollaborators = (from sa in context.SharedApprovals
										 join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
										 join acd in context.ApprovalCollaboratorDecisions on sa.ExternalCollaborator equals acd.ExternalCollaborator
										 join aclr in context.ApprovalCollaboratorRoles on sa.ApprovalCollaboratorRole equals aclr.ID
										 join ajp in context.ApprovalJobPhases on sa.Phase equals ajp.ID
										 where overduePhaseIds.Contains(sa.Phase.Value) && overduePhaseApprovalIds.Contains(sa.Approval) && ec.IsDeleted == false &&
										 acd.Decision == null && aclr.Key == "ANR" && (ajp.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup
										 || ajp.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker && ajp.ExternalPrimaryDecisionMaker == sa.ExternalCollaborator)
										 select new PhaseOverdueModel()
										 {
											 ApprovalId = sa.Approval,
											 PhaseId = sa.Phase.Value,
											 ExternalCollaborator = sa.ExternalCollaborator
										 }).Distinct().ToList();

			return externalCollaborators;
		}

		private static List<PhaseOverdueModel> GetPhasesOverdue(DbContextBL context, int inLastNoOfDays)
		{
			int completedStatusId = JobStatu.GetJobStatus(JobStatu.Status.Completed, context).ID;
			int arhivedStatusId = JobStatu.GetJobStatus(JobStatu.Status.Archived, context).ID;
			int changesCompleteStatusId = JobStatu.GetJobStatus(JobStatu.Status.ChangesComplete, context).ID;

			var overduePhases = (from ajp in context.ApprovalJobPhases
								 join ajw in context.ApprovalJobWorkflows on ajp.ApprovalJobWorkflow equals ajw.ID
								 join j in context.Jobs on ajw.Job equals j.ID

								 let maxVersion = (from a in context.Approvals
												   where a.Job == j.ID
												   orderby a.Version descending
												   select new
												   {
													   a.ID,
													   a.CurrentPhase,
													   a.IsDeleted
												   }).FirstOrDefault()

								 let completedPhases = (from ajpa in context.ApprovalJobPhaseApprovals
														where ajpa.Approval == maxVersion.ID && ajpa.Phase != maxVersion.CurrentPhase.Value
														select ajpa.Phase).ToList()

								 where ajp.Deadline != null &&
								 (DbFunctions.DiffDays(DbFunctions.TruncateTime(ajp.Deadline), DbFunctions.TruncateTime(DateTime.UtcNow)) <= inLastNoOfDays) &&
								 !completedPhases.Contains(ajp.ID) &&
								 maxVersion.IsDeleted == false &&
								 j.Status != completedStatusId &&
								 j.Status != arhivedStatusId &&
								 j.Status != changesCompleteStatusId &&
								 ajp.OverdueNotificationSent == false &&
								 ajp.IsActive
								 select new PhaseOverdueModel()
								 {
									 PhaseId = ajp.ID,
									 ApprovalId = maxVersion.ID
								 }
								).ToList();

			return overduePhases;
		}

        public static List<int> GetAllAvailablePhasesByApprovalId(int approvalID, DbContextBL context)
        {
            int jobID = ApprovalBL.GetJobId(context, approvalID);
            return (from ajp in context.ApprovalJobPhases
                    join ajpa in context.ApprovalJobPhaseApprovals on ajp.ID equals ajpa.Phase
                    join apps in context.Approvals on ajpa.Approval equals apps.ID
                    join jobs in context.Jobs on apps.Job equals jobID

                    let userIsCollaborator = (from acl in context.ApprovalCollaborators
                                              where acl.Approval == approvalID && acl.Phase == ajp.ID
                                              select acl.ID).Any()

                    let hasAnnotation = (from annot in context.ApprovalAnnotations
                                         join app in context.ApprovalPages on annot.Page equals app.ID
                                         join approvals in context.Approvals on app.Approval equals approvals.ID
                                         join job in context.Jobs on approvals.Job equals job.ID
                                         where job.ID == jobID && annot.Parent == null && annot.Phase == ajp.ID
                                         select annot.ID).Any()

                    where jobs.ID == jobID && hasAnnotation && (userIsCollaborator)
                    select ajp.ID).Distinct().ToList();
        }

        public static bool AddNewHtmlApprovalPage(List<Approval> newApprovals, List<GMGColorDAL.ApprovalPage> approvalPageList, DbContextBL context)
        {
            foreach (var objApp in newApprovals)
            {
                var approvalHtmlPage = (from appPage in approvalPageList where appPage.HTMLFilePath.Contains(objApp.Guid) select appPage).ToList();
                int cnt = 1;
                foreach (var objPage in approvalHtmlPage)
                {
                    objPage.Approval = objApp.ID;
                    objPage.Number = cnt++;
                }
            }
            context.ApprovalPages.AddRange(approvalPageList);
            context.SaveChanges();
            System.Threading.Thread.Sleep(2000);//Delay page before loading to dashboard as file need to move to s3 in parallel
            return true;
        }

        public static string GetApprovalType(string ext, DbContextBL context)
        {
            var approvalType = (from ft in context.FileTypes where ft.Extention == ext.Substring(1) select ft.ApprovalTypeColour).FirstOrDefault();
            if(approvalType > 0)
            {
                return approvalType.ToString();
            }
            return "1";
        }

        public static List<PredefinedTagword> GetApprovalsTagwordsOnSearch(string approvalTagwords, string selectedTagwordIds, int loggedAccountId, DbContextBL context)
        {
            List<int> tagwordIds = selectedTagwordIds.Split(',').Select(x => Convert.ToInt32(x)).ToList();
            return (from itm in context.PredefinedTagwords
                    where itm.Account == loggedAccountId && itm.Name.StartsWith(approvalTagwords) && !tagwordIds.Contains(itm.ID)
                    select itm).ToList();
        }
        public static List<PredefinedTagword> GetApprovalsTagwordsOnSearch(string approvalTagwords, int loggedAccountId, DbContextBL context)
        {
            return (from itm in context.PredefinedTagwords
                    where itm.Account == loggedAccountId && itm.Name.StartsWith(approvalTagwords)
                    select itm).ToList();
        }

        public static void InsertApprovalTagwords(List<Approval> newApprovals, NewApprovalModel model, int creatorId, DbContextBL context)
        {
            if (newApprovals.Any() && model.SelectedTagwords != null)
            {
                string[] selectedTagwordslist = model.SelectedTagwords.Split(',').Distinct().ToArray();

                foreach (var approval in newApprovals)
                {
                    var listofTagwords = selectedTagwordslist.Where(x => x.Contains(approval.Guid)).Select(x => x).ToList();

                    foreach (var obj in listofTagwords)
                    {
                        string PredefinedTagwordsId = (obj.ToString().Substring(0, ((obj.ToString().LastIndexOf("_")))));

                        if (PredefinedTagwordsId.Length > 0)
                        {
                            var objApprovalTagword = new ApprovalTagword
                            {
                                Approval1 = approval,
                                Creator = creatorId,
                                CreatedDate = DateTime.UtcNow,
                                PredefinedTagwords = Convert.ToInt32(PredefinedTagwordsId)
                            };
                            context.ApprovalTagwords.Add(objApprovalTagword);
                        }
                    }
                }

            }

        }

        public static int AddNewProjectFolderApproval(BillingPlan collaboratePlan, Account loggedAccount, GMGColorDAL.User loggedUser, NewApprovalModel model, List<ApprovalJob> files, int? webPageSnapshotDelay, ApprovalWorkflowPhasesInfo phasesCollaborators, DbContextBL context, out NewApprovalResponse newApprovalResponse, bool isCollaborateApi = false, List<GMGColorDAL.ApprovalPage> approvalPageList = null)
        {
            //new approvals added in this upload sessions
            var newApprovals = new List<Approval>();
            newApprovalResponse = new NewApprovalResponse();
            DateTime deadlineDate = default(DateTime);
            var duplicatedFiles = string.Empty;
            var ignoreList = string.Empty;
            var newPhases = new Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>>();

            int folderId = 0;
            string submitGroupKey = files.Count() >= 2 ? Guid.NewGuid().ToString() : null;

            if (model.IsDueDateSelected && model.ExecuteWorkflow)
            {
                deadlineDate = GetFormatedApprovalDeadline(model.ApprovalDeadlineDate, model.DeadlineTime,
                    model.DeadlineTimeMeridiem, loggedAccount.DateFormat1.Pattern, loggedAccount.TimeFormat);
            }

            DateTime deadLineDate = model.IsDueDateSelected && model.ExecuteWorkflow
                ? GMGColorFormatData.SetUserTimeToUTC(deadlineDate, loggedAccount.TimeZone)
                : deadlineDate;

            int newJobStatusID = JobStatu.GetJobStatusId(JobStatu.Status.New, context);
            var duplicateFileKey = loggedAccount.UploadEngineAccountSetting != null
                ? loggedAccount.UploadEngineAccountSetting.UploadEngineDuplicateFileName.Key
                : -1;
            var canApplySettings = loggedAccount.UploadEngineAccountSetting != null &&
                                   loggedAccount.UploadEngineAccountSetting.ApplyUploadSettingsForNewApproval;
            var usePrevVersionSettings = canApplySettings && loggedAccount.UploadEngineAccountSetting
                                             .UseUploadSettingsFromPreviousVersion;
            var enableVersionMirroring = false;

            if (canApplySettings && loggedAccount.UploadEngineAccountSetting.EnableVersionMirroring)
            {
                files = files.OrderBy(f => f.FileName).ToList();
            }

            var internalCollaborators = new List<int>(); //internal collaborators set for this upload session
            var sharedApprovals = new List<SharedApproval>(); //external collaborators set for this upload session

            AccountFileTypeFilter fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(loggedUser.ID, context);
            int approverAndReviewerRoleID = GetApprovalRoleID(ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer,
                context);
            var jobSource = isCollaborateApi
                ? JobBL.GetJobSourceByKey(GMG.CoZone.Common.JobSource.WebService, context)
                : JobBL.GetJobSourceByKey(GMG.CoZone.Common.JobSource.WebUI, context);

            Folder objFolder = null;
            if (model.Folder > 0)
            {
                objFolder = context.Folders.FirstOrDefault(f => f.ID == model.Folder);
            }

            var fileTypeIdsAndExtensions = FileType.GetFileTypeIdsAndExtensions(context);

            List<ApprovalNotificationsHelper> approvalNotifications = new List<ApprovalNotificationsHelper>();


            try
            {
                context.Configuration.AutoDetectChangesEnabled = false;

                for (int index = 0; index < files.Count(); index++)
                {
                    ApprovalJob file = files[index];

                    Job objJob = null;

                    string filenameWithoutExtension = GetFileNameWithoutExtension(file.FileName);

                    // first check if the file is valid
                    var fileExtension = FileType.GetFileExtention(file.FileName).ToString().ToLower();
                    var fileType = fileTypeIdsAndExtensions.FirstOrDefault(kvp => kvp.Extension == fileExtension);

                    string extension = fileType.Extension.StartsWith(".")
                        ? fileType.Extension
                        : "." + fileType.Extension;

                    if (!IsValidApprovalFileType(fileTypeFilter, extension))
                    {
                        continue;
                    }

                    if (model.Job > 0)
                    {
                        objJob = DALUtils.GetObject<Job>(model.Job, context);
                        if (model.KeepOriginalFileName)
                        {
                            objJob.Title = filenameWithoutExtension;
                        }
                        else
                        {
                            objJob.Title = file.FileTitle;
                        }

                    }
                    //check whether the folder is selected and 
                    // "Apply upload settings for Collaborate batch New Approval" option from Upload Setting is selected
                    else if (model.Folder > 0 && canApplySettings)
                    {
                        ApprovalFileStatus approvalFileStatus = NewFileStatus(loggedAccount.ID, file.FileName,
                            model.Folder,
                            (FileUploadDuplicateName)duplicateFileKey,
                            out objJob, context);
                        switch (approvalFileStatus)
                        {
                            case ApprovalFileStatus.NewVersion:
                                {
                                    enableVersionMirroring =
                                        loggedAccount.UploadEngineAccountSetting.EnableVersionMirroring;
                                    if (usePrevVersionSettings)
                                    {
                                        //create versions using settings from previous version
                                        Approval newVersion = new Approval();
                                        newVersion.Deadline = deadLineDate;
                                        newVersion.Guid = file.FileGuid;
                                        newVersion.Creator = loggedUser.ID;
                                        newVersion.CreatedDate = DateTime.UtcNow;
                                        newVersion.Modifier = loggedUser.ID;
                                        newVersion.ModifiedDate = DateTime.UtcNow;
                                        newVersion.IsDeleted = false;
                                        newVersion.FileName = Path.GetFileName(file.FileName);
                                        newVersion.FileType = fileType.ID;
                                        newVersion.Size = file.FileSize;
                                        newVersion.FolderPath = String.Empty;
                                        newVersion.VersionSufix = String.Empty;


                                        newVersion.Folders.Add(objFolder);
                                        folderId = objFolder.ID;

                                        newVersion.Type = GetFileType(model.IsUploadApproval, fileType.MediaType, context);

                                        context.Approvals.Add(newVersion);

                                        bool? keepJobName = null;
                                        if (loggedAccount.ID == objJob.Account)
                                        {
                                            keepJobName = GetLastVersionSettingByJobId(objJob.ID, model.Folder, context);
                                        }

                                        if (!keepJobName.GetValueOrDefault())
                                        {
                                            objJob.Title = filenameWithoutExtension;
                                        }

                                        var newVersionSetting = new ApprovalSetting()
                                        {
                                            Approval1 = newVersion,
                                            UpdateJobName = keepJobName ?? !model.KeepOriginalFileName
                                        };

                                        objJob.Status = newJobStatusID;
                                        context.ApprovalSettings.Add(newVersionSetting);

                                        SetSettingsFromPreviousVersion(newVersion, objJob, loggedUser, enableVersionMirroring,
                                            approverAndReviewerRoleID, jobSource,
                                            approvalNotifications, context);

                                        continue;
                                    }
                                }
                                break;
                            case ApprovalFileStatus.DuplicatedFile:
                                {
                                    duplicatedFiles += duplicatedFiles.Length > 0
                                        ? ", " + file.FileName
                                        : file.FileName;
                                    continue;
                                }
                            case ApprovalFileStatus.IgnoredFile:
                                {
                                    ignoreList += ignoreList.Length > 0
                                        ? ", " + file.FileName
                                        : file.FileName;
                                    continue;
                                }
                        }
                    }

                    bool? keepOriginalJobName = null;
                    if (objJob == null)
                    {
                        objJob = new Job();
                        objJob.Title = model.IsUploadApproval && model.KeepOriginalFileName ? filenameWithoutExtension : file.FileTitle;
                        objJob.Account = loggedAccount.ID;
                        objJob.ModifiedDate = DateTime.UtcNow;
                        objJob.Guid = Guid.NewGuid().ToString();
                    }
                    else
                    {
                        if (model.Folder > 0 && canApplySettings)
                        {
                            keepOriginalJobName = GetLastVersionSettingByJobId(objJob.ID, model.Folder, context);

                            if (!keepOriginalJobName.GetValueOrDefault())
                            {
                                objJob.Title = filenameWithoutExtension;
                            }
                        }
                    }
                    if (model.ProjectFolder > 0)
                        objJob.ProjectFolder = model.ProjectFolder;

                    Approval objApproval = new Approval();
                    objApproval.SubmitGroupKey = submitGroupKey;

                    objApproval.ScreenshotUrl = model.Url;
                    objApproval.WebPageSnapshotDelay = webPageSnapshotDelay;

                    objApproval.Deadline = deadLineDate;
                    objApproval.Guid = file.FileGuid;
                    objApproval.Creator = loggedUser.ID;
                    objApproval.CreatedDate = DateTime.UtcNow;
                    objApproval.Modifier = loggedUser.ID;
                    objApproval.ModifiedDate = DateTime.UtcNow;
                    objApproval.IsDeleted = false;
                    int[] versions = objJob.Approvals.Select(t => t.Version).ToArray();
                    objApproval.Version = enableVersionMirroring
                        ? GetFileVersion(file.FileName, versions)
                        : versions.Any()
                            ? (versions.Max() + 1)
                            : 1;
                    if (isCollaborateApi)
                    {
                        newApprovalResponse.JobGuid = objJob.Guid;
                        newApprovalResponse.Version = objApproval.Version;
                        objApproval.ProcessingInProgress = "completed";
                    }

                    if (!model.ExecuteWorkflow)
                    {
                        objApproval.AllowDownloadOriginal = false;
                        objApproval.OnlyOneDecisionRequired = false;
                        objApproval.PrivateAnnotations = false;
                        objApproval.LockProofWhenAllDecisionsMade = false;
                        objApproval.LockProofWhenFirstDecisionsMade = false;
                        objApproval.RestrictedZoom = false;
                    }
                    else
                    {
                        objApproval.AllowDownloadOriginal = model.AllowUsersToDownload;
                        objApproval.OnlyOneDecisionRequired = model.OnlyOneDecisionRequired;
                        objApproval.PrivateAnnotations = model.OnlyOneDecisionRequired ? model.PrivateAnnotations : false;
                        objApproval.LockProofWhenAllDecisionsMade = model.LockProofWhenAllDecisionsMade;
                        objApproval.LockProofWhenFirstDecisionsMade = model.LockProofWhenFirstDecisionsMade;
                        objApproval.RestrictedZoom = model.RestrictedZoom;
                    }
                    objApproval.IsPageSpreads = model.IsPageSpreads;
                    objApproval.JobSource = jobSource;
                    objApproval.AllowOtherUsersToBeAdded = model.AllowOtherUsersToBeAdded;
                    objApproval.VersionSufix = string.Empty;

                    if (model.ChecklistId > 0 && model.ExecuteWorkflow)
                    {
                        objApproval.Checklist = model.ChecklistId;
                    }
                    else
                    {
                        objApproval.Checklist = null;
                    }
                    if (file.ApprovalType > 0)
                    {
                        objApproval.ApprovalTypeColour = file.ApprovalType;
                    }
                    else
                    {
                        objApproval.ApprovalTypeColour = 1;
                    }

                    objApproval.Owner = model.Owner;
                    if (objApproval.OnlyOneDecisionRequired && model.ExecuteWorkflow)
                    {
                        int pdm = 0;
                        if (Int32.TryParse(model.PrimaryDecisionMaker, out pdm))
                        {
                            objApproval.PrimaryDecisionMaker = pdm;
                        }
                    }

                    Folder folder = null;
                    if (model.Job > 0)
                    {
                        if (objJob.Approvals.FirstOrDefault().Folders.Count == 1)
                        {
                            var id = objJob.Approvals.FirstOrDefault().Folders.FirstOrDefault().ID;
                            folder = context.Folders.FirstOrDefault(f => f.ID == id);
                        }
                    }
                    else
                    {
                        folder = objFolder;
                    }

                    if (folder != null)
                    {
                        objApproval.Folders.Add(folder);
                        folderId = folder.ID;
                    }

                    objApproval.FileName = Path.GetFileName(file.FileName);
                    objApproval.FileType = fileType.ID;

                    PlansBL.LogApprovalSizeUsage(collaboratePlan, loggedAccount, file.FileSize, context);

                    objApproval.Size = file.FileSize;

                    objApproval.Type = GetFileType(model.IsUploadApproval, fileType.MediaType, context);

                    //lock previous versions
                    if (objApproval.Version > 1 && objJob != null)
                    {
                        foreach (Approval version in objJob.Approvals)
                        {
                            version.IsLocked = true;
                        }
                    }

                    objApproval.FolderPath = string.Empty;

                    //save owner if approval has adhoc workflow
                    if (model.HasAdHocWorkflow)
                    {
                        objApproval.Owner = loggedUser.ID;
                    }

                    objJob.Approvals.Add(objApproval);
                    objJob.Status = newJobStatusID;

                    if (objJob.ID == 0)
                    {
                        context.Jobs.Add(objJob);
                    }
                    //objApproval.ProcessingInProgress = "completed";
                    context.Approvals.Add(objApproval);

                    if (model.ApprovalWorkflowId > 0 && model.ExecuteWorkflow)
                    {
                        var deadlineTriggers = (from fdt in context.PhaseDeadlineTriggers select fdt).ToList();
                        var deadlineUnits = (from fdu in context.PhaseDeadlineUnits select fdu).ToList();

                        if (objJob.ID == 0)
                        {
                            var selectedWorkflow = context.ApprovalWorkflows.Where(w => w.ID == model.ApprovalWorkflowId)
                                .Select(n => new { n.Name, n.RerunWorkflow }).FirstOrDefault();
                            objJob.JobOwner = loggedUser.ID;
                            var approvalJobWorkflow = new ApprovalJobWorkflow()
                            {
                                Name = selectedWorkflow.Name,
                                Account = loggedAccount.ID,
                                Job1 = objJob,
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow,
                                CreatedBy = loggedUser.ID,
                                ModifiedBy = loggedUser.ID,
                                RerunWorkflow = selectedWorkflow.RerunWorkflow
                            };

                            context.ApprovalJobWorkflows.Add(approvalJobWorkflow);
                            var phases = new List<ApprovalJobPhase>();

                            bool isfirstPhase = true;
                            foreach (var phase in phasesCollaborators.ApprovalWorkflowPhases)
                            {
                                //add default phase template details except collaborators
                                var phaseTemplate =
                                    (from ap in context.ApprovalPhases where ap.ID == phase.Phase select ap)
                                    .FirstOrDefault();

                                if (phaseTemplate != null)
                                {
                                    var approvalJobPhase = new ApprovalJobPhase()
                                    {
                                        Name = phaseTemplate.Name,
                                        ApprovalJobWorkflow1 = approvalJobWorkflow,
                                        Position = phaseTemplate.Position,
                                        DecisionType = phaseTemplate.DecisionType,
                                        ApprovalTrigger = phaseTemplate.ApprovalTrigger,
                                        ShowAnnotationsToUsersOfOtherPhases = phaseTemplate.ShowAnnotationsToUsersOfOtherPhases,
                                        ShowAnnotationsOfOtherPhasesToExternalUsers = phaseTemplate.ShowAnnotationsOfOtherPhasesToExternalUsers,
                                        PhaseTemplateID = phaseTemplate.ID,
                                        Deadline = phaseTemplate.Deadline,
                                        DeadlineTrigger = phaseTemplate.DeadlineTrigger,
                                        DeadlineUnitNumber = phaseTemplate.DeadlineUnitNumber,
                                        DeadlineUnit = phaseTemplate.DeadlineUnit,
                                        IsActive = true,
                                        CreatedDate = DateTime.UtcNow,
                                        ModifiedDate = DateTime.UtcNow,
                                        ModifiedBy = loggedUser.ID,
                                        CreatedBy = loggedUser.ID
                                    };

                                    if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                    {
                                        if (phase.IsExternal)
                                        {
                                            approvalJobPhase.PrimaryDecisionMaker = null;
                                            approvalJobPhase.ExternalPrimaryDecisionMaker = phase.PDMId ??
                                                                                            phaseTemplate
                                                                                                .ExternalPrimaryDecisionMaker;
                                        }
                                        else
                                        {
                                            approvalJobPhase.ExternalPrimaryDecisionMaker = null;
                                            approvalJobPhase.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                                        }
                                    }

                                    //set automatic phase deadline  
                                    if (phaseTemplate.DeadlineTrigger != null && phaseTemplate.DeadlineUnitNumber != null &&
                                        phaseTemplate.DeadlineUnit != null)
                                    {
                                        var deadlineTrigger = deadlineTriggers.Where(t => phaseTemplate.DeadlineTrigger == t.ID).Select(t => t.Key)
                                            .FirstOrDefault();
                                        var deadlineUnit = deadlineUnits.Where(u => phaseTemplate.DeadlineUnit == u.ID).Select(u => u.Key)
                                            .FirstOrDefault();

                                        if (isfirstPhase || deadlineTrigger != (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
                                        {
                                            approvalJobPhase.Deadline = GetAutomaticPhaseDeadline(deadLineDate, deadlineTrigger,
                                                phaseTemplate.DeadlineUnitNumber, deadlineUnit);
                                        }
                                    }

                                    context.ApprovalJobPhases.Add(approvalJobPhase);

                                    if (isfirstPhase)
                                    {
                                        if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                        {
                                            if (phase.IsExternal)
                                            {
                                                objApproval.ExternalPrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.ExternalPrimaryDecisionMaker;
                                                objApproval.PrimaryDecisionMaker = null;
                                            }
                                            else
                                            {
                                                objApproval.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                                                objApproval.ExternalPrimaryDecisionMaker = null;
                                            }
                                        }
                                        objApproval.ApprovalJobPhase = approvalJobPhase;

                                        //add relations between each workflow phase and current approval
                                        var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                                        {
                                            Approval1 = objApproval,
                                            ApprovalJobPhase = approvalJobPhase,
                                            PhaseMarkedAsCompleted = false,
                                            CreatedBy = loggedUser.ID,
                                            ModifiedBy = loggedUser.ID,
                                            CreatedDate = DateTime.UtcNow,
                                            ModifiedDate = DateTime.UtcNow
                                        };
                                        objApproval.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);
                                        isfirstPhase = false;
                                    }

                                    phases.Add(approvalJobPhase);
                                }
                            }

                            newPhases.Add(approvalJobWorkflow, phases);
                        }
                        else
                        {
                            var phase = phasesCollaborators.ApprovalWorkflowPhases[0];
                            var phases = (from ajp in context.ApprovalJobPhases
                                          join ajw in context.ApprovalJobWorkflows on ajp.ApprovalJobWorkflow equals ajw.ID
                                          let currentPhasePosition = (from ajpp in context.ApprovalJobPhases
                                                                      where ajpp.ID == phase.Phase
                                                                      select ajpp.Position).FirstOrDefault()

                                          where ajw.Job == objJob.ID && ajp.Position >= currentPhasePosition
                                          select ajp).ToList();

                            var existingWorkflow = context.ApprovalJobWorkflows.FirstOrDefault(w => w.Job == objJob.ID);
                            newPhases.Add(existingWorkflow, phases);
                            foreach (var approvalJobPhase in phases)
                            {
                                //if phase decision type is PDM, then each phase should be updated with the new changes if any
                                if (approvalJobPhase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                                {
                                    var phaseInfo =
                                        phasesCollaborators.ApprovalWorkflowPhases.FirstOrDefault(p => p.Phase == approvalJobPhase.ID);
                                    if (phaseInfo.IsExternal)
                                    {
                                        approvalJobPhase.ExternalPrimaryDecisionMaker = phaseInfo.PDMId;
                                        approvalJobPhase.PrimaryDecisionMaker = null;
                                    }
                                    else
                                    {
                                        approvalJobPhase.PrimaryDecisionMaker = phaseInfo.PDMId;
                                        approvalJobPhase.ExternalPrimaryDecisionMaker = null;
                                    }
                                }

                                //update deadline, if needed, only for current phase and phases with status "Pendding"
                                var deadlineTrigger = deadlineTriggers.Where(t => approvalJobPhase.DeadlineTrigger == t.ID).Select(t => t.Key)
                                    .FirstOrDefault();
                                var deadlineUnit = deadlineUnits.Where(u => approvalJobPhase.DeadlineUnit == u.ID).Select(u => u.Key)
                                    .FirstOrDefault();

                                if (approvalJobPhase.Deadline == null && approvalJobPhase.DeadlineUnitNumber != null && deadlineTrigger !=
                                    (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
                                {
                                    approvalJobPhase.Deadline = GetAutomaticPhaseDeadline(deadLineDate, deadlineTrigger,
                                        approvalJobPhase.DeadlineUnitNumber, deadlineUnit);
                                }
                            }

                            var currentPhase = phases.FirstOrDefault(p => p.ID == phase.Phase);

                            //if phase decision type is PDM, then the approval should be updated with the new changes if any
                            if (currentPhase.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                            {
                                if (phase.IsExternal)
                                {
                                    objApproval.ExternalPrimaryDecisionMaker = phase.PDMId ?? currentPhase.ExternalPrimaryDecisionMaker;
                                    objApproval.PrimaryDecisionMaker = null;
                                }
                                else
                                {
                                    objApproval.PrimaryDecisionMaker = phase.PDMId ?? currentPhase.PrimaryDecisionMaker;
                                    objApproval.ExternalPrimaryDecisionMaker = null;
                                }
                            }
                            objApproval.CurrentPhase = phase.Phase;

                            //add relations between each workflow phase and current approval
                            var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                            {
                                Approval1 = objApproval,
                                Phase = phasesCollaborators.ApprovalWorkflowPhases[0].Phase,
                                PhaseMarkedAsCompleted = false,
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow,
                                CreatedBy = loggedUser.ID,
                                ModifiedBy = loggedUser.ID
                            };
                            objApproval.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);
                        }
                    }

                    var approvalSetting = new ApprovalSetting()
                    {
                        Approval1 = objApproval,
                        UpdateJobName = keepOriginalJobName ?? !model.KeepOriginalFileName
                    };

                    //check if custom profile rendering option was selected
                    if (model.RenderOption.HasValue &&
                        model.RenderOption.Value == NewApprovalModel.RenderOptions.CustomProfile && model.ExecuteWorkflow)
                    {
                        ApprovalCustomICCProfile appCsmProf = new ApprovalCustomICCProfile();
                        appCsmProf.Approval1 = objApproval;
                        appCsmProf.CustomICCProfile = model.SelectedCustomProfile;
                        context.ApprovalCustomICCProfiles.Add(appCsmProf);
                    }

                    context.ApprovalSettings.Add(approvalSetting);

                    newApprovals.Add(objApproval);
                }

                if (model.ExecuteWorkflow)
                    internalCollaborators = AddProjectFolderApprovalUsers(newApprovals, phasesCollaborators, model, newPhases, loggedAccount, loggedUser, context, ref sharedApprovals);

                InsertApprovalTagwords(newApprovals, model, loggedUser.ID, context);

                using (var ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, GMGColorConfiguration.AppConfiguration.NewApprovalTimeOutInMinutes, 0)))
                {
                    context.Configuration.AutoDetectChangesEnabled = true;
                    context.SaveChanges();
                    ts.Complete();
                }
                if (isCollaborateApi)
                {
                    newApprovalResponse.ApprovalID = newApprovals.Select(a => a.ID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                int loggedUserId = loggedUser?.ID ?? -1;
                int loggedAccountId = loggedAccount?.ID ?? -1;
                var fileNames = files.Select(f => f.FileName).ToArray();

                object message = $"AddNewApproval Failed LoggedAccountId: {loggedAccountId} | LoggedUserId: {loggedUserId} | InternalCollaborators: {internalCollaborators.ToArray()} | ExternalCollaborators {sharedApprovals.ToArray()} | Job: {model.Job} | FileNames: {fileNames}";
                GMGColorLogging.log.Error(loggedUserId, message, ex);
                throw;
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
            }


            if (approvalPageList != null && approvalPageList.Count > 0)
            {
                AddNewHtmlApprovalPage(newApprovals, approvalPageList, context);
            }
            SendNewApprovalNotifications(newApprovals, internalCollaborators, sharedApprovals, loggedAccount, loggedUser, model.OptionalMessage, model.Job, context);
            SendNewVersionNotifications(approvalNotifications, loggedAccount, loggedUser, context);

            if (!string.IsNullOrEmpty(duplicatedFiles))
            {
                model.ErrorMessage = String.Format(GMGColor.Resources.Resources.errPreviousFileExist, duplicatedFiles);
            }
            else if (!string.IsNullOrEmpty(ignoreList))
            {
                model.ErrorMessage = String.Format(GMGColor.Resources.Resources.errIgnoreDuplicatedFiles, ignoreList);
            }

            return folderId;
        }

        public static void UpdateExecteWorkflowPermission(PermissionsModel model, int approvalId , GMGColorDAL.User loggedUser, DbContextBL context)
        {
         try
         {
            #region  update phase template
            var objApproval = (from a in context.Approvals where a.ID == approvalId select a).FirstOrDefault();
            var objJob = (from j in context.Jobs where j.ID == objApproval.Job select j).FirstOrDefault();
            var newApprovals = new List<Approval>();
            newApprovals.Add(objApproval);

            if (model.Checklist != null ? model.Checklist  > 0 : false)
            {
                objApproval.Checklist = model.Checklist.Value;
            }
            
                ApprovalWorkflowPhasesInfo phasesCollaborators = new ApprovalWorkflowPhasesInfo();
            phasesCollaborators = Newtonsoft.Json.JsonConvert.DeserializeObject<ApprovalWorkflowPhasesInfo>(model.PhasesCollaborators);

            var selectedWorkflow = context.ApprovalWorkflows.Where(w => w.ID == model.ApprovalWorkflow)
                                .Select(n => new { n.Name, n.RerunWorkflow }).FirstOrDefault();
            objJob.JobOwner = loggedUser.ID;
            var approvalJobWorkflow = new ApprovalJobWorkflow()
            {
                Name = selectedWorkflow.Name,
                Account = loggedUser.Account,
                Job1 = objJob,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow,
                CreatedBy = loggedUser.ID,
                ModifiedBy = loggedUser.ID,
                RerunWorkflow = selectedWorkflow.RerunWorkflow
            };

            context.ApprovalJobWorkflows.Add(approvalJobWorkflow);
            var phases = new List<ApprovalJobPhase>();

            bool isfirstPhase = true;
            foreach (var phase in phasesCollaborators.ApprovalWorkflowPhases)
            {
                //add default phase template details except collaborators
                var phaseTemplate =
                    (from ap in context.ApprovalPhases where ap.ID == phase.Phase select ap)
                    .FirstOrDefault();

                if (phaseTemplate != null)
                {
                    var approvalJobPhase = new ApprovalJobPhase()
                    {
                        Name = phaseTemplate.Name,
                        ApprovalJobWorkflow1 = approvalJobWorkflow,
                        Position = phaseTemplate.Position,
                        DecisionType = phaseTemplate.DecisionType,
                        ApprovalTrigger = phaseTemplate.ApprovalTrigger,
                        ShowAnnotationsToUsersOfOtherPhases = phaseTemplate.ShowAnnotationsToUsersOfOtherPhases,
                        ShowAnnotationsOfOtherPhasesToExternalUsers = phaseTemplate.ShowAnnotationsOfOtherPhasesToExternalUsers,
                        PhaseTemplateID = phaseTemplate.ID,
                        Deadline = phaseTemplate.Deadline,
                        DeadlineTrigger = phaseTemplate.DeadlineTrigger,
                        DeadlineUnitNumber = phaseTemplate.DeadlineUnitNumber,
                        DeadlineUnit = phaseTemplate.DeadlineUnit,
                        IsActive = true,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow,
                        ModifiedBy = loggedUser.ID,
                        CreatedBy = loggedUser.ID
                    };

                    if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                    {
                        if (phase.IsExternal)
                        {
                            approvalJobPhase.PrimaryDecisionMaker = null;
                            approvalJobPhase.ExternalPrimaryDecisionMaker = phase.PDMId ??
                                                                            phaseTemplate
                                                                                .ExternalPrimaryDecisionMaker;
                        }
                        else
                        {
                            approvalJobPhase.ExternalPrimaryDecisionMaker = null;
                            approvalJobPhase.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                        }
                    }
                    context.ApprovalJobPhases.Add(approvalJobPhase);

                    if (isfirstPhase)
                    {
                        if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                        {
                            if (phase.IsExternal)
                            {
                                objApproval.ExternalPrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.ExternalPrimaryDecisionMaker;
                                objApproval.PrimaryDecisionMaker = null;
                            }
                            else
                            {
                                objApproval.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                                objApproval.ExternalPrimaryDecisionMaker = null;
                            }
                        }
                        objApproval.ApprovalJobPhase = approvalJobPhase;

                        //add relations between each workflow phase and current approval
                        var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                        {
                            Approval1 = objApproval,
                            ApprovalJobPhase = approvalJobPhase,
                            PhaseMarkedAsCompleted = false,
                            CreatedBy = loggedUser.ID,
                            ModifiedBy = loggedUser.ID,
                            CreatedDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        objApproval.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);
                        isfirstPhase = false;
                    }

                    phases.Add(approvalJobPhase);
                }
            }
            var newPhases = new Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>>();
            var sharedApprovals = new List<SharedApproval>();
            newPhases.Add(approvalJobWorkflow, phases);

            #endregion

            var internalCollaborators = UpdateProjectFolderApprovalUsers(newApprovals, phasesCollaborators, model, newPhases, loggedUser.Account1, loggedUser, context);

                //Removed TransactionScope
                //using (var ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, GMGColorConfiguration.AppConfiguration.NewApprovalTimeOutInMinutes, 0)))
                //{
                //    context.Configuration.AutoDetectChangesEnabled = true;
                //    context.SaveChanges();
                //    ts.Complete();
                //}
                context.SaveChanges();
                
        }
        catch (Exception ex)
            {
                int loggedUserId = loggedUser?.ID ?? -1;

        object message = $"Add Permission Failed LoggedAccountId: {loggedUser.Account} | LoggedUserId: {loggedUserId}";
        GMGColorLogging.log.Error(loggedUserId, message, ex);
                throw;
            }
            finally
            {
                context.Configuration.AutoDetectChangesEnabled = true;
            }
            
        }

        public static void UpdateChecklist(PermissionsModel model,int approvalId , DbContextBL context)
        {
            var objApproval = (from a in context.Approvals where a.ID == approvalId select a).FirstOrDefault();

            if (model.Checklist != null ? model.Checklist > 0 : false)
            {
                objApproval.Checklist = model.Checklist.Value;
            }
            context.SaveChanges();
        }

        private static List<int> AddProjectFolderApprovalUsers(List<Approval> newApprovals, ApprovalWorkflowPhasesInfo phasesCollaborators, NewApprovalModel model,
    Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>> newPhases, Account loggedAccount, GMGColorDAL.User loggedUser, DbContextBL context,
    ref List<SharedApproval> sharedApprovals)
        {
            var internalCollaborators = new List<int>();
            if (newApprovals.Any())
            {
                var workflowPhases = GetApprovalPhasesCollaboratorsPerf(phasesCollaborators, newPhases);
                var collaboratorRoles = ApprovalCollaboratorRole.GetRoles(context);

                //set internal users permissions
                var permissionsModel = new PermissionsModel
                {
                    SelectedUsersAndGroups = model.SelectedUsersAndGroups
                };

                var currPhase = model.ApprovalWorkflowId > 0 ? phasesCollaborators.ApprovalWorkflowPhases[0].Phase : (int?)null;

                PermissionsUsersAndRolesModelPerf permissionsUsersAndRolesFromModel = null;
                PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles = null;
                var index = 0;
                var idx = 0;

                try
                {
                    string externalEmails = PermissionsBL.GetFormatedExternalUsersDetails(model.ExternalEmails);

                    foreach (var approval in newApprovals)
                    {
                        ApprovalWorkflowPhasesInfoPerf approvalWorkflowPhases = (workflowPhases != null) ? workflowPhases[idx] : null;
                        if (workflowPhases != null)
                        {
                            var approvalWorkflow = workflowPhases[index];
                            index++;

                            permissionsUsersAndRoles = PermissionsBL.GetPermissionsPerf(approvalWorkflow);
                        }
                        else
                        {
                            if (permissionsUsersAndRolesFromModel == null)
                            {
                                permissionsUsersAndRolesFromModel = PermissionsBL.GetPermissionsPerf(permissionsModel);
                            }
                            permissionsUsersAndRoles = permissionsUsersAndRolesFromModel;
                        }

                        var objProjectFolder = (from d in context.ProjectFolders
                                                where d.ID == model.ProjectFolder
                                                select d).Single();
                        objProjectFolder.Status = 2;

                        PermissionsBL.SetApprovalPermission(approval, permissionsUsersAndRoles, context);

                        string externalPdmEmail = string.Empty;
                        if (model.OnlyOneDecisionRequired && !string.IsNullOrEmpty(model.PrimaryDecisionMaker) && model.PrimaryDecisionMaker.Contains("@"))
                        {
                            externalPdmEmail = model.PrimaryDecisionMaker;
                        }

                        sharedApprovals = PermissionsBL.SetExternalCollaboratorsPermissions(model.ExternalUsers, externalEmails, approvalWorkflowPhases, loggedAccount.ID, collaboratorRoles, approval, model.AllowUsersToDownload, externalPdmEmail, context);
                        idx++;
                    }

                    internalCollaborators = permissionsUsersAndRoles?.Users.Where(p => (p.Phase?.ID == 0 ? p.Phase?.PhaseTemplateID : p.Phase?.ID) == currPhase).Select(u => u.ID).ToList() ?? new List<int>();
                }
                catch (ExceptionBL)
                {
                    throw;
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
                }
                catch (Exception ex)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
                }
            }

            return internalCollaborators;
        }

        private static List<int> UpdateProjectFolderApprovalUsers(List<Approval> newApprovals, ApprovalWorkflowPhasesInfo phasesCollaborators, PermissionsModel model,
    Dictionary<ApprovalJobWorkflow, List<ApprovalJobPhase>> newPhases, Account loggedAccount, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            var internalCollaborators = new List<int>();
            if (newApprovals.Any())
            {
                var workflowPhases = GetApprovalPhasesCollaboratorsPerf(phasesCollaborators, newPhases);
                var collaboratorRoles = ApprovalCollaboratorRole.GetRoles(context);

                //set internal users permissions
                var permissionsModel = new PermissionsModel
                {
                    SelectedUsersAndGroups = model.SelectedUsersAndGroups
                };

                var currPhase = model.ApprovalWorkflow > 0 ? phasesCollaborators.ApprovalWorkflowPhases[0].Phase : (int?)null;

                PermissionsUsersAndRolesModelPerf permissionsUsersAndRolesFromModel = null;
                PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles = null;
                var index = 0;
                var idx = 0;

                try
                {
                    foreach (var approval in newApprovals)
                    {
                        ApprovalWorkflowPhasesInfoPerf approvalWorkflowPhases = (workflowPhases != null) ? workflowPhases[idx] : null;
                        if (workflowPhases != null)
                        {
                            var approvalWorkflow = workflowPhases[index];
                            index++;

                            permissionsUsersAndRoles = PermissionsBL.GetPermissionsPerf(approvalWorkflow);
                        }
                        else
                        {
                            if (permissionsUsersAndRolesFromModel == null)
                            {
                                permissionsUsersAndRolesFromModel = PermissionsBL.GetPermissionsPerf(permissionsModel);
                            }
                            permissionsUsersAndRoles = permissionsUsersAndRolesFromModel;
                        }

                        var ProjectFolderId = (from j in context.Jobs
                                                where j.ID == approval.Job
                                                select j.ProjectFolder).Single();
                        
                        var objProjectFolder = (from d in context.ProjectFolders
                                                where d.ID == ProjectFolderId
                                                select d).Single();
                        objProjectFolder.Status = 2;

                        PermissionsBL.SetProjectFolderApprovalPermission(approval, permissionsUsersAndRoles, context);

                        string externalPdmEmail = string.Empty;
                        
                        idx++;
                    }

                    internalCollaborators = permissionsUsersAndRoles?.Users.Where(p => (p.Phase?.ID == 0 ? p.Phase?.PhaseTemplateID : p.Phase?.ID) == currPhase).Select(u => u.ID).ToList() ?? new List<int>();
                }
                catch (ExceptionBL)
                {
                    throw;
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
                }
                catch (Exception ex)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
                }
            }
            return internalCollaborators;
        }

        public static bool CheckUserCanAccess(List<int> Ids, GMGColorDAL.Approval.CollaborateValidation operation, int loggedUserId, DbContextBL context)
        {
            bool result = false;
            switch (operation)
            {
                case GMGColorDAL.Approval.CollaborateValidation.ApprovalAccess:
                case GMGColorDAL.Approval.CollaborateValidation.DeleteApproval:
                    {
                        result = (from a in context.Approvals
                                join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                where Ids.Contains(a.ID) && (a.Creator == loggedUserId || ac.ApprovalCollaboratorRole == 3)
                                select ac.ID).Any();
                        break;
                    }
                case GMGColorDAL.Approval.CollaborateValidation.MoveApprovals:
                case GMGColorDAL.Approval.CollaborateValidation.RestoreArchivedApproval:
                case GMGColorDAL.Approval.CollaborateValidation.ApprovalAnnotationReport:
                    {
                        result = (from a in context.Approvals
                                  join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                                  where Ids.Contains(a.ID) && (a.Creator == loggedUserId || ac.Collaborator == loggedUserId)
                                  select ac.ID).Any();
                        break;
                    }
                case GMGColorDAL.Approval.CollaborateValidation.DownloadFolder:
                case GMGColorDAL.Approval.CollaborateValidation.EditFolder:
                case GMGColorDAL.Approval.CollaborateValidation.DeleteFolder:
                    {
                        result = (from f in context.Folders
                                  where Ids.Contains(f.ID) && f.Creator == loggedUserId
                                  select f.ID).Any();
                        break;
                    }
                case GMGColorDAL.Approval.CollaborateValidation.FolderAccess:
                case GMGColorDAL.Approval.CollaborateValidation.MoveFolder:
                case GMGColorDAL.Approval.CollaborateValidation.FolderAnnotationReport:
                    {
                        result = (from f in context.Folders
                                  join fc in context.FolderCollaborators on f.ID equals fc.Folder
                                  where Ids.Contains(f.ID) && fc.Collaborator == loggedUserId
                                  select fc.ID).Any();
                            break;
                    }
                case GMGColorDAL.Approval.CollaborateValidation.DeleteProjectFolder:
                case GMGColorDAL.Approval.CollaborateValidation.EditProjectFolder:
                case Approval.CollaborateValidation.EditMemberProjectFolder:
                    {
                        result = (from pf in context.ProjectFolders
                                  where Ids.Contains(pf.ID) 
                                  select pf.ID).Any();
                            break;
                    }
                case GMGColorDAL.Approval.CollaborateValidation.ArchiveProjectFolder:
                case GMGColorDAL.Approval.CollaborateValidation.UnArchiveProjectFolder:
                case GMGColorDAL.Approval.CollaborateValidation.ViewProjectFolder:
                    {
                        result = (from pf in context.ProjectFolders
                                  join pfc in context.ProjectFolderCollaborators on pf.ID equals pfc.ProjectFolder
                                  where Ids.Contains(pf.ID) 
                                  select pfc.ID).Any();
                        break;
                    }
                case GMGColorDAL.Approval.CollaborateValidation.DeleteSimulationProfile:
                    {
                        result = (from sp in context.SimulationProfiles
                                  where Ids.Contains(sp.ID) && sp.AccountId == loggedUserId
                                  select sp.ID).Any();
                        break;
                    }
            }
            return result;
        }

        public static string ArchiveDeleteTimeLimit(int accountId, DbContextBL context)
        {
            try
            {
                string archiveTimelimit = "450 days";
                var accountSettings = (from acc in context.AccountSettings
                                                                where acc.Account == accountId && acc.Name.Contains("ArchiveTimeLimit")
                                                                select acc).ToList();
                if (accountSettings.Count > 0)
                {
                    if (Convert.ToInt32(accountSettings[2].Value) == 1)
                    {
                        archiveTimelimit = accountSettings[1].Value + " " + "days";
                    }
                    else if (Convert.ToInt32(accountSettings[2].Value) == 2)
                    {
                        archiveTimelimit = accountSettings[1].Value + " " + "months";
                    }
                    else if (Convert.ToInt32(accountSettings[2].Value) == 3)
                    {
                        archiveTimelimit = accountSettings[1].Value + " " + "years";
                    }
                }
                return archiveTimelimit;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("Error: ApprovalBL.IsSubstrateFromICCProfileInPrrofStudioEnabled", ex);
            }
        }
    }
}

