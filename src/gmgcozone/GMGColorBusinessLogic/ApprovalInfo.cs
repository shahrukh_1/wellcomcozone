﻿namespace GMGColorBusinessLogic
{
    public class ApprovalInfo
    {
        public int ID { get; set; }
        public int? JobOwner { get; set; }
        public int? CurrentPhase { get; set; }
        public string Domain { get; set; }
        public int Account { get; set; }
        public int JobId { get; set; }
    }
}
