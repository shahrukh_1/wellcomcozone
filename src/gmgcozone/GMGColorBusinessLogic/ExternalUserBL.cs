﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class ExternalUserBL
    {
        /// <summary>
        /// Checks if the external user with the specified email address exists on user account
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool ValidateExternalUserEmail(string email, int userID, DbContextBL context)
        {
            try
            {
                return ExternalCollaborator.ValidateExternalUserEmail(email, userID, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidateExternalUserEmail, ex);
            }
            
        }

        /// <summary>
        /// Gets external userId by email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetExternalUserIDByEmail(string email, int userID, DbContextBL context)
        {
            try
            {
                return ExternalCollaborator.GetExternalUserIDByEmail(email, userID, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetExternalUserIDByEmail, ex);
            }
        }

        /// <summary>
        /// Get external user email by ID
        /// </summary>
        /// <param name="exUserid"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetExternalUserEmailByID(int exUserid, int userID, DbContextBL context)
        {
            try
            {
                return ExternalCollaborator.GetExternalUserEmailByID(exUserid, userID, context);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetExternalUserEmailByID, ex);
            }
            
        }

        public static ExternalCollaborator GetColalboratorByID(int exUserID, DbContextBL context)
        {
            return (from ex in context.ExternalCollaborators
                where ex.ID == exUserID
                select ex).FirstOrDefault();
        }
    }
}
