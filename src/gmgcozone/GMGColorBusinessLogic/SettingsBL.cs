﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Security.Policy;
using GMG.CoZone.Common;
using GMGColorDAL;
using GMGColor.Resources;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using NotificationSchedule = GMGColorDAL.NotificationSchedule;

namespace GMGColorBusinessLogic
{
    public class SettingsBL : BaseBL
    {
        #region Constants

        private static int pairingcodeLength = 6;

        #endregion

        /// <summary>
        /// Generates random numeric value
        /// </summary>
        /// <param name="colorProofPairingCodeRegion"></param>
        /// <returns></returns>
        public static string GetPairingCode(string colorProofPairingCodeRegion)
        {
            var random = new Random();
            int randomCode = random.Next(10000, 99999);
            return colorProofPairingCodeRegion + randomCode;
        }

        /// <summary>
        /// Returns randomn colorproof instance random password
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static void GenerateInstancePassword(out string password)
        {
            password = Utils.GenerateRandomAlphanumeric(10);
        }

        /// <summary>
        /// Checks if the given pairing code is valid
        /// </summary>
        /// <returns></returns>
        public static bool IsValidPairingCode(string pairingCode, string colorProofPairingCodeRegion, GMGColorContext context)
        {
            try
            {
                if (pairingCode.Length != pairingcodeLength || !pairingCode.StartsWith(colorProofPairingCodeRegion) ||
                    !ColorProofInstance.IsPairingCodeUnique(pairingCode, context))
                {
                    return false;
                }
                return true;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidatePairingCode, ex);
            }
        }

        /// <summary>
        /// Returns the expiration time of the pairing code
        /// </summary>
        /// <returns></returns>
        public static DateTime GetExpirationTime()
        {
            return DateTime.UtcNow.AddHours(1);
        }

        /// <summary>
        /// Determines if the PairingCode Is Valid Or Not
        /// </summary>
        /// <param name="pairingCodeExpirationDate"></param>
        /// <returns></returns>
        public static bool IsPairingCodeExpired(DateTime? pairingCodeExpirationDate)
        {
            return DateTime.UtcNow > pairingCodeExpirationDate.GetValueOrDefault();
        }

        /// <summary>
        /// Determines ColorProofInstance ConnectionStatus
        /// </summary>
        /// <param name="stateKey"></param>
        /// <param name="isOnline"></param>
        /// <param name="pairingCode"></param>
        /// <param name="pairingCodeExpirationDate"></param>
        /// <param name="isInstanceFromCurrentAccount"></param>
        /// <returns></returns>
        public static string GetCPConnectionStatus(int stateKey, bool?
            isOnline, string pairingCode, DateTime? pairingCodeExpirationDate, bool isInstanceFromCurrentAccount)
        {
            if (isInstanceFromCurrentAccount)
            {
                if (pairingCode != null)
                {
                    if (stateKey == (int)GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Pending)
                    {
                        if (!SettingsBL.IsPairingCodeExpired(pairingCodeExpirationDate.GetValueOrDefault()))
                        {
                            return GMGColor.Resources.Resources.lblPairingConnectionStatus + " " + pairingCode;
                        }
                        else
                        {
                            return GMGColor.Resources.Resources.lblPairingTimeoutConnectionStatus;
                        }
                    }
                    else if (isOnline.GetValueOrDefault())
                    {
                        return GMGColor.Resources.Resources.lblColorProofInstancesConnectionStatusOnline;
                    }
                    else
                    {
                        return GMGColor.Resources.Resources.lblColorProofInstancesConnectionStatusOffline;
                    }
                }
                else
                {
                    if (isOnline.GetValueOrDefault())
                    {
                        return GMGColor.Resources.Resources.lblColorProofInstancesConnectionStatusOnline;
                    }
                    else
                    {
                        return GMGColor.Resources.Resources.lblColorProofInstancesConnectionStatusOffline;
                    }
                }
            }
            return GMGColor.Resources.Resources.lblColorProofInstancesConnectionStatusShared;
        }

        public static string GetPairingCodeStatus(int stateKey, string pairingCode, DateTime? pairingCodeExpirationDate)
        {
            if (pairingCode != null)
            {
                if (stateKey == (int)GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Pending)
                {
                    if (!SettingsBL.IsPairingCodeExpired(pairingCodeExpirationDate.GetValueOrDefault()))
                    {
                        return GMGColor.Resources.Resources.lblPairingStatusValid;
                    }
                    else
                    {
                        return GMGColor.Resources.Resources.lblPairingStatusExpired;
                    }
                }
                else
                {
                    return GMGColor.Resources.Resources.lblPairingStatusInUse;
                }
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Determines if the specified pairing request is valid or not
        /// </summary>
        /// <param name="cpInstance"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsValidPairingConnection(ColorProofInstance cpInstance)
        {
            if (cpInstance.State == (int)GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Pending && IsPairingCodeExpired(cpInstance.PairingCodeExpirationDate))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Add or Update current account collaborate global settings
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="settingValue"></param>
        /// <param name="settingName"></param>
        /// <param name="context"></param>
        public static void AddOrUpdateCollaborateGlobalSetting(int accId, bool settingValue, string settingName, string description, DbContextBL context)
        {
            AccountSetting accSetting = (from acs in context.AccountSettings
                                         where acs.Account == accId && acs.Name == settingName
                                         select acs).FirstOrDefault();

            if (accSetting == null)
            {
                accSetting = new AccountSetting
                {
                    Account = accId,
                    Name = settingName,
                    ValueDataType = (from vdt in context.ValueDataTypes
                                     where vdt.Key == "BOLN"
                                     select vdt.ID).FirstOrDefault(),
                    Description = description
                };
                context.AccountSettings.Add(accSetting);
            }

            accSetting.Value = settingValue.ToString();
        }

        /// <summary>
        /// Add or Update current account collaborate global settings
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="settingValue"></param>
        /// <param name="settingName"></param>
        /// <param name="context"></param>
        public static void AddOrUpdateCollaborateGlobalSetting(int accId, int settingValue, string settingName, string description, DbContextBL context)
        {
            AccountSetting accSetting = (from acs in context.AccountSettings
                                         where acs.Account == accId && acs.Name == settingName
                                         select acs).FirstOrDefault();

            if (accSetting == null)
            {
                accSetting = new AccountSetting
                {
                    Account = accId,
                    Name = settingName,
                    ValueDataType = (from vdt in context.ValueDataTypes
                                     where vdt.Key == "IN32"
                                     select vdt.ID).FirstOrDefault(),
                    Description = description
                };
                context.AccountSettings.Add(accSetting);
            }

            accSetting.Value = settingValue.ToString();
        }

        /// <summary>
        /// Update the active/inactive Approval Statuses
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="approvalDecisions"></param>
        /// <param name="context"></param>
        public static void AddOrUpdateCollaborateGlobalSetting(int accId, List<AccountSettings.CollaborateApprovalStatusGeneralSettings> approvalDecisions, DbContextBL context)
        {
            var dbDecisions = (from ias in context.InactiveApprovalStatuses
                               where ias.Account == accId
                               select  ias).ToList();
            foreach (var decision in approvalDecisions)
            {
                var existingInactiveStatus = dbDecisions.FirstOrDefault(a => a.ApprovalDecision == decision.Id);
                if (decision.IsChecked && existingInactiveStatus == null)
                    {
                        var newActiveStatus = new InactiveApprovalStatus()
                            {
                                Account = accId,
                                ApprovalDecision = decision.Id
                            };
                        context.InactiveApprovalStatuses.Add(newActiveStatus);
                    }
                else if (!decision.IsChecked && existingInactiveStatus != null)
                    {
                        context.InactiveApprovalStatuses.Remove(existingInactiveStatus);
                    }
            }
        }


        /// <summary>
        /// Get account global setting value by option name
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="settingValue"></param>
        /// <param name="settingName"></param>
        /// <param name="context"></param>
        public static bool GetAccountGlobalSetting(int accId, string settingName,
            DbContextBL context)
        {
            bool settingValue = false;
            string accSetting = (from acs in context.AccountSettings
                                 where acs.Account == accId && acs.Name == settingName
                                 select acs.Value).FirstOrDefault();

            if (accSetting != null)
            {
                bool.TryParse(accSetting, out settingValue);
            }

            return settingValue;
        }


        /// <summary>
        /// Add or Update current account All Collaborators Decision required setting
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="settingValue"></param>
        /// <param name="settingName"></param>
        /// <param name="context"></param>
        public static void AddOrUpdateCollaborateAccountSetting(int accId, bool settingValue, string settingName, string settingDescription, DbContextBL context)
        {
            AccountSetting accSetting = (from acs in context.AccountSettings
                                         where acs.Account == accId && acs.Name == settingName
                                         select acs).FirstOrDefault();

            if (accSetting == null)
            {
                accSetting = new AccountSetting
                {
                    Account = accId,
                    Name = settingName,
                    ValueDataType = (from vdt in context.ValueDataTypes
                                     where vdt.Key == "BOLN"
                                     select vdt.ID).FirstOrDefault(),
                    Description = settingDescription
                };
                context.AccountSettings.Add(accSetting);
            }

            accSetting.Value = settingValue.ToString();
        }

        /// <summary>
        /// Gets notification schedule from database based on PK
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static NotificationSchedule GetNotificationScheduleByID(int ID, DbContextBL context)
        {
            return (from ns in context.NotificationSchedules
                    where ns.ID == ID
                    select ns).FirstOrDefault();
        }

        /// <summary>
        /// Process Input containing schedule settings
        /// </summary>
        /// <param name="model"> Schedule view model</param>
        /// <param name="context">Database Context</param>
        public static void ProcessNotificationScheduleChanges(AddEditNotificationScheduleViewModel model,
            int loggedUserId, DbContextBL context)
        {
            try
            {
                NotificationSchedule notificationSchedule = null;
                if (model.ID > 0)
                {
                    //edit schedule
                    notificationSchedule = GetNotificationScheduleByID(model.ID, context);

                    //add user changes
                    foreach (var user in model.Users)
                    {
                        NotificationScheduleUser existingRelation = (from nsu in context.NotificationScheduleUsers
                                                                     where nsu.NotificationSchedule == model.ID && nsu.User == user.objUserGroup.ID
                                                                     select nsu).FirstOrDefault();

                        if (user.IsUserGroupInUser)
                        {
                            if (existingRelation == null)
                            {
                                existingRelation = AddNewNotificationScheduleUser(loggedUserId, 
                                    context, notificationSchedule, user);
                            }
                        }
                        else
                        {
                            if (existingRelation != null)
                            {
                                context.NotificationScheduleUsers.Remove(existingRelation);
                            }
                        }
                    }

                    if (model.UserGroups != null && model.UserGroups.Count > 0)
                    {
                        //process user group changes
                        foreach (var userGroup in model.UserGroups)
                        {
                            NotificationScheduleUserGroup existingRelation =
                                (from nsu in context.NotificationScheduleUserGroups
                                 where
                                     nsu.NotificationSchedule == model.ID &&
                                     nsu.UserGroup == userGroup.objUserGroup.ID
                                 select nsu).FirstOrDefault();

                            if (userGroup.IsUserGroupInUser)
                            {
                                if (existingRelation == null)
                                {
                                    existingRelation = AddNewNotificationScheduleUserGroup(loggedUserId, context, notificationSchedule, userGroup);
                                }
                            }
                            else
                            {
                                if (existingRelation != null)
                                {
                                    context.NotificationScheduleUserGroups.Remove(existingRelation);
                                }
                            }
                        }
                    }

                    notificationSchedule.ModifiedBy = loggedUserId;
                    notificationSchedule.ModifiedDate = DateTime.UtcNow;
                }
                else
                {
                    //new schedule
                    notificationSchedule = new NotificationSchedule();
                    context.NotificationSchedules.Add(notificationSchedule);

                    //add users to DB
                    foreach (var user in model.Users.Where(t => t.IsUserGroupInUser))
                    {
                        AddNewNotificationScheduleUser(loggedUserId, context, notificationSchedule, user);
                    }

                    if (model.UserGroups != null && model.UserGroups.Count > 0)
                    {
                        //add user groups to database
                        foreach (var userGroup in model.UserGroups.Where(t => t.IsUserGroupInUser))
                        {
                            AddNewNotificationScheduleUserGroup(loggedUserId, context, notificationSchedule, userGroup);
                        }
                    }

                    notificationSchedule.CreatedBy = loggedUserId;
                    notificationSchedule.CreatedDate = DateTime.UtcNow;
                    notificationSchedule.ModifiedBy = loggedUserId;
                    notificationSchedule.ModifiedDate = DateTime.UtcNow;
                }

                notificationSchedule.NotificationPreset = model.SelectedPreset;
                notificationSchedule.NotificatonFrequency = model.SelectedFrecvency;
                notificationSchedule.NotificationCollaborateTrigger = model.SelectedTrigger;
                notificationSchedule.NotificationCollaborateUnit = model.SelectedUnit;
                notificationSchedule.NotificationCollaborateUnitNumber = model.SelectedUnitValue;
                notificationSchedule.NotificationPerPSSession = model.NotificationPerPSSession;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessNotificationScheduleChanges, ex);
            }
        }

        private static NotificationScheduleUserGroup AddNewNotificationScheduleUserGroup(int loggedUserId, DbContextBL context, NotificationSchedule notificationSchedule, UserGroupsInUser userGroup)
        {
            NotificationScheduleUserGroup notifScheduleUser = new NotificationScheduleUserGroup();

            notifScheduleUser.UserGroup = userGroup.objUserGroup.ID;
            notifScheduleUser.NotificationSchedule1 = notificationSchedule;

            notifScheduleUser.CreatedBy = notifScheduleUser.ModifiedBy = loggedUserId;
            notifScheduleUser.CreatedDate = notifScheduleUser.ModifiedDate = DateTime.UtcNow;

            context.NotificationScheduleUserGroups.Add(notifScheduleUser);

            return notifScheduleUser;
        }

        private static NotificationScheduleUser AddNewNotificationScheduleUser(int loggedUserId, DbContextBL context, NotificationSchedule notificationSchedule, UserGroupsInUser user)
        {
            NotificationScheduleUser notifScheduleUser = new NotificationScheduleUser();

            notifScheduleUser.User = user.objUserGroup.ID;
            notifScheduleUser.NotificationSchedule1 = notificationSchedule;

            notifScheduleUser.CreatedBy = loggedUserId;
            notifScheduleUser.CreatedDate = DateTime.UtcNow;

            context.NotificationScheduleUsers.Add(notifScheduleUser);

            return notifScheduleUser;
        }

        /// <summary>
        /// Delete Notification Schedule from Database
        /// </summary>
        /// <param name="id"> Schedule Database Id</param>
        /// <param name="context">Database Context</param>
        public static void DeleteNotificationSchedule(int id, DbContextBL context)
        {
            try
            {
                NotificationSchedule notificationSchedule = GetNotificationScheduleByID(id, context);

                foreach (var scheduleUser in notificationSchedule.NotificationScheduleUsers.ToList())
                {
                    context.NotificationScheduleUsers.Remove(scheduleUser);
                }

                foreach (var scheduleUserGroup in notificationSchedule.NotificationScheduleUserGroups.ToList())
                {
                    context.NotificationScheduleUserGroups.Remove(scheduleUserGroup);
                }

                context.NotificationSchedules.Remove(notificationSchedule);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteNotificationSchedule, ex);
            }
        }

        /// <summary>
        /// Get branding preset by preset id
        /// </summary>
        /// <param name="presetId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static BrandingPreset GetBrandingPresetByID(int presetId, DbContextBL context)
        {
            return (from bp in context.BrandingPresets
                    join bpth in context.BrandingPresetThemes on bp.ID equals bpth.BrandingPreset
                    join tcs in context.ThemeColorSchemes on bpth.ID equals tcs.BrandingPresetTheme
                    where bp.ID == presetId
                    select bp).FirstOrDefault();
        }

        /// <summary>
        /// Save or update the preset
        /// </summary>
        /// <param name="model">Branding preset for update or the new preset</param>
        /// <param name="loggedAccount">Logged account</param>
        /// <param name="headerLogo">Header logo filename</param>
        /// <param name="emailLogo">Email logo filename</param>
        /// <param name="loggedUserTempFolderPath"></param>
        /// <param name="context">Database context</param>
        public static void SaveOrUpdateBrandingPreset(AccountSettings.BrandingPreset model, Account loggedAccount, string headerLogo, string emailLogo, string favIcon, string loggedUserTempFolderPath, DbContextBL context)
        {
            try
            {
                var preset = new BrandingPreset();

                // Update preset
                if (model.ID > 0)
                {
                    preset = UpdateExistingBradingPreset(model, context, preset);
                }
                else // New preset
                {
                    AddBrandingPreset(model, loggedAccount, context, preset);
                }
                UserBL.UpdateBrandingPresetLogo(loggedAccount, headerLogo, preset, Account.LogoType.HeaderLogo, loggedUserTempFolderPath);
                UserBL.UpdateBrandingPresetLogo(loggedAccount, emailLogo, preset, Account.LogoType.EmailLogo, loggedUserTempFolderPath);
                UserBL.UpdateBrandingPresetLogo(loggedAccount, favIcon, preset, Account.LogoType.FavIconLogo, loggedUserTempFolderPath);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSaveOrUpdateBrandingPreset, ex);
            }
        }

        /// <summary>
        /// Save a new brading preset
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loggedAccount"></param>
        /// <param name="context"></param>
        /// <param name="preset"></param>
        private static void AddBrandingPreset(AccountSettings.BrandingPreset model, Account loggedAccount, DbContextBL context, BrandingPreset preset)
        {
            context.BrandingPresets.Add(preset);

            if (model.UserGroups != null && model.UserGroups.Count > 0)
            {
                //add user groups to database
                foreach (var userGroup in model.UserGroups.Where(t => t.IsUserGroupInUser))
                {
                    var brandingPresetUserGroup = new BrandingPresetUserGroup();
                    brandingPresetUserGroup.UserGroup = userGroup.objUserGroup.ID;
                    brandingPresetUserGroup.BrandingPreset1 = preset;
                    context.BrandingPresetUserGroups.Add(brandingPresetUserGroup);
                }
            }

            preset.Account = loggedAccount.ID;
            preset.Name = model.Name;
            preset.Guid = Guid.NewGuid().ToString();

            var themeSchemaToSave = ThemeBL.ConvertThemesColors(model.objCustomise.objColorSchema, Common.ConversionEnum.Convert.RgbToHex);
            preset.BrandingPresetThemes = themeSchemaToSave.Select(th => new BrandingPresetTheme()
            {
                Key = th.Key,
                Active = th.Active,
                ThemeName = th.ThemeName,
                BrandingPreset = preset.ID,
                ThemeColorSchemes = th.Colors.Select(cl => new ThemeColorScheme()
                {
                    Name = cl.Name,
                    THex = cl.TopColor,
                    BHex = cl.BottomColor,
                    BrandingPresetTheme = th.ID
                }).ToList()
            }).ToList();

            context.BrandingPresetThemes.AddRange(preset.BrandingPresetThemes);
        }

        /// <summary>
        /// Update an existing brading preset
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        /// <param name="preset"></param>
        /// <returns></returns>
        private static BrandingPreset UpdateExistingBradingPreset(AccountSettings.BrandingPreset model, DbContextBL context, BrandingPreset preset)
        {
            //edit schedule
            preset = GetBrandingPresetByID(model.ID, context);
            preset.Name = model.Name;

            var updateTheme = ThemeBL.ConvertThemesColors(model.objCustomise.objColorSchema, Common.ConversionEnum.Convert.RgbToHex);

            foreach (var theme in preset.BrandingPresetThemes)
            {
                theme.ThemeName = updateTheme.Where(th => th.Key == theme.Key).Select(th => th.ThemeName).FirstOrDefault();
                theme.Active = updateTheme.Where(th => th.Key == theme.Key).Select(th => th.Active).FirstOrDefault();

                foreach (var color in theme.ThemeColorSchemes)
                {
                    color.BHex = updateTheme.Where(th => th.Key == theme.Key).Select(th => th.Colors).ToList().Select(cl => cl.Where(ci => ci.ID == color.ID).Select(ci => ci.BottomColor).FirstOrDefault()).FirstOrDefault();
                    color.THex = updateTheme.Where(th => th.Key == theme.Key).Select(th => th.Colors).ToList().Select(cl => cl.Where(ci => ci.ID == color.ID).Select(ci => ci.TopColor).FirstOrDefault()).FirstOrDefault();
                }
            }

            if (model.UserGroups != null && model.UserGroups.Count > 0)
            {
                ProcessBrandingPresetUserGroupChanges(model, context, preset);
            }
            return preset;
        }

        /// <summary>
        /// Process user group changes on an existing branding preset
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        /// <param name="preset"></param>
        private static void ProcessBrandingPresetUserGroupChanges(AccountSettings.BrandingPreset model, DbContextBL context, BrandingPreset preset)
        {
            //process user group changes
            foreach (var userGroup in model.UserGroups)
            {
                BrandingPresetUserGroup existingRelation =
                    (from bpug in context.BrandingPresetUserGroups
                     where
                         bpug.BrandingPreset == model.ID &&
                         bpug.UserGroup == userGroup.objUserGroup.ID
                     select bpug).FirstOrDefault();

                if (userGroup.IsUserGroupInUser)
                {
                    if (existingRelation == null)
                    {
                        existingRelation = new BrandingPresetUserGroup
                        {
                            UserGroup = userGroup.objUserGroup.ID,
                            BrandingPreset1 = preset
                        };
                        context.BrandingPresetUserGroups.Add(existingRelation);
                    }
                }
                else
                {
                    if (existingRelation != null)
                    {
                        context.BrandingPresetUserGroups.Remove(existingRelation);
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of user groups that were not selected for any brandig presets, plus groups selected for current preset
        /// </summary>
        /// <param name="accId">Selected account id</param>
        /// <param name="presetId">Selected preset id for update</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static System.Collections.Generic.List<UserGroupsInUser> GetUserGroupsExceptSelected(int accId, int presetId, DbContextBL context)
        {
            return (from ug in context.UserGroups
                    let brandingGroups = (from bpug in context.BrandingPresetUserGroups
                                          join bp in context.BrandingPresets on bpug.BrandingPreset equals bp.ID
                                          where bp.Account == accId && bp.ID != presetId
                                          select bpug.UserGroup).ToList()
                    where ug.Account == accId && !brandingGroups.Contains(ug.ID)
                    select new UserGroupsInUser
                    {
                        objUserGroup = new UserGroupInfo
                        {
                            ID = ug.ID,
                            Name = ug.Name
                        }
                    }).ToList();
        }

        /// <summary>
        /// Checks whether the group is selected for current branding preset
        /// </summary>
        /// <param name="selectedPreset">Selected branding preset</param>
        /// <param name="userGroupId">The id of the group to be checked</param>
        /// <param name="Context">Database context</param>
        /// <returns></returns>
        public static bool IsPresetUserGroupSelected(int selectedPreset, int userGroupId, DbContextBL Context)
        {
            return (from bpug in Context.BrandingPresetUserGroups
                    where bpug.UserGroup == userGroupId && bpug.BrandingPreset == selectedPreset
                    select bpug.ID).Any();
        }

        public static void UnlockUser(int selectedUser, int loggedAccountId, DbContextBL context)
        {
            var user = (from a in context.Accounts
                        join u in context.Users on a.ID equals u.Account
                        join ulfd in context.UserLoginFailedDetails on u.ID equals ulfd.User
                        where a.ID == loggedAccountId && ulfd.User == selectedUser && ulfd.IsUserLoginLocked
                        select ulfd).FirstOrDefault();

            if(user != null)
            {
                context.UserLoginFailedDetails.Remove(user);
                context.SaveChanges();
            }
        }

        public static void ActivateUserStatus(int selectedUser, int loggedAccountId, DbContextBL context)
        {
            var userDetails = (from a in context.Accounts
                               join u in context.Users on a.ID equals u.Account
                               where a.ID == loggedAccountId && u.ID == selectedUser
                               select u).FirstOrDefault();

            userDetails.Status = (int)UserStatus.Active;
            context.SaveChanges();
        }

        public static void DeactivateUserStatus(int selectedUser, int loggedAccountId, DbContextBL context)
        {
            var userDetails = (from a in context.Accounts
                               join u in context.Users on a.ID equals u.Account
                               where a.ID == loggedAccountId && u.ID == selectedUser
                               select u).FirstOrDefault();

            userDetails.Status = (int)UserStatus.Inactive;
            context.SaveChanges();
        }

        public static bool GetUserGroupBrandingPreset(int groupId, DbContextBL context)
        {
            return (from bp in context.BrandingPresets
                    join bpug in context.BrandingPresetUserGroups on bp.ID equals bpug.BrandingPreset
                    where bpug.UserGroup == groupId
                    select bp).Any();
        }

        /// <summary>
        /// Add or update approval custom decisions for current account
        /// </summary>
        /// <param name="accountId">Logged account id</param>
        /// <param name="approvalCustomDecisions">List of decisions to be added or updated</param>
        /// <param name="context"></param>
        public static void AddOrUpdateCustomDecision(int accountId, AccountSettings.ApprovalStatus approvaCustomStatuses, DbContextBL context)
        {
            try
            {
                //decisionss in DB
                var customApprovalDecisions = (from ad in context.ApprovalDecisions
                                               join acd in context.ApprovalCustomDecisions on ad.ID equals acd.Decision
                                               where acd.Account == accountId
                                               select acd).ToList();


                foreach (var approvalCustomDecision in approvaCustomStatuses.CustomDecisions)
                {
                    foreach (var language in approvalCustomDecision.CustomDecisionLanguages)
                    {
                        var existingBOLanguage =
                            customApprovalDecisions.FirstOrDefault(
                                t => t.Decision == approvalCustomDecision.DecisionId && t.Locale == language.Locale);
                        if (existingBOLanguage != null)
                        {
                            if (String.IsNullOrEmpty(language.Name))
                            {
                                context.ApprovalCustomDecisions.Remove(existingBOLanguage);
                            }
                            else
                            {
                                existingBOLanguage.Name = language.Name;
                            }
                        }
                        else if (!String.IsNullOrEmpty(language.Name))
                        {
                            var newLanguageObj = new ApprovalCustomDecision()
                                {
                                    Account = accountId,
                                    Decision = approvalCustomDecision.DecisionId,
                                    Locale = language.Locale,
                                    Name =language.Name
                                };
                            context.ApprovalCustomDecisions.Add(newLanguageObj);
                        }
                    }
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddOrUpdateCustomDecision, ex);
            }
        }

        public static string GetCustomProfileRelativeVirtualPath(string accGuid, string profileGuid)
        {
            return
                    GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath + "/" + accGuid +
                    "/" + GMGColorConfiguration.AppConfiguration.CustomICCProfileRelativePath + "/" +
                    profileGuid + "/";
        }

        public static string GetGlobalCustomProfileRelativeVirtualPath()
        {
            return
                GMGColorConfiguration.AppConfiguration.AccountsGlobalFolderRelativePath + "/" +
                GMGColorConfiguration.AppConfiguration.CustomICCProfileRelativePath + "/";
        }

        public static void UpdateAccountCustomICCProfile(Account loggedAccount, string customProfileName, int? SelectedPaperSubstrate, int userId, DbContextBL context)
        {
            var activeProfile = (from accs in context.AccountCustomICCProfiles
                                 where accs.Account == loggedAccount.ID && accs.IsActive
                                select accs).FirstOrDefault();

            if (!String.IsNullOrEmpty(customProfileName))
            {
                string profileFileName = customProfileName.Trim().Replace("|", string.Empty);

                //if profile is uploaded and there is no other in DB create new one
                if (activeProfile == null || !profileFileName.Equals(activeProfile.Name))
                {
                    //case when the old profile was deleted and a new profile was uploaded in same session
                    if (activeProfile != null)
                    {
                        activeProfile.IsActive = false;
                    }

                    //add new entry in DB
                    AccountCustomICCProfile cstmProfile = new AccountCustomICCProfile();
                    cstmProfile.Account = loggedAccount.ID;
                    cstmProfile.Guid = Guid.NewGuid().ToString();
                    cstmProfile.IsActive = true;
                    cstmProfile.Name = profileFileName;
                    cstmProfile.UploadDate = DateTime.UtcNow;
                    cstmProfile.Uploader = userId;
                    cstmProfile.PrintSubstrate = SelectedPaperSubstrate;

                    context.AccountCustomICCProfiles.Add(cstmProfile);

                    context.SaveChanges();

                    //upload to s3
                    string sourceProfilePath = GMGColorDAL.User.GetUserTempFolderPath(userId, loggedAccount.ID, loggedAccount.Region) + profileFileName;

                    if (GMGColorIO.FileExists(sourceProfilePath, loggedAccount.Region))
                    {
                        //get destination profile path
                        string relativeDestinationPath = GetCustomProfileRelativeVirtualPath(loggedAccount.Guid, cstmProfile.Guid);

                        //create destination folder
                        GMGColorIO.FolderExists(relativeDestinationPath, loggedAccount.Region, true);

                        //move file to destination folder
                        GMGColorIO.MoveFile(sourceProfilePath, relativeDestinationPath + profileFileName, loggedAccount.Region);
                    }

                    //create define queue message for validation of icc profile
                    Dictionary<string, string> dictJobParameters = new Dictionary<string, string>();
                    dictJobParameters.Add(Constants.Action, SoftProofingAction.ValidateCustomProfile.ToString());
                    dictJobParameters.Add(Constants.ProfileId, cstmProfile.ID.ToString());

                    GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.SoftProofing);
                }
                else
                {
                    activeProfile.PrintSubstrate = SelectedPaperSubstrate;
                }
            }
            else
            {
                //mark as inactive when deleted from interface
                if (activeProfile != null)
                {
                    activeProfile.IsActive = false;
                }
            }
        }

        /// <summary>
        /// Get the bounced email users
        /// </summary>
        /// <param name="loggedAccount"></param>
        /// <returns></returns>
        public static List<BouncedEmailUsers> GetBouncedEmailUsers(int loggedAccount)
        {
            var bouncedEmailUsersList = new List<BouncedEmailUsers>();
            using (var context = new DbContextBL())
            {
                bouncedEmailUsersList.AddRange((from acc in context.Accounts
                                             join usr in context.Users on acc.ID equals usr.Account
                                             join bem in context.BouncedEmails on usr.BouncedEmailID equals bem.ID
                                             where acc.ID == loggedAccount
                                             select new BouncedEmailUsers 
                                             {
                                                 ID = bem.ID,
                                                 Email = usr.EmailAddress,
                                                 Message = bem.Message,
                                                 IsExternal = false
                                             }).Distinct().ToList());

                bouncedEmailUsersList.AddRange((from acc in context.Accounts
                                                     join extusr in context.ExternalCollaborators on acc.ID equals extusr.Account
                                                     join bem in context.BouncedEmails on extusr.BouncedEmailID equals bem.ID
                                                     where acc.ID == loggedAccount
                                                     select new BouncedEmailUsers
                                                     {
                                                         ID = bem.ID,
                                                         Email = extusr.EmailAddress,
                                                         Message = bem.Message,
                                                         IsExternal = true
                                                     }).Distinct().ToList());

            }
            return bouncedEmailUsersList;
        }

        /// <summary>
        /// Remove the email block
        /// </summary>
        /// <param name="selectedMessageId"></param>
        /// <param name="isExternal"></param>
        /// <param name="loggedAccount"></param>
        public static void DeleteBouncedEmailBlock(int selectedMessageId, bool isExternal, int loggedAccount)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    if (isExternal)
                    {
                        context.ExternalCollaborators.Where(u => u.Account == loggedAccount && u.BouncedEmailID == selectedMessageId).ToList().ForEach(us => us.BouncedEmailID = null);
                    }
                    else
                    {
                        context.Users.Where(u => u.Account == loggedAccount && u.BouncedEmailID == selectedMessageId).ToList().ForEach(us => us.BouncedEmailID = null);
                    }
                    context.SaveChanges();
                    
                    //delete message
                    var messageToDelete = new BouncedEmail { ID = selectedMessageId };
                    context.BouncedEmails.Attach(messageToDelete);
                    context.BouncedEmails.Remove(messageToDelete);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddOrUpdateCustomDecision, ex);  
            }            
        }

        /// <summary>
        /// Based on notification settings changes get all notification items for the appropriate users
        /// </summary>
        /// <param name="account">The account from which the changes were made</param>
        /// <param name="context">Database context</param>
        /// <param name="preset">The account preset that was updated</param>
        /// <param name="schedule">The account schedule that was updated</param>
        /// <param name="userPersSetting">The userId of the user that updated his personal settings</param>
        public static void MarkNotificationAsDeleted(int account, DbContextBL context, int preset = 0, int schedule = 0, int userPersSetting = 0)
        {
            List<int> notificationItemsToDelete = new List<int>();

            // check if account or personal chages are updated
            if (preset != 0 || schedule != 0)
            {
                // check ii preset or schedule is updated
                if (preset != 0)
                {
                    // get the notification items based on preset
                    notificationItemsToDelete = (from ni in context.NotificationItems
                                                let presetSchedules = (from np in context.AccountNotificationPresets
                                                                    join ns in context.NotificationSchedules on np.ID equals ns.NotificationPreset
                                                                    where np.ID == preset && np.Account == account
                                                                    select ns.ID).ToList()
                                                let usersOnSchedules = (from nsu in context.NotificationScheduleUsers
                                                                        where presetSchedules.Contains(nsu.NotificationSchedule)
                                                                        select nsu.User).Union(from ngu in context.NotificationScheduleUserGroups
                                                                                            join ugu in context.UserGroupUsers on ngu.UserGroup equals ugu.UserGroup
                                                                                            where presetSchedules.Contains(ngu.NotificationSchedule)
                                                                                            select ugu.User).ToList()
                                                where usersOnSchedules.Contains(ni.InternalRecipient.Value) && !ni.IsDeleted
                                                select ni.ID).ToList();
                    
                }
                else
                {
                    // get the notification items based on schedule
                    notificationItemsToDelete = (from ni in context.NotificationItems
                                                let usersOnSchedules = (from nsu in context.NotificationScheduleUsers
                                                                        where nsu.NotificationSchedule == schedule
                                                                        select nsu.User).Union(from ngu in context.NotificationScheduleUserGroups
                                                                                            join ugu in context.UserGroupUsers on ngu.UserGroup equals ugu.UserGroup
                                                                                            where ngu.NotificationSchedule == schedule
                                                                                            select ugu.User).ToList()
                                                where usersOnSchedules.Contains(ni.InternalRecipient.Value) && !ni.IsDeleted
                                                select ni.ID).ToList();
                }
            }
            else
            {
                // get the notification items based on user id
                notificationItemsToDelete = (from ni in context.NotificationItems
                                             join us in context.Users on ni.InternalRecipient equals us.ID
                                             where us.Account == account && us.ID == userPersSetting && !ni.IsDeleted
                                             select ni.ID).ToList();
            }
            UpdateNotificationItem(notificationItemsToDelete, context);
            context.SaveChanges();
        }
        
        /// <summary>
        /// Mark notification item as deleted
        /// </summary>
        /// <param name="notificationItemsToDelete">The list of notification item that should be marked as deleted</param>
        /// <param name="context">Database context</param>
        private static void UpdateNotificationItem(List<int> notificationItemsToDelete, DbContextBL context)
        {
            var notificationItems = context.NotificationItems.Where(n => notificationItemsToDelete.Contains(n.ID));
            foreach (var item in notificationItems)
            {
                item.IsDeleted = true;
            }
        }

        /// <summary>
        /// Delete all notifcation markes as deleted from the database
        /// </summary>
        /// <param name="context">Database context</param>
        public static void RemoveDeletedNotifications(DbContextBL context)
        {
            context.NotificationItems.RemoveRange(context.NotificationItems.Where(ni => ni.IsDeleted == true));
            context.SaveChanges();
        }
    }
}
