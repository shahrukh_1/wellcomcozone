﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMGColorBusinessLogic
{
    public class FolderBL: BaseBL
    {

        /// <summary>
        /// Determines if the folder structure exists on the specified user Account
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="parentFolderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool FolderExists(string folderName, string parentFolderName, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    join prntfl in context.Folders on fl.Parent equals prntfl.ID
                    where fl.Name == folderName && prntfl.Name == parentFolderName && us.ID == userID
                    select fl.ID).Any();
        }

        /// <summary>
        /// Determines if the folder structure exists on the specified user Account
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<int> GetSubfolderIdsForFolder(int folderId, DbContextBL context)
        {
            return (from fl in context.Folders
                    where fl.Parent== folderId && fl.IsDeleted==false
                    select fl.ID).ToList();
        }

        /// <summary>
        /// Determines if the folder exists on the specified user Account
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="parentFolderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool FolderExists(string folderName, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where fl.Name == folderName && us.ID == userID
                    select fl.ID).Any();
        }

        /// <summary>
        /// Gets Folder By Name
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Folder GetFolderByName(string folderName, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where fl.Name == folderName && us.ID == userID
                    select fl).FirstOrDefault();
        }

        /// <summary>
        /// Gets Folder By ID
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Folder GetFolderByID(int ID, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where fl.ID == ID && us.ID == userID
                    select fl).FirstOrDefault();
        }

        /// <summary>
        /// Gets Folder ID By Name
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetFolderIDByName(string folderName, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where fl.Name == folderName && us.ID == userID
                    select fl.ID).FirstOrDefault();
        }

        /// <summary>
        /// Gets Folder By ID
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetFolderIDByNameAndParentID(string folderName, int parentID, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    join prntfl in context.Folders on fl.Parent equals prntfl.ID
                    where fl.Name == folderName && prntfl.ID == parentID && us.ID == userID
                    select fl.ID).FirstOrDefault();
        }

        /// <summary>
        /// Gets Folder by name and parent
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="parentFolderName"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Folder GetFolderByNameAndParent(string folderName, string parentFolderName, int userID, DbContextBL context)
        {
            return (from fl in context.Folders
                    join ac in context.Accounts on fl.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    join prntfl in context.Folders on fl.Parent equals prntfl.ID
                    where fl.Name == folderName && prntfl.Name == parentFolderName && us.ID == userID
                    select fl).FirstOrDefault();
        }

        /// <summary>
        /// Returns Approvals Contained in the specified Folder
        /// </summary>
        /// <param name="objFolder"></param>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<int> ReturnFoldersApprovalsIDs(Folder objFolder, int userID, DbContextBL context)
        {
            try
            {
                return (from app in objFolder.Approvals
                        join acl in context.ApprovalCollaborators on app.ID equals acl.Approval
                        join j in context.Jobs on app.Job equals j.ID
                        join js in context.JobStatus on j.Status equals js.ID
                        where
                            acl.Collaborator == userID && app.AllowDownloadOriginal && app.IsDeleted == false && js.Key != "ARC"
                        select app.ID).ToList();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetFoldersApprovalsID, ex);
            }
        }

        public static void FolderCRD(DbContextBL context, ref FoldersModel model, bool adminHasRights = false)
        {
            if (model.Status == FoldersModel.FolderStatus.Default)
            {
                CreateRenameFolder(context, ref model);
            }
            else
            {
                try
                {
                    if (model.Status == FoldersModel.FolderStatus.EmptyRecycleBin)
                    {
                        int creator = model.Creator;
                        var deletedFolders = (from f in context.Folders
                                              where f.IsDeleted && f.Creator == creator
                                              select f.ID).ToList();

                        foreach (int folder in deletedFolders)
                        {
                            DeleteFolderPermanent(folder, model.Creator, context);
                        }
                    }
                    else if (model.Status == FoldersModel.FolderStatus.PermanentDelete)
                    {
                        DeleteFolderPermanent(model.Folder, model.Creator, context);
                    }
                    else
                    {
                        using (TransactionScope tscope = new TransactionScope())
                        {
                            ExecuteFolderActions(ref context, model.Folder, model.Status, model.Creator, adminHasRights);
                            context.SaveChanges();
                            tscope.Complete();
                        }
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    DALUtils.LogDbEntityValidationException(ex);
                }
                catch (Exception ex)
                {
                    GMGColorLogging.log.ErrorFormat(ex, model.Account, "GMGColorDAL.Folder.PerformFolderAction : Folder action failed. ex: {0}", ex.Message);
                }
            }
        }

        private static void CreateRenameFolder(DbContextBL context, ref FoldersModel model)
        {
            try
            {
                using (TransactionScope tscope = new TransactionScope())
                {
                    Folder objFolder;
                    if (model.Folder > 0)
                    {
                        objFolder = DALUtils.GetObject<Folder>(model.Folder, context);
                    }
                    else
                    {
                        objFolder = new Folder();

                        if (model.Parent > 0)
                        {
                            Folder objParent = DALUtils.GetObject<Folder>(model.Parent, context);

                            objFolder.Parent = objParent.ID;
                            objFolder.IsPrivate = objParent.IsPrivate;

                            if (model.FolderPermissionsAutoInheritanceEnabled)
                            { 
                                foreach (FolderCollaborator objItem in objParent.FolderCollaborators)
                            {
                                FolderCollaborator objFolderCollaborator = new FolderCollaborator();
                                objFolderCollaborator.AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                                objFolderCollaborator.Collaborator = objItem.Collaborator;

                                objFolder.FolderCollaborators.Add(objFolderCollaborator);
                            }
                            foreach (FolderCollaboratorGroup objItem in objParent.FolderCollaboratorGroups)
                            {
                                FolderCollaboratorGroup objFolderCollaboratorGroup = new FolderCollaboratorGroup();
                                objFolderCollaboratorGroup.AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                                objFolderCollaboratorGroup.CollaboratorGroup = objItem.CollaboratorGroup;

                                objFolder.FolderCollaboratorGroups.Add(objFolderCollaboratorGroup);
                            }
                            }
                            else
                            {
                                objFolder.IsPrivate = model.IsPrivate;

                                FolderCollaborator objFolderCollaborator = new FolderCollaborator();
                                objFolderCollaborator.AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                                objFolderCollaborator.Collaborator = model.Creator;

                                objFolder.FolderCollaborators.Add(objFolderCollaborator);
                            }
                        }
                        else
                        {
                            objFolder.IsPrivate = model.IsPrivate;

                            FolderCollaborator objFolderCollaborator = new FolderCollaborator();
                            objFolderCollaborator.AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                            objFolderCollaborator.Collaborator = model.Creator;

                            objFolder.FolderCollaborators.Add(objFolderCollaborator);
                        }

                        objFolder.Account = model.Account;
                        objFolder.Guid = Guid.NewGuid().ToString();
                        objFolder.Creator = model.Creator;
                        objFolder.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    }
                    objFolder.Name = model.Name;
                    objFolder.Modifier = model.Creator;
                    objFolder.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    objFolder.AllCollaboratorsDecisionRequired = model.AllCollaboratorsDecisionRequired;

                    if (objFolder.ID == 0)
                    {
                        context.Folders.Add(objFolder);
                    }

                    context.SaveChanges();
                    tscope.Complete();

                    model.Guid = objFolder.Guid;
                    model.Folder = objFolder.ID;
                    model.IsPrivate = objFolder.IsPrivate;
                }
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, model.Account, "GMGColorDAL.Folder.PerformFolderAction : Folder action failed. ex: {0}", ex.Message);
            }
        }

        private static void ExecuteFolderActions(ref DbContextBL context, int folderId, FoldersModel.FolderStatus status, int modifier, bool adminHasRights = false)
        {
            try
            {
                List<int> subFolders = (from f in context.Folders where f.Parent == folderId select f.ID).ToList();
                foreach (int subFolderId in subFolders)
                {
                    ExecuteFolderActions(ref context, subFolderId, status, modifier, adminHasRights);
                }

                var folder = (from f in context.Folders where f.ID == folderId && (f.Creator == modifier || adminHasRights) select f).Take(1).FirstOrDefault();
                if (folder != null)
                {
                    IEnumerable<Approval> approvals =
                    (from f in context.Folders
                     where f.ID == folderId
                     select f.Approvals.Where(a => a.Version != 0 && !a.DeletePermanently).Select(o => o)).
                        FirstOrDefault() ?? new Approval[] { };

                    if (!approvals.Any() && folder.Creator == modifier)
                    {
                        // delete folder from db only if logged user is the folder owner
                        string retVal = Folder.DeleteFolder(folderId, modifier, context);

                        if (retVal != "")
                        {
                            GMGColorLogging.log.InfoFormat(
                                "Error Occured in WidgetController DeleteFolderPermanent when calling DeleteFolder SPC. Error message {0}, folderId {1}",
                                retVal, folderId);
                        }
                    }
                    else
                    {
                        folder.Modifier = modifier;
                        folder.ModifiedDate = DateTime.UtcNow;
                        folder.IsDeleted = (FoldersModel.FolderStatus.Delete == status);

                        foreach (var approval in approvals)
                        {
                            if (approval.Creator == modifier || adminHasRights)
                            {
                                approval.IsDeleted = (status == FoldersModel.FolderStatus.Delete);
                                ApprovalBL.ApprovalUserRecycleBinHistory(approval, modifier, status == FoldersModel.FolderStatus.Delete, context);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error(ex.Message, ex);
                throw;
            }
        }

        private static void DeleteFolderPermanent(int folderId, int modifier, DbContextBL context)
        {
            IEnumerable<int> subfolders = (from f in context.Folders where f.Parent == folderId select f.ID);
            foreach (int subfolder in subfolders)
            {
                DeleteFolderPermanent(subfolder, modifier, context);
            }

            string retVal = Folder.DeleteFolder(folderId, modifier, context);

            if (retVal != "")
            {
                GMGColorLogging.log.InfoFormat("Error Occured in WidgetController DeleteFolderPermanent when calling DeleteFolder SPC. Error message {0}, folderId {1}", retVal, folderId);
            }
        }

        /// <summary>
        /// Return Folder Tree Html
        /// </summary>
        /// <param name="ActiveFolder">Active folder id.</param>
        /// <param name="IsOwnFolder">If "true" populater Own folders or otherwise shared folders tree.</param>
        /// <param name="UserID">Id of owner, folders to be populated.</param>
        /// <param name="inheritParentFolderPermissions"></param>
        /// <returns></returns>
        public static string PopulateFolders(int ActiveFolder, int UserID, int[] existingFolders, bool inheritParentFolderPermissions, DbContextBL context, bool showPanel = false, bool moveFiles = false)
        {
            try
            {
                GMGColorDAL.User objLoggedUser = DALUtils.GetObject<GMGColorDAL.User>(UserID, context);
                // TODO refactor the line bellow with a single custom query
                Role.RoleName loggedUserRole = Role.GetRole(objLoggedUser.CollaborateRoleKey(context));

                return PopulateFolders(true, (new List<int>()), Folder.GetFolderTree(UserID, context), ActiveFolder, UserID, false, loggedUserRole, null, inheritParentFolderPermissions, showPanel, moveFiles);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while pupulating folder tree, Method: PopulateFolders, Exception: {0}", ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        /// Return Folder Tree Html
        /// </summary>
        /// <param name="ActiveFolder">Active folder id.</param>
        /// <param name="IsOwnFolder">If "true" populater Own folders or otherwise shared folders tree.</param>
        /// <param name="UserID">Id of owner, folders to be populated.</param>
        /// <param name="Parent">Parent folder id.</param>
        /// <param name="inheritParentFolderPermissions"></param>
        /// <returns></returns>
        public static string PopulateFolders(int ActiveFolder, int objLoggedUserID, Role.RoleName loggedUserRole, int Parent, bool inheritParentFolderPermissions, DbContextBL context, bool viewAllFilesAndFolders = false, bool tabletVersion = false)
        {
            try
            {
                loggedUserRole = tabletVersion ? Role.RoleName.AccountViewer : loggedUserRole;

                return PopulateFolders(true, (new List<int>()), Folder.GetFolderTree(objLoggedUserID, context, viewAllFilesAndFolders), ActiveFolder, objLoggedUserID, true, loggedUserRole, null, inheritParentFolderPermissions);
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error occued while pupulating folder tree, Method: PopulateFolders, Exception: {0}", ex.Message);
            }
            return string.Empty;
        }

        /// <summary>
        /// Pupulate Folder Tree HTML
        /// </summary>
        /// <param name="ActiveFolder">Selected Folder Id</param>
        /// <param name="IsOwnFolder">If "true" populater Own folders or otherwise shared folders tree.</param>
        /// <param name="UserID">User who want to populater folder tree</param>
        /// <param name="NeedActionsTask">Need action folder manupulation links</param>
        /// <param name="Parent">Parent folder id</param>
        /// <returns></returns>
        private static string PopulateFolders(bool IsBegin, List<int> folderIDs, List<ReturnFolderTreeView> lstFolders, int ActiveFolder, int UserID, bool NeedActionsTask, Role.RoleName UserRole, int[] existingFolders, bool inheritParentFolderPermissions, bool showPanel = false, bool moveFiles = false)
        {
            bool canMakeFileTransfer = false;
            using (GMGColorContext context = new GMGColorContext()) {
                var fileTransferRole = GMGColorDAL.User.GetRole(GMG.CoZone.Common.AppModule.FileTransfer, UserID, context);
                if (fileTransferRole == "MAN" || fileTransferRole == "ADM")
                {
                    canMakeFileTransfer = true;
                }
            }
            StringBuilder folderTemplate = new StringBuilder();

            if (IsBegin)
            {
                IsBegin = false;
                folderIDs = lstFolders.OrderBy(c => c.Level).ThenBy(p => p.Parent).ThenBy(n => n.ID).Select(m => (int)m.ID).ToList();

                if ((folderIDs.Count == 0) && (UserRole != Role.RoleName.AccountViewer && UserRole != Role.RoleName.AccountContributor && UserRole != Role.RoleName.AccountRetoucher))
                {
                    if (NeedActionsTask)
                    {
                        folderTemplate.Append("<li class=\"text-center\"><a href=\"javascript:void(0);\" title=\"");
                        folderTemplate.Append(GMGColor.Resources.Resources.lblFolderEmptyMessge);
                        folderTemplate.Append("\" class=\"folder-action\" action=\"CreateFolder\" folder=\"0\" private=\"false\">");
                        folderTemplate.Append(GMGColor.Resources.Resources.lblFolderEmptyMessge);
                        folderTemplate.Append("</a></li>");
                    }
                    else
                    {
                        if (showPanel)
                        {
                            folderTemplate.Append("<li class=\"text-center\" style=\"margin: 0 14px;\">");
                            folderTemplate.Append(GMGColor.Resources.Resources.lblThereAreNoFolders);
                            folderTemplate.Append("</li>");
                        }
                        else if (moveFiles)
                        {
                            folderTemplate.Append("<li class=\"text-center\" style=\"margin: 0 14px;\">");
                            folderTemplate.Append(GMGColor.Resources.Resources.lblThereAreNoFoldersToMoveFiles);
                            folderTemplate.Append("</li>");
                        }
                    }
                }
                else if (folderIDs.Count == 0 && UserRole == Role.RoleName.AccountContributor)
                {
                    if (showPanel)
                    {
                        folderTemplate.Append("<li class=\"text-center\" style=\"margin: 0 14px;\">");
                        folderTemplate.Append(GMGColor.Resources.Resources.lblThereAreNoFolders);
                        folderTemplate.Append("</li>");
                    }
                    else if (moveFiles)
                    {
                        folderTemplate.Append("<li class=\"text-center\" style=\"margin: 0 14px;\">");
                        folderTemplate.Append(GMGColor.Resources.Resources.lblThereAreNoFoldersToMoveFiles);
                        folderTemplate.Append("</li>");
                    }
                }
            }

            foreach (int ID in folderIDs)
            {
                if (folderIDs.Contains(ID))
                {
                    ReturnFolderTreeView objFolder = lstFolders.SingleOrDefault(o => o.ID == ID);

                    var subfolderIDs = FilterFolders(ID, (new List<int>()), lstFolders);

                    string strActions = string.Empty; string strAccess = string.Empty;

                    if (UserRole == Role.RoleName.AccountAdministrator || UserRole == Role.RoleName.AccountManager || UserRole == Role.RoleName.AccountModerator)
                    {
                        if (NeedActionsTask)
                        {
                            bool PermanentDelete = !objFolder.FolderHasApprovals(lstFolders);
                            strAccess = "widget-toggle=\"collaboratoraccess\" widget-type=\"folder\" widget-id=\"" + objFolder.ID + "\" widget-creator=\"" + objFolder.Creator + "\" widget-users=\"\" widget-groups=\"\" widget-title=\"" + HttpUtility.HtmlEncode(objFolder.Name) + "\"";

                            strActions = "<div class=\"task btn-group\" >" +
                                            "<button class=\"btn btn-mini dropdown-toggle actionDropDownBtn \" onClick=\"dropDownFixPosition(this, " + ID + ")\" data-toggle=\"dropdown\" type=\"button\"><span class=\"caret\"></span></button>" +
                                            "<ul class=\"dropdown-menu pull-right\" id=\"actionDropDown" + ID + "\" style=\"position: fixed!important; width:200px\">" +
                                            "<li><a href=\"javascript:void(0);\" title=\"" + GMGColor.Resources.Resources.lblNewFolder + "\" class=\"folder-action\" action=\"CreateFolder\" folder=\"" + objFolder.ID + "\" private=\"false\"><i class=\"icon-folder-close\"></i> " + GMGColor.Resources.Resources.lblNewFolder + "</a></li>" +
                                            ((objFolder.Creator == UserID) ? "<li><a href=\"javascript:void(0);\" title=\"" + GMGColor.Resources.Resources.lblEdit + "\" class=\"folder-action\" action=\"RenameFolder\" data-alldecisionrequired=\"" + objFolder.AllCollaboratorsDecisionRequired + "\" folder=\"" + objFolder.ID + "\"><i class=\"icon-pencil\"></i> " + GMGColor.Resources.Resources.lblEdit + "</a></li>" +
                                            "<li><a href=\"javascript:void(0);\" title=\"" + GMGColor.Resources.Resources.lblDownload + "\" class=\"folder-action\" action=\"DownloadFolder\" folder=\"" + objFolder.ID + "\"><i class=\"icon-download-alt\"></i> " + GMGColor.Resources.Resources.lblDownload + "</a></li>" : string.Empty) +
                                            "<li><a href=\"javascript:void(0);\" title=\"" + GMGColor.Resources.Resources.btnCreateLink + "\" class=\"folder-action\" action=\"CreateLink\" folder=\"" + objFolder.ID + "\" private=\"false\"><i class=\"icon-paste\"></i> " + GMGColor.Resources.Resources.btnCreateLink + "</a></li>" +
                                            ((UserRole != Role.RoleName.AccountModerator || objFolder.Creator == UserID) ? "<li class=\"folder-permission\"><a " + strAccess + " href=\"javascript:void(0);\" onclick=\"LoadFolderPermissions(event, " + objFolder.ID + ",'/Widget/ApprovalPermissions?Refresh=" + (UserRole == Role.RoleName.AccountAdministrator && inheritParentFolderPermissions).ToString().ToLower() + "&ShowPanel=false', '/Approvals/GetFolderCollaborators?id=" + objFolder.ID + "', this);\"  title=\"" + GMGColor.Resources.Resources.lblAccess + "\"><i class=\"icon-lock\"></i> " + GMGColor.Resources.Resources.lblAccess + "</a></li>" : String.Empty) +
                                            (UserRole == Role.RoleName.AccountAdministrator || UserRole == Role.RoleName.AccountManager || objFolder.Creator == UserID ? "<li><a href=\"javascript:void(0);\" widget-toggle=\"foldertree\" widget-folders=\"\" widget-folder=\"" + objFolder.ID + "\" onclick=\"LoadFoldersTreePopup('/Widget/LoadFolderTree?showPanel=false&moveFiles=false', this, false)\" title=\"" + GMGColor.Resources.Resources.lblMoveFolder + "\"> <img alt=\"color_swatch\" src=\"../../Content/img/move-to-folder.png\" class=\"img-menu-item\" style=\"padding-right: 2px!important;\" />" + GMGColor.Resources.Resources.lblMoveFolder + "</a></li>" : String.Empty) +
                                            "<li><a href=\"javascript:void(0);\"  onclick=\"openReportFilterPopup(this, true)\" title=\"" + GMGColor.Resources.Resources.lblAnnotationsReport + "\" folder=\"" + objFolder.ID + "\"><i class=\"icon-list-alt\"></i> " + GMGColor.Resources.Resources.lblAnnotationsReport + "</a></li>" +
                                            (UserRole == Role.RoleName.AccountAdministrator && !inheritParentFolderPermissions ? "<li><a href=\"javascript:void(0);\"  title=\"" + GMGColor.Resources.Resources.lblApplyFolderPermissions + "\" onclick=\"applyFolderPermissionsConfirmation(" + objFolder.ID + ")\"><i class=\"icon-lock\"></i>&nbsp;" + GMGColor.Resources.Resources.lblApplyFolderPermissions + "</a></a></li>" : string.Empty) +
                                              (canMakeFileTransfer ?
                                               "<li><a href=\"javascript:void(0);\" title=\"" + GMGColor.Resources.Resources.lblNewFolder + "\" class=\"folder-action\" action=\"TransferFolder\" folder=\"" + objFolder.ID + "\" private=\"false\"><i class=\"icon-folder-close\"></i> " + GMGColor.Resources.Resources.lblTransferFolder
                                               + "</a></li>" : string.Empty);

                            if (objFolder.Creator == UserID)
                            {
                                strActions += "<li class=\"divider\"></li>" +
                                                "<li><a href=\"javascript:void(0);\" title=\"" + GMGColor.Resources.Resources.lblDelete + "\" class=\"folder-action\" action=\"" + (PermanentDelete ? "DeleteFolderPermanent" : "DeleteFolder") + "\" folder=\"" + objFolder.ID.ToString() + "\"><i class=\"icon-trash\"></i> " + GMGColor.Resources.Resources.lblDelete + "</a></li>";
                            }
                            strActions += "</ul></div>";
                        }
                        else
                        {
                            bool PermanentDelete = ((existingFolders == null) || !(existingFolders.Contains((int)objFolder.ID)));
                            strAccess = "widget-type=\"folder\" widget-id=\"" + objFolder.ID + "\" widget-creator=\"" + objFolder.Creator + "\" widget-users=\"\" widget-groups=\"\"";
                            strActions += ((objFolder.Creator == UserID) && PermanentDelete) ? " delete" : string.Empty;
                        }
                    }
                    else if (UserRole == Role.RoleName.AccountContributor)
                    {
                        if (NeedActionsTask)
                        {
                            strAccess = "widget-type=\"folder\" widget-id=\"" + objFolder.ID + "\" widget-creator=\"" +
                                        objFolder.Creator + "\" widget-users=\"\" widget-groups=\"\"";
                            strActions = "<div class=\"task btn-group\">" +
                                         "<button class=\"btn btn-mini dropdown-toggle\" data-toggle=\"dropdown\" type=\"button\"><span class=\"caret\"></span></button>" +
                                         "<ul class=\"dropdown-menu pull-right\">" +
                                         (objFolder.Creator == UserID ? "<li><a href=\"javascript:void(0);\" widget-toggle=\"foldertree\" widget-folders=\"\" widget-folder=\"" +
                                         objFolder.ID +
                                         "\" onclick=\"LoadFoldersTreePopup('/Widget/LoadFolderTree?showPanel=false&moveFiles=false', this)\" title=\"" +
                                         GMGColor.Resources.Resources.lblMoveFolder +
                                         "\"> <img alt=\"color_swatch\" src=\"../../Content/img/move-to-folder.png\" class=\"img-menu-item\" style=\"padding-right: 2px!important;\" />" +
                                         GMGColor.Resources.Resources.lblMoveFolder + "</a></li>" : string.Empty) +
                                          "<li><a href=\"javascript:void(0);\"  onclick=\"openReportFilterPopup(this, true)\" title=\"" + GMGColor.Resources.Resources.lblAnnotationsReport + "\" folder=\"" + objFolder.ID + "\"><i class=\"icon-list-alt\"></i> " + GMGColor.Resources.Resources.lblAnnotationsReport + "</a></li>" + "</ul></div>";
                        }
                    }

                    string cssActive = (ActiveFolder == objFolder.ID) ? " active" : string.Empty;
                    string cssToggler = (subfolderIDs.Count > 0) ? "toggler" : "empty";
                    string cssViewer = UserRole == Role.RoleName.AccountViewer
                        ? "help-inline "
                        : string.Empty;

                    var sharedIcon = UserID != objFolder.Creator && !objFolder.Collaborators.Contains("1") ? "<img class=\"approval-icon shared-folder-icon\" src=\"/Content/img/globe.png\">" : string.Empty;
                    folderTemplate.Append("<li class=\"block level" + objFolder.Level + "\" widget-id=\"");
                    folderTemplate.Append(objFolder.ID);
                    folderTemplate.Append("\">");
                    folderTemplate.Append("<div class=\"block-content ");
                    folderTemplate.Append(cssViewer);
                    folderTemplate.Append(cssActive);
                    folderTemplate.Append("\">");
                    folderTemplate.Append("<i class=\"");
                    folderTemplate.Append(cssToggler);
                    folderTemplate.Append("\"></i>");
                    folderTemplate.Append("<div class=\"title\">");
                    folderTemplate.Append("<div rel=\"tooltip\" data-toggle=\"tooltip\" data-trigger=\"hover\" data-placement=\"bottom\" data-html=\"true\" data-title=\"" + HttpUtility.HtmlEncode(objFolder.Name) + "\" class=\"item-group");
                    folderTemplate.Append(cssActive);
                    folderTemplate.Append("\">");
                    folderTemplate.Append((NeedActionsTask ? string.Empty : "<i class=\"icon-folder-close\"></i>"));
                    folderTemplate.Append((NeedActionsTask
                        ? ("<a class=\"action\" href=\"/Approvals/Populate?topLinkID=" + objFolder.ID +
                           "\" >" + sharedIcon + "<i class=\"icon-folder-close\"></i>" + HttpUtility.HtmlEncode(objFolder.Name) + "</a></div>" + strActions)
                        : ("<a class=\"action" + strActions + "\" href=\"javascript:void(0);\" " + strAccess + ">" +
                           HttpUtility.HtmlEncode(objFolder.Name) + "</a></div>")));
                    folderTemplate.Append("</div>");

                    if (subfolderIDs.Count > 0)
                    {
                        folderTemplate.Append("<ul>");
                        folderTemplate.Append(PopulateFolders(false, subfolderIDs, lstFolders, ActiveFolder, UserID, NeedActionsTask, UserRole, existingFolders, inheritParentFolderPermissions));
                        folderTemplate.Append("</ul>");
                        folderIDs = folderIDs.Except(subfolderIDs).ToList();
                    }
                    folderTemplate.Append("</div></li>");
                }
            }

            return folderTemplate.ToString();
        }

        private static List<int> FilterFolders(int ID, List<int> folderIDs, List<ReturnFolderTreeView> lstFolders)
        {
            var subfolderIDs = lstFolders.Where(o => o.Parent == ID).Select(m => m.ID).ToList();
            foreach (int folder in subfolderIDs)
            {
                folderIDs.Add(folder);
                folderIDs.AddRange(FilterFolders(folder, (new List<int>()), lstFolders));
            }
            return folderIDs;
        }
    }
}
