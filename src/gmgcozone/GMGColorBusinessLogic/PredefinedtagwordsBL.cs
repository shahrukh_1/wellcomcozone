﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.PredefinedTagwords;
using GMGColorNotificationService;
using System.IO;

namespace GMGColorBusinessLogic
{
   public class PredefinedtagwordsBL
    {
        public static int AddNewPredefinedtagword(string newTagword, int accountId, int userId, DbContextBL context, TypeOfTagword tagwordType )
        {
            try
            {
                var predefinedtagwords = new PredefinedTagword();
                bool IsDuplicate = context.PredefinedTagwords.Where(x => x.Account == accountId && x.Name == newTagword).Any();
                if(newTagword != "" && !IsDuplicate)
                {
                    predefinedtagwords.Account = accountId;
                    predefinedtagwords.CreatedBy = userId;
                    predefinedtagwords.Name = newTagword;
                    predefinedtagwords.CreatedDate = DateTime.UtcNow;
                    predefinedtagwords.Guid = Guid.NewGuid().ToString();
                    predefinedtagwords.TagwordType = (int)tagwordType;

                    context.PredefinedTagwords.Add(predefinedtagwords);
                    context.SaveChanges();
                }
                return predefinedtagwords.ID;
               
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddNewPredefinedTagword, ex);
            }
        }

        public static bool CheckIfAnyActiveApprovalForTagword(int tagwordID, int loggedAccountID,DbContextBL context)
        {
            bool approvalExists = (from pt in context.PredefinedTagwords
                                   join aat in context.ApprovalTagwords on pt.ID equals aat.PredefinedTagwords
                                   join a in context.Approvals on aat.Approval equals a.ID
                                   where pt.ID == tagwordID && !a.IsDeleted && a.ErrorMessage == null
                                   select a.ID).Any();
            if (approvalExists)
            {
                return false;
            } else
            {
                var approvalTagwordsToDelete = (from at in context.ApprovalTagwords where at.PredefinedTagwords == tagwordID select at).ToList();
                context.ApprovalTagwords.RemoveRange(approvalTagwordsToDelete);
                context.SaveChanges();
                return true;
            }
        }
        public static void AddPredefinedTagwordsFromFile(string destinationPath, int userId, int accountId)
        {
            using (DbContextBL context = new DbContextBL())
            {
                using (var reader = new StreamReader(destinationPath))
                {
                    List<PredefinedTagword> tagwords = new List<PredefinedTagword>();
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        var name = values[1].Replace("\"", "");

                        bool IsDuplicateFromDB = context.PredefinedTagwords.Where(x => x.Account == accountId && x.Name == name).Any();
                        bool IsDuplicateFromFile = tagwords.Where(x => x.Name == name).Any();
                        if (!IsDuplicateFromDB && !IsDuplicateFromFile)
                        {
                            PredefinedTagword tagword = new PredefinedTagword();
                            tagword.Account = accountId;
                            tagword.CreatedBy = userId;
                            tagword.Name = name;
                            tagword.CreatedDate = DateTime.UtcNow;
                            tagword.Guid = Guid.NewGuid().ToString();
                            tagword.TagwordType = (int)TypeOfTagword.PredefinedTagword;
                            tagwords.Add(tagword);
                        }

                    }
                    context.PredefinedTagwords.AddRange(tagwords);
                    context.SaveChanges();
                }
            }
        }
        public static List<PredefinedTagwords> GetPredefineTagwords(int loggedAccountId, DbContextBL context, string searchText)
        {
            bool searchtagword = !String.IsNullOrEmpty(searchText);
            return (from ptw in context.PredefinedTagwords
                    where ptw.Account == loggedAccountId && (!searchtagword || ptw.Name.Contains(searchText))
                    select new PredefinedTagwords
                    {
                        Id = ptw.ID,
                        tagword = ptw.Name
                    }).ToList();
        }

        public static NewApprovalModel UploadTagwordCSVfile(DbContextBL context)
        {
            NewApprovalModel model = new NewApprovalModel();
            model.objLoggedAccount = context.Accounts.Where(t => t.ID == 2189).FirstOrDefault();
            model.AcceptedFiles = "/(csv)$/i";
            return model;
        }

        public static MemoryStream CreatePredefineTagwordsReportCSV(int loggedAccountId, out string csvFileName, DbContextBL context)
        {
            try
            {
                List<PredefinedTagwords> csvModel = GetPredefineTagwords(loggedAccountId, context, string.Empty);

                csvFileName = GMGColor.Resources.Resources.navPredefinedtagwords;
                csvFileName += ".xls";

                MemoryStream csvStream = new MemoryStream();
                StreamWriter writer = new StreamWriter(csvStream, System.Text.Encoding.UTF8);

                string csvHeaderRow =
                    
                          "\"" + GMGColor.Resources.Resources.lblId
                        + "\",\"" + GMGColor.Resources.Resources.lblTagword;

                csvHeaderRow += "\"";
                writer.WriteLine(csvHeaderRow);
               
                    foreach (var tagwordModel in csvModel)
                    {
                    string currentTagwordRow =

                      "\"" + tagwordModel.Id
                    + "\",\"" + tagwordModel.tagword;

                    currentTagwordRow += "\"";
                        writer.WriteLine(currentTagwordRow);
                    }

                writer.Flush();
                return csvStream;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateTagwordReportCSV, ex);
            }
        }

        public static ApprovalsFilterModel GetPredefineTagwordsWithCount(int loggedAccountId, int loggedUserId, DbContextBL context)
        {
            var model = new ApprovalsFilterModel();
            var approvalCounts = (from atc in context.ApprovalTypeColours
                     let pageCount = (from app in context.ApprovalTypeColours
                                      join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                      join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                      join j in context.Jobs on aprl.Job equals j.ID
                                      join js in context.JobStatus on j.Status equals js.ID
                                      where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                      aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                      ((j.JobOwner == loggedUserId) || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase))
                                      select aprl.ID).Distinct().Count()
                     select new Approval.ApprovalTypeCount
                     {
                         Name = atc.Name,
                         ApprovalCount = pageCount
                     }).ToList();

            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
          
            model.tagwords = (from ptw in context.PredefinedTagwords
                              let count = (from at in context.ApprovalTagwords
                                           join a in context.Approvals on at.Approval equals a.ID
                                           where at.PredefinedTagwords == ptw.ID && a.IsDeleted == false
                                           select at.ID).Count()
                              where ptw.Account == loggedAccountId
                    select new PredefinedTagwordsWithCount
                    {
                        Id = ptw.ID,
                        tagword = ptw.Name,
                        count = count
                    }).ToList();
            return model;
        }

        public static ApprovalsTypeCount GetApprovalsCountForSelectedTagwords(int loggedAccountId, int loggedUserId, List<string> selectedTagwords, int tabId, bool ViewAll, DbContextBL context)
        {
            var model = new ApprovalsTypeCount();
            switch (tabId)
            {

                #region All Approvals
                default:
                    if (ViewAll)
                    {
                        if (selectedTagwords.Count > 0)
                        {
                            var approvalCounts = (from atc in context.ApprovalTypeColours
                                                  let approval_Count = (from pt in context.PredefinedTagwords
                                                                        join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                        join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                        join j in context.Jobs on aprl.Job equals j.ID
                                                                        join js in context.JobStatus on j.Status equals js.ID
                                                                        where aprl.IsDeleted == false && aprl.DeletePermanently == false && j.Account == loggedAccountId && js.Key != "ARC" 
                                                                        && aprl.ApprovalTypeColour == atc.ID && selectedTagwords.Contains(pt.ID.ToString())
                                                                        select j.ID).Distinct().Count()
                                                  select new Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                        }
                        else
                        {
                            var approvalCounts = (from atc in context.ApprovalTypeColours
                             let approval_Count = (from aprl in context.Approvals 
                                                  join j in context.Jobs on aprl.Job equals j.ID
                                                  join js in context.JobStatus on j.Status equals js.ID
                                                  where aprl.IsDeleted == false && aprl.DeletePermanently == false && j.Account == loggedAccountId && js.Key != "ARC"
                                                  && aprl.ApprovalTypeColour == atc.ID
                                                  select j.ID).Distinct().Count()
                                                  select new Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                        }
                    }
                   else
                   { 
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
            }
                    break;


                #endregion


                #region My Open Approvals
                case -7:
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join acd in context.ApprovalCollaboratorDecisions on aprl.ID equals acd.Approval
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && js.Key != "COM" && acd.Decision == null &&
                                                                    selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    join acd in context.ApprovalCollaboratorDecisions on aprl.ID equals acd.Approval
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && js.Key != "COM" &&
                                                                    acd.Decision == null && ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }

                    break;

                #endregion

                #region Owned by me
                case -2:
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                      ((j.JobOwner == loggedUserId || aprl.Owner == loggedUserId))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    (j.JobOwner == loggedUserId || aprl.Owner == loggedUserId)
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }

                    break;
                #endregion

                #region Shared With Me
                case -3:

                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    ((j.JobOwner != loggedUserId && aprl.Owner != loggedUserId && (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    ((j.JobOwner != loggedUserId && aprl.Owner != loggedUserId && (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    break;

                #endregion

                #region Recently viewed
                case -4:
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    join auvi in context.ApprovalUserViewInfoes on aprl.ID equals auvi.Approval
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    (auvi.User == loggedUserId
                                                                    )
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    join auvi in context.ApprovalUserViewInfoes on aprl.ID equals auvi.Approval
                                                                    where aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    (auvi.User == loggedUserId && auvi.Approval == aprl.ID)
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    break;
                    #endregion
            }

            return model;


        }

        public static ApprovalsTypeCount GetProjectFolderApprovalsCountForSelectedTagwords(int ProjectFolderID, int loggedAccountId, int loggedUserId, List<string> selectedTagwords, int tabId, bool ViewAll, DbContextBL context)
        {
            var model = new ApprovalsTypeCount();
            switch (tabId)
            {

                #region All Approvals
                default:
                    if (ViewAll)
                    {
                        if (selectedTagwords.Count > 0)
                        {
                            var approvalCounts = (from atc in context.ApprovalTypeColours
                                                  let approval_Count = (from pt in context.PredefinedTagwords
                                                                        join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                        join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                        join j in context.Jobs on aprl.Job equals j.ID
                                                                        join js in context.JobStatus on j.Status equals js.ID
                                                                        where j.ProjectFolder == ProjectFolderID && aprl.IsDeleted == false && aprl.DeletePermanently == false && j.Account == loggedAccountId && js.Key != "ARC"
                                                                        && aprl.ApprovalTypeColour == atc.ID && selectedTagwords.Contains(pt.ID.ToString())
                                                                        select j.ID).Distinct().Count()
                                                  select new Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                        }
                        else
                        {
                            var approvalCounts = (from atc in context.ApprovalTypeColours
                                                  let approval_Count = (from aprl in context.Approvals
                                                                        join j in context.Jobs on aprl.Job equals j.ID
                                                                        join js in context.JobStatus on j.Status equals js.ID
                                                                        where j.ProjectFolder == ProjectFolderID && aprl.IsDeleted == false && aprl.DeletePermanently == false && j.Account == loggedAccountId && js.Key != "ARC"
                                                                        && aprl.ApprovalTypeColour == atc.ID
                                                                        select j.ID).Distinct().Count()
                                                  select new Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                        }
                    }
                    else
                    {
                        if (selectedTagwords.Count > 0)
                        {

                            var approvalCounts = (from atc in context.ApprovalTypeColours
                                                  let approval_Count = (from pt in context.PredefinedTagwords
                                                                        join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                        join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                        join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                        join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                        join j in context.Jobs on aprl.Job equals j.ID
                                                                        join js in context.JobStatus on j.Status equals js.ID
                                                                        where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                        aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                        ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                        select j.ID).Distinct().Count()
                                                  select new Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                        }
                        else
                        {
                            var approvalCounts = (from atc in context.ApprovalTypeColours
                                                  let approval_Count = (from app in context.ApprovalTypeColours
                                                                        join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                        join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                        join j in context.Jobs on aprl.Job equals j.ID
                                                                        join js in context.JobStatus on j.Status equals js.ID
                                                                        where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                        aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                        ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                        select j.ID).Distinct().Count()
                                                  select new Approval.ApprovalTypeCount
                                                  {
                                                      Name = atc.Name,
                                                      ApprovalCount = approval_Count
                                                  }).ToList();

                            model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                            model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                        }
                    }
                    break;


                #endregion


                #region My Open Approvals
                case -7:
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join acd in context.ApprovalCollaboratorDecisions on aprl.ID equals acd.Approval
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && js.Key != "COM" && acd.Decision == null &&
                                                                    selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    join acd in context.ApprovalCollaboratorDecisions on aprl.ID equals acd.Approval
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && js.Key != "COM" &&
                                                                    acd.Decision == null && ((j.JobOwner == loggedUserId || (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }

                    break;

                #endregion

                #region Owned by me
                case -2:
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                      ((j.JobOwner == loggedUserId || aprl.Owner == loggedUserId))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    (j.JobOwner == loggedUserId || aprl.Owner == loggedUserId)
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }

                    break;
                #endregion

                #region Shared With Me
                case -3:

                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    ((j.JobOwner != loggedUserId && aprl.Owner != loggedUserId && (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {
                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from app in context.ApprovalTypeColours
                                                                    join aprl in context.Approvals on app.ID equals aprl.ApprovalTypeColour
                                                                    join ac in context.ApprovalCollaborators on aprl.ID equals ac.Approval
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    ((j.JobOwner != loggedUserId && aprl.Owner != loggedUserId && (ac.Collaborator == loggedUserId && aprl.CurrentPhase == ac.Phase)))
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    break;

                #endregion

                #region Recently viewed
                case -4:
                    if (selectedTagwords.Count > 0)
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    join auvi in context.ApprovalUserViewInfoes on aprl.ID equals auvi.Approval
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" && selectedTagwords.Contains(pt.ID.ToString()) &&
                                                                    (auvi.User == loggedUserId
                                                                    )
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    else
                    {

                        var approvalCounts = (from atc in context.ApprovalTypeColours
                                              let approval_Count = (from pt in context.PredefinedTagwords
                                                                    join at in context.ApprovalTagwords on pt.ID equals at.PredefinedTagwords
                                                                    join aprl in context.Approvals on at.Approval equals aprl.ID
                                                                    join app in context.ApprovalTypeColours on aprl.ApprovalTypeColour equals app.ID
                                                                    join j in context.Jobs on aprl.Job equals j.ID
                                                                    join js in context.JobStatus on j.Status equals js.ID
                                                                    join auvi in context.ApprovalUserViewInfoes on aprl.ID equals auvi.Approval
                                                                    where j.ProjectFolder == ProjectFolderID && aprl.ApprovalTypeColour == atc.ID && j.Account == loggedAccountId &&
                                                                    aprl.IsDeleted == false && aprl.DeletePermanently == false && js.Key != "ARC" &&
                                                                    (auvi.User == loggedUserId && auvi.Approval == aprl.ID)
                                                                    select j.ID).Distinct().Count()
                                              select new Approval.ApprovalTypeCount
                                              {
                                                  Name = atc.Name,
                                                  ApprovalCount = approval_Count
                                              }).ToList();

                        model.NoOfImageApprovals = approvalCounts.Where(a => a.Name == "Image").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfDocumentApprovals = approvalCounts.Where(a => a.Name == "Document").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfVideoApprovals = approvalCounts.Where(a => a.Name == "Video").Select(a => a.ApprovalCount).FirstOrDefault();
                        model.NoOfHTMLApprovals = approvalCounts.Where(a => a.Name == "HTML").Select(a => a.ApprovalCount).FirstOrDefault();
                    }
                    break;
                    #endregion
            }

            return model;


        }

        public static List<PredefinedTagwordsWithCount> GetTagwordsForselectedApprovalTypes(int loggedAccountId, List<string> selectedTypes, string searchText, DbContextBL context)
        {
            if (searchText != "")
            {
                return (from ptw in context.PredefinedTagwords
                        let count = (from at in context.ApprovalTagwords
                                     join a in context.Approvals on at.Approval equals a.ID
                                     where at.PredefinedTagwords == ptw.ID && a.IsDeleted == false
                                     select at.ID).Count()
                        where ptw.Account == loggedAccountId && ptw.Name.Contains(searchText)
                        select new PredefinedTagwordsWithCount
                        {
                            Id = ptw.ID,
                            tagword = ptw.Name,
                            count = count
                        }).ToList();
            }
            else
            {
                if (selectedTypes.Count > 0)
                {
                    return (from ptw in context.PredefinedTagwords
                            let count = (from at in context.ApprovalTagwords
                                         join a in context.Approvals on at.Approval equals a.ID
                                         join atc in context.ApprovalTypeColours on a.ApprovalTypeColour equals atc.ID
                                         where at.PredefinedTagwords == ptw.ID && a.IsDeleted == false && selectedTypes.Contains(atc.Name)
                                         select at.ID).Count()
                            where ptw.Account == loggedAccountId && count > 0
                            select new PredefinedTagwordsWithCount
                            {
                                Id = ptw.ID,
                                tagword = ptw.Name,
                                count = count
                            }).ToList();
                }
                else
                {
                    return (from ptw in context.PredefinedTagwords
                            let count = (from at in context.ApprovalTagwords
                                         join a in context.Approvals on at.Approval equals a.ID
                                         where at.PredefinedTagwords == ptw.ID && a.IsDeleted == false
                                         select at.ID).Count()
                            where ptw.Account == loggedAccountId
                            select new PredefinedTagwordsWithCount
                            {
                                Id = ptw.ID,
                                tagword = ptw.Name,
                                count = count
                            }).ToList();
                }
            }
        }

        public static List<PredefinedTagwordsWithCount> GetTagwordsForselectedProjectApprovalTypes(int ProjectID, int loggedAccountId, List<string> selectedTypes, string searchText, DbContextBL context)
        {
            if (searchText != "")
            {
                return (from ptw in context.PredefinedTagwords
                        join pat in context.ApprovalTagwords on ptw.ID equals pat.PredefinedTagwords
                        join pa in context.Approvals on pat.Approval equals pa.ID
                        join pj in context.Jobs on pa.Job equals pj.ID
                        let count = (from at in context.ApprovalTagwords
                                     join a in context.Approvals on at.Approval equals a.ID
                                     join j in context.Jobs on a.Job equals j.ID
                                     where j.ProjectFolder == ProjectID &&  at.PredefinedTagwords == ptw.ID && a.IsDeleted == false
                                     select at.ID).Count()
                        where pj.ProjectFolder == ProjectID &&  ptw.Account == loggedAccountId && ptw.Name.Contains(searchText)
                        select new PredefinedTagwordsWithCount
                        {
                            Id = ptw.ID,
                            tagword = ptw.Name,
                            count = count
                        }).ToList();
            }
            else
            {
                if (selectedTypes.Count > 0)
                {
                    return (from ptw in context.PredefinedTagwords
                            join pat in context.ApprovalTagwords on ptw.ID equals pat.PredefinedTagwords
                            join pa in context.Approvals on pat.Approval equals pa.ID
                            join pj in context.Jobs on pa.Job equals pj.ID
                            let count = (from at in context.ApprovalTagwords
                                         join a in context.Approvals on at.Approval equals a.ID
                                         join j in context.Jobs on a.Job equals j.ID
                                         join atc in context.ApprovalTypeColours on a.ApprovalTypeColour equals atc.ID
                                         where j.ProjectFolder == ProjectID && at.PredefinedTagwords == ptw.ID && a.IsDeleted == false && selectedTypes.Contains(atc.Name)
                                         select at.ID).Count()
                            where pj.ProjectFolder == ProjectID && ptw.Account == loggedAccountId && count > 0
                            select new PredefinedTagwordsWithCount
                            {
                                Id = ptw.ID,
                                tagword = ptw.Name,
                                count = count
                            }).ToList();
                }
                else
                {
                    return (from ptw in context.PredefinedTagwords
                            join pat in context.ApprovalTagwords on ptw.ID equals pat.PredefinedTagwords
                            join pa in context.Approvals on pat.Approval equals pa.ID
                            join pj in context.Jobs on pa.Job equals pj.ID
                            let count = (from at in context.ApprovalTagwords
                                         join a in context.Approvals on at.Approval equals a.ID
                                         join j in context.Jobs on a.Job equals j.ID
                                         where j.ProjectFolder == ProjectID && at.PredefinedTagwords == ptw.ID && a.IsDeleted == false
                                         select at.ID).Count()
                            where pj.ProjectFolder == ProjectID && ptw.Account == loggedAccountId
                            select new PredefinedTagwordsWithCount
                            {
                                Id = ptw.ID,
                                tagword = ptw.Name,
                                count = count
                            }).ToList();
                }
            }
        }

        public static List<PredefinedTagwordsWithCount> GetTagwordsForProjectFolder(int loggedAccountId, DbContextBL context)
        {
            var a1 = (from ptw in context.PredefinedTagwords
                     join  pfs in context.ProjectFolderTagwords on ptw.ID equals pfs.PredefinedTagwords
                     let count = (from pf in context.ProjectFolders
                                  join pft in context.ProjectFolderTagwords on pf.ID equals pft.ProjectFolder
                                  join pdt in context.PredefinedTagwords on pft.PredefinedTagwords equals pdt.ID
                                    where pf.IsDeleted == false
                                    select pf.ID).Count()
                    where ptw.Account == loggedAccountId && ptw.ID == pfs.PredefinedTagwords
                     select new PredefinedTagwordsWithCount
                    {
                        Id = ptw.ID,
                        tagword = ptw.Name,
                        count = count
                    }).ToList();


            // var b = (from ptw in context.PredefinedTagwords
            //          let count = (from at in context.ProjectFolderTagwords
            //                       join a in context.ProjectFolders on at.ProjectFolder equals a.ID
            //                       where at.PredefinedTagwords == ptw.ID && a.IsDeleted == false
            //                       select at.ID).Count()
            //          where ptw.Account == loggedAccountId
            //          select new PredefinedTagwordsWithCount
            //          {
            //              Id = ptw.ID,
            //              tagword = ptw.Name,
            //              count = count
            //          }).ToList();


            return a1;   
        }
        public static List<PredefinedTagwords> GetPredefineTagwords(int loggedAccountId, DbContextBL context)
        {

            return (from ptw in context.PredefinedTagwords
                    where ptw.Account == loggedAccountId
                    select new PredefinedTagwords
                    {
                        Id = ptw.ID,
                        tagword = ptw.Name
                    }).ToList();
        }
    }
}
