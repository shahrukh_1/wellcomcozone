﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class WorkstationStatusBL
    {
        public enum WorkstationStatusEnum
        {
            Active = 1,
            Inactive = 2
        }

        internal static int GetWorkstationStatusIdByEnum(WorkstationStatusEnum status, GMGColorContext context)
        {
            try
            {
                int workstationStatusId = (from ws in context.WorkstationStatus where ws.Key == (int) status select ws.ID).FirstOrDefault();
                if (workstationStatusId == 0)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotGetWorkstationStatusIdByEnum);
                }
                return workstationStatusId;
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.Error(ExceptionBLMessages.CouldNotGetWorkstationStatusIdByEnum, ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetWorkstationStatusIdByEnum);
            }
        }
    }
}
