﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading;
using GMGColor.Resources;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorNotificationService;

namespace GMGColorBusinessLogic
{
    public class PlansBL
    {
        #region Constants

        public const int WarningLimit = 90;

        #endregion

        #region Static Methods

        /// <summary>
        /// Deletes Specified Billing Plan from Database
        /// </summary>
        /// <param name="planID"></param>
        /// <param name="context"></param>
        public static void DeleteBillingPlan(int planID, DbContextBL context)
        {
            try
            {
                GMGColorDAL.BillingPlan objBillingPlan = (from bp in context.BillingPlans
                                                          where bp.ID == planID
                                                          select bp).FirstOrDefault();

                if (objBillingPlan != null)
                {
                    context.AttachedAccountBillingPlans.RemoveRange(context.AttachedAccountBillingPlans.Where(aabp => aabp.BillingPlan == objBillingPlan.ID));
                    context.AccountPricePlanChangedMessages.RemoveRange(objBillingPlan.ChangedBillingPlans.SelectMany(t => t.AccountPricePlanChangedMessages));
                    context.ChangedBillingPlans.RemoveRange(objBillingPlan.ChangedBillingPlans);
                    context.AccountBillingPlanChangeLogs.RemoveRange(objBillingPlan.AccountBillingPlanChangeLogs);
                    context.BillingPlans.Remove(objBillingPlan);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteBillingPlan, ex);
            }
        }

        /// <summary>
        /// create notifications for demo plans that are older than 30 days
        /// </summary>
        public static void CheckDemoPlans()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    List<DemoPlan> planList = GetExpiredDemoPlans(context);

                    if (planList.Count > 0)
                    {
                        foreach (var plan in planList)
                        {
                            string moduleName = string.Empty;
                            switch (plan.ModuleKey)
                            {                               
                                case (int) GMG.CoZone.Common.AppModule.Deliver:
                                    {
                                        moduleName = GMGColor.Resources.Resources.lblDeliverModuleName;

                                        List<NotificationItem> notifToBeDeleted = GetDeliverNotificationsForExpiredModule(plan.Account, context);
                                        DeleteNotificationItems(notifToBeDeleted, context);

                                        break;
                                    }
                                case (int)GMG.CoZone.Common.AppModule.Collaborate:
                                    {
                                        moduleName = GMGColor.Resources.Resources.lblCollaborateModuleName;

                                        List<NotificationItem> notifToBeDeleted = GetCollaborateNotificationsForExpiredModule(plan.Account, context);
                                        DeleteNotificationItems(notifToBeDeleted, context);

                                        break;
                                    }                               
                            }

                            NotificationServiceBL.CreateNotification(new DemoPlanExpired()
                            {
                                CreatedDate = DateTime.UtcNow,
                                InternalRecipient = plan.AccountOwner,
                                AppModule = moduleName
                            },
                            null,
                            context);
                        }
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("NotificationExpiredDemoPlans.CheckExpiredDemoPlans() - Error occurred, Message: {0}", ex);
            }
        }
        
        /// <summary>
        /// Returns a list with account owner and application module name for expired plans
        /// </summary>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static List<DemoPlan> GetExpiredDemoPlans(DbContextBL context)
        {
            return (from asp in context.AccountSubscriptionPlans
                    join a in context.Accounts on asp.Account equals a.ID
                    join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(asp.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) == GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS)
                    select new DemoPlan()
                    {
                        AccountOwner = a.Owner.Value,
                        ModuleKey = am.Key,
                        ID = asp.ID,
                        Account = a.ID
                    }).ToList().Union(from spi in context.SubscriberPlanInfoes
                                      join a in context.Accounts on spi.Account equals a.ID
                                      join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                                      join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                                      join am in context.AppModules on bpt.AppModule equals am.ID
                                      where bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(spi.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) == GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS)
                                      select new DemoPlan()
                                      {
                                          AccountOwner = a.Owner.Value,
                                          ModuleKey = am.Key,
                                          ID = spi.ID,
                                          Account = a.ID
                                      }).ToList();
        }

        /// <summary>
        /// Checks whether the user has reached the allowed proofs limit or not, for current module, and returns a warning message if is the case.
        /// Otherwise, returns empty message which means that user can upload jobs
        /// </summary>
        /// <param name="account">Account id</param>
        /// <param name="loggedUser">Logged user Id</param>
        /// <param name="appModule">Application module for which the proofs usage will be checked</param>
        /// <param name="uploadedJobsCount">The new job(s) uploaded</param>
        /// <param name="context">Database context</param>      
        /// <returns></returns>
        public static string CheckIfJobSubmissionAllowed(Account account, GMGColorDAL.User loggedUser, GMG.CoZone.Common.AppModule appModule, int uploadedJobsCount, DbContextBL context, BillingPlan billingPlan = null, decimal uploadedFileSizes = 0, bool isCollaborateApi = false)
        {
            try
            {                
                if (billingPlan == null)
                {
                    billingPlan = account.GetBillingPlanByAppModule(appModule, context); 
                }

                if (billingPlan !=null && !billingPlan.IsFixed)
                {
                    bool allowOverdraw;
                    SubscriberPlanInfo subscriberPlan = null;
                    AccountSubscriptionPlan subscriptionPlan = null;

                    if (account.AccountType == (int) AccountType.Type.Subscriber)
                    {
                        subscriberPlan = Account.GetSubscriberPlan(account.ID, appModule, context);
                        allowOverdraw = subscriberPlan.AllowOverdraw;                       
                    }
                    else
                    {
                        subscriptionPlan = Account.GetNonSubscriberPlan(account.ID, appModule, context);
                        allowOverdraw = subscriptionPlan.AllowOverdraw;
                    }

                    if (!allowOverdraw)
                    {
                        //determine if is collaborate new billing plan type
                        if (appModule == GMG.CoZone.Common.AppModule.Collaborate &&
                            IsNewCollaborateBillingPlan(billingPlan))
                        {
                            return CheckIfSubmissionAllowedForMaxStorage(account, loggedUser, billingPlan, uploadedFileSizes, context);
                        }
                        else
                        {
                            DateTime accountCreatedDate = GMGColorFormatData.GetUserTimeFromUTC(account.CreatedDate,
                            account.TimeZone);

                            int totalProofsCount;
                            int usedProofsCount;

                            if (account.AccountType == (int) AccountType.Type.Subscriber)
                            {
                                totalProofsCount = Account.GetProofsCount(billingPlan, subscriberPlan);
                                usedProofsCount = Account.GetUsedProofsCount(account.ID, appModule, accountCreatedDate,
                                    context, subscriberPlan.ContractStartDate, true);

                            }
                            else
                            {
                                totalProofsCount = Account.GetProofsCount(billingPlan);
                                usedProofsCount = Account.GetUsedProofsCount(account.ID, appModule, accountCreatedDate,
                                    context, subscriptionPlan.ContractStartDate,
                                    subscriptionPlan.IsMonthlyBillingFrequency);
                            }

                            if (totalProofsCount == 0 || (isCollaborateApi && usedProofsCount >= totalProofsCount))
                            {
                                return String.Format(GMGColor.Resources.Resources.lblProofsCountExceeded, appModule);
                            }

                            if (!isCollaborateApi)
                            {
                                var remainingProofsCount = (totalProofsCount - usedProofsCount);
                                int existingAndNewJobsCount = (usedProofsCount + uploadedJobsCount);
                                //existing added jobs + uploaded jobs
                                int existingAndNewJobsPercentage = ((existingAndNewJobsCount*100)/totalProofsCount);
                                // the percentage of the existing added jobs + uploaded jobs

                                //case when "Add job page" is opened and allowed proofs limit is reached
                                if (totalProofsCount <= existingAndNewJobsCount && uploadedJobsCount == 0 ||
                                    (totalProofsCount < existingAndNewJobsCount) && remainingProofsCount <= 0)
                                {
                                    return String.Format(GMGColor.Resources.Resources.lblProofsCountExceeded, appModule);
                                }

                                //case when user is allowed to add (x) more jobs, but he tries to add (x + n) jobs
                                // the second case is when add new job page is opened and remaining proofs count is less than 10
                                if ((totalProofsCount < existingAndNewJobsCount && remainingProofsCount > 0) ||
                                    (remainingProofsCount <= 10 && uploadedJobsCount == 0))
                                {
                                    return String.Format(GMGColor.Resources.Resources.lblAddExactNumberOfJobs, remainingProofsCount);
                                }

                                #region send notification email

                                //the PlanVolumeWarning will be sent only when existing jobs + uploaded jobs count is bigger or equal than 90% of total proofs count and smaller than 100%
                                //and used proofs count is equal or smaller than 90% of total proofs count
                                if ((existingAndNewJobsPercentage >= WarningLimit &&
                                     ((usedProofsCount*100)/totalProofsCount) < WarningLimit
                                     && existingAndNewJobsPercentage < 100 && uploadedJobsCount != 0
                                    //if count is diffrent from 0, this means that is submit action
                                     && remainingProofsCount > 0)
                                    ||
                                    ((totalProofsCount*10)/100) < 1 && uploadedJobsCount != 0 &&
                                    (remainingProofsCount - uploadedJobsCount) == 1)
                                    // second case is when total proofs allowed count is less than 10 and remaining jobs is 1
                                {
                                    NotificationServiceBL.CreateNotification(new PlanLimitWarning()
                                    {
                                        EventCreator = loggedUser != null ? loggedUser.ID : (int?) null,
                                        InternalRecipient = account.Owner,
                                        AppModule = appModule.ToString()
                                    },
                                        loggedUser,
                                        context
                                        );
                                }

                                //PlanLimitReached notification will be sent when "existing jobs count + uploaded jobs = total proofs count" and only on submit action
                                if (existingAndNewJobsCount == totalProofsCount && uploadedJobsCount != 0 &&
                                    remainingProofsCount > 0)
                                {
                                    NotificationServiceBL.CreateNotification(new PlanLimitReached()
                                    {
                                        EventCreator = loggedUser != null ? loggedUser.ID : (int?) null,
                                        InternalRecipient = account.Owner,
                                        AppModule = appModule.ToString()
                                    },
                                        loggedUser,
                                        context
                                        );
                                }

                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("IsJobSubmissionAllowed() - Error occurred, Message: {0}", ex);
            }
           
            return string.Empty;
        }

        /// <summary>
        /// Determine if the current plan is a new collaborate plan based on max storage/users
        /// </summary>
        /// <param name="billingPlan"></param>
        /// <returns></returns>
        public static bool IsNewCollaborateBillingPlan(BillingPlan billingPlan)
        {
            return billingPlan != null && billingPlan.MaxStorageInGb != null && billingPlan.MaxUsers != null;
        }

        /// <summary>
        /// Gets the active number of users for the specified module
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="appModule"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetActiveUsersNr(int accId, GMG.CoZone.Common.AppModule appModule, DbContextBL context)
        {
            return (from u in context.Users
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join apm in context.AppModules on r.AppModule equals apm.ID
                    where u.Account == accId && apm.Key == (int) appModule && r.Key != "NON" && us.Key != "D"
                    select u.ID).Count();
        }

        /// <summary>
        /// log file usage when adding a new approval
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="fileSizeInBytes"></param>
        /// <param name="context"></param>
        public static void LogApprovalSizeUsage(BillingPlan billingPlan, Account loggedAccount, decimal fileSizeInBytes, DbContextBL context)
        {
            if (billingPlan == null)
            {
                var appModule = GMG.CoZone.Common.AppModule.Collaborate;
                billingPlan = loggedAccount.GetBillingPlanByAppModule(appModule, context); 
            }

            if (billingPlan.MaxStorageInGb != null)
            {
                double gigaBytes = GMG.CoZone.Common.Utils.BytestoGB(fileSizeInBytes);

                var accountStorageUsage = (from acsu in context.AccountStorageUsages.Local
                                           where acsu.Account == loggedAccount.ID
                                           select acsu).FirstOrDefault() ?? (from acsu in context.AccountStorageUsages
                                                                             where acsu.Account == loggedAccount.ID
                                                                             select acsu).FirstOrDefault();

                if (accountStorageUsage == null)
                {
                    accountStorageUsage = new AccountStorageUsage { Account = loggedAccount.ID, UsedStorage = gigaBytes };

                    context.AccountStorageUsages.Add(accountStorageUsage);
                }
                else
                {
                    accountStorageUsage.UsedStorage += gigaBytes;
                }   
            }
        }

        #endregion

        #region Private


        private static string CheckIfSubmissionAllowedForMaxStorage(Account account, GMGColorDAL.User loggedUser, BillingPlan billingPlan, decimal uploadedFileSizes, DbContextBL context)
        {
            var maxAllowedStorage = billingPlan.MaxStorageInGb.Value;

            var usedStorage = (from asu in context.AccountStorageUsages
                                where asu.Account == account.ID
                                select asu.UsedStorage).FirstOrDefault();

            double uploadedFileSizeInGb = uploadedFileSizes > 0 ? GMG.CoZone.Common.Utils.BytestoGB(uploadedFileSizes) : 0;

            if (maxAllowedStorage == 0 || usedStorage + uploadedFileSizeInGb > maxAllowedStorage)
            {
                return GMGColor.Resources.Resources.lblStorageSizeExceeded;
            }
            else
            {
                //determine the current usage in the account
                var currentUsagePercent = 100*usedStorage/maxAllowedStorage;

                var remainingPercent = 100 - currentUsagePercent;

                //check if it is submit and need to send notification
                if (uploadedFileSizes > 0)
                {
                    //determine the new account usage taking into account the new uploaded files
                    var newUsagePercent = 100 * (usedStorage + uploadedFileSizeInGb) / maxAllowedStorage;

                    var newRemainingPercent = 100 - newUsagePercent;

                    if (newRemainingPercent <= 10)
                    {
                        //send notif
                        NotificationServiceBL.CreateNotification(new PlanLimitWarning
                        {
                            EventCreator = loggedUser != null ? loggedUser.ID : (int?)null,
                            InternalRecipient = account.Owner,
                            AppModule = GMG.CoZone.Common.AppModule.Collaborate.ToString(),
                            IsStorageWarning = true
                        },
                                        loggedUser,
                                        context
                                        );
                    }
                }
                else if (remainingPercent <= 10)
                {
                    //show limit warning in new approval page
                    return GMGColor.Resources.Resources.lblStorageSizeWarning;
                }
            }

            return String.Empty;
        }

        /// <summary>
        /// Delete notification items for expired module
        /// </summary>
        /// <param name="notifToBeDeleted"></param>
        /// <param name="context"></param>
        private static void DeleteNotificationItems(List<NotificationItem> notifToBeDeleted, DbContextBL context)
        {
            try
            {
                foreach (var notificationItem in notifToBeDeleted)
                {
                    context.NotificationItems.Remove(notificationItem);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("NotificationExpiredDemoPlans.DeleteNotificationItems() - Error occurred, Message: {0}", ex);
            }
        }

        /// <summary>
        /// Returns a list of notifications related with an expired collaborate module for a given account
        /// </summary>
        /// <param name="accId">Account id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        private static List<NotificationItem> GetCollaborateNotificationsForExpiredModule(int accId, DbContextBL context)
        {
            return (from ni in context.NotificationItems
                    join u in context.Users on ni.InternalRecipient equals u.ID
                    join a in context.Accounts on u.Account equals a.ID
                    join evt in context.EventTypes on ni.NotificationType equals evt.ID
                    join egr in context.EventGroups on evt.EventGroup equals egr.ID
                    where (egr.Key == "C" || egr.Key == "A") && a.ID == accId
                    select ni).ToList();
        }

        /// <summary>
        /// Returns a list of notifications related with an expired deliver module for a given account
        /// </summary>
        /// <param name="accId">Account id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        private static List<NotificationItem> GetDeliverNotificationsForExpiredModule(int accId, DbContextBL context)
        {
            return (from ni in context.NotificationItems
                    join u in context.Users on ni.InternalRecipient equals u.ID
                    join a in context.Accounts on u.Account equals a.ID
                    join evt in context.EventTypes on ni.NotificationType equals evt.ID
                    join egr in context.EventGroups on evt.EventGroup equals egr.ID
                    where egr.Key == "DL" && a.ID == accId
                    select ni).ToList();
        }        

        #endregion

        public static NewCollaborateBillingPlanViewModel PopulateNewCollaborateBillingPlan(object selectedBillingPlan, Account loggedAccount, AccountType.Type loggedAccountType, DbContextBL context) 
        {
            var planModel = new NewCollaborateBillingPlanViewModel();
            var accId = loggedAccountType == AccountType.Type.GMGColor ? null : (int?) loggedAccount.ID;

            planModel.BillingPlanTypes = (from bpt in context.BillingPlanTypes
                                         join apm in context.AppModules on bpt.AppModule equals apm.ID
                                         where bpt.Account == accId &&
                                               apm.Key == (int) GMG.CoZone.Common.AppModule.Collaborate
                                        select new BillingPlanTypeViewModel()
                                        {
                                            ID = bpt.ID,
                                            Name = bpt.Name,
                                            MediaService = bpt.MediaService,
                                            EnableMediaTools = bpt.EnableMediaTools,
                                            EnablePrePressTools = bpt.EnablePrePressTools
                                        }).ToList();
                         

            planModel.ProductType = GMGColor.Resources.Resources.lblCollaborateModuleName;
            planModel.Currency = loggedAccount.Currency.Symbol;

            if (selectedBillingPlan != null)
            {
                var billingPlanId = Convert.ToInt32(selectedBillingPlan);
                //get database info
                var dbBpInfo = (from bp in context.BillingPlans
                                where bp.ID == billingPlanId
                                select new 
                                { 
                                    Name = bp.Name,
                                    SelectedType = bp.Type,
                                    Price = bp.Price,
                                    IsFixed = bp.IsFixed,
                                    ColorProofConnections = bp.ColorProofConnections,
                                    IsDemo = bp.IsDemo,
                                    MaxUsers = bp.MaxUsers,
                                    MaxStorage = bp.MaxStorageInGb
                                }).FirstOrDefault();
                
                //populate object
                planModel.ID = billingPlanId;
                planModel.SelectedBilingPlanType = dbBpInfo.SelectedType;
                planModel.BillingPlanName = dbBpInfo.Name;
                planModel.isDemoPlan = dbBpInfo.IsDemo;
                planModel.isFixedPlan = dbBpInfo.IsFixed;
                planModel.MaxUsers = dbBpInfo.MaxUsers;
                planModel.MaxStorageInGb = dbBpInfo.MaxStorage;
                planModel.Price = dbBpInfo.Price;
                return planModel;
            }            
            return planModel;
        }
        
        public static Plans PopulateBillingPlan(object selectedBillingPlan, Account loggedAccount, AccountType.Type loggedAccountType, DbContextBL Context)
        {
            Plans plansModel = new Plans(loggedAccountType, loggedAccount, Context, false);

            if (selectedBillingPlan != null)
            {
                int billingPlan = int.Parse(selectedBillingPlan.ToString());
                if (billingPlan > 0)
                {
                    GMGColorDAL.BillingPlan objBillingPlan = DALUtils.GetObject<GMGColorDAL.BillingPlan>(billingPlan, Context);

                    plansModel.objBillingPlan = objBillingPlan;
                    plansModel.objBillingPlanType.AppModule = objBillingPlan.BillingPlanType.AppModule;
                }
            }
            return plansModel;
        }

        public static bool ProcessNewCollaborateBillingPlanForm(NewCollaborateBillingPlanViewModel planModel, Account loggedAccount, AccountType.Type accType, DbContextBL Context)
        {
            if (planModel.Price > GMGColorDAL.BillingPlan.MaxPrice)
            {
                return false;
            }
            try
            {
                using (System.Transactions.TransactionScope ts = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, new TimeSpan(0, 2, 0)))
                {
                    GMGColorDAL.BillingPlan objBillingPlan;
                    if (planModel.ID > 0)
                    {
                        objBillingPlan = DALUtils.GetObject<GMGColorDAL.BillingPlan>(planModel.ID, Context);

                        bool isPriceChanged = (objBillingPlan.Price != planModel.Price);
                        bool isCPConnectionsChanged = (objBillingPlan.ColorProofConnections != planModel.ColorProofConnections);

                        if (isPriceChanged || isCPConnectionsChanged)
                        {
                            UpdateBillingPlanChangedMessage(objBillingPlan.ID, null, loggedAccount ,Context);
                        }
                    }
                    {
                        objBillingPlan = new BillingPlan { Account = accType == AccountType.Type.GMGColor ? null : (int?)loggedAccount.ID };
                    }

                    objBillingPlan.Type = planModel.SelectedBilingPlanType;
                    objBillingPlan.Name = planModel.BillingPlanName;
                    objBillingPlan.Price = planModel.Price;
                    objBillingPlan.Proofs = 0;
                    objBillingPlan.IsFixed = planModel.isFixedPlan;
                    objBillingPlan.IsDemo = planModel.isDemoPlan;
              
                    if (planModel.isFixedPlan)
                    {
                        objBillingPlan.MaxUsers = 0;
                        objBillingPlan.MaxStorageInGb = 0;
                    }
                    else
                    {
                        objBillingPlan.MaxUsers = planModel.MaxUsers;
                        objBillingPlan.MaxStorageInGb = planModel.MaxStorageInGb;
                    }

                    if (planModel.ColorProofConnections != null)
                    {
                        objBillingPlan.ColorProofConnections = planModel.ColorProofConnections;
                    }

                    if (objBillingPlan.ID == 0)
                    {
                        Context.BillingPlans.Add(objBillingPlan);
                    }

                    Context.SaveChanges();
                    ts.Complete();
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, loggedAccount.ID,
                                                "PlansBL.SaveChanges.ProcessNewCollaborateBillingPlanForm : Saving Billing Plan Failed. {0}",
                                                ex.Message);
                throw ex;
            }
            return true;
        }

        private static void UpdateBillingPlanChangedMessage(int? billingPlan, int? attachedAccountBillingPlan, Account loggedAccount, DbContextBL Context)
        {
            var billingPlanValue = billingPlan.GetValueOrDefault();
            var attachedAccountBillingPlanValue = attachedAccountBillingPlan.GetValueOrDefault();
            GMGColorDAL.ChangedBillingPlan objCBP =
                         DALUtils.SearchObjects<GMGColorDAL.ChangedBillingPlan>(
                             o =>
                             o.Account == loggedAccount.ID &&
                             o.BillingPlan == billingPlanValue &&
                             attachedAccountBillingPlan == attachedAccountBillingPlanValue, Context).SingleOrDefault();

            GMGColorDAL.ChangedBillingPlan objChangedBillingPlan = new GMGColorDAL.ChangedBillingPlan();

            // TODO - check if switch is needed and refactor
            if (objCBP == null && loggedAccount.GetChilds(Context).Count > 0)
            {
                bool isHQ = (GMGColorDAL.AccountType.GetAccountType(loggedAccount, Context) == GMGColorDAL.AccountType.Type.GMGColor);

                objChangedBillingPlan.Account = loggedAccount.ID;
                objChangedBillingPlan.BillingPlan = isHQ ? billingPlan.Value : (int?)null;
                objChangedBillingPlan.AttachedAccountBillingPlan = !isHQ ? attachedAccountBillingPlan.Value : (int?)null;
                Context.ChangedBillingPlans.Add(objChangedBillingPlan);

                if (billingPlan.HasValue)
                {
                    foreach (GMGColorDAL.Account childAccount in loggedAccount.GetChilds(Context).ToList())
                    {
                        // add messages only for child accounts that has the changed billing plan attached
                        if (childAccount.AttachedAccountBillingPlans.Any(a => a.BillingPlan1.ID == billingPlan.Value))
                        {
                            GMGColorDAL.AccountPricePlanChangedMessage objAPPCM = new GMGColorDAL.AccountPricePlanChangedMessage();
                            objAPPCM.ChildAccount = childAccount.ID;
                            objChangedBillingPlan.AccountPricePlanChangedMessages.Add(objAPPCM);
                            Context.AccountPricePlanChangedMessages.Add(objAPPCM);
                        }
                    }
                }
            }
            else if (objCBP != null)
            {
                objChangedBillingPlan = DALUtils.GetObject<GMGColorDAL.ChangedBillingPlan>(objCBP.ID, Context);

                foreach (GMGColorDAL.Account childAccount in objChangedBillingPlan.Account1.GetChilds(Context).Where(o => o.ID != GMGColorDAL.AccountType.GetAccountType(GMGColorDAL.AccountType.Type.Subscriber, Context).ID).ToList())
                {
                    // add messages only for child accounts that has the changed billing plan attached
                    if (childAccount.AttachedAccountBillingPlans.Any(a => a.BillingPlan == objChangedBillingPlan.BillingPlan))
                    {
                        GMGColorDAL.AccountPricePlanChangedMessage objAPPCM = new GMGColorDAL.AccountPricePlanChangedMessage
                        {
                            ChildAccount = childAccount.ID
                        };
                        objChangedBillingPlan.AccountPricePlanChangedMessages.Add(objAPPCM);
                        Context.AccountPricePlanChangedMessages.Add(objAPPCM);
                    }
                }
            }
        }

        public static void UpdateNewCollaborateBillingPlan(NewCollaborateBillingPlanViewModel planModel, Account loggedAccount, DbContextBL context)
        {
            try
            {
                //get database plan info
                var dbInfo = (from bp in context.BillingPlans
                              where bp.ID == planModel.ID
                              select bp).FirstOrDefault();

                //update database plan info
                dbInfo.Type = planModel.SelectedBilingPlanType;
                dbInfo.Name = planModel.BillingPlanName;
                dbInfo.IsDemo = planModel.isDemoPlan;
                dbInfo.IsFixed = planModel.isFixedPlan;
                dbInfo.Price = planModel.Price;
                dbInfo.Proofs = 0;
                if (planModel.isFixedPlan)
                {
                    dbInfo.MaxUsers = 0;
                    dbInfo.MaxStorageInGb = 0;
                }
                else
                {
                    dbInfo.MaxUsers = planModel.MaxUsers;
                    dbInfo.MaxStorageInGb = planModel.MaxStorageInGb;
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, loggedAccount.ID,
                                "PlansBL.UpdateChanges : Updating Billing Plan Failed. {0}",
                                ex.Message);
            }
        }

        public static bool CheckIfPlanTypeNameIsUnique(string planTypeName, DbContextBL context)
        {
            return (from bpt in context.BillingPlanTypes
                    where bpt.Name == planTypeName
                    select bpt.ID).Any();
        }

        /// <summary>
        /// Get the billing plan types list that are created by the current account
        /// </summary>
        /// <param name="accId">Account ID</param>
        /// <param name="context">Database Context</param>
        /// <returns>List of Billing Plan Types Objects</returns>
        public static List<BillingPlanType> GetAccountOwnedBillingPlanTypes(int? accId, DbContextBL context)
        {
            return (from bpt in context.BillingPlanTypes
                    where bpt.Account == accId
                    select bpt).ToList();
        }

        public static List<PlansPerModuleViewModel> GetAccountOwnedBillingPlans(Account loggedAccount, AccountType.Type loggedAccountType, DbContextBL context)
        {
            var accId = loggedAccountType == AccountType.Type.GMGColor ? null : (int?)loggedAccount.ID;
            var currencySymbol = loggedAccountType == AccountType.Type.GMGColor ? loggedAccount.Currency.Symbol : loggedAccount.ParentAccount.Currency.Symbol;

            //Retrieve plan types grouped by application module
            var plansPerModule = (from apm in context.AppModules
                                  let planTypes = (from bpt in context.BillingPlanTypes
                                                   where bpt.Account == accId && bpt.AppModule == apm.ID
                                                   select new PlanTypeViewModel()
                                                   {
                                                       Type = bpt.Name,
                                                       ID = bpt.ID,
                                                       Label = apm.Key == (int)GMG.CoZone.Common.AppModule.Collaborate ? GMGColor.Resources.Resources.lblProofs 
                                                                                                                        : apm.Key == (int)GMG.CoZone.Common.AppModule.Deliver ? GMGColor.Resources.Resources.lblDeliverProofs 
                                                                                                                                                                                : string.Empty,
                                                   }).ToList()
                                  where apm.Key != (int)GMG.CoZone.Common.AppModule.Admin &&
                                        apm.Key != (int)GMG.CoZone.Common.AppModule.SysAdmin
                                  select new PlansPerModuleViewModel()
                                  {
                                      ModuleName = apm.Name,
                                      PlanTypes = planTypes
                                  }).ToList();

            //Get plans avialable for each plan type
            foreach(var planPerModule in plansPerModule)
            {
                foreach(var planType in planPerModule.PlanTypes)
                {
                    var plans = (from bp in context.BillingPlans
                                 let isActive = ((from asp in context.AccountSubscriptionPlans
                                                  where asp.SelectedBillingPlan == bp.ID
                                                  select asp.ID).Any() || (from sp in context.SubscriberPlanInfoes
                                                                           where sp.SelectedBillingPlan == bp.ID
                                                                           select sp.ID).Any())
                                 let isAttached = (from atp in context.AttachedAccountBillingPlans
                                                   where atp.BillingPlan == bp.ID
                                                   select atp.ID).Any()

                                 where bp.Type == planType.ID
                                 select new 
                                 { 
                                   Plan = bp,
                                   IsActive = isActive,
                                   IsAttached = isAttached
                                 }).ToList();

                    foreach(var p in plans)
                    {
                        planType.Plans.Add(new PlanViewModel()
                        {
                            ID = p.Plan.ID,
                            Name = p.Plan.Name,
                            Status = p.IsActive ? GMGColor.Resources.Resources.lblActive : p.IsAttached ? GMGColor.Resources.Resources.lblAssignedAndInactive : GMGColor.Resources.Resources.lblInactive,
                            Price = GMGColorFormatData.GetFormattedCurrency(p.Plan.Price, currencySymbol),
                            MaxStorageInGb = (p.Plan.MaxStorageInGb == null || (p.Plan.IsFixed && p.Plan.MaxStorageInGb == 0)) ? "-" : p.Plan.MaxStorageInGb.ToString(),
                            CostPerProof = BillingPlan.GetCostPerProof(p.Plan.Proofs, p.Plan.Price, p.Plan.IsFixed, currencySymbol),
                            PricePerYear = GMGColorFormatData.GetFormattedCurrency((p.Plan.Price * 12), currencySymbol),
                            Proofs = BillingPlan.GetProofCount(p.Plan.Proofs, p.Plan.IsFixed),
                            MaxUsers = (p.Plan.MaxUsers == null || (p.Plan.IsFixed && p.Plan.MaxUsers == 0)) ? null : p.Plan.MaxUsers.Value.ToString()
                        });
                    }                    
                }
            }

            return plansPerModule;
        }

        /// <summary>
        /// Get the modules to which the account has access to
        /// </summary>
        /// <param name="accType">Account Type</param>
        /// <param name="accId">Account ID</param>
        /// <param name="context">Database Context</param>
        /// <returns>List of Application Modules Objects</returns>
        public static List<AppModule> GetAccountAvailableModules(AccountType.Type accType, int accId, DbContextBL context)
        {
            if (accType == AccountType.Type.GMGColor)
            {
                return (from apm in context.AppModules
                    where apm.Key != (int) GMG.CoZone.Common.AppModule.Admin &&
                          apm.Key != (int) GMG.CoZone.Common.AppModule.SysAdmin 
                    select apm).ToList();
            }

            return (from apm in context.AppModules
                    join bpt in context.BillingPlanTypes on apm.ID equals bpt.AppModule
                    join bp in context.BillingPlans on bpt.ID equals bp.Type
                    join acs in context.AccountSubscriptionPlans on bp.ID equals acs.SelectedBillingPlan
                    where acs.Account == accId &&
                            apm.Key != (int)GMG.CoZone.Common.AppModule.Admin &&
                            apm.Key != (int)GMG.CoZone.Common.AppModule.SysAdmin
                    select apm).Distinct().ToList();
            
        }

        /// <summary>
        /// Get account owned billing plans for the specified app module
        /// </summary>
        /// <param name="accId">Account ID</param>
        /// <param name="appModule">AppLication Module ID</param>
        /// <param name="context">Database Context</param>
        /// <returns>The list of billing plan types objects</returns>
        public static List<BillingPlanType> GetBillingPlanTypesByModule(int? accId, int appModule, DbContextBL context)
        {
            return (from bpt in context.BillingPlanTypes
             where bpt.Account == accId && bpt.AppModule == appModule
             select bpt).ToList();
        }
    }
}
