﻿using System.Collections.Generic;
using System.Linq;
using GMGColorBusinessLogic.Common;
using System.Drawing;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using System;

namespace GMGColorBusinessLogic
{
    public class ExternalCollaboratorBL : BaseBL
    {
        /// <summary>
        /// Generates a new color outside the existing account users colors
        /// </summary>
        /// <returns></returns>
        [ObsoleteAttribute("This method is obsolete. Call GenerateCollaboratorProofStudioColor from ProofstudioApiService instead.", false)]
        public static string GenerateCollaboratorProofStudioColor(int accountID, List<string> avoidColors, DbContextBL context)
        {
            // Retrieve static internal user color and account existing external users color
            List<string> predefinedColors = (from c in context.ProofStudioUserColors select c.HEXValue)
                                            .Concat(from e in context.ExternalCollaborators where e.Account == accountID select e.ProofStudioColor)
                                            .Concat(avoidColors)
                                            .ToList();

            // Transform existing colors to grayscale
            List<Color> colors = predefinedColors.Select(ColorTranslator.FromHtml).ToList();
            List<float> hueVals = colors.Select(i => i.GetHue()).GroupBy(v => v).Select(v=>v.First()).OrderBy(v => v).ToList();
            // Get next available color in grayscale and convert it to RGB
            double nextAvailableColor = ColorHelper.GetNextAvailableColorInGrayscale(hueVals);
            Color externalUSerColor = ColorHelper.ColorFromHSV(nextAvailableColor, 0.75, 0.75);

            return ColorHelper.GetColorAsHex(externalUSerColor);
        }

        /// <summary>
        /// Generates a random color and attach it to the specified user
        /// </summary>
        /// <param name="externalCollaboratorID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        [ObsoleteAttribute("This method is obsolete. ", false)]
        public static string AttachColorToExternalCollaborator(int externalCollaboratorID, int accountID, List<string> avoidColors, DbContextBL context)
        {
            string userColor = GenerateCollaboratorProofStudioColor(accountID, avoidColors, context);

            context.ExternalCollaborators.Where(c => c.ID == externalCollaboratorID).FirstOrDefault().ProofStudioColor = userColor;

            return userColor;
        }

        /// <summary>
        /// Updates the specified external user
        /// </summary>
        /// <param name="extUser"></param>
        /// <param name="context"></param>
        public static void UpdateExternalCollaborator(ExternalUserDetails extUser, DbContextBL context)
        {
            if (extUser.UserType == Enums.ExternalUserType.Collaborate)
            {
                context.ExternalCollaborators.FirstOrDefault(c => c.ID == extUser.ID).Locale = extUser.Locale;
            }
            else
            {
                context.DeliverExternalCollaborators.FirstOrDefault(c => c.ID == extUser.ID).Locale = extUser.Locale;
            }
        }
    }
}
