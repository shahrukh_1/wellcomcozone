﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;
using GMGColorDAL.CustomModels.XMLEngine;
using GMG.CoZone.Common;
using GMGColorDAL.Common;

namespace GMGColorBusinessLogic
{
    public class XMLEngineBL
    {
        #region Templates

        /// <summary>
        /// Validate given Preset Name.
        /// </summary>
        /// <param name="account"></param>
        /// <param name="newUsername"></param>
        /// <returns>returns 'true' if validate true(name not exist in the system)
        /// returns 'false' if validate false(name exist in the system)
        /// </returns>
        public static bool ValidateName(string newName, string currentName, int accId, DbContextBL context)
        {
            try
            {
                List<string> lstUserNames = (from pr in context.UploadEnginePresets
                                             where newName == pr.Name.ToLower() && pr.Account == accId
                                             select pr.Name.ToLower()).ToList();

                if (!string.IsNullOrEmpty(currentName))
                {
                    lstUserNames.Remove(currentName);
                }

                return !lstUserNames.Contains(newName);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidatePresetName, ex);
            }
        }

        /// <summary>
        /// Validates Upload Engine Preset Name for uniqueness
        /// </summary>
        /// <param name="newName">Name to be checked</param>
        /// <param name="currentName">CurrentName( if existing)</param>
        /// <param name="accId">Account ID</param>
        /// <param name="context">DB Context</param>
        /// <returns></returns>
        public static bool ValidateProfileName(string newName, string currentName, int accId, DbContextBL context)
        {
            try
            {
                List<string> lstUserNames = (from pr in context.UploadEngineProfiles
                                             where newName == pr.Name.ToLower() && pr.Account == accId
                                             select pr.Name.ToLower()).ToList();

                if (!string.IsNullOrEmpty(currentName))
                {
                    lstUserNames.Remove(currentName);
                }

                return !lstUserNames.Contains(newName);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidateProfileName, ex);
            }
        }

        /// <summary>
        /// Validates Upload Engine XML Template Name for uniqueness
        /// </summary>
        /// <param name="newName">Name to be checked</param>
        /// <param name="currentName">CurrentName( if existing)</param>
        /// <param name="accId">Account ID</param>
        /// <param name="context">DB Context</param>
        /// <returns></returns>
        public static bool ValidateXMLTemplateName(string newName, string currentName, int accId, DbContextBL context)
        {
            try
            {
                List<string> lstUserNames = (from xlt in context.UploadEngineCustomXMLTemplates
                                             where newName == xlt.Name.ToLower() && xlt.Account == accId
                                             select xlt.Name.ToLower()).ToList();

                if (!string.IsNullOrEmpty(currentName))
                {
                    lstUserNames.Remove(currentName);
                }

                return !lstUserNames.Contains(newName);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidateXMLEngineName, ex);
            }
        }
      
        public static int? GetProfileOutputPresetIfSet(string profileName, int userID, DbContextBL context)
        {
            try
            {
                return (from us in context.Users
                        join ac in context.Accounts on us.Account equals ac.ID
                        join profile in context.UploadEngineProfiles on ac.ID equals profile.Account
                        join preset in context.UploadEnginePresets on profile.OutputPreset equals preset.ID
                        where us.ID == userID && profile.Name.ToLower() == profileName.ToLower()
                        select preset.ID).FirstOrDefault();
            }
            catch (System.Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetProfileOutputPreset, ex);
            }
            
        }

        /// <summary>
        /// Gets Protocol Key Based on ID
        /// </summary>
        /// <param name="protocolId">ID of the desired protocol</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetProtocolKey(int protocolId, DbContextBL context)
        {
            try
            {
                return (from ftp in context.FTPProtocols
                        where ftp.ID == protocolId
                        select ftp.Key).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetFTPProtocolKey, ex);
            }
            
        }

        /// <summary>
        /// Gets username and presets for the specified Preset from Database
        /// </summary>
        /// <param name="id">database ID</param>
        /// <param name="context"></param>
        /// <returns>KeyValuePair containing username as Key and password as value</returns>
        public static KeyValuePair<string, string> GetPresetLogin(int id, DbContextBL context)
        {
            try
            {
                return (from pr in context.UploadEnginePresets
                        where pr.ID == id
                        select new { pr.Username, pr.Password }).ToList()
                    .Select(t => new KeyValuePair<string, string>(t.Username, t.Password)).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUploadEnginePresetLogin, ex);
            }
        }

        /// <summary>
        /// Retrieves the account presets a a collection of ID-name of key value pairs
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<KeyValuePair<int, string>> GetAccountPresets(int accountID, GMG.CoZone.Common.AppModule module, DbContextBL context)
        {
            try
            {
                switch (module)
                {
                    case GMG.CoZone.Common.AppModule.Collaborate:
                        return (from pr in context.UploadEnginePresets
                                where pr.Account == accountID && pr.IsEnabledInCollaborate == true
                                select pr).AsEnumerable().Select(p => new KeyValuePair<int, string>(p.ID, p.Name)).ToList();

                    case GMG.CoZone.Common.AppModule.Deliver:
                        return (from pr in context.UploadEnginePresets
                                where pr.Account == accountID && pr.IsEnabledInDeliver == true
                                select pr).AsEnumerable().Select(p => new KeyValuePair<int, string>(p.ID, p.Name)).ToList();
                    case GMG.CoZone.Common.AppModule.FileTransfer:
                        return (from pr in context.UploadEnginePresets
                                where pr.Account == accountID && pr.IsEnabledInCollaborate == true
                                select pr).AsEnumerable().Select(p => new KeyValuePair<int, string>(p.ID, p.Name)).ToList();
                    default:
                        return null;
                }

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveAccountPresetsEntries, ex);
            }
        }

        /// <summary>
        /// Delete ftp download job object from database
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="context"></param>
        public static void DeleteFTPDownloadJob(FTPPresetJobDownload obj, DbContextBL context)
        {
            try
            {
                context.FTPPresetJobDownloads.Remove(obj);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteFTPDownloadJob, ex);
            }
        }

        /// <summary>
        /// Delete CustomXMLTemplate object from database
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="context"></param>
        public static void DeleteCustomXMLTemplate(UploadEngineCustomXMLTemplate obj, DbContextBL context)
        {
            try
            {
                context.UploadEngineCustomXMLTemplates.Remove(obj);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteCustomXMLTemplate, ex);
            }
        }

        /// <summary>
        /// Delete CustomXMLTemplate Field object from database
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="context"></param>
        public static void DeleteCustomXMLTemplateField(UploadEngineCustomXMLTemplatesField obj, DbContextBL context)
        {
            try
            {
                context.UploadEngineCustomXMLTemplatesFields.Remove(obj);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteCustomXMLTemplate, ex);
            }
        }

        /// <summary>
        /// Get CustomXMLTemplate Object from DB
        /// </summary>
        /// <param name="id">Object ID</param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static UploadEngineCustomXMLTemplate GetCustomXMLTemplateById(int id, DbContextBL context)
        {
            return (from cxmlt in context.UploadEngineCustomXMLTemplates
                    where cxmlt.ID == id
                    select cxmlt).SingleOrDefault();
        }

        /// <summary>
        /// Save CustomXMLTemplates Changes in DB
        /// </summary>
        /// <param name="customXMLTemplateBO"></param>
        /// <param name="customXMLFields"></param>
        /// <param name="isNew"></param>
        /// <param name="context"></param>
        public static void ProcessCustomXMLTemplates(UploadEngineCustomXMLTemplate customXMLTemplateBO, List<CustomXMLField> customXMLFields, bool isNew, DbContextBL context)
        {
            try
            {
                foreach(CustomXMLField customField in customXMLFields.Where(t => t.ID == 0 ))
                {
                    UploadEngineCustomXMLTemplatesField customFieldBO = new UploadEngineCustomXMLTemplatesField();
                    customFieldBO.XPath = customField.XPath;
                    customFieldBO.UploadEngineCustomXMLTemplate1 = customXMLTemplateBO;
                    customFieldBO.UploadEngineDefaultXMLTemplate     = customField.DefaultXMLTemplateID;

                    context.UploadEngineCustomXMLTemplatesFields.Add(customFieldBO);
                }

                if (!isNew)
                {
                    List<CustomXMLField> editedFields = customXMLFields.Where(t => t.ID > 0).ToList();

                    List<UploadEngineCustomXMLTemplatesField> existingFields = (from cxmlt in context.UploadEngineCustomXMLTemplatesFields
                                                                                where cxmlt.UploadEngineCustomXMLTemplate == customXMLTemplateBO.ID
                                                                                select cxmlt).ToList();

                    foreach (UploadEngineCustomXMLTemplatesField existingCustomField in existingFields)
                    {
                        CustomXMLField updatedCustomField = editedFields.SingleOrDefault(t => t.ID == existingCustomField.ID);
                        if (!updatedCustomField.IsDeleted)
                        {
                            #region Update

                            existingCustomField.XPath = updatedCustomField.XPath;

                            #endregion
                        }
                        else
                        {
                            #region Delete

                            context.UploadEngineCustomXMLTemplatesFields.Remove(existingCustomField);

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessCustomXMLTemplates, ex);
            }
        }  

        /// <summary>
        /// Retrieves the default XML fields
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<XMLField> GetXMLTemplateFields(DbContextBL context, int? customTemplateID)
        {
            try
            {
                return UploadEngineCustomXMLTemplate.GetXMLFields(context, customTemplateID);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveXMLFilelds, ex);
            }
        }

        #endregion

        #region Upload Settings

        /// <summary>
        /// Gets Duplicate FileName options from Database
        /// </summary>
        /// <param name="context">Database Context</param>
        /// <returns>List of Duplicate Filename Options</returns>
        public static List<UploadEngineDuplicateFileName> GetDuplicateFileNameUploadOptions(DbContextBL context)
        {
            try
            {
                return context.UploadEngineDuplicateFileNames.ToList();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetDuplicateFileNameOptions, ex);
            }
        }

        /// <summary>
        /// Gets Upload Version Name options from database
        /// </summary>
        /// <param name="context">Database Context</param>
        /// <returns>A list containing Upload Version Name options from database</returns>
        public static List<UploadEngineVersionName> GetUploadVersionNameOptions(DbContextBL context)
        {
            try
            {
                return context.UploadEngineVersionNames.ToList();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUploadVersionNameOptions, ex);
            }
        }

        /// <summary>
        /// Creates View Model for Upload Engine Settings
        /// </summary>
        /// <param name="accountID">Logged Account ID</param>
        /// <param name="context">Database Context</param>
        /// <returns>The Model for Upload Engine Settings View </returns>
        public static UploadEngineAccountSettings GetAccoutUploadEngineSettings(int accountID, DbContextBL context)
        {
            try
            {
                UploadEngineAccountSettings settings = null;
                UploadEngineAccountSetting accountSettings = UploadEngineAccountSetting.GetAccountUploadEngineSettings(accountID, context);
                if (accountSettings != null)
                {
                    settings = new UploadEngineAccountSettings(accountSettings);
                }
                else
                {
                    settings = new UploadEngineAccountSettings()
                    {
                        DuplicateFileOption =
                            context.UploadEngineDuplicateFileNames.SingleOrDefault(u => u.Key == (int)FileUploadDuplicateName.MatchFileNameExactly),
                        VersionNameOption =
                            context.UploadEngineVersionNames.SingleOrDefault(u => u.Key == (int)VersionUploadName.KeepOriginalName),
                        VersionNumberPositionFromStart = true,
                        VersionNumberPosition = 0,
                        AllowPDF = false,
                        AllowImage = false,
                        AllowVideo = false,
                        AllowSWF = false,
                        AllowZip = false,
                        AllowDoc = false,
                        ZipFilesWhenDownloading = false,
                        ApplyUploadSettingsForNewApproval = false,
                        PostStatusUpdatesURL = string.Empty
                    };
                }
                settings.IsEnabledMediaTools = UploadEngineAccountSetting.IsMediaToolsEnabled(accountID, context);

                settings.userGroupPermission = (from ug in context.UserGroups
                                                where ug.Account == accountID
                                                let uploader = (from ugprm in context.UploadEngineUserGroupPermissions
                                                                join prm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals prm.ID
                                                                where ugprm.UserGroup == ug.ID && prm.Key == (int)UploadEnginePermissionEnum.Uploader
                                                                select ugprm.ID).Any()                                               
                                                let downloadFromCollaborate = (from ugprm in context.UploadEngineUserGroupPermissions
                                                                               join prm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals prm.ID
                                                                               where ugprm.UserGroup == ug.ID && prm.Key == (int)UploadEnginePermissionEnum.DownloadFromCollaborate
                                                                               select ugprm.ID).Any()
                                                let downloadFromDeliver = (from ugprm in context.UploadEngineUserGroupPermissions
                                                                           join prm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals prm.ID
                                                                           where ugprm.UserGroup == ug.ID && prm.Key == (int)UploadEnginePermissionEnum.DownloadFromDeliver
                                                                           select ugprm.ID).Any()                                               
                                                select new UserGroupPermissions
                                                {
                                                    UserGroupID = ug.ID,
                                                    UserGroupName = ug.Name,
                                                    Uploader = uploader,                                                   
                                                    DownloadFromCollaborate = downloadFromCollaborate,
                                                    DownloadFromDeliver = downloadFromDeliver,                                                   
                                                    AllPermissionsChecked = uploader && downloadFromCollaborate && downloadFromDeliver
                                                }).ToList();
                return settings;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAccountUploadSettings, ex);
            }
        }

        /// <summary>
        /// Save Changes to Upload Engine User Group Permissions
        /// </summary>
        /// <param name="accId">LoggedAccount ID</param>
        /// <param name="selectedUploaderGroups">A list of UserGroups ID's that were selected with Uploader Permissions</param>
        /// <param name="selectedAutoDownloadFromManage">A list of UserGroups ID's that were selected with AutoDownloadForManage Permissions</param>
        /// <param name="selectedDownloadFromCollaborate">A list of UserGroups ID's that were selected with DownloadFromCollaborate Permissions</param>
        /// <param name="selectedDownloadFromDeliver">A list of UserGroups ID's that were selected with DownloadFromDeliver Permissions</param>
        /// <param name="selectedDownloadFromManage">A list of UserGroups ID's that were selected with DownloadFromManage Permissions</param>
        /// <param name="context">Database Context</param>
        public static void ProcessUserGroupPermissions(int accId, List<int> selectedUploaderGroups, List<int> selectedDownloadFromCollaborate, List<int> selectedDownloadFromDeliver, DbContextBL context)
        {
            try
            {
                List<int> accountUserGroups = (from ug in context.UserGroups
                                               where ug.Account == accId
                                               select ug.ID).ToList();

                foreach (int userGroupId in accountUserGroups)
                {
                    #region UploaderPermissions

                    UploadEngineUserGroupPermission userGroupUploaderPermission = (from ugprm in context.UploadEngineUserGroupPermissions
                                                                                   join prm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals prm.ID
                                                                                   where ugprm.UserGroup == userGroupId && prm.Key == (int)UploadEnginePermissionEnum.Uploader
                                                                                   select ugprm).SingleOrDefault();

                    if (userGroupUploaderPermission != null && !selectedUploaderGroups.Contains(userGroupId))
                    {
                        context.UploadEngineUserGroupPermissions.Remove(userGroupUploaderPermission);
                    }
                    else if (userGroupUploaderPermission == null && selectedUploaderGroups.Contains(userGroupId))
                    {
                        UploadEngineUserGroupPermission newUploaderPermission = new UploadEngineUserGroupPermission();
                        newUploaderPermission.UserGroup = userGroupId;
                        newUploaderPermission.UploadEnginePermission = GetUserGroupPermissionId((int)UploadEnginePermissionEnum.Uploader, context);
                        context.UploadEngineUserGroupPermissions.Add(newUploaderPermission);
                    }

                    #endregion                   

                    #region DownloadFromCollaborate

                    UploadEngineUserGroupPermission userGroupDownloadFromCollaboratePermission = (from ugprm in context.UploadEngineUserGroupPermissions
                                                                                                  join prm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals prm.ID
                                                                                                  where ugprm.UserGroup == userGroupId && prm.Key == (int)UploadEnginePermissionEnum.DownloadFromCollaborate
                                                                                                  select ugprm).SingleOrDefault();

                    if (userGroupDownloadFromCollaboratePermission != null && !selectedDownloadFromCollaborate.Contains(userGroupId))
                    {
                        context.UploadEngineUserGroupPermissions.Remove(userGroupDownloadFromCollaboratePermission);
                    }
                    else if (userGroupDownloadFromCollaboratePermission == null && selectedDownloadFromCollaborate.Contains(userGroupId))
                    {
                        UploadEngineUserGroupPermission newDownloadFromCollaboratePermission = new UploadEngineUserGroupPermission();
                        newDownloadFromCollaboratePermission.UserGroup = userGroupId;
                        newDownloadFromCollaboratePermission.UploadEnginePermission = GetUserGroupPermissionId((int)UploadEnginePermissionEnum.DownloadFromCollaborate, context);
                        context.UploadEngineUserGroupPermissions.Add(newDownloadFromCollaboratePermission);
                    }

                    #endregion

                    #region DownloadFromDeliver

                    UploadEngineUserGroupPermission userGroupDownloadFromDeliverPermission = (from ugprm in context.UploadEngineUserGroupPermissions
                                                                                              join prm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals prm.ID
                                                                                              where ugprm.UserGroup == userGroupId && prm.Key == (int)UploadEnginePermissionEnum.DownloadFromDeliver
                                                                                              select ugprm).SingleOrDefault();

                    if (userGroupDownloadFromDeliverPermission != null && !selectedDownloadFromDeliver.Contains(userGroupId))
                    {
                        context.UploadEngineUserGroupPermissions.Remove(userGroupDownloadFromDeliverPermission);
                    }
                    else if (userGroupDownloadFromDeliverPermission == null && selectedDownloadFromDeliver.Contains(userGroupId))
                    {
                        UploadEngineUserGroupPermission newDownloadFromDeliverPermission = new UploadEngineUserGroupPermission();
                        newDownloadFromDeliverPermission.UserGroup = userGroupId;
                        newDownloadFromDeliverPermission.UploadEnginePermission = GetUserGroupPermissionId((int)UploadEnginePermissionEnum.DownloadFromDeliver, context);
                        context.UploadEngineUserGroupPermissions.Add(newDownloadFromDeliverPermission);
                    }

                    #endregion
                  
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessUserGroupPermission, ex);
            }
        }

        /// <summary>
        /// Gets Upload Engine User Group Permission Id From Database
        /// </summary>
        /// <param name="Key">Key Identifier</param>
        /// <param name="context">Database Context</param>
        /// <returns>Database ID for the specified key</returns>
        public static int GetUserGroupPermissionId(int Key, DbContextBL context)
        {
            try
            {
                return (from prm in context.UploadEnginePermissions
                        where prm.Key == Key
                        select prm.ID).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRetrieveUserGroupPermission, ex);
            }
        }

        /// <summary>
        /// Removes UserGroupPermission From Database
        /// </summary>
        /// <param name="obj">Object to be removed</param>
        /// <param name="context">Database Context</param>
        public static void DeleteUserGroupPermissions(UploadEngineUserGroupPermission obj, DbContextBL context)
        {
            try
            {
                context.UploadEngineUserGroupPermissions.Remove(obj);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteUserGroupPermission, ex);
            }
        }

        #endregion

        #region FTP Service 

        /// <summary>
        /// Validates the User Guid Folder
        /// </summary>
        /// <param name="folderPath">Folder Name that should represent a CoZone User GUID</param>
        public static bool ValidateUserFolder(string folderPath)
        {
            using (var context = new DbContextBL())
            {
                return ValidateUserFolder(folderPath, context);
            }
        }

        public static bool ValidateUserFolder(string folderPath, DbContextBL context)
        {

            int? userId = (from us in context.Users
                            where us.Guid == folderPath
                            select us.ID).SingleOrDefault();

            bool hasUploaderAccess = (from ugprm in context.UploadEngineUserGroupPermissions
                                        join ug in context.UserGroups on ugprm.UserGroup equals ug.ID
                                        join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                        join perm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals perm.ID
                                        where perm.Key == (int)UploadEnginePermissionEnum.Uploader && ugu.User == userId.Value
                                        select ugprm.ID).Any();

            if (!hasUploaderAccess)
            {
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Validates the Profile Guid Folder
        /// </summary>
        /// <param name="profileFolderPath">Folder Name that should represent a Profile GUID</param>
        /// <param name="userFolderPath">Folder Name that should represent a CoZone User GUID</param>
        public static bool ValidateProfileFolder(string profileFolderPath, string userFolderPath)
        {
            using (var context = new DbContextBL())
            {
                return ValidateProfileFolder(profileFolderPath, userFolderPath, context);
            }
        }

        public static bool ValidateProfileFolder(string profileFolderPath, string userFolderPath, DbContextBL context)
        {
            bool isProfileFolderValid = (from us in context.Users
                                            join ac in context.Accounts on us.Account equals ac.ID
                                            join pr in context.UploadEngineProfiles on ac.ID equals pr.Account
                                            where us.Guid == userFolderPath && pr.Name.ToLower() == profileFolderPath.ToLower() && pr.HasInputSettings && pr.IsDeleted == false
                                            select pr.ID).Any();

            return isProfileFolderValid;
        }

        /// <summary>
        /// Checks if the Asset file attached to job is valid to be added in CoZone
        /// </summary>
        /// <param name="extention">File Extension</param>
        /// <param name="userGuid">CoZone User Guid</param>
        /// <returns>Whether file is valid to be added to CoZone or not</returns>
        public static bool IsFileValid(string extention, string userGuid, DbContextBL context = null)
        {
            bool isLocalContext = false;
            try
            {
                if (context == null)
                {
                    context = new DbContextBL();
                    isLocalContext = true;
                }
                var filePermissions = (from us in context.Users
                                        join ac in context.Accounts on us.Account equals ac.ID
                                        join ueset in context.UploadEngineAccountSettings on ac.UploadEngineSetings equals ueset.ID
                                        where us.Guid == userGuid
                                        select new
                                        {
                                            ueset.AllowImage,
                                            ueset.AllowVideo,
                                            ueset.AllowZip,
                                            ueset.AllowPDF,
                                            ueset.AllowSWF
                                        }).SingleOrDefault();

                extention = extention.ToLower();
                if (filePermissions != null)
                {
                    if (extention == ".pdf" && filePermissions.AllowPDF)
                    {
                        return true;
                    }
                    else if (extention == ".swf" && filePermissions.AllowSWF)
                    {
                        return true;
                    }
                    else if (extention == ".zip" && filePermissions.AllowZip)
                    {
                        return true;
                    }
                    else if (GMGColorCommon.IsImageFileExtention(extention) && filePermissions.AllowImage)
                    {
                        return true;
                    }
                    else if (GMGColorCommon.IsVideoFile(extention) && filePermissions.AllowVideo)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidateAssetFile, ex);
            }
            finally
            {
                if (isLocalContext && context != null)
                {
                    context.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets XMLCustomTemplate ID based on XML Profile Name and User Guid 
        /// </summary>
        /// <param name="profileName">Upload Engine Profile Name</param>
        /// <param name="userGuid">CoZone User Guid</param>
        /// <param name="context">DB Context</param>
        /// <returns>XMLCustomTemplate ID</returns>
        public static int? GetCustomTemplateIDByProfileName(string profileName, string userGuid, DbContextBL context)
        {
            try
            {
                return (from ct in context.UploadEngineCustomXMLTemplates
                        join ac in context.Accounts on ct.Account equals ac.ID
                        join us in context.Users on ac.ID equals us.Account
                        join pr in context.UploadEngineProfiles on ct.ID equals pr.XMLTemplate
                        where pr.Name.ToLower() == profileName.ToLower() && us.Guid == userGuid
                        select ct.ID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetCustomTemplateIDByProfileName, ex);
            }
        }

        /// <summary>
        /// Gets the duplicate file name option for the user account
        /// </summary>
        /// <param name="userGUID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int? GetDuplicateFileNameSetting(int userID, DbContextBL context)
        {
            try
            {
                return (from ueset in context.UploadEngineAccountSettings
                        join ac in context.Accounts on ueset.ID equals ac.UploadEngineSetings
                        join us in context.Users on ac.ID equals us.Account
                        join dfn in context.UploadEngineDuplicateFileNames on ueset.DuplicateFileName equals dfn.ID
                        where us.ID == userID
                        select dfn.Key).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetDuplicateFileNameSettings, ex);
            }
        }

        /// <summary>
        /// Checkes Whether a duplicate file Exists from same source ftp folder
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sourceFolder"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool DuplicateFileNameExists(string fileName, int userID, string sourceFolder, DbContextBL context)
        {
            try
            {
                return (from ap in context.Approvals
                        where ap.FTPSourceFolder.ToLower() == sourceFolder.ToLower() && ap.FileName.StartsWith(fileName)
                        select ap.ID).Any();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfDuplicateFileNameExists, ex);
            }
        }

        /// <summary>
        /// Gets CoZone Job for duplciate file from same source folder
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sourceFolder"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Job GetDuplicateFileJob(string fileName, string sourceFolder, DbContextBL context)
        {
            try
            {
                return (from ap in context.Approvals
                        join j in context.Jobs on ap.Job equals j.ID
                        join js in context.JobStatus on j.Status equals js.ID
                        where ap.FTPSourceFolder.ToLower() == sourceFolder.ToLower() && ap.FileName == fileName && js.Key != "ARC" && js.Key != "COM" && ap.DeletePermanently == false
                        select j).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetJobForDuplicateFile, ex);
            }
        }

        /// <summary>
        /// Gets CoZone Job for duplciate file with (_vx) suffix from same source folder
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sourceFolder"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<Approval.DuplicatedFileNamesJobs> GetDuplicateFileWithVersionSuffixJob(string fileName, string sourceFolder, DbContextBL context)
        {
            try
            {
                return (from ap in context.Approvals
                        join j in context.Jobs on ap.Job equals j.ID
                        join js in context.JobStatus on j.Status equals js.ID
                        where ap.FTPSourceFolder.ToLower() == sourceFolder.ToLower() && ap.FileName.ToLower().StartsWith(fileName) && js.Key != "ARC" && js.Key != "COM" && ap.DeletePermanently == false
                        select new Approval.DuplicatedFileNamesJobs
                            {
                                FileName = ap.FileName,
                                JobID = j.ID
                            }).ToList();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetJobForDuplicateFile, ex);
            }
        }

        /// <summary>
        /// Checks if the Profile has jobs attached to it
        /// </summary>
        /// <param name="profileID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool ProfileHasJobsAttached(int profileID, DbContextBL context)
        {
            try
            {
                List<string> profileAttachedJobs = (from ap in context.Approvals
                                                    join j in context.Jobs on ap.Job equals j.ID
                                                    join ac in context.Accounts on j.Account equals ac.ID
                                                    join pr in context.UploadEngineProfiles on ac.ID equals pr.Account
                                                    where pr.ID == profileID && ap.FTPSourceFolder != null
                                                    select ap.FTPSourceFolder).ToList();

                profileAttachedJobs.AddRange((from dj in context.DeliverJobs
                                              join j in context.Jobs on dj.Job equals j.ID
                                              join ac in context.Accounts on j.Account equals ac.ID
                                              join pr in context.UploadEngineProfiles on ac.ID equals pr.Account
                                              where pr.ID == profileID && dj.FTPSourceFolder != null
                                              select dj.FTPSourceFolder).ToList());

                if (profileAttachedJobs.Count == 0)
                {
                    return false;
                }
                else
                {
                    string profileName = (from pr in context.UploadEngineProfiles
                                          where pr.ID == profileID
                                          select pr.Name).SingleOrDefault();

                    string ftpProfileFolder;
                    foreach (string ftpSourceFolder in profileAttachedJobs)
                    {
                        GMG.CoZone.Common.Utils.TryGetProfileFolder(ftpSourceFolder, out ftpProfileFolder);
                        if (!String.IsNullOrEmpty(ftpProfileFolder) && ftpProfileFolder.ToLower() == profileName.ToLower())
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfProfileHasJobsAttachedToIt, ex);
            }
        }


        public static bool IsProfileStillValid(string profileName, string userGuid, DbContextBL context)
        {
            try
            {
                return (from us in context.Users
                        join ac in context.Accounts on us.Account equals ac.ID
                        join pr in context.UploadEngineProfiles on ac.ID equals pr.Account
                        where us.Guid == userGuid && pr.Name.ToLower() == profileName.ToLower() && pr.IsDeleted == false
                        select pr.ID).Any();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfProfileIsStillActive, ex);
            }
        }

        /// <summary>
        /// check whether the current profile is activated or not
        /// </summary>
        /// <param name="profileName">Profile Name</param>
        /// <param name="userGuid">User Guid</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static bool IsProfileActive(string profileName, string userGuid, DbContextBL context)
        {
            return (from pr in context.UploadEngineProfiles
                    join ac in context.Accounts on pr.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where pr.Name.ToLower() == profileName.ToLower() && us.Guid == userGuid
                    select pr.IsActive).FirstOrDefault();
        }
        #endregion
        
    }
}
