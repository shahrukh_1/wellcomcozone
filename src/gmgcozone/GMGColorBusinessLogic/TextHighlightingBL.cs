﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.Api;
using System.Xml;
using System.IO;
using GMGColorDAL.Common;
using System.Runtime.Serialization.Json;
using GMGColorDAL;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorBusinessLogic
{
    public class TextHighlightingBL
    {
        #region Constants

        public static int WordsCounter = 0;

        #endregion

        public static List<TextHighlightingWord> ProcessTetDocument(string textDescriptorFilePath, string xmlFileProcessFolderPath, string jsonFileProcessFolderPath, string region, int pageId)
        {
            string XMLtextDescriptorFilePath = textDescriptorFilePath + Constants.XMLTextHighlightingFileName;

            List<TextHighlightingWord> allFoundWords = new List<TextHighlightingWord>();

            XmlDocument doc = GetXmlDocument(region, XMLtextDescriptorFilePath, xmlFileProcessFolderPath);
            
            XmlNodeList xmlParagraphsList = doc.GetElementsByTagName("Para");
            TextHighlightingPageSize pageSizes = GetPageSizes(doc);

            foreach (XmlNode xmlParagraph in xmlParagraphsList)
            {
                ProcessXMLParagraph(pageSizes, allFoundWords, xmlParagraph, pageId);
            }

            ProcessSeparateWordsInXml(pageId, allFoundWords, doc, pageSizes);

            CreateJsonFile(textDescriptorFilePath, jsonFileProcessFolderPath, region, allFoundWords);

            WordsCounter = 0;

            return allFoundWords;
        }

        public static string GetTextHighlightingJsonData(TextHighlightingGetRequest model)
        {
            try
            {
                using (var context = new DbContextBL())
                {
                    TextHighlightingGetInfo data = GetTextHighlightingGetInfo(context, model);

                    if (data != null) 
                    { 
                        string json = GetTextHighlightingJsonFileString(model, data);

                        if (!string.IsNullOrEmpty(json))
                        {
                            return json;
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUploadTextHighlightingData, ex);
            }

            return null;
        }

        public static string GetTextHighlightingJsonFileString(TextHighlightingGetRequest model, TextHighlightingGetInfo data)
        {
            using (var context = new DbContextBL())
            {

                    if (GMGColorIO.FileExists(GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + data.ApprovalGuid + "/" + data.PageNumber + "/" + Constants.JsonTextHighlightingFileName, data.Region))
                    {
                        using (
                            Stream stream =
                                GMGColorIO.GetMemoryStreamOfAFile(
                                    GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" +
                                    data.ApprovalGuid + "/" + data.PageNumber + "/" + Constants.JsonTextHighlightingFileName, data.Region)
                            )
                        {
                            if (stream.Length > 0)
                            {
                                using (StreamReader sr = new StreamReader(stream))
                                {
                                    return sr.ReadToEnd();
                                }
                            }
                        }
                     }
            }

            return string.Empty;
        }

        public static TextHighlightingGetInfo GetTextHighlightingGetInfo(DbContextBL context, TextHighlightingGetRequest model)
        {
            var data = (from ap in context.ApprovalPages
                        join a in context.Approvals on ap.Approval equals a.ID
                        join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                        join u in context.Users on ac.Collaborator equals u.ID
                        join us in context.UserStatus on u.Status equals us.ID
                        join acc in context.Accounts on u.Account equals acc.ID
                        where
                            ap.ID == model.pageId &&
                            u.Guid == model.user_key &&
                            model.is_external_user == false &&
                            us.Key == "A"
                        select new
                        {
                            a.ID,
                            a.Guid,
                            ap.Number,
                            acc.Region
                        }).Union(
                (from ap in context.ApprovalPages
                 join a in context.Approvals on ap.Approval equals a.ID
                 join sa in context.SharedApprovals on a.ID equals sa.Approval
                 join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                 join acc in context.Accounts on ec.Account equals acc.ID
                 where
                     ap.ID == model.pageId &&
                     ec.Guid == model.user_key &&
                     model.is_external_user == true &&
                     ec.IsDeleted == false
                 select new
                 {
                     a.ID,
                     a.Guid,
                     ap.Number,
                     acc.Region
                 })).FirstOrDefault();

            if (data != null)
            {
                return new TextHighlightingGetInfo() { ApprovalGuid = data.Guid, PageNumber = data.Number, Region = data.Region, ApprovalID = data.ID };
            }

            return null;
        }

        private static void ProcessSeparateWordsInXml(int pageId, List<TextHighlightingWord> allFoundWords, XmlDocument doc, TextHighlightingPageSize pageSize)
        {
            XmlNodeList xmlWordsList = doc.GetElementsByTagName("Word");

            List<XmlNode> wordsList = new List<XmlNode>();
            foreach (XmlNode node in xmlWordsList)
            {
                // If word is not child of a Para element, but found directly under Content
                if (node.ParentNode != null && node.ParentNode.Name == "Content")
                {
                    wordsList.Add(node);
                }
            }

            ProcessXMLWordList(pageSize, allFoundWords, pageId, wordsList);
        }

        private static void ProcessXMLParagraph(TextHighlightingPageSize pageSize, List<TextHighlightingWord> allFoundWords, XmlNode xmlParagraph, int pageId)
        {
            XmlNodeList xmlParagraphChildNodes = xmlParagraph.FirstChild.Name == "Word" ? xmlParagraph.ChildNodes : xmlParagraph.FirstChild.ChildNodes;
            

            List<XmlNode> xmlWordsList = new List<XmlNode>();

            foreach (XmlNode node in xmlParagraphChildNodes)
            {
                // Allow only word elements
                if (node.Name == "Word")
                {
                    xmlWordsList.Add(node);
                }
                else if (node.Name == "Box")
                {
                    xmlWordsList.Add(node.FirstChild);
                }
            }

            ProcessXMLWordList(pageSize, allFoundWords, pageId, xmlWordsList);
        }

        private static void ProcessXMLWordList(TextHighlightingPageSize pageSize, List<TextHighlightingWord> allFoundWords, int pageId, List<XmlNode> xmlWordsList)
        {
            for (int i = 0; i < xmlWordsList.Count; i++)
            {
                int isNotFirstNorLast = 0;
                if (i > 0 && i < xmlWordsList.Count - 1) isNotFirstNorLast = 1;
                else if (i == xmlWordsList.Count - 1) isNotFirstNorLast = 2;

                ProcessXMLWord(pageSize, allFoundWords, xmlWordsList[i], isNotFirstNorLast, pageId);
            }
        }

        public static List<TextHighlightingWord> GetTextHightlightingWords(TextHighlightingGetRequest model, List<TextHighlightingWord> wordsList)
        {
            // Regenerate JSON file as the existing file is containing old format word entries (missing ID and pageId properties)
            if (wordsList.Any(w => string.IsNullOrEmpty(w.ID) || w.pageId == 0) || 
                wordsList.Count == 0)
            {
                if (wordsList.Any(w => string.IsNullOrEmpty(w.ID)) && wordsList[0].ID == null)
                {
                    wordsList.RemoveAt(0);
                    wordsList = wordsList.Any(w => string.IsNullOrEmpty(w.ID)) ? GenerateNewTextHighlightJSONFile(model.approvalId, model.pageId) : wordsList;
                }
                else
                {
                    wordsList = GenerateNewTextHighlightJSONFile(model.approvalId, model.pageId);
                }
            }

            return wordsList;
        }

        public static List<TextHighlightingWord> GenerateNewTextHighlightJSONFile(int approvalId, int pageId)
        {
            string domain;
            string region;
            int pageNumber = 0;
            GMGColorDAL.Approval objApproval;
            List<TextHighlightingWord> wordsList = new List<TextHighlightingWord>();

            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    objApproval = ApprovalBL.GetApprovalById(approvalId, context);

                    if (objApproval != null)
                    {
                        GMGColorLogging.log.InfoFormat("TextHighlightingBL:Approval filename {0}, pageId: {1}, pages count: {2}", objApproval.FileName, pageId, objApproval.ApprovalPages.Count);

                        GMGColorDAL.ApprovalPage approvalPage = objApproval.ApprovalPages.FirstOrDefault(ap => ap.ID == pageId);
                        if (approvalPage != null)
                        {
                            pageNumber = approvalPage.Number;
                        }
                        else
                        {
                            throw new Exception(GMGColor.Resources.Resources.txtTextHighlightPageNotFound);
                        }

                        domain = objApproval.Job1.Account1.IsCustomDomainActive ? objApproval.Job1.Account1.CustomDomain : objApproval.Job1.Account1.Domain;
                        region = objApproval.Job1.Account1.Region;
                    }
                    else
                    {
                        throw new Exception(ExceptionBLMessages.CouldNotGetApprovalById);
                    }
                }
                
                ApprovalJobFolder approvalJobFolder = new ApprovalJobFolder(objApproval.Guid,
                                                                                        objApproval.FileName,
                                                                                        GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ?
                                                                                                            GMGColorConfiguration.AppConfiguration.PathToProcessingFolder :
                                                                                                            GMGColorConfiguration.AppConfiguration.FileSystemPathToDataFolder,
                                                                                        GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ?
                                                                                            GMGColorConfiguration.AppConfiguration.PathToDataFolder(region) :
                                                                                            GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(region));

                string pageDescriptorPath = approvalJobFolder.ApprovalFolderRelativePath + pageNumber + @"\";
                string xmlFileProcessFolderPath = approvalJobFolder.GetXmlFileProcessFolderPath(pageNumber.ToString());
                string jsonFileProcessFolderPath = approvalJobFolder.GetJsonFileProcessFolderPath(pageNumber.ToString());

                wordsList = ProcessTetDocument(pageDescriptorPath, xmlFileProcessFolderPath, jsonFileProcessFolderPath, region, pageId);
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Error in GenerateNewTextHighlightJSONFile Method - ApprovalID: {1}, PageID: {2}; Exception : {0} ", ex.Message, approvalId, pageId);
            }

            return wordsList;
        }

        private static XmlDocument GetXmlDocument(string region, string XMLtextDescriptorFilePath, string xmlFileProcessFolderPath)
        {
            XmlDocument doc = new XmlDocument();

            // If json file is not already uploaded on S3, use processor folder
            if (GMGColorIO.FileExists(XMLtextDescriptorFilePath, region))
            {
                using (Stream memoryStream = GMGColorIO.GetMemoryStreamOfAFile(XMLtextDescriptorFilePath, region))
                {
                    doc.Load(memoryStream);
                }
            }
            else
            {
                using (FileStream memoryStream = new FileStream(xmlFileProcessFolderPath, FileMode.Open, FileAccess.Read))
                {
                    doc.Load(memoryStream);
                }
            }

            return doc;
        }

        private static void CreateJsonFile(string textDescriptorFilePath, string jsonFileProcessFolderPath, string region, List<TextHighlightingWord> allFoundWords)
        {
            string JSONtextDescriptorFilePath = textDescriptorFilePath + Constants.JsonTextHighlightingFileName;

            MemoryStream stream = JsonEncode(allFoundWords);

            // If json file is not already uploaded on S3, use processor folder
            if (!GMGColorIO.FileExists(JSONtextDescriptorFilePath, region))
            {
                var fileStream = File.Create(jsonFileProcessFolderPath);
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
                fileStream.Close();
            }

            if (!GMGColorIO.UploadFile(stream, JSONtextDescriptorFilePath, region, true))
            {
                GMGColorLogging.log.Error("File " + JSONtextDescriptorFilePath + " could not be uploaded to AWS. Region: " + region);
            }
        }

        private static MemoryStream JsonEncode(List<TextHighlightingWord> allFoundWords)
        {
            string json = string.Empty;

            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<TextHighlightingWord>));
            ser.WriteObject(stream, allFoundWords);
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);
            json = sr.ReadToEnd();

            return stream;
        }

        private static TextHighlightingPageSize GetPageSizes(XmlDocument doc)
        {
            TextHighlightingPageSize pageSizes = new TextHighlightingPageSize();
            XmlNodeList xmlPagesList = doc.GetElementsByTagName("Page");
            if (xmlPagesList.Count > 0)
            {
                pageSizes.Width = decimal.Parse(xmlPagesList[0].Attributes["width"].Value);
                pageSizes.Height = decimal.Parse(xmlPagesList[0].Attributes["height"].Value);
            }
            return pageSizes;
        }

        private static void ProcessXMLWord(TextHighlightingPageSize pageSize, List<TextHighlightingWord> allFoundWords, XmlNode xmlWord, int isNotFirstNorLast, int pageId)
        {
            List<XmlNode> boxes = GetWordBoxElement(xmlWord);

            foreach (XmlNode box in boxes)
            {
                decimal firstX = box.FirstChild != null ? decimal.Parse(box.FirstChild.Attributes["x"].Value) : 0;
                decimal firstY = box.FirstChild != null ? decimal.Parse(box.FirstChild.Attributes["y"].Value) : 0;

                StringBuilder wholeWord = new StringBuilder();
                decimal lettersWidth = 0;
                decimal lettersHeight = 0;

                foreach (XmlNode glyph in box.ChildNodes)
                {
                    wholeWord.Append(glyph.InnerText);

                    lettersWidth += decimal.Parse(glyph.Attributes["width"].Value);

                    decimal currentLetterHeight = decimal.Parse(glyph.Attributes["size"].Value);
                    if (currentLetterHeight > lettersHeight)
                        lettersHeight = currentLetterHeight;
                }

                if ((isNotFirstNorLast == 1 || isNotFirstNorLast == 2) &&
                    StringDifferentThanPunctuationMarks(wholeWord))
                {
                    TextHighlightingWord spaceWord = GetSpaceWord(pageSize, allFoundWords, box, firstX, pageId);
                    if (spaceWord.ID != null)
                    {
                        allFoundWords.Add(spaceWord);
                        WordsCounter++;
                    }
                }

                TextHighlightingWord word = new TextHighlightingWord();
                SetWordWithLettersProperties(firstX, firstY, wholeWord, lettersWidth, lettersHeight, pageSize.Height, word);
                int lastWordIndex = allFoundWords.Count > 0 ? allFoundWords.Last().endIndex : 0;
                SetCommonWordsProperties(pageSize, allFoundWords.Count, lastWordIndex, box, word, pageId);

                allFoundWords.Add(word);
                WordsCounter++;

                if(isNotFirstNorLast == 2 && StringDifferentThanPunctuationMarks(wholeWord))
                {
                    TextHighlightingWord spaceWord = GetSpaceWord(pageSize, allFoundWords, box, firstX, pageId);
                    allFoundWords.Add(spaceWord);
                    WordsCounter++;
                }
            }
        }


        private static TextHighlightingWord GetSpaceWord(TextHighlightingPageSize pageSize, List<TextHighlightingWord> words, XmlNode box, decimal firstX, int pageId)
        {
            TextHighlightingWord previousWord = words.Any() ? words.Last() : null;
            TextHighlightingWord spaceWord = new TextHighlightingWord();

            if (previousWord != null)
            {
                SetWordWithSpaceProperties(firstX, previousWord, spaceWord);
                int lastWordIndex = words.Count > 0 ? words.Last().endIndex : 0;
                SetCommonWordsProperties(pageSize, words.Count, lastWordIndex, box, spaceWord, pageId);
            }

            return spaceWord;
        }

        private static bool StringDifferentThanPunctuationMarks(StringBuilder wholeWord)
        {
            return wholeWord.ToString() != "." && wholeWord.ToString() != "," && wholeWord.ToString() != ":" &&
                    wholeWord.ToString() != "?" && wholeWord.ToString() != "!";
        }

        private static List<XmlNode> GetWordBoxElement(XmlNode xmlWord)
        {
            if (xmlWord != null)
            {
                List<XmlNode> boxes = new List<XmlNode>();

                if (xmlWord != null)
                {
                    XmlNodeList wordChilds = xmlWord.ChildNodes;

                    foreach (XmlNode wordChild in wordChilds)
                    {
                        if (wordChild.Name == "Box")
                            boxes.Add(wordChild);
                    }
                }

                return boxes;
            }
            else
            {
                return new List<XmlNode>();
            }
        }

        private static void SetWordWithSpaceProperties(decimal firstX, TextHighlightingWord previousWord, TextHighlightingWord currentWord)
        {
            currentWord.x = previousWord.x + previousWord.width + 1;
            currentWord.y = Math.Round(previousWord.y, 3);
            currentWord.width = firstX - previousWord.x - previousWord.width;
            currentWord.height = previousWord.height;
            currentWord.letters = " ";
        }

        private static void SetWordWithLettersProperties(decimal firstX, decimal firstY, StringBuilder wholeWord, decimal lettersWidth, decimal lettersHeight, decimal givenHeight, TextHighlightingWord word)
        {
            word.x = Math.Round(firstX, 3);
            word.y = givenHeight - Math.Round(firstY, 3) + 2.00M;
            word.width = lettersWidth;
            word.height = lettersHeight + 0.01M;
            word.letters = wholeWord.ToString();
        }

        private static void SetCommonWordsProperties(TextHighlightingPageSize pageSize, int wordsCount, int lastWordEndIndex, XmlNode box, TextHighlightingWord word, int pageId)
        {
            decimal alpha = CalculateAlpha(box);
            word.alpha = Math.Round(alpha, 5);
            word.pageHeight = pageSize.Height;
            word.pageWidth = pageSize.Width;

            int wordLettersCount = word.letters != null ? word.letters.Count() : 0;
            if (wordsCount == 0)
            {
                word.startIndex = 0;
                word.endIndex = wordLettersCount;
            }
            else
            {
                word.startIndex = lastWordEndIndex;
                word.endIndex = word.startIndex + wordLettersCount;
            }

            word.ID = Convert.ToString((Convert.ToUInt64(pageId) * 100000) + Convert.ToUInt64(WordsCounter));
            word.pageId = Convert.ToUInt32(pageId);
        }

        private static decimal CalculateAlpha(XmlNode box)
        {
            if (box.FirstChild.Attributes["alpha"] != null)
            {
                return Convert.ToDecimal(box.FirstChild.Attributes["alpha"].Value);
            }
            else
            {
                if (box.Attributes["lry"] == null)
                {
                    double urx = double.Parse(box.Attributes["urx"].Value);
                    double llrx = double.Parse(box.Attributes["llx"].Value);
                    if (urx > llrx)
                    {
                        return 0;
                    }
                    else
                    {
                        return 180;
                    }
                }

                double lly = double.Parse(box.Attributes["lly"].Value);
                double llx = double.Parse(box.Attributes["llx"].Value);
                double lry = double.Parse(box.Attributes["lry"].Value);
                double lrx = double.Parse(box.Attributes["lrx"].Value);

                decimal alpha = (decimal)(Math.Atan((lly - lry) / (llx - lrx)) * -180 / Math.PI);

                return alpha;
            }
        }
    }
}
