﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorBusinessLogic.Common;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using System.Data.Entity.Validation;
using System.Transactions;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using GMGColorNotificationService;
using System.Threading.Tasks;
using GMG.CoZone.Common;
using static GMGColorDAL.ApprovalCollaboratorRole;

namespace GMGColorBusinessLogic
{
	public class PermissionsBL : BaseBL
	{
		public static DashboardInstantNotification GrantDenyPermissions(PermissionsModel model, DbContextBL context,
			GMGColorDAL.User loggedUser, int loggedAccountId, Role.RoleName loggedUserRole, bool serialize)
		{
			int accountId = 0;
			var instantNotification = new DashboardInstantNotification();
			var approvalPermissions = new GrantDenyApprovalPermission();
			try
			{
				var permissions = new PermissionsUsersAndRolesModel();
				var newCollaborators = new List<int>();

				if (serialize)
				{
					if (model.Groups != null)
						permissions.Groups = model.Groups.Split(',').ToList()
							.Select(m => new GroupPermissions() { Group = Int32.Parse(m), Phase = (int?)null })
							.ToList();

					if (model.Users != null)
					{
						var selectedUsersAndRole = model.Users.Split(',').ToList();
						permissions.Users = GetUsersAndRoles(selectedUsersAndRole, model.CurrentPhase);
					}
				}
				else
				{
					permissions = GetPermissions(model);
				}

				if (model.IsFolder)
				{
					#region Folder

					//Create/Update/Delete folder permissions
					accountId = CUDFolderPermissions(model.ID, permissions, loggedUser, loggedAccountId, loggedUserRole, context);

					#endregion
				}
				else
				{
					#region Approval

					//Create/Update/Delete approval permissions
					approvalPermissions = CUDApprovalPermissions(model, permissions, out accountId, loggedUser, context);
					instantNotification = approvalPermissions.DashboardNotification;

					#endregion
				}

				if (model.SendEmails)
				{
					var tempNotificationItems = NotificationServiceBL.CreateTmpNotifForInternals(model.ID, approvalPermissions.NewCollaborators, loggedUser, NotificationType.ApprovalWasShared, model.OptionalMessage);
					NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);

					if (!model.IsFolder && accountId > 0)
					{
						var sendAndShareModel = new SendAndShareModel
						{
							approvalId = model.ID,
							currPhase = model.CurrentPhase,
							externalUsers = model.ExternalUsers,
							externalEmails = string.Empty,
							context = context,
							loggedUser = loggedUser,
							loggedAccountId = accountId,
							sendEmails = true
						};

						if (SendAndShare(sendAndShareModel).Count == 0)
						{
							//send mail to logged user if it hasn't been sent from share method
							if ((approvalPermissions.NewCollaborators.Count > 0))
							{
								var ownTempNotificationItem = NotificationServiceBL.CreateTmpNotifForOwnAndInternals(model.ID, null, loggedUser, NotificationType.ApprovalWasShared, model.OptionalMessage);
								NotificationServiceBL.PutTmpNotifInQueue(ownTempNotificationItem);
							}
						}
					}
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGrantDenyPermissions, ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGrantDenyPermissions, ex);
			}
			return instantNotification;
		}

        public static DashboardInstantNotification GrantDenyPermissionsForMultiAccess(PermissionsModel model, DbContextBL context,
            GMGColorDAL.User loggedUser, int loggedAccountId, Role.RoleName loggedUserRole, bool serialize)
        {
            int accountId = 0;
            var instantNotification = new DashboardInstantNotification();
            var approvalPermissions = new GrantDenyApprovalPermission();
            try
            {
                var permissions = new PermissionsUsersAndRolesModel();
                var newCollaborators = new List<int>();

                if (serialize)
                {
                    if (model.Groups != null)
                        permissions.Groups = model.Groups.Split(',').ToList()
                            .Select(m => new GroupPermissions() { Group = Int32.Parse(m), Phase = (int?)null })
                            .ToList();

                    if (model.Users != null)
                    {
                        var selectedUsersAndRole = model.Users.Split(',').ToList();
                        permissions.Users = GetUsersAndRoles(selectedUsersAndRole, model.CurrentPhase);
                    }
                }
                else
                {
                    permissions = GetPermissions(model);
                }

                    #region Approval

                    //Create/Update/ approval permissions
                    approvalPermissions = ApprovalPermissionsForMultiAccess(model, permissions, out accountId, loggedUser, context);
                    instantNotification = approvalPermissions.DashboardNotification;

                    #endregion

                if (model.SendEmails)
                {
                    var tempNotificationItems = NotificationServiceBL.CreateTmpNotifForInternals(model.ID, approvalPermissions.NewCollaborators, loggedUser, NotificationType.ApprovalWasShared, model.OptionalMessage);
                    NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);

                    if (!model.IsFolder && accountId > 0)
                    {
                        var sendAndShareModel = new SendAndShareModel
                        {
                            approvalId = model.ID,
                            currPhase = model.CurrentPhase,
                            externalUsers = model.ExternalUsers,
                            externalEmails = string.Empty,
                            context = context,
                            loggedUser = loggedUser,
                            loggedAccountId = accountId,
                            sendEmails = true
                        };

                        if (SendAndShareMultiAccessApproval(sendAndShareModel).Count == 0)
                        {
                            //send mail to logged user if it hasn't been sent from share method
                            if ((approvalPermissions.NewCollaborators.Count > 0))
                            {
                                var ownTempNotificationItem = NotificationServiceBL.CreateTmpNotifForOwnAndInternals(model.ID, null, loggedUser, NotificationType.ApprovalWasShared, model.OptionalMessage);
                                NotificationServiceBL.PutTmpNotifInQueue(ownTempNotificationItem);
                            }
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGrantDenyPermissions, ex);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGrantDenyPermissions, ex);
            }
            return instantNotification;
        }

        public static void CollaborateAPIGrantDenyPermissions(List<ApprovalCollaborator> approvalCollaborators,
			Approval approval, DbContextBL context, GMGColorDAL.User loggedUser, Role.RoleName loggedUserRole)
		{
			try
			{
				var newCollaborators = new List<int>();

				#region Approval Collaborators

				using (var ts = new TransactionScope())
				{
					foreach (ApprovalCollaborator approvalCollaborator in approvalCollaborators)
					{
						ApprovalCollaborator collaborator = (from ac in context.ApprovalCollaborators
															 where ac.Approval == approval.ID && ac.Collaborator == approvalCollaborator.Collaborator &&
																   ac.Phase == approval.CurrentPhase
															 select ac).FirstOrDefault();

						bool isNewApprover = (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer ==
											  ApprovalCollaboratorRole.GetRole(approvalCollaborator.ApprovalCollaboratorRole, context));

						if (collaborator != null)
						{
							if (approval.ApprovalCollaborators.Any(ac => ac.Collaborator == approvalCollaborator.Collaborator &&
																		 ac.ApprovalCollaboratorRole !=
																		 approvalCollaborator.ApprovalCollaboratorRole))
							{
								bool isOldApprover = (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer ==
													  ApprovalCollaboratorRole.GetRole(collaborator.ApprovalCollaboratorRole, context));

								collaborator.AssignedDate = DateTime.UtcNow;
								collaborator.ApprovalCollaboratorRole = approvalCollaborator.ApprovalCollaboratorRole;

								// Add
								if (isNewApprover && !isOldApprover)
								{
									var objDecision = new ApprovalCollaboratorDecision
									{
										Approval = approval.ID,
										Collaborator = approvalCollaborator.Collaborator,
										AssignedDate = DateTime.UtcNow,
										Phase = approvalCollaborator.Phase
									};

									context.ApprovalCollaboratorDecisions.Add(objDecision);
								}
								// Remove pdm
								else if (!isNewApprover && isOldApprover)
								{
									if (collaborator.Approval1.PrimaryDecisionMaker != null &&
										collaborator.Approval1.PrimaryDecisionMaker ==
										collaborator.Collaborator)
									{
										collaborator.Approval1.PrimaryDecisionMaker = null;
									}
								}
							}
						}
						else
						{
							context.ApprovalCollaborators.Add(approvalCollaborator);

							if (isNewApprover)
							{
								var objDecision = new ApprovalCollaboratorDecision
								{
									Approval = approval.ID,
									Collaborator = approvalCollaborator.Collaborator,
									AssignedDate = DateTime.UtcNow,
									Phase = approvalCollaborator.Phase
								};

								context.ApprovalCollaboratorDecisions.Add(objDecision);
							}

							newCollaborators.Add(approvalCollaborator.Collaborator);
						}
					}

					// Lock the Approval if all the approval collaborators have added Decisions..
					if ((approval.ApprovalCollaboratorDecisions.Count > 0) &&
						(approval.LockProofWhenAllDecisionsMade) &&
						(approval.ApprovalCollaboratorDecisions.Count(m => m.Decision.GetValueOrDefault() == 0) ==
						 0))
					{
						approval.IsLocked = true;
					}

					context.SaveChanges();
					ts.Complete();
				}

				#endregion

				#region E-mails

				foreach (var newCollaborator in newCollaborators)
				{
					NotificationServiceBL.CreateNotification(new ApprovalWasShared
					{
						EventCreator = loggedUser.ID,
						InternalRecipient = newCollaborator,
						ApprovalsIds = new[] { approval.ID }
					},
						loggedUser,
						context);
				}

				if (loggedUser.Account > 0)
				{
					//send mail to logged user if it hasn't been sent from share method
					if ((newCollaborators.Count > 0 && loggedUser != null && loggedUser.IncludeMyOwnActivity))
					{
						NotificationServiceBL.CreateNotification(new ApprovalWasShared
						{
							EventCreator = loggedUser.ID,
							InternalRecipient = loggedUser.ID,
							ApprovalsIds = new[] { approval.ID }
						},
							loggedUser,
							context);
					}
				}

				#endregion
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGrantDenyPermissions, ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotGrantDenyPermissions, ex);
			}
		}

		public class SendAndShareModel
		{
			public int approvalId { get; set; }
			public int? currPhase { get; set; }
			public string externalUsers { get; set; }
			public string externalEmails { get; set; }
			public ApprovalWorkflowPhasesInfo workflowPhases { get; set; }
			public DbContextBL context { get; set; }
			public GMGColorDAL.User loggedUser { get; set; }
			public int loggedAccountId { get; set; }
			public bool sendEmails { get; set; }
			public bool AllowUsersToDownload { get; set; }
			public string pdmEmail { get; set; }

			public SendAndShareModel()
			{
				AllowUsersToDownload = true;
				pdmEmail = "";

				currPhase = null;
				externalEmails = null;
				workflowPhases = null;
			}
		}

		public static List<SharedApproval> SendAndShare(SendAndShareModel sendAndShareModel)
		{
			try
			{
				ShareModel shareModel = new ShareModel();
				shareModel.IsFolder = false;
				shareModel.Approval = sendAndShareModel.approvalId;
				shareModel.ExternalEmails = sendAndShareModel.externalEmails;
				shareModel.ExternalUsers = sendAndShareModel.externalUsers;
				shareModel.IsShareDownloadURL = sendAndShareModel.AllowUsersToDownload;
				shareModel.IsShareURL = true;
				shareModel.Message = string.Empty;
				shareModel.SendEmails = sendAndShareModel.sendEmails;
				shareModel.ShowPanel = true; // Send from Permissions
				shareModel.CurrentPhase = sendAndShareModel.currPhase;
				if (!String.IsNullOrEmpty(sendAndShareModel.pdmEmail))
				{
					shareModel.PDMEmail = sendAndShareModel.pdmEmail;
				}
				return ShareApproval(shareModel, sendAndShareModel.context, sendAndShareModel.loggedUser,
					sendAndShareModel.loggedAccountId, sendAndShareModel.workflowPhases);
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSendAndShare, ex);
			}
		}

        public static List<SharedApproval> SendAndShareMultiAccessApproval(SendAndShareModel sendAndShareModel)
        {
            try
            {
                ShareModel shareModel = new ShareModel();
                shareModel.IsFolder = false;
                shareModel.Approval = sendAndShareModel.approvalId;
                shareModel.ExternalEmails = sendAndShareModel.externalEmails;
                shareModel.ExternalUsers = sendAndShareModel.externalUsers;
                shareModel.IsShareDownloadURL = sendAndShareModel.AllowUsersToDownload;
                shareModel.IsShareURL = true;
                shareModel.Message = string.Empty;
                shareModel.SendEmails = sendAndShareModel.sendEmails;
                shareModel.ShowPanel = true; // Send from Permissions
                shareModel.CurrentPhase = sendAndShareModel.currPhase;
                if (!String.IsNullOrEmpty(sendAndShareModel.pdmEmail))
                {
                    shareModel.PDMEmail = sendAndShareModel.pdmEmail;
                }
                return ShareApprovalFromMultiAccess(shareModel, sendAndShareModel.context, sendAndShareModel.loggedUser,
                    sendAndShareModel.loggedAccountId, sendAndShareModel.workflowPhases);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSendAndShare, ex);
            }
        }

        /// <summary>
        /// Update permissions for external users
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        /// <param name="loggedUser"></param>
        /// <param name="loggedAccount"></param>
        /// <param name="workflowPhases"></param>
        /// <returns>Whether new external users have been added or not</returns>
        public static List<SharedApproval> ShareApproval(ShareModel model, DbContextBL context, GMGColorDAL.User loggedUser,
			int loggedAccount, ApprovalWorkflowPhasesInfo workflowPhases = null)
		{
			List<SharedApproval> lstSharedApprovals = new List<SharedApproval>();
			Approval objApproval = (from a in context.Approvals where a.ID == model.Approval select a).FirstOrDefault();
			try
			{
				if (objApproval != null)
				{
					// Get External Users
					var permissions = GetExternalPermissions(model.ExternalUsers, model.ExternalEmails, model.CurrentPhase, workflowPhases);

					// Get Removed Existing External Users
					List<int> deleteExternals = new List<int>();

					if (model.ShowPanel) // Send from Permissions
					{
						var existingExternals = objApproval.SharedApprovals.Select(m => m.ExternalCollaborator).ToList();
						deleteExternals = (permissions.ExternalUsers != null && permissions.ExternalUsers.Any())
							? existingExternals.Except(permissions.ExternalUsers.Select(u => u.ID).ToList()).ToList()
							: existingExternals;
					}

					using (var ts = new TransactionScope())
					{
						#region Add Externals & Decision

						lstSharedApprovals.AddRange(AddExternalCollaboratorsAndDecisions(permissions.NewExternalUsers, model,
							ref objApproval, loggedUser.ID, loggedAccount, context));

						#endregion

						#region Update Externals & Decision

						lstSharedApprovals.AddRange(
							UpdateExternalCollaboratorsAndDecisions(permissions.ExternalUsers, model, ref objApproval, loggedUser , context));

						#endregion

						#region Delete Externals & Decision

						if (model.ShowPanel && deleteExternals.Count > 0) // Sent from Permissions
						{
							var sharedApproval = (from sa in context.SharedApprovals
												  where
												  deleteExternals.Contains(sa.ExternalCollaborator) &&
												  sa.Approval == objApproval.ID
												  select sa).ToList();

							if (sharedApproval.Any())
							{
								//Delete shared approvals
								context.SharedApprovals.RemoveRange(sharedApproval);

								var allApproversAndReviewers = sharedApproval
									.Where(sa => ApprovalCollaboratorRole.GetRole(sa.ApprovalCollaboratorRole, context) ==
												 ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer)
									.Select(exc => exc.ExternalCollaborator);

								//Delete collaborators decision for current approval
								context.ApprovalCollaboratorDecisions.RemoveRange(
									context.ApprovalCollaboratorDecisions.Where(
										acd => allApproversAndReviewers.Contains(acd.ExternalCollaborator.Value) && acd.Approval == objApproval.ID));
							}
						}

						#endregion

						context.SaveChanges();
						ts.Complete();
					}

                     if (model.SendEmails)
					{
						var externalCollaborators = lstSharedApprovals.Select(sa => sa.ExternalCollaborator).ToList();

						var tempNotificationItems = NotificationServiceBL.CreateTmpNotifForExternals(model.Approval, externalCollaborators, loggedUser, NotificationType.ApprovalWasShared, model.Message);
						NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);

						if (lstSharedApprovals.Count > 0)
						{
							// code inserted to be compatible with previews code. TO DO: remove from this method code related to internal collaborators
							var ownTempNotificationItem = NotificationServiceBL.CreateTmpNotifForOwnAndInternals(model.Approval, null, loggedUser, NotificationType.ApprovalWasShared, model.Message);
							NotificationServiceBL.PutTmpNotifInQueue(ownTempNotificationItem);
						}
					}
					objApproval.SOADState = 0; //TODO replace with enum
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(String.Format(ExceptionBLMessages.CouldNotShareApproval, loggedAccount, loggedUser.ID, objApproval.FileName), ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(
					String.Format(ExceptionBLMessages.CouldNotShareApproval, loggedAccount, loggedUser.ID, objApproval.FileName), ex);
			}

			return lstSharedApprovals.Where(p => p.Phase == objApproval.CurrentPhase).ToList();
		}


        public static List<SharedApproval> ShareApprovalFromMultiAccess(ShareModel model, DbContextBL context, GMGColorDAL.User loggedUser,
            int loggedAccount, ApprovalWorkflowPhasesInfo workflowPhases = null)
        {
            List<SharedApproval> lstSharedApprovals = new List<SharedApproval>();
            Approval objApproval = (from a in context.Approvals where a.ID == model.Approval select a).FirstOrDefault();
            try
            {
                if (objApproval != null)
                {
                    // Get External Users
                    var permissions = GetExternalPermissions(model.ExternalUsers, model.ExternalEmails, model.CurrentPhase, workflowPhases);

                    if (model.ShowPanel) // Send from Permissions
                    {
                        var existingExternals = objApproval.SharedApprovals.Select(m => m.ExternalCollaborator).ToList();
                    }

                    using (var ts = new TransactionScope())
                    {
                        #region Add Externals & Decision

                        lstSharedApprovals.AddRange(AddExternalCollaboratorsAndDecisions(permissions.NewExternalUsers, model,
                            ref objApproval, loggedUser.ID, loggedAccount, context));

                        #endregion

                        #region Update Externals & Decision

                        lstSharedApprovals.AddRange(
                            UpdateExternalCollaboratorsAndDecisions(permissions.ExternalUsers, model, ref objApproval, loggedUser, context));

                        #endregion

                        context.SaveChanges();
                        ts.Complete();
                    }

                    if (model.SendEmails)
                    {
                        var externalCollaborators = lstSharedApprovals.Select(sa => sa.ExternalCollaborator).ToList();

                        var tempNotificationItems = NotificationServiceBL.CreateTmpNotifForExternals(model.Approval, externalCollaborators, loggedUser, NotificationType.ApprovalWasShared, model.Message);
                        NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);

                        if (lstSharedApprovals.Count > 0)
                        {
                            // code inserted to be compatible with previews code. TO DO: remove from this method code related to internal collaborators
                            var ownTempNotificationItem = NotificationServiceBL.CreateTmpNotifForOwnAndInternals(model.Approval, null, loggedUser, NotificationType.ApprovalWasShared, model.Message);
                            NotificationServiceBL.PutTmpNotifInQueue(ownTempNotificationItem);
                        }
                    }
                    objApproval.SOADState = 0; //TODO replace with enum
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(String.Format(ExceptionBLMessages.CouldNotShareApproval, loggedAccount, loggedUser.ID, objApproval.FileName), ex);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(
                    String.Format(ExceptionBLMessages.CouldNotShareApproval, loggedAccount, loggedUser.ID, objApproval.FileName), ex);
            }

            return lstSharedApprovals.Where(p => p.Phase == objApproval.CurrentPhase).ToList();
        }


        /// <summary>
        /// Update permissions for external users
        /// </summary>
        /// <param name="model"></param>
        /// <param name="context"></param>
        /// <param name="loggedUser"></param>
        /// <param name="loggedAccount"></param>
        /// <param name="workflowPhases"></param>
        /// <returns>Whether new external users have been added or not</returns>
        public static void ShareApprovalFromApi(PermissionsUsersAndRolesModel permissions, int approvalId,
			DbContextBL context, GMGColorDAL.User loggedUser, int loggedAccount, int? currentPhase = null)
		{
			try
			{
				List<SharedApproval> lstSharedApprovals = new List<SharedApproval>();

				using (var ts = new TransactionScope())
				{
					#region Add Externals & Decision

					lstSharedApprovals.AddRange(AddApiExternalCollaboratorsAndDecisions(permissions.NewExternalUsers, approvalId,
						loggedUser.ID, loggedAccount, context));

					#endregion

					#region Update Externals & Decision

					lstSharedApprovals.AddRange(
						UpdateApiExternalCollaboratorsAndDecisions(permissions.ExternalUsers, approvalId, context));

					#endregion

					context.SaveChanges();
					ts.Complete();
				}

				lstSharedApprovals = lstSharedApprovals.Where(p => p.Phase == currentPhase).ToList();

				foreach (GMGColorDAL.SharedApproval objShared in lstSharedApprovals)
				{
					NotificationServiceBL.CreateNotification(new ApprovalWasShared()
					{
						EventCreator = loggedUser.ID,
						ExternalRecipient = objShared.ExternalCollaborator,
						OptionalMessage = "",
						ApprovalsIds = new[] { approvalId }
					},
						loggedUser,
						context
					);
				}

				//send mail to logged user
				if (lstSharedApprovals.Count > 0 && loggedUser != null && loggedUser.IncludeMyOwnActivity)
				{
					NotificationServiceBL.CreateNotification(new ApprovalWasShared
					{
						EventCreator = loggedUser.ID,
						InternalRecipient = loggedUser.ID,
						OptionalMessage = "",
						ApprovalsIds = new[] { approvalId }
					},
						loggedUser,
						context
					);
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(String.Format(ExceptionBLMessages.CouldNotShareApprovalFromApi, loggedAccount, loggedUser.ID),
					ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(String.Format(ExceptionBLMessages.CouldNotShareApprovalFromApi, loggedAccount, loggedUser.ID),
					ex);
			}
		}

		public static void SetApprovalPermission(Approval approval, PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles, GMGColorContext context)
		{
			try
			{
				SetUsersPermissionsPerf(context, approval, permissionsUsersAndRoles);
				SetGroupsPermissionsPerf(context, approval, permissionsUsersAndRoles);
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
			}
		}

        public static void SetProjectFolderApprovalPermission(Approval approval, PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles, GMGColorContext context)
        {
            try
            {
                SetUsersPermissionsPerfForProjectApprovals(context, approval, permissionsUsersAndRoles);
                SetGroupsPermissionsPerfForProjectApprovals(context, approval, permissionsUsersAndRoles);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (DbEntityValidationException ex)
            {
                DALUtils.LogDbEntityValidationException(ex);
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
            }
        }


        public static void SetApprovalPermissionFromFTP(PermissionsModel model, int approvalId, GMGColorContext context)
		{
			try
			{
				PermissionsUsersAndRolesModel permissionsUsersAndRoles = GetPermissions(model);
				SetUsersPermissions(context, approvalId, permissionsUsersAndRoles);
				SetGroupsPermissions(context, approvalId, permissionsUsersAndRoles);

				context.SaveChanges();
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
			}
		}

		public static PermissionsUsersAndRolesModel GetPermissions(ApprovalWorkflowPhasesInfo workflowPhases)
		{
			PermissionsUsersAndRolesModel permissions = new PermissionsUsersAndRolesModel();

			if (workflowPhases != null && workflowPhases.ApprovalWorkflowPhases.Count > 0)
			{
				foreach (var workflowPhase in workflowPhases.ApprovalWorkflowPhases)
				{
					permissions.Users.AddRange(GetUsersAndRoles(workflowPhase.PhaseCollaborators.CollaboratorsWithRole, workflowPhase.Phase));

					permissions.Groups.AddRange(workflowPhase.PhaseCollaborators.Groups.Select(m => new GroupPermissions { Group = m, Phase = (int?)workflowPhase.Phase }).ToList());

					permissions.ExternalUsers.AddRange(GetExternalUsersAndRoles(workflowPhase.PhaseCollaborators.ExternalCollaboratorsWithRole, workflowPhase.Phase));
				}
			}

			return permissions;
		}
		
		public static PermissionsUsersAndRolesModelPerf GetPermissionsPerf(ApprovalWorkflowPhasesInfoPerf workflowPhases)
		{
			PermissionsUsersAndRolesModelPerf permissions = new PermissionsUsersAndRolesModelPerf();

			if (workflowPhases != null && workflowPhases.ApprovalWorkflowPhases.Count > 0)
			{
				foreach (var workflowPhase in workflowPhases.ApprovalWorkflowPhases)
				{
					permissions.Users.AddRange(GetUsersAndRolesPerf(workflowPhase.PhaseCollaborators.CollaboratorsWithRole, workflowPhase.Phase));

					permissions.Groups.AddRange(workflowPhase.PhaseCollaborators.Groups.Select(m => new GroupPermissionsPerf { Group = m, Phase = workflowPhase.Phase }).ToList());

					permissions.ExternalUsers.AddRange(GetExternalUsersAndRolesPerf(workflowPhase.PhaseCollaborators.ExternalCollaboratorsWithRole, workflowPhase.Phase));
				}
			}

			return permissions;
		}


		public static PermissionsUsersAndRolesModel GetPermissions(PermissionsModel model)
		{
			PermissionsUsersAndRolesModel permissions = new PermissionsUsersAndRolesModel();

			permissions.Groups = model.SelectedUsersAndGroups.Where(m => m.IsChecked == true && m.IsExternal == false && m.IsGroup == true)
				.Select(m => new GroupPermissions { Group = m.ID, Phase = (int?)null })
				.ToList();

			List<string> selectedUsersAndRole = model.SelectedUsersAndGroups.Where(m => m.IsChecked == true && m.IsExternal == false && m.IsGroup == false)
				.Select(m => m.ID + "|" + m.ApprovalRole)
				.ToList();
			permissions.Users = GetUsersAndRoles(selectedUsersAndRole);

			List<string> selectedExternalUsersAndRole = GetSelectedExternalUsersAndRoles(model.ExternalUsers);
			permissions.ExternalUsers = GetExternalUsersAndRoles(selectedExternalUsersAndRole);

			return permissions;
		}

		public static PermissionsUsersAndRolesModelPerf GetPermissionsPerf(PermissionsModel model)
		{
			PermissionsUsersAndRolesModelPerf permissions = new PermissionsUsersAndRolesModelPerf();

			permissions.Groups = model.SelectedUsersAndGroups.Where(m => m.IsChecked == true && m.IsExternal == false && m.IsGroup == true)
				.Select(m => new GroupPermissionsPerf { Group = m.ID, Phase = null })
				.ToList();

			List<string> selectedUsersAndRole = model.SelectedUsersAndGroups.Where(m => m.IsChecked == true && m.IsExternal == false && m.IsGroup == false)
				.Select(m => m.ID + "|" + m.ApprovalRole)
				.ToList();
			permissions.Users = GetUsersAndRolesPerf(selectedUsersAndRole);

			List<string> selectedExternalUsersAndRole = GetSelectedExternalUsersAndRoles(model.ExternalUsers);
			permissions.ExternalUsers = GetExternalUsersAndRolesPerf(selectedExternalUsersAndRole);

			return permissions;
		}

		/// <summary>
		/// Returns an model with formated external users details
		/// </summary>
		/// <param name="externalUsersAndRoles">Selected existing users and roles</param>
		/// <param name="externalUsersAndEmails">New created external users with roles and emails</param>
		/// <param name="workflowPhases">Phases and their collaborators if any</param>
		/// <returns></returns>
		public static PermissionsUsersAndRolesModel GetExternalPermissions(string externalUsersAndRoles, string externalUsersAndEmails, int? currentPhase, ApprovalWorkflowPhasesInfo workflowPhases = null)
		{
			PermissionsUsersAndRolesModel permissions = new PermissionsUsersAndRolesModel();

			if (workflowPhases != null && workflowPhases.ApprovalWorkflowPhases.Count > 0)
			{
				foreach (var workflowPhase in workflowPhases.ApprovalWorkflowPhases)
				{
					permissions.ExternalUsers.AddRange(GetExternalUsersAndRoles(workflowPhase.PhaseCollaborators.ExternalCollaboratorsWithRole, workflowPhase.Phase));
				}
				currentPhase = workflowPhases.ApprovalWorkflowPhases[0].Phase;
			}
			else
			{
				List<string> selectedExternalUsersAndRole = GetSelectedExternalUsersAndRoles(externalUsersAndRoles);
				permissions.ExternalUsers = GetExternalUsersAndRoles(selectedExternalUsersAndRole, currentPhase);
			}
			permissions.NewExternalUsers = GetNewExternalUsersAndEmails(externalUsersAndEmails, currentPhase);

			return permissions;
		}

		public static PermissionsUsersAndRolesModelPerf GetExternalPermissionsPerf(string externalUsersAndRoles, string externalUsersAndEmails, ApprovalJobPhase currentPhase, ApprovalWorkflowPhasesInfoPerf workflowPhases = null)
		{
			PermissionsUsersAndRolesModelPerf permissions = new PermissionsUsersAndRolesModelPerf();

			if (workflowPhases != null && workflowPhases.ApprovalWorkflowPhases.Count > 0)
			{
				foreach (var workflowPhase in workflowPhases.ApprovalWorkflowPhases)
				{
					permissions.ExternalUsers.AddRange(GetExternalUsersAndRolesPerf(workflowPhase.PhaseCollaborators.ExternalCollaboratorsWithRole, workflowPhase.Phase));
				}
				currentPhase = workflowPhases.ApprovalWorkflowPhases[0].Phase;
			}
			else
			{
				List<string> selectedExternalUsersAndRole = GetSelectedExternalUsersAndRoles(externalUsersAndRoles);
				permissions.ExternalUsers = GetExternalUsersAndRolesPerf(selectedExternalUsersAndRole, currentPhase);
			}
			permissions.NewExternalUsers = GetNewExternalUsersAndEmailsPerf(externalUsersAndEmails, currentPhase);

			return permissions;
		}

		public static List<UserPermissions> GetUsersAndRoles(List<string> selectedUsersAndRole, int? phase = null)
		{
			var permissions = selectedUsersAndRole.Select(s => s.Split('|')).Select(t =>
				 new UserPermissions
				 {
					 ID = t.ElementAt(0).ToInt().GetValueOrDefault(),
					 Role = t.ElementAt(1).ToInt().GetValueOrDefault(),
					 Phase = phase
				 }).ToList();

			return permissions;
		}

		public static List<ExternalUserPermissions> GetExternalUsersAndRoles(List<string> selectedExternalsWithRole, int? phase = null)
		{
			var permissions = selectedExternalsWithRole.Select(s => s.Split('|')).Select(
				r =>
					new ExternalUserPermissions
					{
						ID = r.ElementAt(0).ToInt().GetValueOrDefault(),
						Role = r.ElementAt(1).ToInt().GetValueOrDefault(),
						Phase = phase
					}).ToList();

			return permissions;
		}

		private static List<NewExternalUserPermissions> GetNewExternalUsersAndEmails(string emailsWithRoles, int? phase = null)
		{
			List<string> selectedEmailsWithRole = String.IsNullOrEmpty(emailsWithRoles)
															? new List<string>()
															: emailsWithRoles.Split(',').ToList();

			var permissions = selectedEmailsWithRole.Select(s => s.Split('|'))
				.Select(r => new NewExternalUserPermissions()
				{
					Role = r.ElementAt(1).ToInt().GetValueOrDefault(),
					Phase = phase,
					FullName = r.ElementAt(2),
					Email = r.ElementAt(0),
					Locale = r.ElementAt(3).ToInt().GetValueOrDefault()
				}).ToList();

			return permissions;
		}

		public static List<UserPermissionsPerf> GetUsersAndRolesPerf(List<string> selectedUsersAndRole, ApprovalJobPhase phase = null)
		{
			var permissions = selectedUsersAndRole.Select(s => s.Split('|')).Select(t =>
				 new UserPermissionsPerf
				 {
					 ID = t.ElementAt(0).ToInt().GetValueOrDefault(),
					 Role = t.ElementAt(1).ToInt().GetValueOrDefault(),
					 Phase = phase
				 }).ToList();

			return permissions;
		}

		public static List<ExternalUserPermissionsPerf> GetExternalUsersAndRolesPerf(List<string> selectedExternalsWithRole, ApprovalJobPhase phase = null)
		{
			var permissions = selectedExternalsWithRole.Select(s => s.Split('|')).Select(
				r =>
					new ExternalUserPermissionsPerf
					{
						ID = r.ElementAt(0).ToInt().GetValueOrDefault(),
						Role = r.ElementAt(1).ToInt().GetValueOrDefault(),
						Phase = phase
					}).ToList();

			return permissions;
		}

		public static List<NewExternalUserPermissionsPerf> GetNewExternalUsersAndEmailsPerf(string emailsWithRoles, ApprovalJobPhase phase = null)
		{
			List<string> selectedEmailsWithRole = String.IsNullOrEmpty(emailsWithRoles)
															? new List<string>()
															: emailsWithRoles.Split(',').ToList();

			var permissions = selectedEmailsWithRole.Select(s => s.Split('|'))
				.Select(r => new NewExternalUserPermissionsPerf()
				{
					Role = r.ElementAt(1).ToInt().GetValueOrDefault(),
					Phase = phase,
					FullName = r.ElementAt(2),
					Email = r.ElementAt(0),
					Locale = r.ElementAt(3).ToInt().GetValueOrDefault()
				}).ToList();

			return permissions;
		}



		public static void SetApprovalPhasePermissions(PermissionsModel model, int approvalPhaseId, bool isUpdateAction, int loggedUserId, DbContextBL context)
		{
			try
			{
				PermissionsUsersAndRolesModel permissionsUsersAndRoles = GetPermissions(model);

				using (TransactionScope ts = new TransactionScope())
				{
					if (isUpdateAction)
					{
						#region Update internal users

						var selectedCollaborators = permissionsUsersAndRoles.Users.Select(u => u.ID).ToList();

						var collaboratorsToDelete = context.ApprovalPhaseCollaborators.Where(apc => apc.Phase == approvalPhaseId && !selectedCollaborators.Contains(apc.Collaborator)).ToList();
						var collaboratorsToUpdate = context.ApprovalPhaseCollaborators.Where(apc => apc.Phase == approvalPhaseId && selectedCollaborators.Contains(apc.Collaborator)).ToList();

						foreach (var user in permissionsUsersAndRoles.Users)
						{
							var collaborator = collaboratorsToUpdate.FirstOrDefault(c => c.Collaborator == user.ID);
							if (collaborator != null)
							{
								collaborator.ApprovalPhaseCollaboratorRole = user.Role;
							}
							else
							{
								var objCollaborator = new ApprovalPhaseCollaborator();
								objCollaborator.Phase = approvalPhaseId;
								objCollaborator.Collaborator = user.ID;
								objCollaborator.ApprovalPhaseCollaboratorRole = user.Role;
								context.ApprovalPhaseCollaborators.Add(objCollaborator);
							}
						}
						context.ApprovalPhaseCollaborators.RemoveRange(collaboratorsToDelete);

						#endregion

						#region Update groups

						var selectedGroups = permissionsUsersAndRoles.Groups.Select(u => u.Group).ToList();
						var groupsToDetele = context.ApprovalPhaseCollaboratorGroups.Where(apcg => apcg.Phase == approvalPhaseId && !selectedGroups.Contains(apcg.CollaboratorGroup)).ToList();

						foreach (var group in permissionsUsersAndRoles.Groups)
						{
							if (groupsToDetele.All(gd => gd.CollaboratorGroup != group.Group))
							{
								var objCollaboratorGroup = new ApprovalPhaseCollaboratorGroup();
								objCollaboratorGroup.Phase = approvalPhaseId;
								objCollaboratorGroup.CollaboratorGroup = group.Group;
								objCollaboratorGroup.CreatedBy = objCollaboratorGroup.ModifiedBy = loggedUserId;
								objCollaboratorGroup.ModifiedDate = objCollaboratorGroup.CreatedDate = DateTime.UtcNow;
								context.ApprovalPhaseCollaboratorGroups.Add(objCollaboratorGroup);
							}
						}
						context.ApprovalPhaseCollaboratorGroups.RemoveRange(groupsToDetele);

						#endregion

						#region Update external users

						var selectedExternalUsers = permissionsUsersAndRoles.ExternalUsers.Select(ex => ex.ID).ToList();

						var externalToDelete = context.SharedApprovalPhases.Where(spc => spc.Phase == approvalPhaseId && !selectedExternalUsers.Contains(spc.ExternalCollaborator)).ToList();
						var externalToUpdate = context.SharedApprovalPhases.Where(spc => spc.Phase == approvalPhaseId && selectedExternalUsers.Contains(spc.ExternalCollaborator)).ToList();

						foreach (var user in permissionsUsersAndRoles.ExternalUsers)
						{
							var collaborator = externalToUpdate.FirstOrDefault(c => c.ExternalCollaborator == user.ID);
							if (collaborator != null)
							{
								collaborator.ApprovalPhaseCollaboratorRole = user.Role;
							}
							else
							{
								var objCollaborator = new SharedApprovalPhase();
								objCollaborator.Phase = approvalPhaseId;
								objCollaborator.ExternalCollaborator = user.ID;
								objCollaborator.CreatedBy = objCollaborator.ModifiedBy = loggedUserId;
								objCollaborator.ModifiedDate = objCollaborator.CreatedDate = DateTime.UtcNow;
								objCollaborator.ApprovalPhaseCollaboratorRole = user.Role;

								context.SharedApprovalPhases.Add(objCollaborator);
							}
						}
						context.SharedApprovalPhases.RemoveRange(externalToDelete);

						#endregion
					}
					else
					{
						#region Add Users

						foreach (var user in permissionsUsersAndRoles.Users)
						{
							var objCollaborator = new ApprovalPhaseCollaborator();
							objCollaborator.Phase = approvalPhaseId;
							objCollaborator.Collaborator = user.ID;
							objCollaborator.ApprovalPhaseCollaboratorRole = user.Role;
							objCollaborator.CreatedBy = objCollaborator.ModifiedBy = loggedUserId;
							objCollaborator.ModifiedDate = objCollaborator.CreatedDate = DateTime.UtcNow;

							context.ApprovalPhaseCollaborators.Add(objCollaborator);
						}

						foreach (var group in permissionsUsersAndRoles.Groups)
						{
							var objCollaboratorGroup = new ApprovalPhaseCollaboratorGroup();
							objCollaboratorGroup.Phase = approvalPhaseId;
							objCollaboratorGroup.CollaboratorGroup = group.Group;
							objCollaboratorGroup.CreatedBy = objCollaboratorGroup.ModifiedBy = loggedUserId;
							objCollaboratorGroup.ModifiedDate = objCollaboratorGroup.CreatedDate = DateTime.UtcNow;

							context.ApprovalPhaseCollaboratorGroups.Add(objCollaboratorGroup);
						}

						#endregion

						#region Save external phase collaborators

						foreach (var user in permissionsUsersAndRoles.ExternalUsers)
						{
							var objSharedPhase = new SharedApprovalPhase();
							objSharedPhase.Phase = approvalPhaseId;
							objSharedPhase.ExternalCollaborator = user.ID;
							objSharedPhase.ApprovalPhaseCollaboratorRole = user.Role;
							objSharedPhase.CreatedBy = objSharedPhase.ModifiedBy = loggedUserId;
							objSharedPhase.ModifiedDate = objSharedPhase.CreatedDate = DateTime.UtcNow;

							context.SharedApprovalPhases.Add(objSharedPhase);
						}

						#endregion
					}

					context.SaveChanges();
					ts.Complete();
				}
			}
			catch (ExceptionBL)
			{
				throw;
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPhasePermission, ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPhasePermission, ex);
			}

		}

		public static List<string> GetSelectedExternalUsersAndRoles(string externalUsers)
		{
			string strUsersAndRoles = GetFormatedExternalUsersDetails(externalUsers);

			return (String.IsNullOrEmpty(strUsersAndRoles) ? new List<string>()
														   : strUsersAndRoles.Split(',').ToList());
		}

		public static string GetFormatedExternalUsersDetails(string userDetails)
		{
			return !String.IsNullOrEmpty(userDetails)
											  ? userDetails.Replace("[", string.Empty).Replace("]", string.Empty).
															  Replace("\"", string.Empty).Trim()
											  : string.Empty;
		}

		/// <summary>
		/// Populates permission model
		/// </summary>
		/// <param name="loggedAccount">Logged account</param>
		/// <param name="loggedUser">Logged user</param>
		/// <param name="loggedUserCollaborateRole">The role of the logged user for collaborate dashboard</param>
		/// <param name="refresh"></param>
		/// <param name="showPanel">Embed to the page or not</param>
		/// <param name="isFromNewApprovalScreen">Open access popup is triggered from "add new approval" page or not</param>
		/// <param name="isPhaseScreen">Open access popup is triggered from add/edit approval phase page or not</param>
		/// <param name="context">Database context</param>
		/// <returns></returns>
		public static PermissionsModel BuildPermissionsModel(Account loggedAccount, GMGColorDAL.User loggedUser, Role.RoleName loggedUserCollaborateRole, bool refresh, bool showPanel, bool? isFromNewApprovalScreen, bool? isPhaseScreen, int? approvalId, DbContextBL context)
		{
			//showpanel is true only when permissions popup is accessed from new approval page
			var creator = isPhaseScreen.GetValueOrDefault() ? 0 : (showPanel ? loggedUser.ID : 0);
			var modifier = isPhaseScreen.GetValueOrDefault() ? 0 : (showPanel ? 0 : loggedUser.ID);
			var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccount.ID, 0, false, context);

			var model = new PermissionsModel
			{
				IsFolder = false,
				SendEmails = false,
				ShowPanel = showPanel,
				Domain = loggedAccount.IsCustomDomainActive ? loggedAccount.CustomDomain : loggedAccount.Domain,
				Owner = creator,
				PDM = 0,
				Creator = creator,
				Modifier = isFromNewApprovalScreen.GetValueOrDefault() ? creator : modifier,
				IsNewApproval = isFromNewApprovalScreen.GetValueOrDefault(),
				SelectedUsersAndGroups = GetCollaboratorsAndGroups(loggedAccount, loggedUser, loggedUserCollaborateRole, approvalId, context),
				Refresh = refresh,
				IsPhaseScreen = isPhaseScreen.GetValueOrDefault(),
				InheritParentFolderPermissions = collaborateSettings.InheritParentFolderPermissions,
				ShowRetoucherApprovalRole = collaborateSettings.EnableRetoucherWorkflow
			};

			return model;
		}

		public static List<PermissionsModel.UserOrUserGroup> GetCollaboratorsAndGroups(Account loggedAccount, GMGColorDAL.User loggedUser, Role.RoleName loggedUserCollaborateRole, int ?approvalId, DbContextBL context)
		{
			var collaboratorsAndGroups = new List<PermissionsModel.UserOrUserGroup>();
			bool isModerator = loggedUserCollaborateRole == Role.RoleName.AccountModerator;
            bool isManager = loggedUserCollaborateRole == Role.RoleName.AccountManager;

			//Get all account groups
			collaboratorsAndGroups.AddRange(GetAccountGroups(loggedAccount, loggedUser, isModerator, isManager, context));

            if (isManager)
            {
                List<int> groupIds = collaboratorsAndGroups.Select(t => t.ID).ToList();

                List<int> userIds = (from ugu in context.UserGroupUsers where groupIds.Contains(ugu.UserGroup) select ugu.User).ToList();

                var accountUsers = GetAccountUsers(loggedAccount, loggedUser.ID, context);
                accountUsers.RemoveAll(x => !userIds.Contains(x.ID));
                collaboratorsAndGroups.AddRange(accountUsers);
            }
            else
            {
                //Get all account internal users
                collaboratorsAndGroups.AddRange(GetAccountUsers(loggedAccount, loggedUser.ID, context));
            }

            if (!isModerator)
			{
				//Get all collaborate external users
				collaboratorsAndGroups.AddRange(GetAccountExternalUsers(loggedAccount, context));
			}

            if (approvalId > 0)
            {
                var ShowExternalUsers = (from a in context.Approvals where a.ID == approvalId && a.PrimaryDecisionMaker > 0 && a.PrivateAnnotations == true select a.ID).Any();
                if(ShowExternalUsers)
                {
                     collaboratorsAndGroups.RemoveAll(x => x.IsExternal);
                    return collaboratorsAndGroups.OrderByDescending(x => x.IsGroup).ThenBy(x => x.Name).ToList();
                }
                else
                {
                    return collaboratorsAndGroups.OrderBy(x => x.IsExternal).ThenByDescending(x => x.IsGroup).ThenBy(x => x.Name).ToList();
                }
            }
            else
            {
			return collaboratorsAndGroups.OrderBy(x => x.IsExternal).ThenByDescending(x => x.IsGroup).ThenBy(x => x.Name).ToList();
		}
		}

		/// <summary>
		/// Returns all collaborate external users from current account
		/// </summary>
		/// <param name="loggedAccount">Logged account</param>
		/// <param name="context">Database context</param>
		/// <returns></returns>
		private static List<PermissionsModel.UserOrUserGroup> GetAccountExternalUsers(Account loggedAccount, DbContextBL context)
		{
			var accountExternalUsers = new List<PermissionsModel.UserOrUserGroup>();
			List<ExternalCollaborator> externalCollaborators = (from o in loggedAccount.ExternalCollaborators
																where o.IsDeleted == false
																orderby o.ID
																select o).ToList();
            string showExternalUsersEmailId = (from asn in context.AccountSettings where asn.Account == loggedAccount.ID && asn.Name == "DisableExternalUsersEmailIndicator" select asn.Value).FirstOrDefault();

			int readonlyId = ApprovalCollaboratorRole.GetRole(ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly, context).ID;
			string localizedRoleName = ApprovalCollaboratorRole.GetLocalizedRoleName(ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly);

			foreach (ExternalCollaborator item in externalCollaborators)
			{
				string fullName = item.GivenName + " " + item.FamilyName;
				var externalCollaborator = new PermissionsModel.UserOrUserGroup
				{
					IsGroup = false,
					IsExternal = true,
					IsChecked = false,
					ID = item.ID,
					Count = 0,
					Members = string.Empty,
					Name = (!String.IsNullOrEmpty(fullName.Trim()) ? fullName : item.EmailAddress),
					Role = localizedRoleName,
					ApprovalRole = readonlyId,
                    EmailAddress = showExternalUsersEmailId == "True" ? "" : item.EmailAddress
                };
				accountExternalUsers.Add(externalCollaborator);
			}

			return accountExternalUsers;
		}

		/// <summary>
		/// Returns all internal users from current account
		/// </summary>
		/// <param name="loggedAccount">Logged account</param>
		/// <param name="userId">The id of the logged user</param>
		/// <param name="context">Database context</param>
		/// <returns></returns>
		public static List<PermissionsModel.UserOrUserGroup> GetAccountUsers(Account loggedAccount, int userId, DbContextBL context)
		{
            var accountUsers = loggedAccount.GetAccountCollaboratorAndGroupsView(loggedAccount.ID, userId, context);
            return accountUsers.Select(item => new PermissionsModel.UserOrUserGroup
			{
				IsGroup = item.IsGroup.GetValueOrDefault(),
				IsExternal = item.IsExternal.GetValueOrDefault(),
				IsChecked = item.IsChecked.GetValueOrDefault(),
				ID = item.ID,
				Count = item.Count,
				Members = item.Members,
				Name = item.Name,
				Role = item.RoleName,
				ApprovalRole = item.Role,
                EmailAddress = item.EmailAddress,
                PhotoPath = item.PhotoPath

            }).ToList();
		}

		/// <summary>
		/// Returns all groups from current account
		/// </summary>
		/// <param name="loggedAccount">Logged account</param>
		/// <param name="loggedUser">Logged users</param>
		/// <param name="isModerator"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public static List<PermissionsModel.UserOrUserGroup> GetAccountGroups(Account loggedAccount, GMGColorDAL.User loggedUser, bool isModerator, bool isManager, DbContextBL context)
		{
            int[] userstatus = { 2, 3, 4 };
            var accountGroups = new List<PermissionsModel.UserOrUserGroup>();
            var userGrpslst = (from grp in context.UserGroups
                               join ugu in context.UserGroupUsers on grp.ID equals ugu.UserGroup
                               join u in context.Users on ugu.User equals u.ID
                               join ur in context.UserRoles on ugu.User equals ur.User
                               join r in context.Roles on ur.Role equals r.ID
                               let appModuleID =
                                   (from am in context.AppModules where am.Key == 0 select am.ID)
                                   .FirstOrDefault()
                               where
                                                grp.Account == loggedAccount.ID && !userstatus.Contains(u.Status) &&
                                                    r.Key != "NON" &&
                                                    r.AppModule == appModuleID && u.DateLastLogin != null
                               select new
                               {
                                   GroupID = grp.ID,
                                   UserName = u.GivenName + " " + u.FamilyName
                               }).Distinct().ToList();


            foreach (UserGroup item in isModerator ? loggedUser.UserGroupUsers.Select(t => t.UserGroup1) : isManager ? loggedUser.UserGroupUserVisibilities.Count() == 0 ? loggedAccount.UserGroups : loggedUser.UserGroupUserVisibilities.Select(t => t.UserGroup1) : loggedAccount.UserGroups)
            {
                var groupUsers = userGrpslst.Where(x => x.GroupID == item.ID).Select(x => x).ToList();
                string members = (groupUsers.Count > 0) ? string.Empty : GMGColor.Resources.Resources.lblNoCollaborators;
                members = groupUsers.Select(x => x.UserName).Aggregate(members, (current, fullname) => current + (fullname + "<br/>"));
                var collaboratorGroup = new PermissionsModel.UserOrUserGroup
                {
                    IsGroup = true,
                    IsExternal = false,
                    IsChecked = false,
                    Count = groupUsers.Count,
                    ID = item.ID,
                    Members = members,
                    Name = item.Name,
                    Role = string.Empty,
                    ApprovalRole = 0
                };
                accountGroups.Add(collaboratorGroup);
            }
            return accountGroups;
        }

		/// <summary>
		/// Create/Update/Delete approval collaborators and groups for current approval
		/// </summary>
		/// <param name="model">Permission model</param>
		/// <param name="permissions">Selected users and groups</param>
		/// <param name="accountId">Approval account id</param>
		/// <param name="context">Database context</param>
		/// <returns></returns>
		private static GrantDenyApprovalPermission CUDApprovalPermissions(PermissionsModel model, PermissionsUsersAndRolesModel permissions, out int accountId, GMGColorDAL.User loggedUser, DbContextBL context)
		{

			var permissionsResponse = new GrantDenyApprovalPermission();
			accountId = 0;

			Approval objApproval = (from a in context.Approvals where a.ID == model.ID select a).FirstOrDefault();


			if (objApproval != null)
			{
				permissionsResponse.DashboardNotification = ChangeOwner(ref objApproval, model.Owner, loggedUser.ID, model.CurrentPhase, context);

				var existingGroups = (from acg in context.ApprovalCollaboratorGroups
									  where acg.Phase == objApproval.CurrentPhase && objApproval.ID == acg.Approval
									  select acg.CollaboratorGroup).ToList();

				var insertGroups = (permissions.Groups != null && permissions.Groups.Any()) ? permissions.Groups.Select(u => u.Group).ToList().Except(existingGroups).ToList() : new List<int>();
				var deletegroups = (permissions.Groups != null && permissions.Groups.Any()) ? existingGroups.Except(permissions.Groups.Select(u => u.Group).ToList()).ToList() : existingGroups;

				Dictionary<int, int> existingUsers = GetExistingUsersAndTheirRoles(objApproval.ID, model.CurrentPhase, context);

				List<int> deleteUsers = existingUsers.Where(eu => !permissions.Users.Select(u => u.ID).ToList().Contains(eu.Key)).Select(u => u.Key).ToList();
          
                using (var ts = new TransactionScope())
				{
					#region Add/Edit/Update Users, Groups & Decision

					foreach (var user in permissions.Users)
					{
						var objCollaborator = (from ac in context.ApprovalCollaborators
											   where ac.Approval == model.ID && ac.Collaborator == user.ID && ac.Phase == user.Phase
											   select ac).FirstOrDefault();

						bool isNewApprover = (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context));

						if (objCollaborator != null)
						{
							if (existingUsers.Any(nu => nu.Key == user.ID && nu.Value != user.Role))
							{
								bool isOldApprover = (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(objCollaborator.ApprovalCollaboratorRole, context));

								objCollaborator.AssignedDate = DateTime.UtcNow;
								objCollaborator.ApprovalCollaboratorRole = user.Role;

								// Add
								if (isNewApprover && !isOldApprover)
								{
									var objDecision = new ApprovalCollaboratorDecision
									{
										Approval = objApproval.ID,
										Collaborator = user.ID,
										AssignedDate = DateTime.UtcNow,
										Phase = user.Phase
									};
									context.ApprovalCollaboratorDecisions.Add(objDecision);
								}
								// Remove
								else if (!isNewApprover && isOldApprover)
								{
									var userId = user.ID;

									//Delete collaborators decision for current approval
									context.ApprovalCollaboratorDecisions.RemoveRange(context.ApprovalCollaboratorDecisions.Where(acd => acd.Collaborator == userId && acd.Approval == objApproval.ID));

									if (objCollaborator.Approval1.PrimaryDecisionMaker != null &&
										objCollaborator.Approval1.PrimaryDecisionMaker ==
										objCollaborator.Collaborator)
									{
										objCollaborator.Approval1.PrimaryDecisionMaker = null;
									}
								}
							}
							// else { No Changes }
						}
						// Add
						else
						{
							objCollaborator = new ApprovalCollaborator
							{
								Approval = objApproval.ID,
								Collaborator = user.ID,
								AssignedDate = DateTime.UtcNow,
								ApprovalCollaboratorRole = user.Role,
								Phase = user.Phase
							};
							context.ApprovalCollaborators.Add(objCollaborator);

                            if (isNewApprover)
							{
								var objDecision = new ApprovalCollaboratorDecision
								{
									Approval = objApproval.ID,
									Collaborator = user.ID,
									AssignedDate = DateTime.UtcNow,
									Phase = user.Phase
								};
								context.ApprovalCollaboratorDecisions.Add(objDecision);
							}

							if (model.SendEmails)
							{
								permissionsResponse.NewCollaborators.Add(user.ID);
							}
						}
					}

					foreach (int userId in insertGroups)
					{
						var objCollaboratorGroup = new ApprovalCollaboratorGroup
						{
							Approval = objApproval.ID,
							CollaboratorGroup = userId,
							AssignedDate = DateTime.UtcNow,
							Phase = model.CurrentPhase,
						};

						context.ApprovalCollaboratorGroups.Add(objCollaboratorGroup);
					}

					objApproval.SOADState = 0; //TODO replace with enum

					#endregion

					#region Delete Users, Groups & Decision

					//Delete collaborators for current approval
					context.ApprovalCollaborators.RemoveRange(context.ApprovalCollaborators.Where(ac => deleteUsers.Contains(ac.Collaborator) && ac.Approval == objApproval.ID));

					//Delete collaborators decision for current approval
					context.ApprovalCollaboratorDecisions.RemoveRange(context.ApprovalCollaboratorDecisions.Where(acd => deleteUsers.Contains(acd.Collaborator.Value) && acd.Approval == objApproval.ID));

					//Delete collaborators groups for current approval
					context.ApprovalCollaboratorGroups.RemoveRange(context.ApprovalCollaboratorGroups.Where(acg => deletegroups.Contains(acg.CollaboratorGroup) && acg.Approval == objApproval.ID));

					#endregion

					#region Lock Approval

					// Lock the Approval if all the approval collaborators have added Decisions..
					if ((objApproval.ApprovalCollaboratorDecisions.Count > 0) &&
						(objApproval.LockProofWhenAllDecisionsMade) &&
						(objApproval.ApprovalCollaboratorDecisions.Count(m => m.Decision.GetValueOrDefault() == 0) ==
						 0))
					{
						objApproval.IsLocked = true;
					}

					#endregion

					context.SaveChanges();
					ts.Complete();
				}
				accountId = objApproval.Job1.Account;
			}
			return permissionsResponse;
		}

        private static GrantDenyApprovalPermission ApprovalPermissionsForMultiAccess(PermissionsModel model, PermissionsUsersAndRolesModel permissions, out int accountId, GMGColorDAL.User loggedUser, DbContextBL context)
        {

            var permissionsResponse = new GrantDenyApprovalPermission();
            accountId = 0;

            Approval objApproval = (from a in context.Approvals where a.ID == model.ID select a).FirstOrDefault();


            if (objApproval != null)
            {
                permissionsResponse.DashboardNotification = ChangeOwner(ref objApproval, model.Owner, loggedUser.ID, model.CurrentPhase, context);

                var existingGroups = (from acg in context.ApprovalCollaboratorGroups
                                      where acg.Phase == objApproval.CurrentPhase && objApproval.ID == acg.Approval
                                      select acg.CollaboratorGroup).ToList();

                var insertGroups = (permissions.Groups != null && permissions.Groups.Any()) ? permissions.Groups.Select(u => u.Group).ToList().Except(existingGroups).ToList() : new List<int>();

                Dictionary<int, int> existingUsers = GetExistingUsersAndTheirRoles(objApproval.ID, model.CurrentPhase, context);

                using (var ts = new TransactionScope())
                {
                    #region Add/Edit/Update Users, Groups & Decision

                    foreach (var user in permissions.Users)
                    {
                        var objCollaborator = (from ac in context.ApprovalCollaborators
                                               where ac.Approval == model.ID && ac.Collaborator == user.ID && ac.Phase == user.Phase
                                               select ac).FirstOrDefault();

                        bool isNewApprover = (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context));

                        if (objCollaborator != null)
                        {
                            if (existingUsers.Any(nu => nu.Key == user.ID && nu.Value != user.Role))
                            {
                                bool isOldApprover = (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(objCollaborator.ApprovalCollaboratorRole, context));

                                objCollaborator.AssignedDate = DateTime.UtcNow;
                                objCollaborator.ApprovalCollaboratorRole = user.Role;

                                // Add
                                if (isNewApprover && !isOldApprover)
                                {
                                    var objDecision = new ApprovalCollaboratorDecision
                                    {
                                        Approval = objApproval.ID,
                                        Collaborator = user.ID,
                                        AssignedDate = DateTime.UtcNow,
                                        Phase = user.Phase
                                    };
                                    context.ApprovalCollaboratorDecisions.Add(objDecision);
                                }
                                // Remove
                                else if (!isNewApprover && isOldApprover)
                                {
                                    var userId = user.ID;

                                    //Delete collaborators decision for current approval
                                    context.ApprovalCollaboratorDecisions.RemoveRange(context.ApprovalCollaboratorDecisions.Where(acd => acd.Collaborator == userId && acd.Approval == objApproval.ID));

                                    if (objCollaborator.Approval1.PrimaryDecisionMaker != null &&
                                        objCollaborator.Approval1.PrimaryDecisionMaker ==
                                        objCollaborator.Collaborator)
                                    {
                                        objCollaborator.Approval1.PrimaryDecisionMaker = null;
                                    }
                                }
                            }
                            
                        }
                        // Add
                        else
                        {
                            objCollaborator = new ApprovalCollaborator
                            {
                                Approval = objApproval.ID,
                                Collaborator = user.ID,
                                AssignedDate = DateTime.UtcNow,
                                ApprovalCollaboratorRole = user.Role,
                                Phase = user.Phase
                            };
                            context.ApprovalCollaborators.Add(objCollaborator);

                            if (isNewApprover)
                            {
                                var objDecision = new ApprovalCollaboratorDecision
                                {
                                    Approval = objApproval.ID,
                                    Collaborator = user.ID,
                                    AssignedDate = DateTime.UtcNow,
                                    Phase = user.Phase
                                };
                                context.ApprovalCollaboratorDecisions.Add(objDecision);
                            }

                            if (model.SendEmails)
                            {
                                permissionsResponse.NewCollaborators.Add(user.ID);
                            }
                        }
                    }

                    foreach (int userId in insertGroups)
                    {
                        var objCollaboratorGroup = new ApprovalCollaboratorGroup
                        {
                            Approval = objApproval.ID,
                            CollaboratorGroup = userId,
                            AssignedDate = DateTime.UtcNow,
                            Phase = model.CurrentPhase,
                        };

                        context.ApprovalCollaboratorGroups.Add(objCollaboratorGroup);
                    }

                    objApproval.SOADState = 0; //TODO replace with enum

                    #endregion


                    #region Lock Approval

                    // Lock the Approval if all the approval collaborators have added Decisions..
                    if ((objApproval.ApprovalCollaboratorDecisions.Count > 0) &&
                        (objApproval.LockProofWhenAllDecisionsMade) &&
                        (objApproval.ApprovalCollaboratorDecisions.Count(m => m.Decision.GetValueOrDefault() == 0) ==
                         0))
                    {
                        objApproval.IsLocked = true;
                    }

                    #endregion

                    context.SaveChanges();
                    ts.Complete();
                }
                accountId = objApproval.Job1.Account;
            }
            return permissionsResponse;
        }

        private static Dictionary<int, int> GetExistingUsersAndTheirRoles(int approvalId, int? currentPhase, DbContextBL context)
		{
			return (from acl in context.ApprovalCollaborators
					join u in context.Users on acl.Collaborator equals u.ID
					join us in context.UserStatus on u.Status equals us.ID
					join ur in context.UserRoles on u.ID equals ur.User
					join r in context.Roles on ur.Role equals r.ID
					let appModuleID =
						(from am in context.AppModules where am.Key == 0 select am.ID)
						.FirstOrDefault()
					where
						acl.Approval == approvalId && us.Key != "D" && r.Key != "NON" &&
						r.AppModule == appModuleID && acl.Phase == currentPhase
					select new
					{
						acl.Collaborator,
						Role = acl.ApprovalCollaboratorRole
					}
									 ).ToDictionary(t => t.Collaborator, t => t.Role);

		}

        public static Dictionary<int, int> GetExistingUsers(int approvalId, int? currentPhase, DbContextBL context)
        {
            return (from acl in context.ApprovalCollaborators
                    join u in context.Users on acl.Collaborator equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    let appModuleID =
                        (from am in context.AppModules where am.Key == 0 select am.ID)
                        .FirstOrDefault()
                    where
                        acl.Approval == approvalId && us.Key != "D" && r.Key != "NON" &&
                        r.AppModule == appModuleID && acl.Phase == currentPhase
                    select new
                    {
                        acl.Collaborator,
                        Role = acl.ApprovalCollaboratorRole
                    }
                                     ).ToDictionary(t => t.Collaborator, t => t.Role);

        }


        private static DashboardInstantNotification ChangeOwner(ref Approval objApproval, int owner, int loggedUserId, int? currentPhase, DbContextBL context)
		{
			var dashboradInstantNotification = new DashboardInstantNotification();
			if (owner > 0)
			{
				bool ownerIsChanged = false;
				if (currentPhase > 0 && objApproval.Job1.JobOwner != owner)
				{
					ownerIsChanged = true;
					objApproval.Job1.JobOwner = owner;
				}
				else if (objApproval.Owner != owner)
				{
					ownerIsChanged = true;
					objApproval.Owner = owner;
				}

				if (ownerIsChanged)
				{
					var newOwner = UserBL.GetUserById(owner, context);

					if (newOwner != null)
					{
						dashboradInstantNotification.Owner = newOwner.GivenName + " " + newOwner.FamilyName;
					}
				}
			}
			dashboradInstantNotification.ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
			dashboradInstantNotification.Creator = loggedUserId;
			dashboradInstantNotification.Version = objApproval.ID.ToString();

			return dashboradInstantNotification;
		}

        public static DashboardInstantNotification UpdateDashboardInstantNotification(int ApprovalId , int loggedUserId,  DbContextBL context)
        {
            var dashboradInstantNotification = new DashboardInstantNotification();
            
            dashboradInstantNotification.ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
            dashboradInstantNotification.Creator = loggedUserId;
            dashboradInstantNotification.Version = ApprovalId.ToString();

            return dashboradInstantNotification;
        }

        /// <summary>
        /// Create/Update/Delete folder collaborators and groups
        /// </summary>
        /// <param name="folderId">Folder id</param>
        /// <param name="permissions">Users and groups to be added/updated/deleted</param>
        /// <param name="loggedUserRole">Logged user's collaborate role</param>
        /// <param name="loggedUser">Logged user</param>
        /// <param name="loggedAccountId">Logged account id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        private static int CUDFolderPermissions(int folderId, PermissionsUsersAndRolesModel permissions, GMGColorDAL.User loggedUser, int loggedAccountId, Role.RoleName loggedUserRole, DbContextBL context)
		{
			Folder objFolder = (from f in context.Folders where f.ID == folderId select f).FirstOrDefault();

			if (objFolder != null)
			{
				List<int> existingUsers = objFolder.FolderCollaborators.Select(m => m.Collaborator).ToList();
				var insertUsers = (permissions.Users != null && permissions.Users.Any()) ? permissions.Users.Select(u => u.ID).ToList().Except(existingUsers).ToList() : new List<int>();
				var deleteUsers = (permissions.Users != null && permissions.Users.Any()) ? existingUsers.Except(permissions.Users.Select(u => u.ID).ToList()).ToList() : existingUsers;

				List<int> existingGroups = objFolder.FolderCollaboratorGroups.Select(m => m.CollaboratorGroup).ToList();
				var insertGroups = (permissions.Groups != null && permissions.Groups.Any()) ? permissions.Groups.Select(u => u.Group).ToList().Except(existingGroups).ToList() : new List<int>();
				var deletegroups = (permissions.Groups != null && permissions.Groups.Any()) ? existingGroups.Except(permissions.Groups.Select(u => u.Group).ToList()).ToList() : existingGroups;

				if (insertUsers.Any() || deleteUsers.Any() || insertGroups.Any() || deletegroups.Any())
				{
					using (var ts = new TransactionScope())
					{
						#region Add Users & Groups

						foreach (int userId in insertUsers)
						{
							var objCollaborator = new FolderCollaborator
							{
								Folder = folderId,
								Collaborator = userId,
								AssignedDate = DateTime.UtcNow
							};
							context.FolderCollaborators.Add(objCollaborator);
						}

						foreach (int userId in insertGroups)
						{
							var objCollaboratorGroup = new FolderCollaboratorGroup
							{
								Folder = folderId,
								CollaboratorGroup = userId,
								AssignedDate = DateTime.UtcNow
							};
							context.FolderCollaboratorGroups.Add(objCollaboratorGroup);
						}

						#endregion

						#region Delete Users & Groups

						// delete collaborators for this folder
						context.FolderCollaborators.RemoveRange(context.FolderCollaborators.Where(x => deleteUsers.Contains(x.Collaborator) && x.Folder == folderId));

						//delete collaborators groups for this folder
						context.FolderCollaboratorGroups.RemoveRange(context.FolderCollaboratorGroups.Where(x => deletegroups.Contains(x.CollaboratorGroup) && x.Folder == folderId));

						#endregion

						context.SaveChanges();
						ts.Complete();
					}
				}

				var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedAccountId, 0, false, context);
				//check if logged user has administartor role on collaborate and "Automatically inherit permissions from parent folder" option enabled
				// if true, replace access permissions with parent permissions for all jobs and sub-folders within the current folder
				if (collaborateSettings.InheritParentFolderPermissions && loggedUserRole == Role.RoleName.AccountAdministrator)
				{
					ApplyFolderPermissions(folderId, loggedUser, context);

					context.SaveChanges();
				}

				return objFolder.Account;
			}
			return 0;
		}

		private static List<SharedApproval> UpdateExternalCollaboratorsAndDecisions(List<ExternalUserPermissions> existingExternalUsers, ShareModel model, ref Approval objApproval, GMGColorDAL.User loggedUser , DbContextBL context)
		{
			var sharedApprovals = new List<SharedApproval>();
			foreach (var user in existingExternalUsers)
			{
				var objShared = (from sa in context.SharedApprovals
								 where
									sa.Approval == model.Approval &&
									sa.ExternalCollaborator == user.ID && sa.Phase == user.Phase
								 select sa).FirstOrDefault();

				if (objShared != null)
				{
					if (((objShared.IsSharedDownloadURL != model.IsShareDownloadURL) &&
						 (objShared.IsSharedURL != model.IsShareURL)) && // Not Download and not Shared
						((objShared.IsSharedDownloadURL != model.IsShareDownloadURL) &&
						 (objShared.IsSharedURL == model.IsShareURL)) // Not Download and Shared
						)
					{
						if (!objShared.IsSharedDownloadURL)
						{
							objShared.IsSharedDownloadURL = model.IsShareDownloadURL;
						}
						if (!objShared.IsSharedURL)
						{
							objShared.IsSharedURL = model.IsShareURL;
						}

						sharedApprovals.Add(objShared);
					}

					var approvalId = objApproval.ID;
					ApprovalCollaboratorDecision decision = (from acd in context.ApprovalCollaboratorDecisions
															 where acd.Approval == approvalId && acd.ExternalCollaborator == user.ID && acd.Phase == user.Phase
															 select acd).FirstOrDefault();


					if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer != ApprovalCollaboratorRole.GetRole(user.Role, context) &&
						decision != null)
					{
						context.ApprovalCollaboratorDecisions.Remove(decision);

						if (objShared.Approval1.ExternalPrimaryDecisionMaker != null &&
							objShared.Approval1.ExternalPrimaryDecisionMaker ==
							objShared.ExternalCollaborator)
						{
							objShared.Approval1.ExternalPrimaryDecisionMaker = null;
						}
					}
					else if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context) &&
							 decision == null)
					{
						// add ApproverAndReviewer right
						var objDecision = new ApprovalCollaboratorDecision();
						objDecision.Approval = objApproval.ID;
						objDecision.ExternalCollaborator = user.ID;
						objDecision.Phase = user.Phase;

						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}
				}
				else
				{
					objShared = new SharedApproval();
					objShared.Approval = model.Approval;
					objShared.ExternalCollaborator = user.ID;
					objShared.IsSharedDownloadURL = model.IsShareDownloadURL;
					objShared.IsSharedURL = model.IsShareURL;
					objShared.Phase = user.Phase.HasValue ? user.Phase.Value : (int?)null;

					sharedApprovals.Add(objShared);

                    var objExternalCollaborator = (from ec in context.ExternalCollaborators
												   where ec.ID == user.ID
												   select ec).FirstOrDefault();

					if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context) && objExternalCollaborator != null)
					{
						var objDecision = new ApprovalCollaboratorDecision();
						objDecision.Approval = model.Approval;
						objDecision.AssignedDate = DateTime.UtcNow;
						objDecision.Phase = user.Phase;
						objExternalCollaborator.ApprovalCollaboratorDecisions.Add(objDecision);
						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}
				}
				objShared.ApprovalCollaboratorRole = user.Role;
				objShared.IsExpired = false;
				objShared.SharedDate = DateTime.UtcNow;
				objShared.ExpireDate = DateTime.UtcNow.AddDays(7);
				objShared.Notes = model.Message;
				if (objShared.ID == 0)
				{
					context.SharedApprovals.Add(objShared);
				}

				if (!String.IsNullOrEmpty(model.PDMEmail) &&
					objShared.ExternalCollaborator1.EmailAddress == model.PDMEmail)
				{
					objApproval.ExternalPrimaryDecisionMaker = objShared.ExternalCollaborator;
					objApproval.PrimaryDecisionMaker = null;
				}
			}
			return sharedApprovals;
		}

		private static List<SharedApproval> UpdateApiExternalCollaboratorsAndDecisions(List<ExternalUserPermissions> existingExternalUsers, int approvalId, DbContextBL context)
		{
			var sharedApprovals = new List<SharedApproval>();
			foreach (var user in existingExternalUsers)
			{
				var objShared = (from sa in context.SharedApprovals
								 where
									sa.Approval == approvalId &&
									sa.ExternalCollaborator == user.ID && sa.Phase == user.Phase
								 select sa).FirstOrDefault();

				if (objShared != null)
				{
					if (objShared.IsSharedDownloadURL)
					{
						objShared.IsSharedDownloadURL = false;
					}
					if (!objShared.IsSharedURL)
					{
						objShared.IsSharedURL = true;
					}

					sharedApprovals.Add(objShared);


					ApprovalCollaboratorDecision decision = (from acd in context.ApprovalCollaboratorDecisions
															 where acd.Approval == approvalId && acd.ExternalCollaborator == user.ID && acd.Phase == user.Phase
															 select acd).FirstOrDefault();

					if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer != ApprovalCollaboratorRole.GetRole(user.Role, context) && decision != null)
					{
						context.ApprovalCollaboratorDecisions.Remove(decision);

						if (objShared.Approval1.ExternalPrimaryDecisionMaker != null &&
							objShared.Approval1.ExternalPrimaryDecisionMaker ==
							objShared.ExternalCollaborator)
						{
							objShared.Approval1.ExternalPrimaryDecisionMaker = null;
						}
					}
					else if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context) && decision == null)
					{
						// add ApproverAndReviewer right
						var objDecision = new ApprovalCollaboratorDecision();
						objDecision.Approval = approvalId;
						objDecision.ExternalCollaborator = user.ID;
						objDecision.Phase = user.Phase;

						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}
				}
				else
				{
					objShared = new SharedApproval();
					objShared.Approval = approvalId;
					objShared.ExternalCollaborator = user.ID;
					objShared.IsSharedDownloadURL = false;
					objShared.IsSharedURL = true;
					objShared.Phase = user.Phase.HasValue ? user.Phase.Value : (int?)null;

					sharedApprovals.Add(objShared);

					var objExternalCollaborator = (from ec in context.ExternalCollaborators
												   where ec.ID == user.ID
												   select ec).FirstOrDefault();

					if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context) && objExternalCollaborator != null)
					{
						var objDecision = new ApprovalCollaboratorDecision();
						objDecision.Approval = approvalId;
						objDecision.AssignedDate = DateTime.UtcNow;
						objDecision.Phase = user.Phase;
						objExternalCollaborator.ApprovalCollaboratorDecisions.Add(objDecision);
						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}
				}
				objShared.ApprovalCollaboratorRole = user.Role;
				objShared.IsExpired = false;
				objShared.SharedDate = DateTime.UtcNow;
				objShared.ExpireDate = DateTime.UtcNow.AddDays(7);
				objShared.Notes = string.Empty;

				if (objShared.ID == 0)
				{
					context.SharedApprovals.Add(objShared);
				}

				//if (!String.IsNullOrEmpty(model.PDMEmail) &&
				//    objShared.ExternalCollaborator1.EmailAddress == model.PDMEmail)
				//{
				//    objApproval.ExternalPrimaryDecisionMaker = objShared.ExternalCollaborator;
				//    objApproval.PrimaryDecisionMaker = null;
				//}
			}
			return sharedApprovals;
		}

		public static List<SharedApproval> AddExternalCollaboratorsAndDecisions(List<NewExternalUserPermissions> newExternalUsers, ShareModel model, ref Approval approval, int loggedUserID, int loggedAccount, DbContextBL context)
		{
			var sharedApprovals = new List<SharedApproval>();

			List<string> avoidedColors = new List<string>();
			foreach (var user in newExternalUsers)
			{
				var objExternalCollaborator = (from ec in context.ExternalCollaborators
											   where
												   ec.EmailAddress.ToLower() == user.Email &&
												   ec.Account == loggedAccount
											   select ec).FirstOrDefault();

				if (objExternalCollaborator == null)
				{
					string[] givenfamilyNames = user.FullName.Split(' ');

					objExternalCollaborator = new ExternalCollaborator();
					objExternalCollaborator.Account = loggedAccount;
					objExternalCollaborator.Guid = Guid.NewGuid().ToString();
					objExternalCollaborator.Creator = loggedUserID;
					objExternalCollaborator.CreatedDate = DateTime.UtcNow;
					objExternalCollaborator.IsDeleted = false;
					objExternalCollaborator.GivenName = givenfamilyNames[0].Trim();
					objExternalCollaborator.FamilyName = givenfamilyNames[1].Trim();
					objExternalCollaborator.EmailAddress = user.Email.ToLower();
					objExternalCollaborator.Locale = user.Locale;
					objExternalCollaborator.Modifier = loggedUserID;
					objExternalCollaborator.ModifiedDate = DateTime.UtcNow;
					objExternalCollaborator.ProofStudioColor =
						ExternalCollaboratorBL.GenerateCollaboratorProofStudioColor(loggedAccount,
																					avoidedColors,
																					context);
					avoidedColors.Add(objExternalCollaborator.ProofStudioColor);

					context.ExternalCollaborators.Add(objExternalCollaborator);
				}

				if (!String.IsNullOrEmpty(model.PDMEmail) &&
					objExternalCollaborator.EmailAddress == model.PDMEmail)
				{
					approval.ExternalCollaborator = objExternalCollaborator;
					approval.PrimaryDecisionMaker = null;
				}

				SharedApproval objShared = (from sa in context.SharedApprovals
											where
												sa.Approval == model.Approval &&
												sa.ExternalCollaborator ==
												objExternalCollaborator.ID && sa.Phase == user.Phase
											select sa).Take(1).FirstOrDefault();

				if (objShared == null)
				{
					objShared = new SharedApproval { Approval = model.Approval, Phase = user.Phase.HasValue ? user.Phase.Value : (int?)null };
					objExternalCollaborator.SharedApprovals.Add(objShared);

					//Add to list of emails
					sharedApprovals.Add(objShared);

					if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer ==
						ApprovalCollaboratorRole.GetRole(user.Role, context))
					{
						var objDecision = new ApprovalCollaboratorDecision
						{
							Approval = model.Approval,
							AssignedDate = DateTime.UtcNow,
							Phase = user.Phase
						};
						objExternalCollaborator.ApprovalCollaboratorDecisions.Add(objDecision);
						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}
					context.SharedApprovals.Add(objShared);
				}

				objShared.ApprovalCollaboratorRole = user.Role;
				objShared.IsSharedURL = model.IsShareURL;
				objShared.IsSharedDownloadURL = model.IsShareDownloadURL;
				objShared.IsExpired = false;
				objShared.SharedDate = DateTime.UtcNow;
				objShared.ExpireDate = DateTime.UtcNow.AddDays(7);
				objShared.Notes = model.Message;
				objShared.Phase = model.CurrentPhase;
			}

			return sharedApprovals;
		}

		public static List<SharedApproval> AddApiExternalCollaboratorsAndDecisions(List<NewExternalUserPermissions> newExternalUsers, int approvalId, int loggedUserID, int loggedAccount, DbContextBL context)
		{
			var sharedApprovals = new List<SharedApproval>();
			List<string> avoidedColors = new List<string>();
			foreach (var user in newExternalUsers)
			{
				string[] givenfamilyNames = user.FullName.Split(' ');

				var objExternalCollaborator = new ExternalCollaborator
				{
					Account = loggedAccount,
					Guid = Guid.NewGuid().ToString(),
					Creator = loggedUserID,
					CreatedDate = DateTime.UtcNow,
					IsDeleted = false,
					GivenName = givenfamilyNames[0].Trim(),
					FamilyName = givenfamilyNames[1].Trim(),
					EmailAddress = user.Email.ToLower(),
					Locale = user.Locale,
					Modifier = loggedUserID,
					ModifiedDate = DateTime.UtcNow,
					ProofStudioColor = ExternalCollaboratorBL.GenerateCollaboratorProofStudioColor(loggedAccount, avoidedColors, context)
				};
				avoidedColors.Add(objExternalCollaborator.ProofStudioColor);

				context.ExternalCollaborators.Add(objExternalCollaborator);

				SharedApproval objShared = (from sa in context.SharedApprovals
											where
											sa.Approval == approvalId &&
											sa.ExternalCollaborator ==
											objExternalCollaborator.ID && sa.Phase == user.Phase
											select sa).Take(1).FirstOrDefault();

				if (objShared == null)
				{
					objShared = new SharedApproval
					{
						Approval = approvalId,
						Phase = user.Phase.HasValue ? user.Phase.Value : (int?)null
					};
					objExternalCollaborator.SharedApprovals.Add(objShared);

					//Add to list of emails
					sharedApprovals.Add(objShared);

					if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == ApprovalCollaboratorRole.GetRole(user.Role, context))
					{
						var objDecision = new ApprovalCollaboratorDecision
						{
							Approval = approvalId,
							AssignedDate = DateTime.UtcNow,
							Phase = user.Phase
						};
						objExternalCollaborator.ApprovalCollaboratorDecisions.Add(objDecision);
						context.ApprovalCollaboratorDecisions.Add(objDecision);
					}
					context.SharedApprovals.Add(objShared);
				}

				objShared.ApprovalCollaboratorRole = user.Role;
				objShared.IsSharedURL = true;
				objShared.IsSharedDownloadURL = false;
				objShared.IsExpired = false;
				objShared.SharedDate = DateTime.UtcNow;
				objShared.ExpireDate = DateTime.UtcNow.AddDays(7);
				objShared.Notes = "";
				objShared.Phase = user.Phase;
			}
			return sharedApprovals;
		}


		public static void ApplyFolderPermissions(int selectedFolder, GMGColorDAL.User loggedUser, DbContextBL context)
		{
			try
			{
				var watch = System.Diagnostics.Stopwatch.StartNew();
				Folder objParent = ApprovalBL.GetFolderById(selectedFolder, context);
				GMGColorLogging.log.InfoFormat("'GetFolderById' query took: {0} milliseconds", watch.ElapsedMilliseconds);


				watch.Restart();
				var collaboratorsModuleRole = (from u in context.Users
											   join ur in context.UserRoles on u.ID equals ur.User
											   join r in context.Roles on ur.Role equals r.ID
											   join am in context.AppModules on r.AppModule equals am.ID
											   join fc in context.FolderCollaborators on u.ID equals fc.Collaborator
											   where fc.Folder == objParent.ID && am.Key == 0
											   select new UserCollaborateRolePair
											   {
												   UserId = u.ID,
												   CollaborateRoleId = r.Key == "VIE" ? (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly
																					  : r.Key == "CON" ? (int)ApprovalCollaboratorRole.ApprovalRoleName.Reviewer
																									   : (int)ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer
											   }).ToList();

				GMGColorLogging.log.InfoFormat("'GetCollaboratorsModduleRole' query took: {0} milliseconds", watch.ElapsedMilliseconds);
				watch.Reset();
				ApplyFolderPermissionsToAllFilesAndFolders(objParent, loggedUser, objParent.FolderCollaborators, objParent.FolderCollaboratorGroups, collaboratorsModuleRole, context);
				GMGColorLogging.log.InfoFormat("'ApplyFolderPermissionsToAllFilesAndFolders' method took: {0} milliseconds", watch.ElapsedMilliseconds);
				watch.Stop();
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
			}
		}

		/// <summary>
		/// Apply folder permissions to all files and subfolder within the folder to the lowest level
		/// </summary>
		/// <param name="objParent">Current folder</param>
		/// <param name="selectedFolderCollaborators">Collaborators of the folder for which "Apply Folder Permissions" action was selected </param>
		/// <param name="selectedFolderCollaboratorGroups">Collaborators groups of the folder for which "Apply Folder Permissions" action was selected </param>
		/// <param name="context">Database context</param>
		private static void ApplyFolderPermissionsToAllFilesAndFolders(Folder objParent, GMGColorDAL.User loggedUser, ICollection<FolderCollaborator> selectedFolderCollaborators, ICollection<FolderCollaboratorGroup> selectedFolderCollaboratorGroups, List<UserCollaborateRolePair> collaboratorsRoles, DbContextBL context)
		{
			try
			{
				//Check whether the current fodler has children
				foreach (var objChild in objParent.Folder1)
				{
					ApplyFolderPermissionsToAllFilesAndFolders(objChild, loggedUser, selectedFolderCollaborators, selectedFolderCollaboratorGroups, collaboratorsRoles, context);
				}

				//Apply folder's permissions to all folders within the current folder
				ApplyFolderPermisionsToFolders(objParent, loggedUser, selectedFolderCollaborators, selectedFolderCollaboratorGroups, context);

				if (objParent.FolderCollaborators.Any(c => c.Collaborator == loggedUser.ID))
				{
					//Apply folder's permissions to all jobs within the current folder except completed ones
					ApplyFolderPermissionsToJobs(objParent, loggedUser, selectedFolderCollaborators, selectedFolderCollaboratorGroups, collaboratorsRoles, context);
				}
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
				//GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "GMGColorDAL.Approval.ApplyFolderPermissionsToAllFilesAndFolders : Applying folder permissions to files and folders failed. {0}", ex.Message);
			}
		}

		/// <summary>
		/// Overwrite the permissions of the all jobs within the current folder with his access permissions
		/// </summary>
		/// <param name="objParent">The id of the selcted folder</param>
		/// <param name="selectedFolderCollaborators">Collaborators of the folder for which "Apply Folder Permissions" action was selected </param>
		/// <param name="selectedFolderCollaboratorGroups">Collaborators groups of the folder for which "Apply Folder Permissions" action was selected </param>
		/// <param name="context">Database context</param>
		private static void ApplyFolderPermissionsToJobs(Folder objParent, GMGColorDAL.User loggedUser, ICollection<FolderCollaborator> selectedFolderCollaborators, ICollection<FolderCollaboratorGroup> selectedFolderCollaboratorGroups, List<UserCollaborateRolePair> collaboratorRoles, DbContextBL context)
		{
			try
			{
				foreach (var approval in objParent.Approvals)
				{
					if (approval.CurrentPhase == null && !approval.IsDeleted && approval.Job1.JobStatu.Name != JobStatu.Status.Archived.ToString() && approval.Job1.JobStatu.Name != JobStatu.Status.Completed.ToString() && approval.ApprovalCollaborators.Any(c => c.Collaborator == loggedUser.ID))
					{
						var newCollaborators = new List<int>();

						ApplyFolderPermissionsToJobRemoveCollaborators(approval, selectedFolderCollaborators, context);

						newCollaborators = ApplyFolderPermissionsToJobAddCollaborators(selectedFolderCollaborators, approval, newCollaborators, collaboratorRoles);

						context.ApprovalCollaboratorGroups.RemoveRange(approval.ApprovalCollaboratorGroups);

						ApplyFolderPermissionsToJobAddParrentCollaboratorsGroups(selectedFolderCollaboratorGroups, approval);

						if (newCollaborators.Count > 0)
						{
							Task.Factory.StartNew(() => CreateTempNotificationsForFolderPermissions(newCollaborators, loggedUser, approval));
						}
					}
				}
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
				//GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "GMGColorDAL.Approval.ApplyFolderPermisionsToFolders : Applying folder permissions to folders failed. {0}", ex.Message);
			}
		}

		/// <summary>
		/// Create temporay notification items for apply folder permissions
		/// </summary>
		private static void CreateTempNotificationsForFolderPermissions(List<int> newCollaborators, GMGColorDAL.User loggedUser, Approval approval)
		{
			var tempNotificationItems = NotificationServiceBL.CreateTmpNotifForOwnAndInternals(approval.ID, newCollaborators, loggedUser, NotificationType.ApprovalWasShared);

			NotificationServiceBL.PutTmpNotifInQueue(tempNotificationItems);
		}

		/// <summary>
		/// Add parrent collaborators group
		/// </summary>
		/// <param name="selectedFolderCollaboratorGroups">The selected collaborators group</param>
		/// <param name="approval">The approval</param>
		public static void ApplyFolderPermissionsToJobAddParrentCollaboratorsGroups(ICollection<FolderCollaboratorGroup> selectedFolderCollaboratorGroups, Approval approval)
		{
			foreach (FolderCollaboratorGroup objItem in selectedFolderCollaboratorGroups)
			{
				if (approval.ApprovalCollaboratorGroups.All(c => c.CollaboratorGroup != objItem.CollaboratorGroup))
				{
					var objApprovalCollaboratorGroup = new ApprovalCollaboratorGroup
					{
						AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
						CollaboratorGroup = objItem.CollaboratorGroup
					};
					approval.ApprovalCollaboratorGroups.Add(objApprovalCollaboratorGroup);
				}
			}
		}

		/// <summary>
		/// Remove all folder's collaborators except approval's owner and user that have made a decision
		/// </summary>
		/// <param name="approval"></param>
		/// <param name="selectedFolderCollaborators">The selected collaborators</param>
		/// <param name="context"></param>
		public static void ApplyFolderPermissionsToJobRemoveCollaborators(Approval approval, ICollection<FolderCollaborator> selectedFolderCollaborators, DbContextBL context)
		{
			var appCollabDecToDelete = new List<ApprovalCollaboratorDecision>();
			var appCollabToDelete = new List<ApprovalCollaborator>();
			foreach (var approvalCollaborator in approval.ApprovalCollaborators.ToList())
			{
				if (approvalCollaborator.Collaborator != approval.Owner && !approval.ApprovalCollaboratorDecisions.Any(d => d.Collaborator == approvalCollaborator.Collaborator && d.Decision != null) && selectedFolderCollaborators.All(f => f.Collaborator != approvalCollaborator.Collaborator))
				{
					//context.ApprovalCollaborators.Remove(approvalCollaborator);
					appCollabToDelete.Add(approvalCollaborator);
					var collaboratorDecision = approval.ApprovalCollaboratorDecisions.FirstOrDefault(d => d.Collaborator == approvalCollaborator.Collaborator);
					if (collaboratorDecision != null)
					{
						//context.ApprovalCollaboratorDecisions.Remove(collaboratorDecision);
						appCollabDecToDelete.Add(collaboratorDecision);
					}
				}
			}
			context.ApprovalCollaborators.RemoveRange(appCollabToDelete);
			context.ApprovalCollaboratorDecisions.RemoveRange(appCollabDecToDelete);
		}


		/// <summary>
		/// Add parent collaborators
		/// </summary>
		/// <param name="selectedFolderCollaborators"></param>
		/// <param name="approval"></param>
		/// <param name="newCollaborators"></param>
		/// <param name="collaboratorRoles"></param>
		/// <returns></returns>
		public static List<int> ApplyFolderPermissionsToJobAddCollaborators(ICollection<FolderCollaborator> selectedFolderCollaborators, Approval approval, List<int> newCollaborators, List<UserCollaborateRolePair> collaboratorRoles)
		{
			foreach (FolderCollaborator objItem in selectedFolderCollaborators)
			{
				if (approval.ApprovalCollaborators.All(c => c.Collaborator != objItem.Collaborator))
				{
					newCollaborators.Add(objItem.Collaborator);
					var collaborator = new ApprovalCollaborator
					{
						Collaborator = objItem.Collaborator,
						ApprovalCollaboratorRole = collaboratorRoles.FirstOrDefault(t => t.UserId == objItem.Collaborator).CollaborateRoleId,
						AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"))
					};
					approval.ApprovalCollaborators.Add(collaborator);
				}
			}
			return newCollaborators;
		}

		/// <summary>
		/// Create notification user added via apply folder permissions - No longer used
		/// </summary>
		/// <param name="newCollaborators"></param>
		/// <param name="loggedUser"></param>
		/// <param name="approval"></param>
		/// <param name="context"></param>
		[Obsolete]
		public static void CreateApplyFolderPermissionsToJobsNotifications(List<int> newCollaborators, GMGColorDAL.User loggedUser, Approval approval, DbContextBL context)
		{
			foreach (var newCollaborator in newCollaborators)
			{
				NotificationServiceBL.CreateNotification(new ApprovalWasShared()
				{
					EventCreator = loggedUser.ID,
					InternalRecipient = newCollaborator,
					ApprovalsIds = new[] { approval.ID }
				},
				loggedUser,
				context,
				saveChanges: false
				);
			}

			//send mail to logged user
			if (newCollaborators.Any() && loggedUser != null && loggedUser.IncludeMyOwnActivity)
			{
				NotificationServiceBL.CreateNotification(new ApprovalWasShared
				{
					EventCreator = loggedUser.ID,
					InternalRecipient = loggedUser.ID,
					ApprovalsIds = new[] { approval.ID }
				},
				loggedUser,
				context,
				saveChanges: false
				);
			}
		}

		/// <summary>
		/// Overwrite the permissions of the children folders (only first level) with access permissions of the current folder
		/// </summary>
		/// <param name="objParent">Selected folder id</param>
		/// <param name="selectedFolderCollaborators">Collaborators of the folder for which "Apply Folder Permissions" action was selected </param>
		/// <param name="selectedFolderCollaboratorGroups">Collaborators groups of the folder for which "Apply Folder Permissions" action was selected </param>
		/// <param name="context">Database context</param>
		private static void ApplyFolderPermisionsToFolders(Folder objParent, GMGColorDAL.User loggedUser, ICollection<FolderCollaborator> selectedFolderCollaborators, ICollection<FolderCollaboratorGroup> selectedFolderCollaboratorGroups, DbContextBL context)
		{
			try
			{
				Parallel.ForEach(objParent.Folder1, (childFolder) =>
				{

					GMGColorLogging.log.InfoFormat("Running ApplyFolderPermisionsToFolders to '{0}' folder", childFolder.Name);
					var watch = System.Diagnostics.Stopwatch.StartNew();
					if (!childFolder.IsDeleted && childFolder.FolderCollaborators.Any(c => c.Collaborator == loggedUser.ID))
					{
						var loopWatch = System.Diagnostics.Stopwatch.StartNew();
						foreach (var folderCollaborator in childFolder.FolderCollaborators.ToList())
						{
							//remove all folder's collaborators except folder's owner
							if (folderCollaborator.Collaborator != childFolder.Creator)
							{
								context.FolderCollaborators.Remove(folderCollaborator);
							}
						}
						GMGColorLogging.log.InfoFormat("'Remove All Existing Folder Collborators' took {0} milliseconds", watch.ElapsedMilliseconds);
						loopWatch.Restart();

						//add parent collaborators 
						foreach (FolderCollaborator objItem in selectedFolderCollaborators)
						{
							if (childFolder.FolderCollaborators.All(f => f.Collaborator != objItem.Collaborator))
							{
								var objFolderCollaborator = new FolderCollaborator
								{
									AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
									Collaborator = objItem.Collaborator
								};

								childFolder.FolderCollaborators.Add(objFolderCollaborator);
							}
						}
						GMGColorLogging.log.InfoFormat("'Add New Folder Collborators' took {0} milliseconds", watch.ElapsedMilliseconds);
						loopWatch.Restart();

						context.FolderCollaboratorGroups.RemoveRange(childFolder.FolderCollaboratorGroups);

						GMGColorLogging.log.InfoFormat("'Remove existing Folder Collborator Groups' took {0} milliseconds", watch.ElapsedMilliseconds);
						loopWatch.Restart();

						//add parent collaborators groups
						foreach (FolderCollaboratorGroup objItem in selectedFolderCollaboratorGroups)
						{
							var objFolderCollaboratorGroup = new FolderCollaboratorGroup
							{
								AssignedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
								CollaboratorGroup = objItem.CollaboratorGroup
							};

							childFolder.FolderCollaboratorGroups.Add(objFolderCollaboratorGroup);
						}
						GMGColorLogging.log.InfoFormat("'Add New Folder Collborator Groups' took {0} milliseconds", watch.ElapsedMilliseconds);
						watch.Restart();
					}
					GMGColorLogging.log.InfoFormat("Completed ApplyFolderPermisionsToFolders to '{0}' folder took {1} milliseconds", childFolder.Name, watch.ElapsedMilliseconds);
					watch.Restart();
				});
			}
			catch (DbEntityValidationException ex)
			{
				DALUtils.LogDbEntityValidationException(ex);
			}
			catch (Exception ex)
			{
				throw new ExceptionBL(ExceptionBLMessages.CouldNotSetApprovalPermission, ex);
				//GMGColorLogging.log.ErrorFormat(ex, this.LoggedAccount.ID, "GMGColorDAL.Approval.ApplyFolderPermisionsToFolders : Applying folder permissions to folders failed. {0}", ex.Message);
			}
		}

		/// <summary>
		/// Add, update delete group collaborators
		/// </summary>
		/// <param name="groupId"></param>
		/// <param name="collaboratorsWhereThisIsCollaboratorGroup"></param>
		/// <param name="context"></param>
		public static void CUDGroupCollaborators(int groupId, List<GroupsModel.UsersInUserGroup> collaboratorsWhereThisIsCollaboratorGroup, DbContextBL context)
		{
			try
			{
				//group collaborators from database
				var groupCollaborators = (from g in context.UserGroups
										  join ugu in context.UserGroupUsers on g.ID equals ugu.UserGroup
										  where g.ID == groupId
										  select new { ugu.IsPrimary, ugu.User }).Distinct().ToList();

				//all selected collaborators from UI
				var collaborators = collaboratorsWhereThisIsCollaboratorGroup.Where(u => u.IsUserInGroup)
																				   .Select(u => u.objUser.ID)
																				   .ToList();

				var newGroupCollaborators = collaborators.Except(groupCollaborators.Select(gc => gc.User)).ToList();
				var deletedGroupCollaborators = groupCollaborators.Select(gc => gc.User).Except(collaborators).ToList();

				//retrieve all collaborators from approvals that are related with the current group
				var approvalCollaborators = (from a in context.Approvals
											 join acg in context.ApprovalCollaboratorGroups on a.ID equals acg.Approval
											 join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
											 where acg.CollaboratorGroup == groupId && a.IsDeleted == false
											 select new
											 {
												 a.ID,
												 ac.Collaborator,
												 IsOwner = (a.Owner == ac.Collaborator)
											 }).Distinct().ToList();

				var approvals = approvalCollaborators.Select(a => a.ID).Distinct().ToList();

				//retrieve all collaborators from folders that are related with the current group
				var folderCollaborators = (from f in context.Folders
										   join fcg in context.FolderCollaboratorGroups on f.ID equals fcg.Folder
										   join fc in context.FolderCollaborators on f.ID equals fc.Folder
										   where fcg.CollaboratorGroup == groupId && f.IsDeleted == false
										   select new
										   {
											   fc.Folder,
											   fc.Collaborator,
											   IsOwner = (f.Creator == fc.Collaborator)
										   }).ToList();

				var folders = folderCollaborators.Select(f => f.Folder).Distinct().ToList();

				var isPrimaryGroup = groupCollaborators.Select(g => g.IsPrimary).FirstOrDefault();
				foreach (int newGroupCollaborator in newGroupCollaborators)
				{
					var userGroupUser = new UserGroupUser
					{
						User = newGroupCollaborator,
						UserGroup = groupId,
						IsPrimary = isPrimaryGroup
					};
					context.UserGroupUsers.Add(userGroupUser);

					if (approvals.Count > 0)
					{
						var role = ApprovalCollaboratorRole.GetCollaborateDefaultRoleId(newGroupCollaborator, context);
                        List<int> lstValidApprovals = ApprovalBL.ValidApprovalList(approvals, newGroupCollaborator, context);
                        var lstApprovals1 = approvals.Where(x => !lstValidApprovals.Contains(x)).ToList();


                        var objApprovalCollaboratorList = (from la in lstApprovals1
                                                           select new ApprovalCollaborator
                                                           {
                                                               Approval = la,
                                                               Collaborator = newGroupCollaborator,
                                                               ApprovalCollaboratorRole = role,
                                                               AssignedDate = DateTime.UtcNow,

                                                           }).ToList();

                        context.ApprovalCollaborators.AddRange(objApprovalCollaboratorList);
                    }

                    if(folders.Count > 0)
                    {
                        List<int> lstValidFolders = ApprovalBL.ValidFolderList(folders, newGroupCollaborator, context);
                        var lstFolderList = folders.Where(x => !lstValidFolders.Contains(x)).ToList();
                        var objFolderCollaboratorList = (from fo in lstFolderList
                                                         select new FolderCollaborator
                                                         {
                                                             Folder = fo,
                                                             Collaborator = newGroupCollaborator,
                                                             AssignedDate = DateTime.UtcNow,
                                                         }).ToList();

                        context.FolderCollaborators.AddRange(objFolderCollaboratorList);
                    }
				}

				if (deletedGroupCollaborators.Count > 0)
				{
					//get all collaborators to be deleted that are not owners on approvals
					var approvalCollaboratorsToDelete = approvalCollaborators.Where(ac => !ac.IsOwner && deletedGroupCollaborators.Contains(ac.Collaborator)).Select(a => a.Collaborator).Distinct().ToList();

					//get all collaborators to be deleted that are not owners on folders
					var folderCollaboratorsToDelete = folderCollaborators.Where(fc => !fc.IsOwner && deletedGroupCollaborators.Contains(fc.Collaborator)).Select(f => f.Collaborator).Distinct().ToList();

					//Delete users from group
					context.UserGroupUsers.RemoveRange(context.UserGroupUsers.Where(ugu => deletedGroupCollaborators.Contains(ugu.User) && ugu.UserGroup == groupId));

					//Delete approval collaborators
					context.ApprovalCollaborators.RemoveRange(context.ApprovalCollaborators.Where(ac =>
																									approvalCollaboratorsToDelete.Contains(ac.Collaborator) && ac.Approval1.Owner != ac.Collaborator &&
																									approvals.Contains(ac.Approval)));
					//Delete folder collaborators
					context.FolderCollaborators.RemoveRange(context.FolderCollaborators.Where(fc => folderCollaboratorsToDelete.Contains(fc.Collaborator) && fc.Folder1.Creator != fc.Collaborator && folders.Contains(fc.Folder)));

				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		private static void SetUsersPermissions(GMGColorContext context, int approvalId, PermissionsUsersAndRolesModel permissionsUsersAndRoles)
		{
			int role = -1;
			ApprovalCollaboratorRole.ApprovalRoleName roleName = ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;
			foreach (var user in permissionsUsersAndRoles.Users)
			{
				var objCollaborator = new ApprovalCollaborator
				{
					Approval = approvalId,
					Collaborator = user.ID,
					AssignedDate = DateTime.UtcNow,
					ApprovalCollaboratorRole = user.Role,
					Phase = user.Phase
				};
				context.ApprovalCollaborators.Add(objCollaborator);

				if (user.Role != role)
				{
					role = user.Role;
					roleName = ApprovalCollaboratorRole.GetRole(user.Role, context);
				}

				if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == roleName)
				{
					var objDecision = new ApprovalCollaboratorDecision
					{
						Approval = approvalId,
						Collaborator = user.ID,
						AssignedDate = DateTime.UtcNow,
						Phase = user.Phase
					};
					context.ApprovalCollaboratorDecisions.Add(objDecision);
				}
			}
		}

		private static void SetGroupsPermissions(GMGColorContext context, int approvalId,
			PermissionsUsersAndRolesModel permissionsUsersAndRoles)
		{
			foreach (var group in permissionsUsersAndRoles.Groups)
			{
				var objCollaboratorGroup = new ApprovalCollaboratorGroup
				{
					Approval = approvalId,
					CollaboratorGroup = group.Group,
					Phase = group.Phase,
					AssignedDate = DateTime.UtcNow
				};

				context.ApprovalCollaboratorGroups.Add(objCollaboratorGroup);
			}
		}


		private static void SetUsersPermissionsPerf(GMGColorContext context, Approval approval, PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles)
		{
			int role = -1;
			ApprovalCollaboratorRole.ApprovalRoleName roleName = ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;
            List<ApprovalCollaborator> lstapprovalCollaborator = new List<ApprovalCollaborator>();
            List<ApprovalCollaboratorDecision> lstapprovalCollaboratorDecision = new List<ApprovalCollaboratorDecision>();

            foreach (var user in permissionsUsersAndRoles.Users)
			{
				var objCollaborator = new ApprovalCollaborator
				{
					Approval1 = approval,
					Collaborator = user.ID,
					AssignedDate = DateTime.UtcNow,
					ApprovalCollaboratorRole = user.Role,
					ApprovalJobPhase = user.Phase
				};
                lstapprovalCollaborator.Add(objCollaborator);

                if (user.Role != role)
				{
					role = user.Role;
					roleName = ApprovalCollaboratorRole.GetRole(user.Role, context);
				}

				if (ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer == roleName)
				{
					var objDecision = new ApprovalCollaboratorDecision
					{
						Approval1 = approval,
						Collaborator = user.ID,
						AssignedDate = DateTime.UtcNow,
						ApprovalJobPhase = user.Phase
					};
                    lstapprovalCollaboratorDecision.Add(objDecision);
                }
			}
            context.ApprovalCollaborators.AddRange(lstapprovalCollaborator);
            context.ApprovalCollaboratorDecisions.AddRange(lstapprovalCollaboratorDecision);
        }

        private static void SetGroupsPermissionsPerf(GMGColorContext context, Approval approval, PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles)
		{
            List<ApprovalCollaboratorGroup> lstapprovalCollaboratorGroup = new List<ApprovalCollaboratorGroup>();

            foreach (var group in permissionsUsersAndRoles.Groups)
			{
				var objCollaboratorGroup = new ApprovalCollaboratorGroup
				{
					Approval1 = approval,
					CollaboratorGroup = group.Group,
					ApprovalJobPhase = group.Phase,
					AssignedDate = DateTime.UtcNow
				};
                lstapprovalCollaboratorGroup.Add(objCollaboratorGroup);
			}
            context.ApprovalCollaboratorGroups.AddRange(lstapprovalCollaboratorGroup);
        }

        private static void SetUsersPermissionsPerfForProjectApprovals(GMGColorContext context, Approval approval, PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles)
        {
            var objApprovalCollaborators = (from p in  permissionsUsersAndRoles.Users
                                            select new ApprovalCollaborator 
                                            {
                                                Approval1 = approval,
                                                Collaborator = p.ID,
                                                AssignedDate = DateTime.UtcNow,
                                                ApprovalCollaboratorRole = p.Role,
                                                ApprovalJobPhase = p.Phase
                                            }).ToList();

            context.ApprovalCollaborators.AddRange(objApprovalCollaborators);

            var objApprovalCollaboratorDecision = (from p in permissionsUsersAndRoles.Users
                                                   where p.Role == 3
                                            select new ApprovalCollaboratorDecision
                                            {
                                                Approval1 = approval,
                                                Collaborator = p.ID,
                                                AssignedDate = DateTime.UtcNow,
                                                ApprovalJobPhase = p.Phase
                                            }).ToList();

            context.ApprovalCollaboratorDecisions.AddRange(objApprovalCollaboratorDecision);

        }

        private static void SetGroupsPermissionsPerfForProjectApprovals(GMGColorContext context, Approval approval, PermissionsUsersAndRolesModelPerf permissionsUsersAndRoles)
        {
            var objApprovalCollaboratorGroup = (from p in permissionsUsersAndRoles.Groups
                                                   
                                                   select new ApprovalCollaboratorGroup
                                                   {
                                                       Approval1 = approval,
                                                       CollaboratorGroup = p.Group,
                                                       AssignedDate = DateTime.UtcNow,
                                                       ApprovalJobPhase = p.Phase
                                                   }).ToList();

            context.ApprovalCollaboratorGroups.AddRange(objApprovalCollaboratorGroup);
        }

        public static List<SharedApproval> AddNewExternalUsersPerf(List<NewExternalUserPermissionsPerf> newExternalUsers, int loggedAccount, int loggedUserID, Approval approval, bool allowUsersToDownload, Dictionary<int, ApprovalRoleName> collaboratorRoles, string pdmEmail, DbContextBL context)
		{
			var avoidedColors = new List<string>();
			var sharedApprovals = new List<SharedApproval>();

			foreach (var newExternalUser in newExternalUsers)
			{
				string[] givenfamilyNames = newExternalUser.FullName.Split(' ');
				var externalCollaborator = new ExternalCollaborator();
				externalCollaborator.Account = loggedAccount;
				externalCollaborator.Guid = Guid.NewGuid().ToString();
				externalCollaborator.Creator = loggedUserID;
				externalCollaborator.CreatedDate = DateTime.UtcNow;
				externalCollaborator.IsDeleted = false;
				externalCollaborator.GivenName = givenfamilyNames[0].Trim();
				externalCollaborator.FamilyName = givenfamilyNames[1].Trim();
				externalCollaborator.EmailAddress = newExternalUser.Email.ToLower();
				externalCollaborator.Locale = newExternalUser.Locale;
				externalCollaborator.Modifier = loggedUserID;
				externalCollaborator.ModifiedDate = DateTime.UtcNow;
				externalCollaborator.ProofStudioColor = ExternalCollaboratorBL.GenerateCollaboratorProofStudioColor(loggedAccount, avoidedColors, context);
				avoidedColors.Add(externalCollaborator.ProofStudioColor);

                if (context.ExternalCollaborators.Local.Where(x => x.EmailAddress == externalCollaborator.EmailAddress).Any())
                {
                    externalCollaborator = context.ExternalCollaborators.Local.Where(x => x.EmailAddress == externalCollaborator.EmailAddress).FirstOrDefault();
                }
                else
                {
				context.ExternalCollaborators.Add(externalCollaborator);
                }

				if (!string.IsNullOrEmpty(pdmEmail) && externalCollaborator.EmailAddress == pdmEmail)
				{
					approval.ExternalCollaborator = externalCollaborator;
					approval.PrimaryDecisionMaker = null;
				}

				var sharedApproval = new SharedApproval
				{
					Approval1 = approval,
					ApprovalJobPhase = newExternalUser.Phase,
					ApprovalCollaboratorRole = newExternalUser.Role,
					IsSharedURL = true,
					IsSharedDownloadURL = allowUsersToDownload,
					IsExpired = false,
					SharedDate = DateTime.UtcNow,
					ExpireDate = DateTime.UtcNow.AddDays(7),
					Notes = string.Empty,
					ExternalCollaborator1 = externalCollaborator
				};

				context.SharedApprovals.Add(sharedApproval);
				
				sharedApprovals.Add(sharedApproval);

				if (ApprovalRoleName.ApproverAndReviewer == collaboratorRoles[newExternalUser.Role])
				{
					var objDecision = new ApprovalCollaboratorDecision
					{
						Approval1 = approval,
						AssignedDate = DateTime.UtcNow,
						ApprovalJobPhase = newExternalUser.Phase,
						ExternalCollaborator1 = externalCollaborator
					};

					context.ApprovalCollaboratorDecisions.Add(objDecision);
				}

			}
			return sharedApprovals;
		}

		public static List<SharedApproval> AddExistingExternalCollaboratorsPerf(List<ExternalUserPermissionsPerf> existingExternalUsers, Approval approval, bool allowUsersToDownload, Dictionary<int, ApprovalRoleName> collaboratorRoles, string pdmEmail, DbContextBL context)
		{
			List<SharedApproval> sharedApprovals = new List<SharedApproval>();
            List<ApprovalCollaboratorDecision> approvalCollaboratorDecision = new List<ApprovalCollaboratorDecision>();

            foreach (var externalUser in existingExternalUsers)
			{
				var sharedApproval = new SharedApproval();
				sharedApproval.Approval1 = approval;
				sharedApproval.ExternalCollaborator = externalUser.ID;
				sharedApproval.IsSharedDownloadURL = allowUsersToDownload;
				sharedApproval.IsSharedURL = true;
				sharedApproval.ApprovalJobPhase = externalUser.Phase;
				sharedApproval.ApprovalCollaboratorRole = externalUser.Role;
				sharedApproval.IsExpired = false;
				sharedApproval.SharedDate = DateTime.UtcNow;
				sharedApproval.ExpireDate = DateTime.UtcNow.AddDays(7);
				sharedApproval.Notes = string.Empty;

				sharedApprovals.Add(sharedApproval);

				if (ApprovalRoleName.ApproverAndReviewer == collaboratorRoles[externalUser.Role])
				{
					var decision = new ApprovalCollaboratorDecision();
					decision.Approval1 = approval;
					decision.AssignedDate = DateTime.UtcNow;
					decision.ApprovalJobPhase = externalUser.Phase;
					decision.ExternalCollaborator = externalUser.ID;
                    approvalCollaboratorDecision.Add(decision);

                }


				if (!string.IsNullOrEmpty(pdmEmail))
				{
					var externalCollaborator = context.ExternalCollaborators.FirstOrDefault(ec => ec.ID == sharedApproval.ExternalCollaborator && ec.EmailAddress == pdmEmail);
					if (externalCollaborator != null)
					{
						approval.ExternalPrimaryDecisionMaker = sharedApproval.ExternalCollaborator;
						approval.PrimaryDecisionMaker = null;
					}					
				}
			}
            context.SharedApprovals.AddRange(sharedApprovals);
            context.ApprovalCollaboratorDecisions.AddRange(approvalCollaboratorDecision);

            return sharedApprovals;
		}

		internal static List<SharedApproval> SetExternalCollaboratorsPermissions(string externalUsers, string externalEmails, ApprovalWorkflowPhasesInfoPerf approvalWorkflowPhases, int loggedAccountId, Dictionary<int, ApprovalRoleName> collaboratorRoles, Approval approval, bool allowUsersToDownload, string pdmEmail, DbContextBL context)
		{
			var sharedApprovals = new List<SharedApproval>();
			var currentPhase = approvalWorkflowPhases?.ApprovalWorkflowPhases[0].Phase;

			var permissions = GetExternalPermissionsPerf(externalUsers, externalEmails, currentPhase, approvalWorkflowPhases);

			var newExternalSharedApprovals = AddNewExternalUsersPerf(permissions.NewExternalUsers, loggedAccountId, approval.Creator, approval, allowUsersToDownload, collaboratorRoles, pdmEmail, context);
			if (newExternalSharedApprovals.Count > 0)
			{
				sharedApprovals.AddRange(newExternalSharedApprovals);
			}

			var externalSharedApprovals = AddExistingExternalCollaboratorsPerf(permissions.ExternalUsers, approval, allowUsersToDownload, collaboratorRoles, pdmEmail, context);
			if (externalSharedApprovals.Count > 0)
			{
				sharedApprovals.AddRange(externalSharedApprovals);
			}
			return sharedApprovals;
		}

        public static void UpdateProjectFolderApprovalOwner(GMGColorContext context, int approvalId, int creator, int? currentPhase, int? checkList)
        {
            var updateApprovalCreator = context.Approvals.Where(x => x.ID == approvalId).Select(x => x).FirstOrDefault();
            updateApprovalCreator.Creator = creator;
            updateApprovalCreator.CurrentPhase = currentPhase;
            if(checkList != null || checkList != 0)
            {
                updateApprovalCreator.Checklist = checkList;
            }
            var projectFolderId = (from itm in context.Jobs
                                          join ap in context.Approvals on itm.ID equals ap.Job
                                          where ap.ID == approvalId
                                          select itm.ProjectFolder).FirstOrDefault();

            var updateProjectFoldersStatus = context.ProjectFolders.Where(x => x.ID == projectFolderId).Select(x => x).FirstOrDefault();
            updateProjectFoldersStatus.Status = 2;
            context.SaveChanges();
        }

        public static void UpdateProjectFolderApprovalChecklist(GMGColorContext context, int approvalId,  int checkListId)
        {
            var objApproval = context.Approvals.Where(x => x.ID == approvalId).Select(x => x).FirstOrDefault();
            objApproval.Checklist = checkListId;
            context.SaveChanges();
        }

        public static int UpdateProjectFolderApprovalWorkflow(GMGColorContext context, int loggedAccount, int approvalId, int workFlowId, string phaseCollaborators, int creator)
        {
            var phasesCollaborators = new ApprovalWorkflowPhasesInfo();
            phasesCollaborators = Newtonsoft.Json.JsonConvert.DeserializeObject<ApprovalWorkflowPhasesInfo>(phaseCollaborators);

            DateTime deadLineDate = default(DateTime);
            Approval objApproval = null;
            objApproval = new Approval();
            objApproval = (from a in context.Approvals where a.ID == approvalId select a).FirstOrDefault();

            var deadlineTriggers = (from fdt in context.PhaseDeadlineTriggers select fdt).ToList();
            var deadlineUnits = (from fdu in context.PhaseDeadlineUnits select fdu).ToList();

            var selectedWorkflow = context.ApprovalWorkflows.Where(w => w.ID == workFlowId)
                .Select(n => new { n.Name, n.RerunWorkflow }).FirstOrDefault();
            var approvalJobWorkflow = new ApprovalJobWorkflow()
            {
                Name = selectedWorkflow.Name,
                Account = loggedAccount,
                Job = objApproval.Job,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow,
                CreatedBy = creator,
                ModifiedBy = creator,
                RerunWorkflow = selectedWorkflow.RerunWorkflow
            };

            context.ApprovalJobWorkflows.Add(approvalJobWorkflow);
            var phases = new ApprovalJobPhase();

            bool isfirstPhase = true;
            foreach (var phase in phasesCollaborators.ApprovalWorkflowPhases)
            {
                //add default phase template details except collaborators
                var phaseTemplate =
                    (from ap in context.ApprovalPhases where ap.ID == phase.Phase select ap)
                    .FirstOrDefault();

                if (phaseTemplate != null)
                {
                    ApprovalJobPhase approvalJobPhase = new ApprovalJobPhase();
                    approvalJobPhase.Name = phaseTemplate.Name;
                    //ApprovalJobWorkflow1 = approvalJobWorkflow,
                    approvalJobPhase.Position = phaseTemplate.Position;
                    approvalJobPhase.DecisionType = phaseTemplate.DecisionType;
                    approvalJobPhase.ApprovalTrigger = phaseTemplate.ApprovalTrigger;
                    approvalJobPhase.ShowAnnotationsToUsersOfOtherPhases = phaseTemplate.ShowAnnotationsToUsersOfOtherPhases;
                    approvalJobPhase.ShowAnnotationsOfOtherPhasesToExternalUsers = phaseTemplate.ShowAnnotationsOfOtherPhasesToExternalUsers;
                    approvalJobPhase.PhaseTemplateID = phaseTemplate.ID;
                    approvalJobPhase.Deadline = phaseTemplate.Deadline;
                    approvalJobPhase.DeadlineTrigger = phaseTemplate.DeadlineTrigger;
                    approvalJobPhase.DeadlineUnitNumber = phaseTemplate.DeadlineUnitNumber;
                    approvalJobPhase.DeadlineUnit = phaseTemplate.DeadlineUnit;
                    approvalJobPhase.IsActive = true;
                    approvalJobPhase.CreatedDate = DateTime.UtcNow;
                    approvalJobPhase.ModifiedDate = DateTime.UtcNow;
                    approvalJobPhase.ModifiedBy = creator;
                    approvalJobPhase.CreatedBy = creator;

                    if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                    {
                        if (phase.IsExternal)
                        {
                            approvalJobPhase.PrimaryDecisionMaker = null;
                            approvalJobPhase.ExternalPrimaryDecisionMaker = phase.PDMId ??
                                                                            phaseTemplate
                                                                                .ExternalPrimaryDecisionMaker;
                        }
                        else
                        {
                            approvalJobPhase.ExternalPrimaryDecisionMaker = null;
                            approvalJobPhase.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                        }
                    }

                    //set automatic phase deadline  
                    if (phaseTemplate.DeadlineTrigger != null && phaseTemplate.DeadlineUnitNumber != null &&
                        phaseTemplate.DeadlineUnit != null)
                    {
                        var deadlineTrigger = deadlineTriggers.Where(t => phaseTemplate.DeadlineTrigger == t.ID).Select(t => t.Key)
                            .FirstOrDefault();
                        var deadlineUnit = deadlineUnits.Where(u => phaseTemplate.DeadlineUnit == u.ID).Select(u => u.Key)
                            .FirstOrDefault();

                        if (isfirstPhase || deadlineTrigger != (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted)
                        {
                            approvalJobPhase.Deadline = ApprovalBL.GetAutomaticPhaseDeadline(deadLineDate, deadlineTrigger,
                                phaseTemplate.DeadlineUnitNumber, deadlineUnit);
                        }
                    }

                    context.ApprovalJobPhases.Add(approvalJobPhase);

                    if (isfirstPhase)
                    {
                        //objApproval.CurrentPhase = approvalJobPhase.ID;
                        if (phaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                        {
                            if (phase.IsExternal)
                            {
                                objApproval.ExternalPrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.ExternalPrimaryDecisionMaker;
                                objApproval.PrimaryDecisionMaker = null;
                            }
                            else
                            {
                                objApproval.PrimaryDecisionMaker = phase.PDMId ?? phaseTemplate.PrimaryDecisionMaker;
                                objApproval.ExternalPrimaryDecisionMaker = null;
                            }
                        }
                        //objApproval.ApprovalJobPhase = approvalJobPhase;

                        ////add relations between each workflow phase and current approval
                        var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                        {
                            Approval = objApproval.ID,
                            ApprovalJobPhase = approvalJobPhase,
                            PhaseMarkedAsCompleted = false,
                            CreatedBy = creator,
                            ModifiedBy = creator,
                            CreatedDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        objApproval.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);

                        isfirstPhase = false;
                        phases = approvalJobPhase;
                    }

                }
            }

            context.SaveChanges();
            return phases.ID;
        }

    }
}
