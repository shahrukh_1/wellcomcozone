﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorNotificationService;
using GMGColorDAL.CustomModels;

namespace GMGColorBusinessLogic
{
	public class AccountBL : BaseBL
    {
        /// <summary>
        /// Gets the account region.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        internal static string GetAccountRegion(string userGuid, GMGColorContext context)
        {
            try
            {
                // validate query only for active accounts
                return
                    (from u in context.Users
                     join a in context.Accounts on u.Account equals a.ID
                     join acs in context.AccountStatus on a.Status equals acs.ID
                     where u.Guid == userGuid && acs.Key == "A"
                     select a.Region).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CoulNotGetAccountRegion, ex);
            }
        }
        
        /// <summary>
        /// Gets the account region.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        public static string GetAccountRegion(int userId, GMGColorContext context)
        {
            try
            {
                return
                    (from u in context.Users
                     join a in context.Accounts on u.Account equals a.ID
                     where u.ID == userId
                     select a.Region).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CoulNotGetAccountRegion, ex);
            }
        }

        /// <summary>
        /// Gets the name of the time zone.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        internal static string GetTimeZoneName(int accountId, GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts where a.ID == accountId select a.TimeZone).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetTimeZoneNameByAccountId, ex);
            }
        }

        public static string GetAccountDomain(int accountId, GMGColorContext context)
        {
            string domain = (from ac in context.Accounts
                             where ac.ID == accountId
                             select ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain).FirstOrDefault();

            return domain;
        }

        /// <summary>
        /// Gets the list of modules that has billing plan.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        internal static List<GMG.CoZone.Common.AppModule> GetModules(int accountId, GMGColorContext context)
        {
            try
            {
                var spiModules = (from acc in context.Accounts
                                  join spi in context.SubscriberPlanInfoes on acc.ID equals spi.Account
                                  join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                                  join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                  join am in context.AppModules on bpt.AppModule equals am.ID
                                  where acc.ID == accountId
                                  select (GMG.CoZone.Common.AppModule)am.Key).ToList();
                var aspModules = (from acc in context.Accounts
                                  join asp in context.SubscriberPlanInfoes on acc.ID equals asp.Account
                                  join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                                  join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                  join am in context.AppModules on bpt.AppModule equals am.ID
                                  where acc.ID == accountId
                                  select (GMG.CoZone.Common.AppModule)am.Key).ToList();
                return spiModules.Union(aspModules).ToList();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetModulesThatHasBillingPlan, ex);
            }
        }

        /// <summary>
        /// Gets the list of modules that has billing plan.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <returns></returns>
        public static List<GMG.CoZone.Common.AppModule> GetModules(int accountId)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return GetModules(accountId, context);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetModulesThatHasBillingPlan, ex);
            }
        }

        /// <summary>
        /// Gets the users from specified account.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <returns></returns>
        public static List<KeyValuePair<int, string>> GetUsers(int accountId)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return (from u in context.Users
                            join uss in context.UserStatus on u.Status equals uss.ID
                            where u.Account == accountId && uss.Key == "A"
                            select new { u.ID, u.Username}).ToList().Select(
                                o => new KeyValuePair<int, string>(o.ID, o.Username)).ToList();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUsersByAccountId, ex);
            }
        }

        public static bool HasMultimediaOptionsEnabled(int accountID, GMG.CoZone.Common.AppModule module, DbContextBL context)
        {
            try
            {
                return Account.IsEnableMediaTools(accountID, module, context);
            }
            catch (System.Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetMultimediaOptions, ex);
            }

        }

        public static bool HasPrePressOptionsEnabled(int accountID, GMG.CoZone.Common.AppModule module, DbContextBL context)
        {
            try
            {
                return Account.IsEnablePrePressTools(accountID, module, context);
            }
            catch (System.Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetPrePressSettings, ex);
            }

        }

        public static List<AccountsReportView> GetAccountsReport(string selectedLocations, string selectedAccountTypes, string selectedAccountStatuses, string selectedAccountSubsciptionPlans,
                                                                Account loggedAccount, AccountType.Type loggedAccountType, DbContextBL context)
        {
            List<AccountsReportView> lstAccounts = new GMGColorDAL.Account().GetAccountsReport(selectedLocations, selectedAccountTypes, selectedAccountStatuses, selectedAccountSubsciptionPlans, loggedAccount.ID, context);

            return lstAccounts;
        }

        public static void SuspendAccount(int accountId, string reason, int loggedUserId, DbContextBL context, bool suspendedOrDeletedByParent = true)
        {
            try
            {
                Account objAccount = (from a in context.Accounts where a.ID == accountId select a).SingleOrDefault();

                if (objAccount.AccountStatu.Key != "S")
                {
                    int suspendedStatus =
                        GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Suspended, context).ID;
                    objAccount.Status = suspendedStatus;
                    objAccount.SuspendReason = reason;
                    objAccount.IsSuspendedOrDeletedByParent = suspendedOrDeletedByParent;
                    objAccount.ModifiedDate = DateTime.UtcNow;
                    objAccount.Modifier = loggedUserId;

                    List<Account> childrenToBeSuspended = (from ac in context.Accounts
                        join acs in context.AccountStatus on ac.Status equals acs.ID
                        where ac.Parent == objAccount.ID && acs.Key != "D" && acs.Key != "S" 
                        select ac).ToList();

                    foreach (Account account in childrenToBeSuspended)
                    {
                        account.Status = suspendedStatus;
                        account.SuspendReason = reason;
                        account.IsSuspendedOrDeletedByParent = false;
                        account.ModifiedDate = DateTime.UtcNow;
                        account.Modifier = loggedUserId;

                        if (account.Account1.Count > 0)
                        {
                            foreach (var childAccount in account.Account1)
                            {
                                SuspendAccount(childAccount.ID, reason, loggedUserId, context, false);
                            }
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotSuspendAccount, ex);
            }
        }

        public static void RestoreAccount(int accountId, DbContextBL context)
        {
            try
            {
                Account objAccount = (from a in context.Accounts where a.ID == accountId select a).SingleOrDefault();

                if (objAccount.AccountStatu.Key == "S")
                {
                    int accountRestoreStatus =
                        (GMGColorDAL.UserStatu.GetUserStatus(objAccount.OwnerUser.Status, context) ==
                         GMGColorDAL.UserStatu.Status.Invited)
                            ? GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Inactive,
                                context).ID
                            : GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Active, context)
                                .ID;

                    objAccount.Status = accountRestoreStatus;
                    objAccount.IsSuspendedOrDeletedByParent = false;
                    objAccount.SuspendReason = string.Empty;

                    foreach (GMGColorDAL.Account account in objAccount.GetChilds(context))
                    {
                        GMGColorDAL.Account objSubAccount = DALUtils.GetObject<GMGColorDAL.Account>(account.ID,
                            context);

                        if (!objSubAccount.IsSuspendedOrDeletedByParent)
                        {
                             int subAccountRestoreStatus =
                                    (GMGColorDAL.UserStatu.GetUserStatus(objSubAccount.OwnerUser.Status,
                                        context) == GMGColorDAL.UserStatu.Status.Invited)
                                        ? GMGColorDAL.AccountStatu.GetAccountStatus(
                                            GMGColorDAL.AccountStatu.Status.Inactive, context).ID
                                        : GMGColorDAL.AccountStatu.GetAccountStatus(
                                            GMGColorDAL.AccountStatu.Status.Active, context).ID;

                                objSubAccount.Status = subAccountRestoreStatus;
                                objSubAccount.SuspendReason = string.Empty;

                            foreach (var childAccount in objSubAccount.Account1)
                            {
                                RestoreAccount(childAccount.ID, context);
                            }
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotRestoreAccount, ex);
            }
        }

        public static void DeleteAccount(int accountId, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            try
            {
                var objAccountInfo = (from a in context.Accounts
                                      let shWf = (from shw in context.SharedColorProofWorkflows
                                                  join cpczw in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals cpczw.ID
                                                  join cpw in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpw.ID
                                                  join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                                                  join u in context.Users on cpi.Owner equals u.ID
                                                  where u.Account == a.ID
                                                  select shw).ToList()
                                      where a.ID == accountId
                                      select new
                                      {
                                          account = a,
                                          sharedWorkflows = shWf,
                                          accountName = a.Name
                                      }).SingleOrDefault();

                Account objAccount = objAccountInfo.account;

                foreach (var share in objAccountInfo.sharedWorkflows)
                {
                    DeliverBL.DeleteSharedColorProofWorkflow(share, context);
                }

                if (objAccount.AccountStatu.Key != "A")
                {
                    objAccount.Status = GMGColorDAL.AccountStatu.GetAccountStatus(GMGColorDAL.AccountStatu.Status.Deleted, context).ID;
                    objAccount.IsSuspendedOrDeletedByParent = true;
                    objAccount.Domain = objAccount.Domain + "_Deleted";
                    objAccount.DeletedBy = loggedUser.ID;
                    objAccount.DeletedDate = DateTime.UtcNow;

                    foreach (GMGColorDAL.Account account in objAccount.GetChilds(context))
                    {
                        List<SharedColorProofWorkflow> sharedWorkflows = (from shw in context.SharedColorProofWorkflows
                                                                          join cpczw in context.ColorProofCoZoneWorkflows on shw.ColorProofCoZoneWorkflow equals cpczw.ID
                                                                          join cpw in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpw.ID
                                                                          join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                                                                          join u in context.Users on cpi.Owner equals u.ID
                                                                          join ac in context.Accounts on u.Account equals ac.ID
                                                                          where ac.ID == account.ID
                                                                          select shw).ToList();

                        foreach (var share in sharedWorkflows)
                        {
                            DeliverBL.DeleteSharedColorProofWorkflow(share, context);

                            var workflowName = DeliverBL.GetColorProofCoZoneWorkflowName(share.ColorProofCoZoneWorkflow, context);
                            NotificationServiceBL.CreateNotification(new SharedWorkflowDeleted()
                                                                    {
                                                                        EventCreator = loggedUser.ID,
                                                                        InternalRecipient = share.User,
                                                                        WorkflowName = workflowName
                                                                    },
                                                                    loggedUser,
                                                                    context
                                                                    );
                        }
                    
                        account.Status = objAccount.Status;
                        account.IsSuspendedOrDeletedByParent = false;
                        account.Domain = account.Domain + "_Deleted";
                        account.DeletedBy = loggedUser.ID;
                        account.DeletedDate = DateTime.UtcNow;

                        NotificationServiceBL.CreateNotification(new AccountWasDeleted
                                                            {
                                                                EventCreator = loggedUser.ID,
                                                                AccountName = account.Name
                                                            },
                                                            loggedUser,
                                                            context
                                                            );

                        foreach (var childAccount in account.Account1)
                        {
                            DeleteAccount(childAccount.ID, loggedUser, context);
                        }
                    }

                    NotificationServiceBL.CreateNotification(new AccountWasDeleted
                                                             {
                                                                EventCreator = loggedUser.ID,
                                                                AccountName = objAccountInfo.accountName
                                                            },
                                                            loggedUser,
                                                            context
                                                            );
                }

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteAccount, ex);
            }
        }

        /// <summary>
        /// Gets date formatt pattern for specified user account
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetAccountDateFormatPattern(int userId, DbContextBL context)
        {
            return (from us in context.Users
                    join ac in context.Accounts on us.Account equals ac.ID
                    join df in context.DateFormats on ac.DateFormat equals df.ID
                    where us.ID == userId
                    select df.Pattern).FirstOrDefault();
        }

        /// <summary>
        /// Gets timezone for specified user account
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetAccountTimeZone(int userId, DbContextBL context)
        {
            return (from us in context.Users
                    join ac in context.Accounts on us.Account equals ac.ID
                    where us.ID == userId
                    select ac.TimeZone).FirstOrDefault();
        }

        public static string GetRegionByID(int accId, DbContextBL context)
        {
            return (from ac in context.Accounts
                    where ac.ID == accId
                    select ac.Region).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves the time format for the specified user account
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUSerAccountAccountTimeFormat(int userID, DbContextBL context)
        {
            string timeFormat = (from us in context.Users
                                 join ac in context.Accounts on us.Account equals ac.ID
                                 join tf in context.TimeFormats on ac.TimeFormat equals tf.ID
                                 where us.ID == userID
                                 select tf.Key).FirstOrDefault();

            string format = String.Empty;
            switch (TimeFormat.GetTimeFormat(timeFormat))
            {
                case TimeFormat.Format._12Hour:
                {
                    format = TimeFormat.TimeFormat12;
                    break;
                }
                case TimeFormat.Format._24Hour:
                {
                    format = TimeFormat.TimeFormat24;
                    break;
                }
            }

            return format;
        }

        public static string GetAccountName(int accId, DbContextBL context)
        {
            return (from ac in context.Accounts
                    where ac.ID == accId
                    select ac.Name).FirstOrDefault();
        }

        /// <summary>
        /// Gets Company Name based on account ID
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetCompanyNameByAccountID(int accId, DbContextBL context)
        {
            return (from ac in context.Accounts
                    join com in context.Companies on ac.ID equals com.Account
                    where ac.ID == accId
                    select com.Name).FirstOrDefault();
        }

        /// <summary>
        /// Get admins ids based on account id
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<int> GetAdminIdsByAccount(int accountId, DbContextBL context)
        {
            return (from u in context.Users
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where
                        u.Account == accountId && r.Key == "ADM" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Admin && us.Key == "A"
                    select u.ID).Distinct();
        }

        /// <summary>
        /// Returns the data needed for clean up account assets from S3
        /// </summary>
        /// <param name="accId">Account id to be deleted</param>
        /// <returns></returns>
        public static AccountCustomProperties GetAccountDetailsForCleanupAssets(int accId, DbContextBL context)
        {
            return (from ac in context.Accounts
                    let approvalsGuids = (from ap in context.Approvals
                                            join j in context.Jobs on ac.ID equals j.Account
                                            where ap.Job == j.ID
                                            select ap.Guid).ToList()
                    let deliverGuids = (from dj in context.DeliverJobs
                                        join j in context.Jobs on ac.ID equals j.Account
                                        where dj.Job == j.ID
                                        select dj.Guid).ToList()                   
                    let userIds = (from u in context.Users
                                    where u.Account == ac.ID
                                    select u.ID).ToList()
                    where ac.ID == accId
                    select new AccountCustomProperties
                        {
                            Guid = ac.Guid,
                            Region = ac.Region,
                            ApprovalGuids = approvalsGuids,
                            DeliverGuids = deliverGuids,
                            UserIds = userIds
                        }).FirstOrDefault();            
        }


        /// <summary>
        /// Cleanup assets For the current Account
        /// </summary>
        /// <param name="accId">Account which should be cleaned</param>
        /// <param name="accountInfo">AccountCustomProperties object</param>
        public static void CleanupAccountAssets(int accId, AccountCustomProperties accountInfo)
        {
            if (accountInfo != null)
            {
                #region Account Folder

                //delete account guid folder from storage
                GMGColorIO.DeleteFolderIfExists(String.Format("{0}/{1}/", GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath,  accountInfo.Guid), accountInfo.Region);

                #endregion

                #region Temp Folder

                //cleanup users folder from temp
                foreach (int userId in accountInfo.UserIds)
                {
                    string subfolderPath = userId.ToString("000") + "-" + accId.ToString("000");

                    GMGColorIO.DeleteFolderIfExists(String.Format("temp/{0}/", subfolderPath), accountInfo.Region);
                }

                #endregion

                #region Approval Folders

                //cleanup approval jobs folders from storage
                foreach (string appGuid in accountInfo.ApprovalGuids)
                {
                    GMGColorIO.DeleteFolderIfExists(String.Format("{0}/{1}/", GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath, appGuid), accountInfo.Region);
                }

                #endregion

                #region Deliver Folders

                //cleanup deliver jobs folders from storage
                foreach (string delGuid in accountInfo.DeliverGuids)
                {
                    GMGColorIO.DeleteFolderIfExists(String.Format("{0}/{1}/", GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath, delGuid), accountInfo.Region);
                }

                #endregion
            }
        }

        /// <summary>
        /// Get a list of accounts ids that should be deleted based on the selected account id
        /// </summary>
        /// <param name="context">Database context</param>
        public static List<int> GetAccountsIdsForDelete(DbContextBL context)
        {
            return (from acc in context.Accounts 
                    where acc.Status == 5 
                    select acc.ID).ToList();
        }
       

        /// <summary>
        /// (Stored procedure) Delete all accounts, marked as deleted, from database 
        /// </summary>
        /// <param name="context">Database context</param>
        /// <returns>Rtruns empty string if delete opeartion succeed</returns>
        public static void DeleteAccount(int accountId, DbContextBL context)
        {
            try
            {
                 context.DeleteAccount(accountId);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error occured in DeleteAccount() for account id {0}", accountId), ex);
            }
            finally
            {
            }
        }

        public static List<int> GetAccountsWithNoChildren(DbContextBL context)
        {
            return (from a in context.Accounts
                    join acs in context.AccountStatus on a.Status equals acs.ID
                    where (acs.Key == "D" && DbFunctions.DiffYears(a.DeletedDate, DateTime.UtcNow) >= 1 && !context.Accounts.Any(acc => (acc.Parent == a.ID)))
                        || (a.IsTemporary && DbFunctions.DiffHours(a.CreatedDate, DateTime.UtcNow) > 24)
                    select a.ID).Take(5).ToList();
        }

        public static string GetAccountCultureByUserId(int userId, DbContextBL context)
        {
            return (from a in context.Accounts
                    join l in context.Locales on a.Locale equals l.ID
                    join u in context.Users on a.ID equals u.Account
                    where u.ID == userId
                    select l.Name).SingleOrDefault();
        }

		public static string GetAccountCultureByDeliverJobId(int deliverJobId, DbContextBL context)
		{
			return (from a in context.Accounts
					join l in context.Locales on a.Locale equals l.ID
					join j in context.Jobs on a.ID equals j.Account
					join d in context.DeliverJobs on j.ID equals d.Job
					where d.ID == deliverJobId
					select l.Name).SingleOrDefault();
		}

		public static void DeleteGarbageAccountFolders(DbContextBL context)
        {
            try
            {
                List<string> accountsFolders = GMGColor.AWS.AWSSimpleStorageService.GetAllFolders(
                        GMGColorConfiguration.AppConfiguration.DataFolderName(GMGColorConfiguration.AppConfiguration.AWSRegion),
                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey, "accounts/");

                foreach (string accountsFolder in accountsFolders)
                {
                    string accountGuid = System.IO.Path.GetFileName(accountsFolder.Remove(accountsFolder.Length - 1));
                    if (!context.Accounts.Any(t => t.Guid == accountGuid))
                    {
                        GMGColorDAL.Common.GMGColorIO.DeleteFolder(accountsFolder, GMGColorConfiguration.AppConfiguration.AWSRegion);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string GetColorSchemeByAccountId(DbContextBL context, int accountId, int userId, bool isExternal)
        {
            string themeKey = isExternal
                ? string.Empty
                : (from bp in context.BrandingPresets
                    join th in context.BrandingPresetThemes on bp.ID equals th.BrandingPreset
                    join bpug in context.BrandingPresetUserGroups on bp.ID equals bpug.BrandingPreset
                    join ug in context.UserGroups on bpug.UserGroup equals ug.ID
                    join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                    where ugu.User == userId && ugu.IsPrimary && th.Active == true
                    select th.Key).FirstOrDefault();

            if (string.IsNullOrEmpty(themeKey))
            {
                themeKey = (from a in context.Accounts
                    join th in context.AccountThemes on a.ID equals th.Account
                    where a.ID == accountId && th.Active == true
                    select th.Key).FirstOrDefault();
            }

            return Theme.GetColorTheme(themeKey);
        }
		
		public static Account GetAccountByUserGuid(string userGuid, DbContextBL context)
        {
            try
            {
                    return (from u in context.Users
                            join a in context.Accounts on u.Account equals a.ID
                            join ass in context.AccountStatus on a.Status equals ass.ID
                            where u.Guid == userGuid && ass.Key == "A"
                            select a).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAccount, ex);
            }
        }

        public static bool HasRetoucherUser(int accId, DbContextBL context)
        {
            return Account.HasRetoucherUser(accId, context);
        }

        public static void ResetAccountUserdStorage(int accId, DbContextBL context) 
        {
            var accountUsedStorage = (from asu in context.AccountStorageUsages
                                      where asu.Account == accId
                                      select asu).FirstOrDefault();

            if (accountUsedStorage != null)
            {
                accountUsedStorage.UsedStorage = 0;
            }
        }

        public static GMGColorDAL.CustomModels.EditAccount.BillingInfo PopulateBillingInfoModel(int accId, GMGColorDAL.CustomModels.SubscriberPlanPerModule subscriberModel, GMGColorDAL.CustomModels.SubscriptionPlanPerModule subscriptionModel, bool isEditAccountPlansFromReport, Account loggedAccount, DbContextBL context)
        {
            var model = new GMGColorDAL.CustomModels.EditAccount.BillingInfo(accId, context);

            var objAccount = context.Accounts.FirstOrDefault(ac => ac.ID == accId);

            if (GMGColorDAL.AccountType.GetAccountType(objAccount, context) != GMGColorDAL.AccountType.Type.Subscriber)
            {
                if (subscriptionModel != null)
                {
                    model.PlanPerModule = subscriptionModel;
                    model.PlanPerModule = UpdateDropDownsDataSourceForSubscriptionPlanPerModule(model.PlanPerModule, false, context, true);
                }
                else
                {
                    model.PlanPerModule = UpdateDropDownsDataSourceForSubscriptionPlanPerModule(model.PlanPerModule, true, context);
                }
            }
            else
            {
                if (subscriberModel != null)
                {
                    model.SubscriberPlans = subscriberModel;
                    model.SubscriberPlans = UpdateDropDownsDataSourceForSubscriberPlanPerModule(model.SubscriberPlans, false, loggedAccount, context, true);
                }
                else
                {
                    model.SubscriberPlans = UpdateDropDownsDataSourceForSubscriberPlanPerModule(model.SubscriberPlans, true, loggedAccount, context);
                }
            }
            model.IsEditAccountPlansFromReport = isEditAccountPlansFromReport;
            return model;
        }

        /// <summary>
        /// If updateAll is true then all the properties on model are updated from DB.
        /// If updateAll is false then only the BillingPlans lists are updated form DB.
        /// if updateAll is false and updateContractStartDate is true then BillingPlans lists and ContractStartDate property are updated form DB.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateAll"></param>
        /// <param name="context"></param>
        /// <param name="updateContractStartDate"></param>
        /// <returns></returns>
        public static SubscriptionPlanPerModule UpdateDropDownsDataSourceForSubscriptionPlanPerModule(SubscriptionPlanPerModule model, bool updateAll, DbContextBL context, bool updateContractStartDate = false)
        {
            GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.AccountId, context);
            string datePattern = objAccount.DateFormat1.Pattern;
            #region Bind Subscription Plans Per Module

            List<GMGColorDAL.AccountSubscriptionPlan> lstAccountSubscriptionPlans = GMGColorDAL.AccountSubscriptionPlan.GetSubscriptionPlanPerAccount(model.AccountId, context);

            AccountBillingPlans objAccountBillingPlans = GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(objAccount, true, context);

            decimal discount = (objAccount.GPPDiscount != null) ? (objAccount.GPPDiscount.Value / 100) : 0;
            decimal currRate = 1;
            if ((GMGColorDAL.AccountType.GetAccountType(objAccount.ParentAccount, context) != GMGColorDAL.AccountType.Type.GMGColor) && objAccount.CurrencyFormat != objAccount.ParentAccount.CurrencyFormat)
            {
                currRate = (new GMGColorDAL.Common.GMGColorCommon()).GetUpdatedCurrencyRate(objAccount, context);
            }

            #region Collaborate Module

            List<BillingPlanTypePlans> listCollaborate = objAccountBillingPlans.BillingPlans.Where(o => o.BillingPlanTypeAppModuleKey == (int)GMG.CoZone.Common.AppModule.Collaborate).ToList();
            foreach (BillingPlanTypePlans billingPlanTypePlansItem in listCollaborate)
            {
                foreach (BillingPlanInfo billingPlanInfo in billingPlanTypePlansItem.BillingPlans)
                {
                    decimal planPrice = GMGColorDAL.AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.GetValueOrDefault(), billingPlanInfo.ID, context, currRate);

                    model.CollaborateBillingPlans.Add(new BillingPlanItem()
                    {
                        Id = billingPlanInfo.ID,
                        Title = string.Format("{0} ( {1} )", billingPlanInfo.Name, GMGColorFormatData.GetFormattedCurrency(planPrice, objAccount.Parent.GetValueOrDefault(), context)),
                        IsFixedPrice = billingPlanInfo.IsFixed,
                        Price = string.Format("{0}", planPrice),
                        Cpp = billingPlanInfo.CostPerProof,
                        IsDemo = billingPlanInfo.IsDemo,
                        MaxStorage = billingPlanInfo.Storage
                    });
                }
            }
            model.CollaborateBillingPlans.Sort((a, b) => (System.String.Compare(a.Title, b.Title, System.StringComparison.OrdinalIgnoreCase)));

            GMGColorDAL.AccountSubscriptionPlan collaborateSelectedPlan =
                lstAccountSubscriptionPlans.FirstOrDefault(
                    o =>
                    o.BillingPlan.BillingPlanType.AppModule1.Key == (int)GMG.CoZone.Common.AppModule.Collaborate);

            if (collaborateSelectedPlan != null)
            {
                if (updateAll)
                {
                    model.CollaborateBillingPlanId = collaborateSelectedPlan.SelectedBillingPlan;
                    model.CollaborateChargeCostPerProofIfExceeded = collaborateSelectedPlan.ChargeCostPerProofIfExceeded;
                    model.CollaborateContractStartDate = collaborateSelectedPlan.ContractStartDate != null ? collaborateSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                    model.CollaborateAllowOverdraw = collaborateSelectedPlan.AllowOverdraw;
                    model.CollaborateIsMonthlyBillingFrequency =
                        (Enums.BillingFrequency)Enum.Parse(typeof(Enums.BillingFrequency),
                                                            collaborateSelectedPlan.IsMonthlyBillingFrequency
                                                                ? Convert.ToString((int)Enums.BillingFrequency.Monthly)
                                                                : Convert.ToString((int)Enums.BillingFrequency.Yearly));
                    model.CollaborateDemoRemainingDays = collaborateSelectedPlan.AccountPlanActivationDate.HasValue ?
                        (collaborateSelectedPlan.AccountPlanActivationDate.Value.AddDays(GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) - DateTime.UtcNow)
                        .Days : (int?)null;
                }
                else if (updateContractStartDate)
                {
                    model.CollaborateContractStartDate = collaborateSelectedPlan.ContractStartDate != null ? collaborateSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                }
            }

            #endregion

            #region Deliver Module

            List<BillingPlanTypePlans> listDeliver = objAccountBillingPlans.BillingPlans.Where(o => o.BillingPlanTypeAppModuleKey == (int)GMG.CoZone.Common.AppModule.Deliver).ToList();
            foreach (BillingPlanTypePlans billingPlanTypePlansItem in listDeliver)
            {
                foreach (BillingPlanInfo billingPlanInfo in billingPlanTypePlansItem.BillingPlans)
                {
                    decimal planPrice = GMGColorDAL.AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.GetValueOrDefault(), billingPlanInfo.ID, context, currRate);
                    string price = GMGColorFormatData.GetFormattedCurrency((planPrice - planPrice * discount), objAccount.Parent.GetValueOrDefault(), context);

                    model.DeliverBillingPlans.Add(new BillingPlanItem()
                    {
                        Id = billingPlanInfo.ID,
                        Title = string.Format("{0} ( {1} )", billingPlanInfo.Name, GMGColorFormatData.GetFormattedCurrency(planPrice, objAccount.Parent.GetValueOrDefault(), context)),
                        IsFixedPrice = billingPlanInfo.IsFixed,
                        Price = string.Format("{0}", planPrice),
                        Cpp = billingPlanInfo.CostPerProof,
                        IsDemo = billingPlanInfo.IsDemo
                    });
                }
            }
            model.DeliverBillingPlans.Sort((a, b) => (System.String.Compare(a.Title, b.Title, System.StringComparison.OrdinalIgnoreCase)));

            GMGColorDAL.AccountSubscriptionPlan deliverSelectedPlan =
                lstAccountSubscriptionPlans.FirstOrDefault(
                    o => o.BillingPlan.BillingPlanType.AppModule1.Key == (int)GMG.CoZone.Common.AppModule.Deliver);
            if (deliverSelectedPlan != null)
            {
                if (updateAll)
                {
                    model.DeliverBillingPlanId = deliverSelectedPlan.SelectedBillingPlan;
                    model.DeliverChargeCostPerProofIfExceeded = deliverSelectedPlan.ChargeCostPerProofIfExceeded;
                    model.DeliverContractStartDate = deliverSelectedPlan.ContractStartDate != null ? deliverSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                    model.DeliverAllowOverdraw = deliverSelectedPlan.AllowOverdraw;
                    model.DeliverIsMonthlyBillingFrequency = (GMGColorDAL.CustomModels.Enums.BillingFrequency)
                        Enum.Parse(typeof(Enums.BillingFrequency),
                            deliverSelectedPlan.IsMonthlyBillingFrequency
                                ? Convert.ToString((int)Enums.BillingFrequency.Monthly)
                                : Convert.ToString((int)Enums.BillingFrequency.Yearly));
                    model.DeliverDemoRemainingDays = deliverSelectedPlan.AccountPlanActivationDate.HasValue ?
                       (deliverSelectedPlan.AccountPlanActivationDate.Value.AddDays(GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) - DateTime.UtcNow)
                       .Days : (int?)null;
                }
                else if (updateContractStartDate)
                {
                    model.DeliverContractStartDate = deliverSelectedPlan.ContractStartDate != null ? deliverSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                }
            }

            #endregion

            #endregion

            #region Extra Costs

            if (model.CollaborateBillingPlanId.HasValue)
            {
                decimal costPerProof = GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(objAccount.Parent.GetValueOrDefault(), model.CollaborateBillingPlanId.Value, context);
                model.CollaborateCostPerProof = GMGColorFormatData.GetFormattedCurrency(costPerProof, objAccount.Parent.GetValueOrDefault(), context);
            }          
            if (model.DeliverBillingPlanId.HasValue)
            {
                decimal costPerProof = GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(objAccount.Parent.GetValueOrDefault(), model.DeliverBillingPlanId.Value, context);
                model.DeliverCostPerProof = GMGColorFormatData.GetFormattedCurrency(costPerProof, objAccount.Parent.GetValueOrDefault(), context);
            }           

            #endregion

            return model;
        }

        /// <summary>
        /// If updateAll is true then all the properties on model are updated from DB.
        /// If updateAll is false then only the BillingPlans lists are updated form DB.
        /// if updateAll is false and updateContractStartDate is true then BillingPlans lists and ContractStartDate property are updated form DB.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="updateAll"></param>
        /// <param name="context"></param>
        /// <param name="updateContractStartDate"></param>
        /// <returns></returns>
        public static SubscriberPlanPerModule UpdateDropDownsDataSourceForSubscriberPlanPerModule(SubscriberPlanPerModule model, bool updateAll, Account loggedAccount, DbContextBL context, bool updateContractStartDate = false)
        {
            GMGColorDAL.Account objAccount = DALUtils.GetObject<GMGColorDAL.Account>(model.AccountId, context);

            #region Bind Subscription Plans Per Module

            List<GMGColorDAL.SubscriberPlanInfo> lstAccountSubscriptionPlans = new GMGColorDAL.SubscriberPlanInfo().GetSubscriberPlanPerAccount(model.AccountId, context);

            AccountBillingPlans objAccountBillingPlans = GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(objAccount, true, context);

            decimal discount = (objAccount.GPPDiscount != null) ? (objAccount.GPPDiscount.Value / 100) : 0;
            decimal currRate = 1;
            if ((GMGColorDAL.AccountType.GetAccountType(objAccount.ParentAccount, context) != GMGColorDAL.AccountType.Type.GMGColor) && objAccount.CurrencyFormat != objAccount.ParentAccount.CurrencyFormat)
            {
                currRate = (new GMGColorDAL.Common.GMGColorCommon()).GetUpdatedCurrencyRate(objAccount, context);
            }

            #region Collaborate Module

            List<BillingPlanTypePlans> listCollaborate = objAccountBillingPlans.BillingPlans.Where(o => o.BillingPlanTypeAppModuleKey == (int)GMG.CoZone.Common.AppModule.Collaborate).ToList();
            foreach (BillingPlanTypePlans billingPlanTypePlansItem in listCollaborate)
            {
                foreach (BillingPlanInfo billingPlanInfo in billingPlanTypePlansItem.BillingPlans)
                {
                    decimal planPrice = GMGColorDAL.AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.GetValueOrDefault(), billingPlanInfo.ID, context, currRate);
                    string price = GMGColorFormatData.GetFormattedCurrency((planPrice - planPrice * discount), objAccount.Parent.GetValueOrDefault(), context);

                    model.CollaborateBillingPlans.Add(new BillingPlanItem()
                    {
                        Id = billingPlanInfo.ID,
                        Title = string.Format("{0} ( {1} )", billingPlanInfo.Name, GMGColorFormatData.GetFormattedCurrency(planPrice, objAccount.Parent.GetValueOrDefault(), context)),
                        IsFixedPrice = billingPlanInfo.IsFixed,
                        Price = string.Format("{0}", planPrice),
                        Cpp = billingPlanInfo.CostPerProof,
                        IsDemo = billingPlanInfo.IsDemo,
                        MaxStorage = billingPlanInfo.Storage
                    });
                }
            }
            model.CollaborateBillingPlans.Sort((a, b) => (System.String.Compare(a.Title, b.Title, System.StringComparison.OrdinalIgnoreCase)));

            GMGColorDAL.SubscriberPlanInfo collaborateSelectedPlan =
                lstAccountSubscriptionPlans.FirstOrDefault(
                    o =>
                    o.BillingPlan.BillingPlanType.AppModule1.Key == (int)GMG.CoZone.Common.AppModule.Collaborate);

            string datePattern = objAccount.DateFormat1.Pattern;

            if (collaborateSelectedPlan != null)
            {
                if (updateAll)
                {
                    model.CollaborateBillingPlanId = collaborateSelectedPlan.SelectedBillingPlan;
                    model.CollaborateChargeCostPerProofIfExceeded = collaborateSelectedPlan.ChargeCostPerProofIfExceeded;
                    model.CollaborateContractStartDate = collaborateSelectedPlan.ContractStartDate != null ? collaborateSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                    model.CollaborateNrOfCredits = collaborateSelectedPlan.NrOfCredits;
                    model.CollaborateCreditAllocationType = collaborateSelectedPlan.IsQuotaAllocationEnabled ? CreditAllocationTypeEnum.Quota : CreditAllocationTypeEnum.Floating;
                    model.CollaborateAllowOverdraw = collaborateSelectedPlan.AllowOverdraw;
                    model.CollaborateDemoRemainingDays = collaborateSelectedPlan.AccountPlanActivationDate.HasValue ?
                   (collaborateSelectedPlan.AccountPlanActivationDate.Value.AddDays(GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) - DateTime.UtcNow).Days : (int?)null;
                }
                else if (updateContractStartDate)
                {
                    model.CollaborateContractStartDate = collaborateSelectedPlan.ContractStartDate != null ? collaborateSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                }
            }


            #endregion

            #region Deliver Module

            List<BillingPlanTypePlans> listDeliver = objAccountBillingPlans.BillingPlans.Where(o => o.BillingPlanTypeAppModuleKey == (int)GMG.CoZone.Common.AppModule.Deliver).ToList();
            foreach (BillingPlanTypePlans billingPlanTypePlansItem in listDeliver)
            {
                foreach (BillingPlanInfo billingPlanInfo in billingPlanTypePlansItem.BillingPlans)
                {
                    decimal planPrice = GMGColorDAL.AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.GetValueOrDefault(), billingPlanInfo.ID, context, currRate);
                    string price = GMGColorFormatData.GetFormattedCurrency((planPrice - planPrice * discount), objAccount.Parent.GetValueOrDefault(), context);

                    model.DeliverBillingPlans.Add(new BillingPlanItem()
                    {
                        Id = billingPlanInfo.ID,
                        Title = string.Format("{0} ( {1} )", billingPlanInfo.Name, GMGColorFormatData.GetFormattedCurrency(planPrice, objAccount.Parent.GetValueOrDefault(), context)),
                        IsFixedPrice = billingPlanInfo.IsFixed,
                        Price = string.Format("{0}", planPrice),
                        Cpp = billingPlanInfo.CostPerProof,
                        IsDemo = billingPlanInfo.IsDemo
                    });
                }
            }
            model.DeliverBillingPlans.Sort((a, b) => (System.String.Compare(a.Title, b.Title, System.StringComparison.OrdinalIgnoreCase)));

            GMGColorDAL.SubscriberPlanInfo deliverSelectedPlan =
                lstAccountSubscriptionPlans.FirstOrDefault(
                    o => o.BillingPlan.BillingPlanType.AppModule1.Key == (int)GMG.CoZone.Common.AppModule.Deliver);

            if (deliverSelectedPlan != null)
            {
                if (updateAll)
                {
                    model.DeliverBillingPlanId = deliverSelectedPlan.SelectedBillingPlan;
                    model.DeliverChargeCostPerProofIfExceeded = deliverSelectedPlan.ChargeCostPerProofIfExceeded;
                    model.DeliverContractStartDate = deliverSelectedPlan.ContractStartDate != null ? deliverSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                    model.DeliverNrOfCredits = deliverSelectedPlan.NrOfCredits;
                    model.DeliverCreditAllocationType = deliverSelectedPlan.IsQuotaAllocationEnabled
                        ? CreditAllocationTypeEnum.Quota
                        : CreditAllocationTypeEnum.Floating;
                    model.DeliverAllowOverdraw = deliverSelectedPlan.AllowOverdraw;
                    model.DeliverDemoRemainingDays = deliverSelectedPlan.AccountPlanActivationDate.HasValue ?
                  (deliverSelectedPlan.AccountPlanActivationDate.Value.AddDays(GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) - DateTime.UtcNow).Days : (int?)null;
                }
                else if (updateContractStartDate)
                {
                    model.DeliverContractStartDate = deliverSelectedPlan.ContractStartDate != null ? deliverSelectedPlan.ContractStartDate.GetValueOrDefault().ToString(datePattern) : string.Empty;
                }
            }

            #endregion

            #endregion

            #region Extra Costs

            if (model.CollaborateBillingPlanId.HasValue)
            {
                decimal costPerProof = GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(objAccount.Parent.GetValueOrDefault(), model.CollaborateBillingPlanId.Value, context);
                model.CollaborateCostPerProof = GMGColorFormatData.GetFormattedCurrency(costPerProof, objAccount.Parent.GetValueOrDefault(), context);
            }          
            if (model.DeliverBillingPlanId.HasValue)
            {
                decimal costPerProof = GMGColorDAL.AttachedAccountBillingPlan.GetCostPerProof(objAccount.Parent.GetValueOrDefault(), model.DeliverBillingPlanId.Value, context);
                model.DeliverCostPerProof = GMGColorFormatData.GetFormattedCurrency(costPerProof, objAccount.Parent.GetValueOrDefault(), context);
            }           

            #endregion

            model.objLoggedAccount = loggedAccount;

            return model;
        }

        public static void UnassignPlanType(int planId, int accId, DbContextBL context)
        {
            UnassignPlanTypeFromChildren(planId, accId, context);
            context.AttachedAccountBillingPlans.Remove(context.AttachedAccountBillingPlans.Where(p => p.BillingPlan == planId && p.Account == accId).FirstOrDefault());            
        }

        private static void UnassignPlanTypeFromChildren(int planId, int accId, DbContextBL context)
        {
            var childrenAccounts = context.Accounts.Where(child => child.Parent == accId).Select(c => c.ID).ToList();
            if (childrenAccounts.Count > 0)
            {
                foreach (var account in childrenAccounts)
                {
                    UnassignPlanTypeFromChildren(planId, account, context);
                }
                context.AttachedAccountBillingPlans.RemoveRange(context.AttachedAccountBillingPlans.Where(at => at.BillingPlan == planId && childrenAccounts.Contains(at.Account)));
            }    
        }
    }
}
