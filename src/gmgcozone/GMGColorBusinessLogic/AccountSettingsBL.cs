﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class AccountSettingsBL : BaseBL
    {
        public enum AccountSettingsKeyEnum
        {
            PDF2SWFArguments,
            SoftproofingCalibrationTimeoutDays,
            ContactSupportEmail,
            ProofStudioBackgroundColor,
            ProofStudioPenWidth,
            ProofStudioShowAnnotationsForExternalUsers,
            CustomSmtpServer, 
            SingleSignOn
        }
        public enum AccountSettingValueType
        {
            STRG,
            IN32,
            BOLN
        }

        public class AccountSettingsType
        {
            public string Value { get; set; }
            public string ValueType { get; set; }
        }

        /// <summary>
        /// Saves the setting.
        /// </summary>
        /// <param name="keyEnum">The key enum.</param>
        /// <param name="value">The value.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">EF context.</param>
        internal static void SaveSetting(AccountSettingsKeyEnum keyEnum, string value, AccountSettingValueType valueType, int? accountId, GMGColorContext context)
        {
            try
            {
                string settingsName = Enum.GetName(typeof(AccountSettingsBL.AccountSettingsKeyEnum), keyEnum);

                AccountSetting accountSetting = accountId.HasValue
                                                    ? (from accs in context.AccountSettings
                                                       where
                                                           accs.Name.ToLower() == settingsName.ToLower() &&
                                                           accs.Account != null &&
                                                           accs.Account.Value == accountId.Value
                                                       select accs).FirstOrDefault()
                                                    : (from accs in context.AccountSettings
                                                       where
                                                           accs.Name.ToLower() == settingsName.ToLower() &&
                                                           accs.Account == null
                                                       select accs).FirstOrDefault();
                if (accountSetting != null)
                {
                    // update current item
                    accountSetting.Value = value;
                }
                else
                {
                    // add new item
                    string valueTypeString = Enum.GetName(typeof(AccountSettingValueType), valueType);
                    int valueTypeId = (from vdt in context.ValueDataTypes
                                       where vdt.Key.ToLower() == valueTypeString.ToLower()
                                       select vdt.ID).FirstOrDefault();
                    accountSetting = new AccountSetting()
                                         {
                                             Account = accountId,
                                             ValueDataType = valueTypeId,
                                             Name = Enum.GetName(typeof(AccountSettingsKeyEnum), keyEnum),
                                             Description = Enum.GetName(typeof(AccountSettingsKeyEnum), keyEnum),
                                             Value = value
                                         };

                    context.AccountSettings.Add(accountSetting);
                }
                context.SaveChanges();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new Exception(ExceptionBLMessages.CouldNotAddAccountSetting, ex);
            }
        }

        /// <summary>
        /// Gets the setting.
        /// </summary>
        /// <param name="keyEnum">The key enum.</param>
        /// <param name="context">EF context.</param>
        /// <param name="accountId">The account id.</param>
        /// <returns></returns>
        internal static AccountSettingsType GetSetting(AccountSettingsKeyEnum keyEnum, GMGColorContext context, int? accountId = null)
        {
            try
            {
                string keyEnumValue = Enum.GetName(typeof(AccountSettingsKeyEnum), keyEnum);
                return (accountId.HasValue
                            ? (from acs in context.AccountSettings
                               join vdt in context.ValueDataTypes on acs.ValueDataType equals vdt.ID
                               where
                                   acs.Name.ToLower() == keyEnumValue.ToLower() &&
                                   acs.Account.HasValue && acs.Account.Value == accountId.Value
                               select new AccountSettingsType()
                                          {
                                              Value = acs.Value,
                                              ValueType = vdt.Name,
                                          }).FirstOrDefault()
                            : (from acs in context.AccountSettings
                               join vdt in context.ValueDataTypes on acs.ValueDataType equals vdt.ID
                               where
                                   acs.Name.ToLower() == keyEnumValue.ToLower() &&
                                   acs.Account == null
                               select new AccountSettingsType()
                                          {
                                              Value = acs.Value,
                                              ValueType = vdt.Name,
                                          }).FirstOrDefault());
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAccountSetting, ex);
            }
        }

        /// <summary>
        /// Gets the softproofing calibration timeout days.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="accountId">EF account id.</param>
        /// <returns></returns>
        internal static AccountSettingsType GetSoftproofingCalibrationTimeoutDays(GMGColorContext context, int? accountId = null)
        {
            try
            {
                // return default calibration timeout days if settings does not exists for current account
                return GetSetting(AccountSettingsKeyEnum.SoftproofingCalibrationTimeoutDays, context, accountId) ??
                       GetSetting(AccountSettingsKeyEnum.SoftproofingCalibrationTimeoutDays, context, null);
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCalibrationTimeoutDaysSetting, ex);
            }
        }

        public static AccountSettingsType GetSoftproofingCalibrationTimeoutDays(int? accountId = null)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    // return default calibration timeout days if settings does not exists for current account
                    return GetSetting(AccountSettingsKeyEnum.SoftproofingCalibrationTimeoutDays, context, accountId) ??
                           GetSetting(AccountSettingsKeyEnum.SoftproofingCalibrationTimeoutDays, context, null);
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCalibrationTimeoutDaysSetting, ex);
            }
        }

        public static string GetHelpDeskEmailAddress()
        {
            try
            {
                //get the default helpDeskEmailAddress
                string helpDeskEmailAddress = "noreply@gmgmcozone.com";
                using (GMGColorContext context = new GMGColorContext())
                {
                    string keyEnumValue = Enum.GetName(typeof(AccountSettingsKeyEnum), AccountSettingsKeyEnum.ContactSupportEmail);
                    var value = (from acs in context.AccountSettings
                                                         where acs.Name.ToLower() == keyEnumValue.ToLower() && acs.Account == null
                                                         select acs.Value).FirstOrDefault();
                    if (!string.IsNullOrEmpty(value))
                    {
                        helpDeskEmailAddress = value;
                    }
                }
                return helpDeskEmailAddress;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetHelpDeskEmailAddress, ex);
            }
        }

        public static string GetContactSupportEmail(int? accID)
        {
            try
            {
                string contactEmail = String.Empty;
                using (GMGColorContext context = new GMGColorContext())
                {
                    string keyEnumValue = Enum.GetName(typeof(AccountSettingsKeyEnum), AccountSettingsKeyEnum.ContactSupportEmail);
                    contactEmail = (from acs in context.AccountSettings
                                 where acs.Name.ToLower() == keyEnumValue.ToLower() && acs.Account == accID
                                 select acs.Value).FirstOrDefault();

                    if (string.IsNullOrEmpty(contactEmail))
                    {
                        contactEmail = GetHelpDeskEmailAddress();
                    }
                }
                return contactEmail;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetHelpDeskEmailAddress, ex);
            }
        }

    }
}
