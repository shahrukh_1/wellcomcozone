﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
	public class ApprovalNotificationsHelper
	{
		public Approval Approval { get; set; }

		public List<int> InternalCollaborators { get; set; }

		public List<int> ExternalCollaborators { get; set; }

		public ApprovalNotificationsHelper()
		{
			InternalCollaborators = new List<int>();
			ExternalCollaborators = new List<int>();
		}
	}
}