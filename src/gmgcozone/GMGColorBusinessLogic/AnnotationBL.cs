﻿using System;
using System.Collections.Generic;
using System.Linq;
using Approval = GMGColorDAL.Approval;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using GMGColorDAL;
using GMG.CoZone.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.Common;
using System.IO;
using GMGColorDAL.CustomModels.Api;
using GMG.CoZone.Collaborate.DomainModels.Annotation;
using System.Data.SqlClient;

namespace GMGColorBusinessLogic
{
    public class AnnotationBL 
    {
        #region Constants

        public static string AnnotationAttachmentFolder = "annotation-attachments";
        #endregion

        #region Properties

        private static string SVGFolderRelativePath = "Content\\svg";
        private static string AnnotationMarkerSVG = "AnnotationMarker.svg";
        private static string AnnotationPinSVG = "AnnotationPin.svg";
        #endregion

        public static string GetAccountRegion(int annotationID, GMGColorContext context)
        {
            return ApprovalAnnotation.GetAccountRegion(annotationID, context);
        }

        public static int GetParentAnnotationId(int annotatonID, DbContextBL context)
        {
            int? parentId = (from an in context.ApprovalAnnotations where an.ID == annotatonID select an.Parent).FirstOrDefault();
            return parentId == null ? annotatonID : (int)parentId;
        }

        public static int GetParentAnnotationCreator(int annotationID, GMGColorContext context)
        {
            int uid = 0;
            uid = (from ann in context.ApprovalAnnotations
                        join annp in context.ApprovalAnnotations on ann.Parent equals annp.ID
                        join u in context.Users on annp.Creator equals u.ID
                        where ann.ID == annotationID
                        select u.ID).FirstOrDefault();

            //For External User Parent Annotation Creator
            if (uid == 0)
            {
                uid = (from ann in context.ApprovalAnnotations
                        join annp in context.ApprovalAnnotations on ann.Parent equals annp.ID
                        join ec in context.ExternalCollaborators on annp.ExternalCreator equals ec.ID
                        where ann.ID == annotationID
                        select ec.ID).FirstOrDefault();
            }

            return uid;
         }

        public static List<AnnoatationReplayToMyCommentUser> GetPreviousCommentUsers(int annotationParentId, int creator, bool IsExternalCreator, GMGColorContext context)
        {
            
                var AnnotationRelatedUser = context.Database.SqlQuery<AnnoatationReplayToMyCommentUser>(@"    SELECT 
                Distinct  
                CASE WHEN ([Extent1].[Creator] IS NULL) THEN cast(1 as bit) ELSE cast(0 as bit) END AS [IsExternal], 
                CASE WHEN 
	            (
                [Extent1].[ExternalCreator] IS NULL
                ) THEN [Extent1].[Creator] ELSE [Extent1].[ExternalCreator] END AS UserId 
            FROM 
                [dbo].[ApprovalAnnotation] AS [Extent1] 
            WHERE 
                [Extent1].[Parent] = @annotationParentId
                OR [Extent1].[ID] = @annotationParentId", new SqlParameter("annotationParentId", annotationParentId)).ToList();


            AnnotationRelatedUser.RemoveAll(t => t.UserId == creator && t.IsExternal == IsExternalCreator);
            return AnnotationRelatedUser;
        }

        /// <summary>
        /// Creates the Individual Annotation Report View Model
        /// </summary>
        /// <param name="approvalId">Approval ID</param>
        /// <param name="reportGrouping">Report type</param>
        /// <param name="loggedAccount">LoggedAccount</param>
        /// <param name="context">DB Context</param>
        /// <param name="annotationsPerPage">number of annotations per page</param>
        /// <returns>The View Model</returns>
        public static List<ApprovalPageAnnotationsModel> GetCommentsAnnotationReportModel(int approvalId, AnnotationReportGrouping reportGrouping,
            List<AnnotationsReportUser> selectedUsers , List<int> selectedPhases, Account loggedAccount, int annotationsPerPage, GMGColorDAL.User LoggedUser, bool IsApprovalTypeVideo, DbContextBL context)
        {
            try
            {
                List<ApprovalPageAnnotationsModel> annotationReportModel = new List<ApprovalPageAnnotationsModel>();

                var arePhaseSelected = selectedPhases.Count > 0;
                var approvalPages = (from ap in context.ApprovalPages
                                     join a in context.Approvals on ap.Approval equals a.ID
                                     let hasAnnotations = (from ann in context.ApprovalAnnotations
                                                           where ann.Page == ap.ID && (ann.Phase == null || !arePhaseSelected && a.CurrentPhase == ann.Phase || selectedPhases.Contains(ann.Phase.Value))
                                                           select ann.ID).Any()
                                         let videoOriginalSize = (from ann in context.ApprovalAnnotations
                                                                  where ann.Page == ap.ID && (ann.Phase == null || !arePhaseSelected && a.CurrentPhase == ann.Phase || selectedPhases.Contains(ann.Phase.Value))
                                                                  select new { ann.OriginalVideoHeight, ann.OriginalVideoWidth }).FirstOrDefault()
                                     where a.ID == approvalId && hasAnnotations
                                     select new
                                     {
                                         ap.ID,
                                         a.Guid,
                                         ap.Number,
                                         VideoHeight = videoOriginalSize.OriginalVideoHeight,
                                         VideoWidth = videoOriginalSize.OriginalVideoWidth,
                                         ap.OutputRenderHeight,
                                         ap.OutputRenderWidth
                                     });

                var internalUsersIds = selectedUsers.Where(t => !t.IsExternal).Select(t => t.ID);
                var externalUsersIds = selectedUsers.Where(t => t.IsExternal).Select(t => t.ID);

                int[] approvalPagesIds = approvalPages.Select(a => a.ID).ToArray();
                var listannotationsDetails = GetAnnotationsInfo(approvalPagesIds, LoggedUser, reportGrouping, internalUsersIds, externalUsersIds, selectedPhases, true, context);

                foreach (var approvalPage in approvalPages)
                {
                    //Get annotations together with replies  and related info from database
                    var annotations = (from itm in listannotationsDetails where itm.ApprovalPageId == approvalPage.ID select itm).ToArray();

                    List<int> commentSize = new List<int>();
                    foreach (var annotation in annotations)
                    {
                        int totalCommentLength = 0;
                        if (annotation.AnnotationRepliesLength.Count > 0)
                        {
                            totalCommentLength = GetSumOfElementsInList(annotation.AnnotationRepliesLength);
                        }
                        if (annotation.AnnotationAttachmentsCount > 0)
                        {
                            totalCommentLength = totalCommentLength + annotation.AnnotationAttachmentsCount;
                        }
                        if (annotation.AnnotationChecklistLength.Count > 0)
                        {
                            totalCommentLength = totalCommentLength + GetSumOfElementsInList(annotation.AnnotationChecklistLength);
                        }
                        totalCommentLength = totalCommentLength + annotation.AnnotationCommentLength;
                        commentSize.Add(totalCommentLength);
                    }

                    List<int> annotationsPerSide = new List<int>();
                    List<int> annotationsPerPage1 = new List<int>();
                    string reportModel = "reportModel";
                    double MaxLinesOnEachPage;
                    if (IsApprovalTypeVideo)
                    {
                         MaxLinesOnEachPage = GetNoOfLinesBasedOnHeight((int)approvalPage.VideoHeight, (int)approvalPage.VideoWidth, reportModel);
                    }
                    else
                    {
                         MaxLinesOnEachPage = GetNoOfLinesBasedOnHeight((int)approvalPage.OutputRenderHeight, (int)approvalPage.OutputRenderWidth, reportModel);
                    }
                    int NoOfPagesRequired = 0;
                    int NoOfSidesRequired = 0;
                    bool NoOfCommentsOnPageSatisfied = true;
                    int lines = 0;
                    for (int commentLength = 0; commentLength < commentSize.Count(); commentLength++)
                    {
                        if (lines + commentSize[commentLength] + 3 < MaxLinesOnEachPage)
                        {
                            lines = lines + commentSize[commentLength] + 3;
                        }
                        else
                        {

                            NoOfSidesRequired++;
                            if (annotationsPerSide.Count > 0)
                            {
                                int lastannotationnumber = GetSumOfElementsInList(annotationsPerSide);
                                if (commentLength - lastannotationnumber > 0)
                                {
                                    annotationsPerSide.Add(commentLength - lastannotationnumber);
                                }
                                else
                                {
                                    annotationsPerSide.Add(1);
                                }
                            }
                            else if (commentLength == 0)
                            {
                                annotationsPerSide.Add(commentLength + 1);
                            }
                            else
                            {
                                annotationsPerSide.Add(commentLength);
                            }
                            if (commentLength != 0 && lines > 0)
                            {
                                commentLength--;
                            }
                            lines = 0;
                        }
                    }
                    if (lines > 0)
                    {
                        NoOfSidesRequired++;
                        int lastannotationnumber1 = GetSumOfElementsInList(annotationsPerSide);
                        annotationsPerSide.Add(commentSize.Count - lastannotationnumber1);
                    }

                    if (NoOfSidesRequired % 2 == 0) { 
                            NoOfPagesRequired = NoOfSidesRequired / 2;
                    }
                    else { 
                        NoOfPagesRequired = (NoOfSidesRequired / 2) + 1;
                    }
                    for(var noOfAnnotations = 1; noOfAnnotations <= annotationsPerSide.Count; noOfAnnotations++)
                    {
                        var totalAnnotationsOnPage = annotationsPerSide[noOfAnnotations - 1];
                        if (annotationsPerSide.Count >= (noOfAnnotations + 1))
                        {
                            totalAnnotationsOnPage = totalAnnotationsOnPage + annotationsPerSide[noOfAnnotations];
                            annotationsPerSide.RemoveAt(noOfAnnotations - 1);
                            annotationsPerSide.RemoveAt(noOfAnnotations - 1);
                        }
                        else
                        {
                            annotationsPerSide.RemoveAt(noOfAnnotations - 1);
                        }
                        noOfAnnotations--;
                        annotationsPerPage1.Add(totalAnnotationsOnPage);
                    }
                    for(var perPageAnnotations = 0; perPageAnnotations < annotationsPerPage1.Count; perPageAnnotations++)
                    {
                        if(annotationsPerPage1[perPageAnnotations] < annotationsPerPage && perPageAnnotations < annotationsPerPage1.Count - 1)
                        {
                            NoOfCommentsOnPageSatisfied = false;
                        }else{
                            NoOfCommentsOnPageSatisfied = true;
                        }
                    }

                    //number of annotations for current page
                    int annotationsCount = annotations.Count();

                    //check if current page has annotations based on filters
                    if (annotationsCount == 0)
                    {
                        continue;
                    }

                    //calculate how many times current page must be repeated
                    long requiredPages = Convert.ToInt64(Math.Ceiling((double)annotationsCount / annotationsPerPage));
                    int annotationsIndex = 0;
                    if (!IsApprovalTypeVideo)
                    {
                    if (NoOfPagesRequired <= requiredPages && NoOfCommentsOnPageSatisfied)
                    {
                        //Create page objects for current approval page
                        while (requiredPages > 0)
                        {
                            ApprovalPageAnnotationsModel pageModel = new ApprovalPageAnnotationsModel
                            {
                                ApprovalGuid = approvalPage.Guid,
                                PageNumber = approvalPage.Number
                            };

                            //current count for current page copy
                            int annotationCountForCurrentPageObject = 0;

                            //create comma separated string for current page object
                            StringBuilder annotationsForCurrentPageObject = new StringBuilder();
                            for (; annotationsIndex < annotationsCount && annotationCountForCurrentPageObject < annotationsPerPage; annotationsIndex++, annotationCountForCurrentPageObject++)
                            {
                                annotationsForCurrentPageObject.Append(annotations[annotationsIndex].ID);
                                annotationsForCurrentPageObject.Append(",");
                            }

                            //remove last occuring comma
                            annotationsForCurrentPageObject.Length--;
                            //set page object annotations id's
                            pageModel.AnnotationsIDs = annotationsForCurrentPageObject.ToString();

                            annotationReportModel.Add(pageModel);

                            //deincrement page variable
                            requiredPages--;
                        }
                    }
                    else
                    {
                        for (int q = 0; q < NoOfPagesRequired; q++)
                        {
                            ApprovalPageAnnotationsModel pageModel = new ApprovalPageAnnotationsModel
                            {
                                ApprovalGuid = approvalPage.Guid,
                                PageNumber = approvalPage.Number
                            };

                            //current count for current page copy
                            int annotationCountForCurrentPageObject = 0;

                            //create comma separated string for current page object
                            StringBuilder annotationsForCurrentPageObject = new StringBuilder();
                            for (; annotationsIndex < annotationsCount && annotationCountForCurrentPageObject < annotationsPerPage1[q]; annotationsIndex++, annotationCountForCurrentPageObject++)
                            {
                                annotationsForCurrentPageObject.Append(annotations[annotationsIndex].ID);
                                annotationsForCurrentPageObject.Append(",");
                            }

                            //remove last occuring comma
                            annotationsForCurrentPageObject.Length--;
                            //set page object annotations id's
                            pageModel.AnnotationsIDs = annotationsForCurrentPageObject.ToString();

                            annotationReportModel.Add(pageModel);
                        }
                    }
                }
                    else
                    {
                        List<int> annotationIds = new List<int>();
                        var videoAnnotations = GetAnnotationsInfoVideoReport(approvalPage.ID, LoggedUser, reportGrouping, internalUsersIds, externalUsersIds, selectedPhases, false, context);
                        annotationsCount = videoAnnotations.Count();
                        foreach (var annotation in videoAnnotations)
                        {
                            annotationIds.Add(annotation.ID);
                        }

                        var TimeFrameOfScreenShots = (from an in context.ApprovalAnnotations where annotationIds.Contains(an.ID) select an.TimeFrame).ToList();
                        TimeFrameOfScreenShots = TimeFrameOfScreenShots.Distinct().ToList();
                      
                        for (int q = 0; q < TimeFrameOfScreenShots.Count(); q++)
                        {
                          
                            List<int> SizeOfComments = new List<int>();
                            foreach (var annotation in videoAnnotations)
                            {
                                if (annotation.TimeFrame == TimeFrameOfScreenShots[q])
                                {
                                    int totalCommentLength = 0;
                                    if (annotation.AnnotationRepliesLength.Count > 0)
                                    {
                                        totalCommentLength = GetSumOfElementsInList(annotation.AnnotationRepliesLength);
                                    }
                                    if (annotation.AnnotationAttachmentsCount > 0)
                                    {
                                        totalCommentLength = totalCommentLength + annotation.AnnotationAttachmentsCount;
                                    }
                                    if (annotation.AnnotationChecklistLength.Count > 0)
                                    {
                                        totalCommentLength = totalCommentLength + GetSumOfElementsInList(annotation.AnnotationChecklistLength);
                                    }
                                    totalCommentLength = totalCommentLength + annotation.AnnotationCommentLength;
                                    SizeOfComments.Add(totalCommentLength);
                                }
                            }

                            int NoOfPagesRequiredAtThisInstance = 0;
                            int NoOflines = 0;
                            int NoOfPagesRequired1 = 0;
                            List<int> annotationsPerpage1 = new List<int>();
                            List<int> annotationsPerSide1 = new List<int>();
                            for (int commentLength = 0; commentLength < SizeOfComments.Count(); commentLength++)
                            {
                                if (NoOflines + SizeOfComments[commentLength] + 7 < MaxLinesOnEachPage)
                                {
                                    NoOflines = NoOflines + SizeOfComments[commentLength] + 7;
                                }
                                else
                                {

                                    NoOfPagesRequiredAtThisInstance++;
                                    if (annotationsPerSide1.Count > 0)
                                    {
                                        int lastannotationnumber = GetSumOfElementsInList(annotationsPerSide1);
                                        if (commentLength - lastannotationnumber > 0)
                                        {
                                            annotationsPerSide1.Add(commentLength - lastannotationnumber);
                                        }
                                        else
                                        {
                                            annotationsPerSide1.Add(1);
                                        }
                                    }
                                    else if (commentLength == 0)
                                    {
                                        annotationsPerSide1.Add(commentLength + 1);
                                    }
                                    else
                                    {
                                        annotationsPerSide1.Add(commentLength);
                                    }
                                    if (commentLength != 0 && NoOflines > 0)
                                    {
                                        commentLength--;
                                    }
                                    NoOflines = 0;
                                }
                            }
                            if (NoOflines > 0)
                            {
                                NoOfPagesRequiredAtThisInstance++;
                                int lastannotationnumber1 = GetSumOfElementsInList(annotationsPerSide1);
                                annotationsPerSide1.Add(SizeOfComments.Count - lastannotationnumber1);
                            }

                            if (NoOfPagesRequiredAtThisInstance % 2 == 0)
                            {
                                NoOfPagesRequired1 = NoOfPagesRequiredAtThisInstance / 2;
                            }
                            else
                            {
                                NoOfPagesRequired1 = (NoOfPagesRequiredAtThisInstance / 2) + 1;
                            }

                            for (var noOfAnnotations = 1; noOfAnnotations <= annotationsPerSide1.Count; noOfAnnotations++)
                            {
                                var totalAnnotationsOnPage = annotationsPerSide1[noOfAnnotations - 1];
                                if (annotationsPerSide1.Count >= (noOfAnnotations + 1))
                                {
                                    totalAnnotationsOnPage = totalAnnotationsOnPage + annotationsPerSide1[noOfAnnotations];
                                    annotationsPerSide1.RemoveAt(noOfAnnotations - 1);
                                    annotationsPerSide1.RemoveAt(noOfAnnotations - 1);
                                }
                                else
                                {
                                    annotationsPerSide1.RemoveAt(noOfAnnotations - 1);
                                }
                                noOfAnnotations--;
                                annotationsPerpage1.Add(totalAnnotationsOnPage);
                            }
                            #region
                            int requiredPages1 = (SizeOfComments.Count() / annotationsPerPage) + 1;
                            for (var perPageAnnotations = 0; perPageAnnotations < annotationsPerpage1.Count; perPageAnnotations++)
                            {
                                if (annotationsPerpage1[perPageAnnotations] < annotationsPerPage && perPageAnnotations < annotationsPerpage1.Count - 1)
                                {
                                    NoOfCommentsOnPageSatisfied = false;
                                }
                                else
                                {
                                    NoOfCommentsOnPageSatisfied = true;
                                }
                            }
                            if (NoOfPagesRequired1 <= requiredPages1 && NoOfCommentsOnPageSatisfied)
                            {
                                NoOfPagesRequired1 = requiredPages1;
                                List<int> annotationsOnAPage = new List<int>();
                                for(int a = 0; a < NoOfPagesRequired1; a++)
                                {
                                    annotationsOnAPage.Add(annotationsPerPage);
                                }
                                annotationsPerpage1 = annotationsOnAPage;
                            }
                                #endregion
                            annotationsIndex = 0;
                            for (int r = 0; r < NoOfPagesRequired1; r++)
                            { 
                            ApprovalPageAnnotationsModel pageModel = new ApprovalPageAnnotationsModel
                            {
                                ApprovalGuid = approvalPage.Guid,
                                PageNumber = approvalPage.Number,
                                PageId = approvalPage.ID
                            };

                            //current count for current page copy
                            int annotationCountForCurrentPageObject = 0;
                            
                            //create comma separated string for current page object
                            StringBuilder annotationsForCurrentPageObject = new StringBuilder();
                            for (; annotationsIndex < annotationsCount && annotationCountForCurrentPageObject < annotationsPerpage1[r]; annotationsIndex++)
                            {
                                if (TimeFrameOfScreenShots[q] == videoAnnotations[annotationsIndex].TimeFrame)
                                {
                                    annotationsForCurrentPageObject.Append(videoAnnotations[annotationsIndex].ID);
                                    annotationsForCurrentPageObject.Append(",");
                                    annotationCountForCurrentPageObject++;
                                }
                            }

                            //remove last occuring comma
                            annotationsForCurrentPageObject.Length--;
                            //set page object annotations id's
                            pageModel.AnnotationsIDs = annotationsForCurrentPageObject.ToString();
                            pageModel.TimeFrame = TimeFrameOfScreenShots[q];

                            annotationReportModel.Add(pageModel);
                         }
                        }
                    }
                }

                return annotationReportModel;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateIndividualAnnotationReport, ex);
            }
        }



        public static List<ApprovalPageAnnotationsModelPDF> GetCommentsAnnotationPDFReportModel(int approvalId, AnnotationReportGrouping reportGrouping,
                    List<AnnotationsReportUser> selectedUsers, List<int> selectedPhases, Account loggedAccount, GMGColorDAL.User loggedUser, bool IsApprovalTypeVideo, DbContextBL context)
        {
            try
            {
                List<ApprovalPageAnnotationsModelPDF> annotationReportModelPDF = new List<ApprovalPageAnnotationsModelPDF>();

                var arePhaseSelected = selectedPhases.Count > 0;
                var approvalPages = (from ap in context.ApprovalPages
                                     join a in context.Approvals on ap.Approval equals a.ID
                                     let hasAnnotations = (from ann in context.ApprovalAnnotations
                                                           where ann.Page == ap.ID && (ann.Phase == null || !arePhaseSelected && a.CurrentPhase == ann.Phase || selectedPhases.Contains(ann.Phase.Value))
                                                           select ann.ID).Any()
                                     let videoOriginalSize = (from ann in context.ApprovalAnnotations
                                                              where ann.Page == ap.ID && (ann.Phase == null || !arePhaseSelected && a.CurrentPhase == ann.Phase || selectedPhases.Contains(ann.Phase.Value))
                                                              select new { ann.OriginalVideoHeight, ann.OriginalVideoWidth }).FirstOrDefault()
                                     where a.ID == approvalId && hasAnnotations
                                     select new
                                     {
                                         ap.ID,
                                         a.Guid,
                                         ap.Number,
                                         VideoHeight = videoOriginalSize.OriginalVideoHeight,
                                         VideoWidth = videoOriginalSize.OriginalVideoWidth,
                                         ap.OutputRenderHeight,
                                         ap.OutputRenderWidth,
                                         ap.HTMLFilePath
                                     });

                var internalUsersIds = selectedUsers.Where(t => !t.IsExternal).Select(t => t.ID);
                var externalUsersIds = selectedUsers.Where(t => t.IsExternal).Select(t => t.ID);

                int[] approvalPagesIds = approvalPages.Select(a => a.ID).ToArray();
                var listannotationsDetails = GetAnnotationsInfoPDFReport(approvalPagesIds, approvalId, loggedUser, reportGrouping, internalUsersIds, externalUsersIds, selectedPhases, true, context);

                foreach (var approvalPage in approvalPages)
                {
                    var annotations = (from itm in listannotationsDetails where itm.ApprovalPageId == approvalPage.ID select itm).ToArray();

                    List < int > commentSize = new List<int>();
                    foreach (var annotation in annotations)
                    {
                        int totalCommentLength = 0;
                        if (annotation.AnnotationRepliesLength.Count > 0)
                        {
                            totalCommentLength = GetSumOfElementsInList(annotation.AnnotationRepliesLength);
                        }
                        if (annotation.AnnotationAttachmentsCount > 0)
                        {
                            totalCommentLength = totalCommentLength + annotation.AnnotationAttachmentsCount;
                        }
                        if (annotation.AnnotationChecklistLength.Count > 0)
                        {
                            totalCommentLength = totalCommentLength + GetSumOfElementsInList(annotation.AnnotationChecklistLength);
                        }
                        totalCommentLength = totalCommentLength + annotation.AnnotationCommentLength;
                        commentSize.Add(totalCommentLength);
                    }

                    List<int> annotationsPerPage = new List<int>();
                    double MaxLinesOnEachPage;
                    string pdfModel = "pdfModel";
                    int physicalImageWidth;
                    int physicalImageHeight;
                    if (IsApprovalTypeVideo)
                    {
                        physicalImageWidth = (int)approvalPage.VideoWidth;
                        physicalImageHeight = (int)approvalPage.VideoHeight;
                    }
                    else if(approvalPage.HTMLFilePath != null)
                    {
                        physicalImageWidth = (int)approvalPage.OutputRenderWidth;
                        physicalImageHeight = (int)approvalPage.OutputRenderHeight;
                    }
                    else
                    {
                    var image = GetApprovalThumbnailPath(approvalPage.Guid, approvalPage.Number, loggedAccount, context);
                        physicalImageWidth = image.Width;
                        physicalImageHeight = image.Height;
                    }
                    if (!(physicalImageWidth == approvalPage.OutputRenderWidth && physicalImageHeight == approvalPage.OutputRenderHeight))
                    {
                        MaxLinesOnEachPage = GetNoOfLinesBasedOnHeight(physicalImageHeight, physicalImageWidth, pdfModel);
                    }
                    else
                    {
                         MaxLinesOnEachPage = GetNoOfLinesBasedOnHeight((int)approvalPage.OutputRenderHeight, (int)approvalPage.OutputRenderWidth, pdfModel);
                    }
                    int NoOfPagesRequired = 0;
                    int lines = 0; 
                    for(int commentLength = 0; commentLength < commentSize.Count(); commentLength++)
                    {
                        if( (lines + commentSize[commentLength] + 9 < MaxLinesOnEachPage) && (approvalPage.HTMLFilePath == null || lines == 0) )
                        {
                            lines = lines + commentSize[commentLength] + 9;
                        }
                        else
                        {
                           
                            NoOfPagesRequired++;
                            if(annotationsPerPage.Count > 0)
                            {
                                int lastannotationnumber = GetSumOfElementsInList(annotationsPerPage);
                                if (commentLength - lastannotationnumber > 0)
                                {
                                    annotationsPerPage.Add(commentLength - lastannotationnumber);
                                }
                                else
                                {
                                    annotationsPerPage.Add(1);
                                }
                            }
                            else if(commentLength == 0)
                            {
                                annotationsPerPage.Add(commentLength + 1);
                            }
                            else
                            {
                                annotationsPerPage.Add(commentLength);
                            }
                            if (commentLength != 0 && lines > 0)
                            {
                                commentLength--;
                            }
                            lines = 0;
                        }
                    }
                    if (lines > 0)
                    {
                        NoOfPagesRequired++;
                        int lastannotationnumber1 = GetSumOfElementsInList(annotationsPerPage);
                        annotationsPerPage.Add(commentSize.Count - lastannotationnumber1);
                    }
                  
                    //number of annotations for current page
                    int annotationsCount = annotations.Count();

                    //check if current page has annotations based on filters
                    if (annotationsCount == 0)
                    {
                        continue;
                    }

                    //calculate how many times current page must be repeated
                    //long requiredPages = Convert.ToInt64(Math.Ceiling((double)annotationsCount / annotationsPerPage));

                    int annotationsIndex = 0;
                    if (!IsApprovalTypeVideo)
                    { 
                    //Create page objects for current approval page
                       for(int q = 0; q < NoOfPagesRequired; q++)
                        {
                        ApprovalPageAnnotationsModelPDF pageModel = new ApprovalPageAnnotationsModelPDF
                        {
                            ApprovalGuid = approvalPage.Guid,
                            PageNumber = approvalPage.Number
                        };

                        //current count for current page copy
                        int annotationCountForCurrentPageObject = 0;

                        //create comma separated string for current page object
                        StringBuilder annotationsForCurrentPageObject = new StringBuilder();
                        List<ApprovalAnnotationInfo> annotationDetails = new List<ApprovalAnnotationInfo>();
                        for (; annotationsIndex < annotationsCount && annotationCountForCurrentPageObject < annotationsPerPage[q]; annotationsIndex++, annotationCountForCurrentPageObject++)
                        {
                            annotationsForCurrentPageObject.Append(annotations[annotationsIndex].ID);
                            annotationsForCurrentPageObject.Append(",");
                            ApprovalAnnotationInfo annotationInfo = new ApprovalAnnotationInfo();
                            annotationInfo = annotations[annotationsIndex].AnnotationInfo;
                            annotationInfo.AnnotationReplies = annotations[annotationsIndex].AnnotationReplies;
                            annotationDetails.Add(annotationInfo);
                        }

                        //remove last occuring comma
                        annotationsForCurrentPageObject.Length--;
                        //set page object annotations id's
                        pageModel.AnnotationsIDs = annotationsForCurrentPageObject.ToString();
                        pageModel.AnnotationInfo = annotationDetails;
                        pageModel.ApprovalPageID = approvalPage.ID;
                        annotationReportModelPDF.Add(pageModel);
                    }
                }
                    else
                    {
                        List<int> annotationIds = new List<int>();
                        var videoAnnotations = GetAnnotationsInfoPDFVideoReport(approvalPage.ID, loggedUser, reportGrouping, internalUsersIds, externalUsersIds, selectedPhases, false, context);
                        annotationsCount = videoAnnotations.Count();
                        foreach (var annotation in videoAnnotations)
                        {
                            annotationIds.Add(annotation.ID);
                        }

                        var TimeFrameOfScreenShots = (from an in context.ApprovalAnnotations where annotationIds.Contains(an.ID) select an.TimeFrame).ToList();
                        TimeFrameOfScreenShots = TimeFrameOfScreenShots.Distinct().ToList();

                        for (int q = 0; q < TimeFrameOfScreenShots.Count(); q++)
                        {

                            #region
                            List<int> SizeOfComments = new List<int>();
                            foreach (var annotation in videoAnnotations)
                            {
                                if (annotation.TimeFrame == TimeFrameOfScreenShots[q])
                                {
                                    int totalCommentLength = 0;
                                    if (annotation.AnnotationRepliesLength.Count > 0)
                                    {
                                        totalCommentLength = GetSumOfElementsInList(annotation.AnnotationRepliesLength);
                                    }
                                    if (annotation.AnnotationAttachmentsCount > 0)
                                    {
                                        totalCommentLength = totalCommentLength + annotation.AnnotationAttachmentsCount;
                                    }
                                    if (annotation.AnnotationChecklistLength.Count > 0)
                                    {
                                        totalCommentLength = totalCommentLength + GetSumOfElementsInList(annotation.AnnotationChecklistLength);
                                    }
                                    totalCommentLength = totalCommentLength + annotation.AnnotationCommentLength;
                                    SizeOfComments.Add(totalCommentLength);
                                }
                            }

                            int NoOfPagesRequiredAtThisInstance = 0;
                            int NoOflines = 0;
                            List<int> annotationsPerpage1 = new List<int>();
                            for (int commentLength = 0; commentLength < SizeOfComments.Count(); commentLength++)
                            {
                                if (NoOflines + SizeOfComments[commentLength] + 7 < MaxLinesOnEachPage)
                                {
                                    NoOflines = NoOflines + SizeOfComments[commentLength] + 7;
                                }
                                else
                                {

                                    NoOfPagesRequiredAtThisInstance++;
                                    if (annotationsPerpage1.Count > 0)
                                    {
                                        int lastannotationnumber = GetSumOfElementsInList(annotationsPerpage1);
                                        if (commentLength - lastannotationnumber > 0)
                                        {
                                            annotationsPerpage1.Add(commentLength - lastannotationnumber);
                                        }
                                        else
                                        {
                                            annotationsPerpage1.Add(1);
                                        }
                                    }
                                    else if (commentLength == 0)
                                    {
                                        annotationsPerpage1.Add(commentLength + 1);
                                    }
                                    else
                                    {
                                        annotationsPerpage1.Add(commentLength);
                                    }
                                    if (commentLength != 0 && NoOflines > 0)
                                    {
                                        commentLength--;
                                    }
                                    NoOflines = 0;
                                }
                            }
                            if (NoOflines > 0)
                            {
                                NoOfPagesRequiredAtThisInstance++;
                                int lastannotationnumber1 = GetSumOfElementsInList(annotationsPerpage1);
                                annotationsPerpage1.Add(SizeOfComments.Count - lastannotationnumber1);
                            }
                            #endregion
                            annotationsIndex = 0;
                            for (int r = 0; r < NoOfPagesRequiredAtThisInstance; r++) { 
                            ApprovalPageAnnotationsModelPDF pageModel = new ApprovalPageAnnotationsModelPDF
                            {
                                ApprovalGuid = approvalPage.Guid,
                                PageNumber = approvalPage.Number,
                            };

                            //current count for current page copy
                            int annotationCountForCurrentPageObject = 0;

                            //create comma separated string for current page object
                            StringBuilder annotationsForCurrentPageObject = new StringBuilder();
                            List<ApprovalAnnotationInfo> annotationDetails = new List<ApprovalAnnotationInfo>();
                            for (; annotationsIndex < annotationsCount && annotationCountForCurrentPageObject < annotationsPerpage1[r]; annotationsIndex++)
                            {
                                if (TimeFrameOfScreenShots[q] == videoAnnotations[annotationsIndex].TimeFrame)
                                {
                                    annotationsForCurrentPageObject.Append(videoAnnotations[annotationsIndex].ID);
                                    annotationsForCurrentPageObject.Append(",");
                                    ApprovalAnnotationInfo annotationInfo = new ApprovalAnnotationInfo();
                                    annotationInfo = videoAnnotations[annotationsIndex].AnnotationInfo;
                                    annotationInfo.AnnotationReplies = videoAnnotations[annotationsIndex].AnnotationReplies;
                                    annotationDetails.Add(annotationInfo);
                                    annotationCountForCurrentPageObject++;
                                }
                            }

                            //remove last occuring comma
                            annotationsForCurrentPageObject.Length--;
                            //set page object annotations id's
                            pageModel.AnnotationsIDs = annotationsForCurrentPageObject.ToString();
                            pageModel.TimeFrame = TimeFrameOfScreenShots[q];
                            pageModel.AnnotationInfo = annotationDetails;
                            pageModel.ApprovalPageID = approvalPage.ID;
                            annotationReportModelPDF.Add(pageModel);
                         }
                        }
                    }
                }
                
                return annotationReportModelPDF;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateIndividualAnnotationReport, ex);
            }
        }


        public static bool GetApprovalType(List<int> selectedApprovals, DbContextBL context)
        {
            return (from a in context.Approvals
                     join ft in context.FileTypes on a.FileType equals ft.ID
                     where selectedApprovals.Contains(a.ID) && ft.Type == "video"
                     select a.ID).Any();
            
        }

        public static System.Drawing.Image GetApprovalThumbnailPath(string appGuid, int pageNumber, Account LoggedAccount, DbContextBL Context)
        {
            try
            {
                var pageDimmensions = AnnotationBL.GetPageDimensions(appGuid, pageNumber, Context);

                string pageRelativePath;
                if (pageDimmensions.HasValue && pageDimmensions.Value > Approval.PreviewMaxSize)
                {
                    pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + appGuid +
                                       "/" + pageNumber + "/Thumb-" + Approval.ProcessedThumbnailDimension + "px.jpg";
                }
                else
                {
                    pageRelativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + @"\" + appGuid +
                                    @"\" + pageNumber + @"\" + Approval.FullSizePreview;
                }

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    System.Net.WebRequest req = System.Net.WebRequest.Create(GMGColorConfiguration.AppConfiguration.PathToDataFolder(LoggedAccount.Region) +
                            pageRelativePath);
                    System.Net.WebResponse response = req.GetResponse();
                    Stream stream = response.GetResponseStream();

                    System.Drawing.Image File = System.Drawing.Image.FromStream(stream);
                    return File;
                }
                else
                {

                    System.Drawing.Image File = System.Drawing.Image.FromFile(GMGColorConfiguration.AppConfiguration.PathToDataFolder(LoggedAccount.Region) + pageRelativePath);
                    return File;
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, LoggedAccount.ID, "ApprovalsController.GetApprovalThumbnailPath : Failed to get approval thumbnail for annotation report. {0}", ex.Message);
                throw;
            }
        }


        public static int GetSumOfElementsInList(List<int> listofElements)
        {
            int total = 0;
            foreach(var element in listofElements)
            {
                total = total + element;
            }

            return total;
        }

        public static double GetNoOfLinesBasedOnHeight(int height, int width, string typeOfModel)
        {
            var DISPLAY_CANVAS_WIDTH = typeOfModel == "reportModel" ? 1092 : 1100;
            var BALLOON_MARGIN_LEFT_RIGTH = 15;
            var MAX_BALLOON_WIDTH = typeOfModel == "reportModel" ? 170 : 200;
            var PREVIEW_TOP_BOTTOM_MARGIN = 100;
            var MAXIMUM_LINES_ON_POTRAIT = 65;
            var MINIMUM_CANVAS_HEIGHT = 625;

            double Width = width + 2 * (MAX_BALLOON_WIDTH + (2 * BALLOON_MARGIN_LEFT_RIGTH));
            double Height = height + 2 * PREVIEW_TOP_BOTTOM_MARGIN;
            if (Height > Width && typeOfModel != "reportModel")
            {
                return MAXIMUM_LINES_ON_POTRAIT;
            }
            else
            {
                double heightWidthRatio = (Height / Width);
                var CanvasHeight = Math.Round(DISPLAY_CANVAS_WIDTH * heightWidthRatio);
                CanvasHeight = CanvasHeight >= MINIMUM_CANVAS_HEIGHT ? CanvasHeight : MINIMUM_CANVAS_HEIGHT;
                return Math.Round(CanvasHeight / 16);
            }
        }

        /// <summary>
        /// Gets Annotation Related Info from Database needed to create the view model
        /// </summary>
        /// <param name="approvalPageId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static AnnotationDatabaseModelPDFreport[] GetAnnotationsInfo(int[] approvalPageId, GMGColorDAL.User userDetails, AnnotationReportGrouping reportGrouping, IEnumerable<int> internalUsersIds, IEnumerable<int> externalUsersIds, List<int> phasesId, bool approvalTypeImage, DbContextBL context)
        {
            bool isPhasingOn = phasesId.Count > 0;
            //Get annotations together with replies  and related info from database
            //Commented replies part (not required for first version)
           
            var annotations = (from ann in context.ApprovalAnnotations
                               join ap in context.ApprovalPages on ann.Page equals ap.ID
                               join a in context.Approvals on ap.Approval equals a.ID
                               let publicPrivateAnnotations = (from ca in context.Approvals
                                                               join cap in context.ApprovalPages on ca.ID equals cap.Approval
                                                               where approvalPageId.Contains(cap.ID) && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()
                               where approvalPageId.Contains(ann.Page) && ann.Parent == null && (approvalTypeImage || ann.ShowVideoReport) &&
                               (!publicPrivateAnnotations || userDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == userDetails.ID) &&
                               (!isPhasingOn && ann.Phase == a.CurrentPhase || isPhasingOn && ann.Phase.HasValue && phasesId.Contains(ann.Phase.Value)) &&
                               (ann.ExternalCreator != null && externalUsersIds.Contains(ann.ExternalCreator.Value)
                               || ann.Creator != null && internalUsersIds.Contains(ann.Creator.Value))
                               let AnnotationAttachmentLength = (from ana in context.ApprovalAnnotationAttachments
                                                                 where ana.ApprovalAnnotation == ann.ID
                                                                 select ana.ApprovalAnnotation).Count()
                               let AnnotationRepliesLength = (from ann1 in context.ApprovalAnnotations
                                                              where ann1.Parent == ann.ID
                                                              select (ann1.Comment.Length / 30) + 5).ToList()  //maximum number of chars in one line in annotation box in canvas
                               let AnnotationChecklistLength = (from aac in context.ApprovalAnnotationCheckListItems
                                                                where aac.ApprovalAnnotation == ann.ID
                                                                select (aac.TextValue.Length / 30) + 3).ToList()  //maximum number of chars in one line in annotation box in canvas

                               select new AnnotationDatabaseModelPDFreport
                               {
                                   ID = ann.ID,
                                   ApprovalPageId = ap.ID,
                                   AnnotatedDate = ann.AnnotatedDate,
                                   AnnotationCommentLength = (ann.Comment.Length / 30),  //maximum number of chars in one line in annotation box in canvas
                                   AnnotationAttachmentsCount = AnnotationAttachmentLength > 0 ? AnnotationAttachmentLength + 2 : 0,
                                   AnnotationRepliesLength = AnnotationRepliesLength,
                                   AnnotationChecklistLength = AnnotationChecklistLength,
                                   IsExternalCreator = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0,
                                   TimeFrame = ann.TimeFrame > 0 ? (int)ann.TimeFrame : 0,
                                   GroupId = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0 ? 0 : (from u in context.Users
                                                                                                            join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                                                                            where u.ID == ann.Creator
                                                                                                            select ugu.UserGroup).FirstOrDefault(),
                                   UserDisplayName = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                                where u.ID == ann.Creator
                                                                                                select u.GivenName + " " + u.FamilyName
                                                                                         ).FirstOrDefault()
                                                                                           :
                                                                                           (from exc in context.ExternalCollaborators
                                                                                            where exc.ID == ann.ExternalCreator
                                                                                            select exc.GivenName + " " + exc.FamilyName
                                                                                            ).FirstOrDefault()
                               });



            //Order annotations based on current setting
            switch (reportGrouping)
            {
                case AnnotationReportGrouping.User:
                    annotations = annotations.OrderBy(u => u.IsExternalCreator).ThenBy(u => u.UserDisplayName);
                    break;
                case AnnotationReportGrouping.Date:
                    annotations = annotations.OrderBy(u => u.AnnotatedDate);
                    break;
                case AnnotationReportGrouping.Group:
                    annotations = annotations.OrderBy(u => u.GroupId);
                    break;
            }

            return annotations.ToArray();
        }

        public static AnnotationDatabaseModelPDFreport[] GetAnnotationsInfoVideoReport(int approvalPageId, GMGColorDAL.User userDetails, AnnotationReportGrouping reportGrouping, IEnumerable<int> internalUsersIds, IEnumerable<int> externalUsersIds, List<int> phasesId, bool approvalTypeImage, DbContextBL context)
        {
            bool isPhasingOn = phasesId.Count > 0;
            //Get annotations together with replies  and related info from database
            //Commented replies part (not required for first version)

            var annotations = (from ann in context.ApprovalAnnotations
                               join ap in context.ApprovalPages on ann.Page equals ap.ID
                               join a in context.Approvals on ap.Approval equals a.ID
                               let publicPrivateAnnotations = (from ca in context.Approvals
                                                               join cap in context.ApprovalPages on ca.ID equals cap.Approval
                                                               where cap.ID == approvalPageId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations
                                                               select ca.ID).Any()
                               where ann.Page == approvalPageId && ann.Parent == null && (approvalTypeImage || ann.ShowVideoReport) &&
                               (!publicPrivateAnnotations || userDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == userDetails.ID) &&
                               (!isPhasingOn && ann.Phase == a.CurrentPhase || isPhasingOn && ann.Phase.HasValue && phasesId.Contains(ann.Phase.Value)) &&
                               (ann.ExternalCreator != null && externalUsersIds.Contains(ann.ExternalCreator.Value)
                               || ann.Creator != null && internalUsersIds.Contains(ann.Creator.Value))
                               let AnnotationAttachmentLength = (from ana in context.ApprovalAnnotationAttachments
                                                                 where ana.ApprovalAnnotation == ann.ID
                                                                 select ana.ApprovalAnnotation).Count()
                               let AnnotationRepliesLength = (from ann1 in context.ApprovalAnnotations
                                                              where ann1.Parent == ann.ID
                                                              select (ann1.Comment.Length / 30) + 5).ToList()  //maximum number of chars in one line in annotation box in canvas
                               let AnnotationChecklistLength = (from aac in context.ApprovalAnnotationCheckListItems
                                                                where aac.ApprovalAnnotation == ann.ID
                                                                select (aac.TextValue.Length / 30) + 3).ToList()  //maximum number of chars in one line in annotation box in canvas

                               select new AnnotationDatabaseModelPDFreport
                               {
                                   ID = ann.ID,
                                   ApprovalPageId = ap.ID,
                                   AnnotatedDate = ann.AnnotatedDate,
                                   AnnotationCommentLength = (ann.Comment.Length / 30),  //maximum number of chars in one line in annotation box in canvas
                                   AnnotationAttachmentsCount = AnnotationAttachmentLength > 0 ? AnnotationAttachmentLength + 2 : 0,
                                   AnnotationRepliesLength = AnnotationRepliesLength,
                                   AnnotationChecklistLength = AnnotationChecklistLength,
                                   IsExternalCreator = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0,
                                   TimeFrame = ann.TimeFrame > 0 ? (int)ann.TimeFrame : 0,
                                   GroupId = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0 ? 0 : (from u in context.Users
                                                                                                            join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                                                                            where u.ID == ann.Creator
                                                                                                            select ugu.UserGroup).FirstOrDefault(),
                                   UserDisplayName = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                                where u.ID == ann.Creator
                                                                                                select u.GivenName + " " + u.FamilyName
                                                                                         ).FirstOrDefault()
                                                                                           :
                                                                                           (from exc in context.ExternalCollaborators
                                                                                            where exc.ID == ann.ExternalCreator
                                                                                            select exc.GivenName + " " + exc.FamilyName
                                                                                            ).FirstOrDefault()
                               });



            //Order annotations based on current setting
            switch (reportGrouping)
            {
                case AnnotationReportGrouping.User:
                    annotations = annotations.OrderBy(u => u.IsExternalCreator).ThenBy(u => u.UserDisplayName);
                    break;
                case AnnotationReportGrouping.Date:
                    annotations = annotations.OrderBy(u => u.AnnotatedDate);
                    break;
                case AnnotationReportGrouping.Group:
                    annotations = annotations.OrderBy(u => u.GroupId);
                    break;
            }

            return annotations.ToArray();
        }


        public static ApprovalAnnotationDatabaseModelPDFreport[] GetAnnotationsInfoPDFVideoReport(int approvalPageId, GMGColorDAL.User userDetails, AnnotationReportGrouping reportGrouping, IEnumerable<int> internalUsersIds, IEnumerable<int> externalUsersIds, List<int> phasesId, bool isApprovalImage, DbContextBL context)
        {
            bool isPhasingOn = phasesId.Count > 0;
            int approvalId = (from ap in context.ApprovalPages where ap.ID == approvalPageId select ap.Approval).FirstOrDefault();
            //Get annotations together with replies  and related info from database
            //Commented replies part (not required for first version)
            var annotations = (from ann in context.ApprovalAnnotations
                               join ap in context.ApprovalPages on ann.Page equals ap.ID
                               join a in context.Approvals on ap.Approval equals a.ID
                               let publicPrivateAnnotations = (from ca in context.Approvals
                                                               join cap in context.ApprovalPages on ca.ID equals cap.Approval
                                                               where cap.ID == approvalPageId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations
                                                               select ca.ID).Any()
                               where ann.Page == approvalPageId && ann.Parent == null && (isApprovalImage || ann.ShowVideoReport) &&
                               (!publicPrivateAnnotations || userDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == userDetails.ID) &&
                               (!isPhasingOn && ann.Phase == a.CurrentPhase || isPhasingOn && ann.Phase.HasValue && phasesId.Contains(ann.Phase.Value)) &&
                               (ann.ExternalCreator != null && externalUsersIds.Contains(ann.ExternalCreator.Value)
                               || ann.Creator != null && internalUsersIds.Contains(ann.Creator.Value))
                               let AnnotationAttachmentLength = (from ana in context.ApprovalAnnotationAttachments
                                                                 where ana.ApprovalAnnotation == ann.ID
                                                                 select ana.ApprovalAnnotation).Count()
                               let AnnotationRepliesLength = (from ann1 in context.ApprovalAnnotations
                                                              where ann1.Parent == ann.ID
                                                              select (ann1.Comment.Length / 50) + 5).ToList()  //maximum number of chars in one line in annotation box in canvas
                               let AnnotationChecklistLength = (from aac in context.ApprovalAnnotationCheckListItems
                                                                where aac.ApprovalAnnotation == ann.ID
                                                                select (aac.TextValue.Length / 50) + 3).ToList()  //maximum number of chars in one line in annotation box in canvas
                               let AnnotationInfo = (from ann1 in context.ApprovalAnnotations
                                                     let internalUser = (from u in context.Users where ann1.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                     let externalUser = (from ex in context.ExternalCollaborators where ann1.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                     let phaseName = (from ajp in context.ApprovalJobPhases
                                                                      where ajp.ID == ann1.Phase
                                                                      select ajp.Name).FirstOrDefault()
                                                     let checklistName = (from anci in context.ApprovalAnnotationCheckListItems
                                                                          join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                          join c in context.CheckList on ci.CheckList equals c.ID
                                                                          where anci.ApprovalAnnotation == ann.ID
                                                                          select c.Name).FirstOrDefault()
                                                     let userStatus = (from u in context.Users where ann1.Creator == u.ID select u.Status).FirstOrDefault()


                                                     let attachments = (from ana in context.ApprovalAnnotationAttachments
                                                                        where ann.ID == ana.ApprovalAnnotation
                                                                        select ana.DisplayName).ToList()
                                                     let checklistItems = (from anci in context.ApprovalAnnotationCheckListItems
                                                                           join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                           join c in context.CheckList on ci.CheckList equals c.ID
                                                                           where anci.ApprovalAnnotation == ann.ID
                                                                           select new AnnotationChecklistIteminfo
                                                                           {
                                                                               ChecklistItemName = ci.Name,
                                                                               ChecklistItemValue = anci.TextValue
                                                                           }).ToList()

                                                     where ann1.ID == ann.ID
                                                     select new ApprovalAnnotationInfo
                                                     {
                                                         AnnotationID = ann1.ID,
                                                         AnnotationComment = ann1.Comment,
                                                         AnnotationOwner = internalUser == null ? externalUser : internalUser + (((from ac in context.ApprovalCollaborators
                                                                                                                                   join u in context.Users
                                                                                                                                   on ac.SubstituteUser equals u.ID
                                                                                                                                   where ac.Approval == approvalId && ac.SubstituteUser == u.ID && ac.Collaborator == ann.Creator
                                                                                                                                   select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                             ).FirstOrDefault()) ?? ""),
                                                         AnnotatedDate = ann1.AnnotatedDate,
                                                         AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                         AnnotationChecklist = checklistName != null ? checklistName : null,
                                                         //AnnotationReplies = annotationReplies,
                                                         AnnotationAttachments = attachments,
                                                         AnnotationChecklistItems = checklistItems,
                                                         UserStatus = userStatus > 0 ? userStatus : 0
                                                     }).FirstOrDefault()

                               let annotationReplies = (from ann2 in context.ApprovalAnnotations
                                                        let internalUser = (from u in context.Users where ann2.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                        let externalUser = (from ex in context.ExternalCollaborators where ann2.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                        let userStatus = (from u in context.Users where ann2.Creator == u.ID select u.Status).FirstOrDefault()
                                                        let phaseName = (from ajp in context.ApprovalJobPhases
                                                                         where ajp.ID == ann2.Phase
                                                                         select ajp.Name).FirstOrDefault()
                                                        where ann2.Parent == ann.ID
                                                        select new ApprovalAnnotationRepliesInfo
                                                        {
                                                            AnnotationID = ann2.ID,
                                                            AnnotationComment = ann2.Comment,
                                                            AnnotationOwner = internalUser == null ? externalUser : internalUser,
                                                            AnnotatedDate = ann2.AnnotatedDate,
                                                            AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                            UserStatus = userStatus > 0 ? userStatus : 0
                                                        }).OrderBy(x => x.AnnotationID).ToList()

                               select new ApprovalAnnotationDatabaseModelPDFreport
                               {
                                   ID = ann.ID,
                                   AnnotatedDate = ann.AnnotatedDate,
                                   AnnotationCommentLength = (ann.Comment.Length / 50),  //maximum number of chars in one line in annotation box in canvas
                                   AnnotationAttachmentsCount = AnnotationAttachmentLength > 0 ? AnnotationAttachmentLength + 2 : 0,
                                   AnnotationRepliesLength = AnnotationRepliesLength,
                                   AnnotationChecklistLength = AnnotationChecklistLength,
                                   TimeFrame = ann.TimeFrame > 0 ? (int)ann.TimeFrame : 0,
                                   IsExternalCreator = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0,
                                   GroupId = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0 ? 0 : (from u in context.Users
                                                                                                            join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                                                                            where u.ID == ann.Creator
                                                                                                            select ugu.UserGroup).FirstOrDefault(),
                                   UserDisplayName = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                                where u.ID == ann.Creator
                                                                                                select u.GivenName + " " + u.FamilyName
                                                                                         ).FirstOrDefault()
                                                                                           :
                                                                                           (from exc in context.ExternalCollaborators
                                                                                            where exc.ID == ann.ExternalCreator
                                                                                            select exc.GivenName + " " + exc.FamilyName
                                                                                            ).FirstOrDefault(),
                                   AnnotationInfo = AnnotationInfo,
                                   AnnotationReplies = annotationReplies,
                               });

            //Order annotations based on current setting
            switch (reportGrouping)
            {
                case AnnotationReportGrouping.User:
                    annotations = annotations.OrderBy(u => u.IsExternalCreator).ThenBy(u => u.UserDisplayName);
                    break;
                case AnnotationReportGrouping.Date:
                    annotations = annotations.OrderBy(u => u.AnnotatedDate);
                    break;
                case AnnotationReportGrouping.Group:
                    annotations = annotations.OrderBy(u => u.GroupId);
                    break;
            }

            return annotations.ToArray();
        }


        public static ApprovalAnnotationDatabaseModelPDFreport[] GetAnnotationsInfoPDFReport(int[] approvalPageId,int approvalId, GMGColorDAL.User userDetails, AnnotationReportGrouping reportGrouping, IEnumerable<int> internalUsersIds, IEnumerable<int> externalUsersIds, List<int> phasesId, bool isApprovalImage, DbContextBL context)
        {
            bool isPhasingOn = phasesId.Count > 0;
            //Get annotations together with replies  and related info from database
            //Commented replies part (not required for first version)
            var annotations = (from ann in context.ApprovalAnnotations
                               join ap in context.ApprovalPages on ann.Page equals ap.ID
                               join a in context.Approvals on ap.Approval equals a.ID
                               let publicPrivateAnnotations = (from ca in context.Approvals
                                                               join cap in context.ApprovalPages on ca.ID equals cap.Approval
                                                               where approvalPageId.Contains(cap.ID) && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()
                               where approvalPageId.Contains(ann.Page) && ann.Parent == null && (isApprovalImage || ann.ShowVideoReport) &&
                               (!publicPrivateAnnotations || userDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == userDetails.ID) &&
                               (!isPhasingOn && ann.Phase == a.CurrentPhase || isPhasingOn && ann.Phase.HasValue && phasesId.Contains(ann.Phase.Value)) &&
                               (ann.ExternalCreator != null && externalUsersIds.Contains(ann.ExternalCreator.Value)
                               || ann.Creator != null && internalUsersIds.Contains(ann.Creator.Value))
                               let AnnotationAttachmentLength = (from ana in context.ApprovalAnnotationAttachments
                                                                 where ana.ApprovalAnnotation == ann.ID
                                                                 select ana.ApprovalAnnotation).Count()
                               let AnnotationRepliesLength = (from ann1 in context.ApprovalAnnotations
                                                              where ann1.Parent == ann.ID
                                                              select (ann1.Comment.Length / 50) + 5).ToList()  //maximum number of chars in one line in annotation box in canvas
                               let AnnotationChecklistLength = (from aac in context.ApprovalAnnotationCheckListItems
                                                                where aac.ApprovalAnnotation == ann.ID
                                                                select (aac.TextValue.Length / 50) + 3).ToList()  //maximum number of chars in one line in annotation box in canvas
                               let AnnotationInfo = (from ann1 in context.ApprovalAnnotations
                                                     let internalUser = (from u in context.Users where ann1.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                     let externalUser = (from ex in context.ExternalCollaborators where ann1.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                     let phaseName = (from ajp in context.ApprovalJobPhases
                                                                      where ajp.ID == ann1.Phase
                                                                      select ajp.Name).FirstOrDefault()
                                                     let checklistName = (from anci in context.ApprovalAnnotationCheckListItems
                                                                          join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                          join c in context.CheckList on ci.CheckList equals c.ID
                                                                          where anci.ApprovalAnnotation == ann.ID
                                                                          select c.Name).FirstOrDefault()
                                                     let userStatus = (from u in context.Users where ann1.Creator == u.ID select u.Status).FirstOrDefault()

                                                   
                                                     let attachments = (from ana in context.ApprovalAnnotationAttachments
                                                                        where ann.ID == ana.ApprovalAnnotation
                                                                        select ana.DisplayName).ToList()
                                                     let checklistItems = (from anci in context.ApprovalAnnotationCheckListItems
                                                                           join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                           join c in context.CheckList on ci.CheckList equals c.ID
                                                                           where anci.ApprovalAnnotation == ann.ID
                                                                           select new AnnotationChecklistIteminfo
                                                                           {
                                                                               ChecklistItemName = ci.Name,
                                                                               ChecklistItemValue = anci.TextValue
                                                                           }).ToList()

                                                     where ann1.ID == ann.ID
                                                     select new ApprovalAnnotationInfo
                                                     {
                                                         AnnotationID = ann1.ID,
                                                         AnnotationComment = ann1.Comment,
                                                         AnnotationOwner = internalUser == null ? externalUser : internalUser + (((from ac in context.ApprovalCollaborators
                                                                                                                                join u in context.Users
                                                                                                                                on ac.SubstituteUser equals u.ID
                                                                                                                                where ac.Approval == approvalId && ac.SubstituteUser == u.ID && ac.Collaborator == ann.Creator
                                                                                                                                select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                             ).FirstOrDefault()) ?? ""),
                                                         AnnotatedDate = ann1.AnnotatedDate,
                                                         AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                         AnnotationChecklist = checklistName != null ? checklistName : null,
                                                         //AnnotationReplies = annotationReplies,
                                                         AnnotationAttachments = attachments,
                                                         AnnotationChecklistItems = checklistItems,
                                                         UserStatus = userStatus > 0 ? userStatus : 0
                                                     }).FirstOrDefault()

                               let annotationReplies = (from ann2 in context.ApprovalAnnotations
                                                        let internalUser = (from u in context.Users where ann2.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                        let externalUser = (from ex in context.ExternalCollaborators where ann2.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                        let userStatus = (from u in context.Users where ann2.Creator == u.ID select u.Status).FirstOrDefault()
                                                        let phaseName = (from ajp in context.ApprovalJobPhases
                                                                         where ajp.ID == ann2.Phase
                                                                         select ajp.Name).FirstOrDefault()
                                                        where ann2.Parent == ann.ID
                                                        select new ApprovalAnnotationRepliesInfo
                                                        {
                                                            AnnotationID = ann2.ID,
                                                            AnnotationComment = ann2.Comment,
                                                            AnnotationOwner = internalUser == null ? externalUser : internalUser,
                                                            AnnotatedDate = ann2.AnnotatedDate,
                                                            AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                            UserStatus = userStatus > 0 ? userStatus : 0
                                                        }).OrderBy(x => x.AnnotationID).ToList()
                              
                               select new ApprovalAnnotationDatabaseModelPDFreport
            {
                                   ID = ann.ID,
                                   ApprovalPageId = ap.ID,
                                   AnnotatedDate = ann.AnnotatedDate,
                                   AnnotationCommentLength = (ann.Comment.Length / 50),  //maximum number of chars in one line in annotation box in canvas
                                   AnnotationAttachmentsCount = AnnotationAttachmentLength > 0 ? AnnotationAttachmentLength + 2 : 0,
                                   AnnotationRepliesLength = AnnotationRepliesLength,
                                   AnnotationChecklistLength = AnnotationChecklistLength,
                                   TimeFrame = ann.TimeFrame > 0 ? (int)ann.TimeFrame : 0,
                                   IsExternalCreator = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0,
                                   GroupId = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0 ? 0 : (from u in context.Users
                                                                                                            join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                                                                            where u.ID == ann.Creator
                                                                                                            select ugu.UserGroup).FirstOrDefault(),
                                   UserDisplayName = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                                where u.ID == ann.Creator
                                                                                                select u.GivenName + " " + u.FamilyName
                                                                                         ).FirstOrDefault()
                                                                                           :
                                                                                           (from exc in context.ExternalCollaborators
                                                                                            where exc.ID == ann.ExternalCreator
                                                                                            select exc.GivenName + " " + exc.FamilyName
                                                                                            ).FirstOrDefault(),
                                   AnnotationInfo = AnnotationInfo,
                                   AnnotationReplies = annotationReplies,
                                 });

            //Order annotations based on current setting
            switch (reportGrouping)
            {
                case AnnotationReportGrouping.User:
                    annotations = annotations.OrderBy(u => u.IsExternalCreator).ThenBy(u => u.UserDisplayName);
                    break;
                case AnnotationReportGrouping.Date:
                    annotations = annotations.OrderBy(u => u.AnnotatedDate);
                    break;
                case AnnotationReportGrouping.Group:
                    annotations = annotations.OrderBy(u => u.GroupId);
                    break;
            }

            return annotations.ToArray();
        }

        public static ApprovalAnnotationDatabaseModelPDFreport[] GetApprovalAnnotationsInfo(int approvalPageId, int[] annotationIds ,DbContextBL context)
        {
            int approvalId = (from ap in context.ApprovalPages where ap.ID == approvalPageId select ap.Approval).FirstOrDefault();
            var annotations = (from ann in context.ApprovalAnnotations
                               join ap in context.ApprovalPages on ann.Page equals ap.ID
                               join a in context.Approvals on ap.Approval equals a.ID
                               where ann.Page == approvalPageId && ann.Parent == null && annotationIds.Contains(ann.ID)
                               let AnnotationInfo = (from ann1 in context.ApprovalAnnotations
                                                     let internalUser = (from u in context.Users where ann1.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                     let externalUser = (from ex in context.ExternalCollaborators where ann1.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                     let phaseName = (from ajp in context.ApprovalJobPhases
                                                                      where ajp.ID == ann1.Phase
                                                                      select ajp.Name).FirstOrDefault()
                                                     let checklistName = (from anci in context.ApprovalAnnotationCheckListItems
                                                                          join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                          join c in context.CheckList on ci.CheckList equals c.ID
                                                                          where anci.ApprovalAnnotation == ann.ID
                                                                          select c.Name).FirstOrDefault()
                                                     let attachments = (from ana in context.ApprovalAnnotationAttachments
                                                                        where ann.ID == ana.ApprovalAnnotation
                                                                        select ana.DisplayName).ToList()
                                                     let userStatus = (from u in context.Users where ann1.Creator == u.ID select u.Status).FirstOrDefault()
                                                     let checklistItems = (from anci in context.ApprovalAnnotationCheckListItems
                                                                           join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                           join c in context.CheckList on ci.CheckList equals c.ID
                                                                           where anci.ApprovalAnnotation == ann.ID
                                                                           select new AnnotationChecklistIteminfo
                                                                           {
                                                                               ChecklistItemName = ci.Name,
                                                                               ChecklistItemValue = anci.TextValue
                                                                           }).ToList()
                                                     where ann1.ID == ann.ID
                                                     select new ApprovalAnnotationInfo
                                                     {
                                                         AnnotationID = ann1.ID,
                                                         AnnotationComment = ann1.Comment,
                                                         AnnotationOwner = internalUser == null ? externalUser : internalUser +
                                                                            (((from ac in context.ApprovalCollaborators
                                                                               join u in context.Users
                                                                               on ac.SubstituteUser equals u.ID
                                                                               where ac.Approval == approvalId && ac.SubstituteUser == u.ID && ac.Collaborator == ann.Creator
                                                                               select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                             ).FirstOrDefault()) ?? ""),
                                                         AnnotatedDate = ann1.AnnotatedDate,
                                                         AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                         AnnotationChecklist = checklistName != null ? checklistName : null,
                                                         AnnotationAttachments = attachments,
                                                         AnnotationChecklistItems = checklistItems,
                                                         AnnotationTimeFrame = ann1.TimeFrame,
                                                         UserStatus = userStatus > 0 ? userStatus : 0
                                                     }).FirstOrDefault()

                               let annotationReplies = (from ann2 in context.ApprovalAnnotations
                                                        let internalUser = (from u in context.Users where ann2.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                        let externalUser = (from ex in context.ExternalCollaborators where ann2.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                        let userStatus = (from u in context.Users where ann2.Creator == u.ID select u.Status).FirstOrDefault()
                                                        let phaseName = (from ajp in context.ApprovalJobPhases
                                                                         where ajp.ID == ann2.Phase
                                                                         select ajp.Name).FirstOrDefault()
                                                        where ann2.Parent == ann.ID
                                                        select new ApprovalAnnotationRepliesInfo
                                                        {
                                                            AnnotationID = ann2.ID,
                                                            AnnotationComment = ann2.Comment,
                                                            AnnotationOwner = internalUser == null ? externalUser : internalUser,
                                                            AnnotatedDate = ann2.AnnotatedDate,
                                                            AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                            UserStatus = userStatus > 0 ? userStatus : 0
                                                        }).OrderBy(x => x.AnnotationID).ToList()

                               select new ApprovalAnnotationDatabaseModelPDFreport
                               {
                                   ID = ann.ID,
                                   AnnotationInfo = AnnotationInfo,
                                   AnnotationReplies = annotationReplies,
                               });

            return annotations.ToArray();
        }

        public static ApprovalAnnotationDatabaseModelPDFreport[] GetApprovalAnnotationsInfo1(int[] approvalPageId, List<int> annotationIds, DbContextBL context)
        {
            var annotations = (from ann in context.ApprovalAnnotations
                               join ap in context.ApprovalPages on ann.Page equals ap.ID
                               join a in context.Approvals on ap.Approval equals a.ID
                               where approvalPageId.Contains(ann.Page) && ann.Parent == null && annotationIds.Contains(ann.ID)
                               let AnnotationInfo = (from ann1 in context.ApprovalAnnotations
                                                     let internalUser = (from u in context.Users where ann1.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                     let externalUser = (from ex in context.ExternalCollaborators where ann1.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                     let phaseName = (from ajp in context.ApprovalJobPhases
                                                                      where ajp.ID == ann1.Phase
                                                                      select ajp.Name).FirstOrDefault()
                                                     let checklistName = (from anci in context.ApprovalAnnotationCheckListItems
                                                                          join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                          join c in context.CheckList on ci.CheckList equals c.ID
                                                                          where anci.ApprovalAnnotation == ann.ID
                                                                          select c.Name).FirstOrDefault()
                                                     let attachments = (from ana in context.ApprovalAnnotationAttachments
                                                                        where ann.ID == ana.ApprovalAnnotation
                                                                        select ana.DisplayName).ToList()
                                                     let userStatus = (from u in context.Users where ann1.Creator == u.ID select u.Status).FirstOrDefault()
                                                     let checklistItems = (from anci in context.ApprovalAnnotationCheckListItems
                                                                           join ci in context.CheckListItem on anci.CheckListItem equals ci.ID
                                                                           join c in context.CheckList on ci.CheckList equals c.ID
                                                                           where anci.ApprovalAnnotation == ann.ID
                                                                           select new AnnotationChecklistIteminfo
                                                                           {
                                                                               ChecklistItemName = ci.Name,
                                                                               ChecklistItemValue = anci.TextValue
                                                                           }).ToList()
                                                     where ann1.ID == ann.ID
                                                     select new ApprovalAnnotationInfo
                                                     {
                                                         AnnotationID = ann1.ID,
                                                         AnnotationComment = ann1.Comment,
                                                         AnnotationOwner = internalUser == null ? externalUser : internalUser +
                                                                            (((from ac in context.ApprovalCollaborators
                                                                               join u in context.Users
                                                                               on ac.SubstituteUser equals u.ID
                                                                               where ac.Approval == ap.Approval && ac.SubstituteUser == u.ID && ac.Collaborator == ann.Creator
                                                                               select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                             ).FirstOrDefault()) ?? ""),
                                                         AnnotatedDate = ann1.AnnotatedDate,
                                                         AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                         AnnotationChecklist = checklistName != null ? checklistName : null,
                                                         AnnotationAttachments = attachments,
                                                         AnnotationChecklistItems = checklistItems,
                                                         AnnotationTimeFrame = ann1.TimeFrame,
                                                         UserStatus = userStatus > 0 ? userStatus : 0
                                                     }).FirstOrDefault()

                               let annotationReplies = (from ann2 in context.ApprovalAnnotations
                                                        let internalUser = (from u in context.Users where ann2.Creator == u.ID select u.GivenName + " " + u.FamilyName).FirstOrDefault()
                                                        let externalUser = (from ex in context.ExternalCollaborators where ann2.ExternalCreator == ex.ID select ex.GivenName + " " + ex.FamilyName).FirstOrDefault()
                                                        let userStatus = (from u in context.Users where ann2.Creator == u.ID select u.Status).FirstOrDefault()
                                                        let phaseName = (from ajp in context.ApprovalJobPhases
                                                                         where ajp.ID == ann2.Phase
                                                                         select ajp.Name).FirstOrDefault()
                                                        where ann2.Parent == ann.ID
                                                        select new ApprovalAnnotationRepliesInfo
                                                        {
                                                            AnnotationID = ann2.ID,
                                                            AnnotationComment = ann2.Comment,
                                                            AnnotationOwner = internalUser == null ? externalUser : internalUser,
                                                            AnnotatedDate = ann2.AnnotatedDate,
                                                            AnnotationPhaseName = phaseName != null ? phaseName : null,
                                                            UserStatus = userStatus > 0 ? userStatus : 0
                                                        }).OrderBy(x => x.AnnotationID).ToList()

                               select new ApprovalAnnotationDatabaseModelPDFreport
                               {
                                   ID = ann.ID,
                                   ApprovalPageId = ap.ID,
                                   AnnotationInfo = AnnotationInfo,
                                   AnnotationReplies = annotationReplies,
                               });

            return annotations.ToArray();
        }


        /// <summary>
        /// Returns annotations for Composite report type
        /// </summary>
        /// <param name="annotationIds"></param>
        /// <returns></returns>
        public static List<CompositeAnnotation> CompositeAnnotations(int[] annotationIds, DbContextBL context)
        {
            List<CompositeAnnotation> annotations = (from ann in context.ApprovalAnnotations
                                                     join ap in context.ApprovalPages on ann.Page equals ap.ID
                                                     join a in context.Approvals on ap.Approval equals a.ID
                                                     where annotationIds.Contains(ann.ID) && a.CurrentPhase == ann.Phase
                                                     select new CompositeAnnotation()
                                                        {
                                                            ID = a.ID,
                                                            ImageWidth = ap.OriginalImageWidth.Value,
                                                            ImageHeight = ap.OriginalImageHeight.Value,
                                                            PositionX = ann.CrosshairXCoord.HasValue ? ann.CrosshairXCoord.Value : 0,
                                                            PositionY = ann.CrosshairYCoord.HasValue ? ann.CrosshairYCoord.Value : 0,
                                                        }).ToList();
            return annotations;
        }

        public static string GetAnnotationMarkerSVG()
        {
            return GMGColorCommon.ReadServerFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine(SVGFolderRelativePath, AnnotationMarkerSVG)));
        }

        public static string GetAnnotationPinSVG()
        {
            return GMGColorCommon.ReadServerFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine(SVGFolderRelativePath, AnnotationPinSVG)));
        }

        public static UploadAnnotationAttachmentFile UploadAttachment(DbContextBL context, string fileName, Stream inputStream, UploadAnnotationAttachmentRequest model)
        {
            try
            {
                var data = (from u in context.Users
                    join acc in context.Accounts on u.Account equals acc.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where u.Guid == model.user_key && us.Key == "A" && model.is_external_user == false
                    select new {acc.Region, userId = u.ID, account = acc.ID}).Union(
                        (from ec in context.ExternalCollaborators
                            join acc in context.Accounts on ec.Account equals acc.ID
                            where
                                ec.Guid == model.user_key && ec.IsDeleted == false && model.is_external_user == true
                            select new {acc.Region, userId = ec.ID, account = acc.ID})).FirstOrDefault();

                var approvalData = (from a in context.Approvals
                    join ap in context.ApprovalPages on a.ID equals ap.Approval
                    join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                    where aa.ID == model.annotationId.Value && a.IsDeleted == false
                    select new {a.Guid, ap.Number}).FirstOrDefault();

                if (data != null && inputStream.Length <= 262144000) // each file must have the size <= 250MB
                {
                    string guid = Guid.NewGuid().ToString();
                    if (model.annotationId.HasValue && approvalData != null)
                    {
                        // check if the number of the annotations is smaller than 5
                        bool canUploadAnnotation = AnnotationBL.CanUploadAnnotation(context, model.annotationId.Value);
                        if (canUploadAnnotation)
                        {
                            string annotationFolderPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + approvalData.Guid + "/" +
                                                            AnnotationAttachmentFolder;
                            string destinationFolderPath = annotationFolderPath + "/" + guid;
                            if (GMGColorIO.FolderExists(annotationFolderPath, data.Region, true) &&
                                GMGColorIO.FolderExists(destinationFolderPath, data.Region, true))
                            {
                                GMG.CoZone.ScanFile.ScanFile fileScanner = new GMG.CoZone.ScanFile.ScanFile();
                                bool IsCleanFile = fileScanner.ProcessFileScanner(inputStream, fileName);
                                if (IsCleanFile)
                                { 
                                GMGColorIO.UploadFile(inputStream, destinationFolderPath + "/" + fileName,
                                    data.Region);
                                AnnotationBL.AddAnnotationAttachment(context, model.annotationId.Value, guid,
                                    fileName);
                                GMGColorLogging.log.Debug("AnnotationBL.UploadAttachment - file was uploaded with success in " + annotationFolderPath + "/" + guid);
                            }
                            }
                            else
                            {
                                throw new Exception("Could not upload attachment annotation since the file does not exist on storage");
                            }
                            return new UploadAnnotationAttachmentFile
                            {
                                filename = fileName,
                                guid = guid,
                                id = model.annotationId.Value
                            };
                        }
                        else
                        {
                            throw new Exception("You can not upload more than 5 attachments per annotation");
                        }
                    }
                    else
                    {
                        string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(data.userId, data.account, data.Region);
                        if (GMGColorIO.FolderExists(loggedUserTempFolderPath + guid + "/", data.Region, true))
                        {
                            if (!GMGColorIO.UploadFile(inputStream, loggedUserTempFolderPath + guid + "/" + fileName, data.Region))
                            {
                                throw new Exception("File " + fileName + " could not be uploaded into " + loggedUserTempFolderPath + guid + " ( stream length: " + inputStream.Length + ") ");
                            }
                            GMGColorLogging.log.Debug("File " + fileName + " was uploaded with success into " + loggedUserTempFolderPath + guid);
                        }
                        return new UploadAnnotationAttachmentFile { filename = fileName, guid = guid, id = 0 };
                    }
                }
                else if (data == null)
                {
                    GMGColorLogging.log.Debug("Upload attachment model is invalid");
                }

            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUploadAttachment, ex);
            }
            return new UploadAnnotationAttachmentFile{filename = "no file", guid = ""};
        }

        private static bool CanUploadAnnotation(DbContextBL context, int annotationId, int maxAnnotationAttachmentNr = 5)
        {
            try
            {
                return
                    (from aaa in context.ApprovalAnnotationAttachments
                        where aaa.ApprovalAnnotation == annotationId
                        select aaa.ID).Count() < maxAnnotationAttachmentNr;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCheckIfCanUploadAnnotation, ex);
            }
        }

        private static bool AddAnnotationAttachment(DbContextBL context, int annotationId, string guid, string filename, bool saveChanges = true)
        {
            try
            {
                var annotation =
                    (from aa in context.ApprovalAnnotations
                        where aa.ID == annotationId
                        select aa).FirstOrDefault();
                if (annotation != null)
                {
                    var attachment = new GMGColorDAL.ApprovalAnnotationAttachment
                    {
                        DisplayName = filename,
                        Name = filename,
                        Guid = guid,
                    };
                    annotation.ApprovalAnnotationAttachments.Add(attachment);
                    context.ApprovalAnnotationAttachments.Add(attachment);
                }
                else
                {
                    GMGColorLogging.log.Debug("AnnotationBl.AddAnnotationAttachment - annotation is null");
                }

                if (saveChanges)
                {
                    context.SaveChanges();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddAnnotationAttachment, ex);
            }
            return false;
        }

        public static DeleteAnnotationAttachmentFile DeleteAttachment(DbContextBL context, DeleteAnnotationAttachmentRequest model)
        {
            try
            {
                var data = (from u in context.Users
                            join acc in context.Accounts on u.Account equals acc.ID
                            join us in context.UserStatus on u.Status equals us.ID
                            where u.Guid == model.user_key && us.Key == "A" && model.is_external_user == false
                            select new { acc.Region, userId = u.ID, account = acc.ID }).Union(
                        (from ec in context.ExternalCollaborators
                            join acc in context.Accounts on ec.Account equals acc.ID
                            where
                                ec.Guid == model.user_key && ec.IsDeleted == false && model.is_external_user == true
                            select new { acc.Region, userId = ec.ID, account = acc.ID })).FirstOrDefault();

                if (data != null)
                {
                    if (model.annotationId.HasValue)
                    {
                        var approvalData = (from a in context.Approvals
                            join ap in context.ApprovalPages on a.ID equals ap.Approval
                            join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                            where aa.ID == model.annotationId.Value && a.IsDeleted == false
                            select new
                            {
                                a.Guid,
                                ap.Number
                            }).FirstOrDefault();

                        var attachment =
                            (from aaa in context.ApprovalAnnotationAttachments
                             where aaa.Guid.ToLower() == model.guid.ToLower() && aaa.ApprovalAnnotation == model.annotationId
                                select aaa).FirstOrDefault();

                        if (approvalData != null && attachment != null)
                        {
                            context.ApprovalAnnotationAttachments.Remove(attachment);
                            context.SaveChanges();

                            string annotationFolderPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + approvalData.Guid + "/" + AnnotationAttachmentFolder;
                            string destinationFolderPath = annotationFolderPath + "/" + model.guid;

                            if (GMGColorIO.FileExists(destinationFolderPath + "/" + model.filename, data.Region))
                            {
                                GMGColorIO.DeleteFolder(destinationFolderPath, data.Region);
                                return new DeleteAnnotationAttachmentFile
                                {
                                    filename = model.filename,
                                    guid = model.guid,
                                    result = true
                                };
                            }
                        }
                    }
                    else
                    {
                        string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(data.userId, data.account, data.Region);
                        if (GMGColorIO.FileExists(loggedUserTempFolderPath + model.guid + "/" + model.filename, data.Region))
                        {
                            GMGColorIO.DeleteFolder(loggedUserTempFolderPath + model.guid + "/", data.Region);
                            return new DeleteAnnotationAttachmentFile
                            {
                                filename = model.filename,
                                guid = model.guid,
                                result = true
                            };
                        }
                    }
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteAttachment, ex);
            }
            return new DeleteAnnotationAttachmentFile
            {
                filename = model.filename,
                guid = model.guid,
                result = false
            };
        }

        public static bool MoveAnnotationAttachmentFile(DbContextBL context, string userGuid, bool isExternal, int pageId, string guid, string filename)
        {
            try
            {
                var data = (from u in context.Users
                    join acc in context.Accounts on u.Account equals acc.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where isExternal == false && u.Guid == userGuid && us.Key == "A"
                    select new {acc.Region, userId = u.ID, account = acc.ID}).Union(
                        (from ec in context.ExternalCollaborators
                            join acc in context.Accounts on ec.Account equals acc.ID
                            where isExternal == true &&
                                    ec.Guid == userGuid && ec.IsDeleted == false
                            select new {acc.Region, userId = ec.ID, account = acc.ID})).FirstOrDefault();

                var approvalData = (from ap in context.ApprovalPages
                    join a in context.Approvals on ap.Approval equals a.ID
                    where ap.ID == pageId && a.IsDeleted == false
                    select new {ap.Number, a.Guid}).FirstOrDefault();

                if (data != null && approvalData != null)
                {
                    string loggedUserTempFolderPath = GMGColorDAL.User.GetUserTempFolderPath(data.userId, data.account, data.Region);

                    string sourceApprovalAnnotationFolderPath = loggedUserTempFolderPath + guid + "/";
                    string destinationApprovalAnnotationFolderPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + approvalData.Guid + "/" + AnnotationAttachmentFolder + "/" + guid;
                    bool sourceExists = GMGColorIO.FolderExists(sourceApprovalAnnotationFolderPath , data.Region) &&
                                        GMGColorIO.FileExists(sourceApprovalAnnotationFolderPath + filename,data.Region);
                    bool isDestinationPathExists = GMGColorIO.FolderExists(destinationApprovalAnnotationFolderPath, data.Region);

                    if (isDestinationPathExists)
                    {
                        var attachmentID = (from aaa in context.ApprovalAnnotationAttachments
                                      where aaa.Guid.ToLower() == guid.ToLower()
                                      select aaa.ID).FirstOrDefault();
                        var copiedAttachment = (from aaa in context.ApprovalAnnotationAttachments where aaa.Guid.ToLower() == guid.ToLower() && aaa.ID != attachmentID select aaa).FirstOrDefault();
                        copiedAttachment.Guid = Guid.NewGuid().ToString();
                        context.SaveChanges();
                        string newDestinationPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + approvalData.Guid + "/" + AnnotationAttachmentFolder + "/" + copiedAttachment.Guid;
                        if (GMGColorIO.FolderExists(newDestinationPath, data.Region, true) && GMGColorIO.CopyFile(destinationApprovalAnnotationFolderPath +"/" + filename, newDestinationPath + "/" + filename, data.Region))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (sourceExists)
                        {
                            if (GMGColorIO.FolderExists(destinationApprovalAnnotationFolderPath, data.Region, true) &&
                                GMGColorIO.MoveFile(sourceApprovalAnnotationFolderPath + filename,
                                    destinationApprovalAnnotationFolderPath + "/" + filename, data.Region))
                            {
                                GMGColorIO.DeleteFolder(sourceApprovalAnnotationFolderPath, data.Region);
                                return true;
                            }
                            else
                            {
                                throw new Exception("Annotation attachment could not be moved from temporary folder");
                            }
                        }
                        else
                        {
                            throw new Exception("Annotation attachment could not be moved from temporary folder, because the source file (" + sourceApprovalAnnotationFolderPath + filename + ") does not exists.");
                        }
                    }
                }
                else
                {
                    throw new Exception("Annotation attachment could not be moved from temporary folder, data or approval data is null");
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotMoveTempAttachment, ex);
            }
        }

        public static bool HasAccessToDownloadAttachmentFile(DbContextBL context, string key, string userKey, bool isExternalUser, string guid)
        {
            try
            {
                if (key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
                {
                    return false;
                }

                string userCollaborateRoleKey = (from u in context.Users
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where u.Guid.ToLower() == userKey.ToLower() && am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate
                    select r.Key).FirstOrDefault();

                if (isExternalUser)
                {
                    return (from a in context.Approvals
                            join ap in context.ApprovalPages on a.ID equals ap.Approval
                            join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                            join aaa in context.ApprovalAnnotationAttachments on aa.ID equals aaa.ApprovalAnnotation
                            join sa in context.SharedApprovals on a.ID equals sa.Approval
                            join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                            join j in context.Jobs on a.Job equals j.ID
                            join acc in context.Accounts on j.Account equals acc.ID
                            join accs in context.AccountStatus on acc.Status equals accs.ID
                            where
                                ec.Guid.ToLower() == userKey.ToLower() &&
                                aaa.Guid.ToLower() == guid.ToLower() &&
                                a.IsDeleted == false &&
                                ec.IsDeleted == false &&
                                sa.IsExpired == false &&
                                accs.Key == "A"
                            select a.ID).Take(1).Any();
                }
                else
                {
                    return (from a in context.Approvals
                            join ap in context.ApprovalPages on a.ID equals ap.Approval
                            join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                            join aaa in context.ApprovalAnnotationAttachments on aa.ID equals aaa.ApprovalAnnotation
                            join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                            join u in context.Users on ac.Collaborator equals u.ID
                            join us in context.UserStatus on u.Status equals us.ID
                            join j in context.Jobs on a.Job equals j.ID
                            join acc in context.Accounts on j.Account equals acc.ID
                            join accs in context.AccountStatus on acc.Status equals accs.ID
                            where
                                a.IsDeleted == false &&
                                aaa.Guid.ToLower() == guid.ToLower() &&
                                (u.Guid.ToLower() == userKey.ToLower() || userCollaborateRoleKey == "ADM") &&
                                accs.Key == "A"
                            select a.ID).Take(1).Any();
                }
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDownloadAnnotationAttachment, ex);
            }
        }

        public static string GetAttachmentFilename(DbContextBL context, string guid)
        {
            try
            {
                return
                    (from aaa in context.ApprovalAnnotationAttachments
                        where aaa.Guid.ToLower() == guid.ToLower()
                        select aaa.Name).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAnnotationAttachmentFileName, ex);
            }
        }

        public static Stream GetAttachmentStream(DbContextBL context, string guid)
        {
            try
            {
                var data = (
                        from a in context.Approvals
                        join ap in context.ApprovalPages on a.ID equals ap.Approval
                        join aa in context.ApprovalAnnotations on ap.ID equals aa.Page
                        join aaa in context.ApprovalAnnotationAttachments on aa.ID equals aaa.ApprovalAnnotation
                        join j in context.Jobs on a.Job equals j.ID
                        join acc in context.Accounts on j.Account equals acc.ID
                        join accs in context.AccountStatus on acc.Status equals accs.ID
                        where aaa.Guid.ToLower() == guid.ToLower() && accs.Key == "A" && a.IsDeleted == false
                        select new
                        {
                            acc.Region,
                            Filename = aaa.Name,
                            a.Guid
                        }).FirstOrDefault();

                if (data != null)
                {
                    string relativePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + data.Guid + "/" + AnnotationAttachmentFolder + "/" + guid + "/" + data.Filename;
                    return GMGColorIO.GetMemoryStreamOfAFile(relativePath, data.Region);
                }
                return null;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAnnotationAttachmentStream, ex);
            }
        }

        public static int GetAttachmentsCount(DbContextBL context, AnnotationAttachmentCountRequest model)
        {
            try
            {
                if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
                {
                    return 5;
                }
                return (from aaa in context.ApprovalAnnotationAttachments
                    join aa in context.ApprovalAnnotations on aaa.ApprovalAnnotation equals aa.ID
                    join ap in context.ApprovalPages on aa.Page equals ap.ID
                    join a in context.Approvals on ap.Approval equals a.ID
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    join u in context.Users on ac.Collaborator equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where
                        aa.ID == model.annotationId && u.Guid == model.user_key && us.Key == "A" &&
                        model.is_external_user == false
                    select aaa.ID).Count() +

                       (from aaa in context.ApprovalAnnotationAttachments
                           join aa in context.ApprovalAnnotations on aaa.ApprovalAnnotation equals aa.ID
                           join ap in context.ApprovalPages on aa.Page equals ap.ID
                           join a in context.Approvals on ap.Approval equals a.ID
                           join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                           join sa in context.SharedApprovals on a.ID equals sa.Approval
                           join ec in context.ExternalCollaborators on sa.ExternalCollaborator equals ec.ID
                           where
                               aa.ID == model.annotationId && ec.Guid == model.user_key && ec.IsDeleted == false &&
                               model.is_external_user == true
                           select aaa.ID).Count();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAnnotationAttachmentsCount, ex);
            }
        }

        public static long? GetPageDimensions(string appGuid, int pageNumber, DbContextBL context)
        {
            try
            {
                // In case of approvals processed with the new rendering engine use FullSize preview which is always close to 1200px
                if ((from app in context.Approvals
                    join sfse in context.SoftProofingSessionParams on app.ID equals sfse.Approval
                    where app.Guid == appGuid
                    select app.ID).Any())
                {
                    return null;
                }

                return
                    (from ap in context.ApprovalPages
                     join a in context.Approvals on ap.Approval equals a.ID
                     where a.Guid.ToLower() == appGuid.ToLower() && ap.Number == pageNumber
                     select ap.OriginalImageWidth * ap.OriginalImageHeight).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAnnotationAttachmentFileName, ex);
            }
        }

        public static MemoryStream CreateAnnotationReportCSV(List<int> approvalIds, GMGColorDAL.User LoggedUser, AnnotationReportGrouping reportGrouping,
                                                      List<AnnotationsReportUser> selectedUsers, Account loggedAccount, out string csvFileName, DbContextBL context)
        {
            try
            {
                List<ApprovalReportCSVModel> csvModel = GetAnnotationReportCSVModel(approvalIds, LoggedUser, reportGrouping, selectedUsers, loggedAccount, context);

                bool singleApprovalReport = csvModel.Count == 1;
                csvFileName = singleApprovalReport? csvModel.FirstOrDefault().ApprovalName : GMGColor.Resources.Resources.lblAnnotationsReport;
                csvFileName += ".csv";

                MemoryStream csvStream = new MemoryStream();
                StreamWriter writer = new StreamWriter(csvStream, System.Text.Encoding.UTF8);

                int maxNrOfReplies = csvModel.Max(t => t.Annotations.Count() > 0 ? t.Annotations.Select(o => o.Replies.Count).Max() : 0);
                bool IsVideoApproval = csvModel[0].Annotations[0].TimeFrame > 0 ? true : false;
                int MaxNrOfAttachments = csvModel.Max(t => t.Annotations.Count() > 0 ? t.Annotations.Select(o => o.AnnotationAttachments.Count).Max() : 0);

                string csvHeaderRow =
                    "\"" + (singleApprovalReport ? "" : GMGColor.Resources.Resources.lblApprovalName + "\",\"")
                                  + GMGColor.Resources.Resources.lblPageNumber
                        + "\",\"" + GMGColor.Resources.Resources.lblAnnotationNumber
                        + "\",\"" + GMGColor.Resources.Resources.lblAnnotationDate
                        + "\",\"" + GMGColor.Resources.Resources.lblUser
                        + "\",\"" + GMGColor.Resources.Resources.lblAnnotationComment
                        + "\",\"" + (IsVideoApproval ?  GMGColor.Resources.Resources.lblTimeFrame : "");

                for (int i = 1; i <= MaxNrOfAttachments; i++)
                {
                    csvHeaderRow += "\",\"" + "Attachment "+i;
                }

                for (int i = 1; i <= maxNrOfReplies; i++)
                {
                    csvHeaderRow += "\",\"" + (GMGColor.Resources.Resources.lblAnnotationReply + i);
                }

                csvHeaderRow += "\"";
                writer.WriteLine(csvHeaderRow);

                foreach (var approvalModel in csvModel)
                {
                    foreach (var annotation in approvalModel.Annotations)
                    {
                        // Current annotation attachment list
                        int NrAttachments = annotation.AnnotationAttachments.Count();
                        string annotationTimeFrame = string.Empty;
                        if (IsVideoApproval)
                        {
                            var longtime = Math.Round((double)annotation.TimeFrame / 1000000);
                            var mins = longtime >= 60 ? Math.Round(longtime / 60) : 0;
                            annotationTimeFrame = string.Format("{0:00}", mins) + ":" + string.Format("{0:00}", Math.Round(longtime) % 60);
                        }

                        string comment_String = annotation.Comment.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("<p>", "").Replace("</p>", "").Replace("<strong>", "").Replace("</strong>", "").Replace("<em>", "").Replace("</em>", "").Replace("<u>", "").Replace("</u>", "");
                        if((!System.Text.RegularExpressions.Regex.Match(comment_String, Constants.CollaborateNameRegex).Success)
                            &&(!System.Text.RegularExpressions.Regex.Match(comment_String, @"^[a-zA-Z ]+$").Success) && (!comment_String.Contains("@")))
                        {   
                            comment_String += " .";
                        }

                        string currentAnnotationRow = 
                        "\"" + (singleApprovalReport ? "" : approvalModel.ApprovalName + "\",\"")
                                  + annotation.PageNumber
                        + "\",\"" + annotation.AnnotationNumber
                        + "\",\"" + annotation.AnnotatedDate
                        + "\",\"" + ( annotation.UserStatus == (int)UserStatus.Inactive ? annotation.CreatorName + "(Inactive)" : annotation.CreatorName)
                        + "\",\"" + comment_String
                        + "\",\"" + (IsVideoApproval ? annotationTimeFrame : "");

                        foreach (var attachment in annotation.AnnotationAttachments)
                        {
                            currentAnnotationRow += "\",\"" + attachment;
                        }

                        for (int i = 1; i <= MaxNrOfAttachments - NrAttachments; i++)
                        {
                            currentAnnotationRow += "\",\"";
                        }
               
                        foreach (var reply in annotation.Replies)
                        {
                            currentAnnotationRow += "\",\"" + reply.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("<p>", "").Replace("</p>", "").Replace("<strong>", "").Replace("</strong>", "").Replace("<em>", "").Replace("</em>", "").Replace("<u>", "").Replace("</u>", "");
                        }

                        currentAnnotationRow += "\"";
                        writer.WriteLine(currentAnnotationRow);
                    }
                }

                writer.Flush();
                return csvStream;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateAnnotationReportCSV, ex);
            }
        }

        public static List<ApprovalReportCSVModel> GetAnnotationReportCSVModel(List<int> approvalIds, GMGColorDAL.User userDetails, AnnotationReportGrouping reportGrouping,
                                                                  List<AnnotationsReportUser> selectedUsers, string timeZone, string dateFormat, DbContextBL context)
        {
            List<ApprovalReportCSVModel> csvModel = new List<ApprovalReportCSVModel>();

            var internalUsersIds = selectedUsers.Where(t => !t.IsExternal).Select(t => t.ID);
            var externalUsersIds = selectedUsers.Where(t => t.IsExternal).Select(t => t.ID);
            int AnnotationIndex = 0;
            foreach (int approvalId in approvalIds)
            {
                ApprovalReportCSVModel approvalModel = new ApprovalReportCSVModel();
                approvalModel.ApprovalName = (from ap in context.Approvals
                                              join j in context.Jobs on ap.Job equals j.ID
                                              where ap.ID == approvalId
                                              select j.Title).FirstOrDefault();


                var annotations = (from ann in context.ApprovalAnnotations
                                   join ap in context.ApprovalPages on ann.Page equals ap.ID
                                   join a in context.Approvals on ap.Approval equals a.ID
                                   let publicPrivateAnnotations = (from ca in context.Approvals where ca.ID == approvalId && ca.PrimaryDecisionMaker > 0 && ca.PrivateAnnotations select ca.ID).Any()
                                   where ap.Approval == approvalId && ann.Parent == null && 
                                   ( !publicPrivateAnnotations || userDetails.PrivateAnnotations == false || ann.IsPrivateAnnotation == false || ann.Creator == userDetails.ID) &&
                                   (ann.ExternalCreator != null && externalUsersIds.Contains(ann.ExternalCreator.Value)
                                   || ann.Creator != null && internalUsersIds.Contains(ann.Creator.Value))
                                   select new
                                   {
                                       ann.ID,
                                       PageNumber = ap.Number,
                                       ann.Comment,
                                       ann.AnnotatedDate,
                                       ann.TimeFrame,
                                       UserStatus = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                               join us in context.UserStatus on u.Status equals us.ID
                                                                                               where u.ID == ann.Creator select u.Status).FirstOrDefault()
                                                                                               : 1,
                                       IsExternalCreator = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0,
                                       GroupId = ann.ExternalCreator.HasValue && ann.ExternalCreator > 0 ? 0 : (from u in context.Users
                                                                                                                join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                                                                                                where u.ID == ann.Creator
                                                                                                                select ugu.UserGroup).FirstOrDefault(),
                                       UserDisplayName = ann.Creator.HasValue && ann.Creator > 0 ? (from u in context.Users
                                                                                                    join us in context.UserStatus on u.Status equals us.ID
                                                                                                    where u.ID == ann.Creator
                                                                                                    select u.GivenName + " " + u.FamilyName
                                                                                             ).FirstOrDefault() +


                                                                                            (

                                                                                                ((from ac in context.ApprovalCollaborators
                                                                                                  join u in context.Users
                                                                                                  on ac.SubstituteUser equals u.ID
                                                                                                  where ac.Approval == approvalId && ac.SubstituteUser == u.ID && ac.Collaborator == ann.Creator
                                                                                                  select " (To " + u.GivenName + " " + u.FamilyName + ")"
                                                                                                ).FirstOrDefault()) ?? ""

                                                                                            )

                                                                                               :
                                                                                               (from exc in context.ExternalCollaborators
                                                                                                where exc.ID == ann.ExternalCreator
                                                                                                select exc.GivenName + " " + exc.FamilyName
                                                                                                ).FirstOrDefault(),
                                        Replies = (from rep in context.ApprovalAnnotations
                                                   where rep.Parent == ann.ID
                                                    select rep.Comment).ToList(),

                                       AnnotationAttachments = (from annatt in context.ApprovalAnnotationAttachments
                                                                where ann.ID == annatt.ApprovalAnnotation 
                                                                select annatt.DisplayName).ToList(),

                                       AnnotationChecklist = (from anncl in context.ApprovalAnnotationCheckListItems
                                                              join cli in context.CheckListItem on anncl.CheckListItem equals cli.ID
                                                              join cl in context.CheckList on cli.CheckList equals cl.ID
                                                              where ann.ID == anncl.ApprovalAnnotation
                                                              select new AnnotationChecklistModel
                                                              {
                                                                  Checklist = cl.Name,
                                                                  ChecklistItem = cli.Name,
                                                                  TextValue = anncl.TextValue
                                                              }).ToList()

                                   });

                //Order annotations based on current setting
                switch (reportGrouping)
                {
                    case AnnotationReportGrouping.User:
                        annotations = annotations.OrderBy(t => t.PageNumber).ThenBy(u => u.IsExternalCreator).ThenBy(u => u.UserDisplayName);
                        break;
                    case AnnotationReportGrouping.Date:
                        annotations = annotations.OrderBy(t => t.PageNumber).ThenBy(u => u.AnnotatedDate);
                        break;
                    case AnnotationReportGrouping.Group:
                        annotations = annotations.OrderBy(t => t.PageNumber).ThenBy(u => u.GroupId);
                        break;
                }

                foreach (var ann in annotations)
                {
                    ApprovalAnnotationReportCSVModel annotationModel = new ApprovalAnnotationReportCSVModel();
                    annotationModel.PageNumber = ann.PageNumber;
                    annotationModel.Comment = ann.Comment;
                    annotationModel.CreatorName = ann.UserDisplayName;
                    DateTime userTimeAnnotatedDate = GMGColorFormatData.GetUserTimeFromUTC(ann.AnnotatedDate, timeZone);
                    annotationModel.AnnotatedDate = GMGColorFormatData.GetFormattedDate(userTimeAnnotatedDate, dateFormat) + " " + userTimeAnnotatedDate.ToString("h:mm tt");
                    annotationModel.Replies = ann.Replies;
                    annotationModel.TimeFrame = ann.TimeFrame;
                    annotationModel.AnnotationAttachments = ann.AnnotationAttachments;
                    annotationModel.AnnotationNumber = (AnnotationIndex += 1);
                    annotationModel.UserStatus = ann.UserStatus;
                    approvalModel.Annotations.Add(annotationModel);
                }

                csvModel.Add(approvalModel);
            }


            return csvModel;
        }

        public static List<ApprovalReportCSVModel> GetAnnotationReportCSVModel(List<int> approvalIds, GMGColorDAL.User LoggedUser,
            AnnotationReportGrouping reportGrouping,
            List<AnnotationsReportUser> selectedUsers, Account loggedAccount, DbContextBL context)
        {
            return GetAnnotationReportCSVModel(approvalIds, LoggedUser, reportGrouping, selectedUsers, loggedAccount.TimeZone,
                loggedAccount.DateFormat1.Pattern, context);
        }

        public static void CopyAnnotationAttachments(DbContextBL context, CopyAnnotationRequest model, ApprovalAnnotation annotation, ApprovalAnnotation copyAnnotation)
        {
            foreach (var attachment in annotation.ApprovalAnnotationAttachments)
            {
                GMGColorDAL.ApprovalAnnotationAttachment copyAttachment = new GMGColorDAL.ApprovalAnnotationAttachment();
                copyAttachment.ApprovalAnnotation = copyAnnotation.ID;
                copyAttachment.DisplayName = attachment.DisplayName;
                copyAttachment.Name = attachment.Name;
                copyAttachment.ApprovalAnnotation1 = copyAnnotation;

                Stream attachmentStream = AnnotationBL.GetAttachmentStream(context, attachment.Guid);
                UploadAnnotationAttachmentRequest uploadAnnotationAttachmentRequest = new UploadAnnotationAttachmentRequest();
                uploadAnnotationAttachmentRequest.annotationId = copyAnnotation.ID;
                uploadAnnotationAttachmentRequest.user_key = model.user_key;
                uploadAnnotationAttachmentRequest.is_external_user = model.is_external_user;
                UploadAnnotationAttachmentFile uploadAttachmentFile = new UploadAnnotationAttachmentFile();
                uploadAttachmentFile = AnnotationBL.UploadAttachment(context, attachment.Name, attachmentStream, uploadAnnotationAttachmentRequest);

                copyAttachment.Guid = uploadAttachmentFile.guid;

                context.ApprovalAnnotationAttachments.Add(copyAttachment);
                copyAnnotation.ApprovalAnnotationAttachments.Add(copyAttachment);
            }
        }

    }
}
