﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using GMGColorNotificationService;

namespace GMGColorBusinessLogic
{
    public class ApprovalWorkflowBL
    {
        public static List<ApprovalWorkflowModel> GetApprovalWorkflows(int accountId, DbContextBL context, string searchText = null)
        {
            bool searchWorkflowName = !String.IsNullOrEmpty(searchText);
            return (from aw in context.ApprovalWorkflows
                    let hasPhases = (from ap in context.ApprovalPhases
                                     where ap.ApprovalWorkflow == aw.ID
                                     select ap.ID).Any()
                    where aw.Account == accountId && (!searchWorkflowName || aw.Name.Contains(searchText))
                    select new ApprovalWorkflowModel()
                        {
                            ID = aw.ID,
                            Account = aw.Account,
                            Name = aw.Name,
                            HasPhases = hasPhases,
                            RerunWorkflow = aw.RerunWorkflow
                        }).ToList();

        }

        public static int AddOrUpdateWorkflow(ApprovalWorkflowModel model, int accountId, int loggedUserId, DbContextBL context)
        {
            try
            {
                var approvalWorkflow = new ApprovalWorkflow();
                if (model != null)
                {
                    if (model.ID > 0)
                    {
                        approvalWorkflow = context.ApprovalWorkflows.FirstOrDefault(aw => aw.ID == model.ID);                       

                        if (approvalWorkflow != null) {
                            approvalWorkflow.Name = model.Name;
                            approvalWorkflow.ModifiedDate = DateTime.UtcNow;
                            approvalWorkflow.ModifiedBy = loggedUserId;
                            approvalWorkflow.RerunWorkflow = model.RerunWorkflow;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Name))
                        {
                            approvalWorkflow.Name = model.Name;
                            approvalWorkflow.Account = accountId;
                            approvalWorkflow.Guid = Guid.NewGuid().ToString();
                            approvalWorkflow.CreatedDate = approvalWorkflow.ModifiedDate = DateTime.UtcNow;
                            approvalWorkflow.CreatedBy = approvalWorkflow.ModifiedBy = loggedUserId;
                            approvalWorkflow.RerunWorkflow = model.RerunWorkflow;
                            context.ApprovalWorkflows.Add(approvalWorkflow);
                        }
                    }
                    context.SaveChanges();
                }
                return approvalWorkflow.ID;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddOrUpdateApprovalWorkflow, ex);
            }
        }

        public static PhaseViewModel GetPhaseDetails(Account loggedAccount, int? selectedPhase, DbContextBL context)
        {
            var phase = (from ph in context.ApprovalPhases
                         let triggerKey = (from pdt in context.PhaseDeadlineTriggers where ph.DeadlineTrigger == pdt.ID select pdt.Key).FirstOrDefault()
                         let unitKey = (from pdu in context.PhaseDeadlineUnits where ph.DeadlineUnit == pdu.ID select pdu.Key).FirstOrDefault()
                         where ph.ID == selectedPhase.Value
                         select new
                             {
                                 ph.Name,
                                 ph.Position,
                                 ph.ApprovalTrigger,
                                 ph.DecisionType,
                                 DeadlineUnit = unitKey,
                                 DeadlineTrigger = triggerKey,
                                 ph.DeadlineUnitNumber,
                                 ph.ShowAnnotationsToUsersOfOtherPhases,
                                 ph.ShowAnnotationsOfOtherPhasesToExternalUsers,
                                 ph.Deadline,
                                 ph.ApprovalShouldBeViewedByAll,
                                 ph.DecisionsShouldBeMadeByAll
                             }).FirstOrDefault();
            if (phase != null)
            {
                var phaseTrigger = (from ad in context.ApprovalDecisions
                                    let isChecked = (from ph in context.ApprovalPhases
                                                     join apt in context.ApprovalPhaseTrigger
                                                     on ph.ID equals apt.ApprovalPhase
                                                     where ph.ID == selectedPhase.Value && apt.ApprovalDecision == ad.ID
                                                     select apt.ApprovalDecision.ToString()).Any()
                                    orderby ad.Name ascending
                                    select (new AccountSettings.CollaborateApprovalStatusGeneralSettings
                                    {
                                        Id = ad.ID,
                                        Name = ad.Name,
                                        IsChecked = ad.ID == 4 ? true : isChecked
                                    })).ToList();


                var phaseModel = new PhaseViewModel
                    {
                        PhaseName = phase.Name,
                        Position = phase.Position,
                        SelectedApprovalTrigger = phase.ApprovalTrigger,
                        SelectedApprovalPhaseTrigger = phaseTrigger,
                        SelectedDecisionType = phase.DecisionType,
                        SelectedUnit = phase.DeadlineUnit,
                        SelectedTrigger = phase.DeadlineTrigger,
                        SelectedUnitValue = phase.DeadlineUnitNumber,
                        ShowAnnotationsToUsersOfOtherPhases = phase.ShowAnnotationsToUsersOfOtherPhases,
                        ShowAnnotationsOfOtherPhasesToExternalUsers = phase.ShowAnnotationsOfOtherPhasesToExternalUsers,
                        DecisionMakers = (from apc in context.ApprovalPhaseCollaborators
                                          join ap in context.ApprovalPhases on apc.Phase equals ap.ID
                                          join u in context.Users on apc.Collaborator equals u.ID
                                          join us in context.UserStatus on u.Status equals us.ID
                                          join acr in context.ApprovalCollaboratorRoles on
                                              apc.ApprovalPhaseCollaboratorRole equals acr.ID
                                          where apc.Phase == selectedPhase && acr.Key == "ANR" && us.Key != "D"
                                          select new DecisionMaker()
                                              {
                                                  ID = apc.Collaborator,
                                                  Name = u.GivenName + " " + u.FamilyName,
                                                  IsExternal = false,
                                                  IsSelected = ap.PrimaryDecisionMaker == u.ID
                                              }
                                         ).ToList().Union(from sap in context.SharedApprovalPhases
                                                          join ap in context.ApprovalPhases on sap.Phase equals ap.ID
                                                          join ec in context.ExternalCollaborators on
                                                              sap.ExternalCollaborator equals ec.ID
                                                          join acr in context.ApprovalCollaboratorRoles on
                                                              sap.ApprovalPhaseCollaboratorRole equals acr.ID
                                                          where
                                                              sap.Phase == selectedPhase && acr.Key == "ANR" &&
                                                              !ec.IsDeleted
                                                          select new DecisionMaker()
                                                              {
                                                                  ID = sap.ExternalCollaborator,
                                                                  Name = ec.GivenName + " " + ec.FamilyName,
                                                                  IsExternal = true,
                                                                  IsSelected = ap.ExternalPrimaryDecisionMaker == ec.ID
                                                              }).ToList(),
                        ApprovalShouldBeViewedByAll = phase.ApprovalShouldBeViewedByAll,
                        DecisionsShoulBeMadeByAll = phase.DecisionsShouldBeMadeByAll == null ? false : (bool)phase.DecisionsShouldBeMadeByAll
                };

                if (phase.Deadline != null)
                {
                    DateTime deadLine = GMGColorFormatData.GetUserTimeFromUTC(phase.Deadline.Value, loggedAccount.TimeZone);
                    phaseModel.DeadlineDate = GMGColorFormatData.GetFormattedDate(deadLine, loggedAccount, context);
                    phaseModel.DeadlineTime = new string(GMGColorFormatData.GetFormattedTime(deadLine, loggedAccount)
                                                                           .TakeWhile(c => c != ' ')
                                                                           .ToArray());
                    phaseModel.DeadlineTimeMeridiem = GMGColorFormatData.GetFormattedTime(deadLine, loggedAccount).Split(' ').Last();
                    phaseModel.SelectedDeadlineType = (int)GMG.CoZone.Common.DeadlineType.FixDate;
                }
                else if (phase.DeadlineTrigger != null && phase.DeadlineUnitNumber != null && phase.DeadlineUnit != null)
                {
                    phaseModel.SelectedDeadlineType = (int)GMG.CoZone.Common.DeadlineType.Automatic;
                }

                PopulateStaticData(phaseModel, loggedAccount, context);

                return phaseModel;
            }
            return new PhaseViewModel();
        }

        public static void PopulateStaticData(PhaseViewModel model, Account loggedAccount, DbContextBL context)
        {
            if (string.IsNullOrEmpty(model.DeadlineDate))
            {
                model.DeadlineDate = GMGColorFormatData.GetFormattedDate(DateTime.UtcNow, loggedAccount, context);
            }

            if (string.IsNullOrEmpty(model.DeadlineTime))
            {
                model.DeadlineTime = "hh:mm";
            }

            if (string.IsNullOrEmpty(model.DeadlineTimeMeridiem))
            {
                model.DeadlineTimeMeridiem = GMGColorFormatData.GetFormattedTime(DateTime.UtcNow, loggedAccount).Split(' ').Last();
            }

            if (model.SelectedUnitValue == null)
            {
                model.SelectedUnitValue = 1;
            }

            if (model.SelectedApprovalPhaseTrigger == null)
            {

                model.ApprovalTriggers = (from ad in context.ApprovalDecisions
                                          orderby ad.Name ascending
                                          select new
                                          {
                                              ad.ID,
                                              ad.Name
                                          }).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();

                model.SelectedApprovalPhaseTrigger = (from ad in model.ApprovalTriggers
                                                      select (new AccountSettings.CollaborateApprovalStatusGeneralSettings
                                                      {
                                                          Id = ad.Key,
                                                          Name = ad.Value.ToString(),
                                                          IsChecked = ad.Key == 4 ? true : false
                                                      })).ToList();

            }

            model.DecisionTypes = (from dt in context.DecisionTypes
                                   select new
                                   {
                                       dt.ID,
                                       dt.Name
                                   }).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();

            model.PhaseDeadlineTriggerList = (from fdt in context.PhaseDeadlineTriggers
                                              select new
                                              {
                                                  Key = fdt.Key,
                                                  Name = fdt.Name
                                              }).ToList().Select(t => new KeyValuePair<int, string>(t.Key, ApprovalPhase.GetPhaseDeadlineTriggerLabel(t.Key))).ToList();

            model.PhaseDeadlineUnitList = (from fdu in context.PhaseDeadlineUnits
                                           select new
                                           {
                                               Key = fdu.Key,
                                               Name = fdu.Name
                                           }).ToList().Select(t => new KeyValuePair<int, string>(t.Key, ApprovalPhase.GetPhaseDeadlineUnitLabel(t.Key))).ToList();
        }

        /// <summary>
        /// Adds or updates current approval phase
        /// </summary>
        /// <param name="model">Approval phase model</param>
        /// <param name="context">Database context</param>
        public static int AddOrUpdatePhase(PhaseViewModel model, Account loggedAccount, int loggedUserId, DbContextBL context)
        {
            try
            {
                var primaryDecisionMaker = model.DecisionMakers.FirstOrDefault(pdm => pdm.IsSelected);
                int? internalPdm = primaryDecisionMaker != null && !primaryDecisionMaker.IsExternal ? (int?)primaryDecisionMaker.ID : null;
                int? externalPdm = primaryDecisionMaker != null && primaryDecisionMaker.IsExternal ? (int?)primaryDecisionMaker.ID : null;

                DateTime deadline = ApprovalBL.GetFormatedApprovalDeadline(model.DeadlineDate, model.DeadlineTime, model.DeadlineTimeMeridiem, loggedAccount.DateFormat1.Pattern, loggedAccount.TimeFormat);
                deadline = GMGColorFormatData.SetUserTimeToUTC(deadline, loggedAccount.TimeZone);

                var approvalPhase = new ApprovalPhase();
                if (model.ID > 0 && !model.IsCopyPhase)
                {
                    approvalPhase = context.ApprovalPhases.FirstOrDefault(ap => ap.ID == model.ID);
                    if (approvalPhase != null)
                    {
                        //update cloned phases with new changes from template phase
                        var phases = context.ApprovalJobPhases.Where(p => p.PhaseTemplateID == model.ID).ToList();
                        foreach (var approvalJobPhase in phases)
                        {
                            approvalJobPhase.ShowAnnotationsToUsersOfOtherPhases = model.ShowAnnotationsToUsersOfOtherPhases;
                            approvalJobPhase.ShowAnnotationsOfOtherPhasesToExternalUsers = model.ShowAnnotationsOfOtherPhasesToExternalUsers;
                            approvalJobPhase.Name = model.PhaseName;
                        }
                    }

                    approvalPhase.ModifiedDate = DateTime.UtcNow;
                    approvalPhase.ModifiedBy = loggedUserId;
                }
                else
                {
                    var workflowId = model.IsCopyPhase ? model.SelectedApprovalWorkflow : model.ApprovalWorkflow;
                    int[] phasePositions = context.ApprovalPhases.Where(aw => aw.ApprovalWorkflow == workflowId).Select(t => t.Position).ToArray();
                    approvalPhase.ApprovalWorkflow = workflowId;
                    approvalPhase.Position = phasePositions.Any() ? phasePositions.Max() + 1 : 1;

                    approvalPhase.CreatedBy = approvalPhase.ModifiedBy = loggedUserId;
                    approvalPhase.CreatedDate = approvalPhase.ModifiedDate = DateTime.Now;
                }

                if (approvalPhase != null)
                {
                    approvalPhase.Name = model.PhaseName;
                    approvalPhase.PrimaryDecisionMaker = internalPdm;
                    approvalPhase.ShowAnnotationsToUsersOfOtherPhases = model.ShowAnnotationsToUsersOfOtherPhases;
                    approvalPhase.ShowAnnotationsOfOtherPhasesToExternalUsers = model.ShowAnnotationsOfOtherPhasesToExternalUsers;
                    approvalPhase.ExternalPrimaryDecisionMaker = externalPdm;
                    approvalPhase.DecisionType = model.SelectedDecisionType;
                    approvalPhase.ApprovalTrigger = 3;
                    approvalPhase.ApprovalShouldBeViewedByAll = model.ApprovalShouldBeViewedByAll;
                    approvalPhase.DecisionsShouldBeMadeByAll = model.DecisionsShoulBeMadeByAll;
                    
                    if (model.SelectedDeadlineType == (int)GMG.CoZone.Common.DeadlineType.FixDate)
                    {
                        approvalPhase.Deadline = deadline;
                        approvalPhase.DeadlineUnit = null;
                        approvalPhase.DeadlineUnitNumber = null;
                        approvalPhase.DeadlineTrigger = null;
                    }
                    else if (model.SelectedDeadlineType == (int)GMG.CoZone.Common.DeadlineType.Automatic)
                    {
                        var deadlineTrigger = context.PhaseDeadlineTriggers.Where(t => t.Key == model.SelectedTrigger).Select(t => t.ID).FirstOrDefault();
                        var deadlineUnit = context.PhaseDeadlineUnits.Where(t => t.Key == model.SelectedUnit).Select(t => t.ID).FirstOrDefault();

                        approvalPhase.DeadlineUnit = deadlineUnit;
                        approvalPhase.DeadlineUnitNumber = model.SelectedUnitValue;
                        approvalPhase.DeadlineTrigger = deadlineTrigger;
                        approvalPhase.Deadline = null;
                    }
                    else
                    {
                        approvalPhase.Deadline = null;
                        approvalPhase.DeadlineUnit = null;
                        approvalPhase.DeadlineUnitNumber = null;
                        approvalPhase.DeadlineTrigger = null;
                    }
                }

                if (model.ID == 0 || model.IsCopyPhase)
                {
                    approvalPhase.Guid = Guid.NewGuid().ToString();
                    context.ApprovalPhases.Add(approvalPhase);
                }

                ApprovalPhase objPhase = approvalPhase;
                context.SaveChanges();


                var existingPhaseTrigger = context.ApprovalPhaseTrigger.Where(x => x.ApprovalPhase == approvalPhase.ID).ToList();
                context.ApprovalPhaseTrigger.RemoveRange(existingPhaseTrigger);

                List<ApprovalPhaseTrigger> listApprovalPhaseTrigger = new List<ApprovalPhaseTrigger>();

                foreach (var decisionId in model.SelectedApprovalPhaseTrigger.Where(x => x.IsChecked == true))
                {
                    ApprovalPhaseTrigger approvalPhaseTrigger = new ApprovalPhaseTrigger();
                    approvalPhaseTrigger.ApprovalPhase = approvalPhase.ID;
                    approvalPhaseTrigger.ApprovalDecision = Convert.ToInt32(decisionId.Id);
                    listApprovalPhaseTrigger.Add(approvalPhaseTrigger);
                }

                if (model.SelectedApprovalPhaseTrigger.Where(x => x.Id == 4 && x.IsChecked == true).Count() == 0)
                {
                    ApprovalPhaseTrigger approvalPhaseTrigger = new ApprovalPhaseTrigger();
                    approvalPhaseTrigger.ApprovalPhase = approvalPhase.ID;
                    approvalPhaseTrigger.ApprovalDecision = Convert.ToInt32(4);
                    listApprovalPhaseTrigger.Add(approvalPhaseTrigger);
                }

                context.ApprovalPhaseTrigger.AddRange(listApprovalPhaseTrigger);
                context.SaveChanges();

                #region Approval phase collaborators

                var permissionsModel = new PermissionsModel
                    {
                        SelectedUsersAndGroups = model.SelectedUsersAndGroups,
                        ExternalUsers = model.ExternalUsers
                    };

                if (objPhase != null) PermissionsBL.SetApprovalPhasePermissions(permissionsModel, objPhase.ID, (model.ID > 0), loggedUserId, context);
                return objPhase.ID;
                #endregion
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotAddOrUpdateApprovalPhase, ex);
            }
        }

        public static ApprovalWorkflowPhasesDashboardModel GetApprovalWorkflowPhasesById(int workflow, int accountId, DbContextBL context)
        {
            var model = new ApprovalWorkflowPhasesDashboardModel { Workflow = workflow };

            var approvalPhaseInfo = (from ap in context.ApprovalPhases
                                     join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                                     join at in context.DecisionTypes on ap.DecisionType equals at.ID
                                     join ad in context.ApprovalDecisions on ap.ApprovalTrigger equals ad.ID

                                     let externalPDM = (from sap in context.SharedApprovalPhases
                                                        join ec in context.ExternalCollaborators on sap.ExternalCollaborator equals ec.ID
                                                        where ap.ID == sap.Phase && ap.ExternalPrimaryDecisionMaker == ec.ID && !ec.IsDeleted
                                                        select ec.GivenName + " " + ec.FamilyName).FirstOrDefault()

                                     let internalPDM = (from apc in context.ApprovalPhaseCollaborators
                                                        join u in context.Users on apc.Collaborator equals u.ID
                                                        join us in context.UserStatus on u.Status equals us.ID
                                                        where ap.ID == apc.Phase && ap.PrimaryDecisionMaker == u.ID && us.Key != "D"
                                                        select u.GivenName + " " + u.FamilyName).FirstOrDefault()

                                     where aw.Account == accountId && aw.ID == workflow
                                     orderby ap.Position
                                     select new
                                     {
                                         ID = ap.ID,
                                         Order = ap.Position,
                                         Name = ap.Name,
                                         DecisionType = at.Name,
                                         ApprovalTrigger = ad.Name,
                                         PrimaryDecisionMaker = internalPDM ?? externalPDM
                                     }).ToList();

            foreach (var phase in approvalPhaseInfo)
            {
                var phaseDetails = new PhaseDetails
                    {
                        ID = phase.ID,
                        ApprovalTrigger = phase.ApprovalTrigger,
                        DecisionType = phase.DecisionType,
                        Name = phase.Name,
                        Order = phase.Order,
                        PrimaryDecisionMaker = phase.PrimaryDecisionMaker
                    };

                model.ApprovalWorkflowPhaseDetails.Add(phaseDetails);
            }

            return model;
        }

        /// <summary>
        /// Delete approval phase and all its relations
        /// </summary>
        /// <param name="phase">The approval phase to be deleted</param>
        /// <param name="context">Database context</param>
        public static void DeleteApprovalPhase(int phase, DbContextBL context)
        {
            try
            {
                var approvalPhase = (from ap in context.ApprovalPhases where ap.ID == phase select ap).FirstOrDefault();
                if (approvalPhase != null)
                {
                    //Delete external users
                    context.SharedApprovalPhases.RemoveRange(approvalPhase.SharedApprovalPhases);

                    //Delete groups
                    context.ApprovalPhaseCollaboratorGroups.RemoveRange(approvalPhase.ApprovalPhaseCollaboratorGroups);

                    //Delete internal users
                    context.ApprovalPhaseCollaborators.RemoveRange(approvalPhase.ApprovalPhaseCollaborators);

                    //Delete approval phase trigger
                    var existingPhaseTrigger = context.ApprovalPhaseTrigger.Where(x => x.ApprovalPhase == phase).ToList();
                    context.ApprovalPhaseTrigger.RemoveRange(existingPhaseTrigger);

                    context.ApprovalPhases.Remove(approvalPhase);
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotDeleteApprovalPhase, ex);
            }
        }

        public static void ChangeApprovalPhasePosition(int changingPhase, int changedPhase, DbContextBL context)
        {
            try
            {
                var objChangingPhase = (from ap in context.ApprovalPhases where ap.ID == changingPhase select ap).FirstOrDefault();
                var objChangedPhase = (from ap in context.ApprovalPhases where ap.ID == changedPhase select ap).FirstOrDefault();

                if (objChangingPhase != null && objChangedPhase != null)
                {
                    var changingPosition = objChangingPhase.Position;
                    var changedPosition = objChangedPhase.Position;

                    objChangedPhase.Position = changingPosition;
                    objChangingPhase.Position = changedPosition;
                }

                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotChnageApprovalPhasePosition, ex);
            }
        }

        /// <summary>
        /// Checks to see if the approval meets the requiremets to move to the next phase on open in PS
        /// </summary>
        /// <param name="approvalIds">List of approval ids open in PS</param>
        public static LockApproval CheckApprovalShouldMoveToNextPhase(List<int> approvalIds, int? loggedUserId, bool? loggedUserIsExternal, int? decision)
        {
            LockApproval lockApproval = null;
            using (var context = new DbContextBL())
            {
                //filter approvals on workflow with ShouldBeViewedBy
                var appOnWorkflowWithViewedByAllCurrentPhase = (from app in context.Approvals
                                                                join ajp in context.ApprovalJobPhases on app.CurrentPhase equals ajp.ID
                                                                join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
                                                                where approvalIds.Contains(app.ID)
                                                                select new
                                                                {
                                                                    app.ID,
                                                                    aph.ApprovalShouldBeViewedByAll,
                                                                }).Where(t => t.ApprovalShouldBeViewedByAll == true).Select(t => t.ID).ToList();

                if (appOnWorkflowWithViewedByAllCurrentPhase.Count > 0)
                {
                    var approvalsDetails = GetApprovalWorkflowPhaseDetails(appOnWorkflowWithViewedByAllCurrentPhase, context);

                    foreach (var approval in approvalsDetails)
                    {
                        var isViewedByAll = approval.Collaborators.All(c => c.HasViewed == true);
                        var isPdmApproved = approval.Collaborators.Any(c => c.IsPDM == true && approval.PhaseTrigger.Contains(c.Decision));

                        if (isPdmApproved)
                        {
                            lockApproval = ApprovalBL.LockApprovalOnPhase(approval.PhaseID, approval.Approval.ID, decision, loggedUserId, context, isViewedByAll, null);
                        }
                    }
                }
                else
                {
                    var appOnWorkflowWithNoViewAllRequiredPhase = approvalIds.Where(a => !appOnWorkflowWithViewedByAllCurrentPhase.Any(ap => ap == a)).ToList();
                    var approvalsDetails = GetApprovalWorkflowPhaseDetails(appOnWorkflowWithNoViewAllRequiredPhase, context);

                    foreach (var app in approvalsDetails)
                    {
                        if (app.Collaborators.Any(c => c.IsPDM))
                        {
                            var isPdm = app.Collaborators.Any(c => ((c.IsExternal && loggedUserIsExternal.GetValueOrDefault()) || (!c.IsExternal && !loggedUserIsExternal.GetValueOrDefault())) && c.UserId == loggedUserId.GetValueOrDefault() && c.IsPDM);
                            if (isPdm)
                            {
                                var isPdmApproved = app.Collaborators.Any(c => c.IsPDM == true && app.PhaseTrigger.Contains(c.Decision));
                                lockApproval = ApprovalBL.LockApprovalOnPhase(app.PhaseID, app.Approval.ID, decision, loggedUserId, context, null, isPdmApproved);
                            }
                            else
                            {
                                lockApproval = new LockApproval();
                                lockApproval.PhaseDetails = ApprovalBL.GetInfoAboutCurrentAndNextPhases(app.Approval.ID, app.PhaseID, context);
                            }
                        }
                        else
                        {
                            var isApprovedByAll = app.Collaborators.All(c => app.PhaseTrigger.Contains(c.Decision));
                            lockApproval = ApprovalBL.LockApprovalOnPhase(app.PhaseID, app.Approval.ID, decision, loggedUserId, context, null, isApprovedByAll);
                        }
                    }
                }

                context.SaveChanges();
            }
            return lockApproval;
        }

        /// <summary>
        /// Get the approval details for the approvals opened in PS required for auto moving approval to next phase
        /// </summary>
        /// <param name="approvalsOnWorkflow">List of approvals that are on a workflow</param>
        /// <param name="context">DB context</param>
        /// <returns>A list of approval workflow phase details</returns>
        public static List<ApprovalWorkflowPhaseDetails> GetApprovalWorkflowPhaseDetails(List<int> approvalsOnWorkflow, DbContextBL context)
        {
            return (from ap in context.Approvals
                    let intCollaborators = (from apc in context.ApprovalCollaborators
                                            join u in context.Users on apc.Collaborator equals u.ID
                                            let hasViewed = (from auvi in context.ApprovalUserViewInfoes
                                                             where auvi.Approval == ap.ID && auvi.Phase == ap.CurrentPhase && auvi.User == u.ID
                                                             select auvi.ID).FirstOrDefault()
                                            let decission = (from acd in context.ApprovalCollaboratorDecisions
                                                             join apd in context.ApprovalDecisions on acd.Decision equals apd.ID
                                                             where acd.Approval == ap.ID && acd.Collaborator == u.ID && acd.Phase == ap.CurrentPhase
                                                             select apd.Key).FirstOrDefault()
                                            where apc.Approval == ap.ID && apc.Phase == ap.CurrentPhase
                                            select new CollaboratorDetails()
                                            {
                                                UserId = u.ID,
                                                HasViewed = hasViewed != 0 ? true : false,
                                                IsPDM = ap.PrimaryDecisionMaker == u.ID ? true : false,
                                                Decision = decission,
                                                IsExternal = false
                                            }).ToList()
                    let extCollaborators = (from sa in context.SharedApprovals
                                            join exc in context.ExternalCollaborators on sa.ExternalCollaborator equals exc.ID
                                            let hasViewed = (from auvi in context.ApprovalUserViewInfoes
                                                             where auvi.Approval == ap.ID && auvi.Phase == ap.CurrentPhase && auvi.ExternalUser == exc.ID
                                                             select auvi.ID).FirstOrDefault()
                                            let decission = (from acd in context.ApprovalCollaboratorDecisions
                                                             join apd in context.ApprovalDecisions on acd.Decision equals apd.ID
                                                             where acd.Approval == ap.ID && acd.ExternalCollaborator == exc.ID && acd.Phase == ap.CurrentPhase
                                                             select apd.Key).FirstOrDefault()
                                            where sa.Approval == ap.ID
                                            select new CollaboratorDetails()
                                            {
                                                UserId = exc.ID,
                                                HasViewed = hasViewed != 0 ? true : false,
                                                IsPDM = ap.ExternalPrimaryDecisionMaker == exc.ID ? true : false,
                                                Decision = decission,
                                                IsExternal = true
                                            }).ToList()
                    let phaseTrigger = (from ajp in context.ApprovalJobPhases
                                        join aph in context.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID
                                        join apt in context.ApprovalPhaseTrigger on aph.ID equals apt.ApprovalPhase
                                        join apd in context.ApprovalDecisions on apt.ApprovalDecision equals apd.ID
                                        where ajp.ID == ap.CurrentPhase
                                        select apd.Key).ToList()
                    where approvalsOnWorkflow.Contains(ap.ID)
                    select new ApprovalWorkflowPhaseDetails()
                    {
                        Approval = ap,
                        PhaseID = ap.CurrentPhase,
                        PhaseTrigger = phaseTrigger,
                        Collaborators = intCollaborators.Concat(extCollaborators)
                    }).ToList();
        }

        /// <summary>
        /// Checks if the provided workflow name is unique
        /// </summary>
        /// <param name="approvalWorkflowName"></param>
        /// <returns></returns>
        public static bool CheckAdHocWorkflowNameIsUnique(string approvalWorkflowName, int accountID, DbContextBL context)
        {
            return context.ApprovalWorkflows.Any(t => t.Name == approvalWorkflowName && t.Account == accountID);
        }

        public static string RerunFromThisPhase(int approvalId, int phaseId, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            var errors = string.Empty;
            try
            {
                //Get request details
                var requestDetails = GetReRunWorkflowDetails(approvalId, phaseId, context);

                //Exclude the new current phase from phases to reset and updated the PhaseMarkedAsCompleted to false
                var newCurrentPhase = requestDetails.PhasesToReset.First(ph => ph.Phase == phaseId);
                newCurrentPhase.PhaseMarkedAsCompleted = false;
                requestDetails.PhasesToReset.Remove(newCurrentPhase);

                ReRunWorkflowUpdateDetails(requestDetails, approvalId, phaseId, context);

                var listOfPhase = requestDetails.PhasesToReset.Select(x => x.Phase).ToList();
                var updatePhaseRerun= context.ApprovalPages.Join(context.ApprovalAnnotations, ap => ap.ID, aa => aa.Page, (ap, aa) => new { Apage = ap, Aannotation = aa }).Where(pageAndAnnotaio => pageAndAnnotaio.Apage.Approval == approvalId && listOfPhase.Contains((int)pageAndAnnotaio.Aannotation.Phase)).Select(x=>x.Aannotation).ToList();
                updatePhaseRerun.ForEach(ph => ph.IsPhaseRerun = true);

                context.SaveChanges();

                SendWorkflowPhaseRerunEmail(loggedUser, phaseId, approvalId, context);
            }
            catch (Exception ex)
            {
                errors = String.Format("Rerun approval workflow failed, with error:{0}", ex);
            }
            return errors;
        }

        private static void ReRunWorkflowUpdateDetails(ReRunWorkflowDetails requestDetails, int approvalId, int phaseId, DbContextBL context)
        {
            //Reset Approval info
            context.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().CurrentPhase = phaseId;
            context.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().IsLocked = false;            

            if (requestDetails.InternalExternalPdm.InternalPdm != null && requestDetails.InternalExternalPdm.ExternalPdm == null)
            {
                context.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().PrimaryDecisionMaker = requestDetails.InternalExternalPdm.InternalPdm;
            }
            else if (requestDetails.InternalExternalPdm.InternalPdm == null && requestDetails.InternalExternalPdm.ExternalPdm != null)
            {
                context.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().ExternalPrimaryDecisionMaker = requestDetails.InternalExternalPdm.ExternalPdm;
            }
            else
            {
                context.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().PrimaryDecisionMaker = null;
                context.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().ExternalPrimaryDecisionMaker = null;
            }           

            //Remove data generated by workflow behaviour 
            context.ApprovalJobPhaseApprovals.RemoveRange(requestDetails.PhasesToReset);
            context.ApprovalUserViewInfoes.RemoveRange(requestDetails.PhaseOpenedDatesToReset);

            //Reset collaborators decissions
            foreach (var item in requestDetails.CollabDecisionsToRest)
            {
                item.Decision = null;
                item.CompletedDate = null;
                item.OpenedDate = null;
            }
        }

        private static ReRunWorkflowDetails GetReRunWorkflowDetails(int approvalId, int phaseId, DbContextBL context)
        {
            return (from ap in context.Approvals
                    let collabDecisionsToRest = (from apcd in context.ApprovalCollaboratorDecisions
                                                 where apcd.Approval == ap.ID && apcd.Phase >= phaseId
                                                 select apcd).ToList()
                    let phasesToRest = (from apjpa in context.ApprovalJobPhaseApprovals
                                        where apjpa.Approval == ap.ID && apjpa.Phase >= phaseId
                                        select apjpa).ToList()
                    let phaseOpenedDatesToReset = (from aui in context.ApprovalUserViewInfoes
                                                   where aui.Approval == ap.ID && aui.Phase >= phaseId
                                                   select aui).ToList()
                    let approvalbHasPdm = (from ajp in context.ApprovalJobPhases
                                           where ajp.ID == phaseId
                                           select new InternalExternalPdm
                                           {
                                               InternalPdm = ajp.PrimaryDecisionMaker,
                                               ExternalPdm = ajp.ExternalPrimaryDecisionMaker
                                           }).FirstOrDefault()
                    where ap.ID == approvalId
                    select new ReRunWorkflowDetails
                    {
                        ApprovalIsLocked = ap.IsLocked,
                        CollabDecisionsToRest = collabDecisionsToRest,
                        PhasesToReset = phasesToRest,
                        PhaseOpenedDatesToReset = phaseOpenedDatesToReset,
                        InternalExternalPdm = approvalbHasPdm
                    }).FirstOrDefault();
        }
		
		public static List<int> GetExternalCollaboratorByPhaseId(int phaseId, int approvalId, DbContextBL context)
        {
            return (from sa in context.SharedApprovals
                    where sa.Approval == approvalId && sa.Phase == phaseId
                    select sa.ExternalCollaborator).ToList();
        }

        public static void SendPhaseWasActivatedEmail(int phase, int approvalId, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            //TODO This code is related with CZ-2548. Should be removed if the netzwerk don't want externals users to recieve activate/deactivate emails
            //if (externalCollaborators.Count > 0)
            //{
            //    foreach (var extCollaborator in externalCollaborators)
            //    {
            //        NotificationServiceBL.CreateNotification(new PhaseWasActivated()
            //                                                     {
            //                                                         EventCreator = loggedUser.ID,
            //                                                         InternalRecipient = (int?)null,
            //                                                         ExternalRecipient = extCollaborator,
            //                                                         CreatedDate = DateTime.UtcNow,
            //                                                         ApprovalId = approvalId,
            //                                                         Phase = phase
            //                                                     },
            //                                                     loggedUser,
            //                                                     context
            //                                                 );
            //    }
            //}
            //else
            //{
                NotificationServiceBL.CreateNotification(new PhaseWasActivated()
                                                               {
                                                                   EventCreator = loggedUser.ID,
                                                                   InternalRecipient = (int?)null,
                                                                   //ExternalRecipient = (int?)null,
                                                                   CreatedDate = DateTime.UtcNow,
                                                                   ApprovalId = approvalId,
                                                                   Phase = phase
                                                               },
                                                               loggedUser,
                                                               context
                                                           );
            //}
        }

        public static ApprovalWorkflowInstantNotification ActivateOrDeactivatePhase(ActivateDeactivatePhase item, DbContextBL context)
        {
            var phaseObj = ApprovalJobPhase.GetById(item.Phase, context);
            if (phaseObj != null)
            {
                var approval = (from a in context.Approvals where a.ID == item.ApprovalId select a).FirstOrDefault();
                if (approval != null)
                {
                    bool isPhaseRerun = false;
                    if (item.ActivatePhase)
                    {
                        if (item.UpdateStatusAndRerun || MaxActivePhaseIsCompleted(approval.IsLocked, phaseObj.ApprovalJobWorkflow, phaseObj.Position, context))
                        {
                            var approvalJobPhaseApproval = new ApprovalJobPhaseApproval()
                            {
                                Approval = item.ApprovalId,
                                Phase = item.Phase, 
                                CreatedBy = item.LoggedUserId, 
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow,
                                ModifiedBy = item.LoggedUserId
                            };

                            context.ApprovalJobPhaseApprovals.Add(approvalJobPhaseApproval);

                            approval.IsLocked = false;
                            approval.CurrentPhase = item.Phase;
                            approval.PrimaryDecisionMaker = phaseObj.PrimaryDecisionMaker;
                            approval.ExternalPrimaryDecisionMaker = phaseObj.ExternalPrimaryDecisionMaker;

                            isPhaseRerun = true;
                        }
                    }
                    phaseObj.IsActive = item.ActivatePhase;
                    context.SaveChanges();

                    if (item.UpdateStatusAndRerun)
                    {
                        return null;
                    }
                    return isPhaseRerun ? GetApprovalWorkflowInstantNotification(approval.ID, item.LoggedAccount, item.LoggedUserId, context, true) 
                                        : GetDashboardInstantNotification(phaseObj.Position, approval.CurrentPhase, item.LoggedUserId, item.ApprovalId, context);
                }
            }
            return null;
        }

        public static ApprovalWorkflowInstantNotification GetInstantNotification(int approvalId, int phase, Account loggedAccount, int loggedUserId, DbContextBL context)
        {
            var phaseObj = ApprovalJobPhase.GetById(phase, context);
            bool approvalIsLocked = Approval.IsApprovalLocked(approvalId, context);
            var isLastActivePhaseCompleted = MaxActivePhaseIsCompleted(approvalIsLocked, phaseObj.ApprovalJobWorkflow, phaseObj.Position, context);

            return GetApprovalWorkflowInstantNotification(approvalId, loggedAccount, loggedUserId, context, isLastActivePhaseCompleted);
        }

        private static ApprovalWorkflowInstantNotification GetApprovalWorkflowInstantNotification(int approvalId, Account loggedAccount, int loggedUserId, DbContextBL context, bool isLastPhaseCompleted)
        {
            var approval = ApprovalBL.GetInfoAboutCurrentAndNextPhases(approvalId, context);
           
            if (approval != null)
            {
                var instantNotification = new ApprovalWorkflowInstantNotification
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Version = approvalId.ToString(),
                    Creator = loggedUserId,
                    Decision = string.Empty,
                    NextPhase = approval.NextPhase != null ? approval.NextPhase.Name : "-"
                };

                approval.NextPhase = null;

                ApprovalBL.GetPhaseDetails(approval, isLastPhaseCompleted, context, loggedAccount, ref instantNotification);
                return instantNotification;
            }
            return null;
        }

        private static ApprovalWorkflowInstantNotification GetDashboardInstantNotification(int phasePosition, int? currentPhase, int loggedUserId, int approvalId, DbContextBL context)
        {
            var nextActivePhase = GetNextActivePhase(currentPhase.GetValueOrDefault(), context);

            var nextPhaseName = nextActivePhase != null ? phasePosition <= nextActivePhase.Position ? nextActivePhase.Name : string.Empty
                                                        : "-";
            if (!string.IsNullOrEmpty(nextPhaseName))
            {
                var instantNotificationItem = new ApprovalWorkflowInstantNotification
                {
                    ID = BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0),
                    Creator = loggedUserId,
                    Version = approvalId.ToString(),
                    NextPhase = nextPhaseName
                };
                return instantNotificationItem;
            }
            return null;
        }

        private static bool MaxActivePhaseIsCompleted(bool approvalIsLocked, int workflowId, int phasePosition, DbContextBL context)
        {  
            var maxPhase = (from ajp in context.ApprovalJobPhases
                            join ajpa in context.ApprovalJobPhaseApprovals on ajp.ID equals ajpa.Phase                            
                            where ajp.IsActive && ajp.ApprovalJobWorkflow == workflowId
                            orderby ajp.Position descending
                            select new { Id = ajp.ID, Position = ajp.Position }).FirstOrDefault();

            var maxActivePhaseIsCompleted = (maxPhase != null && maxPhase.Position <= phasePosition && approvalIsLocked);
            return maxActivePhaseIsCompleted;
        }

        public static void SendActivateOrDeactivateEmail(GMGColorDAL.User loggedUser, int phase, int approvalId, bool activatePhase, DbContextBL context)
        {
            //TODO This code is related with CZ-2548. Should be removed if the netzwerk don't want externals users to recieve activate/deactivate emails
            //List<int> externalCollaborators = GetExternalCollaboratorByPhaseId(phase, approvalId, context);

            if (activatePhase)
            {
                SendPhaseWasActivatedEmail(phase, approvalId, loggedUser, context);
            }
            else
            {
                SendPhaseWasDeactivatedEmail(phase, approvalId, loggedUser, context);
            }
        }

        public static void SendPhaseWasDeactivatedEmail(int phase, int approvalId, GMGColorDAL.User loggedUser, DbContextBL context)
        {
            //TODO This code is related with CZ-2548. Should be removed if the netzwerk don't want externals users to recieve activate/deactivate emails
            //if (externalCollaborators.Count > 0)
            //{
            //    foreach (var extCollaborator in externalCollaborators)
            //    {
            //        NotificationServiceBL.CreateNotification(new PhaseWasDeactivated()
            //                                                     {
            //                                                         EventCreator = loggedUser.ID,
            //                                                         InternalRecipient = (int?)null,
            //                                                         ExternalRecipient = extCollaborator,
            //                                                         CreatedDate = DateTime.UtcNow,
            //                                                         ApprovalId = approvalId,
            //                                                         Phase = phase
            //                                                     },
            //                                                     loggedUser,
            //                                                     context
            //                                                 );
            //    }
            //}
            //else
            //{
                NotificationServiceBL.CreateNotification(new PhaseWasDeactivated()
                                                               {
                                                                   EventCreator = loggedUser.ID,
                                                                   InternalRecipient = (int?)null,
                                                                   //ExternalRecipient = (int?)null,
                                                                   CreatedDate = DateTime.UtcNow,
                                                                   ApprovalId = approvalId,
                                                                   Phase = phase
                                                               },
                                                               loggedUser,
                                                               context
                                                           );
            //}
        }

        public static void SendWorkflowPhaseRerunEmail(GMGColorDAL.User loggedUser, int phase, int approvalId, DbContextBL context)
        {
            List<int> externalCollaborators = ApprovalWorkflowBL.GetExternalCollaboratorByPhaseId(phase, approvalId, context);

            foreach (var extCollaborator in externalCollaborators)
            {
                NotificationServiceBL.CreateNotification(new WorkflowPhaseRerun()
                                                            {
                                                                EventCreator = loggedUser.ID,
                                                                ExternalRecipient = extCollaborator,
                                                                CreatedDate = DateTime.UtcNow,
                                                                ApprovalId = approvalId,
                                                                Phase = phase
                                                            },
                                                             loggedUser,
                                                             context
                                                         );
            }
            
            NotificationServiceBL.CreateNotification(new WorkflowPhaseRerun()
                                                            {
                                                                EventCreator = loggedUser.ID,
                                                                InternalRecipient = (int?)null,
                                                                CreatedDate = DateTime.UtcNow,
                                                                ApprovalId = approvalId,
                                                                Phase = phase
                                                            },
                                                           loggedUser,
                                                           context
                                                       );
        }

        private static ApprovalJobPhase GetNextActivePhase(int currentPhase, DbContextBL context)
        {
            return (from ajp in context.ApprovalJobPhases
                    let phase = (from ajw in context.ApprovalJobPhases
                                 where currentPhase == ajw.ID && ajw.IsActive
                                select new
                                {
                                    ajw.ApprovalJobWorkflow,
                                    ajw.Position
                                }).FirstOrDefault()
                where ajp.Position > phase.Position && ajp.ApprovalJobWorkflow == phase.ApprovalJobWorkflow && ajp.IsActive
                orderby ajp.Position
                select ajp).FirstOrDefault();
        }
    }
}
