﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Remoting.Contexts;
using System.Text;
using GMGColor.AWS;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using AppModule = GMG.CoZone.Common.AppModule;
using GMG.CoZone.Common;
using User = GMGColorDAL.User;
using GMGColorNotificationService;

namespace GMGColorBusinessLogic
{
    public class UserBL: BaseBL
    {
        /// <summary>
        /// Gets the logged user info
        /// </summary>
        /// <param name="accountDomain"></param>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static LoggedUserInfo GetLoggedUserInfo(string accountDomain, int? userId, GMGColorContext context)
        {
            try
            {
                LoggedUserInfo loggedUserInfo = null;
                if (userId != null)
                {
                    loggedUserInfo = (from u in context.Users
                                      join ac in context.Accounts on u.Account equals ac.ID
                                      join act in context.AccountTypes on ac.AccountType equals act.ID
                                      join acs in context.AccountStatus on ac.Status equals acs.ID
                                      join lcl in context.Locales on ac.Locale equals lcl.ID
                                      join th in context.AccountThemes on ac.ID equals th.Account
                                      where u.ID == userId &&
                                              !ac.IsTemporary &&
                                              ((ac.IsCustomDomainActive && ac.CustomDomain.ToLower() == accountDomain) ||
                                              (!ac.IsCustomDomainActive && ac.Domain.ToLower() == accountDomain)) &&
                                              th.Active == true
                                      select new LoggedUserInfo()
                                      {
                                          User = u,
                                          LoggedAccountTypeKey = act.Key,
                                          LoggedAccountStatusKey = acs.Key,
                                          LoggedAccountCulture = lcl.Name,
                                          LoggedAccountThemeKey = th.Key
                                      }).FirstOrDefault();

                    if (loggedUserInfo != null)
                    {
                        //check if logged user has PrimaryGroup with BrandingPreset and update theme key
                        var userBrandingPresetInfo = (from ugu in context.UserGroupUsers
                                                      join bprug in context.BrandingPresetUserGroups on ugu.UserGroup equals bprug.UserGroup
                                                      join bpr in context.BrandingPresets on bprug.BrandingPreset equals bpr.ID
                                                      join thm in context.BrandingPresetThemes on bpr.ID equals thm.BrandingPreset
                                                      where ugu.IsPrimary && ugu.User == userId.Value && thm.Active == true
                                                      select new
                                                      {
                                                          brandingPresetGuid = bpr.Guid,
                                                          headerLogo = bpr.HeaderLogoPath,
                                                          favIcon = bpr.FavIconPath,
                                                          themeKey = thm.Key
                                                      }).FirstOrDefault();

                        if (userBrandingPresetInfo != null)
                        {
                            loggedUserInfo.LoggedAccountThemeKey = userBrandingPresetInfo.themeKey;

                            //check if header logo is selected
                            if (!String.IsNullOrEmpty(userBrandingPresetInfo.headerLogo))
                            {
                                loggedUserInfo.LoggedUserBrandingHeaderLogo =
                                    userBrandingPresetInfo.brandingPresetGuid + "/" + userBrandingPresetInfo.headerLogo;
                            }

                            if (!String.IsNullOrEmpty(userBrandingPresetInfo.favIcon))
                            {
                                loggedUserInfo.LoggedUserFavIcon =
                                    userBrandingPresetInfo.brandingPresetGuid + "/" + userBrandingPresetInfo.favIcon;
                            }
                        }
                    }
                }
                else
                {
                    var accInfo = (from ac in context.Accounts
                                   join act in context.AccountTypes on ac.AccountType equals act.ID
                                   join acs in context.AccountStatus on ac.Status equals acs.ID
                                   join lcl in context.Locales on ac.Locale equals lcl.ID
                                   join th in context.AccountThemes on ac.ID equals th.Account
                                   where !ac.IsTemporary && ((ac.IsCustomDomainActive && ac.CustomDomain.ToLower() == accountDomain) ||
                                   (!ac.IsCustomDomainActive && ac.Domain.ToLower() == accountDomain)) && th.Active == true
                                   select new
                                   {
                                       ac,
                                       LoggedAccountTypeKey = act.Key,
                                       LoggedAccountStatusKey = acs.Key,
                                       LoggedAccountCulture = lcl.Name,
                                       LoggedUserFavIcon = ac.FavIconPath,
                                       LoggedAccountThemeKey = th.Key,
                                   }).FirstOrDefault();

                    if (accInfo != null)
                    {
                        loggedUserInfo = new LoggedUserInfo
                        {
                            User = new User { Account1 = accInfo.ac },
                            LoggedAccountCulture = accInfo.LoggedAccountCulture,
                            LoggedAccountStatusKey = accInfo.LoggedAccountStatusKey,
                            LoggedAccountTypeKey = accInfo.LoggedAccountTypeKey,
                            LoggedUserFavIcon = accInfo.LoggedUserFavIcon,
                            LoggedAccountThemeKey = accInfo.LoggedAccountThemeKey,

                        };
                    }
                }

                return loggedUserInfo;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidateUserGuid, ex);
            }
        }

        /// <summary>
        /// Validates the user GUID.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        internal static bool ValidateUserGuid(string userGuid, GMGColorContext context)
        {
            try
            {
                return (from u in context.Users where u.Guid == userGuid select u.ID).Any();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotValidateUserGuid, ex);
            }
        }

        public static void OutOfOfficeNotification(AddNewOutOfOffice model,User LoggedUser, DbContextBL context)
        {
          
            NotificationServiceBL.CreateNotificationSubstituteuser(new GMGColorNotificationService.OutOfOffice
            {
                EventCreator = LoggedUser.ID,
                SubstitutedUserID = model.UserId,
                Account = LoggedUser.Account
            },
            LoggedUser,
            model.UserId,
            context,
            true);
       }
         

        public static GetOutOfOfficeData GetOutOfOfficeDetails(int Id, string LoggedAccountPattern, DbContextBL context)
        {
            return (from o in context.OutOfOffices
                    where o.ID == Id
                    select new GetOutOfOfficeData
                    {
                        ID = o.ID,
                        ToDate = o.EndDate,
                        FromDate = o.StartDate,
                        Description = o.Description,
                        SubstituteUserId = o.SubstituteUser
                    }).FirstOrDefault();
        }
        public static List<GetOutOfOfficeData> GetDataFromOutOfOffice(int loggedUserId,string LoggedAccountPattern, DbContextBL context)
        {
            var outOfOfficedata = (from o in context.OutOfOffices
                     join u in context.Users on o.Owner equals u.ID
                     where o.Owner == loggedUserId
                     select new GetOutOfOfficeData
                     {
                         ID = o.ID,
                         ToDate = o.EndDate,
                         FromDate = o.StartDate,
                         UserName = u.GivenName + " " + u.FamilyName,
                         SubstituteUserId = o.SubstituteUser,
                         SubstituteUser = (from u in context.Users where u.ID == o.SubstituteUser select u.GivenName + " " + u.FamilyName).FirstOrDefault(),
                         Description = o.Description
                         
                     }).ToList();
            return outOfOfficedata;
        }

        public static List<GetOutOfOfficeUser> GetOutOfOfficeUsers(int loggedUserId, DbContextBL context)
        {
            DateTime D = DateTime.UtcNow.AddDays(-1);
            var OutOfOfficeUsers = (from o in context.OutOfOffices
                                   join u in context.Users on o.Owner equals u.ID
                                   where o.SubstituteUser == loggedUserId && 
                                   (DateTime.UtcNow >= o.StartDate && D <= o.EndDate )
                                    select new GetOutOfOfficeUser
                                   {
                                       ID = o.Owner,
                                       Name = u.GivenName + " " + u.FamilyName
                                   }).ToList();
            return OutOfOfficeUsers;
        }

        public static List<ListOfDeputyUsers> GetUsersByAccount(User loggedUserId, Account loggedAccountID, Role.RoleName loggedUserRole, DbContextBL context)
        {
            int? loggedUserRolePriority = 10;//  No Access Priority
            bool isModerator = loggedUserRole == Role.RoleName.AccountModerator;
            bool isManager = loggedUserRole == Role.RoleName.AccountManager;
            List<int> userIdsToShow = new List<int>();
            if (isManager)
            {
                List<PermissionsModel.UserOrUserGroup> collaboratorsAndGroups = PermissionsBL.GetAccountGroups(loggedAccountID, loggedUserId, isModerator, isManager, context);
                List<int> groupIds = collaboratorsAndGroups.Select(t => t.ID).ToList();

                List<int> userIds = (from ugu in context.UserGroupUsers where groupIds.Contains(ugu.UserGroup) select ugu.User).ToList();

                List<PermissionsModel.UserOrUserGroup> accountUsers = PermissionsBL.GetAccountUsers(loggedAccountID, loggedUserId.ID, context);
                accountUsers.RemoveAll(x => !userIds.Contains(x.ID));
                userIdsToShow = accountUsers.Select(t => t.ID).ToList();
            }

            loggedUserRolePriority = (from r in context.Roles
                                           join ur in context.UserRoles on r.ID equals ur.Role
                                           join u in context.Users on ur.User equals u.ID
                                           where u.ID == loggedUserId.ID && r.AppModule == 1
                                           select r.Priority).FirstOrDefault();

            List<ListOfDeputyUsers> result = (from aa in context.Users
                                                   join ur in context.UserRoles on aa.ID equals ur.User
                                                   join r in context.Roles on ur.Role equals r.ID
                                                   join am in context.AppModules on r.AppModule equals am.ID
                                                   where aa.Account == loggedAccountID.ID && !(aa.ID == loggedUserId.ID) && aa.Status == 1
                                                   && aa.DateLastLogin != null && r.Priority <= loggedUserRolePriority.Value && am.ID == 1
                                                   orderby aa.GivenName
                                                   select new ListOfDeputyUsers
                                                   {
                                                      ID = aa.ID,
                                                      Name = aa.GivenName + " " + aa.FamilyName
                                                   }).ToList();
            if(isManager)
            {
                result.RemoveAll(x => !userIdsToShow.Contains(x.ID));
            }

            return result; 
        }

        /// <summary>
        /// Gets the user id based on guid.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="context">EF context.</param>
        /// <param name="deleted">if set to <c>true</c> [deleted].</param>
        /// <returns></returns>
        public static int GetUserId(string userGuid, GMGColorContext context, bool deleted = false)
        {
            try
            {
                int userId =
                    deleted
                        ? (from u in context.Users
                           join uss in context.UserStatus on u.Status equals uss.ID
                           where u.Guid == userGuid
                           select u.ID).FirstOrDefault()
                        : (from u in context.Users
                           join uss in context.UserStatus on u.Status equals uss.ID
                           where u.Guid == userGuid && uss.Key != "D"
                           select u.ID).FirstOrDefault();
                if (userId == 0)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserIdByUserGuid);
                }
                return userId;
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserIdByUserGuid, ex);
            }
        }

        /// <summary>
        /// Gets the user roles.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        internal static List<KeyValuePair<string, GMG.CoZone.Common.AppModule>> GetUserRoles(int userId, GMGColorContext context)
        {
            try
            {
                return (from ur in context.UserRoles
                        join r in context.Roles on ur.Role equals r.ID
                        join am in context.AppModules on r.AppModule equals am.ID
                        where ur.User == userId
                        select new {r.Key, Module = am.Key}).ToList().Select(
                            o =>
                            new KeyValuePair<string, GMG.CoZone.Common.AppModule>(o.Key,
                                                                               (GMG.CoZone.Common.AppModule)o.Module)).
                    ToList();
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserRolesByUserId, ex);
            }
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <param name="userGuid">The user GUID.</param>
        /// <param name="deleted">if set to <c>true</c> [deleted].</param>
        /// <returns></returns>
        public static int GetUserId(string userGuid, bool deleted = false)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return GetUserId(userGuid, context, deleted);
                }
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserIdByUserGuid, ex);
            }
        }

        /// <summary>
        /// Gets the user roles.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static List<KeyValuePair<string, GMG.CoZone.Common.AppModule>> GetUserRoles(int userId)
        {
            try
            {
                using (GMGColorContext context = new GMGColorContext())
                {
                    return GetUserRoles(userId, context);
                }
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserRolesByUserId, ex);
            }
        }

        /// <summary>
        /// Gets the role Key.
        /// </summary>
        /// <param name="appModuleKey">The app module key.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static string GetRoleKey(GMG.CoZone.Common.AppModule appModuleKey, int userId, GMGColorContext context)
        {
            return (from ur in context.UserRoles
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && ur.User == userId
                    select r.Key).FirstOrDefault();
        }

        /// <summary>
        /// Gets the account id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">EF context.</param>
        /// <param name="deleted">if set to <c>true</c> [deleted].</param>
        /// <returns></returns>
        public static int GetAccountId(int userId, DbContextBL context, bool deleted = false)
        {
            try
            {
                int accountId = deleted
                           ? (from u in context.Users where u.ID == userId select u.Account).FirstOrDefault()
                           : (from u in context.Users
                              join a in context.Accounts on u.Account equals a.ID
                              join ass in context.AccountStatus on a.Status equals ass.ID
                              where u.ID == userId && ass.Key == "A"    // active account
                              select u.Account).FirstOrDefault();
                if (accountId == 0)
                {
                    throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAccountId);
                }
                return accountId;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAccountId, ex);
            }
        }

        /// <summary>
        /// Gets the account id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="isActive">Account is active or not</param>
        /// <param name="context">EF context.</param>
        /// <returns></returns>
        public static int GetAccountIdByUser(int userId, out bool isActive, DbContextBL context)
        {
            var account =  (from u in context.Users
                            join a in context.Accounts on u.Account equals a.ID
                            where u.ID == userId
                            select a).FirstOrDefault();

            if (account != null && account.ID != 0)
            {
                isActive = account.AccountStatu.Key == "A";

                return account.ID;
            }

            isActive = false;

            return 0;
        }

        /// <summary>
        /// Gets the account id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">EF context.</param>
        /// <param name="deleted">if set to <c>true</c> [deleted].</param>
        /// <returns></returns>
        public static int GetAccountId(string userGuid, DbContextBL context)
        {
            try
            {
                    return (from u in context.Users
                            join a in context.Accounts on u.Account equals a.ID
                            join ass in context.AccountStatus on a.Status equals ass.ID
                            where u.Guid == userGuid && ass.Key == "A"    // active account
                            select u.Account).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetAccountId, ex);
            }
        }

        public static string GetUserGuid(int userId, bool isExternal, DbContextBL context)
        {
            try
            {
                return isExternal
                           ? (from ec in context.ExternalCollaborators where ec.ID == userId select ec.Guid).Take(1).
                                 FirstOrDefault()
                           : (from u in context.Users where u.ID == userId select u.Guid).Take(1).FirstOrDefault();
            }
            catch(ExceptionBL)
            {
                throw;
            }
            catch(Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserGuid, ex);
            }
        }

        public static string GetUserName(int userId, bool isExternal, DbContextBL context)
        {
            try
            {
                return isExternal
                           ? (from ec in context.ExternalCollaborators where ec.ID == userId select ec.EmailAddress).Take(1).
                                 FirstOrDefault()
                           : (from u in context.Users where u.ID == userId select u.Username).Take(1).FirstOrDefault();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserName, ex);
            }
        }


        public static bool IsUserDeleted(int userId, DbContextBL context)
        {
            try
            {
                return (from u in context.Users
                        join us in context.UserStatus on u.Status equals us.ID
                        where u.ID == userId && us.Key == "D"
                        select u.ID).Any();
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUserGuid, ex);
            }
        }

        public static bool HasAccessToFTP(int userId, UploadEnginePermissionEnum permission, DbContextBL context)
        {
            try
            {
                return (from ugprm in context.UploadEngineUserGroupPermissions
                        join ug in context.UserGroups on ugprm.UserGroup equals ug.ID
                        join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                        join perm in context.UploadEnginePermissions on ugprm.UploadEnginePermission equals perm.ID
                        where perm.Key == (int)permission && ugu.User == userId
                        select ugprm.ID).Any();
            }
            catch (Exception ex)
            {
            	throw new ExceptionBL(ExceptionBLMessages.CouldNotVerifyAccessToFTP, ex);
            }
        }

        /// <summary>
        /// Checks if the user with the specified UserName exists
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool UserNameExists(string userName, int accID, DbContextBL context)
        {
            return (from us in context.Users
                    join st in context.UserStatus on us.Status equals st.ID
                    where us.Username.ToLower() == userName.ToLower() && us.Account == accID && st.Key == "A"
                    select us.ID).Any();
        }

        /// <summary>
        /// Gets user Username by DB ID
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUserNameByID(int userId, DbContextBL context)
        {
            return (from us in context.Users
                    where us.ID == userId
                    select us.Username).FirstOrDefault();
        }

        /// <summary>
        /// Gets user ID by UserName
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int GetUserIdByUserName(string userName, int accID, DbContextBL context)
        {
            return (from us in context.Users
                    join st in context.UserStatus on us.Status equals st.ID
                    where us.Username.ToLower() == userName.ToLower() && us.Account == accID && st.Key == "A"
                    select us.ID).FirstOrDefault();
        }


        /// <summary>
        /// Gets user ID by UserName without accountID
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static User GetUserByUserName(string userName,DbContextBL context)
        {
            return (from us in context.Users
                    join st in context.UserStatus on us.Status equals st.ID
                    where us.Username.ToLower() == userName.ToLower() && st.Key == "A"
                    select us).FirstOrDefault();
        }

        /// <summary>
        /// Gets User from database based on ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static User GetUserById(int id, DbContextBL context)
        {
            return (from u in context.Users
                    where u.ID == id
                    select u).FirstOrDefault();
        }

        /// <summary>
        /// Gets User Locale
        /// </summary>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUserLocale(int id, DbContextBL context)
        {
            return (from u in context.Users
                    join ac in context.Accounts on u.Account equals ac.ID
                    join l in context.Locales on ac.Locale equals l.ID
                    where u.ID == id
                    select l.Name).FirstOrDefault();
        }

        public static List<User> GetUserOnSearch (string searchText, int loggedAccountId, DbContextBL context)
        {
            return (from u in context.Users
                    join ur in context.UserRoles on u.ID equals ur.User
                    join us in context.UserStatus on u.Status equals us.ID
                    where u.Account == loggedAccountId && (u.GivenName.StartsWith(searchText) || u.FamilyName.StartsWith(searchText) || u.EmailAddress.StartsWith(searchText) )
                    && us.Key == "A" && (ur.Role == (int)RoleID.ProjectReport_Administrator || ur.Role == (int)RoleID.ProjectReport_Manager)
                    select u).ToList();
        }
        public static User GetUserByGuid(string guid, DbContextBL context)
        {
            return (from u in context.Users
                    where u.Guid == guid
                    select u).FirstOrDefault();
        }

        /// <summary>
        /// Retrievs all users with the given email address
        /// </summary>
        /// <param name="email"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<UserWorkflowShare> GetUsersByEmail(string email, int workflowID, int loggedAccountID, DbContextBL context)
        {
            try
            {
                var users = (from u in context.Users
                             join ur in context.UserRoles on u.ID equals ur.User
                             join us in context.UserStatus on u.Status equals us.ID
                             join r in context.Roles on ur.Role equals r.ID
                             join apm in context.AppModules on r.AppModule equals apm.ID
                             join ac in context.Accounts on u.Account equals ac.ID
                             join acs in context.AccountStatus on ac.Status equals acs.ID
                             join com in context.Companies on ac.ID equals com.Account
                             let isAlreadyShared = (from shw in context.SharedColorProofWorkflows
                                                    where shw.User == u.ID && shw.ColorProofCoZoneWorkflow == workflowID
                                                    select shw.ID).Any()
                             let deliversubscription = (from acsubs in context.AccountSubscriptionPlans
                                                        join bp in context.BillingPlans on acsubs.SelectedBillingPlan equals bp.ID
                                                        join bpt in context.BillingPlanTypes on bp.Type equals  bpt.ID
                                                        where acsubs.Account == ac.ID && bpt.AppModule == apm.ID && (!bp.IsDemo || (bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(acsubs.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS)))
                                                        select acsubs.ID).Any()
                             let deliversubscriber = (from acsubs in context.SubscriberPlanInfoes
                                                        join bp in context.BillingPlans on acsubs.SelectedBillingPlan equals bp.ID
                                                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                                      where acsubs.Account == ac.ID && bpt.AppModule == apm.ID && (!bp.IsDemo || (bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(acsubs.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS)))
                                                        select acsubs.ID).Any()
                             where u.EmailAddress.ToLower() == email.ToLower() && ac.ID != loggedAccountID
                             && apm.Key == (int)AppModule.Deliver && (r.Key == "ADM" || r.Key == "MAN")
                             && !isAlreadyShared && us.Key == "A" && acs.Key == "A" && (deliversubscriber || deliversubscription)
                             select new UserWorkflowShare
                             {
                                 Email = email,
                                 GivenName = u.GivenName,
                                 FamilyName = u.FamilyName,
                                 Company = com.Name,
                                 ID = u.ID
                             }).ToList();

                users.ForEach(t => t.UserAvatar = GMGColorDAL.User.GetImagePath(t.ID, true, context));
                return users;
            }
            catch (ExceptionBL)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotGetUsersByEmailAddress, ex);
            }
        }

        public static List<User> GetUserForPlanType(int planTypeID, DbContextBL context)
        {
            return (from bpt in context.BillingPlanTypes
                join bp in context.BillingPlans on bpt.ID equals bp.Type
                join acsp in context.AccountSubscriptionPlans on bp.ID equals acsp.SelectedBillingPlan
                join ac in context.Accounts on acsp.Account equals ac.ID
                join u in context.Users on ac.ID equals u.Account
                join us in context.UserStatus on u.Status equals us.ID
                where bpt.ID == planTypeID && us.Key == "A"
                select u).Union(from bpt in context.BillingPlanTypes
                    join bp in context.BillingPlans on bpt.ID equals bp.Type
                    join spi in context.SubscriberPlanInfoes on bp.ID equals spi.SelectedBillingPlan
                    join ac in context.Accounts on spi.Account equals ac.ID
                    join u in context.Users on ac.ID equals u.Account
                    join us in context.UserStatus on u.Status equals us.ID
                    where bpt.ID == planTypeID && us.Key == "A"
                    select u).ToList();
        }

        /// <summary>
        /// Retrieves the availabie account user ProofStudio colors (inbcluding the sepcified user color)
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="accountID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<ProofStudioUserColor> GetAvailableUserColors(int userID, int accountID, DbContextBL context)
        {            
            List<ProofStudioUserColor> availableColors = (from c in context.ProofStudioUserColors                                                           
                                                          select c).ToList();
            return availableColors;
        }

        /// <summary>
        /// Get the next user ProofStudio color for the specified account
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static ProofStudioUserColor GetAvailableUserColor(int? accountID, DbContextBL context)
        {            
            ProofStudioUserColor userColor = null;
            if (accountID.HasValue)
            {
                userColor = (from c in context.ProofStudioUserColors
                             orderby Guid.NewGuid()                             
                             select c).FirstOrDefault();
            }
            else
            {
                userColor = context.ProofStudioUserColors.OrderBy(r => Guid.NewGuid()).FirstOrDefault();
            }

            return userColor;
        }

        /// <summary>
        /// Checks wether or not the specified color is valid
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="colorHEX"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static ProofStudioUserColor ValidateProofStudioUserColor(int accountID, int userID, string colorHEX, DbContextBL context)
        {            
            ProofStudioUserColor userColor = (from c in context.ProofStudioUserColors
                                              where c.HEXValue == colorHEX                                            
                                              select c).FirstOrDefault();

            return userColor;
        }

        /// <summary>
        /// Process UserGroup Users for Edit User
        /// </summary>
        /// <param name="userGroupsInUserList"></param>
        /// <param name="objUser"></param>
        /// <param name="selectedPrimaryGroup"></param>
        /// <param name="lstGroupUsers"></param>
        /// <param name="context"></param>
        public static void AddEditUserGroupUsers(IEnumerable<UserGroupsInUser> userGroupsInUserList, User objUser, int? selectedPrimaryGroup,
            List<int> lstGroupUsers, DbContextBL context)
        {
            try
            {
                foreach (var group in userGroupsInUserList)
                {
                    UserGroup objUserGroup = GetUserGroup(group.objUserGroup.ID, context);

                    var lstApprovals = objUserGroup.ApprovalCollaboratorGroups.Select(o => o.Approval).ToList();
                    var lstFolders = objUserGroup.FolderCollaboratorGroups.Select(o => o.Folder).ToList();

                    if (group.IsUserGroupInUser)
                    {
                        if ((lstGroupUsers.Count == 0) || !lstGroupUsers.Contains(group.objUserGroup.ID))
                        {
                            UserGroupUser ugu = new UserGroupUser();
                            ugu.User1 = objUser;
                            ugu.UserGroup1 = objUserGroup;
                            //Set Primary Group
                            if (selectedPrimaryGroup.GetValueOrDefault() == objUserGroup.ID)
                            {
                                ugu.IsPrimary = true;
                            }
                            context.UserGroupUsers.Add(ugu);

                            //TODO refactor the line bellow with a single custom query
                            int GlobalCollaboratorRole = ApprovalCollaboratorRole.GetDefaultApprovalRole(Role.GetRole(objUser.CollaborateRoleKey(context)), context);

                            List<int> lstValidApprovals = ApprovalBL.ValidApprovalList(lstApprovals, objUser.ID, context);
                            lstApprovals = lstApprovals.Where(x => !lstValidApprovals.Contains(x)).ToList();

                            List<ApprovalCollaborator> listobjAppCol = new List<ApprovalCollaborator>();
                            foreach (var itemID in lstApprovals)
                            {
                                var objAppCol = new ApprovalCollaborator();
                                objAppCol.Approval = itemID;
                                objAppCol.Collaborator = objUser.ID;
                                objAppCol.AssignedDate = DateTime.UtcNow;
                                objAppCol.ApprovalCollaboratorRole = GlobalCollaboratorRole;
                                listobjAppCol.Add(objAppCol);
                            }
                            context.ApprovalCollaborators.AddRange(listobjAppCol);


                            foreach (var folderId in lstFolders)
                            {
                                if (!ApprovalBL.ExistsFolderCollborator(folderId, objUser.ID, context))
                                {
                                    var folder = new FolderCollaborator
                                    {
                                        Folder = folderId,
                                        Collaborator = objUser.ID,
                                        AssignedDate = DateTime.UtcNow
                                    };
                                    context.FolderCollaborators.Add(folder);
                                }
                            }
                        }
                        else 
                        {
                            //Change Primary Group accordingly
                            UserGroupUser ugu = objUserGroup.UserGroupUsers.FirstOrDefault(t => t.User == objUser.ID);
                            if (selectedPrimaryGroup.GetValueOrDefault() == ugu.UserGroup)
                            {
                                ugu.IsPrimary = true;
                            }
                            else if (ugu.IsPrimary)
                            {
                                ugu.IsPrimary = false;
                            }
                        }
                    }
                    else
                    {
                        if (lstGroupUsers.Contains(group.objUserGroup.ID))
                        {
                            objUser.RemoveUserFromGroup(objUserGroup.ID, context);

                            List<ApprovalCollaborator> lstApproval = ApprovalBL.GetListOfApprovalCollaborator(lstApprovals, objUser.ID, context);

                            context.ApprovalCollaborators.RemoveRange(lstApproval);
       

                            foreach (var folderId in lstFolders)
                            {
                                var folder = ApprovalBL.GetFolderCollaborator(folderId, objUser.ID, context);

                                if (folder != null && folder.ID > 0)
                                {
                                    if (!ApprovalBL.IsFolderOwner(folderId, objUser.ID, context))
                                    {
                                        context.FolderCollaborators.Remove(folder);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessUserGroupUserChanges, ex);
            }
        }

        public static void AddEditUserGroupUserVisibility(IEnumerable<UserGroupsInUser> userGroupsInUserList, User objUser, DbContextBL context)
        {
            try
            {
                foreach (var group in userGroupsInUserList)
                {
                    UserGroup objUserGroup = GetUserGroup(group.objUserGroup.ID, context);

                    List<int> lstGroupUserVisibility = (from uguv in context.UserGroupUserVisibilities
                                                  where uguv.User == objUser.ID
                                                  select uguv.UserGroup).ToList();

                    if (group.IsUserGroupInVisibility)
                    {
                                               
                        if(!lstGroupUserVisibility.Contains(group.objUserGroup.ID))
                        { 
                          UserGroupUserVisibility uguv = new UserGroupUserVisibility();
                           uguv.User1 = objUser;
                           uguv.UserGroup1 = objUserGroup;
                           uguv.PrimaryVisiblility = true;
                           context.UserGroupUserVisibilities.Add(uguv);
                         }
                        else
                        {
                            UserGroupUserVisibility uguv = objUserGroup.UserGroupUserVisibilities.FirstOrDefault(t => t.User == objUser.ID && t.UserGroup == group.objUserGroup.ID);
                            uguv.PrimaryVisiblility = true;
                        }
                        
                    }
                    else
                    {
                        UserGroupUserVisibility uguv = objUserGroup.UserGroupUserVisibilities.FirstOrDefault(t => t.User == objUser.ID && t.UserGroup == group.objUserGroup.ID);
                        if(uguv != null)
                        {
                            context.UserGroupUserVisibilities.Remove(uguv);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotProcessUserGroupUserVisibilityChanges, ex);
            }
        }

        public static List<AnnotationsReportUser> GetUsersByApprovalID(int[] approvalsIDs, DbContextBL context, bool selectUserName = false)
        {
            return
                (from aa in context.ApprovalAnnotations
                 join ap in context.ApprovalPages on aa.Page equals ap.ID
                 join a in context.Approvals on ap.Approval equals a.ID
                 join u in context.Users on aa.Creator equals u.ID
                 let isValid = aa.Phase == null || a.CurrentPhase == aa.Phase || (from apjp in context.ApprovalJobPhases
                                                                                    where apjp.ID == aa.Phase
                                                                                    select apjp.ShowAnnotationsToUsersOfOtherPhases).FirstOrDefault()
                 where approvalsIDs.Contains(a.ID) && aa.Parent == null && isValid
                select new AnnotationsReportUser
                    {
                        ID = u.ID,
                        Name = selectUserName ? u.Username : u.GivenName + " " + u.FamilyName,
                        IsExternal = false
                    }).Distinct().ToList()
                    .Union((from aa in context.ApprovalAnnotations
                           join ap in context.ApprovalPages on aa.Page equals ap.ID
                           join a in context.Approvals on ap.Approval equals a.ID
                           join ex in context.ExternalCollaborators on aa.ExternalCreator equals ex.ID
                           join sa in context.SharedApprovals on ex.ID equals sa.ExternalCollaborator
                            let isValid = aa.Phase == null || a.CurrentPhase == aa.Phase || (from apjp in context.ApprovalJobPhases
                                                                                             where apjp.ID == aa.Phase
                                                                                             select apjp.ShowAnnotationsToUsersOfOtherPhases).FirstOrDefault()
                            where approvalsIDs.Contains(a.ID) && aa.Parent == null && isValid
                            select new AnnotationsReportUser
                                {
                                    ID = ex.ID,
                                    Name = selectUserName ? ex.EmailAddress : ex.GivenName + " " + ex.FamilyName,
                                    IsExternal = true
                                }).Distinct()).ToList();
        }

        public static List<AnnotationsReportUser> GetAllAnnotationReportUsersByJobID(int jobID, DbContextBL context, bool selectUserName = false)
        {
            return
                (from aa in context.ApprovalAnnotations
                 join ap in context.ApprovalPages on aa.Page equals ap.ID
                 join a in context.Approvals on ap.Approval equals a.ID
                 join u in context.Users on aa.Creator equals u.ID
                 let isValid = aa.Phase == null || a.CurrentPhase == aa.Phase || (from apjp in context.ApprovalJobPhases
                                                                                  where apjp.ID == aa.Phase
                                                                                  select apjp.ShowAnnotationsToUsersOfOtherPhases).FirstOrDefault()
                 where a.Job == jobID && aa.Parent == null && isValid
                 select new AnnotationsReportUser
                 {
                     ID = u.ID,
                     Name = selectUserName ? u.Username : u.GivenName + " " + u.FamilyName,
                     IsExternal = false
                 }).Distinct().ToList()
                    .Union((from aa in context.ApprovalAnnotations
                            join ap in context.ApprovalPages on aa.Page equals ap.ID
                            join a in context.Approvals on ap.Approval equals a.ID
                            join ex in context.ExternalCollaborators on aa.ExternalCreator equals ex.ID
                            join sa in context.SharedApprovals on ex.ID equals sa.ExternalCollaborator
                            let isValid = aa.Phase == null || a.CurrentPhase == aa.Phase || (from apjp in context.ApprovalJobPhases
                                                                                             where apjp.ID == aa.Phase
                                                                                             select apjp.ShowAnnotationsToUsersOfOtherPhases).FirstOrDefault()
                            where a.Job == jobID && aa.Parent == null && isValid
                            select new AnnotationsReportUser
                            {
                                ID = ex.ID,
                                Name = selectUserName ? ex.EmailAddress : ex.GivenName + " " + ex.FamilyName,
                                IsExternal = true
                            }).Distinct()).ToList();
        }

        public static List<AnnotationsReportGroup> GetGroupsByUsersId(IEnumerable<int> usersIds , DbContextBL context)
        {
            return (from ug in context.UserGroups
                    join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                    where usersIds.Contains(ugu.User)
                    select new AnnotationsReportGroup
                        {
                            ID = ug.ID,
                            Name = ug.Name
                        }
                        ).Distinct().ToList();
        }

        public static List<AnnotationsReportUser> GetUsersByGroupsId(IEnumerable<int> groupsIds, DbContextBL context)
        {
            return (from ug in context.UserGroups
                    join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                    where groupsIds.Contains(ugu.UserGroup)
                    select new AnnotationsReportUser
                        {
                            ID = ugu.User,
                            IsExternal = false
                        }).Distinct().ToList();
        }


        /// <summary>
        /// Add or update user setting value
        /// </summary>
        /// <param name="userId">Logged user id</param>
        /// <param name="userSetting"></param>
        /// <param name="settingName">The name of the sentting</param>
        /// <param name="settingValue">The setting value</param>
        /// <param name="context">Database context</param>
        public static void AddOrUpdateUserSetting(int userId, UserSetting userSetting, string settingName, int settingValue, DbContextBL context)
        {
            if (userSetting == null)
            {
                userSetting = new UserSetting
                {
                    User = userId,
                    Setting = settingName,
                    ValueDataType = (from vdt in context.ValueDataTypes
                                     where vdt.Key == "IN32"
                                    select vdt.ID).FirstOrDefault()
                };
                context.UserSettings.Add(userSetting);
            }

            userSetting.Value = settingValue.ToString();
        }
        public static void UpdateLoginFailedDetails(int userId, DbContextBL context)
        {
            if (userId > 0)
            {
                var userLoginFaildDetails = (from ulfd in context.UserLoginFailedDetails
                                             where ulfd.User == userId
                                             select ulfd).FirstOrDefault();

                if (userLoginFaildDetails != null)
                {
                    userLoginFaildDetails.Attempts = 0;
                    userLoginFaildDetails.IsUserLoginLocked = false;
                }
            }

        }
        /// <summary>
        /// Add or Update "Show all users" check for current user settings
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="settingValue"></param>
        /// <param name="settingName"></param>
        /// <param name="context"></param>
        public static void AddOrUpdateUserSetting(int userId, bool settingValue, string settingName, DbContextBL context)
        {
            UserSetting userSetting = (from us in context.UserSettings
                                         where us.User == userId && us.Setting == settingName
                                         select us).FirstOrDefault();

            if (userSetting == null)
            {
                userSetting = new UserSetting
                {
                    User = userId,
                    Setting = settingName,
                    ValueDataType = (from vdt in context.ValueDataTypes
                                     where vdt.Key == "BOLN"
                                     select vdt.ID).FirstOrDefault()
                };
                context.UserSettings.Add(userSetting);
            }

            userSetting.Value = settingValue.ToString();
        }

        public static UserSetting GetUserSetting(int userId, string settingName, DbContextBL context)
        {
            return (from us in context.UserSettings
                    where us.User == userId && us.Setting == settingName
                    select us).FirstOrDefault();
        }

        /// <summary>
        /// Duplicates an user
        /// </summary>
        /// <param name="model">Duplicate user view model</param>
        /// <param name="loggedUser">Current logged user</param>
        /// <param name="objAccount">duplicated user account</param>
        /// <param name="context">Database Context</param>
        /// <returns>The new created duplicated user</returns>
        public static User DuplicateUser(DuplicateUser model, int loggedUser, Account objAccount, bool duplicatedUserIsSso, DbContextBL context)
        {
            try
            {
                var duplicateUser = new User();
                duplicateUser.FamilyName = model.LastName;
                duplicateUser.GivenName = model.FirstName;
                duplicateUser.EmailAddress = model.EmailAddress;
                duplicateUser.Username = model.EmailAddress;
                duplicateUser.Password = User.GetNewEncryptedRandomPassword(context);
                duplicateUser.Account = objAccount.ID;
                duplicateUser.Creator = loggedUser;
                duplicateUser.Modifier = loggedUser;
                duplicateUser.CreatedDate = DateTime.UtcNow;
                duplicateUser.ModifiedDate = DateTime.UtcNow;
                duplicateUser.OfficeTelephoneNumber = String.Empty;
                duplicateUser.Locale = objAccount.Locale;

                duplicateUser.IsSsoUser = duplicatedUserIsSso;

                duplicateUser.Status = duplicatedUserIsSso ? UserStatu.GetUserStatus(UserStatu.Status.Active, context).ID : UserStatu.GetUserStatus(UserStatu.Status.Invited, context).ID;
                duplicateUser.Preset = Preset.GetPreset(Preset.PresetType.Low, context).ID;

                duplicateUser.NotificationFrequency = NotificationFrequency.GetNotificationFrequency(NotificationFrequency.Frequency.Never, context).ID;
                duplicateUser.Guid = Guid.NewGuid().ToString();

                duplicateUser.ProofStudioColor = GetAvailableUserColor(objAccount.ID, context).ID;

                User sourceUser = context.Users.FirstOrDefault(t => t.ID == model.DuplicatedUser);
                duplicateUser.PrinterCompany = sourceUser.PrinterCompany;

                //copy groups membership
                foreach (var ugu in sourceUser.UserGroupUsers)
                {
                    UserGroupUser ugUser = new UserGroupUser();
                    ugUser.User1 = duplicateUser;
                    ugUser.UserGroup = ugu.UserGroup;
                    ugUser.IsPrimary = ugu.IsPrimary;

                    context.UserGroupUsers.Add(ugUser);
                }

                //copy application permissions
                foreach (var url in sourceUser.UserRoles)
                {
                    UserRole userRole = new UserRole();
                    userRole.User1 = duplicateUser;
                    userRole.Role = url.Role;

                    context.UserRoles.Add(userRole);
                }

                context.Users.Add(duplicateUser);

                return duplicateUser;
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotCreateDuplicateUser, ex);
            }
        }


        /// <summary>
        /// Gets user header logo path
        /// </summary>
        /// <param name="objAccount"></param>
        /// <param name="loggedUserBrandingLogo"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUserBrandingLogoPath(Account objAccount, string loggedUserBrandingLogo, Account.LogoType logoType, DbContextBL context, bool isExternal = false)
        {
            if (isExternal || String.IsNullOrEmpty(loggedUserBrandingLogo))
            {
                return GMGColorDAL.Account.GetLogoPath(objAccount, logoType);
            }

            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + GMGColorConfiguration.AppConfiguration.UserGroupBrandingFolder + "/" + loggedUserBrandingLogo;
            return GMGColorIO.GetAbsoluteFilePath(relativeFileURL, objAccount.Region, objAccount.IsCustomDomainActive ? objAccount.CustomDomain : objAccount.Domain);
        }

        /// <summary>
        /// Gets user favicon logo path
        /// </summary>
        /// <param name="objAccount"></param>
        /// <param name="loggedUserBrandingLogo"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetUserFavIconPath(Account objAccount, string loggedUserFavIconLogo, bool isExternal = false)
        {
            if (isExternal || String.IsNullOrEmpty(loggedUserFavIconLogo))
            {
                return GMGColorDAL.Account.GetLogoPath(objAccount, Account.LogoType.FavIconLogo);
            }

            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + GMGColorConfiguration.AppConfiguration.UserGroupBrandingFolder + "/" + loggedUserFavIconLogo;
            return GMGColorIO.GetAbsoluteFilePath(relativeFileURL, objAccount.Region, objAccount.IsCustomDomainActive ? objAccount.CustomDomain : objAccount.Domain);
        }
      
        /// <summary>
        /// Updates BrandingPreset EmailLogo
        /// </summary>
        /// <param name="objAccount">Branding Preset Account</param>
        /// <param name="uploadedEmailLogo">HeaderLogo FileName</param>
        /// <param name="brandingPreset">Brading Preset</param>
        /// <param name="logoType">Logo type Header/Email</param>
        /// <param name="loggedUserTempFolderPath">LoggedUser Temp Folder Path</param>
        public static void UpdateBrandingPresetLogo(Account objAccount, string uploadedEmailLogo, BrandingPreset brandingPreset, Account.LogoType logoType, string loggedUserTempFolderPath)
        {
            try
            {
                string relativeSourceFilePath = string.Empty, uploadFileName = string.Empty;
                string relativeFolderPath = GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath + "/" + objAccount.Guid + "/" + GMGColorConfiguration.AppConfiguration.UserGroupBrandingFolder + "/" + brandingPreset.Guid + "/";
                GMGColorIO.FolderExists(relativeFolderPath, objAccount.Region, true);
                
                uploadFileName = (!String.IsNullOrEmpty(uploadedEmailLogo.Trim())) ? uploadedEmailLogo.Replace("|", string.Empty) : string.Empty;
                if (!String.IsNullOrEmpty(uploadFileName))
                {
                    switch (logoType)
                    {
                        case Account.LogoType.HeaderLogo:
                            {
                                brandingPreset.HeaderLogoPath = "headerlogo-122px-33px" + Path.GetExtension(uploadFileName);

                                relativeSourceFilePath = loggedUserTempFolderPath + uploadFileName;
                                if (GMGColorIO.FileExists(relativeSourceFilePath, objAccount.Region))
                                {
                                    GMGColorCommon.ResizeImageBasedOnHeight(relativeSourceFilePath, relativeFolderPath + brandingPreset.HeaderLogoPath, 33, objAccount.Region);
                                }

                                break;
                            }
                        case Account.LogoType.EmailLogo:
                            {
                                brandingPreset.EmailLogoPath = "emaillogo-160px-60px" + Path.GetExtension(uploadFileName);

                                relativeSourceFilePath = loggedUserTempFolderPath + uploadFileName;
                                if (GMGColorIO.FileExists(relativeSourceFilePath, objAccount.Region))
                                {
                                    GMGColorCommon.ResizeImageBasedOnHeight(relativeSourceFilePath, relativeFolderPath + brandingPreset.EmailLogoPath, 60, objAccount.Region);
                                }
                                break;
                            }
                        case Account.LogoType.FavIconLogo:
                            {
                                brandingPreset.FavIconPath = "favicon-16px-16px" + Path.GetExtension(uploadFileName);

                                relativeSourceFilePath = loggedUserTempFolderPath + uploadFileName;
                                if (GMGColorIO.FileExists(relativeSourceFilePath, objAccount.Region))
                                {
                                    GMGColorCommon.ResizeImageBasedOnHeight(relativeSourceFilePath, relativeFolderPath + brandingPreset.FavIconPath, 16, objAccount.Region);
                                }
                                break;
                            }
                    }
                }
                else //File has been deleted
                {
                    string relativeDestinationFilePath = relativeFolderPath;

                    switch (logoType)
                    {
                        case Account.LogoType.HeaderLogo:
                            {
                                relativeDestinationFilePath += brandingPreset.HeaderLogoPath;
                                brandingPreset.HeaderLogoPath = null;
                                break;
                            }
                        case Account.LogoType.EmailLogo:
                            {
                                relativeDestinationFilePath += brandingPreset.EmailLogoPath;
                                brandingPreset.EmailLogoPath = null;
                                break;
                            }
                        case Account.LogoType.FavIconLogo:
                            {
                                relativeDestinationFilePath += brandingPreset.FavIconPath;
                                brandingPreset.FavIconPath = null;
                                break;
                            }
                    }

                    try
                    {
                        if (GMGColorIO.DeleteFileIfExists(relativeDestinationFilePath, objAccount.Region))
                        {
                            GMGColorLogging.log.Info("File was deleted from the location of :" + relativeDestinationFilePath);
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(String.Format("GMGColorIO.DeleteFileIfExists : {0} Exception details : ", relativeDestinationFilePath) + ex.Message, ex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL(ExceptionBLMessages.CouldNotUpdateBrandingPresetLogo, ex);
            }
        }

        public static int GetUserBrandingPresetTheme(int userId, DbContextBL context)
        {
            return (from ugu in context.UserGroupUsers
                    join user in context.Users on ugu.User equals user.ID
                    join bprug in context.BrandingPresetUserGroups on ugu.UserGroup equals
                        bprug.UserGroup
                    join bpr in context.BrandingPresets on bprug.BrandingPreset equals bpr.ID
                    join bpth in context.BrandingPresetThemes on bpr.ID equals bpth.BrandingPreset
                    where ugu.IsPrimary && user.ID == userId
                    select bpth.ID).FirstOrDefault();
        }

        public static List<int> GetAccountUsersByRoleAndModule(int accId, int appModuleKey, string roleKey, DbContextBL context)
        {
            return (from u in context.Users
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where am.Key == appModuleKey && u.Account == accId && r.Key == roleKey && us.Key =="A"
                    select u.ID).ToList();
        }

        public static UserGroup GetUserGroup(int groupId, DbContextBL context)
        {
            return (from gr in context.UserGroups
                    where gr.ID == groupId
                    select gr).FirstOrDefault();
        }

        /// <summary>
        /// Checks whether the user is from current account or not
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isExternal"></param>
        /// <param name="acountId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsUserFromCurrentAccount(int userId, bool isExternal, int acountId, DbContextBL context)
        {
            return isExternal
                       ? (from ec in context.ExternalCollaborators
                          where ec.Account == acountId && ec.ID == userId
                          select ec.ID).Any()
                       : (from u in context.Users
                          where u.Account == acountId && u.ID == userId
                          select u.ID).Any();
        }

        /// <summary>
        /// Check whether the current user is PDM on any template phase
        /// </summary>
        /// <param name="userId">The id of the user to be deleted</param>
        /// <param name="isExternal"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsTemplatePhasePrimaryDecisionMaker(int userId, bool isExternal, DbContextBL context)
        {
            return isExternal
                       ? (from ap in context.ApprovalPhases
                          join ec in context.ExternalCollaborators on ap.ExternalPrimaryDecisionMaker equals ec.ID
                          where !ec.IsDeleted && ap.ExternalPrimaryDecisionMaker == userId
                          select ec.ID).Any()
                       : (from ap in context.ApprovalPhases
                          join u in context.Users on ap.PrimaryDecisionMaker equals u.ID
                          join us in context.UserStatus on u.Status equals us.ID
                          where us.Key != "D" && ap.PrimaryDecisionMaker == userId
                          select u.ID).Any();
        }

        /// <summary>
        /// Check whether the current user is PDM on any pending approval phase
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isExternal"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsPendingPhasePrimaryDecisionMaker(int userId, bool isExternal, DbContextBL context)
        {
            return isExternal
                       ? (from ap in context.ApprovalJobPhases
                          let completedPhases = (from ajpa in context.ApprovalJobPhaseApprovals
                                                     join sa in context.SharedApprovals on ajpa.Phase equals sa.Phase
                                                     where sa.ExternalCollaborator == userId
                                                     select ajpa.Phase).ToList()
                          where ap.ExternalPrimaryDecisionMaker == userId && !completedPhases.Contains(ap.ID)
                          select ap.ID).Any()
                       : (from ap in context.ApprovalJobPhases
                          let completedPhases = (from ajpa in context.ApprovalJobPhaseApprovals
                                                 join ac in context.ApprovalCollaborators on ajpa.Phase equals ac.Phase
                                                 where ac.Collaborator == userId
                                                 select ajpa.Phase).ToList()
                          where ap.PrimaryDecisionMaker == userId && !completedPhases.Contains(ap.ID)
                          select ap.ID).Any();
        }

        /// <summary>
        /// Check whether the current user is owner of any approval workflows or not
        /// </summary>
        /// <param name="userId">The id of teh user to be deleted</param>
        /// <param name="accountId">The id of the current logged account</param>
        /// <param name="isExternal"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsApprovalWorkflowOwner(int userId, DbContextBL context)
        {
            return (from j in context.Jobs where j.JobOwner == userId select j.ID).Any();
        }
		
		public static bool IsAccountOwner(int userId, DbContextBL context)
        {
            return (from u in context.Users
                join ac in context.Accounts on u.Account equals ac.ID
                where u.ID == userId
                select ac.Owner).FirstOrDefault().Equals(userId);
        }

        public static JobOwnerInfo GetJobOwnerInfo(int userId, DbContextBL context)
        {
            var settingName = CollaborateGlobalSetting.ShowAllFilesToAdmins.ToString();

            //get user rights info
            var userInfo = (from u in context.Users
                            join ur in context.UserRoles on u.ID equals ur.User
                            join r in context.Roles on ur.Role equals r.ID
                            join apm in context.AppModules on r.AppModule equals apm.ID
                            where u.ID == userId && apm.Key == (int)AppModule.Collaborate
                            select new
                            {
                                userRole = r.Key,
                                isShowFileToAllAdminsOn = (from acs in context.AccountSettings
                                                           where acs.Account == u.Account && acs.Name == settingName
                                                           select acs.Value).FirstOrDefault()
                            }).FirstOrDefault();

            bool adminsCanSeeAllFiles;
            bool.TryParse(userInfo.isShowFileToAllAdminsOn, out adminsCanSeeAllFiles);

            return new JobOwnerInfo
            {
                Role = userInfo.userRole,
                CanViewAllFiles = userInfo.userRole == "ADM" && adminsCanSeeAllFiles

            };
        }
		
		public static string GetUserCulture(int userId, DbContextBL context)
        {
            return (from u in context.Users
                    join l in context.Locales on u.Locale equals l.ID
                    where u.ID == userId
                    select l.Name).SingleOrDefault();
        }

    }
}
