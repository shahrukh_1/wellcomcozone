﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Sockets;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using GMGColorDAL.Extensions;
using GMGColorNotificationService;
using System.Data.SqlClient;


namespace GMGColorBusinessLogic
{
    public class GlobalNotificationBL: BaseBL
    {
        /// <summary>
        /// Returns a list of events ids based on preset id
        /// </summary>
        /// <param name="presetId">Preset id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static List<int> GetEventsIdsByNotificationPresetID(int presetId, DbContextBL context)
        {
            return (from acpet in context.AccountNotificationPresetEventTypes
                    where acpet.NotificationPreset == presetId
                    select acpet.EventType).ToList();
        }

        /// <summary>
        /// Checks whether the event for the current preset exists in database 
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="presetId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool EventExistsInDB(int eventId, int presetId, DbContextBL context)
        {
            return (from acpet in context.AccountNotificationPresetEventTypes
                    where
                        acpet.NotificationPreset == presetId &&
                        acpet.EventType == eventId
                    select acpet.ID).Any();
        }

        /// <summary>
        /// Returns AccountNotificationPresetEventType by preset and event id
        /// </summary>
        /// <param name="presetId">Preset id</param>
        /// <param name="eventId">Event type id</param>
        /// <param name="context">Context</param>
        /// <returns></returns>
        public static GMGColorDAL.AccountNotificationPresetEventType GetAccountNotificationPresetEventType(int presetId, int eventId, DbContextBL context)
        {
            return (from acpet in context.AccountNotificationPresetEventTypes
                    where
                        acpet.NotificationPreset == presetId &&
                        acpet.EventType == eventId
                    select acpet).FirstOrDefault();
        }

        /// <summary>
        /// Get AccountNotificationPreset object by id
        /// </summary>
        /// <param name="presetId">Preset id</param>
        /// <param name="context">Context</param>
        /// <returns></returns>
        public static GMGColorDAL.AccountNotificationPreset GetAccountNotificationPreset(int presetId, DbContextBL context)
        {
            return (from acpet in context.AccountNotificationPresets
                    where acpet.ID == presetId 
                    select acpet).FirstOrDefault();
        }

        /// <summary>
        /// Get Notification Schedules for users
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<UserAccountNotificationSetting> GetUserAccountNotificationSettings(int userId,
            DbContextBL context, out bool hasPersonalNotifDisabled)
        {
            List<UserAccountNotificationSetting> userNotif = new List<UserAccountNotificationSetting>();

            var notificationSchedules = ((from nsch in context.NotificationSchedules
                                            join nsus in context.NotificationScheduleUsers on nsch.ID equals nsus.NotificationSchedule
                                            where nsus.User == userId
                                            select nsch.ID).Union(from nsch in context.NotificationSchedules
                                                join nsus in context.NotificationScheduleUserGroups on nsch.ID equals nsus.NotificationSchedule
                                                join usu in context.UserGroupUsers on nsus.UserGroup equals usu.UserGroup
                                                where usu.User == userId
                                                select nsch.ID)).Distinct();

            //get notifications schedules for user
            userNotif.AddRange(from anp in context.AccountNotificationPresets
                join apret in context.AccountNotificationPresetEventTypes on anp.ID equals apret.NotificationPreset
                join nsch in context.NotificationSchedules on anp.ID equals nsch.NotificationPreset
                join nf in context.NotificationPresetFrequencies on nsch.NotificatonFrequency equals nf.ID
                where notificationSchedules.Contains(nsch.ID)
                select new UserAccountNotificationSetting
                {
                    EventType = apret.EventType,
                    NotificationFrecvencyKey = nf.Key,
                    IsPersonalNotifDisabled = anp.DisablePersonalNotifications
                });

            hasPersonalNotifDisabled = userNotif.Any(t => t.IsPersonalNotifDisabled);

            //filter notifications by frecvency key get the events with the lowest priority
            userNotif = (from item in userNotif
                        group item by item.EventType
                        into grouped
                        let minFrecv = grouped.Min(i => i.NotificationFrecvencyKey)
                        select grouped.First(i => i.NotificationFrecvencyKey == minFrecv)).ToList();

            return userNotif;
        }

        /// <summary>
        /// Gets list of notification items for users that have daily or weekly email frequency but have notifications ins schedules with as event occur selected
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="eventTypes"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<UserNotification> GetUserNotificationItemsScheduledAsEventOccur(IEnumerable<int> eventTypes, DbContextBL context)
        {
            return (from nitem in context.NotificationItems
                join u in context.Users on nitem.InternalRecipient equals u.ID
                join nf in context.NotificationFrequencies on u.NotificationFrequency equals nf.ID
                where
                    eventTypes.Contains(nitem.NotificationType) &&
                    (nf.Key == "DLY" || nf.Key == "WLY")
                select new UserNotification
                {
                    InternalUserCreator = (nitem.Creator == null ? null : new InternalUserCreator()
                    {
                        UserId = nitem.Creator.Value
                    }),
                    NotificationItem = nitem
                }).ToList();
        }

        /// <summary>
        /// Get account users that have access schedules for current event type
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="eventTypeKey"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<int> GetScheduleUsersForEventType(int accId, string eventTypeKey, DbContextBL context)
        {
            string query = @"SELECT distinct
                   [Project2].[ID] AS[ID]
                FROM
                   (
                      SELECT
                         [Extent2].[ID] AS[ID],
                         [Extent2].[Account] AS[Account],
                         [Extent4].[EventType] AS[EventType],
                         [Extent6].[ID] AS[ID1],
                         [Extent7].[ID] AS[ID2],
                         [Extent7].[Key] AS[Key],
                         CASE
                            WHEN
                               (
                                  EXISTS
                                  (
                                     SELECT
                                        1 AS[C1]
                                     FROM
                                        [dbo].[NotificationScheduleUser] AS[Extent8]
                                     WHERE
                                        (
                                           [Extent8].[User] = [Extent2].[ID]
                                        )
                                        AND
                                        (
                                           [Extent8].[NotificationSchedule] = [Extent6].[ID]
                                        )
                                  )
                               )
                            THEN
                               cast(1 as bit)
                            ELSE
                               cast(0 as bit)
                         END
                         AS[C1],
                         CASE
                            WHEN
                               (
                                  EXISTS
                                  (
                                     SELECT
                                        1 AS[C1]
                                     FROM
                                        [dbo].[UserGroupUser] AS[Extent9]
                                        INNER JOIN
                                           [dbo].[NotificationScheduleUserGroup] AS[Extent10]
                                           ON[Extent9].[UserGroup] = [Extent10].[UserGroup]
                                     WHERE
                                        (
                                           [Extent9].[User] = [Extent2].[ID]
                                        )
                                        AND
                                        (
                                           [Extent10].[NotificationSchedule] = [Extent6].[ID]
                                        )
                                  )
                               )
                            THEN
                               cast(1 as bit)
                            ELSE
                               cast(0 as bit)
                         END
                         AS[C2]
                      FROM
                         [dbo].[AccountNotificationPreset] AS[Extent1]
                         INNER JOIN
                            [dbo].[User]
                        AS[Extent2]
                            ON[Extent1].[Account] = [Extent2].[Account]
                        INNER JOIN
                [dbo].[UserRole]
                        AS[Extent3]
                ON[Extent2].[ID] = [Extent3].[User]
                        INNER JOIN
                [dbo].[RolePresetEventType]
                        AS[Extent4]
                ON[Extent3].[Role] = [Extent4].[Role]
                        INNER JOIN
                [dbo].[AccountNotificationPresetEventType]
                        AS[Extent5]
                ON[Extent1].[ID] = [Extent5].[NotificationPreset]
                        INNER JOIN
                [dbo].[NotificationSchedule]
                        AS[Extent6]
                ON[Extent1].[ID] = [Extent6].[NotificationPreset]
                        INNER JOIN
                [dbo].[EventType]
                        AS[Extent7]
                ON[Extent5].[EventType] = [Extent7].[ID]
                        Where
                [Extent2].[Account] = @accId 
                         AND
                         (
                            [Extent7].[Key] = @eventTypeKey
                         )
                         and
                         (
                            [Extent4].[EventType] = [Extent7].[ID]
                         )
                   )
                   AS[Project2]
                where
                   (
                      [Project2].[C1] = 1
                      OR[Project2].[C2] = 1
                   )";

            return context.Database.SqlQuery<int>(query,
                  new SqlParameter("@accId", accId),
                 new SqlParameter("@eventTypeKey", eventTypeKey)
                ).ToList();
        }

        /// <summary>
        /// Check whether the user's account has collaborate, manage or deliver module active
        /// </summary>
        /// <param name="appModuleKey"></param>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <param name="isManageApi"></param>
        /// <returns></returns>
        public static bool HasSubscriptionPlan(GMG.CoZone.Common.AppModule appModuleKey, int userId, GMGColorContext context, bool isManageApi = false)
        {
            return (from asp in context.AccountSubscriptionPlans
                    join a in context.Accounts on asp.Account equals a.ID
                    join u in context.Users on a.ID equals u.Account
                    join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && u.ID == userId && (!bp.IsDemo || (bp.IsDemo && DbFunctions.DiffDays(DbFunctions.TruncateTime(asp.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS))
                    select asp.ID).Any()
                    ||
                    (from spi in context.SubscriberPlanInfoes
                     join a in context.Accounts on spi.Account equals a.ID
                     join u in context.Users on a.ID equals u.Account
                     join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                     join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                     join am in context.AppModules on bpt.AppModule equals am.ID
                     where am.Key == (int)appModuleKey && u.ID == userId && (!bp.IsDemo || (bp.IsDemo && DbFunctions.DiffDays(DbFunctions.TruncateTime(spi.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS))
                     select spi.ID).Any();
        }

        /// <summary>
        /// Check whether the current user has schedule with "Disable personal notifications" preset option checked
        /// </summary>
        /// <param name="userId">User id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static bool HasPersonalNotificationsDisabled(int userId, GMGColorContext context)
        {
            return (from u in context.Users
                     let userInSchedules = (from nsu in context.NotificationScheduleUsers
                                            join ns in context.NotificationSchedules on nsu.NotificationSchedule equals ns.ID
                                            join anp in context.AccountNotificationPresets on ns.NotificationPreset equals anp.ID
                                            where nsu.User == userId && anp.DisablePersonalNotifications
                                            select nsu.ID).ToList()
                     let userGroupInSchedules = (from ugu in context.UserGroupUsers
                                                 join nsug in context.NotificationScheduleUserGroups on ugu.UserGroup equals nsug.UserGroup
                                                 join ns in context.NotificationSchedules on nsug.NotificationSchedule equals ns.ID
                                                 join anp in context.AccountNotificationPresets on ns.NotificationPreset equals anp.ID
                                                 where ugu.User == userId && anp.DisablePersonalNotifications
                                                 select ugu.ID).ToList()

                     where u.ID == userId && (userInSchedules.Any() || userGroupInSchedules.Any())
                     select u).Any();
        }


        /// <summary>
        /// Determines if for the specified notif type proofstudio emails per session setting is enabled
        /// </summary>
        /// <param name="notifType"></param>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsUserPerSessionEmailsEnabled(string notifType, int userId, DbContextBL context)
        {
            //get notifications schedules for user
            var isPsSessionEnabled = (from anp in context.AccountNotificationPresets
                                       join apret in context.AccountNotificationPresetEventTypes on anp.ID equals apret.NotificationPreset
                                       join et in context.EventTypes on apret.EventType equals et.ID
                                       join nsch in context.NotificationSchedules on anp.ID equals nsch.NotificationPreset
                                       join nsus in context.NotificationScheduleUsers on nsch.ID equals nsus.NotificationSchedule
                                       where nsus.User == userId && et.Key == notifType && nsch.NotificationPerPSSession
                                       select nsch.ID).Any();

            if (!isPsSessionEnabled)
            {
                //get notification schedules for usergroup where user belongs
                isPsSessionEnabled = (from anp in context.AccountNotificationPresets
                    join apret in context.AccountNotificationPresetEventTypes on anp.ID equals apret.NotificationPreset
                    join et in context.EventTypes on apret.EventType equals et.ID
                    join nsch in context.NotificationSchedules on anp.ID equals nsch.NotificationPreset
                    join nsus in context.NotificationScheduleUserGroups on nsch.ID equals nsus.NotificationSchedule
                    join usu in context.UserGroupUsers on nsus.UserGroup equals usu.UserGroup
                    where usu.User == userId && et.Key == notifType && nsch.NotificationPerPSSession
                    select nsch.ID).Any();
            }

            return isPsSessionEnabled;
        }

        /// <summary>
        /// Determines if for the specified notif type proofstudio emails per session setting is enabled
        /// </summary>
        /// <param name="notifType"></param>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static int? GetPSSessionId(string sessionGuid, DbContextBL context)
        {
             var psSessionId = (from pss in context.ProofStudioSessions
                                where pss.Guid == sessionGuid
                                select pss.ID).FirstOrDefault();

            return (psSessionId > 0 ? (int?)psSessionId :
            null);
        }
      

        /// <summary>
        /// Delete notificationitems after one week.
        /// </summary>
        public static void DeleteNotificationItemsAfterOneWeek()
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    var dt = DateTime.Now.AddDays(-3);

                    var NotificationItemsList = (from pss in context.NotificationItems
                                                 where pss.CreatedDate < dt
                                                 select pss).ToList();

                    // Taking NotificationItems Backup before removing from table
                    try
                    {
                    var NotificationItemBackupsList = (from itm in NotificationItemsList
                                              select new NotificationItem_Backup
                                              {
                                                  InternalRecipient = itm.InternalRecipient,
                                                  NotificationType = itm.NotificationType,
                                                  CreatedDate = itm.CreatedDate,
                                                  Creator = itm.Creator,
                                                  CustomFields = itm.CustomFields,
                                                  PSSessionID = itm.PSSessionID,
                                                  IsDeleted = itm.IsDeleted
                                              }).ToList();

                    context.NotificationItem_Backup.AddRange(NotificationItemBackupsList);
                    context.SaveChanges();

                    }

                    catch(Exception ex)
                    {
                        throw new ExceptionBL("Exception In - GlobalNotificationBL.DeleteNotificationItemsAfterOneWeek() > Taking NotificationItems Backup before removing from table - Error occurred , Message: {0}", ex);
                    }

                    if (NotificationItemsList.Count > 0)
                    {

                        context.NotificationItems.RemoveRange(NotificationItemsList);
                            
                         context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ExceptionBL("GlobalNotificationBL.DeleteNotificationItemsAfterOneWeek() - Error occurred, Message: {0}", ex);
            }
        }
    }
}
