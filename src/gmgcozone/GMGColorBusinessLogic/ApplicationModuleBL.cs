﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorBusinessLogic
{
    public class ApplicationModuleBL
    {
        public static List<int> GetModulesExceptSysAdmin(DbContextBL context)
        {
            try
            {
                return (from am in context.AppModules where am.Key != (int)GMG.CoZone.Common.AppModule.SysAdmin select am.ID).ToList();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Could not get module id, error: " + ex.Message, ex);
                return null;
            }
        }

        public static int GetModuleKeyById(int moduleId, DbContextBL context)
        {
            try
            {
                return (from am in context.AppModules where am.ID == moduleId  select am.Key).FirstOrDefault();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Could not get module id, error: " + ex.Message, ex);
                return default(int);
            }
        }
    }
}
