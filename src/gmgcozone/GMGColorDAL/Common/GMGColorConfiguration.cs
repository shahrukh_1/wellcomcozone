using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

using GMGColor.AWS;

namespace GMGColorDAL
{
    public class GMGColorConfiguration
    {
        #region Contants

        const string CONFIGURATION_VALUE_LOCAL = "local";
        const string CONFIGURATION_VALUE_AMAZONE = "AWS";
        public const string CONVERTED = "Converted";
        public const string THUMBNAIL = "Thumbnail";
        public const string ORIGINALSWF = "OriginalSwfFile";
        public const string VIDEOTHUMBNAIL = "thumbnail_00002.gif";
        public const int DEMOBILLINGPLANACTIVEDAYS = 30;
        const string AWS_AccessKey = "AKIAR6KWPSILSIVWAX5Z";
        const string AWS_SecretKey = "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";
        const string AWS_SMTPUserName = "AKIAR6KWPSILWN7BABFV";
        const string AWS_SMTPPassword = "BHiVfLvSl26oyqchR6fOdCkfq9MjjyHyvd93mYfQQCaj";
        const string Cloud_Convert_API_V2_key = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiM2M2OWJkOWI3NjJjODFjOTJkNWUyNGI1OWI2ZTY1ZmJkMjdiMTY5ZGFlMGRmMjI1Mzk5ZTQ0MTQ5ZDMzMTRhNjZjODVkNjQ5OGZjYzAxNmYiLCJpYXQiOiIxNjE1ODE2MDA0LjQ3MzY0OCIsIm5iZiI6IjE2MTU4MTYwMDQuNDczNjUwIiwiZXhwIjoiNDc3MTQ4OTYwNC40MzMwNzQiLCJzdWIiOiIzODY4NDM5NCIsInNjb3BlcyI6WyJ1c2VyLnJlYWQiLCJ1c2VyLndyaXRlIiwidGFzay5yZWFkIiwidGFzay53cml0ZSIsIndlYmhvb2sucmVhZCIsIndlYmhvb2sud3JpdGUiLCJwcmVzZXQucmVhZCIsInByZXNldC53cml0ZSJdfQ.pOzhypp0kpcXrJMh6rmxC2yywMZShpIb-FAuwvjGPUl-3-hTYAH3X8d_-l6Yw9pV571A1_QnWeCbVTc6ca0kli5UOBsWKlKR8LVrAsel9cZWe0xK69finOHyETNEv9D0Nzl0XKXCk_wpMNs2XjJlMIz8sECp7nDGeb2vJCFAMddK9dZEP7NET5wIRkD3oGkFTWrIKPQOkjLR6sdaSbB29P2EW1p4O_Kz9rGpQget1HQNqvMFgXh9aM_BfelRShUEzFmZTytJSzfE6fDjidjl87ktFDNPVL3jbCExb8dU5UBp63U6mR51MvzPWvAOHPSZydVbYos030a8C70u2CZ5Yu1h4OiGR4XlXusXFwLOTfwpf8nvMcWXzVKwjW81-r1iIiVHaKnvrPJSseAiQgjUZaMCsL9gRitI6Nb6E4lMxGTe515z0sBlCI5yAuDTyOVh9OsVNXGi932aageq4CruyQrSMbHGoDCSnlt51jRzxjO_hS-ExL8lnHgdagAetEcUKUuWBb1C01efQg8_JtL-9fvss3S-P9u65XKR4GMD9u_3klLZ3FNT3VVU4_EFlK9o9u84Q8OweluH9vyz0Q_f5j8yRVFSkTnaCDloqY0POkOG-dJldFTd3YUO9pP1XIN0JOAo7mj_tFaAO5SlrHXAk2noZ4D5rSkXQgHFYNt_UKo";

        #endregion

        #region Fields

        private static string pairingCode = string.Empty;
        private static GMGColorConfiguration instance;
        private static ReaderWriterLock rwl = new ReaderWriterLock();
        /// <summary>
        /// Lock synchronization object
        /// </summary>
        private static object syncLock = new object();
        /// <summary>
        /// Site
        /// </summary>
        private static string siteDefaultLocale = string.Empty;
        private static string siteEnvironment = string.Empty;
        private static string restApiSecurityToken = string.Empty;
        private static string siteHostAddress = string.Empty;
        private static string siteIISSiteName = string.Empty;
        private static string siteReleaseDateTime = string.Empty;
        private static string serverProtocol = string.Empty;
        /// <summary>
        /// Deployment
        /// </summary>        
        private static bool? siteIsErrorLogging = null;
        private static bool? isHideDashboard = null;
        private static bool? isHideHelpCentre = null;
        private static bool? isHideFolderFunctionality = null;

        private static long approvalMaxFileSize = 0;
        private static int whiteLableMaxFileSize = 0;
        private static int approvalsMaxNrOfFiles = 0;
        private static int directoryCodeMaxValue = 0;
        private static int notificationEmailQueueIntervalInMinutes = 0;
        private static string siteConfiguration = string.Empty;
        private static int colorProofInstanceTimeoutSec = 0;
        private static int pendingXMLJobLifeTimeInMinutes = 0;
        private static int approvalsProcessingTimeoutInHrs = 0;
        /// <summary>
        /// Amazon
        /// </summary>
        private static bool? amazonS3BucketEnabled = null;
        private static string amazonAccessKey = string.Empty;
        private static string cloudconvertApikey = string.Empty;
        private static string amazonSecretKey = string.Empty;
        private static string amazonRegion = string.Empty;
        private static bool? amazonSQSEnabled = null;
        private static string amazonSNSTopicName = string.Empty;
        private static string amazonTranscodingVideoName = string.Empty;
        private static string amazonETSRoleARN = string.Empty;
        private static string amazonEC2ATPInstanceID = string.Empty;
        private static int amazonEC2ATPInstanceConnectionTimeoutInMinutes = 0;        
        private static string videoPipelineName = string.Empty;
        private static string videoTranscoderPresetName = string.Empty;
        private string transcoderRegion = string.Empty;
        private static string _amazonSQSBounceQueue = string.Empty;
        private static string _amazonSNSBounceTopic = string.Empty;
        /// <summary>
        /// Path
        /// </summary>
        private static string dataFolderName = string.Empty;
        private static string pathToDataFolder = string.Empty;
        private static string fileSystemPathToDataFolder = string.Empty;
        private static string pathToFileTranferTempFolder = string.Empty;
        private static string jobQueueName = string.Empty;
        private static string deliverJobQueueName = string.Empty;      
        private static string ftpJobQueueName = string.Empty;
        private static string spDataQueueName = string.Empty;
        private static string spCalibrationReadQueueName = string.Empty;
        private static string spCalibrationWriteQueueName = string.Empty;
        private static string simProfQueueName = string.Empty;
        private static string tileServerQueueName = string.Empty;
        private static string deliverJobCacheServerAddress = string.Empty;
        private static string inProgressMessageQueueName = string.Empty;
        private static string accountS3Region = string.Empty;
        private static string domainUrl = string.Empty;
        private static string ftpRootFolder = string.Empty;
        private static string xmlJobsTempFolder = string.Empty;
        private static string xmlJobsFailedFolder = string.Empty;
        private static string tempNotificationQueueName = string.Empty;
        /// <summary>
        /// Mail
        /// </summary>
        private static bool? isSendEmails = null;
        private static string mailServerType = string.Empty;
        private static string mailServer = string.Empty;
        private static string mailServerPort = string.Empty;
        private static string awsMailServer = string.Empty;
        private static string awsSmtpUserName = string.Empty;
        private static string awsSmtpPassword = string.Empty;
        private static string systemEmailFromAddress = string.Empty;
        private static string systemEmailFromName = string.Empty;
        private static string colorProofAdminName = string.Empty;
        private static string dayOfWeekForrWeeklyEmails = string.Empty;
        private static int utcHourForSendingDailyWeeklyEmails = 0;
        private static int utcHourForSuspendTrialAccountsTask = 0;
        private static int numberOfEmailsToSend = 0;
        private static string contactSupportEmail = string.Empty;
        private static int PsSessionTimeOut = 0;
        /// <summary>
        /// Media Processor
        /// </summary>
        private static string pathToJava = string.Empty;
        private static string pathToMagickTiler = string.Empty;
        private static string pathtoZencorderOutput = string.Empty;
        private static string pathToProcessingFolder = string.Empty;
        private static int webPageNrOftrays = 0;
        private static int webPageTimeout = 0;
        private static int tileServerNrOfThreads = 0;
        private static int mediaDefineNrOfThreads = 0;
        private static string browshotAccessKey = string.Empty;
        private static int browshotInstanceId = 0;
        private static string outputProfile = string.Empty;
        private static string inputProfile = string.Empty;
        private static string grayProfile = string.Empty;
        private static string monitorDesaturationProfile = string.Empty;
        private static int _defaultImageSize = 0;
        private static uint maxNrOfChannels = 0;
        private static bool? paparTintDefaultValue;
        private static int bandsCacheExpirationHours = 0;
        private static int restrictedZoomDPI = 0;

        /// <summary>
        /// Maintenance
        /// </summary>
        private static string dayOfWeekForWeeklyTasks = string.Empty;
        private static int softProofingExpirationTimeInDays = 0;
        private static int utcHourForRunningMaintenaceTasks = 0;
        private static int utcHourForRunningRecreateDBIndexesTasks = 0;
        private static int approvalsOverdueByNoOfDays = 0;
        private static int waitForProcessedWorkstationDataTimeInSeconds = 5;
		private static int _newApprovalTimeOutInMinutes = 0;

		/// <summary>
		/// Log
		/// </summary>
		private static int logLevel = -1;

        private static string namingPrefix = string.Empty;
        private static string metadataRequestUrl = "http://169.254.169.254/latest/meta-data/placement/availability-zone";

        private static string softProofingProfilesRepo = string.Empty;

        /// <summary>
        /// Mantainance
        /// </summary>
        private static int collabSessAllowedHours = 0;

        /// <summary>
        /// ColorProof 
        /// </summary>
        private static int utcHourForUpdatingInstancesList = 0;

        private static string win64SoftProofingAgentUrl = string.Empty;
        private static string mac64SoftProofingAgentUrl = string.Empty;

        #endregion

        #region Properties

        #region General

        public static GMGColorConfiguration AppConfiguration
        {
            get
            {
                // 'Double checked locking' pattern which (once
                // the instance exists) avoids locking each
                // time the method is invoked
                if (instance == null)
                {
                    lock (syncLock)
                    {
                        if (instance == null)
                        {
                            instance = new GMGColorConfiguration();
                        }
                    }
                }

                return instance;
            }
        }

        public static Thread GetContextAwareThread(ThreadStart s)
        {
            Thread t = new Thread(s);

            // Ensure the new Thread gets the same ownership settings as the CurrentThread
            rwl.AcquireWriterLock(Timeout.Infinite);
            try
            {
                //m_htOwnerCompanyID[t] = OwnerCompanyID;
                //m_htAccountID[t] = AccountID;
            }
            finally
            {
                rwl.ReleaseWriterLock();
            }

            return t;
        }

        #endregion

        #region Site settings

        public string DefaultLocale
        {
            get
            {
                if (String.IsNullOrEmpty(siteDefaultLocale))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultLocale"]))
                    {
                        siteDefaultLocale = ConfigurationManager.AppSettings["DefaultLocale"].ToString();
                    }
                    else
                    {
                        siteDefaultLocale = "en-AU";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DefaultLocale");
                    }
                }

                return siteDefaultLocale;
            }
        }

        [Obsolete]
        public string ServerProtocol
        {
            get
            {
                if (String.IsNullOrEmpty(serverProtocol))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ServerProtocol"]))
                    {
                        serverProtocol = ConfigurationManager.AppSettings["ServerProtocol"].ToString();
                    }
                    else
                    {
                        serverProtocol = "http";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ServerProtocol");
                    }
                }
                return serverProtocol;
            }
        }

        public string RestApiSecurityToken
        {
            get
            {
                if (String.IsNullOrEmpty(restApiSecurityToken))
                {
                    restApiSecurityToken = ConfigurationManager.AppSettings["RestApiSecurityToken"].ToString();
                }
                if (String.IsNullOrEmpty(restApiSecurityToken))
                {
                    restApiSecurityToken = System.Guid.NewGuid().ToString();
                    GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "RestApiSecurityToken");
                }
                return restApiSecurityToken;
            }
        }

        public string Environment
        {
            get
            {
                if (String.IsNullOrEmpty(siteEnvironment))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["Environment"]))
                    {
                        siteEnvironment = ConfigurationManager.AppSettings["Environment"].ToString();
                    }
                    else
                    {
                        siteEnvironment = "prod";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "Environment");
                    }
                }
                return siteEnvironment;
            }
        }

        public string HostAddress
        {
            get
            {
                if (String.IsNullOrEmpty(siteHostAddress))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["HostAddress"]))
                    {
                        siteHostAddress = ConfigurationManager.AppSettings["HostAddress"].ToString();
                    }
                    else
                    {
                        siteHostAddress = ".wellcomapproval.com";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "HostAddress");
                    }
                }
                return siteHostAddress;
            }
        }

        public string IISSiteName
        {
            get
            {
                if (String.IsNullOrEmpty(siteIISSiteName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IISSiteName"]))
                    {
                        siteIISSiteName = ConfigurationManager.AppSettings["IISSiteName"].ToString();
                    }
                    else
                    {
                        siteIISSiteName = "GMGCoZone";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "IISSiteName");
                    }
                }

                return siteIISSiteName;
            }
        }

        public string ReleasedDateTime
        {
            get
            {
                if (String.IsNullOrEmpty(siteReleaseDateTime))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ReleaseDateTime"]))
                    {
                        siteReleaseDateTime = ConfigurationManager.AppSettings["ReleaseDateTime"].ToString();
                        try
                        {
                            siteReleaseDateTime = DateTime.Parse(siteReleaseDateTime).ToString("ddd, d MMM yyyy hh:mm tt");
                        }
                        catch
                        {
                            siteReleaseDateTime = DateTime.Now.ToString("ddd, d MMM yyyy hh:mm tt");
                        }
                    }
                    else
                    {
                        siteReleaseDateTime = DateTime.Now.ToString("ddd, d MMM yyyy hh:mm tt");
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ReleaseDateTime");
                    }
                }

                return siteReleaseDateTime;
            }
        }

        #endregion

        #region Deployment settings

        public bool IsErrorLogging
        {
            get
            {
                if (siteIsErrorLogging == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsErrorLogging"]))
                    {
                        try
                        {
                            siteIsErrorLogging = Convert.ToBoolean(ConfigurationManager.AppSettings["IsErrorLogging"]);
                        }
                        catch
                        {
                            siteIsErrorLogging = true;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Error occured while converting the config value for {0}", "IsErrorLogging");
                        }
                    }
                    else
                    {
                        siteIsErrorLogging = true;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "IsErrorLogging");
                    }
                }

                return (bool)siteIsErrorLogging;
            }
        }

        public bool IsHideDashbordMenuItem
        {
            get
            {
                if (isHideDashboard == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsHideDashboardMenuItem"]))
                    {
                        try
                        {
                            isHideDashboard = Convert.ToBoolean(ConfigurationManager.AppSettings["IsHideDashboardMenuItem"]);
                        }
                        catch
                        {
                            isHideDashboard = false;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Error occured while converting the config value for {0}", "IsHideDashboardMenuItem");
                        }
                    }
                    else
                    {
                        isHideDashboard = false;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "IsHideDashboardMenuItem");
                    }
                }

                return (bool)isHideDashboard;
            }
        }

        public bool IsHideHelpCentreMenuItem
        {
            get
            {
                if (isHideHelpCentre == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsHideHelpCentreMenuItem"]))
                    {
                        try
                        {
                            isHideHelpCentre = Convert.ToBoolean(ConfigurationManager.AppSettings["IsHideHelpCentreMenuItem"]);
                        }
                        catch
                        {
                            isHideHelpCentre = false;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Error occured while converting the config value for {0}", "IsHideHelpCentreMenuItem");
                        }
                    }
                    else
                    {
                        isHideHelpCentre = false;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "IsHideHelpCentreMenuItem");
                    }
                }

                return (bool)isHideHelpCentre;
            }
        }

        public bool IsHideFolderFunctionality
        {
            get
            {
                if (isHideFolderFunctionality == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsHideFolderFunctionality"]))
                    {
                        try
                        {
                            isHideFolderFunctionality = Convert.ToBoolean(ConfigurationManager.AppSettings["IsHideFolderFunctionality"]);
                        }
                        catch
                        {
                            isHideFolderFunctionality = false;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Error occured while converting the config value for {0}", "IsHideFolderFunctionality");
                        }
                    }
                    else
                    {
                        isHideFolderFunctionality = false;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "IsHideFolderFunctionality");
                    }
                }

                return (bool)isHideFolderFunctionality;
            }
        }

        /// <summary>
        /// Default 100MB
        /// </summary>
        public long ApprovalMaxFileSize
        {
            get
            {
                if (approvalMaxFileSize == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ApprovalMaxFileSize"]))
                    {
                        approvalMaxFileSize = long.Parse(ConfigurationManager.AppSettings["ApprovalMaxFileSize"].ToString());
                    }
                    else
                    {
                        approvalMaxFileSize = (100 * 1024); //100MB
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalMaxFileSize");
                    }
                }

                return approvalMaxFileSize;
            }
        }

        /// <summary>
        /// Default 5MB
        /// </summary>
        public int WhiteLableMaxFileSize
        {
            get
            {
                if (whiteLableMaxFileSize == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["WhiteLableMaxFileSize"]))
                    {
                        whiteLableMaxFileSize = int.Parse(ConfigurationManager.AppSettings["WhiteLableMaxFileSize"].ToString());
                    }
                    else
                    {
                        whiteLableMaxFileSize = (5 * 1024); //5MB
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WhiteLableMaxFileSize");
                    }
                }

                return whiteLableMaxFileSize;
            }
        }

        /// <summary>
        /// Default 8 files
        /// </summary>
        public int ApprovalsMaxNrOfFiles
        {
            get
            {
                if (approvalsMaxNrOfFiles == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ApprovalJobsMaxNumberOfFiles"]))
                    {
                        approvalsMaxNrOfFiles = int.Parse(ConfigurationManager.AppSettings["ApprovalJobsMaxNumberOfFiles"].ToString());
                    }
                    else
                    {
                        approvalsMaxNrOfFiles = 8; 
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalJobsMaxNumberOfFiles");
                    }
                }

                return approvalsMaxNrOfFiles;
            }
        }

        public int DirectoryLocation
        {
            get
            {
                if (directoryCodeMaxValue == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DirectoryCodeMaxValue"]))
                    {
                        try
                        {
                            directoryCodeMaxValue = int.Parse(ConfigurationManager.AppSettings["DirectoryCodeMaxValue"].ToString().Trim());
                        }
                        catch
                        {
                            directoryCodeMaxValue = 100;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DirectoryCodeMaxValue");
                        }
                    }
                    else
                    {
                        directoryCodeMaxValue = 100;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DirectoryCodeMaxValue");
                    }
                }

                return directoryCodeMaxValue;
            }
        }

        public int NotificationEmailQueueIntervalInMinutes
        {
            get
            {
                if (notificationEmailQueueIntervalInMinutes == 0)
                {
                    int result;
                    if (ConfigurationManager.AppSettings["NotificationEmailQueueIntervalInMinutes"] != null &&
                        int.TryParse(ConfigurationManager.AppSettings["NotificationEmailQueueIntervalInMinutes"].ToString(), out result))
                    {
                        notificationEmailQueueIntervalInMinutes = result;
                    }
                    else
                    {
                        notificationEmailQueueIntervalInMinutes = (24 * 60); // Hours * Minuts
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve/convert the config value for {0}", "NotificationEmailQueueIntervalInMinutes");
                    }
                }

                return notificationEmailQueueIntervalInMinutes;
            }
        }

        public string ActiveConfiguration
        {
            get
            {
                if (String.IsNullOrEmpty(siteConfiguration))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ActiveConfiguration"]))
                    {
                        siteConfiguration = ConfigurationManager.AppSettings["ActiveConfiguration"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ActiveConfiguration");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ActiveConfiguration"));
                    }
                }

                return siteConfiguration;
            }
        }

        public string ColorProofPairingCodeRegion
        {
            get
            {
                if (String.IsNullOrEmpty(pairingCode))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ColorProofPairingCodeRegion"]))
                    {
                        pairingCode = ConfigurationManager.AppSettings["ColorProofPairingCodeRegion"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ColorProofPairingCodeRegion");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ColorProofPairingCodeRegion"));
                    }
                }

                return pairingCode;
            }
        }
        #endregion

        #region Amazon

        public string CloudConvertApiKey
        {
            get
            {
                if (String.IsNullOrEmpty(cloudconvertApikey))
                {
                    if (!String.IsNullOrEmpty(Cloud_Convert_API_V2_key))
                    {
                        cloudconvertApikey = Cloud_Convert_API_V2_key;
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "CloudConvertApiKey");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "CloudConvertApiKey"));
                    }
                }

                return cloudconvertApikey;
            }
        }

        public string AWSAccessKey
        {
            get
            {
                if (String.IsNullOrEmpty(amazonAccessKey))
                {
                    if (!String.IsNullOrEmpty(AWS_AccessKey))
                    {
                        amazonAccessKey = AWS_AccessKey;
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSAccessKey");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSAccessKey"));
                    }
                }

                return amazonAccessKey;
            }
        }

        public string AWSSecretKey
        {
            get
            {
                if (String.IsNullOrEmpty(amazonSecretKey))
                {
                    if (!String.IsNullOrEmpty(AWS_SecretKey))
                    {
                        amazonSecretKey = AWS_SecretKey;
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSecretKey");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSecretKey"));
                    }
                }

                return amazonSecretKey;
            }
        }

        public string AWSRegion
        {
            get
            {
                if (String.IsNullOrEmpty(amazonRegion))
                {
                    try
                    {
                        WebClient client = new WebClient();
                        Stream data = client.OpenRead(metadataRequestUrl);
                        StreamReader reader = new StreamReader(data);
                        string content = reader.ReadToEnd().Trim();
                        data.Close();
                        reader.Close();

                        Amazon.RegionEndpoint endpoint = Amazon.RegionEndpoint.EnumerableAllRegions.SingleOrDefault(o => content.StartsWith(o.SystemName));
                        if (endpoint != null)
                        {
                            amazonRegion = endpoint.SystemName;
                        }
                        else
                        {
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to resolve endpoint. metadata value : {0}", content);
                            throw new Exception(String.Format("GMGColorConfiguration class: Unable to resolve endpoint. metadata value : {0}", content));
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.ErrorFormat(ex, "GMGColorMediaConfiguration class: Unable to send the request to availability-zone. URL: {0}, Exception : {1}", metadataRequestUrl, ex.Message);
                        throw new Exception(String.Format("GMGColorMediaConfiguration class: Unable to send the request to availability-zone. URL: {0}, Exception : {1}", metadataRequestUrl, ex.Message));
                    }
                }

                return amazonRegion;
            }
        }

        public bool IsEnabledS3Bucket
        {
            get
            {
                if (amazonS3BucketEnabled == null)
                {
                    string dataStorageType = string.Empty;

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DataStorageType"]))
                    {
                        dataStorageType = ConfigurationManager.AppSettings["DataStorageType"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DataStorageType");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DataStorageType"));
                    }

                    amazonS3BucketEnabled = (dataStorageType == CONFIGURATION_VALUE_AMAZONE);
                }

                return (bool)amazonS3BucketEnabled;
            }
        }

		public int NewApprovalTimeOutInMinutes
		{
			get
			{
				if (_newApprovalTimeOutInMinutes == 0)
				{
					if (!Int32.TryParse(ConfigurationManager.AppSettings["NewApprovalTimeOutInMinutes"], out _newApprovalTimeOutInMinutes))
					{
						_newApprovalTimeOutInMinutes = 2;//The default value will be 2 in case the value from config is not set
					}
				}

				return _newApprovalTimeOutInMinutes;
			}
		}

        public int ColorProofInstanceTimeoutInSec
        {
            get
            {
                if (colorProofInstanceTimeoutSec == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["ColorProofInstanceTimeoutSec"], out colorProofInstanceTimeoutSec))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ColorProofInstanceTimeoutSec");
                        colorProofInstanceTimeoutSec = 120;
                    }
                }
                return colorProofInstanceTimeoutSec;
            }
        }

        public int PendingXMLJobLifeTimeInMinutes
        {
            get
            {
                if (pendingXMLJobLifeTimeInMinutes == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["PendingXMLJobLifeTimeInMinutes"], out pendingXMLJobLifeTimeInMinutes))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PendingXMLJobLifeTimeInMinutes");
                        pendingXMLJobLifeTimeInMinutes = 30;
                    }
                }
                return pendingXMLJobLifeTimeInMinutes;
            }
        }

        public int ApprovalsProcessingTimeoutInHrs
        {
            get
            {
                if (approvalsProcessingTimeoutInHrs == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["ApprovalsProcessingTimeoutInHrs"], out approvalsProcessingTimeoutInHrs))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalsProcessingTimeoutInHrs");
                        approvalsProcessingTimeoutInHrs = 12;
                    }
                }
                return approvalsProcessingTimeoutInHrs;
            }
        }

        public bool IsEnabledAmazonSQS
        {
            get
            {
                if (amazonSQSEnabled == null)
                {
                    string queueType = string.Empty;

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["QueueType"]))
                    {
                        queueType = ConfigurationManager.AppSettings["QueueType"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "QueueType");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "QueueType"));
                    }

                    amazonSQSEnabled = (queueType == CONFIGURATION_VALUE_AMAZONE);
                }

                return (bool)amazonSQSEnabled;
            }
        }

        public string AWSSNSTopicName
        {
            get
            {
                if (String.IsNullOrEmpty(amazonSNSTopicName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSSNSTopicName"]))
                    {
                        amazonSNSTopicName = ConfigurationManager.AppSettings["AWSSNSTopicName"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSTopicName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSTopicName"));
                    }
                }

                return amazonSNSTopicName;
            }
        }

        public string AWSTranscodedVideoTopicName
        {
            get
            {
                if (String.IsNullOrEmpty(amazonTranscodingVideoName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSSNSVideoTranscodingTopicName"]))
                    {
                        amazonTranscodingVideoName = ConfigurationManager.AppSettings["AWSSNSVideoTranscodingTopicName"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSVideoTranscodingTopicName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSVideoTranscodingTopicName"));
                    }
                }

                return amazonTranscodingVideoName;
            }
        }

        public string AWSETSRoleARN
        {
            get
            {
                if(String.IsNullOrEmpty(amazonETSRoleARN))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSVideoTranscoderRoleARN"]))
                    {
                        amazonETSRoleARN = ConfigurationManager.AppSettings["AWSVideoTranscoderRoleARN"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSVideoTranscoderRoleARN");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSVideoTranscoderRoleARN"));
                    }

                }
                return amazonETSRoleARN;
            }
        }

        public string DomainUrl
        {
            get
            {
                if (String.IsNullOrEmpty(domainUrl))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DomainUrl"]))
                    {
                        domainUrl = ConfigurationManager.AppSettings["DomainUrl"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DomainUrl");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DomainUrl"));
                    }
                }

                return domainUrl;
            }
        }

        public string ApprovalJobQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    jobQueueName = this.ActiveConfiguration + "-define-queue";
                }
                else if (String.IsNullOrEmpty(jobQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ApprovalQueueName"]))
                    {
                        jobQueueName = ConfigurationManager.AppSettings["ApprovalQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalQueueName"));
                    }
                }

                return jobQueueName;
            }
        }

        public string TempNotificationQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    tempNotificationQueueName = this.ActiveConfiguration + "-notification-queue";
                }
                else if (String.IsNullOrEmpty(tempNotificationQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["TempNotificationQueueName"]))
                    {
                        tempNotificationQueueName = ConfigurationManager.AppSettings["TempNotificationQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "TempNotificationQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "TempNotificationQueueName"));
                    }
                }

                return tempNotificationQueueName;
            }
        }

        public string DeliverJobQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    deliverJobQueueName = this.ActiveConfiguration + "-deliver-queue";
                }
                else if (String.IsNullOrEmpty(deliverJobQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DeliverQueueName"]))
                    {
                        deliverJobQueueName = ConfigurationManager.AppSettings["DeliverQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DeliverQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DeliverQueueName"));
                    }
                }

                return deliverJobQueueName;
            }
        }       

        public string FTPJobQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    ftpJobQueueName = this.ActiveConfiguration + "-ftp-queue";
                }
                else if (String.IsNullOrEmpty(ftpJobQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FTPQueueName"]))
                    {
                        ftpJobQueueName = ConfigurationManager.AppSettings["FTPQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FTPQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FTPQueueName"));
                    }
                }

                return ftpJobQueueName;
            }
        }

        public string SoftProofingQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    spDataQueueName = ActiveConfiguration + "-softproofing-queue";
                }
                else if (String.IsNullOrEmpty(spDataQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SoftProofingQueueName"]))
                    {
                        spDataQueueName = ConfigurationManager.AppSettings["SoftProofingQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingQueueName"));
                    }
                }

                return spDataQueueName;
            }
        }

        public string SoftProofingCalibrationReadQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    spCalibrationReadQueueName = ActiveConfiguration + "-softproofingcalibrationread-queue";
                }
                else if (String.IsNullOrEmpty(spCalibrationReadQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SoftProofingCalibrationReadQueueName"]))
                    {
                        spCalibrationReadQueueName = ConfigurationManager.AppSettings["SoftProofingCalibrationReadQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingCalibrationReadQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingCalibrationReadQueueName"));
                    }
                }

                return spCalibrationReadQueueName;
            }
        }

        public string SoftProofingCalibrationWriteQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    spCalibrationWriteQueueName = ActiveConfiguration + "-softproofingcalibrationwrite-queue";
                }
                else if (String.IsNullOrEmpty(spCalibrationWriteQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SoftProofingCalibrationWriteQueueName"]))
                    {
                        spCalibrationWriteQueueName = ConfigurationManager.AppSettings["SoftProofingCalibrationWriteQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingCalibrationWriteQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingCalibrationWriteQueueName"));
                    }
                }

                return spCalibrationWriteQueueName;
            }
        }


        public string TileServerQueueName
        {
            get
            {
                if (IsEnabledAmazonSQS && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
                {
                    tileServerQueueName = this.ActiveConfiguration + "-tileserver-queue";
                }
                else if (String.IsNullOrEmpty(tileServerQueueName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["TileServerQueueName"]))
                    {
                        tileServerQueueName = ConfigurationManager.AppSettings["TileServerQueueName"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "TileServerQueueName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "TileServerQueueName"));
                    }
                }

                return tileServerQueueName;
            }
        }

        public string FTPRootFolder
        {
            get
            {
                if (String.IsNullOrEmpty(ftpRootFolder))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FTPRootFolder"]))
                    {
                        ftpRootFolder = ConfigurationManager.AppSettings["FTPRootFolder"].ToString().Trim();
                    }
                    else if (String.IsNullOrEmpty(ftpRootFolder))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FTPRootFolder");
                        throw new Exception( String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FTPRootFolder"));
                    }
                }

                return ftpRootFolder;
            }
        }

        public string XMLJobsTempFolder
        {
            get
            {
                if (String.IsNullOrEmpty(xmlJobsTempFolder))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["JobsTempFolder"]))
                    {
                        xmlJobsTempFolder = ConfigurationManager.AppSettings["JobsTempFolder"].ToString().Trim();
                    }
                    else if (String.IsNullOrEmpty(xmlJobsTempFolder))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "JobsTempFolder");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "JobsTempFolder"));
                    }
                }

                return xmlJobsTempFolder;
            }
        }

        public string XMLJobsFailedFolder
        {
            get
            {
                if (String.IsNullOrEmpty(xmlJobsFailedFolder))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FailedJobsFolder"]))
                    {
                        xmlJobsFailedFolder = ConfigurationManager.AppSettings["FailedJobsFolder"].ToString().Trim();
                    }
                    else if (String.IsNullOrEmpty(xmlJobsFailedFolder))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FailedJobsFolder");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FailedJobsFolder"));
                    }
                }

                return xmlJobsFailedFolder;
            }
        }

        public string DeliverJobCacheServerAddress
        {
            get
            {
                if (String.IsNullOrEmpty(deliverJobCacheServerAddress))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DeliverJobCacheServer"]))
                    {
                        deliverJobCacheServerAddress = ConfigurationManager.AppSettings["DeliverJobCacheServer"].ToString().Trim();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DeliverJobCacheServer");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DeliverJobCacheServer"));
                    }
                }

                return deliverJobCacheServerAddress; 
            }
        }

        public string VideoPipelineName
        {
            get
            {
                if (String.IsNullOrEmpty(videoPipelineName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["VideoPipelineName"]))
                    {
                        videoPipelineName = ConfigurationManager.AppSettings["VideoPipelineName"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "VideoPipelineName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "VideoPipelineName"));
                    }
                }

                return videoPipelineName;
            }
        }

        public string VideoTranscoderPresetName
        {
            get
            {
                if (String.IsNullOrEmpty(videoTranscoderPresetName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["VideoTranscoderPresetName"]))
                    {
                        videoTranscoderPresetName = ConfigurationManager.AppSettings["VideoTranscoderPresetName"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "VideoTranscoderPresetName");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "VideoTranscoderPresetName"));
                    }
                }

                return videoTranscoderPresetName;
            }
        }

        public string VideoTranscoderAWSRegion
        {
            get
            {
                if (String.IsNullOrEmpty(transcoderRegion))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["VideoTranscoderAWSRegion"]))
                    {
                        transcoderRegion = ConfigurationManager.AppSettings["VideoTranscoderAWSRegion"];
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "VideoTranscoderAWSRegion");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "VideoTranscoderAWSRegion"));
                    }
                }

                return transcoderRegion;
            }
        }

        public string AWSSQSBounceQueue
        {
            get
            {
                if (String.IsNullOrEmpty(_amazonSQSBounceQueue))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ActiveConfiguration"]))
                    {
                        _amazonSQSBounceQueue = ConfigurationManager.AppSettings["ActiveConfiguration"] + "-bounces-queue";
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSQSBounceQueue");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSQSBounceQueue"));
                    }
                }

                return _amazonSQSBounceQueue;
            }
        }

        public string AWSSNSBounceTopic
        {
            get
            {
                if (String.IsNullOrEmpty(_amazonSNSBounceTopic))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSSNSBounceTopic"]))
                    {
                        _amazonSNSBounceTopic = ConfigurationManager.AppSettings["AWSSNSBounceTopic"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSBounceTopic");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSNSBounceTopic"));
                    }
                }

                return _amazonSNSBounceTopic;
            }
        }

        #endregion

        #region Application Settings

        public string DataFolderPrefix
        {
            get
            {
                if (String.IsNullOrEmpty(namingPrefix))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DataFolderPrefix"]))
                    {
                        namingPrefix = ConfigurationManager.AppSettings["DataFolderPrefix"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DataFolderPrefix");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DataFolderPrefix"));
                    }
                }

                return namingPrefix;
            }
        }

        public string DataFolderName(string AccountRegion)
        {
            if (IsEnabledS3Bucket && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
            {
                dataFolderName = DataFolderPrefix + "-" + ActiveConfiguration + "-data-" + AccountRegion;
            }
            else if (String.IsNullOrEmpty(dataFolderName))
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemDataFolderName"]))
                {
                    dataFolderName = ConfigurationManager.AppSettings["FileSystemDataFolderName"].ToString().Trim();
                }
                else
                {
                    GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemDataFolderName");
                    throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemDataFolderName"));
                }
            }

            return dataFolderName;
        }

        public string HtmlDataFolderName(string AccountRegion)
        {
            if (IsEnabledS3Bucket && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
            {
                dataFolderName = DataFolderPrefix + "-" + ActiveConfiguration + "-htmldata-" + AccountRegion;
            }
            else if (String.IsNullOrEmpty(dataFolderName))
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemDataFolderName"]))
                {
                    dataFolderName = ConfigurationManager.AppSettings["FileSystemDataFolderName"].ToString().Trim();
                }
                else
                {
                    GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemDataFolderName");
                    throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemDataFolderName"));
                }
            }

            return dataFolderName;
        }

        public string PathToDataFolder(string AccountRegion)
        {
            if (IsEnabledS3Bucket && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
            {
                // Special case for US Standard *
                if (this.AWSRegion == "us-east-1")
                {
                    pathToDataFolder = "https://" + DataFolderName(AccountRegion) + ".s3.amazonaws.com/";
                }
                else
                {
                    pathToDataFolder = "https://" + DataFolderName(AccountRegion) + ".s3-" + AccountRegion + ".amazonaws.com/";
                }
            }
            else if (String.IsNullOrEmpty(pathToDataFolder))
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemPathToDataFolder"]))
                {
                    pathToDataFolder = ConfigurationManager.AppSettings["FileSystemPathToDataFolder"].ToString().Trim();
                }
                else
                {
                    GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder");
                    throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder"));
                }
            }

            return pathToDataFolder;
        }

        public string PathToHtmlDataFolder(string AccountRegion)
        {
            if (IsEnabledS3Bucket && (ActiveConfiguration != CONFIGURATION_VALUE_LOCAL))
            {
                // Special case for US Standard *
                if (this.AWSRegion == "us-east-1")
                {
                    pathToDataFolder = "https://" + HtmlDataFolderName(AccountRegion) + ".s3.amazonaws.com/";
                }
                else
                {
                    pathToDataFolder = "https://" + HtmlDataFolderName(AccountRegion) + ".s3-" + AccountRegion + ".amazonaws.com/";
                }
            }
            else if (String.IsNullOrEmpty(pathToDataFolder))
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemPathToDataFolder"]))
                {
                    pathToDataFolder = ConfigurationManager.AppSettings["FileSystemPathToDataFolder"].ToString().Trim();
                }
                else
                {
                    GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder");
                    throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder"));
                }
            }

            return pathToDataFolder;
        }

        public string FileSystemPathToDataFolder
        {
            get
            {
                if (String.IsNullOrEmpty(fileSystemPathToDataFolder))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["FileSystemPathToDataFolder"]))
                    {
                        fileSystemPathToDataFolder = ConfigurationManager.AppSettings["FileSystemPathToDataFolder"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "FileSystemPathToDataFolder"));
                    }
                }

                return fileSystemPathToDataFolder;
            }
        }

        public static string RemoteProfileRepoURI
        {
            get
            {
                return
                    GMGColorConfiguration.AppConfiguration.PathToDataFolder(
                    (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.AWSRegion : "")) +
                    GMGColorConfiguration.AppConfiguration.SoftProofingProfilesRepo + @"/";
            }
        }

        public string WIN64SoftProofingAgentUrl
        {
            get
            {
                if (String.IsNullOrEmpty(win64SoftProofingAgentUrl))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["WIN64SoftProofingAgentUrl"]))
                    {
                        win64SoftProofingAgentUrl = ConfigurationManager.AppSettings["WIN64SoftProofingAgentUrl"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WIN64SoftProofingAgentUrl");
                    }
                }

                return win64SoftProofingAgentUrl;
            }
        }

        public string MAC64SoftProofingAgentUrl
        {
            get
            {
                if (String.IsNullOrEmpty(mac64SoftProofingAgentUrl))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MAC64SoftProofingAgentUrl"]))
                    {
                        mac64SoftProofingAgentUrl = ConfigurationManager.AppSettings["MAC64SoftProofingAgentUrl"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MAC64SoftProofingAgentUrl");
                    }
                }

                return mac64SoftProofingAgentUrl;
            }
        }

        #endregion

        #region Mail settings

        public string ContactSupportEmail
        {
            get
            {
                if (String.IsNullOrEmpty(contactSupportEmail))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ContactSupportEmail"]))
                    {
                        contactSupportEmail = ConfigurationManager.AppSettings["ContactSupportEmail"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ContactSupportEmail");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ContactSupportEmail"));
                    }
                }

                return contactSupportEmail;
            }
        }

        public bool IsSendEmails
        {
            get
            {
                if (isSendEmails == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["IsSendEmails"]))
                    {
                        try
                        {
                            isSendEmails = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSendEmails"]);
                        }
                        catch
                        {
                            isSendEmails = true;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Error occured while converting the config value for {0}", "IsSendEmails");
                        }
                    }
                    else
                    {
                        isSendEmails = true;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "IsSendEmails");
                    }
                }

                return (bool)isSendEmails;
            }
        }

        public string MailServerType
        {
            get
            {
                if (String.IsNullOrEmpty(mailServerType))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MailServerType"]))
                    {
                        mailServerType = ConfigurationManager.AppSettings["MailServerType"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServerType");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServerType"));
                    }
                }

                return mailServerType;
            }
        }

        public string MailServerPort
        {
            get
            {
                if (String.IsNullOrEmpty(mailServerPort))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MailServerPort"]))
                    {
                        mailServerPort = ConfigurationManager.AppSettings["MailServerPort"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServerPort");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServerPort"));
                    }
                }

                return mailServerPort;
            }
        }

        public string MailServer
        {
            get
            {
                if (String.IsNullOrEmpty(mailServer))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MailServer"]))
                    {
                        mailServer = ConfigurationManager.AppSettings["MailServer"].ToString();
                    }
                    else
                    {
                        mailServer = "localhost";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MailServer");
                    }
                }

                return mailServer;
            }
        }

        public string AWSMailServer
        {
            get
            {
                if (String.IsNullOrEmpty(awsMailServer))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSMailServer"]))
                    {
                        awsMailServer = ConfigurationManager.AppSettings["AWSMailServer"].ToString();
                    }
                    else
                    {
                        awsMailServer = "TOFILLAWSMailServer";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSMailServer");
                    }
                }

                return awsMailServer;
            }
        }

        public string AWSSMTPUserName
        {
            get
            {
                if (String.IsNullOrEmpty(awsSmtpUserName))
                {
                    if (!String.IsNullOrEmpty(AWS_SMTPUserName))
                    {
                        awsSmtpUserName = AWS_SMTPUserName;
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSMTPUserName");
                    }
                }

                return awsSmtpUserName;
            }
        }

        public string AWSSMTPPassword
        {
            get
            {
                if (String.IsNullOrEmpty(awsSmtpPassword))
                {
                    if (!String.IsNullOrEmpty(AWS_SMTPPassword))
                    {
                        awsSmtpPassword = AWS_SMTPPassword;
                    }
                    else
                    {
                        awsSmtpPassword = "AiWi6lc6s4BpHivXqfMa4mgn4Na1RZfw1wd4Ox/6M3Ml";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "AWSSMTPPassword");
                    }
                }

                return awsSmtpPassword;
            }
        }

        public string EmailSystemFromAddress
        {
            get
            {
                if (String.IsNullOrEmpty(systemEmailFromAddress))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SystemEmailFromAddress"]))
                    {
                        systemEmailFromAddress = ConfigurationManager.AppSettings["SystemEmailFromAddress"].ToString();
                    }
                    else
                    {
                        systemEmailFromAddress = "helpdesk@wellcomww.co.uk";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SystemEmailFromAddress");
                    }
                }

                return systemEmailFromAddress;
            }
        }

        public string EmailSystemFromName
        {
            get
            {
                if (String.IsNullOrEmpty(systemEmailFromName))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SystemEmailFromName"]))
                    {
                        systemEmailFromName = ConfigurationManager.AppSettings["SystemEmailFromName"].ToString();
                    }
                    else
                    {
                        systemEmailFromName = "Collaborate Administrator";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SystemEmailFromName");
                    }
                }

                return systemEmailFromName;
            }
        }

        public string DayOfWeekForrWeeklyEmails
        {
            get
            {
                if (String.IsNullOrEmpty(dayOfWeekForrWeeklyEmails))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DayOfWeekForrWeeklyEmails"]))
                    {
                        dayOfWeekForrWeeklyEmails = ConfigurationManager.AppSettings["DayOfWeekForrWeeklyEmails"].ToString();
                    }
                    else
                    {
                        dayOfWeekForrWeeklyEmails = "Sunday";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DayOfWeekForrWeeklyEmails");
                    }
                }

                return dayOfWeekForrWeeklyEmails;
            }
        }

        public int UTCHourForSendingDailyWeeklyEmails
        {
            get
            {
                if (utcHourForSendingDailyWeeklyEmails == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["UTCHourForSendingDailyWeeklyEmails"], out utcHourForSendingDailyWeeklyEmails))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "UTCHourForSendingDailyWeeklyEmails");
                        utcHourForSendingDailyWeeklyEmails = 7;
                    }
                }
                return utcHourForSendingDailyWeeklyEmails;
            }
        }

        public int UTCHourForUpdatingInstancesList
        {
            get
            {
                if (utcHourForUpdatingInstancesList == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["UTCHourForUpdatingInstancesList"], out utcHourForUpdatingInstancesList))
                    {
                        utcHourForUpdatingInstancesList = 0;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "UTCHourForUpdatingInstancesList");
                    }
                }
                return utcHourForUpdatingInstancesList;
            }
        }

        public int NumberOfEmailsToSend
        {
            get
            {
                if (numberOfEmailsToSend == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["NumberOfEmailsToSend"], out numberOfEmailsToSend))
                    {
                        numberOfEmailsToSend = 20;
                    }
                }
                return numberOfEmailsToSend;
            }
        }

        #endregion

        #region Media Processor

        public string OutputProfile
        {
            get
            {
                if (String.IsNullOrEmpty(outputProfile))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["OutputProfile"]))
                    {
                        outputProfile = ConfigurationManager.AppSettings["OutputProfile"].ToString();
                    }
                    else
                    {
                        outputProfile = "AdobeRGB1998.icc";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "OutputProfile");
                    }
                }

                return outputProfile;
            }
        }

        public string InputProfile
        {
            get
            {
                if (String.IsNullOrEmpty(inputProfile))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["InputProfile"]))
                    {
                        inputProfile = ConfigurationManager.AppSettings["InputProfile"].ToString();
                    }
                    else
                    {
                        inputProfile = "ISOcoated_v2_eci.icc";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "InputProfile");
                    }
                }
                return inputProfile;
            }
        }

        public string GrayProfile
        {
            get
            {
                if (String.IsNullOrEmpty(grayProfile))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["GrayProfile"]))
                    {
                        grayProfile = ConfigurationManager.AppSettings["GrayProfile"].ToString();
                    }
                    else
                    {
                        grayProfile = "ISOcoated_v2_eci.icc";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "GrayProfile");
                    }
                }
                return grayProfile;
            }
        }

        public int ProofStudioSessionTimeOut
        {
            get
            {
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["ProofStudioSessionTimeoutInDays"]))
                {
                    PsSessionTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["ProofStudioSessionTimeoutInDays"]);
                }
                return PsSessionTimeOut;
            }
        }

        public string MonitorDesaturationProfile
        {
            get
            {
                if (String.IsNullOrEmpty(monitorDesaturationProfile))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MonitorDesaturationProfile"]))
                    {
                        monitorDesaturationProfile = ConfigurationManager.AppSettings["MonitorDesaturationProfile"].ToString();
                    }
                    else
                    {
                        monitorDesaturationProfile = "Lab-to-Lab_Desaturation5.icc";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MonitorDesaturationProfile");
                    }
                }
                return monitorDesaturationProfile;
            }
        }

        public string JavaPath
        {
            get
            {
                if (String.IsNullOrEmpty(pathToJava))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PathToJava"]))
                    {
                        pathToJava = ConfigurationManager.AppSettings["PathToJava"].ToString();
                    }
                    else
                    {
                        pathToJava = @"C:\Program Files\Java\jre7\bin\Java.exe";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PathToJava");
                    }
                }
                return pathToJava;
            }
        }

        public string ZoomifyPath
        {
            get
            {
                if (String.IsNullOrEmpty(pathToMagickTiler))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PathToZoomify"]))
                    {
                        pathToMagickTiler = ConfigurationManager.AppSettings["PathToZoomify"].ToString();
                    }
                    else
                    {
                        pathToMagickTiler = @"C:\Projects\GMGCoZone\src\gmgcozone\GMGColorMediaDefine\Zoomify.exe";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PathToZoomify");
                    }
                }
                return pathToMagickTiler;
            }
        }

        public string PathToProcessingFolder
        {
            get
            {
                if (String.IsNullOrEmpty(pathToProcessingFolder))
                {

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PathToProcessingFolder"]))
                    {
                        pathToProcessingFolder = ConfigurationManager.AppSettings["PathToProcessingFolder"].ToString().Trim();
                    }
                    else
                    {
                        pathToProcessingFolder = @"C:\Temp\";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PathToProcessingFolder");
                    }
                }

                return pathToProcessingFolder;
            }
        }


        public string PathToFileTranferTempFolder
        {
            get
            {
                if (String.IsNullOrEmpty(pathToFileTranferTempFolder))
                {

                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PathToFileTranferTempFolder"]))
                    {
                        pathToFileTranferTempFolder = ConfigurationManager.AppSettings["PathToFileTranferTempFolder"].ToString().Trim();
                    }
                    else
                    {
                        pathToFileTranferTempFolder = @"C:\Temp\";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PathToFileTranferTempFolder");
                    }
                }

                return pathToFileTranferTempFolder;
            }
        }



        public int WebPageNrOfTries
        {
            get
            {
                if (webPageNrOftrays == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["WebPageNrOfTries"]))
                    {
                        webPageNrOftrays = Convert.ToInt32(ConfigurationManager.AppSettings["WebPageNrOfTries"]);
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WebPageNrOfTries");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WebPageNrOfTries"));
                    }
                }

                return webPageNrOftrays;
            }
        }

        public int TileServerNrOfThreads
        {
            get
            {
                if (tileServerNrOfThreads == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["TileServerNrOfThreads"]))
                    {
                        tileServerNrOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["TileServerNrOfThreads"]);
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "TileServerNrOfThreads");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "TileServerNrOfThreads"));
                    }
                }

                return tileServerNrOfThreads;
            }
        }

        public int MediaDefineNrOfThreads
        {
            get
            {
                if (mediaDefineNrOfThreads == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MediaDefineNrOfThreads"]))
                    {
                        mediaDefineNrOfThreads = Convert.ToInt32(ConfigurationManager.AppSettings["MediaDefineNrOfThreads"]);
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MediaDefineNrOfThreads");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MediaDefineNrOfThreads"));
                    }
                }

                return mediaDefineNrOfThreads;
            }
        }

        public int WebPageTimeout
        {
            get
            {
                if (webPageTimeout == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["WebPageTimeout"]))
                    {
                        webPageTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["WebPageTimeout"]);
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WebPageTimeout");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WebPageTimeout"));
                    }
                }

                return webPageTimeout;
            }
        }

        public string BrowshotAccessKey
        {
            get
            {
                if (String.IsNullOrEmpty(browshotAccessKey))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["BrowshotAccessKey"]))
                    {
                        browshotAccessKey = ConfigurationManager.AppSettings["BrowshotAccessKey"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "BrowshotAccessKey");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "BrowshotAccessKey"));
                    }
                }
                return browshotAccessKey;
            }
        }

        public int BrowshotInstanceID
        {
            get
            {
                if (browshotInstanceId == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["BrowshotInstanceID"]))
                    {
                        browshotInstanceId = Convert.ToInt32(ConfigurationManager.AppSettings["BrowshotInstanceID"]);
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "BrowshotInstanceID");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "BrowshotInstanceID"));
                    }
                }

                return browshotInstanceId;
            }
        }

        public string SoftProofingProfilesRepo
        {
            get
            {
                if (String.IsNullOrEmpty(softProofingProfilesRepo))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["SoftProofingProfilesRepo"]))
                    {
                        softProofingProfilesRepo = ConfigurationManager.AppSettings["SoftProofingProfilesRepo"].ToString();
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingProfilesRepo");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingProfilesRepo"));
                    }
                }

                return softProofingProfilesRepo;
            }
        }

        public int DefaultImageSize
        {
            get
            {
                if (_defaultImageSize == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultImageSize"]))
                    {
                        _defaultImageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultImageSize"]);
                    }
                    else
                    {
                        _defaultImageSize = 7000;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DefaultImageSize");
                    }
                }

                return _defaultImageSize;
            }
        }

        public int BandsCacheExpirationHours
        {
            get
            {
                if (bandsCacheExpirationHours == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["BandsCacheExpirationHours"]))
                    {
                        bandsCacheExpirationHours = Convert.ToInt32(ConfigurationManager.AppSettings["BandsCacheExpirationHours"]);
                    }
                    else
                    {
                        bandsCacheExpirationHours = 4;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "BandsCacheExpirationHours");
                    }
                }

                return bandsCacheExpirationHours;
            }
        }

        public int RestrictedZoomDPI
        {
            get
            {
                if (restrictedZoomDPI == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["RestrictedZoomDPI"]))
                    {
                        restrictedZoomDPI = Convert.ToInt32(ConfigurationManager.AppSettings["RestrictedZoomDPI"]);
                    }
                    else
                    {
                        restrictedZoomDPI = 72;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "RestrictedZoomDPI");
                    }
                }

                return restrictedZoomDPI;
            }
        }

        public uint MaxNrOfChannels
        {
            get
            {
                if (maxNrOfChannels == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["MaxNrOfChannels"]))
                    {
                        maxNrOfChannels = Convert.ToUInt32(ConfigurationManager.AppSettings["MaxNrOfChannels"]);
                    }
                    else
                    {
                        maxNrOfChannels = 12;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "MaxNrOfChannels");
                    }
                }

                return maxNrOfChannels;
            }
        }

        public bool PaperTintDefaultValue
        {
            get
            {
                if (paparTintDefaultValue == null)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["PaperTintSimulationDefault"]))
                    {
                        try
                        {
                            paparTintDefaultValue = Convert.ToBoolean(ConfigurationManager.AppSettings["PaperTintSimulationDefault"]);
                        }
                        catch
                        {
                            paparTintDefaultValue = true;
                            GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Error occured while converting the config value for {0}", "PaperTintSimulationDefault");
                        }
                    }
                    else
                    {
                        paparTintDefaultValue = true;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "PaperTintSimulationDefault");
                    }
                }

                return (bool)paparTintDefaultValue;
            }
        }

        public int CollaborateSessionValidNrOfHrs
        {
            get
            {
                if (collabSessAllowedHours == 0)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["CollaborateSessionValidNrOfHrs"]))
                    {
                        collabSessAllowedHours = Convert.ToInt32(ConfigurationManager.AppSettings["CollaborateSessionValidNrOfHrs"]);
                    }
                    else
                    {
                        collabSessAllowedHours = 2;
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "CollaborateSessionValidNrOfHrs");
                    }
                }

                return collabSessAllowedHours;
            }
        }

        #endregion

        #region LogLevel

        public int LogLevel
        {
            get
            {
                if (logLevel == -1)
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["LogLevel"]))
                    {
                        logLevel = Convert.ToInt32(ConfigurationManager.AppSettings["LogLevel"]);
                    }
                    else
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "LogLevel");
                        throw new Exception(String.Format("GMGColorConfiguration class: Unable to retrieve the config value for {0}", "LogLevel"));
                    }
                }

                return logLevel;
            }
        }
        #endregion

        #region Maintenance

        public string DayOfWeekForWeeklyTasks
        {
            get
            {
                if (String.IsNullOrEmpty(dayOfWeekForWeeklyTasks))
                {
                    if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["DayOfWeekForWeeklyTasks"]))
                    {
                        dayOfWeekForWeeklyTasks = ConfigurationManager.AppSettings["DayOfWeekForWeeklyTasks"].ToString();
                    }
                    else
                    {
                        systemEmailFromName = "Saturday";
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "DayOfWeekForWeeklyTasks");
                    }
                }

                return dayOfWeekForWeeklyTasks;
            }
        }

        public int UTCHourForRunningMaintenaceTasks
        {
            get
            {
                if (utcHourForRunningMaintenaceTasks == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["UTCHourForRunningMaintenaceTasks"], out utcHourForRunningMaintenaceTasks))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "UTCHourForRunningMaintenaceTasks");
                        utcHourForRunningMaintenaceTasks = 4;
                    }
                }
                return utcHourForRunningMaintenaceTasks;
            }
        }

        public int UTCHourForRunningRecreateDBIndexesTasks
        {
            get
            {
                if (utcHourForRunningRecreateDBIndexesTasks == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["UTCHourForRunningRecreateDBIndexesTasks"], out utcHourForRunningRecreateDBIndexesTasks))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "UTCHourForRunningRecreateDBIndexesTasks");
                        utcHourForRunningRecreateDBIndexesTasks = 3;
                    }
                }
                return utcHourForRunningRecreateDBIndexesTasks;
            }
        }

        public int UTCHourForSuspendTrialAccountsTask
        {
            get
            {
                if (utcHourForSuspendTrialAccountsTask == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["UTCHourForSuspendTrialAccountsTask"], out utcHourForSuspendTrialAccountsTask))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "UTCHourForSuspendTrialAccountsTask");
                        utcHourForSuspendTrialAccountsTask = 5;
                    }
                }
                return utcHourForSuspendTrialAccountsTask;
            }
        }

        public int SoftProofingExpirationTimeInDays 
        {
            get 
            {
                if (softProofingExpirationTimeInDays == 0)
                {
                    if(!Int32.TryParse(ConfigurationManager.AppSettings["SoftProofingExpirationTimeInDays"], out softProofingExpirationTimeInDays))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "SoftProofingExpirationTimeInDays");
                        softProofingExpirationTimeInDays = 30;
                    }
                }
                return softProofingExpirationTimeInDays;
            } 
        }

        public int ApprovalsOverdueByNoOfDays
        {
            get
            {
                if (approvalsOverdueByNoOfDays == 0)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["ApprovalsOverdueByNoOfDays"], out approvalsOverdueByNoOfDays))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "ApprovalsOverdueByNoOfDays");
                        approvalsOverdueByNoOfDays = 1;
                    }
                }
                return approvalsOverdueByNoOfDays;
            }
        }

        public int WaitForProcessedWorkstationDataTimeInSeconds
        {
            get
            {
                if (waitForProcessedWorkstationDataTimeInSeconds == 5)
                {
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["WaitForProcessedWorkstationDataTimeInSeconds"], out waitForProcessedWorkstationDataTimeInSeconds))
                    {
                        GMGColorLogging.log.ErrorFormat(null, "GMGColorConfiguration class: Unable to retrieve the config value for {0}", "WaitForProcessedWorkstationDataTimeInSeconds");
                        waitForProcessedWorkstationDataTimeInSeconds = 1;
                    }
                }
                return waitForProcessedWorkstationDataTimeInSeconds;
            }
        }

    #endregion

        public string CertificateFolderRelativePath
        {
            get { return "certificate"; }
        }

        public string AccountsFolderRelativePath
        {
            get { return "accounts";  }
        }

        public string AccountsGlobalFolderRelativePath
        {
            get { return "accounts-global"; }
        }

        public string ApprovalFolderRelativePath
        {
            get { return "approvals";  }
            // "approvals/"
        }

        public string FileTransferFolderRelativePath
        {
            get { return "filetransfer"; }
            // "filetransfer/"
        }

        public string DeliverFolderRelativePath
        {
            get { return "deliver"; }
            // "deliver/"
        }      

        public string CMSFolderRelativePath
        {
            get { return "cms"; }
        }

        public string CMSManualsFolderRelativePath
        {
            get { return "manuals"; }
        }

        public string CMSDownloadsFolderRelativePath
        {
            get { return "downloads"; }
        }

        public string SoftProofDataFolderRelativePath
        {
            get { return "softproofdata"; }
        }

        public string EmailTempFolderRelativePath
        {
            get { return "email-temp"; }
        }

        public string CustomICCProfileRelativePath
        {
            get
            {
                return "customICCprofiles";
            }
        }

        public string SimulationProfilesRelativePath
        {
            get { return "simulationProfiles"; }
        }

        public string UserGroupBrandingFolder
        {
            get { return "usergroupbranding"; }
        }

        #endregion
    }
}
