﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using GMGColor.Resources;

namespace GMGColorDAL.Common
{
    public static class GMGColorFormatData
    {
        private static string GetDatePattern(int accountId, GMGColorContext context)
        {
            return
                (from a in context.Accounts
                    join df in context.DateFormats on a.DateFormat equals df.ID
                    where a.ID == accountId
                    select df.Pattern).FirstOrDefault() ?? string.Empty;
        }

        public static string GetCurrencySymbol(int accountId, GMGColorContext context)
        {
            return
                (from a in context.Accounts
                    join c in context.Currencies on a.CurrencyFormat equals c.ID
                    where a.ID == accountId
                    select c.Symbol).FirstOrDefault() ?? string.Empty;
        }

        public static string GetFormattedDate(string dateTime, Account loggedAccount, GMGColorContext context)
        {
            string dateFormatPattern = GetDatePattern(loggedAccount.ID, context);

            DateTime dt;
            if (!DateTime.TryParse(dateTime, out dt))
            {
                DateTime.TryParseExact(dateTime, dateFormatPattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            }

            if (!string.IsNullOrEmpty(dateTime))
            {
                return String.Format("{0:" + dateFormatPattern + "}", dt);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetFormattedDate(DateTime? dateTime, Account loggedAccount, GMGColorContext context)
        {
            return dateTime.HasValue ? GetFormattedDate(dateTime.Value, loggedAccount, context) : string.Empty;
        }

        public static string GetFormattedDate(DateTime dateTime, Account loggedAccount, GMGColorContext context)
        {
            string dateFormatPattern = GetDatePattern(loggedAccount.ID, context);
            return String.Format("{0:" + dateFormatPattern + "}", dateTime);
        }

        public static string GetFormattedDate(DateTime dateTime, string datePattern)
        {
            if (!string.IsNullOrEmpty(datePattern))
            {
                return String.Format("{0:" + datePattern + "}", dateTime);
            }
            else
            {
                return string.Empty;
            }
        }

        

        public static string GetUserFormattedDate(DateTime? dateTime, string datePattern)
        {
            if(dateTime != null)
            { 
                if (!string.IsNullOrEmpty(datePattern))
                {
                    return String.Format("{0:" + datePattern + "}", dateTime);
                }
            }
            
            return string.Empty;

        }

        public static string GetUserFormattedTime(DateTime? dateTime, int id)
        {
            if (dateTime != null)
            {
                return GetFormattedTime(dateTime.Value, id);
            }
            return string.Empty;

        }

        public static string GetFormattedCurrency(decimal amount, int accountId, GMGColorContext context)
        {
            decimal number;
            decimal curr = amount;
            string symbol = GetCurrencySymbol(accountId, context);

            if (Decimal.TryParse(amount.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out number))
            {
                curr = number;
            }

            return symbol + " " + String.Format(CultureInfo.InvariantCulture, "{0:N}", curr);
        }

        public static string GetFormattedCurrency(decimal amount, string currencySymbol)
        {
            decimal number;
            decimal curr = amount;

            if (Decimal.TryParse(amount.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out number))
            {
                curr = number;
            }

            return currencySymbol + " " + String.Format(CultureInfo.InvariantCulture, "{0:N}", curr);
        }

        public static string GetFormattedTime(DateTime dateTime, Account loggedAccount)
        {
            return GetFormattedTime(dateTime, loggedAccount.TimeFormat);
        }

        public static string GetFormattedTime(DateTime dateTime, int timeFormat)
        {
            return (timeFormat == 1) ? dateTime.ToString("hh:mm tt") : dateTime.ToString("HH:mm");
        }

        public static string GetTimeDifference(DateTime comparingUTCDateTime /*, string userTimeZoneID*/)
        {
            string timeDifference = string.Empty;

            DateTime currentUserTime = DateTime.UtcNow; // (string.IsNullOrEmpty(userTimeZoneID)) ? DateTime.UtcNow : comparingUTCDateTime; // GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, userTimeZoneID);

            var dateSpan = GMGColorDAL.Common.DateTimeSpan.CompareDates(comparingUTCDateTime, currentUserTime);

            if (dateSpan.Years > 0)
            {
                timeDifference = dateSpan.Years + " " + Resources.lblYears + " " + Resources.lblAgo + " ";
            }
            else if (dateSpan.Months > 0)
            {
                timeDifference = dateSpan.Months + " " + Resources.lblMonths + " " + Resources.lblAgo + " ";
            }
            else if (dateSpan.Days > 0)
            {
                timeDifference = dateSpan.Days + " " + Resources.lblDays + " " + Resources.lblAgo + " ";
            }
            else if (dateSpan.Hours > 0)
            {
                timeDifference = dateSpan.Hours + " " + Resources.lblHours + " " + Resources.lblAgo + " ";
            }
            else if (dateSpan.Minutes > 0)
            {
                timeDifference = dateSpan.Minutes + " " + Resources.lblMinutes + " " + Resources.lblAgo + " ";
            }
            else
            {
                timeDifference = " " + Resources.lblALessThanAMinute + " " + Resources.lblAgo + " ";
            }

            return timeDifference;
        }

        /// <summary>
        /// calculate and returns the input file size in KB or MB.
        /// </summary>
        /// <param name="fileSizeInBytes">file size in KB</param>
        /// <returns></returns>
        public static string GetFileSizeString(decimal fileSizeInBytes)
        {
            fileSizeInBytes = fileSizeInBytes / 1024; // KB

            if (fileSizeInBytes < 1024 / 2)
                return (fileSizeInBytes).ToString("0.00") + "KB";
            else
                return (fileSizeInBytes / 1024).ToString("0.00") + "MB";
        }

        public static DateTime GetUserTimeFromUTC(DateTime dtUtcTime, string id)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dtUtcTime, TimeZoneInfo.FindSystemTimeZoneById(id));
        }

        public static DateTime SetUserTimeToUTC(DateTime dtUserTime, String id)
        {
            return TimeZoneInfo.ConvertTime(dtUserTime, TimeZoneInfo.FindSystemTimeZoneById(id), TimeZoneInfo.Utc);
        }

        public static int FloatToInt(float f)
        {
            return (int)Math.Floor(f + 0.5);
        }
    }

    public struct DateTimeSpan
    {
        private readonly int years;
        private readonly int months;
        private readonly int days;
        private readonly int hours;
        private readonly int minutes;
        private readonly int seconds;
        private readonly int milliseconds;

        public DateTimeSpan(int years, int months, int days, int hours, int minutes, int seconds, int milliseconds)
        {
            this.years = years;
            this.months = months;
            this.days = days;
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
            this.milliseconds = milliseconds;
        }

        public int Years { get { return years; } }
        public int Months { get { return months; } }
        public int Days { get { return days; } }
        public int Hours { get { return hours; } }
        public int Minutes { get { return minutes; } }
        public int Seconds { get { return seconds; } }
        public int Milliseconds { get { return milliseconds; } }

        enum Phase { Years, Months, Days, Done }

        public static DateTimeSpan CompareDates(DateTime date1, DateTime date2)
        {
            if (date2 < date1)
            {
                var sub = date1;
                date1 = date2;
                date2 = sub;
            }

            DateTime current = date1;
            int years = 0;
            int months = 0;
            int days = 0;

            Phase phase = Phase.Years;
            DateTimeSpan span = new DateTimeSpan();
            int officialDay = current.Day;

            while (phase != Phase.Done)
            {
                switch (phase)
                {
                    case Phase.Years:
                        if (current.AddYears(years + 1) > date2)
                        {
                            phase = Phase.Months;
                            current = current.AddYears(years);
                        }
                        else
                        {
                            years++;
                        }
                        break;
                    case Phase.Months:
                        if (current.AddMonths(months + 1) > date2)
                        {
                            phase = Phase.Days;
                            current = current.AddMonths(months);
                            if (current.Day < officialDay && officialDay <= DateTime.DaysInMonth(current.Year, current.Month))
                                current = current.AddDays(officialDay - current.Day);
                        }
                        else
                        {
                            months++;
                        }
                        break;
                    case Phase.Days:
                        if (current.AddDays(days + 1) > date2)
                        {
                            current = current.AddDays(days);
                            var timespan = date2 - current;
                            span = new DateTimeSpan(years, months, days, timespan.Hours, timespan.Minutes, timespan.Seconds, timespan.Milliseconds);
                            phase = Phase.Done;
                        }
                        else
                        {
                            days++;
                        }
                        break;
                }
            }

            return span;
        }
    }
}
