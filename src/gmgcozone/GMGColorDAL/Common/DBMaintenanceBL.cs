﻿using System;
using System.Activities.Statements;
using GMGColorDAL;
using GL = GMGColorDAL.GMGColorLogging;

namespace GMGColorDAL
{
    public class DBMaintenanceBL
    {
        #region Public Methods

        /// <summary>
        /// Recreate database indexes
        /// </summary>
        public void RecreateIndexes(GMGColorContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand(
                                @"USE GMGCoZone
                                DECLARE @SQL NVARCHAR(MAX)
                                SET @SQL = ( SELECT Row + ';'
                                                FROM   ( SELECT    'ALTER INDEX [' + ix.name + '] ON [' + s.name
                                                                + '].[' + t.name + '] '
                                                                + CASE WHEN ps.avg_fragmentation_in_percent > 29
                                                                        THEN 'REBUILD'
                                                                        ELSE 'REORGANIZE'
                                                                    END
                                                                + CASE WHEN pc.partition_count > 1
                                                                        THEN ' PARTITION = '
                                                                            + CAST(ps.partition_number AS NVARCHAR(MAX))
                                                                        ELSE ''
                                                                    END AS [Row]
                                                        FROM      sys.indexes AS ix
                                                                INNER JOIN sys.tables t ON t.object_id = ix.object_id
                                                                INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
                                                                INNER JOIN ( SELECT object_id ,
                                                                                    index_id ,
                                                                                    avg_fragmentation_in_percent ,
                                                                                    partition_number
                                                                                FROM   sys.dm_db_index_physical_stats(DB_ID(),
                                                                                                NULL, NULL, NULL,
                                                                                                NULL)
                                                                            ) ps ON t.object_id = ps.object_id
                                                                                    AND ix.index_id = ps.index_id
                                                                INNER JOIN ( SELECT object_id ,
                                                                                    index_id ,
                                                                                    COUNT(DISTINCT partition_number) AS partition_count
                                                                                FROM   sys.partitions
                                                                                GROUP BY object_id ,
                                                                                    index_id
                                                                            ) pc ON t.object_id = pc.object_id
                                                                                    AND ix.index_id = pc.index_id
                                                        WHERE     ps.avg_fragmentation_in_percent > 10
                                                                AND ix.name IS NOT NULL
                                                    ) AS SLCT
                                            FOR
                                                XML PATH('')
                                            )
                                EXEC sp_executeSql @SQL"); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Clear stored procedure
        /// </summary>
        public void ClearStoredProceduresCache(GMGColorContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DBCC FREEPROCCACHE");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update statistics
        /// </summary>
        public void UpdateStatistics(GMGColorContext context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("[dbo].[up_updstats]");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
