﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using GMGColor.AWS;
using GMGColorDAL.CustomModels.Api;
using GMG.CoZone.Common.Util;

namespace GMGColorDAL.Common
{
    public static class GMGColorIO
    {
        #region Constants

        const string ENCRIPT_DCRIPT_KEY = "1ZpkHc2K7a1RxQDJPDJfnbRy7SyoDx";

        #endregion

        #region Methods


        /// <summary>
        /// Method used to retrieve the custom profiles from the storage
        /// Note: for local environment the path will be replaced with the custom profile folder from the solution
        /// </summary>
        /// <param name="relativeFilePath"></param>
        /// <param name="accountRegion"></param>
        /// <param name="accountDomain"></param>
        /// <returns></returns>
        public static List<CustomModels.Api.AWSFileObject> ListCustomProfiles(string relativeFilePath, string accountRegion, string accountDomain)
        {
            List<CustomModels.Api.AWSFileObject> list = new List<AWSFileObject>();

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                var lst = AWSSimpleStorageService.GetObjects(
                    GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                    GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                    GMGColorConfiguration.AppConfiguration.AWSSecretKey, relativeFilePath);

                list.AddRange(
                    lst.Select(
                        s3Object =>
                            new AWSFileObject()
                            {
                                FileName = s3Object.Key.Split(new char[] { '\\', '/' }).LastOrDefault()
                            }));
            }
            else
            {
                string localDirectoryPath = GMG.CoZone.Common.Utils.AssemblyDirectory + @"\..\..\softproofing-profiles-repo\";
                List<string> files = Directory.GetFiles(localDirectoryPath).ToList();

                list.AddRange(files.Select(file => new AWSFileObject
                {
                    FileName = file.Split(new char[] { '\\', '/' }).LastOrDefault(),
                }));
            }
            return list;
        }

        public static List<CustomModels.Api.AWSFileObject> ListFiles(string relativeFilePath, string accountRegion, string accountDomain)
        {
            List<CustomModels.Api.AWSFileObject> list = new List<AWSFileObject>();

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                var lst = AWSSimpleStorageService.GetObjects(
                    GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                    GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                    GMGColorConfiguration.AppConfiguration.AWSSecretKey, relativeFilePath);

                var url = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);

                list.AddRange(
                    lst.Select(
                        s3Object =>
                            new AWSFileObject()
                            {
                                FileName = s3Object.Key.Split(new char[] {'\\', '/'}).LastOrDefault(),
                                Size = s3Object.Size,
                                Url = url + s3Object.Key
                            }));
            }
            else
            {
                string localDirectoryPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
                string urlDirectoryPath = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion) + "/";
                List<string> files = Directory.GetFiles(localDirectoryPath + relativeFilePath).ToList();

                list.AddRange(files.Select(file => new AWSFileObject()
                {
                    Url = file.Replace(localDirectoryPath, urlDirectoryPath),
                    FileName = file.Split(new char[] {'\\', '/'}).LastOrDefault(),
                    Size = new FileInfo(file).Length
                }));
            }
            return list;
        }

        public static bool FileExists(string relativeFilePath, string accountRegion)
        {
            bool fileExists = false;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                fileExists = AWSSimpleStorageService.FileExistsInBucket(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
            }
            else
            {
                var fi = new FileInfo(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath);
                fileExists = fi.Exists;
            }

            return fileExists;
        }

        public static bool FolderExists(string relativeFolderPath, string accountRegion, bool IsCreateIfNotExists = false, DateTime? expireDate = null)
        {
            bool folderExists = false;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion);
                folderExists = AWSSimpleStorageService.FolderExistsInBucket(dataFolderName, relativeFolderPath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                if (IsCreateIfNotExists && !folderExists)
                {
                    folderExists = AWSSimpleStorageService.CreateNewFolder(dataFolderName, relativeFolderPath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, expireDate);
                }
            }
            else
            {
                string folderPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFolderPath;
                folderExists = System.IO.Directory.Exists(folderPath);

                if (IsCreateIfNotExists && !folderExists)
                {
                    try
                    {
                        Directory.CreateDirectory(folderPath);
                        folderExists = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return folderExists;
        }

        public static bool HtmlFolderExists(string relativeFolderPath, string accountRegion, bool IsCreateIfNotExists = false, DateTime? expireDate = null)
        {
            bool folderExists = false;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                string dataFolderName = GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion);
                folderExists = AWSSimpleStorageService.FolderExistsInBucket(dataFolderName, relativeFolderPath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                if (IsCreateIfNotExists && !folderExists)
                {
                    folderExists = AWSSimpleStorageService.CreateNewFolder(dataFolderName, relativeFolderPath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, expireDate);
                }
            }
            else
            {
                string folderPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFolderPath;
                folderExists = System.IO.Directory.Exists(folderPath);

                if (IsCreateIfNotExists && !folderExists)
                {
                    try
                    {
                        Directory.CreateDirectory(folderPath);
                        folderExists = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return folderExists;
        }


        /// <summary>
        /// Move file
        /// </summary>
        /// <param name="relativeSourceFilePath">Relative source path of the file(From).</param>
        /// <param name="relativeDestinationFilePath">Relative destination path of the file(To).</param>
        /// <returns></returns>
        public static bool MoveFile(string relativeSourceFilePath, string relativeDestinationFilePath, string accountRegion)
        {
            bool success = false;

            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    success = AWSSimpleStorageService.MoveFileInsideBucket(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeSourceFilePath, relativeDestinationFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
                    string fileLocalTempPath = pathtoDataFolder + relativeSourceFilePath;

                    File.Move(fileLocalTempPath, pathtoDataFolder + relativeDestinationFilePath);
                    success = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return success;
        }

        /// <summary>
        /// Move file
        /// </summary>
        /// <param name="relativeSourceFilePath">Relative source path of the file(From).</param>
        /// <param name="relativeDestinationFilePath">Relative destination path of the file(To).</param>
        /// <param name="accountRegion">The account region.</param>
        /// <param name="expireDate">The expires date.</param>
        /// <returns></returns>
        public static bool CopyFile(string relativeSourceFilePath, string relativeDestinationFilePath, string accountRegion, DateTime? expireDate = null)
        {
            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    AWSSimpleStorageService.CopyFileInsideBucket(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeSourceFilePath, relativeDestinationFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, expireDate);
                }
                else
                {
                    string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
                    string fileLocalTempPath = pathtoDataFolder + relativeSourceFilePath;

                    File.Copy(fileLocalTempPath, pathtoDataFolder + relativeDestinationFilePath);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }

        public static bool UploadFile(Stream fileStream, string relativeDestinationFilePath, string accountRegion, bool disposeStream = true)
        {
            bool success = false;

            try
            {
                fileStream.Seek(0, SeekOrigin.Begin);

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    success =
                        AWSSimpleStorageService.UploadFile(
                            GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                            relativeDestinationFilePath, fileStream, fileStream.Length,
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    string fileLocalPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) +
                                           relativeDestinationFilePath;

                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName((fileLocalPath)));
                    using (var fileSavingStream = File.Create(fileLocalPath))
                    {
                        fileStream.CopyTo(fileSavingStream);
                    }
                }
                success = true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (fileStream != null && disposeStream)
                {
                    fileStream.Dispose();
                    fileStream.Close();
                }
            }

            return success;
        }

        public static async System.Threading.Tasks.Task<bool> UploadZipFile(Stream fileStream, string relativeDestinationFilePath, string accountRegion, bool disposeStream = true)
        {
            bool success = false;

            try
            {
                fileStream.Seek(0, SeekOrigin.Begin);

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                   if(relativeDestinationFilePath.Contains(".html"))
                    {
                        StreamReader reader = new StreamReader(fileStream);
                        string htmltextOriginal = reader.ReadToEnd();
                        StringBuilder html = new StringBuilder();
                        html.Append("<script type=\"text/javascript\"  src=\"https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js\" > </script>\n ");
                        html.Append("<script> \n window.addEventListener('message', function (e) { \n  html2canvas(document.body).then(canvas => { \n  parent.postMessage(canvas.toDataURL(), e.origin );      \n }); \n }); \n </script> ");
                        string stringtext = htmltextOriginal + html;
                        byte[] byteArray = Encoding.ASCII.GetBytes(stringtext);
                        MemoryStream stream = new MemoryStream(byteArray);
                        fileStream = stream;
                    }

                    success =
                    await AWSSimpleStorageService.UploadFileAync(
                            GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion),
                            relativeDestinationFilePath, fileStream, fileStream.Length,
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey);

                }
                else
                {
                    string fileLocalPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) +
                                           relativeDestinationFilePath;

                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName((fileLocalPath)));
                    using (var fileSavingStream = File.Create(fileLocalPath))
                    {
                        fileStream.CopyTo(fileSavingStream);
                        
                    }
                    if (fileLocalPath.Contains(".html"))
                    {
                        using (FileStream aFile = new FileStream(fileLocalPath, FileMode.Append, FileAccess.Write))
                        using (StreamWriter sw = new StreamWriter(aFile))
                        {
                            System.Text.StringBuilder htmlScripts = new System.Text.StringBuilder();
                            htmlScripts.Append("<script type=\"text/javascript\"  src=\"https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js\" > </script>\n ");
                            htmlScripts.Append("<script> \n window.addEventListener('message', function (e) { \n  html2canvas(document.body).then(canvas => { \n  parent.postMessage(canvas.toDataURL(), e.origin );      \n }); \n }); \n </script> ");
                            sw.WriteLine(htmlScripts);
                        }
                    }
                    
                }
                success = true;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (fileStream != null && disposeStream)
                {
                    fileStream.Dispose();
                    fileStream.Close();
                }
            }

            return success;
        }


        /// <summary>
        /// Move stream
        /// </summary>
        /// <param name="fileStream">Memory stream of the file(From).</param>
        /// <param name="relativeDestinationFilePath">Relative destination path of the file(To).</param>
        /// <returns></returns>
        public static bool MoveStream(Stream fileStream, string relativeDestinationFilePath, string accountRegion)
        {
            bool success = false;

            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    success = AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeDestinationFilePath, fileStream, 0, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    string fileLocalPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeDestinationFilePath;
                    string file_Local_Path = fileLocalPath;
                    if (file_Local_Path.Length > 240)
                    {
                        string fileName = fileLocalPath.Substring(0, ((fileLocalPath.ToString().LastIndexOf("."))));
                        string FileExtention = fileLocalPath.Substring(((fileLocalPath.ToString().LastIndexOf("."))));
                        file_Local_Path = fileName.Substring(0, 240) + FileExtention;

                    }
                    
                    using (var fileSavingStream = File.Create(file_Local_Path))
                    {
                        fileStream.CopyTo(fileSavingStream);
                    }
                }

                fileStream.Dispose();
                fileStream.Close();
                success = true;
            }
            catch (Exception)
            {
                throw;
            }

            return success;
        }

        public static Bitmap GetImageBitmap(string relativeImagePath, string accountRegion)
        {
            Bitmap bitmap;
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                bitmap = new Bitmap(AWSSimpleStorageService.GetFileStream(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeImagePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey));
            }
            else
            {
                bitmap = new Bitmap(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeImagePath);
            }

            return bitmap;
        }

        public static void WriteProgressFile(string progress, string relativeFilePath, string accountRegion)
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                AWSSimpleStorageService.UploadFileWithContent(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, progress, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
            }
            else
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath, false);
                file.WriteLine(progress);
                file.Close();
            }
        }

        public static bool DeleteFile(string relativeFilePath, string accountRegion)
        {
            bool success = false;

            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    FolderExists("backup/", accountRegion, true);

                    var destinationPath = "backup/" + relativeFilePath;

                    AWSSimpleStorageService.CopyFileInsideBucket(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, destinationPath,
                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey);

                    success = AWSSimpleStorageService.FileExistsInBucketThenDelete(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    File.Delete(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath);
                    success = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return success;
        }


        private static void DeleteLocalFolder(string path)
        {
            string[] files = Directory.GetFiles(path);
            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            string[] dirs = Directory.GetDirectories(path);
            foreach (string dir in dirs)
            {
                DeleteLocalFolder(dir);
            }

            Directory.Delete(path, false);
            Thread.Sleep(50);
        }

        public static void DeleteFolder(string relativeFolderPath, string accountRegion)
        {
            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    FolderExists("backup/", accountRegion, true);

                    AWSSimpleStorageService.CopyAllFilesInsideBucket(
                        GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFolderPath,
                        "backup/", GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    
                    AWSSimpleStorageService.DeleteAllFiles(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFolderPath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    DeleteLocalFolder(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFolderPath);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool DeleteFolderIfExists(string folderPath, string accountRegion)
        {
            bool isFolderExsits = false;
            
            isFolderExsits = FolderExists(folderPath, accountRegion);

            if (isFolderExsits)
            {
                TryDeleteFolder(folderPath, accountRegion);
            }
            return true;
        }

        public static void TryDeleteFolder(string folderPath, string accountRegion)
        {
            (new RetryMechanism()).Run(
                () => DeleteFolder(folderPath, accountRegion),
                TimeSpan.FromSeconds(1));
        }

        public static bool DeleteFileIfExists(string relativeFilePath, string accountRegion)
        {
            bool fileExsits = false;

            fileExsits = FolderExists(relativeFilePath, accountRegion);

            if (fileExsits)
            {
                TryDeleteFile(relativeFilePath, accountRegion);
            }
            return true;
        }

        public static void TryDeleteFile(string relativeFilePath, string accountRegion)
        {
            (new RetryMechanism()).Run(
                () => DeleteFile(relativeFilePath, accountRegion),
                TimeSpan.FromSeconds(1));
        }

        public static byte[] GetByteArrayOfALocalFile(string imageFileName)
        {
            byte[] image;
            BinaryReader bReader;

            try
            {
                Stream stream = new FileStream(imageFileName, FileMode.Open, FileAccess.Read);
                bReader = new BinaryReader(stream);
                image = bReader.ReadBytes((int)stream.Length);
                bReader.Close();
                stream.Close();

                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Stream GetMemoryStreamOfAFile(string relativeFilePath, string accountRegion)
        {
            Stream stream;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                stream = AWSSimpleStorageService.GetFileStream(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
            }
            else
            {
                stream = new FileStream(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath, FileMode.Open, FileAccess.Read);
            }
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        public static Stream GetMemoryStreamOfZipFile(string relativeFilePath, string accountRegion)
        {
            Stream stream;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                stream = AWSSimpleStorageService.GetFileStream(GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion), relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
            }
            else
            {
                stream = new FileStream(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath, FileMode.Open, FileAccess.Read);
            }
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        public static byte[] GetByteArrayOfAFile(string relativePath, string accountRegion)
        {
            byte[] fileByteArray = new byte[] {};
            using (Stream bandStream = GetMemoryStreamOfAFile(relativePath, accountRegion))
            {
                if (bandStream is MemoryStream)
                {
                    //AWS
                    fileByteArray = (bandStream as MemoryStream).ToArray();
                }
                else
                {
                    //local
                    using (var memoryStream = new MemoryStream())
                    {
                        bandStream.CopyTo(memoryStream);
                        fileByteArray = memoryStream.ToArray();
                    }
                }
            }

            return fileByteArray;
        }

        public static byte[] GetByteArrayOfAFile(string relativePath, string localSavePath, string accountRegion, ref string errorMessage)
        {
            byte[] fileByteArray = new byte[] { };
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket && !File.Exists(localSavePath))
            {
                using (Stream bandStream = GetMemoryStreamOfAFile(relativePath, accountRegion))
                {
                    if (bandStream is MemoryStream)
                    {
                        //AWS
                        fileByteArray = (bandStream as MemoryStream).ToArray();
                        //save locally
                        try
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(localSavePath));
                            File.WriteAllBytes(localSavePath, fileByteArray);
                        }
                        catch (Exception ex)
                        {
                            errorMessage = ex.Message;
                        }
                    }
                }
            }
            else
            {
                try
                {
                    //get from cache
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        return File.ReadAllBytes(localSavePath);
                    }
                    else
                    {
                        return File.ReadAllBytes(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativePath);
                    }
                }
                catch (Exception ex)
                {
                    //fallback to s3 in case of error
                    fileByteArray = GetByteArrayOfAFile(relativePath, accountRegion);
                    errorMessage = ex.Message;
                }
            }

            return fileByteArray;
        }

        public static int GetAccessedMaxVersion(int jobId, int userId, GMGColorContext context)
        {
            try
            {
                IEnumerable<int> versions = (from j in context.Jobs
                        join a in context.Approvals on j.ID equals a.Job
                        join js in context.JobStatus on j.Status equals js.ID
                        join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                        where js.Key != "ARC" && a.IsDeleted == false && ac.Collaborator == userId
                        select a.Version);

                return versions.Any() ? versions.Max() : -1;
            }
            finally
            {
            }
        }

        public static void CreateVirtualFolderStructure(string folderPath, int folderId, int userId, GMGColorContext context)
        {
            try
            {
                Folder objFolder = (from f in context.Folders where f.ID == folderId select f).FirstOrDefault();

                List<Approval> lstFolderApprovals =
                    (from o in objFolder.Approvals
                     where
                         GetAccessedMaxVersion(o.Job, userId, context) > -1 &&
                         o.Version == GetAccessedMaxVersion(o.Job, userId, context) &&
                         o.AllowDownloadOriginal == true
                     select o).ToList();

                List<Folder> lstSubFolders = (from o in objFolder.GetChilds(context)
                                              where
                                                  o.FolderCollaborators.Select(m => m.ID).Contains(userId)
                                              select o).ToList();

                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                folderPath = folderPath + "/" + objFolder.Name + "/";
                GMGColorIO.FolderExists(folderPath, objFolder.Account1.Region, true);

                // Copy Approvals
                foreach (Approval objApproval in lstFolderApprovals)
                {
                    string physicalFolderPath = "approvals/" + objApproval.Guid + "/" + objApproval.FileName;
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        int attempt = 0;
                        do
                        {
                            WebClient client = new WebClient();
                            client.DownloadFile(GMGColorConfiguration.AppConfiguration.PathToDataFolder(objApproval.Job1.Account1.Region) + physicalFolderPath, folderPath + objApproval.FileName);

                            if (!File.Exists(folderPath + objApproval.FileName))
                            {
                                attempt++;
                            }
                        }
                        while (attempt < 3);
                    }
                    else
                    {
                        GMGColorIO.MoveFile(physicalFolderPath, folderPath + objApproval.FileName, objApproval.Job1.Account1.Region);
                    }
                }

                // Copy Subfolders
                foreach (Folder objSubFolder in lstSubFolders)
                {
                    GMGColorIO.CreateVirtualFolderStructure(folderPath, objSubFolder.ID, userId, context);
                }
            }
            finally
            {
            }
        }

        public static string ReadFile(string relativeFilePath, string accountRegion)
        {
            string fileContent = string.Empty;
            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    fileContent = GMGColor.AWS.AWSSimpleStorageService.ReadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    TextReader tr = new StreamReader(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath);
                    fileContent = tr.ReadToEnd();
                    tr.Close();
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Error occured while reading the file " + relativeFilePath, ex);
            }

            return fileContent;
        }

        public static decimal GetFileSizeInBytes(string relativeFilePath, string accountRegion)
        {
            decimal size = 0;

            try
            {
                if (FileExists(relativeFilePath, accountRegion))
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        Stream stream = GetMemoryStreamOfAFile(relativeFilePath, accountRegion);
                        size = stream.Length;
                        stream.Dispose();
                        stream.Close();
                    }
                    else
                    {
                        FileInfo info = new FileInfo(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath);
                        size = (info.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Error occured while reading the file size " + relativeFilePath, ex);
            }

            return size;
        }

        public static void CopyLocalDirectory(string strSource, string strDestination)
        {
            if (!Directory.Exists(strDestination))
            {
                Directory.CreateDirectory(strDestination);
            }
            DirectoryInfo dirInfo = new DirectoryInfo(strSource);
            FileInfo[] files = dirInfo.GetFiles();
            foreach (FileInfo tempfile in files)
            {
                tempfile.CopyTo(Path.Combine(strDestination, tempfile.Name));
            }
            DirectoryInfo[] dirctororys = dirInfo.GetDirectories();
            foreach (DirectoryInfo tempdir in dirctororys)
            {
                CopyLocalDirectory(Path.Combine(strSource, tempdir.Name), Path.Combine(strDestination, tempdir.Name));
            }
        }

        public static string Encrypt(string stringToEncrypt, bool useHashing = true)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(stringToEncrypt);

            // If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRIPT_DCRIPT_KEY));
                // Always release the resources and flush data of the Cryptographic service provide. Best Practice
                hashmd5.Clear();
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(ENCRIPT_DCRIPT_KEY);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            // Set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            // Mode of operation. there are other 4 modes. here choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            // Padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            // Transform the specified region of bytes array to resultArray
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            // Release resources held by TripleDes Encryptor
            tdes.Clear();

            // Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string stringToDcript, bool useHashing = true)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(stringToDcript);

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRIPT_DCRIPT_KEY));
                //release any resource held by the MD5CryptoServiceProvider
                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(ENCRIPT_DCRIPT_KEY);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            // Mode of operation. there are other 4 modes. here we choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            // Padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            // Release resources held by TripleDes Encryptor                
            tdes.Clear();

            // Return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static void ClearTempFolder(string relativeFolderPath, string accountRegion, int olderDays = 2)
        {
            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    List<string> usersFolders = AWSSimpleStorageService.GetAllFolders(
                        GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey, relativeFolderPath);

                    foreach (string userFolder in usersFolders)
                    {
                        List<string> folders =
                            AWSSimpleStorageService.GetAllFolders(
                                GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                userFolder, false);

                        foreach (string folder in folders)
                        {
                            DateTime modifiedDate = AWSSimpleStorageService.GetS3ObjectModifiedDate(
                                GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                                folder,
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                GMGColorConfiguration.AppConfiguration.AWSSecretKey);

                            if ((DateTime.UtcNow - modifiedDate).TotalDays >= olderDays)
                            {
                                DeleteFolder(folder, accountRegion);
                            }
                        }

                        List<string> files =
                            AWSSimpleStorageService.GetAllFiles(
                                GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                userFolder);

                        foreach (string file in files)
                        {
                            DateTime modifiedDate = AWSSimpleStorageService.GetS3ObjectModifiedDate(
                                GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                                file,
                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                GMGColorConfiguration.AppConfiguration.AWSSecretKey);

                            if ((DateTime.UtcNow - modifiedDate).TotalDays >= olderDays)
                            {
                                DeleteFile(file, accountRegion);
                            }
                        }
                    }
                }
                else
                {
                    string folderPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFolderPath;
                    string[] userFolders = Directory.GetDirectories(folderPath);
                    foreach (string userFolder in userFolders)
                    {
                        string[] folders = Directory.GetDirectories(userFolder);
                        foreach (string folder in folders)
                        {
                            DateTime modifiedDate = Directory.GetLastWriteTime(folder);
                            if ((DateTime.UtcNow - modifiedDate).TotalDays >= olderDays)
                            {
                                DeleteLocalFolder(folder);
                            }
                        }

                        string[] files = Directory.GetFiles(userFolder);
                        foreach (string file in files)
                        {
                            DateTime modifiedDate = File.GetLastWriteTime(file);
                            if ((DateTime.UtcNow - modifiedDate).TotalDays >= olderDays)
                            {
                                File.Delete(file);
                            }
                        }
                    }
                }
            }
            catch(Exception)
            {
                throw;
            }
        }

        public static string GetAbsoluteFilePath(string relativeFilePath, string accountRegion, string accountDomain, bool disableCaching = true)
        {
            string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion);
            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);

            string absoluteFileUrl = string.Empty;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                absoluteFileUrl = pathtoDataFolder;
            }
            else
            {
                absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + dataFolderName + "/";
            }

            absoluteFileUrl += relativeFilePath + (disableCaching ? ("?" + (new Random()).Next()) : "");

            return absoluteFileUrl.ToLower();
        }

        public static bool UploadChunkFile(Stream fileStream, string relativeDestinationFilePath, string accountRegion,  Dictionary<string, string> cacheChunkInfo, int chunkNo, string fileContentRange, string fileName, Int64 filePartSize, bool disposeStream = true)
        {
            bool success = false;

            try
            {
                fileStream.Seek(0, SeekOrigin.Begin);
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    var contentRangeSplit = fileContentRange.Split('/');
                    if (contentRangeSplit[0] == contentRangeSplit[1])
                    {
                        AWSSimpleStorageService.UploadFilePartStream(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeDestinationFilePath + fileName,
                                                cacheChunkInfo["awsFileUploadId"], chunkNo, filePartSize, fileStream, GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                GMGColorConfiguration.AppConfiguration.AWSSecretKey);

                        IEnumerable<AWSPartETag> awsparts = AWSSimpleStorageService.ListPartsUpload(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), 
                                                            relativeDestinationFilePath + fileName,cacheChunkInfo["awsFileUploadId"], GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                            GMGColorConfiguration.AppConfiguration.AWSSecretKey);

                        success = AWSSimpleStorageService.CompleteMultipartUploadRequest(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                                                          relativeDestinationFilePath + fileName,cacheChunkInfo["awsFileUploadId"], awsparts,
                                                          GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    }
                    else
                    {
                        if (cacheChunkInfo["awsFileUploadId"] == "")
                        {
                            cacheChunkInfo["awsFileUploadId"] = AWSSimpleStorageService.InitMultipartUploadRequest(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion),
                                relativeDestinationFilePath + fileName, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                        }

                        AWSSimpleStorageService.UploadFilePartStream(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeDestinationFilePath + fileName,
                                                cacheChunkInfo["awsFileUploadId"], chunkNo, filePartSize, fileStream, GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    }
                }
                else
                {
                    string fileLocalPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) +
                   relativeDestinationFilePath;

                    using (var fileSavingStream = File.Create(fileLocalPath + chunkNo + ".tmp"))
                    {
                        fileStream.CopyTo(fileSavingStream);
                    }

                    var contentRangeSplit = fileContentRange.Split('/');
                    if (contentRangeSplit[0] == contentRangeSplit[1])
                    {
                        mergeTempFiles(fileLocalPath, GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeDestinationFilePath, fileName);
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, 0, "UploadHandler: UploadChunkFile file failed. {0}", ex.Message);
                throw;
            }
            finally
            {
                if (fileStream != null && disposeStream)
                {
                    fileStream.Dispose();
                    fileStream.Close();
                }
            }

            return success;
        }

        public static void mergeTempFiles(string pathOrigin, string pathToSave, string filename)
        {
            try
            {
                string[] tmpfiles = System.IO.Directory.GetFiles(pathOrigin, "*.tmp");

                if (!System.IO.Directory.Exists(pathToSave))
                {
                    System.IO.Directory.CreateDirectory(pathToSave);
                }
                System.IO.FileStream outPutFile = new System.IO.FileStream(pathToSave + filename, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                foreach (string tempFile in tmpfiles)
                {
                    int bytesRead = 0;
                    byte[] buffer = new byte[1024];
                    System.IO.FileStream inputTempFile = new System.IO.FileStream(tempFile, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Read);
                    while ((bytesRead = inputTempFile.Read(buffer, 0, 1024)) > 0)
                        outPutFile.Write(buffer, 0, bytesRead);
                    inputTempFile.Close();
                    System.IO.File.Delete(tempFile);
                }
                outPutFile.Close();
            }
            catch (Exception e)
            {

            }
        }

        public static bool MoveDirectory(string sourceFolderPath,  string destinationFolderPath, string loggedUserTempFolderPath, string accountRegion)
        {
            bool success = false;
            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    success = AWSSimpleStorageService.MoveFolderInsideBucket(GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion), sourceFolderPath.Replace("\\", "/"), loggedUserTempFolderPath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                else
                {
                    string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
                    Directory.Move(pathtoDataFolder + sourceFolderPath, pathtoDataFolder + destinationFolderPath);

                    success = true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return success;
        }

        public static List<string> GetObjects(string bucketName, string awsAccessKey, string awsSecretKey, string awsPrefix, string accountRegion)
        {
            List<string> lsthtmlfiles = new List<string>();
            var lsthtmlfilesS3Object = AWSSimpleStorageService.GetObjects(bucketName, awsAccessKey, awsSecretKey, awsPrefix);
            if (lsthtmlfilesS3Object != null)
            {
                lsthtmlfiles = lsthtmlfilesS3Object.Where(x => x.Key.EndsWith(".html") && !x.Key.Contains("__MACOSX")).Select(x => GMGColorConfiguration.AppConfiguration.PathToHtmlDataFolder(accountRegion) + x.Key).ToList();
            }
            return lsthtmlfiles;
        }

        #endregion
    }
}
