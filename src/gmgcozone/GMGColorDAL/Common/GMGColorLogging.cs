using System;
using System.Linq;
using Elmah;
using System.Threading;

namespace GMGColorDAL
{
    /// <summary>
    /// This class is responsible for logging information, warnings, errors etc.
    /// </summary>
    public static class GMGColorLogging
    {
        // Create a logger for use in this class
        public static readonly ELMAHLogging log = new ELMAHLogging();
    }

    public class ELMAHLogging
    {
        private enum LogLevel
        {
            Debug = 0,
            Info = 1,
            Warning = 2,
            Error = 3,
            Fatal = 4,
            APIInfo = 5
        }
        public enum Project { GMGColor, GMGColorAWS, GMGColorDAL, GMGColorDataCleaningService, GMGColorLogging, GMGColorMediaDefine, GMGColorMediaProcess, GMGColorNotificationEngine }

        public void InfoAPIFormat(string format,bool isApiLog, params object[] args)
        {
            if (isApiLog)
            {
                if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.APIInfo)
                {
                    format = string.Format(format, args);
                    this.InfoAPI(format);
                }
            }
        }
        public void InfoFormat(string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Info)
            {
                format = string.Format(format, args);
                this.Info(format);
            }
        }
        public void InfoFormat(int account, string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Info)
            {
                format = string.Format(format, args);
                this.Info(account, format, null);
            }
        }

        public void WarnFormat(string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Warning)
            {
                format = string.Format(format, args);
                this.Warn(format);
            }
        }
        public void WarnFormat(int account, string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Warning)
            {
                format = string.Format(format, args);
                this.Warn(account, format, null);
            }
        }

        public void ErrorFormat(Exception ex, string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Error)
            {
                format = string.Format(format, args);
                if (ex != null)
                {
                    this.Error(format, ex);
                }
                else
                {
                    this.Error(format);
                }
            }
        }
        public void ErrorFormat(Exception ex, int account, string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Error)
            {
                format = string.Format(format, args);
                this.Error(account, format, ex);
            }
        }

        public void FatalFormat(string format, Project currentProject, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Fatal)
            {
                format = string.Format(format, args);
                this.Fatal(format, currentProject);
            }
        }
        public void FatalFormat(int account, string format, Project currentProject, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Fatal)
            {
                format = string.Format(format, args);
                this.Fatal(account, format, null, currentProject);
            }
        }

        public void DebugFormat(string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Debug)
            {
                format = string.Format(format, args);
                this.Debug(format);
            }
        }
        public void DebugFormat(int account, string format, params object[] args)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Debug)
            {
                format = string.Format(format, args);
                this.Debug(account, format, new Exception());
            }
        }


        public void Info(Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Info)
            {
                //ErrorSignal.FromCurrentContext().Raise(string.Empty, exception, LogThreshold.Threshold.Info);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, string.Empty, 0, LogThreshold.Threshold.Info));
            }
        }
        public void Info(object message)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Info)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), (new Exception()), LogThreshold.Threshold.Info);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(null/*new Exception()*/, null, message.ToString(), 0, LogThreshold.Threshold.Info));
            }
        }
        public void Info(object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Info)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Info);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), 0, LogThreshold.Threshold.Info));
            }
        }
        public void Info(int account, object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Info)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Info, account);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), account, LogThreshold.Threshold.Info));
            }
        }
        public void InfoAPI(object message)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.APIInfo)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), (new Exception()), LogThreshold.Threshold.Info);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(null/*new Exception()*/, null, message.ToString(), 0, LogThreshold.Threshold.Info));
            }
        }
        public void Warn(Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Warning)
            {
                //ErrorSignal.FromCurrentContext().Raise(string.Empty, exception, LogThreshold.Threshold.Warning);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, string.Empty, 0, LogThreshold.Threshold.Warning));
            }
        }
        public void Warn(object message)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Warning)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), (new Exception()), LogThreshold.Threshold.Warning);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(null/*new Exception()*/, null, message.ToString(), 0, LogThreshold.Threshold.Warning));
            }
        }
        public void Warn(object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Warning)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Warning);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), 0, LogThreshold.Threshold.Warning));
            }
        }
        public void Warn(int account, object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Warning)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Warning, account);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), account, LogThreshold.Threshold.Warning));
            }
        }

        public void Error(Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Error)
            {
                //ErrorSignal.FromCurrentContext().Raise(string.Empty, exception, LogThreshold.Threshold.Error);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, string.Empty, 0, LogThreshold.Threshold.Error));
            }
        }

        public void Error(object message)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Error)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), (new Exception()), LogThreshold.Threshold.Error);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(null/*new Exception()*/, null, message.ToString(), 0, LogThreshold.Threshold.Error));
            }
        }
        public void Error(object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Error)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Error);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), 0, LogThreshold.Threshold.Error));
            }
        }
        public void Error(int account, object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Error)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Error, account);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), account, LogThreshold.Threshold.Error));
            }
        }

        public void Fatal(Exception exception, Project currentProject)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Fatal)
            {
                Thread t = new Thread(delegate()
                {
                    //ErrorSignal.FromCurrentContext().Raise(string.Empty, exception, LogThreshold.Threshold.Fatal);
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(exception, null, string.Empty, 0, LogThreshold.Threshold.Fatal));

                    try
                    {
                        GMGColor.AWS.AWSSNSData objSNSData = new GMGColor.AWS.AWSSNSData(0, string.Empty, (!string.IsNullOrEmpty(exception.Message) ? exception.Message : string.Empty), exception, currentProject.ToString());
                        GMGColor.AWS.AWSSimpleNotificationService.WriteMessage(
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey
                            , GMGColorConfiguration.AppConfiguration.AWSSecretKey
                            , GMGColorConfiguration.AppConfiguration.AWSSNSTopicName
                            , objSNSData);
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(ex);
                    }
                });

                t.Start();
            }
        }
        public void Fatal(object message, Project currentProject)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Fatal)
            {
                Thread t = new Thread(delegate()
                {
                    //ErrorSignal.FromCurrentContext().Raise(message.ToString(), (new Exception()), LogThreshold.Threshold.Fatal);
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(null, null, message.ToString(), 0, LogThreshold.Threshold.Fatal));

                    try
                    {
                        GMGColor.AWS.AWSSNSData objSNSData = new GMGColor.AWS.AWSSNSData(0, string.Empty, message.ToString(), null, currentProject.ToString());
                        GMGColor.AWS.AWSSimpleNotificationService.WriteMessage(
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey
                            , GMGColorConfiguration.AppConfiguration.AWSSecretKey
                            , GMGColorConfiguration.AppConfiguration.AWSSNSTopicName
                            , objSNSData);
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(ex);
                    }
                });

                t.Start();
            }
        }
        public void Fatal(object message, Exception exception, Project currentProject)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Fatal)
            {
                Thread t = new Thread(delegate()
                {
                    //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Fatal);
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), 0, LogThreshold.Threshold.Fatal));

                    try
                    {
                        GMGColor.AWS.AWSSNSData objSNSData = new GMGColor.AWS.AWSSNSData(0, string.Empty, message.ToString(), exception, currentProject.ToString());
                        GMGColor.AWS.AWSSimpleNotificationService.WriteMessage(
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey
                            , GMGColorConfiguration.AppConfiguration.AWSSecretKey
                            , GMGColorConfiguration.AppConfiguration.AWSSNSTopicName
                            , objSNSData);
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(ex);
                    }
                });

                t.Start();
            }
        }
        public void Fatal(int account, object message, Exception exception, Project currentProject)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Fatal)
            {
                Thread t = new Thread(delegate()
                {
                    //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Fatal, account);
                    var elmahCon = Elmah.ErrorLog.GetDefault(null);
                    elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), account, LogThreshold.Threshold.Fatal));

                    try
                    {
                        string accountRegion;
                        using (GMGColorContext context = new GMGColorContext())
                        {
                            accountRegion = (from a in context.Accounts where a.ID == account select a.Region).FirstOrDefault();
                        }

                        GMGColor.AWS.AWSSNSData objSNSData = new GMGColor.AWS.AWSSNSData(account, accountRegion, message.ToString(), exception, currentProject.ToString());
                        GMGColor.AWS.AWSSimpleNotificationService.WriteMessage(
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey
                            , GMGColorConfiguration.AppConfiguration.AWSSecretKey
                            , GMGColorConfiguration.AppConfiguration.AWSSNSTopicName
                            , objSNSData);
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error(ex);
                    }
                });

                t.Start();
            }
        }

        public void Debug(Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Debug)
            {
                //ErrorSignal.FromCurrentContext().Raise(string.Empty, exception, LogThreshold.Threshold.Debug);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, string.Empty, 0, LogThreshold.Threshold.Debug));
            }
        }
        public void Debug(object message)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Debug)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), (new Exception()), LogThreshold.Threshold.Debug);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(null/*new Exception()*/, null, message.ToString(), 0, LogThreshold.Threshold.Debug));
            }
        }
        public void Debug(object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Debug)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Debug);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), 0, LogThreshold.Threshold.Debug));
            }
        }
        public void Debug(int account, object message, Exception exception)
        {
            if (GMGColorConfiguration.AppConfiguration.LogLevel <= (int)LogLevel.Debug)
            {
                //ErrorSignal.FromCurrentContext().Raise(message.ToString(), exception, LogThreshold.Threshold.Debug, account);
                var elmahCon = Elmah.ErrorLog.GetDefault(null);
                elmahCon.Log(new Elmah.Error(exception, null, message.ToString(), account, LogThreshold.Threshold.Debug));
            }
        }
    }
}
