﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Net;
using System.Transactions;
using System.Web.Services;
using System.Drawing;
using System.Xml;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using GMGColor.AWS;
using GMGColor.Resources;
using System.Windows.Media.Imaging;

namespace GMGColorDAL.Common
{
    public class GMGColorCommon
    {
        #region Enum

        private enum ResizeMode
        {
            Both,
            Height,
            Width
        }

        public enum ImageType { Original, Thumbnail };

        public enum JobType
        {
            ApprovalJob = 0,
            DeliverJob,
            ValidateICCProfile
        }

        public enum MessageType
        {
            ApprovalJob,
            Download,
            DeliverJob,
            FTPJob,
            TileServerJob,
            ValidateICCProfile,
            SoftProofing,
            SoftProofingCalibrationRead,
            SoftProofingCalibrationWrite
        }

        public enum ApprovalGrigdSortColumn
        {
           Deadline = 0,
           Title = 4,
           PrimaryDecisionMaker = 5,
           PhaseName = 6,
           NextPhase = 7,
           DecisionMakers = 8,
           Version = 9
        }

        #endregion

        #region Fields

        #endregion

        #region Properties


        #endregion

        #region Methods

        public decimal GetUpdatedCurrencyRate(Account account, GMGColorContext context)
        {
            string sysAdminCurrency = (from a in context.Accounts
                                        join act in context.AccountTypes on a.AccountType equals act.ID
                                        join cur in context.Currencies on a.CurrencyFormat equals cur.ID
                                        where act.Key == "GMHQ"
                                        select cur.Code).FirstOrDefault();

            var currency = new Currency
            {
                Amount = 1,
                From = sysAdminCurrency.Trim(),
                To = account.Currency.Code.Trim()
            };

            if (currency.From == currency.To)
            {
                return 1;
            }

            decimal rate = (new CurrencyConvertor()).Convert(currency);
            rate = Math.Round(rate, 2);

            if (AccountType.GetAccountType(account, context) == AccountType.Type.GMGColor)
            {
                rate = 1;
            }
            else
            {
                if (rate != 0)
                {
                    try
                    {
                        using (TransactionScope ts = new TransactionScope())
                        {
                            CurrencyRate objCurrencyRate =
                                (from cr in context.CurrencyRates
                                    where cr.Currency == account.CurrencyFormat
                                    select cr).FirstOrDefault();
                            if (objCurrencyRate != null && objCurrencyRate.Rate != rate)
                            {
                                objCurrencyRate.Rate = rate;
                                context.SaveChanges();
                            }

                            ts.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        GMGColorLogging.log.Error("Error occured while updating the currecy rates", ex);
                    }
                }
            }

            return rate;
        }

        public static bool ClaimDomain(string domainName)
        {
            try
            {
                string domain = GMGColorConfiguration.AppConfiguration.HostAddress.Remove(0, 1);//Remove first dot ('.') fom domain name
                IPHostEntry targeHost = Dns.GetHostEntry(domainName.ToLower());
                IPHostEntry sourceHost = Dns.GetHostEntry(domain);
                return (targeHost.AddressList.Select(o => o.Address.ToString()).SingleOrDefault() == sourceHost.AddressList.Select(o => o.Address.ToString()).SingleOrDefault());
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "BaseController.ClaimDomain : Failed to calim domain {0}. {1}", domainName, ex.Message);
                return false;
            }
        }

        public static bool PingDomain(string customDomain, int accountId)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadString("http://" + customDomain);
                }

                return true;
            }
            catch (Exception ex1)
            {
                try
                {
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://" + customDomain);
                    req.ServerCertificateValidationCallback = delegate { return true; };
                    WebResponse webResponse = req.GetResponse();
                    StreamReader responseStream = new StreamReader(webResponse.GetResponseStream());
                    string res = responseStream.ReadToEnd();

                    return true;
                }
                catch (Exception ex2)
                {
                    //Any exception will returns false.
                    GMGColorLogging.log.Error(string.Format("Ping failed for account {0} and custom domain {1}", accountId, customDomain), ex2);
                    return false;
                }
            }
        }

        public static string ReadServerFile(string absoluteFilePath)
        {
            string fileContent = string.Empty;
            try
            {
                TextReader tr = new StreamReader(absoluteFilePath);
                fileContent = tr.ReadToEnd();
                tr.Close();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Error occured while reading the file " + absoluteFilePath, ex);
            }

            return fileContent;
        }

        #region Image Dimensions

        public static ResizedImage GetResizedImageDimension(int originalWidth, int originalHeight, float newWidth, float newHeight)
        {
            ResizedImage resizedImage = new ResizedImage()
            {
                Height = 0,
                Width = 0
            };

            if (originalHeight > originalWidth)
            {
                if (originalHeight > newHeight)
                {
                    resizedImage.Height = newHeight;
                    resizedImage.Width = (float)Math.Round(((newHeight / originalHeight) * originalWidth), 0);

                    if (resizedImage.Width > newWidth)
                    {
                        resizedImage.Width = newWidth;
                        resizedImage.Height = (float)Math.Round(((newWidth / originalWidth) * originalHeight), 0);
                    }
                }
                else
                {
                    resizedImage.Height = originalHeight;
                    resizedImage.Width = originalWidth;
                }
            }
            else
            {
                if (originalWidth > newWidth)
                {
                    resizedImage.Width = newWidth;
                    resizedImage.Height = (float)Math.Round(((newWidth / originalWidth) * originalHeight), 0);

                    if (resizedImage.Height > newHeight)
                    {
                        resizedImage.Height = newHeight;
                        resizedImage.Width = (float)Math.Round(((newHeight / originalHeight) * originalWidth), 0);
                    }
                }
                else
                {
                    resizedImage.Height = originalHeight;
                    resizedImage.Width = originalWidth;
                }
            }
            return resizedImage;
        }

        public static ResizedImage GetResizedImageDimensionForHeight(int originalWidth, int originalHeight, float newHeight)
        {
            ResizedImage resizedImage = new ResizedImage()
            {
                Height = 0,
                Width = 0
            };

            if (originalHeight > newHeight)
            {
                resizedImage.Height = newHeight;
                resizedImage.Width = (float)Math.Round(((newHeight / originalHeight) * originalWidth), 0);
            }
            else
            {
                resizedImage.Height = originalHeight;
                resizedImage.Width = originalWidth;
            }

            return resizedImage;
        }

        public static ResizedImage GetResizedImageDimensionForWidth(int originalWidth, int originalHeight, float newWidth)
        {
            ResizedImage resizedImage = new ResizedImage()
            {
                Height = 0,
                Width = 0
            };

            if (originalWidth > newWidth)
            {
                resizedImage.Width = newWidth;
                resizedImage.Height = (float)Math.Round(((newWidth / originalWidth) * originalHeight), 0);
            }
            else
            {
                resizedImage.Height = originalHeight;
                resizedImage.Width = originalWidth;
            }

            return resizedImage;
        }

        public static void ResizeImageBasedOnWidthAndHeight(string relativeSourceFilePath, string relativeDestinationFilePath, float newWidth, float newHeight, string accountRegion)
        {
            ResizeImage(relativeSourceFilePath, relativeDestinationFilePath, newWidth, newHeight, ResizeMode.Both, accountRegion);
        }

        public static void ResizeImageBasedOnHeight(string relativeSourceFilePath, string relativeDestinationFilePath, float newHeight, string accountRegion)
        {
            ResizeImage(relativeSourceFilePath, relativeDestinationFilePath, 0, newHeight, ResizeMode.Height, accountRegion);
        }

        public static void ResizeImageBasedOnWidth(string relativeSourceFilePath, string relativeDestinationFilePath, float newWidth, string accountRegion)
        {
            ResizeImage(relativeSourceFilePath, relativeDestinationFilePath, newWidth, 0, ResizeMode.Width, accountRegion);
        }

        public static void CreateThumbnail(string relativeSourceFilePath, string relativeDestinationFilePath, float newWidth, float newHeight, string accountRegion)
        {
            using (Stream originalImageStream = GMGColorIO.GetMemoryStreamOfAFile(relativeSourceFilePath, accountRegion))
            {
                BitmapFrame bi = BitmapFrame.Create(originalImageStream, BitmapCreateOptions.DelayCreation, BitmapCacheOption.OnDemand);

                BitmapSource bs;
                // If this is a photo there should be a thumbnail image, this is VERY fast
                if (bi.Thumbnail != null)
                {
                    bs = bi.Thumbnail;
                    using (Stream stream = new MemoryStream())
                    {
                        BitmapEncoder bitmapEncoder = new BmpBitmapEncoder();
                        bitmapEncoder.Frames.Add(BitmapFrame.Create(bs));
                        bitmapEncoder.Save(stream);
                        stream.Position = 0;
                        GMGColorIO.MoveStream(stream, relativeDestinationFilePath, accountRegion);
                    }
                }
                else
                {
                    ResizeImageBasedOnWidthAndHeight(relativeSourceFilePath, relativeDestinationFilePath, newWidth, newHeight, accountRegion);
                }
            }
        }

        private static void ResizeImage(string relativeSourceFilePath, string relativeDestinationFilePath, float newWidth, float newHeight, ResizeMode mode, string accountRegion)
        {
            Stream originalImageStream = GMGColorIO.GetMemoryStreamOfAFile(relativeSourceFilePath, accountRegion);
            Image FullsizeImage = Bitmap.FromStream(originalImageStream);

            // Prevent using images internal thumbnail
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            FullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

            bool saveoriginal = false;
            switch (mode)
            {
                default:
                case ResizeMode.Both: saveoriginal = (FullsizeImage.Width <= newWidth && FullsizeImage.Height <= newHeight);
                    break;
                case ResizeMode.Height: saveoriginal = (FullsizeImage.Height <= newHeight);
                    break;
                case ResizeMode.Width: saveoriginal = (FullsizeImage.Width <= newWidth);
                    break;
            }

            Stream stream = new MemoryStream();

            if (saveoriginal)
            {
                stream = originalImageStream;
            }
            else
            {
                ResizedImage resizedImage;
                switch (mode)
                {
                    default:
                    case ResizeMode.Both: resizedImage = GetResizedImageDimension(FullsizeImage.Width, FullsizeImage.Height, newWidth, newHeight);
                        break;
                    case ResizeMode.Height: resizedImage = GetResizedImageDimensionForHeight(FullsizeImage.Width, FullsizeImage.Height, newHeight);
                        break;
                    case ResizeMode.Width: resizedImage = GetResizedImageDimensionForWidth(FullsizeImage.Width, FullsizeImage.Height, newWidth);
                        break;
                }
                Image NewImage = FullsizeImage.GetThumbnailImage(GMGColorFormatData.FloatToInt(resizedImage.Width), GMGColorFormatData.FloatToInt(resizedImage.Height), null, IntPtr.Zero);
                FullsizeImage.Dispose();

                System.Drawing.Imaging.ImageFormat imgFormat;
                switch (Path.GetExtension(relativeDestinationFilePath).ToUpper())
                {
                    case ".GIF": imgFormat = System.Drawing.Imaging.ImageFormat.Gif;
                        break;
                    case ".JPG":
                    case ".JPEG":
                    case ".PSD":
                        imgFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                        break;
                    default:
                    case ".PNG": imgFormat = System.Drawing.Imaging.ImageFormat.Png;
                        break;
                }

                NewImage.Save(stream, imgFormat);
            }

            stream.Position = 0;
            GMGColorIO.MoveStream(stream, relativeDestinationFilePath, accountRegion);
            originalImageStream.Close();
            stream.Close();
        }

        public static Image ResizeLocalImage(string originalFilePath, int newWidth, int newHeight)
        {
            Image FullsizeImage = Image.FromFile(originalFilePath);
            Image NewImage = FullsizeImage.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);
            FullsizeImage.Dispose();

            return NewImage;
        }

        #endregion

        public static bool IsImageFile(string Extension)
        {
            return Extension.ToLower() == "gif" || Extension.ToLower() == "jpg" || Extension.ToLower() == "jpeg" || Extension.ToLower() == "png" || Extension.ToLower() == "psd";
        }

        public static bool IsDocumentFile(string extension)
        {
            extension = extension.ToLower();
            return extension == ".doc" || extension == ".docx" || extension == ".ppt" || extension == ".pptx";
        }

        public static bool IsImageFileExtention(string extension)
        {
            extension = extension.ToLower();
            return extension == ".gif" || extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".tiff" || extension == ".tif" || extension == ".eps" || extension == ".psd";
        }

        public static bool IsVideoFile(string Extension)
        {
            Extension = Extension.ToLower();

            if (Extension == ".mp4" || Extension == ".m4v" || Extension == ".flv"
                || Extension == ".f4v" || Extension == ".mov" || Extension == ".mpg"
                || Extension == ".mpg2" || Extension == ".3gp" || Extension == ".wmv"
                || Extension == ".avi" || Extension == ".asf" || Extension == ".rm"
                || Extension == ".ogv" || Extension == ".swf" || Extension == ".vob"
                || Extension == ".xvid" || Extension == ".mjpeg" || Extension == ".3vix")
            {
                return true;
            }

            return false;
        }

        public static bool IsAudioFile(string Extension)
        {
            Extension = Extension.ToLower();

            if (Extension == ".m4a" || Extension == ".wma" || Extension == ".wav" || Extension == ".mp3" || Extension == ".aac" || Extension == ".ogg")
            {
                return true;
            }

            return false;
        }

        public static string EncodeFile(string fileName)
        {
            return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName));
        }

        public static string GetContentType(string fileName)
        {
            string contentType = "unknown/unknown";
            string fileExtension = System.IO.Path.GetExtension(fileName).ToLower();

            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(fileExtension);
            if (rk != null && rk.GetValue("Content Type") != null)
                contentType = rk.GetValue("Content Type").ToString();

            return contentType;
        }

        public static void CreateFileProcessMessage(Dictionary<string, string> dicPara, MessageType type)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode jobNode = doc.CreateElement("Job");
            doc.AppendChild(jobNode);

            foreach (KeyValuePair<string, string> item in dicPara)
            {
                XmlNode attributeNode = doc.CreateElement(item.Key);
                attributeNode.AppendChild(doc.CreateTextNode(item.Value));
                jobNode.AppendChild(attributeNode);
            }

            string jobMessageXml = doc.OuterXml.ToString();
            string messageQueueName = string.Empty;

            switch (type)
            {
                case MessageType.ApprovalJob:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.ApprovalJobQueueName;
                    break;
                case MessageType.DeliverJob:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.DeliverJobQueueName;
                    break;               
                case MessageType.FTPJob:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.FTPJobQueueName;
                    break;
                case MessageType.TileServerJob:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.TileServerQueueName;
                    break;
                case MessageType.SoftProofing:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.SoftProofingQueueName;
                    break;
                case MessageType.SoftProofingCalibrationRead:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationReadQueueName;
                    break;
                case MessageType.SoftProofingCalibrationWrite:
                    messageQueueName = GMGColorConfiguration.AppConfiguration.SoftProofingCalibrationWriteQueueName;
                    break;
            }

            
            if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
            {
                AWSSimpleQueueService.CreateMessage(jobMessageXml, messageQueueName, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
            }
            else
            {
                if (IsQueueAvailable(messageQueueName))
                {
                    MessageQueue mq = new MessageQueue(messageQueueName);
                    Message mm = new Message();
                    mm.Body = jobMessageXml;
                    mq.Send(mm);
                }
                else
                {
                    throw (new GMGColorException("The specified Queue (" + messageQueueName + ") was not found on this server."));
                }
            }
        }

        public static bool IsQueueAvailable(string queueName)
        {
            var queue = new MessageQueue(queueName);
            try
            {
                queue.Peek(TimeSpan.FromMilliseconds(100));
                return true;
            }
            catch (MessageQueueException ex)
            {
                return (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout);
            }
        }

        public static DateTime GetAvalilableDate(int year, int month, int date)
        {
            //TODO Quick Fix Refactor needed
            if (month < 1)
            {
                year--;
                month = 12;
            }
            else if (month > 12)
            {
                year = year + month / 12;
                month = month % 12 != 0 ? month % 12 : 1;
            }

            int daysOfMonth = DateTime.DaysInMonth(year, month);

            DateTime availableDate = new DateTime(year, month, (date > daysOfMonth) ? daysOfMonth : date);

            return availableDate;
        }

        public static string GetModuleName(GMG.CoZone.Common.AppModule module)
        {
            switch (module)
            {
                case GMG.CoZone.Common.AppModule.Collaborate: return Resources.lblCollaborateModuleName;
                case GMG.CoZone.Common.AppModule.Deliver: return Resources.lblDeliverModuleName;
                case GMG.CoZone.Common.AppModule.Admin: return Resources.lblAdministration;
                case GMG.CoZone.Common.AppModule.SysAdmin: return Resources.lblSysAdminRole;
                case GMG.CoZone.Common.AppModule.FileTransfer: return Resources.lblFileTransfer;
                case GMG.CoZone.Common.AppModule.Reports: return Resources.lblReport;
                case GMG.CoZone.Common.AppModule.ProjectFolders: return Resources.lblProjectFolders;
                default: return String.Empty;
            }
        }

        public static string ReplaceInvalidCharactors(string text)
        {
            StringBuilder sb = new StringBuilder();
            //var lastWasInvalid = false;
            foreach (char c in text)
            {
                if (char.IsLetterOrDigit(c))
                {
                    sb.Append(c);
                    //lastWasInvalid = false;
                }
                else
                {
                    //if (!lastWasInvalid)
                    sb.Append("_");
                    //lastWasInvalid = true;
                }
            }

            return sb.ToString().ToLowerInvariant().Trim();
        }

        public static string GetDataFolderHttpPath(string accountRegion, string domainName = "")
        {
            string absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domainName + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion) + "/";
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                absoluteFileUrl = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);
            }

            return absoluteFileUrl;
        }

        public static string GetZipHttpPath(string accountRegion, string domainName = "")
        {
            string absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domainName + "/" + GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion) + "/";
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                absoluteFileUrl = GMGColorConfiguration.AppConfiguration.PathToHtmlDataFolder(accountRegion);
            }

            return absoluteFileUrl;
        }

        public static bool FileExists(string relativeFilePath, string accountRegion, bool checkIfExists = true)
        {
            if (checkIfExists == false)
            {
                return true;
            }

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                return AWSSimpleStorageService.FileExistsInBucket(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), relativeFilePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
            }
            else if (System.IO.File.Exists(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + relativeFilePath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetDefaultImagePath(GMGColorCommon.ImageType type, string domain)
        {
            string absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/";
            switch (type)
            {
                case GMGColorCommon.ImageType.Original:
                default: absoluteFileUrl += "noimage-640px-360px.png";
                    break;
                case GMGColorCommon.ImageType.Thumbnail: absoluteFileUrl += "noimage-240px-135px.png";
                    break;
            }
            return absoluteFileUrl.ToLower();
        }

        #endregion
    }

    public class Currency
    {
        public decimal Amount { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Rate { get; set; }
    }

    public class CurrencyConvertor
    {
        [WebMethod]
        public decimal Convert(Currency currency)
        {
            decimal rate = 0;
            try
            {
                using (var web = new WebClient())
                {
                    string url = string.Format("http://www.google.com/finance/converter?a={2}&from={0}&to={1}",
                        currency.From.ToUpper(), currency.To.ToUpper(), currency.Amount);
                    string response = web.DownloadString(url);

                    int position = response.IndexOf("<span class=bld>");

                    if (position >= 0)
                    {
                        response = response.Substring(position);
                        response = response.Remove(response.IndexOf(currency.To.ToUpper()));
                        response = response.Substring(response.LastIndexOf(">") + 1);
                    }
                    Decimal.TryParse(response, out rate);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Error occured while converting currency with the google server", ex);
            }

            return rate;
        }
    }

    public class ResizedImage
    {
        public float Height { get; set; }
        public float Width { get; set; }
    }

    public static class OrderByHelper
    {
        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> enumerable, string orderBy)
        {
            return enumerable.AsQueryable().OrderBy(orderBy).AsEnumerable();
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> collection, string orderBy)
        {
            foreach (OrderByInfo orderByInfo in ParseOrderBy(orderBy))
                collection = ApplyOrderBy<T>(collection, orderByInfo);

            return collection;
        }

        private static IQueryable<T> ApplyOrderBy<T>(IQueryable<T> collection, OrderByInfo orderByInfo)
        {
            string[] props = orderByInfo.PropertyName.Split('.');
            Type type = typeof(T);

            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);
            string methodName = String.Empty;

            if (!orderByInfo.Initial && collection is IOrderedQueryable<T>)
            {
                if (orderByInfo.Direction == SortDirection.Ascending)
                    methodName = "ThenBy";
                else
                    methodName = "ThenByDescending";
            }
            else
            {
                if (orderByInfo.Direction == SortDirection.Ascending)
                    methodName = "OrderBy";
                else
                    methodName = "OrderByDescending";
            }

            //TODO: apply caching to the generic methodsinfos?
            return (IOrderedQueryable<T>)typeof(Queryable).GetMethods().Single(
                method => method.Name == methodName
                        && method.IsGenericMethodDefinition
                        && method.GetGenericArguments().Length == 2
                        && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(T), type)
                .Invoke(null, new object[] { collection, lambda });

        }

        private static IEnumerable<OrderByInfo> ParseOrderBy(string orderBy)
        {
            if (String.IsNullOrEmpty(orderBy))
                yield break;

            string[] items = orderBy.Split(',');
            bool initial = true;
            foreach (string item in items)
            {
                string[] pair = item.Trim().Split(' ');

                if (pair.Length > 2)
                    throw new ArgumentException(String.Format("Invalid OrderBy string '{0}'. Order By Format: Property, Property2 ASC, Property2 DESC", item));

                string prop = pair[0].Trim();

                if (String.IsNullOrEmpty(prop))
                    throw new ArgumentException("Invalid Property. Order By Format: Property, Property2 ASC, Property2 DESC");

                SortDirection dir = SortDirection.Ascending;

                if (pair.Length == 2)
                    dir = ("desc".Equals(pair[1].Trim(), StringComparison.OrdinalIgnoreCase) ? SortDirection.Descending : SortDirection.Ascending);

                yield return new OrderByInfo() { PropertyName = prop, Direction = dir, Initial = initial };

                initial = false;
            }

        }

        private class OrderByInfo
        {
            public string PropertyName { get; set; }
            public SortDirection Direction { get; set; }
            public bool Initial { get; set; }
        }

        private enum SortDirection
        {
            Ascending = 0,
            Descending = 1
        }
    }
}