//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class BillingPlanType
    {
        public BillingPlanType()
        {
            this.BillingPlans = new HashSet<BillingPlan>();
        }
    
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int MediaService { get; set; }
        public bool EnablePrePressTools { get; set; }
        public bool EnableMediaTools { get; set; }
        public int AppModule { get; set; }
        public Nullable<int> Account { get; set; }
    
        public virtual Account Account1 { get; set; }
        public virtual AppModule AppModule1 { get; set; }
        public virtual ICollection<BillingPlan> BillingPlans { get; set; }
        public virtual MediaService MediaService1 { get; set; }
    }
}
