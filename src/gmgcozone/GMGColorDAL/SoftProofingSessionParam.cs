//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SoftProofingSessionParam
    {
        public SoftProofingSessionParam()
        {
            this.SoftProofingSessionErrors = new HashSet<SoftProofingSessionError>();
        }
    
        public int ID { get; set; }
        public int Approval { get; set; }
        public string SessionGuid { get; set; }
        public string ActiveChannels { get; set; }
        public bool SimulatePaperTint { get; set; }
        public string OutputRGBProfileName { get; set; }
        public int ZoomLevel { get; set; }
        public string ViewTiles { get; set; }
        public System.DateTime LastAccessedDate { get; set; }
        public Nullable<int> ViewingCondition { get; set; }
        public bool DefaultSession { get; set; }
        public bool HighResolution { get; set; }
        public bool HighResolutionInProgress { get; set; }
    
        public virtual Approval Approval1 { get; set; }
        public virtual ICollection<SoftProofingSessionError> SoftProofingSessionErrors { get; set; }
        public virtual ViewingCondition ViewingCondition1 { get; set; }
    }
}
