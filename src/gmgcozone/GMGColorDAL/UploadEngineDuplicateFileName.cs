//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class UploadEngineDuplicateFileName
    {
        public UploadEngineDuplicateFileName()
        {
            this.UploadEngineAccountSettings = new HashSet<UploadEngineAccountSetting>();
        }
    
        public int ID { get; set; }
        public int Key { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<UploadEngineAccountSetting> UploadEngineAccountSettings { get; set; }
    }
}
