﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Transactions;

namespace GMGColorDAL
{
    public partial class Email: BusinessObjectBase
    {
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static bool SaveEmail(string fromName, string fromEmail, string toName, string toEmail, string toCC, string subject, string bodyText, string bodyHtml, GMGColorContext context, bool saveChanges = true)
        {
            bool result = true;
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    Email item = new Email()
                                     {
                                         fromName = fromName,
                                         fromEmail = fromEmail,
                                         toName = toName,
                                         toEmail = toEmail,
                                         toCC = toCC ?? string.Empty,
                                         subject = subject ?? string.Empty,
                                         bodyText = bodyText ?? string.Empty,
                                         bodyHtml = bodyHtml ?? string.Empty,
                                         LastErrorMessage = string.Empty,
                                         TryCount = 0,
                                         ProcessingInProgress = null
                                     };

                    context.Emails.Add(item);
                    if (saveChanges)
                    {
                        context.SaveChanges();
                    }
                    ts.Complete();
                }
            }
            catch (DbEntityValidationException dex)
            {
                DALUtils.LogDbEntityValidationException(dex);
                result = false;
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                result = false;
            }
            return result;
        }

        #endregion
    }
}
