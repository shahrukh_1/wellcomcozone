using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class NotificationFrequency : BusinessObjectBase
    {
        #region Enums

        public enum Frequency
        {
            As_Events_Occur,
            Daily,
            Weekly,
            Never,
            PerProofStudioSession
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static string GetKey(Frequency notificationName)
        {
            switch (notificationName)
            {
                case Frequency.Daily: return "DLY";
                case Frequency.Weekly: return "WLY";
                case Frequency.As_Events_Occur: return "AEO";
                case Frequency.Never: return "NVR";
            }
            return String.Empty;
        }

        public static Frequency GetNotificationFrequency(int notificationFrequency, GMGColorContext context)
        {
            try
            {
                string key =
                    (from nf in context.NotificationFrequencies where nf.ID == notificationFrequency select nf.Key).
                        FirstOrDefault();
                return NotificationFrequency.GetNotificationFrequency(key);
            }
            finally
            {
            }
        }

        public static Frequency GetNotificationFrequency(NotificationFrequency objNotificationFrequency)
        {
            return NotificationFrequency.GetNotificationFrequency(objNotificationFrequency.Key);
        }

        public static Frequency GetNotificationFrequency(string key)
        {
            switch (key)
            {
                case "NVR": return Frequency.Never;
                case "AEO": return Frequency.As_Events_Occur;
                case "DLY": return Frequency.Daily;
                case "WLY":
                default: return Frequency.Weekly;
            }
        }

        public static NotificationFrequency GetNotificationFrequency(Frequency notificationFrequency, GMGColorContext context)
        {
            try
            {
                return (from nf in context.NotificationFrequencies
                        where
                            (
                                (notificationFrequency == Frequency.Never && nf.Key == "NVR") ||
                                (notificationFrequency == Frequency.As_Events_Occur && nf.Key == "AEO") ||
                                (notificationFrequency == Frequency.Daily && nf.Key == "DLY") ||
                                (notificationFrequency == Frequency.Weekly && nf.Key == "WLY")
                            )
                        select nf).FirstOrDefault() ??
                       (from nf in context.NotificationFrequencies where (nf.Key == "WLY") select nf).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static string GetLocalizedNotificationFrequencyName(int notificationFrequency, GMGColorContext context)
        {
            try
            {
                string key = (from nf in context.NotificationFrequencies where nf.ID == notificationFrequency select nf.Key).FirstOrDefault();
                return NotificationFrequency.GetLocalizedNotificationFrequencyName(NotificationFrequency.GetNotificationFrequency(key));
            }
            finally
            {
            }
        }

        public static string GetLocalizedNotificationFrequencyName(NotificationFrequency objNotificationFrequency)
        {
            return NotificationFrequency.GetLocalizedNotificationFrequencyName(NotificationFrequency.GetNotificationFrequency(objNotificationFrequency));
        }

        public static string GetLocalizedNotificationFrequencyName(Frequency notificationFrequency)
        {
            switch (notificationFrequency)
            {
                case Frequency.Never: return Resources.resNotificationFreqNever;
                case Frequency.As_Events_Occur: return Resources.resNotificationFreqAsEventOccur;
                case Frequency.Daily: return Resources.resNotificationFreqDaily;
                case Frequency.Weekly: return Resources.resNotificationFreqWeekly;
                default: return null;
            }
        }

        public static Dictionary<int, string> GetLocalizedNotificationFrequencyNames(GMGColorContext context)
        {
            try
            {
                return (from nf in context.NotificationFrequencies
                        select new
                                   {
                                       nf.ID,
                                       nf.Key
                                   }
                       ).ToDictionary(o => o.ID,
                                      o => NotificationFrequency.GetLocalizedNotificationFrequencyName(
                                          NotificationFrequency.GetNotificationFrequency(o.Key)));
            }
            finally
            {
            }
        }

        #endregion
    }
}