using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class RolePresetEventType : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static bool IsChecked(int eventTypeId, int presetId, int userId, List<int> roles, GMGColorContext context)
        {
            int customPresetId = (from p in context.Presets where p.Key == "C" select p.ID).FirstOrDefault();

            if (presetId != customPresetId)
            {
                return (from rpet in context.RolePresetEventTypes
                        where
                            roles.Contains(rpet.Role) && rpet.Preset == presetId &&
                            rpet.EventType == eventTypeId
                        select rpet.ID).Any();
            }
            else
            {
                return (from ucpetv in context.UserCustomPresetEventTypeValues
                        join rpet in context.RolePresetEventTypes on ucpetv.RolePresetEventType equals rpet.ID
                        join et in context.EventTypes on rpet.EventType equals et.ID
                        where
                            et.ID == eventTypeId &&
                            rpet.Preset == presetId &&
                            ucpetv.User == userId
                        select ucpetv.Value).FirstOrDefault();
            }
        }

        public static bool HasNotifications(int userId, List<int> eventGrpIds, GMGColorContext context)
        {
            return (from rpet in context.RolePresetEventTypes
                    join et in context.EventTypes on rpet.EventType equals et.ID
                    join ur in context.UserRoles on rpet.Role equals ur.Role
                    where ur.User == userId && eventGrpIds.Contains(et.EventGroup)
                    select rpet.ID).Any();
        }

        #endregion
    }
}

