using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class Role : BusinessObjectBase
    {
        #region Enums

        public enum RoleName
        {
            /// <summary>
            /// GMG Color HQ Addministrator
            /// </summary>
            GMGAdministrator,
            /// <summary>
            /// GMG Color HQ Manager Role
            /// </summary>
            GMGManager,
            /// <summary>
            /// Account Administrator Role
            /// </summary>
            AccountAdministrator,
            /// <summary>
            /// Account Manager Role
            /// </summary>
            AccountManager,
            /// <summary>
            /// Account Moderator Role
            /// </summary>
            AccountModerator,
            /// <summary>
            /// Account Contributor role
            /// </summary>
            AccountContributor,
            /// <summary>
            /// Account Viewer Role
            /// </summary>
            AccountViewer,
            /// <summary>
            /// Account Collaborate Retoucher Role
            /// </summary>
            AccountRetoucher,
            /// <summary>
            /// Account None Role
            /// </summary>
            AccountNone,
            /// <summary>
            /// DataInput Role
            /// </summary>
            DataInput,
            /// <summary>
            /// Reporting Role
            /// </summary>
            Reporting,
            /// <summary>
            /// colloborate role
            /// </summary>
            Manager
        };

        #endregion

        #region Constants 

        public const string NoAccessRole = "NoAccess";

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static RoleName GetRole(int role, GMGColorContext context)
        {
            string key = (from r in context.Roles where r.ID == role select r.Key).FirstOrDefault();
            return Role.GetRole(key);
        }

        public static RoleName GetRole(Role objRole)
        {
            return Role.GetRole(objRole.Key);
        }

        public static RoleName GetRole(string key)
        {
            switch (key)
            {
                case "HQA": return RoleName.GMGAdministrator;
                case "HQM": return RoleName.GMGManager;
                case "ADM": return RoleName.AccountAdministrator;
                case "MAN": return RoleName.AccountManager;
                case "MOD": return RoleName.AccountModerator;
                case "CON": return RoleName.AccountContributor;
                case "VIE": return RoleName.AccountViewer;
                case "REP": return RoleName.Reporting;
                case "DIN": return RoleName.DataInput;
                case "RET":return RoleName.AccountRetoucher;

                case "NON":
                default:
                    return RoleName.AccountNone;
            }
        }

        public static string GetRoleKey(RoleName role)
        {
            switch (role)
            {
                case RoleName.GMGAdministrator: return "HQA";
                case RoleName.GMGManager: return "HQM";
                case RoleName.AccountAdministrator: return "ADM";
                case RoleName.AccountManager: return "MAN";
                case RoleName.AccountModerator: return "MOD";
                case RoleName.AccountContributor: return "CON";
                case RoleName.AccountViewer: return "VIE";
                case RoleName.Reporting: return "REP";
                case RoleName.DataInput: return "DIN";
                case RoleName.AccountRetoucher: return "RET";
                default:
                    return "NON";
            }
        }

        public static string GetLocalizedRoleName(int role, GMGColorContext context)
        {
            RoleName objRole = GetRole(role, context);
            return Role.GetLocalizedRoleName(objRole);
        }

        public static string GetLocalizedRoleName(Role objRole)
        {
            return Role.GetLocalizedRoleName(Role.GetRole(objRole));
        }

        public static string GetLocalizedRoleName(RoleName userRole)
        {
            switch (userRole)
            {
                case RoleName.GMGAdministrator: return Resources.resRoleGMGAdministrator;
                case RoleName.GMGManager: return Resources.resRoleGMGManager;
                case RoleName.AccountAdministrator: return Resources.resRoleAccountAdministrator;
                case RoleName.AccountManager: return Resources.resRoleAccountManager;
                case RoleName.AccountModerator: return Resources.resRoleAccountModerator;
                case RoleName.AccountContributor: return Resources.resRoleAccountContributor;
                case RoleName.AccountViewer: return Resources.resRoleAccountViewer;
                case RoleName.AccountRetoucher: return Resources.resRoleAccountRetoucher;
                default: return null;
            }
        }

        public static string GetLocalizedRoleName(string key)
        {
            return Role.GetLocalizedRoleName(Role.GetRole(key));
        }

        public static Dictionary<int, string> GetLocalizedRoleNames(GMGColorContext context)
        {
            return context.Roles.ToDictionary(o => o.ID, o => Role.GetLocalizedRoleName(o.ID, context));
        }

        public static List<int> GetRoles(int userId, GMGColorContext context)
        {
            return (from ur in context.UserRoles where ur.User == userId select ur.Role).ToList();
        }

        #endregion
    }
}

