using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class AccountSubscriptionPlan : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static List<AccountSubscriptionPlan> GetSubscriptionPlanPerAccount(int accountId, GMGColorContext context)
        {
            try
            {
                return (from asp in context.AccountSubscriptionPlans
                        where asp.Account == accountId
                        select asp).ToList();
            }
            finally
            {
            }
        }
        #endregion
    }
}

