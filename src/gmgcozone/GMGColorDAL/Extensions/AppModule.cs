using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class AppModule : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static int GetModuleId(GMG.CoZone.Common.AppModule moduleKey, GMGColorContext context)
        {
            try
            {
                return (from am in context.AppModules where am.Key == (int) moduleKey select am.ID).FirstOrDefault();
            }
            catch(Exception ex)
            {
                GMGColorLogging.log.Error("Could not get module id, error: " + ex.Message, ex);
                return default(int);
            }
        }
        #endregion
    }
}

