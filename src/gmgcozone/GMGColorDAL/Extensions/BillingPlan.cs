using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using RSC = GMGColor.Resources;

namespace GMGColorDAL
{
    public class BillingPlan_Validation
    {
        [Required(ErrorMessageResourceName = "reqPlanType", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int Type { get; set; }

        [Required(ErrorMessageResourceName = "reqPlanName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Name { get; set; }

        [RegularExpression(Constants.PlanPriceRegex, ErrorMessageResourceType = typeof(RSC.Resources), ErrorMessageResourceName = "revBillingPlanPriceValidationMsg")]
        public decimal Price { get; set; }

        [Range(0, BillingPlan.MaxProofs, ErrorMessageResourceType = typeof(RSC.Resources), ErrorMessageResourceName = "revBillingPlanProofsValidationMsg")]
        public int? Proofs { get; set; }
    }

    [MetadataType(typeof(BillingPlan_Validation))]
    public partial class BillingPlan : BusinessObjectBase
    {
        public bool CanBeDeleted { get; set; }

        public enum FreePlans
        {
            /// <summary>
            /// Key = Essentials Free
            /// </summary>
            Essentials_Free,
            /// <summary>
            /// Key = PrePress Free
            /// </summary>
            PrePress_Free,
            Other
        }

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static BillingPlanInfo FillBillingPlanData(BillingPlan billingPlan, Account account, bool isNewAccountPlans, bool isEditAccountPlans, decimal rate, AccountType.Type accountType, AccountType.Type parentAccountType, GMGColorContext context)
        {
            try
            {
                //TODO Check in depth for child accounts when removing a plan for a parent account bigger then 2 levels in depth
                BillingPlanInfo billingPlanInfo = new BillingPlanInfo
                {
                    ID = billingPlan.ID,
                    Name = billingPlan.Name,
                    IsFixed = billingPlan.IsFixed,
                    IsDemo = billingPlan.IsDemo,
                    Storage = billingPlan.MaxStorageInGb.GetValueOrDefault(),
                    Account = billingPlan.Account
                };

                AttachedAccountBillingPlan objAttachedAccountBillingPlan =
                    (from aabp in context.AttachedAccountBillingPlans
                     where aabp.Account == account.ID && aabp.BillingPlan == billingPlan.ID
                     select aabp).FirstOrDefault();

                var newProofPrice = objAttachedAccountBillingPlan != null
                    ? objAttachedAccountBillingPlan.NewProofPrice
                    : 0;

                billingPlanInfo.HasChildren = false;

                if (isNewAccountPlans)
                {
                    billingPlanInfo.SelectAttachedPlans = false;
                    billingPlanInfo.Price = AttachedAccountBillingPlan.GetPlanPrice(account.ID, billingPlan.ID, billingPlan.Price, accountType, context);
                    billingPlanInfo.PricePerYear = (decimal)(billingPlanInfo.Price * 12);
                    billingPlanInfo.DiscountedPrice = 0;
                    billingPlanInfo.Proofs = billingPlan.Proofs.GetValueOrDefault();
                    //billingPlanInfo.CostPerProof = (decimal)(billingPlanInfo.Proofs > 0 ? (billingPlanInfo.Price / billingPlanInfo.Proofs) : 0);
                    billingPlanInfo.CostPerProof = AttachedAccountBillingPlan.GetCostPerProof(newProofPrice, billingPlan, accountType);

                    billingPlanInfo.AdjustedPrice = 0;
                    billingPlanInfo.AdjustedCostPerProof = 0;
                }
                else if (isEditAccountPlans)
                {
                    billingPlanInfo.SelectAttachedPlans = (objAttachedAccountBillingPlan != null);
                    billingPlanInfo.HasChildren = (objAttachedAccountBillingPlan != null) &&
                                                  (objAttachedAccountBillingPlan.BillingPlan1.
                                                       AccountSubscriptionPlans.Select(
                                                           p => p.Account1).Any(
                                                               m => m.Parent != null && m.Parent.Value == account.ID && m.AccountStatu.Key != "D") ||
                                                   objAttachedAccountBillingPlan.BillingPlan1.
                                                       SubscriberPlanInfoes.Select(
                                                           p => p.Account1).Any(
                                                               m => m.Parent != null && m.Parent.Value == account.ID && m.AccountStatu.Key != "D"));

                    billingPlanInfo.Price = AttachedAccountBillingPlan.GetPlanPrice(account.Parent.GetValueOrDefault(), billingPlan.ID, context, rate);
                    billingPlanInfo.PricePerYear = (decimal)(billingPlanInfo.Price * 12);
                    billingPlanInfo.DiscountedPrice = (decimal)(billingPlanInfo.Price - (billingPlanInfo.Price * (decimal)((account.GPPDiscount != null) ? account.GPPDiscount / 100 : 0)));
                    billingPlanInfo.Proofs = billingPlan.Proofs.GetValueOrDefault();
                    //billingPlanInfo.CostPerProof = (decimal)(billingPlanInfo.Proofs > 0 ? (billingPlanInfo.Price / billingPlanInfo.Proofs) : 0);
                    billingPlanInfo.CostPerProof = AttachedAccountBillingPlan.GetCostPerProof(account.Parent.GetValueOrDefault(), billingPlan.ID, billingPlan.IsFixed, context, rate);

                    billingPlanInfo.AdjustedPrice = 0;
                    billingPlanInfo.AdjustedCostPerProof = 0;
                }
                else //View Account Plans
                {
                    billingPlanInfo.AdjustedPrice = AttachedAccountBillingPlan.GetPlanPrice(account.ID, billingPlan.ID, billingPlan.Price, accountType, context, rate);
                    billingPlanInfo.Price = AttachedAccountBillingPlan.GetPlanPrice(account.Parent.GetValueOrDefault(), billingPlan.ID, billingPlan.Price, parentAccountType, context, rate);
                    billingPlanInfo.AdjustedCostPerProof = AttachedAccountBillingPlan.GetCostPerProof(newProofPrice, billingPlan, accountType);
                    billingPlanInfo.CostPerProof = AttachedAccountBillingPlan.GetCostPerProof(account.Parent.GetValueOrDefault(), billingPlan, parentAccountType, context);
                    billingPlanInfo.IsModifiedProofPrice = objAttachedAccountBillingPlan != null &&
                                                           objAttachedAccountBillingPlan.IsModifiedProofPrice;

                    decimal discount = (decimal)((account.GPPDiscount != null) ? account.GPPDiscount / 100 : 0);
                    billingPlanInfo.PricePerYear = (decimal)(billingPlanInfo.Price * 12);
                    billingPlanInfo.DiscountedPrice = (decimal)(billingPlanInfo.Price - (billingPlanInfo.Price * discount));
                    billingPlanInfo.Proofs = billingPlan.Proofs.GetValueOrDefault();
                    //billingPlanInfo.CostPerProof = (decimal)(billingPlanInfo.Proofs > 0 ? (billingPlanInfo.AdjustedPrice / billingPlanInfo.Proofs) : 0);
                }

                return billingPlanInfo;
            }
            finally
            {
            }
        }

        public static BillingPlanType.Type GetBillingPlanType(int billingPlanId, GMGColorContext context)
        {
            try
            {
                return GMGColorDAL.BillingPlanType.GetBillingPlanType((from bp in context.BillingPlans
                                                                       join bpt in context.BillingPlanTypes on
                                                                           bp.BillingPlanType.ID
                                                                           equals bpt.ID
                                                                       where bp.ID == billingPlanId
                                                                       select bpt.Key).FirstOrDefault());
            }
            finally
            {
            }
        }

        public static bool IsAccountInAFreePlan(BillingPlan billingPlan)
        {
            return BillingPlan.IsAccountInAFreePlan(billingPlan.Price);
        }

        public static bool IsAccountInAFreePlan(int billingPlan, GMGColorContext context)
        {
            try
            {
                return BillingPlan.IsAccountInAFreePlan((from bp in context.BillingPlans where bp.ID == billingPlan select bp.Price).FirstOrDefault());
            }
            finally
            {
            }
        }

        public static bool IsAccountInAFreePlan(decimal price)
        {
            return price == 0 ? true : false;
        }

        public static string GetFormatedCostPerProof(int billingPlan, Account objAccount, GMGColorContext context)
        {
            bool isFixed = (from bp in context.BillingPlans where bp.ID == billingPlan select bp.IsFixed).FirstOrDefault();
            if (!isFixed)
            {
                decimal costPerProof = AttachedAccountBillingPlan.GetCostPerProof(objAccount.Parent.Value, billingPlan, context);
                return GMGColorFormatData.GetFormattedCurrency(costPerProof, objAccount.Parent.GetValueOrDefault(), context);
            }
            else
            {
                return BillingPlan.GetProofCount(null, true);
            }
        }

        public static string GetFormatedCostPerProof(int billingPlan, Account objAccount, string currency, bool isFixed, GMGColorContext context)
        {
            if (!isFixed)
            {
                decimal costPerProof = AttachedAccountBillingPlan.GetCostPerProof(objAccount.Parent.Value, billingPlan, isFixed, context);
                return GMGColorFormatData.GetFormattedCurrency(costPerProof, currency);
            }
            else
            {
                return BillingPlan.GetProofCount(null, true);
            }
        }

        public static string GetProofCount(int? proofs, bool isFixed)
        {
            if (isFixed)
            {
                return "-";
            }
            else
            {
                return (proofs != null) ? proofs.ToString() : "0";
            }
        }

        public static string GetCostPerProof(int? proofs, decimal price, bool isFixed, Account account, GMGColorContext context)
        {
            if (isFixed)
            {
                return "-";
            }
            else
            {
                decimal costPerProof = ((proofs != null && proofs != 0) ? (price / proofs.Value) : 0);
                return BillingPlan.GetCostPerProof(costPerProof, account, isFixed, context);
            }
        }

        public static string GetCostPerProof(decimal costPerProof, Account objAccount, bool isFixed, GMGColorContext context)
        {
            if (isFixed)
            {
                return "-";
            }
            else
            {
                return GMGColorFormatData.GetFormattedCurrency(costPerProof, objAccount.ID, context);
            }
        }

        public static string GetCostPerProof(int? proofs, decimal price, bool isFixed, string currenySymbol)
        {
            if (isFixed)
            {
                return "-";
            }
            else
            {
                decimal costPerProof = ((proofs != null && proofs != 0) ? (price / proofs.Value) : 0);
                return BillingPlan.GetCostPerProof(costPerProof, isFixed, currenySymbol);
            }
        }

        public static string GetCostPerProof(decimal costPerProof, bool isFixed, string currenySymbol)
        {
            if (isFixed)
            {
                return "-";
            }
            else
            {
                return GMGColorFormatData.GetFormattedCurrency(costPerProof, currenySymbol);
            }
        }

        /// <summary>
        /// Returns a list of billing plans
        /// </summary>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static List<BillingPlan> GetBilingPlans(int? accId, GMGColorContext context)
        {
            var plans = (from bp in context.BillingPlans
                         where bp.Account == accId
                         select bp).ToList();

            foreach (var billingPlan in plans)
            {
                billingPlan.CanBeDeleted = CanDeleteBilingPlan(billingPlan);
            }

            return plans;
        }

        #endregion

        #region private

        /// <summary>
        /// Returns true (can be deleted) if current billing plan has no relation with any account, otherwise returns false
        /// </summary>
        /// <param name="item">Current billing plan</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        private static bool CanDeleteBilingPlan(BillingPlan item)
        {
            //if there are no relations between billing plan and accounts, then current billing plan can pe deleted
            if ((item.AccountBillingPlanChangeLogs.Count == 0) &&
               (item.AccountSubscriptionPlans.Count == 0) &&
               (item.SubscriberPlanInfoes.Count == 0) &&
               (item.AttachedAccountBillingPlans.Count == 0))
            {
                return true;
            }

            return false;
        }

        #endregion

        public const int MaxProofs = 2147483647;
        public const decimal MaxPrice = 999999.99M;
       
    }
}

