﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class ApprovalPhase : BusinessObjectBase
    {
        #region OnPropertyChange Methods

        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Enums

        /// <summary>
        /// Enumeration of available  Approval Statuses
        /// </summary>
        public enum ApprovalDecisionType
        {
            PrimaryDecisionMaker = 1,
            ApprovalGroup
        }

        /// <summary>
        /// Enumeration for Approval Decision
        /// </summary>
        public enum ApprovalDecisionSatusType
        {
            Pending = 1,
            ChangesRequired,
            ApprovedwithChanges,
            Approved,
            NotApplicable

        }

        #endregion

        #region Extension Methods

        /// <summary>
        /// Returns whether or not the approval phase name is unique at the workflow level
        /// </summary>
        /// <param name="workflowId">Workflow id</param>
        /// <param name="phaseName">Approval phase name</param>
        /// <param name="id">The id.</param>
        /// <param name="context">Database context.</param>
        /// <returns>
        ///  <c>true</c> if [is approval phase name unique] [the specified approval phase name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsApprovalPhaseNameUnique(int workflowId, string phaseName, int id, GMGColorContext context)
        {
            if (!string.IsNullOrEmpty(phaseName))
            {
                return !(from ap in context.ApprovalPhases
                         where ap.Name.ToLower() == phaseName.ToLower().Trim() && ap.ID != id && ap.ApprovalWorkflow == workflowId
                         select ap.ID).Any();
            }
            return true;
        }

        public static string GetPhaseDeadlineTriggerLabel(int triggerKey)
        {
            switch (triggerKey)
            {
                case (int)GMG.CoZone.Common.PhaseDeadlineTrigger.AfterUpload:
                    return Resources.lblPhaseTriggerAfterUpload;
                case (int)GMG.CoZone.Common.PhaseDeadlineTrigger.LastPhaseCompleted:
                    return Resources.lblPhaseTriggerLastPhaseCompleted;
                default:
                    return Resources.lblPhaseTriggerApprovalDeadline;
            }
        }

        public static string GetPhaseDeadlineUnitLabel(int unitKey)
        {
            switch (unitKey)
            {
                case (int)GMG.CoZone.Common.PhaseDeadlineUnit.Days:
                    return Resources.lblPhaseUnitDays;
                default:
                    return Resources.lblPhaseUnitHours;
            }
        }

        #endregion
    }
}
