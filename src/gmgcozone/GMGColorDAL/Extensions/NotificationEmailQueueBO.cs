namespace GMGColorDAL
{
    public partial class NotificationEmailQueue : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        // Put methods to manipulate Business Objects here. Add, Update etc

       /*public static void ProcessNotificationEmailQueue(GMGColorContext context, string logEmailTemplate)
       {
          LogSchedulerBO objLogScheduler = new LogSchedulerBO(context);
           objLogScheduler.IsDeleted = false;

           foreach (LogSchedulerBO logItem in objLogScheduler.SearchObjects())
           {
               string toName = logItem.objCreator.GivenName + " " + logItem.objCreator.FamilyName;
               string toEmail = logItem.objCreator.EmailAddress;
               string subject = "Log Report";
               string bodyHtml = logEmailTemplate;
               string logMessageHtml = string.Empty;
               string toCC = string.Empty;
               int dateFrequency = 1;
               Account objAccount = logItem.objCreator.UserCompanysWhereThisIsUser[0].objCompany.objAccount;
               DateTime startDate = DateTime.Now;
               DateTime endDate = DateTime.Now;
               string hostUrl = objAccount.AccountURL;

               switch (logItem.Frequency)
               {
                   case "Daily":
                       {
                           dateFrequency = 1;
                           break;
                       }
                   case "Weekly":
                       {
                           dateFrequency = 7;
                           break;
                       }
                   case "Fortnightly":
                       {
                           dateFrequency = 14;
                           break;
                       }
                   case "Monthly":
                       {
                           dateFrequency = 30;
                           break;
                       }
               }

               if (logItem.LogSchedulerEmailDeliverysWhereThisIsLogScheduler.Count > 0)
               {
                   LogSchedulerEmailDeliveryBO objLogEmailDelivery = logItem.LogSchedulerEmailDeliverysWhereThisIsLogScheduler.Last();
                   if (objLogEmailDelivery.CreatedDate.AddDays(dateFrequency) != DateTime.Now)
                   {
                       continue;
                   }
               }

               switch (logItem.DateSelection)
               {
                   case "Custom":
                       {
                           startDate = DateTime.Parse(logItem.FromDate.Value.ToString());
                           endDate = DateTime.Parse(logItem.ToDate.Value.ToString());
                           break;
                       }
                   case "Today":
                       {
                           startDate = DateTime.Now.AddDays(-1);
                           endDate = DateTime.Now;
                           break;
                       }
                   case "Yesterday":
                       {
                           startDate = DateTime.Now.AddDays(-2);
                           endDate = DateTime.Now.AddDays(-1);
                           break;
                       }
                   case "Last Week":
                       {
                           startDate = DateTime.Now.AddDays(-7);
                           endDate = DateTime.Now;
                           break;
                       }
                   case "Last Month":
                       {
                           startDate = DateTime.Now.AddDays(-30);
                           endDate = DateTime.Now;
                           break;
                       }
               }

               HistoryActionBO objHistoryAction = new HistoryActionBO(context);
               List<ReturnFilteredLogInfoViewBO> lstFilterLogInfo = objHistoryAction.GetReturnFilteredLogInfo(logItem.Accounts, logItem.Suppliers, logItem.Users, logItem.Campaigns, logItem.Activities, logItem.HistoryActions, startDate, endDate);

               if (logItem.IsHideMyActivity)
                   lstFilterLogInfo = lstFilterLogInfo.Where(p => lstFilterLogInfo.All(q => p.Creator != logItem.Creator)).ToList<ReturnFilteredLogInfoViewBO>();

               //bodyHtml = "<div><h1>Log Report</h1><p>" + DateTime.Now.ToString("dd MMMM yyyy") + "</p><ul><li><ul>";
               bodyHtml = bodyHtml.Replace("<$dateToday$>", DateTime.Now.ToString("dd MMMM yyyy"));

               foreach (ReturnFilteredLogInfoViewBO lstItem in lstFilterLogInfo)
               {
                   User objCreator = new User(context);
                   objCreator.ID = lstItem.Creator.Value;
                   objCreator.GetObject();

                   string profilePicturePath = WebvolumeConfiguration.AppConfiguration.DataFolderName + "\\Users\\" + objCreator.Guid + "\\thumb_" + objCreator.PhotoPath;
                   string publishImagePath = string.Empty;

                   if (File.Exists(WebvolumeConfiguration.AppConfiguration.PathToProjectFolder + "\\" + profilePicturePath))
                   {
                       publishImagePath = "http://" + hostUrl + "/" + profilePicturePath.Replace("\\", "/");
                   }
                   else
                   {
                       publishImagePath = "http://" + hostUrl + "/" + WebvolumeConfiguration.AppConfiguration.DataFolderName + "/Users/nouser-32px-32px.png";
                   }

                   logMessageHtml += "<li class=\"history notification\" style=\"border-top:1px solid #e5e5e5;list-style:none;margin:0;padding:10px 10px 8px;\"><ul style=\"margin: 0 0 0 0px !important;padding:0;width:auto;\">"
                       + "<li class=\"img\" style\"padding: 0 0;clear:none;margin: 0 0 0;overflow: hidden;padding: 0;border-bottom: 0px !important;\"><img style=\"float:left; margin-right:0px; height:38px; width:38px; background:#f5f5f5; border:1px solid #e5e5e5!important; display:inline; padding:0px; vertical-align:middle;\" src=\"" + publishImagePath + "\" alt=\"Profile Photo\" class=\"avatar32\" id=\"\"> </li>"
                       + "<li class=\"log_timestamp\" style\"clear:none;overflow:hidden;padding:0;border-bottom:0px !important; margin:0px 5px 0 30px; line-height:normal; float:right;text-align:right;line-height:16px;color:#999;font-size:11px;\">" + lstItem.CreatedDateTime.Value.ToString("MMMM dd, yyyy") + "<br>" + lstItem.CreatedDateTime.Value.ToShortTimeString() + "</li>"
                       + "<li class=\"log_message\" style\"padding:0 0;clear:none;margin:0 0 0 45px; overflow:hidden; padding:0; border-bottom:0px !important;\"><h6 style\"line-height:18px\">" + objCreator.GivenName + " " + objCreator.FamilyName + "</h6><p class=\"log_event\" style=\"line-height:18px;margin:0;padding:0;\">" + Regex.Replace(lstItem.Message, @"<(.|\n)*?>", string.Empty) + "</p></li>"
                       + "</ul></li>";
               }

               bodyHtml = bodyHtml.Replace("<$notificationMessage$>", logMessageHtml);
               bodyHtml = bodyHtml.Replace("<$accountURL$>", objAccount.AccountURL);
               bodyHtml = bodyHtml.Replace("<$logSchedulerID$>", logItem.ID.ToString());
               bodyHtml = bodyHtml.Replace("<$accountname$>", objAccount.Name);

               //bodyHtml += "</ul></li></ul>"
               //    + "<a href=\"http://" + hostUrl + "/AccountLog.aspx?id=" + logItem.ID + "&mod=1\">Edit this report</a>" + "</div>";
               //bodyHtml += "You received this message because you have an account with " + objAccount.Name + ", digital management solution. Please do not reply to this email; replies are not monitored."
               //    + "<a href=\"http://" + hostUrl + "/Login.aspx\">Click here</a> to login and change your email preferences.";

               try
               {
                   using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0,2,0)))
                   {
                       LogSchedulerEmailDeliveryBO objLogEmail = new LogSchedulerEmailDeliveryBO(context);
                       objLogEmail.LogScheduler = logItem.id;
                       objLogEmail.Creator = logItem.Creator;
                       objLogEmail.CreatedDate = DateTime.Now;
                       objLogEmail.SentContent = bodyHtml;

                       try
                       {
                           WebvolumeEmail.SendMailFromSystem(toName, toEmail, toCC, subject, bodyHtml, true);
                           objLogEmail.IsFailed = false;
                       }
                       catch (Exception ex)
                       {
                           objLogEmail.IsFailed = true;
                       }

                       context.SaveChanges();
                       ts.Complete();
                   }
               }
               catch (Exception exc)
               {
                   // Log the error
                   WebvolumeLogging.log.Error("Error occured while saving LogSchedulerEmailDelivery", exc);
               }
           }
       }*/
        #endregion

        #region Static Methods



        #endregion
    }
}

