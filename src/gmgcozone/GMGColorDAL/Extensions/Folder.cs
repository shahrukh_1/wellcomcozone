using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GMGColorDAL
{
    public partial class Folder : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Return true if given folder has approvals
        /// </summary>
        /// <param name="folderId"> </param>
        /// <param name="context"> </param>
        /// <returns>True if folder has approvals</returns>
        public static bool IsFolderHasApprovals(int folderId, GMGColorContext context)
        {
            try
            {
                Folder objFolder = (from f in context.Folders where f.ID == folderId select f).FirstOrDefault();

                // Foder Approvals
                if (objFolder.Approvals.Any())
                {
                    return true;
                }
                else
                {
                    bool hasApprovals = false;
                    // Sub Folders
                    foreach (Folder objSubFolder in objFolder.GetChilds(context))
                    {
                        hasApprovals = Folder.IsFolderHasApprovals(objSubFolder.ID, context);
                        if (hasApprovals)
                            break;
                    }

                    return hasApprovals;
                }
            }
            finally
            {
            }
        }

        public static string DeleteFolder(int id, int modifier, GMGColorContext context)
        {
            try
            {
                return context.DeleteFolder(id, modifier).ToList()[0].RetVal;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in DeleteFolder()", ex);
            }
            finally
            {
            }
        }

        public static string MoveApproval(int approvalId, int folderId, GMGColorContext context)
        {
            try
            {
                return context.MoveApproval(approvalId, folderId).ToList()[0].RetVal;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in MoveApproval()", ex);
            }
            finally
            {
            }
        }

        public static List<ReturnFolderTreeView> GetFolderTree(int user, GMGColorContext context, bool viewAllFilesAndFolders = false)
        {
            try
            {
                var hasfolder = (from fc in context.FolderCollaborators
                                 where fc.Collaborator == user
                          select fc.ID).Any();
                if (hasfolder || viewAllFilesAndFolders)
                {
                    return context.GetFolderTree(user, viewAllFilesAndFolders).ToList();
                }
                else
                {
                    return new List<ReturnFolderTreeView>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetFolderTree()", ex);
            }
            finally
            {
            }
        }

        public List<Folder> GetChilds(GMGColorContext context)
        {
            try
            {
                return (from f in context.Folders where f.Parent == ID select f).ToList();
            }
            finally
            {
            }
        }

        #endregion

    }
}

