﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.XMLEngine;

namespace GMGColorDAL
{
    public partial class UploadEngineCustomXMLTemplate : BusinessObjectBase
    {
        /// <summary>
        /// Gets the list of presets for current account
        /// </summary>
        /// <param name="accId">Account Id</param>
        /// <param name="context">DB Context</param>
        /// <returns></returns>
        public static List<UploadEngineXMLTemplatesDashBoardViewModel> PopulateXMLTemplatesGrid(int accId, GMGColorContext context)
        {
            try
            {
                List<UploadEngineXMLTemplatesDashBoardViewModel> xmlTemplatesList = new List<UploadEngineXMLTemplatesDashBoardViewModel>();

                xmlTemplatesList = (from xmlt in context.UploadEngineCustomXMLTemplates
                                    where xmlt.Account == accId 
                                select new UploadEngineXMLTemplatesDashBoardViewModel
                                {
                                    ID = xmlt.ID,
                                    Name = xmlt.Name,
                                    CanBeDeleted = xmlt.UploadEngineProfiles.Count == 0
                                }).ToList();

                return xmlTemplatesList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in UploadEngineXMLTemplates.cs PopulateXMLTemplatesGrid()", ex);
            }
        }

        /// <summary>
        /// Returns Default XML Nodes from Database
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<DefaultXMLField> GetDefaultXMLFields(GMGColorContext context)
        {
            try
            {
                List<DefaultXMLField> xmlTemplatesList = new List<DefaultXMLField>();

                xmlTemplatesList = (from xmlt in context.UploadEngineDefaultXMLTemplates
                                    select new DefaultXMLField
                                    {
                                        ID = xmlt.ID,
                                        XPath = xmlt.XPath,
                                        Key = xmlt.Key
                                    }).ToList();

                return xmlTemplatesList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in UploadEngineXMLTemplates.cs GetDefaultXMLFields()", ex);
            }
        }

        /// <summary>
        /// Get Custom XML Fields from DB
        /// </summary>
        /// <param name="customXMLTemplateID">the specified Custom XML Template</param>
        /// <param name="context">DB Context</param>
        /// <returns>A list containing the specified Custom fields for the requested XML Template</returns>
        public static List<CustomXMLField> GetCustomXMLFields(int customXMLTemplateID, GMGColorContext context)
        {
            try
            {
                List<CustomXMLField> xmlTemplatesList = new List<CustomXMLField>();

                xmlTemplatesList = (from xmlt in context.UploadEngineCustomXMLTemplatesFields
                                    join defxmlt in context.UploadEngineDefaultXMLTemplates on xmlt.UploadEngineDefaultXMLTemplate equals defxmlt.ID
                                    where xmlt.UploadEngineCustomXMLTemplate == customXMLTemplateID
                                    select new CustomXMLField
                                    {
                                        ID = xmlt.ID,
                                        XPath = xmlt.XPath,
                                        DefaultXMLTemplateID = defxmlt.ID,
                                        DefaultXMLTemplateKey = defxmlt.Key
                                    }).ToList();

                return xmlTemplatesList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in UploadEngineXMLTemplates.cs GetDefaultXMLFields()", ex);
            }
        }

        /// <summary>
        /// Gets XML Fields for specified Custom Template , whether they are custom or default. All default Fields are returned in case of no template
        /// </summary>
        /// <param name="context">DB Context</param>
        /// <param name="customTemplateID">Custom Template Id</param>
        /// <returns></returns>
        public static List<XMLField> GetXMLFields(GMGColorContext context, int? customTemplateID)
        {
            try
            {
                List<XMLField> xmlFields = new List<XMLField>();

                if (customTemplateID.HasValue)
                {
                    //Get Default Fields that are not customized
                    xmlFields = (from dxmlt in context.UploadEngineDefaultXMLTemplates
                                 where !(from csxmlf in context.UploadEngineCustomXMLTemplatesFields
                                         join csxmlt in context.UploadEngineCustomXMLTemplates on csxmlf.UploadEngineCustomXMLTemplate equals csxmlt.ID
                                         where csxmlt.ID == customTemplateID.Value
                                         select csxmlf.UploadEngineDefaultXMLTemplate).Contains(dxmlt.ID)
                                 select new XMLField
                                 {
                                     ID = dxmlt.ID,
                                     XPath = dxmlt.XPath,
                                     Key = dxmlt.Key,
                                     NodeName = dxmlt.NodeName,
                                     IsCustom = false
                                 }).ToList();

                    //Add customized fields set for this current template
                    xmlFields.AddRange((from ct in context.UploadEngineCustomXMLTemplatesFields
                                        join dxmlt in context.UploadEngineDefaultXMLTemplates on ct.UploadEngineDefaultXMLTemplate equals dxmlt.ID
                                        where ct.UploadEngineCustomXMLTemplate == customTemplateID.Value
                                        select new XMLField
                                        {
                                            ID = ct.ID,
                                            XPath = ct.XPath,
                                            Key = dxmlt.Key,
                                            IsCustom = true
                                        }
                                        ).ToList());
                }
                else
                {
                    //No template was selected for this profile, default will be used
                    xmlFields = (from xmlt in context.UploadEngineDefaultXMLTemplates
                                 select new XMLField
                                {
                                    ID = xmlt.ID,
                                    XPath = xmlt.XPath,
                                    Key = xmlt.Key,
                                    NodeName = xmlt.NodeName,
                                    IsCustom = false
                                }).ToList();
                }

                return xmlFields;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in UploadEngineXMLTemplates.cs GetXMLFields()", ex);
            }
        }

        public static string GetCustomTemplateName(int customXMLTemplateID, GMGColorContext context)
        {
            try
            {
                string xmlTemplateName = (from xmlt in context.UploadEngineCustomXMLTemplates
                                    where xmlt.ID == customXMLTemplateID
                                    select xmlt.Name).SingleOrDefault();

                return xmlTemplateName;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in UploadEngineXMLTemplates.cs GetCustomTemplateName()", ex);
            }
        }
    }
}
