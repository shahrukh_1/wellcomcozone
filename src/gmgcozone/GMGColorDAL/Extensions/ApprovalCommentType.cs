using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class ApprovalCommentType : BusinessObjectBase
    {
        #region Enums

        public enum CommentType
        {
            MarkerComment = 1,
            AreaComment = 2,
            TextComment = 3,
            DrawComment = 4,
            AddTextComment = 5,
            ColorpickerComment = 6
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static CommentType GetApprovalCommentType(int approvalCommentTypeId, GMGColorContext context)
        {
            try
            {
                return
                    ApprovalCommentType.GetApprovalCommentType(
                        (from act in context.ApprovalCommentTypes where act.ID == approvalCommentTypeId select act.Key).FirstOrDefault());
            }
            finally
            {
            }
        }

        public static CommentType GetApprovalCommentType(ApprovalCommentType objApprovalCommentType)
        {
            return ApprovalCommentType.GetApprovalCommentType(objApprovalCommentType.Key);
        }

        public static CommentType GetApprovalCommentType(string key)
        {
            switch (key)
            {
                case "MKR": return CommentType.MarkerComment;
                case "ARA": return CommentType.AreaComment;
                case "TXT": return CommentType.TextComment;
                case "DRW": return CommentType.DrawComment;
                case "ADT": return CommentType.AddTextComment;
                case "CPR": return CommentType.ColorpickerComment;
                default: return CommentType.AddTextComment;
            }
        }

        public static string GetLocalizedApprovalCommentTypeName(ApprovalCommentType objApprovalCommentType)
        {
            return ApprovalCommentType.GetLocalizedApprovalCommentType(ApprovalCommentType.GetApprovalCommentType(objApprovalCommentType));
        }

        public static string GetLocalizedApprovalCommentType(CommentType commentType)
        {
            switch (commentType)
            {
                case CommentType.MarkerComment: return Resources.lblMarkerComment;
                case CommentType.AreaComment: return Resources.lblAreaComment;
                case CommentType.TextComment: return Resources.lblTextComment;
                case CommentType.DrawComment: return Resources.lblDrawComment;
                case CommentType.AddTextComment: return Resources.lblAddTextComment;
                case CommentType.ColorpickerComment: return Resources.lblColorpickerComment;
                default: return string.Empty;
            }
        }

        #endregion
    }
}

