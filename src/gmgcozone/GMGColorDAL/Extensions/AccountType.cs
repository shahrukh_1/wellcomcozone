using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class AccountType : BusinessObjectBase
    {
        #region Enums

        public enum Type
        {
            /// <summary>
            /// GMG Color HQ account
            /// </summary>
            GMGColor = 1,
            /// <summary>
            /// Subsidiary account
            /// </summary>
            Subsidiary,
            /// <summary>
            /// Dealer account
            /// </summary>
            Dealer,
            /// <summary>
            /// Client account
            /// </summary>
            Client,
            /// <summary>
            /// Subscriber account
            /// </summary>
            Subscriber
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Return Account Type of the given account id
        /// </summary>
        /// <param name="account">Account id</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static Type GetAccountType(int account, GMGColorContext context)
        {
            try
            {
                return AccountType.GetAccountType((from at in context.AccountTypes
                                                   join a in context.Accounts on at.ID equals a.AccountType
                                                   where a.ID == account
                                                   select at.Key).SingleOrDefault());
            }
            finally
            {
            }
        }

        /// <summary>
        /// Return Account Type of the given Account
        /// </summary>
        /// <param name="objAccount">Object type Account</param>
        /// <returns></returns>
        public static Type GetAccountType(Account objAccount, GMGColorContext context)
        {
            return AccountType.GetAccountType(objAccount.ID, context);
        }

        public static Type GetAccountType(string key)
        {
            switch (key)
            {
                case "GMHQ": return Type.GMGColor;
                case "SBSY": return Type.Subsidiary;
                case "DELR": return Type.Dealer;
                case "CLNT": return Type.Client;
                default: return Type.Subscriber;
            }
        }

        /// <summary>
        /// Return AccountType of the given Account Type
        /// </summary>
        /// <param name="type">Account Type</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static AccountType GetAccountType(Type type, GMGColorContext context)
        {
            try
            {
                return (from at in context.AccountTypes
                        where
                            (type == Type.GMGColor && at.Key == "GMHQ") ||
                            (type == Type.Subsidiary && at.Key == "SBSY") ||
                            (type == Type.Dealer && at.Key == "DELR") ||
                            (type == Type.Client && at.Key == "CLNT") ||
                            (type == Type.Subscriber && at.Key == "SBSC")
                        select at).FirstOrDefault();
            }
            finally
            {
            }
        }


        public static string GetLocalizedAccountTypeName(Account objAccount, GMGColorContext context)
        {
            return AccountType.GetLocalizedAccountTypeName(AccountType.GetAccountType(objAccount, context));
        }

        public static string GetLocalizedAccountTypeName(int type, GMGColorContext context)
        {
            AccountType objAccountType = (from at in context.AccountTypes where at.ID == type select at).FirstOrDefault();
            return AccountType.GetLocalizedAccountTypeName(objAccountType);
        }

        public static string GetLocalizedAccountTypeName(AccountType objAccountType)
        {
            return AccountType.GetLocalizedAccountTypeName(AccountType.GetAccountType(objAccountType.Key));
        }

        public static string GetLocalizedAccountTypeName(Type type)
        {
            switch (type)
            {
                case Type.GMGColor: return Resources.resAccountTypeGMGColor;
                case Type.Subsidiary: return Resources.resAccountTypeSubsidiary;
                case Type.Dealer: return Resources.resAccountTypeDealers;
                case Type.Client: return Resources.resAccountTypeClient;
                case Type.Subscriber: return Resources.resAccountTypeSubscriber;
                default: return string.Empty;
            }
        }

        public static string GetLocalizedAccountTypeName(string name)
        {
            switch (name)
            {
                case "GMGcoZone": return Resources.resAccountTypeGMGColor;
                case "Subsidiary": return Resources.resAccountTypeSubsidiary;
                case "Dealer": return Resources.resAccountTypeDealers;
                case "Client": return Resources.resAccountTypeClient;
                case "Subscriber": return Resources.resAccountTypeSubscriber;
                default: return string.Empty;
            }
        }

        public static Dictionary<int, string> GetLocalizedAccountTypeNames(GMGColorContext context)
        {
            return (from at in context.AccountTypes select at).ToDictionary(
                o => o.ID,
                o =>AccountType.GetLocalizedAccountTypeName(o.ID, context)
            );
        }

        #endregion
    }
}