using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ExternalCollaborator : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static string GetImagePath(int externalUserId, GMGColorContext context)
        {
            var account = (from a in context.Accounts
                            join ec in context.ExternalCollaborators on a.ID equals ec.Account
                            where ec.ID == externalUserId
                            select new {Domain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain, a.Region}).
                FirstOrDefault();
            if (account != null)
            {
                string absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + account.Domain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(account.Region) + "/";
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    absoluteFileUrl = GMGColorConfiguration.AppConfiguration.PathToDataFolder(account.Region);
                }

                absoluteFileUrl += "users/nouser-48px-48px.png";
                return absoluteFileUrl.ToLower();
            }
            return string.Empty;
        }

        public static List<ExternalCollaborator> GetActiveUsers(int accountId, int loggedUserId, bool isFromDeliver, GMGColorContext context)
        {
            var collaborators =  isFromDeliver
                                ? (from ec in context.DeliverExternalCollaborators
                                   let isinGroupWithCreator = (from ugu in context.UserGroupUsers
                                                               join ug in context.UserGroups on ugu.UserGroup equals ug.ID
                                                               where ugu.User == loggedUserId
                                                               select ug.ID).Intersect(from ugu in context.UserGroupUsers
                                                                                               where ugu.User == ec.Creator
                                                                                               select ugu.UserGroup).Any()
                                   where ec.Account == accountId && isinGroupWithCreator
                                    select new
                                        {
                                            ID = ec.ID,
                                            Email = ec.EmailAddress,
                                            FamilyName = ec.FamilyName,
                                            GivenName = ec.GivenName,
                                            IsDeleted = false
                                        }).ToList()
                                : (from ec in context.ExternalCollaborators
                                    where ec.Account == accountId && ec.IsDeleted == false
                                    select new
                                        {
                                            ID = ec.ID,
                                            Email = ec.EmailAddress,
                                            FamilyName = ec.FamilyName,
                                            GivenName = ec.GivenName,
                                            IsDeleted = ec.IsDeleted
                                        }).ToList();

            return collaborators.Select(collaborator => new ExternalCollaborator()
                {
                    ID = collaborator.ID,
                    EmailAddress = collaborator.Email,
                    FamilyName = collaborator.FamilyName,
                    GivenName = collaborator.GivenName,
                    IsDeleted = collaborator.IsDeleted
                }).ToList();
        }

        public static int GetExternalUserIDByEmail(string email, int userID, GMGColorContext context)
        {
            return (from exu in context.ExternalCollaborators
                    join ac in context.Accounts on exu.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where us.ID == userID && exu.EmailAddress == email
                    select exu.ID).FirstOrDefault();
        }

        public static bool ValidateExternalUserEmail(string email, int userID, GMGColorContext context)
        {
            return (from exu in context.ExternalCollaborators
                    join ac in context.Accounts on exu.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where us.ID == userID && exu.EmailAddress == email
                    select exu.ID).Any();
        }

        public static string GetExternalUserEmailByID(int exUserid, int userID, GMGColorContext context)
        {
            return (from exu in context.ExternalCollaborators
                    join ac in context.Accounts on exu.Account equals ac.ID
                    join us in context.Users on ac.ID equals us.Account
                    where us.ID == userID && exu.ID == exUserid
                    select exu.EmailAddress).FirstOrDefault();
        }

        public static ExternalCollaborator GetObjectExternal(int ExternalUserId, GMGColorContext context)
        {
            try
            {
                return (from ec in context.ExternalCollaborators where ec.ID == ExternalUserId select ec).FirstOrDefault();
            }
            finally
            {
            }
        }
        #endregion
    }
}

