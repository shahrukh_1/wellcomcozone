﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace GMGColorDAL.Extensions
{
    public static class ObjectExtensions
    {
        public static T ToObject<T>(this string source)
            where T : class
        {
            var dictionary  = source.Split(';')
                .Select(part => part.Split(new char[] { '=' }, 2))
                .Where(part => part.Length == 2)
                .ToDictionary(sp => sp[0], sp => sp[1]);

            Type type = typeof(T);
            T result = (T)Activator.CreateInstance(type);

            foreach (var item in dictionary)
            {
                int intVal;
                bool boolVal;
                if (int.TryParse(item.Value, out intVal))
                {
                    type.GetProperty(item.Key).SetValue(result, intVal, null);
                }
                else if (bool.TryParse(item.Value, out boolVal))
                {
                    type.GetProperty(item.Key).SetValue(result, boolVal, null);
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.Value))
                    {
                        type.GetProperty(item.Key).SetValue(result, item.Value, null);
                    }
                }
             
            }
            return result;
        }

        public static string AsDictionaryString(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            var dictionary = source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );
            return string.Join(";", dictionary.Select(x => x.Key + "=" + x.Value));
        }
    }
}
