using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class HighlightType : BusinessObjectBase
    {
        #region Enums

        public enum HighlightTypeE
        {
            Normal,
            Insert,
            Replace,
            Delete
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static HighlightTypeE GetHighlightType(HighlightType objHighlightType)
        {
            return HighlightType.GetHighlightType(objHighlightType.Key);
        }

        private static HighlightTypeE GetHighlightType(string key)
        {
            switch (key)
            {
                case "N": return HighlightTypeE.Normal;
                case "I": return HighlightTypeE.Insert;
                case "R": return HighlightTypeE.Replace;
                case "D": return HighlightTypeE.Delete;
                default: return HighlightTypeE.Normal;
            }
        }

        public static string GetLocalizedHighlightTypeName(HighlightType objHighlightType)
        {
            return HighlightType.GetLocalizedHighlightType(HighlightType.GetHighlightType(objHighlightType));
        }

        public static string GetLocalizedHighlightType(HighlightTypeE highlightType)
        {
            switch (highlightType)
            {
                case HighlightTypeE.Normal: return Resources.lblNormal;
                case HighlightTypeE.Insert: return Resources.lblInsert;
                case HighlightTypeE.Replace: return Resources.lblReplace;
                case HighlightTypeE.Delete: return Resources.lblDelete;
                default: return string.Empty;
            }
        }

        #endregion
    }
}

