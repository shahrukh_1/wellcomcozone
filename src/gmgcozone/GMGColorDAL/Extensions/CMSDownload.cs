﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL
{
    public partial class CMSDownload : BusinessObjectBase
    {
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
        public static List<CMSDownload> GetAllCMSDownloads(GMGColorContext context)
        {
            return (from cmsd in context.CMSDownloads
                    where cmsd.IsDeleted == false
                    orderby cmsd.Name ascending
                    select cmsd).ToList();
        }

        public List<CMSDownload> GetDownloads(List<string> fileGuids, GMGColorContext context)
        {
            return (from cmsd in context.CMSDownloads
                    where fileGuids.Contains(cmsd.FileGuid)
                    select cmsd).ToList();
        }

        #endregion
    }
    
}
