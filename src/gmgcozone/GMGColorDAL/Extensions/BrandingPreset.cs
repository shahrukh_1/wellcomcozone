
using System.Linq;

namespace GMGColorDAL
{
    public partial class BrandingPreset : BusinessObjectBase
    {
        #region Constructors

        #endregion

        #region OnPropertyChange Methods

        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Properties

        #endregion

        #region Extension Methods
        /// <summary>
        /// Returns whether or not the branding preset name is unique in the system
        /// </summary>
        /// <param name="presetName">The branding preset name.</param>
        /// <param name="id">The id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is preset name unique] [the specified preset name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPresetNameUnique(string presetName, int id, int account, GMGColorContext context)
        {
            if (presetName != null)
            {
                return !(from bp in context.BrandingPresets
                         where bp.Name.ToLower() == presetName.ToLower().Trim() && bp.ID != id && bp.Account == account
                         select bp.ID).Any();
            }
            return true;
        }
       
        #endregion
    }
}

