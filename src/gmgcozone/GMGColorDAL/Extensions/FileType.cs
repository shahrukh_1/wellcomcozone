using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class FileType : BusinessObjectBase
    {
        #region Enums

        public enum FileExtention
        {
            Unkown,
            JPEG,
            JPG ,
            PDF,
            PNG,
            TIFF,
            TIF,
            EPS,
            MP4,
            M4V,
            FLV,
            F4V,
            MOV,
            MPG,
            MPG2,
            THREE_GP,
            WMV,
            AVI,
            ASF,
            FM,
            OGV,
            SWF,
            QT,
            MJPEG,
            VOB,
            XVID,
            THREE_VIX,
            PSD,
            ZIP,
            DOC,
            DOCX,
            PPT,
            PPTX
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Return the file extention of given file type
        /// </summary>
        /// <param name="id">File Type Id</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static FileExtention GetFileType(int id, GMGColorContext context)
        {
            try
            {
                FileType objFileType = (from ft in context.FileTypes where ft.ID == id select ft).FirstOrDefault();
                return FileType.GetFileExtention(objFileType);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Return the file extention of given file type
        /// </summary>
        /// <param name="objFileType">Object type FileType</param>
        /// <returns></returns>
        public static FileExtention GetFileExtention(FileType objFileType)
        {
            return FileType.GetFileExtention(objFileType.Extention);
        }

        /// <summary>
        /// Return the file extention of given file name or extention
        /// </summary>
        /// <param name="fileNameOrExtention">File Name/Extention</param>
        /// <returns></returns>
        public static FileExtention GetFileExtention(string fileNameOrExtention)
        {
            string extension = (System.IO.Path.GetExtension(fileNameOrExtention) ?? string.Empty).Replace(".", string.Empty).ToUpper().Trim();
            switch (extension)
            {
                case "JPEG": return FileExtention.JPEG;
                case "JPG": return FileExtention.JPG;
                case "PDF": return FileExtention.PDF;
                case "PNG": return FileExtention.PNG;
                case "TIFF": return FileExtention.TIFF;
                case "TIF": return FileExtention.TIF;
                case "EPS": return FileExtention.EPS;
                case "MP4": return FileExtention.MP4;
                case "M4V": return FileExtention.M4V;
                case "FLV": return FileExtention.FLV;
                case "F4V": return FileExtention.F4V;
                case "MOV": return FileExtention.MOV;
                case "MPG": return FileExtention.MPG;
                case "MPG2": return FileExtention.MPG2;
                case "3GP": return FileExtention.THREE_GP;
                case "WMV": return FileExtention.WMV;
                case "AVI": return FileExtention.AVI;
                case "ASF": return FileExtention.ASF;
                case "FM": return FileExtention.FM;
                case "OGV": return FileExtention.OGV;
                case "SWF": return FileExtention.SWF;
                case "QT": return FileExtention.QT;
                case "MJPEG": return FileExtention.MJPEG;
                case "VOB": return FileExtention.VOB;
                case "XVID": return FileExtention.XVID;
                case "3VIX": return FileExtention.THREE_VIX;
                case "PSD": return FileExtention.PSD;
                case "ZIP": return FileExtention.ZIP;
                case "DOC": return FileExtention.DOC;
                case "DOCX": return FileExtention.DOCX;
                case "PPT": return FileExtention.PPT;
                case "PPTX": return FileExtention.PPTX;
                default: return FileExtention.Unkown;
            }
        }

        /// <summary>
        /// Return FileType of the given file name or extention
        /// </summary>
        /// <param name="fileNameOrExtention">File Name/Extention</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static FileType GetFileType(string fileNameOrExtention, GMGColorContext context)
        {
            try
            {
                return FileType.GetFileType(FileType.GetFileExtention(fileNameOrExtention), context);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Return FileType of the given extention
        /// </summary>
        /// <param name="fileExtention">enum FileExtention</param>
        /// <param name="context"> </param>
        /// <returns></returns>
		[Obsolete("Use 'List<FileTypeModel> GetFileTypeIdsAndExtensions' instead")]
        public static FileType GetFileType(FileExtention fileExtention, GMGColorContext context)
        {
            try
            {
                string extension = fileExtention.ToString().ToLower();
                return (from ft in context.FileTypes
                        where
                            (
                                (fileExtention == FileExtention.THREE_GP && ft.Extention == "3gp") ||
                                (fileExtention == FileExtention.THREE_VIX && ft.Extention == "evix")
                            )
                        select ft).FirstOrDefault() ??
                       (from ft in context.FileTypes
                        where ft.Extention.ToLower() == extension
                        select ft).FirstOrDefault();
            }
            finally
            {
            }
        }

		/// <returns></returns>
		public static List<FileTypeModel> GetFileTypeIdsAndExtensions(GMGColorContext context)
		{				
			return (from ft in context.FileTypes						
					select new FileTypeModel { ID = ft.ID, Extension = ft.Extention, MediaType = ft.Type }).ToList();			
		}
		
		#endregion
	}

	public class FileTypeModel
	{
		public int ID { get; set; }
		public string Extension { get; set; }
		public string MediaType { get; set; }
	}
}

