using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Common;

namespace GMGColorDAL
{
    public partial class DeliverJobStatu : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static DeliverJobStatu GetObjectsByKey(DeliverJobStatus status, GMGColorContext context)
        {
            try
            {
                return (from djs in context.DeliverJobStatus where djs.Key == (int) status select djs).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static int GetDeliverJobStatusID(GMG.CoZone.Common.DeliverJobStatus status, GMGColorContext context)
        {
            return (from ds in context.DeliverJobStatus
                    where ds.Key == (int)status
                    select ds.ID).FirstOrDefault();
        }

        #endregion

    }
}

