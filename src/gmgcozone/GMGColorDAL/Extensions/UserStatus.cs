using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class UserStatu : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        public enum Status
        {
            Active,
            Invited,
            Inactive,
            Deleted
        };

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// This method will returns UserStatus of the given user id
        /// </summary>
        /// <param name="userStatusId"> </param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static Status GetUserStatus(int userStatusId, GMGColorContext context)
        {
            try
            {
                string key = (from us in context.UserStatus where us.ID == userStatusId select us.Key).FirstOrDefault();
                return UserStatu.GetUserStatus(key);
            }
            finally
            {
            }
        }

        public static Status GetUserStatus(string key)
        {
            switch (key)
            {
                case "A": return Status.Active;
                case "N": return Status.Invited;
                case "I": return Status.Inactive;
                default: return Status.Deleted;
            }
        }

        /// <summary>
        /// This method will returns the UserStatu of the given user status
        /// </summary>
        /// <param name="status" type="enum"></param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static UserStatu GetUserStatus(Status status, GMGColorContext context)
        {
            try
            {
                return (from us in context.UserStatus
                        where
                            (
                                (status == Status.Active && us.Key == "A") ||
                                (status == Status.Invited && us.Key == "N") ||
                                (status == Status.Inactive && us.Key == "I")
                            )
                        select us).FirstOrDefault() ??
                       (from us in context.UserStatus where us.Key == "D" select us).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static string GetLocalizedUserStatusName(int userStatusId, GMGColorContext context)
        {
            try
            {
                string key = (from us in context.UserStatus where us.ID == userStatusId select us.Key).FirstOrDefault();
                return UserStatu.GetLocalizedUserStatusName(UserStatu.GetUserStatus(key));
            }
            finally
            {
            }
        }

        public static string GetLocalizedUserStatusName(Status status)
        {
            switch (status)
            {
                case Status.Active: return Resources.lblActive;
                case Status.Invited: return Resources.lblInvited;
                case Status.Inactive: return Resources.lblInactive;
                case Status.Deleted: return Resources.lblDeleted;
                default: return string.Empty;
            }
        }

        #endregion
    }
}

