﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMGColorDAL
{
    public partial class CheckListItem : BusinessObjectBase
    {
        #region OnPropertyChange Methods

        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        /// <summary>
        /// Returns whether or not the checklist item name is unique at the account level
        /// </summary>
        /// <param name="checklistId"></param>
        /// <param name="itemName"></param>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsChecklistItemNameUnique(int checklistId, string itemName, int id, GMGColorContext context)
        {
            if (!string.IsNullOrEmpty(itemName))
            {
                return !(from ap in context.CheckListItem
                         where ap.Name.ToLower() == itemName.ToLower().Trim() && ap.ID != id && ap.CheckList == checklistId
                         select ap.ID).Any();
            }
            return true;
        }

    }
}
