using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using RSC = GMGColor.Resources;

namespace GMGColorDAL
{
    public class MediaService_Validation
    {
        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "reqSmoothing", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int Smoothing { get; set; }

        [Required(ErrorMessageResourceName = "reqResolution", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int Resolution { get; set; }

        [Required(ErrorMessageResourceName = "reqColorSpace", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int Colorspace { get; set; }

        [Required(ErrorMessageResourceName = "reqImageFormat", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int ImageFormat { get; set; }

        [Required(ErrorMessageResourceName = "reqPageBox", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int PageBox { get; set; }

        [Required(ErrorMessageResourceName = "reqTileImageFormat", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int TileImageFormat { get; set; }

        [Required(ErrorMessageResourceName = "reqJpegCompression", ErrorMessageResourceType = typeof(RSC.Resources))]
        [Range(0, 100, ErrorMessageResourceName = "invalidJpegCompression", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int JPEGCompression { get; set; }
    }

    [MetadataType(typeof(MediaService_Validation))]
    public partial class MediaService : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
        #endregion
    }
}

