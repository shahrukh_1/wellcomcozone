﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.XMLEngine;

namespace GMGColorDAL
{
    public partial class UploadEngineProfile : BusinessObjectBase
    {
        /// <summary>
        /// Gets the list of presets for current account
        /// </summary>
        /// <param name="accId">Account Id</param>
        /// <param name="context">DB Context</param>
        /// <returns></returns>
        public static List<UploadEngineProfileDashBoardViewModel> PopulateProfilesGrid(int accId, GMGColorContext context)
        {
            try
            {
                List<UploadEngineProfileDashBoardViewModel> profilesList = new List<UploadEngineProfileDashBoardViewModel>();

                profilesList = (from pr in context.UploadEngineProfiles
                                let presetName = (from preset in context.UploadEnginePresets where pr.OutputPreset == preset.ID select preset.Name).FirstOrDefault()
                                let xmlTemplateName = (from xmlTemplate in context.UploadEngineCustomXMLTemplates where pr.XMLTemplate == xmlTemplate.ID select xmlTemplate.Name).FirstOrDefault()
                                where pr.Account == accId && pr.IsDeleted == false
                                select new UploadEngineProfileDashBoardViewModel
                                {
                                    ID = pr.ID,
                                    Name = pr.Name,
                                    HasInputSettings = pr.HasInputSettings,
                                    OutputPreset = presetName,
                                    XMLTemplate = xmlTemplateName,
                                    IsActive = pr.IsActive
                                }).ToList();

                return profilesList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in XML Engine PopulateProfilesGrid()", ex);
            }
        }

        /// <summary>
        /// Gets all presets from current account
        /// </summary>
        /// <param name="accId">Account Id</param>
        /// <param name="context">DB Context</param>
        /// <returns>List of Presets id and Name for the current account</returns>
        public static List<KeyValuePair<int, string>> GetXMLTemplatesList(int accId, GMGColorContext context)
        {
            try
            {
                List<KeyValuePair<int, string>> presets = new List<KeyValuePair<int, string>>();

                presets = (from xmlt in context.UploadEngineCustomXMLTemplates
                           where xmlt.Account == accId
                           select xmlt).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();

                return presets;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetXMLTemplatesList() for AddEdit Upload Engine Profile", ex);
            }
        }

        /// <summary>
        /// Gets the specified object from Database
        /// </summary>
        /// <param name="ID">DB ID</param>
        /// <param name="context">DB Context</param>
        /// <returns>UploadEngineProfile BO Object</returns>
        public static UploadEngineProfile GetObjectById(int ID, GMGColorContext context)
        {
            try
            {

                return (from pr in context.UploadEngineProfiles
                        where pr.ID == ID
                        select pr).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetViewModel() for AddEdit IO Preset", ex);
            }
        }
    }
}
