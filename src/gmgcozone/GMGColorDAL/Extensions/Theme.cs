using System.Linq;

namespace GMGColorDAL
{
    public partial class Theme : BusinessObjectBase
    {
        #region Enums

        public enum ColorScheme
        {
            Default,
            Splash,
            Juicy,
            Fresh,
            Moody
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// This method will returns the bootstrap theme based on the theme key from db
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetColorTheme(string key)
        {
            string colorTheme = "default";
            ColorScheme colorScheme = GetColorScheme(key);
            switch (colorScheme)
            {
                case GMGColorDAL.Theme.ColorScheme.Splash: 
                    colorTheme = "pink";
                    break;
                case GMGColorDAL.Theme.ColorScheme.Juicy: 
                    colorTheme = "green";
                    break;
                case GMGColorDAL.Theme.ColorScheme.Fresh: 
                    colorTheme = "blue";
                    break;
                case GMGColorDAL.Theme.ColorScheme.Moody: 
                    colorTheme = "white";
                    break;
                case GMGColorDAL.Theme.ColorScheme.Default:
                default: colorTheme = "default";
                    break;
            }
            return colorTheme;
        }

        /// <summary>
        /// This method will returns ColorScheme of the given theme object key
        /// </summary>
        /// <param name="objTheme"> </param>
        /// <returns></returns>
        public static ColorScheme GetColorScheme(string key)
        {
            switch (key)
            {
                case "S": return ColorScheme.Splash;
                case "J": return ColorScheme.Juicy;
                case "F": return ColorScheme.Fresh;
                case "M": return ColorScheme.Moody;
                case "D":
                default: return ColorScheme.Default;
            }
        }
        #endregion
    }
}