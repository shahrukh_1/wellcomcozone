using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ContractPeriod : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        public enum Period
        {
            OneYear,
            TwoYears,
            ThreeYears,
            FourYears,
            FiveYears,
            NoLimit
        };

        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// This method will returns Contract Period of the given account id
        /// </summary>
        /// <param name="id" type="int"></param>
        /// <returns></returns>
        public static Period GetContractPeriod(int accountId, GMGColorContext context)
        {
            string name;
            name =
                (from a in context.Accounts
                    join cp in context.ContractPeriods on a.ContractPeriod equals cp.ID
                    where a.ID == accountId
                    select cp.Name).FirstOrDefault();
            switch (name)
            {
                case "One Year": return Period.OneYear;
                case "Two Years": return Period.TwoYears;
                case "Three Year": return Period.ThreeYears;
                case "Four Year": return Period.FourYears;
                case "Five Year": return Period.FiveYears;
                default: return Period.NoLimit;
            }
        }

        /// <summary>
        /// This method will returns the ContractPeriodBO of the given contract period
        /// </summary>
        /// <param name="period" type="ContractPeriodBO.Period"></param>
        /// <returns></returns>
        public static ContractPeriod GetAccountType(Period period, GMGColorContext context)
        {
            return
                (from cp in context.ContractPeriods
                    where
                        ((period == Period.OneYear && cp.Name == "One Year") ||
                        (period == Period.TwoYears && cp.Name == "Two Years") ||
                        (period == Period.ThreeYears && cp.Name == "Three Year")||
                        (period == Period.FourYears && cp.Name == "Four Year") ||
                        (period == Period.NoLimit && cp.Name == "No Limit")
                        )
                    select cp).FirstOrDefault();
        }

        /// <summary>
        /// This function will returns the expiration date of the given account's contract period
        /// If it returns null, that means the contract period is unlimited.
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public static DateTime? GetContractPeriodExpirationDate(int accountId, GMGColorContext context)
        {
            DateTime createdDate;
            createdDate = (from a in context.Accounts where a.ID == accountId select a.CreatedDate).FirstOrDefault();

            switch (ContractPeriod.GetContractPeriod(accountId, context))
            {
                case Period.OneYear: return createdDate.AddYears(1);
                case Period.TwoYears: return createdDate.AddYears(2);
                case Period.ThreeYears: return createdDate.AddYears(3);
                case Period.FourYears: return createdDate.AddYears(4);
                case Period.FiveYears: return createdDate.AddYears(5);
                default: return null;
            }
        }

        #endregion
    }
}

