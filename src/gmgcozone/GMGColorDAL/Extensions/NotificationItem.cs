﻿using System.Collections.Generic;

namespace GMGColorDAL.Extensions
{
    public class InternalUserCreator
    {
        public int UserId { get; set; } 
    }

    public class InternalUserRecipient
    {
        public int UserId { get; set; }
        public string NotificationFrequencyKey { get; set; }
        public string UserStatusKey { get; set; }
        public string AccountStatusKey { get; set; }
    }

    public class UserNotification
    {
        public InternalUserCreator InternalUserCreator { get; set; }
        public NotificationItem NotificationItem { get; set; }
        public string EventGroupKey { get; set; }
        public int NotificationPresetFrecvencyKey { get; set; }
        public bool IsFromGlobalSchedule { get; set; }
        public int SessionId { get; set; }
    }
    
    public class UserNotifications
    {
        public InternalUserRecipient InternalUserRecipient { get; set; }
        public List<UserNotification> Notifications { get; set; }
    }
}
