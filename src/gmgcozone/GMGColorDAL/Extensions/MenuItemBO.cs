using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class MenuItem : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static bool HasKey(int menuId, string[] menusKey, GMGColorContext context)
        {
            bool hasKey;
            hasKey = (from m in context.MenuItems
                            where m.ID == menuId && menusKey.Contains(m.Key)
                            select m.ID).Any();
            return hasKey;
        }

        public static string MenuForDeliverApplication = "DLIN";
        public static string MenuForCollaborateApplication = "APIN";
        public static string MenuForFileTransferApplication = "TRAN";
        public static string[] MenusForApplication = new string[] { MenuForDeliverApplication, MenuForCollaborateApplication, MenuForFileTransferApplication };
        public static string[] MenusForAdmin = new string[] { "ACIN", "PLIN", "RPIN", "SVIN", "GSET", "ACST","APST" };
        public static string[] MenusForHome = new string[] { "HOME" };
        public static string[] MenusForSiteSettings = new string[] { "SEIN" };
        public static string[] MenusForPersonalSettings = new string[] { "PSIN" };
        public const string Divider = "DIVIDER";

        #endregion
    }
}

