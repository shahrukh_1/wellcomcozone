using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

using RSC = GMGColor.Resources;

namespace GMGColorDAL
{
    public class BillingPlanType_Validation
    {
        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [System.Web.Mvc.Remote("IsPlanTypeNameUnique", "Plans", AdditionalFields = "ID")]
        public int Name { get; set; }

        [Required(ErrorMessageResourceName = "reqPlanProduct", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int AppModule { get; set; }
    }

    [MetadataType(typeof(BillingPlanType_Validation))]
    public partial class BillingPlanType : BusinessObjectBase
    {
        #region Enums

        public enum Type
        {
            /// <summary>
            /// Key = E
            /// </summary>
            Essentials,
            /// <summary>
            /// Key = P
            /// </summary>
            PrePress,
            /// <summary>
            /// Key = F
            /// </summary>
            Fixed,
            /// <summary>
            /// CustomKey
            /// </summary>
            Custom
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static Type GetBillingPlanType(int billingPlanTypeId, GMGColorContext context)
        {
            try
            {
                return
                    BillingPlanType.GetBillingPlanType(
                        (from bpt in context.BillingPlanTypes where bpt.ID == billingPlanTypeId select bpt.Key).
                            FirstOrDefault());
            }
            finally
            {
            }
        }

        public static Type GetBillingPlanType(string key)
        {
            switch (key)
            {
                case "E": return Type.Essentials;
                case "P": return Type.PrePress;
                case "F": return Type.Fixed;
                default: return Type.Custom;
            }
        }

        public static BillingPlanType GetBillingPlanType(BillingPlanType.Type billingPlanType, GMGColorContext context)
        {
            try
            {
                return (from bpt in context.BillingPlanTypes
                        where
                            ((billingPlanType == Type.Essentials && bpt.Key == "E") ||
                             (billingPlanType == Type.PrePress && bpt.Key == "P") ||
                             (billingPlanType == Type.Fixed && bpt.Key == "F"))
                        select bpt).FirstOrDefault();
            }
            finally
            {
            }
        }

        #endregion
    }
}

