using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ApprovalCollaborator : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static List<int> GetCollaboratorsByAnnotationId(int approvalAnnotationId, GMGColorContext context)
        {
            return (from aa in context.ApprovalAnnotations
                    join ap in context.ApprovalPages on aa.Page equals ap.ID
                    join a in context.Approvals on ap.Approval equals a.ID
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    join u in context.Users on ac.Collaborator equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where aa.ID == approvalAnnotationId && ac.Phase == a.CurrentPhase && us.Key == "A"
                    && (!a.PrivateAnnotations || (a.PrivateAnnotations && !u.PrivateAnnotations))
                    select ac.Collaborator).Distinct().ToList();
        }

        public static IEnumerable<int> GetCollaboratorsByApprovalIdAndEventType(string eventType, int approvalId, GMGColorContext context)
        {
                return (from a in context.Approvals
                join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                join u in context.Users on ac.Collaborator equals u.ID
                join us in context.UserStatus on u.Status equals us.ID
                join ur in context.UserRoles on u.ID equals ur.User
                join r in context.Roles on ur.Role equals r.ID
                join am in context.AppModules on r.AppModule equals am.ID
                join rpet in context.RolePresetEventTypes on r.ID equals rpet.Role
                join et in context.EventTypes on rpet.EventType equals et.ID
                where
                    a.ID == approvalId && et.Key == eventType && us.Key == "A" &&
                    am.Key == (int) GMG.CoZone.Common.AppModule.Collaborate && ac.Phase == a.CurrentPhase
                // only active users
                select ac.Collaborator).Distinct();
        }

        public static IEnumerable<int> GetOwnerByApprovalIdAndEventType(int approvalId, GMGColorContext context)
        {
            return (from a in context.Approvals
                    join u in context.Users on a.Owner equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where a.ID == approvalId && us.Key == "A"
                    select a.Owner);
        }

        public static IEnumerable<int> GetCollaboratorsByApprovalIdAndOverdueEventType(string eventType, int approvalId, GMGColorContext context)
        {
            var sendNotificationsToRoles = new List<int>
            {
                GMGColorDAL.ApprovalCollaboratorRole.GetApproverAndReviewerCollaboratorRoleId(context)
            };

            return (from a in context.Approvals
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    join u in context.Users on ac.Collaborator equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    join rpet in context.RolePresetEventTypes on r.ID equals rpet.Role
                    join et in context.EventTypes on rpet.EventType equals et.ID
                    join acds in context.ApprovalCollaboratorDecisions on a.ID equals acds.Approval
                    where
                        a.ID == approvalId && et.Key == eventType && us.Key == "A" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && ac.Phase == a.CurrentPhase && sendNotificationsToRoles.Contains(ac.ApprovalCollaboratorRole) &&
                        acds.Phase == a.CurrentPhase && acds.Collaborator == ac.Collaborator && acds.Decision == null
                    // only active users
                    select ac.Collaborator).Distinct();
        }

        public static IEnumerable<int> GetCollaboratorsByApprovalIdAndPhaseIdOverdueEventType(string eventType, int approvalId, int phaseId, GMGColorContext context)
        {
            var sendNotificationsToRoles = new List<int>
            {
                GMGColorDAL.ApprovalCollaboratorRole.GetApproverAndReviewerCollaboratorRoleId(context)
            };

            return (from a in context.Approvals
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    join ajp in context.ApprovalJobPhases on ac.Phase equals ajp.ID
                    join u in context.Users on ac.Collaborator equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    join rpet in context.RolePresetEventTypes on r.ID equals rpet.Role
                    join et in context.EventTypes on rpet.EventType equals et.ID
                    join acds in context.ApprovalCollaboratorDecisions on a.ID equals acds.Approval
                    where
                        a.ID == approvalId &&
                        et.Key == eventType && us.Key == "A" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && 
                        ac.Phase == phaseId && 
                        sendNotificationsToRoles.Contains(ac.ApprovalCollaboratorRole) &&
                        acds.Collaborator == ac.Collaborator &&
                        acds.Decision == null && acds.Phase == phaseId
                        &&
                        ((ajp.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup) ||
                        (ajp.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker
                        && ajp.PrimaryDecisionMaker == ac.Collaborator))
                    // only active users
                    select ac.Collaborator).Distinct();
        }

        public static IEnumerable<int> GetCollaboratorsByApprovalIdPhaseAndEventType(string eventType, int phase, int approvalId, GMGColorContext context)
        {
            return (from a in context.Approvals
                    join ac in context.ApprovalCollaborators on a.ID equals ac.Approval
                    join u in context.Users on ac.Collaborator equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    join rpet in context.RolePresetEventTypes on r.ID equals rpet.Role
                    join et in context.EventTypes on rpet.EventType equals et.ID
                    where
                        a.ID == approvalId && et.Key == eventType && us.Key == "A" &&
                        am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate && ac.Phase == phase
                    // only active users
                    select ac.Collaborator).Distinct();
        }
        #endregion       
    }
}

