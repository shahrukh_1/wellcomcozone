﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels;
using System.Data.SqlClient;

namespace GMGColorDAL
{
    public partial class ApprovalCustomDecision : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        #endregion

        public static List<AccountSettings.ApprovalCustomDecision> GetCustomDecisionsByAccountId(int accountId, int userLocale, GMGColorContext context)
        {
            string query = @"Select 
	                        AD.ID, AD.[Key], 
	                        case when ACD.ID is not null then ACD.[Name] else '3' end [C1],
	                         isnull(ACD.ID,0) [C2]
                        from
	                        ApprovalDecision AD
                        Left Join
	                        ApprovalCustomDecision ACD
                        On
	                        AD.ID=ACD.Decision
	                        and
	                        ACD.Account= @accountId
	                        and
	                        ACD.Locale= @userLocale";

            return context.Database.SqlQuery<AccountSettings.ApprovalCustomDecision>(query, new SqlParameter("accountId", accountId),
                new SqlParameter("userLocale", userLocale)).ToList();
        }

        public static List<AccountSettings.ApprovalCustomDecisionViewModel> GetCustomDecisionsViewModel(int accountId, GMGColorContext context)
        {
            //get all custom decision translations
            var CustomDecisions = (from ad in context.ApprovalDecisions
                    join acd in context.ApprovalCustomDecisions on ad.ID equals acd.Decision
                    where acd.Account == accountId
                    group acd by new { acd.Decision, ad.Key }
                        into groupedDecisions
                        select new AccountSettings.ApprovalCustomDecisionViewModel
                        {
                            DecisionId = groupedDecisions.Key.Decision,
                            Key = groupedDecisions.Key.Key,
                            CustomDecisionLanguages =
                                groupedDecisions.Select(
                                    t =>
                                    new AccountSettings.ApprovalCustomDecisionLanguage
                                    {
                                        Id = t.ID,
                                        Locale = t.Locale,
                                        Name = t.Name
                                    }).ToList()
                        }).ToList();

            var existingDecisionIds = CustomDecisions.Select(o => o.DecisionId);

            //if there is no translation then the default is used
            CustomDecisions.AddRange(context.ApprovalDecisions
                                            .Where(t => !existingDecisionIds.Contains(t.ID))
                                            .Select(t => new AccountSettings.ApprovalCustomDecisionViewModel
                                                        {
                                                            DecisionId = t.ID,
                                                            Key = t.Key
                                                        }
                                                    ).ToList());
            return CustomDecisions.OrderBy(d => d.DecisionId).ToList();
        }
    }
}
