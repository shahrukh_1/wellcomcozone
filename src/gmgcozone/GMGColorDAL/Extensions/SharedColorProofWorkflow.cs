﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    [Serializable]
    public partial class SharedColorProofWorkflow : BusinessObjectBase
    {
        /// <summary>
        /// Gets all workflows that are shared to this external user
        /// </summary>
        /// <param name="externalUserId">External user's id</param>
        /// <param name="context">Database context</param>
        /// <returns>List of workflows</returns>
        public static List<SharedColorProofWorkflow> GetSharedWorkflowsByExUserID(int externalUserId, int accId, GMGColorContext context)
        {
            try
            {
                return (from scpw in context.SharedColorProofWorkflows
                        join cpczw in context.ColorProofCoZoneWorkflows on scpw.ColorProofCoZoneWorkflow equals cpczw.ID
                        join cpwf in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpwf.ID
                        join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                        join cpuo in context.Users on cpi.Owner equals cpuo.ID
                        join ac in context.Accounts on cpuo.Account equals ac.ID
                        where
                            ac.ID == accId && scpw.User == externalUserId
                        select scpw).ToList();
        
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetSharedWorkflowsByExUserID()", ex);
            }
        }

       
        /// <summary>
        /// Returns the user for which workflow was shared
        /// </summary>
        /// <param name="workflowId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static User GetUserBySharedWorkflowId(int workflowId, GMGColorContext context)
        {
            try
            {
                return (from scpw in context.SharedColorProofWorkflows
                        join u in context.Users on scpw.User equals u.ID
                        where scpw.ID == workflowId
                        select u).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetExternalUserBySharedWorkflowId()", ex);
            }
        }
    }
}
