using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class PrePressFunction : BusinessObjectBase
    {
        #region Enums

        public enum Functions
        {
            /// <summary>
            /// Key = CKRB
            /// </summary>
            CYMK_RGB_Indicator,
            /// <summary>
            /// Key = OTPW
            /// </summary>
            Overprint_Preview,
            /// <summary>
            /// Key = DSTR
            /// </summary>
            Densitometer,
            /// <summary>
            /// Key = CKSP
            /// </summary>
            CMYK_Separations,
            /// <summary>
            /// Key = ICCP
            /// </summary>
            ICC_Profile_Preview
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods

        public static PrePressFunction GetPrePressFunction(Functions function, GMGColorContext context)
        {
            try
            {

                return
                    (from ppf in context.PrePressFunctions
                     where (
                               (function == Functions.CYMK_RGB_Indicator && ppf.Key == "CKRB") ||
                               (function == Functions.Overprint_Preview && ppf.Key == "OTPW") ||
                               (function == Functions.Densitometer && ppf.Key == "DSTR") ||
                               (function == Functions.CMYK_Separations && ppf.Key == "CKSP") ||
                               (function == Functions.ICC_Profile_Preview && ppf.Key == "ICCP")
                           )
                     select ppf).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static Functions GetPrePressFunction(int prePressFunction, GMGColorContext context)
        {
            try
            {
                string key = (from ppf in context.PrePressFunctions where ppf.ID == prePressFunction select ppf.Key).FirstOrDefault();
                return PrePressFunction.GetPrePressFunction(key);
            }
            finally
            {
            }
        }

        public static Functions GetPrePressFunction(PrePressFunction objPrePressFunction)
        {
            return PrePressFunction.GetPrePressFunction(objPrePressFunction.Key);
        }

        public static Functions GetPrePressFunction(string key)
        {
            switch (key)
            {
                case "CKRB": return Functions.CYMK_RGB_Indicator;
                case "OTPW": return Functions.Overprint_Preview;
                case "DSTR": return Functions.Densitometer;
                case "CKSP": return Functions.CMYK_Separations;
                case "ICCP":
                default: return Functions.ICC_Profile_Preview;
            }
        }

        public static string GetLocalizedPrePressFunctionName(int prePressFunction, GMGColorContext context)
        {
            try
            {
                return
                    PrePressFunction.GetLocalizedPrePressFunctionName(
                        (from ppf in context.PrePressFunctions where ppf.ID == prePressFunction select ppf.Key).
                            FirstOrDefault());
            }
            finally
            {
            }
        }

        public static string GetLocalizedPrePressFunctionName(PrePressFunction objPrePressFunction)
        {
            return PrePressFunction.GetLocalizedPrePressFunctionName(objPrePressFunction.Key);
        }

        public static string GetLocalizedPrePressFunctionName(string key)
        {
            switch (key)
            {
                case "CKRB": return Resources.resPrePressCYMK_RGBIndicator;
                case "OTPW": return Resources.resPrePressOverprintPreview;
                case "DSTR": return Resources.resPrePressDensitometer;
                case "CKSP": return Resources.resPrePressCMYKSeparations;
                case "ICCP":
                default: return Resources.resPrePressICCProfilePreview;
            }
        }

        #endregion
    }
}

