using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class Locale : BusinessObjectBase
    {
        #region Enums

        public enum Languages
        {
            /// <summary>
            /// en - US
            /// </summary>
            English,
            /// <summary>
            /// de - DE
            /// </summary>
            German,
            /// <summary>
            /// ko - KR
            /// </summary>
            Korean,
            /// <summary>
            /// ja - JP
            /// </summary>
            Japanese,
            /// <summary>
            /// zh - Hans
            /// </summary>
            ChineseSimplified,
            /// <summary>
            /// zh - Hant
            /// </summary>
            ChineseTraditional,
            /// <summary>
            /// pt - PT
            /// </summary>
            Portuguese,
            /// <summary>
            /// es - ES
            /// </summary>
            Spanish,
            /// <summary>
            /// fr - FR
            /// </summary>
            French,
            /// <summary>
            /// it - IT
            /// </summary>
            Italian,
			/// <summary>
			/// pl -PL
			/// </summary>
			Polish,
			/// <summary>
			/// da -DK
			/// </summary>
			Danish,
			/// <summary>
			/// nl-NL
			/// </summary>
			Dutch,
			/// <summary>
			/// nn-NO
			/// </summary>
			Norwegian
		}

		#endregion

		#region OnPropertyChange Methods
		// Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
		// and OnBusinessObjectNameBOPropertyNameChanged() here

		#endregion

		#region Extension Methods

		public static Languages GetLocale(int localeId, GMGColorContext context)
        {
            try
            {
                return GetLocale((from l in context.Locales where l.ID == localeId select l.Name).FirstOrDefault());
            }
            finally
            {
            }
        }

        public static Languages GetLocale(Locale objLocale)
        {
            return Locale.GetLocale(objLocale.Name);
        }

        public static Locale GetLocale(Languages language, GMGColorContext context)
        {
            try
            {
                return (from l in context.Locales
                        where
                            (
                                (language == Languages.German && l.Name == "de-DE") ||
                                (language == Languages.Korean && l.Name == "ko-KR") ||
                                (language == Languages.Japanese && l.Name == "ja-JP") ||
                                (language == Languages.ChineseSimplified && l.Name == "zh-Hans") ||
                                (language == Languages.ChineseTraditional && l.Name == "zh-Hant") ||
                                (language == Languages.Portuguese && l.Name == "pt-PT") ||
                                (language == Languages.Spanish && l.Name == "es-ES") ||
                                (language == Languages.French && l.Name == "fr-FR") ||
                                (language == Languages.Italian && l.Name == "it-IT") ||
								(language == Languages.Polish && l.Name == "pl-PL") ||
								(language == Languages.Danish && l.Name == "da-DK") ||
								(language == Languages.Dutch && l.Name == "nl-NL") ||
								(language == Languages.Norwegian && l.Name == "nn-NO")
							)
                        select l).FirstOrDefault() ??
                       (from l in context.Locales where l.Name == "en-US" select l).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static Languages GetLocale(string key)
        {
            switch (key)
            {
                case "de-DE": return Languages.German;
                case "ko-KR": return Languages.Korean;
                case "ja-JP": return Languages.Japanese;
                case "zh-Hans": return Languages.ChineseSimplified;
                case "zh-Hant": return Languages.ChineseTraditional;
                case "pt-PT": return Languages.Portuguese;
                case "es-ES": return Languages.Spanish;
                case "fr-FR": return Languages.French;
                case "it-IT": return Languages.Italian;
				case "pl-PL": return Languages.Polish;
				case "da-DK": return Languages.Danish;
				case "nl-NL": return Languages.Dutch;
				case "nn-NO": return Languages.Norwegian;
				case "en-US":
                default: return Languages.English;
            }
        }

        public static string GetLocalizeLocaleName(string key)
        {
            return GetLocalizeLocaleName(Locale.GetLocale(key));
        }

        public static string GetLocalizeLocaleName(int locale, GMGColorContext context)
        {
            try
            {
                return Locale.GetLocalizeLocaleName(
                    Locale.GetLocale((from l in context.Locales where l.ID == locale select l.Name).FirstOrDefault()));
            }
            finally
            {
            }
        }

        public static string GetLocalizeLocaleName(Locale objLocale)
        {
            return Locale.GetLocalizeLocaleName(Locale.GetLocale(objLocale));
        }

        public static string GetLocalizeLocaleName(Languages language)
        {
            switch (language)
            {                
                case Languages.German: return Resources.resLocaleGerman;
                case Languages.Korean: return Resources.resLocaleKorean;
                case Languages.Japanese: return Resources.resLocaleJapanese;
                case Languages.ChineseSimplified: return Resources.resLocaleChineseSimplified;
                case Languages.ChineseTraditional: return Resources.resLocaleChineseTraditional;
                case Languages.Portuguese: return Resources.resLocalePortuguese;
                case Languages.Spanish: return Resources.resLocaleSpanish;
                case Languages.French: return Resources.resLocaleFrench;
                case Languages.Italian: return Resources.resLocaleItalian;
				case Languages.Polish: return Resources.resLocalePolish;
				case Languages.Danish: return Resources.resLocaleDanish;
				case Languages.Dutch: return Resources.resLocaleDutch;
				case Languages.Norwegian: return Resources.resLocaleNorwegian;
				case Languages.English:
                default: return Resources.resLocaleEnglish;
            }
        }

        public static Dictionary<int, string> GetLocalizeLocaleNames(GMGColorContext context)
        {
            try
            {
                return (from l in context.Locales select new {l.ID}).ToDictionary(o => o.ID,
                                                                                  o =>
                                                                                  Locale.GetLocalizeLocaleName(o.ID,
                                                                                                               context));
            }
            finally
            {
            }
        }

        #endregion
    }
}

