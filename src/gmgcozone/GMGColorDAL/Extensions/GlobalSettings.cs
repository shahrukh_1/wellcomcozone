using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class GlobalSetting : BusinessObjectBase
    {
        #region Enum

        public enum Setting
        {
            NotSpecified,
            StoreArchivedFor,
            StoreCompletedFor,
            StroreInRecycleBin
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static GlobalSetting GetGlobalSetting(Setting setting, GMGColorContext context)
        {
            try
            {
                string key = GlobalSetting.GetSetting(setting);
                return (from gs in context.GlobalSettings where gs.Key == key select gs).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static string GetSetting(Setting setting)
        {
            switch (setting)
            {
                case Setting.StoreArchivedFor: return "STAF";
                case Setting.StoreCompletedFor: return "STCF";
                case Setting.StroreInRecycleBin: return "STRB";
                default: return "NotSpecified";
            }
        }

        #endregion
    }
}