using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.IO;
using GMGColorDAL.Common;

namespace GMGColorDAL
{
    public partial class AccountNotificationPreset : BusinessObjectBase
    {
        #region OnPropertyChange Methods

        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Fields

        #endregion

        #region Extension Methods

        /// <summary>
        /// Returns whether or not the preset name is unique in the system
        /// </summary>
        /// <param name="presetName">The preset name.</param>
        /// <param name="id">The id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is preset name unique] [the specified preset name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsPresetNameUnique(string presetName, int? id, GMGColorContext context, int loggedAccountID)
        {
            if (presetName != null)
            {
                return !(from anp in context.AccountNotificationPresets
                         where anp.Name.ToLower() == presetName.ToLower().Trim() && anp.ID != id && anp.Account == loggedAccountID
                         select anp.ID).Any();
            }
            return true;
        }

        /// <summary>
        /// Determines whether [is co zone name unique] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static bool IsPresetNameUnique(string name, GMGColorContext context, int loggedAccountID)
        {
            return IsPresetNameUnique(name, null, context, loggedAccountID);
        }
        #endregion
    }
}

