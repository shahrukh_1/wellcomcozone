﻿
using System.Linq;
using System.Web.Mvc.Html;

namespace GMGColorDAL
{
    public partial class UploadEngineAccountSetting : BusinessObjectBase
    {
        /// <summary>
        /// Retrieves the upload engine settings for the specified account
        /// </summary>
        /// <param name="accountID"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static UploadEngineAccountSetting GetAccountUploadEngineSettings(int accountID, GMGColorContext context)
        {
            UploadEngineAccountSetting accountSetting = (from u in context.UploadEngineAccountSettings
                                                         join a in context.Accounts on u.ID equals a.UploadEngineAccountSetting.ID
                                                         where a.ID == accountID
                                                         select u).SingleOrDefault();
            return accountSetting;
        }

        /// <summary>
        /// Get selected option for duplicated files that were uploaded from the same source folder
        /// </summary>
        /// <param name="accountID">Account id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static int GetAccountDuplicateFileNameOption(int accountID, GMGColorContext context)
        {
            var duplicateOption = (from uedfn in context.UploadEngineDuplicateFileNames
                                                         join u in context.UploadEngineAccountSettings on uedfn.ID equals u.DuplicateFileName
                                                         join a in context.Accounts on u.ID equals a.UploadEngineAccountSetting.ID
                                                         where a.ID == accountID
                                                         select uedfn.Key).FirstOrDefault();
            return duplicateOption;
        }

        /// <summary>
        /// Check whether the collaborate billing plan has media tools enabled
        /// </summary>
        /// <param name="accountID">Logged account id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static bool IsMediaToolsEnabled(int accountID, GMGColorContext context)
        {
            return ((from asp in context.AccountSubscriptionPlans
                     join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                     join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                     join am in context.AppModules on bpt.AppModule equals am.ID
                     where
                         asp.Account == accountID && bpt.EnableMediaTools &&
                         am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate
                     select asp.ID).Any() 
                     ||
                    (from spi in context.SubscriberPlanInfoes
                        join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where
                            spi.Account == accountID && bpt.EnableMediaTools == true &&
                            am.Key == (int)GMG.CoZone.Common.AppModule.Collaborate
                        select spi.ID).Any()
                );
        }
    }
}
