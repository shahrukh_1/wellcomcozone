using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;


using RSC = GMGColor.Resources;

namespace GMGColorDAL
{
    public class Company_Validation
    {
        [Required(ErrorMessageResourceName = "reqCompanyName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "reqAddress1", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Address1 { get; set; }

        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Address2 { get; set; }

        [Required(ErrorMessageResourceName = "reqCity", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string City { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string State { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Postcode { get; set; }

        [Required(ErrorMessageResourceName = "reqCountry", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int Country { get; set; }

    }

    [MetadataType(typeof(Company_Validation))]
    public partial class Company : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
        #endregion
    }
}

