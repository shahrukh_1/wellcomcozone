using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class EventGroup : BusinessObjectBase
    {
        #region Enums

        public enum GroupName
        {
            /// <summary>
            /// Key = C
            /// </summary>
            Comments,
            /// <summary>
            /// Key = A
            /// </summary>
            Approvals,
            /// <summary>
            /// Key = U
            /// </summary>
            Users,
            /// <summary>
            /// Key = G
            /// </summary>
            Groups,
            /// <summary>
            /// Key = AC
            /// </summary>
            Account,            
            /// <summary>
            /// Key = DL
            /// </summary>
            Deliver,
            /// <summary>
            /// Key = IU
            /// </summary>
            InvitedUser,
            /// <summary>
            /// Key = HA
            /// </summary>
            HostAdmin,
            /// <summary>
            /// Key = PF
            /// </summary>
            ProjectFolder
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static EventGroup GetEventGroup(GroupName eventGroup, GMGColorContext context)
        {
            return (from eg in context.EventGroups
                    where
                        (
                            (eventGroup == GroupName.Comments && eg.Key == "C") ||
                            (eventGroup == GroupName.Approvals && eg.Key == "A") ||
                            (eventGroup == GroupName.Users && eg.Key == "U") ||
                            (eventGroup == GroupName.Groups && eg.Key == "G") ||
                            eventGroup == GroupName.Deliver && eg.Key == "DL" ||                           
                            eventGroup == GroupName.InvitedUser && eg.Key == "IU" ||
                            eventGroup == GroupName.HostAdmin && eg.Key == "HA" ||
                            eventGroup == GroupName.ProjectFolder && eg.Key == "PF"
                        )
                    select eg).FirstOrDefault() ??
                    (from eg in context.EventGroups where eg.Key == "AC" select eg).FirstOrDefault();
        }

        public static GroupName GetEventGroup(int eventGroupId, GMGColorContext context)
        {
            string key;
            key = (from eg in context.EventGroups where eg.ID == eventGroupId select eg.Key).FirstOrDefault();

            return GetEventGroup(key);
        }

        public static GroupName GetEventGroup(string eventGroupKey)
        {
            switch (eventGroupKey)
            {
                case "C": return GroupName.Comments;
                case "A": return GroupName.Approvals;
                case "U": return GroupName.Users;
                case "G": return GroupName.Groups;               
                case "DL": return GroupName.Deliver;
                case "IU": return GroupName.InvitedUser;
                case "HA": return GroupName.HostAdmin;
                case "PF": return GroupName.ProjectFolder;
                default: return GroupName.Account;
            }
        }

        public static int GetEventGroupModuleID(int evGrpId, GMGColorContext context)
        {
            return (from rpev in context.RolePresetEventTypes
                    join rl in context.Roles on rpev.Role equals rl.ID
                    join evtp in context.EventTypes on rpev.EventType equals evtp.ID
                    join evgrp in context.EventGroups on evtp.EventGroup equals evgrp.ID
                    join apm in context.AppModules on rl.AppModule equals apm.ID
                    where evgrp.ID == evGrpId && apm.ID != 5
                    select apm.ID).FirstOrDefault();
        }

        public static string GetKey(GroupName eventGroupType)
        {
            switch (eventGroupType)
            {
                case GroupName.Comments: return "C";
                case GroupName.Approvals: return "A";
                case GroupName.Users: return "U";
                case GroupName.Groups: return "G";
                case GroupName.Account: return "AC";              
                case GroupName.Deliver: return "DL";
                case GroupName.InvitedUser: return "IU";
                case GroupName.HostAdmin: return "HA";
                case GroupName.ProjectFolder: return "PF";
            }
            return String.Empty;
        }

        public static string GetLabelByNotificationEventGroupName(GroupName eventGroupName)
        {
            switch (eventGroupName)
            {
                case GroupName.Comments: return Resources.lblNotificationEventGroup_Comments;
                case GroupName.Approvals: return Resources.lblNotificationEventGroup_Approvals;
                case GroupName.Users: return Resources.lblNotificationEventGroup_Users;
                case GroupName.Groups: return Resources.lblNotificationEventGroup_Groups;
                case GroupName.Account: return Resources.lblAccount;               
                case GroupName.Deliver: return Resources.lblDeliver;
                case GroupName.InvitedUser: return Resources.lblInvitedUser;
                case GroupName.HostAdmin: return Resources.lblHostAdmin;               
            }
            return String.Empty;
        }
        #endregion
    }
}

