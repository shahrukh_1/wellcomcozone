using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ColorProofWorkflowInstanceView : BusinessObjectBase
    {        
        #region Extension Methods

        // Put methods to manipulate Business Objects here. Add, Update etc
        public static bool IsWorkflowNameUnique(string name, int instanceId, GMGColorContext context)
        {
            return IsWorkflowNameUnique(name, instanceId, null, context);
        }

        /// <summary>
        /// Determines whether [is workflow name unique] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="instanceId">The instance id.</param>
        /// <param name="id">The id.</param>
        /// <returns>
        ///   <c>true</c> if [is workflow name unique] [the specified name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsWorkflowNameUnique(string name, int instanceId, int? id, GMGColorContext context)
        {
            return id.HasValue
                        ? !(from cpwiv in context.ColorProofWorkflowInstanceViews
                            where
                                cpwiv.CoZoneName == name.TrimStart().TrimEnd() &&
                                cpwiv.ColorProofInstance == instanceId &&
                                cpwiv.ColorProofCoZoneWorkflowID != id.Value
                            select cpwiv.IsAvailable).Any()
                        : !(from cpwiv in context.ColorProofWorkflowInstanceViews
                            where
                                cpwiv.CoZoneName == name.TrimStart().TrimEnd() &&
                                cpwiv.ColorProofInstance == instanceId
                            select cpwiv.IsAvailable).Any();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the obj color proof instance.
        /// </summary>
        public ColorProofInstance objColorProofInstance(GMGColorContext context)
        {
            return (from cpi in context.ColorProofInstances where cpi.ID == ColorProofInstance select cpi).FirstOrDefault();
        }

        /// <summary>
        /// Gets the supported spot colors for web.
        /// </summary>
        public string SupportedSpotColorsForWeb
        {
            get { return (SupportedSpotColors ?? string.Empty).Replace(Environment.NewLine, "<br/>"); }
        }

        public static List<ColorProofWorkflowInstanceView> GetList(int accountId, GMGColorContext context, string searchCoZoneName = null)
        {
            return !string.IsNullOrEmpty(searchCoZoneName)
                       ? (from cpwiv in context.ColorProofWorkflowInstanceViews
                          join cpi in context.ColorProofInstances on cpwiv.ColorProofInstance equals cpi.ID
                          join u in context.Users on cpi.Owner equals u.ID
                          join ac in context.Accounts on u.Account equals ac.ID 
                          where
                              cpwiv.CoZoneName.ToLower().Contains(searchCoZoneName.ToLower()) &&
                              u.Account == accountId
                          select new
                          {
                              CoZoneName = cpwiv.CoZoneName,
                              SystemName = cpwiv.SystemName,
                              ProofStandard = cpwiv.ProofStandard,
                              ColorProofCoZoneWorkflowID = cpwiv.ColorProofCoZoneWorkflowID,
                              ColorProofInstance = cpwiv.ColorProofInstance,
                              ColorProofWorkflow = cpwiv.ColorProofWorkflow,
                              ColorProofWorkflowID = cpwiv.ColorProofWorkflowID,
                              Guid = cpwiv.Guid,
                              IsActivated = cpwiv.IsActivated,
                              IsAvailable = cpwiv.IsAvailable,
                              IsInlineProofVerificationSupported = cpwiv.IsInlineProofVerificationSupported,
                              MaximumUsablePaperWidth = cpwiv.MaximumUsablePaperWidth,
                              Name = cpwiv.Name,
                              SupportedSpotColors = cpwiv.SupportedSpotColors,
                              TransmissionTimeout = cpwiv.TransmissionTimeout,
                              IsFromCurrentAccount = (ac.ID == accountId)
                          }).ToList()
                                .Union
                             (from scpw in context.SharedColorProofWorkflows
                              join u in context.Users on scpw.User equals u.ID
                              join cpwiv in context.ColorProofWorkflowInstanceViews on scpw.ColorProofCoZoneWorkflow equals cpwiv.ColorProofCoZoneWorkflowID
                              join cpi in context.ColorProofInstances on cpwiv.ColorProofInstance equals cpi.ID
                              join cpuo in context.Users on cpi.Owner equals cpuo.ID
                              join ac in context.Accounts on cpuo.Account equals ac.ID
                              join acs in context.AccountStatus on ac.Status equals acs.ID
                              where
                                  u.Account == accountId &&
                                  cpwiv.CoZoneName.ToLower().Contains(searchCoZoneName.ToLower()) &&
                                  scpw.IsAccepted == true && scpw.IsEnabled &&
                                  acs.Key != "D"
                              select new 
                              {
                                  CoZoneName = cpwiv.CoZoneName,
                                  SystemName = cpwiv.SystemName,
                                  ProofStandard = cpwiv.ProofStandard,
                                  ColorProofCoZoneWorkflowID = cpwiv.ColorProofCoZoneWorkflowID,
                                  ColorProofInstance = cpwiv.ColorProofInstance,
                                  ColorProofWorkflow = cpwiv.ColorProofWorkflow,
                                  ColorProofWorkflowID = cpwiv.ColorProofWorkflowID,
                                  Guid = cpwiv.Guid,
                                  IsActivated = cpwiv.IsActivated,
                                  IsAvailable = cpwiv.IsAvailable,
                                  IsInlineProofVerificationSupported = cpwiv.IsInlineProofVerificationSupported,
                                  MaximumUsablePaperWidth = cpwiv.MaximumUsablePaperWidth,
                                  Name = cpwiv.Name,
                                  SupportedSpotColors = cpwiv.SupportedSpotColors,
                                  TransmissionTimeout = cpwiv.TransmissionTimeout,
                                  IsFromCurrentAccount = (ac.ID == accountId)
                              }).ToList()
                              .Select(cpwiv => new ColorProofWorkflowInstanceView
                              {
                                  CoZoneName = cpwiv.CoZoneName,
                                  SystemName = cpwiv.SystemName,
                                  ProofStandard = cpwiv.ProofStandard,
                                  ColorProofCoZoneWorkflowID = cpwiv.ColorProofCoZoneWorkflowID,
                                  ColorProofInstance = cpwiv.ColorProofInstance,
                                  ColorProofWorkflow = cpwiv.ColorProofWorkflow,
                                  ColorProofWorkflowID = cpwiv.ColorProofWorkflowID,
                                  Guid = cpwiv.Guid,
                                  IsActivated = cpwiv.IsActivated,
                                  IsAvailable = cpwiv.IsAvailable,
                                  IsInlineProofVerificationSupported = cpwiv.IsInlineProofVerificationSupported,
                                  MaximumUsablePaperWidth = cpwiv.MaximumUsablePaperWidth,
                                  Name = cpwiv.Name,
                                  SupportedSpotColors = cpwiv.SupportedSpotColors,
                                  TransmissionTimeout = cpwiv.TransmissionTimeout,
                                  IsFromCurrentAccount = cpwiv.IsFromCurrentAccount
                              }).ToList()
                       : (from cpwiv in context.ColorProofWorkflowInstanceViews
                          join cpi in context.ColorProofInstances on cpwiv.ColorProofInstance equals cpi.ID
                          join u in context.Users on cpi.Owner equals u.ID
                          join ac in context.Accounts on u.Account equals ac.ID 
                          where
                              u.Account == accountId
                          select new 
                          {
                              CoZoneName = cpwiv.CoZoneName,
                              SystemName = cpwiv.SystemName,
                              ProofStandard = cpwiv.ProofStandard,
                              ColorProofCoZoneWorkflowID = cpwiv.ColorProofCoZoneWorkflowID,
                              ColorProofInstance = cpwiv.ColorProofInstance,
                              ColorProofWorkflow = cpwiv.ColorProofWorkflow,
                              ColorProofWorkflowID = cpwiv.ColorProofWorkflowID,
                              Guid = cpwiv.Guid,
                              IsActivated = cpwiv.IsActivated,
                              IsAvailable = cpwiv.IsAvailable,
                              IsInlineProofVerificationSupported = cpwiv.IsInlineProofVerificationSupported,
                              MaximumUsablePaperWidth = cpwiv.MaximumUsablePaperWidth,
                              Name = cpwiv.Name,
                              SupportedSpotColors = cpwiv.SupportedSpotColors,
                              TransmissionTimeout = cpwiv.TransmissionTimeout,
                              IsFromCurrentAccount = (ac.ID == accountId)
                          }).ToList()
                                .Union
                             (from scpw in context.SharedColorProofWorkflows
                              join u in context.Users on scpw.User equals u.ID
                              join cpwiv in context.ColorProofWorkflowInstanceViews on scpw.ColorProofCoZoneWorkflow equals cpwiv.ColorProofCoZoneWorkflowID
                              join cpi in context.ColorProofInstances on cpwiv.ColorProofInstance equals cpi.ID
                              join cpuo in context.Users on cpi.Owner equals cpuo.ID
                              join ac in context.Accounts on cpuo.Account equals ac.ID 
                              where
                                  u.Account == accountId &&
                                  scpw.IsAccepted == true && scpw.IsEnabled
                              select new 
                              {
                                  CoZoneName = cpwiv.CoZoneName,
                                  SystemName = cpwiv.SystemName,
                                  ProofStandard = cpwiv.ProofStandard,
                                  ColorProofCoZoneWorkflowID = cpwiv.ColorProofCoZoneWorkflowID,
                                  ColorProofInstance = cpwiv.ColorProofInstance,
                                  ColorProofWorkflow = cpwiv.ColorProofWorkflow,
                                  ColorProofWorkflowID = cpwiv.ColorProofWorkflowID,
                                  Guid = cpwiv.Guid,
                                  IsActivated = cpwiv.IsActivated,
                                  IsAvailable = cpwiv.IsAvailable,
                                  IsInlineProofVerificationSupported = cpwiv.IsInlineProofVerificationSupported,
                                  MaximumUsablePaperWidth = cpwiv.MaximumUsablePaperWidth,
                                  Name = cpwiv.Name,
                                  SupportedSpotColors = cpwiv.SupportedSpotColors,
                                  TransmissionTimeout = cpwiv.TransmissionTimeout,
                                  IsFromCurrentAccount = (ac.ID == accountId)
                              }).ToList()
                              .Select(cpwiv => new ColorProofWorkflowInstanceView
                              {
                                  CoZoneName = cpwiv.CoZoneName,
                                  SystemName = cpwiv.SystemName,
                                  ProofStandard = cpwiv.ProofStandard,
                                  ColorProofCoZoneWorkflowID = cpwiv.ColorProofCoZoneWorkflowID,
                                  ColorProofInstance = cpwiv.ColorProofInstance,
                                  ColorProofWorkflow = cpwiv.ColorProofWorkflow,
                                  ColorProofWorkflowID = cpwiv.ColorProofWorkflowID,
                                  Guid = cpwiv.Guid,
                                  IsActivated = cpwiv.IsActivated,
                                  IsAvailable = cpwiv.IsAvailable,
                                  IsInlineProofVerificationSupported = cpwiv.IsInlineProofVerificationSupported,
                                  MaximumUsablePaperWidth = cpwiv.MaximumUsablePaperWidth,
                                  Name = cpwiv.Name,
                                  SupportedSpotColors = cpwiv.SupportedSpotColors,
                                  TransmissionTimeout = cpwiv.TransmissionTimeout,
                                  IsFromCurrentAccount = cpwiv.IsFromCurrentAccount
                              }).ToList();
        }
        #endregion

        #region Enums

        /// <summary>
        /// Enum For WorkFlow Status
        /// </summary>
        public enum WorkFlowStatusEnum { Inactive = 0, Active = 1 };

        #endregion
    }
}

