using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class EventType : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
        #endregion

        #region Static Methods

        public static TypeEnum GetTypeEnum(string eventType) // TODO - remove notification
        {
            switch (eventType)
            {
                case "CCM": return TypeEnum.When_a_comment_is_made;
                case "CRC": return TypeEnum.Replies_to_my_comments;
                case "CNC": return TypeEnum.New_comments_made;
                case "AAA": return TypeEnum.New_approvals_added;
                case "ADW": return TypeEnum.Approvals_was_downloaded;
                case "AAU": return TypeEnum.Approvals_was_updated;
                case "AAD": return TypeEnum.Approvals_was_deleted;
                case "ASC": return TypeEnum.Approvals_status_changed;
                case "AVC": return TypeEnum.New_version_was_created;
                case "AAS": return TypeEnum.Approval_was_shared;
                case "UUA": return TypeEnum.User_was_added;
                case "GGC": return TypeEnum.Group_was_created;
                case "MPC": return TypeEnum.Plan_was_changed;
                case "MAD": return TypeEnum.Account_was_deleted;
                case "MAA": return TypeEnum.Account_was_added;
                case "DFS": return TypeEnum.Deliver_File_Submitted;
                case "DFE": return TypeEnum.Deliver_File_Errored;
                case "DFC": return TypeEnum.Deliver_File_Completed;
                case "AWR": return TypeEnum.Workstation_Registered;
                case "IAR": return TypeEnum.Access_Revoked;
                case "IUD": return TypeEnum.Invited_User_Deleted;
                case "IFS": return TypeEnum.Invited_User_File_Submitted;
                case "HSA": return TypeEnum.Share_Request_Accepted;
                case "HSD": return TypeEnum.Shared_Workflow_Deleted;
                case "HUD": return TypeEnum.Host_Admin_User_Deleted;
                case "HFS": return TypeEnum.Host_Admin_User_File_Submitted;
                case "ACC": return TypeEnum.ApprovalChangesComplete;
                case "APR": return TypeEnum.ApprovalWasRejected;
                case "APO": return TypeEnum.ApprovalOverdue;
                case "ACP": return TypeEnum.ApprovalStatusChangedByPdm;
                case "PWD": return TypeEnum.PhaseWasDeactivated;
                case "PWA": return TypeEnum.PhaseWasActivated;
                case "WPR": return TypeEnum.WorkflowPhaseRerun;
                case "CIE": return TypeEnum.ChecklistItemEdited;
                case "AJC": return TypeEnum.ApprovalJobComplete;
                case "PFN": return TypeEnum.NewProjectFolderAdded;
                case "PFS": return TypeEnum.ProjectFolderWasShared;
                case "PFU": return TypeEnum.ProjectFolderWasUpdated;
                default:    return TypeEnum.Deliver_File_Timeout;
            }
        }

        /// <summary>
        /// Returns deliver event type key based on  event type
        /// </summary>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public static string GetDeliverEventTypeKey(TypeEnum eventType)
        {
            switch (eventType)
            {
                case TypeEnum.Deliver_File_Submitted: return "DFS";
                case TypeEnum.Deliver_File_Errored  : return "DFE";
                case TypeEnum.Deliver_File_Completed: return "DFC";
                default : return "DFT";
            }
        }

        /// <summary>
        /// Gets the event type by groups.
        /// </summary>
        /// <param name="rolesId">The roles id.</param>
        /// <param name="eventGrpIds">The event GRP ids.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<EventType> GetEventTypeByGroups(List<int> rolesId, List<int> eventGrpIds, GMGColorContext context)
        {
            try
            {
                return (from rpet in context.RolePresetEventTypes
                        join r in context.Roles on rpet.Role equals r.ID
                        join p in context.Presets on rpet.Preset equals p.ID
                        join et in context.EventTypes on rpet.EventType equals et.ID
                        join eg in context.EventGroups on et.EventGroup equals eg.ID
                        where
                            rolesId.Contains(r.ID) && p.Key == "H" &&
                            eventGrpIds.Contains(eg.ID)
                        select et).ToList();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Determines whether [has right to event type] [the specified event type].
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [has right to event type] [the specified event type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasRightToEventType(string eventType, int userId, GMGColorContext context)
        {
            return (from rpet in context.RolePresetEventTypes
                    join et in context.EventTypes on rpet.EventType equals et.ID
                    join p in context.Presets on rpet.Preset equals p.ID
                    join u in context.Users on p.ID equals u.Preset
                    join nf in context.NotificationFrequencies on u.NotificationFrequency equals nf.ID
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on rpet.Role equals r.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    where
                        u.ID == userId && us.Key == "A" && p.Key != "C" && et.Key == eventType && ur.Role == r.ID &&
                        nf.Key != "NVR" 
                    select rpet.ID).Any()
                   ||
                   (from rpet in context.RolePresetEventTypes
                    join et in context.EventTypes on rpet.EventType equals et.ID
                    join p in context.Presets on rpet.Preset equals p.ID
                    join u in context.Users on p.ID equals u.Preset
                    join nf in context.NotificationFrequencies on u.NotificationFrequency equals nf.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join ucpetv in context.UserCustomPresetEventTypeValues on rpet.ID equals
                        ucpetv.RolePresetEventType
                    where
                        u.ID == userId && ucpetv.User == userId && us.Key == "A" && p.Key == "C" && et.Key == eventType &&
                        nf.Key != "NVR"
                    select rpet.ID).Any()
                    ||
                     (from anp in context.AccountNotificationPresets
                    join u in context.Users on anp.Account equals u.Account
                    join ur in context.UserRoles on u.ID equals ur.User
                    join r in context.Roles on ur.Role equals r.ID
                    join rpet in context.RolePresetEventTypes on r.ID equals rpet.Role
                    join anpet in context.AccountNotificationPresetEventTypes on anp.ID equals
                        anpet.NotificationPreset
                    join ns in context.NotificationSchedules on anp.ID equals ns.NotificationPreset
                    join et in context.EventTypes on anpet.EventType equals et.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    let isInScheduleUser = (from nsu in context.NotificationScheduleUsers
                        where nsu.User == u.ID
                        select nsu.ID).Any()
                    let isInScheduleUserGroup = (from ugu in context.UserGroupUsers
                        join nsug in context.NotificationScheduleUserGroups on ugu.UserGroup equals nsug.UserGroup
                        where ugu.User == u.ID
                        select ugu.ID).Any()
                    where
                        u.ID == userId && et.Key == eventType && rpet.EventType == et.ID && us.Key == "A" &&
                        (isInScheduleUser || isInScheduleUserGroup)
                      select u.ID).Any();
        }

        #endregion

        #region Enums

        public enum TypeEnum  // TODO - remove notification
        {
            /// <summary>
            /// key = CCM
            /// </summary>
            When_a_comment_is_made,
            /// <summary>
            /// key = CRC 
            /// </summary>
            Replies_to_my_comments,
            /// <summary>
            /// key = CNC
            /// </summary>
            New_comments_made,
            /// <summary>
            /// key = AAA
            /// </summary>
            New_approvals_added,
            /// <summary>
            /// key = ADW
            /// </summary>
            Approvals_was_downloaded,
            /// <summary>
            /// key = AAU
            /// </summary>
            Approvals_was_updated,
            /// <summary>
            /// key = AAD
            /// </summary>
            Approvals_was_deleted,
            /// <summary>
            /// key = ASC
            /// </summary>
            Approvals_status_changed,
            /// <summary>
            /// key = ACP
            /// </summary>
            ApprovalStatusChangedByPdm,
            /// <summary>
            /// key = AVC 
            /// </summary>
            New_version_was_created,
            /// <summary>
            /// key = AAS
            /// </summary>
            Approval_was_shared,
            ///// <summary>
            ///// key = AFC
            ///// </summary>
            //Folder_was_created,
            ///// <summary>
            ///// AFD
            ///// </summary>
            //Folder_was_deleted,
            /// <summary>
            /// key = UUA
            /// </summary>
            User_was_added,
            /// <summary>
            /// key = GGC
            /// </summary>
            Group_was_created,
            /// <summary>
            /// key = MPC
            /// </summary>
            Plan_was_changed,
            /// <summary>
            /// key = MAD
            /// </summary>
            Account_was_deleted,
            /// <summary>
            /// key = MAA
            /// </summary>
            Account_was_added,
            /// <summary>
            /// key = MAA
            /// </summary>
            ColorProofInstance_was_added, //TODO - Not handled in NotificationProcess.SendNotifications
            /// <summary>
            /// CPWWA
            /// </summary>
            ColorProofCoZoneWorkflow_was_added, //TODO Not handled in NotificationProcess.SendNotifications
            /// <summary>
            /// 
            /// </summary>
            DeliverJob_Timeout, //Currently not used, old version when ColorProof Admin received Deliver Timeout Mail(TODO to be removed)
            Workstation_Registered,
            /// <summary>
            /// key = MAA
            /// </summary>
            Deliver_File_Submitted,
            /// <summary>
            /// key = GGC
            /// </summary>
            Deliver_File_Completed,
            /// <summary>
            /// key = MPC
            /// </summary>
            Deliver_File_Errored,
            /// <summary>
            /// key = MAD
            /// </summary>
            Deliver_File_Timeout,            
            /// <summary>
            /// key = IAR
            /// </summary>
            Access_Revoked,
            /// <summary>
            /// key = IUD
            /// </summary>
            Invited_User_Deleted,
            /// <summary>
            /// key = IFS
            /// </summary>
            Invited_User_File_Submitted,
            /// <summary>
            /// key = HSA
            /// </summary>
            Share_Request_Accepted,
            /// <summary>
            /// key = HSD
            /// </summary>
            Shared_Workflow_Deleted,
            /// <summary>
            /// key = HUD
            /// </summary>
            Host_Admin_User_Deleted,
            /// <summary>
            /// key = HFS
            /// </summary>
            Host_Admin_User_File_Submitted,
            /// <summary>
            /// key = ACC
            /// </summary>
            ApprovalChangesComplete,
            /// <summary>
            /// key = APR
            /// </summary>
            ApprovalWasRejected,
            /// <summary>
            /// key = APO
            /// </summary>
            ApprovalOverdue,
            /// <summary>
            /// key = PWD
            /// </summary>
            PhaseWasDeactivated,
            /// <summary>
            /// key = PWA
            /// </summary>
            PhaseWasActivated,
            /// <summary>
            /// key = WPR
            /// </summary>
            WorkflowPhaseRerun,
            /// <summary>
            /// key = CIE
            /// </summary>>
            ChecklistItemEdited,
            /// <summary>
            /// key = AJC
            /// </summary>>
            ApprovalJobComplete = 59,
            NewProjectFolderAdded,
            ProjectFolderWasShared,
            ProjectFolderWasUpdated

        };

        #endregion
    }
}

