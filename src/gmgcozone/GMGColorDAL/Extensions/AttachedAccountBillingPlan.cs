using System;
using System.Linq;

namespace GMGColorDAL
{
    public partial class AttachedAccountBillingPlan : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static decimal GetPlanPrice(int accountId, int billingPlanId, GMGColorContext context, decimal rate = 1)
        {
            try
            {
                decimal price = 0;
                if (AccountType.GetAccountType(accountId, context) == AccountType.Type.GMGColor)
                {
                    price = (from bp in context.BillingPlans where bp.ID == billingPlanId select bp.Price).FirstOrDefault();
                }
                else
                {
                    price =
                        (from aabp in context.AttachedAccountBillingPlans
                         where 
                            aabp.Account == accountId && 
                            aabp.BillingPlan == billingPlanId
                         select aabp.NewPrice).FirstOrDefault().GetValueOrDefault();
                }
                return Math.Round(price * rate, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        public static decimal GetPlanPrice(int accountId, int billingPlanId, decimal planPriceprice, AccountType.Type accountType, GMGColorContext context, decimal rate = 1)
        {
            try
            {
                decimal price = 0;
                if (accountType == AccountType.Type.GMGColor)
                {
                    price = planPriceprice;
                }
                else
                {
                    price =
                        (from aabp in context.AttachedAccountBillingPlans
                         where
                            aabp.Account == accountId &&
                            aabp.BillingPlan == billingPlanId
                         select aabp.NewPrice).FirstOrDefault().GetValueOrDefault();
                }
                return Math.Round(price * rate, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        public static decimal GetCostPerProof(int accountId, int billingPlanId, GMGColorContext context, decimal rate = 1)
        {
            try
            {
                decimal cost = 0;
                bool isFixed = (from bp in context.BillingPlans where bp.ID == billingPlanId select bp.IsFixed).FirstOrDefault();
                if (isFixed) return 0;

                if (AccountType.GetAccountType(accountId, context) == AccountType.Type.GMGColor)
                {
                    var billingPlan = (from bp in context.BillingPlans where bp.ID == billingPlanId select new {bp.Price, bp.Proofs}).FirstOrDefault();
                    cost = billingPlan != null ? billingPlan.Proofs.GetValueOrDefault() != 0 ? (billingPlan.Price * rate) / billingPlan.Proofs.GetValueOrDefault() : 0
                                                : 0;
                }
                else
                {
                    var billingPlan = (from aabp in context.AttachedAccountBillingPlans
                                       where aabp.Account == accountId && aabp.BillingPlan == billingPlanId
                                       select aabp.NewProofPrice).FirstOrDefault();
                    cost = billingPlan != null ? billingPlan.GetValueOrDefault() : 0;
                }
                return Math.Round(cost, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        public static decimal GetCostPerProof(int accountId, int billingPlanId, bool isFixed, GMGColorContext context, decimal rate = 1)
        {
            try
            {
                decimal cost = 0;
                if (isFixed) return 0;

                if (AccountType.GetAccountType(accountId, context) == AccountType.Type.GMGColor)
                {
                    var billingPlan = (from bp in context.BillingPlans where bp.ID == billingPlanId select new { bp.Price, bp.Proofs }).FirstOrDefault();
                    cost = billingPlan != null ? billingPlan.Proofs.GetValueOrDefault() != 0 ? (billingPlan.Price * rate) / billingPlan.Proofs.GetValueOrDefault() : 0
                                                : 0;
                }
                else
                {
                    var billingPlan = (from aabp in context.AttachedAccountBillingPlans
                                       where aabp.Account == accountId && aabp.BillingPlan == billingPlanId
                                       select aabp.NewProofPrice).FirstOrDefault();
                    cost = billingPlan != null ? billingPlan.GetValueOrDefault() : 0;
                }
                return Math.Round(cost, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        public static decimal GetCostPerProof(int accountId, BillingPlan billingPlan, AccountType.Type accountType, GMGColorContext context, decimal rate = 1)
        {
            try
            {
                decimal cost = 0;
                if (billingPlan.IsFixed) return 0;

                if (accountType == AccountType.Type.GMGColor)
                {
                    cost = billingPlan.Proofs.GetValueOrDefault() != 0
                        ? (billingPlan.Price*rate)/billingPlan.Proofs.GetValueOrDefault()
                        : 0;

                }
                else
                {
                    var newPrice = (from aabp in context.AttachedAccountBillingPlans
                                       where aabp.Account == accountId && aabp.BillingPlan == billingPlan.ID
                                       select aabp.NewProofPrice).FirstOrDefault();
                    cost = newPrice != null ? newPrice.GetValueOrDefault() : 0;
                }
                return Math.Round(cost, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        public static decimal GetCostPerProof(int accountId, int billingPlanId, AccountType.Type accountType, GMGColorContext context)
        {
            try
            {
                decimal cost = 0;

                if (accountType == AccountType.Type.GMGColor)
                {
                    var billingPlan = (from bp in context.BillingPlans where bp.ID == billingPlanId select new { bp.Price, bp.Proofs }).FirstOrDefault();
                    cost = billingPlan != null ? billingPlan.Proofs.GetValueOrDefault() != 0 ? billingPlan.Price  / billingPlan.Proofs.GetValueOrDefault() : 0
                                                : 0;
                }
                else
                {
                    var billingPlan = (from aabp in context.AttachedAccountBillingPlans
                                       where aabp.Account == accountId && aabp.BillingPlan == billingPlanId
                                       select aabp.NewProofPrice).FirstOrDefault();
                    cost = billingPlan != null ? billingPlan.GetValueOrDefault() : 0;
                }
                return Math.Round(cost, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        public static decimal GetCostPerProof(decimal? newProofPrice, BillingPlan billingPlan, AccountType.Type accountType, decimal rate = 1)
        {
            try
            {
                decimal cost = 0;
                if (billingPlan.IsFixed) return 0;

                if (accountType == AccountType.Type.GMGColor)
                {
                    cost = billingPlan.Proofs.GetValueOrDefault() != 0
                        ? (billingPlan.Price*rate)/billingPlan.Proofs.GetValueOrDefault()
                        : 0;

                }
                else
                {
                    cost = newProofPrice != null ? newProofPrice.GetValueOrDefault() : 0;
                }
                return Math.Round(cost, 2, MidpointRounding.AwayFromZero);
            }
            finally
            {
            }
        }

        #endregion
    }
}

