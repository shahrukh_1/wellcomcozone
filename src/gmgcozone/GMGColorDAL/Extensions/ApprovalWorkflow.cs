﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL
{
    public partial class ApprovalWorkflow : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        /// <summary>
        /// Returns whether or not the approval workflow name is unique at the account level
        /// </summary>
        /// <param name="accountId">Logged acount id</param>
        /// <param name="workflowName">The approval workflow name.</param>
        /// <param name="id">The id.</param>
        /// <param name="context">Database context.</param>
        /// <returns>
        ///  <c>true</c> if [is approval workflow name unique] [the specified approval workflow name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsApprovalWorkflowNameUnique(int accountId, string workflowName, int id, GMGColorContext context)
        {
            if (workflowName != null)
            {
                return !(from aw in context.ApprovalWorkflows
                         join ac in context.Accounts on aw.Account equals ac.ID
                         where aw.Name.ToLower() == workflowName.ToLower().Trim() && aw.ID != id && ac.ID == accountId
                         select aw.ID).Any();
            }
            return true;
        }

        #endregion
    }
}
