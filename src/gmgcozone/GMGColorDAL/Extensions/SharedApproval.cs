using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class SharedApproval : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Marks the shared approval as expired.
        /// </summary>
        /// <param name="externalUserId">The external user id.</param>
        /// <param name="approvalId">The approval id.</param>
        /// <returns></returns>
        public static bool MarkSharedApprovalAsExpired(int externalUserId, int approvalId, GMGColorContext context)
        {
            bool result = false;
            SharedApproval sharedApproval = (from sa in context.SharedApprovals
                                                where sa.ExternalCollaborator == externalUserId && sa.Approval == approvalId
                                                select sa).FirstOrDefault();
            if (sharedApproval != null)
            {
                sharedApproval.IsExpired = true;
                context.SaveChanges();
                result = true;
            }
            return result;
        }

        public static bool MarkSharedApprovalAsNotExpired(int externalUserId, int approvalId, GMGColorContext context)
        {
            bool result = false;
            SharedApproval sharedApproval = (from sa in context.SharedApprovals
                                             where sa.ExternalCollaborator == externalUserId && sa.Approval == approvalId
                                             select sa).FirstOrDefault();
            if (sharedApproval != null)
            {
                sharedApproval.IsExpired = false;
                context.SaveChanges();
                result = true;
            }
            return result;
        }
         

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <param name="approvalId">The approval id.</param>
        /// <param name="primaryRecipient">The primary recipient.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static SharedApproval GetObject(int approvalId, int primaryRecipient, GMGColorContext context)
        {
            try
            {
                return (from sa in context.SharedApprovals
                        where sa.Approval == approvalId && sa.ExternalCollaborator == primaryRecipient
                        select sa).FirstOrDefault();
            }
            finally
            {
            }
        }

        #endregion

    }
}

