﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL
{
    public partial class CMSManual: BusinessObjectBase
    {
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
        public static List<CMSManual> GetAllCMSManuals(GMGColorContext context)
        {
            List<CMSManual> result = new List<CMSManual>();

            return (from cmsm in context.CMSManuals
                    where cmsm.IsDeleted == false
                    orderby cmsm.Name ascending
                    select cmsm).ToList();
        }

        public List<CMSManual> GetManuals(List<string> fileGuids, GMGColorContext context)
        {
            return (from cmsm in context.CMSManuals
                    where fileGuids.Contains(cmsm.FileGuid)
                    select cmsm).ToList();
        }

        #endregion
    }
}
