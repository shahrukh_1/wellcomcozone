using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class ApprovalCollaboratorRole : BusinessObjectBase
    {
        #region Enums

        public enum ApprovalRoleName
        {
            ReadOnly = 1,
            Reviewer,
            ApproverAndReviewer,
            Retoucher
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static ApprovalRoleName GetRole(int role, GMGColorContext context)
        {
            string key = (from acr in context.ApprovalCollaboratorRoles where acr.ID == role select acr.Key).
                    FirstOrDefault() ?? string.Empty;

            return ApprovalCollaboratorRole.GetRole(key);
        }

		public static Dictionary<int, ApprovalRoleName> GetRoles( GMGColorContext context)
		{
			var dictionary = new Dictionary<int, ApprovalRoleName>();
			var collaboratorsRoles = (from acr in context.ApprovalCollaboratorRoles select acr).ToList();
			foreach (var collaboratorsRole in collaboratorsRoles)
			{
				var approvalRoleName = GetRole(collaboratorsRole.Key);
				dictionary.Add(collaboratorsRole.ID, approvalRoleName);
			}

			return dictionary;
		}

		public static ApprovalRoleName GetApprovalCollaboratorRole(int userId, int approvalId, GMGColorContext context)
        {
            string key;
            key =
                (from ac in context.ApprovalCollaborators
                    join acr in context.ApprovalCollaboratorRoles on ac.ApprovalCollaboratorRole equals acr.ID
                    where ac.Collaborator == userId && ac.Approval == approvalId
                    select acr.Key).
                    FirstOrDefault() ?? string.Empty;

            return ApprovalCollaboratorRole.GetRole(key);
        }

        public static ApprovalRoleName GetApprovalExternalCollaboratorRole(int userId, int approvalId, GMGColorContext context)
        {
            string key;
            key =
                (from sap in context.SharedApprovals
                 join ap in context.Approvals on sap.Approval equals ap.ID
                 join acr in context.ApprovalCollaboratorRoles on sap.ApprovalCollaboratorRole equals acr.ID
                 where sap.ExternalCollaborator == userId && ap.ID == approvalId 
                 select acr.Key).
                    FirstOrDefault() ?? string.Empty;

            return ApprovalCollaboratorRole.GetRole(key);
        }

        public static ApprovalRoleName GetRole(string key)
        {
            switch (key)
            {
                case "ANR": return ApprovalRoleName.ApproverAndReviewer;
                case "RVW": return ApprovalRoleName.Reviewer;
                case "RET": return ApprovalRoleName.Retoucher;
                case "RDO":
                default: return ApprovalRoleName.ReadOnly;
            }
        }

        public static ApprovalCollaboratorRole GetRole(ApprovalRoleName roleName, GMGColorContext context)
        {
            return (from acr in context.ApprovalCollaboratorRoles
                    where
                        ((roleName == ApprovalRoleName.ApproverAndReviewer && acr.Key == "ANR") ||
                            (roleName == ApprovalRoleName.Reviewer && acr.Key == "RVW") ||
                            (roleName == ApprovalRoleName.ReadOnly && acr.Key == "RDO") ||
                            (roleName == ApprovalRoleName.Retoucher && acr.Key == "RET"))
                    select acr).FirstOrDefault();
        }


        public static string GetLocalizedRoleName(int role, GMGColorContext context)
        {
            return ApprovalCollaboratorRole.GetLocalizedRoleName(ApprovalCollaboratorRole.GetRole(role, context));
        }

        public static string GetLocalizedRoleName(ApprovalRoleName userRole)
        {
            switch (userRole)
            {
                case ApprovalRoleName.ApproverAndReviewer: return Resources.resRoleApproverAndReviewer;
                case ApprovalRoleName.Reviewer: return Resources.resRoleReviewer;
                case ApprovalRoleName.ReadOnly: return Resources.resRoleReadOnly;
                case ApprovalRoleName.Retoucher: return Resources.resRoleRetoucher;
                default: return null;
            }
        }

        public static string GetLocalizedRoleName(string key)
        {
            return ApprovalCollaboratorRole.GetLocalizedRoleName(ApprovalCollaboratorRole.GetRole(key));
        }

        public static Dictionary<int, string> GetLocalizedRoleNames(GMGColorContext context)
        {
            try
            {
                return (from acr in context.ApprovalCollaboratorRoles select acr).
                    ToDictionary(o => o.ID, o => ApprovalCollaboratorRole.GetLocalizedRoleName(o.Key));
            }
            finally
            {
            }
        }


        public static int GetDefaultApprovalRole(Role.RoleName roleName, GMGColorContext context)
        {
            switch (roleName)
            {
                case GMGColorDAL.Role.RoleName.AccountAdministrator:
                case GMGColorDAL.Role.RoleName.AccountManager:
                case GMGColorDAL.Role.RoleName.AccountModerator: return ApprovalCollaboratorRole.GetRole(ApprovalCollaboratorRole.ApprovalRoleName.ApproverAndReviewer, context).ID;
                case GMGColorDAL.Role.RoleName.AccountContributor: return ApprovalCollaboratorRole.GetRole(ApprovalCollaboratorRole.ApprovalRoleName.Reviewer, context).ID;
                case GMGColorDAL.Role.RoleName.AccountRetoucher: return ApprovalCollaboratorRole.GetRole(ApprovalCollaboratorRole.ApprovalRoleName.Retoucher, context).ID;
                case GMGColorDAL.Role.RoleName.AccountViewer:
                default: return ApprovalCollaboratorRole.GetRole(ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly, context).ID;
            }
        }

        public static int GetCollaborateDefaultRoleId(int userId, GMGColorContext context)
        {
            var roleKey = (from u in context.Users
                           join ur in context.UserRoles on u.ID equals ur.User
                           join r in context.Roles on ur.Role equals r.ID
                           join am in context.AppModules on r.AppModule equals am.ID
                           where u.ID == userId &&
                                 am.Key == 0
                           select r.Key).FirstOrDefault();

            return GetDefaultApprovalRole(Role.GetRole(roleKey), context);
        }

        public static int GetReadOnlyCollaboratorRoleId(GMGColorContext context)
        {
            return GetRole(ApprovalRoleName.ReadOnly, context).ID;
        }
        public static int GetReviewerCollaboratorRoleId(GMGColorContext context)
        {
            return GetRole(ApprovalRoleName.Reviewer, context).ID;
        }
        public static int GetApproverAndReviewerCollaboratorRoleId(GMGColorContext context)
        {
            return GetRole(ApprovalRoleName.ApproverAndReviewer, context).ID;
        }
        public static int GetRetoucherCollaboratorRoleId(GMGColorContext context)
        {
            return GetRole(ApprovalRoleName.Retoucher, context).ID;
        }

        #endregion
    }
}

