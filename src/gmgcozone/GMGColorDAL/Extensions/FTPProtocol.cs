﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL
{
    public partial class FTPProtocol : BusinessObjectBase
    {
        public static List<CustomModels.XMLEngine.FTPProtocol> GetProtocols(GMGColorContext context = null)
        {
            bool isLocalContext = false;
            if (context == null)
            {
                context = new GMGColorContext();
                isLocalContext = true;
            }

            List<CustomModels.XMLEngine.FTPProtocol> protocols = new List<CustomModels.XMLEngine.FTPProtocol>();

            protocols = (from pr in context.FTPProtocols
                         select new CustomModels.XMLEngine.FTPProtocol
                         {
                             ID = pr.ID,
                             Name = pr.Name
                         }).ToList();

            if (isLocalContext)
            {
                context.Dispose();
            }

            return protocols;
        }
    }
}
