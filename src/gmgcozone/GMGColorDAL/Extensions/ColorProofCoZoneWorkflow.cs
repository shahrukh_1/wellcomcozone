using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ColorProofCoZoneWorkflow : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion
        
        #region Extension Methods

        public static bool CoZoneColorProofWorkflowExists(string workflowName, string instanceName, GMGColorContext context)
        {
            return (from cpczwf in context.ColorProofCoZoneWorkflows
                    join cpwf in context.ColorProofWorkflows on cpczwf.ColorProofWorkflow equals cpwf.ID
                    join cpins in context.ColorProofInstances on cpwf.ColorProofInstance equals cpins.ID
                    where cpins.SystemName.ToLower() == instanceName.ToLower() && cpczwf.Name.ToLower() == workflowName.ToLower() && cpczwf.IsDeleted == false
                    select cpczwf.ID).Any();
        }

        public static int GetCoZoneColorProofWorkflowByNameAndInstanceName(string workflowName, string instanceName, GMGColorContext context)
        {
            return (from cpczwf in context.ColorProofCoZoneWorkflows
                    join cpwf in context.ColorProofWorkflows on cpczwf.ColorProofWorkflow equals cpwf.ID
                    join cpins in context.ColorProofInstances on cpwf.ColorProofInstance equals cpins.ID
                    where cpins.SystemName.ToLower() == instanceName.ToLower() && cpczwf.Name.ToLower() == workflowName.ToLower() && cpczwf.IsDeleted == false
                    select cpczwf.ID).FirstOrDefault();
        }

        public static bool IsCoZoneWorkFlowInSecurityGroup(string workflowName, string instanceName, int uploaderUserId, GMGColorContext context)
        {
            IEnumerable<int> groupIdList = (from u in context.Users
                                            join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                            where u.ID == uploaderUserId
                                            select ugu.UserGroup);

            return      (from cpczwug in context.ColorProofCoZoneWorkflowUserGroups
                        join cpczw in context.ColorProofCoZoneWorkflows on cpczwug.ColorProofCoZoneWorkflow equals cpczw.ID
                        join cpw in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpw.ID
                        join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                        join cpis in context.ColorProofInstanceStates on cpi.State equals cpis.ID
                        where cpczw.IsAvailable == true && cpczw.Name.ToLower() == workflowName.ToLower() && cpi.SystemName.ToLower() == instanceName.ToLower() &&
                            groupIdList.Contains(cpczwug.UserGroup)
                        select cpi.ID
                    ).Any();
        }

        #endregion

        #region Properties

        #endregion


        #region Constructors

        #endregion
    }
}

