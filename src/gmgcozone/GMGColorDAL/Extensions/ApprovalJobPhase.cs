﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class ApprovalJobPhase : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static ApprovalJobPhase GetById(int phaseId, GMGColorContext context)
        {
            return (from ajp in context.ApprovalJobPhases where phaseId == ajp.ID select ajp).FirstOrDefault();
        }

        #endregion
    }
}
