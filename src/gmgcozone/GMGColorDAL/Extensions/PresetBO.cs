using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class Preset : BusinessObjectBase
    {
        #region Enums

        public enum PresetType
        {
            Low,
            Medium,
            High,
            Custom
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods

        public static PresetType GetPreset(int presetId, GMGColorContext context)
        {
            try
            {
                return Preset.GetPreset((from p in context.Presets where p.ID == presetId select p.Key).FirstOrDefault());
            }
            finally
            {
            }
        }

        public static PresetType GetPreset(Preset objPreset)
        {
            return Preset.GetPreset(objPreset.Key);
        }

        public static Preset GetPreset(PresetType preset, GMGColorContext context)
        {
            try
            {
                return (from p in context.Presets
                        where
                            (
                            (preset == PresetType.Low && p.Key == "L") || 
                            (preset == PresetType.Medium && p.Key == "M") ||
                            (preset == PresetType.High && p.Key == "H") ||
                            (preset == PresetType.Custom && p.Key == "C")
                            )
                        select p).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static PresetType GetPreset(string key)
        {
            switch (key)
            {
                case "L": return PresetType.Low;
                case "M": return PresetType.Medium;
                case "H": return PresetType.High;
                default: return PresetType.Custom;
            }
        }

        public static string GetLocalizedPresetName(int preset, GMGColorContext context)
        {
            try
            {
                string key = (from p in context.Presets where p.ID == preset select p.Key).FirstOrDefault();
                return GetLocalizedPresetName(Preset.GetPreset(key));
            }
            finally
            {
            }
        }

        public static string GetLocalizedPresetName(Preset objPreset)
        {
            return Preset.GetLocalizedPresetName(Preset.GetPreset(objPreset));
        }

        public static string GetLocalizedPresetName(PresetType preset)
        {
            switch (preset)
            {
                case PresetType.Low: return Resources.resPresetLow;
                case PresetType.Medium: return Resources.resPresetMedium;
                case PresetType.High: return Resources.resPresetHigh;
                case PresetType.Custom: return Resources.resPresetCustom;
                default: return null;
            }
        }

        public static Dictionary<int, string> GetLocalizedPresetNames(GMGColorContext context)
        {
            try
            {
                return (from p in context.Presets select new {p.ID, p.Key}).ToDictionary(o => o.ID,
                                                                                         o =>
                                                                                         Preset.GetLocalizedPresetName(
                                                                                             GetPreset(o.Key)));
            }
            finally
            {
            }
        }

        #endregion
    }
}

