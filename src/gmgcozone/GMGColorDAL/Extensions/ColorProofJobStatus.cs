using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ColorProofJobStatu : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods

        public static int GetDeliverJobStatusInColorProofID(GMG.CoZone.Common.ColorProofJobStatus status, GMGColorContext context)
        {
            return (from ds in context.ColorProofJobStatus
                    where ds.Key == (int)status
                    select ds.ID).FirstOrDefault();
        }

        #endregion
    }
}

