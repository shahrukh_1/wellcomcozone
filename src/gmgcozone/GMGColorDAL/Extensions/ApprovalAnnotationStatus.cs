using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class ApprovalAnnotationStatu : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static string GetLocalizedApprovalAnnotationStatus(string key)
        {
            switch (key)
            {

                case "D":
                    return Resources.lblAnnotationStatusNotDone;
                case "N":
                    return Resources.lblAnnotationStatusPending;
                case "A":
                default:
                    return Resources.lblAnnotationStatusCompleted;
            }
        }

        #endregion
    }
}

