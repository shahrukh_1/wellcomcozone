using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class ApprovalDecision : BusinessObjectBase
    {
        #region Enums

        [ObsoleteAttribute("This enum is obsolete. Call Decision from ApprovalEnums in collaborate module instead.", false)]
        public enum Decision
        {
            Pending,
            ChangesRequired,
            ApprovedwithChanges,
            Approved,
            NotApplicable
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static Decision GetApprovalDecision(int approvalDecisionId, GMGColorContext context)
        {
            try
            {
                return ApprovalDecision.GetApprovalDecision
                    ((from ad in context.ApprovalDecisions where ad.ID == approvalDecisionId select ad.Key).
                         FirstOrDefault());
            }
            finally
            {
            }
        }

        public static Decision GetApprovalDecision(ApprovalDecision objApprovalDecision)
        {
            return ApprovalDecision.GetApprovalDecision(objApprovalDecision.Key);
        }


        public static Decision GetApprovalDecision(string key)
        {
            switch (key)
            {
                case "PDG": return Decision.Pending;
                case "CHR": return Decision.ChangesRequired;
                case "AWC": return Decision.ApprovedwithChanges;
                case "APD": return Decision.Approved;
                case "NAP": return Decision.NotApplicable;
                default: return Decision.NotApplicable;
            }
        }

        public static string GetLocalizedApprovalDecisionName(ApprovalDecision objApprovalDecision)
        {
            return ApprovalDecision.GetLocalizedApprovalDecision(ApprovalDecision.GetApprovalDecision(objApprovalDecision));
        }

        public static string GetLocalizedApprovalDecision(Decision decision)
        {
            switch (decision)
            {
                case Decision.Pending: return Resources.lblPending;
                case Decision.ChangesRequired: return Resources.lblChangesRequired;
                case Decision.ApprovedwithChanges: return Resources.lblApprovedwithChanges;
                case Decision.Approved: return Resources.lblApproved;
                case Decision.NotApplicable: return Resources.lblNotApplicable;
                default: return string.Empty;
            }
        }

        public static string GetLocalizedApprovalDecision(string decisionKey)
        {
            Decision decision = GetApprovalDecision(decisionKey);
            switch (decision)
            {
                case Decision.Pending: return Resources.lblPending;
                case Decision.ChangesRequired: return Resources.lblChangesRequired;
                case Decision.ApprovedwithChanges: return Resources.lblApprovedwithChanges;
                case Decision.Approved: return Resources.lblApproved;
                case Decision.NotApplicable: return Resources.lblNotApplicable;
                default: return string.Empty;
            }
        }


        #endregion
    }
}

