﻿using System;
using System.Collections.Generic;

namespace GMGColorDAL.Extensions
{
    public class UserCollaborateReminder
    {
        public int ID { get; set; }
    }

    public class UserNotificationSchedule
    {
        public int Frequency { get; set; }
        public int? UnitNumber { get; set; }
        public int? Unit { get; set; }
        public int? Trigger { get; set; }

    }

    public class NotificationApproval
    {
        public int ID { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class UserCollaborateReminders
    {
        public int InternalUserRecipient { get; set; }
        public List<NotificationApproval> Approvals { get; set; }
        public List<UserNotificationSchedule> NotificationsSchedules { get; set; }
    }

    public class Compare : IEqualityComparer<NotificationApproval>
    {
        public bool Equals(NotificationApproval x, NotificationApproval y)
        {
            return x.ID == y.ID && x.Deadline == y.Deadline && x.CreatedDate == y.CreatedDate;
        }

        public int GetHashCode(NotificationApproval codeh)
        {
            return codeh.ID.GetHashCode();
        }
    }
}
