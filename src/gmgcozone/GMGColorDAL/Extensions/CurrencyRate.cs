using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class CurrencyRate : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods

        public static decimal GetCurrencyRate(int account, GMGColorContext context)
        {
            return
                (from cr in context.CurrencyRates
                    join a in context.Accounts on cr.Currency equals a.CurrencyFormat
                    select cr.Rate).FirstOrDefault();
        }

        #endregion
    }
}

