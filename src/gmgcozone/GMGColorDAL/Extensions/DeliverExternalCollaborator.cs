using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorDAL.CustomModels;

namespace GMGColorDAL
{
    public partial class DeliverExternalCollaborator : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
       
        public static DeliverExternalCollaborator GetDeliverExternalCollaboratorById(int userId, GMGColorContext context)
        {
            return (from ec in context.DeliverExternalCollaborators
                    where ec.ID == userId
                    select ec).FirstOrDefault();
        }

        public static string GetDeliverExternalCollaboratorGuid(int userId, GMGColorContext context)
        {
            return (from ec in context.DeliverExternalCollaborators
                    where ec.ID == userId
                    select ec.Guid).FirstOrDefault();
        }
       
        public static List<int> GetDeliverCollaboratorsByJobGuids(string[] jobGuids, string eventTypeKey, GMGColorContext context)
        {
            return (from dsj in context.DeliverSharedJobs
                    join dj in context.DeliverJobs on dsj.DeliverJob equals dj.ID
                    join np in context.AccountNotificationPresets on dsj.NotificationPreset equals np.ID
                    join aevt in context.AccountNotificationPresetEventTypes on np.ID equals aevt.NotificationPreset
                    join evt in context.EventTypes on aevt.EventType equals evt.ID
                    where jobGuids.Contains(dj.Guid) && evt.Key == eventTypeKey
                    select dsj.DeliverExternalCollaborator).Distinct().ToList();
        }

        #endregion
    }
}

