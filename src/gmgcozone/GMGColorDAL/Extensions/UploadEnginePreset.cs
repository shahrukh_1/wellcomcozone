﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.XMLEngine;

namespace GMGColorDAL
{
    public partial class UploadEnginePreset : BusinessObjectBase
    {
        #region Properties

        public bool CanBeDeleted { get; set; }

        #endregion

        #region Extension Methods

        public static List<UploadEnginePreset> PopulatePresetsGrid(int accId, GMGColorContext context)
        {
            try
            {
                List<UploadEnginePreset> presetsList = new List<UploadEnginePreset>();

                presetsList = (from pr in context.UploadEnginePresets
                               where pr.Account == accId
                               select pr).ToList();

                presetsList.ForEach(t => t.CanBeDeleted = t.UploadEngineProfiles.Count == 0);

                return presetsList;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in XML Engine PopulatePresetsGrid()", ex);
            }
        }

        public static UploadEnginePreset GetObjectById(int ID, GMGColorContext context)
        {
            try
            {

                return (from pr in context.UploadEnginePresets
                        where pr.ID == ID
                        select pr).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetViewModel() for AddEdit IO Preset", ex);
            }
        }

        /// <summary>
        /// Gets all presets from current account
        /// </summary>
        /// <param name="accId">Account Id</param>
        /// <param name="context">DB Context</param>
        /// <returns>List of Presets id and Name for the current account</returns>
        public static List<KeyValuePair<int, string>> GetPresetsList(int accId, GMGColorContext context)
        {
            try
            {
                List<KeyValuePair<int, string>> presets = new List<KeyValuePair<int, string>>();

                presets = (from pr in context.UploadEnginePresets
                           where pr.Account == accId
                           select pr).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();

                return presets;
            }
            catch (Exception ex)
            {
                throw new Exception("Error Occured in GetPresetsList() for AddEdit Upload Engine Profile", ex);
            }
        }

        #endregion
    }
}
