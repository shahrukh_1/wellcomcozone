using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class TimeFormat : BusinessObjectBase
    {
        #region Enums

        public enum Format
        {
            _12Hour,
            _24Hour
        }

        #endregion

        #region Prublic Constants
        public const string TimeFormat24 = "HH:mm";
        public const string TimeFormat12 = "hh:mm tt";
        #endregion

        #region Extension Methods

        public static Format GetTimeFormat(int id, GMGColorContext context)
        {
            try
            {
                string key = (from tf in context.TimeFormats where tf.ID == id select tf.Key).FirstOrDefault();
                return TimeFormat.GetTimeFormat(key);
            }
            finally
            {
            }
        }

        public static Format GetTimeFormat(TimeFormat objTimeFormat)
        {
            return TimeFormat.GetTimeFormat(objTimeFormat.Key);
        }

        public static Format GetTimeFormat(string key)
        {
            switch (key)
            {
                case "12HR": return Format._12Hour;
                default: return Format._24Hour;
            }
        }

        public static string GetLocalizedTimeFormat(Format format)
        {
            switch (format)
            {
                case Format._12Hour: return Resources.lbl12Hr;
                case Format._24Hour: return Resources.lbl24Hr;
                default: return string.Empty;
            }
        }

        public static string GetLocalizedTimeFormat(int timeFormat, GMGColorContext context)
        {
            return TimeFormat.GetLocalizedTimeFormat(TimeFormat.GetTimeFormat(timeFormat, context));
        }

        public static string GetLocalizedTimeFormat(TimeFormat objTimeFormat)
        {
            return TimeFormat.GetLocalizedTimeFormat(TimeFormat.GetTimeFormat(objTimeFormat));
        }

        #endregion
    }
}

