using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class SubscriberPlanInfo : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public List<SubscriberPlanInfo> GetSubscriberPlanPerAccount(int accountId, GMGColorContext context)
        {
            return (from o in context.SubscriberPlanInfoes
                    where o.Account == accountId
                    select o).ToList();
        }

        #endregion
    }
}

