using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ImageFormat : BusinessObjectBase
    {
        #region Enums

        public enum Format
        {
            /// <summary>
            /// Key = JPG
            /// </summary>
            JPEG,

            /// <summary>
            /// Key = COM
            /// </summary>
            Compression,

            /// <summary>
            /// Key = MIN
            /// </summary>
            JPEG_minimum,

            /// <summary>
            /// Key = LOW
            /// </summary>
            JPEG_low,

            /// <summary>
            /// Key = MED
            /// </summary>
            JPEG_medium,

            /// <summary>
            /// Key = HIG
            /// </summary>
            JPEG_high,

            /// <summary>
            /// Key = MAX
            /// </summary>
            JPEG_maximum,

            /// <summary>
            /// Key = PNG
            /// </summary>
            PNG,

            /// <summary>
            /// Key = TIF
            /// </summary>
            TIFF
        }

        public ImageFormat ParentImageFormat(GMGColorContext context)
        {
            return (from imgf in context.ImageFormats where imgf.ID == Parent select imgf).FirstOrDefault();
        }


    #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static Format GetImageFormat(string key)
        {
            switch (key)
            {
                case "JPG": return Format.JPEG;
                case "JPEG": return Format.JPEG;
                case "COM": return Format.Compression;
                case "MIN": return Format.JPEG_minimum;
                case "LOW": return Format.JPEG_low;
                case "MED": return Format.JPEG_medium;
                case "HIG": return Format.JPEG_high;
                case "MAX": return Format.JPEG_maximum;
                case "PNG": return Format.PNG;
                default: return Format.JPEG;
            }
        }

        public static Format GetImageFormat(int imageFormatId, GMGColorContext context)
        {
            return
                GetImageFormat(
                    (from imgFormat in context.ImageFormats where imgFormat.ID == imageFormatId select imgFormat.Key)
                        .FirstOrDefault());
        }

        public static ImageFormat GetImageFormat(Format format, GMGColorContext context)
        {
            return
                (from imgFormat in context.ImageFormats
                    where (
                            (format == Format.JPEG && imgFormat.Key == "JPG") ||
                            (format == Format.JPEG_minimum && imgFormat.Key == "MIN") ||
                            (format == Format.JPEG_low && imgFormat.Key == "LOW") ||
                            (format == Format.JPEG_medium && imgFormat.Key == "MED") ||
                            (format == Format.JPEG_high && imgFormat.Key == "HIG") ||
                            (format == Format.JPEG_maximum && imgFormat.Key == "MAX") ||
                            (format == Format.PNG && imgFormat.Key == "PNG")
                        )
                    select imgFormat).FirstOrDefault();
        }

        #endregion
    }
}

