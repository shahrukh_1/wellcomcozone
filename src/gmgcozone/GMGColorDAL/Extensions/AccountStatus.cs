using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;
using System.Data.SqlClient;

namespace GMGColorDAL
{
    public partial class AccountStatu : BusinessObjectBase
    {
        #region Enums

        public enum Status
        {
            Active = 1,
            Inactive,
            Draft,
            Suspended,
            Deleted
        };

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc


        /// <summary>
        /// Gets the account status.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static Status GetAccountStatus(int account, GMGColorContext context)
        {
            try
            {
                string query = @"SELECT 
                                [Extent2].[Key] AS [Key] 
                            FROM [dbo].[Account] AS [Extent1] 
                                INNER JOIN [dbo].[AccountStatus] AS [Extent2] ON  [Extent2].[ID] = [Extent1].[Status]
                            WHERE [Extent1].[ID] = @account";

                string objStatusKey = context.Database.SqlQuery<string>(query, new SqlParameter("account", account)).FirstOrDefault();
                return AccountStatu.GetAccountStatus(objStatusKey);
            }
            finally 
            {
            }
        }

        public static Status GetAccountStatus(AccountStatu status)
        {
            return AccountStatu.GetAccountStatus(status.Key);
        }

        public static Status GetAccountStatus(string key)
        {
            switch (key)
            {
                case "A": return Status.Active;
                case "I": return Status.Inactive;
                case "R": return Status.Draft;
                case "S": return Status.Suspended;
                case "D":
                default: return Status.Deleted;
            }
        }

        /// <summary>
        /// This method will returns the AccountStatusBO of the given account status
        /// </summary>
        /// <param name="status" type="enum"></param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static AccountStatu GetAccountStatus(Status status, GMGColorContext context)
        {
            try
            {
                return
                    (from accs in context.AccountStatus
                     where
                         ((status == Status.Active && accs.Key == "A") ||
                          (status == Status.Inactive && accs.Key == "I") ||
                          (status == Status.Draft && accs.Key == "R") ||
                          (status == Status.Suspended && accs.Key == "S") ||
                          (status == Status.Deleted && accs.Key == "D")
                         )
                     select accs).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static string GetLocalizedAccountStatus(int statusId, GMGColorContext context)
        {
            AccountStatu accountStatus = (from acs in context.AccountStatus where acs.ID == statusId select acs).FirstOrDefault();
            return AccountStatu.GetLocalizedAccountStatus(accountStatus);
        }

        public static string GetLocalizedAccountStatus(AccountStatu status)
        {
            return AccountStatu.GetLocalizedAccountStatus(AccountStatu.GetAccountStatus(status));
        }

        public static string GetLocalizedAccountStatus(Status status)
        {
            switch (status)
            {
                case Status.Active: return Resources.lblActive;
                case Status.Inactive: return Resources.lblInactive;
                case Status.Draft:return Resources.lblDraft;
                case Status.Suspended: return Resources.lblSuspended;
                case Status.Deleted: return Resources.lblDeleted;
                default: return string.Empty;
            }
        }

        #endregion
    }
}

