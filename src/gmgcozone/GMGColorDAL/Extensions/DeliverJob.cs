using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using DeliverJobStatus = GMG.CoZone.Common.DeliverJobStatus;

namespace GMGColorDAL
{
    public partial class DeliverJob : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

         public static string PageToString( PagesSelectionEnum pageRangeType, string pageRangeValue)
         {
             return pageRangeType != PagesSelectionEnum.Range
                        ? pageRangeType.ToString()
                        : string.Format("{0}:{1}", pageRangeType.ToString(), pageRangeValue);
         }

         public const string ConvertedPdfFileName = "Converted.pdf";
         public const int SmallThumbnailDimension = 650;

         public static string GetThumbnailImagePath(string deliverJobId, GMGColorContext context, int page = 1)
         {
             int deliverId = 0;
             if (Int32.TryParse(deliverJobId, out deliverId))
             {
                 return GetThumbnailImagePath(deliverId, context, page);
             }
             return string.Empty;
         }

        public static string GetThumbnailImagePath(int deliverJobId, GMGColorContext context, int page = 1 )
        {
            try
            {
                DeliverJob objDeliverJob = (from dj in context.DeliverJobs where dj.ID == deliverJobId select dj).FirstOrDefault();
                return ((objDeliverJob != null && objDeliverJob.ID > 0)
                            ? GetImagePath(objDeliverJob, GMGColorCommon.ImageType.Thumbnail, context, page)
                            : string.Empty);
            }
            finally
            {
            }
        }

        public static string GetThumbnailImagePath(string jobGuid, string accountDomain, string fileName, string region, int page = 1)
        {
            try
            {
                string relativeFolderPath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + jobGuid + "/";
                string absoluteFilePath = GMGColorCommon.GetDataFolderHttpPath(region, accountDomain);

                relativeFolderPath += page + "/Thumb-" + SmallThumbnailDimension + "px.jpg";
                absoluteFilePath += relativeFolderPath;

                return absoluteFilePath;
            }
            finally
            {
            }
        }

        private static string GetImagePath(DeliverJob objDeliverJob, GMGColorCommon.ImageType type, GMGColorContext context, int page = 1)
         {
             string relativeFolderPath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + objDeliverJob.Guid + "/";
             string absoluteFilePath = GMGColorCommon.GetDataFolderHttpPath(objDeliverJob.Job1.Account1.Region, Account.GetDomain(objDeliverJob.Job1.Account, context));

             string fileName = objDeliverJob.FileName;
             if (type != GMGColorCommon.ImageType.Original)
                 fileName = GMGColorCommon.ReplaceInvalidCharactors(Path.GetFileNameWithoutExtension(fileName)) + Path.GetExtension(fileName);

             bool fileNotFound = true;
             if (!String.IsNullOrEmpty(fileName))
             {
                 switch (type)
                 {
                     case GMGColorCommon.ImageType.Original:
                         {
                             relativeFolderPath += fileName;
                             if (GMGColorCommon.FileExists(relativeFolderPath, objDeliverJob.Job1.Account1.Region))
                             {
                                 absoluteFilePath += relativeFolderPath;
                                 fileNotFound = false;
                             }
                             break;
                         }
                     case GMGColorCommon.ImageType.Thumbnail:
                         {
                             if (GMGColorCommon.IsVideoFile(Path.GetExtension(objDeliverJob.FileName)))
                                 relativeFolderPath += page + "/Thumb-128px_0000.jpg";
                             else
                                 relativeFolderPath += page + "/Thumb-" + SmallThumbnailDimension + "px.jpg";
                             if (GMGColorCommon.FileExists(relativeFolderPath, objDeliverJob.Job1.Account1.Region))
                             {
                                 absoluteFilePath += relativeFolderPath;
                                 fileNotFound = false;
                             }
                             break;
                         }
                     default:
                         break;
                 }
             }

             if (fileNotFound)
             {
                 absoluteFilePath = GMGColorCommon.GetDefaultImagePath(type, Account.GetDomain(objDeliverJob.Job1.Account, context));
             }
             return absoluteFilePath;
         }

         public static List<ReturnDeliversPageView> GetDeliverJobsInGrid(int account, int user, int rowsPerPage, int pageNr, int searchField, bool orderDesc, string searchText, ref int totalRows, GMGColorContext context)
         {
             try
             {
                 System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof (int));
                 List<ReturnDeliversPageView> lst = context.GetDeliverJobs(account, user, rowsPerPage, pageNr, searchField, orderDesc, searchText, recCount).ToList();
                 totalRows = Convert.ToInt32(recCount.Value);
                 return lst;
             }
             finally
             {
             }
         }
        
         public static int GetAddedDeliversCount(int accountId, DateTime startingDate, GMGColorContext context)
         {
             // make sure you have the UTC time in starting time
             startingDate = startingDate.ToUniversalTime().Date;
             DateTime endingDate = startingDate.AddMonths(1).Date;

             int countDelivers = (from o in context.DeliverJobs
                                  join j in context.Jobs on o.Job equals j.ID
                                  where
                                      j.Account == accountId &&
                                      (o.CreatedDate >= startingDate) &&
                                      (o.CreatedDate <= endingDate)
                                  select o.ID)
                                  .Concat(from d in context.DeliverJobDeleteHistories
                                          where d.Account == accountId &&
                                          (d.CreatedDate >= startingDate) &&
                                          (d.CreatedDate <= endingDate)
                                          select d.ID).Count();
             return countDelivers;
         }

        public static DeliverJob GetFullJobItem(int deliverJobId, int loggedAccountId, GMGColorContext context)
        {
            try
            {
                return (from dj in context.DeliverJobs
                        join j in context.Jobs on dj.Job equals j.ID
                        where j.Account == loggedAccountId && dj.ID == deliverJobId
                        select dj).FirstOrDefault();
            }
            finally
            {
            }
        }

        public string JobDetailsLink
        {
            get
            {
                return string.Format("{0}{1}/Deliver/JobDetailLink?selectedinstance={2}",
                                     GMGColorConfiguration.AppConfiguration.ServerProtocol,
                                     this.Job1.Account1.IsCustomDomainActive
                                         ? Job1.Account1.CustomDomain
                                         : Job1.Account1.Domain, ID);
            }
        }

        public static List<DeliverJobStatusFromCP> GetJobStatuses(int[] jobIdList, List<KeyValuePair<int, string>> translatedStatuses, GMGColorContext context)
        {
            return (from dj in context.DeliverJobs
                    join djs in context.DeliverJobStatus on dj.DeliverJobStatu.ID equals djs.ID
                    where jobIdList.Contains(dj.ID)
                    select new 
                    {
                        Id = dj.ID,
                        Status = (DeliverJobStatus)djs.Key,
                        IsCancelRequested = dj.IsCancelRequested
                    }).ToList().Select(t => new DeliverJobStatusFromCP
                    { Id = t.Id, Status = t.Status, IsCancelRequested = t.IsCancelRequested, TranslatedStatus = translatedStatuses.FirstOrDefault(o => o.Key == (int)t.Status).Value }).ToList();
        }

        public static DeliverJob GetJobByIdAndAccountId(int jobId, int accountId, GMGColorContext context)
        {
            return context.DeliverJobs.FirstOrDefault(o => o.ID == jobId && o.Job1.Account == accountId);
        }

         #endregion
    }

    public class DeliverJobStatusFromCP
    {
        public DeliverJobStatus Status;
        public string TranslatedStatus;
        public int Id;
        public bool IsCancelRequested;

        public bool ShowJobDetailsMenu
        {
            get { return true; }
        }

        public bool ShowJobCancelMenu
        {
            get
            {
                return Status != DeliverJobStatus.Cancelled && Status != DeliverJobStatus.Completed &&
                       Status != DeliverJobStatus.CompletedDeleted && Status != DeliverJobStatus.Submitting &&
                       Status != DeliverJobStatus.ErrorDeleted && IsCancelRequested == false;
            }
        }

        public bool ShowJobCancelRequestedMenu
        {
            get
            {
                return Status != DeliverJobStatus.Cancelled && Status != DeliverJobStatus.Completed &&
                       Status != DeliverJobStatus.CompletedDeleted && Status != DeliverJobStatus.Submitting &&
                       Status != DeliverJobStatus.ErrorDeleted && IsCancelRequested == true;
            }
        }
    }
}

