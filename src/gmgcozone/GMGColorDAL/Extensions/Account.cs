using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using GMG.CoZone.Common;
using GMGColor.AWS;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using RSC = GMGColor.Resources;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace GMGColorDAL
{
    public class Account_Validation
    {
        [Required(ErrorMessageResourceName = "reqAccountType", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int AccountType { get; set; }

        [Required(ErrorMessageResourceName = "reqDomain", ErrorMessageResourceType = typeof(RSC.Resources))]
        [RegularExpression(GMG.CoZone.Common.Constants.DomainNameRegex, ErrorMessageResourceName = "reqInvalidDomain", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(255, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Domain { get; set; }

        [RegularExpression(GMG.CoZone.Common.Constants.DomainNameRegex, ErrorMessageResourceName = "reqInvalidDomain", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string CustomDomain { get; set; }

        [Required(ErrorMessageResourceName = "reqAccountName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "reqDefaultLanguage", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int Locale { get; set; }

        [Required(ErrorMessageResourceName = "reqDateFormat", ErrorMessageResourceType = typeof(RSC.Resources))]
        public DateTime DateFormat { get; set; }

        [Required(ErrorMessageResourceName = "reqTimeZone", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string TimeZone { get; set; }

        [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string CustomFromAddress { get; set; }

        [Required(ErrorMessageResourceName = "reqSiteName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string SiteName { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string CustomerNumber { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string DealerNumber { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string VATNumber { get; set; }
    }

    [MetadataType(typeof(Account_Validation))]
    [Serializable]
    public partial class Account: BusinessObjectBase
    {
        #region Private Methods

        private AccountSubscriptionPlan GetSubscriptionPlan(GMG.CoZone.Common.AppModule appModuleKey, GMGColorContext context)
        {
            return (from asp in context.AccountSubscriptionPlans
                    join a in context.Accounts on asp.Account equals a.ID
                    join ac in context.AccountStatus on a.Status equals ac.ID
                    join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && a.ID == ID && (!bp.IsDemo || (bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(asp.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) || ac.Key != "A"))
                    select asp).FirstOrDefault();
        }

        private SubscriberPlanInfo GetSubscriberPlan(GMG.CoZone.Common.AppModule appModuleKey, GMGColorContext context)
        {
            return (from spi in context.SubscriberPlanInfoes
                    join a in context.Accounts on spi.Account equals a.ID
                    join ac in context.AccountStatus on a.Status equals ac.ID
                    join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && a.ID == ID && (!bp.IsDemo || (bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(spi.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) || ac.Key != "A"))
                    select spi).FirstOrDefault();
        }

        private BillingPlan GetBillingPlan(GMG.CoZone.Common.AppModule appModuleKey, GMGColorContext context)
        {
            return (from asp in context.AccountSubscriptionPlans
                    join a in context.Accounts on asp.Account equals a.ID
                    join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where a.ID == ID && am.Key == (int)appModuleKey
                    select bp).FirstOrDefault()
                   ??
                   (from spi in context.SubscriberPlanInfoes
                    join a in context.Accounts on spi.Account equals a.ID
                    join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where a.ID == ID && am.Key == (int)appModuleKey
                    select bp).FirstOrDefault();
        }

        private bool HasSubscriptionPlan(GMG.CoZone.Common.AppModule appModuleKey, GMGColorContext context)
        {
            return (from asp in context.AccountSubscriptionPlans
                    join a in context.Accounts on asp.Account equals a.ID
                    join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && a.ID == ID && (!bp.IsDemo || (bp.IsDemo && DbFunctions.DiffDays(DbFunctions.TruncateTime(asp.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS))
                    select asp.ID).Any() 
                    ||
                    (from spi in context.SubscriberPlanInfoes
                    join a in context.Accounts on spi.Account equals a.ID
                    join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                    join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                    join am in context.AppModules on bpt.AppModule equals am.ID
                     where am.Key == (int)appModuleKey && a.ID == ID && (!bp.IsDemo || (bp.IsDemo && DbFunctions.DiffDays(DbFunctions.TruncateTime(spi.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS))
                    select spi.ID).Any();
        }

        #endregion

        #region Properties

        public AccountSubscriptionPlan CollaborateSubscriptionPlan(GMGColorContext context)
        {
            return GetSubscriptionPlan(GMG.CoZone.Common.AppModule.Collaborate, context);
        }

        public AccountSubscriptionPlan DeliverSubscriptionPlan(GMGColorContext context)
        {
            return GetSubscriptionPlan(GMG.CoZone.Common.AppModule.Deliver, context);
        }
       
        public SubscriberPlanInfo CollaborateSubscriberPlan(GMGColorContext context)
        {
            return GetSubscriberPlan(GMG.CoZone.Common.AppModule.Collaborate, context);
        }

        public SubscriberPlanInfo DeliverSubscriberPlan (GMGColorContext context)
        {
            return GetSubscriberPlan(GMG.CoZone.Common.AppModule.Deliver, context);
        }

        public BillingPlan CollaborateBillingPlan (GMGColorContext context)
        {
            return GetBillingPlan(GMG.CoZone.Common.AppModule.Collaborate, context);
        }
        
        public BillingPlan DeliverBillingPlan (GMGColorContext context)
        {
            return GetBillingPlan(GMG.CoZone.Common.AppModule.Deliver, context);
        }       

        public bool HasCollaborateSubscriptionPlan (GMGColorContext context)
        {
            return HasSubscriptionPlan(GMG.CoZone.Common.AppModule.Collaborate, context);
        }

        public bool HasDeliverSubscriptionPlan (GMGColorContext context)
        {
            return HasSubscriptionPlan(GMG.CoZone.Common.AppModule.Deliver, context);
        }		       

        public BillingPlan GetBillingPlanByAppModule(GMG.CoZone.Common.AppModule appModule, GMGColorContext context)
        {
            return GetBillingPlan(appModule, context);
        }

        #endregion

        #region Enums

        public enum LogoType
        {
            /// <summary>
            /// Account email logo
            /// </summary>
            EmailLogo,
            /// <summary>
            /// Account header logo
            /// </summary>
            HeaderLogo,
            /// <summary>
            /// Account login logo
            /// </summary>
            LoginLogo,
            /// <summary>
            /// Favorite Icon logo
            /// </summary>
            FavIconLogo
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Gets the default logo path.
        /// </summary>
        /// <param name="logoType">Type of the logo.</param>
        /// <param name="objAccount">The obj account.</param>
        /// <returns></returns>
        public static string GetDefaultLogoPath(LogoType logoType, Account objAccount)
        {
            string accountDomain = objAccount.IsCustomDomainActive ? objAccount.CustomDomain : objAccount.Domain;
            string absoluteFileURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/content/img/";

            var activeTheme = objAccount.AccountThemes.Where(th => th.Active == true).FirstOrDefault();
            switch (logoType)
            {
                case LogoType.EmailLogo:
                    {
                        switch (GMGColorDAL.Theme.GetColorScheme(activeTheme.Key))
                        {
                            default:
                            case GMGColorDAL.Theme.ColorScheme.Default: absoluteFileURL += "nologo-160px-60px.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Splash: absoluteFileURL += "nologo-160px-60px-pink.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Juicy: absoluteFileURL += "nologo-160px-60px-green.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Fresh: absoluteFileURL += "nologo-160px-60px-blue.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Moody: absoluteFileURL += "nologo-160px-60px-white.png";
                                break;
                        }
                        break;
                    }
                case LogoType.HeaderLogo:
                    {
                        switch (GMGColorDAL.Theme.GetColorScheme(activeTheme.Key))
                        {
                            default:
                            case GMGColorDAL.Theme.ColorScheme.Default: absoluteFileURL += "nologo-122px-33px.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Splash: absoluteFileURL += "nologo-122px-33px-pink.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Juicy: absoluteFileURL += "nologo-122px-33px-green.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Fresh: absoluteFileURL += "nologo-122px-33px-blue.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Moody: absoluteFileURL += "nologo-122px-33px-white.png";
                                break;
                        }
                        break;
                    }
                case LogoType.LoginLogo:
                    {
                        switch (GMGColorDAL.Theme.GetColorScheme(activeTheme.Key))
                        {
                            default:
                            case GMGColorDAL.Theme.ColorScheme.Default: absoluteFileURL += "nologo-200px-120px.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Splash: absoluteFileURL += "nologo-200px-120px-pink.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Juicy: absoluteFileURL += "nologo-200px-120px-green.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Fresh: absoluteFileURL += "nologo-200px-120px-blue.png";
                                break;
                            case GMGColorDAL.Theme.ColorScheme.Moody: absoluteFileURL += "nologo-200px-120px-white.png";
                                break;
                        }
                        break;
                    }
                case LogoType.FavIconLogo:
                {
                    absoluteFileURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/favicon.ico";
                    break;
                }
            }
            return absoluteFileURL.ToLower();
        }

        /// <summary>
        /// Returns logo path of given logo type.
        /// Ex-: Login logo, Header logo, Email logo
        /// </summary>
        /// <param name="accountId">Account ID</param>
        /// <param name="logoType">Expected logo type(EmailLogo, HeaderLogo, LoginLogo)</param>
        /// <param name="defaultImageIfNotExist">If 'true', returns default logo.If 'false', returns String.Empty</param>
        /// <param name="recursive">If not exists, get parent's logo</param>
        /// <returns></returns>
        public static string GetLogoPath(int accountId, LogoType logoType, GMGColorContext context, bool defaultImageIfNotExist = true, bool recursive = true)
        {
            Account objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();
            return Account.GetLogoPath(objAccount, logoType, defaultImageIfNotExist, recursive);
        }

        /// <summary>
        /// Returns logo path of given logo type.
        /// Ex-: Login logo, Header logo, Email logo
        /// </summary>
        /// <param name="objAccount">Account object</param>
        /// <param name="logoType">Expected logo type(EmailLogo, HeaderLogo, LoginLogo)</param>
        /// <param name="context">The context.</param>
        /// <param name="defaultImageIfNotExist">If 'true', returns default logo.If 'false', returns String.Empty</param>
        /// <param name="recursive">If not exists, get parent's logo</param>
        /// <returns></returns>
        public static string GetLogoPath(Account objAccount, LogoType logoType, bool defaultImageIfNotExist = true, bool recursive = true)
        {
            return Account.GetImagePath(objAccount, logoType, defaultImageIfNotExist, recursive);
        }

        /// <summary>
        /// Gets the image path.
        /// </summary>
        /// <param name="objAccount">The obj account.</param>
        /// <param name="logoType">Type of the logo.</param>
        /// <param name="defaultImageIfNotExist">if set to <c>true</c> [default image if not exist].</param>
        /// <param name="recursive">if set to <c>true</c> [recursive].</param>
        /// <param name="colorScheme">The color scheme.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        private static string GetImagePath(Account objAccount, LogoType logoType, bool defaultImageIfNotExist, bool recursive)
        {
            string logoPath = string.Empty;
            switch (logoType)
            {
                case LogoType.EmailLogo:
                    logoPath = objAccount.EmailLogoPath;
                    break;
                case LogoType.HeaderLogo:
                    logoPath = objAccount.HeaderLogoPath;
                    break;
                case LogoType.LoginLogo:
                    logoPath = objAccount.LoginLogoPath;
                    break;
                case LogoType.FavIconLogo:
                    logoPath = objAccount.FavIconPath;
                    break;
                default:
                    break;
            }

            if (string.IsNullOrEmpty(logoPath) && objAccount.Parent.GetValueOrDefault() > 0 && recursive)
            {
                Account parentAccount = objAccount.ParentAccount;
                return Account.GetImagePath(parentAccount, logoType, defaultImageIfNotExist, true);
            }
            else
            {
                return Account.GetImagePath(objAccount, logoType, defaultImageIfNotExist);
            }
        }

        /// <summary>
        /// Gets the image path.
        /// </summary>
        /// <param name="objAccount">The obj account.</param>
        /// <param name="logoType">Type of the logo.</param>
        /// <param name="defaultImageIfNotExist">if set to <c>true</c> [default image if not exist].</param>
        /// <param name="colorScheme">The color scheme.</param>
        /// <returns></returns>
        private static string GetImagePath(Account objAccount, LogoType logoType, bool defaultImageIfNotExist)
        {
            string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(objAccount.Region);
            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(objAccount.Region);
            string accountDomain = objAccount.IsCustomDomainActive ? objAccount.CustomDomain : objAccount.Domain;

            string absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + dataFolderName + "/";
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                absoluteFileUrl = pathtoDataFolder;
            }

            bool fileNotFound = true;
            switch (logoType)
            {
                case LogoType.EmailLogo:
                    {
                        if (!String.IsNullOrEmpty(objAccount.EmailLogoPath))
                        {
                            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + objAccount.EmailLogoPath;
                            // Don't merge the two condions together.
                            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                if (AWSSimpleStorageService.FileExistsInBucket(dataFolderName, relativeFileURL, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                            else
                            {
                                string absoluteFilePath = pathtoDataFolder + "accounts\\" + objAccount.Guid + "\\" + objAccount.EmailLogoPath;
                                if (System.IO.File.Exists(absoluteFilePath))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                        }

                        if (fileNotFound)
                        {
                            if (defaultImageIfNotExist)
                            {
                                absoluteFileUrl = Account.GetDefaultLogoPath(logoType, objAccount);
                            }
                            else
                            {
                                absoluteFileUrl = string.Empty;
                            }
                        }
                        break;
                    }
                case LogoType.HeaderLogo:
                    {
                        if (!String.IsNullOrEmpty(objAccount.HeaderLogoPath))
                        {
                            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + objAccount.HeaderLogoPath;
                            // Don't merge the two condions together.
                            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                if (AWSSimpleStorageService.FileExistsInBucket(dataFolderName, relativeFileURL, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                            else
                            {
                                string absoluteFilePath = pathtoDataFolder + "accounts\\" + objAccount.Guid + "\\" + objAccount.HeaderLogoPath;
                                if (System.IO.File.Exists(absoluteFilePath))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                        }

                        if (fileNotFound)
                        {
                            if (defaultImageIfNotExist)
                            {
                                absoluteFileUrl = Account.GetDefaultLogoPath(logoType, objAccount);
                            }
                            else
                            {
                                absoluteFileUrl = string.Empty;
                            }
                        }
                        break;
                    }
                case LogoType.LoginLogo:
                    {
                        if (!String.IsNullOrEmpty(objAccount.LoginLogoPath))
                        {
                            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + objAccount.LoginLogoPath;
                            // Don't merge the two condions together.
                            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                if (AWSSimpleStorageService.FileExistsInBucket(dataFolderName, relativeFileURL, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                            else
                            {
                                string absoluteFilePath = pathtoDataFolder + "accounts\\" + objAccount.Guid + "\\" + objAccount.LoginLogoPath;
                                if (System.IO.File.Exists(absoluteFilePath))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                        }

                        if (fileNotFound)
                        {
                            if (defaultImageIfNotExist)
                            {
                                absoluteFileUrl = Account.GetDefaultLogoPath(logoType, objAccount);
                            }
                            else
                            {
                                absoluteFileUrl = string.Empty;
                            }
                        }
                        break;
                    }
                case LogoType.FavIconLogo:
                    {
                        if (!String.IsNullOrEmpty(objAccount.FavIconPath))
                        {
                            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + objAccount.FavIconPath;
                            // Don't merge the two condions together.
                            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                if (AWSSimpleStorageService.FileExistsInBucket(dataFolderName, relativeFileURL, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                            else
                            {
                                string absoluteFilePath = pathtoDataFolder + "accounts\\" + objAccount.Guid + "\\" + objAccount.FavIconPath;
                                if (System.IO.File.Exists(absoluteFilePath))
                                {
                                    absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();
                                    fileNotFound = false;
                                }
                            }
                        }

                        if (fileNotFound)
                        {
                            absoluteFileUrl = defaultImageIfNotExist ? Account.GetDefaultLogoPath(logoType, objAccount) : string.Empty;
                        }
                        break;
                    }
                default:
                    break;
            }
            return absoluteFileUrl.ToLower();
        }

        /// <summary>
        /// Gets the domain.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetDomain(int accountId, GMGColorContext context)
        {
            var account =
                (from a in context.Accounts
                 where a.ID == accountId
                 select new {a.IsCustomDomainActive, a.CustomDomain, a.Domain}).FirstOrDefault();

            return account == null
                       ? string.Empty
                       : (account.IsCustomDomainActive ? account.CustomDomain : account.Domain);
        }

        /// <summary>
        /// Gets the account accessible account types.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<AccountType> GetAccountAccessibleAccountTypes(int accountId, GMGColorContext context)
        {
            List<GMGColorDAL.AccountType.Type> lstAccountTypes = null;
            switch (GMGColorDAL.AccountType.GetAccountType(accountId, context))
            {
                case GMGColorDAL.AccountType.Type.GMGColor:
                    lstAccountTypes =
                        new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Subsidiary,
                                GMGColorDAL.AccountType.Type.Dealer,
                                GMGColorDAL.AccountType.Type.Client,
                                GMGColorDAL.AccountType.Type.Subscriber
                            }.ToList();
                    break;
                case GMGColorDAL.AccountType.Type.Subsidiary:
                    lstAccountTypes =
                        new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Dealer,
                                GMGColorDAL.AccountType.Type.Client,
                                GMGColorDAL.AccountType.Type.Subscriber
                            }.ToList();
                    break;
                case GMGColorDAL.AccountType.Type.Dealer:
                    lstAccountTypes =
                        new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Client,
                                GMGColorDAL.AccountType.Type.Subscriber
                            }.ToList();
                    break;
                case GMGColorDAL.AccountType.Type.Client:
                    lstAccountTypes =
                        new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Subscriber
                            }.ToList();
                    break;
                case GMGColorDAL.AccountType.Type.Subscriber:
                    default:
                    lstAccountTypes =
                        new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Subscriber
                            }.ToList();
                    break;
            }
            
            {
                List<int> accountTypeListId = lstAccountTypes.Select(o => GMGColorDAL.AccountType.GetAccountType(o, context).ID).ToList();
                return (from at in context.AccountTypes where accountTypeListId.Contains(at.ID) select at).ToList();
            }
        }

        /// <summary>
        /// Determines whether [is account accessible account type] [the specified account].
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="accountType">Type of the account.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is account accessible account type] [the specified account]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAccountAccessibleAccountType(int account, int accountType, GMGColorContext context)
        {
            List<AccountType> lstAccountTypes = Account.GetAccountAccessibleAccountTypes(account, context);

            if (lstAccountTypes.Count > 0)
            {
                return lstAccountTypes.Select(o => o.ID).Contains(accountType);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Constructor for new account creation
        /// this constructor will fill data for NewAccountBillingPlans
        /// </summary>
        /// <param name="objAccount"></param>
        public static AccountBillingPlans GetAccountBillingPlanPriceInfo(Account objAccount, GMGColorContext context)
        {
            try
            {
                AccountType.Type accountType = GMGColorDAL.AccountType.GetAccountType(objAccount, context);
                AccountType.Type parentAccountType = GMGColorDAL.AccountType.GetAccountType(objAccount.Parent.GetValueOrDefault(), context);

                AccountBillingPlans accountBillingPlanPrice = new AccountBillingPlans
                                                                  {
                                                                      NewPrice = 0,
                                                                      NewProof = 0,
                                                                      SelectedPrice = 0,
                                                                      SelectedProof = 0
                                                                  };

                decimal rate = 1;
                if (objAccount.Parent.GetValueOrDefault() > 0)
                {
                    Account objParent = (from a in context.Accounts where a.ID == objAccount.Parent select a).FirstOrDefault();
                    if (objParent != null && (parentAccountType != GMGColorDAL.AccountType.Type.GMGColor) && (objAccount.CurrencyFormat != objParent.CurrencyFormat))
                    {
                        rate = (new GMGColorCommon()).GetUpdatedCurrencyRate(objAccount, context);
                    }
                }

                accountBillingPlanPrice.BillingPlans = new List<BillingPlanTypePlans>();
                List<int> lstAttachedBillingPlanIds = accountType ==
                                                      GMGColorDAL.AccountType.Type.GMGColor
                                                          ? (from bp in context.BillingPlans select bp.ID).ToList()
                                                          : objAccount.AttachedAccountBillingPlans.Select(o => o.BillingPlan)
                                                                                                   .Union(
                                                                                                        (from bp in context.BillingPlans
                                                                                                         where bp.Account == objAccount.ID 
                                                                                                         select bp.ID)).ToList();

                List<BillingPlanType> billingPlanTypes = context.BillingPlanTypes.ToList();
                foreach (BillingPlanType planType in billingPlanTypes)
                {
                    List<BillingPlan> lstBillingPlans = planType.BillingPlans.Where(o => lstAttachedBillingPlanIds.Contains(o.ID)).ToList();

                    if (lstBillingPlans.Count > 0)
                    {
                        BillingPlanTypePlans objDisplayBillingPlanType = new BillingPlanTypePlans();

                        foreach (BillingPlan billingPlan in lstBillingPlans)
                        {
                            BillingPlanInfo objBillingPlan = BillingPlan.FillBillingPlanData(billingPlan, objAccount, true, false, rate, accountType, parentAccountType, context);

                            objDisplayBillingPlanType.BillingPlans.Add(objBillingPlan);
                        }

                        objDisplayBillingPlanType.BillingPlanTypeID = planType.ID;
                        objDisplayBillingPlanType.BillingPlanTypeName = planType.Name;
                        objDisplayBillingPlanType.BillingPlanTypeAppModuleID = planType.AppModule;
                        objDisplayBillingPlanType.BillingPlanTypeAppModuleKey = planType.AppModule1.Key;
                        accountBillingPlanPrice.BillingPlans.Add(objDisplayBillingPlanType);
                    }
                }
                accountBillingPlanPrice.BillingPlans.ForEach(o => o.BillingPlans.Sort((a, b) => (System.String.Compare(a.Name, b.Name, System.StringComparison.OrdinalIgnoreCase))));

                return accountBillingPlanPrice;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Consrtuctor for EditAccount and ViewAccountPlans
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="isEditAccountPlans">if set to <c>true</c> [is edit account plans].</param>
        /// <param name="context">The db context.</param>
        /// <returns></returns>
        public static AccountBillingPlans GetAccountBillingPlanPriceInfo(Account account, bool isEditAccountPlans, GMGColorContext context)
        {
            try
            {
                AccountType.Type accountType = GMGColorDAL.AccountType.GetAccountType(account, context);
                AccountType.Type parentAccountType = GMGColorDAL.AccountType.GetAccountType(account.Parent.GetValueOrDefault(), context);
                Account objParent = null;
                AccountBillingPlans accountBillingPlanPrice = new AccountBillingPlans
                                                                  {
                                                                      NewPrice = 0,
                                                                      NewProof = 0,
                                                                      SelectedPrice = 0,
                                                                      SelectedProof = 0
                                                                  };

                decimal rate = 1;
                if (accountType != GMGColorDAL.AccountType.Type.GMGColor)
                {
                    objParent = (from a in context.Accounts where a.ID == account.Parent select a).FirstOrDefault();
                    if (objParent != null && parentAccountType != GMGColorDAL.AccountType.Type.GMGColor && (account.CurrencyFormat != objParent.CurrencyFormat))
                    {
                        rate = (new GMGColorCommon()).GetUpdatedCurrencyRate(account, context);
                    }
                }

                accountBillingPlanPrice.BillingPlans = new List<BillingPlanTypePlans>();
                List<int> lstAttachedBillingPlanIds;

                if (isEditAccountPlans && parentAccountType == GMGColorDAL.AccountType.Type.GMGColor)
                {
                    lstAttachedBillingPlanIds = context.BillingPlans.Where(t => t.Account == null).Select(o => o.ID).ToList();
                }
                else
                {
                    if (objParent != null && isEditAccountPlans)
                    {
                        lstAttachedBillingPlanIds = objParent.AttachedAccountBillingPlans
                                                             .Select(o => o.BillingPlan)
                                                             .Union(from bp in context.BillingPlans
                                                                    where bp.Account == objParent.ID
                                                                    select bp.ID).ToList();
                    }
                    else
                    {
                        lstAttachedBillingPlanIds = account.AttachedAccountBillingPlans.Select(o => o.BillingPlan).ToList();
                    }
                }

                foreach (BillingPlanType planType in context.BillingPlanTypes.ToList())
                {
                    List<BillingPlan> lstBillingPlans = planType.BillingPlans.Where(o => lstAttachedBillingPlanIds.Contains(o.ID)).ToList();

                    if (lstBillingPlans.Count > 0)
                    {
                        var objDisplayBillingPlanType = new BillingPlanTypePlans();

                        foreach (BillingPlan billingPlan in lstBillingPlans)
                        {
                            BillingPlanInfo objBillingPlan = BillingPlan.FillBillingPlanData(billingPlan, account, false, isEditAccountPlans, rate, accountType, parentAccountType, context);

                            objDisplayBillingPlanType.BillingPlans.Add(objBillingPlan);
                        }

                        objDisplayBillingPlanType.BillingPlanTypeID = planType.ID;
                        objDisplayBillingPlanType.BillingPlanTypeName = planType.Name;
                        objDisplayBillingPlanType.BillingPlanTypeAppModuleID = planType.AppModule;
                        objDisplayBillingPlanType.BillingPlanTypeAppModuleKey = planType.AppModule1.Key;
                        accountBillingPlanPrice.BillingPlans.Add(objDisplayBillingPlanType);
                    }
                }
                accountBillingPlanPrice.BillingPlans.ForEach(o => o.BillingPlans.Sort((a, b) => (System.String.Compare(a.Name, b.Name, System.StringComparison.OrdinalIgnoreCase))));

                return accountBillingPlanPrice;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the account customise info.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="folderPath">The folder path.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public AccountCustomiseInfo GetAccountCustomiseInfo(Account account, string folderPath, GMGColorContext context)
        {
            try
            {
                AccountCustomiseInfo accountCustomiseInfo = new AccountCustomiseInfo
                                                                {
                                                                    PathToTempFolder = folderPath,
                                                                    PrePressFunctionsWhereThisIsAccount = new List<AcctPrePressFunction>()
                                                                };

                return accountCustomiseInfo;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the style.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns></returns>
        public static string GetStyle(object session)
        {
            string style = string.Empty;

            if (session != null)
            {
                bool isCompleted = Convert.ToBoolean(session.ToString());
                style = isCompleted ? " completed-step" : string.Empty;
            }

            return style;
        }

        /// <summary>
        /// Gets the action.
        /// </summary>
        /// <param name="step">The step.</param>
        /// <param name="session">The session.</param>
        /// <param name="accountId">The account id.</param>
        /// <returns></returns>
        public static string GetAction(int step, object session, int accountId)
        {
            string action = "#";

            if (session != null)
            {
                switch (step)
                {
                    case 1:
                        {
                            bool isCompleted = Convert.ToBoolean(session);
                            action = isCompleted ? "AccountInfo?id=" + accountId : "#";
                            break;
                        }
                    case 2:
                        {
                            bool isCompleted = Convert.ToBoolean(session);
                            action = isCompleted ? "AccountPlans?id=" + accountId : "#";
                            break;
                        }
                    case 3:
                        {
                            bool isCompleted = Convert.ToBoolean(session);
                            action = isCompleted ? "AccountCustomise?id=" + accountId : "#";
                            break;
                        }
                    case 4:
                        {
                            bool isCompleted = Convert.ToBoolean(session);
                            action = isCompleted ? "AccountConfirm?id=" + accountId : "#";
                            break;
                        }
                    default:
                        break;
                }
            }

            return action;
        }

        /// <summary>
        /// Determines whether [is account in A free plan] [the specified account].
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="appModule">The app module.</param>
        /// <param name="context">The context.</param>        
        /// <returns>
        ///   <c>true</c> if [is account in A free plan] [the specified account]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAccountInAFreePlan(Account account, GMG.CoZone.Common.AppModule appModule, GMGColorContext context)
        {
            BillingPlan billingPlan;
            switch (appModule)
            {
                case GMG.CoZone.Common.AppModule.Collaborate :
                    {
                        billingPlan = account.CollaborateBillingPlan(context);
                        break;
                    }

                case GMG.CoZone.Common.AppModule.Deliver:
                    {
                        billingPlan = account.DeliverBillingPlan(context);
                        break;
                    }
                default:
                    {
                        billingPlan = null;
                        break;
                    }
            }
            if (billingPlan == null) return false;

            return BillingPlan.IsAccountInAFreePlan(billingPlan.Price);
        }

        /// <summary>
        /// Gets the HQ account URL.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetHQAccountUrl(GMGColorContext context)
        {
            try
            {
                int accountType = GMGColorDAL.AccountType.GetAccountType(GMGColorDAL.AccountType.Type.GMGColor, context).ID;
                return (from a in context.Accounts where a.AccountType == accountType select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Returns added jobs count (used proofs) for a specific period of time (yearly or monthly)
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <param name="appModule">Application module for which added jobs count will be retrieved</param>
        /// <param name="accountCreatedDate">Account created date</param>
        /// <param name="context">Database context</param>
        /// <param name="isMonthlyBillingFrequency"></param>       
        /// <param name="contractStartDate"></param>
        /// <returns></returns>
        public static int GetUsedProofsCount(int accountId, GMG.CoZone.Common.AppModule appModule, DateTime accountCreatedDate,
                                         GMGColorContext context, DateTime? contractStartDate, bool isMonthlyBillingFrequency = true)
       {
            int billingAnniversaryDay = contractStartDate.GetValueOrDefault().Day;
            return (isMonthlyBillingFrequency 
                    ? GetMonthlyJobsCount(billingAnniversaryDay, accountId, accountCreatedDate, context, appModule) 
                    : GetYearlyJobsCount(billingAnniversaryDay, accountId, contractStartDate, accountCreatedDate, context, appModule));
           
       }

       /// <summary>
       /// Returns added jobs count for specific month
       /// </summary>
       /// <param name="billingAnniversaryDay">Biling plan contract start date</param>
       /// <param name="accountId">Account id</param>
       /// <param name="accountCreatedDate">Account created date</param>
       /// <param name="context">Database context</param>
       /// <param name="appModule">The module for which jobs count are retrieved</param>      
       /// <returns></returns>
       private static int GetMonthlyJobsCount(int billingAnniversaryDay, int accountId, DateTime accountCreatedDate, GMGColorContext context, GMG.CoZone.Common.AppModule appModule)
       {
           DateTime startDate = DateTime.UtcNow;
           DateTime endDate = DateTime.UtcNow;
           int jobsCount = 0;

           DateTime startingDate = (startDate.Day == billingAnniversaryDay)
                                       ? startDate
                                       : GMGColorCommon.GetAvalilableDate(startDate.Year,
                                                                          (startDate.Day > billingAnniversaryDay)
                                                                              ? startDate.Month
                                                                              : startDate.Month - 1,
                                                                          billingAnniversaryDay);
           startingDate = (startingDate < accountCreatedDate) ? accountCreatedDate : startingDate;

           DateTime endingDate = (endDate.Day == billingAnniversaryDay)
                                     ? (startingDate.Month == endDate.Month)
                                           ? endDate.AddMonths(1)
                                           : endDate
                                     : (endDate.Month == 12)
                                           ? GMGColorCommon.GetAvalilableDate(endDate.Year + 1, 1,
                                                                              billingAnniversaryDay)
                                           : GMGColorCommon.GetAvalilableDate(endDate.Year, endDate.Month + 1,
                                                                              billingAnniversaryDay);

           int cycles = (endingDate.Year == startingDate.Year)
                            ? (endingDate.Month - startingDate.Month)
                            : ((endingDate.Year - startingDate.Year) * 12) + endingDate.Month - startingDate.Month;

           for (int i = 1; i < (cycles + 1); i++)
           {
               jobsCount += GetAddedJobsCount(accountId, startingDate, context, appModule);
               startingDate = startingDate.AddMonths(1);
           }

           return jobsCount;
       }

        /// <summary>
        /// Returns added jobs count for specific year
        /// </summary>
        /// <param name="billingAnniversaryDay">Biling plan contract start date</param>
        /// <param name="accountId">Account id</param>
        /// <param name="contractStartDate"></param>
        /// <param name="accountCreatedDate">Account created date</param>
        /// <param name="context">Database context</param>
        /// <param name="appModule">The module for which jobs count are retrieved</param>       
        /// <returns></returns>
        private static int GetYearlyJobsCount(int billingAnniversaryDay, int accountId, DateTime? contractStartDate, DateTime accountCreatedDate, GMGColorContext context, GMG.CoZone.Common.AppModule appModule)
        {
           DateTime startDate = DateTime.UtcNow;
           DateTime endDate = DateTime.UtcNow;
           int jobsCount = 0;

           DateTime startingDate = (startDate.Day == billingAnniversaryDay &&
                                            startDate.Month == contractStartDate.GetValueOrDefault().Month)
                                               ? startDate
                                               : GMGColorCommon.GetAvalilableDate(startDate.Year,
                                                                                  contractStartDate.GetValueOrDefault().Month,
                                                                                  billingAnniversaryDay);
           startingDate = (startingDate < accountCreatedDate) ? accountCreatedDate : startingDate;

           DateTime endingDate = GMGColorCommon.GetAvalilableDate(endDate.Year + 1, contractStartDate.GetValueOrDefault().Month, billingAnniversaryDay);

           int cycles = endingDate.Year - startingDate.Year;

           for (int i = 1; i < cycles + 1; i++)
           {
               jobsCount += GetAddedJobsCount(accountId, startingDate, context, appModule);
               startingDate = startingDate.AddYears(1);
           }

           return jobsCount;
       }

        /// <summary>
        ///  Get added jobs count by application module
        /// </summary>
        /// <param name="accountId">Account id</param>
        /// <param name="startingDate">The start date from which jobs count are retrieved</param>
        /// <param name="context">Database context</param>
        /// <param name="appModule">Applictaion module for which the jobs count are retrieved</param>      
        /// <returns></returns>
        private static int GetAddedJobsCount(int accountId, DateTime startingDate, GMGColorContext context, GMG.CoZone.Common.AppModule appModule)
        {
            int jobsCount = 0;
            switch (appModule)
            {
                case GMG.CoZone.Common.AppModule.Collaborate:
                    {
                        jobsCount = Approval.GetAddedApprovalsCount(accountId, startingDate, context);
                        break;
                    }

                case GMG.CoZone.Common.AppModule.Deliver:
                    {
                        jobsCount = DeliverJob.GetAddedDeliversCount(accountId, startingDate, context);
                        break;
                    }
            }
            return jobsCount;
        }

        /// <summary>
        /// Gets the billing report info.
        /// </summary>
        /// <param name="accountTypeList">The account type list.</param>
        /// <param name="accountList">The account list.</param>
        /// <param name="locationList">The location list.</param>
        /// <param name="billingPlanList">The billing plan list.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="loggedAccount">The logged account.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public List<ReturnBillingReportInfoView> GetBillingReportInfo(string accountTypeList, string accountList, string locationList, string billingPlanList, DateTime startDate, DateTime endDate, int loggedAccount, GMGColorContext context)
        {
            try
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 120;
                return context.GetBillingReportInfo(accountTypeList, accountList, locationList, billingPlanList, startDate, endDate, loggedAccount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetBillingReportInfo()", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the account parameters.
        /// </summary>
        /// <param name="loggedAccount">The logged account.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public List<ReturnAccountParametersView> GetAccountParameters(int loggedAccount, GMGColorContext context)
        {
            try
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 120;
                return context.GetAccountParameters(loggedAccount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetAccountParameters()", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the account collaborator and groups view.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public List<ReturnAccountUserCollaboratorsAndGroupsView> GetAccountCollaboratorAndGroupsView(int accountId, int loggedUserId, GMGColorContext context)
        {
            try
            {
                return context.GetAccountUserCollaboratorsAndGroups(accountId, loggedUserId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetAccountCollaboratorAndGroupsView()", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Removes the subscription plans for the specified account
        /// </summary>
        /// <param name="context"></param>
        /// <param name="accountId"> </param>
        public static void DeleteAccountSubscriptionPlans(int accountId, GMGColorContext context)
        {
            try
            {
                #region Delete ASP
                List<int> billingPlans =
                    (from a in context.AccountSubscriptionPlans
                     join b in context.BillingPlans on a.SelectedBillingPlan equals b.ID
                     join t in context.BillingPlanTypes on b.Type equals t.ID
                     join m in context.AppModules on t.AppModule equals m.ID
                     where a.Account == accountId &&
                           (                              
                               m.Key == (int) GMG.CoZone.Common.AppModule.Deliver ||
                               m.Key == (int) GMG.CoZone.Common.AppModule.Collaborate
                           )
                     select a.ID).ToList();

                foreach (int accountSubscriptionPlanId in billingPlans)
                {
                    AccountSubscriptionPlan plan = new AccountSubscriptionPlan() {ID = accountSubscriptionPlanId};
                    context.Entry(plan).State = EntityState.Deleted;
                }
                #endregion

                context.SaveChanges();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Sets the users status to inactive.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        public static void SetUsersStatusToInactive(int accountId, GMGColorContext context)
        {
            try
            {
                IEnumerable<User> userList =
                    (from u in context.Users
                     join a in context.Accounts on u.Account equals a.ID
                     join acs in context.AccountStatus on a.AccountStatu.ID equals acs.ID
                     where a.ID == accountId && u.ID != a.Owner && acs.Key == "A"
                     select u);

                var inactiveStatus = (from us in context.UserStatus where us.Key == "I" select us.ID).FirstOrDefault();

                if (inactiveStatus > 0 && userList.Any())
                {
                    foreach (User user in userList)
                    {
                        user.Status = inactiveStatus;
                    }
                }
            }
            finally
            {
            }
        }

        /// <summary>
        /// Sets the users status to active.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        public static void SetUsersStatusToActive(int accountId, GMGColorContext context)
        {
            try
            {
                IEnumerable<User> userList =
                    (from u in context.Users
                     join a in context.Accounts on u.Account equals a.ID
                     join acs in context.AccountStatus on a.AccountStatu.ID equals acs.ID
                     where a.ID == accountId && u.ID != a.Owner && acs.Key == "I"
                     select u);

                var activeStatus = (from us in context.UserStatus where us.Key == "A" select us.ID).FirstOrDefault();

                if (activeStatus > 0 && userList.Any())
                {
                    foreach (User user in userList)
                    {
                        user.Status = activeStatus;
                    }
                }
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the account owner id.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static int? GetAccountOwnerId(int accountId, GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts where a.ID == accountId select a.Owner).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Determines whether [is sys admin account] [the specified user id].
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is sys admin account] [the specified user id]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSysAdminAccount(int userId, GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts
                        join u in context.Users on a.ID equals u.Account
                        join at in context.AccountTypes on a.AccountType equals at.ID
                        where
                            u.ID == userId &&
                            at.Key == "GMHQ"
                        select a.ID).Any();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the childs.
        /// </summary>
        /// <returns></returns>
        public List<Account> GetChilds(GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts where a.Parent == ID select a).ToList();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the childs.
        /// </summary>
        /// <returns></returns>
        public List<Account> GetNotDeletedChilds(GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts
                        join acs in context.AccountStatus on a.Status equals acs.ID
                        where a.Parent == ID && acs.Key != "D"
                        select a).ToList();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets all regions.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<string> GetAllRegions(GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts select a.Region).Distinct().ToList();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the subscriber plan.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="module">The module.</param>
        /// <param name="context">The context.</param>       
        /// <returns></returns>
        public static SubscriberPlanInfo GetSubscriberPlan(int accountId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts
                        join spi in context.SubscriberPlanInfoes on a.ID equals spi.Account
                        join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where a.ID == accountId && am.Key == (int) module
                        select spi).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the non subscriber plan.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="module">The module.</param>
        /// <param name="context">The context.</param>       
        /// <returns></returns>
        public static AccountSubscriptionPlan GetNonSubscriberPlan(int accountId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts
                        join asp in context.AccountSubscriptionPlans on a.ID equals asp.Account
                        join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where a.ID == accountId && am.Key == (int)module
                        select asp).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Determines whether [is enable media tools] [the specified account id].
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="module">The module.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is enable media tools] [the specified account id]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEnableMediaTools(int accountId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
        {
            try
            {
                return (from asp in context.AccountSubscriptionPlans
                        join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where asp.Account == accountId && bpt.EnableMediaTools == true && am.Key == (int)module
                        select asp.ID).Any()
                       ||
                       (from spi in context.SubscriberPlanInfoes
                        join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where spi.Account == accountId && bpt.EnableMediaTools == true && am.Key == (int)module
                        select spi.ID).Any();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Retursn the allowed file types based on the account Collaborate plan and the account upload settings
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static AccountFileTypeFilter GetAccountApprovalFileTypesAllowed(int userId, GMGColorContext context)
        {
            int module = (int)GMG.CoZone.Common.AppModule.Collaborate;

            return (from a in context.Accounts
                    let planIncludesVideo = (
                        (from asp in context.AccountSubscriptionPlans
                             join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                             join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                             join am in context.AppModules on bpt.AppModule equals am.ID
                             where asp.Account == a.ID && bpt.EnableMediaTools == true && am.Key == (int)module
                             select asp.ID).Any() ||
                        (from spi in context.SubscriberPlanInfoes
                            join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                            join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                            join am in context.AppModules on bpt.AppModule equals am.ID
                         where spi.Account == a.ID && bpt.EnableMediaTools == true && am.Key == module
                            select spi.ID).Any()
                        )
                    join user in context.Users on a.ID equals user.Account
                    join u in context.UploadEngineAccountSettings on a.UploadEngineSetings equals u.ID into uploadSettings
                    from uSettings in uploadSettings.DefaultIfEmpty()
                    where user.ID == userId
                    select new AccountFileTypeFilter()
                    {
                        PlanIncludesVideo = planIncludesVideo,
                        HasUploadSettings = (uSettings != null),
                        UploadSettingsFilter = new UploadSettingsFileTypeFilter {
                                                    AllowPDF = (uSettings != null && uSettings.AllowPDF),
                                                    AllowImage = (uSettings != null && uSettings.AllowImage),
                                                    AllowVideo = (uSettings != null && uSettings.AllowVideo),
                                                    AllowSWF = (uSettings != null && uSettings.AllowSWF),
                                                    AllowZip = (uSettings != null && uSettings.AllowZip),
                                                    AllowDoc = (uSettings != null && uSettings.AllowDoc),
                                                    ApplyUploadSettingsForNewApproval = (uSettings != null && uSettings.ApplyUploadSettingsForNewApproval),
                                                    UseSettingsFromPreviousVersion = (uSettings != null && uSettings.UseUploadSettingsFromPreviousVersion)
                                                }
                    }).FirstOrDefault();
        }

        /// <summary>
        /// Get custom profiles available for current account
        /// </summary>
        /// <param name="accId"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static List<KeyValuePair<int, string>> GetAccountCustomProfiles(int accId, GMGColorContext context)
        {
            List<KeyValuePair<int, string>> cstmProfiles = (from accs in context.AccountCustomICCProfiles
                                                            where accs.Account == null
                                                            select new
                                                            {
                                                                accs.Name,
                                                                accs.ID
                                                            }).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();

            cstmProfiles.AddRange((from accs in context.AccountCustomICCProfiles
                                   where accs.Account == accId && accs.IsActive && accs.IsValid.HasValue && accs.IsValid.Value
                                   select new
                                   {
                                       accs.Name,
                                       accs.ID
                                   }).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList());
            return cstmProfiles;
        }

        /// <summary>
        /// Determines whether [is enable pre press tools] [the specified account id].
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="module">The module.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is enable pre press tools] [the specified account id]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEnablePrePressTools(int accountId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
        {
            try
            {
                return (from asp in context.AccountSubscriptionPlans
                        join bp in context.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where asp.Account == accountId && bpt.EnablePrePressTools == true && am.Key == (int)module
                        select asp.ID).Any()
                       ||
                       (from spi in context.SubscriberPlanInfoes
                        join bp in context.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                        join am in context.AppModules on bpt.AppModule equals am.ID
                        where spi.Account == accountId && bpt.EnablePrePressTools == true && am.Key == (int)module
                        select spi.ID).Any();
            }
            finally
            {
            }
        }
        

        public static bool HasRight(int accountId, int loggedUserId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
        {
            return ((from asp in context.AccountSubscriptionPlans
                     join a in context.Accounts on asp.Account equals a.ID
                     join acs in context.AccountStatus on a.Status equals acs.ID
                     join bp in context.BillingPlans on asp.BillingPlan.ID equals bp.ID
                     join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                     join am in context.AppModules on bpt.AppModule equals am.ID
                     where
                         am.Key == (int)module &&
                         a.ID == accountId &&
                        (!bp.IsDemo || (bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(asp.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) || acs.Key != "A"))
                     select asp.ID).Any()
                    ||
                    (from spi in context.SubscriberPlanInfoes
                     join a in context.Accounts on spi.Account equals a.ID
                     join acs in context.AccountStatus on a.Status equals acs.ID
                     join bp in context.BillingPlans on spi.BillingPlan.ID equals bp.ID
                     join bpt in context.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                     join am in context.AppModules on bpt.AppModule equals am.ID
                     where
                         am.Key == (int)module &&
                         a.ID == accountId &&
                         (!bp.IsDemo || (bp.IsDemo && (DbFunctions.DiffDays(DbFunctions.TruncateTime(spi.AccountPlanActivationDate), DbFunctions.TruncateTime(DateTime.UtcNow)) < GMGColorConfiguration.DEMOBILLINGPLANACTIVEDAYS) || acs.Key != "A"))
                     select spi.ID).Any())
                   &&
                   (from ur in context.UserRoles
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where
                        ur.User == loggedUserId &&
                        am.Key == (int)module &&
                        r.Key != "NON"
                    select ur.ID).Any();
        }

        /// <summary>
        /// Creates the billing plan for trial account.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="moduleKey">The module key.</param>
        /// <param name="billingPlanName">Name of the billing plan.</param>
        /// <param name="billingPlanProofs">The billing plan proofs.</param>
        /// <returns></returns>
        private static string CreateBillingPlanForTrialAccount(GMGColorContext context, GMG.CoZone.Common.AppModule moduleKey, string billingPlanName, int billingPlanProofs)
        {
            try
            {
                bool exists = (from bp in context.BillingPlans
                               where
                                   bp.Name == billingPlanName
                               select bp.ID).Any();
                if (!exists)
                {
                    BillingPlan billingPlan = new BillingPlan();
                    billingPlan.Type = (from bpt in context.BillingPlanTypes
                                                   join am in context.AppModules on bpt.AppModule equals am.ID
                                                   where am.Key == (int)moduleKey
                                                   select bpt.ID).FirstOrDefault();
                    billingPlan.Name = billingPlanName;
                    billingPlan.Proofs = billingPlanProofs;
                    billingPlan.Price = 0;
                    billingPlan.IsFixed = false;

                    context.BillingPlans.Add(billingPlan);
                    context.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(e);
                GMGColorDAL.GMGColorLogging.log.ErrorFormat(e, "Account.CreateBillingPlanForTrialAccount -> {0}", errorMessage);
                return errorMessage;
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Account.CreateBillingPlanForTrialAccount -> " + ex.Message, ex);
                return ex.Message;
            }
            return null;
        }

        /// <summary>
        /// Creates the parent for trial account.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        private static string CreateParentForTrialAccount(GMGColorContext context)
        {
            try
            {
                #region Create billing plans

                string result = CreateBillingPlanForTrialAccount(context, GMG.CoZone.Common.AppModule.Collaborate,
                                                                 TrialAccountMessages.CollaborateBillingPlanName,
                                                                 TrialAccountMessages.CollaborateBillingPlanUnits);
                if (!string.IsNullOrEmpty(result)) return result;

                result = CreateBillingPlanForTrialAccount(context, GMG.CoZone.Common.AppModule.Deliver,
                                                          TrialAccountMessages.DeliverBillingPlanName,
                                                          TrialAccountMessages.DeliverBillingPlanUnits);
                if (!string.IsNullOrEmpty(result)) return result;            

                #endregion

                #region Get foreign keys for the account

                int languageId =
                    (from l in context.Locales where l.Name == TrialAccountMessages.DefaultLanguage select l.ID).
                        FirstOrDefault();
                int dateFormatId =
                    (from df in context.DateFormats
                     where df.Pattern == TrialAccountMessages.DefaultDateFormat
                     select df.ID).FirstOrDefault();
                int timeFormatId =
                    (from tf in context.TimeFormats where tf.Key == TrialAccountMessages.DefaultTimeFormat select tf.ID)
                        .FirstOrDefault();
                int contractPeriodId =
                    (from cp in context.ContractPeriods
                     where cp.Key == TrialAccountMessages.DefaultContractPeriodKey
                     select cp.ID).FirstOrDefault();
                int accountStatusId =
                    (from status in context.AccountStatus
                     where status.Key == TrialAccountMessages.DefaultAccountStatusKey
                     select status.ID).FirstOrDefault(); //Active
                int accountTypeId =
                    (from at in context.AccountTypes
                     where at.Key == TrialAccountMessages.DefaultAccountTypeKeyForParent
                     select at.ID).FirstOrDefault();
                int currencyId =
                    (from c in context.Currencies where c.Code == TrialAccountMessages.DefaultCurrencyKey select c.ID).
                        FirstOrDefault();
                int userStatusId =
                    (from us in context.UserStatus
                     where us.Key == TrialAccountMessages.DefaultUserstateKeyForParent
                     select us.ID).FirstOrDefault();
                int userNotificationFrequencyId = (from nf in context.NotificationFrequencies
                                                   where nf.Key == TrialAccountMessages.DefaultNotificationsFrequencyKey
                                                   select nf.ID).FirstOrDefault();
                int userPresetId =
                    (from p in context.Presets where p.Key == TrialAccountMessages.DefaultUserPresetKey select p.ID).
                        FirstOrDefault();
                List<AccountTheme> acccThemes =
                    (from t in context.AccountThemes where t.Key == TrialAccountMessages.DefaultThemeKey select t).ToList();                       
                int collaborateBillingPlanId =
                    (from bp in context.BillingPlans
                     where bp.Name == TrialAccountMessages.CollaborateBillingPlanName
                     select bp.ID).FirstOrDefault();               
                int deliverBillingPlanId =
                    (from bp in context.BillingPlans
                     where bp.Name == TrialAccountMessages.DeliverBillingPlanName
                     select bp.ID).FirstOrDefault();               
                int adminAccountId = (from a in context.Accounts where a.Parent == null select a.ID).FirstOrDefault();

                #endregion

                #region Get creator user and company from the sysadmin account

                GMGColorDAL.User creatorUser =
                    (from a in context.Accounts
                     join u in context.Users on a.Owner equals u.ID
                     where a.Parent == null
                     select u).FirstOrDefault();

                GMGColorDAL.Company creatorCompany =
                    (from a in context.Accounts
                     join c in context.Companies on a.ID equals c.Account
                     where a.Parent == null
                     select c).FirstOrDefault();

                if (creatorUser == null)
                {
                    GMGColorDAL.GMGColorLogging.log.ErrorFormat(null, "Account.CreateParentForTrialAccount -> {0}",
                                                                TrialAccountMessages.CreateUserOfParentAccountIsNull);
                    return TrialAccountMessages.CreateUserOfParentAccountIsNull;
                }
                if (creatorCompany == null)
                {
                    GMGColorDAL.GMGColorLogging.log.ErrorFormat(null, "Account.CreateParentForTrialAccount -> {0}",
                                                                TrialAccountMessages.CompanyOfParentAccountIsNull);
                    return TrialAccountMessages.CompanyOfParentAccountIsNull;
                }

                #endregion

                #region Create and save the account with all references in a transaction

                string accountRegion = GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? GMGColorConfiguration.AppConfiguration.AWSRegion : String.Empty;

                using (TransactionScope tscope = new TransactionScope())
                {
                    #region Account

                    GMGColorDAL.Account account = new GMGColorDAL.Account();
                    account.Name = TrialAccountMessages.TrialAccountsName;
                    account.AccountType = accountTypeId;
                    account.Parent = adminAccountId;
                    account.ContactFirstName = creatorUser.GivenName;
                    account.ContactPhone = creatorCompany.Phone;
                    account.ContactLastName = creatorUser.FamilyName;
                    account.ContactEmailAddress = creatorUser.EmailAddress;
                    account.Locale = languageId;
                    account.TimeZone = TrialAccountMessages.DefaultTimeZoneUtc;
                    account.DateFormat = dateFormatId;
                    account.TimeFormat = timeFormatId;
                    account.Domain = (GMGColorConfiguration.AppConfiguration.Environment == "prod")
                                         ? TrialAccountMessages.TrialAccountsName.ToLower().Trim() +
                                           GMGColorConfiguration.AppConfiguration.HostAddress
                                         : TrialAccountMessages.TrialAccountsName.ToLower().Trim();
                    account.IsCustomDomainActive = false;
                    account.CustomDomain = null;
                    account.ContractPeriod = contractPeriodId;
                    account.Status = accountStatusId;
                    account.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    account.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    account.Creator = creatorUser.ID;
                    account.Modifier = creatorUser.ID;
                    account.Region = accountRegion;
                    account.CurrencyFormat = currencyId;
                    account.Guid = System.Guid.NewGuid().ToString();
                    account.SiteName = TrialAccountMessages.TrialAccountsName;
                    account.AccountThemes = acccThemes;
                    account.GPPDiscount = 0;
                    account.NeedSubscriptionPlan = true;

                    #endregion

                    #region Company

                    GMGColorDAL.Company company = new GMGColorDAL.Company();
                    company.Name = TrialAccountMessages.TrialAccountsName;
                    company.Phone = creatorCompany.Phone;
                    company.Email = creatorUser.EmailAddress;
                    company.City = creatorCompany.City;
                    company.Address1 = creatorCompany.Address1;
                    company.Country = creatorCompany.Country;
                    company.Guid = System.Guid.NewGuid().ToString();

                    account.Companies.Add(company);

                    #endregion

                    #region Owner user

                    GMGColorDAL.User ownerUser = new GMGColorDAL.User();
                    ownerUser.GivenName = creatorUser.GivenName;
                    ownerUser.FamilyName = creatorUser.FamilyName;
                    ownerUser.EmailAddress = creatorUser.EmailAddress;
                    ownerUser.Username = creatorUser.Username;
                    ownerUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(TrialAccountMessages.DefaultPassword,
                                                                                  context);
                    ownerUser.Guid = System.Guid.NewGuid().ToString();
                    ownerUser.OfficeTelephoneNumber = creatorUser.OfficeTelephoneNumber;
                    ownerUser.Status = userStatusId;
                    ownerUser.NotificationFrequency = userNotificationFrequencyId;
                    ownerUser.Preset = userPresetId;
                    ownerUser.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    ownerUser.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    ownerUser.Creator = creatorUser.ID;
                    ownerUser.Modifier = creatorUser.ID;

                    #region Roles for the owner user

                    GMGColorDAL.Role adminRole = (from r in context.Roles
                                                  join am in context.AppModules on r.AppModule equals am.ID
                                                  where
                                                      am.Key == TrialAccountMessages.DefaultAdminModuleKey &&
                                                      r.Key == TrialAccountMessages.DefaultAdminRoleKey
                                                  select r).FirstOrDefault();
                    ownerUser.UserRoles.Add(new UserRole() {Role1 = adminRole});

                    GMGColorDAL.Role collaborateRole = (from r in context.Roles
                                                        join am in context.AppModules on r.AppModule equals am.ID
                                                        where
                                                            am.Key == TrialAccountMessages.DefaultCollaborateModuleKey &&
                                                            r.Key == TrialAccountMessages.DefaultAdminRoleKey
                                                        select r).FirstOrDefault();
                    ownerUser.UserRoles.Add(new UserRole() {Role1 = collaborateRole});

                    GMGColorDAL.Role deliverRole = (from r in context.Roles
                                                    join am in context.AppModules on r.AppModule equals am.ID
                                                    where
                                                        am.Key == TrialAccountMessages.DefaultDeliverModuleKey &&
                                                        r.Key == TrialAccountMessages.DefaultAdminRoleKey
                                                    select r).FirstOrDefault();
                    ownerUser.UserRoles.Add(new UserRole() {Role1 = deliverRole});                 

                    #endregion

                    account.Users.Add(ownerUser);

                    #endregion

                    #region AttachedAccountBillingPlans

                    AttachedAccountBillingPlan attachedCollaboratePlan = new AttachedAccountBillingPlan
                                                                             {BillingPlan = collaborateBillingPlanId};                   

                    AttachedAccountBillingPlan attachedDeliverPlan = new AttachedAccountBillingPlan
                                                                         {BillingPlan = deliverBillingPlanId};

                    #endregion

                    #region AccountSubscriptionPlans

                    AccountSubscriptionPlan collaborateAccountSubscriptionPlan = new AccountSubscriptionPlan
                                                                                     {
                                                                                         SelectedBillingPlan =
                                                                                             collaborateBillingPlanId,
                                                                                         ChargeCostPerProofIfExceeded =
                                                                                             false,
                                                                                         IsMonthlyBillingFrequency =
                                                                                             true,
                                                                                         ContractStartDate = null
                                                                                     };
                  
                    AccountSubscriptionPlan deliverAccountSubscriptionPlan = new AccountSubscriptionPlan
                                                                                 {
                                                                                     SelectedBillingPlan =
                                                                                         deliverBillingPlanId,
                                                                                     ChargeCostPerProofIfExceeded =
                                                                                         false,
                                                                                     IsMonthlyBillingFrequency = true,
                                                                                     ContractStartDate = null
                                                                                 };

                    #endregion

                    #region Account Help Centre

                    GMGColorDAL.AccountHelpCentre objAccHelpCentre = new GMGColorDAL.AccountHelpCentre();
                    objAccHelpCentre.ContactEmail = creatorUser.EmailAddress;
                    account.AccountHelpCentres.Add(objAccHelpCentre);

                    #endregion

                    #region Relations between entities

                    account.AttachedAccountBillingPlans.Add(attachedCollaboratePlan);
                    account.AttachedAccountBillingPlans.Add(attachedDeliverPlan);

                    account.AccountSubscriptionPlans.Add(collaborateAccountSubscriptionPlan);
                    account.AccountSubscriptionPlans.Add(deliverAccountSubscriptionPlan);

                    context.Accounts.Add(account);
                    context.Users.Add(ownerUser);
                    context.Companies.Add(company);

                    context.AttachedAccountBillingPlans.Add(attachedCollaboratePlan);
                    context.AttachedAccountBillingPlans.Add(attachedDeliverPlan);

                    context.AccountSubscriptionPlans.Add(collaborateAccountSubscriptionPlan);
                    context.AccountSubscriptionPlans.Add(deliverAccountSubscriptionPlan);

                    #endregion

                    context.SaveChanges();

                    #region Save the owner user on account

                    account.Owner = ownerUser.ID;

                    context.SaveChanges();

                    #endregion

                    tscope.Complete();
                }

                #endregion
            }
            catch (DbEntityValidationException e)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(e);
                GMGColorDAL.GMGColorLogging.log.ErrorFormat(e, "AccountController.AddParentForTrialAccounts -> {0}", errorMessage);

                return TrialAccountMessages.UnknowError;
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("AccountController.AddParentForTrialAccounts -> " + ex.Message, ex);

                return TrialAccountMessages.UnknowError;
            }

            return null;
        }

        /// <summary>
        /// Creates the trial account.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="context">The context.</param>
        /// <param name="accountDomain">The account domain.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <returns></returns>
        public static string CreateTrialAccount(TrialAccount model, GMGColorContext context, string accountDomain, string companyName)
        {
            try
            {
                #region Retrieve parent account

                Account parentAccount = (from a in context.Accounts
                                         join accs in context.AccountStatus on a.Status equals accs.ID
                                         where a.Name == TrialAccountMessages.TrialAccountsName && accs.Key != "D"
                                         select a).FirstOrDefault();

                if (parentAccount == null)
                {
                    string result = CreateParentForTrialAccount(context);
                    if (!string.IsNullOrEmpty(result))
                    {
                        return result;
                    }
                    parentAccount = (from a in context.Accounts
                                     join accs in context.AccountStatus on a.Status equals accs.ID
                                     where a.Name == TrialAccountMessages.TrialAccountsName && accs.Key != "D"
                                     select a).FirstOrDefault();
                }

                if (parentAccount == null)
                {
                    GMGColorDAL.GMGColorLogging.log.Error("Account.CreateTrialAccount -> Parent account is null");
                    return TrialAccountMessages.UnknowError;
                }

                #endregion

                #region Query language, country and timezone by words - search by words

                var languages = (from l in context.Locales select new {l.DisplayName, l.ID}).ToList();
                int languageId = languages.Where(
                        o => o.DisplayName.ToLower().ContainsWord(model.Language.ToLower())).Select(o => o.ID).
                        FirstOrDefault();
                if (languageId == 0)
                {
                    return TrialAccountMessages.LanguageNotFound;
                }

                var countries = (from c in context.Countries select new {c.ShortName, c.ID}).ToList();
                int countryId = countries.Where(o=>o.ShortName.ToLower().ContainsWord(model.Country.ToLower())).Select(o=>o.ID).FirstOrDefault();
                if (countryId == 0)
                {
                    return TrialAccountMessages.CountryNotFound;
                }

                string timezone = TimeZoneInfo.GetSystemTimeZones().Where(o => o.DisplayName.ToLower().ContainsWord(model.Timezone.ToLower())).Select(o => o.Id).FirstOrDefault();
                if (string.IsNullOrEmpty(timezone))
                {
                    return TrialAccountMessages.TimezoneNotFound;
                }
                #endregion

                #region Get foreign keys for account

                int clientAccountTypeId = (from at in context.AccountTypes where at.Key == TrialAccountMessages.DefaultAccountTypeKey select at.ID).FirstOrDefault();
                int accountStatusId = (from status in context.AccountStatus where status.Key == TrialAccountMessages.DefaultAccountStatusKey select status.ID).FirstOrDefault();
                int userStatusId = (from us in context.UserStatus where us.Key == TrialAccountMessages.DefaultUserstateKey select us.ID).FirstOrDefault();
                int userNotificationFrequencyId = (from nf in context.NotificationFrequencies where nf.Key == TrialAccountMessages.DefaultNotificationsFrequencyKey select nf.ID).FirstOrDefault();
                int userPresetId = (from p in context.Presets where p.Key == TrialAccountMessages.DefaultUserPresetKey select p.ID).FirstOrDefault();
                List<AccountTheme> accThemes = (from t in context.AccountThemes where t.Key == TrialAccountMessages.DefaultThemeKey select t).ToList();

                int collaborateBillingPlanId = (from bp in context.BillingPlans where bp.Name == TrialAccountMessages.CollaborateBillingPlanName select bp.ID).FirstOrDefault();              
                int deliverBillingPlanId = (from bp in context.BillingPlans where bp.Name == TrialAccountMessages.DeliverBillingPlanName select bp.ID).FirstOrDefault();

                if (collaborateBillingPlanId == 0)
                {
                    CreateBillingPlanForTrialAccount(context, GMG.CoZone.Common.AppModule.Collaborate,
                                                          TrialAccountMessages.CollaborateBillingPlanName,
                                                          TrialAccountMessages.CollaborateBillingPlanUnits);
                }

                if (deliverBillingPlanId == 0)
                {
                    CreateBillingPlanForTrialAccount(context, GMG.CoZone.Common.AppModule.Deliver,
                                                          TrialAccountMessages.DeliverBillingPlanName,
                                                          TrialAccountMessages.DeliverBillingPlanUnits);
                }

                #endregion

                #region Create Account

                using (TransactionScope tscope = new TransactionScope())
                {
                    #region Account

                    Account account = new Account();
                    account.IsTrial = true;
                    account.Name = companyName;
                    account.SiteName = companyName;
                    account.Domain = accountDomain;
                    account.AccountType = clientAccountTypeId;
                    account.Parent = parentAccount.ID;
                    account.ContactFirstName = model.FirstName;
                    account.ContactLastName = model.LastName;
                    account.ContactEmailAddress = model.Email;
                    account.ContactPhone = model.Phone;
                    account.Locale = languageId;
                    account.TimeZone = timezone;
                    account.DateFormat = parentAccount.DateFormat;
                    account.TimeFormat = parentAccount.DateFormat;
                    
                    account.IsCustomDomainActive = false;
                    account.CustomDomain = null;
                    account.ContractPeriod = parentAccount.DateFormat;
                    account.Status = accountStatusId;
                    account.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    account.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    account.Creator = parentAccount.Creator;
                    account.Modifier = parentAccount.Modifier;
                    account.Region = parentAccount.Region;
                    account.CurrencyFormat = parentAccount.CurrencyFormat;
                    account.Guid = System.Guid.NewGuid().ToString();
                    account.AccountThemes = accThemes;
                    account.GPPDiscount = 0;
                    account.NeedSubscriptionPlan = true;
                    #endregion

                    #region Company
                    GMGColorDAL.Company company = new GMGColorDAL.Company();
                    company.Name = model.CompanyName;
                    company.Phone = model.Phone;
                    company.Email = model.Email;
                    company.City = model.City;
                    company.Address1 = model.Address1;
                    company.Address2 = model.Address2;
                    company.Country = countryId;
                    company.Guid = System.Guid.NewGuid().ToString();
                    #endregion

                    #region Owner User

                    GMGColorDAL.User ownerUser = new GMGColorDAL.User();
                    ownerUser.GivenName = model.FirstName;
                    ownerUser.FamilyName = model.LastName;
                    ownerUser.EmailAddress = model.Email;
                    ownerUser.Username = model.Email;
                    ownerUser.Password = GMGColorDAL.User.GetNewEncryptedPassword(TrialAccountMessages.DefaultPassword, context);
                    ownerUser.Guid = System.Guid.NewGuid().ToString();
                    ownerUser.OfficeTelephoneNumber = model.Phone;
                    ownerUser.Status = userStatusId;
                    ownerUser.NotificationFrequency = userNotificationFrequencyId;
                    ownerUser.Preset = userPresetId;
                    ownerUser.CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    ownerUser.ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g"));
                    ownerUser.Creator = parentAccount.Modifier;
                    ownerUser.Modifier = parentAccount.Modifier;
                    #endregion

                    #region Owner User Roles

                    Role adminRole = (from r in context.Roles
                                      join am in context.AppModules on r.AppModule equals am.ID
                                      where
                                          am.Key == TrialAccountMessages.DefaultAdminModuleKey &&
                                          r.Key == TrialAccountMessages.DefaultAdminRoleKey
                                      select r).FirstOrDefault();
                    ownerUser.UserRoles.Add(new UserRole() { Role1 = adminRole });

                    Role collaborateRole = (from r in context.Roles
                                            join am in context.AppModules on r.AppModule equals am.ID
                                            where
                                                am.Key == TrialAccountMessages.DefaultCollaborateModuleKey &&
                                                r.Key == TrialAccountMessages.DefaultAdminRoleKey
                                            select r).FirstOrDefault();
                    ownerUser.UserRoles.Add(new UserRole() { Role1 = collaborateRole });

                    Role deliverRole = (from r in context.Roles
                                        join am in context.AppModules on r.AppModule equals am.ID
                                        where
                                            am.Key == TrialAccountMessages.DefaultDeliverModuleKey &&
                                            r.Key == TrialAccountMessages.DefaultAdminRoleKey
                                        select r).FirstOrDefault();
                    ownerUser.UserRoles.Add(new UserRole() { Role1 = deliverRole });
                   
                    #endregion

                    #region Attached Account BillingPlans

                    AttachedAccountBillingPlan attachedCollaboratePlan = new AttachedAccountBillingPlan { BillingPlan = collaborateBillingPlanId };

                    AttachedAccountBillingPlan attachedDeliverPlan = new AttachedAccountBillingPlan { BillingPlan = deliverBillingPlanId };

                    #endregion

                    #region Account Subscription Plans

                    AccountSubscriptionPlan collaborateAccountSubscriptionPlan = new AccountSubscriptionPlan
                    {
                        SelectedBillingPlan = collaborateBillingPlanId,
                        ChargeCostPerProofIfExceeded = false,
                        IsMonthlyBillingFrequency = true,
                        ContractStartDate = null
                    };
                   
                    AccountSubscriptionPlan deliverAccountSubscriptionPlan = new AccountSubscriptionPlan
                    {
                        SelectedBillingPlan = deliverBillingPlanId,
                        ChargeCostPerProofIfExceeded = false,
                        IsMonthlyBillingFrequency = true,
                        ContractStartDate = null
                    };
                    #endregion

                    #region Account Help Centre

                    GMGColorDAL.AccountHelpCentre objAccHelpCentre = new GMGColorDAL.AccountHelpCentre();
                    objAccHelpCentre.ContactEmail = model.Email;
                    #endregion

                    #region Relations between entities

                    account.AccountHelpCentres.Add(objAccHelpCentre);
                    account.Users.Add(ownerUser);
                    account.Companies.Add(company);

                    account.AttachedAccountBillingPlans.Add(attachedCollaboratePlan);                  
                    account.AttachedAccountBillingPlans.Add(attachedDeliverPlan);

                    account.AccountSubscriptionPlans.Add(collaborateAccountSubscriptionPlan);
                    account.AccountSubscriptionPlans.Add(deliverAccountSubscriptionPlan);

                    context.Accounts.Add(account);
                    context.Users.Add(ownerUser);
                    context.Companies.Add(company);

                    context.AttachedAccountBillingPlans.Add(attachedCollaboratePlan);
                    context.AttachedAccountBillingPlans.Add(attachedDeliverPlan);

                    context.AccountSubscriptionPlans.Add(collaborateAccountSubscriptionPlan);                  
                    context.AccountSubscriptionPlans.Add(deliverAccountSubscriptionPlan);
                    #endregion

                    context.SaveChanges();

                    #region Save owner user on account

                    account.Owner = ownerUser.ID;
                    context.SaveChanges();
                    #endregion

                    tscope.Complete();
                }

                #endregion
            }
            catch (DbEntityValidationException e)
            {
                string errorMessage = DALUtils.GetDbEntityValidationException(e);
                GMGColorDAL.GMGColorLogging.log.ErrorFormat(e, "Account.CreateTrialAccount -> {0}", errorMessage);

                return TrialAccountMessages.UnknowError;
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Account.CreateTrialAccount -> " + ex.Message, ex);

                return TrialAccountMessages.UnknowError;
            }
            return null;
        }

        /// <summary>
        /// Gets the company name for trial accounts.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetCompanyNameForTrialAccounts(string companyName, GMGColorContext context)
        {
            try
            {
                int countCompaniesWithSameName =
                    (from c in context.Companies where c.Name.ToLower() == companyName.ToLower() select c.ID).Count();
                if (countCompaniesWithSameName > 0)
                {
                    companyName = string.Format("{0}{1}", companyName, countCompaniesWithSameName);
                }
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Account.GetCompanyNameForTrialAccounts -> " + ex.Message, ex);
            }
            return companyName;
        }

        /// <summary>
        /// Gets the account domain for trial accounts.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetAccountDomainForTrialAccounts(string companyName, GMGColorContext context)
        {
            string accontDomainWithHostAddress = string.Empty;
            try
            {
                // remove any ilegal characters
                string accountDomain = Regex.Replace(companyName.Replace(" ", "_").ToLower(), TrialAccountMessages.AccountDomainReplacePattern, string.Empty);

                accontDomainWithHostAddress = (GMGColorConfiguration.AppConfiguration.Environment == "prod")
                                    ? accountDomain.ToLower().Trim() + GMGColorConfiguration.AppConfiguration.HostAddress
                                    : accountDomain.ToLower().Trim();

                bool valid = !(from a in context.Accounts where a.Domain == accontDomainWithHostAddress select a.ID).Any();
                int step = 1;
                while (!valid)
                {
                    string domain = string.Format("{0}{1}", accountDomain, step);
                    accontDomainWithHostAddress = (GMGColorConfiguration.AppConfiguration.Environment == "prod")
                                    ? domain.ToLower().Trim() + GMGColorConfiguration.AppConfiguration.HostAddress
                                    : domain.ToLower().Trim();
                    valid = !(from a in context.Accounts where a.Domain == accontDomainWithHostAddress select a.ID).Any();
                    step++;
                }
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Account.GetAccountDomainForTrialAccounts -> " + ex.Message, ex);
            }
            return accontDomainWithHostAddress;
        }
        
        /// <summary>
        /// Gets the proof count
        /// </summary>
        /// <param name="billingPlan">Billing plan</param>
        /// <param name="subscriberPlan">Subscriber plan</param>
        /// <returns></returns>
        public static int GetProofsCount(BillingPlan billingPlan, SubscriberPlanInfo subscriberPlan = null)
        {
            int proofCount = 0;
            try
            {               
                if (subscriberPlan != null)
                {
                    proofCount = (billingPlan == null
                        ? 0
                        : (billingPlan.IsFixed
                            ? 0
                            : (subscriberPlan.IsQuotaAllocationEnabled
                                ? subscriberPlan.NrOfCredits
                                : billingPlan.Proofs)).GetValueOrDefault());
                }
                else
                {
                    proofCount = (billingPlan == null ? 0
                                                  : billingPlan.IsFixed ? 0 : billingPlan.Proofs
                              ).GetValueOrDefault();
                }
            }
            catch (Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error("Account.GetProofsCount -> " + ex.Message, ex);
            }
            return proofCount;
        }

        /// <summary>
        /// Gets the parent account.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The database context.</param>
        /// <returns></returns>
        public static int? GetParentAccount(int accountId, GMGColorContext context)
        {
            return (from a in context.Accounts where a.ID == accountId select a.Parent).FirstOrDefault();
        }

        public static List<KeyValuePair<int, string>> GetAccountStatuses(int accId, GMGColorContext context)
        {
            try
            {
                var list = (from acc in context.Accounts
                            join accst in context.AccountStatus on acc.Status equals accst.ID
                            where acc.Parent == accId && accst.Key != "D"
                            select new {accst.ID, accst.Name }).Distinct().AsEnumerable().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();

                return list;
            }
            finally
            {
            }
        }

        public List<AccountsReportView> GetAccountsReport(string selectedLocations, string selectedAccountTypes, string selectedAccountStatuses, string selectedAccountSubsciptionPlans,int loggedAccountId, GMGColorContext context)
        {
            try
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 120;
                return context.GetAccountsReport(selectedLocations, selectedAccountTypes, selectedAccountStatuses, selectedAccountSubsciptionPlans, loggedAccountId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetAccountsReport()", ex);
            }
            finally
            {
            }
        }

        public static bool IsSubscriberAccount(int userId, GMGColorContext context)
        {
            return
                (from a in context.Accounts
                 join u in context.Users on a.ID equals u.Account
                 join at in context.AccountTypes on a.AccountType equals at.ID
                 where u.ID == userId && at.Key == "SBSC"
                 select a.ID).Any();
        }

        public static bool IsRetoucherRoleAvailable(int accId, GMGColorContext context)
        {
            string isRetoucherAvaialableSetting = (from acs in context.AccountSettings
                                                    where acs.Name == "EnableRetoucherWorkflow" && acs.Account == accId
                                                    select acs.Value).FirstOrDefault();

            bool isRetouchAvailable;
            bool.TryParse(isRetoucherAvaialableSetting, out isRetouchAvailable);

            return isRetouchAvailable;
        }

        public static bool HasRetoucherUser(int accId, GMGColorContext context)
        {
            string query = @"SELECT Extent1.ID
                                  FROM 
                                    [dbo].[User] AS [Extent1] 
			                        INNER JOIN [dbo].[UserRole] AS [Extent3] ON   [Extent3].[User] = [Extent1].[ID]
                                  WHERE 
			                        [Extent1].Account=@accId 	
			                        and [Extent1].Status in (1,2,3)
			                        and [Extent3].[Role] = 28";


            return context.Database.SqlQuery<int>(query, new SqlParameter("accId", accId)).Any();
        }

        public static List<int> GetPlanTypeOptions(int accountId, GMG.CoZone.Common.AppModule module, GMGColorContext context)
        {
            List<int> planTypeOptionsList = new List<int>();

            if (IsEnablePrePressTools(accountId, module, context))
            {
                planTypeOptionsList.Add((int)PlanFeatures.CustomProfile);
                planTypeOptionsList.Add((int)PlanFeatures.RotateTool);
                planTypeOptionsList.Add((int)PlanFeatures.Separations);
                planTypeOptionsList.Add((int)PlanFeatures.Densitometer);
                planTypeOptionsList.Add((int)PlanFeatures.Ruler);
            }

            planTypeOptionsList.Add((int)PlanFeatures.ProfilePreview);
            planTypeOptionsList.Add((int)PlanFeatures.ViewingConditions);

            return planTypeOptionsList;
        }

        #endregion
    }
}

