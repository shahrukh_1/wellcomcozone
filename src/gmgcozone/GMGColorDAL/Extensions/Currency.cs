using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class Currency : BusinessObjectBase
    {
        #region Enums

        public enum CurrencyType
        {
            /// <summary>
            /// Key = AUD
            /// </summary>
            AustralianDollar,
            /// <summary>
            /// Key = GBP
            /// </summary>
            BritishPound,
            /// <summary>
            /// Key = EUR
            /// </summary>
            EURO,
            /// <summary>
            /// Key = JPY
            /// </summary>
            JapaneseYen,
            /// <summary>
            /// Key = USD
            /// </summary>
            USDollar
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        public static Currency GetCurrency(CurrencyType type, GMGColorContext context)
        {
            try
            {
                return (from c in context.Currencies
                        where
                            (
                                (type == CurrencyType.AustralianDollar && c.Code == "AUD") ||
                                (type == CurrencyType.BritishPound && c.Code == "GBP") ||
                                (type == CurrencyType.EURO && c.Code == "EUR") ||
                                (type == CurrencyType.JapaneseYen && c.Code == "JPY") ||
                                (type == CurrencyType.USDollar && c.Code == "USD")
                            )
                        select c).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static string GetLocalizedCurrencyName(int currency, GMGColorContext context)
        {
            try
            {
                return
                    Currency.GetLocalizedCurrencyName(
                        (from c in context.Currencies where c.ID == currency select c.Code).FirstOrDefault());
            }
            finally
            {
            }
        }
        
        public static string GetLocalizedCurrencyName(CurrencyType type, GMGColorContext context)
        {
            return Currency.GetLocalizedCurrencyName(Currency.GetCurrency(type, context).Code);
        }

        public static string GetLocalizedCurrencyName(string key)
        {
            switch (key)
            {
                case "AUD": return Resources.resCurrencyTypeAUD;
                case "GBP": return Resources.resCurrencyTypeGBP;
                case "EUR": return Resources.resCurrencyTypeEUR;
                case "JPY": return Resources.resCurrencyTypeJPY;
                case "USD": return Resources.resCurrencyTypeUSD;
                default: return string.Empty;
            }
        }

        public static Dictionary<int, string> GetLocalizedCurrencyNames(GMGColorContext context)
        {
            try
            {
                return (from c in context.Currencies select new {c.ID, c.Code}).ToDictionary(o => o.ID,
                                                                                             o =>
                                                                                             Currency.
                                                                                                 GetLocalizedCurrencyName
                                                                                                 (o.Code));
            }
            finally
            {
            }
        }

        #endregion
    }
}