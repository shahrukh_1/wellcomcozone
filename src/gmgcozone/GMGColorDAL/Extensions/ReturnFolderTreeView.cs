using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ReturnFolderTreeView : BusinessObjectBase
    {        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public bool FolderHasApprovals(List<ReturnFolderTreeView> lstFolders)
        {
            if (HasApprovals.GetValueOrDefault())
            {
                return true;
            }
            else
            {
                bool hasApprovals = false;
                // Sub Folders
                foreach (ReturnFolderTreeView objSubFolder in lstFolders.Where(t => t.Parent == ID))
                {
                    hasApprovals = objSubFolder.FolderHasApprovals(lstFolders);

                    if(hasApprovals)
                        break;
                }

                return hasApprovals;
            }
        }

        #endregion
    }
}

