using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using RSC = GMGColor.Resources;

namespace GMGColorDAL
{
    public class UserGroup_Validation
    {
        [Required(ErrorMessageResourceName = "reqGroupName", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }
    }

    [MetadataType(typeof(UserGroup_Validation))]
    public partial class UserGroup : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static bool IsGroupNameUnique(string groupName, int? groupId, int? accountId, GMGColorContext context)
        {
            bool exists = false;
            exists = groupId.HasValue && groupId.Value > 0
                                ? ((from g in context.UserGroups
                                    where g.Name == groupName && g.ID != groupId.Value && g.Account == accountId
                                    select g.ID).Any())
                                : ((from g in context.UserGroups
                                    where g.Name == groupName && g.Account == accountId
                                    select g.ID).Any());
            return !exists;
        }

        public static UserGroup GetObject(int userGroupId, GMGColorContext context)
        {
            try
            {
                return (from ug in context.UserGroups where ug.ID == userGroupId select ug).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static List<UserGroup> GetUserGroups(int accountId, GMGColorContext context)
        {
            try
            {
                return (from ug in context.UserGroups where ug.Account == accountId orderby ug.Name select ug).ToList();
            }
            finally
            {
            }
        }

        public static List<KeyValuePair<int, string>> GetUserGroupsInfo(int accountId, GMGColorContext context)
        {
            try
            {
                return (from ug in context.UserGroups
                       where ug.Account == accountId
                        select new
                        {
                            ug.ID,
                            ug.Name
                        }).ToList().Select(t => new KeyValuePair<int, string>(t.ID, t.Name)).ToList();
            }
            finally
            {
            }
        }

        #endregion

    }
}

