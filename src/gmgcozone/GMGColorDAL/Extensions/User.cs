using System;
using System.Activities.Expressions;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Security;
using GMG.CoZone.Common;
using GMGColor.AWS;
using GMGColorDAL.Common;
using Resources = GMGColor.Resources;
using System.Data.Entity.Infrastructure;

namespace GMGColorDAL
{
    public class User_Validation
    {
        //[Required(ErrorMessageResourceName = "reqEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
        //[RegularExpression(Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string EmailAddress { get; set; }

        [Required(ErrorMessageResourceName = "reqFirstName", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string GivenName { get; set; }

        [Required(ErrorMessageResourceName = "reqLastName", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string FamilyName { get; set; }

        [Required(ErrorMessageResourceName = "reqUsername", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Username { get; set; }

        [Required(ErrorMessageResourceName = "reqPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string Password { get; set; }
    }

    [Serializable]
    [MetadataType(typeof(User_Validation))]
    public partial class User : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Logins the specified account.
        /// </summary>
        /// <param name="account">The account.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static GMGColorDAL.User Login(int account, string username, string password, GMGColorContext context)
        {
            try
            {
                return (from u in context.GetUserLogin(account, username, password) select u).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the default image path.
        /// </summary>
        /// <param name="domain">The domain.</param>
        /// <returns></returns>
        public static string GetDefaultImagePath(string domain)
        {
            return GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/nouser-48px-48px.png";
        }

        /// <summary>
        /// Gets the new random password.
        /// </summary>
        /// <returns></returns>
        public static string GetNewRandomPassword()
        {
            return GetNewRandomPassword(8);
        }

        /// <summary>
        /// Gets the new random password.
        /// </summary>
        /// <param name="passwordLength">Length of the password.</param>
        /// <returns></returns>
        public static string GetNewRandomPassword(int passwordLength)
        { 
            string _allowedCharsUpperCase = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
            string _allowedCharsLowerCase = "abcdefghijkmnopqrstuvwxyz";
            string _allowedCharsNumeric = "0123456789";
            string _allowedCharsSpecial = @"!""#$%&'()*+,./:;<>?@[\]^_{|}~";
            Random randNum = new Random();
            char[] chars = new char[passwordLength];
            
            for (int i = 0; i < passwordLength; i++)
            {
                if(i < 2)
                {
                    chars[i] = _allowedCharsUpperCase[(int)((_allowedCharsUpperCase.Length) * randNum.NextDouble())];
                }
                else if(i < 4)
                {
                    chars[i] = _allowedCharsSpecial[(int)((_allowedCharsSpecial.Length) * randNum.NextDouble())];
                }
                else if (i < 6)
                {
                    chars[i] = _allowedCharsNumeric[(int)((_allowedCharsNumeric.Length) * randNum.NextDouble())];
                }
                else
                {
                    chars[i] = _allowedCharsLowerCase[(int)((_allowedCharsLowerCase.Length) * randNum.NextDouble())];
                }
            }

            return new string(chars);
        }

        /// <summary>
        /// Gets the new encrypted random password.
        /// </summary>
        /// <returns></returns>
        public static string GetNewEncryptedRandomPassword(GMGColorContext context)
        {
            return GetNewEncryptedPassword(GetNewRandomPassword(8), context);
        }

        /// <summary>
        /// Gets the new encrypted random password.
        /// </summary>
        /// <param name="passwordLength">Length of the password.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetNewEncryptedRandomPassword(int passwordLength, GMGColorContext context)
        {
            return GetNewEncryptedPassword(GetNewRandomPassword(passwordLength), context);
        }

        /// <summary>
        /// Gets the new encrypted password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetNewEncryptedPassword(string password, GMGColorContext context)
        {
            try
            {
                var passw = (from p in context.GetEncryptedPassword(password) select p).FirstOrDefault();
                return passw != null ? passw.RetVal : string.Empty;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the image path.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="defaultImageIfNotExist">if set to <c>true</c> [default image if not exist].</param>
        /// <param name="context"></param>
        /// <param name="isExternal"></param>
        /// <returns></returns>
        public static string GetImagePath(int userId, GMGColorContext context, bool isExternal = false)
        {
            string absoluteFileUrl = string.Empty; 
            string relativeFileUrl = string.Empty;

            try
            {
                var accountData = !isExternal
                    ? (from a in context.Accounts
                        join u in context.Users on a.ID equals u.Account
                        join accs in context.AccountStatus on a.Status equals accs.ID
                        where u.ID == userId && accs.Key == "A" // only active accounts
                        select new
                        {
                            a.Region, 
                            Domain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain,
                            userPhotoPath = u.PhotoPath,
                            userGuid = u.Guid
                        })
                        .FirstOrDefault()
                    : (from a in context.Accounts
                        join ec in context.ExternalCollaborators on a.ID equals ec.Account
                        where ec.ID == userId
                        select new
                        {
                            a.Region, 
                            Domain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain,
                            userPhotoPath = "",
                            userGuid = ec.Guid
                        })
                        .FirstOrDefault();

                if (accountData != null)
                {
                    string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(accountData.Region);
                    string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountData.Region);

                    absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountData.Domain + "/" + dataFolderName + "/";
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        absoluteFileUrl = pathtoDataFolder;
                    }


                    if (!String.IsNullOrEmpty(accountData.userPhotoPath))
                    {
                        relativeFileUrl = "users/" + accountData.userGuid + "/" + accountData.userPhotoPath;
                       
                        absoluteFileUrl += relativeFileUrl + "?" + (new Random()).Next();
                    }
                    else
                    {
                        absoluteFileUrl = User.GetDefaultImagePath(accountData.Domain);
                    }
                }
                else
                {
                    string domain = (from a in context.Accounts
                                     join accs in context.AccountStatus on a.Status equals accs.ID
                                     where accs.Key == "A"
                                     orderby a.ID ascending
                                     select a.IsCustomDomainActive ? a.CustomDomain : a.Domain).Take(1).FirstOrDefault();

                    absoluteFileUrl = User.GetDefaultImagePath(domain);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, string.Format("Error occured in image path. absoluteFileURL: {0}, relativeFileURL: {1}", absoluteFileUrl, relativeFileUrl), ex);
                throw;
            }

            return absoluteFileUrl.ToLower();
        }

        public static string GetImagePath(string accountRegion, string accountDomain, string userPhotoPath, string userGuid, bool defaultImageIfNotExist = false, bool isExternal = false)
        {
            string absoluteFileUrl = string.Empty;
            string relativeFileUrl = string.Empty;
            bool fileNotFound = true;

            string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion);
            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion);

            absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + dataFolderName + "/";
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                absoluteFileUrl = pathtoDataFolder;
            }


            if (!String.IsNullOrEmpty(userPhotoPath))
            {
                relativeFileUrl = "users/" + userGuid + "/" + userPhotoPath;
                // Don't merge the two condions together.
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    if (AWSSimpleStorageService.FileExistsInBucket(dataFolderName, relativeFileUrl, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey))
                    {
                        absoluteFileUrl += relativeFileUrl + "?" + (new Random()).Next();
                        fileNotFound = false;
                    }
                }
                else
                {
                    string absoluteFilePath = pathtoDataFolder + "users\\" + userGuid + "\\" + userPhotoPath;
                    if (System.IO.File.Exists(absoluteFilePath))
                    {
                        absoluteFileUrl += relativeFileUrl + "?" + (new Random()).Next();
                        fileNotFound = false;
                    }
                }
            }

            if (fileNotFound)
            {
                absoluteFileUrl = User.GetDefaultImagePath(accountDomain);
            }

            return absoluteFileUrl.ToLower();
        }

        /// <summary>
        /// Gets the image path.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="defaultImageIfNotExist">if set to <c>true</c> [default image if not exist].</param>
        /// <param name="context"></param>
        /// <param name="isExternal"></param>
        /// <returns></returns>
        public static string GetImagePath(int userId, bool defaultImageIfNotExist, GMGColorContext context, bool isExternal = false)
        {
            var accountData = !isExternal
                ? (from a in context.Accounts
                    join u in context.Users on a.ID equals u.Account
                    join accs in context.AccountStatus on a.Status equals accs.ID
                    where u.ID == userId && accs.Key == "A"
                    // only active accounts
                    select new
                    {
                        a.Region,
                        Domain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain,
                        userPhotoPath = u.PhotoPath,
                        userGuid = u.Guid
                    })
                    .FirstOrDefault()
                : (from a in context.Accounts
                    join ec in context.ExternalCollaborators on a.ID equals ec.Account
                    where ec.ID == userId
                    select new
                    {
                        a.Region,
                        Domain = a.IsCustomDomainActive ? a.CustomDomain : a.Domain,
                        userPhotoPath = "",
                        userGuid = ec.Guid
                    })
                    .FirstOrDefault();

            if (accountData != null)
            {
                return GetImagePath(accountData.Region, accountData.Domain, accountData.userPhotoPath,
                    accountData.userGuid, defaultImageIfNotExist, isExternal);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets image for the specified User
        /// </summary>
        /// <param name="user"></param>
        /// <param name="account"></param>
        /// <param name="defaultImageIfNotExist"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetImagePath(User user, Account account)
        {
            string absoluteFileUrl = string.Empty;
            string relativeFileUrl = string.Empty;

            try
            {
                string accountDomain;

                accountDomain = account.IsCustomDomainActive ? account.CustomDomain : account.Domain;

                string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(account.Region);
                string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(account.Region);

                absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + dataFolderName + "/";
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    absoluteFileUrl = pathtoDataFolder;
                }

                if (!String.IsNullOrEmpty(user.PhotoPath))
                {
                    relativeFileUrl = "users/" + user.Guid + "/" + user.PhotoPath;
                    absoluteFileUrl += relativeFileUrl + "?" + (new Random()).Next();
                }
                else
                {
                    absoluteFileUrl = User.GetDefaultImagePath(accountDomain);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, string.Format("Error occured in image path. absoluteFileURL: {0}, relativeFileURL: {1}", absoluteFileUrl, relativeFileUrl), ex);
                throw ex;
            }

            return absoluteFileUrl.ToLower();
        }

        /// <summary>
        /// Gets the active users.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<User> GetActiveUsers(int accountId, GMGColorContext context)
        {
            try
            {
                return
                    (from a in context.Accounts
                     join u in context.Users on a.ID equals u.Account
                     join us in context.UserStatus on u.Status equals us.ID
                     where
                         a.ID == accountId && 
                         us.Key != "I" && // Inactive
                         us.Key != "D" // Delete
                     select u).ToList();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the admin user report info.
        /// </summary>
        /// <param name="accountTypeList">The account type list.</param>
        /// <param name="accountList">The account list.</param>
        /// <param name="locationList">The location list.</param>
        /// <param name="userStatusList">The user status list.</param>
        /// <param name="loggedAccount">The logged account.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public List<ReturnAdminUserReportInfoView> GetUserReportInfo(string accountTypeList, string accountList, string locationList, string userStatusList, int loggedAccount, GMGColorContext context)
        {
            try
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 120;
                return context.GetAdminUserReportInfo(accountTypeList, accountList, locationList, userStatusList, loggedAccount).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetUserReportInfo()", ex);
            }
            finally
            {
            }
        }

        public List<ReturnFileReportInfoView> GetFileReportInfo(string accountList, string userGroupList, DateTime startDate, DateTime endDate,int reportType, GMGColorContext context)
        {
            try
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 120;
                return context.GetFileReportInfo(accountList, userGroupList, startDate, endDate, reportType).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetFileReportInfo()", ex);
            }
            finally
            {
            }
        }

        // <summary>
        /// Gets the user report info.
        /// </summary>
        /// <param name="accountTypeList">The account type list.</param>
        /// <param name="accountList">The account list.</param>
        /// <param name="locationList">The location list.</param>
        /// <param name="userStatusList">The user status list.</param>
        /// <param name="loggedAccount">The logged account.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public List<ReturnUserReportInfoView> GetAccountUserReportInfo(string accountList, string selectedUserGroup, DateTime StartDate, DateTime EndDate, int loggedAccount, GMGColorContext context)
        {
            try
            {
                ((IObjectContextAdapter)context).ObjectContext.CommandTimeout = 120;
                return context.GetUserReportInfo(accountList, selectedUserGroup, loggedAccount, StartDate , EndDate).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetUserReportInfo()", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Removes the user from group.
        /// </summary>
        /// <param name="userGroupId">The user group id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public bool RemoveUserFromGroup(int userGroupId, GMGColorContext context)
        {
            try
            {
                UserGroupUser userGroupUser = (from ugu in context.UserGroupUsers
                                           where ugu.UserGroup == userGroupId && ugu.User == ID
                                           select ugu).FirstOrDefault();

                if (userGroupUser != null)
                {
                    context.UserGroupUsers.Remove(userGroupUser);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error removing user from user group", ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the role.
        /// </summary>
        /// <param name="appModuleKey">The app module key.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static string GetRole(GMG.CoZone.Common.AppModule appModuleKey, int userId, GMGColorContext context)
        {
            return (from ur in context.UserRoles
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where am.Key == (int) appModuleKey && ur.User == userId
                    select r.Key).FirstOrDefault();
        }

        /// <summary>
        /// Gets the role id.
        /// </summary>
        /// <param name="appModuleKey">The app module key.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static int? GetRoleId(GMG.CoZone.Common.AppModule appModuleKey, int userId, GMGColorContext context)
        {
            return (from ur in context.UserRoles
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && ur.User == userId
                    select r.ID).FirstOrDefault();
        }

        /// <summary>
        /// Gets the role Key.
        /// </summary>
        /// <param name="appModuleKey">The app module key.</param>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        public static string GetRoleKey(GMG.CoZone.Common.AppModule appModuleKey, int userId, GMGColorContext context)
        {
            return (from ur in context.UserRoles
                    join r in context.Roles on ur.Role equals r.ID
                    join am in context.AppModules on r.AppModule equals am.ID
                    where am.Key == (int)appModuleKey && ur.User == userId
                    select r.Key).FirstOrDefault();
        }

        /// <summary>
        /// Needs the re login from database.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public bool NeedReLoginFromDatabase(GMGColorContext context)
        {
            try
            {
                return (from u in context.Users where u.ID == ID select u.NeedReLogin).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the user statuses.
        /// </summary>
        /// <param name="accountIds">The account ids.</param>
        /// <param name="loggedAccount">The logged account.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<KeyValuePair<int, string>> GetUserStatuses(List<int> accountIds, int loggedAccount, GMGColorContext context)
        {
            try
            {
                var list = (from u in context.Users
                            join us in context.UserStatus on u.Status equals us.ID
                            join a in context.Accounts on u.Account equals a.ID
                            join acs in context.AccountStatus on a.Status equals acs.ID
                            where (u.Account == loggedAccount || accountIds.Contains(u.Account)) && acs.Key =="A" && us.Key != "D" 
                            select new {Key = us.ID, Value = us.Name}).ToList().Distinct();

                List<KeyValuePair<int, string>> returnList = new List<KeyValuePair<int, string>>();
                foreach (var item in list)
                {
                    returnList.Add(new KeyValuePair<int, string>(item.Key, item.Value));
                }
                return returnList;
            }
            finally
            {
            }
        }

        public static User GetUser(int userId, GMGColorContext context)
        {
            try
            {
                return (from usr in context.Users
                        where usr.ID == userId
                        select usr).SingleOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting user object from database", ex);
            }
        }

        /// <summary>
        /// Determines whether this instance [can edit user] the specified user id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="loggedAccountId">The logged account id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if this instance [can edit user] the specified user id; otherwise, <c>false</c>.
        /// </returns>
        public static bool CanEditUser(int userId, int loggedAccountId, GMGColorContext context)
        {
            try
            {
                return (from u in context.Users
                        join a in context.Accounts on u.Account equals a.ID
                        where u.ID == userId && a.Parent == loggedAccountId
                        select u.ID).Any();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Determines whether the specified user id is admin.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if the specified user id is admin; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAdmin(int userId, GMGColorContext context)
        {
            try
            {
                return
                    (from ur in context.UserRoles
                     join r in context.Roles on ur.Role equals r.ID
                     join am in context.AppModules on r.AppModule equals am.ID
                     where
                         ur.User == userId &&
                         (
                         (am.Key == (int) GMG.CoZone.Common.AppModule.Admin && r.Key == "ADM") ||
                         (am.Key == (int) GMG.CoZone.Common.AppModule.SysAdmin && r.Key == "HQA")
                         )
                     select ur.ID).Any();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Determines whether [is need subscription] [the specified user id].
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is need subscription] [the specified user id]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNeedSubscription(int userId, GMGColorContext context)
        {
            try
            {
                return (from a in context.Accounts
                        join u in context.Users on a.ID equals u.Account
                        where u.ID == userId
                        select a.NeedSubscriptionPlan).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the roles.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<IGrouping<int, Role>> GetRoles(int accountId, GMGColorContext context)
        {
            try
            {
                List<IGrouping<int, Role>> roles =  AccountType.GetAccountType(accountId, context) != AccountType.Type.GMGColor
                                                        ? (from r in context.Roles
                                                           join am in context.AppModules on r.AppModule equals am.ID
                                                           where
                                                               am.Key != (int) GMG.CoZone.Common.AppModule.SysAdmin
                                                           select r).GroupBy(o => o.AppModule).OrderBy(g => g.Key).ToList()
                                                        : (from r in context.Roles
                                                           join am in context.AppModules on r.AppModule equals am.ID
                                                           where
                                                               am.Key == (int) GMG.CoZone.Common.AppModule.SysAdmin
                                                           select r).GroupBy(o => o.AppModule).ToList();

                return roles;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static User GetObject(int userId, GMGColorContext context)
        {
            try
            {
                return (from u in context.Users where u.ID == userId select u).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the users list by event type and account.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The database context.</param>
        /// <returns></returns>
        public static IEnumerable<int> GetUsersListByEventTypeAndAccount(string eventType, int accountId, GMGColorContext context)
        {
            return (from et in context.EventTypes
                    join rpet in context.RolePresetEventTypes on et.ID equals rpet.EventType
                    join r in context.Roles on rpet.Role equals r.ID
                    join ur in context.UserRoles on r.ID equals ur.Role
                    join u in context.Users on ur.User equals u.ID
                    join us in context.UserStatus on u.Status equals us.ID
                    join nfreq in context.NotificationFrequencies on u.NotificationFrequency equals nfreq.ID
                    where et.Key == eventType && u.Account == accountId && us.Key == "A" // only active users
                    select u.ID).Distinct();
        }

        /// <summary>
        /// Gets the users list by event type and account.
        /// </summary>
        /// <param name="accountId">The account id.</param>
        /// <param name="context">The database context.</param>
        /// <returns></returns>
        public static IEnumerable<int> GetProjectFolderUsers( int accountId,int ProjectFolderID , GMGColorContext context)
        {
            
            var users = (from  pc in context.ProjectFolderCollaborators 
                         join u in context.Users on pc.Collaborator equals u.ID
                         where pc.ProjectFolder == ProjectFolderID
                         select u.ID).Distinct();
            return users;
        }

        public static IEnumerable<int> GetProjectFolderUsersForUpdate(int accountId, int ProjectFolderID, GMGColorContext context)
        {
            var d = DateTime.UtcNow.AddMinutes(-1);
            var users = (from pc in context.ProjectFolderCollaborators
                         join u in context.Users on pc.Collaborator equals u.ID
                         where pc.ProjectFolder == ProjectFolderID &&  pc.AssignedDate > d 
                         select u.ID).Distinct();
            return users;
        }
        
        
        // TODO - remove notification
        /// <summary>
        /// Includes his own activity.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="context">The database context.</param>
        /// <returns></returns>
        public static bool IncludeHisOwnActivity(int userId, GMGColorContext context)
        {
            return (from u in context.Users where u.ID == userId select u.IncludeMyOwnActivity).FirstOrDefault();
        }

        /// <summary>
        /// Gets the user temp folder path.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="accountRegion">The account region.</param>
        /// <returns></returns>
        public static string GetUserTempFolderPath(int? userId, int? accountId, string accountRegion)
        {
            string subfolderPath = string.Empty;
            if (userId.HasValue)
            {
                subfolderPath = userId.Value.ToString("000") + "-" + accountId.GetValueOrDefault().ToString("000");
            }

            string relativeFolderURL = "temp/" + subfolderPath + "/";
            GMGColorIO.FolderExists(relativeFolderURL, accountRegion, true);

            return relativeFolderURL;
        }

        public static string GetUserHtmlTempFolderPath(int? userId, int? accountId, string accountRegion)
        {
            string subfolderPath = string.Empty;
            if (userId.HasValue)
            {
                subfolderPath = userId.Value.ToString("000") + "-" + accountId.GetValueOrDefault().ToString("000");
            }

            string relativeFolderURL = "temp/" + subfolderPath + "/";
            GMGColorIO.FolderExists(relativeFolderURL, accountRegion, true);

            return relativeFolderURL;
        }

        /// <summary>
        /// Gets user email logo path
        /// </summary>
        /// <param name="objAccount">Recipient's acoount</param>
        /// <param name="loggedUserBrandingEmailLogo">Branding preset guid + slash + email logo name</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static string GetUserEmailLogoPath(Account objAccount, string loggedUserBrandingEmailLogo, GMGColorContext context)
        {
            string dataFolderName = GMGColorConfiguration.AppConfiguration.DataFolderName(objAccount.Region);
            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(objAccount.Region);

            string absoluteFileUrl = String.Empty;
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                absoluteFileUrl = pathtoDataFolder;
            }
            else
            {
                string accountDomain = objAccount.IsCustomDomainActive ? objAccount.CustomDomain : objAccount.Domain;

                absoluteFileUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + dataFolderName + "/";
            }

            string relativeFileURL = "accounts/" + objAccount.Guid + "/" + GMGColorConfiguration.AppConfiguration.UserGroupBrandingFolder + "/" + loggedUserBrandingEmailLogo;

            absoluteFileUrl += relativeFileURL + "?" + (new Random()).Next();

            return absoluteFileUrl.ToLower();
        }

        #endregion

        #region Extension Properties

        public int? FileTransferRoleId(GMGColorContext context)
        {
            return GetRoleId(GMG.CoZone.Common.AppModule.FileTransfer, ID, context);
        }

        public string FileTransferRoleKey(GMGColorContext context)
        {
            return GetRoleKey(GMG.CoZone.Common.AppModule.FileTransfer, ID, context);
        }

        public string FileTransferRole(GMGColorContext context)
        {
            return GetRole(GMG.CoZone.Common.AppModule.FileTransfer, ID, context);
        }

        public int? CollaborateRoleId(GMGColorContext context)
        {
            return GetRoleId(GMG.CoZone.Common.AppModule.Collaborate, ID, context);
        }

        public string CollaborateRoleKey(GMGColorContext context)
        {
            return GetRoleKey(GMG.CoZone.Common.AppModule.Collaborate, ID, context);
        }

        public string CollaborateRole (GMGColorContext context)
        {
            return GetRole(GMG.CoZone.Common.AppModule.Collaborate, ID, context);
        }       

        public int? DeliverRoleId(GMGColorContext context)
        {
            return GetRoleId(GMG.CoZone.Common.AppModule.Deliver, ID, context);
        }

        public string DeliverRoleKey(GMGColorContext context)
        {
            return GetRoleKey(GMG.CoZone.Common.AppModule.Deliver, ID, context);
        }

        public string DeliverRole(GMGColorContext context)
        {
            return GetRole(GMG.CoZone.Common.AppModule.Deliver, ID, context);
        }

        public int? AdminRoleId (GMGColorContext context)
        {
            return GetRoleId(GMG.CoZone.Common.AppModule.Admin, ID, context);
        }

        public string AdminRoleKey(GMGColorContext context)
        {
            return GetRoleKey(GMG.CoZone.Common.AppModule.Admin, ID, context);
        }

        public string AdminRole(GMGColorContext context)
        {
            return GetRole(GMG.CoZone.Common.AppModule.Admin, ID, context);
        }

        public int? SysAdminRoleId(GMGColorContext context)
        {
            return GetRoleId(GMG.CoZone.Common.AppModule.SysAdmin, ID, context);
        }

        public string SysAdminRoleKey(GMGColorContext context)
        {
            return GetRoleKey(GMG.CoZone.Common.AppModule.SysAdmin, ID, context);
        }

        public string SysAdminRole(GMGColorContext context)
        {
            return GetRole(GMG.CoZone.Common.AppModule.SysAdmin, ID, context);
        }
       
        #endregion


        public static string GetUserFullName(int userId, GMGColorContext context)
        {
            return (from u in context.Users
                    where u.ID == userId
                    select u.GivenName + " " + u.FamilyName).FirstOrDefault();
        }

        public static  int? GetUserByUserName(int Account , string userName, GMGColorContext context)
        {
            return (from u in context.Users
                    where u.Username == userName && u.Account == Account
                    select u.ID).FirstOrDefault();
        }

        public static UserLoginFailedDetail GetUserLoginAttempts(int UserId, GMGColorContext context)
        {
            return (from ua in context.UserLoginFailedDetails
                    where ua.User == UserId 
                    select ua).FirstOrDefault();
        }

        public string ReportRoleKey(GMGColorContext context)
        {
            return GetRoleKey(GMG.CoZone.Common.AppModule.Reports, ID, context);
        }

        public static DateTime? GetModifiedDateByUserName(int Account, string userName, GMGColorContext context)
        {
            return (from u in context.Users
                    where u.Username == userName && u.Account == Account && u.Status == 1
                    select u.ModifiedDate).FirstOrDefault();
        }

    }
}

