using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace GMGColorDAL
{
    public partial class Job : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Determines whether this instance can access the specified user id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="jobIdList">The job id list.</param>
        /// <param name="operation">The operation.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if this instance can access the specified user id; otherwise, <c>false</c>.
        /// </returns>
        public static bool CanAccess(int userId, IEnumerable<int> jobIdList, Approval.ApprovalOperation operation, GMGColorContext context)
        {
            bool result = false;
            try
            {
                bool hasRole =
                        (from u in context.Users
                         join ur in context.UserRoles on u.ID equals ur.User
                         join r in context.Roles on ur.Role equals r.ID
                         join am in context.AppModules on r.AppModule equals am.ID
                         where u.ID == userId &&
                               am.Key == 0 &&
                               r.Key != "VIE" &&
                               r.Key != "NON"
                         select u.ID).Any();

                if (!hasRole)
                    return false;

                switch (operation)
                {
                    case Approval.ApprovalOperation.AddNewVersion:
                        {
                            result = (from a in context.Approvals
                                      join j in context.Jobs on a.Job equals j.ID
                                      join js in context.JobStatus on j.Status equals js.ID

                                      let collaborators = (from ac in context.ApprovalCollaborators
                                                     where ac.Approval == a.ID
                                                     select ac.Collaborator).ToList()
                                      where
                                          jobIdList.Contains(j.ID) &&
                                          (collaborators.Contains(userId) || j.JobOwner == userId) &&
                                          a.IsDeleted == false &&
                                          (js.Key != "ARC" )
                                      select a.ID).Any();
                            break;
                        }
                }
            }
            finally
            {
            }
            return result;
        }
        

        /// <summary>
        /// Gets the archive completed jobs.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<Job> GetCompletedJobs(GMGColorContext context)
        {
            try
            {
                DateTime dtNow = DateTime.UtcNow;
                int daysCount = int.Parse(GlobalSetting.GetGlobalSetting(GlobalSetting.Setting.StoreCompletedFor, context).Value);
                int completedStatusId = JobStatu.GetJobStatus(JobStatu.Status.Completed, context).ID;
                return (from j in context.Jobs
                        where
                            j.Status == completedStatusId &&
                            DbFunctions.DiffDays(j.ModifiedDate, dtNow) >= daysCount
                        select j).ToList();
            }
            finally
            {
            }
        }

        public static List<Job> GetArchivedJobs(GMGColorContext context)
        {
            try
            {
                int daysCount = int.Parse(GlobalSetting.GetGlobalSetting(GlobalSetting.Setting.StoreArchivedFor, context).Value);
                int archivedStatusId = JobStatu.GetJobStatus(JobStatu.Status.Archived, context).ID;
                DateTime dtNow = DateTime.UtcNow;
                return (from j in context.Jobs
                        where
                            j.Status == archivedStatusId &&
                            DbFunctions.DiffDays(j.ModifiedDate, dtNow) >= daysCount
                        select j).ToList();
            }
            finally
            {
            }
        }

        public static void ArchiveJob(int jobId, GMGColorContext context)
        {
            try
            {
                var job = (from j in context.Jobs where j.ID == jobId select j).FirstOrDefault();
                if (job != null)
                {
                    job.Status = JobStatu.GetJobStatus(JobStatu.Status.Archived, context).ID;
                    job.ModifiedDate = DateTime.UtcNow;
                }
                context.SaveChanges();
            }
            finally
            {
            }
        }

        public static void ArchiveJob(Job job, int archivedStatus)
        {
            try
            {
                if (job != null)
                {
                    job.Status = archivedStatus;
                    job.ModifiedDate = DateTime.UtcNow;
                }
            }
            finally
            {
            }
        }

        #endregion
    }
}

