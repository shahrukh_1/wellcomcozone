using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ColorProofWorkflow : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion

        #region Properties
        public string SupportedSpotColorsForWeb
        {
            get
            {
                return GMG.CoZone.Common.Utils.GetSupportedSpotColorsForWeb(SupportedSpotColors);
            }
        }
        #endregion

        #region Extension Methods

        public static string GetColorProofWorkflowName(int czwfID, GMGColorContext context)
        {
            return (from cpczwf in context.ColorProofCoZoneWorkflows
                    join cpwf in context.ColorProofWorkflows on cpczwf.ColorProofWorkflow equals cpwf.ID
                    where cpczwf.ID == czwfID
                    select cpwf.Name).FirstOrDefault();
        }

        public static string GetColorProofCoZoneWorkflowName(int czwfID, GMGColorContext context)
        {
            return (from cpczwf in context.ColorProofCoZoneWorkflows
                    where cpczwf.ID == czwfID
                    select cpczwf.Name).FirstOrDefault();
        }
        #endregion
    }
}

