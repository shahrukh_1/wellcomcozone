﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMGColorDAL
{
    public partial class FileTransfer : BusinessObjectBase
    {
        public static List<ReturnFileTransferHistoryPageView> GetFileTransferInGrid(int account, int user, int rowsPerPage, int pageNr, int searchField, bool orderDesc, string searchText, string searchSenderText, ref int totalRows, GMGColorContext context)
        {
            try
            {
                System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof(int));
                //List<ReturnFileTransferHistoryPageView> lst = context.SPC_ReturnFileTransfersHistoryPageInfo(account, user, rowsPerPage, pageNr, searchField, orderDesc, searchText, recCount).ToList();
                List<ReturnFileTransferHistoryPageView> lst = context.GetFileTransfersHistoryPageInfo(account, user, rowsPerPage, pageNr, searchField, orderDesc, searchText,searchSenderText, recCount).ToList();

                totalRows = Convert.ToInt32(recCount.Value);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {

            }
        }

        public static List<ReturnFileTransferHistoryPageView> GetAdministratorFileTransferInGrid(int account, int rowsPerPage, int pageNr, int searchField, bool orderDesc, string searchText, string searchSender, ref int totalRows, GMGColorContext context)
        {
            try
            {
                System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof(int));
                //List<ReturnFileTransferHistoryPageView> lst = context.SPC_ReturnFileTransfersHistoryPageInfo(account, user, rowsPerPage, pageNr, searchField, orderDesc, searchText, recCount).ToList();
                List<ReturnFileTransferHistoryPageView> lst = context.GetAdministratorFileTransfersHistoryPageInfo(account, rowsPerPage, pageNr, searchField, orderDesc, searchText, searchSender, recCount).ToList();

                totalRows = Convert.ToInt32(recCount.Value);
                return lst;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {

            }
        }
    }
}
