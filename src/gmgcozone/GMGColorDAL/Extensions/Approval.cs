using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.IO;
using GMG.CoZone.Common;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;

namespace GMGColorDAL
{
    public partial class Approval : BusinessObjectBase
    {
        #region OnPropertyChange Methods

        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Enums

        /// <summary>
        /// Enumeration of available  Approval Statuses
        /// </summary>
        public enum ApprovalDecision
        {
            Pending = 1,
            ChangesRequired = 2,
            ApprovedWithChanges = 3,
            Approved = 4,
            NotApplicable = 5
        }

        public enum ImageType
        {
            Original,
            Processed,
            Thumbnail
        }

        public enum ProcessingStatus
        {
            Queued,
            Processing,
            Error,
            Success
        }

        public enum ApprovalTypeEnum
        {
            Image = 0,
            Movie,
            WebPage,
            Zip
        }

        #endregion

        #region Constants

        public const int SmallThumbnailDimension = 128;
        public const int SmallThumbnailDimension_220px = 220;
        public const int ProcessedThumbnailDimension = 650;
        public const int AnnotationWidth = 625;
        public const string FullSizePreview = "Converted.jpg";
        public const long PreviewMaxSize = 100000000;
        public const string ConvertedPdfFileName = "Converted.pdf";

        #endregion

        #region Extension Fields

        #endregion

        #region Extension Methods

        public static string GetDefaultImagePath(ImageType type, string domain)
        {
            string absoluteFileURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/";
            switch (type)
            {
                case ImageType.Original:
                case ImageType.Processed:
                default: absoluteFileURL += "noimage-640px-360px.png";
                    break;
                case ImageType.Thumbnail: absoluteFileURL += "noimage-240px-135px.png";
                    break;
            }
            return absoluteFileURL.ToLower();
        }

        public static string GetApprovalTypeChar(string key)
        {
            switch (key)
            {
                case "Image": return "I";
                case "Video": return "V";
                case "Document": return "D";
                case "Audio": return "A";
                case "HTML": return "H";
                default: return "I";
            }
        }
        public class ApprovalTypeCount
        {
            public string Name { get; set; }
            public int ApprovalCount { get; set; }
        }
        public static string GetOriginalImagePath(int approvalId, GMGColorContext context)
        {
            return Approval.GetImagePath(approvalId, ImageType.Original, context);
        }

        public static string GetProcessedImagePath(string accountDomain, string accountRegion, string approvalGuid, string approvalFilename, int page = 1, bool checkIfExists = true)
        {
            return Approval.GetImagePath(accountDomain, accountRegion, approvalGuid, approvalFilename, ImageType.Processed, page, checkIfExists);
        }

        public static string GetProcessedImagePath(int approvalId, GMGColorContext context, int page = 1, bool checkIfExists = true)
        {
            return Approval.GetImagePath(approvalId, ImageType.Processed, context, page, checkIfExists);
        }

        public static string GetThumbnailImagePath(string accountDomain, string accountRegion, string approvalGuid, string approvalFilename, int page = 1, bool checkIfExists = true)
        {
            return Approval.GetImagePath(accountDomain, accountRegion, approvalGuid, approvalFilename, ImageType.Thumbnail, page, checkIfExists);
        }

        public static string GetThumbnailImagePath(int approvalId, GMGColorContext context, int page = 1, bool checkIfExists = true)
        {
            return Approval.GetImagePath(approvalId, ImageType.Thumbnail, context, page, checkIfExists);
        }

        public static string GetThumbnailImagePath(int approvalId, GMGColorContext context)
        {
            return Approval.GetImagePath(approvalId, ImageType.Thumbnail, context);
        }

        public static string GetThumbnailZipPath(int approvalId, string domain, GMGColorContext context)
        {
            try
            {
                return GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/html.gif";
            }
            finally
            {
            }
        }

        public static string GetVideoImagePath(int approvalId, string domain, GMGColorContext context)
        {
            string absoluteFileURL = String.Empty;
            try
            {
                var objApproval = (from a in context.Approvals
                                   join j in context.Jobs on a.Job equals j.ID
                                   join acc in context.Accounts on j.Account equals acc.ID
                                   where a.ID == approvalId
                                   select new {a.Guid, acc.Region}).FirstOrDefault();

                if (objApproval != null && GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    absoluteFileURL = GMGColorCommon.GetDataFolderHttpPath(objApproval.Region, domain);
                    string relativeFolderPath = "approvals/" + objApproval.Guid + "/" + GMGColorConfiguration.THUMBNAIL + "/" + GMGColorConfiguration.VIDEOTHUMBNAIL;
                    if (GMGColorCommon.FileExists(relativeFolderPath, objApproval.Region))
                    {
                        absoluteFileURL += relativeFolderPath;
                    }
                    else
                    {
                        absoluteFileURL = Approval.GetDefaultImagePath(ImageType.Thumbnail, domain);
                    }
                }
                else
                {
                    absoluteFileURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/video.png";
                }
                return absoluteFileURL;
            }
            finally
            {
            }
        }

        public static string GetVideoImagePath(string guid, string region, string domain)
        {
            string absoluteFileURL = GMGColorCommon.GetDataFolderHttpPath(region, domain);
            string relativeFolderPath = "approvals/" + guid + "/" + GMGColorConfiguration.THUMBNAIL + "/thumbnail_00002.jpg";

            GMGColorLogging.log.InfoFormat("GetVideoImagePath relativeFolderPath: {0} and absoluteFileURL: {1} ", relativeFolderPath, absoluteFileURL);


            if (!GMGColorCommon.FileExists(absoluteFileURL += relativeFolderPath, region, true))
            {
                absoluteFileURL = GMGColorCommon.GetDataFolderHttpPath(region, domain);
                relativeFolderPath = "approvals/" + guid + "/" + GMGColorConfiguration.THUMBNAIL + "/" + GMGColorConfiguration.VIDEOTHUMBNAIL;
                absoluteFileURL += relativeFolderPath;

                GMGColorLogging.log.InfoFormat("GetVideoImagePath FileExists not exist relativeFolderPath: " + absoluteFileURL);

            }
            return absoluteFileURL;

            //try
            //{
            //    if ( GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            //    {
            //        absoluteFileURL = GMGColorCommon.GetDataFolderHttpPath(region, domain);

            //        absoluteFileURL += relativeFolderPath;
            //    }
            //    else
            //    {
            //        absoluteFileURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/video.png";
            //    }
            //    return absoluteFileURL;
            //}
            //finally
            //{
            //}
        }

        public static string GetZipPath(string domain)
        {
            string absoluteFileURL = String.Empty;
            try
            {
                absoluteFileURL = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/content/img/html.gif";
                return absoluteFileURL;
            }
            finally
            {
            }
        }

        private static string GetImagePath(string accountDomain, string accountRegion, string approvalGuid, string approvalFilename, ImageType type, int page = 1, bool checkIfExists = true)
        {
            string relativeFolderPath = "approvals/" + approvalGuid + "/";
            string absoluteFilePath = GMGColorCommon.GetDataFolderHttpPath(accountRegion, accountDomain);

            string fileName = approvalFilename;
            if (type != ImageType.Original)
                fileName = GMGColorCommon.ReplaceInvalidCharactors(Path.GetFileNameWithoutExtension(fileName)) + Path.GetExtension(fileName);

            bool fileNotFound = true;
            if (!String.IsNullOrEmpty(fileName))
            {
                switch (type)
                {
                    case ImageType.Original:
                        {
                            relativeFolderPath += fileName;
                            if (GMGColorCommon.FileExists(relativeFolderPath, accountRegion, checkIfExists))
                            {
                                absoluteFilePath += relativeFolderPath;
                                fileNotFound = false;
                            }
                            break;
                        }
                    case ImageType.Processed:
                        {
                            if (GMGColorCommon.IsVideoFile(Path.GetExtension(approvalFilename)))
                                relativeFolderPath += page + "/Thumb-128px.jpg-360px_0000.jpg";
                            else
                                relativeFolderPath += page + "/Thumb-" + ProcessedThumbnailDimension + "px.jpg";
                            if (GMGColorCommon.FileExists(relativeFolderPath, accountRegion, checkIfExists))
                            {
                                absoluteFilePath += relativeFolderPath;
                                fileNotFound = false;
                            }
                            break;
                        }
                    case ImageType.Thumbnail:
                        {
                            if (GMGColorCommon.IsVideoFile(Path.GetExtension(approvalFilename)))
                                relativeFolderPath += page + "/Thumb-128px_0000.jpg";
                            else
                                relativeFolderPath += page + "/Thumb-" + SmallThumbnailDimension + "px.jpg";
                            if (GMGColorCommon.FileExists(relativeFolderPath, accountRegion, checkIfExists))
                            {
                                absoluteFilePath += relativeFolderPath;
                                fileNotFound = false;
                            }
                            break;
                        }
                    default:
                        break;
                }
            }

            if (fileNotFound)
            {
                absoluteFilePath = Approval.GetDefaultImagePath(type, accountDomain);
            }
            return absoluteFilePath;
        }

        private static string GetImagePath(int approvalId, ImageType type, GMGColorContext context, int page = 1, bool checkIfExists = true)
        {
            var objApproval = (from a in context.Approvals
                                join j in context.Jobs on a.Job equals j.ID
                                join acc in context.Accounts on j.Account equals acc.ID
                                where a.ID == approvalId
                                select
                                    new
                                        {
                                            a.Guid,
                                            acc.Region,
                                            Domain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                            a.FileName
                                        }).FirstOrDefault();
            if (objApproval != null)
            {
                return GetImagePath(objApproval.Domain, objApproval.Region, objApproval.Guid, objApproval.FileName,
                    type, page, checkIfExists);
            }
            return string.Empty;
        }

        public static string GetApprovalImage(string domain, string region, string appGuid, string fileName, ImageType type, int page = 1)
        {
            try
            {
                    string relativeFolderPath = "approvals/" + appGuid + "/";
                    string absoluteFilePath = GMGColorCommon.GetDataFolderHttpPath(region, domain);

                    if (type != ImageType.Original)
                        fileName = GMGColorCommon.ReplaceInvalidCharactors(Path.GetFileNameWithoutExtension(fileName)) + Path.GetExtension(fileName);

                    if (!String.IsNullOrEmpty(fileName))
                    {
                        switch (type)
                        {
                            case ImageType.Original:
                                {
                                    relativeFolderPath += fileName;
                                    absoluteFilePath += relativeFolderPath;
                                    break;
                                }
                            case ImageType.Processed:
                                {
                                    relativeFolderPath += page + "/Thumb-" + ProcessedThumbnailDimension + "px.jpg";
                                    absoluteFilePath += relativeFolderPath;
                                    break;
                                }
                            case ImageType.Thumbnail:
                                {
                                    relativeFolderPath += page + "/Thumb-" + SmallThumbnailDimension + "px.jpg";
                                    absoluteFilePath += relativeFolderPath;
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                    return absoluteFilePath;
            }
            finally
            {
            }
        }

        public static string GetApprovalPageImagePhysicalPath(int approvalPageId, GMGColorContext context)
        {
            try
            {
                string accountRegion = (from ap in context.ApprovalPages
                                        join a in context.Approvals on ap.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join acc in context.Accounts on j.Account equals acc.ID
                                        where ap.ID == approvalPageId
                                        select acc.Region).FirstOrDefault();
                return GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) +
                    GetApprovalPageImageRelativePath(approvalPageId, context);
            }
            finally
            {
            }
        }

        public static string GetApprovalPageImageRelativePath(int approvalPageId, GMGColorContext context)
        {
            try
            {
                string extension = ".jpg";

                // Commented since current implementation supports only jpeg
                //string imageFormatName = (from ap in context.ApprovalPages
                //                          join a in context.Approvals on ap.Approval equals a.ID
                //                          join j in context.Jobs on a.Job equals j.ID
                //                          join acc in context.Accounts on j.Account equals acc.ID
                //                          join asp in context.AccountSubscriptionPlans on acc.ID equals asp.Account
                //                          join bpt in context.BillingPlanTypes on asp.BillingPlan.Type equals bpt.ID
                //                          join ms in context.MediaServices on bpt.MediaService equals ms.ID
                //                          join imgf in context.ImageFormats on ms.ImageFormat equals imgf.ID
                //                          where ap.ID == approvalPageId
                //                          select imgf.Name
                //                         ).FirstOrDefault()
                //                         ??
                //                         (from ap in context.ApprovalPages
                //                          join a in context.Approvals on ap.Approval equals a.ID
                //                          join j in context.Jobs on a.Job equals j.ID
                //                          join acc in context.Accounts on j.Account equals acc.ID
                //                          join spi in context.SubscriberPlanInfoes on acc.ID equals spi.Account
                //                          join bpt in context.BillingPlanTypes on spi.BillingPlan.Type equals bpt.ID
                //                          join ms in context.MediaServices on bpt.MediaService equals ms.ID
                //                          join imgf in context.ImageFormats on ms.ImageFormat equals imgf.ID
                //                          where ap.ID == approvalPageId select imgf.Name
                //                         ).FirstOrDefault();
                //switch (imageFormatName)
                //{
                //    case "JPEG":
                //    case "Compression":
                //    case "JPEG_minimum":
                //    case "JPEG_low":
                //    case "JPEG_medium":
                //    case "JPEG_high":
                //    case "JPEG_maximum":
                //        extension = ".jpg";
                //        break;
                //    case "PNG":
                //        extension = ".png";
                //        break;
                //    case "TIFF":
                //        extension = ".tiff";
                //        break;
                //    default:
                //        extension = ".jpg";
                //        break;
                //}

                var objApprovalPage = (from ap in context.ApprovalPages
                                   join a in context.Approvals on ap.Approval equals a.ID
                                   where ap.ID == approvalPageId
                                   select new {a.Guid, ap.Number}).FirstOrDefault();
                if (objApprovalPage != null)
                {
                    string physicalImagePath;

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        physicalImagePath = "approvals/" + objApprovalPage.Guid + "/" +
                                            objApprovalPage.Number +
                                            "/Converted" + extension;
                    }
                    else
                    {
                        physicalImagePath = "approvals\\" + objApprovalPage.Guid + "\\" +
                                            objApprovalPage.Number +
                                            "\\Converted" + extension;
                    }

                    return physicalImagePath;
                }
                return string.Empty;
            }
            finally
            {
            }
        }

        public static string GetApprovalPageImageRelativePath(string approvalGuid, int pageNumber)
        {
            string physicalImagePath;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                physicalImagePath = "approvals/" + approvalGuid + "/" +
                                    pageNumber +
                                    "/Converted" + ".jpg";
            }
            else
            {
                physicalImagePath = "approvals\\" + approvalGuid + "\\" +
                                    pageNumber +
                                    "\\Converted" + ".jpg";
            }

            return physicalImagePath;
        }

        public static string GetOriginalFilePath(string accountDomain, string accountRegion, string approvalGuid, string approvalFilename)
        {
            string filePath;

            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                filePath = GMGColorCommon.GetDataFolderHttpPath(accountRegion, accountDomain) + "approvals/" + approvalGuid + "/" + approvalFilename;
            }
            else
            {
                filePath = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + accountDomain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion) + "/Approvals/" + approvalGuid + "/" + approvalFilename;
            }

            return filePath;
        }
        
        public static string GetApprovalFolderPath(int approvalId, GMGColorContext context)
        {
            try
            {
                var objApproval = (from app in context.Approvals
                                   join j in context.Jobs on app.Job equals j.ID
                                   join acc in context.Accounts on j.Account equals acc.ID
                                   where app.ID == approvalId
                                   select
                                       new
                                           {
                                               app.FileName,
                                               Domain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                               acc.Region,
                                               app.Guid
                                           }).
                    FirstOrDefault();
                if (objApproval != null)
                {
                    string physicalFolderPath = string.Empty;

                    if (GMGColorCommon.IsVideoFile(Path.GetExtension(objApproval.FileName)) && GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        string absoluteFilePath = GMGColorCommon.GetDataFolderHttpPath(objApproval.Region, objApproval.Domain);

                        physicalFolderPath = absoluteFilePath + "approvals/" + objApproval.Guid + "/";
                    }
                    else if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        physicalFolderPath = GMGColorConfiguration.AppConfiguration.PathToDataFolder(objApproval.Region) + "\\Approvals\\" + objApproval.Guid + "\\";
                    }

                    return physicalFolderPath;
                }
                return string.Empty;
            }
            finally
            {
                
            }
        }

        public static int GetApprovalFileTypeKey(int id, GMGColorContext context)
        {
            try
            {
                return (from a in context.Approvals
                        join at in context.ApprovalTypes on a.Type equals at.ID
                        where a.ID == id
                        select at.Key).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static ApprovalProcessingStatus GetApprovalProcessingStatus(int approvalId, GMGColorContext context)
        {
            int progressValue = -1;
            ProcessingStatus status = ProcessingStatus.Error;

            try
            {
                var isError =
                    (from a in context.Approvals where a.ID == approvalId select a.IsError).FirstOrDefault();
                if (isError)
                {
                    status = ProcessingStatus.Error;
                }
                else
                {
                     progressValue =
                        (from ap in context.ApprovalPages
                            where ap.Approval == approvalId && ap.Number == 1
                            select ap.Progress).FirstOrDefault();

                    if (progressValue == 100)
                    {
                        status = ProcessingStatus.Success;
                    }
                    else if (progressValue == 0)
                    {
                        status = ProcessingStatus.Queued;
                    }
                    else
                    {
                        status = ProcessingStatus.Processing;
                    }
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, "Approval.IsApprovalProcessing : {0}", ex.Message);
            }
            finally
            {
            }
            return new ApprovalProcessingStatus { Status = status, Progress = progressValue, ProgressText = progressValue.ToString() };
        }

        public static string GetApprovalFolderHttpPath(int approvalId, GMGColorContext context)
        {
            try
            {
                var objApproval = (from a in context.Approvals
                                   join j in context.Jobs on a.Job equals j.ID
                                   join acc in context.Accounts on j.Account equals acc.ID
                                   where a.ID == approvalId
                                   select
                                       new
                                           {
                                               Domain = acc.IsCustomDomainActive ? acc.CustomDomain : acc.Domain,
                                               acc.Region,
                                               a.Guid
                                           }).FirstOrDefault();
                if (objApproval != null)
                {
                    string httpPath = string.Empty;

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        httpPath = GMGColorCommon.GetDataFolderHttpPath(objApproval.Region, objApproval.Domain) + "approvals/" + objApproval.Guid + "/";
                    }
                    else
                    {
                        httpPath = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + objApproval.Domain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(objApproval.Region) + "/approvals/" + objApproval.Guid + "/";
                    }

                    return httpPath;
                }
                return string.Empty;
            }
            finally
            {
            }
        }

        public static ProcessingStatus GetApprovalStatusByProgress(int progress)
        {
            if (progress < 0)
            {
                return ProcessingStatus.Error;
            }
            else if (progress == 0)
            {
                return ProcessingStatus.Queued;
            }
            else if (progress < 100)
            {
                return ProcessingStatus.Processing;
            }
            else
            {
                return ProcessingStatus.Success;
            }
        }

        public static string GetApprovalHtml5ViewURL(Approval objApproval, User objRecipientUser,
            ExternalCollaborator objRecipientExUser, SharedApproval objSharedApproval, bool isExternalCollaborator, int annotationID)
        {
            if (isExternalCollaborator)
            {
                return GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +
                       (objApproval.Job1.Account1.IsCustomDomainActive
                           ? objApproval.Job1.Account1.CustomDomain
                           : objApproval.Job1.Account1.Domain) + "/Auth/Redirect?adhoc=1" +
                            "&exgousg=" + objRecipientExUser.Guid + "&shp=" + objSharedApproval.ID +
                           "&apg=" + objApproval.Guid + "&aid=" + annotationID;

            }
            else
            {
                return GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +
                       (objApproval.Job1.Account1.IsCustomDomainActive
                           ? objApproval.Job1.Account1.CustomDomain
                           : objApproval.Job1.Account1.Domain) + "/Studio/HtmlViewer?apg=" +
                             objApproval.Guid + "&aid=" + annotationID; 
            }
        }

        #region App Studio methods

        public static List<ReturnApprovalsPageView> GetApprovals(bool hideCompletedApprovals, int account, int user,
            int topLinkId, int maxRows, int set, int searchField, bool order, string search, int statuses, bool viewAll,
            out int count, GMGColorContext context, GMGColorDAL.CustomModels.AdvancedSearch advS, string timeZone, string datePattern, string selectedApprovalTypeIds, string selectedTagwordIds)

        {
            var lstApprovalsPageView = new List<ReturnApprovalsPageView>();
            if (advS != null)
            {
                bool? isOverDue = (advS.AdvSIsOverdue || advS.AdvSInProgress) ? advS.AdvSIsOverdue : (bool?)null;
                DateTime? deadlineMin = DateTime.ParseExact(advS.AdvSTimeFrameMin, datePattern, null); //Convert.ToDateTime(advS.AdvSTimeFrameMin);
                if (deadlineMin != null)
                {
                    deadlineMin = GMGColorFormatData.SetUserTimeToUTC(deadlineMin.GetValueOrDefault(), timeZone);
                }
                DateTime? deadlineMax = DateTime.ParseExact(advS.AdvSTimeFrameMax, datePattern, null); //Convert.ToDateTime(advS.AdvSTimeFrameMax);
                if (deadlineMax != null)
                {
                    deadlineMax = deadlineMax.GetValueOrDefault().AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(10);// make it to the end of the day
                    deadlineMax = GMGColorFormatData.SetUserTimeToUTC(deadlineMax.GetValueOrDefault(), timeZone);
                }
                if (!advS.AdvSWithinATimeframe)
                {
                    deadlineMin = null;
                    deadlineMax = null;
                }
                string groups = ConvertAdvancedSearchSelected(advS.AdvSSelectedGroups);
                string internalUsers = ConvertAdvancedSearchSelected(advS.AdvSSelectedUsers.Where(x => x > 0).ToList());
                string externalUsers = ConvertAdvancedSearchSelected(advS.AdvSSelectedUsers.Where(x => x < 0).Select(x => -x).ToList());
                string workflows = ConvertAdvancedSearchSelected(advS.AdvSSelectedWorkflows);
                string phases = ConvertAdvancedSearchSelected(advS.AdvSSelectedPhases);

                lstApprovalsPageView = Approval.GetApprovals(hideCompletedApprovals, account, user, topLinkId, maxRows, set, searchField, order, search, statuses, viewAll, out count, context, selectedApprovalTypeIds, selectedTagwordIds,
                    isOverDue, deadlineMin, deadlineMax, groups, internalUsers, externalUsers, workflows, phases);
            }
            else
            {
                lstApprovalsPageView = Approval.GetApprovals(hideCompletedApprovals, account, user, topLinkId, maxRows, set, searchField, order, search, statuses, viewAll, out count, context, selectedApprovalTypeIds, selectedTagwordIds);
            }
            return lstApprovalsPageView;
        }


        public static List<ReturnApprovalsPageView> GetApprovals(bool hideCompletedApprovals, int account, int user, int topLinkId, int maxRows, int set, int searchField, bool order, string search, int statuses, bool viewAll, out int count, GMGColorContext context, string selectedApprovalTypeIds, string selectedTagwordIds,
                bool? isOverdue = null, DateTime? deadlineMin = null, DateTime? deadlineMax = null, string sharedWithGroups = null, string sharedWithInternalUsers = null, string sharedWithExternalUsers = null, string byWorkflow = null, string byPhase = null)
        {
            try
            {
                ObjectParameter recCount = new ObjectParameter("P_RecCount", typeof(int));
                context.ReturnApprovalsPageViews.AsNoTracking();

                var watch = System.Diagnostics.Stopwatch.StartNew();
                
                List<ReturnApprovalsPageView> lst = context.GetApprovals(account, user, topLinkId, maxRows, set, searchField, order, search, statuses, viewAll, hideCompletedApprovals,isOverdue, deadlineMin, deadlineMax, sharedWithGroups,
                    sharedWithInternalUsers, sharedWithExternalUsers, byWorkflow, byPhase, recCount,selectedApprovalTypeIds, selectedTagwordIds).ToList();

                GMGColorLogging.log.WarnFormat("GetApprovals SPC_ReturnApprovalsPageInfo method execution took: {0}", watch.ElapsedMilliseconds);


                count = (int)recCount.Value;
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error occured in GetApprovals(): Account - {0}, User - {1}, MaxRows - {2}, TopLinkId - {3}", account, user, maxRows, topLinkId), ex);
            }
            finally
            {
            }
        }

        public static List<ReturnApprovalInTreeView> GetApprovalsInTree(string userKey, int folderId, int pageNr, int approvalTypeToExclude, GMGColorContext context)
        {
            try
            {
                context.ReturnApprovalInTreeViews.AsNoTracking();
                List<ReturnApprovalInTreeView> lst = context.GetApprovalsInTree(userKey, folderId, approvalTypeToExclude, pageNr, Constants.NumberOfApprovalsPerPageInProofStudio).ToList(); //context.GetApprovalsInTree(userKey, folderId, pageNr, approvalTypeToExclude).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetApprovalsInTree()", ex);
            }
        }

        public static List<ReturnApprovalsPageView> GetRecentViwedApprovals(bool hideCompletedApprovals, int account, int user, int maxRows, int set, int searchField, bool order, string search, int statuses, out int count, GMGColorContext context, string selectedApprovalTypeIds, string selectedTagwordIds)
        {
            try
            {
                System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof(int));
                List<ReturnApprovalsPageView> lst = context.GetRecentViwedApprovals(account, user, maxRows, set, searchField, order, search, statuses, hideCompletedApprovals, recCount, selectedApprovalTypeIds, selectedTagwordIds).ToList();
                count = (int)recCount.Value;
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error occured in GetRecentViwedApprovals(): Account - {0}, User - {1}, MaxRows - {2}", account, user, maxRows), ex);
            }
            finally
            {
            }
        }

        public static List<ReturnApprovalsPageView> GetFoldersApprovals(int account, int user, int folderId, int maxRows, int set, int searchField, bool order, string search, int statuses, out int count, GMGColorContext context, bool viewAll)
        {
            try
            {
                System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof(int));
                List<ReturnApprovalsPageView> lst = context.GetFoldersApprovals(account, user, folderId, maxRows, set, searchField, order, search, statuses, viewAll,recCount).ToList();
                count = (int)recCount.Value;
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error occured in GetFoldersApprovals(): Account - {0}, User - {1}, MaxRows - {2}, FolderId - {3}, SearchField - {4}, Search - {5}, Statuses - {6}", account, user, maxRows, folderId, searchField, search, statuses), ex);
            }
            finally
            {
            }
        }

        public static List<ReturnApprovalsPageView> GetRecycleBin(int account, int user, int maxRows, int set, int searchField, bool order, string search, bool viewAll, out int count, GMGColorContext context)
        {
            try
            {
                System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof(int));
                List<ReturnApprovalsPageView> lst = context.GetRecycleBin(account, user, maxRows, set, searchField, order, viewAll, search, recCount).ToList();
                count = (int)recCount.Value;
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Error occured in GetRecycleBin(): Account - {0}, User - {1}, MaxRows - {2}", account, user, maxRows), ex);
            }
            finally
            {
            }
        }

        public static List<ReturnApprovalsPageView> GetArchived(int account, int user, int maxRows, int set, int searchField, bool order, bool viewAll, string search, out int count, GMGColorContext context)
        {
            try
            {
                System.Data.Entity.Core.Objects.ObjectParameter recCount = new System.Data.Entity.Core.Objects.ObjectParameter("P_RecCount", typeof(int));
                List<ReturnApprovalsPageView> lst = context.GetArchivedApprovals(account, user, maxRows, set, searchField, order,  search, viewAll, recCount).ToList();
                count = (int)recCount.Value;
                return lst;
            }
            catch (Exception ex)
            {
                
                throw new Exception(String.Format("Error occured in GetArchived() : Account - {0}, User - {1}, MaxRows - {2}", account, user, maxRows), ex);
            }
            finally
            {
            }
        }

        public static List<ApprovalCountsView> ReCallGetApprovalCounts(bool hideCompletedApprovals, int account, int user, bool viewAll, bool viewAllFromRecycle, string userRoleKey, GMGColorContext context, string timeZone, string datePattern, CustomModels.AdvancedSearch advS = null)
        {
            try
            {
                if (advS != null)
                {
                    bool? isOverDue = (advS.AdvSIsOverdue || advS.AdvSInProgress) ? advS.AdvSIsOverdue : (bool?)null;
                    DateTime? deadlineMin = DateTime.ParseExact(advS.AdvSTimeFrameMin, datePattern, null);
                    if (deadlineMin != null)
                    {
                        deadlineMin = GMGColorFormatData.SetUserTimeToUTC(deadlineMin.GetValueOrDefault(), timeZone);
                    }
                    DateTime? deadlineMax = DateTime.ParseExact(advS.AdvSTimeFrameMax, datePattern, null);
                    if (deadlineMax != null)
                    {
                        deadlineMax = deadlineMax.GetValueOrDefault().AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(10);
                        deadlineMax = GMGColorFormatData.SetUserTimeToUTC(deadlineMax.GetValueOrDefault(), timeZone);
                    }
                    if (!advS.AdvSWithinATimeframe)
                    {
                        deadlineMin = null;
                        deadlineMax = null;
                    }
                    string groups = ConvertAdvancedSearchSelected(advS.AdvSSelectedGroups);
                    string internalUsers = ConvertAdvancedSearchSelected(advS.AdvSSelectedUsers.Where(x => x > 0).ToList());
                    string externalUsers = ConvertAdvancedSearchSelected(advS.AdvSSelectedUsers.Where(x => x < 0).Select(x => -x).ToList());
                    string workflows = ConvertAdvancedSearchSelected(advS.AdvSSelectedWorkflows);
                    string phases = ConvertAdvancedSearchSelected(advS.AdvSSelectedPhases);

                    var result = context.GetApprovalCounts(account, user, viewAll, viewAllFromRecycle, userRoleKey, hideCompletedApprovals, isOverDue, deadlineMin, deadlineMax, groups, internalUsers, externalUsers, workflows, phases).ToList();
                    return result;
                }
                else
                {
                    var result = context.GetApprovalCounts(account, user, viewAll, viewAllFromRecycle, userRoleKey, hideCompletedApprovals, null, null, null, null, null, null, null, null).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GetDecisionMakers(int currentPhase, int approval, int? primaryDecisionMaker, int? p_ExternalPrimaryDecisionMaker, bool p_IsLoadAll , GMGColorContext context)
        {
            try
            {
                var result = context.GetApprovalDecisionMakers(currentPhase, approval, primaryDecisionMaker, p_ExternalPrimaryDecisionMaker, p_IsLoadAll).ToList()[0].RetVal; 
                return result;
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetDecisionMakers()", ex);
            }
        }

        public static List<ApprovalCountsView> GetApprovalCounts(bool hideCompletedApprovals, int account, int user, bool viewAll, bool viewAllFromRecycle, string userRoleKey, GMGColorContext context, string timeZone, string datePattern, CustomModels.AdvancedSearch advS = null)
        {
            try
            {
                if (advS != null)
                {
                    bool? isOverDue = (advS.AdvSIsOverdue || advS.AdvSInProgress) ? advS.AdvSIsOverdue : (bool?)null;
                    DateTime? deadlineMin = DateTime.ParseExact(advS.AdvSTimeFrameMin, datePattern, null);
                    if (deadlineMin != null)
                    {
                        deadlineMin = GMGColorFormatData.SetUserTimeToUTC(deadlineMin.GetValueOrDefault(), timeZone);
                    }
                    DateTime? deadlineMax = DateTime.ParseExact(advS.AdvSTimeFrameMax, datePattern, null);
                    if (deadlineMax != null)
                    {
                        deadlineMax = deadlineMax.GetValueOrDefault().AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(10);// make it to the end of the day
                        deadlineMax = GMGColorFormatData.SetUserTimeToUTC(deadlineMax.GetValueOrDefault(), timeZone);
                    }
                    if (!advS.AdvSWithinATimeframe)
                    {
                        deadlineMin = null;
                        deadlineMax = null;
                    }
                    string groups = ConvertAdvancedSearchSelected(advS.AdvSSelectedGroups);
                    string internalUsers = ConvertAdvancedSearchSelected(advS.AdvSSelectedUsers.Where(x => x > 0).ToList());
                    string externalUsers = ConvertAdvancedSearchSelected(advS.AdvSSelectedUsers.Where(x => x < 0).Select(x => -x).ToList());
                    string workflows = ConvertAdvancedSearchSelected(advS.AdvSSelectedWorkflows);
                    string phases = ConvertAdvancedSearchSelected(advS.AdvSSelectedPhases);

                    var watch = System.Diagnostics.Stopwatch.StartNew();

                    var approvalCountsdatas = context.GetApprovalCounts(account, user, viewAll, viewAllFromRecycle, userRoleKey, hideCompletedApprovals, isOverDue, deadlineMin, deadlineMax, groups, internalUsers, externalUsers, workflows, phases).ToList();

                    GMGColorLogging.log.WarnFormat("GetApprovalCounts advS is not null method execution took: {0}", watch.ElapsedMilliseconds);

                    return approvalCountsdatas;
                   
                }
                var watch1 = System.Diagnostics.Stopwatch.StartNew();

                var approvalCountsdata = context.GetApprovalCounts(account, user, viewAll, viewAllFromRecycle, userRoleKey, hideCompletedApprovals, null, null, null, null, null,null, null, null).ToList();

                GMGColorLogging.log.WarnFormat("GetApprovalCounts method execution took: {0}", watch1.ElapsedMilliseconds);

                return approvalCountsdata;
            }
            catch (Exception ex)
            {
                for (int i = 0; i < 2; i++)
                {
                    System.Threading.Thread.Sleep(1500);
                    var watch = System.Diagnostics.Stopwatch.StartNew();

                    var result = ReCallGetApprovalCounts(hideCompletedApprovals, account, user, viewAll, viewAllFromRecycle, userRoleKey, context, timeZone, datePattern, advS);

                    GMGColorLogging.log.WarnFormat("ReCallGetApprovalCounts method execution took: {0}", watch.ElapsedMilliseconds);

                    if (result != null)
                    {
                        return result;
                    }
                }
                throw new Exception(String.Format("Error occured in GetApprovalCounts() : Account - {0}, User - {1}, ViewAll - {2}, ViewAllFromRecycle - {3}, UserRoleKey - {4}, hideCompletedApprovals - {5}, IsAdvancedSearch - {6}", account, user, viewAll, viewAllFromRecycle, userRoleKey, hideCompletedApprovals, advS != null), ex);
            }
            finally
            {
            }
        }

        public static void MarkApprovalForPermanentDeletion(int approvalId, GMGColorContext context)
        {
            var approval = (context.Approvals.FirstOrDefault(a => a.ID == approvalId));
            approval.DeletePermanently = true;
            approval.ModifiedDate = DateTime.UtcNow;
            approval.Version = 0;

        }

        public static void MarkApprovalForPermanentDeletion(Approval approval, int userID,  GMGColorContext context)
        {
            var RecycleBinApproval = (context.ApprovalUserRecycleBinHistories.FirstOrDefault(a => a.Approval == approval.ID));
            if (RecycleBinApproval != null)
            {
                RecycleBinApproval.User = userID;
            }
            approval.ModifiedDate = DateTime.UtcNow;
            approval.Modifier = userID;
            approval.DeletePermanently = true;
            approval.Version = 0; // set version tp 0 to ignore it in case new versions are submitted before it will be completly removed from the system
        }

        public static string DeleteApproval(int approvalId, GMGColorContext context)
        {
            try
            {
                return context.RemoveApproval(approvalId).ToList()[0].RetVal;
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in DeleteApproval()", ex);
            }
            finally
            {
            }
        }

        #endregion

        public static string GetStatusStyleName(int intStatus)
        {
            switch (intStatus)
            {
                case (int)Approval.ApprovalDecision.Pending:
                    return "PDG";
                case (int)Approval.ApprovalDecision.ChangesRequired:
                    return "CHR";
                case (int)Approval.ApprovalDecision.ApprovedWithChanges:
                    return "AWC";
                case (int)Approval.ApprovalDecision.Approved:
                    return "APD";
                case (int)Approval.ApprovalDecision.NotApplicable:
                    return "NAP";
                default:
                    return string.Empty;
            }
        }

        public static string ConvertImagePath(string strFilename)
        {
            if (strFilename.IndexOf("data") > -1)
            {
                string firstPass = strFilename.Substring(strFilename.IndexOf("data")).Replace("\\", "/");
                if (firstPass.IndexOf("/") > -1)
                    return firstPass = "../data" + firstPass.Substring(firstPass.IndexOf("/"));
            }

            return String.Empty;
        }
        
        public static int GetAddedApprovalsCount(int accountId,  DateTime startingDate, GMGColorContext context)
        {
            try
            {
                // make sure you have the UTC time in starting time
                startingDate = startingDate.ToUniversalTime().Date;
                DateTime endingDate = startingDate.AddMonths(1).Date;

                //this query returns the proofs count of the approvals (except permanent deleted with NULL value for job field), which is calculated as follow: Sum of ( Ceiling((approvals grouped by job id) / 4))
                var approvalsProofsCount = (from o in context.Approvals
                                            join j in context.Jobs on o.Job equals j.ID
                                            where
                                                j.Account == accountId &&
                                                (o.CreatedDate >= startingDate) &&
                                                (o.CreatedDate <= endingDate)
                                            select new { ID = o.ID, Job = o.Job }).Concat(from a in context.ApprovalJobDeleteHistories
                                                                                          where a.Account == accountId &&
                                                                                                  (a.CreatedDate >= startingDate) &&
                                                                                                  (a.CreatedDate <= endingDate) &&
                                                                                                  a.Job != null
                                                                                          select new { ID = a.ID, Job = (int)a.Job })
                                                                                          .GroupBy(v => v.Job)
                                                                                          .ToList()
                                                                                          .Sum(s => Convert.ToInt32(Math.Ceiling((double)s.Count() / 4)));
               
                //this query returns the proofs count of the deleted approvals, which is calculated as follow: Sum of ( Ceiling((deleted approvals grouped by name) / 4))
                var deletedApprovalsProofsCountWihoutJobId = (from a in context.ApprovalJobDeleteHistories
                                                              where a.Account == accountId &&
                                                                   (a.CreatedDate >= startingDate) &&
                                                                   (a.CreatedDate <= endingDate) &&
                                                                    a.Job == null
                                                              select new { a.ID, a.JobName }).GroupBy(a => a.JobName)
                                                              .ToList()
                                                              .Sum(s => Convert.ToInt32(Math.Ceiling((double)s.Count() / 4)));

                return approvalsProofsCount +  deletedApprovalsProofsCountWihoutJobId;
            }
            finally
            {
            }
        }

        public int DocumentPagesCount(GMGColorContext context)
        {
            return (from ap in context.ApprovalPages where ap.Approval == ID select ap.ID).Count();
        }

        #endregion

        public class ApprovalProcessingStatus
        {
            public ProcessingStatus Status;
            public decimal Progress;
            public string ProgressText;
        }

        public class DuplicatedFileNamesJobs
        {
            public string FileName { get; set; }
            public int JobID { get; set; }
        }

        public static Approval GetApproval(int approvalId, int userId, int accountId, bool isAdministrator, GMGColorContext context)
        {
            try
            {
                return (
                    isAdministrator
                        ? (from av in context.Approvals
                            join j in context.Jobs on av.Job equals j.ID
                            where
                                j.Account == accountId && 
                                av.ID == approvalId &&
                                !av.DeletePermanently
                            select av)
                            .FirstOrDefault()
                        : (from av in context.Approvals
                            join ac in context.ApprovalCollaborators on av.ID equals ac.Approval
                            join j in context.Jobs on av.Job equals j.ID
                            where
                                j.Account == accountId && ac.Collaborator == userId &&
                                av.ID == approvalId && !av.DeletePermanently
                            select av
                            ).FirstOrDefault()
                    );
            }
            finally
            {
            }
        }

        public enum CollaborateValidation
        {
            ApprovalAccess,
            ApprovalAnnotationReport,
            DeleteApproval,
            EditFolder,
            DownloadFolder,
            FolderAccess,
            MoveFolder,
            FolderAnnotationReport,
            DeleteFolder,
            MoveApprovals,
            RestoreArchivedApproval,
            DeleteProjectFolder,
            ArchiveProjectFolder,
            UnArchiveProjectFolder,
            ViewProjectFolder,
            EditProjectFolder,
            EditMemberProjectFolder,
            DeleteSimulationProfile
        }

        public enum ApprovalOperation
        {
            Delete,
            ViewDetails,
            Download,
            AddNewVersion,
            Share,
            Access,
            Archive,
            Restore,
            DeletePermanent,
            DeleteVersion,
            ViewApproval,
            RenameApproval,
            LockApproval,
            PDM,
            RevokeAccess,
            SendJobToDeliver,
            SendJobToManage,
            GetProgress,
            Annotations
        }

        public static bool CanAccess(int userId, IEnumerable<int> approvalIdList, ApprovalOperation operation, GMGColorContext context)
        {
            try
            {
                bool result = false;
                bool hasRole =
                    (from u in context.Users
                        join ur in context.UserRoles on u.ID equals ur.User
                        join r in context.Roles on ur.Role equals r.ID
                        join am in context.AppModules on r.AppModule equals am.ID
                        where u.ID == userId &&
                            am.Key == 0 &&
                            r.Key != "VIE" &&
                            r.Key != "NON"
                        select u.ID).Any();

                if (!hasRole)
                    return false;

                switch (operation)
                {
                    case ApprovalOperation.ViewApproval:
                    case ApprovalOperation.RevokeAccess:
                    case ApprovalOperation.SendJobToDeliver:
                    case ApprovalOperation.SendJobToManage:
                    case ApprovalOperation.GetProgress:
                        {
                            result =
                                (from ac in context.ApprovalCollaborators
                                    join a in context.Approvals on ac.Approval equals a.ID
                                    where
                                        approvalIdList.Contains(a.ID) &&
                                        (ac.Collaborator == userId || a.Owner == userId) && !a.DeletePermanently && !a.IsDeleted
                                    select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.RenameApproval:
                    case ApprovalOperation.Delete:
                        {
                            // check if the user is the owner
                            result =
                                (from ac in context.ApprovalCollaborators
                                    join a in context.Approvals on ac.Approval equals a.ID
                                    where
                                        approvalIdList.Contains(a.ID) &&
                                        ac.Collaborator == userId &&
                                        a.IsDeleted == false &&
                                        a.Owner == userId
                                    select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.LockApproval:
                    case ApprovalOperation.Annotations:
                    case ApprovalOperation.PDM:
                    case ApprovalOperation.ViewDetails:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.IsDeleted == false &&
                                            js.Key != "ARC"
                                        select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.Download:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.IsDeleted == false &&
                                            js.Key != "ARC" &&
                                            a.AllowDownloadOriginal
                                        select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.Share:
                    case ApprovalOperation.Access:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.IsDeleted == false &&
                                            (js.Key != "ARC" && js.Key != "COM")
                                        select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.Archive:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.IsDeleted == false &&
                                            (js.Key == "COM")
                                        select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.Restore:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.Owner == userId &&
                                            a.IsDeleted &&
                                             (js.Key != "ARC" && js.Key != "COM")
                                        select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.DeletePermanent:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.Owner == userId &&
                                            (a.IsDeleted || js.Key == "ARC")
                                        select a.ID).Any();
                            break;
                        }
                    case ApprovalOperation.DeleteVersion:
                        {
                            result = (from ac in context.ApprovalCollaborators
                                        join a in context.Approvals on ac.Approval equals a.ID
                                        join j in context.Jobs on a.Job equals j.ID
                                        join js in context.JobStatus on j.Status equals js.ID
                                        where
                                            approvalIdList.Contains(a.ID) &&
                                            ac.Collaborator == userId &&
                                            a.Owner == userId &&
                                            a.IsDeleted == false &&
                                            js.Key != "ARC"
                                        select a.ID).Any();
                            break;
                        }
                }
                return result;
            }
            finally
            {
            }
        }

        public class ApprovalFolder
        {
            public int ApprovalId { get; set; }
            public string ApprovalGuid { get; set; }
            public string AccountRegion { get; set; }
            public int approvalOwner { get; set; }
        }

        public static List<Approval> GetExpiredApprovals(GMGColorContext context)
        {
            DateTime dateNow = DateTime.UtcNow;
            try
            {
                return (from a in context.Approvals
                        where a.IsDeleted &&
                        !a.DeletePermanently &&
                        DbFunctions.DiffDays(a.ModifiedDate, dateNow) >= 30
                        select a).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error occured in GetExpiredApprovals()", ex);
            }
            finally
            {
            }
        }

        public static List<string> GetListOfGuids(GMGColorContext context)
        {
            try
            {
                return (from a in context.Approvals select a.Guid).ToList();
            }
            finally
            {
            }
        }

        public string GetPrimaryDecisionMakerFullName(GMGColorContext context)
        {
            try
            {
                return (from a in context.Approvals
                        join u in context.Users on a.PrimaryDecisionMaker equals u.ID
                        where a.ID == ID
                        select u.GivenName + " " + u.FamilyName).FirstOrDefault();
            }
            finally
            {
            }
        }

        public static int? GetFolder(int approvalId, GMGColorContext context)
        {
            return
                (from a in context.Approvals
                 where a.ID == approvalId
                 select a.Folders.Any() ? (int?) a.Folders.FirstOrDefault().ID : null).FirstOrDefault();
        }

        public static string GetFolderName(int approvalId, GMGColorContext context)
        {
            return
                (from a in context.Approvals
                 where a.ID == approvalId
                 select a.Folders.Any() ? a.Folders.FirstOrDefault().Name : null).FirstOrDefault();
        }

        public static List<int> GetExternalCollaborators(int approvalID, GMGColorContext context)
        {
            List<int> externalCollaborators =
                (from sa in context.SharedApprovals
                 where sa.Approval == approvalID
                 select sa.ExternalCollaborator).ToList();

            return externalCollaborators;
        }

        public static Approval GetApproval(int approvalID, GMGColorContext context)
        {
            return (from ap in context.Approvals
                    where ap.ID == approvalID
                    select ap).FirstOrDefault();
        }

        public static bool IsApprovalLocked(int approvalId, GMGColorContext context)
        {
            return (from ap in context.Approvals
                    where ap.ID == approvalId
                    select ap.IsLocked).FirstOrDefault();
        }

        public class CollaboratorDecision
        {
            public string CollaboratorName { get; set; }
            public string ApprovalDecision { get; set; }
        }

        public class ApprovalDecisions
        {
            public int ApprovalId { get; set; }
            public List<CollaboratorDecision> Decisions { get; set; }
            public string PhaseStatus { get; set; }
            public string WorkflowName { get; set; }
            public string PhaseName { get; set; }
        }

        public class ApprovalsVisualIndicatorData
        {
            public int ApprovalId { get; set; }
            public List<string> redUsers { get; set; }
            public List<string> whiteUsers { get; set; }
            public List<string> greenUsers { get; set; }
            public List<string> greyUsers { get; set; }
            public List<string> orangeUsers { get; set; }

            public ApprovalsVisualIndicatorData()
            {
                redUsers = new List<string>();
                whiteUsers = new List<string>();
                greenUsers = new List<string>();
                greyUsers = new List<string>();
                orangeUsers = new List<string>();
            }
        }

        public class ApprovalRetoucher
        {
            public int ApprovalId { get; set; }
            public List<string> Retouchers { get; set; } 
        }

        public class ApprovalNotViewedAnnotations
        {
            public int ApprovalId { get; set; }
            public int NotViewed { get; set; }
        }

        public class NewApprovalUploadInfo
        {
            public int AccountId { get; set; }
            public int DuplicateFileKey { get; set; }
            public bool CanApplySettings { get; set; }
        }

        public class ApprovalFileStatusInfo
        {
            public string FileName { get; set; }
            public string FileStatus { get; set; }
            public string FileStatusLabel { get; set; }
        }

        public class ApprovalPageCountInfo
        {
            public int ID { get; set; }
            public int PageCount { get; set; }
        }

        #region Private methods

        private static string ConvertAdvancedSearchSelected(List<int> selectedItems)
        {
            if (selectedItems.Count == 0)
                return null;
            else
            {
                return string.Join(",", selectedItems.ToArray());
            }
        }
        private static string ConvertAdvancedSearchSelected(List<string> selectedItems)
        {
            if (selectedItems.Count == 0)
                return null;
            else
            {
                return string.Join(",", selectedItems.ToArray());
            }
        }
        #endregion

        
    }
}

