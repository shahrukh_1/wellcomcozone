using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL
{
    public partial class JobStatu : BusinessObjectBase
    {
        #region Enums

        public enum Status
        {
            /// <summary>
            /// Key = NEW
            /// </summary>
            New = 1,
            /// <summary>
            /// Key = CRQ
            /// </summary>
            ChangesRequired = 2,
            /// <summary>
            /// Key = CCM
            /// </summary>
            ChangesComplete = 4,
            /// <summary>
            /// Key = COM
            /// </summary>
            Completed = 8,
            /// <summary>
            /// Key = ARC
            /// </summary>
            Archived = 16,
            /// <summary>
            /// Key = ERR
            /// </summary>
            ERROR
        };

        public enum ApprovalDecision
        {
            Pending =1,
            ChangesRequired,
            ApprovedWithChanges,
            Approved,
            NotApplicable

        }

        #endregion

      

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Return Job Status of the given approval id
        /// </summary>
        /// <param name="jobStatusId">Aapproval ID</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static Status GetJobStatus(int jobStatusId, GMGColorContext context)
        {
            try
            {
                string key = (from js in context.JobStatus where js.ID == jobStatusId select js.Key).FirstOrDefault();
                return JobStatu.GetJobStatus(key);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static Status GetJobStatus(string key)
        {
            switch (key)
            {
                case "NEW": return Status.New;
                case "CRQ": return Status.ChangesRequired;
                case "CCM": return Status.ChangesComplete;
                case "COM": return Status.Completed;
                case "ARC": return Status.Archived;
                case "ERR": return Status.ERROR;
                default: return Status.ERROR;
            }
        }

        /// <summary>
        /// Return JobStatu of the given Job Status
        /// </summary>
        /// <param name="status">Job Status</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static JobStatu GetJobStatus(Status status, GMGColorContext context)
        {
            try
            {
                return (from js in context.JobStatus
                        where
                            (
                            (status == Status.New && js.Key == "NEW") ||
                            (status == Status.ChangesRequired && js.Key == "CRQ") ||
                            (status == Status.ChangesComplete && js.Key == "CCM") ||
                            (status == Status.Completed && js.Key == "COM") ||
                            (status == Status.Archived && js.Key == "ARC")
                            )
                        select js).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Return JobStatu of the given Job Status
        /// </summary>
        /// <param name="status">Job Status</param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static int GetJobStatusId(Status status, GMGColorContext context)
        {
            try
            {
                return (from js in context.JobStatus
                        where
                            (
                            (status == Status.New && js.Key == "NEW") ||
                            (status == Status.ChangesRequired && js.Key == "CRQ") ||
                            (status == Status.ChangesComplete && js.Key == "CCM") ||
                            (status == Status.Completed && js.Key == "COM") ||
                            (status == Status.Archived && js.Key == "ARC")
                            )
                        select js.ID).FirstOrDefault();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Returns Localized approval status name of the given approval id.
        /// </summary>
        /// <param name="jobStatusId"> </param>
        /// <param name="context"> </param>
        /// <returns></returns>
        public static string GetLocalizedJobStatus(int jobStatusId, GMGColorContext context)
        {
            try
            {
                string key = (from js in context.JobStatus where js.ID == jobStatusId select js.Key).FirstOrDefault();
                return JobStatu.GetLocalizedJobStatus(GetJobStatus(key));
            }
            finally
            {
            }
        }

         public static string GetLocalizedJobStatus(Status status)
        {
            switch (status)
            {
                case Status.New: return Resources.lblNew;
                case Status.ChangesRequired: return Resources.lblChangesRequired;
                case Status.ChangesComplete: return Resources.lblChangesComplete;
                case Status.Completed: return Resources.lblCompleted;
                case Status.Archived: return Resources.lblArchive_PastTense;
                case Status.ERROR: return Resources.lblError;
                default: return string.Empty;
            }
        }

        public static string GetLocalizedJobStatus(Status status, bool isRetoucherWorkflow)
        {
            switch (status)
            {
                case Status.New: return Resources.lblNew;
                case Status.ChangesRequired:
                    {
                        if (isRetoucherWorkflow)
                            return Resources.lblChangesRequired;
                        else
                            return Resources.lblInProgress;
                    };
                case Status.ChangesComplete: return Resources.lblChangesComplete;
                case Status.Completed: return Resources.lblCompleted;
                case Status.Archived: return Resources.lblArchive_PastTense;
                case Status.ERROR: return Resources.lblError;
                default: return string.Empty;
            }
        }

        #endregion
    }
}

