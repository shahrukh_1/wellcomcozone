using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class PageBox : BusinessObjectBase
    {
        #region Enums

        public enum PageBoxName
        {
            /// <summary>
            /// Key = CRP
            /// </summary>
            CROPBOX,
            /// <summary>
            /// Key = TRM
            /// </summary>
            TRIMBOX,
            /// <summary>
            /// Key = BLD
            /// </summary>
            BLEEDBOX
        }

        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        #region Extension Methods

        private static PageBoxName GetPageBox(string key)
        {
            switch (key)
            {
                case "CRP": return PageBoxName.CROPBOX;
                case "TRM": return PageBoxName.TRIMBOX;
                default: return PageBoxName.BLEEDBOX;
            }
        }

        public static PageBoxName GetPageBox(int pageBox, GMGColorContext context)
        {
            try
            {
                string key = (from pb in context.PageBoxes where pb.ID == pageBox select pb.Key).FirstOrDefault();
                return PageBox.GetPageBox(key);
            }
            finally
            {
            }
        }

        public static PageBox GetPageBox(PageBoxName name, GMGColorContext context)
        {
            try
            {
                return (from pb in context.PageBoxes
                        where
                            (
                                (name == PageBoxName.CROPBOX && pb.Key == "CRP") ||
                                (name == PageBoxName.TRIMBOX && pb.Key == "TRM")
                            )
                        select pb).FirstOrDefault() ??
                       (from pb in context.PageBoxes where pb.Key == "BLD" select pb).FirstOrDefault();
            }
            finally
            {
            }
        }

        #endregion
    }
}

