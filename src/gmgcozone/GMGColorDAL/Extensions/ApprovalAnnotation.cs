using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GMGColorDAL
{
    public partial class ApprovalAnnotation : BusinessObjectBase
    {
        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion
        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        public static ApprovalAnnotation GetObject(int approvalAnnotationId, GMGColorContext context)
        {
            try
            {
                return (from aa in context.ApprovalAnnotations where aa.ID == approvalAnnotationId select aa).FirstOrDefault();
            }
            finally
            {
            }
        }
    
        public List<ApprovalAnnotation> GetChilds(GMGColorContext context)
        {
            try
            {
                return (from aa in context.ApprovalAnnotations where aa.Parent == ID select aa).ToList();
            }
            finally
            {
            }
        }

        public static string GetAccountRegion(int annotationID, GMGColorContext context)
        {
            return (from ann in context.ApprovalAnnotations
                    join appage in context.ApprovalPages on ann.Page equals appage.ID
                    join ap in context.Approvals on appage.Approval equals ap.ID
                    join us in context.Users on ap.Owner equals us.ID
                    join acc in context.Accounts on us.Account equals acc.ID
                    where ann.ID == annotationID
                    select acc.Region).FirstOrDefault();
        }

        #endregion
    }
}

