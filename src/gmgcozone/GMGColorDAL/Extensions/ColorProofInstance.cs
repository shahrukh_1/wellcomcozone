using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;


namespace GMGColorDAL
{
    public partial class ColorProofInstance : BusinessObjectBase
    {
        #region Constructors
        #endregion

        #region OnPropertyChange Methods
        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here
        
        #endregion

        #region Properties
        public bool IsFromCurrentAccount { get; set; }

        public string SharingCompany { get; set; }
        #endregion

        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc

        /// <summary>
        /// Determines whether [is co zone username and password unique] [the specified user name].
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <returns>
        ///   <c>true</c> if [is co zone username and password unique] [the specified user name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCoZoneUsernameUnique(string userName, GMGColorContext context)
        {
            return IsCoZoneUsernameUnique(userName, null, context);
        }

        /// <summary>
        /// Checks if the pairing code is unique throughout the system
        /// </summary>
        /// <param name="pairingCode"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsPairingCodeUnique(string pairingCode, GMGColorContext context)
        {
            return !(from cp in context.ColorProofInstances where cp.PairingCode == pairingCode select cp).Any();
        }

        /// <summary>
        /// Gets the encrypted password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetEncryptedPassword(string password, GMGColorContext context)
        {
            var passw = (from p in context.GetColorProofInstanceEncryptedPassword(password) select p).FirstOrDefault();
            return passw != null ? passw.RetVal : string.Empty;
        }


        /// <summary>
        /// Gets the encrypted password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static string GetDecryptedPassword(string encryptedPassword, GMGColorContext context)
        {
            var passw = (from p in context.GetColorProofInstanceDecryptedPassword(encryptedPassword) select p).FirstOrDefault();
            return passw != null ? passw.RetVal : string.Empty;
        }

        /// <summary>
        /// Logins the specified ColorProof Instance.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static ColorProofInstance Login(string username, string password, GMGColorContext context)
        {
            return (from u in context.GetColorProofInstanceLogin(username, password) select u).FirstOrDefault();
        }

        /// <summary>
        /// Logins the specified ColorProof Instance.
        /// </summary>
        /// <param name="pairingCode">The pairing code for the specified instance.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static ColorProofInstance Login(string pairingCode, GMGColorContext context)
        {
            return (from u in context.GetColorProofInstanceLoginByPairingCode(pairingCode) select u).FirstOrDefault();
        }

        /// <summary>
        /// Returns whether or not the username is unique in the system
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="id">The id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is co zone username unique] [the specified user name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCoZoneUsernameUnique(string userName, int? id, GMGColorContext context)
        {
            if (userName != null)
            {
                return !(from cpi in context.ColorProofInstances
                        where cpi.UserName.ToLower() == userName.ToLower() && cpi.ID != id
                        select cpi.ID).Any();
            }
            return true;
        }

        /// <summary>
        /// Determines whether [is co zone name unique] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static bool IsCoZoneNameUnique(string name, GMGColorContext context)
        {
            return IsCoZoneNameUnique(name, null, context);
        }

        /// <summary>
        /// Determines whether [is co zone name unique] [the specified name].
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The id.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        ///   <c>true</c> if [is co zone name unique] [the specified name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCoZoneNameUnique(string name, int? id, GMGColorContext context)
        {
            return id.HasValue
                       ? !(from cpi in context.ColorProofInstances
                           where
                               cpi.SystemName == name.TrimStart().TrimEnd() &&
                               cpi.ID != id.Value &&
                               cpi.IsDeleted == false
                           select cpi.ID).Any()
                       : !(from cpi in context.ColorProofInstances
                           where
                               cpi.SystemName == name.TrimStart().TrimEnd() &&
                               cpi.IsDeleted == false
                           select cpi).Any();
        }

        /// <summary>
        /// Gets the list instances.
        /// </summary>
        /// <param name="isDeleted">if set to <c>true</c> [is deleted].</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="searchSystemName">Name of the search system.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static List<ColorProofInstance> GetList(bool isDeleted, int accountId, GMGColorContext context, string searchSystemName = null)
        {
            try
            {
                var instances = new List<ColorProofInstance>();
                instances = !string.IsNullOrEmpty(searchSystemName)
                                ? (from cpi in context.ColorProofInstances
                                   join u in context.Users on cpi.Owner equals u.ID
                                   join ac in context.Accounts on u.Account equals ac.ID
                                   join sc in context.Companies on ac.ID equals sc.Account
                                   where
                                       cpi.IsDeleted == isDeleted &&
                                       cpi.SystemName.ToLower().Contains(searchSystemName.ToLower()) &&
                                       u.Account == accountId
                                   select new
                                       {
                                           SystemName = cpi.SystemName,
                                           Address = cpi.Address,
                                           AdminName = cpi.AdminName,
                                           ComputerName = cpi.ComputerName,
                                           ColorProofWorkflows = cpi.ColorProofWorkflows,
                                           ColorProofInstanceState = cpi.ColorProofInstanceState,                                          
                                           Email = cpi.Email,
                                           Guid = cpi.Guid,
                                           IsDeleted = cpi.IsDeleted,
                                           IsOnline = cpi.IsOnline,
                                           LastSeen = cpi.LastSeen,
                                           Owner = cpi.Owner,
                                           PairingCode = cpi.PairingCode,
                                           PairingCodeExpirationDate = cpi.PairingCodeExpirationDate,
                                           Password = cpi.Password,
                                           SerialNumber = cpi.SerialNumber,
                                           State = cpi.State,
                                           User = cpi.User,
                                           UserName = cpi.UserName,
                                           Version = cpi.Version,
                                           WorkflowsLastModifiedTimestamp = cpi.WorkflowsLastModifiedTimestamp,
                                           IsFromCurrentAccount = (ac.ID == accountId),
                                           ID = cpi.ID,
                                           SharingCompany = sc.Name
                                       }).ToList()
                                         .Union
                                      (from scpw in context.SharedColorProofWorkflows
                                       join u in context.Users on scpw.User equals u.ID
                                       join cpcw in context.ColorProofCoZoneWorkflows on scpw.ColorProofCoZoneWorkflow
                                           equals cpcw.ID
                                       join cpw in context.ColorProofWorkflows on cpcw.ColorProofWorkflow equals cpw.ID
                                       join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                                       join cpuo in context.Users on cpi.Owner equals cpuo.ID
                                       join ac in context.Accounts on cpuo.Account equals ac.ID
                                       join sc in context.Companies on ac.ID equals sc.Account
                                       where
                                           u.Account == accountId &&
                                           cpi.SystemName.ToLower().Contains(searchSystemName.ToLower()) &&
                                           scpw.IsAccepted == true && scpw.IsEnabled
                                       select new
                                           {
                                               SystemName = cpi.SystemName,
                                               Address = cpi.Address,
                                               AdminName = cpi.AdminName,
                                               ComputerName = cpi.ComputerName,
                                               ColorProofWorkflows = cpi.ColorProofWorkflows,
                                               ColorProofInstanceState = cpi.ColorProofInstanceState,                                              
                                               Email = cpi.Email,
                                               Guid = cpi.Guid,
                                               IsDeleted = cpi.IsDeleted,
                                               IsOnline = cpi.IsOnline,
                                               LastSeen = cpi.LastSeen,
                                               Owner = cpi.Owner,
                                               PairingCode = cpi.PairingCode,
                                               PairingCodeExpirationDate = cpi.PairingCodeExpirationDate,
                                               Password = cpi.Password,
                                               SerialNumber = cpi.SerialNumber,
                                               State = cpi.State,
                                               User = cpi.User,
                                               UserName = cpi.UserName,
                                               Version = cpi.Version,
                                               WorkflowsLastModifiedTimestamp = cpi.WorkflowsLastModifiedTimestamp,
                                               IsFromCurrentAccount = (ac.ID == accountId),
                                               ID = cpi.ID,
                                               SharingCompany = sc.Name
                                           }).ToList()
                                         .Select(cpi => new ColorProofInstance
                                             {
                                                 SystemName = cpi.SystemName,
                                                 Address = cpi.Address,
                                                 AdminName = cpi.AdminName,
                                                 ComputerName = cpi.ComputerName,
                                                 ColorProofWorkflows = cpi.ColorProofWorkflows,
                                                 ColorProofInstanceState = cpi.ColorProofInstanceState,                                               
                                                 Email = cpi.Email,
                                                 Guid = cpi.Guid,
                                                 IsDeleted = cpi.IsDeleted,
                                                 IsOnline = cpi.IsOnline,
                                                 LastSeen = cpi.LastSeen,
                                                 Owner = cpi.Owner,
                                                 PairingCode = cpi.PairingCode,
                                                 PairingCodeExpirationDate = cpi.PairingCodeExpirationDate,
                                                 Password = cpi.Password,
                                                 SerialNumber = cpi.SerialNumber,
                                                 State = cpi.State,
                                                 User = cpi.User,
                                                 UserName = cpi.UserName,
                                                 Version = cpi.Version,
                                                 WorkflowsLastModifiedTimestamp = cpi.WorkflowsLastModifiedTimestamp,
                                                 IsFromCurrentAccount = cpi.IsFromCurrentAccount,
                                                 ID = cpi.ID,
                                                 SharingCompany = cpi.SharingCompany
                                             }).ToList()
                                : (from cpi in context.ColorProofInstances
                                   join u in context.Users on cpi.Owner equals u.ID
                                   join ac in context.Accounts on u.Account equals ac.ID
                                   join sc in context.Companies on ac.ID equals sc.Account
                                   where
                                       cpi.IsDeleted == isDeleted &&
                                       u.Account == accountId
                                   select new
                                       {
                                           SystemName = cpi.SystemName,
                                           Address = cpi.Address,
                                           AdminName = cpi.AdminName,
                                           ComputerName = cpi.ComputerName,
                                           ColorProofWorkflows = cpi.ColorProofWorkflows,
                                           ColorProofInstanceState = cpi.ColorProofInstanceState,                                           
                                           Email = cpi.Email,
                                           Guid = cpi.Guid,
                                           IsDeleted = cpi.IsDeleted,
                                           IsOnline = cpi.IsOnline,
                                           LastSeen = cpi.LastSeen,
                                           Owner = cpi.Owner,
                                           PairingCode = cpi.PairingCode,
                                           PairingCodeExpirationDate = cpi.PairingCodeExpirationDate,
                                           Password = cpi.Password,
                                           SerialNumber = cpi.SerialNumber,
                                           State = cpi.State,
                                           User = cpi.User,
                                           UserName = cpi.UserName,
                                           Version = cpi.Version,
                                           WorkflowsLastModifiedTimestamp = cpi.WorkflowsLastModifiedTimestamp,
                                           IsFromCurrentAccount = (ac.ID == accountId),
                                           ID = cpi.ID,
                                           SharingCompany = sc.Name
                                       }).ToList()
                                         .Union
                                      (from scpw in context.SharedColorProofWorkflows
                                       join u in context.Users on scpw.User equals u.ID
                                       join cpcw in context.ColorProofCoZoneWorkflows on scpw.ColorProofCoZoneWorkflow
                                           equals cpcw.ID
                                       join cpw in context.ColorProofWorkflows on cpcw.ColorProofWorkflow equals cpw.ID
                                       join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                                       join cpuo in context.Users on cpi.Owner equals cpuo.ID
                                       join ac in context.Accounts on cpuo.Account equals ac.ID
                                       join sc in context.Companies on ac.ID equals sc.Account
                                       where
                                           u.Account == accountId &&
                                           scpw.IsAccepted == true && scpw.IsEnabled
                                       select new
                                           {
                                               SystemName = cpi.SystemName,
                                               Address = cpi.Address,
                                               AdminName = cpi.AdminName,
                                               ComputerName = cpi.ComputerName,
                                               ColorProofWorkflows = cpi.ColorProofWorkflows,
                                               ColorProofInstanceState = cpi.ColorProofInstanceState,
                                               Email = cpi.Email,
                                               Guid = cpi.Guid,
                                               IsDeleted = cpi.IsDeleted,
                                               IsOnline = cpi.IsOnline,
                                               LastSeen = cpi.LastSeen,
                                               Owner = cpi.Owner,
                                               PairingCode = cpi.PairingCode,
                                               PairingCodeExpirationDate = cpi.PairingCodeExpirationDate,
                                               Password = cpi.Password,
                                               SerialNumber = cpi.SerialNumber,
                                               State = cpi.State,
                                               User = cpi.User,
                                               UserName = cpi.UserName,
                                               Version = cpi.Version,
                                               WorkflowsLastModifiedTimestamp = cpi.WorkflowsLastModifiedTimestamp,
                                               IsFromCurrentAccount = (ac.ID == accountId),
                                               ID = cpi.ID,
                                               SharingCompany = sc.Name
                                           }).ToList()
                                         .Select(cpi => new ColorProofInstance
                                             {
                                                 SystemName = cpi.SystemName,
                                                 Address = cpi.Address,
                                                 AdminName = cpi.AdminName,
                                                 ComputerName = cpi.ComputerName,
                                                 ColorProofWorkflows = cpi.ColorProofWorkflows,
                                                 ColorProofInstanceState = cpi.ColorProofInstanceState,
                                                 Email = cpi.Email,
                                                 Guid = cpi.Guid,
                                                 IsDeleted = cpi.IsDeleted,
                                                 IsOnline = cpi.IsOnline,
                                                 LastSeen = cpi.LastSeen,
                                                 Owner = cpi.Owner,
                                                 PairingCode = cpi.PairingCode,
                                                 PairingCodeExpirationDate = cpi.PairingCodeExpirationDate,
                                                 Password = cpi.Password,
                                                 SerialNumber = cpi.SerialNumber,
                                                 State = cpi.State,
                                                 User = cpi.User,
                                                 UserName = cpi.UserName,
                                                 Version = cpi.Version,
                                                 WorkflowsLastModifiedTimestamp = cpi.WorkflowsLastModifiedTimestamp,
                                                 IsFromCurrentAccount = cpi.IsFromCurrentAccount,
                                                 ID = cpi.ID,
                                                 SharingCompany = cpi.SharingCompany
                                             }).ToList();

                var distinctInstances = instances.OrderByDescending(instance => instance.SystemName)
                                                 .GroupBy(instance => instance.SystemName)
                                                 .SelectMany(instance => instance.Take(1)).ToList();
                return distinctInstances;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Checks if there are active colorProof Available
        /// </summary>
        /// <param name="isDeleted">if set to <c>true</c> [is deleted].</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="searchSystemName">Name of the search system.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public static bool IsRunningColorProofAvailable(int accountId, GMGColorContext context)
        {
            try
            {
                return   (from cpi in context.ColorProofInstances
                             join u in context.Users on cpi.Owner equals u.ID
                             where u.Account == accountId && (ColorProofInstanceStateEnum)cpi.State == ColorProofInstanceStateEnum.Running
                             && cpi.IsDeleted == false
                             select cpi.ID).Any();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="stateArr">The state arr.</param>
        /// <param name="isDeleted">if set to <c>true</c> [is deleted].</param>
        /// <param name="accountId">The account id.</param>
        /// <returns></returns>
        public static List<ColorProofInstance> GetList(ColorProofInstanceStateEnum[] stateArr, bool isDeleted, int accountId, GMGColorContext context)
        {
            int[] arr = stateArr.Select(o => (int) o).ToArray();
            return (from cpi in context.ColorProofInstances
                    join cpis in context.ColorProofInstanceStates on cpi.State equals cpis.ID
                    join u in context.Users on cpi.Owner equals u.ID
                    where 
                        arr.Contains(cpis.Key) && 
                        cpi.IsDeleted == isDeleted && 
                        u.Account == accountId
                    select cpi).ToList();
        }

        #endregion

        [Serializable]
        public enum ColorProofInstanceStateEnum { Pending = 1, Ready = 2, Stopped = 3, Running = 4, Error = 5 }

        public static bool AnyAvailableValidInstances(int userId, GMGColorContext context)
        {
            IEnumerable<int> groupIdList = (from u in context.Users where u.ID == userId
                                            join ugu in context.UserGroupUsers on u.ID equals ugu.User
                                            where u.ID == userId
                                            select ugu.UserGroup);

            var valid = (from cpczwug in context.ColorProofCoZoneWorkflowUserGroups
                            join cpczw in context.ColorProofCoZoneWorkflows on cpczwug.ColorProofCoZoneWorkflow equals cpczw.ID
                            join cpw in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpw.ID
                            join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                            join cpis in context.ColorProofInstanceStates on cpi.State equals cpis.ID
                            where cpis.Key == (int) GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Running &&
                                cpczw.IsDeleted == false && cpczw.IsAvailable == true &&
                                groupIdList.Contains(cpczwug.UserGroup)
                            select cpi.ID
                        ).Any();

            return valid;
        }

        public static bool ColorProofInstanceExists(string instanceName, GMGColorContext context)
        {
            return (from cpins in context.ColorProofInstances
                    join cpis in context.ColorProofInstanceStates on cpins.State equals cpis.ID
                    where cpins.SystemName.ToLower() == instanceName.ToLower() && cpins.IsDeleted == false
                     && cpis.Key == (int)ColorProofInstanceStateEnum.Running
                    select cpins.ID).Any();
        }

        public static bool IsColorProofInstanceRunning(string instanceName, GMGColorContext context)
        {
            return (from cpins in context.ColorProofInstances
                    join cpis in context.ColorProofInstanceStates on cpins.State equals cpis.ID
                    where cpins.SystemName.ToLower() == instanceName.ToLower() && cpins.IsDeleted == false && cpis.Key == (int) GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Running
                    select cpins.ID).Any();
        }

        public static string GetColorProofInstanceGuidByWorkflow(int coZoneWorkflowID, GMGColorContext context)
        {
            return (from cpczwf in context.ColorProofCoZoneWorkflows
                    join cpwf in context.ColorProofWorkflows on cpczwf.ColorProofWorkflow equals cpwf.ID
                    join cpins in context.ColorProofInstances on cpwf.ColorProofInstance equals cpins.ID
                    where cpczwf.ID == coZoneWorkflowID
                    select cpins.Guid).FirstOrDefault();
        }

        public static int GetNumberOfCPInstances(int accID, GMGColorContext context)
        {
            return (from cp in context.ColorProofInstances
                    join us in context.Users on cp.Owner equals us.ID
                    join ac in context.Accounts on us.Account equals ac.ID
                    where ac.ID == accID && cp.IsDeleted == false
                    select cp.ID).Count();
        }
    }

    public class ColorProofInstanceEqualityComparer : IEqualityComparer<ColorProofInstance>
    {
        bool IEqualityComparer<ColorProofInstance>.Equals(ColorProofInstance x, ColorProofInstance y)
        {
            //Check whether the compared objects reference the same data. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null. 
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal. 
            return x.ID == y.ID;
        }

        int IEqualityComparer<ColorProofInstance>.GetHashCode(ColorProofInstance obj)
        {
            //Check whether the object is null 
            if (Object.ReferenceEquals(obj, null)) return 0;

            //Calculate the hash code for the product. 
            return obj.ID ^ obj.Guid.GetHashCode() ^ obj.SystemName.GetHashCode() ^ obj.UserName.GetHashCode() ^ obj.Password.GetHashCode();
        }
    }
}

