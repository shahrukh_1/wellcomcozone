﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorDAL
{
    public partial class MenuItem
    {
        public class MenuItemPair
        {
            #region Properties

            public int ID { get; set; }

            public string Key { get; set; }

            #endregion
        }
        public static MenuItemPair GetParentMenuId(int menuId, GMGColorContext context)
        {
            return context.Database.SqlQuery<MenuItemPair>(@"
            WITH    RecursiveQuery ( id, parent, [key], level )
                      AS ( SELECT   ID ,
                                    Parent ,
                                    [Key],
                                    0 AS level
                           FROM     dbo.MenuItem
                           WHERE    id = @menuId
                           UNION ALL
                           SELECT   PM.ID ,
                                    PM.Parent ,
                                    PM.[Key],
                                    level + 1 AS level
                           FROM     dbo.MenuItem PM
                                    INNER JOIN RecursiveQuery SM ON SM.Parent = PM.ID
                         )
                SELECT  TOP 1 id, [key]
                FROM    RecursiveQuery
                WHERE   parent IS NULL", new SqlParameter("menuId", menuId)).FirstOrDefault();
        }
    }
}
