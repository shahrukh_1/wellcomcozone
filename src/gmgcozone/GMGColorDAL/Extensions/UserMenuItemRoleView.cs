using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL
{
    public partial class UserMenuItemRoleView : BusinessObjectBase
    {        
        #region Extension Methods
        // Put methods to manipulate Business Objects here. Add, Update etc
        #endregion

        public class UserMenuItemRoleViewEqualityComparer : IEqualityComparer<UserMenuItemRoleView>
        {
            bool IEqualityComparer<UserMenuItemRoleView>.Equals(UserMenuItemRoleView x, UserMenuItemRoleView y)
            {
                //Check whether the compared objects reference the same data. 
                if (Object.ReferenceEquals(x, y)) return true;

                //Check whether any of the compared objects is null. 
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                //Check whether the products' properties are equal. 
                return x.MenuItem == y.MenuItem ;
            }

            int IEqualityComparer<UserMenuItemRoleView>.GetHashCode(UserMenuItemRoleView obj)
            {
                //Check whether the object is null 
                if (Object.ReferenceEquals(obj, null)) return 0;

                //Calculate the hash code for the product. 
                return obj.MenuItem.GetHashCode();
            }
        }
    }
}

