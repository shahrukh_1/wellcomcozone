﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMGColorDAL
{
    public partial class CheckList : BusinessObjectBase
    {
        #region OnPropertyChange Methods

        // Put implementations of OnBusinessObjectNameBOPropertyNameChanging()
        // and OnBusinessObjectNameBOPropertyNameChanged() here

        #endregion

        /// <summary>
        /// Returns whether or not the checklist name is unique at the account level
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="checkListName"></param>
        /// <param name="id"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool IsCheckListNameUnique(int accountId, string checkListName, int id, GMGColorContext context)
        {
            if (checkListName != null)
            {
                return !(from aw in context.CheckList
                         join ac in context.Accounts on aw.Account equals ac.ID
                         where aw.Name.ToLower() == checkListName.ToLower().Trim() && aw.ID != id && ac.ID == accountId
                         select aw.ID).Any();
            }
            return true;
        }

    }
}
