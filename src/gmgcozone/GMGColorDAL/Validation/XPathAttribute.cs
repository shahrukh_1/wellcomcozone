﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Xml.XPath;

namespace GMGColorDAL.Validation
{
    /// <summary>
    /// XPath validator attribute
    /// </summary>
    public class XPathAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes("<xml></xml>")))
            {
                XPathDocument doc = new XPathDocument(stream);
                XPathNavigator nav = doc.CreateNavigator();
                try
                {
                    nav.Compile((string)value);
                }
                catch (Exception)
                {
                    return new ValidationResult(GMGColor.Resources.Resources.errInvalidXPath);
                }
            }
            return ValidationResult.Success;
        }
    }
}
