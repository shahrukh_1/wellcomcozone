//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReturnUserReportInfoView
    {
        public int ID { get; set; }
        public string Account { get; set; }
        public string Name { get; set; }
        public int NumberOfApprovalsIn { get; set; }
        public int NumberOfUploads { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public System.DateTime DateLastLogin { get; set; }
        public string GroupMembership { get; set; }
    }
}
