//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApprovalJobPhase
    {
        public ApprovalJobPhase()
        {
            this.AdvancedSearchApprovalPhases = new HashSet<AdvancedSearchApprovalPhase>();
            this.Approvals = new HashSet<Approval>();
            this.ApprovalAnnotations = new HashSet<ApprovalAnnotation>();
            this.ApprovalCollaborators = new HashSet<ApprovalCollaborator>();
            this.ApprovalCollaboratorDecisions = new HashSet<ApprovalCollaboratorDecision>();
            this.ApprovalCollaboratorGroups = new HashSet<ApprovalCollaboratorGroup>();
            this.ApprovalJobPhaseApprovals = new HashSet<ApprovalJobPhaseApproval>();
            this.SharedApprovals = new HashSet<SharedApproval>();
        }
    
        public int ID { get; set; }
        public int ApprovalJobWorkflow { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }
        public int DecisionType { get; set; }
        public Nullable<int> PrimaryDecisionMaker { get; set; }
        public Nullable<int> ExternalPrimaryDecisionMaker { get; set; }
        public int ApprovalTrigger { get; set; }
        public bool ShowAnnotationsToUsersOfOtherPhases { get; set; }
        public bool ShowAnnotationsOfOtherPhasesToExternalUsers { get; set; }
        public int PhaseTemplateID { get; set; }
        public Nullable<int> DeadlineUnitNumber { get; set; }
        public Nullable<int> DeadlineUnit { get; set; }
        public Nullable<int> DeadlineTrigger { get; set; }
        public Nullable<System.DateTime> Deadline { get; set; }
        public bool IsActive { get; set; }
        public bool OverdueNotificationSent { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    
        public virtual ICollection<AdvancedSearchApprovalPhase> AdvancedSearchApprovalPhases { get; set; }
        public virtual ICollection<Approval> Approvals { get; set; }
        public virtual ICollection<ApprovalAnnotation> ApprovalAnnotations { get; set; }
        public virtual ICollection<ApprovalCollaborator> ApprovalCollaborators { get; set; }
        public virtual ICollection<ApprovalCollaboratorDecision> ApprovalCollaboratorDecisions { get; set; }
        public virtual ICollection<ApprovalCollaboratorGroup> ApprovalCollaboratorGroups { get; set; }
        public virtual ApprovalDecision ApprovalDecision { get; set; }
        public virtual ApprovalJobWorkflow ApprovalJobWorkflow1 { get; set; }
        public virtual DecisionType DecisionType1 { get; set; }
        public virtual ExternalCollaborator ExternalCollaborator { get; set; }
        public virtual PhaseDeadlineTrigger PhaseDeadlineTrigger { get; set; }
        public virtual PhaseDeadlineUnit PhaseDeadlineUnit { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ApprovalJobPhaseApproval> ApprovalJobPhaseApprovals { get; set; }
        public virtual ICollection<SharedApproval> SharedApprovals { get; set; }
    }
}
