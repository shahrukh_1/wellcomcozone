//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccountHelpCentre
    {
        public int ID { get; set; }
        public int Account { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string SupportDays { get; set; }
        public string SupportHours { get; set; }
        public string UserGuideLocation { get; set; }
        public string UserGuideName { get; set; }
        public Nullable<System.DateTime> UserGuideUpdatedDate { get; set; }
    
        public virtual Account Account1 { get; set; }
    }
}
