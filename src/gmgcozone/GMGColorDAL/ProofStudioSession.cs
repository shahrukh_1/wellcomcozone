//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProofStudioSession
    {
        public ProofStudioSession()
        {
            this.NotificationItems = new HashSet<NotificationItem>();
        }
    
        public int ID { get; set; }
        public string Guid { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsCompleted { get; set; }
        public int User { get; set; }
        public bool IsDeleted { get; set; }
    
        public virtual ICollection<NotificationItem> NotificationItems { get; set; }
        public virtual User User1 { get; set; }
    }
}
