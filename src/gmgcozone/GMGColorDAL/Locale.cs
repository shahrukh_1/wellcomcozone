//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Locale
    {
        public Locale()
        {
            this.Accounts = new HashSet<Account>();
            this.ApprovalCustomDecisions = new HashSet<ApprovalCustomDecision>();
            this.DeliverExternalCollaborators = new HashSet<DeliverExternalCollaborator>();
            this.ExternalCollaborators = new HashSet<ExternalCollaborator>();
            this.Users = new HashSet<User>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
    
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<ApprovalCustomDecision> ApprovalCustomDecisions { get; set; }
        public virtual ICollection<DeliverExternalCollaborator> DeliverExternalCollaborators { get; set; }
        public virtual ICollection<ExternalCollaborator> ExternalCollaborators { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
