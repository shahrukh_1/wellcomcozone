//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class SharedColorProofWorkflow
    {
        public int ID { get; set; }
        public int ColorProofCoZoneWorkflow { get; set; }
        public int User { get; set; }
        public Nullable<bool> IsAccepted { get; set; }
        public bool IsEnabled { get; set; }
        public string Token { get; set; }
    
        public virtual ColorProofCoZoneWorkflow ColorProofCoZoneWorkflow1 { get; set; }
        public virtual User User1 { get; set; }
    }
}
