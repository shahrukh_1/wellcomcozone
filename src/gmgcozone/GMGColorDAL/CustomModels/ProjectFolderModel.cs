﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Linq;
using GMG.CoZone.Common;
using GMGColor.Resources;


namespace GMGColorDAL.CustomModels
{
    public class ProjectFolderModel : Common
    {
        [Serializable]
        public class ProjectFoldersModel : Common
        {
            #region Private Data

            private string _searchText;

            #endregion

            #region Properties
            public List<ReturnProjectFolderPageView> ListProjectFolders { get; set; }
            public int PageNr { get; set; }
            public int OrderField { get; set; }
            public bool OrderDesc { get; set; }
            public int status { get; set; }
            [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
            public string selectedTagwards { get; set; }

            public int LoggedUserId { get; set; }
            public int AccountId { get; set; }
            public int RowsPerPage;
            public int TotalRows;
            public string LoggedAccountTimeZone { get; set; }
            public bool IsProjectNameSelectable { get; set; }

            
            public ProjectFolder projectDetails { get; set; }
            public List<ProjectFolderCollaboratorRole> projectCollaboratorRole { get; set; }
            public ApprovalsFilterModel ApprovalsFilter { get; set; }

            #endregion

            public string SearchText
            {
                get { return _searchText; }
                set
                {
                    _searchText = value;
                }
            }

            public int ProjrctFolderID { get; set; }
            

            #region Constructors

            public ProjectFoldersModel()
            {
            }

            //Constructor for File Transfer History Grid
            public ProjectFoldersModel(Account objAccount, int loggedUserId, int pageNr, int statusFilter , int orderField, bool orderDesc, GMGColorContext context, int rowsPerPage = 10, string searchText = "")
                : base(objAccount, context)
            {
                AccountId = objAccount.ID;
                LoggedUserId = loggedUserId;
                PageNr = pageNr;

                this.OrderField = orderField;
                this.OrderDesc = orderDesc;

                ListProjectFolders = ProjectFolder.GetProjectFolderInfo(objAccount.ID, LoggedUserId , statusFilter , rowsPerPage, PageNr, orderField, orderDesc, searchText == null ? String.Empty : searchText, selectedTagwards == null ? String.Empty : selectedTagwards,  ref TotalRows, context);

                RowsPerPage = rowsPerPage;
                LoggedAccountTimeZone = objAccount.TimeZone;
            }

            //Constructor for ProjectFoldersModel 
            public ProjectFoldersModel(Account objAccount, int loggedUserId, string folderPath, GMGColorContext context)
            {
                AccountId = objAccount.ID;
                LoggedUserId = loggedUserId;
                LoggedAccountTimeZone = objAccount.TimeZone;
            }

            #endregion

        }

        [Serializable]
        public class ProjectFolderTagwordDetails
        {
            public int ID { get; set; }
            public string Tagword { get; set; }
        }

        [Serializable]
        public class ProjectFolderCollaboratorsDetails
        {
            public bool IsCollaboratorCreator { get; set; }
            public int CollaboratorID { get; set; }
            public string CollaboratorName { get; set; }
            public string CollaboratorEmailAddress { get; set; }
            public  bool CollaboratorIsPDM { get; set; }
            public int CollaboratorRole { get; set; }
            public string CollaboratorRoleName { get; set; }
        }
    }

}