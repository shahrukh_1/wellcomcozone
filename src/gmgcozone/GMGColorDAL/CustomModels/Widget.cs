﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using RSC = GMGColor.Resources;
using System.Web.Mvc;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class GroupsModel
    {
        #region Properties

        public bool IsDelete { get; set; }
        public bool ShowModelDialog { get; set; }
        public int Group { get; set; }
        public string Creator { get; set; }
        public string Modifier { get; set; }
        public bool IsModelError { get; set; }

        [Required(ErrorMessageResourceName = "reqGroupName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }
        public string RedirectURL { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<UsersInUserGroup> CollaboratorsWhereThisIsCollaboratorGroup { get; set; }
        public string BrandingPresetName { get; set; }

        #endregion

        #region Contructors & Methods

        public GroupsModel()
        {

        }

        public GroupsModel(int accountId, bool isShow, GMGColorContext context)
        {
            this.PopulaterGroups(accountId, isShow, 0, context);
        }

        public GroupsModel(int accountId, bool isShow, int groupId, GMGColorContext context)
        {
            this.PopulaterGroups(accountId, isShow, groupId, context);
        }

        private void PopulaterGroups(int accountId, bool isShow, int groupId, GMGColorContext context)
        {
            try
            {
                int statusActive = UserStatu.GetUserStatus(UserStatu.Status.Active, context).ID;
                int statusInvited = UserStatu.GetUserStatus(UserStatu.Status.Invited, context).ID;

                BrandingPresetName = (from bp in context.BrandingPresets
                                      join bpug in context.BrandingPresetUserGroups on bp.ID equals bpug.BrandingPreset
                                      where bpug.UserGroup == groupId
                                      select bp.Name).FirstOrDefault();

                this.CollaboratorsWhereThisIsCollaboratorGroup = (from u in context.Users
                                                                  join a in context.Accounts on u.Account equals a.ID
                                                                  where
                                                                      a.ID == accountId &&
                                                                      (u.Status == statusActive ||
                                                                       u.Status == statusInvited)
                                                                  orderby u.GivenName
                                                                  select new UsersInUserGroup()
                                                                             {
                                                                                 objUser = u,
                                                                                 IsUserInGroup = false
                                                                             }).ToList();
                if (groupId > 0)
                {
                    foreach (UsersInUserGroup usersInUserGroup in CollaboratorsWhereThisIsCollaboratorGroup)
                    {
                        usersInUserGroup.IsUserInGroup = (from ug in context.UserGroups
                                                          join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                                          where
                                                              ug.ID == groupId && ugu.User == usersInUserGroup.objUser.ID
                                                          select ug.ID).Any();

                        usersInUserGroup.IsPrimary = (from ug in context.UserGroups
                                                          join ugu in context.UserGroupUsers on ug.ID equals ugu.UserGroup
                                                          where
                                                              ug.ID == groupId && ugu.User == usersInUserGroup.objUser.ID
                                                          select ugu.IsPrimary).FirstOrDefault();
                    }
                }
                
                this.IsDelete = false;
                this.ShowModelDialog = isShow;
                this.Group = groupId;
                this.Name = groupId > 0
                                ? (from ug in context.UserGroups where ug.ID == groupId select ug.Name).FirstOrDefault() ??
                                  string.Empty
                                : string.Empty;

                if (groupId > 0)
                {
                    this.Creator = (from ug in context.UserGroups
                                    join u in context.Users on ug.Creator equals u.ID
                                    where ug.ID == groupId
                                    select u.GivenName + " " + u.FamilyName).FirstOrDefault();
                    this.CreatedDate = (from ug in context.UserGroups where ug.ID == groupId select ug.CreatedDate).FirstOrDefault();
                    this.Modifier = (from ug in context.UserGroups
                                     join u in context.Users on ug.Modifier equals u.ID
                                     where ug.ID == groupId
                                     select u.GivenName + " " + u.FamilyName).FirstOrDefault();
                    this.ModifiedDate = (from ug in context.UserGroups where ug.ID == groupId select ug.ModifiedDate).FirstOrDefault();
                }
            }
            finally
            {
            }
        }

        #endregion

        #region Sub Classes

        [Serializable]
        public class UsersInUserGroup
        {
            #region Properties

            public bool IsUserInGroup { get; set; }
            public bool IsPrimary { get; set; }
            public GMGColorDAL.User objUser { get; set; }

            #endregion
        }

        #endregion
    }
    [Serializable]
    public class ProjectFolderSelectedCollaboratorsModel
    {
        public int collaboratorID { get; set; }
        public bool isPDM { get; set; }
        public int roleID { get; set; }
    }
  
    [Serializable]
    public class ApprovalsFilterModel
    {
        #region Properties
        public bool Image { get; set; }
        public int NoOfImageApprovals { get; set; }
        public int NoOfDocumentApprovals { get; set; }
        public int NoOfVideoApprovals { get; set; }
        public int NoOfAudioApprovals { get; set; }
        public int NoOfHTMLApprovals { get; set; }
        public bool Document { get; set; }
        public bool Video { get; set; }
        public bool Audio { get; set; }
        public bool HTML { get; set; }
        public bool IsPrivate { get; set; }
        public bool ShowPanel { get; set; }
        public bool Refresh { get; set; }
        public int CurrentTab { get; set; }
        public List<GMGColorDAL.CustomModels.PredefinedTagwords.PredefinedTagwordsWithCount> tagwords { get; set; }
        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string SearchText { get; set; }
        #endregion
        public ApprovalsFilterModel()
        {

        }
    }
    [Serializable]
    public class ApprovalsTypeCount
    {
        public int NoOfImageApprovals { get; set; }
        public int NoOfDocumentApprovals { get; set; }
        public int NoOfVideoApprovals { get; set; }
        public int NoOfAudioApprovals { get; set; }
        public int NoOfHTMLApprovals { get; set; }
    }
    [Serializable]
    public class FoldersModel
    {
        #region Enums

        public enum FolderStatus
        {
            Default,
            Delete,
            Restore,
            PermanentDelete,
            EmptyRecycleBin
        }

        #endregion

        #region Properties

        public bool IsDelete { get; set; }
        public bool IsPrivate { get; set; }
        public bool ShowPanel { get; set; }
        public int Account { get; set; }
        public int LoggedUserId { get; set; }
        public int Creator { get; set; }
        public int Folder { get; set; }
        public int Parent { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string ExistingFolders { get; set; }
        public FolderStatus Status { get; set; }
        public string SelectedApprovals { get; set; }
        public string SelectedFolders { get; set; }
        public bool Refresh { get; set; }
        public bool MoveFiles { get; set; }
        public int CurrentTab { get; set; }
        public bool FolderPermissionsAutoInheritanceEnabled { get; set; }

        public bool AllCollaboratorsDecisionRequired { get; set; }

        #endregion

        #region Contrustors & Methods

        public FoldersModel()
        {

        }

        #endregion
    }

    [Serializable]
    public class PermissionsModel
    {
        #region Properties

        public bool IsFolder { get; set; }
        public bool Refresh { get; set; }
        public bool SendEmails { get; set; }
        public bool ShowPanel { get; set; }
        public int Creator { get; set; }
        public int Modifier { get; set; }
        public int ID { get; set; }
        public int PDM { get; set; }
        public int Owner { get; set; }
        public string Domain { get; set; }
        public string Groups { get; set; }
        public string ExternalUsers { get; set; }
        public string Users { get; set; }
        public bool IsNewApproval { get; set; }
        public bool IsPhaseScreen { get; set; }
        public int? CurrentPhase { get; set; }
		public bool InheritParentFolderPermissions { get; set; }
        public bool ShowRetoucherApprovalRole { get; set; }
        public string SelectedApprovals { get; set; }

        public List<UserOrUserGroup> SelectedUsersAndGroups { get; set; }
        public string PhasesCollaborators { get; set; }
        public int? ApprovalWorkflow { get; set; }
        public int? Checklist { get; set; }
        [AllowHtml]
        public string OptionalMessage { get; set; }
        public bool IncludeAnOptionalMessage { get; set; }

        #endregion

        #region Contructors & Methods

        public PermissionsModel()
        {
            
        }

        #endregion

        #region Sub Classes

        [Serializable]
        public class UserOrUserGroup
        {
            #region Properties

            public bool IsChecked { get; set; }
            public bool IsExternal { get; set; }
            public bool IsGroup { get; set; }
            public int Count { get; set; }
            public int ID { get; set; }
            public int ApprovalRole { get; set; }
            public string Members { get; set; }
            public string Name { get; set; }
            public string Role { get; set; }
            public string EmailAddress { get; set; }
            public string PhotoPath { get; set; }


            #endregion

            #region Contrustors & Methods

            public UserOrUserGroup()
            {

            }

            #endregion
        }

        #endregion
    }

    [Serializable]
    public class ShareModel
    {
        #region Properties

        public bool IsFolder { get; set; }
        public bool SendEmails { get; set; }
        public bool IsShareURL { get; set; }
        public bool IsShareDownloadURL { get; set; }
        public bool ShowPanel { get; set; }
        public int Approval { get; set; }
        public string Domain { get; set; }
        public string ExternalUsers { get; set; }
        public string ExternalEmails { get; set; }
        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Message { get; set; }
        public string PDMEmail { get; set; }
        public string ExternalUsersEvents { get; set; }
        public bool IsPopupShown { get; set; }
        public bool IsPhaseScreen { get; set; }
        public int? CurrentPhase { get; set; }
        public bool ShowRetoucherApprovalRole { get; set; }
        public int DefaultLanguage { get; set; }

        public List<ExternalCollaborator> ExistingExternalUsers { get; set; }
        public Dictionary<int, string> ListRoles { get; set; }
        public List<NotificationPreset> NotificationsPresets { get; set; }

        public int ReadOnlyCollaboratorRole { get; set; }
        public int ReviewerCollaboratorRole { get; set; }
        public int ApproverAndReviewerCollaboratorRole { get; set; }
        public int RetoucherCollaboratorRole { get; set; }

        public string DefaultImagePath { get; set; }
        public Dictionary<int, string> LocalizeLocaleNames { get; set; }

        #endregion

        #region Contrustors & Methods

        public ShareModel()
        {

        }

        #endregion
    }
    
    [Serializable]
    public class GlobalDeleteModel
    {
        #region Properties

        public int WidgetID { get; set; }
        public string RedirectURL { get; set; }
        public string WidgetType { get; set; }

        #endregion
    }

    public class PermissionsUsersAndRolesModel
    {
        #region Properties

        public List<GroupPermissions> Groups { get; set; }
        public List<UserPermissions> Users { get; set; }
        public List<ExternalUserPermissions> ExternalUsers { get; set; }
        public List<NewExternalUserPermissions> NewExternalUsers { get; set; } 

        #endregion

        public PermissionsUsersAndRolesModel()
        {
            Groups = new List<GroupPermissions>();
            Users = new List<UserPermissions>();
            ExternalUsers =  new List<ExternalUserPermissions>();
            NewExternalUsers = new List<NewExternalUserPermissions>();
        }
    }

    public class UserPermissions
    {
        #region Properties

        public int ID { get; set; }
        public int Role { get; set; }
        public int? Phase { get; set; }

        #endregion
    }

    public class ExternalUserPermissions
    {
        #region Properties

        public int ID { get; set; }
        public int Role { get; set; }
        public int? Phase { get; set; }

        #endregion
    }

    public class NewExternalUserPermissions
    {
        #region Properties

        public int Role { get; set; }
        public int? Phase { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public int Locale { get; set; }
        #endregion
    }
    public class GroupPermissions
    {
        #region Properties

        public int Group { get; set; }

        public int? Phase { get; set; }

        #endregion
    }


	public class PermissionsUsersAndRolesModelPerf
	{
		#region Properties

		public List<GroupPermissionsPerf> Groups { get; set; }
		public List<UserPermissionsPerf> Users { get; set; }
		public List<ExternalUserPermissionsPerf> ExternalUsers { get; set; }
		public List<NewExternalUserPermissionsPerf> NewExternalUsers { get; set; }

		#endregion

		public PermissionsUsersAndRolesModelPerf()
		{
			Groups = new List<GroupPermissionsPerf>();
			Users = new List<UserPermissionsPerf>();
			ExternalUsers = new List<ExternalUserPermissionsPerf>();
			NewExternalUsers = new List<NewExternalUserPermissionsPerf>();
		}
	}

	public class UserPermissionsPerf
	{
		#region Properties

		public int ID { get; set; }
		public int Role { get; set; }
		public ApprovalJobPhase Phase { get; set; }

		#endregion
	}

	public class ExternalUserPermissionsPerf
	{
		#region Properties

		public int ID { get; set; }
		public int Role { get; set; }
		public ApprovalJobPhase Phase { get; set; }

		#endregion
	}

	public class NewExternalUserPermissionsPerf
	{
		#region Properties

		public int Role { get; set; }
		public ApprovalJobPhase Phase { get; set; }
		public string Email { get; set; }
		public string FullName { get; set; }
		public int Locale { get; set; }
		#endregion
	}
	public class GroupPermissionsPerf
	{
		#region Properties

		public int Group { get; set; }

		public ApprovalJobPhase Phase { get; set; }

		#endregion
	}
}
