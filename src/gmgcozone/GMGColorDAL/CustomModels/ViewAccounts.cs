﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ViewAccounts : Common
    {
        #region Properties

        public int LoggedUserID { get; set; }
        public int? page { get; set; }
        public List<AccountsView> ListAccountsView { get; set; }

        public List<string> SelectedAccountTypes { get; set; }
        public List<string> SelectedLocations { get; set; }
        public List<string> SelectedAccounts { get; set; }
        public List<string> SelectedAccountStatues { get; set; }

        public Dictionary<int, string> AccountTypes { get; set; }
        public Dictionary<int, string> Locations { get; set; }
        public Dictionary<int, string> Accounts { get; set; }
        public Dictionary<int, string> AccountStatuses { get; set; }

        #endregion

        #region Constructors

        public ViewAccounts()
        {
        }

        public ViewAccounts(Account loggedAccount, int loggedUser, GMGColorContext context)
        {
            this.LoggedUserID = loggedUser;
            this.objLoggedAccount = loggedAccount;

            
            {
                this.objLoggedAccountType = AccountType.GetAccountType(loggedAccount.ID, context);

                List<int> lstRelatedAccountIds = this.objLoggedAccount.GetChilds(context).Where(o => o.ID != this.objLoggedAccount.ID && o.Status != AccountStatu.GetAccountStatus(AccountStatu.Status.Deleted, context).ID).Select(o => o.ID).ToList();
                List<AccountsView> lstAccountsView = (from av in context.AccountsViews where lstRelatedAccountIds.Contains(av.ID) && av.IsTemporary == false select av).ToList();

                this.ListAccountsView = new List<AccountsView>();
                if (lstAccountsView.Count > 0)
                {
                    foreach (AccountsView account in lstAccountsView)
                    {
                        account.AccountTypeName = AccountType.GetLocalizedAccountTypeName(account.AccountTypeID, context);
                        //account.StatusName = AccountStatu.GetLocalizedAccountStatus(account.StatusId.Value);
                        this.ListAccountsView.Add(account);
                    }
                }

                this.AccountTypes = new Dictionary<int, string>();
                this.Locations = new Dictionary<int, string>();
                this.Accounts = new Dictionary<int, string>();
                this.AccountStatuses = new Dictionary<int, string>();

                foreach (AccountsView item in this.ListAccountsView)
                {
                    if (!this.AccountTypes.ContainsKey(item.AccountTypeID))
                        this.AccountTypes.Add(item.AccountTypeID, AccountType.GetLocalizedAccountTypeName(item.AccountTypeID, context));
                    if (!this.Locations.ContainsKey(item.Country))
                        this.Locations.Add(item.Country, item.CountryName);
                    if (!this.Accounts.ContainsKey(item.ID))
                        this.Accounts.Add(item.ID, item.Name);
                    if (!this.AccountStatuses.ContainsKey(item.StatusId))
                        this.AccountStatuses.Add(item.StatusId, item.StatusName);
                }
            }
        }

        #endregion
    }
}
