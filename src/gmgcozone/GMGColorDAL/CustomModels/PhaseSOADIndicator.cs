﻿namespace GMGColorDAL.CustomModels
{
    public class PhaseSOADIndicator
    {
        public int ApprovalId { get; set; }
        public int? PhaseId { get; set; }
        public int UserId { get; set; }
    }
}
