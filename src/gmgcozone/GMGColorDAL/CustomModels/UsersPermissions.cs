﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ModulePermision
    {
        [Required(ErrorMessageResourceName = "reqCPServerId", ErrorMessageResourceType = typeof (Resources))]
        public int ModuleId { get; set; }
        public GMG.CoZone.Common.AppModule ModuleType { get; set; }
        public string ModuleName { get; set; }
        public List<RolePermision> Roles { get; set; }
        public int SelectedRole { get; set; }
        public bool ReadOnly { get; set; }

        public ModulePermision()
        {
            Roles = new List<RolePermision>();
            ModuleName = string.Empty;
            ModuleId = 0;
        }

    }

    [Serializable]
    public class RolePermision
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool RoleChecked { get; set; }

        public RolePermision()
        {
            RoleId = 0;
            RoleName = string.Empty;
        }
    }

    public class UserCollaborateRolePair
    {
        public int UserId { get; set; }
        public int CollaborateRoleId { get; set; }
    }
}
