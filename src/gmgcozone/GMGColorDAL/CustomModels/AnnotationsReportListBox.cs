﻿namespace GMGColorDAL.CustomModels
{
    public class AnnotationsReportUser
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public bool IsExternal { get; set; }
    }

   
    public class AnnotationsReportGroup
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }
    }

    public class AnnotationReportPhase
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }
    }
}
