﻿using System.Collections.Generic;

namespace GMGColorDAL.CustomModels.ApprovalBL
{
    public class ApprovalPhaseDetails
    {
       public List<string> ApprovalTrigger { get; set; }
        public int DecisionType { get; set; }
        public int? PrimaryDecisionMaker { get; set; }
        public ApprovalJobPhase NextPhase { get; set; }
        public Approval Approval { get; set; }
        public ApprovalJobPhaseApproval ApprovalJobPhaseApproval { get; set; }
        public string WorkflowName { get; set; }
        public string PhaseName { get; set; }
        public int? ApprovalTriggerPriority { get; set; }
    }
}
