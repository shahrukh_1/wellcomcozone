﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.ApprovalBL
{
    public class ApprovalDashboardPhaseDetails
    {
        public int ApprovalID { get; set; }
        public bool PhaseShouldBeViewedByAll { get; set; }
        public int? CurrentPhase { get; set; }
        public string PhaseName { get; set; }
        public string WorkflowName { get; set; }
        public string PhaseTrigger { get; set; }
        public bool PhaseManuallyCompleted { get; set; }
        public List<ApprovalDashboardCollaboratorsDetails> Collaborators { get; set; }
    }
    public class ApprovalDashboardCollaboratorsDetails
    {
        public int CollaboratorID { get; set; }
        public bool HasViewed { get; set; }
        public bool IsPDM { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Decision { get; set; }
    }
}
