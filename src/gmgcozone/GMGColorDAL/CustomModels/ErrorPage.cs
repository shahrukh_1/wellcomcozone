﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class CustomError
    {
        public string Title { get; set; }

        public string Message { get; set; }
    }
}
