﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public class SoftProof
    {
        [Required(ErrorMessageResourceName = "reqAuthKey", ErrorMessageResourceType = typeof(Resources))]
        public string AuthKey { get; set; }

        [Required(ErrorMessageResourceName = "reqMachineId", ErrorMessageResourceType = typeof(Resources))]
        public string MachineId { get; set; }

        [Required(ErrorMessageResourceName = "reqDataFile", ErrorMessageResourceType = typeof(Resources))]
        public string FileData { get; set; }

        [Required(ErrorMessageResourceName = "reqFileName", ErrorMessageResourceType = typeof(Resources))]
        public string FileName { get; set; }
    }
}
