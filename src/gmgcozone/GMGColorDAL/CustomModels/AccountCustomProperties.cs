﻿using System;
using System.Collections.Generic;

namespace GMGColorDAL
{
    [Serializable]
    public class AccountCustomProperties
    {
        #region Properties

        public string Guid { get; set; }
        public string Region { get; set; }
        public List<string> ApprovalGuids { get; set; }
        public List<string> DeliverGuids { get; set; }       
        public List<int> UserIds { get; set; } 

        #endregion

        #region Constructors

        public AccountCustomProperties()
        {
           ApprovalGuids =  new List<string>();
           DeliverGuids =  new List<string>();          
           UserIds =  new List<int>();
        }

        #endregion
    }
}
