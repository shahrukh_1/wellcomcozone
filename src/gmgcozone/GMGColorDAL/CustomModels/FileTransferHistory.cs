﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Linq;
using GMG.CoZone.Common;
using GMGColor.Resources;


namespace GMGColorDAL.CustomModels
{
    public class FileTransferHistory : Common
    {
        [Serializable]
        public class FileTransferHistoryModel : Common
        {
            #region Private Data

            private string _searchText;
            private string _searchSenderText;

            #endregion

            #region Properties
            public List<ReturnFileTransferHistoryPageView> ListJobs { get; set; }
            public bool ValidServerAvailable { get; set; }
            public string SelectedInstance { get; set; }
            public int PageNr { get; set; }
            public int OrderField { get; set; }
            public bool OrderDesc { get; set; }
            public int LoggedUserId { get; set; }
            public int AccountId { get; set; }
            public int RowsPerPage;
            public int TotalRows;
            public string LoggedAccountTimeZone { get; set; }
            public bool IsJobNameSelectable { get; set; }


            public string SearchText
            {
                get { return _searchText; }
                set
                {
                    _searchText = value;
                }
            }

            public string SearchSenderText
            {
                get { return _searchSenderText; }
                set
                {
                    _searchSenderText = value;
                }
            }


            #endregion

            #region Constructors

            public FileTransferHistoryModel()
            {
            }

            //Constructor for File Transfer History Grid
            public FileTransferHistoryModel(Account objAccount, int loggedUserId, string folderPath, int pageNr, int orderField, bool orderDesc, GMGColorContext context, int rowsPerPage = 10, string searchText = "", string searchSender = "")
                : base(objAccount, context)
            {
                AccountId = objAccount.ID;
                LoggedUserId = loggedUserId;
                PageNr = pageNr;

                this.OrderField = orderField;
                this.OrderDesc = orderDesc;

                var fileTransferRole =GMGColorDAL.User.GetRole(GMG.CoZone.Common.AppModule.FileTransfer,LoggedUserId,context);

                if(fileTransferRole == "ADM")
                {
                    ListJobs = FileTransfer.GetAdministratorFileTransferInGrid(objAccount.ID, rowsPerPage, PageNr, orderField, orderDesc, searchText, searchSender, ref TotalRows, context);
                }
                else
                {
                    ListJobs = FileTransfer.GetFileTransferInGrid(objAccount.ID, loggedUserId, rowsPerPage, PageNr, orderField, orderDesc, searchText, searchSender, ref TotalRows, context);
                }
              
                

                SelectedInstance = "0";

                RowsPerPage = rowsPerPage;
                LoggedAccountTimeZone = objAccount.TimeZone;
            }

            //Constructor for File Transfer
            public FileTransferHistoryModel(Account objAccount, int loggedUserId, string folderPath, GMGColorContext context)
            {
                AccountId = objAccount.ID;
                LoggedUserId = loggedUserId;


                LoggedAccountTimeZone = objAccount.TimeZone;
            }

            #endregion

            public void UpdateDataSource(GMGColorContext context)
            {
                // search is made after Job title name
                var fileTransferRole = GMGColorDAL.User.GetRole(GMG.CoZone.Common.AppModule.FileTransfer, LoggedUserId, context);

                if (fileTransferRole == "ADM")
                {
                    ListJobs = FileTransfer.GetAdministratorFileTransferInGrid(AccountId, RowsPerPage, PageNr, OrderField, OrderDesc, _searchText,_searchSenderText, ref TotalRows, context);

                }
                else
                {
                    
                    ListJobs = FileTransfer.GetFileTransferInGrid(AccountId, LoggedUserId, RowsPerPage, PageNr, OrderField, OrderDesc, _searchText, _searchSenderText, ref TotalRows, context);
                }


            }
        }





        [Serializable]
        public class FileTransferDetailsModel
        {
            #region Properties

            public int ID { get; set; }
            public string Title { get; set; }
            public Account objLoggedAccount { get; set; }
            public int LoggedUserID { get; set; }

            public List<FileTransferDetailsCollaboratorActivity> ColloaboratorActivities { get; set; }
            public List<FileTransferDetailsCollaboratorActivity> ExternalColloaboratorActivities { get; set; }
            


            #endregion

            #region Constructors

            public FileTransferDetailsModel()
            {

            }


            #endregion
        }




        [Serializable]
        public class FileTransferDetailsCollaboratorActivity
        {
            #region Properties

            public int UserID { get; set; }

            public string Name { get; set; }

            public DateTime? Downloaded { get; set; }

            public string UserName { get; set; }

            public string EmailAddress { get; set; }
            public int UserStatus { get; set; }

            #endregion
        }


        
    }

}