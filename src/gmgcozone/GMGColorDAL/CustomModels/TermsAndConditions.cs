﻿using System.IO;
using System.Linq;
using GMGColorDAL.Common;

namespace GMGColorDAL.CustomModels
{
    public class TermsAndConditions
    {
        #region Properties

        public string SiteName { get; set; }
        public Locale.Languages Locale { get; set; }
        public string LanguageFilePath { get; set; }
        public string ModelBody { get; set; }

        #endregion

        #region Consctuctors

        public TermsAndConditions()
        {
        }

        public TermsAndConditions(int accountId, GMGColorContext context)
        {
            Account objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();

            this.SiteName = (objAccount.IsRemoveAllGMGCollaborateBranding) ? objAccount.SiteName : "CoZone";
            this.Locale = GMGColorDAL.Locale.GetLocale(objAccount.Locale, context);
            this.LanguageFilePath = this.GetLanguageFilePath();
            this.ModelBody = this.GetModelBody().Replace("<$sitename$>", this.SiteName);
        }

        #endregion

        #region Methods

        private string GetLanguageFilePath()
        {
            string folderPath = System.Web.HttpContext.Current.Server.MapPath("/") + "\\Content\\terms\\";
            string filePath = File.Exists(folderPath + this.Locale.ToString() + ".txt") ? folderPath + this.Locale.ToString() + ".txt" : folderPath + GMGColorDAL.Locale.Languages.English.ToString() + ".txt";
            return filePath;
        }

        private string GetModelBody()
        {
            return GMGColorCommon.ReadServerFile(this.LanguageFilePath);
        }

        #endregion
    }
}
