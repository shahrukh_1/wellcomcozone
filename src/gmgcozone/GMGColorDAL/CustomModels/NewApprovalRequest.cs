﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorDAL.CustomModels
{
    public struct ApprovalJob
    {
        public int ApprovalID { get; set; }
        public string ApprovalGUID { get; set; }
        public bool IsVideoFile { get; set; }
        public string FileTitle { get; set; }
        public string FileName { get; set; }
        public decimal FileSize { get; set; }
        public string FileGuid { get; set; }
        public int TotalNrOfPages { get; set; }
        public int ApprovalType { get; set; }
    }

    public class NewApprovalRequest
    {
        public GMGColorDAL.User User { get; set; }
        public Account Account{ get; set; }
        public List<ApprovalJob> Files { get; set; }
        public string LoggedUserTempFolderPath { get; set; }
        public List<PermissionsModel.UserOrUserGroup> UsersAndGroupsWhereThisIsApproval { get; set; }
        public string ExternalUsers { get; set; }
        public string ExternalEmails { get; set; }
        public bool IsUploadApproval { get; set; }
        public bool Separations { get; set; }
        public string Url { get; set; }
        public string WebPageSnapshotDelay { get; set; }
    }
}
