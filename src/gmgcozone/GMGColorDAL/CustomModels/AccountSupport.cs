﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AccountSupport
    {
        #region Properties

        [Required(ErrorMessageResourceName = "reqYourName", ErrorMessageResourceType = typeof(Resources))]
        public string YourName { get; set; }
        [Required(ErrorMessageResourceName = "reqEmail", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        public string Email { get; set; }
        [Required(ErrorMessageResourceName = "reqSubject", ErrorMessageResourceType = typeof(Resources))]
        public string Subject { get; set; }
        [Required(ErrorMessageResourceName = "reqMessage", ErrorMessageResourceType = typeof(Resources))]
        public string Message { get; set; }

        public string UserPathToTempFolder { get; set; }
        public string Region { get; set; }

        #endregion

        #region Constructors

        public AccountSupport()
        {
            Region = string.Empty;
            UserPathToTempFolder = string.Empty;
        }

        public AccountSupport(GMGColorDAL.User objUser, string region, string pathToTempFolder)
        {
            this.YourName = objUser.GivenName + " " + objUser.FamilyName;
            this.Email = objUser.EmailAddress;

            Region = region;
            UserPathToTempFolder = pathToTempFolder;
        }

        #endregion
    }
}
