﻿using GMGColor.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace GMGColorDAL.CustomModels.ApprovalWorkflows
{
    [Serializable]
    public class PhaseViewModel
    {
        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [Remote("IsApprovalPhaseNameUnique", "ApprovalWorkflow", AdditionalFields = "ID, ApprovalWorkflow, SelectedApprovalWorkflow, IsCopyPhase")]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string PhaseName { get; set; }
      
        public int ApprovalWorkflow { get; set; }

        public int Position { get; set; }

        [Required(ErrorMessageResourceName = "reqDecisionType", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public int SelectedDecisionType { get; set; }

        [Required(ErrorMessageResourceName = "reqApprovalWorkflow", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public int SelectedApprovalWorkflow { get; set; }

        [Required(ErrorMessageResourceName = "reqApprovalTrigger", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public int SelectedApprovalTrigger { get; set; }

        public List<AccountSettings.CollaborateApprovalStatusGeneralSettings> SelectedApprovalPhaseTrigger { get; set; }


        public string ExternalUsers { get; set; }

        public string InternalUsers { get; set; }
     
        public string ExternalEmails { get; set; }

        public bool IsCopyPhase { get; set; }

        public List<PermissionsModel.UserOrUserGroup> SelectedUsersAndGroups { get; set; }
        public List<DecisionMaker> DecisionMakers { get; set; }

        public List<KeyValuePair<int, string>> DecisionTypes { get; set; }
        public List<KeyValuePair<int, string>> ApprovalTriggers { get; set; }
     
        public ApprovalPhaseCollaboratorInfo ApprovalPhaseCollaboratorInfo { get; set; }

        public List<ApprovalWorkflowModel> ApprovalWorkflows { get; set; }

        public bool ShowAnnotationsToUsersOfOtherPhases { get; set; }

        public bool ShowAnnotationsOfOtherPhasesToExternalUsers { get; set; }

        public int? SelectedUnit { get; set; }

        public int? SelectedTrigger { get; set; }

        [Range(1, 99, ErrorMessageResourceName = "numberMustBeBetweenRange", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public int? SelectedUnitValue { get; set; }

        public int SelectedDeadlineType { get; set; }

        public string DeadlineDate { get; set; }

        public string DeadlineTime { get; set; }

        public string DeadlineTimeMeridiem { get; set; }

        public Account LoggedAccount { get; set; }

        public List<KeyValuePair<int, string>> PhaseDeadlineTriggerList { get; set; }

        public List<KeyValuePair<int, string>> PhaseDeadlineUnitList { get; set; }

        public bool ApprovalShouldBeViewedByAll { get; set; }

        public bool DecisionsShoulBeMadeByAll { get; set; }

        public bool isAdHocWorkflow { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string AdHocWorkflowName { get; set; }

        public bool RerunWorkflow { get; set; }

        #endregion

        public PhaseViewModel()
        {
            DecisionMakers =  new List<DecisionMaker>();
            ApprovalPhaseCollaboratorInfo = new ApprovalPhaseCollaboratorInfo();
            ApprovalWorkflows =  new List<ApprovalWorkflowModel>();
            ShowAnnotationsToUsersOfOtherPhases = true;
            PhaseDeadlineUnitList = new List<KeyValuePair<int, string>>();
            PhaseDeadlineTriggerList = new List<KeyValuePair<int, string>>();
            ApprovalShouldBeViewedByAll = false;
            DecisionsShoulBeMadeByAll = false;
            isAdHocWorkflow = false;
        }
    }

    public class DecisionMaker
    {
        #region Properties

        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsExternal { get; set; }
        public bool IsSelected { get; set; }

        #endregion
    }
   
    public class ApprovalPhaseCollaboratorInfo
    {
        #region Properties

        public List<int> Groups { get; set; }

        public List<string> CollaboratorsWithRole { get; set; }

        public List<string> ExternalCollaboratorsWithRole { get; set; }

        #endregion

        #region Methods

        public void LoadPhaseCollaborats(int pahseId, GMGColorContext context)
        {
            var collaboratorInfo = (from ap in context.ApprovalPhases
                                    let clbGroups = (from acg in context.ApprovalPhaseCollaboratorGroups
                                                     where acg.Phase == ap.ID
                                                     select acg.CollaboratorGroup).ToList()
                                    let clbWithRole = (from apc in context.ApprovalPhaseCollaborators
                                                       join u in context.Users on apc.Collaborator equals u.ID
                                                       join us in context.UserStatus on u.Status equals us.ID
                                                       join ur in context.UserRoles on u.ID equals ur.User
                                                       join r in context.Roles on ur.Role equals r.ID
                                                       let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                                       where apc.Phase == ap.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID
                                                       select SqlFunctions.StringConvert((double)apc.Collaborator).Trim() + "|" + SqlFunctions.StringConvert((double)apc.ApprovalPhaseCollaboratorRole).Trim()).ToList()
                                 
                                    let extWithRole = (from shap in context.SharedApprovalPhases
                                                       where shap.Phase == ap.ID
                                                       select SqlFunctions.StringConvert((double)shap.ExternalCollaborator).Trim() + "|" + SqlFunctions.StringConvert((double)shap.ApprovalPhaseCollaboratorRole).Trim()).ToList()
                                   
                                    where ap.ID == pahseId
                                    select new
                                    {
                                        Groups = clbGroups,
                                        CollaboratorsWithRole = clbWithRole,
                                        ExternalCollaboratorsWithRole = extWithRole
                                    }).FirstOrDefault();

            Groups = collaboratorInfo.Groups;
            CollaboratorsWithRole = collaboratorInfo.CollaboratorsWithRole;
            ExternalCollaboratorsWithRole = collaboratorInfo.ExternalCollaboratorsWithRole;
        }

        public void LoadJobPhasesCollaborators(int phaseId, int approval, GMGColorContext context)
        {
            var collaboratorInfo = (from ap in context.ApprovalJobPhases
                                    let clbGroups = (from acg in context.ApprovalCollaboratorGroups
                                                     where acg.Phase == ap.ID && acg.Approval == approval
                                                     select acg.CollaboratorGroup).ToList()
                                    let clbWithRole = (from apc in context.ApprovalCollaborators
                                                       join u in context.Users on apc.Collaborator equals u.ID
                                                       join us in context.UserStatus on u.Status equals us.ID
                                                       join ur in context.UserRoles on u.ID equals ur.User
                                                       join r in context.Roles on ur.Role equals r.ID
                                                       let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                                       where apc.Approval == approval && apc.Phase == ap.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID && apc.Phase == phaseId
                                                       select SqlFunctions.StringConvert((double)apc.Collaborator).Trim() + "|" + SqlFunctions.StringConvert((double)apc.ApprovalCollaboratorRole).Trim()).ToList()

                                    let extWithRole = (from shap in context.SharedApprovals
                                                       join ec in context.ExternalCollaborators on shap.ExternalCollaborator equals ec.ID
                                                       where shap.Phase == phaseId && shap.Approval == approval && !ec.IsDeleted
                                                       select SqlFunctions.StringConvert((double)shap.ExternalCollaborator).Trim() + "|" + SqlFunctions.StringConvert((double)shap.ApprovalCollaboratorRole).Trim()).ToList()

                                    where ap.ID == phaseId
                                    select new
                                    {
                                        Groups = clbGroups,
                                        CollaboratorsWithRole = clbWithRole,
                                        ExternalCollaboratorsWithRole = extWithRole
                                    }).FirstOrDefault();

            Groups = collaboratorInfo.Groups;
            CollaboratorsWithRole = collaboratorInfo.CollaboratorsWithRole;
            ExternalCollaboratorsWithRole = collaboratorInfo.ExternalCollaboratorsWithRole;
        }
        #endregion
    }

    [Serializable]
    public class ApprovalWorkflowPhasesInfo
    {
        public List<ApprovalWorkflowPhase> ApprovalWorkflowPhases { get; set; }

        public ApprovalWorkflowPhasesInfo()
        {
            ApprovalWorkflowPhases = new List<ApprovalWorkflowPhase>();
        }
    }

    public class ApprovalWorkflowPhase
    {
        public int Phase { get; set; }
        public int DecisionType { get; set; }
        public int? PDMId { get; set; }
        public bool IsExternal { get; set; }
        public ApprovalPhaseCollaboratorInfo PhaseCollaborators { get; set; }
    }

	public class ApprovalWorkflowPhasePerf
	{
		public ApprovalJobPhase Phase { get; set; }
		public int DecisionType { get; set; }
		public int? PDMId { get; set; }
		public bool IsExternal { get; set; }
		public ApprovalPhaseCollaboratorInfo PhaseCollaborators { get; set; }
	}

	public class ApprovalWorkflowPhasesInfoPerf
	{
		public List<ApprovalWorkflowPhasePerf> ApprovalWorkflowPhases { get; set; }

		public ApprovalWorkflowPhasesInfoPerf()
		{
			ApprovalWorkflowPhases = new List<ApprovalWorkflowPhasePerf>();
		}
	}

}
