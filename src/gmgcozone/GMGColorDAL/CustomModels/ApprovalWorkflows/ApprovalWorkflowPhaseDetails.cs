﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.ApprovalWorkflows
{
    public class ApprovalWorkflowPhaseDetails
    {
        public Approval Approval { get; set; }
        public int? PhaseID { get; set; }
        public List<string> PhaseTrigger { get; set; }
        public IEnumerable<CollaboratorDetails> Collaborators { get; set; }
    }

    public class CollaboratorDetails
    {
        public int UserId { get; set; }
        public bool HasViewed { get; set; }
        public bool IsPDM { get; set; }
        public string Decision { get; set; }
        public bool IsExternal { get; set; }
    }
}
