﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels.ApprovalWorkflows
{
    public class ApprovalWorkflowDashboardModel
    {
        #region Properties
       
        public string SearchText { get; set; }
        public List<ApprovalWorkflowModel> ApprovalWorkflows { get; set; }
        public ApprovalWorkflowModel ApprovalWorkflow { get; set; }

        #endregion

        #region Constructors

        public ApprovalWorkflowDashboardModel()
        {
            ApprovalWorkflows = new List<ApprovalWorkflowModel>();
            ApprovalWorkflow = new ApprovalWorkflowModel();
        }
        #endregion
    }

    public class ApprovalWorkflowPhasesDashboardModel
    {
        #region Properties

        public int Workflow { get; set; }
        public List<PhaseDetails> ApprovalWorkflowPhaseDetails { get; set; }

        #endregion

        #region Constructors

        public ApprovalWorkflowPhasesDashboardModel()
        {
            ApprovalWorkflowPhaseDetails = new List<PhaseDetails>();
        }
        #endregion
    }

    public class PhaseDetails
    {
        #region Properties

        public int ID { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string DecisionType { get; set; }
        public string PrimaryDecisionMaker { get; set; }
        public string ApprovalTrigger { get; set; }

        #endregion
    }

    [Serializable]
    public class ApprovalWorkflowModel
    {
        #region Properties

        public int ID { get; set; }

        public int Account { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Remote("IsApprovalWorkflowName_Available", "ApprovalWorkflow", AdditionalFields = "ID")]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        public bool HasPhases { get; set; }

        public bool RerunWorkflow { get; set; }

        #endregion
    }

    [Serializable]
    public class AdHocWorkflowDetails {
        public int WorkflowID { get; set; }
        public int PhaseID { get; set; }
        public string WorkflowName { get; set; }
        public string PhaseName { get; set; }
    }
}
