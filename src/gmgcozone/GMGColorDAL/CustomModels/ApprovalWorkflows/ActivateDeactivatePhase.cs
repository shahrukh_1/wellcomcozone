﻿namespace GMGColorDAL.CustomModels.ApprovalWorkflows
{
    public class ActivateDeactivatePhase
    {
        public int Phase { get; set; } 
        public bool ActivatePhase { get; set; }
        public int ApprovalId { get; set; }
        public int LoggedUserId { get; set; }
        public Account LoggedAccount { get; set; }
        public bool UpdateStatusAndRerun { get; set; }
    }
}
