﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.ApprovalWorkflows
{
    public class ReRunWorkflowDetails
    {
        public bool ApprovalIsLocked { get; set; }
        public List<ApprovalCollaboratorDecision> CollabDecisionsToRest { get; set; }
        public List<ApprovalJobPhaseApproval> PhasesToReset { get; set; }
        public List<ApprovalUserViewInfo> PhaseOpenedDatesToReset { get; set; }
        public InternalExternalPdm InternalExternalPdm { get; set; }
    }
}
