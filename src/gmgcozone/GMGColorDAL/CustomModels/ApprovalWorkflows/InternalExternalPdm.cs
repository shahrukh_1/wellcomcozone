﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.ApprovalWorkflows
{
    public class InternalExternalPdm
    {
        public int? InternalPdm { get; set; }
        public int? ExternalPdm { get; set; }
    }
}
