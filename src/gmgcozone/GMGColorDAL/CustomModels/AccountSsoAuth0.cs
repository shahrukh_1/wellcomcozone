﻿namespace GMGColorDAL.CustomModels
{
    public class AccountSsoAuth0
    {
        public string Domain { get; set; }
        public string ClientId { get; set; }
        public string Type { get; set; }
    }
}
