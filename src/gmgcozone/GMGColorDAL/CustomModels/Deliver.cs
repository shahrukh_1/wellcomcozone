﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public class Deliver : Common
    {
        public class AddInstance: Common
        {
            #region Properties

            public List<ColorProofInstance> ListServers { get; set; }

            [Required(ErrorMessageResourceName = "reqCPRegistration", ErrorMessageResourceType = typeof(Resources))]
            public int? SelectedCPServerId { get; set; }

            [Required(ErrorMessageResourceName = "reqCPWorkflow", ErrorMessageResourceType = typeof(Resources))]
            public int? SelectedCPWorkflowId { get; set; }

            public List<ColorProofWorkflow> ListWorkflows { get; set; }

            public string PagesRadioButton { get; set; }

            [Required(ErrorMessageResourceName = "reqRangeValue", ErrorMessageResourceType = typeof(Resources))]
            public string PagesRangeHiddenField { get; set; }

            [Required(ErrorMessageResourceName = "reqRangeValueMaxPageCount", ErrorMessageResourceType = typeof(Resources))]
            public string PagesRangeMaxPageCountHiddenField { get; set; }

            [RegularExpression(@"^(\s*\d+\s*\-\s*\d+\s*,?|\s*\d+\s*,?)+$", ErrorMessageResourceName = "reqPages", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string PagesRangeValue { get; set; }

            public bool IncludeProofMetaInformation { get; set; }

            public bool LogProofControlResults { get; set; }

            public string PathToTempFolder { get; set; }

            public bool ShowUploader { get; set; }

            public bool ShowModalPopup { get; set; }

            public string SelectedApprovalJobs { get; set; }

            public string ExternalUsers { get; set; }
            public string ExternalEmails { get; set; }
            [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
            public string OptionalMessage { get; set; }
            public string ExternalUsersEvents { get; set; }

            public int PageNr { get; set; }
            public int RowsPerPage { get; set; }

            [Required(ErrorMessageResourceName = "reqDeliverJobTtile", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
            public string FileTitleValidator { get; set; }

            public string ErrorMessage { get; set; }

            #endregion
            
            #region Constructors

            public AddInstance(Account objAccount, int loggedUserId, string folderPath, GMGColorContext context) : base(objAccount, context)
            {
                try
                {
                    PathToTempFolder = folderPath;

                    List<ColorProofCoZoneWorkflow> activatedWorkflows =
                        (from cpczw in context.ColorProofCoZoneWorkflows
                         where cpczw.IsDeleted == false && cpczw.IsAvailable == true
                         select cpczw).ToList();

                    GMGColorDAL.User loggedUser = (from u in context.Users where u.ID == loggedUserId select u).FirstOrDefault();

                    if (loggedUser == null)
                        throw new Exception("User with ID='" + loggedUserId + "' could not be found in database");

                    List<int> lstUserGroups = loggedUser.UserGroupUsers.Select(t => t.UserGroup).ToList();

                    List<ColorProofCoZoneWorkflow> securedList = new List<ColorProofCoZoneWorkflow>();
                    foreach (ColorProofCoZoneWorkflow colorProofCoZoneWorkflow in activatedWorkflows)
                    {
                        List<int> groups = colorProofCoZoneWorkflow.ColorProofCoZoneWorkflowUserGroups.Select(o => o.UserGroup).ToList();
                        if (groups.Any(lstUserGroups.Contains))
                            securedList.Add(colorProofCoZoneWorkflow);
                    }

                    ListServers = securedList.Select(t => t.ColorProofWorkflow1.ColorProofInstance1).ToList();

                    // filter the instances that has at least one activated workflow
                    ListServers = ListServers.Where(o => o.ColorProofWorkflows.Any(w => w.IsActivated == true) && o.ColorProofInstanceState.Key == (int)ColorProofInstance.ColorProofInstanceStateEnum.Running).ToList();

                    ListServers = ListServers.Distinct(new ColorProofInstanceEqualityComparer()).OrderBy(o => o.SystemName).ToList();

                    ListWorkflows = new List<ColorProofWorkflow>();
                }
                finally
                {
                }
            }

            public AddInstance()
            {
            }

            #endregion

            #region Sub Class
            #endregion
        }

        public class JobDetailsPage
        {
            public string PageName { get; set; }
            public string PageResultMessage { get; set; }
            public DeliverPageStatus Status { get; set; }
            public List<JobDetailVerificationResult> Results { get; set; }

            public JobDetailsPage()
            {
                PageName = string.Empty;
                PageResultMessage = string.Empty;
                Results = new List<JobDetailVerificationResult>();
            }
    }

        public class JobDetailVerificationResult
        {
            public bool? VerificationResult { get; set; }
            public bool? IsSpotColorVerification { get; set; }
            public double? MaxDelta { get; set; }
            public double? AllowedMaxDelta { get; set; }
            public double? AvgDelta { get; set; }
            public double? AllowedAvgDelta { get; set; }
            public string StripName { get; set; }

            public JobDetailVerificationResult()
            {
                StripName = string.Empty;
            }
        }

        public class JobDetailsInstance: Common
        {
            #region Properties

            public string Id { get; set; }
            public string JobName { get; set; }
            public string JobStatus { get; set; }
            public string Location { get; set; }
            public string ProofControlInline { get; set; }
            public string Filename { get; set; }
            public string ColorProofId { get; set; }
            public string Owner { get; set; }
            public string Pages { get; set; }

            public string ColorProofServerName { get; set; }
            public string WorkflowName { get; set; }
            public string ProofStandard { get; set; }
            public string SpotColors { get; set; }
            public string MaximumUsablePaperWidth { get; set; }
            public int? PageNr { get; set; }
            public int? RowsPerPage { get; set; }
            public string SearchText { get; set; }

            public bool IsSharedJob { get; set; }
            public string Username { get; set; }
            public string Email { get; set; }
            public string AccountUrl { get; set; }

            public List<JobDetailsPage> ResultsPages { get; set; }
            public List<AccountSettings.DeliverExternalUser> DeliverExternalUsers { get; set; } 

            #endregion

            #region Constructors
            public JobDetailsInstance(Account objAccount, GMGColorContext context): base(objAccount, context)
            {
                ResultsPages = new List<JobDetailsPage>();
                DeliverExternalUsers = new List<AccountSettings.DeliverExternalUser>();
            }

            public JobDetailsInstance()
            {
                ResultsPages = new List<JobDetailsPage>();
            }

            #endregion
        }

        [Serializable]
        public class DeliverModel:Common
        {
            #region Private Data

            private string _searchText;

            #endregion
 
            #region Properties
            public List<ReturnDeliversPageView> ListJobs { get; set; }
            public bool ValidServerAvailable { get; set; }
            public JobDetailsInstance JobDetails { get; set; }
            public string SelectedInstance { get; set; }
            public int PageNr { get; set; }
            public int OrderField { get; set; }
            public bool OrderDesc { get; set; }
            public int LoggedUserId { get; set; }
            public int AccountId { get; set; }
            public int RowsPerPage;
            public int TotalRows;
            public string LoggedAccountTimeZone { get; set; }
            public bool IsJobNameSelectable { get; set; }
            public Role.RoleName DeliverRole { get; set; }

            public string SearchText
            {
                get { return _searchText; }
                set
                {
                    _searchText = value;
                }
            }
            public AddInstance PopupModel { get; set; }

            #endregion

            #region Constructors

            public DeliverModel()
            {
            }

            //Constructor for Deliver Jobs Grid
            public DeliverModel(Account objAccount, int loggedUserId, string folderPath, int pageNr, int orderField, bool orderDesc, GMGColorContext context, int rowsPerPage = 10, string searchText = "")
                : base(objAccount, context)
            {
                AccountId = objAccount.ID;
                LoggedUserId = loggedUserId;
                PageNr = pageNr;

                this.OrderField = orderField;
                this.OrderDesc = orderDesc;
                ListJobs = DeliverJob.GetDeliverJobsInGrid(objAccount.ID, loggedUserId, rowsPerPage, PageNr, orderField, orderDesc, searchText, ref TotalRows, context);

                SelectedInstance = "0";

                RowsPerPage = rowsPerPage;
                LoggedAccountTimeZone = objAccount.TimeZone;
            }

            //Constructor for New Deliver 
            public DeliverModel(Account objAccount, int loggedUserId, string folderPath, GMGColorContext context)
            {
                AccountId = objAccount.ID;
                LoggedUserId = loggedUserId;

                PopupModel = new AddInstance(objAccount, loggedUserId, folderPath, context) { ShowUploader = true };
                LoggedAccountTimeZone = objAccount.TimeZone;
            }

            #endregion

            public void UpdateDataSource(GMGColorContext context)
            {
                // search is made after Job title name
                ListJobs = DeliverJob.GetDeliverJobsInGrid(AccountId, LoggedUserId, RowsPerPage, PageNr, OrderField, OrderDesc, _searchText, ref TotalRows, context);
            }
        }
    }
}
