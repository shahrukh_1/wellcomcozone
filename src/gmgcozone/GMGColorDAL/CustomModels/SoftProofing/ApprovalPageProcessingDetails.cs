﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class ApprovalPageProcessingDetails
    {
        #region Nested Class

        public class CustomICCProfileDetails
        {
            #region Properties

            public bool IsGlobal { get; set; }
            public string Guid { get; set; }
            public string AccGuid { get; set; }
            public string Name { get; set; }

            #endregion
        }

        public class SimulationICCProfileDetails
        {
            #region Properties

            public bool IsGlobal { get; set; }
            public string Guid { get; set; }
            public string AccGuid { get; set; }
            public string Name { get; set; }
            public int MeasurementCond { get; set; }

            #endregion
        }

        public class PaperTintValues 
        {
            public double L { get; set; }

            public double a { get; set; }

            public double b { get; set; }
        }

        #endregion
        #region Properties

        public string JobGuid { get; set; }

        public string FileName { get; set; }

        public string Region { get; set; }

        public int PageNumber { get; set; }

        public int Resolution { get; set; }

        public List<ApprovalSeparationPlate> PageChannels { get; set; }

        public int PageWidth { get; set; }

        public int PageHeight { get; set; }

        public string  DocumentEmbeddedProfile { get; set; }

        public int ApprovalId { get; set; }

        public CustomICCProfileDetails CustomProfileDetails { get; set; }

        public SimulationICCProfileDetails SimulationProfileDetails { get; set; }

        public PaperTintValues PaperTintWhite { get; set; }

        #endregion
    }
}
