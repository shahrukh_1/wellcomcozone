﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public class ApprovalDefaultViewingConditionsModel
    {
        #region Properties

        public int? SelectedSimulationProfile { get; set; }

        public int? SelectedPaperTint { get; set; }

        public List<SimulationProfileModel> SimulationProfiles { get; set; }
 
        public List<CustomPaperTintModel> PaperTints { get; set; } 

        #endregion

        #region Constructors

        public ApprovalDefaultViewingConditionsModel()
        {
            
        }

        public ApprovalDefaultViewingConditionsModel(int jobId, int accId, GMGColorContext context)
        {
            var existingViewingConditions = (from jobvwcnd in context.ApprovalJobViewingConditions
                                            join vwcnd in context.ViewingConditions on jobvwcnd.ViewingCondition equals vwcnd.ID
                                            where jobvwcnd.Job == jobId
                                            select new
                                            {
                                                SelectedPaperTint = vwcnd.PaperTint,
                                                SelectedProfile = vwcnd.SimulationProfile
                                            }).FirstOrDefault();

            if (existingViewingConditions != null)
            {
                SelectedSimulationProfile = existingViewingConditions.SelectedProfile;
                SelectedPaperTint = existingViewingConditions.SelectedPaperTint;
            }

            SimulationProfiles = (from smp in context.SimulationProfiles
                                  join msrm in context.MeasurementConditions on smp.MeasurementCondition equals msrm.ID
                                  where smp.IsValid.HasValue && smp.IsDeleted != true && smp.IsValid.Value && (smp.AccountId == accId || smp.Account == null && !context.ExcludedAccountDefaultProfiles.Any(t => t.Account == accId && t.SimulationProfile == smp.ID))
                                    select new SimulationProfileModel
                                    {
                                        ID = smp.ID,
                                        Name = smp.ProfileName,
                                        MeasurementCondition = msrm.Name,
                                        MediaCategory = smp.MediaCategory.HasValue ? (from mct in context.MediaCategories
                                                                             where smp.MediaCategory == mct.ID
                                                                             select mct.Name).FirstOrDefault()
                                                                             : null,
                                        L = smp.L,
                                        a = smp.a,
                                        b = smp.b
                                    }).ToList();

            PaperTints = (from ppt in context.PaperTints
                        join mdcat in context.MediaCategories on ppt.MediaCategory equals mdcat.ID
                        where ppt.AccountId == accId
                        select new CustomPaperTintModel
                        {
                            ID = ppt.ID,
                            Name = ppt.Name,
                            Measurements =  (from pptmsrm in context.PaperTintMeasurements 
                                                    join msrm in context.MeasurementConditions on pptmsrm.MeasurementCondition equals msrm.ID
                                                    where pptmsrm.PaperTint == ppt.ID
                                                    select new CustomPaperTintMeasurements
                                                    {
                                                        MeasurementCondition = msrm.Name,
                                                        L = pptmsrm.L,
                                                        a = pptmsrm.a,
                                                        b = pptmsrm.b
                                                    }).ToList(),
                            MediaCategory = mdcat.Name
                        }).ToList();
        }

        #endregion
    }

    public class SimulationProfileModel : IDropDownExtItem
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string MediaCategory { get; set; }

        public string MeasurementCondition { get; set; }

        public double? L { get; set; }

        public double? a { get; set; }

        public double? b { get; set; }

        #region IDropDownExtItem implementation

        public IDictionary<string, string> GetHtmlAttributes()
        {
            IDictionary<string, string> htmlAttributes = new Dictionary<string, string>();
            htmlAttributes.Add("data-mediaCategory", MediaCategory);
            htmlAttributes.Add("data-measurementCondition", MeasurementCondition);
            htmlAttributes.Add("data-L", L.ToString());
            htmlAttributes.Add("data-a", a.ToString());
            htmlAttributes.Add("data-b", b.ToString());
            return htmlAttributes;
        }
        #endregion
    }

    public class CustomPaperTintModel : IDropDownExtItem
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string MediaCategory { get; set; }

        public List<CustomPaperTintMeasurements> Measurements { get; set; }

        #region IDropDownExtItem implementation

        public IDictionary<string, string> GetHtmlAttributes()
        {
            IDictionary<string, string> htmlAttributes = new Dictionary<string, string>();
            htmlAttributes.Add("data-mediaCategory", MediaCategory);
            return htmlAttributes;
        }
        #endregion
    }

    public class CustomPaperTintMeasurements
    {
        public string MeasurementCondition { get; set; }

        public double L { get; set; }

        public double a { get; set; }

        public double b { get; set; }
    }
}
