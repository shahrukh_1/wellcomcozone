﻿namespace GMGColorDAL.CustomModels
{
    public class DeliverExternalCollaboratorPreset
    {
        public int PresetId { get; set; }

        public DeliverExternalCollaborator DeliverExternalCollaborator { get; set; }

        public DeliverExternalCollaboratorPreset()
        {
            DeliverExternalCollaborator = new DeliverExternalCollaborator();
        }
    }
}
