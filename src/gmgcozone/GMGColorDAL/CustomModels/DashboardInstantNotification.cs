﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.Api;

namespace GMGColorDAL.CustomModels
{
    public interface IInstantNotification
    {
        
    }

    public class BaseInstantNotification : IInstantNotification
    {
        public int ID { get; set; }
        public int? Creator { get; set; }
        public int? ExternalCreator { get; set; }
        public string Version { get; set; }
    }

    public class DashboardInstantNotification
    {
        public int ID { get; set; }
        public int? Creator { get; set; }
        public int? ExternalCreator { get; set; }
        public string Version { get; set; }
        public InstantNotificationEntityType EntityType { get; set; }
        public string ApprovalName { get; set; }
        public string Decision { get; set; }
        public string DecisionMaker { get;set; }
        public string DecisionDisplayText { get; set; }
        public string DecisionLabelColorName { get; set; }
        public string PDM { get; set; }
        public string Owner { get; set; }
        public bool IsPhaseDeadline { get; set; }

        public string DeadLineDate { get; set; }
        public string DeadLineTime { get; set; }
        public OverdueTimeDifference OverdueTimeDifference { get; set; }
        public bool IsOverdue { get; set; }
    }

    public class ApprovalWorkflowInstantNotification : DashboardInstantNotification
    {
        public string PhaseName { get; set; }     
        public string WorkflowTooltip { get; set; }
        public string NextPhase { get; set; }
        public string PrimaryDecisionMaker { get; set; }
        public int DecisionType { get; set; }
        public bool IsPhaseComplete { get; set; }
        public bool MoveNext { get; set; }
    }
}
