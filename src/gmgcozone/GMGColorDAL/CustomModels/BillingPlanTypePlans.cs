﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class BillingPlanTypePlans
    {
        #region Properties

        public int BillingPlanTypeID { get; set; }
        public string BillingPlanTypeName { get; set; }
        public int BillingPlanTypeAppModuleID { get; set; }
        public int BillingPlanTypeAppModuleKey { get; set; }
        public List<BillingPlanInfo> BillingPlans { get; set; }

        #endregion

        #region Constructor

        public BillingPlanTypePlans()
        {
            this.BillingPlans = new List<BillingPlanInfo>();
        }

        #endregion
    }
}
