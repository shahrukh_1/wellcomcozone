﻿using System;
using System.Linq;
using GMGColorDAL.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class Plans : Common
    {
        #region Properties

        public BillingPlan objBillingPlan { get; set; }
        public BillingPlanType objBillingPlanType { get; set; }
        public CurrencyRate objCurrencyRate { get; set; }
        public Account objAccount { get; set; }
        public AccountBillingPlans objEditPlanPrice { get; set; }
        public string CurrencyConversionRateString { get; set; }
        public decimal CurrencyConversionRate { get; set; }
        public string CurrencySymbol { get; set; }
        public bool IsPrePressOptionDisabled { get; set; }
        public bool IsMultimediaOptionDisabled { get; set; }
        public bool HasNewCollaborateBillingPlan { get; set; }

        #endregion

        #region Constructors

        public Plans()
        {

        }

        public Plans(AccountType.Type loggedAccountType, Account loggedAccount, GMGColorContext context, bool isEditPlanPrice = true)
        {
            this.objBillingPlan = new BillingPlan();
            this.objBillingPlanType = new BillingPlanType();
            this.objCurrencyRate = new CurrencyRate();
            this.objLoggedAccount = loggedAccount;
            this.objLoggedAccountType = loggedAccountType;

            this.objAccount = loggedAccount;

            if (this.objLoggedAccountType == AccountType.Type.GMGColor)
            {
                this.CurrencyConversionRate = 1;
                CurrencySymbol = objAccount.Currency.Symbol;
            }
            else
            {
                this.CurrencyConversionRate = (this.objLoggedAccount.ParentAccount != null &&
                                               this.objLoggedAccount.ParentAccount.CurrencyFormat !=
                                               this.objLoggedAccount.CurrencyFormat)
                                                  ? (new GMGColorCommon()).GetUpdatedCurrencyRate(this.objAccount,
                                                                                                  context)
                                                  : 1;
                this.CurrencyConversionRateString = "1 " + objAccount.ParentAccount.Currency.Code + " = " + this.CurrencyConversionRate + " " + objAccount.Currency.Code;
                CurrencySymbol = objAccount.ParentAccount.Currency.Symbol;
            }

            if (isEditPlanPrice)
            {
                this.objEditPlanPrice = Account.GetAccountBillingPlanPriceInfo(loggedAccount, false, context);
            }
            else if (objLoggedAccountType == AccountType.Type.Client)
            {
                var collaborateBillingPlantypeInfo = (from acs in context.AccountSubscriptionPlans
                                                        join bp in context.BillingPlans on acs.SelectedBillingPlan equals bp.ID
                                                        join bpt in context.BillingPlanTypes on bp.Type equals bpt.ID
                                                        join apm in context.AppModules on bpt.AppModule equals apm.ID
                                                        where acs.Account == objLoggedAccount.ID && apm.Key == (int) GMG.CoZone.Common.AppModule.Collaborate
                                                        select new
                                                        {
                                                            bpt.EnableMediaTools,
                                                            bpt.EnablePrePressTools,
                                                            bp.MaxUsers,
                                                            bp.MaxStorageInGb
                                                        }).FirstOrDefault();

                if (collaborateBillingPlantypeInfo != null)
                {
                    IsMultimediaOptionDisabled = !collaborateBillingPlantypeInfo.EnableMediaTools;
                    IsPrePressOptionDisabled = !collaborateBillingPlantypeInfo.EnablePrePressTools;
                    HasNewCollaborateBillingPlan = collaborateBillingPlantypeInfo.MaxUsers != null &&
                                                   collaborateBillingPlantypeInfo.MaxStorageInGb != null;
                }
            }
        }

        #endregion
    }

    public class NewCollaborateBillingPlanViewModel
    {
        public int ID { get; set; }
        
        [Required]
        public string ProductType { get; set; }
        
        [Required]
        public List<BillingPlanTypeViewModel> BillingPlanTypes { get; set; }

        [Required(ErrorMessage = "The Plan Type is required")]
        public int SelectedBilingPlanType { get; set; }

        [Required(ErrorMessage = "The Plan Name is required")]
        [StringLength(50)]
        public string BillingPlanName { get; set; }
        public bool isFixedPlan { get; set; }
        public bool isDemoPlan { get; set; }
        
        [Required(ErrorMessage="The Max Number is required")]
        [RangeAttribute(1, int.MaxValue, ErrorMessage = "The value must be higher than 0")]
        public int? MaxUsers { get; set; }

        [Required(ErrorMessage="The Max Storage in GB is required")]
        [RangeAttribute(1, int.MaxValue, ErrorMessage="The value must be higher than 0")]
        public int? MaxStorageInGb { get; set; }

        public decimal Price { get; set; }
        public string Currency { get; set; }
        public int? ColorProofConnections { get; set; }
    }

    public class BillingPlanTypeViewModel
    {
        public int ID { get; set; }
        public int MediaService { get; set; }
        public string Name { get; set; }
        public bool EnablePrePressTools { get; set; }
        public bool EnableMediaTools { get; set; }
    }
}
