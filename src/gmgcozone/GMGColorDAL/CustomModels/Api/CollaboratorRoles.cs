﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class CollaboratorRolesRequest
    {
        public string key { get; set; }
        public string locale_name { get; set; }
    }

    public class CollaboratorRolesResponse
    {
        public List<CollaboratorRole> collaborator_roles { get; set; }
    }

    public class CollaboratorRole
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
    }
}
