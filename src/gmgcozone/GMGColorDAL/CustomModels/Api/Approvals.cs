﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using GMG.CoZone.Proofstudio.DomainModels.Requests;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;

namespace GMGColorDAL.CustomModels.Api
{
    public enum InstantNotificationEntityType
    {
        Annotation = 1,
        Decision = 2,
        Collaborator = 3,
        ViewingConditions = 4,
        VersionSwitchedToHighRes = 5
    }

    public enum InstantNotificationOperationType
    {
        Added = 1,
        Modified = 2,
        Deleted = 3
    }

    public class BaseGetEntityRequest : BaseGetEntityRequestModel
    {

    }
    public class ApprovalRequest : ApprovalRequestModel
    {
    }

    //public class ApprovalsRequest : BaseGetEntityRequest
    //{
    //    private new int? Id { get; set; }
    //    public int approvalId { get; set; }
    //}

    public class CustomProfilesRequest : BaseGetEntityRequest
    {
        public string versionsIds { get; set; }
        public int nextProfileId { get; set; }
    }

    public class ApprovalPageRequest : BaseGetEntityRequest
    {
        public int? pageId { get; set; }
    }

    public class ProjectApprovalRequest : BaseGetEntityRequest
    {
        public int Page { get; set; }
        public List<string> SelectedApprovalTypes { get; set; }
        public int Top { get; set; }
    }

    public class ApprovalZoomLevelRequest : BaseGetEntityRequest
    {
        public int ZoomLevel { get; set; }
        public int ApprovalId { get; set; }
        
    }
    public class HtmlPageRequest : BaseGetEntityRequest
    {
        public int Page { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public int? ApprovalId { get; set; }
        public int? ProjectID { get; set; }
        public string SelectedApprovalTypes { get; set; }
        public string SelectedTagwords { get; set; }
    }

    public class ApprovalsInTreeRequest : BaseGetEntityRequest
    {
        public int TreeFolderId { get; set; }
        public int Page { get; set; }
    }

    public class GenerateTilesRequest : ApprovalPageRequest
    {
        public string SessionGuid { get; set; }
    }

    public class GenerateHighResRequest : BaseGetEntityRequest
    {
        public string SessionGuid { get; set; }
    }

    public class TilesPageRequest : ApprovalPageRequest
    {
        public string OutputProfileName { get; set; }
        public string ActiveChannels { get; set; }
        public bool SimulatePaperTint { get; set; }
        public bool DefaultProfile { get; set; }
        public int ZoomLevel { get; set; }
        public string ViewTiles { get; set; }
        public int? SimulationProfileId { get; set; }
        public int? EmbeddedProfileId { get; set; }
        public int? CustomProfileId { get; set; }
        public int? PaperTintId { get; set; }
        public bool HighResolution { get; set; }
        public string OutputRgbProfileName { get; set; }
    }

    public class PageSoftProofingSession : BaseGetEntityRequest
    {
        public string SoftProofingSessionGUID { get; set; }
        public int PageID { get; set; }
    }

    [Serializable]
    public class SoftProofingStatusRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public string machineId { get; set; }
    }

    [Serializable]
    public class AccountSettingsData
    {
        public int AccountId { get; set; }
        public string ProofStudioBackgroundColor { get; set; }
    }

    [Serializable]
    public class AccountSettingsResponse
    {
        public AccountSettingsResponse()
        {
            AccountSetting = new AccountSettingsData();
        }
        public AccountSettingsData AccountSetting { get; set; }
    }

    [Serializable]
    public class AccountSettingsRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public AccountSettingsData AccountSetting { get; set; }
    }

    public enum SoftProofingLevel
    {
        None = 0,
        Calibrated = 1,
        NotCalibrated = 2,
        Expired = 3
    }

    public class SoftProofingSession
    {
        public SoftProofingSession()
        {
            Enabled = false;
            Level = SoftProofingLevel.Expired;
            MonitorName = string.Empty;
        }

        public int ID { get; set; }
        public bool Enabled { get; set; }
        public SoftProofingLevel Level { get; set; }
        public string MonitorName { get; set; }
        public int DisplayWidth { get; set; }
        public int DisplayHeight { get; set; }
    }

    public class SoftProofingStatusResponse
    {
        public SoftProofingSession Softproofingstatus { get; set; }
    }

    [Serializable]
    public class AnnotationRequest
    {
        public ApprovalAnnotationSet annotation { get; set; }
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public string session_key { get; set; }
        public bool is_external_user { get; set; }
        public string machineId { get; set; }
        public int? substituteLoggedUser { get; set; }
    }

    [Serializable]
    public class AnnotationResponse
    {
        public ApprovalAnnotationGet annotation { get; set; }
        public ApprovalAnnotationShape[] custom_shapes { get; set; }
        public ApprovalAnnotationAttachment[] AnnotationAttachments { get; set; }
        public List<ViewingConditionApiModel> viewing_conditions { get; set; }
        public bool DisplayChangesCompletePopup { get; set; }
        public ApprovalChecklistItem[] ApprovalannotationChecklistitems { get; set; }
        public ApprovalAnnotationChecklistItemDetail[] AnnotationAllchecklistitems { get; set; }

    }

    [Serializable]
    public class CopyAnnotationRequest
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public int? destinationPageId { get; set; }
        public bool is_external_user { get; set; }
    }


    [Serializable]
    public class CopyAnnotationResponse
    {
        public string ErrorMessage { get; set; }
        public List<AnnotationResponse> annotation { get; set; }

        public CopyAnnotationResponse()
        {
            annotation = new List<AnnotationResponse>();
        }
    }

    [Serializable]
    public class UpdateApprovalDecision
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public string session_key { get; set; }
        public int? collaboratorId { get; set; }
        public bool isExternal { get; set; }
        public int? decisionId { get; set; }
        public int? substituteLoggedUser { get; set; }
    }

    [Serializable]
    public class UpdateApprovalDecisionResponse
    {
        public Decision decision { get; set; }
        public List<ApprovalCollaboratorLink> internal_collaborators_links { get; set; }
        public List<ApprovalCollaboratorLink> external_collaborators_links { get; set; }
        public List<Api.UserGroup> user_groups { get; set; }
        public List<UserGroupDecision> user_group_links { get; set; }

        public UpdateApprovalDecisionResponse()
        {
            decision = new Decision();
            internal_collaborators_links = new List<ApprovalCollaboratorLink>();
            user_groups = new List<UserGroup>();
            user_group_links = new List<UserGroupDecision>();
        }
    }

    [Serializable]
    public class DecisionResponseItem
    {
        public int? Id { get; set; }
        public int? Decision { get; set; }
    }

    [Serializable]
    public class UpdateVersionLocking
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public UpdateVersionData[] data { get; set; }
    }

    public class UpdateVersionData
    {
        public int Id { get; set; }
        public bool isLocked { get; set; }
    }

    [Serializable]
    [ObsoleteAttribute("This method is obsolete. Call UserGroupModel instead.", false)]
    public class UserGroup : UserGroupModel
    {

    }

    [Serializable]
    [ObsoleteAttribute("This method is obsolete. Call UserGroupDecisionModel instead.", false)]
    public class UserGroupDecision: UserGroupDecisionModel
    {

    }

    public class VersionResponse
    {
        public ApprovalVersion version { get; set; }
        public List<ApprovalCollaborator> external_collaborators { get; set; }
        public List<ApprovalCollaborator> internal_collaborators { get; set; }
        public List<ApprovalCollaboratorLink> internal_collaborators_links { get; set; }
        public List<ApprovalCollaboratorLink> external_collaborators_links { get; set; }
        public List<UserGroup> user_groups { get; set; }
        public List<UserGroupDecision> user_group_links { get; set; }
        public List<ApprovalAnnotationAttachment> AnnotationAttachments { get; set; }
        public List<SoftProofingSessionParams> softProofingParams { get; set; }

        public VersionResponse()
        {
            external_collaborators = new List<ApprovalCollaborator>();
            internal_collaborators = new List<ApprovalCollaborator>();
            internal_collaborators_links = new List<ApprovalCollaboratorLink>();
            external_collaborators_links = new List<ApprovalCollaboratorLink>();
            softProofingParams = new List<SoftProofingSessionParams>();

            user_groups = new List<UserGroup>();
            user_group_links = new List<UserGroupDecision>();
        }
    }

    [ObsoleteAttribute("This method is obsolete. Call ApprovalResponseModel instead.", false)]
    public class ApprovalResponse // TODO : to be removed when the response is in service
    {
        public List<Approval> approvals { get; set; }
        public List<ApprovalVersion> versions { get; set; }
        public List<ApprovalCollaborator> external_collaborators { get; set; }
        public List<ApprovalCollaborator> internal_collaborators { get; set; }
        public List<ApprovalCollaboratorLink> internal_collaborators_links { get; set; }
        public List<ApprovalCollaboratorLink> external_collaborators_links { get; set; }
        public List<UserGroup> user_groups { get; set; }
        public List<UserGroupDecision> user_group_links { get; set; }
        public List<ApprovalAnnotationAttachment> AnnotationAttachments { get; set; }
        public List<SoftProofingSessionParams> softProofingParams { get; set; }

        public ApprovalResponse()
        {
            approvals = new List<Approval>();
            versions = new List<ApprovalVersion>();
            external_collaborators = new List<ApprovalCollaborator>();
            internal_collaborators = new List<ApprovalCollaborator>();
            internal_collaborators_links = new List<ApprovalCollaboratorLink>();
            external_collaborators_links = new List<ApprovalCollaboratorLink>();
            softProofingParams = new List<SoftProofingSessionParams>();

            user_groups = new List<UserGroup>();
            user_group_links = new List<UserGroupDecision>();
        }
    }

    [ObsoleteAttribute("This method is obsolete. Call ApprovalInternalUseModel from collaborate module instead.", false)]
    public class ApprovalInternalModel : ApprovalInternalUseModel
    {

    }

    [ObsoleteAttribute("This method is obsolete. Call ApprovalModel instead.", false)]
    public class Approval : ApprovalModel
    {

    }

    [ObsoleteAttribute("This method is obsolete. Call ApprovalVersionModel instead.", false)]
    public class ApprovalVersion : ApprovalVersionModel
    {

    }

    public class ApprovalPagesHeightAndWidth
    {
        public int? OriginalImageHeight { get; set; }
        public int? OriginalImageWidth { get; set; }
    }

    public class ApprovalPage
    {
        public int ID { get; set; }
        public int PageNumber { get; set; }
        public int Approval { get; set; }
        public string PageFolder { get; set; }
        public string SmallThumbnailPath { get; set; }
        public string LargeThumbnailPath { get; set; }
        public int SmallThumbnailHeight { get; set; }
        public int LargeThumbnailHeight { get; set; }
        public int SmallThumbnailWidth { get; set; }
        public int LargeThumbnailWidth { get; set; }
        public int OriginalImageWidth { get; set; }
        public int OriginalImageHeight { get; set; }
        public int OutputImageWidth { get; set; }
        public int OutputImageHeight { get; set; }
        public int DPI { get; set; }
        public string TranscodingJobId { get; set; }
        public int? NextPage { get; set; }
        public int? PrevPage { get; set; }
        public string EmbeddedProfile { get; set; }
        public string HTMLFilePath { get; set; }
        public string HTMLFileName { get; set; }

    }

    public class ApprovalAnnotationBase
    {
        public int ID { get; set; }
        public int Status { get; set; }
        public int? Parent { get; set; }
        public int Page { get; set; }
        public int? OrderNumber { get; set; }
        public int PageNumber { get; set; }
        public int? Creator { get; set; }
        public int? ExternalCreator { get; set; }
        public int? Modifier { get; set; }
        public int? ExternalModifier { get; set; }
        public string ModifiedDate { get; set; }
        public string Comment { get; set; }
        public string base64 { get; set; }
        public string ReferenceFilePath { get; set; }
        public string AnnotatedDate { get; set; }
        public int CommentType { get; set; }
        public int? StartIndex { get; set; }
        public int? EndIndex { get; set; }
        public int? HighlightType { get; set; }
        public int? CrosshairXCoord { get; set; }
        public int? CrosshairYCoord { get; set; }
        public string HighlightData { get; set; }
        public bool IsPrivate { get; set; }
        public int? TimeFrame { get; set; }
        public string Checklist { get; set; }
        public string SelectedUserDetails { get; set; }
        public int AnnotationNr { get; set; }
        public int Version { get; set; }
        public SoftProofingLevel SoftProofingLevel { get; set; }
        public string PhaseName { get; set; }
        public int? Phase { get; set; }
        public int? ViewingCondition { get; set; }
        public string SPPSessionGUID { get; set; }
        public int CalibrationStatus { get; set; }
        public int AnnotatedOnImageWidth { get; set; }
        public int? OriginalVideoHeight { get; set; }
        public int? OriginalVideoWidth { get; set; }
        public string SubstituteUser { get; set; }
        public bool ChecklistItemUpdate { get; set; }
        public bool IsDifferentPhase { get; set; }
        public bool IsPrivateAnnotation { get; set; }
        public bool IsVideo { get; set; }
        public bool ShowVideoReport { get; set; }
        public bool IsSamePage { get; set; }

    }

    public class AccountTimeZoneandPattern
    {
        public string timeZone { get; set; }
        public string pattern { get; set; }
    }
    public class ApprovalChecklistApi
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<ApprovalChecklistItem> ChecklistItem { get; set; }

        public ApprovalChecklistApi()
        {
            ChecklistItem = new List<ApprovalChecklistItem>();
        }
    }

    [Serializable]
    public class ApprovalAnnotationApi
    {
        public List<ApprovalAnnotationGet> annotations { get; set; }
        public List<ApprovalAnnotationAttachment> AnnotationAttachments { get; set; }
        public List<ViewingConditionApiModel> viewing_conditions { get; set; }
        public List<ApprovalChecklistItem> ApprovalannotationChecklistitems { get; set; }
        public List<ApprovalAnnotationChecklistItemDetail> AnnotationAllchecklistitems { get; set; }
        public List<ApprovalAnnotationShape> custom_shapes { get; set; }
        public ApprovalAnnotationApi()
        {
            annotations = new List<ApprovalAnnotationGet>();
            AnnotationAttachments = new List<ApprovalAnnotationAttachment>();
            ApprovalannotationChecklistitems = new List<ApprovalChecklistItem>();
            AnnotationAllchecklistitems = new List<ApprovalAnnotationChecklistItemDetail>();
            custom_shapes = new List<ApprovalAnnotationShape>();
        }
    }

    [Serializable]
    public class ApprovalAnnotationGet : ApprovalAnnotationBase
    {
        public int[] AnnotationAttachments { get; set; }
        public int[] ApprovalannotationChecklistitems { get; set; }
        public int [] AnnotationAllchecklistitems { get; set; }
        public int[] AnnotationChecklistItemsHistory { get; set; }
    }

    [Serializable]
    public class ApprovalAnnotationSet : ApprovalAnnotationBase
    {
        public ApprovalAnnotationAttachment ApprovalAnnotationAttachment { get; set; }
        public ApprovalAnnotationPrivateCollaborator[] ApprovalAnnotationPrivateCollaborators { get; set; }
        public ApprovalAnnotationShape[] Shapes { get; set; }
        public ApprovalAnnotationAttachment[] AnnotationAttachments { get; set; }
    }

    public class ViewingConditionApiModel
    {
        #region Properties

        public int ID { get; set; }
        public int? SimulationProfileId { get; set; }
        public int? EmbeddedProfileId { get; set; }
        public int? CustomProfileId { get; set; }
        public int? PaperTintId { get; set; }

        #endregion
    }

    [ObsoleteAttribute("This method is obsolete. Call ApprovalCollaboratorModel instead.", false)]
    public class ApprovalCollaborator : ApprovalCollaboratorModel // TODO: in time remove ApprovalCollaborator
    {

    }

    [ObsoleteAttribute("This method is obsolete. Call ApprovalCollaboratorLinkModel instead.", false)]
    public class ApprovalCollaboratorLink : ApprovalCollaboratorLinkModel
    {

    }

    [Serializable]
    public class ApprovalAnnotationShape
    {
        public int ID { get; set; }
        public bool IsCustom { get; set; }
        public string SVG { get; set; }
        public string FXG { get; set; }

        public int ApprovalAnnotation { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double ScaleX { get; set; }
        public double ScaleY { get; set; }
        public string Color { get; set; }
        public int? Size { get; set; }
        public string BackgroundColor { get; set; }
        public int? Rotation { get; set; }
        public int? HighlighType { get; set; }
    }

    [Serializable]

    [ObsoleteAttribute("This method is obselete. Call ApprovalAnnotationAttachmentModel instead.", false)]
    public class ApprovalChecklistItem : ApprovalChecklistItemModel
    {

    }

    [Serializable]
    [ObsoleteAttribute("This method is obselete. Call AnnotationChecklistItemDetails instead.", false)]
    public class ApprovalAnnotationChecklistItemDetail : ApprovalAnnotationAllChecklistItemModel
    {

    }

    [Serializable]

    [ObsoleteAttribute("This method is obsolete. Call ApprovalAnnotationAttachmentModel instead.", false)]
    public class ApprovalAnnotationAttachment : ApprovalAnnotationAttachmentModel
    {

    }

    [Serializable]
    [ObsoleteAttribute("This method is obsolete. Call SoftProofingSessionParamsModel instead.", false)]
    public class SoftProofingSessionParams : SoftProofingSessionParamsModel
    {

    }

    [Serializable]
    public class SoftProofingPapertint
    {
        public int ID { get; set; }
        public string PaperTintName { get; set; }
        public string MediaCategory { get; set; }
    }

    [Serializable]
    public class SoftProofingPapertintMeasurement
    {
        public int ID { get; set; }
        public string Measurement { get; set; }
        public int PaperTint { get; set; }
        public double L { get; set; }
        public double a { get; set; }
        public double b { get; set; }
    }

    public class SoftProofingPaperTintModel
    {
        public List<SoftProofingPapertint> PaperTints { get; set; }
        public List<SoftProofingPapertintMeasurement> PaperTintsMeasurements { get; set; }
    }

    [Serializable]
    public class TreeFoldersModel
    {
        public int ID { get; set; }
        public int Parent { get; set; }
        public string Name { get; set; }
        public int Creator { get; set; }
        public int Level { get; set; }
        //public string Collaborators { get; set; }
        //public bool? AllCollaboratorsDecisionRequired { get; set; }
        public bool? HasApprovals { get; set; }
    }

    [Serializable]
    public class ApprovalInTree
    {
        public int ID { get; set; }
        public int? TreeFolderId { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public string Owner { get; set; }
    }

    [Serializable]
    public class ApprovalAnnotationPrivateCollaborator
    {
        public int ID { get; set; }
        public int ApprovalAnnotation { get; set; }
        public int Collaborator { get; set; }
        public string AssignedDate { get; set; }
    }

    public class ApprovalSeparationPlate
    {
        public int ID { get; set; }
        public int Page { get; set; }
        public string Name { get; set; }
        public int? ChannelIndex { get; set; }
        public string RGBHex { get; set; }
    }

    [Serializable]
    public class UpdateAnnotationStatusRequest
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public int status { get; set; }
    }

    [Serializable]
    public class JobChangesCompleteRequest
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public int pageId { get; set; }
    }

    [Serializable]
    public class UpdateAnnotationCommentRequest
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public string comment { get; set; }
    }

    [Serializable]
    public class UpdatePublicAnnotationsRequest
    {
        public List<int> Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        
    }

    [Serializable]
    public class TextHighlightingGetRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public int pageId { get; set; }
        public int approvalId { get; set; }

        public TextHighlightingGetRequest()
        {
        }
    }

    [Serializable]
    public class UploadAnnotationAttachmentRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public int? annotationId { get; set; }
    }

    [Serializable]
    public class DeleteAnnotationAttachmentRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public string guid { get; set; }
        public string filename { get; set; }
        public int? annotationId { get; set; }
    }

    [Serializable]
    public class DownloadAnnotationAttachmentRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public string guid { get; set; }
    }

    [Serializable]
    public class AnnotationAttachmentCountRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public int annotationId { get; set; }
    }

    public class DeleteAnnotationAttachmentFile
    {
        public string filename { get; set; }
        public string guid { get; set; }
        public bool result { get; set; }
    }

    [Serializable]
    public class UploadAnnotationAttachmentResponse
    {
        public List<UploadAnnotationAttachmentFile> files { get; set; }

        public UploadAnnotationAttachmentResponse()
        {
            files = new List<UploadAnnotationAttachmentFile>();
        }
    }

    [Serializable]
    public class UploadAnnotationAttachmentFile
    {
        public int id { get; set; }
        public string filename { get; set; }
        public string guid { get; set; }
    }

    [Serializable]
    public class PhaseCompleteGetRequest
    {
        public int? Id { get; set; }
        public string key { get; set; }
        public string user_key { get; set; }
        public int? collaboratorId { get; set; }
        public bool isExternal { get; set; }
        public int? decisionId { get; set; }
        public int currentPhase { get; set; }
    }

    [Serializable]
    public class PhaseCompleteResponse
    {
        public bool isCompleted { get; set; }
        public bool isRejected { get; set; }
    }

    [Serializable]
    public class LockApprovalRequest
    {
        public string key { get; set; }
        public int approvalId { get; set; }
        public int collaboratorId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class TextHighlightingWord
    {
        [DataMember]
        public string ID { get; set; }
        [DataMember]
        public decimal x { get; set; }
        [DataMember]
        public decimal y { get; set; }
        [DataMember]
        public decimal width { get; set; }
        [DataMember]
        public decimal height { get; set; }
        [DataMember]
        public string letters { get; set; }
        [DataMember]
        public decimal alpha { get; set; }
        [DataMember]
        public int startIndex { get; set; }
        [DataMember]
        public int endIndex { get; set; }
        [DataMember]
        public decimal pageWidth { get; set; }
        [DataMember]
        public decimal pageHeight { get; set; }
        [DataMember]
        public uint pageId { get; set; }
    }

    public class TextHighlightingPageSize
    {
        public decimal Width { get; set; }
        public decimal Height { get; set; }
    }

    public class SimulationProfileResponseModel
    {
        public int ID { get; set; }
        public string DisplayName { get; set; }
        public int? SimulationProfileId { get; set; }
        public int? EmbeddedProfileId { get; set; }
        public int? CustomProfileId { get; set; }
        public string MediaCategory { get; set; }
        public string MeasurementCondition { get; set; }
        public double? L { get; set; }
        public double? a { get; set; }
        public double? b { get; set; }
        public int? VersionId { get; set; }
    }

    public class AWSFileObject
    {
        public int ID { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }
        public long Size { get; set; }

        public AWSFileObject()
        {
            Url = string.Empty;
        }
    }

    public class PageBand : AWSFileObject
    {
        public int Page { get; set; }
    }

    public class InstantNotification
    {
        public int ID { get; set; }
        public int? Creator { get; set; }
        public int? ExternalCreator { get; set; }
        public int Version { get; set; }
        public InstantNotificationEntityType EntityType { get; set; }
    }

    public class TextHighlightingGetInfo
    {
        public int ApprovalID { get; set; }
        public string ApprovalGuid { get; set; }
        public int PageNumber { get; set; }
        public string Region { get; set; }
    }
}
