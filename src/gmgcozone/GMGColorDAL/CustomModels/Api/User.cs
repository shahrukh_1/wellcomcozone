﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class UserRequest
    {
        public string key { get; set; }
        public int approval_id { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public bool showAnnotationsOfAllPhases { get; set; }
    }
    public class UserResponse
    {
        public int ID { get; set; }
        public string UserPhotoPath { get; set; }
        public string Username { get; set; }
        public string UserAvatar { get;set; }
        public int? UserRole { get; set; }
        public bool UserIsExternal { get; set; }
        public string UserLocaleName { get; set; }
        public string UserColor { get; set; }
        public string BrandingHeaderLogo { get; set; }
        public bool CanSeeAllAnnotations { get; set; }
        public bool ShowAnnotationsOfAllPhases { get; set; }
        public bool PrivateAnnotations { get; set; }
    }

    public enum UserSettings
    {
        ShowAnnotationsOfAllPhases
    }
}
