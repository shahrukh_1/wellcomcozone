﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class RolesRequest
    {
        public string key { get; set; }
        public string locale_name { get; set; }
    }

    public class RolesResponse
    {
        public List<Role> roles { get; set; }
    }

    public class Role
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
    }
    
}
