﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class TranslationsRequest
    {
        public string[] TranslationKeys { get; set; }
        public string locale { get; set; }
        public string key { get; set; }
    }

    public class TranslationsResponse
    {
        public Dictionary<string, string> Translations { get; set; }

        public TranslationsResponse()
        {
            Translations = new Dictionary<string, string>();
        }
    }
}
