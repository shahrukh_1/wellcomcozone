﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class DecisionsRequest
    {
        public string key { get; set; }
        public string locale_name { get; set; }
        public string user_key { get; set; }
    }

    public class DecisionsResponse
    {
        public List<Decision> decisions { get; set; }
    }

    public class Decision
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
        public string JobStatus { get; set; }
    }
}
