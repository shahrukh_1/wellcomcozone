﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class AccountRequest
    {
        public string key { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
    }

    public class AvailableFeature
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }

    public class AccountResponse
    {
        public int ID { get; set; }
        public string AccountDateFormat { get; set; }
        public string AccountTimeFormat { get; set; }
        public string ProofStudioBackgroundColor { get; set; }
        public int ProofStudioPenWidth { get; set; }
        public List<int> AvailableFeatures { get; set; }
    }
}
