﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class AnnotationsResponse
    {
        public DashboardInstantNotification InstantNotification { get; set; }
        public int? AnnotationId { get; set; }
        public int? ParentAnnotationId { get; set; }
        public bool? IsDifferentPhase { get; set; }
    }
}
