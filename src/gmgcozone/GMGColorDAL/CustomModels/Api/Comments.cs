﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class CommentsRequest
    {
        public string key { get; set; }
        public string locale_name { get; set; }
    }

    public class CommentsResponse
    {
        public List<Comment> comment_types { get; set; }
    }

    public class Comment
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
    }
}
