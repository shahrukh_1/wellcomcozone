﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class ShapesRequest
    {
        public string key { get; set; }
    }

    public class ShapesResponse
    {
        public List<Shape> shapes { get; set; }
    }

    public class Shape
    {
        public int ID { get; set; }
        public bool IsCustom { get; set; }
        public string SVG { get; set; }
    }
}
