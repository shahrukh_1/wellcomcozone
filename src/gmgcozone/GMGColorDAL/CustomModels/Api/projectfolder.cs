﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class ProjectFolderRequest
    {
        public string key { get; set; }
        public string locale_name { get; set; }
        public string user_key { get; set; }
    }
    public class ProjectFolderResponse
    {
        public List<ProjectFolderDetails> projectfolders { get; set; }
    }
    public class ProjectFolderDetails
    {
       public int ID { get; set; }
       public string ProjectFolderName { get; set; }
       public List<ProjectFolderApprovalDetails> projectfolderapproval { get; set; }
    }

    public class ProjectFolderApprovalDetails
    {
        public int ID { get; set; }
        public string ApprovalName { get; set; }
        public string ApprovalThumbnailPath { get; set; }
        public string ApprovalType { get; set; }
    }
   public class ProjectFolder
    {
    }
}
