﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class ApprovalUsersAndGroupsRequest
    {
        public string key { get; set; }
        public int approval_id { get; set; }
        public string user_key { get; set; }
        public bool is_external_user { get; set; }
        public bool showAnnotationsOfAllPhases { get; set; }
    }

   public class ApprovalUsersAndGroupsResponse
    {
        public CollaboratorsAndGroups collaborators_and_group { get; set; }
        public List<ApprovalCollaborator> internal_collaborators { get; set; }
        public List<ApprovalCollaborator> external_collaborators { get; set; }
        public List<UserGroup> user_groups { get; set; }
        public List<UserGroupDecision> user_group_links { get; set; }
        public List<ApprovalCollaboratorLink> internal_collaborators_links { get; set; }
        public List<ApprovalCollaboratorLink> external_collaborators_links { get; set; }

        public ApprovalUsersAndGroupsResponse()
        {
            external_collaborators_links = new List<ApprovalCollaboratorLink>();
            internal_collaborators_links = new List<ApprovalCollaboratorLink>();
            user_group_links = new List<UserGroupDecision>();
            user_groups = new List<UserGroup>();
            external_collaborators = new List<ApprovalCollaborator>();
            internal_collaborators = new List<ApprovalCollaborator>();
        }
    }

    public class CollaboratorsAndGroups
    {
        public int ID { get; set; }
    }
}
