﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.Api
{
    public class AnnotationStatusRequest
    {
        public string key { get; set; }
        public string locale_name { get; set; }
    }

    public class AnnotationStatusResponse
    {
        public List<AnnotationStatus> annotation_statuses { get; set; }
    }

    public class AnnotationStatus
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
    }
}
