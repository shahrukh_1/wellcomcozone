﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AccountCustomiseInfo
    {
        #region Properties

        public bool IsCustomFromEmailAddress { get; set; }
        public string PathToTempFolder { get; set; }
        public List<AcctPrePressFunction> PrePressFunctionsWhereThisIsAccount { get; set; }

        #endregion

        #region Consctructors

        public AccountCustomiseInfo()
        {
            this.PrePressFunctionsWhereThisIsAccount = new List<AcctPrePressFunction>();
        }

        public AccountCustomiseInfo(string folderPath)
        {
            this.PathToTempFolder = folderPath;
            this.PrePressFunctionsWhereThisIsAccount = new List<AcctPrePressFunction>();
        }

        #endregion
    }

    [Serializable]
    public class AcctPrePressFunction
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }
}
