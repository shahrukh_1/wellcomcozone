﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GMG.CoZone.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class TrialAccount
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredFirstName)]
        [StringLength(128, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersFirstName)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredLastName)]
        [StringLength(128, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersLastName)]
        public string LastName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredPhone)]
        [StringLength(20, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersFirstName)]
        public string Phone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredEmail)]
        [StringLength(64, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersEmail)]
        [RegularExpression(Constants.EmailRegex, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.InvalidEmail)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredCompanyName)]
        [StringLength(128, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersCompanyName)]
        public string CompanyName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredAddress1)]
        [StringLength(128, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersAddress1)]
        public string Address1 { get; set; }

        [StringLength(128, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersAddress2)]
        public string Address2 { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredCity)]
        [StringLength(64, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersCity)]
        public string City { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredCountry)]
        [StringLength(64, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersCountry)]
        public string Country { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredTimeZone)]
        [StringLength(64, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersTimezone)]
        public string Timezone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.RequiredLanguage)]
        [StringLength(64, ErrorMessage = GMGColorDAL.Common.TrialAccountMessages.ToManyCharactersLanguage)]
        public string Language { get; set; }

    }

    [Serializable]
    public class TrialAccounts
    {
        public List<TrialAccount> Accounts { get; set; }

        public TrialAccounts()
        {
            Accounts = new List<TrialAccount>();
        }
    }
}
