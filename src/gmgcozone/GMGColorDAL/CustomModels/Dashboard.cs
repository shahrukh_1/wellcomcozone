﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class Dashboard : CommonDashboard
    {
        public AdminDashboard objAdminDashbord { get; set; }
        public StudioDashboard objStudioDashboard { get; set; }

        public Dashboard()
            : base()
        {
        }

        public Dashboard(int accountId, GMGColorContext context)
            : base(accountId, context)
        {
        }
    }

    [Serializable]
    public class AdminDashboard
    {

    }

    [Serializable]
    public class StudioDashboard
    {

    }

    [Serializable]
    public class CommonDashboard
    {
        public Account objAccount { get; set; }
        //public AccountWelcome objAccountWelcome { get; set; }
        public bool IsWelcome { get; set; }

        public CommonDashboard()
        {
            this.objAccount = new Account();
            //this.objAccountWelcome = new AccountWelcome();
        }

        public CommonDashboard(int accountId, GMGColorContext context)
        {
            this.objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();
            //this.objAccountWelcome = new AccountWelcome(accountId);
        }
    }
}
