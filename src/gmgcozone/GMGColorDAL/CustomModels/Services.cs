﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class MediaServiceDetails
    {
        #region Properties

        public int SelectedMediaService { get; set; }
        public int ImageFormatParent { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        public string NewService { get; set; }

        [Required(ErrorMessageResourceName = "reqSmoothing", ErrorMessageResourceType = typeof(Resources))]
        public int Smoothing { get; set; }

        [Required(ErrorMessageResourceName = "reqResolution", ErrorMessageResourceType = typeof(Resources))]
        public int Resolution { get; set; }

        [Required(ErrorMessageResourceName = "reqColorSpace", ErrorMessageResourceType = typeof(Resources))]
        public int ColorSpace { get; set; }

        [Required(ErrorMessageResourceName = "reqImageFormat", ErrorMessageResourceType = typeof(Resources))]
        public int ImageFormat { get; set; }

        [Required(ErrorMessageResourceName = "reqPageBox", ErrorMessageResourceType = typeof(Resources))]
        public int PageBox { get; set; }

        [Required(ErrorMessageResourceName = "reqTileImageFormat", ErrorMessageResourceType = typeof(Resources))]
        public int TileImageFormat { get; set; }

        [Required(ErrorMessageResourceName = "reqJpegCompression", ErrorMessageResourceType = typeof(Resources))]
        [Range(0, 100, ErrorMessageResourceName = "invalidJpegCompression", ErrorMessageResourceType = typeof(Resources))]
        public int JpegCompression { get; set; }

        #endregion

        #region Constructors

        public MediaServiceDetails() { }

        public MediaServiceDetails(MediaService objMediaService)
        {
            SelectedMediaService = objMediaService.ID;
            Smoothing = objMediaService.Smoothing;
            Resolution = objMediaService.Resolution;
            ColorSpace = objMediaService.Colorspace;
            ImageFormatParent = (objMediaService.ImageFormat1.Parent > 0) ? objMediaService.ImageFormat1.Parent.Value : objMediaService.ImageFormat;
            ImageFormat = objMediaService.ImageFormat;
            PageBox = objMediaService.PageBox;
            TileImageFormat = objMediaService.TileImageFormat;
            JpegCompression = objMediaService.JPEGCompression;
        }

        #endregion

    }
}
