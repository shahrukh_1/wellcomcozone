﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class Authentication
    {
        public GMGColorDAL.User objUser { get; set; }
        public Account objAccount { get; set; }

        public string LoginDoUrl { get; set; }

        public bool DisplayLoginError { get; set; }
        public bool DisplayLoginInactive { get; set; }
        public bool EmailNotFound { get; set; }

        public bool RememberMe { get; set; }

        public int? Relogin { get; set; }

        public int? UserPermissionsHaveBeenChanged { get; set; }

        public AccountSsoAuth0 SsoAuth0 { get; set; }

        public AccountSsoSaml SsoSaml { get; set; }

        public bool IsTemporarilyLocked { get; set; }

        public bool IsPermanentlyLocked { get; set; }

        public bool IsNewPasswordSent { get; set; }

        public string EmailIdOfNewPasswordSent { get; set; }

        public bool IsPasswordResetRequired { get; set; }

    }

    [Serializable]
    public class Login
    {
        [Required(ErrorMessageResourceName = "reqUsername", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Username { get; set; }

        [Required(ErrorMessageResourceName = "reqPassword2", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Password { get; set; }

        [Required(ErrorMessageResourceName = "reqAccountUrl", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string AccountUrl { get; set; }
    }
}
