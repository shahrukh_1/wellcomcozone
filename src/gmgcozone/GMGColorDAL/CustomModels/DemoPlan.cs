﻿namespace GMGColorDAL.CustomModels
{
    public class DemoPlan
    {
        public int AccountOwner { get; set; }

        public int ModuleKey{ get; set; }

        public int ID { get; set; }

        public int Account { get; set; }
    }
}
