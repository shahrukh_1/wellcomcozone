﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class UserInfo
    {
        #region Properties

        public OwnerUser objUser { get; set; }
        public ChangePassword objChangePassword { get; set; }
        public string Controller { get; set; }
        [Required(ErrorMessageResourceName = "reqRole", ErrorMessageResourceType = typeof(Resources))]
        public int AccountID { get; set; }
        public int UploaderKey { get; set; }
        public IEnumerable<UserGroupsInUser> UserGroupsInUserList { get; set; }
        public int SelectedPrimaryGroup { get; set; }
        public int LoggedAccountId { get; set; }
        public bool IsAggreeToTermsAndConditions { get; set; }
        public bool LoggedAccountHasNoSubscription { get; set; }

        public List<ModulePermision> ModulePermisions { get; set; }
        public int ProofStudioColorID { get; set; }
        public string ProofStudioColor { get; set; }

        public int Locale { get; set; }
        #endregion

        #region Constructors

        public UserInfo()
        {
        }

        public UserInfo(int userId, int loggedAccountId, GMGColorContext context)
        {
            try
            {
                this.LoggedAccountId = loggedAccountId;

                GMGColorDAL.User objUser = context.Users.Include("ProofStudioUserColor").FirstOrDefault(o => o.ID == userId);

                SelectedPrimaryGroup =
                        objUser.UserGroupUsers.Where(t => t.IsPrimary).Select(t => t.UserGroup).FirstOrDefault();

                this.AccountID = objUser.Account;
                this.objUser = new OwnerUser(objUser);
                this.objChangePassword = new ChangePassword();
                this.ProofStudioColorID = objUser.ProofStudioColor;
                List<UserGroup> lstUserGroups = GMGColorDAL.UserGroup.GetUserGroups(this.objUser.Account, context);

                this.UserGroupsInUserList = lstUserGroups.Select(x =>
                                             new UserGroupsInUser
                                             {
                                                 objUserGroup =
                                                 {
                                                     ID = x.ID,
                                                     Name = x.Name,
                                                     Account = x.Account,
                                                     Creator = x.Creator,
                                                     Modifier = x.Modifier,
                                                     CreatedDate = x.CreatedDate,
                                                     ModifiedDate = x.ModifiedDate
                                                 },
                                                 IsUserGroupInUser = x.UserGroupUsers.Any(o => o.User == objUser.ID),
                                                 IsUserGroupInVisibility = x.UserGroupUserVisibilities.Any(o => o.User == objUser.ID)
                                             });
                InitializePermissions(userId, context);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Creates a new UserInfo Object
        /// </summary>
        /// <param name="userOrAccountId"></param>
        /// <param name="loggedAccountId"></param>
        /// <param name="loggedUserTempFolderPath"></param>
        /// <param name="context"></param>
        /// <param name="isNewUser"></param>
        public UserInfo(int accId, int loggedAccountId, GMGColorContext context, bool isNewUser = true)
        {
            try
            {
                this.LoggedAccountId = loggedAccountId;

                GMGColorDAL.User objUser = null;
                List<UserGroup> lstUserGroups = new List<UserGroup>();

                objUser = new GMGColorDAL.User();
                objUser.Account = accId;
                AccountID = accId;
                lstUserGroups = GMGColorDAL.UserGroup.GetUserGroups(accId, context);

                this.objUser = new OwnerUser(objUser);
                this.objChangePassword = new ChangePassword();

                this.UserGroupsInUserList = lstUserGroups.Select(x =>
                                             new UserGroupsInUser
                                             {
                                                 objUserGroup =
                                                 {
                                                     ID = x.ID,
                                                     Name = x.Name,
                                                     Account = x.Account,
                                                     Creator = x.Creator,
                                                     Modifier = x.Modifier,
                                                     CreatedDate = x.CreatedDate,
                                                     ModifiedDate = x.ModifiedDate
                                                 },
                                                 IsUserGroupInUser = x.UserGroupUsers.Any(o => o.User == objUser.ID)
                                             });

                InitializePermissions(context);
            }
            finally
            {
            }
        }

        private  void InitializePermissions(int userId, GMGColorContext context)
        {
            try
            {
                if (this.ModulePermisions == null || !this.ModulePermisions.Any())
                {
                    this.ModulePermisions = new List<ModulePermision>();

                    List<IGrouping<int, Role>> groups = GMGColorDAL.User.GetRoles(AccountID, context);

                    MoveAdministrationRoleToTheEnd(groups, context);

                    groups.ForEach(
                        g =>
                        this.ModulePermisions.Add(new ModulePermision()
                        {
                            ModuleName = GMGColorCommon.GetModuleName((GMG.CoZone.Common.AppModule)g.First().AppModule1.Key),
                            ModuleId = g.First().AppModule,
                            ModuleType = (GMG.CoZone.Common.AppModule)g.First().AppModule1.Key,
                            Roles =
                                g.Select(
                                    r =>
                                    new RolePermision()
                                    {
                                        RoleId = r.ID,
                                        RoleName = r.Name
                                    }).ToList()
                        }));
                }
                InitializePermissions(userId, this.ModulePermisions.ToDictionary(o => o.ModuleType, o => (int?)null), context);
            }
            finally
            {
            }
        }

        private void InitializePermissions(GMGColorContext context)
        {
            try
            {
                this.ModulePermisions = new List<ModulePermision>();
                List<IGrouping<int, Role>> groups = GMGColorDAL.User.GetRoles(AccountID, context);

                MoveAdministrationRoleToTheEnd(groups, context);

                groups.ForEach(
                    g =>
                    this.ModulePermisions.Add(new ModulePermision()
                    {
                        ModuleName =
                            GMGColorCommon.GetModuleName(
                                (GMG.CoZone.Common.AppModule)g.First().AppModule1.Key),
                        ModuleId = g.First().AppModule,
                        ModuleType =
                            (GMG.CoZone.Common.AppModule)g.First().AppModule1.Key,
                        Roles =
                            g.Select(
                                r =>
                                new RolePermision()
                                {
                                    RoleId = r.ID,
                                    RoleName = r.Name,
                                    RoleChecked = false
                                }).ToList()
                    }));

                InitializePermissions(this.ModulePermisions.ToDictionary(o => o.ModuleType, o => (int?)null), context);
            }
            finally
            {
            }
        }

        public void InitializePermissions(Dictionary<GMG.CoZone.Common.AppModule, int?> selectedRolePerModule, GMGColorContext context)
        {
            foreach (KeyValuePair<GMG.CoZone.Common.AppModule, int?> keyValuePair in selectedRolePerModule)
            {
                var module = ModulePermisions.FirstOrDefault(o => o.ModuleType == keyValuePair.Key);
                if (module != null)
                {
                    module.Roles = (from r in context.Roles
                                     join am in context.AppModules on r.AppModule equals am.ID
                                     orderby r.Priority
                                     where am.Key == (int)module.ModuleType
                                     select new RolePermision()
                                     {
                                         RoleId = r.ID,
                                         RoleName = r.Name,
                                         RoleChecked = false
                                     }).ToList();

                    if (keyValuePair.Value.HasValue)
                    {
                        module.SelectedRole = keyValuePair.Value.Value;
                        RolePermision role = module.Roles.FirstOrDefault(o => o.RoleId == keyValuePair.Value);
                        if (role != null)
                        {
                            role.RoleChecked = true;
                        }
                    }
                    else
                    {
                        RolePermision rolePermision = module.Roles.FirstOrDefault(t => t.RoleName == GMGColorDAL.Role.NoAccessRole);
                        if (rolePermision != null)
                        {
                            module.SelectedRole = rolePermision.RoleId;
                            rolePermision.RoleChecked = true;
                        }
                    }
                }
            }

            //if retoucher workflow is disabled for current account remove retoucher from roles list
            if (!Account.IsRetoucherRoleAvailable(AccountID, context))
            {
                var collaborateModule = ModulePermisions.FirstOrDefault(o => o.ModuleType == GMG.CoZone.Common.AppModule.Collaborate);
                if (collaborateModule != null)
                {
                    collaborateModule.Roles.RemoveAll(t => t.RoleName == "Retoucher");
                }
            }
        }

        public void InitializePermissions(int userId, Dictionary<GMG.CoZone.Common.AppModule, int?> selectedRolePerModule, GMGColorContext context)
        {
            try
            {
                foreach (KeyValuePair<GMG.CoZone.Common.AppModule, int?> keyValuePair in selectedRolePerModule)
                {
                    var module = ModulePermisions.FirstOrDefault(o => o.ModuleType == keyValuePair.Key);
                    if (module != null)
                    {
                        module.Roles = (from r in context.Roles
                                        join am in context.AppModules on r.AppModule equals am.ID
                                        orderby r.Priority
                                        where am.Key == (int) module.ModuleType
                                        select new RolePermision()
                                                   {
                                                       RoleId = r.ID,
                                                       RoleName = r.Name,
                                                       RoleChecked =  false
                                                   }).ToList();
                    }
                }

                //if retoucher workflow is disabled for current account remove retoucher from roles list
                if (!Account.IsRetoucherRoleAvailable(AccountID, context))
                {
                    var collaborateModule = ModulePermisions.FirstOrDefault(o => o.ModuleType == GMG.CoZone.Common.AppModule.Collaborate);
                    if (collaborateModule != null)
                    {
                        collaborateModule.Roles.RemoveAll(t => t.RoleName == "Retoucher");
                    }
                }

                foreach (KeyValuePair<GMG.CoZone.Common.AppModule, int?> keyValuePair in selectedRolePerModule)
                {
                    var module = ModulePermisions.FirstOrDefault(o => o.ModuleType == keyValuePair.Key);
                    if (module != null)
                    {
                        UserRole userRole = (from ur in context.UserRoles
                                         where
                                             ur.User == userId &&
                                             (GMG.CoZone.Common.AppModule) ur.Role1.AppModule1.Key == keyValuePair.Key
                                         select ur).FirstOrDefault();

                        if (keyValuePair.Value.HasValue)
                        {
                            module.SelectedRole = keyValuePair.Value.Value;
                            RolePermision role = module.Roles.FirstOrDefault(o => o.RoleId == keyValuePair.Value);
                            if (role != null)
                            {
                                role.RoleChecked = true;
                            }
                        }
                        else if (userRole != null)
                        {
                            module.SelectedRole = userRole.Role;
                            module.Roles.Where(o => o.RoleId == userRole.Role).ToList().ForEach(o => o.RoleChecked = true);
                        }
                        else
                        {
                           
                            RolePermision rolePermision = module.Roles.FirstOrDefault();
                            if (module.ModuleId == 7) rolePermision.RoleId = 31;
                            if (rolePermision != null)
                            {
                                module.SelectedRole = rolePermision.RoleId;
                                rolePermision.RoleChecked = true;
                            }
                        }
                    }
                }
            }
            finally
            {
            }
        }

        public static void MoveAdministrationRoleToTheEnd(List<IGrouping<int, Role>> permissions, GMGColorContext context)
        {
            int administrationRoleID = AppModule.GetModuleId(GMG.CoZone.Common.AppModule.Admin, context);

            IGrouping<int, Role> admininstrationRole = permissions.SingleOrDefault(t => t.Key == administrationRoleID);

            if (admininstrationRole != null)
            {
                permissions.Remove(admininstrationRole);
                permissions.Add(admininstrationRole);
            }
        }
        #endregion
    }

    [Serializable]
    public class GetOutOfOfficeData
    {
        public int ID { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public string UserName { get; set; }
        public string SubstituteUser { get; set; }
        public string Description { get; set; }
        public int SubstituteUserId { get; set; }
       
    }

    [Serializable]
    public class OutOfOfficeModel
    {
        #region Properties
        public int id { get; set; }
        public bool HasNotifications { get; set; }
        public List<GetOutOfOfficeData> OutOfOfficeData { get; set; }
        #endregion

        #region Ctors
        public OutOfOfficeModel() { }

        public OutOfOfficeModel(int userId)
        {
            id = userId;
        }
        #endregion
    }

    [Serializable]
    public class ChangePassword
    {
        #region Properties

        [Required(ErrorMessageResourceName = "reqOldpasswordisrequired", ErrorMessageResourceType = typeof(Resources))]
        [DataType(DataType.Password)]
        [Display(Name = "lblYourcurrentpassword", ResourceType = typeof(Resources))]

        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceName = "reqNewpasswordisrequired", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(20, ErrorMessageResourceName = "reqPasswordLength", MinimumLength = 8, ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(Constants.PasswordValidDataPattern, ErrorMessage = "&#9758  &nbsp Password must contain:  <br /> &nbsp &nbsp \u2022 At least 1 uppercase letter  and At least 1 lowercase letter <br /> &nbsp &nbsp \u2022 At least 1 number <br /> &nbsp &nbsp \u2022 At least 1 special character")]
        [DataType(DataType.Password)]
        [Display(Name = "lblNewPassword", ResourceType = typeof(Resources))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "lblConfirmPassword", ResourceType = typeof(Resources))]
        [StringLength(20, ErrorMessageResourceName = "reqPasswordLength", MinimumLength = 8, ErrorMessageResourceType = typeof(Resources))]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessageResourceName = "reqPasswordsDoNotMatch", ErrorMessageResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }

        public int UserID { get; set; }

        public bool HasNotifications { get; set; }

        #endregion


        #region Ctors

        public ChangePassword(){}

        public ChangePassword(int userID)
        {
            UserID = userID;
        }

        #endregion
    }

    [Serializable]
    public class ChangePhoto
    {
        #region Properties

        public Enums.PhotoType PhotoType { get; set; }

        public int UserID { get; set; }

        public string PathToTempFolder { get; set; }

        public string ProofStudioColor { get; set; }

        public string AccountRegion { get; set; }

        public bool HasNotifications { get; set; }

        #endregion


        #region Ctors

        public ChangePhoto(){}

        public ChangePhoto(GMGColorDAL.User user, string pathToTempFolder)
        {
            UserID = user.ID;
            PathToTempFolder = pathToTempFolder;
            ProofStudioColor = user.ProofStudioUserColor.HEXValue;
            AccountRegion = user.Account1.Region;
        }

        #endregion
    }

    [Serializable]
    public class NotificationsModel
    {
        #region Properties

        public GMGColorDAL.User objUser { get; set; }
        public List<Role.RoleName> objRoles { get; set; }
        public List<EventGroup> EventGroups { get; set; }
        public bool HasNotifications { get; set; }
        public bool DisablePersonalNotifications { get; set; }

        #endregion

        #region Constructors

        public NotificationsModel()
        {
        }

        public NotificationsModel(GMGColorDAL.User objUser, GMGColorContext context)
        {
            try
            {
                if (objUser != null)
                {
                    this.PopulateModel(objUser.ID, objUser.Preset, context);
                }
            }
            finally
            {
            }
        }

        public NotificationsModel(int userId, int presetId, GMGColorContext context)
        {
            try
            {
                this.PopulateModel(userId, presetId, context);
            }
            finally
            {
            }
        }

        #endregion

        #region Methods

        public bool PopulateModel(int userId, int presetId, GMGColorContext context)
        {
            try
            {
                DisablePersonalNotifications = (from u in context.Users
                                                let userInSchedules = (from nsu in context.NotificationScheduleUsers
                                                                       join ns in context.NotificationSchedules on nsu.NotificationSchedule equals ns.ID
                                                                       join anp in context.AccountNotificationPresets on ns.NotificationPreset equals anp.ID
                                                                       where nsu.User == u.ID && anp.DisablePersonalNotifications
                                                                       select nsu.ID).ToList()
                                                let userGroupInSchedules = (from ugu in context.UserGroupUsers
                                                                            join nsug in context.NotificationScheduleUserGroups on ugu.UserGroup equals nsug.UserGroup
                                                                            join ns in context.NotificationSchedules on nsug.NotificationSchedule equals ns.ID
                                                                            join anp in context.AccountNotificationPresets on ns.NotificationPreset equals anp.ID
                                                                            where ugu.User == u.ID && anp.DisablePersonalNotifications
                                                                            select ugu.ID).ToList()

                                                where u.ID == userId && (userInSchedules.Any() || userGroupInSchedules.Any())
                                                select u).Any();

                this.objUser = (from u in context.Users where u.ID == userId select u).FirstOrDefault();
                this.objRoles = objUser.UserRoles.Select(r => Role.GetRole(r.Role1.Key)).ToList();

                this.EventGroups = new List<EventGroup>();
                List<int> eventGrpIds = NotificationsModel.GetGroups(userId, context);

                if (eventGrpIds.Any())
                {
                    List<int> roles = GMGColorDAL.Role.GetRoles(userId, context);
                    List<EventType> eventTypes = GMGColorDAL.EventType.GetEventTypeByGroups(roles, eventGrpIds, context);
                    foreach (IGrouping<int, GMGColorDAL.EventType> eventType in eventTypes.GroupBy(o => o.EventGroup).ToList())
                    {
                        EventGroup eventGroup = new EventGroup(eventType, presetId, userId, roles, context);

                        // ignore the added event groups
                        if (this.EventGroups.All(o => o.ID != eventGroup.ID))
                        {
                            this.EventGroups.Add(eventGroup);
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
            }
        }

        public static List<int> GetGroups(int userId, GMGColorContext context)
        {
            try
            {
                List<int> eventGrpIds = (from eg in context.EventGroups select eg.ID).ToList();

                bool isAdmin = GMGColorDAL.User.IsAdmin(userId, context);
                bool isSysAdmin = Account.IsSysAdminAccount(userId, context);
                bool needSubscription = GMGColorDAL.User.IsNeedSubscription(userId, context);
                bool isSubscriber = Account.IsSubscriberAccount(userId, context);

                if (!isAdmin && !isSysAdmin)
                {
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Users, context).ID);
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Groups, context).ID);
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Account, context).ID);
                }
                if (isSysAdmin)
                {
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Groups, context).ID);
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Deliver, context).ID);
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.InvitedUser, context).ID);
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.HostAdmin, context).ID);
                }
                if (!needSubscription)
                {
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Comments, context).ID);
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Approvals, context).ID);
                }
                if (isSubscriber)
                {
                    eventGrpIds.Remove(GMGColorDAL.EventGroup.GetEventGroup(GMGColorDAL.EventGroup.GroupName.Account, context).ID);
                }
                return eventGrpIds;
            }
            finally
            {
            }
        }

        #endregion

        [Serializable]
        public class EventGroup
        {
            #region Properties

            public int ID { get; set; }
            public int ModuleID { get; set; }
            public string Name { get; set; }
            public List<EventType> EventTypes { get; set; }

            #endregion

            #region Constructors

            public EventGroup()
            {
                
            }
          
            public EventGroup(IGrouping<int, GMGColorDAL.EventType> grouppingEventType, int presetId, int userId, List<int> roles, GMGColorContext context)
            {
                this.ID = grouppingEventType.Key;

                ModuleID = GMGColorDAL.EventGroup.GetEventGroupModuleID(ID, context);

                #region Set Localized EventGroupName

                this.Name = DALUtils.GetEventGroupName(grouppingEventType.Key, context);

                #endregion

                #region Set Localized EventTypeName

                this.EventTypes = new List<EventType>();

                foreach (GMGColorDAL.EventType et in grouppingEventType)
                {
                    EventType eventType = new EventType(){ ID = et.ID};
                   
                    eventType.Name = DALUtils.GetEventTypeName(et.Key);

                    eventType.IsChecked = RolePresetEventType.IsChecked(et.ID, presetId, userId, roles, context);

                    if (this.EventTypes.All(o=>o.ID != eventType.ID))
                    {
                        // ignore the added event types
                        this.EventTypes.Add(eventType);
                    }
                }
                #endregion
            }


            #endregion

            [Serializable]
            public class EventType
            {
                #region Properties

                public int ID { get; set; }
                public string Name { get; set; }
                public bool IsChecked { get; set; }

                #endregion
            }
        }
    }

    [Serializable]
    public class UserWorkflowShare
    {
        #region Properties
        
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public int ID { get; set; }
        public string UserAvatar { get; set; }

        #endregion
    }

    public class LoggedUserInfo
    {
        #region Properties

        public GMGColorDAL.User User { get; set; }
        public string LoggedAccountTypeKey { get; set; }
        public string LoggedAccountStatusKey { get; set; }
        public string LoggedAccountThemeKey { get; set; }
        public string LoggedAccountCulture { get; set; }
        public string LoggedUserBrandingHeaderLogo { get; set; }
        public string LoggedUserFavIcon { get; set; }

        #endregion
    }

    public class JobOwnerInfo
    {
        #region Properties

        public string Role { get; set; }
        public bool CanViewAllFiles { get; set; }

        #endregion
    }

    public class AddNewOutOfOffice
    {
        #region Properties
        public int ID { get; set; }
        public int UserId { get; set; }
        public List<ListOfDeputyUsers> SubstituteUser { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string Description { get; set; }
              

        #endregion
    }

    public class ListOfDeputyUsers
    {
        #region Properties
        public int ID { get; set; }
        public string Name { get; set; }
        #endregion

    }
}
