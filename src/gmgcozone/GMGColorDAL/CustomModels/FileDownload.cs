﻿using System;
using System.Collections.Generic;
using GMG.CoZone.Common;

namespace GMGColorDAL.CustomModels
{
    [Obsolete("The domain model can be used now from Modules -> Common.DomainModels -> FileDownloadModel", false)]
    [Serializable]
    public class FileDownload
    {
        #region Properties
        public int AccountID { get; set; }
        public DownloadVersionTypes DownloadVersionTypes { get; set; }
        public DownloadFileTypes DownloadFileTypes { get; set; }
        public GMG.CoZone.Common.AppModule ApplicationModule { get; set; }
        public int Preset { get; set; }
        public string FileIds { get; set; }
        public string FolderIds { get; set; }
        public bool HasAccessToFTP { get; set; }
        public int SelectedPresetId { get; set; } = 0;
        public List<KeyValuePair<int, string>> AccountPressets { get; set; }

        #endregion
    }
}
