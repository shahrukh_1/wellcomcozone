﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public class WebGridNavigationItem: Common
    {
        public string Value { get; set; }
        public bool IsFirst { get; set; }
        public bool IsLast { get; set; }
        public bool IsNext { get; set; }
        public bool IsPrev { get; set; }

        public WebGridNavigationItem()
        {
            Value = string.Empty;
        }
    }

    public class WebGridNavigationModel: Common
    {
        public List<WebGridNavigationItem> Items { get; set; }
        public int PageNr { get; set; }
        public int RowsPerPage { get; set; }
        public int TotalRows { get; set; }
        public Dictionary<int, string> DicPageSizes = new Dictionary<int, string>();
        public static readonly List<int> PagesCount = new List<int>
                                            {
                                                10,
                                                20,
                                                30,
                                                50
                                            };

        public string ControllerName { get; set; }
        public string ActionName { get; set; }

        public WebGridNavigationModel(int currentPage, int totalRows, int rowsPerPage)
        {
            PageNr = currentPage;
            TotalRows = totalRows;
            RowsPerPage = rowsPerPage;

            DicPageSizes.Add(10, "10 " + Resources.lblPerPage);
            DicPageSizes.Add(20, "20 " + Resources.lblPerPage);
            DicPageSizes.Add(30, "30 " + Resources.lblPerPage);
            DicPageSizes.Add(50, "50 " + Resources.lblPerPage);

            Items = new List<WebGridNavigationItem>();
            int totalPages = Convert.ToInt32(totalRows / rowsPerPage) + (totalRows % rowsPerPage == 0 ? 0 : 1);

            int actualNumberOfPages = totalPages >= 5 ? 5 : totalPages;

            List<int> pages = new List<int>();
            if (totalPages >= 1)
            {
                if (currentPage == 1)
                {
                    pages.AddRange(Enumerable.Range(currentPage, actualNumberOfPages));
                }
                else if (currentPage == totalPages)
                {
                    pages.AddRange(Enumerable.Range(currentPage - actualNumberOfPages + 1, actualNumberOfPages));
                }
                else
                {
                    int pageNrBefore;
                    if (currentPage > 2 && currentPage < totalPages - 1)
                    {
                        pageNrBefore = currentPage >= 2 ? 2 : actualNumberOfPages - 2 + currentPage;
                    }
                    else
                    {
                        pageNrBefore = (currentPage == 2) ? 1 : actualNumberOfPages - 2;
                    }

                    pages.AddRange(Enumerable.Range(currentPage - pageNrBefore, actualNumberOfPages));
                }
            }

            List<WebGridNavigationItem> previousPages = new List<WebGridNavigationItem>();
            //if (currentPage > 2)
            //{
            //    previousPages.AddRange(new WebGridNavigationItem[]
            //                               {
            //                                   new WebGridNavigationItem() {IsFirst = true, Value = Convert.ToString(1)},
            //                                   new WebGridNavigationItem()
            //                                       {IsPrev = true, Value = Convert.ToString(currentPage - 1)}
            //                               });
            //}
            //else if (currentPage > 3)
            //{
            //    previousPages.AddRange(new WebGridNavigationItem[]
            //                               {
            //                                   new WebGridNavigationItem()
            //                                       {IsPrev = true, Value = Convert.ToString(currentPage - 1)}
            //                               });
            //}

            if (!pages.Contains(1) && pages.Count > 0)
            {
                previousPages.AddRange(new WebGridNavigationItem[]{ new WebGridNavigationItem() { IsFirst = true, Value = Convert.ToString(1)} });
            }

            List<WebGridNavigationItem> nextPages = new List<WebGridNavigationItem>();
            //if (currentPage <= totalPages - 2)
            //    nextPages.AddRange(new WebGridNavigationItem[]
            //                           {
            //                               new WebGridNavigationItem()
            //                                   {IsNext = true, Value = Convert.ToString(currentPage + 1)},
            //                               new WebGridNavigationItem()
            //                                   {IsLast = true, Value = Convert.ToString(totalPages)}
            //                           });
            //else if (currentPage <= totalPages - 3)
            //    nextPages.AddRange(new WebGridNavigationItem[]
            //                           {
            //                               new WebGridNavigationItem()
            //                                   {IsLast = true, Value = Convert.ToString(totalPages)}
            //                           });
            if (!pages.Contains(totalPages) && pages.Count > 0)
            {
                nextPages.AddRange(new WebGridNavigationItem[] { new WebGridNavigationItem() { IsLast = true, Value = Convert.ToString(totalPages) } });
            }

            Items = new List<WebGridNavigationItem>();
            Items.AddRange(previousPages);
            Items.AddRange(pages.Select(o => new WebGridNavigationItem() {Value = Convert.ToString(o)}));
            Items.AddRange(nextPages);
        }
    }
}
