﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class Enums
    {
        #region  Enums

        public enum BillingFrequency { Monthly, Yearly, None }

        public enum PhotoType { DefaultPhoto, CustomPhoto, DefaultAccountLogo, CustomAccountLogo, DefaultLoginLogo, CustomLoginLogo };

        public enum DeliverSettingsType { ColorProofInstances = 0, ColorProofWorkflows = 1  };

        public enum Template { Welcome, LostPassword, ResetPassword, CommonTemplate, ReportTemplate, ApprovalReminder, DeliverPairingCode, SharedWorkflow }

        public enum CMSItemsType { Manuals = 0, Downloads = 1 };

        public enum UploadEngineTemplatesType { Presets = 0, Profiles = 1, XMLTemplates = 2 };

        public enum ScheduleType { Groups = 0, Individual };

        public enum ExternalUserType { Collaborate = 0, Deliver };

        public enum Notification { Schedule = 0, Presets };

        #endregion
    }
}
