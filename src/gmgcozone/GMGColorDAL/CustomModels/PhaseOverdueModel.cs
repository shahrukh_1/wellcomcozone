﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMGColorDAL.CustomModels
{
    public class PhaseOverdueModel
    {
        public int ApprovalId { get; set; }
        public int PhaseId { get; set; }

        public int? ExternalCollaborator { get; set; }
    }
}
