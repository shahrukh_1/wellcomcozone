﻿using System;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class WelcomeModel : Common
    {
        public GMGColorDAL.User objLoggedUser { get; set; }

        public WelcomeModel()
        {
             
        }

        public WelcomeModel(Account objAccount, AccountType.Type objAccountType, GMGColorDAL.User objUser)
        {
            this.objLoggedAccount = objAccount;
            this.objLoggedAccountType = objAccountType;
            this.objLoggedUser = objUser;
        }
    }
}
