﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Routing;
using GMGColorDAL.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class Common
    { 
        #region Properties

        public Account objLoggedAccount { get; set; }
        public AccountType.Type objLoggedAccountType { get; set; }

        #endregion

        #region Constructors

        public Common()
        {
        }

        public Common(Account objAccount, GMGColorContext context)
        {
            this.objLoggedAccount = objAccount;
            this.objLoggedAccountType = AccountType.GetAccountType(objAccount, context);
        }

        public static RouteValueDictionary GetCurrentQueryStrings(NameValueCollection queryString, string otherKey = null, object otherObj = null)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            foreach (string key in queryString.Keys)
            {
                dictionary[key.ToLower()] = (queryString.GetValues(key.ToLower()) ?? new string[] {string.Empty}).LastOrDefault();
            }
            if (otherKey != null && otherObj != null)
            {
                dictionary[otherKey.ToLower()] = otherObj;
            }
            return new RouteValueDictionary(dictionary);
        }

        public static RouteValueDictionary GetCurrentQueryStrings(NameValueCollection queryString, string[] otherKeys, object[] otherObjs)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            foreach (string key in queryString.Keys)
            {
                dictionary[key.ToLower()] = (queryString.GetValues(key.ToLower()) ?? new string[] { string.Empty }).LastOrDefault();
            }
            if (otherKeys.Length == otherObjs.Length)
            {
                for (int i = 0; i < otherKeys.Length; i++)
                {
                    dictionary[otherKeys[i].ToLower()] = otherObjs[i];
                }
            }
            return new RouteValueDictionary(dictionary);
        }

        #endregion
    }

    [Serializable]
    public class CustomTimeZone
    {
        #region Properties

        public string Id { get; set; }
        public string Name { get; set; }

        #endregion
    }

    [Serializable]
    public class BillingInfo
    {
        #region Properties

        public string NextTransactionDate { get; set; }
        public List<TransactionHistory> ListTransactionHistory { get; set; }
        public List<BillingPlanChangeLog> ListBillingPlanChangeLog { get; set; }

        #endregion

        #region Constructors

        public BillingInfo()
        {

        }

        public BillingInfo(int accountId, GMGColorContext context)
        {
            List<BillingPlanChangeLog> lstBillingPlanChangeLog = new List<BillingPlanChangeLog>();
            List<TransactionHistory> lstTransactionHistory = new List<TransactionHistory>();

            Account objAccount;
            
            {
                objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();
                
                int trnsCount = objAccount != null ? objAccount.AccountBillingTransactionHistories.Count : 0;

                if (trnsCount > 0)
                {
                    foreach (AccountBillingTransactionHistory transactionHistory in objAccount.AccountBillingTransactionHistories.OrderBy(o => o.CreatedDate).ToList().GetRange(0, (trnsCount > 10) ? 10 : trnsCount))
                    {
                        TransactionHistory objTransactionHistory = new TransactionHistory();
                        objTransactionHistory.Date = transactionHistory.CreatedDate.ToString(objAccount.DateFormat1.Pattern);
                        BillingPlan collaborateBillingPlan = transactionHistory.Account1.CollaborateBillingPlan(context);
                        //TODO INTEGRATE THIS FUNCTIONALITY WITH THE SAVING OF THE BILLING PLAN IN ACCOUNTCONTROLLER
                        objTransactionHistory.Plan = (collaborateBillingPlan != null
                                                          ? collaborateBillingPlan.Name
                                                          : null);
                        objTransactionHistory.BaseAmount =
                            GMGColorFormatData.GetFormattedCurrency(transactionHistory.BaseAmount, objAccount.ID, context);
                        objTransactionHistory.DiscountAmount =
                            GMGColorFormatData.GetFormattedCurrency(transactionHistory.DiscountAmount, objAccount.ID, context);

                        lstTransactionHistory.Add(objTransactionHistory);
                    }
                }
                lstBillingPlanChangeLog.AddRange((from bpcl in context.AccountBillingPlanChangeLogs
                                                  join u in context.Users on bpcl.Creator equals u.ID
                                                  where bpcl.Account == accountId
                                                  select new BillingPlanChangeLog
                                                             {
                                                                 Date =
                                                                     bpcl.CreatedDate.ToString(
                                                                         objAccount.DateFormat1.Pattern),
                                                                 User = u.GivenName,
                                                                 Activity = bpcl.LogMessage
                                                             }
                                                 ));
            }
            this.ListTransactionHistory = lstTransactionHistory;
            this.ListBillingPlanChangeLog = lstBillingPlanChangeLog;
        }

        #endregion

        #region Sub Class

        [Serializable]
        public class TransactionHistory
        {
            public string Date { get; set; }
            public string Plan { get; set; }
            public string BaseAmount { get; set; }
            public string DiscountAmount { get; set; }
        }

        [Serializable]
        public class BillingPlanChangeLog
        {
            public string Date { get; set; }
            public string User { get; set; }
            public string Activity { get; set; }
        }

        #endregion
    }
}
