﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class OutOfOfficeUsers
    {
        public List<GetOutOfOfficeUser> List_OutOfOfficeUsers { get; set; }
        public OutOfOfficeUsers() { }
    }
    public class GetOutOfOfficeUser
    {
        public int ID { get; set; }
        public string Name { get; set; }

    }

}
