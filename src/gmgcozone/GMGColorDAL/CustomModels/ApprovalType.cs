﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using GMG.CoZone.Common;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using AppModule = GMGColorDAL.AppModule;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ApprovalTypeModel
    {
     #region Properties

      public int ID { get; set; }
      public string Name { get; set; }
      public string FileTypes { get; set; }
      public string Colour { get; set; }
     #endregion
    }

   
}
