﻿namespace GMGColorDAL.CustomModels
{
    public class AccountSsoSaml
    {
        public string IdpUrl { get; set; }
        public string Type { get; set; }
    }
}
