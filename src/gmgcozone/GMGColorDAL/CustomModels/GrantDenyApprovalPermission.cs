﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class GrantDenyApprovalPermission
    {
        public List<int> NewCollaborators { get; set; }
        public DashboardInstantNotification DashboardNotification { get; set; }

        public GrantDenyApprovalPermission()
        {
            NewCollaborators = new List<int>();
        }
    }
}
