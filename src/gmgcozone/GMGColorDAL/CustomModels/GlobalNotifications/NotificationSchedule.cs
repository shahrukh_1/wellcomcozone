﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;
using System.ComponentModel.DataAnnotations;
using RSC = GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public class NotificationSchedules
    {
        #region Properties

        public List<NotificationSchedule> NotificationSchedulesList { get; set; }
 
        #endregion

        #region Constructors

        public NotificationSchedules(int accId, GMGColorContext context)
        {
           var notifSchedules = (from ns in context.NotificationSchedules
                                join np in context.AccountNotificationPresets on ns.NotificationPreset equals np.ID
                                join nf in context.NotificationPresetFrequencies on ns.NotificatonFrequency equals nf.ID
                                where np.Account == accId
                                orderby np.Name
                                select new 
                                {
                                    ns.ID,
                                    NotificationPreset = np.Name,
                                    NotificationFrequency = nf.Key,
                                    NotificationTrigger = ns.NotificationCollaborateTrigger != null ? (int?)ns.NotificationPresetCollaborateTrigger.Key : null,
                                    NotificationUnit = ns.NotificationCollaborateUnit != null ? (int?)ns.NotificationPresetCollaborateUnit.Key : null,
                                    NotificationUnitValue = ns.NotificationCollaborateUnitNumber
                                });

            NotificationSchedulesList = new List<NotificationSchedule>();
            foreach (var schedule in notifSchedules)
            {
                NotificationSchedulesList.Add(new NotificationSchedule
                                            {
                                                ID = schedule.ID,
                                                NotificationPreset = schedule.NotificationPreset,
                                                Frequency = NotificationSchedule.GetNotificationFrequncyLabel(schedule.NotificationFrequency),
                                                Trigger = schedule.NotificationTrigger != null ? NotificationSchedule.GetNotificationCollaborateTriggerLabel(schedule.NotificationTrigger.Value) : "-",
                                                Unit = schedule.NotificationUnit != null ? NotificationSchedule.GetNotificationCollaborateUnit(schedule.NotificationUnit.Value) : "-",
                                                UnitMeasure = schedule.NotificationUnitValue
                                            });
            }
        }

        #endregion
    }

    public class NotificationSchedule
    {
        #region Properties

        public int ID { get; set; }

        public string Frequency { get; set; }

        public string Trigger { get; set; }

        public string Unit { get; set; }

        public int? UnitMeasure { get; set; }

        public string NotificationPreset { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns Notification frequency translation based on db key
        /// </summary>
        /// <param name="frequencyKey"></param>
        /// <returns></returns>
        public static string  GetNotificationFrequncyLabel(int frequencyKey)
        {
            switch (frequencyKey)
            {
                case (int)GMG.CoZone.Common.NotificationPresetFrequency.Every2Hours:
                    return Resources.lblEvery2Hours;
                case (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryHour:
                    return Resources.lblEveryHour;
                case (int)GMG.CoZone.Common.NotificationPresetFrequency.Every4Hours:
                    return Resources.lblEvery4Hours;
                case (int)GMG.CoZone.Common.NotificationPresetFrequency.EveryDay:
                    return Resources.lblDaily;
                case (int)GMG.CoZone.Common.NotificationPresetFrequency.AsEventOccur:
                    return Resources.resNotificationFreqAsEventOccur;
                default:
                    return Resources.lblWeekly;
            }
        }

        /// <summary>
        /// Returns Notification trigger translation based on db key
        /// </summary>
        /// <param name="triggerKey"></param>
        /// <returns></returns>
        public static string GetNotificationCollaborateTriggerLabel(int triggerKey)
        {
            switch (triggerKey)
            {
                case (int)GMG.CoZone.Common.NotificationPresetCollaborateTrigger.AfterStart:
                    return Resources.lblAfterStart;
                default:
                    return Resources.lblBeforeEnd;
            }
        }

        /// <summary>
        /// Returns Colalborate Notification Unit translation based on db key
        /// </summary>
        /// <param name="unitKey"></param>
        /// <returns></returns>
        public static string GetNotificationCollaborateUnit(int unitKey)
        {
            switch (unitKey)
            {
                case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Days:
                    return Resources.lblNotificationDays;
                case (int)GMG.CoZone.Common.NotificationPresetCollaborateUnit.Hours:
                    return Resources.lblNotificationHours;
                default:
                    return Resources.lblWeeks;
            }
        }

        #endregion
    }

    [Serializable]
    public class AddEditNotificationScheduleViewModel
    {
        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqNotificationPreset", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int SelectedPreset { get; set; }

        [Required(ErrorMessageResourceName = "reqNotificationFrequency", ErrorMessageResourceType = typeof(RSC.Resources))]
        public int SelectedFrecvency { get; set; }

        public bool NotificationPerPSSession { get; set; }

        public int? SelectedUnit { get; set; }

        public int? SelectedTrigger { get; set; }

        public int? SelectedUnitValue { get; set; }

        public List<UserGroupsInUser> UserGroups { get; set; }

        public List<UserGroupsInUser> Users { get; set; }

        public List<NotificationPresetViewModel> NotificationPresetList { get; set; }

        public List<NotificationFrequencyViewModel> NotificationFrequencyList { get; set; }

        public List<NotificationPresetCollaborateTriggerViewModel> NotificationCollaborateTriggerList { get; set; }

        public List<NotificationPresetCollaborateUnitViewModel> NotificationCollaborateUnitList { get; set; }

        #endregion

        #region Constructors

        public AddEditNotificationScheduleViewModel()
        {
            
        }

        public AddEditNotificationScheduleViewModel(GMGColorContext context, int accId, int id = 0)
        {
            NotificationPresetList = (from np in context.AccountNotificationPresets
                                    where np.Account == accId
                                    select new NotificationPresetViewModel
                                    {
                                        ID = np.ID,
                                        Name = np.Name
                                    }).ToList();

            NotificationFrequencyList = (from nf in context.NotificationPresetFrequencies
                                        select new NotificationFrequencyViewModel
                                        {
                                            ID = nf.ID,
                                            Key = nf.Key
                                        }).ToList();

            //set the name from translations
            NotificationFrequencyList.ForEach(t => t.Name = NotificationSchedule.GetNotificationFrequncyLabel(t.Key));

            NotificationCollaborateTriggerList = (from nct in context.NotificationPresetCollaborateTriggers
                                                  select new NotificationPresetCollaborateTriggerViewModel
                                                 {
                                                     ID = nct.ID,
                                                     Key = nct.Key
                                                 }).ToList();

            //set the name from translations
            NotificationCollaborateTriggerList.ForEach(t => t.Name = NotificationSchedule.GetNotificationCollaborateTriggerLabel(t.Key));

            NotificationCollaborateUnitList = (from ncu in context.NotificationPresetCollaborateUnits
                                               select new NotificationPresetCollaborateUnitViewModel
                                                 {
                                                     ID = ncu.ID,
                                                     Key = ncu.Key
                                                 }).ToList();

            //set the name from translations
            NotificationCollaborateUnitList.ForEach(t => t.Name = NotificationSchedule.GetNotificationCollaborateUnit(t.Key));

            UserGroups = (from ug in context.UserGroups
                        where ug.Account == accId
                        orderby ug.Name
                        select new UserGroupsInUser
                        {
                            objUserGroup = new UserGroupInfo
                            {
                                ID = ug.ID,
                                Name = ug.Name
                            }
                        }).ToList();

            Users = (from u in context.Users
                     join us in context.UserStatus on u.Status equals us.ID
                          where u.Account == accId && us.Key == "A"
                          orderby u.GivenName
                          select new UserGroupsInUser
                          {
                              objUserGroup = new UserGroupInfo
                              {
                                  ID = u.ID,
                                  Name = u.GivenName + " " + u.FamilyName
                              }
                          }).ToList();

            if (id > 0)
            {
                //edit case

                ID = id;

                GMGColorDAL.NotificationSchedule editedSchedule = (from ns in context.NotificationSchedules
                                                                    where ns.ID == id
                                                                    select ns).FirstOrDefault();

                SelectedPreset = editedSchedule.NotificationPreset;
                SelectedFrecvency = editedSchedule.NotificatonFrequency;
                SelectedTrigger = editedSchedule.NotificationCollaborateTrigger;
                SelectedUnit = editedSchedule.NotificationCollaborateUnit;
                SelectedUnitValue = editedSchedule.NotificationCollaborateUnitNumber;
                NotificationPerPSSession = editedSchedule.NotificationPerPSSession;

                var selectedUsers = (from nsu in context.NotificationScheduleUsers
                                     where nsu.NotificationSchedule == id
                                     select nsu.User).ToList();

                //mark the selected users
                foreach (var userGroupsInUser in Users)
                {
                    userGroupsInUser.IsUserGroupInUser = selectedUsers.Contains(userGroupsInUser.objUserGroup.ID);
                }

                var selectedGroups = (from nsu in context.NotificationScheduleUserGroups
                                      where nsu.NotificationSchedule == id
                                      select nsu.UserGroup).ToList();

                //mark the selected groups
                foreach (var userGroupsInUser in UserGroups)
                {
                    userGroupsInUser.IsUserGroupInUser = selectedGroups.Contains(userGroupsInUser.objUserGroup.ID);
                }

            }
        }

        #endregion

    }

    [Serializable]
    public class NotificationPresetViewModel
    {
        #region Properties

        public int ID { get; set; }

        public string Name { get; set; }

        #endregion
    }

    [Serializable]
    public class NotificationFrequencyViewModel
    {
        #region Properties

        public int ID { get; set; }

        public int Key { get; set; }

        public string Name { get; set; }

        #endregion
    }

    [Serializable]
    public class NotificationPresetCollaborateTriggerViewModel
    {
        #region Properties

        public int ID { get; set; }

        public int Key { get; set; }

        public string Name { get; set; }

        #endregion
    }

    [Serializable]
    public class NotificationPresetCollaborateUnitViewModel
    {
        #region Properties

        public int ID { get; set; }

        public int Key { get; set; }

        public string Name { get; set; }

        #endregion
    }
}
