﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class UserAccountNotificationSetting
    {
        #region Properties

        public int EventType { get; set; }

        public int NotificationFrecvencyKey { get; set; }

        public bool IsPersonalNotifDisabled { get; set; }

        #endregion
    }
}
