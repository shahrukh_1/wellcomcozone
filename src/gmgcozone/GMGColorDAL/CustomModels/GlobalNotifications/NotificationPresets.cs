﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class NotificationPresets
    {
        #region Properties

        public List<NotificationPreset> NotificationPresetsList { get; set; }
        public NotificationPreset NotificationPreset { get; set; }
        public int? page { get; set; }
 
        #endregion

        #region Constructors

        public NotificationPresets(GMGColorContext context)
        {
            NotificationPreset =  new NotificationPreset(context);
        }

        public NotificationPresets(int accId, GMGColorContext context)
        {
            NotificationPresetsList = (from anp in context.AccountNotificationPresets
                                        where anp.Account == accId
                                        orderby anp.Name
                                        select new NotificationPreset
                                        {
                                            ID = anp.ID,
                                            Name = anp.Name,
                                            DisablePersonalNotifications = anp.DisablePersonalNotifications,
                                            CanBeDeleted = !(from notsch in context.NotificationSchedules
                                                            where notsch.NotificationPreset == anp.ID
                                                            select notsch.ID).Any()
                                        }).ToList();
        }
        #endregion
    }

    [Serializable]
    public class NotificationPreset
    {
        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        [Remote("IsPresetName_Available", "Settings", AdditionalFields = "ID")]
        public string Name { get; set; }

        public List<NotificationModule> NotificationModules { get; set; }

        public bool CanBeDeleted { get; set; }
		
        public bool DisablePersonalNotifications { get; set; }

        #endregion

        #region Constructors

        public NotificationPreset(GMGColorContext context)
        {
            NotificationModules = new List<NotificationModule>();

            var modules = GetNotificationModules(context);
            var eventGroups = GetNotificationEventGroups(context);

            foreach (var module in modules)
            {
                var groups = eventGroups.Where(t => t.ModuleID == module.ID).ToList();
                NotificationModules.Add(new NotificationModule
                    {
                        ID = module.ID,
                        ModuleName = module.ModuleName,
                        EventGroups = groups
                    });
            }
        }
        
        public NotificationPreset()
        {
        }
        #endregion


        #region Methods
        /// <summary>
        /// Returns a list of modules except sysadmin module
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private List<NotificationModule> GetNotificationModules(GMGColorContext context)
        {
            try
            {
                return (from am in context.AppModules
                        where am.Key != (int)GMG.CoZone.Common.AppModule.SysAdmin
                        select new NotificationModule
                        {
                            ID = am.ID,
                            ModuleName = am.Name
                        }).ToList();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Could not get modules, error: " + ex.Message, ex);
                return null;
            }
        }

        /// <summary>
        /// Returns a list of event groups from database
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private List<EventGroup> GetNotificationEventGroups(GMGColorContext context)
        {
            try
            {
                var groups = (from eg in context.EventGroups
                              select eg).ToList();

                return (from eventGroup in groups
                        let eventTypes = eventGroup.EventTypes.Select(eventType => new NotificationEventType()
                        {
                            ID = eventType.ID,
                            Name = DALUtils.GetEventTypeName(eventType.Key),
                            IsChecked = true,
                            Description = DALUtils.GetEventTypeDescription(eventType.Key)
                        }).ToList()
                        select new EventGroup()
                        {
                            ID = eventGroup.ID,
                            GroupName = DALUtils.GetEventGroupName(eventGroup.Key),
                            EventTypes = eventTypes,
                            ModuleID = GMGColorDAL.EventGroup.GetEventGroupModuleID(eventGroup.ID, context)
                        }).ToList();
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.Error("Could not get groups, error: " + ex.Message, ex);
                return null;
            }
        }

        #endregion
    }


    [Serializable]
    public class NotificationModule
    {
        #region Properties

        public int ID { get; set; }
        public string ModuleName { get; set; }
        public List<EventGroup> EventGroups { get; set; }

        #endregion

        public NotificationModule()
        {
            EventGroups =  new List<EventGroup>();
        }
    }

    [Serializable]
    public class EventGroup
    {
        #region Properties

        public int ID { get; set; }
        public int ModuleID { get; set; }
        public string GroupName { get; set; }
        public List<NotificationEventType> EventTypes { get; set; }

        #endregion

        #region Ctors

        public EventGroup()
        {
            EventTypes = new List<NotificationEventType>();
        }
        #endregion
    }

    public class NotificationEventType
    {
        #region Properties

        public int ID { get; set; }
        public bool IsChecked { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        #endregion
    }
}
