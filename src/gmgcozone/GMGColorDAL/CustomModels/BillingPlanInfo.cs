﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class BillingPlanInfo
    {
        #region Properties

        public bool SelectAttachedPlans { get; internal set; } //For New Account Back Button Functionlaty
        public bool HasChildren { get; internal set; }

        public int ID { get; internal set; }
        public string Name { get; internal set; }
        public bool IsFixed { get; internal set; }
        public bool IsDemo { get; internal set; }
        public int? Account { get; internal set; }

        public decimal Price { get; internal set; }
        public decimal PricePerYear { get; internal set; }
        public decimal DiscountedPrice { get; internal set; }
        public decimal AdjustedPrice { get; internal set; }
        public bool IsModifiedProofPrice { get; internal set; }

        public int Proofs { get; internal set; }
        public decimal CostPerProof { get; internal set; }
        public decimal AdjustedCostPerProof { get; internal set; }

        public int SoftProofingWorkstations { get; internal set; }
        public int Storage { get; set; }

        #endregion
    }
}
