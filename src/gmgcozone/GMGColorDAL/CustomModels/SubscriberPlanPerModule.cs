﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public enum CreditAllocationTypeEnum
    {
        Quota = 0,
        Floating
    }

    [Serializable]
    public class SubscriberPlanPerModule: Common
    {
        public int AccountId { get; set; }

        public Dictionary<int, string> ContractStartDate = new Dictionary<int, string>
                           {
                               {(int) ContractStartDateEnum.WhenModuleIsActivated, Resources.lblWhenModuleIsActivated},
                               {(int) ContractStartDateEnum.Today, Resources.lblToDay}
                           };

        [Required(ErrorMessageResourceName = "lblAtLeastOneSubscriptionPlanShouldBeChoosen", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string SelectedPlans { get; set; } // used only for client side validation

        public List<BillingPlanItem> CollaborateBillingPlans { get; set; }
        public int? CollaborateBillingPlanId { get; set; }
        public bool SelectedCollaboratePlanIsFixed { get; set; }
        public CreditAllocationTypeEnum CollaborateCreditAllocationType { get; set; }
        public int CollaborateNrOfCredits { get; set; }
        public ContractStartDateEnum CollaborateContractStartDateNow { get; set; }    
        public string CollaborateContractStartDate { get; set; }     
        public bool CollaborateChargeCostPerProofIfExceeded { get; set; }
        public string CollaborateCostPerProof { get; set; }
        public string CollaborateSelectedContractStartDate { get; set; }
        public bool CollaborateAllowOverdraw { get; set; }
        public int? CollaborateDemoRemainingDays { get; set; }

        public List<BillingPlanItem> DeliverBillingPlans { get; set; }
        public int? DeliverBillingPlanId { get; set; }
        public bool SelectedDeliverPlanIsFixed { get; set; }
        public CreditAllocationTypeEnum DeliverCreditAllocationType { get; set; }
        public int DeliverNrOfCredits { get; set; }
        public ContractStartDateEnum DeliverContractStartDateNow { get; set; }
        public string DeliverContractStartDate { get; set; }
        public bool DeliverChargeCostPerProofIfExceeded { get; set; }
        public string DeliverCostPerProof { get; set; }
        public string DeliverSelectedContractStartDate { get; set; }
        public bool DeliverAllowOverdraw { get; set; }
        public int? DeliverDemoRemainingDays { get; set; }      

        public SubscriberPlanPerModule()
        {
            CollaborateBillingPlans = new List<BillingPlanItem>();          
            DeliverBillingPlans = new List<BillingPlanItem>();           
        }

    }
}
