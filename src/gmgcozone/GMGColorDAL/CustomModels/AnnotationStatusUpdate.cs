﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{

    public class AnnotationUpdate
    {
        public int id { get; set; }
        public string url { get; set; }
    }

    public class AnnotationResult
    {
        public bool success { get; set; }
        public int id { get; set; }
    }
}
