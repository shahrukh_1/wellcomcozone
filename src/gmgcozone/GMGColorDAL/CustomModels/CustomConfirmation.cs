﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    public class CustomConfirmation
    {
        public int Key { get; set; }
        public string CustomMessageTitle { get; set; }
        public string CustomMessageHeading { get; set; }
        public string CustomMessage { get; set; }
        public string CustomMessagePleaseNote { get; set; }
        public string Action { get; set; }
        public string LoadingText { get; set; }

        public CustomConfirmation()
        {
        }

        public CustomConfirmation(int key, string action, string customLoadingText, string customMessageTitle, string customMessageHeading, string customMessage, string customMessagePleaseNote)
        {
            this.Key = key;
            this.Action = action;
            this.LoadingText = customLoadingText;
            this.CustomMessageTitle = customMessageTitle;

            this.CustomMessageHeading = customMessageHeading;
            this.CustomMessage = customMessage;
            this.CustomMessagePleaseNote = customMessagePleaseNote;
        }
    }
}
