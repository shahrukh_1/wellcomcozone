﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ThemeColors
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string TopColor { get; set; }
        public string BottomColor { get; set; }
    }
}
