﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GMGColor.Resources;
using GMGColorDAL.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ReportAccountData
    {
        public IEnumerable<string> SelectedAccountTypes { get; set; }
        public IEnumerable<string> SelectedLocations { get; set; }
        public IEnumerable<string> SelectedAccountStatuses { get; set; }
        public IEnumerable<string> ListLocations { get; set; }
        public IEnumerable<string> SelectedBillingPlans { get; set; }
    }

    [Serializable]
    public class ReportUserData
    {
        public IEnumerable<string> SelectedAccountTypes { get; set; }
        public IEnumerable<string> SelectedLocations { get; set; }
        public IEnumerable<string> SelectedUserStatuses { get; set; }
        public IEnumerable<string> SelectedAccounts { get; set; }
    }

    [Serializable]
    public class ReportFileData
    {
        public IEnumerable<string> SelectedAccounts { get; set; }
        public DateTime SelectedStartDate { get; set; } 
        public DateTime SelectedEndDate { get; set; }
        public int SelectedReportType { get; set; }
        public IEnumerable<string> SelectedUserGroup { get; set; }


    }

    [Serializable]
    public class ReportBillingData
    {
        public IEnumerable<string> SelectedAccountTypes { get; set; }
        public IEnumerable<string> SelectedLocations { get; set; }
        public IEnumerable<string> SelectedAccounts { get; set; }
        public IEnumerable<string> SelectedBillingPlans { get; set; }
        public bool SelectedBillingPeriod { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? MinStartDate { get; set; }

        public ReportBillingData()
        {
            ToDate = DateTime.Now;
            FromDate = DateTime.Now;
        }
    }

    [Serializable]
    public class ReportAccountUserData
    {
        public IEnumerable<string> SelectedAccounts { get; set; }
        public bool SelectedReportPeriod { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime? MinStartDate { get; set; }
        public bool IsAdminUser { get; set; }
        public IEnumerable<string> SelectedUserGroup { get; set; }
        public ReportAccountUserData()
        {
            ToDate = DateTime.Now;
            FromDate = DateTime.Now;
        }
    }

    [Serializable]
    public class Reports
    {
        public string ReportType { get; set; }
        public string FileReportType { get; set; }
        public string UserReportType { get; set; }


        public IEnumerable<string> SelectedAccounts { get; set; }
        public IEnumerable<string> SelectedLocations { get; set; }
        public IEnumerable<string> SelectedAccountTypes { get; set; }
        public IEnumerable<string> SelectedBillingPlans { get; set; }
        public IEnumerable<string> SelectedUserStatuses { get; set; }
        public IEnumerable<string> SelectedAccountStatuses { get; set; }
        public IEnumerable<string> SelectedUserGroup { get; set; }

        public string ToDate { get; set; }
        public string FromDate { get; set; }
        public string CurrentDate { get; set; }


        public EmailReport EmailReport { get; set; }
        public int LoggedAccount { get; set; }

        public List<KeyValuePair<int, string>> ListAccounts { get; set; }
        public List<KeyValuePair<int, string>> ListLocations { get; set; }
        public List<KeyValuePair<int, string>> ListAccountTypes { get; set; }
        public List<KeyValuePair<int, string>> ListBillingPlans { get; set; }
        public List<KeyValuePair<int, string>> ListUserStatuses { get; set; }
        public List<KeyValuePair<int, string>> ListAccountStatuses { get; set; }

        public List<User> ListUserReportDetails { get; set; }
        public List<AccountsReportView> ListAccountReportDetails { get; set; }
        public List<ListAccountReportDetails> ListAccountBillingReportDetails { get; set; }
        public List<ApprovalUserActivityReportViewData> ApprovalUserActivityReportView_Data { get; set; }
        public List<AccountUserReportDetails> ListAccountUserReportDetails { get; set; }
        public List<FileReportDetails> ListFileReportDetails { get; set; }
        public List<KeyValuePair<int, string>> ListUserGroups { get; set; }


        public string GrandTotal { get; set; }

        public bool IsAdminUser { get; set; }
        public bool IsSysAdminAccount { get; set; }

        public int? page { get; set; }

        public Reports(GMGColorDAL.Account objAccount, GMGColorDAL.User loggedUser, GMGColorContext context)// Billing Report
        {
            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAccount.TimeZone);

            this.ListAccounts = new List<KeyValuePair<int, string>>();
            this.ListLocations = new List<KeyValuePair<int, string>>();
            this.ListAccountTypes = new List<KeyValuePair<int, string>>();
            this.ListBillingPlans = new List<KeyValuePair<int, string>>();

            List<ReturnAccountParametersView> lstReportPara = objAccount.GetAccountParameters(loggedUser.Account, context);
            lstReportPara.RemoveAll(a => a.AccountID == objAccount.ID);

            foreach (ReturnAccountParametersView objReportPara in lstReportPara.Where(t => t.AccountStatusName == "Active"))
            {
                this.ListAccounts.Add(new KeyValuePair<int, string>(objReportPara.AccountID, objReportPara.AccountName));
                this.ListLocations.Add(new KeyValuePair<int, string>(objReportPara.LocationID, objReportPara.LocationName));
                this.ListAccountTypes.Add(new KeyValuePair<int, string>(objReportPara.AccountTypeID, objReportPara.AccountTypeName));
                
                this.ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.CollaborateBillingPlanId, objReportPara.CollborateBillingPlanName));
                this.ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.DeliverBillingPlanId, objReportPara.DeliverBillingPlanName));
            }

            this.ListAccounts = this.ListAccounts.Distinct().ToList();
            this.ListLocations = this.ListLocations.Distinct().ToList();
            this.ListAccountTypes = this.ListAccountTypes.Distinct().ToList();
            this.ListBillingPlans = this.ListBillingPlans.Distinct().ToList();

            string datePattern = objAccount.DateFormat1.Pattern;
            this.ToDate = userCurTime.ToString(datePattern);
            this.FromDate = userCurTime.ToString(datePattern);
            this.CurrentDate = GMGColorFormatData.GetFormattedDate(userCurTime, datePattern);

            this.ListAccountBillingReportDetails = new List<ListAccountReportDetails>();
            this.EmailReport = new EmailReport(objAccount.ID, context);
        }

        public Reports(GMGColorDAL.Account objAccount, GMGColorContext context)// Admin Report, Account Report
        {
            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAccount.TimeZone);
            ListAccounts = new List<KeyValuePair<int, string>>();
            ListLocations = new List<KeyValuePair<int, string>>();
            ListAccountTypes = new List<KeyValuePair<int, string>>();
            ListBillingPlans = new List<KeyValuePair<int, string>>();
            ListAccountStatuses = new List<KeyValuePair<int, string>>();

            List<ReturnAccountParametersView> lstReportPara = objAccount.GetAccountParameters(objAccount.ID, context);
            lstReportPara.RemoveAll(a => a.AccountID == objAccount.ID);

            foreach (ReturnAccountParametersView objReportPara in lstReportPara)
            {
                ListAccounts.Add(new KeyValuePair<int, string>(objReportPara.AccountID, objReportPara.AccountName));
                ListLocations.Add(new KeyValuePair<int, string>(objReportPara.LocationID, objReportPara.LocationName));
                ListAccountTypes.Add(new KeyValuePair<int, string>(objReportPara.AccountTypeID, objReportPara.AccountTypeName));
                ListAccountStatuses.Add(new KeyValuePair<int, string>(objReportPara.AccountStatusID, objReportPara.AccountStatusName));
                
                ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.CollaborateBillingPlanId, objReportPara.CollborateBillingPlanName));
                ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.DeliverBillingPlanId, objReportPara.DeliverBillingPlanName));
            }

            ListAccounts = ListAccounts.Distinct().ToList();
            ListLocations = ListLocations.Distinct().ToList();
            ListAccountTypes = ListAccountTypes.Distinct().ToList();
            ListBillingPlans = ListBillingPlans.Distinct().ToList();
            ListAccountStatuses = ListAccountStatuses.Distinct().ToList();
            ListUserStatuses = GMGColorDAL.User.GetUserStatuses(ListAccounts.Select(o => o.Key).ToList(), objAccount.ID, context);
            string datePattern = objAccount.DateFormat1.Pattern;
            ListUserReportDetails = new List<User>();
            ListAccountReportDetails = new List<AccountsReportView>();
            ToDate = userCurTime.ToString(datePattern);
            FromDate = userCurTime.ToString(datePattern);
            CurrentDate = GMGColorFormatData.GetFormattedDate(userCurTime, datePattern);

            SelectedAccountTypes = new string[0];
        }

        public Reports(int LoggedUserID, GMGColorContext context, GMGColorDAL.Account objAccount)// File Report
        {
            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAccount.TimeZone);
            ListAccounts = new List<KeyValuePair<int, string>>();
            ListLocations = new List<KeyValuePair<int, string>>();
            ListAccountTypes = new List<KeyValuePair<int, string>>();
            ListBillingPlans = new List<KeyValuePair<int, string>>();
            ListAccountStatuses = new List<KeyValuePair<int, string>>();

            List<ReturnAccountParametersView> lstReportPara = objAccount.GetAccountParameters(objAccount.ID, context);

            foreach (ReturnAccountParametersView objReportPara in lstReportPara)
            {
                ListAccounts.Add(new KeyValuePair<int, string>(objReportPara.AccountID, objReportPara.AccountName));
                ListLocations.Add(new KeyValuePair<int, string>(objReportPara.LocationID, objReportPara.LocationName));
                ListAccountTypes.Add(new KeyValuePair<int, string>(objReportPara.AccountTypeID, objReportPara.AccountTypeName));
                ListAccountStatuses.Add(new KeyValuePair<int, string>(objReportPara.AccountStatusID, objReportPara.AccountStatusName));

                ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.CollaborateBillingPlanId, objReportPara.CollborateBillingPlanName));
                ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.DeliverBillingPlanId, objReportPara.DeliverBillingPlanName));
            }
            if (lstReportPara.Count == 1)
            {
                ListUserGroups = objAccount.UserGroups.Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
            }
            else
            {
                ListUserGroups = context.UserGroups.Select(o => new { o.ID, o.Name }).AsEnumerable().Select(o => new KeyValuePair<int, string>(o.ID, o.Name)).ToList();
            }
            ListAccounts = ListAccounts.Distinct().ToList();
            ListLocations = ListLocations.Distinct().ToList();
            ListAccountTypes = ListAccountTypes.Distinct().ToList();
            ListBillingPlans = ListBillingPlans.Distinct().ToList();
            ListAccountStatuses = ListAccountStatuses.Distinct().ToList();
            ListUserStatuses = GMGColorDAL.User.GetUserStatuses(ListAccounts.Select(o => o.Key).ToList(), objAccount.ID, context);
            string datePattern = objAccount.DateFormat1.Pattern;
            ListUserReportDetails = new List<User>();
            ListAccountReportDetails = new List<AccountsReportView>();
            ToDate = userCurTime.ToString(datePattern);
            FromDate = userCurTime.ToString(datePattern);
            CurrentDate = GMGColorFormatData.GetFormattedDate(userCurTime, datePattern);

            SelectedAccountTypes = new string[0];
        }


        public Reports(GMGColorDAL.User loggedUser , GMGColorDAL.Account objAccount, GMGColorContext context)// User Report
        {
            DateTime userCurTime = GMGColorFormatData.GetUserTimeFromUTC(DateTime.UtcNow, objAccount.TimeZone);

            this.ListAccounts = new List<KeyValuePair<int, string>>();
            this.ListLocations = new List<KeyValuePair<int, string>>();
            this.ListAccountTypes = new List<KeyValuePair<int, string>>();
            this.ListBillingPlans = new List<KeyValuePair<int, string>>();

            List<ReturnAccountParametersView> lstReportPara = objAccount.GetAccountParameters(loggedUser.Account, context);

            foreach (ReturnAccountParametersView objReportPara in lstReportPara.Where(t => t.AccountStatusName == "Active"))
            {
                this.ListAccounts.Add(new KeyValuePair<int, string>(objReportPara.AccountID, objReportPara.AccountName));
                this.ListLocations.Add(new KeyValuePair<int, string>(objReportPara.LocationID, objReportPara.LocationName));
                this.ListAccountTypes.Add(new KeyValuePair<int, string>(objReportPara.AccountTypeID, objReportPara.AccountTypeName));

                this.ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.CollaborateBillingPlanId, objReportPara.CollborateBillingPlanName));
                this.ListBillingPlans.Add(new KeyValuePair<int, string>(objReportPara.DeliverBillingPlanId, objReportPara.DeliverBillingPlanName));
            }

            this.ListAccounts = this.ListAccounts.Distinct().ToList();
            this.ListLocations = this.ListLocations.Distinct().ToList();
            this.ListAccountTypes = this.ListAccountTypes.Distinct().ToList();
            this.ListBillingPlans = this.ListBillingPlans.Distinct().ToList();
            string datePattern = objAccount.DateFormat1.Pattern;
            this.ToDate = userCurTime.ToString(datePattern);
            this.FromDate = userCurTime.ToString(datePattern);
            this.CurrentDate = GMGColorFormatData.GetFormattedDate(userCurTime, datePattern);

            this.ListAccountUserReportDetails = new List<AccountUserReportDetails>();
        }


        public Reports(GMGColorContext context, GMGColorDAL.Account objAccount)// File Report
        {
            ListAccounts = new List<KeyValuePair<int, string>>();

            List<ReturnAccountParametersView> lstReportPara = objAccount.GetAccountParameters(objAccount.ID, context);

            foreach (ReturnAccountParametersView objReportPara in lstReportPara)
            {
                ListAccounts.Add(new KeyValuePair<int, string>(objReportPara.AccountID, objReportPara.AccountName));
            }

            ListAccounts = ListAccounts.Distinct().ToList();
            ListUserReportDetails = new List<User>();
            ListAccountReportDetails = new List<AccountsReportView>();

            SelectedAccountTypes = new string[0];
        }


        public Reports()
        {

        }

        public List<Account> GetAllChildAccounts(int accountId, GMGColorContext context)
        {
            return (from a in context.Accounts where a.Parent == accountId select a).ToList();
        }

        public string GetSubordinatesDisplayList(List<string> lstAccountNames, string prefix, string suffix)
        {
            string subordinatesString = string.Empty;

            foreach (string subordinate in lstAccountNames)
            {
                subordinatesString += prefix + subordinate + suffix;
            }

            return subordinatesString;
        }

        public string GetExceededProofsString(int loggedAccountId, decimal costPerProof, int totalExceededCount, GMGColorContext context)
        {
            return GMGColorFormatData.GetFormattedCurrency(costPerProof, loggedAccountId, context) + " x " + totalExceededCount;
        }

        public static string GetExceededProofsString(decimal costPerProof, int totalExceededCount, string currencySymbol)
        {
            return GMGColorFormatData.GetFormattedCurrency(costPerProof, currencySymbol) + " x " + totalExceededCount;
        }
    }

    [Serializable]
    public class ReportsIndex : Common
    {
        public bool ShowAccountReports { get; set; }
        
        public ReportsIndex(GMGColorDAL.Account objAccount, GMGColorContext context): base(objAccount, context)
        {
            ShowAccountReports = objLoggedAccountType == GMGColorDAL.AccountType.Type.GMGColor || objLoggedAccountType == GMGColorDAL.AccountType.Type.Subsidiary;
        }

        public ReportsIndex()
        {

        }
    }
       
   
    [Serializable]
    public class AccountBillingDetails
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Plan { get; set; }
        public string Type { get; set; }
        public string PricePerMonth { get; set; }
        public string Usage { get; set; }
        public int TotalExceededProofs { get; set; }
        public decimal CostPerProof { get; set; }
        public string BillingMonth { get; set; }
        public string Amount { get; set; }
        public string BillingFrequency { get; set; }
        public string ContractStartDate { get; set; }
        public string ContractEndDate { get; set; }
        public string ExceededProofsString { get; set; }
        public string ModuleName { get; set; }
        public bool BillingPlanIsFixed { get; set; }
        public string AccountPath { get; set; }
        public bool IsExpired { get; set; }
    }

     [Serializable]
    public class ListAccountReportDetails
    {
        public string SubAccountName { get; set; }
        public List<AccountBillingDetails> AccountBillingDetails { get; set; }
        public string Total { get; set; }
    }

    [Serializable]
    public class EmailReport
    {
        [Required(ErrorMessageResourceName = "reqSelectedUsers", ErrorMessageResourceType = typeof(Resources))]
        public IEnumerable<string> SelectedUsers { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public bool IsSendToMyself { get; set; }
        public List<User> ListUsers { get; set; }

        public EmailReport() { }

        public EmailReport(int accountId, GMGColorContext context)
        {
            var list = (from u in context.Users
             join us in context.UserStatus on u.Status equals us.ID
             join a in context.Accounts on u.Account equals a.ID
             where u.Account == accountId && us.Key == "A"
             select new {User = u, Account = a}).ToList();

            this.ListUsers = list.Select(o => new User(o.User, o.Account, context)).ToList();
        }
    }

    [Serializable]
    public class AccountUserReportDetails
    {
        #region Properties
        public string Account { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public int NumberOfApprovals { get; set; }
        public int NumberOfUploads { get; set; }
        public string LastLoginDate { get; set; }
        public string GroupMembership { get; set; }
        #endregion

        #region Constructors

        public AccountUserReportDetails()
        {
        }

        public AccountUserReportDetails(GMGColorDAL.ReturnUserReportInfoView accountuserreportdetails, GMGColorDAL.Account loggedAccount, GMGColorContext context)
        {
            DateTime UserTimeZoneDateTime = GMGColorFormatData.GetUserTimeFromUTC(accountuserreportdetails.DateLastLogin, loggedAccount.TimeZone);
            this.Account = accountuserreportdetails.Account;
            this.Name = accountuserreportdetails.Name;
            this.UserName = accountuserreportdetails.UserName;
            this.EmailAddress = accountuserreportdetails.EmailAddress;
            this.NumberOfApprovals = accountuserreportdetails.NumberOfApprovalsIn;
            this.NumberOfUploads = accountuserreportdetails.NumberOfUploads;
            this.LastLoginDate = (UserTimeZoneDateTime.Year == 1900) ? string.Empty : GMGColorFormatData.GetFormattedDate(UserTimeZoneDateTime, loggedAccount, context) 
               +" / " + GMGColorFormatData.GetFormattedTime(UserTimeZoneDateTime, loggedAccount ); 
            this.GroupMembership = accountuserreportdetails.GroupMembership;
        }
        #endregion
    }

    [Serializable]
    public class FileReportDetails
    {
        #region Properties
        public string Account { get; set; }
        public string OwnerName { get; set; }
        public string OwnerGroupMembership { get; set; }
        public string FileName { get; set; }
        public int V1PageCount { get; set; }
        public int NumberOfVersion { get; set; }
        public string UploadDate { get; set; }
        public string OverDue { get; set; }
        public string CompletedDate { get; set; }
        public int V1Users { get; set; }
        public int LatestVersionUsers { get; set; }
        public string Modifier { get; set; }
        public string ModifiedDate { get; set; }
        public string DeletedByUser { get; set; }
        public string DeletedDate { get; set; }

        #endregion

        #region Constructors

        public FileReportDetails()
        {
        }

        public FileReportDetails(GMGColorDAL.ReturnFileReportInfoView filereportdetails, GMGColorDAL.Account loggedAccount, GMGColorContext context)
        {
            DateTime UserOverDueTimeZoneDateTime =  GMGColorFormatData.GetUserTimeFromUTC(filereportdetails.OverDue.Value, loggedAccount.TimeZone);
            this.Account = filereportdetails.AccountName;
            this.OwnerName = filereportdetails.OwnerName;
            this.OwnerGroupMembership = filereportdetails.OwnerGroupMembership;
            this.FileName = filereportdetails.FileName;
            this.V1PageCount = filereportdetails.V1PageCount;
            this.NumberOfVersion = filereportdetails.TotalVersion;
            this.UploadDate = (filereportdetails.UploadDate.HasValue) ? GMGColorFormatData.GetFormattedDate(filereportdetails.UploadDate, loggedAccount, context) : string.Empty;
            this.OverDue = (UserOverDueTimeZoneDateTime.Year != 0001) ? GMGColorFormatData.GetFormattedDate(UserOverDueTimeZoneDateTime, loggedAccount, context)+
                    " / " + GMGColorFormatData.GetFormattedTime(UserOverDueTimeZoneDateTime , loggedAccount) : string.Empty;
            this.CompletedDate = (filereportdetails.CompletedDate.HasValue) ?  GMGColorFormatData.GetFormattedDate(filereportdetails.CompletedDate, loggedAccount, context) : string.Empty;
            this.V1Users = filereportdetails.V1Users;
            this.LatestVersionUsers = filereportdetails.LatestVersionUsers;
            this.Modifier = filereportdetails.Modifier;
            this.ModifiedDate = (filereportdetails.ModifiedDate.HasValue) ? GMGColorFormatData.GetFormattedDate(filereportdetails.ModifiedDate, loggedAccount, context) : string.Empty;
            this.DeletedByUser = filereportdetails.DeletedByUser;
            this.DeletedDate = (filereportdetails.DeletedDate.HasValue) ? GMGColorFormatData.GetFormattedDate(filereportdetails.DeletedDate, loggedAccount, context) : string.Empty;
        }
        #endregion
    }

    [Serializable]
    public class User
    {
        #region Properties

        public int ID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Status { get; set; }
        public string EmailAddress { get; set; }
        public string DateLastLogin { get; set; }
        public string AccountPath { get; set; }
        public int CollaborateApprovalsNumber { get; set; }
        public int DeliverJobsNumber { get; set; }

        #endregion

        #region Constructors

        public User()
        {
        }

        public User(GMGColorDAL.ReturnAdminUserReportInfoView user, GMGColorDAL.Account loggedAccount, GMGColorContext context)
        {
            this.ID = user.ID;
            this.Name = user.Name;
            this.Username = user.UserName;
            this.Status = user.StatusName;
            this.EmailAddress = user.EmailAddress;
            this.DateLastLogin = (user.DateLastLogin.Year == 1900) ? string.Empty : GMGColorFormatData.GetFormattedDate(user.DateLastLogin, loggedAccount, context);
            AccountPath = user.AccountPath;
            this.CollaborateApprovalsNumber = user.CollaborateApprovalsNumber;
            this.DeliverJobsNumber = user.DeliverJobsNumber;
        }

        public User(GMGColorDAL.User user, GMGColorDAL.Account loggedAccount, GMGColorContext context)
        {
            this.ID = user.ID;
            this.Name = user.GivenName + " " + user.FamilyName;
            this.Username = user.Username;
            this.Status = UserStatu.GetLocalizedUserStatusName(user.Status, context);
            this.EmailAddress = user.EmailAddress;
            this.DateLastLogin = (user.DateLastLogin != null) ? GMGColorFormatData.GetFormattedDate(user.DateLastLogin.Value, loggedAccount, context) : string.Empty;
        }

        #endregion
    }

    [Serializable]
    public class CycleDetails
    {
        public int CycleCount { get; set; }
        public int AddedProofsCount { get; set; }
        public int TotalExceededCount { get; set; }
    }

    [Serializable]
    public class AccountReportDetails
    {
        public int AccountID { get; set; }
        public string AccountName{ get; set; }
        public string AccountURL{ get; set; }
        public string OwnerName{ get; set; }
        public string OwnerEmail { get; set; }
        public List<KeyValuePair<int, string>> ListBillingPlans { get; set; }       

        public AccountReportDetails()
        {
            AccountName = string.Empty;
            AccountURL = string.Empty;
            OwnerName = string.Empty;
            OwnerEmail = string.Empty;
            ListBillingPlans = new List<KeyValuePair<int, string>>();           
        }
    }

    public class ModuleSubsciptionBillingDetails
    {
        #region Properties

        public int BillingPlan { get; set; }
        public string BillingCycleDetails { get; set; }
        public int Account { get; set; }
        public string BillingPlanName { get; set; }
        public string AccountTypeName { get; set; }
        public string AccountName { get; set; }
        public bool IsFixedPlan { get; set; }
        public bool IsMonthlyBillingFrequency { get; set; }
        public DateTime ContractStartDate { get; set; }
        public decimal GPPDiscount { get; set; }
        public int Proofs { get; set; }
        public string ModuleName { get; set; }
        public string AccountPath { get; set; }
        public bool IsQuotaAllocationEnabled { get; set; }

        #endregion
    }

    [Serializable]
    public class ApprovalUserActivityReportViewData
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string FileName { get; set; }
        public string Decision { get; set; }
        public int? DecisionId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public int Version { get; set; }
        public string Phase { get; set; }
        public int AnnotationCount { get; set; }
        public int AnnotationReplayCount { get; set; }
        
    }

    [Serializable]
    public class UserActivity_Report
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string FileName { get; set; }
        public string Decision { get; set; }
        public int? DecisionId { get; set; }
        public int Version { get; set; }
        public string Phase { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public List<int> AnnotationReplay { get; set; }
        public List<int> Annotation { get; set; }
    }

    public class UserActivityReportModel
    {
        public int AccountId { get; set; }
        public string ToDate { get; set; }
        public string FromDate { get; set; }
        public string CurrentDate { get; set; }
        public int LoggedAccount { get; set; }

        public List<ApprovalUserActivityReportViewData> ApprovalUserActivityReportViewData { get; set; }
    }

    [Serializable]
    public class UserActivityReportViewData
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string FileName { get; set; }
        public string Decision { get; set; }
        public DateTime ContractStartDate { get; set; }
        public int Version { get; set; }
        public string Phase { get; set; }
        public int AnnotationCount { get; set; }
        public int AnnotationReplayCount { get; set; }
    }

}
