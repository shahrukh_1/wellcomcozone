﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using GMGColor.Resources;
using System.Threading.Tasks;

namespace GMGColorDAL.CustomModels.PredefinedTagwords
{
    public class PredefinedTagwordsOnDashboard
    {
        #region Properties
        public string SearchText { get; set; }
        public List<PredefinedTagwords> predefinedTagwords { get; set; }
        public PredefinedTagwords predefinedtagword { get; set; }
        public string region { get; set; }
        public string AcceptedFiles { get; set; }
        public string PathToTempFolder { get; set; }
        #endregion

        #region Constructors

        public PredefinedTagwordsOnDashboard()
        {
            predefinedTagwords = new List<PredefinedTagwords>();
            predefinedtagword = new PredefinedTagwords();
        }
        #endregion
    }

    public class PredefinedTagwordsWithCount
    {
        #region Properties
        public int Id { get; set; }
        public int count { get; set; }
        public string tagword { get; set; }
        #endregion
    }
    public class PredefinedTagwords
    {
        #region Properties
        public int Id { get; set; }
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string tagword { get; set; }
        #endregion
    }

   public enum TypeOfTagword
    {
        PredefinedTagword =  1,
        ApprovalTagword = 2,
        ProjectfolderTagword = 3
    }
        }
