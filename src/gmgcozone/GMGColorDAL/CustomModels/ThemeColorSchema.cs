﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ThemeColorSchema
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public bool Active { get; set; }

        [Required]
        [StringLength(25, ErrorMessageResourceName = "reqStringLength25", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "lblName", ResourceType = typeof(Resources))]
        public string ThemeName { get; set; }
        
        public List<ThemeColors> Colors { get; set; }
    }
}
