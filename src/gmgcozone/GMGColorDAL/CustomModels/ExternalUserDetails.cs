﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class ExternalUserDetails
    {
        #region Properties

        public int ID { get; set; }

        public int Locale { get; set; }

        public Dictionary<int, string> Languages { get; set; }

        public Enums.ExternalUserType UserType { get; set; }

        #endregion

        #region Constructor

        public ExternalUserDetails()
        {
            
        }

        public ExternalUserDetails(int id, Enums.ExternalUserType userType, GMGColorContext context)
        {
            ID = id;

            if (userType == Enums.ExternalUserType.Collaborate)
            {
                Locale = context.ExternalCollaborators.FirstOrDefault(t => t.ID == ID).Locale;
            }
            else
            {
                Locale = context.DeliverExternalCollaborators.FirstOrDefault(t => t.ID == ID).Locale;
            }

            Languages = GMGColorDAL.Locale.GetLocalizeLocaleNames(context);
        }

        #endregion
    }
}
