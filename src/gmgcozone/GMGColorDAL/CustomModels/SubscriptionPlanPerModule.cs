﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class BillingPlanItem
    {
        public string Title { get; set; }
        public int Id { get; set; }
        public bool IsFixedPrice { get; set; }
        public decimal PlanPrice { get; set; }
        public string Price { get; set; }
        public decimal Cpp { get; set; }
        public bool IsDemo { get; set; }
        public int MaxStorage { get; set; }

        public BillingPlanItem()
        {
            Title = string.Empty;
            Price = string.Empty;
        }
    }

    [Serializable]
    public enum ContractStartDateEnum
    {
        Today = 0,
        WhenModuleIsActivated = 1
    }

    [Serializable]
    public class SubscriptionPlanPerModule: Common
    {
        public int AccountId { get; set; }

        public Dictionary<int, string> ContractStartDate = new Dictionary<int, string>
                           {
                               {(int) ContractStartDateEnum.WhenModuleIsActivated, Resources.lblWhenModuleIsActivated},
                               {(int) ContractStartDateEnum.Today, Resources.lblToDay}
                           };

        [Required(ErrorMessageResourceName = "lblAtLeastOneSubscriptionPlanShouldBeChoosen", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string SelectedPlans { get; set; } // used only for client side validation

        public List<BillingPlanItem> CollaborateBillingPlans { get; set; }
        public int? CollaborateBillingPlanId { get; set; }
        public Enums.BillingFrequency CollaborateIsMonthlyBillingFrequency { get; set; }
        public ContractStartDateEnum CollaborateContractStartDateNow { get; set; }
        public string CollaborateContractStartDate { get; set; }
        public bool CollaborateChargeCostPerProofIfExceeded { get; set; }
        public string CollaborateCostPerProof { get; set; }
        public string CollaborateSelectedContractStartDate { get; set; }
        public bool CollaborateAllowOverdraw { get; set; }
        public int? CollaborateDemoRemainingDays { get; set; }

        public List<BillingPlanItem> DeliverBillingPlans { get; set; }
        public int? DeliverBillingPlanId { get; set; }
        public Enums.BillingFrequency DeliverIsMonthlyBillingFrequency { get; set; }
        public ContractStartDateEnum DeliverContractStartDateNow { get; set; }
        public string DeliverContractStartDate { get; set; }
        public bool DeliverChargeCostPerProofIfExceeded { get; set; }
        public string DeliverCostPerProof { get; set; }
        public string DeliverSelectedContractStartDate { get; set; }
        public bool DeliverAllowOverdraw { get; set; }
        public int? DeliverDemoRemainingDays { get; set; }       

        public SubscriptionPlanPerModule()
        {
            CollaborateBillingPlans = new List<BillingPlanItem>();          
            DeliverBillingPlans = new List<BillingPlanItem>();
        }
    }
}
