﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class WorkflowPhase
    {
        public int ID { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }
        public string Status { get; set; }
        public DateTime? Deadline { get; set; }
        public bool ForceApproved { get; set; }
        public bool IsActive { get; set; }
        public bool InPending { get; set; }
        public ApprovalDetailsModel.VersionDetailsModel PhaseVersionDetails { get; set; }
    }
}
