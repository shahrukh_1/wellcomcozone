﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGColorDAL.CustomModels
{
    #region View Models

    [Serializable]
    public class ApprovalPageAnnotationsModel
    {
        #region Properties

        public string ApprovalGuid { get; set; }

        public int PageNumber { get; set; }

        public string AnnotationsIDs { get; set; }
        public int PageId { get; set; }
        public int? TimeFrame { get; set; }

        #endregion
    }

    [Serializable]
    public class ApprovalPageAnnotationsModelPDF
    {
        #region Properties

        public string ApprovalGuid { get; set; }

        public int PageNumber { get; set; }

        public string AnnotationsIDs { get; set; }
        public List<ApprovalAnnotationInfo> AnnotationInfo { get; set; }

        public int ApprovalPageID { get; set; }
        public int? TimeFrame { get; set; }
        #endregion
    }

    #endregion

    #region Annoatation Replay To MyComment User Models

    [Serializable]
    public class AnnoatationReplayToMyCommentUser
    {
        public int? UserId { get; set; }
        public bool IsExternal { get; set; }
    }

    #endregion

    #region Utility Classes

    public class ApprovalAnnotationInfo
    {
        #region Properties
        public int AnnotationOrderNumber { get; set; }
        public int AnnotationID { get; set; }
        public string AnnotationOwner { get; set; }
        public string AnnotationComment { get; set; }
        public DateTime AnnotatedDate { get; set; }
        public string AnnotationPhaseName { get; set; }
        public List<ApprovalAnnotationRepliesInfo> AnnotationReplies { get; set; }
        public List<string> AnnotationAttachments { get; set; }
        public string AnnotationChecklist { get; set; }
        public int? AnnotationTimeFrame { get; set; }
        public List<AnnotationChecklistIteminfo> AnnotationChecklistItems { get; set; }
        public int UserStatus { get; set; }
        public List<string> Annotationcomment { get; set; }
        #endregion
    }

    public class ApprovalAnnotationRepliesInfo
    {
        #region Properties
        public int AnnotationOrderNumber { get; set; }
        public int AnnotationID { get; set; }
        public string AnnotationOwner { get; set; }
        public string AnnotationComment { get; set; }
        public DateTime AnnotatedDate { get; set; }
        public string AnnotationPhaseName { get; set; }
        public int UserStatus { get; set; }
        public List<string> Annotationcomment { get; set; }

        #endregion
    }
    public class AnnotationChecklistIteminfo
    {
        #region Properties
        public string ChecklistItemName { get; set; }
        public string ChecklistItemValue { get; set; }
        #endregion
    }

    public class AnnotationDatabaseModel
    {
        #region Properties

        public int ID { get; set; }

        public int OrderNumber { get; set; }

        public DateTime AnnotatedDate { get; set; }

        public bool IsExternalCreator { get; set; }

        public int GroupId { get; set; }

        public string UserDisplayName { get; set; }

        #endregion
    }

    public class AnnotationDatabaseModelPDFreport
    {
        #region Properties

        public int ID { get; set; }
        public int ApprovalPageId { get; set; }


        public int AnnotationCommentLength { get; set; }

        public int AnnotationAttachmentsCount { get; set; }

        public List<int> AnnotationRepliesLength { get; set; }

        public List<int> AnnotationChecklistLength { get; set; }

        public int OrderNumber { get; set; }

        public DateTime AnnotatedDate { get; set; }

        public bool IsExternalCreator { get; set; }

        public int GroupId { get; set; }

        public string UserDisplayName { get; set; }
        public int TimeFrame { get; set; }

        #endregion
    }

    public class ApprovalAnnotationDatabaseModelPDFreport
    {
        #region Properties

        public int ID { get; set; }

        public int ApprovalPageId { get; set; }

        public int AnnotationCommentLength { get; set; }

        public int AnnotationAttachmentsCount { get; set; }

        public List<int> AnnotationRepliesLength { get; set; }

        public List<int> AnnotationChecklistLength { get; set; }

        public int OrderNumber { get; set; }

        public DateTime AnnotatedDate { get; set; }

        public bool IsExternalCreator { get; set; }

        public int GroupId { get; set; }

        public string UserDisplayName { get; set; }
        public int TimeFrame { get; set; }
        public ApprovalAnnotationInfo AnnotationInfo { get; set; }
        public List<ApprovalAnnotationRepliesInfo> AnnotationReplies { get; set; }
        #endregion
    }

    public class CompositeAnnotation // TODO - remove it
    {
        #region Properties
        public int ID { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
     
        #endregion
    }

    #endregion
}
