﻿using GMG.CoZone.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class CMSAnnouncementModel
    {
        #region Properties 

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqAnnouncement", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(1024, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [Remote("IsValidHtml", "Widget", ErrorMessageResourceName = "regInvalidHtml", HttpMethod = "POST", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources)), AllowHtml]
        public string Content { get; set; }

        #endregion

    }

    [Serializable]
    public class CMSVideoModel
    {
        private string _url;
        private string _description;
        private string _title;

        #region Constants 

        public static readonly Regex YoutubeVideoRegex = new Regex(@"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        public const string PreviewLink = "http://img.youtube.com/vi/{0}/0.jpg";

        #endregion 

        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqVideoURL", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [RegularExpression(@"(http://|https://)*([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?", ErrorMessageResourceName = "revVideoUrl", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(512, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Url
        {
            get { return _url; }
            set { _url = value.ToLower().StartsWith("http://") || value.ToLower().StartsWith("https://") ? value : string.Format("http://{0}", value); }
        }

        [Required(ErrorMessageResourceName = "reqVideoTitle", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public bool IsVideoTraining { get; set; }
        public string Preview
        {
            get { return GetPreviewURL(_url); }
        }
        #endregion

        #region Methods

        public static string GetPreviewURL(string Url)
        {
            Match youtubeMatch = YoutubeVideoRegex.Match(Url);

            string VideoId = string.Empty;

            if (youtubeMatch.Success)
                VideoId = youtubeMatch.Groups[1].Value;

            return String.Format(PreviewLink, VideoId);
        }
 
        #endregion
    }

    [Serializable]
    public class Home
    {
        public List<CMSAnnouncementModel> Announcements { get; set; }
        public List<CMSVideoModel> TrainingVideos { get; set; }
        public List<CMSVideoModel> FeatureVideos { get; set; }
        public List<CMSItemModel> Manuals { get; set; }
        public List<CMSItemModel> Downloads { get; set; }
        public string AbsoluteFilesPath { get; set; }

        public Home()
        {
            Announcements = new List<CMSAnnouncementModel>();
            TrainingVideos = new List<CMSVideoModel>();
            FeatureVideos = new List<CMSVideoModel>();
            Manuals = new List<CMSItemModel>();
            Downloads = new List<CMSItemModel>();
        }
    }

    [Serializable]
    public class CMSItemModel
    {
        public int ID { get; set; }
        public string FileGuid { get; set; }
        public string FileName { get; set; }
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(120, ErrorMessageResourceName = "errTooManyCharactors",ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [Required(ErrorMessageResourceName = "reqCMSItemName", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Name { get; set;}
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Description { get; set; }

        public bool IsDeleted { get; set; }

        public string Url { get; set; }

        public int? Account { get; set; }

        public string mimetype { get; set; }


    }
}
