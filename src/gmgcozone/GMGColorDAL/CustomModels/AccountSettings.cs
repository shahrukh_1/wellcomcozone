﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using GMG.CoZone.Common;
using RSC = GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AccountSettings
    {
        [Serializable]
        public class AccountSummary
        {
            #region Properties 

            public string ModuleAnualTotal { get; set; }

            public string ModuleAnualTotalMessage { get; set; }

            public string GPP { get; set; }

            public BillingPlan objBillingPlan { get; set; }

            public AccountSubscriptionPlan objAccountSubscriptionPlan { get; set; }

            public SubscriberPlanInfo objAccountSubscriberPlan { get; set; }

            public string ModuleName { get; set; }

            public EditAccount.Summary.Proofs objProofs { get; set; }

            public EditAccount.Summary.Proofs objSoftProofingWorkStations { get; set; }

            public EditAccount.Summary.Proofs objColorProofConnections { get; set; }

            public EditAccount.Summary.Storage objAccountStorage { get; set; }

            public EditAccount.Summary.AccountUserCount objAccountUserCount { get; set; }

            public GMG.CoZone.Common.AppModule appModule { get; set; }

            public string ModuleCurrentPlan { get; set; }

            public bool IsAccountInFreePlan { get; set; }

            public GMGColorDAL.AccountStatu.Status AccountStatus { get; set; }

            #endregion
        }

        [Serializable]
        public class Summary : Common
        {
            #region Properties

            public EditAccount.Summary objSummary { get; set; }

            #endregion

            #region Constructors

            public Summary()
            {
            }

            public Summary(Account objAccount, AccountSubscriptionDetails accountSubsciptions, AccountBillingPlansInUse accountBillingPlans, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objSummary = new EditAccount.Summary(objAccount.ID, objAccount, context, accountSubsciptions, accountBillingPlans);
            }

            #endregion
        }

        [Serializable]
        public class Profile : Common
        {
            #region Properties

            public EditAccount.Profile objProfile { get; set; }

            #endregion

            #region Constructors

            public Profile()
            {
            }

            public Profile(Account objAccount, GMGColorDAL.User objUser, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objProfile = new EditAccount.Profile(objAccount.ID, objUser.ID, context);
            }

            #endregion
        }

        [Serializable]
        public class Customise : Common
        {
            #region Properties

            public EditAccount.Customise objCustomise { get; set; }

            #endregion

            #region Constructors

            public Customise()
            {
            }

            public Customise(Account objAccount, string folderPath, GMGColorContext context)
                : base(objAccount,context)
            {
                this.objCustomise = new EditAccount.Customise(objAccount.ID, folderPath, context);
            }

            #endregion
        }

        [Serializable]
        public class BrandingPreset : Common
        {
            #region Properties

            public int ID { get; set; }

            public int? Page { get; set; }

            [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(RSC.Resources))]
            [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
            [Remote("IsBrandingPresetName_Available", "Settings", AdditionalFields = "ID")]
            //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
            public string Name { get; set; }
            
            public List<UserGroupsInUser> UserGroups { get; set; } 

            public EditAccount.Customise objCustomise { get; set; }

            #endregion

            #region Constructors

            public BrandingPreset()
            {
            }

            public BrandingPreset(int accId, string folderPath, GMGColorContext context)
            {
               objCustomise = new EditAccount.Customise(accId, folderPath, context);
               UserGroups = (from ug in context.UserGroups
                             let brandingGroups = (from bpug in context.BrandingPresetUserGroups
                                                   join bp in context.BrandingPresets on bpug.BrandingPreset equals bp.ID
                                                   where bp.Account == accId
                                                   select bpug.UserGroup).ToList()
                             where ug.Account == accId && !brandingGroups.Contains(ug.ID)
                            select new UserGroupsInUser
                            {
                                objUserGroup = new UserGroupInfo
                                {
                                    ID = ug.ID,
                                    Name = ug.Name
                                }
                            }).ToList();
            }
            #endregion
        }

        [Serializable]
        public class SiteSettings : Common
        {
            #region Properties

            public EditAccount.SiteSettings objSiteSettings { get; set; }

            #endregion

            #region Constructors

            public SiteSettings()
            {
            }

            public SiteSettings(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objSiteSettings = new EditAccount.SiteSettings(objAccount.ID, this.objLoggedAccount.AccountType, context);
            }

            #endregion
        }

        [Serializable]
        public class WhiteLabelSettings : Common
        {
            #region Properties

            public EditAccount.WhiteLabelSettings objWhiteLabelSettings { get; set; }

            #endregion

            #region Constructors

            public WhiteLabelSettings()
            {
            }

            public WhiteLabelSettings(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objWhiteLabelSettings = new EditAccount.WhiteLabelSettings(objAccount.ID, context);
            }

            #endregion
        }

        [Serializable]
        public class DNSAndCustomDomain : Common
        {
            #region Properties

            public EditAccount.DNSAndCustomDomain objDNSAndCustomDomain { get; set; }

            #endregion

            #region Conscturcors

            public DNSAndCustomDomain()
            {
            }

            public DNSAndCustomDomain(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objDNSAndCustomDomain = new EditAccount.DNSAndCustomDomain(objAccount.ID, context);
            }

            #endregion
        }

        [Serializable]
        public class AddEditUser : Common
        {
            #region Properties

            public EditAccount.AddEditUser objUsers { get; set; }

            #endregion

            #region Constructors

            public AddEditUser()
            {
            }

            public AddEditUser(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objUsers = new EditAccount.AddEditUser(objLoggedAccount, context);
            }

            #endregion
        }

        [Serializable]
        public class Users : Common
        {
            #region Properties

            public EditAccount.Users objUsers { get; set; }

            #endregion

            #region Constructors

            public Users()
            {
            }

            public Users(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                this.objUsers = new EditAccount.Users(objAccount.ID, this.objLoggedAccount, context);
            }

            #endregion
        }

        [Serializable]
        public class UserGroups : Common
        {
            #region Properties

            public bool ShowModelDialog { get; set; }
            public int UserGroupID { get; set; }
            public List<UserGroup> ListUserGroups { get; set; }

            #endregion

            #region Constructors

            public UserGroups()
            {
            }

            public UserGroups(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                try
                {
                    this.ShowModelDialog = false;
                    this.UserGroupID = 0;
                    this.ListUserGroups = (from ug in context.UserGroups
                                           where ug.Account == objAccount.ID
                                           orderby ug.Name
                                           select ug).ToList();
                }
                finally
                {
                }
            }

            #endregion
        }

        [Serializable]
        public class Branding : Common
        {
            #region Properties

            public int? Page { get; set; }

            public List<GMGColorDAL.BrandingPreset> ListBrandingPresets { get; set; }

            #endregion

            #region Constructors

            public Branding()
            {
            }

            public Branding(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                try
                {
                    this.ListBrandingPresets = (from bp in context.BrandingPresets
                                                where bp.Account == objAccount.ID 
                                                orderby bp.Name
                                                select bp).ToList();
                }
                finally
                {
                }
            }

            #endregion
        }

        public class ExternalUsers : Common
        {
            #region Properties

            public string SearchText { get; set; }
            public List<ExternalUser> ListExternalUsers { get; set; }

            #endregion

            #region Constructors

            public ExternalUsers()
            {
            }

            public ExternalUsers(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                ListExternalUsers = new List<ExternalUser>();

                ListExternalUsers = (from exu in context.ExternalCollaborators
                                     let sha = (from sh in context.SharedApprovals
                                                where sh.ExternalCollaborator == exu.ID
                                                select sh).ToList()
                                     where exu.Account == objAccount.ID && exu.IsDeleted == false
                                     orderby exu.GivenName, exu.FamilyName
                                     select new ExternalUser
                                     {
                                         ID = exu.ID,
                                         Name = exu.GivenName + " " + exu.FamilyName,
                                         Email = exu.EmailAddress,
                                         LastLogin = exu.DateLoggedIn,
                                         IsAccessRevoked = sha.Count(s => s.IsExpired) == sha.Count(),
                                         AccessRevoked = sha.Count(s => s.IsExpired) == sha.Count() ? RSC.Resources.lblYes : RSC.Resources.lblNo
                                     }).ToList();
            }

            #endregion

            public class ExternalUser
            {
                public int ID { get; set; }
                public string Name { get; set; }
                public string Email { get; set; }
                public DateTime? LastLogin { get; set; }
                public string AccessRevoked { get; set; }
                public bool IsAccessRevoked { get; set; }

                public ExternalUser()
                {
                }

                public ExternalUser(ExternalCollaborator objExUser, GMGColorContext context)
                {
                    try
                    {
                        this.ID = objExUser.ID;
                        this.Name = objExUser.GivenName + " " + objExUser.FamilyName;
                        this.Email = objExUser.EmailAddress;
                        this.LastLogin = objExUser.DateLoggedIn;
                        this.IsAccessRevoked = objExUser.SharedApprovals.Any(o => o.IsExpired);
                        this.AccessRevoked = this.IsAccessRevoked ? RSC.Resources.lblYes : RSC.Resources.lblNo;
                    }
                    finally
                    {
                    }
                }
            }
        }

        [Serializable]
        public class DeliverExternalUsers : Common
        {
            #region Properties

            public string SearchText { get; set; }
            public List<DeliverExternalUser> ListDeliverExternalUsers { get; set; }

            #endregion

            #region Constructors

            public DeliverExternalUsers()
            {
            }

            public DeliverExternalUsers(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                ListDeliverExternalUsers = new List<DeliverExternalUser>();

                ListDeliverExternalUsers = (from scpw in context.SharedColorProofWorkflows
                                            join cpczw in context.ColorProofCoZoneWorkflows on scpw.ColorProofCoZoneWorkflow equals cpczw.ID
                                            join cpwf in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpwf.ID
                                            join cpi in context.ColorProofInstances on cpwf.ColorProofInstance equals cpi.ID
                                            join u in context.Users on cpi.Owner equals u.ID 
                                            join exu in context.Users on scpw.User equals exu.ID
                                            join ac in context.Accounts on exu.Account equals ac.ID
                                            join acs in context.AccountStatus on ac.Status equals acs.ID
                                            join com in context.Companies on ac.ID equals com.Account
                                            where
                                                u.Account == objAccount.ID && acs.Key != "D"
                                            orderby exu.GivenName, exu.FamilyName
                                            select new DeliverExternalUser
                                            {
                                                ID = exu.ID,
                                                Name = exu.GivenName + " " + exu.FamilyName,
                                                Email = exu.EmailAddress,
                                                Company = com.Name,
                                                IsDeliverExternalUser = false
                                            }).Distinct().ToList()
                                            .Union
                                            (from dec in context.DeliverExternalCollaborators
                                             join ac in context.Accounts on dec.Account equals ac.ID
                                            join acs in context.AccountStatus on ac.Status equals acs.ID
                                            join com in context.Companies on ac.ID equals com.Account
                                            where
                                                dec.Account == objAccount.ID && acs.Key != "D"
                                             orderby dec.GivenName, dec.FamilyName
                                            select new DeliverExternalUser
                                            {
                                                ID = dec.ID,
                                                Name = dec.GivenName + " " + dec.FamilyName,
                                                Email = dec.EmailAddress,
                                                Company = com.Name,
                                                IsDeliverExternalUser = true
                                            }).ToList();
            }

            #endregion
        }

        [Serializable]
        public class DeliverExternalUser
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public string Company { get; set; }
            public bool IsDeliverExternalUser { get; set; }
        }

        public class ReservedDomains
        {
            #region Properties

            public int ID { get; set; }
            [Required(ErrorMessageResourceName = "reqDomain", ErrorMessageResourceType = typeof(RSC.Resources))]
            [StringLength(255, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
            public string Domain { get; set; }
            public string SearchText { get; set; }
            public List<ReservedDomainName> ListReservedDomains { get; set; }

            #endregion

            #region Constructors

            public ReservedDomains()
            {
                
            }

            public ReservedDomains(GMGColorContext context)
            {
                try
                {
                    this.ListReservedDomains = context.ReservedDomainNames.ToList();
                }
                finally
                {
                }
            }

            public ReservedDomains(int id, GMGColorContext context)
            {
                try
                {
                    this.ID = id;
                    this.Domain = (from rd in context.ReservedDomainNames where rd.ID == id select rd.Name).FirstOrDefault();
                    this.ListReservedDomains = context.ReservedDomainNames.ToList();
                }
                finally
                {
                }
            }

            #endregion
        }

        [Serializable]
        public class CMSHomeVideos
        {
            public const int VideoMaxCount = 6;
            public CMSVideos TrainingVideos { get; set; }
            public CMSVideos FeatureVideos { get; set; }
            public CMSVideoModel NewVideo { get; set; }
            public bool isSysAdmin { get; set; }

            public CMSHomeVideos()
            {
                TrainingVideos = new CMSVideos() { Title = RSC.Resources.lblCMSTrainingVideo, NoVideoLabel = RSC.Resources.lblNoTrainingVideoAdded };
                FeatureVideos = new CMSVideos() { Title = RSC.Resources.lblCMSFeatureVideo, NoVideoLabel = RSC.Resources.lblNoFeatureVideoAdded };
                NewVideo = new CMSVideoModel();
                isSysAdmin = false;
            }
        }

        [Serializable]
        public class CMSAnnouncements
        {
            public List<CMSAnnouncementModel> Announcements { get; set; }
            public CMSAnnouncementModel NewAnnouncement { get; set; }
            public CMSAnnouncementModel EditAnnouncement { get; set; }
            public bool isSysAdmin { get; set; }

            public CMSAnnouncements()
            {
                Announcements = new List<CMSAnnouncementModel>();
                NewAnnouncement = new CMSAnnouncementModel();
                EditAnnouncement = new CMSAnnouncementModel();
                isSysAdmin = false;
            }
        }

        [Serializable]
        public class CMSVideos
        {
            public List<CMSVideoModel> Videos { get; set; }
            public string Title { get; set; }
            public string NoVideoLabel { get; set; }
            public bool isSysAdmin { get; set; }

            public CMSVideos()
            {
                Videos = new List<CMSVideoModel>();
                isSysAdmin = false;
            }
        }

        [Serializable]
        public class  CMSItems
        {
            public string UserPathToTempFolder { get; set; }
            public string Region { get; set; }
            public List<CMSItemModel> Items { get; set; }
            public Enums.CMSItemsType Type { get; set; }
            public bool isSysAdmin { get; set; }

            public CMSItems()
            {
                UserPathToTempFolder = string.Empty;
                Region = String.Empty;
                Items = new List<CMSItemModel>();
                isSysAdmin = false;
            }
        }

        [Serializable]
        public class CollaborateGlobalSettings
        {
            #region Nested Classes

            public class CustomICCProfileModel
            {
                #region Properties

                public string Name { get; set; }
                public string DefaultProfiles { get; set; }
                public string InfoMessage { get; set; }
                public bool? IsValid { get; set; }
                public int? SelectedPaperSubstrate { get; set; }

                #endregion

                #region Constructors

                public CustomICCProfileModel()
                {}

                public CustomICCProfileModel(int accId, GMGColorContext context)
                {
                    var customProfInfo = (from accCustProfile in context.AccountCustomICCProfiles
                                          where accCustProfile.Account == null
                                          select accCustProfile.Name).ToList();

                    DefaultProfiles = string.Join(", ", customProfInfo);

                    var profileInfo = (from accCustProfile in context.AccountCustomICCProfiles
                                       where accCustProfile.Account == accId && accCustProfile.IsActive
                                       select new {
                                           accCustProfile.Name,
                                           accCustProfile.IsValid,
                                           SelectedPaperSubstrate = accCustProfile.PrintSubstrate
                                           }).FirstOrDefault();

                    if (profileInfo != null)
                    {
                        IsValid = profileInfo.IsValid;
                        Name = profileInfo.Name;
                        InfoMessage = profileInfo.IsValid.HasValue
                            ? profileInfo.IsValid.Value
                                ? RSC.Resources.lblICCProfileValid
                                : RSC.Resources.errInvalidICCProfile
                            : RSC.Resources.lblICCProfileInProgress;
                        SelectedPaperSubstrate = profileInfo.SelectedPaperSubstrate;
                    }
                    else
                    {
                        InfoMessage = RSC.Resources.lblICCProfileInfoMsg;
                    }

                }
                #endregion
            }

            #endregion

            #region Properties

            public bool DisableSOADIndicator { get; set; }
            public bool DisableTagwordsInDashbord { get; set; }

            public bool DisableVisualIndicator { get; set; }

            public bool DisableExternalUsersEmailIndicator { get; set; }

            public bool AllCollaboratorsDecisionRequired { get; set; }

            public bool HideCompletedApprovalsInDashboard { get; set; }

            public bool ShowAllFilesToManagers { get; set; }

            public bool EnableFlexViewer { get; set; }

            public bool UseUploadSettingsFromPreviousVersion { get; set; }

            public bool ShowAllFilesToAdmins { get; set; }

            public bool ProofStudioShowAnnotationsForExternalUsers { get; set; }

            public bool EnableRetoucherWorkflow { get; set; }

            public bool DisplayRetouchers { get; set; }

            public bool ShowBadges { get; set; }

            [Range(1, 99, ErrorMessageResourceName = "numberMustBeBetweenRange", ErrorMessageResourceType = typeof(RSC.Resources))]
            public int ProofStudioPenWidth { get; set; }

            public bool RetoucherUserExists { get; set; }

            public bool EnableCustomProfile { get; set; }

            public bool InheritParentFolderPermissions { get; set; }

            public CustomICCProfileModel CustomProfileModel { get; set; }

            public List<CollaborateApprovalStatusGeneralSettings> ApprovalDecisions { get; set; }

            public bool DisableFileDueDateinUploader { get; set; }

            public bool KeepExternalUsersWhenUploadingNewVersion { get; set; }
            public bool EnableProofStudioZoomlevelFitToPageOnload { get; set; }
            public bool ShowSubstrateFromICCProfileInProofStudio { get; set; }
            public bool EnableArchiveTimeLimit { get; set; }

            public string ArchiveTimeLimit { get; set; }

            public string ArchiveTimeLimitFormat { get; set; }


            #endregion

            #region Constructors

            public CollaborateGlobalSettings()
            {
                
            }

            public CollaborateGlobalSettings(int accId, int penDefaultValue, bool isCustomProfileEnable, GMGColorContext context, bool retrieveApprovalDec = false)
            {
                var globalSettings = (from acs in context.AccountSettings
                                                    where acs.Account == accId
                                                    select acs).ToList();

                //Set value for each setting option
                foreach (var globalSetting in globalSettings)
                {
                    bool settingValue;
                    if (!bool.TryParse(globalSetting.Value, out settingValue))
                    {
                        settingValue = false;
                    }

                    CollaborateGlobalSetting setting;

                    if (Enum.TryParse(globalSetting.Name, out setting))
                    {
                        switch (setting)
                        {
                            case CollaborateGlobalSetting.AllCollaboratorsDecisionRequired:
                            {
                                AllCollaboratorsDecisionRequired = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.EnableFlexViewer:
                            {
                                EnableFlexViewer = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.ProofStudioPenWidth:
                            {
                                ProofStudioPenWidth = Int32.Parse(globalSetting.Value);
                                break;
                            }
                            case CollaborateGlobalSetting.ShowAllFilesToAdmins:
                            {
                                ShowAllFilesToAdmins = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.ShowAllFilesToManagers:
                            {
                                ShowAllFilesToManagers = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.DisableExternalUsersEmailIndicator:
                                {
                                    DisableExternalUsersEmailIndicator = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.ProofStudioShowAnnotationsForExternalUsers:
                            {
                                ProofStudioShowAnnotationsForExternalUsers = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.DisplayRetouchers:
                            {
                                DisplayRetouchers = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.EnableRetoucherWorkflow:
                            {
                                EnableRetoucherWorkflow = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.EnableCustomProfile:
                            {
                                EnableCustomProfile = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.InheritPermissionsFromParentFolder:
                            {
                                InheritParentFolderPermissions = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.DisableFileDueDateinUploader:
                            {
                                DisableFileDueDateinUploader = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.HideCompletedApprovalsInDashboard:
                            {
                                HideCompletedApprovalsInDashboard = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.ShowBadges:
                            {
                                ShowBadges = settingValue;
                                break;
                            }
                            case CollaborateGlobalSetting.DisableSOADIndicator:
                                {
                                    DisableSOADIndicator = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.DisableVisualIndicator:
                                {
                                    DisableVisualIndicator = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.DisableTagwordsInDashbord:
                                {
                                    DisableTagwordsInDashbord = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.KeepExternalUsersWhenUploadingNewVersion:
                                {
                                    KeepExternalUsersWhenUploadingNewVersion = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.EnableProofStudioZoomlevelFitToPageOnload:
                                {
                                    EnableProofStudioZoomlevelFitToPageOnload = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.ShowSubstrateFromICCProfileInProofStudio:
                                {
                                    ShowSubstrateFromICCProfileInProofStudio = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.EnableArchiveTimeLimit:
                                {
                                    EnableArchiveTimeLimit = settingValue;
                                    break;
                                }
                            case CollaborateGlobalSetting.ArchiveTimeLimit:
                                {
                                    ArchiveTimeLimit = globalSetting.Value;
                                    break;
                                }
                            case CollaborateGlobalSetting.ArchiveTimeLimitFormat:
                                {
                                    ArchiveTimeLimitFormat = globalSetting.Value;
                                    break;
                                }
                        }
                    }
                }

                if (ProofStudioPenWidth == 0)
                {
                    ProofStudioPenWidth = penDefaultValue;
                }

                if (ArchiveTimeLimit == null)
                {
                    ArchiveTimeLimit = "450";
                    ArchiveTimeLimitFormat = "1";
                }

                RetoucherUserExists = Account.HasRetoucherUser(accId, context);

                if (isCustomProfileEnable)
                {
                    CustomProfileModel = new CustomICCProfileModel(accId, context);

                }

                // Get a list of active approval statuses
                if (retrieveApprovalDec)
                {
                    ApprovalDecisions = (from ad in context.ApprovalDecisions
                        let isChecked = (from ias in context.InactiveApprovalStatuses
                            where ias.Account == accId && ias.ApprovalDecision == ad.ID
                            select ias.ID).Any()
                        orderby ad.ID
                        select (new CollaborateApprovalStatusGeneralSettings
                        {
                            Id = ad.ID,
                            Name = ad.Name,
                            IsChecked = isChecked
                        })).ToList();
                }

            }
            #endregion
        }

        [Serializable]
        public class ApprovalStatus
        {
            #region Properties

            public int Account { get; set; }

            public int LoadedUserLanguage { get; set; }

            public List<ApprovalCustomDecisionViewModel> CustomDecisions { get; set; } 

            #endregion

            #region Constructors
           
            public ApprovalStatus()
            {
                CustomDecisions = new List<ApprovalCustomDecisionViewModel>();
            }
            #endregion
        }

        [Serializable]
        public class ApprovalCustomDecision
        {
            #region Properties
            
            public int Id { get; set; }
            
            public string Name { get; set; }

            public string Key { get; set; }

            public int DecisionId { get; set; }

            #endregion
        }

        public class ApprovalCustomDecisionViewModel
        {
            public string Key { get; set; }

            public int DecisionId { get; set; }

            public List<ApprovalCustomDecisionLanguage> CustomDecisionLanguages { get; set; }

            #region Constructors

            public ApprovalCustomDecisionViewModel()
            {
                CustomDecisionLanguages = new List<ApprovalCustomDecisionLanguage>();
            }
            #endregion
        }

        [Serializable]
        public class ApprovalCustomDecisionLanguage
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Locale { get; set; }
        }

        public class CollaborateApprovalStatusGeneralSettings
        {
            public int Id { get; set; }
            public bool IsChecked { get; set; }
            public string Name { get; set; }
        }
    }
}
