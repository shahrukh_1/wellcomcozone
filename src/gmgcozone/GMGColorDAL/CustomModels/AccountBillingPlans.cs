﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AccountBillingPlans
    {
        #region Properties

        public List<BillingPlanTypePlans> BillingPlans { get; set; }
        public int SelectedPrice { get; set; }
        public int SelectedProof { get; set; }
        public decimal NewPrice { get; set; }
        public decimal NewProof { get; set; }

        #endregion
    }

    [Serializable]
    public class AccountSubscriptionDetails
    {
        #region Properties

        #region Subscriber Accounts

        public SubscriberPlanInfo CollaborateSubscriberPlan { get; set; }
        public SubscriberPlanInfo DeliverSubscriberPlan { get; set; }

        #endregion

        #region Non-Subscriber

        public AccountSubscriptionPlan CollaborateSubscriptionPlan { get; set; }
        public AccountSubscriptionPlan DeliverSubscriptionPlan { get; set; }

        #endregion

        /// <summary>
        /// Returns wheter the current user has a collaborate plan attached
        /// </summary>
        public bool HasCollaborateSubscriptionPlan
        {
            get
            {
                return CollaborateSubscriptionPlan != null || CollaborateSubscriberPlan != null;
            }
        }

        /// <summary>
        /// Returns wheter the current user has a deliver plan attached
        /// </summary>
        public bool HasDeliverSubscriptionPlan
        {
            get
            {
                return DeliverSubscriptionPlan != null || DeliverSubscriberPlan != null;    
            }
        }


        #endregion
    }

    [Serializable]
    public class AccountBillingPlansInUse
    {
        #region Properties 

        public BillingPlan CollaborateBillingPlan { get; set; }
        public BillingPlan DeliverBillingPlan { get; set; }

        #endregion
    }


}
