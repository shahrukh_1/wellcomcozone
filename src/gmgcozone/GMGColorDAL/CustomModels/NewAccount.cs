﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AccountInfo
    {
        #region Properties

        public int LoggedAccount { get; set; }
        public int ID { get; set; }
        
        [Required(ErrorMessageResourceName = "reqAccountType", ErrorMessageResourceType = typeof(Resources))]
        public int Type { get; set; }
        
        [Required(ErrorMessageResourceName = "reqAccountName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }
        
        [Required(ErrorMessageResourceName = "reqDefaultLanguage", ErrorMessageResourceType = typeof(Resources))]
        public int DefaultLanguage { get; set; }
        
        [Required(ErrorMessageResourceName = "reqCurrencyFormat", ErrorMessageResourceType = typeof(Resources))]
        public int CurrencyFormat { get; set; }
        
        [Required(ErrorMessageResourceName = "reqDateFormat", ErrorMessageResourceType = typeof(Resources))]
        public int DateFormat { get; set; }
        
        [Required(ErrorMessageResourceName = "reqTimeFormat", ErrorMessageResourceType = typeof(Resources))]
        public int TimeFormat { get; set; }
        
        [Required(ErrorMessageResourceName = "reqTimeZone", ErrorMessageResourceType = typeof(Resources))]
        public string TimeZone { get; set; }
        
        [StringLength(255, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Domain { get; set; }

        [StringLength(255, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string CustomDomain { get; set; }

        public bool IsCustomDomainActive { get; set; }
        
        [Required(ErrorMessageResourceName = "reqFirstName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string ContactFirstName { get; set; }
        
        [Required(ErrorMessageResourceName = "reqLastName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string ContactLastName { get; set; }
        
        [Required(ErrorMessageResourceName = "reqPhone", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string ContactPhone { get; set; }
        
        [Required(ErrorMessageResourceName = "reqEmail", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string ContactEmailAddress { get; set; }
        
        [Required(ErrorMessageResourceName = "reqDataStorageLocation", ErrorMessageResourceType = typeof(Resources))]
        public string DataStorageLocation { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string DealerNumber { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string CustomerNumber { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string VATNumber { get; set; }

        public Company ObjCompany { get; set; }

        public OwnerUser ObjOwner { get; set; }

        #endregion

        #region Constructors

        public AccountInfo()
        {
        }

        public AccountInfo(int loggedAccountId, GMGColorContext context)
        {
            Account objLoggedAccount = (from a in context.Accounts where a.ID == loggedAccountId select a).FirstOrDefault();

            this.LoggedAccount = loggedAccountId;
            if (objLoggedAccount.AccountType != AccountType.GetAccountType(AccountType.Type.GMGColor, context).ID)
                this.CurrencyFormat = objLoggedAccount.CurrencyFormat;

            this.ObjCompany = new Company();
            this.ObjOwner = new OwnerUser();

            this.Domain = string.Empty;
        }

        public AccountInfo(int loggedAccountId, int accountId, GMGColorContext context)
        {
            Account objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();

            this.LoggedAccount = loggedAccountId;
            this.ID = objAccount.ID;
            this.Type = objAccount.AccountType;
            this.Name = objAccount.Name;
            this.DefaultLanguage = objAccount.Locale;
            this.CurrencyFormat = objAccount.CurrencyFormat;
            this.DateFormat = objAccount.DateFormat;
            this.TimeFormat = objAccount.TimeFormat;
            this.TimeZone = objAccount.TimeZone;
            this.Domain = (GMGColorConfiguration.AppConfiguration.Environment == "prod")
                                ? objAccount.Domain.Replace(GMGColorConfiguration.AppConfiguration.HostAddress,
                                                            string.Empty)
                                : objAccount.Domain;
            this.ContactFirstName = objAccount.ContactFirstName;
            this.ContactLastName = objAccount.ContactLastName;
            this.ContactPhone = objAccount.ContactPhone;
            this.ContactEmailAddress = objAccount.ContactEmailAddress;
            this.DealerNumber = objAccount.DealerNumber;
            this.CustomerNumber = objAccount.CustomerNumber;
            this.VATNumber = objAccount.VATNumber;
            this.DataStorageLocation = objAccount.Region;

            this.ObjCompany = new Company(objAccount.Companies.FirstOrDefault());
            this.ObjOwner =
                new OwnerUser(
                    (from a in context.Accounts
                        join u in context.Users on a.Owner equals u.ID
                        where a.ID == accountId
                        select u).FirstOrDefault());
        }

        #endregion
    }

    [Serializable]
    public class AccountPlans
    {
        #region Properties

        public int LoggedAccount { get; set; }
        public int Account { get; set; }
        [Required(ErrorMessageResourceName = "reqBillingPlan", ErrorMessageResourceType = typeof(Resources))]
        public bool NeedSubscriptionPlan { get; set; }

        [RegularExpression(@"^\d{1,2}$|^\d{1,2}\.\d{1,2}$", ErrorMessageResourceName = "gppDiscountInvalid", ErrorMessageResourceType = typeof(Resources))]
        public decimal? GPPDiscount { get; set; }
        public List<string> AttachedBillingPlans { get; set; }
        public AccountBillingPlans objAccountBillingPlans { get; set; }


        public SubscriptionPlanPerModule PlanPerModule { get; set; }
        public SubscriberPlanPerModule SubscriberPlans { get; set; }

        public bool HasFileTransferAccess { get; set; }

        #endregion

        #region Constructors

        public AccountPlans()
        {
            PlanPerModule = new SubscriptionPlanPerModule();
            SubscriberPlans = new SubscriberPlanPerModule();
        }

        public AccountPlans(Account objLoggedAccount, int accountId, GMGColorContext context, bool isBack = false)
        {
            Account objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();

            this.LoggedAccount = objLoggedAccount.ID;
            this.Account = accountId;
            this.GPPDiscount = (objAccount.GPPDiscount > 0) ? objAccount.GPPDiscount : 0;
            this.NeedSubscriptionPlan = objAccount.NeedSubscriptionPlan;

            this.AttachedBillingPlans = objAccount.AttachedAccountBillingPlans.Select(o => o.BillingPlan.ToString()).ToList();
            this.objAccountBillingPlans = isBack
                                              ? GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(objAccount, true, context)
                                              : GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(objLoggedAccount, context);

            if (AttachedBillingPlans != null && AttachedBillingPlans.Count > 0)
            {
                if (objAccountBillingPlans != null)
                {
                    foreach (BillingPlanTypePlans bptplans in objAccountBillingPlans.BillingPlans)
                    {
                        foreach (BillingPlanInfo billingInfo in bptplans.BillingPlans)
                        {
                            if (AttachedBillingPlans.Contains(billingInfo.ID.ToString()))
                            {
                                billingInfo.SelectAttachedPlans = true;
                            }
                        }
                    }
                }
            }

            PlanPerModule = new SubscriptionPlanPerModule() { AccountId = accountId };
            SubscriberPlans = new SubscriberPlanPerModule() { AccountId = accountId };
        }

        #endregion
    }

    [Serializable]
    public class AccountCustomise
    {
        #region Properties

        public AccountCustomiseInfo objCustomiseAccount { get; set; }

        public int Account { get; set; }
        
        [Required(ErrorMessageResourceName = "reqSiteName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string SiteName { get; set; }
        

        [Required]
        public List<ThemeColorSchema> AccountThemes { get; set; }
        

        public bool IsRemoveAllGMGCollaborateBranding { get; set; }
        
        [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(255, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string CustomFromAddress { get; set; }

        #endregion

        #region Constructors

        public AccountCustomise()
        {
        }

        public AccountCustomise(string folderPath, int accountId, GMGColorContext context)
        {
            Account objAccount = (from a in context.Accounts where a.ID == accountId select a).FirstOrDefault();
            if (objAccount.AccountThemes.Count == 0)
            {
                var defaultThemesList = (from dt in context.DefaultThemes
                                         let colors = (from tcs in context.ThemeColorSchemes
                                                       where tcs.DefaultTheme == dt.ID
                                                       select new ThemeColors
                                                       {
                                                           ID = tcs.ID,
                                                           Name = tcs.Name,
                                                           BottomColor = tcs.BHex,
                                                           TopColor = tcs.THex
                                                       }).ToList()
                                         select new ThemeColorSchema
                                         {
                                             ID = dt.ID,
                                             Key = dt.Key,
                                             ThemeName = dt.ThemeName,
                                             Colors = colors
                                         }).ToList();

                defaultThemesList.ForEach(th => th.Colors.ForEach(cl =>
                {
                    cl.BottomColor = string.IsNullOrEmpty(cl.BottomColor) ? null : GMG.CoZone.Common.Utils.ConvertHexToRGB(cl.BottomColor);
                    cl.TopColor = GMG.CoZone.Common.Utils.ConvertHexToRGB(cl.TopColor);
                }));
                defaultThemesList.First().Active = true;
                this.AccountThemes = defaultThemesList;
            }
            else
            {
                this.AccountThemes = objAccount.AccountThemes.Select(th => new ThemeColorSchema()
                                    {
                                        ID = th.ID,
                                        Active = th.Active,
                                        Key = th.Key,
                                        ThemeName = th.ThemeName,
                                        Colors = th.ThemeColorSchemes.Select(cl => new ThemeColors() 
                                                                                    {
                                                                                        ID = cl.ID,
                                                                                        Name = cl.Name,
                                                                                        BottomColor = string.IsNullOrEmpty(cl.BHex)? null : GMG.CoZone.Common.Utils.ConvertHexToRGB(cl.BHex),
                                                                                        TopColor = GMG.CoZone.Common.Utils.ConvertHexToRGB(cl.THex)
                                                                                    }).ToList()
                                    }).ToList();
            }


            this.Account = accountId;
            this.SiteName = objAccount.SiteName;
            this.IsRemoveAllGMGCollaborateBranding = objAccount.IsRemoveAllGMGCollaborateBranding;
            this.CustomFromAddress = objAccount.CustomFromAddress;
            this.objCustomiseAccount = objAccount.GetAccountCustomiseInfo(objAccount, folderPath, context);
        }

        #endregion
    }
}
