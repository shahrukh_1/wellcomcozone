﻿using GMGColor.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace GMGColorDAL.CustomModels.Checklists
{
    [Serializable]
    public class ChecklistItemViewModel
    {
        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [Remote("IsCheckListItemNameUnique", "Settings", AdditionalFields = "ID, Checklist, SelectedChecklist, ID")]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string ItemName { get; set; }
      
        public int Checklist { get; set; }

        public int Position { get; set; }

        [Required(ErrorMessageResourceName = "reqApprovalWorkflow", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public int SelectedChecklist { get; set; }

        public Account LoggedAccount { get; set; }

        public List<ChecklistModel> Checklists { get; set; }

        #endregion

        public ChecklistItemViewModel()
        {
            Checklists =  new List<ChecklistModel>();          
        }
    }

        

    //[Serializable]
    //public class ChecklistItemesInfo
    //{
    //    public List<ChecklistItem> ChecklistItems { get; set; }

    //    public ChecklistItemsInfo()
    //    {
    //        ChecklistItems = new List<ChecklistItem>();
    //    }
    //}

    //public class ChecklistItem
    //{
    //    public int ChecklistItem { get; set; }

    //}


}
