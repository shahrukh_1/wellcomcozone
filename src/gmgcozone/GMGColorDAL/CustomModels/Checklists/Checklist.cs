﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels.Checklists
{
    public class ChecklistDashboardModel
    {
        #region Properties

        public string SearchText { get; set; }
        public List<ChecklistModel> Checklists { get; set; }
        public ChecklistModel Checklist { get; set; }

        #endregion

        #region Constructors

        public ChecklistDashboardModel()
        {
            Checklists = new List<ChecklistModel>();
            Checklist = new ChecklistModel();
        }
        #endregion
    }

    public class ChecklistItemsDashboardModel
    {
        #region Properties

        public int Checklist { get; set; }
        public List<ItemDetails> ChecklistItemDetails { get; set; }

        #endregion

        #region Constructors

        public ChecklistItemsDashboardModel()
        {
            ChecklistItemDetails = new List<ItemDetails>();
        }
        #endregion
    }

    public class ItemDetails
    {
        #region Properties

        public int ID { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }

        #endregion
    }

    [Serializable]
    public class ChecklistModel
    {
        #region Properties

        public int ID { get; set; }

        public int Account { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Remote("IsCheckListName_Available", "Settings", AdditionalFields = "ID")]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        public bool HasItems { get; set; }

        #endregion
    }

    
}
