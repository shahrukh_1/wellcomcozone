﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class PlansPerModuleViewModel
    {
        #region Properties

        public string ModuleName { get; set; }
        public List<PlanTypeViewModel> PlanTypes { get; set; }

        #endregion

        #region Ctors

        public PlansPerModuleViewModel()
        {
            PlanTypes = new List<PlanTypeViewModel>();
        }
        #endregion
    }

    public class PlanTypeViewModel
    {
        #region Properties

        public string Type { get; set; }
        public int ID { get; set; }
        public string Label { get; set; }
        public List<PlanViewModel> Plans { get; set; }

        #endregion

        #region Ctors

        public PlanTypeViewModel()
        {
            Plans = new List<PlanViewModel>();
        }
        #endregion
    }

    public class PlanViewModel
    {
        #region Properties

        public int ID { get; set; }    
        public string Name { get; set; }
        public string Price { get; set; }
        public string MaxUsers { get; set; }
        public string MaxStorageInGb { get; set; }
        public string Proofs { get; set; }
        public string Status { get; set; }
        public string CostPerProof { get; set; }
        public string PricePerYear { get; set; }

        #endregion
    }
}
