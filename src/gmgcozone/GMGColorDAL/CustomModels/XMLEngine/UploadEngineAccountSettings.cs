﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels.XMLEngine
{
    [Serializable]
    public class UploadEngineAccountSettings
    {
        #region Properties

        public UploadEngineDuplicateFileName DuplicateFileOption { get; set; }
        public UploadEngineVersionName VersionNameOption { get; set; }
        public bool VersionNumberPositionFromStart { get; set; }
        public int VersionNumberPosition { get; set; }
        public bool AllowPDF { get; set; }
        public bool AllowDoc { get; set; }
        public bool AllowImage { get; set; }
        public bool AllowVideo { get; set; }
        public bool AllowSWF { get; set; }
        public bool AllowZip { get; set; }
        public bool ZipFilesWhenDownloading { get; set; }
        public bool ApplyUploadSettingsForNewApproval { get; set; }
        public bool UseUploadSettingsFromPreviousVersion { get; set; }
        public bool EnableVersionMirroring { get; set; }
        public List<UserGroupPermissions> userGroupPermission { get; set; }
        public bool IsEnabledMediaTools { get; set; }

        public bool AllowApiStatusUpdate { get; set; }
        public bool AllowApiApprovalDueDate { get; set; }
        public bool AllowApiPhaseDueDate { get; set; }
        public bool AllowApiOnAnnotation { get; set; }
        [StringLength(250, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [RegularExpression(@"^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$", ErrorMessageResourceName = "errUrlFormatIsWrong", ErrorMessageResourceType = typeof(Resources))]
        public string PostStatusUpdatesURL { get; set; }

        #endregion

        #region Ctors

        public UploadEngineAccountSettings()
        {
        }

        public UploadEngineAccountSettings(UploadEngineAccountSetting settings)
        {
            if (settings != null)
            {
                DuplicateFileOption = settings.UploadEngineDuplicateFileName;
                VersionNameOption = settings.UploadEngineVersionName;
                VersionNumberPositionFromStart = settings.VersionNumberPositionFromStart.HasValue && settings.VersionNumberPositionFromStart.Value;
                VersionNumberPosition = settings.VersionNumberPositionInName.HasValue ? settings.VersionNumberPositionInName.Value : 0;
                AllowPDF = settings.AllowPDF;
                AllowImage = settings.AllowImage;
                AllowVideo = settings.AllowVideo;
                AllowSWF = settings.AllowSWF;
                AllowZip = settings.AllowZip;
                AllowDoc = settings.AllowDoc;
                ZipFilesWhenDownloading = settings.ZipFilesWhenDownloading;
                ApplyUploadSettingsForNewApproval = settings.ApplyUploadSettingsForNewApproval;
                UseUploadSettingsFromPreviousVersion = settings.UseUploadSettingsFromPreviousVersion;
                EnableVersionMirroring = settings.EnableVersionMirroring;
                PostStatusUpdatesURL = settings.PostStatusUpdatesURL;
                AllowApiStatusUpdate = settings.AllowApiStatusUpdate;
                AllowApiApprovalDueDate = settings.AllowApiApprovalDueDate;
                AllowApiPhaseDueDate = settings.AllowApiPhaseDueDate;
                AllowApiOnAnnotation = settings.AllowApiOnAnnotation;
            }
        }

        #endregion
    }

    [Serializable]
    public class UserGroupPermissions
    {
        #region Properties

        public int UserGroupID { get; set; }
        public string UserGroupName { get; set; }       
        public bool Uploader { get; set; }
        public bool DownloadFromCollaborate { get; set; }
        public bool DownloadFromDeliver { get; set; }       
        public bool AllPermissionsChecked { get; set; }

        #endregion
    }
}
