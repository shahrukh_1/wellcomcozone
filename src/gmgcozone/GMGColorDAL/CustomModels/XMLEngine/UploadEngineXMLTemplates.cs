﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GMGColorDAL.Validation;
using System.Linq;
using RSC = GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorDAL.CustomModels.XMLEngine
{
    [Serializable]
    public class UploadEngineXMLTemplatesDashBoardViewModel
    {
        #region Properties 

        public int ID { get; set; }

        public string Name { get; set; }

        public bool CanBeDeleted { get; set; }

        #endregion
    }

    [Serializable]
    public class UploadEngineXMLTemplates : Common
    {
        #region Properties 

        public int? page { get; set; }

        public int? SelectedInstance { get; set; }

        public List<UploadEngineXMLTemplatesDashBoardViewModel> ListXMLTemplates { get; set; }

        #endregion

        #region Constructors 

        public UploadEngineXMLTemplates(Account objAccount, GMGColorContext context)
            : base(objAccount, context)
        {
            ListXMLTemplates = GMGColorDAL.UploadEngineCustomXMLTemplate.PopulateXMLTemplatesGrid(objAccount.ID, context);
        }

        #endregion
    }

    [Serializable]
    public class XMLTemplateModel
    {
        #region Properties

        public int? page { get; set; }

        public int? ID { get; set; }

        public List<DefaultXMLField> DefaultFieldsList { get; set; }

        public List<DefaultXMLField> AllDefaultTemplatesList { get; set; }

        public List<CustomXMLField> SelectedCustomFields { get; set; }

        [Required(ErrorMessageResourceName = "reqCustomTemplateName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }

        #endregion

        #region Constructors

        public XMLTemplateModel()
        {

        }

        public XMLTemplateModel(GMGColorContext context, int? selectedCustomTemplate = null)
        {
            AllDefaultTemplatesList = GMGColorDAL.UploadEngineCustomXMLTemplate.GetDefaultXMLFields(context);

            if (selectedCustomTemplate != null)
            {
                SelectedCustomFields = GMGColorDAL.UploadEngineCustomXMLTemplate.GetCustomXMLFields(selectedCustomTemplate.Value, context);
                ID = selectedCustomTemplate;
                Name = GMGColorDAL.UploadEngineCustomXMLTemplate.GetCustomTemplateName(selectedCustomTemplate.Value, context);
                DefaultFieldsList = AllDefaultTemplatesList.Where(t => SelectedCustomFields.All(o => o.DefaultXMLTemplateKey != t.Key)).ToList();
            }
            else
            {
                DefaultFieldsList = AllDefaultTemplatesList;
            }
        }

        #endregion
    }

    [Serializable]
    public class CustomXMLField 
    {
        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqXPath", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        [XPath]
        public string XPath { get; set; }

        public int DefaultXMLTemplateID { get; set; }

        public int DefaultXMLTemplateKey { get; set; }

        public string DefaultXMLTemplateNodeName { get; set; }

        public bool IsDeleted { get; set; }

        #endregion
    }

    [Serializable]
    public class DefaultXMLField
    {
        #region Properties

        public int ID { get; set; }

        public int Key { get; set; }

        public string XPath { get; set; }

        public string NodeName { get; set; }

        #endregion
    }

    public class XMLField : DefaultXMLField
    {
        #region Properties

        public bool IsCustom { get; set; }

        #endregion
    }
}
