﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RSC = GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorDAL.CustomModels.XMLEngine
{
    [Serializable]
    public class UploadEngineProfileDashBoardViewModel
    {
        #region Properties 

        public int ID { get; set; }
        public string Name { get; set; }
        public bool HasInputSettings { get; set; }
        public string OutputPreset { get; set; }
        public string XMLTemplate { get; set; }
        public bool IsActive { get; set; }

        #endregion
    }

    [Serializable]
    public class UploadEngineProfiles : Common
    {
        #region Properties 

        public List<UploadEngineProfileDashBoardViewModel> ListProfiles { get; set; }

        public int? page { get; set; }

        public string SelectedInstance { get; set; }

        #endregion

        #region Constructors 

        public UploadEngineProfiles(Account objAccount, GMGColorContext context)
            : base(objAccount, context)
        {
            ListProfiles = GMGColorDAL.UploadEngineProfile.PopulateProfilesGrid(objAccount.ID, context);
        }

        #endregion
    }

    [Serializable]
    public class ProfileModel
    {
        #region Properties 

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqProfileName", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }

        public bool HasInputSettings { get; set; }

        public int? OutputPreset { get; set; }

        public int? XMLTemplate { get; set; }

        public List<KeyValuePair<int, string>> ListPresets { get; set; }

        public List<KeyValuePair<int, string>> ListXMLTemplates { get; set; }

        public int? page { get; set; }

        #endregion

        #region Constructors

        public ProfileModel()
        {

        }

        public ProfileModel(int accountId, GMGColorContext context, int selectedProfile = 0)
        {
            ListPresets = GMGColorDAL.UploadEnginePreset.GetPresetsList(accountId, context);
            ListXMLTemplates = GMGColorDAL.UploadEngineProfile.GetXMLTemplatesList(accountId, context);
            if (selectedProfile > 0)
            {
                UploadEngineProfile profileBo = GMGColorDAL.UploadEngineProfile.GetObjectById(selectedProfile, context);
                ID = profileBo.ID;
                Name = profileBo.Name;
                OutputPreset = profileBo.OutputPreset;
                XMLTemplate = profileBo.XMLTemplate;
                HasInputSettings = profileBo.HasInputSettings;
            }
        }

        #endregion
    }
}
