﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels.XMLEngine
{
    [Serializable]
    public class FTPProtocol
    {
        #region Properties

        public int ID { get; set; }

        public string Name { get; set; }

        #endregion
    }
}
