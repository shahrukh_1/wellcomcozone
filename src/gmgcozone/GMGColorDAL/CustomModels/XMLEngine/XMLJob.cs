﻿using System;
using System.Collections.Generic;
using GMG.CoZone.Common;
using System.IO;

namespace GMGColorDAL.CustomModels.XMLEngine
{
    /// <summary>
    /// XML job object (XML job representation of the CoZone job)
    /// </summary>
    public class XMLJob
    {
        #region Inner classes
        
        /// <summary>
        /// Collaborate XML job 
        /// </summary>
        public class  XMLCollaborateJob
        {
            #region Properties

            public Dictionary<string, string> InternalCollaborator { get; set; }
            public Dictionary<string, string> ExternalCollaborator { get; set; }
            public KeyValuePair<string, bool> PrimaryDecisionMaker { get; set; }
            public string Owner { get; set; }
            public bool GenerateSeparations { get; set; }
            public bool LockWhenAllDecisionsHaveBeenMade { get; set; }
            public string Deadline { get; set; }
            public bool AllowUsersToDownloadOriginalFile { get; set; }
            public string DestinationFolder { get; set; }
            public bool LockWhenFirstDecisionHasBeenMade { get; set; }

            #endregion

            #region Ctors

            public XMLCollaborateJob()
            {
                InternalCollaborator = new Dictionary<string, string>();
                ExternalCollaborator = new Dictionary<string, string>();
            }

            #endregion

        }

        /// <summary>
        /// Deliver XML job
        /// </summary>
        public class XMLDeliverJob
        {
            #region Properties

            public string ColorProofCozoneInstanceName { get; set; }
            public string ColorProofCozoneWorkflow { get; set; }
            public string Pages { get; set; }
            public bool LogProofControlResults { get; set; }
            public bool IncludeProofMetaInformationOnProofLabel { get; set; }
            #endregion
        }       

        #endregion

        #region Properties

        public string UserGUID { get; set; }
        public string JobSourceFolderRelativeToUserFolder { get; set; }
        public string OriginalFolderPath { get; set; }
        public string XMLFilePath { get; set; }
        public string AssetFilePath { get; set; }
        public string JobName { get; set; }
        public string FileName { get; set; }
        public FileType.FileExtention FileType { get; set; }
        public long FileSize { get; set; }
        public int Modules { get; set; }
        public XMLCollaborateJob Collaborate { get; set; }
        public XMLDeliverJob Deliver { get; set; }

        #endregion

        #region Ctors

        public XMLJob(string filePath)
        {
            Collaborate = new XMLCollaborateJob();          
            Deliver = new XMLDeliverJob();

            AssetFilePath = filePath;
            FileInfo fileInfo = new FileInfo(filePath);
            FileName = fileInfo.Name;
            FileSize = fileInfo.Length;
            FileType = GMGColorDAL.FileType.GetFileExtention(FileName);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sets Modules For Current Job
        /// </summary>
        /// <param name="modules"></param>
        public void SetModules(string modules)
        {
            string[] modulesArray = modules.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string module in modulesArray)
            {
                switch (module)
                {
                    case Constants.CollaborateFolderName:
                        {
                            Modules |= (int)XMLJobType.Collaborate;
                            break;
                        }
                    case Constants.DeliverFolderName:
                        {
                            Modules |= (int)XMLJobType.Deliver;
                            break;
                        }
                }
            }
        }

        #endregion
    }
}
