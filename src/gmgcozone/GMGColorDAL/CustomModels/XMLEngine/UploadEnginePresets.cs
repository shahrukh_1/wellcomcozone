﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using RSC = GMGColor.Resources;
using GMG.CoZone.Common;

namespace GMGColorDAL.CustomModels.XMLEngine
{
    [Serializable]
    public class UploadEnginePresets : Common
    {
        public List<GMGColorDAL.UploadEnginePreset> ListPresets { get; set; }

        public int? page { get; set; }

        public string SelectedInstance { get; set; }

        public UploadEnginePresets(Account objAccount, GMGColorContext context)
            : base(objAccount, context)
        {
            ListPresets = GMGColorDAL.UploadEnginePreset.PopulatePresetsGrid(objAccount.ID, context);
        }
    }

    [Serializable]
    public class PresetModel
    {
        #region Properties 

        public int ID { get; set; }

        public string Guid { get; set; }

        public int Account { get; set; }

        [Required(ErrorMessageResourceName = "lblPresetNameRequired", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Name { get; set; }

        public bool IsEnabledInCollaborate { get; set; }

        public bool IsEnabledInDeliver { get; set; }

        [Required(ErrorMessageResourceName = "lblHostRequired", ErrorMessageResourceType = typeof(RSC.Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Host { get; set; }

        public int Port { get; set; }

        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Username { get; set; }

        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        public string Password { get; set; }

        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(RSC.Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string DefaultDirectory { get; set; }

        public bool ReplicateCoZoneFolders { get; set; }

        public bool IsActiveTransferMode { get; set; }

        public int Protocol { get; set; }

        public List<CustomModels.XMLEngine.FTPProtocol> ProtocolList { get; set; }

        public int? page { get; set; }

        public bool IsChangeLogin { get; set; }

        public string OldUserName { get; set; }

        #endregion

        #region Constructors

        public PresetModel()
        {
            ProtocolList = GMGColorDAL.FTPProtocol.GetProtocols();
            Port = 21;
            IsActiveTransferMode = true;
        }

        public PresetModel(int id, GMGColorContext context)
        {
            UploadEnginePreset presetBo = GMGColorDAL.UploadEnginePreset.GetObjectById(id, context);
            ID = presetBo.ID;
            Name = presetBo.Name;
            Host = presetBo.Host;
            Port = presetBo.Port;
            Username = presetBo.Username;
            Password = presetBo.Password;
            IsActiveTransferMode = presetBo.IsActiveTransferMode;
            IsEnabledInCollaborate = presetBo.IsEnabledInCollaborate;
            IsEnabledInDeliver = presetBo.IsEnabledInDeliver;           
            Protocol = presetBo.Protocol;
            DefaultDirectory = presetBo.DefaultDirectory;
            ReplicateCoZoneFolders = presetBo.ReplicateCoZoneFolders;
            OldUserName = presetBo.Username;

            ProtocolList = GMGColorDAL.FTPProtocol.GetProtocols(context);
        }
        #endregion
    }
}
