﻿using GMGColor.Resources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GMGColorDAL.CustomModels
{
    public class CustomSMTPServerViewModel
    {
        public CustomSMTPServerViewModel()
        {
            IsEnabled = false;
        }

        [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "lblSmtpServer", ResourceType = typeof(Resources))]
        [Required(ErrorMessageResourceName = "reqHostName", ErrorMessageResourceType = typeof(Resources))]
        public string ServerName { get; set; }
        
        [Required]
        public int? Port { get; set; }

        public bool IsPasswordEnabled { get; set; }

        public bool IsEnabled { get; set; }

        public bool EnableSsl { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "lblEmail", ResourceType = typeof(Resources))]
        [Required]
        public string EmailAddress { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "lblPassword", ResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Validation.RequiredIf("IsPasswordEnabled", true, ErrorMessageResourceName = "errPasswordRequired", ErrorMessageResourceType = typeof(Resources))]
        public string Password { get; set; }

        [Display(Name = "lblConfirmPassword", ResourceType = typeof(Resources))]
        [System.Web.Mvc.Compare("Password", ErrorMessageResourceName = "reqPasswordsDoNotMatch", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Validation.RequiredIf("IsPasswordEnabled", true, ErrorMessageResourceName = "errConfirmPassword", ErrorMessageResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }
    }
}
