﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class NavigationModel : Common
    {
        private bool _isAdminApp;
        private int _childMenuItemId;
        private int _parentMenuItemId;
        private bool _isHomePage;
        private bool _isPersonalSettingsApp;

        #region Properties

        public GMGColorDAL.User LoggedUser { get; set; }
        public List<UserMenuItemRoleView> TopLeftMenuItems { get; set; }
        public List<UserMenuItemRoleView> SettingsMenuItems { get; set; }
        public List<UserMenuItemRoleView> AdministrationMenuItems { get; set; }

        public List<UserMenuItemRoleView> MenuItemsUnderAccount { get; set; }
        public List<UserMenuItemRoleView> MenuItemsUnderUser { get; set; }

        public List<UserMenuItemRoleView> PageMenuItems { get; set; }

        public UserMenuItemRoleView ApplicationItem { get; set; }

        public bool CurrentPageIsHome
        {
            get { return _isHomePage; }
        }

        public bool IsAdminApp
        {
            get { return _isAdminApp; }
        }

        public bool IsPersonalSettingsApp
        {
            get { return _isPersonalSettingsApp; }
        }

        public string HomePageUrl { get; set; }
        public int ParentMenuItemID
        {
            get
            {
                return _parentMenuItemId;
            }
            set { 
                _parentMenuItemId = value;
            }
        }
        public int ChildMenuItemID
        {
            get
            {
                return _childMenuItemId;
            }
            set
            {
                _childMenuItemId = value;
            }
        }

        public string ParentMenuKey { get; set; }

        #endregion

        #region Constructors

        public NavigationModel()
        {

        }

        public NavigationModel(Account objAccount, AccountType.Type accountType)
        {
            this.objLoggedAccount = objAccount;
            this.objLoggedAccountType = accountType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get Localized Menu Item Name
        /// </summary>
        /// <param name="ID">Menu Item ID</param>
        /// <returns></returns>
        public static string GetLocalizedMenuName(int ID, GMGColorContext context)
        {
            string key = (from mi in context.MenuItems where mi.ID == ID select mi.Key).FirstOrDefault();
            return NavigationModel.GetLocalizedMenuName(key);
        }

        /// <summary>
        /// Gets the name of the localized header.
        /// </summary>
        /// <param name="ID">The ID.</param>
        /// <returns></returns>
        public static string GetLocalizedHeaderName(int ID, GMGColorContext context)
        {
            string key = (from mi in context.MenuItems where mi.ID == ID select mi.Key).FirstOrDefault();
            return NavigationModel.GetLocalizedHeaderName(key);
        }

        /// <summary>
        /// Gets the name of the localized header.
        /// </summary>
        /// <param name="KEY">The KEY.</param>
        /// <returns></returns>
         public static string GetLocalizedHeaderName(string KEY)
         {
             switch (KEY)
             {
                 case "SEIN" :
                     return Resources.navSettingsIndex;
                 default:
                     return GetLocalizedMenuName(KEY);
             }
         }

        /// <summary>
        /// Get Localized Menu Item Name
        /// </summary>
        /// <param name="ID">Menu Item BO</param>
        /// <returns></returns>
        public static string GetLocalizedMenuName(MenuItem objMenuItem)
        {
            return NavigationModel.GetLocalizedMenuName(objMenuItem.Key);
        }

        /// <summary>
        /// Get Localized Menu Item Name
        /// </summary>
        /// <param name="KEY">Menu Item Key</param>
        /// <returns></returns>
        public static string GetLocalizedMenuName(string KEY)
        {
            switch (KEY)
            {
                // Dashboard
                case "DBIN": return Resources.navDashboardIndex;
                case "DBAP":
                case "ACST": return Resources.navDashboardAdministratorSite;
                // Account    
                case "ACIN": return Resources.navAccountIndex;
                case "ACNW": return Resources.navAccountNewAccount;
                case "ACAI": return Resources.navAccountWizardStep1;
                case "ACAP": return Resources.navAccountWizardStep2;
                case "ACAC": return Resources.navAccountWizardStep3;
                case "ACCA": return Resources.navAccountWizardStep4;
                case "SESM":
                case "ACSM": return Resources.navAccountSummary;
                case "SEPF":
                case "ACPF": return Resources.navAccountProfile;
                case "SECU":
                case "ACCU": return Resources.navAccountCustomise;
                case "SESS":
                    return Resources.navGlobalSettings;
                case "ACSS": return Resources.navAccountSiteSettings;
                case "SEDN":
                case "ACDN": return Resources.navAccountDNS;
                case "SEWL":
                case "ACWL": return Resources.navAccountWhiteLabels;
                case "BBEU": return Resources.lblBouncedEmailUsers;
                case "SEUS":
                case "ACUS": return Resources.navAccountUsers;
                case "SEEU":
                case "ACEU": return Resources.navAccountEditUser;
                case "RPBI":
                case "ACBI": return Resources.navAccountBilingInfo;
                case "RPPL":
                case "ACPL": return Resources.navAccountPlans;
                case "ACSA": return Resources.navAccountSuspendAccount;
                case "SEGR": return Resources.navAccountGroups;
                case "SEDS": return Resources.navAccountDeliverSettings;
                case "SEEG": return Resources.navAccountEditGroups;
                case "CLGS": return Resources.lblGeneralSettings;
                case "SSRS": return Resources.navRegionalSettings;
                case "SSN": return Resources.navNotifications;
                case "SCB": return Resources.navBranding;
                case "SCGL": return Resources.navCustomizeGlobal;
                case "AEBP": return Resources.navAddBrandigPreset;
                case "AABP": return Resources.navAddBrandigPreset;
                case "CLCD": return Resources.navApprovalStatus;
                case "CLAW": return Resources.navApprovalWorkflow;
                case "CLCH": return Resources.navCheckList;
                case "PDTW": return Resources.navPredefinedtagwords;
                // Plans
                case "PLIN": return Resources.navPlansIndex;
                case "PLBP": return Resources.navPlansNewPlan;
                case "PLNC": return Resources.navNewCollaborateBillingPlan;
                // Reports
                case "RPIN": return Resources.navReportsIndex;
                case "RPAR": return Resources.navReportsAccount;
                case "RPUR": return Resources.navReportsUser;
                case "RPBR": return Resources.navReportsBilling;
                case "RPPR": return Resources.navReportsPrint;
                // Application Settings
                case "APST": return Resources.navApplicationSettingsIndex;
                // Services
                case "SVIN": return Resources.navServicesIndex;
                case "SVMS": return Resources.navMediaServices;
                // Global Settings
                case "GSET": return Resources.navSettings;
                case "GSEP": return Resources.navGlobalSettings;
                // Settings
                case "SEIN": return Resources.navSettingsIndex;
                case "SEEX": return Resources.navAccountExternalUsers;
                case "SERD": return Resources.navSettingsReservedSubDomains;
                case "CSMT": return Resources.navCustomSmtpServer;
                case "SISO": return Resources.navSso;
                // Welcome
                case "WLIN": return Resources.navWelcomeIndex;
                // Personal Settings
                case "PSIN": return Resources.navPersonalSettingsIndex;
                case "PSPI": return Resources.navPersonalSettingsInformation;
                case "PSPH": return Resources.navPersonalSettingsPhoto;
                case "PSPW": return Resources.navPersonalSettingsPassword;
                case "PSNT": return Resources.navPersonalSettingsNotifications;
                case "PSOO": return Resources.navPersonalSettingsOutOfOffice;
                // Help
                case "HLIN": return Resources.navHelpIndex;
                // Logout
                case "LGOT": return Resources.navLogout;
                // Studio
                case "STIN": return Resources.navStudioIndex;
                case "STAD":
                case "APDB": return Resources.navStudioDashboard;
                case "STFL": return Resources.navStudioFlex;
                // Approvals
                case "APIN": return Resources.navApprovalsIndex;
                case "APNA": return Resources.navApprovalsNewApproval;
                case "APDE": return Resources.navApprovalsDetails;
                // Deliver
                case "DLIN": return Resources.navDeliverIndex;
                case "DLIM": return Resources.navStudioDashboard;
                case "DLNJ": return Resources.navDeliverNewJob;
                case "HOME": return Resources.navHome;
                case "CONT": return Resources.navContact;               

                case "SESP": return Resources.navSoftProofingSettings;
                case "SETS": return Resources.navSettingsIndexTopMenu;
                case "ACSE":
                case "SETT": return Resources.navSettings;
                case "USGR": return Resources.navUsersAndGroups;
                case "COLS": return Resources.lblCollaborateModuleName;
                case "DEVS": return Resources.lblDeliverModuleName;
                case "APPS": return Resources.navApplicationSettings;
                case "CMSI": return Resources.navCMS;
                case "CMSA": return Resources.navCMSAnnouncements;
                case "CMSV": return Resources.navCMSVideos;
                case "CMSM": return Resources.navCMSManuals;
                case "CMSD": return Resources.navCMSDownloads;
                // XML Engine
                case "SEUE": return Resources.navUploadEngine;
                case "UETM": return Resources.navUploadEngineTemplates;
                case "UESE": return Resources.navUploadEngineSettings;
                // SoftProofing
                case "GSSP": return Resources.lblPreviewProfiles;
                case "GSSM": return Resources.lblSimulationProfile;
                case "GSPS": return Resources.lblSoftProofingMediaCategory;
                //CMS Admin
                case "CMIT": return Resources.navCMS;
                case "CMAN": return Resources.navCMSAnnouncements;
                case "CMVD": return Resources.navCMSVideos;
                case "CMDA": return Resources.navCMSDownloads;
                case "CMMA": return Resources.navCMSManuals;

                //Account Reports
                case "RPAD": return Resources.lblAccountDetails;

                //File Transfer
                case "TRAN": return Resources.lblFileTransfer;
                case "TRNT": return Resources.lblFileTransfer;
                case "TRNA": return Resources.lblFileTransfer;
                case "TRFO": return Resources.lblFileTransfer;
                case "URIN": return Resources.lblReport;

                //Project Folders
                case "MYPF": return Resources.navMyProjectFoldersIndex;

                default: return "? Resource Not Found ! [" + KEY + "]";
            }
        }

        /// <summary>
        /// Updates the flags.
        /// </summary>
        /// <param name="context">The context.</param>
        public void UpdateFlags()
        {
            _isAdminApp = MenuItem.MenusForAdmin.Contains(ParentMenuKey);
            _isHomePage = MenuItem.MenusForHome.Contains(ParentMenuKey);
            _isPersonalSettingsApp = MenuItem.MenusForPersonalSettings.Contains(ParentMenuKey);
        }

        #endregion
    }

    public class SecondaryNavigationModel
    {
        public List<NavigationItem> Menus { get; set; }
        public SecondaryNavigationTypeEnum Type { get; set; }

        public SecondaryNavigationModel()
        {
            Menus = new List<NavigationItem>();
            Type = SecondaryNavigationTypeEnum.AccountSettings;
        }
    }

    public class NavigationItem
    {
        public string MenuKey { get; set; }
        public int MenuId { get; set; }
        public string DisplayName { get; set; }
        public bool Active { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int Parent { get; set; }
        public int Level { get; set; }
        public int Position { get; set; }

        public bool ActiveChild
        {
            get
            {
                return Active || Childs.Any(o => o.ActiveChild);
            }
        }

        public bool AnyChildWithControllerAndAction
        {
            get
            {
                return (!string.IsNullOrEmpty(Controller) && !string.IsNullOrEmpty(Action)) ||
                       Childs.Any(o => o.AnyChildWithControllerAndAction);
            }
        }

        public List<NavigationItem> Childs { get; set; }

        public NavigationItem()
        {
            Childs = new List<NavigationItem>();
        }
    }

    public enum SecondaryNavigationTypeEnum
    {
        AccountSettings,
        AdminSettings,
        EditAccountSettings,
        EditAccountReports
    }
}