﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class DuplicateUser
    {
        #region Properties

        [Required(ErrorMessageResourceName = "reqFirstName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string FirstName { get; set; }

        [Required(ErrorMessageResourceName = "reqLastName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string LastName { get; set; }

        [Required(ErrorMessageResourceName = "reqEmail", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(91, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string EmailAddress { get; set; }

        public int DuplicatedUser { get; set; }

        public bool IsFromEditAccount { get; set; }

        #endregion
    }
}
