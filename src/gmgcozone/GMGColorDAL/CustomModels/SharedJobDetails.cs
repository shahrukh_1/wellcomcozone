﻿using System;
using System.Linq;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class SharedJobDetails
    {
        #region Properties

        public string Username { get; set; }

        public string Email { get; set; }

        public string AccountUrl { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get details for shared job
        /// </summary>
        /// <param name="selectedDeliverJobId">Selected deliver job Id</param>
        /// <param name="context">Database context</param>
        /// <returns></returns>
        public static SharedJobDetails GetSharedJobDetails(int? selectedDeliverJobId, GMGColorContext context)
        {
            return (from j in context.DeliverJobs
                    join u in context.Users on j.Owner equals  u.ID
                    join ac in context.Accounts on u.Account equals ac.ID
                    where j.ID == selectedDeliverJobId
                    select new SharedJobDetails()
                        {
                            Username = u.Username,
                            Email = u.EmailAddress,
                            AccountUrl = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain
                        }).FirstOrDefault();
        }

        #endregion
    }
}
