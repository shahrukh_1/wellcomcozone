﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class OwnerUser
    {
        #region Properties

        public int ID { get; set; }
        
        public int Account { get; set; }

        public bool IsSsoUser { get; set; }

        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Validation.RequiredIf("IsSsoUser", false, ErrorMessageResourceName = "reqFirstName", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string FirstName { get; set; }
        
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        [Validation.RequiredIf("IsSsoUser", false, ErrorMessageResourceName = "reqLastName", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string LastName { get; set; }
        
        [Required(ErrorMessageResourceName = "reqUsername", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(91, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string Username { get; set; }
        
        [Required(ErrorMessageResourceName = "reqEmail", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(91, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string EmailAddress { get; set; }
        
        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string HomeTelephoneNumber { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string MobileTelephoneNumber { get; set; }

        public string PhotoPath { get; set; }
        
        public string Guid { get; set; }
          
        public bool HasNotifications { get; set; }

        public List<ModulePermision> ModulePermisions { get; set; }

        public int SelectedLanguage { get; set; }

        public Dictionary<int, string> Languages { get; set; }

        public int? PrintCompany { get; set; }

        public bool HasPasswordEnabled { get; set; }

        public bool PrivateAnnotations { get; set; }

        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8, ErrorMessageResourceName = "errIncorrectPassword", ErrorMessageResourceType = typeof(Resources))]
        [Validation.RequiredIf("HasPasswordEnabled", true, ErrorMessageResourceName = "errPasswordRequired", ErrorMessageResourceType = typeof(Resources))]
        [RegularExpression(Constants.PasswordValidDataPattern,ErrorMessage = "&#9758  &nbsp Password must contain:  <br /> &nbsp &nbsp \u2022 At least 1 uppercase letter  and At least 1 lowercase letter <br /> &nbsp &nbsp \u2022 At least 1 number <br /> &nbsp &nbsp \u2022 At least 1 special character")]
        [Display(Name = "lblSetPassword", ResourceType = typeof(Resources))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8, ErrorMessageResourceName = "errIncorrectPassword", ErrorMessageResourceType = typeof(Resources))]
        [System.Web.Mvc.Compare("Password")]
        [Validation.RequiredIf("HasPasswordEnabled", true, ErrorMessageResourceName = "errPasswordRequired", ErrorMessageResourceType = typeof(Resources))]
        [Display(Name = "lblConfirmPassword", ResourceType = typeof(Resources))]
        public string ConfirmPassword { get; set; }

        public int Locale { get; set; }

        #endregion

        #region Constructors

        public OwnerUser()
        {

        }

        public OwnerUser(GMGColorDAL.User objUser)
        {
            this.ID = objUser.ID;
            this.Account = objUser.Account;
            this.FirstName = objUser.GivenName;
            this.LastName = objUser.FamilyName;
            this.Username = objUser.Username;
            this.EmailAddress = objUser.EmailAddress;
            this.HomeTelephoneNumber = objUser.HomeTelephoneNumber;
            this.MobileTelephoneNumber = objUser.MobileTelephoneNumber;
            this.PhotoPath = objUser.PhotoPath;
            this.Guid = objUser.Guid;
            this.HasNotifications = false;
            this.IsSsoUser = objUser.IsSsoUser;
            this.Locale = objUser.Locale;
            this.PrivateAnnotations = objUser.PrivateAnnotations;
            PrintCompany = objUser.PrinterCompany;

        }

        public OwnerUser(GMGColorDAL.User objUser, GMGColorContext context)
        {
            ID = objUser.ID;
            Account = objUser.Account;
            FirstName = objUser.GivenName;
            LastName = objUser.FamilyName;
            Username = objUser.Username;
            EmailAddress = objUser.EmailAddress;
            HomeTelephoneNumber = objUser.HomeTelephoneNumber;
            MobileTelephoneNumber = objUser.MobileTelephoneNumber;
            PhotoPath = objUser.PhotoPath;
            Guid = objUser.Guid;
            PrintCompany = objUser.PrinterCompany;
            SelectedLanguage = objUser.Locale;
            Languages = GMGColorDAL.Locale.GetLocalizeLocaleNames(context);


            InitializePermissions(objUser.ID, objUser.Account, context);
        }

        #endregion

        #region Public Methods

        public void InitializePermissions(int userId, int accountId, GMGColorContext context)
        {
            try
            {
                if (this.ModulePermisions == null || this.ModulePermisions != null && !this.ModulePermisions.Any())
                {
                    this.ModulePermisions = new List<ModulePermision>();

                    List<IGrouping<int, Role>> groups = GMGColorDAL.User.GetRoles(accountId, context);

                    UserInfo.MoveAdministrationRoleToTheEnd(groups, context);

                    groups.ForEach(
                        g =>
                        this.ModulePermisions.Add(new ModulePermision()
                        {
                            ModuleName = GMGColorCommon.GetModuleName((GMG.CoZone.Common.AppModule)g.First().AppModule1.Key),
                            ModuleId = g.First().AppModule,
                            ModuleType = (GMG.CoZone.Common.AppModule)g.First().AppModule1.Key,
                            Roles =
                                g.Select(
                                    r =>
                                    new RolePermision()
                                    {
                                        RoleId = r.ID,
                                        RoleName = r.Name
                                    }).ToList()
                        }));
                }
               InitializePermissions(userId, this.ModulePermisions.ToDictionary(o => o.ModuleType, o => (int?)null), context);
            }
            finally
            {
            }
        }

        public void InitializePermissions(int userId, Dictionary<GMG.CoZone.Common.AppModule, int?> selectedRolePerModule, GMGColorContext context)
        {
            try
            {
                foreach (KeyValuePair<GMG.CoZone.Common.AppModule, int?> keyValuePair in selectedRolePerModule)
                {
                    var module = ModulePermisions.FirstOrDefault(o => o.ModuleType == keyValuePair.Key);
                    if (module != null)
                    {
                        module.Roles = (from r in context.Roles
                                        join am in context.AppModules on r.AppModule equals am.ID
                                        orderby r.Priority
                                        where am.Key == (int)module.ModuleType
                                        select new RolePermision()
                                        {
                                            RoleId = r.ID,
                                            RoleName = r.Name,
                                            RoleChecked = false
                                        }).ToList();
                    }
                }

                foreach (KeyValuePair<GMG.CoZone.Common.AppModule, int?> keyValuePair in selectedRolePerModule)
                {
                    var module = ModulePermisions.FirstOrDefault(o => o.ModuleType == keyValuePair.Key);
                    if (module != null)
                    {
                        UserRole userRole = (from ur in context.UserRoles
                                             where
                                                 ur.User == userId &&
                                                 (GMG.CoZone.Common.AppModule)ur.Role1.AppModule1.Key == keyValuePair.Key
                                             select ur).FirstOrDefault();

                        if (keyValuePair.Value.HasValue)
                        {
                            module.SelectedRole = keyValuePair.Value.Value;
                            RolePermision role = module.Roles.FirstOrDefault(o => o.RoleId == keyValuePair.Value);
                            if (role != null)
                            {
                                role.RoleChecked = true;
                            }
                        }
                        else if (userRole != null)
                        {
                            module.SelectedRole = userRole.Role;
                            module.Roles.Where(o => o.RoleId == userRole.Role).ToList().ForEach(o => o.RoleChecked = true);
                        }
                        else
                        {
                            RolePermision rolePermision = module.Roles.FirstOrDefault();
                            if (rolePermision != null)
                            {
                                module.SelectedRole = rolePermision.RoleId;
                                rolePermision.RoleChecked = true;
                            }
                        }
                    }
                }
            }
            finally
            {
            }
        }


        #endregion
    }
}
