﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL.Common;
using System.Data.Entity.SqlServer;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using GMGColorDAL.CustomModels.Checklists;
using System.Web.Mvc;
using GMG.CoZone.Collaborate.DomainModels.Annotation;
using System.Web;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ApprovalsModel
    {
        #region Properties

        public int FolderID { get; set; }
        public int Modifier { get; set; }
        public int LoggedUserID { get; set; }
        public string BreadCrum { get; set; }
        public FolderCR objFolderCR { get; set; }
        public bool FolderPermissionsAutoInheritanceEnabled { get; set; }

        #endregion

        #region Constructors

        public ApprovalsModel()
        {
           
        }

        #endregion

        #region Sub Class

        public class FolderCR
        {
            public int ID { get; set; }
            public int Parent { get; set; }
            [Required(ErrorMessageResourceName = "reqFolderName", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
            public string Name { get; set; }
            public bool IsPrivate { get; set; }
            public bool AllCollaboratorsDecisionRequired { get; set; }
        }

        #endregion
    }


   
    /// add
    [Serializable]
    public class NewFileTransferFolderModel : Common
    {

        #region Constants

        public const string acceptedImageExtenssions = "jpe?g|png|tiff|eps|pdf";
        public const string acceptedPdfExtenssions = "pdf";
        public const string acceptedZipExtensions = "zip";
        public const string acceptedSWFExtensions = "swf";
        public const string acceptedVideoExtenssions = "mp4|m4v|avi|f4v|flv|mpg|mpg2|wmv|rm|asf|3gp|mov|vob|xvid|3vix|mjpeg|qt";

        #endregion

        #region Properties

        public int Folder { get; set; }

        [Required(ErrorMessageResourceName = "reqTitle", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Title { get; set; }

        [RegularExpression(@"^((?:https?\:\/\/|.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$|^([a-zA-Z0-9]{1,})(\.[a-zA-Z]{2,})+$", ErrorMessageResourceName = "valUrl", ErrorMessageResourceType = typeof(Resources))]
        public string Url { get; set; }

        public string AcceptedFiles { get; set; }
       

        public int Creator { get; set; }
        public int Owner { get; set; }
        public string ExternalUsers { get; set; }
        public string ExternalEmails { get; set; }
        public List<PermissionsModel.UserOrUserGroup> SelectedUsersAndGroups { get; set; }


        public bool IsDueDateSelected { get; set; }


        public string ErrorMessage { get; set; }
        public bool HasWarnings { get; set; }

        //[AllowHtml]
        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string OptionalMessage { get; set; }

        public FolderCollaboratorInfo ApprovalVersionCollaboratorInfo { get; set; }


        [Required(ErrorMessageResourceName = "reqDeliverJobTtile", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string FileTitleValidator { get; set; }



        public string OriginalFileName { get; set; }

        #endregion

        #region Constructors

        public NewFileTransferFolderModel()
        {

        }

        public NewFileTransferFolderModel(int folderId,Account objAccount, int loggedUserId, string folderPath, GMGColorContext context, out AccountFileTypeFilter fileTypeFilter)
            : base(objAccount, context)
        {

            ErrorMessage = string.Empty;

            // Accepted file types
            fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(loggedUserId, context);
            List<string> filters = new List<string>();
            ApprovalVersionCollaboratorInfo = new FolderCollaboratorInfo();


            if (fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval)
            {
                if (fileTypeFilter.UploadSettingsFilter.AllowPDF) filters.Add(acceptedPdfExtenssions);
                if (fileTypeFilter.UploadSettingsFilter.AllowImage) filters.Add(acceptedImageExtenssions);
                if (fileTypeFilter.UploadSettingsFilter.AllowSWF && fileTypeFilter.PlanIncludesVideo) filters.Add(acceptedSWFExtensions);
                if (fileTypeFilter.UploadSettingsFilter.AllowZip) filters.Add(acceptedZipExtensions);
                if (fileTypeFilter.UploadSettingsFilter.AllowVideo && fileTypeFilter.PlanIncludesVideo) filters.Add(acceptedVideoExtenssions);
            }
            else
            {
                filters.AddRange(new List<string> { acceptedImageExtenssions, acceptedPdfExtenssions, acceptedZipExtensions });
                if (fileTypeFilter.PlanIncludesVideo)
                    filters.AddRange(new List<string> { acceptedVideoExtenssions, acceptedSWFExtensions });
            }

            AcceptedFiles = "/(" + String.Join("|", filters.Select(i => i)) + ")$/i";

            Folder = folderId;


        }

        #endregion
    }
    /// stop

    [Serializable]
    public class NewFileTransferApprovalModel : Common
    {

        #region Constants

        public const string acceptedImageExtenssions = "jpe?g|png|tiff|eps|pdf";
        public const string acceptedPdfExtenssions = "pdf";
        public const string acceptedZipExtensions = "zip";
        public const string acceptedSWFExtensions = "swf";
        public const string acceptedVideoExtenssions = "mp4|m4v|avi|f4v|flv|mpg|mpg2|wmv|rm|asf|3gp|mov|vob|xvid|3vix|mjpeg|qt";

        #endregion

        #region Properties
        

        public int Approval { get; set; }
      
        [Required(ErrorMessageResourceName = "reqTitle", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Title { get; set; }

        [RegularExpression(@"^((?:https?\:\/\/|.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$|^([a-zA-Z0-9]{1,})(\.[a-zA-Z]{2,})+$", ErrorMessageResourceName = "valUrl", ErrorMessageResourceType = typeof(Resources))]
        public string Url { get; set; }

        public string AcceptedFiles { get; set; }
        public int Folder { get; set; }

        public int Creator { get; set; }
        public int Owner { get; set; }
        public string ExternalUsers { get; set; }
        public string ExternalEmails { get; set; }
        public List<PermissionsModel.UserOrUserGroup> SelectedUsersAndGroups { get; set; }

 
        public bool IsDueDateSelected { get; set; }
        

        public string ErrorMessage { get; set; }
        public bool HasWarnings { get; set; }

        //[AllowHtml]
        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        //[StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string OptionalMessage { get; set; }

        public ApprovalVersionCollaboratorInfo ApprovalVersionCollaboratorInfo { get; set; }
    

        [Required(ErrorMessageResourceName = "reqDeliverJobTtile", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string FileTitleValidator { get; set; }

       

        public string OriginalFileName { get; set; }

        #endregion

        #region Constructors

        public NewFileTransferApprovalModel()
        {

        }

        public NewFileTransferApprovalModel(Account objAccount, int loggedUserId, string folderPath, GMGColorContext context, out AccountFileTypeFilter fileTypeFilter)
            : base(objAccount, context)
        {
            
            ErrorMessage = string.Empty;

            // Accepted file types
            fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(loggedUserId, context);
            List<string> filters = new List<string>();
            ApprovalVersionCollaboratorInfo = new ApprovalVersionCollaboratorInfo();
           

            if (fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval)
            {
                if (fileTypeFilter.UploadSettingsFilter.AllowPDF) filters.Add(acceptedPdfExtenssions);
                if (fileTypeFilter.UploadSettingsFilter.AllowImage) filters.Add(acceptedImageExtenssions);
                if (fileTypeFilter.UploadSettingsFilter.AllowSWF && fileTypeFilter.PlanIncludesVideo) filters.Add(acceptedSWFExtensions);
                if (fileTypeFilter.UploadSettingsFilter.AllowZip) filters.Add(acceptedZipExtensions);
                if (fileTypeFilter.UploadSettingsFilter.AllowVideo && fileTypeFilter.PlanIncludesVideo) filters.Add(acceptedVideoExtenssions);
            }
            else
            {
                filters.AddRange(new List<string> { acceptedImageExtenssions, acceptedPdfExtenssions, acceptedZipExtensions });
                if (fileTypeFilter.PlanIncludesVideo)
                    filters.AddRange(new List<string> { acceptedVideoExtenssions, acceptedSWFExtensions });
            }

            AcceptedFiles = "/(" + String.Join("|", filters.Select(i => i)) + ")$/i";

            Folder = 0;


        }

        #endregion
    }
    

    [Serializable]
    public class NewApprovalModel : Common
    {
        #region Enums

        public enum RenderOptions
        {
            EmbeddedProfile = 0,
            CustomProfile
        }

        #endregion

        #region Constants

        public const string acceptedImageExtenssions = "jpe?g|png|tiff|eps|pdf|psd";
        public const string acceptedPdfExtenssions = "pdf";
        public const string acceptedZipExtensions = "zip";
        public const string acceptedSWFExtensions = "swf";
        public const string acceptedDocumentExtensions = "docx|doc|ppt|pptx";
        public const string acceptedVideoExtenssions = "mp4|m4v|avi|f4v|flv|mpg|mpg2|wmv|rm|asf|3gp|mov|vob|xvid|3vix|mjpeg|qt";

        #endregion

        #region Properties

        public bool AllowUsersToDownload { get; set; }
        public bool IsPageSpreads { get; set; }
        public bool OnlyOneDecisionRequired { get; set; }
        public bool PrivateAnnotations { get; set; }
        public bool RestrictedZoom { get; set; }
        public bool LockProofWhenAllDecisionsMade { get; set; }
        public bool LockProofWhenFirstDecisionsMade { get; set; }
        public bool KeepOriginalFileName { get; set; }

        public int Checklist { get; set; }

        public string PrimaryDecisionMaker { get; set; }

        [Required(ErrorMessageResourceName = "reqPrimaryDecisionMaker", ErrorMessageResourceType = typeof(Resources))]
        public string PrimaryDecisionMakerHiddenField { get; set; }

        public int Job { get; set; }
        public int Version { get; set; }
        public int Folder { get; set; }

        public string ApprovalDeadlineDate { get; set; }
        public string DeadlineTime { get; set; }
        public string DeadlineTimeMeridiem { get; set; }
        public string PathToTempFolder { get; set; }
        [Required(ErrorMessageResourceName = "reqTitle", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Title { get; set; }

        [RegularExpression(@"^((?:https?\:\/\/|.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$|^([a-zA-Z0-9]{1,})(\.[a-zA-Z]{2,})+$", ErrorMessageResourceName = "valUrl", ErrorMessageResourceType = typeof(Resources))]
        public string Url { get; set; }

        public string AcceptedFiles { get; set; }
        public string WebPageSnapshotDelay { get; set; }
        public Dictionary<int, string> ListDecisionMakers { get; set; }

        public int Creator { get; set; }
        public int PDM { get; set; }
        public int Owner { get; set; }
        public string ExternalUsers { get; set; }
        public string ExternalEmails { get; set; }
        public List<PermissionsModel.UserOrUserGroup> SelectedUsersAndGroups { get; set; }

        public bool IsUploadApproval { get; set; }
        public bool IsDueDateSelected { get; set; }

        public int ApprovalWorkflowId { get; set; }
        public List<ApprovalWorkflowModel> ApprovalWorkflows { get; set; }
        public bool AllowOtherUsersToBeAdded { get; set; }

        public int ChecklistId { get; set; }
        public List<ChecklistModel> Checklists { get; set; }

        public string ErrorMessage { get; set; }
        public bool HasWarnings { get; set; }

        [AllowHtml]
       // [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        //[StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string OptionalMessage { get; set; }
        public string PhasesCollaborators { get; set; }
        public ApprovalVersionCollaboratorInfo ApprovalVersionCollaboratorInfo { get; set; }
        public List<ApprovalWorkflowDetails> WorkflowPhases { get; set; }
        public ApprovalWorkflowPhasesInfo PhaseCollaboratorsModel { get; set; }
        
        [Required(ErrorMessageResourceName = "reqDeliverJobTtile", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string FileTitleValidator { get; set; }

        public bool HasRenderingOptions { get; set; }
        public RenderOptions? RenderOption { get; set; }
        public List<KeyValuePair<int, string>> CustomProfiles { get; set; }
        public int SelectedCustomProfile { get; set; }
        public bool HasAdHocWorkflow { get; set; }
        public string ApprovalAdHocWorkflowName { get; set; }

        public string OriginalFileName { get; set; }
        public string SelectedTagwords { get; set; }
        public int ProjectFolder { get; set; }
        public bool ExecuteWorkflow { get; set; }


        #endregion

        #region Constructors

        public NewApprovalModel()
        {

        }

        public NewApprovalModel(Account objAccount, int loggedUserId, string folderPath, GMGColorContext context, out AccountFileTypeFilter fileTypeFilter)
            : base(objAccount, context)
        {
            HasAdHocWorkflow = false;
            OnlyOneDecisionRequired = false;
            AllowUsersToDownload = true;
            Folder = 0;
            PathToTempFolder = folderPath;
            IsUploadApproval = true;
            
            ErrorMessage = string.Empty;
            KeepOriginalFileName = true;
            ExecuteWorkflow = true;

            // Accepted file types
            fileTypeFilter = Account.GetAccountApprovalFileTypesAllowed(loggedUserId, context);
            List<string> filters = new List<string>();
            ApprovalVersionCollaboratorInfo =  new ApprovalVersionCollaboratorInfo();
            ApprovalWorkflows =  new List<ApprovalWorkflowModel>();
            Checklists = new List<ChecklistModel>();

            if (fileTypeFilter.HasUploadSettings && fileTypeFilter.UploadSettingsFilter.ApplyUploadSettingsForNewApproval)
            {
                if (fileTypeFilter.UploadSettingsFilter.AllowPDF) filters.Add(acceptedPdfExtenssions);
                if (fileTypeFilter.UploadSettingsFilter.AllowImage) filters.Add(acceptedImageExtenssions);
                if (fileTypeFilter.UploadSettingsFilter.AllowSWF && fileTypeFilter.PlanIncludesVideo) filters.Add(acceptedSWFExtensions);
                if (fileTypeFilter.UploadSettingsFilter.AllowZip) filters.Add(acceptedZipExtensions);
                if (fileTypeFilter.UploadSettingsFilter.AllowVideo && fileTypeFilter.PlanIncludesVideo) filters.Add(acceptedVideoExtenssions);
                if(fileTypeFilter.UploadSettingsFilter.AllowDoc) filters.Add(acceptedDocumentExtensions);

            }
            else
            {
                filters.AddRange(new List<string>{ acceptedImageExtenssions,acceptedPdfExtenssions, acceptedZipExtensions, acceptedDocumentExtensions });
                if(fileTypeFilter.PlanIncludesVideo)
                    filters.AddRange(new List<string> { acceptedVideoExtenssions, acceptedSWFExtensions });
            }

            AcceptedFiles = "/(" + String.Join("|", filters.Select(i => i)) + ")$/i";

            string customProfileActivated = (from acs in context.AccountSettings
                                            where acs.Account == objAccount.ID && acs.Name == "EnableCustomProfile"
                                            select acs.Value).FirstOrDefault();
            bool isCustomProfileActive;
            bool.TryParse(customProfileActivated, out isCustomProfileActive);

            if (isCustomProfileActive)
            {
                HasRenderingOptions = Account.GetPlanTypeOptions(objAccount.ID, GMG.CoZone.Common.AppModule.Collaborate, context).Contains((int)PlanFeatures.CustomProfile);
                if (HasRenderingOptions)
                {
                    CustomProfiles = Account.GetAccountCustomProfiles(objAccount.ID, context);
                }
            }
        }

        #endregion
    }

    [Serializable]
    public class ApprovalDetailsModel : Common
    {
        #region Properties

        public int ID { get; set; }
        public int Job { get; set; }
        public int HiddenSelectedApprovals { get; set; }
        [Required(ErrorMessageResourceName = "reqTitle", ErrorMessageResourceType = typeof(Resources))]
        public string Title { get; set; }
        public List<VersionInfoModel> Versions { get; set; }
        public List<VersionDetailsModel> VersionDetails { get; set; }
        public int LoggedUserID { get; set; }
        public JobStatu.Status JobStatus { get; set; }
        public string LocalizedJobStatus { get; set; }
        public List<KeyValuePair<int, string>> AvailableStatusesKeys { get; set; } 
        public AnnotationsReportFilters AnnotationsReportFilters { get; set; }
        public bool CanAddVersion { get; set; }
        public int JobOwner { get; set; }
        public ApprovalDefaultViewingConditionsModel DefaultViewingConditions { get; set; }
       
        #endregion

        #region Constructors

        public ApprovalDetailsModel()
        {

        }

        public ApprovalDetailsModel(Account objAccount, int loggedUserId, int approvalId, Role.RoleName loggedUserRole, AccountSettings.CollaborateGlobalSettings collaborateSett, GMGColorContext context) : base(objAccount, context)
        {
            try
            {
                var objApprovalInfo = (from a in context.Approvals
                                       join j in context.Jobs on a.Job equals j.ID
                                       join js in context.JobStatus on j.Status equals js.ID
                                       where a.ID == approvalId && !a.DeletePermanently
                                       select new
                                              {
                                                  a.Job,
                                                  a.Owner,
                                                  j.Title,
                                                  j.Status,
                                                  j.JobOwner,
                                                  JobStatusKey = js.Key,
                                                  versions = j.Approvals.Where(t => t.IsDeleted == false && t.Version != 0 && a.DeletePermanently == false).OrderBy(t => t.Version).Select(t => new VersionInfoModel
                                                                                    {
                                                                                        ID = t.ID,
                                                                                        Version = t.Version,
                                                                                        VersionSufix = t.VersionSufix,
                                                                                        IsLoggedUserCollaborator = t.ApprovalCollaborators.Any(c => c.Collaborator == loggedUserId && c.Phase == a.CurrentPhase),
                                                                                        IsLocked = t.IsLocked,
                                                                                        CurrentPhase = t.CurrentPhase,
                                                                                        Type = (Approval.ApprovalTypeEnum)t.ApprovalType.Key
                                                                                    }).ToList(),
                                                  ListStatuses = (from jsts in context.JobStatus
                                                                  orderby jsts.Priority
                                                                  where jsts.Key != "NEW" && jsts.ID != js.ID
                                                                  && (collaborateSett.EnableRetoucherWorkflow || jsts.Key != "CCM")
                                                                  select new
                                                                  {
                                                                      jsts.Key,
                                                                      jsts.ID
                                                                  }).ToList(),
                                              }).FirstOrDefault();
                Job = objApprovalInfo.Job;
                ID = approvalId;
                Title = objApprovalInfo.Title;
                LoggedUserID = loggedUserId;
                Versions = objApprovalInfo.versions;
                JobStatus = JobStatu.GetJobStatus(objApprovalInfo.JobStatusKey);
                LocalizedJobStatus = JobStatu.GetLocalizedJobStatus(JobStatus, collaborateSett.EnableRetoucherWorkflow);
                JobOwner = objApprovalInfo.JobOwner.GetValueOrDefault();
                VersionInfoModel maxVers = objApprovalInfo.versions.OrderByDescending(v => v.Version).FirstOrDefault();
                var isPhaseCompleted = context.ApprovalJobPhaseApprovals.Where(
                                        ajp => ajp.Approval == maxVers.ID && ajp.Phase == maxVers.CurrentPhase)
                                        .Select(ajpa => ajpa.PhaseMarkedAsCompleted)
                                        .FirstOrDefault();
                bool IsApprovalDecisionChangesRequired = false;
                if(JobStatus == JobStatu.Status.Completed)
                {
                    IsApprovalDecisionChangesRequired = context.ApprovalCollaboratorDecisions.Where(a => a.Approval == approvalId && a.Collaborator == loggedUserId).Select(ap => ap.Decision).FirstOrDefault() == (int)JobStatu.Status.ChangesRequired;
                }
                CanAddVersion = maxVers != null && (((JobStatus == JobStatu.Status.New) || (JobStatus == JobStatu.Status.ChangesRequired) || (JobStatus == JobStatu.Status.ChangesComplete) || (JobStatus == JobStatu.Status.Completed && IsApprovalDecisionChangesRequired))
                                                && (loggedUserRole != Role.RoleName.AccountViewer && loggedUserRole != Role.RoleName.AccountContributor && loggedUserRole != Role.RoleName.AccountRetoucher)
                                                && (maxVers.IsLoggedUserCollaborator || loggedUserId == objApprovalInfo.JobOwner || loggedUserId == objApprovalInfo.Owner) && (maxVers.CurrentPhase == null || (maxVers.CurrentPhase > 0)));

                if (JobStatus != JobStatu.Status.Completed)
                {
                    objApprovalInfo.ListStatuses.RemoveAll(o => o.Key == "ARC");
                }

                AvailableStatusesKeys = objApprovalInfo.ListStatuses.Select(t => new KeyValuePair<int, string>(t.ID, JobStatu.GetLocalizedJobStatus(JobStatu.GetJobStatus(t.Key), collaborateSett.EnableRetoucherWorkflow
                    ))).ToList();

                int approvalVersion = context.Approvals.Where(a => a.ID == approvalId).Select(ap => ap.Version).FirstOrDefault();

                AnnotationsReportFilters = new AnnotationsReportFilters
                {
                    SelectedApproval = approvalId,
                    IsFromDashboard = false,
                    AnnotationsPerPage = 10,
                    SelectedVersion = approvalVersion
                };

                if (Versions.Any(t => t.Type != Approval.ApprovalTypeEnum.Movie) &&
                    Account.GetPlanTypeOptions(objAccount.ID, GMG.CoZone.Common.AppModule.Collaborate, context)
                        .Contains((int) PlanFeatures.ViewingConditions))
                {
                    DefaultViewingConditions = new ApprovalDefaultViewingConditionsModel(Job, objAccount.ID, context);

                }
            }
            finally
            {
            }
        }

        #endregion

        #region Sub Classes

        [Serializable]
        public class VersionDetailsModel
        {
            #region Properties

            public int ID { get; set; }
            public int Version { get; set; }
            public string Title { get; set; }
            public Account objLoggedAccount { get; set; }
            public Approval objApprovalVersion { get; set; }
            public int LoggedUserID { get; set; }
            public Role.RoleName LoggedUserRole { get; set; }
            public JobStatu.Status JobStatus { get; set; }
            public List<ApprovalAnnotationStatusChangeLog> AnnotationsStatusLogs { get; set; }
            public List<ApprovalDetailsCollaboratorActivity> ColloaboratorActivities { get; set; }
            public List<ApprovalDetailsCollaboratorActivity> ExternalColloaboratorActivities { get; set; }
            public List<ApprovalDetailsCollaboratorActivity> DeletedColloaboratorActivitiesWhoAnnotated { get; set; }
            public List<ApprovalDetailsCollaboratorActivity> DeletedExternalColloaboratorActivitiesWhoAnnotated { get; set; }
            public ApprovalVersionCollaboratorInfo ApprovalVersionCollaboratorInfo { get; set; }
            public int GroupByValue { get; set; }
            public int ReportType { get; set; }
            public bool ShowAllFilter { get; set; }
            public string ApprovalWorkflowName { get; set; }
            public string ApprovalPhaseName { get; set; }
            public int? PhaseId { get; set; }
            public bool VersionCanBeDeleted { get; set; }
            public int? JobOwner { get; set; }
            public bool IsPhaseCompleted { get; set; }
            public bool IsDueDateEditDisabled { get; set; }
            public bool IsLastVersion { get; set; }
            public bool DisableActions { get; set; }
            public bool DisableSOADIndicator { get; set; }
            public bool DisableTagwordsInDashbord { get; set; }
            public bool IsExternalPrimaryDecisionMaker { get; set; }
            public string Checklist { get; set; }
            public string FileName { get; set; }         

            #endregion

            #region Constructors

            public VersionDetailsModel()
            {
                
            }
            

            #endregion
        }

        public class VersionInfoModel
        {
            #region Properties 

            public int ID { get; set; }
            public int Version { get; set; }
            public bool IsLoggedUserCollaborator { get; set; }
            public bool IsLocked { get; set; }
            public int? CurrentPhase { get; set; }
            public string VersionSufix { get; set; }
            public Approval.ApprovalTypeEnum Type { get; set; }

            #endregion
        }

        public class ApprovalCollaborateSendReminderInfo
        {
            public int Collaborator { get; set; }
            public Nullable<bool> IsReminderMailSent { get; set; }
            public string MailSentDate { get; set; }
            public bool IsExternalUser { get; set; }
            public int? PhaseId { get; set; }

        }

        #endregion
    }

    [Serializable]
    public class ApprovalAnnotationsModel
    {
        #region Properties

        public List<ApprovalPageAnnotationsModel> PageAnnotations { get; set; }

        public GMG.CoZone.Common.AnnotationReportType ReportType { get; set; }

        public string JobTitle { get; set; }

        public int Version { get; set; }

        public string Creator { get; set; }

        public int NrOfPages { get; set; }

        public string CreatedDate { get; set; }

        public int Comments { get; set; }

        public string JobStatus { get; set; }

        public string ApprovalName { get; set; }

        public int ApprovalID { get; set; }

       public bool IsApprovalTypeVideo { get; set; }
        #endregion

    }

    [Serializable]
    public class ApprovalAnnotationsModelPDF
    {
        #region Properties

        public List<ApprovalPageAnnotationsModelPDF> PageAnnotations { get; set; }

        public GMG.CoZone.Common.AnnotationReportType ReportType { get; set; }

        public string JobTitle { get; set; }

        public int Version { get; set; }

        public string Creator { get; set; }

        public int NrOfPages { get; set; }

        public string CreatedDate { get; set; }

        public int Comments { get; set; }

        public string JobStatus { get; set; }

        public string ApprovalName { get; set; }

        public int ApprovalID { get; set; }
        public bool IsApprovalTypeVideo { get; set; }

        #endregion

    }

    public class ApprovalDownloadDetails
    {
        public string Region { get; set; }
        public string Domain { get; set; }
        public string FileName { get; set; }
    }

    [Serializable]
    public class ApprovalAnnotationsReportModel
    {
        #region Properties

        public List<ApprovalAnnotationsModel> ApprovalAnnotationsModel { get; set; }

        #endregion

        #region Ctors

        public ApprovalAnnotationsReportModel()
        {
            ApprovalAnnotationsModel = new List<ApprovalAnnotationsModel>();
        }

        #endregion

    }

    [Serializable]
    public class ApprovalAnnotationsReportModelPDF
    {
        #region Properties

        public List<ApprovalAnnotationsModelPDF> ApprovalAnnotationsModelPDF { get; set; }

        #endregion

        #region Ctors

        public ApprovalAnnotationsReportModelPDF()
        {
            ApprovalAnnotationsModelPDF = new List<ApprovalAnnotationsModelPDF>();
        }

        #endregion

    }

    [Serializable]
    public class ApprovalAnnotationReportCSVModel
    {
        #region Properties

        public int PageNumber { get; set; }
        public int AnnotationNumber { get; set; }
        public string AnnotatedDate { get; set; }
        public string CreatorName { get; set; }
        public string Comment { get; set; }
        public int? TimeFrame { get; set; }
        public List<string> Replies { get; set; }
        public List<string> AnnotationAttachments { get; set; }
        public List<AnnotationChecklistModel> AnnotationChecklist { get; set; }
        public int UserStatus { get; set; }

        #endregion

        #region Constructors

        public ApprovalAnnotationReportCSVModel()
        {
            Replies = new List<string>();
            AnnotationAttachments = new List<string>();
            AnnotationChecklist = new List<AnnotationChecklistModel>();
        }
        #endregion
    }

    [Serializable]
    public class ApprovalReportCSVModel
    {
        #region Properties

        public string ApprovalName { get; set; }
        public List<ApprovalAnnotationReportCSVModel> Annotations { get; set; }

        #endregion

        #region Constructors

        public ApprovalReportCSVModel()
        {
            Annotations = new List<ApprovalAnnotationReportCSVModel>();
        }
        #endregion
    }

    [Serializable]
    public class ApprovalDetailsCollaboratorActivity
    {
        #region Properties

        public int UserID { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public DateTime Sent { get; set; }
        public string Opened { get; set; }
        public string Completed { get; set; }
        public int Annotations { get; set; }
        public int Replies { get; set; }
        public string DecisionMade { get; set; }
        public bool IsPrimaryDecisionMaker { get; set; }
        public bool IsAccessRevoked { get; set; }
        public string CollaboratorRoleKey { get; set; }
        public int DecisionId { get; set; }
        public string UserName { get; set; }
        public DateTime OpenedDate { get; set; }
        public string EmailAddress { get; set; }
        public int? PhaseId { get; set; }
        public int TotalApprovalAnnotations { get; set; }
        public int TotalAnnotations { get; set; }
        public int TotalVideoAnnotationsReport { get; set; }
        public int AnnotationsPhaseRerun { get; set; }
        public bool selectedForPublicAnnotations { get; set; }
        public int userStatus { get; set; }
        public Nullable<bool> IsReminderMailSent { get; set; }
        #endregion
    }

    [Serializable]
    public class ApprovalWorkflowDetails
    {
        public int WorkflowId { get; set; }
        public int PhaseId { get; set; }
        public string PhaseName { get; set; }
    }

    [Serializable]
    public class ApprovalAnnotationStatusChangeLog
    {
        #region Properties

        public string ModifiedDate { get; set; }
        public string ModifiedDateHour { get; set; }
        public string CollaboratorName { get; set; }
        public int UserStatus { get; set; }
        public string Status { get; set; }
        public string AnnotationComment { get; set; }

        #endregion
    }

    [Serializable]
    public class ApprovalsGrid : Common
    {
        #region Properties

        public List<ReturnProjectFolderApprovalsPageView> ProjectFolderApprovalsPageView { get; set; }
        public List<ReturnApprovalsPageView> ApprovalsPageView { get; set; }
        public GMGColorDAL.User LoggedUser { get; set; }
        public Account LoggedAccount { get; set; }
        public Role.RoleName LoggedUserRole { get; set; }
        public string HiddenSelectedApprovals { get; set; }
        public string HiddenSelectedFolders { get; set; }
        public Dictionary<int, string> dicFilters = new Dictionary<int, string>();
        public Dictionary<int, string> dicPageSizes = new Dictionary<int, string>();
        public Dictionary<int, string> DashboardViewTypes = new Dictionary<int, string>();
        public bool AnyAvailableValidInstances { get; set; }
        public AnnotationsReportFilters AnnotationsReportFilters { get; set; }
        public OutOfOfficeUsers OutOfOfficeUsers { get; set; }
        public string InProgressApprovalsIds { get; set; }
        public bool ShowAllFilesToManagers { get; set; }
        public ApprovalsFilterModel ApprovalsFilter { get; set; }

        #endregion

        public ApprovalsGrid(Account objAccount, GMGColorContext context)
            : base(objAccount, context)
        {
            dicFilters.Add(0, Resources.lblDuebytime);
            dicFilters.Add(1, Resources.lblDateCreated);
            dicFilters.Add(2, Resources.lblLastModified);
            dicFilters.Add(3, Resources.lblStatus);
            dicFilters.Add(4, Resources.lblName);
            dicFilters.Add(5, Resources.lblType);
            dicFilters.Add(10, Resources.lblApproved);
            dicFilters.Add(11, Resources.lblRejectedStatus);

            dicPageSizes.Add(10, "10 " + Resources.lblPerPage);
            dicPageSizes.Add(20, "20 " + Resources.lblPerPage);
            dicPageSizes.Add(30, "30 " + Resources.lblPerPage);
            dicPageSizes.Add(50, "50 " + Resources.lblPerPage);

            DashboardViewTypes.Add(0, Resources.lblSummaryView);
            DashboardViewTypes.Add(1, Resources.lblStatusView);
        }
    }

    [Serializable]
    public class ProjectFolderApprovalsGrid : Common
    {
        #region Properties

        public List<ReturnProjectFolderApprovalsPageView> ProjectFolderApprovalsPageView { get; set; }
        public List<ReturnApprovalsPageView> ApprovalsPageView { get; set; }
        public GMGColorDAL.User LoggedUser { get; set; }
        public Account LoggedAccount { get; set; }
        public Role.RoleName LoggedUserRole { get; set; }
        public string HiddenSelectedApprovals { get; set; }
        public string HiddenSelectedFolders { get; set; }
        public Dictionary<int, string> dicFilters = new Dictionary<int, string>();
        public Dictionary<int, string> dicPageSizes = new Dictionary<int, string>();
        public Dictionary<int, string> DashboardViewTypes = new Dictionary<int, string>();
        public bool AnyAvailableValidInstances { get; set; }
        public AnnotationsReportFilters AnnotationsReportFilters { get; set; }
        public OutOfOfficeUsers OutOfOfficeUsers { get; set; }
        public string InProgressApprovalsIds { get; set; }
        public bool ShowAllFilesToManagers { get; set; }
        public ApprovalsFilterModel ApprovalsFilter { get; set; }
        public NewApprovalModel NewProjectFolderApproval { get; set; }


        #endregion

        public ProjectFolderApprovalsGrid(Account objAccount, GMGColorContext context)
            : base(objAccount, context)
        {
            dicFilters.Add(0, Resources.lblDuebytime);
            dicFilters.Add(1, Resources.lblDateCreated);
            dicFilters.Add(2, Resources.lblLastModified);
            dicFilters.Add(3, Resources.lblStatus);
            dicFilters.Add(4, Resources.lblName);
            dicFilters.Add(5, Resources.lblType);
            dicFilters.Add(10, Resources.lblApproved);
            dicFilters.Add(11, Resources.lblRejectedStatus);

            dicPageSizes.Add(10, "10 " + Resources.lblPerPage);
            dicPageSizes.Add(20, "20 " + Resources.lblPerPage);
            dicPageSizes.Add(30, "30 " + Resources.lblPerPage);
            dicPageSizes.Add(50, "50 " + Resources.lblPerPage);

            DashboardViewTypes.Add(0, Resources.lblSummaryView);
            DashboardViewTypes.Add(1, Resources.lblStatusView);
        }
    }

    [Serializable]
    public class ApprovalFolder
    {
        #region Properties

        public int FolderID { get; set; }
        public int LoggedUserID { get; set; }
        public int Parent { get; set; }
        public bool ViewAllFilesAndFolders { get; set; }

        #endregion
    }

    [Serializable]
    public class ApprovalBatchUploadSettings
    {
        #region Properties

        public bool ApplyBatchUploadSettings { get; set; }
        public bool UseSettingsFromPrvVersion { get; set; }
        public bool AllowPdf { get; set; }
        public bool AllowImage{ get; set; }
        public bool AllowVideo{ get; set; }
        public bool AllowSWF{ get; set; }
        public bool AllowZip { get; set; }
        #endregion
    }

    [Serializable]
    public class ApprovalVersionCollaboratorInfo
    {
        #region Properties
        public List<int> Groups { get; set; }
        public List<string> CollaboratorsWithRole { get; set; }
        public List<int> CollaboratorsWithDecision { get; set; }
        public List<string> ExternalCollaboratorsWithRole { get; set; }
        public List<int> ExternalCollaboratorsWithDecision { get; set; }
        #endregion

        #region Methods

        public void LoadCollaborats(int approvalVersionID, GMGColorContext context)
        {
            var collaboratorInfo = (from apv in context.Approvals
             let clbGroups = (from acg in context.ApprovalCollaboratorGroups
                              where acg.Approval == apv.ID && apv.CurrentPhase == acg.Phase
                              select acg.CollaboratorGroup).ToList()
             let clbWithRole = (from acg in context.ApprovalCollaborators
                                join u in context.Users on acg.Collaborator equals u.ID
                                join us in context.UserStatus on u.Status equals us.ID
                                join ur in context.UserRoles on u.ID equals ur.User
                                join r in context.Roles on ur.Role equals r.ID
                                let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                where acg.Approval == apv.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID && apv.CurrentPhase == acg.Phase
                                select SqlFunctions.StringConvert((double)acg.Collaborator).Trim() + "|" + SqlFunctions.StringConvert((double)acg.ApprovalCollaboratorRole).Trim()).ToList()
             let clbWithDecision = (from acd in context.ApprovalCollaboratorDecisions
                                    where acd.Approval == apv.ID && acd.CompletedDate != null && acd.Collaborator != null && apv.CurrentPhase == acd.Phase
                                    select acd.Collaborator.Value).ToList()
             let extWithRole = (from sha in context.SharedApprovals
                                join ec in context.ExternalCollaborators on sha.ExternalCollaborator equals ec.ID
                                where sha.Approval == apv.ID && apv.CurrentPhase == sha.Phase && !ec.IsDeleted
                                select SqlFunctions.StringConvert((double)sha.ExternalCollaborator).Trim() + "|" + SqlFunctions.StringConvert((double)sha.ApprovalCollaboratorRole).Trim()).ToList()
             let extWithDecision = (from acd in context.ApprovalCollaboratorDecisions
                                    where acd.Approval == apv.ID && acd.CompletedDate != null && acd.Collaborator == null && apv.CurrentPhase == acd.Phase
                                    select acd.ExternalCollaborator.Value).ToList()
             where apv.ID == approvalVersionID
             select new
             {
                 Groups = clbGroups,
                 CollaboratorsWithRole = clbWithRole,
                 CollaboratorsWithDecision = clbWithDecision,
                 ExternalCollaboratorsWithRole = extWithRole,
                 ExternalCollaboratorsWithDecision = extWithDecision
             }).FirstOrDefault();

            Groups = collaboratorInfo.Groups;
            CollaboratorsWithRole = collaboratorInfo.CollaboratorsWithRole;
            CollaboratorsWithDecision = collaboratorInfo.CollaboratorsWithDecision;
            ExternalCollaboratorsWithRole = collaboratorInfo.ExternalCollaboratorsWithRole;
            ExternalCollaboratorsWithDecision = collaboratorInfo.ExternalCollaboratorsWithDecision;
        }

        public void LoadNewVersionCollaborats(int approvalVersionID, GMGColorContext context)
        {
            
            var collaboratorInfo = (from apv in context.Approvals
                                    let clbGroups = (from acg in context.ApprovalCollaboratorGroups
                                                     where acg.Approval == apv.ID
                                                     select acg.CollaboratorGroup).ToList()
                                    let clbWithRole = (from acg in context.ApprovalCollaborators
                                                       join u in context.Users on acg.Collaborator equals u.ID
                                                       join us in context.UserStatus on u.Status equals us.ID
                                                       join ur in context.UserRoles on u.ID equals ur.User
                                                       join r in context.Roles on ur.Role equals r.ID
                                                       let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                                       where acg.Approval == apv.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID && acg.Phase == null
                                                       select SqlFunctions.StringConvert((double)acg.Collaborator).Trim() + "|" + SqlFunctions.StringConvert((double)acg.ApprovalCollaboratorRole).Trim()).ToList()
                                    let clbWithDecision = (from acd in context.ApprovalCollaboratorDecisions
                                                           where acd.Approval == apv.ID && acd.CompletedDate != null && acd.Collaborator != null
                                                           select acd.Collaborator.Value).ToList()
                                    where apv.ID == approvalVersionID
                                    select new
                                    {
                                        Groups = clbGroups,
                                        CollaboratorsWithRole = clbWithRole,
                                        CollaboratorsWithDecision = clbWithDecision
                                    }).FirstOrDefault();

            Groups = collaboratorInfo.Groups;
            CollaboratorsWithRole = collaboratorInfo.CollaboratorsWithRole;          
            CollaboratorsWithDecision = collaboratorInfo.CollaboratorsWithDecision;            
        }

        public void LoadNewVersionExternalCollaborators(int approvalVersionID, GMGColorContext context)
        {
            ExternalCollaboratorsWithRole = (from apv in context.Approvals
                                                join sha in context.SharedApprovals on apv.ID equals sha.Approval
                                                join ec in context.ExternalCollaborators on sha.ExternalCollaborator equals ec.ID                                  
                                                where apv.ID == approvalVersionID && apv.CurrentPhase == sha.Phase && !ec.IsDeleted
                                                select SqlFunctions.StringConvert((double)sha.ExternalCollaborator).Trim() + "|" + SqlFunctions.StringConvert((double)sha.ApprovalCollaboratorRole).Trim()).ToList();
        }
        #endregion
    }

    [Serializable]
    public class FolderCollaboratorInfo
    {
        #region Properties
        public List<int> Groups { get; set; }
        public List<string> CollaboratorsWithRole { get; set; }
        #endregion

        #region Methods

        public void LoadCollaborats(int folderID, GMGColorContext context)
        {
            var collaboratorInfo = (from apv in context.Folders
                                    let clbGroups = (from acg in context.FolderCollaboratorGroups
                                                     where acg.Folder == apv.ID 
                                                     select acg.CollaboratorGroup).ToList()
                                    let clbWithRole = (from acg in context.FolderCollaborators
                                                       join u in context.Users on acg.Collaborator equals u.ID
                                                       join us in context.UserStatus on u.Status equals us.ID
                                                       join ur in context.UserRoles on u.ID equals ur.User
                                                       join r in context.Roles on ur.Role equals r.ID
                                                       let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                                       where acg.Folder == apv.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID 
                                                       select SqlFunctions.StringConvert((double)acg.Collaborator).Trim() + "|" + 3).ToList()
                                   
                                    where apv.ID == folderID
                                    select new
                                    {
                                        Groups = clbGroups,
                                        CollaboratorsWithRole = clbWithRole,
                                     
                                    }).FirstOrDefault();

            Groups = collaboratorInfo.Groups;
            CollaboratorsWithRole = collaboratorInfo.CollaboratorsWithRole;
        }

        public void LoadNewVersionCollaborats(int folderId, GMGColorContext context)
        {

            var collaboratorInfo = (from apv in context.Approvals
                                    let clbGroups = (from acg in context.FolderCollaboratorGroups
                                                     where acg.Folder == apv.ID
                                                     select acg.CollaboratorGroup).ToList()
                                    let clbWithRole = (from acg in context.FolderCollaborators
                                                       join u in context.Users on acg.Collaborator equals u.ID
                                                       join us in context.UserStatus on u.Status equals us.ID
                                                       join ur in context.UserRoles on u.ID equals ur.User
                                                       join r in context.Roles on ur.Role equals r.ID
                                                       let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                                       where acg.Folder == apv.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID 
                                                       select SqlFunctions.StringConvert((double)acg.Collaborator).Trim() + "|" + 3).ToList()
                                   
                                    where apv.ID == folderId
                                    select new
                                    {
                                        Groups = clbGroups,
                                        CollaboratorsWithRole = clbWithRole,
                                        
                                    }).FirstOrDefault();

            Groups = collaboratorInfo.Groups;
            CollaboratorsWithRole = collaboratorInfo.CollaboratorsWithRole;
          
        }

      
        #endregion
    }

    public class ApprovalFolderCollaboratorInfo
    {
        #region Properties

        public List<int> Groups { get; set; }
        public List<string> CollaboratorsWithRole { get; set; }

        #endregion

        #region Methods

        public void LoadFolderCollaborators(int folderId, GMGColorContext context)
        {
            var folderCollaboratorInfo = (from f in context.Folders
                                          let clbGroups = (from fcg in context.FolderCollaboratorGroups
                                                           where fcg.Folder == f.ID
                                                           select fcg.CollaboratorGroup).ToList()
                                          let clbWithRole = (from fc in context.FolderCollaborators
                                                             join u in context.Users on fc.Collaborator equals u.ID
                                                             join us in context.UserStatus on u.Status equals us.ID
                                                             join ur in context.UserRoles on u.ID equals ur.User
                                                             join r in context.Roles on ur.Role equals r.ID
                                                             let appModuleID = (from am in context.AppModules where am.Key == 0 select am.ID).FirstOrDefault()
                                                             where fc.Folder == f.ID && us.Key != "D" && r.Key != "NON" && r.AppModule == appModuleID
                                                             select SqlFunctions.StringConvert((double)fc.Collaborator).Trim()).ToList()
                                          where f.ID == folderId
                                          select new
                                          {
                                              Groups = clbGroups,
                                              CollaboratorsWithRole = clbWithRole
                                          }).FirstOrDefault();

            Groups = folderCollaboratorInfo.Groups;
            CollaboratorsWithRole = folderCollaboratorInfo.CollaboratorsWithRole;
        }

        public void LoadGroupCollaborators(int folderId, int loggedUserID, GMGColorContext context)
        {
            var folderCollaboratorInfo = (from f in context.Folders
                                          let clbGroups = (from ugu in context.UserGroupUsers
                                                           join fcg in context.FolderCollaboratorGroups on ugu.UserGroup equals fcg.CollaboratorGroup
                                                           join ug in context.UserGroups on ugu.UserGroup equals ug.ID
                                                           where ugu.User == loggedUserID && fcg.Folder == f.ID
                                                           select ugu.UserGroup).ToList()
                                          let clbWithRole = (from usr in context.Users
                                                            join fc in context.FolderCollaborators on usr.ID equals fc.Collaborator
                                                            join ugu in context.UserGroupUsers on usr.ID equals ugu.User
                                                            join ug in context.UserGroupUsers on ugu.UserGroup equals ug.UserGroup
                                                            join grp in context.UserGroups on ug.UserGroup equals grp.ID
                                                             where ug.User == loggedUserID && fc.Folder == f.ID
                                                            select SqlFunctions.StringConvert((double)usr.ID).Trim()).ToList()
                                          where f.ID == folderId
                                          select new
                                          {
                                              Groups = clbGroups,
                                              CollaboratorsWithRole = clbWithRole
                                          }).FirstOrDefault();

            Groups = folderCollaboratorInfo.Groups;
            CollaboratorsWithRole = folderCollaboratorInfo.CollaboratorsWithRole;

            if (!CollaboratorsWithRole.Any())
            {
                CollaboratorsWithRole.Add(loggedUserID.ToString());
            }
        }
        #endregion
    }

    public class ApprovalTypeGuid
    {
        public string Guid { get; set; }
        public int ApprovalTypeKey { get; set; }
        public string FileName { get; set; }
    }

    public class AccountFileTypeFilter
    {
        public bool PlanIncludesVideo { get; set; }
        public UploadSettingsFileTypeFilter UploadSettingsFilter { get; set; }
        public bool HasUploadSettings { get; set; }
    }

    public class UploadSettingsFileTypeFilter
    {
        public bool AllowPDF { get; set; }
        public bool AllowImage { get; set; }
        public bool AllowVideo { get; set; }
        public bool AllowSWF { get; set; }
        public bool AllowZip { get; set; }
        public bool AllowDoc { get; set; }
        public bool ApplyUploadSettingsForNewApproval { get; set; }
        public bool UseSettingsFromPreviousVersion { get; set; }
    }

    public class NewApprovalResponse
    {
        public string JobGuid { get; set; }
        public int ApprovalID { get; set; }
        public int Version { get; set; }
    }
}
