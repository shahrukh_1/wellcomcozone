﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSC = GMGColor.Resources.Resources;

namespace GMGColorDAL.CustomModels
{
    public class DeleteConfirmation
    {
        public int Key { get; set; }
        public string DeleteMessageTitle { get; set; }
        public string DeleteMessageHeading { get; set; }
        public string DeleteMessage { get; set; }
        public string DeleteSecondMessage { get; set; }
        public string DeleteMessagePleaseNote { get; set; }
        public string Action { get; set; }
        public string SecondAction { get; set; }
        public string SecondActionLabel { get; set; }
        public string LoadingText { get; set; }

        public DeleteConfirmation()
        {
        }

        public DeleteConfirmation(int key, string action, bool useDefaultText = true)
        {
            this.Key = key;
            this.Action = action;
            this.LoadingText = RSC.btnDeleting;
            this.DeleteMessageTitle = RSC.btnDelete;

            if (useDefaultText)
            {
                this.DeleteMessageHeading = RSC.lblDeleteMessageHeading;
                this.DeleteMessage = RSC.lblDeleteMessage;
                this.DeleteMessagePleaseNote = RSC.lblDeleteMessagePleaseNote;
            }
        }

        public DeleteConfirmation(int key, string action, string secondAction, bool useDefaultText = true)
        {
            this.Key = key;
            this.Action = action;
            this.SecondAction = secondAction;
            this.LoadingText = RSC.btnDeleting;
            this.DeleteMessageTitle = RSC.btnDelete;

            if (useDefaultText)
            {
                this.DeleteMessageHeading = RSC.lblDeleteMessageHeading;
                this.DeleteMessage = RSC.lblDeleteMessage;
                this.DeleteMessagePleaseNote = RSC.lblDeleteMessagePleaseNote;
            }
        }
    }
}
