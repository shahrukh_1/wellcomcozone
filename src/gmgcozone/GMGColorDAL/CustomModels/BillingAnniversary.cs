﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using GMGColor.Resources;
using CMEnums = GMGColorDAL.CustomModels.Enums;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class BillingAnniversary
    {
        #region Properties

        public Dictionary<int, string> ContractStartDate
        {
            get
            {
                Dictionary<int, string> dicCSD = new Dictionary<int, string>();                
                dicCSD.Add(1, Resources.lblWhenAccountIsActivated);
                dicCSD.Add(0, Resources.lblToDay);

                return dicCSD;
            }
        }

        #endregion

        #region Constructors

        #endregion

        #region Methods

        public static string AddOrdinal(int num)
        {
            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num.ToString() + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num.ToString() + "st";
                case 2:
                    return num.ToString() + "nd";
                case 3:
                    return num.ToString() + "rd";
                default:
                    return num.ToString() + "th";
            }

        }

        public static string GetMonth(int monthNumber)
        {
            string month = string.Empty;

            if (monthNumber >= 0 && monthNumber < 12)
            {
                if (DateTimeFormatInfo.CurrentInfo != null)
                    month = DateTimeFormatInfo.CurrentInfo.GetMonthName(++monthNumber).ToString(CultureInfo.InvariantCulture);
            }

            return month;
        }

        #endregion
    }
}
