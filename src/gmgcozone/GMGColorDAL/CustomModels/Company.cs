﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]    
    public class Company
    {
        #region Properties

        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqCompanyName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [StringLength(32, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Number { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Phone { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Mobile { get; set; }

        [Required(ErrorMessageResourceName = "reqAddress1", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Address1 { get; set; }

        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Address2 { get; set; }

        [Required(ErrorMessageResourceName = "reqCity", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(64, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string City { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string State { get; set; }

        [StringLength(20, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string PostCode { get; set; }

        [Required(ErrorMessageResourceName = "reqCountry", ErrorMessageResourceType = typeof(Resources))]
        public int Country { get; set; }

        #endregion

        #region Constructors

        public Company()
        {

        }

        public Company(GMGColorDAL.Company objCompany)
        {
            this.ID = objCompany.ID;
            this.Name = objCompany.Name;
            this.Number = objCompany.Number;
            this.Phone = objCompany.Phone;
            this.Mobile = objCompany.Mobile;

            this.Address1 = objCompany.Address1;
            this.Address2 = objCompany.Address2;
            this.City = objCompany.City;
            this.State = objCompany.State;
            this.PostCode = objCompany.Postcode;
            this.Country = objCompany.Country;
        }

        #endregion
    }
}
