﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class ApprovalPhaseDeadline
    {
        public int Approval { get; set; }
        public string DeadlineDate { get; set; }
        public string DeadlineTime { get; set; }
    }
}
