﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class BouncedEmailUsers
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public bool IsExternal { get; set; }
    }
}
