﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class Settings
    {
        #region Properties

        [Range(0, 730, ErrorMessage = "730 is the maximum allowed number of")]
        public int StoreArchivedFor { get; set; }

        [Range(0, 730, ErrorMessage = "730 is the maximum allowed number of")]
        public int StoreCompletedFor { get; set; }

        #endregion

        #region Constructors

        public Settings()
        {
            
        }

        public Settings(GMGColorContext context)
        {
            try
            {
                int stCompFor, stArchFor;
                var successParsStCompFor = int.TryParse((from gs in context.GlobalSettings where gs.Key == "STCF" select gs.Value).FirstOrDefault(), out stCompFor);
                var successParsStArchFor = int.TryParse((from gs in context.GlobalSettings where gs.Key == "STAF" select gs.Value).FirstOrDefault(), out stArchFor);
                if (successParsStArchFor && successParsStCompFor)
                {
                    this.StoreCompletedFor = stCompFor;
                    this.StoreArchivedFor = stArchFor;   
                }

            }
            finally
            {
            }
        }

        #endregion
    }
}
