﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ApprovalVersionWorkflowDetails
    {
        public int Id { get; set; }
        public List<WorkflowPhase> Phases { get; set; }
        public bool IsMaxPhaseCompleted { get; set; }
        public DateTime Deadline { get; set; }
        public int? CurrentPhase { get; set; }
        public bool CanEditPhaseDeadline { get; set; }
        public Account objLoggedAccount { get; set; }
        public int CurrentPhasePosition { get; set; }
        public bool LoggedUserIsJobOwner { get; set; }
        public bool IsMaxVersion { get; set; }
        public string FileName { get; set; }
        public int VersionNumber { get; set; }
    }
}
