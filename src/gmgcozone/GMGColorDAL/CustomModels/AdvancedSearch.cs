﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AdvancedSearch
    {
        public int ID { get; set; }

        [StringLength(50, ErrorMessageResourceName = "errToManyCharacters", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string AdvSName { get; set; }

        [Display(Name = "lblJobTitle", ResourceType = typeof(GMGColor.Resources.Resources))]
        [StringLength(25, ErrorMessageResourceName = "errToManyCharacters25", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string AdvSSearchByTitle { get; set; }
        public bool AdvSIsOverdue { get; set; }
        public bool AdvSInProgress { get; set; }
        public bool AdvSWithinATimeframe { get; set; }
        public string AdvSTimeFrameMin { get; set; }
        public string AdvSTimeFrameMax { get; set; }

        public Dictionary<int, string> AdvSGroups { get; set; }
        public List<int> AdvSSelectedGroups { get; set; }
        public Dictionary<int, string> AdvSUsers { get; set; }
        public List<int> AdvSSelectedUsers { get; set; }

        public Dictionary<string, string> AdvSWorkflows { get; set; }
        public List<string> AdvSSelectedWorkflows { get; set; }

        public Dictionary<int, string> AdvSPhases { get; set; }
        public List<int> AdvSSelectedPhases { get; set; }

        public Role.RoleName LoggedUserRole { get; set; }

        public AdvancedSearch() : this("")
        {

        }
        public AdvancedSearch(string datePattern)
        {
            AdvSTimeFrameMin = datePattern == "" ? DateTime.Now.ToString() : DateTime.Now.ToString(datePattern);
            AdvSTimeFrameMax = datePattern == "" ? DateTime.Now.ToString() : DateTime.Now.ToString(datePattern);

            AdvSGroups = new Dictionary<int, string>();
            AdvSSelectedGroups = new List<int>();

            AdvSUsers = new Dictionary<int, string>();
            AdvSSelectedUsers = new List<int>();

            AdvSWorkflows = new Dictionary<string, string>();
            AdvSSelectedWorkflows = new List<string>();

            AdvSPhases = new Dictionary<int, string>();
            AdvSSelectedPhases = new List<int>();
        }

        public GMGColorDAL.AdvancedSearch ToDALObj(string datePattern, GMGColorDAL.AdvancedSearch advancedSearch = null)
        {
            if (advancedSearch == null)
            {
                advancedSearch = new GMGColorDAL.AdvancedSearch();
            }

            advancedSearch.JobTitle = AdvSSearchByTitle;
            advancedSearch.AdvancedSearchId = ID;
            advancedSearch.Name = AdvSName;
            advancedSearch.OverdueFiles = !AdvSIsOverdue && !AdvSInProgress ? (bool?)null : AdvSIsOverdue;
            advancedSearch.WithinATimeframe = AdvSWithinATimeframe;
            advancedSearch.MinTimeDeadline = DateTime.ParseExact(AdvSTimeFrameMin, datePattern, null);
            advancedSearch.MaxTimeDeadline = DateTime.ParseExact(AdvSTimeFrameMax, datePattern, null);
            advancedSearch.AdvancedSearchGroups = AdvSSelectedGroups.Select(gId => new AdvancedSearchGroup()
            {
                AdvancedSearchId = ID,
                GroupId = gId
            }).ToList();
            advancedSearch.AdvancedSearchUsers = AdvSSelectedUsers.Select(uId => new AdvancedSearchUser()
            {
                AdvancedSearchId = ID,
                UserId = uId
            }).ToList();
            advancedSearch.AdvancedSearchWorkflows = AdvSSelectedWorkflows.Select(w => new AdvancedSearchWorkflow()
            {
                AdvancedSearchId = ID,
                Workflow = w
            }).ToList();
            advancedSearch.AdvancedSearchApprovalPhases = AdvSSelectedPhases.Select(pId => new AdvancedSearchApprovalPhase()
            {
                AdvancedSearchId = ID,
                ApprovalPhaseId = pId
            }).ToList();

            return advancedSearch;
        }

        public bool IsManagerRole
        {
            get { return LoggedUserRole == Role.RoleName.AccountAdministrator || LoggedUserRole == Role.RoleName.AccountManager; }
        }
    }

    [Serializable]
    public class AdvancedSearchName
    {
        public int ID { set; get; }
        public string Name { set; get; }
    }
}
