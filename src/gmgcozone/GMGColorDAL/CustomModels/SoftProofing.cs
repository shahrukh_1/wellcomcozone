﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    #region Enums
    public enum SoftProofingSwitchEnum
    {
        PaperTint = 1,
        SimulationProfile = 2,
        Workstations = 3,
        Reports = 4
    }

    public enum CalibrationSoftwareEnum
    {
        BasicColorDisplay,
        Other
    }

    public enum SoftProofingSessionState
    {
        Ok,
        InProgress,
        NotActive
    }

    #endregion

    public class SoftProofing
    {
        [Required(ErrorMessageResourceName = "reqAuthKey", ErrorMessageResourceType = typeof(Resources))]
        public string AuthKey { get; set; }

        [Required(ErrorMessageResourceName = "reqMachineId", ErrorMessageResourceType = typeof(Resources))]
        public string MachineId { get; set; }

        [Required(ErrorMessageResourceName = "reqDataFile", ErrorMessageResourceType = typeof(Resources))]
        public string FileData { get; set; }

        [Required(ErrorMessageResourceName = "reqFileName", ErrorMessageResourceType = typeof(Resources))]
        public string FileName { get; set; }

        [Required(ErrorMessageResourceName = "reqCalibrationSoftware", ErrorMessageResourceType = typeof(Resources))]
        public string CalibrationSoftware { get; set; }

        [Required(ErrorMessageResourceName = "reqWorkstationName", ErrorMessageResourceType = typeof(Resources))]
        public string WorkstationName { get; set; }

        [Required(ErrorMessageResourceName = "reqDisplayIdentifier", ErrorMessageResourceType = typeof(Resources))]
        public string DisplayIdentifier { get; set; }

        [Required(ErrorMessageResourceName = "reqDisplayName", ErrorMessageResourceType = typeof(Resources))]
        public string DisplayName { get; set; }

        [Required(ErrorMessageResourceName = "reqCalibrationTimeZoneOffset", ErrorMessageResourceType = typeof(Resources))]
        public string CalibrationDateTimeUTC { get; set; }

        [Required(ErrorMessageResourceName = "reqDisplayProfileName", ErrorMessageResourceType = typeof(Resources))]
        public string DisplayProfileName { get; set; }

        public int DisplayWidth { get; set; }

        public int DisplayHeight { get; set; }
    }

    public class Workstation
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Username { get; set; }

        public string MachineId { get; set; }

        public DateTime? CalibrationDate { get; set; }

        public string LastVerificationDate { get; set; }

        public bool IsCalibrated { get; set; }

        public int Status { get; set; }

        public string LastErrorMessage { get; set; }

        public string DisplayProfileName { get; set; }

        public string CalibrationSoftware { get; set; }

    }

    public class WorkstationReport
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string FileGuid { get; set; }

        public int UserId { get; set; }

        public string Username { get; set; }

        public DateTime? CalibrationDate { get; set; }

        public string LastCalibrationDate { get; set; }

        public IEnumerable<string> Results { get; set; }

        public string ResultsString { get; set; }

    }

    public class WorkstationEdit
    {
        public int ID { get; set; }

        [Required(ErrorMessageResourceName = "reqName", ErrorMessageResourceType = typeof(Resources))]
        [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "reqUsername", ErrorMessageResourceType = typeof(Resources))]
        public int SelectedUserId { get; set; }

        public string MachineId { get; set; }

    }

    public class PaperTintViewModel
    {
        public PaperTintViewModel()
        {
            MeasurementConditionValues = new List<MeasurementConditionValuesViewModel>();
        }
        public int ID { get; set; }
        
        [Required]
        [StringLength(50)]
        //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
        public string Name { get; set; }

        public string SelectedMediaCategoryName { get; set; }
        
        [Required]
        public int SelectedMediaCategory { get; set; }
        public int SelectedMeasurementCondition { get; set; }
        public List<MediaCategoryViewModel> MediaCategories { get; set; }
        public List<MeasurementConditionValuesViewModel> MeasurementConditionValues { get; set; }
        public List<MeasurementConditionViewModel> MeasurementConditions { get; set; }
    }

    public class PaperTintDashBoardViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string MediaCategory { get; set; }
        public List<DashboardMeasurementConditions> MeasurementConditions { get; set; }
    }

    public class DashboardMeasurementConditions
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double? L { get; set; }
        public double? a { get; set; }
        public double? b { get; set; }
    }

    public class SimulationProfileLabCalculations
    {
        public int ID { get; set; }
        public double? L { get; set; }
        public double? a { get; set; }
        public double? b { get; set; }
        public bool? IsValid { get; set; }
    }

    public class MediaCategoryViewModel
    {
        public int ID { get; set; }
        [Required]
        public string MediaCategoryName { get; set; }
    }

    public class MeasurementConditionViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class MeasurementConditionValuesViewModel
    {
        public int MeasurementCondition { get; set; }

        [Required]
        public double L { get; set; }

        [Required]
        public double a { get; set; }

        [Required]
        public double b { get; set; }
    }

    public class SimulationProfileViewModel
    {
        public int ID { get; set; }
       
        [Required]
        [Display(Name="Name")]
        [StringLength(50)]
        //[RegularExpression(GMG.CoZone.Common.Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(Resources))]
        public string ProfileName { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        public int? SelectedMediaCategory { get; set; }
        
        public int SelectedMeasurementCondition { get; set; }

        [Required]
        public List<MediaCategoryViewModel> MediaCategories { get; set; }

        public List<MeasurementConditionViewModel> MeasurementConditions { get; set; }
    }

    public class SimulationProfileDashBoardViewModel
    {
        public int ID { get; set; }
        public bool IsDefaultProfile { get; set; }
        public string ProfileName { get; set; }
        public string FileName { get; set; }
        public string MediaCategory { get; set; }
        public bool? IsValid { get; set; }
        public List<DashboardMeasurementConditions> MeasurementConditions { get; set; }
    }

    public class SoftProofingSettings
    {
        public List<Workstation> Workstations { get; set; }

        public List<WorkstationReport> Reports { get; set; }

        public List<PaperTintDashBoardViewModel> PaperTints { get; set; }

        public List<SimulationProfileDashBoardViewModel> SimulationProfiles { get; set; }

        public int Page { get; set; }
        public string TimezoneOffset { get; set; }
        public string SearchText { get; set; }
        public string SortField { get; set; }
        public string SortDir { get; set; }
        public int SelectedInstance { get; set; }
        public SoftProofingSwitchEnum Type { get; set; }
        public int CalibrationTimeoutDays { get; set; }
        public int TotalRowsNr { get; set; }
        public int RowsPerPage { get; set; }
        public ReportFilters Filters { get; set; }
        public bool DisplayAgentDownload { get; set; }

        public SoftProofingSettings()
        {
            Workstations = new List<Workstation>();
            Reports = new List<WorkstationReport>();
            PaperTints = new List<PaperTintDashBoardViewModel>();
            SimulationProfiles = new List<SimulationProfileDashBoardViewModel>();
            Filters = new ReportFilters();
            DisplayAgentDownload = false;
        }
    }

    public class ReportFilters
    {
        private DateTime? calibrationDateBetweenStartDate;
        private DateTime? calibrationDateBetweenEndDate;

        public List<string> ListWorkstations { get; set; }
        public List<string> SelectedWorkstations { get; set; }

        public List<KeyValuePair<int, string>> ListUsers { get; set; }
        public List<int> SelectedUsers { get; set; }

        public string CalibrationStartString { get; set; }

        public DateTime? CalibrationStartDate
        {
            get
            {
                return calibrationDateBetweenStartDate ??
                       (calibrationDateBetweenStartDate = Utils.ConvertString2Datetime(CalibrationStartString,
                                                                                        DateTimeFormat));
            }
        }

        public string CalibrationEndString { get; set; }

        public DateTime? CalibrationEndDate
        {
            get
            {
                return calibrationDateBetweenEndDate ??
                       (calibrationDateBetweenEndDate =
                        Utils.ConvertString2Datetime(CalibrationEndString, DateTimeFormat));
            }
        }

        public string DateTimeFormat { get; set; }

        public DateTime? MinCalibrationDate { get; set; }

        public DateTime? MaxCalibrationDate { get; set; }

        public ReportFilters()
        {
            ListWorkstations = new List<string>();
            SelectedWorkstations = new List<string>();

            ListUsers = new List<KeyValuePair<int, string>>();
            SelectedUsers = new List<int>();

            DateTimeFormat = string.Empty;

        }
    }

    public class StartSession
    {
        [Required(ErrorMessageResourceName = "reqUserGuid", ErrorMessageResourceType = typeof(Resources))]
        public string UserGuid { get; set; }

        [Required(ErrorMessageResourceName = "reqMachineId", ErrorMessageResourceType = typeof(Resources))]
        public string MachineId { get; set; }

        public new string ToString()
        {
            return string.Format("{0}|{1}", UserGuid, MachineId);
        }

        public static bool TryParse(string stringValue, out StartSession session)
        {
            session = null;
            if (!string.IsNullOrEmpty(stringValue))
            {
                string[] values = stringValue.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
                if (values.Length == 2)
                {
                    session = new StartSession() {UserGuid = values.ElementAt(0), MachineId = values.ElementAt(1)};
                    return true;
                }
            }
            return false;
        }
    }
    
}
