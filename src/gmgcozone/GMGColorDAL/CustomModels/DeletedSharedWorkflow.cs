﻿using System;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class DeletedSharedWorkflow
    {
        #region Properties

        public string WorkflowName { get; set; }
        public int User { get; set; }

        #endregion

        #region Constructors
        #endregion
    }
}

