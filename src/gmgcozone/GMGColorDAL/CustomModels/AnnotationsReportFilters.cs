﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class AnnotationsReportFilters
    {
        #region Properties

        public int? SelectedApproval { get; set; }

        public string SelectedJob { get; set; }

        public int? SelectedJobId { get; set; }

        public string Guid { get; set; }

        public int ReportTypeID { get; set; }

        public AnnotationReportType? ReportType { get; set; }

        public int ReportGroupingID { get; set; }

        public AnnotationReportGrouping? GroupBy { get; set; }

        [Range(1, 40, ErrorMessageResourceName = "numberMustBeBetweenOneAndFifteen", ErrorMessageResourceType = typeof(Resources))]
        public int AnnotationsPerPage { get; set; }

        public bool? IsFromDashboard { get; set; }

        public int ReportFilteringID { get; set; }

        public AnnotationReportFiltering FilterBy { get; set; }

        public List<AnnotationsReportUser> AnnotationsReportUsers { get; set; }

        public List<AnnotationsReportGroup> AnnotationsReportGroups { get; set; }

        public List<AnnotationReportPhase> AnnotationsReportPhases { get; set; } 

        public string SelectedApprovals { get; set; }

        public bool ShowSummaryPage { get; set; }

        public AnnotationReportFormatType ReportFormatType { get; set; }

        public bool IncludeAllVersions { get; set; }
        public bool IncludeAllInternalUsers { get; set; }
        public bool IncludeAllExternalUsers { get; set; }
        public bool IncludeAllGroups { get; set; }
        public bool IncludeAllPhases { get; set; }

        public int SelectedVersion { get; set; }

        public bool? IsFromAPICall { get; set; }


        #endregion


        public AnnotationsReportFilters()
        {
            AnnotationsReportUsers =  new List<AnnotationsReportUser>();
            AnnotationsReportGroups = new List<AnnotationsReportGroup>();
            AnnotationsReportPhases = new List<AnnotationReportPhase>();
        }


        /// <summary>
        /// Create a list of options based on a given Enum type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SelectList ListOf<T>()
        {
            Type t = typeof(T);
            if (t.IsEnum)
            {
                var values = from Enum e in Enum.GetValues(t)
                             select new { ID = (int)Enum.Parse(typeof(T), e.ToString()), Name = GetOptionName(typeof(T), e.ToString()) };
                return new SelectList(values, "Id", "Name");
            }
            return null;
        }

        /// <summary>
        /// Get option name by enum option key
        /// </summary>
        /// <param name="type"></param>
        /// <param name="enumName"></param>
        /// <returns></returns>
        private static string GetOptionName(Type type, string enumName)
        {
            if (type == typeof(AnnotationReportFiltering))
            {
                var value = (AnnotationReportFiltering)Enum.Parse(typeof(AnnotationReportFiltering), enumName);
                switch (value)
                {
                    case AnnotationReportFiltering.User:
                        return Resources.lblUsers;
                    case AnnotationReportFiltering.Group:
                        return Resources.lblGroups;
                }
            }

            if (type == typeof (AnnotationReportGrouping))
            {
                var value = (AnnotationReportGrouping)Enum.Parse(typeof(AnnotationReportGrouping), enumName);
                switch (value)
                {
                    case AnnotationReportGrouping.User:
                        return Resources.lblUser;
                    case AnnotationReportGrouping.Group:
                        return Resources.lblGroup;
                    case AnnotationReportGrouping.Date:
                        return Resources.lblDateCreated;
                }
            }

            if (type == typeof(AnnotationReportType))
            {
                var value = (AnnotationReportType)Enum.Parse(typeof(AnnotationReportType), enumName);
                switch (value)
                {
                    case AnnotationReportType.Comments:
                        return Resources.lblComments;
                    case AnnotationReportType.Pins:
                        return Resources.lblPins;
                    case AnnotationReportType.CommentsAndPins:
                        return Resources.lblCommentsAndPins;
                }
            }
            return string.Empty;
        }
    }
}
