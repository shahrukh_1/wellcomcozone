﻿namespace GMGColorDAL.CustomModels
{
    public class ApprovalOverdueM
    {
        public int ApprovalId { get; set; }
        public int CreatorUserId { get; set; }
    }
}
