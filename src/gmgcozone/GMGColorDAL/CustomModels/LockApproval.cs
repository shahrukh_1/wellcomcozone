﻿using GMGColorDAL.CustomModels.ApprovalBL;

namespace GMGColorDAL.CustomModels
{
    public class LockApproval
    {
        public bool IsLastPhaseCompleted { get; set; }
        public bool IsRejected { get; set; }
        public bool MoveNext { get; set; }
        public string PhaseName { get; set; }
        public ApprovalPhaseDetails PhaseDetails { get; set; }

        public LockApproval()
        {
            PhaseDetails = new ApprovalPhaseDetails();
        }
    }
}
