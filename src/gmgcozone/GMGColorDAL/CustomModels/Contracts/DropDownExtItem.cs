﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public interface IDropDownExtItem
    {
        IDictionary<string, string> GetHtmlAttributes();
    }
}
