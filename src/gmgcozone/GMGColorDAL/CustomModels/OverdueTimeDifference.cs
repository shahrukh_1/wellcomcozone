﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGColorDAL.CustomModels
{
    public class OverdueTimeDifference
    {
        public int Days { get; set; }
        public int Hours { get; set; }
    }
}
