﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using GMG.CoZone.Common;
using GMGColor.Resources;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class ColorProofSettings
    {
        [Serializable]
        public class ColorProofInstance
        {
            #region Properties

            public int ID { get; set; }

            public string ComputerName { get; set; }

            [Required(ErrorMessageResourceName = "reqColorProofCoZoneNameRequired", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string CoZoneName { get; set; }

            [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string Version { get; set; }

            [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string SerialNumber { get; set; }

            public long? LastSeen { get; set; }

            public int ManualStateKey { get; set; }

            public bool IsOnline { get; set; }

            [Required(ErrorMessageResourceName = "reqColorProofInstanceAdministrator", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(128, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string AdminName { get; set; }

            [Required(ErrorMessageResourceName = "reqEmail", ErrorMessageResourceType = typeof(Resources))]
            [RegularExpression(Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string Email { get; set; }

            [Required(ErrorMessageResourceName = "reqColorProofInstanceLocationRequired", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string Address { get; set; }

            public long WorkflowsLastModifiedTimestamp { get; set; }

            public string PairingCode { get; set; }

            public DateTime PairingCodeExpirationDate { get; set; }

            [Remote("IsCPUsername_Available", "Settings", AdditionalFields = "ID")]
            public string ColorProofUsername { get; set; }
          
            public string ColorProofPassword { get; set; }

            public bool IsChangeLogin { get; set; }

            public bool IsUserPassLogin { get; set; }

            public string OldUserName { get; set; }

            #endregion

            #region Constructors
            public ColorProofInstance()
            {
                ManualStateKey = (int) GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Pending;
            }
            #endregion
        }

        [Serializable]
        public class ColorProofWorkflow
        {
            #region Properties

            public int ID { get; set; }

            public int? SelectedWorkflow { get; set; }

            [Required(ErrorMessageResourceName = "reqColorProofWorkFlowNameRequired", ErrorMessageResourceType = typeof(Resources))]
            [StringLength(50, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string Name { get; set; }

            [Required(ErrorMessageResourceName = "reqCPRegistration", ErrorMessageResourceType = typeof(Resources))]
            public int? SelectedCPServerId { get; set; }

            [Required(ErrorMessageResourceName = "reqCPWorkflow", ErrorMessageResourceType = typeof(Resources))]
            public int? SelectedCPWorkflowId { get; set; }

            [Required(ErrorMessageResourceName = "reqStatus", ErrorMessageResourceType = typeof(Resources))]
            public bool? IsAvailable { get; set; }

            [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(Resources))]
            public string ProofStandard { get; set; }

            public int MaximumUsablePaperWidth { get; set; }

            public bool IsInlineProofVerificationSupported { get; set; }

            [Required(ErrorMessageResourceName = "reqTransmissionTimeout", ErrorMessageResourceType = typeof(Resources))]
            [RegularExpression(@"^[1-9][0-9]*$", ErrorMessageResourceName = "invalidTransmission", ErrorMessageResourceType = typeof(Resources))]
            public int? TransmissionTimeout { get; set; }

            public string SupportedSpotColors { get; set; }

            public List<UserGroupsInUser> ListUserGroups { get; set; }
            public List<GMGColorDAL.ColorProofInstance> ListServers { get; set; }
            public List<GMGColorDAL.ColorProofWorkflow> ListWorkflows { get; set; }

            public int? page { get; set; }

            #endregion

            #region Constructors

            public ColorProofWorkflow()
            {
                ListUserGroups = new List<UserGroupsInUser>();
            }

            public ColorProofWorkflow(Account objAccount, int selectedWorkflow, GMGColorContext context)
             {
                ListUserGroups = new List<UserGroupsInUser>();

                var groups = new List<int>();

                if (selectedWorkflow > 0)
                {
                    groups = DALUtils.SearchObjects<ColorProofCoZoneWorkflowUserGroup>(
                                    o => o.ColorProofCoZoneWorkflow == selectedWorkflow, context)
                                   .Select(t => t.UserGroup)
                                   .ToList();
                }

                List<UserGroup> userGroups =
                DALUtils.SearchObjects<UserGroup>(
                    o => o.Account == objAccount.ID, context)
                        .ToList();

                foreach (UserGroup userGroupBo in userGroups)
                {
                    UserGroupInfo objUserGroupInfo = new UserGroupInfo
                    {
                        ID = userGroupBo.ID,
                        Name = userGroupBo.Name,
                        Account = userGroupBo.Account,
                        Creator = userGroupBo.Creator,
                        Modifier = userGroupBo.Modifier,
                        CreatedDate = userGroupBo.CreatedDate,
                        ModifiedDate = userGroupBo.ModifiedDate
                    };

                    UserGroupsInUser userGroupsInUser = new UserGroupsInUser
                    {
                        IsUserGroupInUser =
                            groups.Contains(userGroupBo.ID),
                        objUserGroup = objUserGroupInfo
                    };
                    ListUserGroups.Add(userGroupsInUser);
                }

                ListServers =
                   GMGColorDAL.ColorProofInstance.GetList(
                       new GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum[]
                            {
                                GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Running,
                                GMGColorDAL.ColorProofInstance.ColorProofInstanceStateEnum.Ready
                            }, false,
                       objAccount.ID, context
                       );
                ListWorkflows = new List<GMGColorDAL.ColorProofWorkflow>();
            }

            #endregion
        }
        
        [Serializable]
        public class ColorProofInstances : Common
        {
            #region Properties

            private string _searchText;
            public List<GMGColorDAL.ColorProofInstance> ListInstances { get; set; }
            public ColorProofInstance ColorProofInstance { get; set; }
            public int TimezoneOffset { get; set; }
            public string SearchText
            {
                get { return _searchText; }
                set { _searchText = value;}
            }
            public int UploaderKey { get; set; }
            public int? page { get; set; }

            #endregion

            #region Constructors

            public ColorProofInstances()
            {
                ColorProofInstance = new ColorProofInstance();
            }

            public ColorProofInstances(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                ColorProofInstance = new ColorProofInstance();
                UpdateDataSource(context);
            }

            #endregion

            public void UpdateDataSource(GMGColorContext context, string searchText = null)
            {
                ListInstances = GMGColorDAL.ColorProofInstance.GetList(false, objLoggedAccount.ID, context, searchText);
            }
           
        }

        [Serializable]
        public class ColorProofWorkflows : Common
        {
            #region Properties

            private string _searchText;
            //TODO rename to ListWorkflows
            public List<ColorProofWorkflowInstanceView> Workflows { get; set; }
            public ColorProofWorkflow WorkflowInstance { get; set; }
            public int? page { get; set; }

            public string SearchText
            {
                get { return _searchText; }
                set
                {
                    _searchText = value;
                }
            }
            public int UploaderKey { get; set; }
            public string SelectedInstance { get; set; }

            #endregion

            #region Constructors

            public ColorProofWorkflows()
            {
            }

            public ColorProofWorkflows(Account objAccount, GMGColorContext context)
                : base(objAccount, context)
            {
                Workflows = ColorProofWorkflowInstanceView.GetList(this.objLoggedAccount.ID, context);
            }

            #endregion

            #region Public Methods
            public void UpdateDataSource(GMGColorContext context, string searchText = null)
            {
                Workflows = GMGColorDAL.ColorProofWorkflowInstanceView.GetList(this.objLoggedAccount.ID, context, searchText);
            }
            #endregion
        }

        [Serializable]
        public class SharedColorProofWorkflowDetails
        {
            #region Properties

            public int ID { get; set; }

            public string UserName { get; set; }

            public string CompanyName { get; set; }

            public string Email { get; set; }

            public bool IsEnabled { get; set; }

            public bool? IsAccepted { get; set; }

            public string InviteStatus { get; set; }

            public string AccountName { get; set; }

            public string AccountUrl { get; set; }

            #endregion
        }

    }
}
