﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class UserGroupsInUser
    {
        #region Properties

        public UserGroupInfo objUserGroup { get; set; }
        public bool IsUserGroupInUser { get; set; }
        public bool IsUserGroupInVisibility { get; set; }

        #endregion

        public UserGroupsInUser()
        {
            this.objUserGroup = new UserGroupInfo();
        }
    }

    [Serializable]
    public class UserGroupInfo
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int Account { get; set; }
        [Required]
        public int Creator { get; set; }
        [Required]
        public int Modifier { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public DateTime ModifiedDate { get; set; }
    }
}
