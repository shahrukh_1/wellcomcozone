﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorDAL.CustomModels.Api;

namespace GMGColorDAL.CustomModels
{
    public class UpdateDecision
    {
        public UpdateApprovalDecisionResponse UpdateApprovalDecisionResponse { get; set; }
        public ApprovalWorkflowInstantNotification ApprovalWorkflowInstantNotification { get; set; }
    }
}
