﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using GMGColor.Resources;
using GMGColorDAL.Common;
using GMG.CoZone.Common;

namespace GMGColorDAL.CustomModels
{
    [Serializable]
    public class EditAccount
    {
        [Serializable]
        public class Summary
        {
            #region Properties

            public Account objAccount { get; set; }
            public Proofs objCollaborateProofs { get; set; }           
            public Proofs objDeliverProofs { get; set; }
            public Proofs objSoftProofingWorkStations { get; set; }
            public Proofs objColorProofConnections { get; set; }
            public Storage objStorage { get; set; }
            public AccountUserCount objAccountUserCount { get; set; }
            public Information objInformation { get; set; }
            public AccountBillingPlansInUse accBillingPlans { get; set; }
            public AccountSubscriptionDetails accSubscriptionDetails { get; set; }
            
            #endregion

            #region Constructors

            public Summary()
            {
            }

            public Summary(int idAccount, Account loggedAccount, GMGColorContext context, AccountSubscriptionDetails accountSubscription = null, AccountBillingPlansInUse accountBillingPlans = null)
            {
                {
                    this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();

                    if (accountBillingPlans == null)
                    {
                        accountBillingPlans = new AccountBillingPlansInUse()
                        {
                            CollaborateBillingPlan = objAccount.CollaborateBillingPlan(context),                           
                            DeliverBillingPlan = objAccount.DeliverBillingPlan(context)
                        };
                        accBillingPlans = accountBillingPlans;
                    }

                    if (objAccount != null && objAccount.NeedSubscriptionPlan)
                    {
                        if (accountBillingPlans.DeliverBillingPlan != null)
                        {
                            int numberOfUsedColorProofInstances =
                                ColorProofInstance.GetNumberOfCPInstances(objAccount.ID, context);

                            objColorProofConnections = new Proofs(numberOfUsedColorProofInstances, accountBillingPlans.DeliverBillingPlan.ColorProofConnections.GetValueOrDefault());

                        }
                        else
                        {
                            objColorProofConnections = new Proofs();
                        }

                        DateTime accountCreatedDate = GMGColorFormatData.GetUserTimeFromUTC(objAccount.CreatedDate, loggedAccount.TimeZone);
                        if (AccountType.GetAccountType(this.objAccount, context) != AccountType.Type.Subscriber)
                        {
                            #region NON SUBSCRIBER

                            int usedCollaborateProofs = 0;                           
                            int usedDeliverProofs = 0;

							AccountSubscriptionPlan collaborateSubscriptionPlan, deliverSubscriptionPlan;
                              

                            if (accountSubscription == null)
                            {
                                collaborateSubscriptionPlan = objAccount.CollaborateSubscriptionPlan(context);
                                deliverSubscriptionPlan = objAccount.DeliverSubscriptionPlan(context);

                                accSubscriptionDetails = new AccountSubscriptionDetails()
                                    {
                                        CollaborateSubscriptionPlan = collaborateSubscriptionPlan,
                                        DeliverSubscriptionPlan = deliverSubscriptionPlan
                                    };
                            }
                            else
                            {
                                collaborateSubscriptionPlan = accountSubscription.CollaborateSubscriptionPlan;
                                deliverSubscriptionPlan = accountSubscription.DeliverSubscriptionPlan;
                            }

                            int collaborateProofs = Account.GetProofsCount(accountBillingPlans.CollaborateBillingPlan);                           
                            int deliverProofs = Account.GetProofsCount(accountBillingPlans.DeliverBillingPlan);

                            if (collaborateSubscriptionPlan != null && collaborateSubscriptionPlan.ContractStartDate.HasValue)
                            {
                                usedCollaborateProofs = Account.GetUsedProofsCount(objAccount.ID, GMG.CoZone.Common.AppModule.Collaborate, accountCreatedDate, context,
                                                                                   collaborateSubscriptionPlan.ContractStartDate, collaborateSubscriptionPlan.IsMonthlyBillingFrequency);
                            }
                            if (deliverSubscriptionPlan != null && deliverSubscriptionPlan.ContractStartDate.HasValue)
                            {
                                usedDeliverProofs = Account.GetUsedProofsCount(objAccount.ID, GMG.CoZone.Common.AppModule.Deliver, accountCreatedDate, context, 
                                                                               deliverSubscriptionPlan.ContractStartDate, deliverSubscriptionPlan.IsMonthlyBillingFrequency);
                            }                           

                            this.objCollaborateProofs = (accountBillingPlans.CollaborateBillingPlan != null &&
                                                         !accountBillingPlans.CollaborateBillingPlan.IsFixed)
                                                            ? new Proofs(usedCollaborateProofs, collaborateProofs)
                                                            : new Proofs();
                          
                            this.objDeliverProofs = (accountBillingPlans.DeliverBillingPlan != null &&
                                                     !accountBillingPlans.DeliverBillingPlan.IsFixed)
                                                        ? new Proofs(usedDeliverProofs, deliverProofs)
                                                        : new Proofs();
                            if (accountBillingPlans !=null &&
                                accountBillingPlans.CollaborateBillingPlan != null &&
                                accountBillingPlans.CollaborateBillingPlan.MaxStorageInGb != null && 
                                accountBillingPlans.CollaborateBillingPlan.MaxStorageInGb > 0)
                            {
                                var storageUsed = (from aus in context.AccountStorageUsages
                                                   where aus.Account == this.objAccount.ID
                                                   select aus.UsedStorage).FirstOrDefault();
                                this.objStorage = new Storage(storageUsed, (double)accountBillingPlans.CollaborateBillingPlan.MaxStorageInGb);
                                this.objAccountUserCount = new AccountUserCount();
                                this.objAccountUserCount.MaxNumberOfUsers = accountBillingPlans.CollaborateBillingPlan.MaxUsers;
                                this.objAccountUserCount.CurrentNumberOfUsers = (from ac in context.Accounts
                                                                                join us in context.Users on ac.ID equals us.Account
                                                                                where ac.ID == this.objAccount.ID && us.Status != (int)GMG.CoZone.Common.UserStatus.Delete
                                                                                select us.ID).Count();
                            }
                            #endregion
                        }
                        else
                        {
                            #region SUBSCRIBER

                            int usedCollaborateProofs = 0;                         
                            int usedDeliverProofs = 0;

							SubscriberPlanInfo collaborateSubscriberPlan, deliverSubscriberPlan;
                             

                            if (accountSubscription == null)
                            {
                                collaborateSubscriberPlan = objAccount.CollaborateSubscriberPlan(context);
                                deliverSubscriberPlan = objAccount.DeliverSubscriberPlan(context);

                                accSubscriptionDetails = new AccountSubscriptionDetails()
                                {
                                    CollaborateSubscriberPlan = collaborateSubscriberPlan,
                                    DeliverSubscriberPlan = deliverSubscriberPlan
                                };
                            }
                            else
                            {
                                collaborateSubscriberPlan = accountSubscription.CollaborateSubscriberPlan;
                                deliverSubscriberPlan = accountSubscription.DeliverSubscriberPlan;
                            }

                            int collaborateProofs = Account.GetProofsCount(accountBillingPlans.CollaborateBillingPlan, collaborateSubscriberPlan);                           
                            int deliverProofs = Account.GetProofsCount(accountBillingPlans.DeliverBillingPlan, deliverSubscriberPlan);

                            if (collaborateSubscriberPlan != null && collaborateSubscriberPlan.ContractStartDate.HasValue)
                            {
                                usedCollaborateProofs = Account.GetUsedProofsCount(objAccount.ID, GMG.CoZone.Common.AppModule.Collaborate, accountCreatedDate, context, collaborateSubscriberPlan.ContractStartDate);
                            }
                            if (deliverSubscriberPlan != null && deliverSubscriberPlan.ContractStartDate.HasValue)
                            {
                                usedDeliverProofs = Account.GetUsedProofsCount(objAccount.ID, GMG.CoZone.Common.AppModule.Deliver, accountCreatedDate, context, deliverSubscriberPlan.ContractStartDate);
                            }                          

                            this.objCollaborateProofs = (accountBillingPlans.CollaborateBillingPlan != null &&
                                                         !accountBillingPlans.CollaborateBillingPlan.IsFixed)
                                                            ? new Proofs(usedCollaborateProofs, collaborateProofs)
                                                            : new Proofs();                         
                            this.objDeliverProofs = (accountBillingPlans.DeliverBillingPlan != null &&
                                                     !accountBillingPlans.DeliverBillingPlan.IsFixed)
                                                        ? new Proofs(usedDeliverProofs, deliverProofs)
                                                        : new Proofs();
                            if (accountBillingPlans != null &&
                                accountBillingPlans.CollaborateBillingPlan != null &&
                                accountBillingPlans.CollaborateBillingPlan.MaxStorageInGb !=
                                null && accountBillingPlans.CollaborateBillingPlan.MaxStorageInGb > 0)
                                {
                                    var storageUsed = (from aus in context.AccountStorageUsages
                                                        where aus.Account == loggedAccount.ID
                                                        select aus.UsedStorage).FirstOrDefault();
                                    objStorage = new Storage(storageUsed, (double)accountBillingPlans.CollaborateBillingPlan.MaxStorageInGb);

                                    objAccountUserCount = new AccountUserCount
                                    {
                                        MaxNumberOfUsers = accountBillingPlans.CollaborateBillingPlan.MaxUsers,
                                        CurrentNumberOfUsers = (from ac in context.Accounts
                                            join us in context.Users on ac.ID equals us.Account
                                            where
                                                ac.ID == this.objAccount.ID &&
                                                us.Status != (int) GMG.CoZone.Common.UserStatus.Delete
                                            select us.ID).Count()
                                    };
                                }   
                            #endregion
                        }

                        //this.objStorage = new Storage((decimal)1.9, (decimal)50);
                        this.objInformation = new Information(this.objAccount, accountBillingPlans, context, accountSubscription);
                    }
                    else
                    {
                        objCollaborateProofs = new Proofs();
                        objDeliverProofs = new Proofs();
                        objSoftProofingWorkStations = new Proofs();
                        objColorProofConnections = new Proofs();
                        objInformation = new Information();
                        objStorage = new Storage();
                    }
                }
            }

            #endregion

            #region Sub Classes

            [Serializable]
            public class Proofs
            {
                #region Properties

                public int ProofsUsed { get; private set; }
                public int ProofsTotal { get; private set; }
                public decimal ProofsPercentage { get; private set; }

                #endregion

                #region Constructors

                public Proofs()
                {
                }

                public Proofs(int pUsed, int pTotal)
                {
                    this.ProofsUsed = pUsed;
                    this.ProofsTotal = pTotal;
                    this.ProofsPercentage = pTotal != 0 ? (((decimal)pUsed / (decimal)pTotal) * 100) : 0;
                }

                #endregion
            }

            [Serializable]
            public class Storage
            {
                #region Properties

                public double StorageUsed { get; set; }
                public double StorageTotal { get; set; }
                public double StoragePercentage { get; set; }

                #endregion

                #region Constructors

                public Storage()
                {
                }

                public Storage(double sUsed, double sTotal)
                {
                    this.StorageUsed = sUsed;
                    this.StorageTotal = sTotal;
                    this.StoragePercentage = ((sUsed / sTotal) * 100);
                }

                #endregion
            }

            [Serializable]
            public class AccountUserCount
            {
                public int? MaxNumberOfUsers { get; set; }
                public int? CurrentNumberOfUsers { get; set; }
            }


            [Serializable]
            public class Information
            {
                #region Properties

                public string CollaborateCurrentPlan { get; private set; }
                public string CollaborateAnualTotal { get; private set; }
                public string CollaborateTotalDiscount { get; private set; }
                public string CollaborateAnualTotalMessage { get; private set; }
                public decimal CollaborateAnualTotalValue { get; private set; }
                public decimal CollaborateTotalDiscountValue { get; private set; }

                public string DeliverCurrentPlan { get; private set; }
                public string DeliverAnualTotal { get; private set; }
                public string DeliverTotalDiscount { get; private set; }
                public string DeliverAnualTotalMessage { get; private set; }
                public decimal DeliverAnualTotalValue { get; private set; }
                public decimal DeliverTotalDiscountValue { get; private set; }                

                public string GPP { get; private set; }

                #endregion

                #region Constructors

                public Information()
                {

                }

                public Information(Account objAccount, AccountBillingPlansInUse accountBillingPlans, GMGColorContext context, AccountSubscriptionDetails accountSubscription = null)
                {
                    decimal rate = 1;

                    if (objAccount.Parent.GetValueOrDefault() > 0 && (AccountType.GetAccountType(objAccount.Parent.GetValueOrDefault(),context) != AccountType.Type.GMGColor) && objAccount.CurrencyFormat != objAccount.ParentAccount.CurrencyFormat)
                    {
                        rate = (new GMGColorCommon()).GetUpdatedCurrencyRate(objAccount, context);
                    }

                    string symbol = GMGColorFormatData.GetCurrencySymbol(objAccount.Parent.GetValueOrDefault(), context);

                    BillingPlan collaborateBillingPlan = accountBillingPlans.CollaborateBillingPlan;                 
                    BillingPlan deliverBillingPlan = accountBillingPlans.DeliverBillingPlan;                  

                    if (AccountType.GetAccountType(objAccount, context) != AccountType.Type.Subscriber)
                    {
						AccountSubscriptionPlan collaborateSubscriptionPlan, deliverSubscriptionPlan;

                        if (accountSubscription == null)
                        {
                            collaborateSubscriptionPlan = objAccount.CollaborateSubscriptionPlan(context);
                            deliverSubscriptionPlan = objAccount.DeliverSubscriptionPlan(context);
                        }
                        else
                        {
                            collaborateSubscriptionPlan = accountSubscription.CollaborateSubscriptionPlan;
                            deliverSubscriptionPlan = accountSubscription.DeliverSubscriptionPlan;
                        }

                        #region NON SUBSCRIBER
                        if (collaborateSubscriptionPlan != null && collaborateBillingPlan != null)
                        {
                            #region COLLABORATE
                            decimal billingPlanPrice = 0;
                            if (objAccount.Parent != null && objAccount.Parent.Value > 0)
                            {
                                billingPlanPrice = AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.Value, collaborateBillingPlan.ID, context, rate);
                            }
                            decimal discount = (objAccount.GPPDiscount != null) ? ((billingPlanPrice * (decimal)objAccount.GPPDiscount) / 100) : 0;
                            decimal discountedPrice = billingPlanPrice - discount;
                            decimal anualTotal = ((collaborateSubscriptionPlan.IsMonthlyBillingFrequency) ? discountedPrice : discountedPrice * 12);

                            discount = collaborateSubscriptionPlan.IsMonthlyBillingFrequency ? discount : (decimal)(discount * 12);
                            string totalSavings = GMGColorFormatData.GetFormattedCurrency(discount, symbol);

                            this.CollaborateAnualTotalMessage = string.Format((collaborateSubscriptionPlan.IsMonthlyBillingFrequency) ? Resources.lblAnualTotSavingPerMonth : Resources.lblAnualTotSavingPerYear, totalSavings);
                            this.CollaborateAnualTotalValue = anualTotal;
                            this.CollaborateTotalDiscountValue = discount;

                            this.GPP = (objAccount.GPPDiscount != null) ? ((decimal)objAccount.GPPDiscount).ToString("0.00") + "%" : "0.00%";
                            this.CollaborateCurrentPlan = collaborateBillingPlan.Name;
                            this.CollaborateAnualTotal = GMGColorFormatData.GetFormattedCurrency(this.CollaborateAnualTotalValue, objAccount.Parent.GetValueOrDefault(), context);
                            this.CollaborateTotalDiscount = totalSavings;
                            #endregion
                        }
                      
                        if (deliverSubscriptionPlan != null && deliverBillingPlan != null)
                        {
                            #region Deliver
                            decimal billingPlanPrice = 0;
                            if (objAccount.Parent != null && objAccount.Parent.Value > 0)
                            {
                                billingPlanPrice = AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.Value, deliverBillingPlan.ID, context, rate);
                            }
                            decimal discount = (objAccount.GPPDiscount != null) ? ((billingPlanPrice * (decimal)objAccount.GPPDiscount) / 100) : 0;
                            decimal discountedPrice = billingPlanPrice - discount;
                            decimal anualTotal = ((deliverSubscriptionPlan.IsMonthlyBillingFrequency) ? discountedPrice : discountedPrice * 12);

                            discount = deliverSubscriptionPlan.IsMonthlyBillingFrequency ? discount : (decimal)(discount * 12);
                            string totalSavings = GMGColorFormatData.GetFormattedCurrency(discount, symbol);

                            this.DeliverAnualTotalMessage = string.Format((deliverSubscriptionPlan.IsMonthlyBillingFrequency) ? Resources.lblAnualTotSavingPerMonth : Resources.lblAnualTotSavingPerYear, totalSavings);
                            this.DeliverAnualTotalValue = anualTotal;
                            this.DeliverTotalDiscountValue = discount;

                            this.GPP = (objAccount.GPPDiscount != null) ? ((decimal)objAccount.GPPDiscount).ToString("0.00") + "%" : "0.00%";
                            this.DeliverCurrentPlan = deliverBillingPlan.Name;
                            this.DeliverAnualTotal = GMGColorFormatData.GetFormattedCurrency(this.DeliverAnualTotalValue, objAccount.Parent.GetValueOrDefault(), context);
                            this.DeliverTotalDiscount = totalSavings;
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
						SubscriberPlanInfo collaborateSubscriberPlan, deliverSubscriberPlan;
                              

                        if (accountSubscription == null)
                        {
                            collaborateSubscriberPlan = objAccount.CollaborateSubscriberPlan(context);
                            deliverSubscriberPlan = objAccount.DeliverSubscriberPlan(context);
                        }
                        else
                        {
                            collaborateSubscriberPlan = accountSubscription.CollaborateSubscriberPlan;
                            deliverSubscriberPlan = accountSubscription.DeliverSubscriberPlan;
                        }

                        #region SUBSCRIBER
                        if (collaborateSubscriberPlan != null && collaborateBillingPlan != null)
                        {
                            #region COLLABORATE
                            decimal billingPlanPrice = 0;
                            if (objAccount.Parent != null && objAccount.Parent.Value > 0)
                            {
                                billingPlanPrice = AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.Value, collaborateBillingPlan.ID, context, rate);
                            }
                            decimal discount = (objAccount.GPPDiscount != null) ? ((billingPlanPrice * (decimal)objAccount.GPPDiscount) / 100) : 0;
                            decimal discountedPrice = billingPlanPrice - discount;
                            decimal anualTotal = discountedPrice;

                            string totalSavings = GMGColorFormatData.GetFormattedCurrency(discount, symbol);

                            this.CollaborateAnualTotalMessage = string.Format(Resources.lblAnualTotSavingPerMonth, totalSavings);
                            this.CollaborateAnualTotalValue = anualTotal;
                            this.CollaborateTotalDiscountValue = discount;

                            this.GPP = (objAccount.GPPDiscount != null) ? ((decimal)objAccount.GPPDiscount).ToString("0.00") + "%" : "0.00%";
                            this.CollaborateCurrentPlan = collaborateBillingPlan.Name;
                            this.CollaborateAnualTotal = GMGColorFormatData.GetFormattedCurrency(this.CollaborateAnualTotalValue, objAccount.Parent.GetValueOrDefault(), context);
                            this.CollaborateTotalDiscount = totalSavings;
                            #endregion
                        }                       
                        if (deliverSubscriberPlan != null && deliverBillingPlan != null)
                        {
                            #region Deliver
                            decimal billingPlanPrice = 0;
                            if (objAccount.Parent != null && objAccount.Parent.Value > 0)
                            {
                                billingPlanPrice = AttachedAccountBillingPlan.GetPlanPrice(objAccount.Parent.Value, deliverBillingPlan.ID, context, rate);
                            }
                            decimal discount = (objAccount.GPPDiscount != null) ? ((billingPlanPrice * (decimal)objAccount.GPPDiscount) / 100) : 0;
                            decimal discountedPrice = billingPlanPrice - discount;
                            decimal anualTotal = discountedPrice;

                            string totalSavings = GMGColorFormatData.GetFormattedCurrency(discount, symbol);

                            this.DeliverAnualTotalMessage = string.Format(Resources.lblAnualTotSavingPerMonth, totalSavings);
                            this.DeliverAnualTotalValue = anualTotal;
                            this.DeliverTotalDiscountValue = discount;

                            this.GPP = (objAccount.GPPDiscount != null) ? ((decimal)objAccount.GPPDiscount).ToString("0.00") + "%" : "0.00%";
                            this.DeliverCurrentPlan = deliverBillingPlan.Name;
                            this.DeliverAnualTotal = GMGColorFormatData.GetFormattedCurrency(this.DeliverAnualTotalValue, objAccount.Parent.GetValueOrDefault(), context);
                            this.DeliverTotalDiscount = totalSavings;
                            #endregion
                        }
                      
                        #endregion
                    }
                }

                #endregion
            }

            #endregion
        }

        [Serializable]
        public class Profile
        {
            #region Properties

            public GMGColorDAL.Account objAccount { get; set; }
            public GMGColorDAL.User objLoggedUser { get; set; }
            public GMGColorDAL.Company objCompany { get; set; }
            public string AccountCreator { get; set; }
            public string AccountCreatedDate { get; set; }

            public List<AccountType> ListLoggedAccountAccountTypes(AccountType.Type accountType, GMGColorContext context)
            {
                List<AccountType.Type> lstAccountTypes = new List<AccountType.Type>();
                switch (accountType)
                {
                    case GMGColorDAL.AccountType.Type.GMGColor:
                        lstAccountTypes =
                            new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Subsidiary,
                                GMGColorDAL.AccountType.Type.Dealer,
                                GMGColorDAL.AccountType.Type.Client,
                            }.ToList();
                        break;
                    case GMGColorDAL.AccountType.Type.Subsidiary:
                        lstAccountTypes =
                            new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Subsidiary,
                                GMGColorDAL.AccountType.Type.Dealer,
                                GMGColorDAL.AccountType.Type.Client,
                            }.ToList();
                        break;
                    case GMGColorDAL.AccountType.Type.Dealer:
                        lstAccountTypes =
                            new GMGColorDAL.AccountType.Type[]
                            {
                                GMGColorDAL.AccountType.Type.Dealer,
                                GMGColorDAL.AccountType.Type.Client,
                            }.ToList();
                        break;
                    default:
                        break;
                }

                {
                    return  lstAccountTypes.Select(o => GMGColorDAL.AccountType.GetAccountType(o, context)).ToList();
                }
            }

            #endregion

            #region Constructors

            public Profile()
            {
            }

            public Profile(int idAccount, int idUser, GMGColorContext context)
            {
                this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();
                this.objLoggedUser = (from u in context.Users where u.ID == idUser select u).FirstOrDefault();
                this.objCompany = (from c in context.Companies
                        join a in context.Accounts on c.Account equals a.ID
                        where a.ID == idAccount
                        select c).FirstOrDefault();

                var accountDetails = (from ac in context.Accounts
                                      join u in context.Users on ac.Modifier equals u.ID
                                      where ac.ID == idAccount
                                      select new
                                      {
                                          Creator = u.GivenName + " " + u.FamilyName,
                                          CreatedDate = ac.CreatedDate
                                      }).FirstOrDefault();
               AccountCreator = System.Web.HttpUtility.HtmlEncode(accountDetails.Creator);
               AccountCreatedDate = GMGColorFormatData.GetUserTimeFromUTC(accountDetails.CreatedDate, objAccount.TimeZone).ToString(objAccount.DateFormat1.Pattern + ", hh:mm tt"); 
            }

            #endregion
        }

        [Serializable]
        public class Customise
        {
            #region Properties

            public GMGColorDAL.Account objAccount { get; set; }
            public GMGColorDAL.Company objCompany { get; set; }
            public AccountCustomiseInfo objCustomiseAccount { get; set; }
            public List<ThemeColorSchema> objColorSchema { get; set; }
            public bool ShowSelectedTheme { get; set; }

            #endregion

            #region Constructors

            public Customise()
            {
            }

            public Customise(int idAccount, string folderPath, GMGColorContext context)
            {
                this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();

                this.objCompany = this.objAccount != null ? this.objAccount.Companies.FirstOrDefault() : null;
                this.objCustomiseAccount = (new Account()).GetAccountCustomiseInfo(this.objAccount, folderPath, context);

                //this.objCustomiseAccount.PrePressFunctionsWhereThisIsAccount = new List<AcctPrePressFunction>();
                
                this.objCustomiseAccount.PathToTempFolder = folderPath;
            }

            #endregion
        }

        [Serializable]
        public class SiteSettings
        {
            #region Properties

            public int LoggedAccountType { get; set; }
            public GMGColorDAL.Account objAccount { get; set; }
            public bool ApplyLanguageToAllUsers { get; set; }
            public string MeasurementType { get; set; }

            #endregion

            #region Constructors

            public SiteSettings()
            {
            }

            public SiteSettings(int idAccount, int loggedAccountType, GMGColorContext context)
            {
                this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();
                this.LoggedAccountType = loggedAccountType;
            }

            #endregion
        }

        [Serializable]
        public class WhiteLabelSettings
        {
            #region Properties
            
            public int AccountID { get; set; }
            public string AccountName { get; set; }
            public bool IsRemoveAllGMGCollaborateBranding { get; set; }
            //[Required(ErrorMessageResourceName = "reqCustomFromEmail", ErrorMessageResourceType = typeof(Resources.Resources))]
            [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
            public string CustomFromEmail { get; set; }
            //[Required(ErrorMessageResourceName = "reqCustomFromName", ErrorMessageResourceType = typeof(Resources.Resources))]
            //[RegularExpression(Constants.CollaborateNameRegex, ErrorMessageResourceName = "lblSpecialCharactersNotAllowed", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
            public string CustomFromName { get; set; }
            //public Account objAccount { get; set; }
            public bool DisableLandingPage { get; set; }
            public GMG.CoZone.Common.AppModule DashboardToShow { get; set; }
            [RegularExpression(GMG.CoZone.Common.Constants.EmailRegex, ErrorMessageResourceName = "reqEmailAddress", ErrorMessageResourceType = typeof(Resources))]
            public string ContactSupportEmail { get; set; }

            #endregion

            #region Constructors

            public WhiteLabelSettings()
            {
            }

            public WhiteLabelSettings(int idAccount, GMGColorContext context)
            {
                Account objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();
                if (objAccount != null)
                {
                    this.AccountID = objAccount.ID;
                    this.AccountName = objAccount.Name;
                    this.IsRemoveAllGMGCollaborateBranding = objAccount.IsRemoveAllGMGCollaborateBranding;
                    this.CustomFromEmail = objAccount.CustomFromAddress;
                    this.CustomFromName = objAccount.CustomFromName;
                    this.DisableLandingPage = objAccount.DisableLandingPage;
                    this.DashboardToShow = (GMG.CoZone.Common.AppModule) objAccount.DashboardToShow;
                }

                AccountSetting objAccountSetting = (from acs in context.AccountSettings
                                                    where acs.Name == "ContactSupportEmail" && (acs.Account == idAccount || acs.Account == null)
                                                    orderby acs.Account descending 
                                                    select acs).FirstOrDefault();
                if (objAccountSetting != null)
                {
                    this.ContactSupportEmail = objAccountSetting.Value;
                }
            }

            #endregion
        }

        [Serializable]
        public class DNSAndCustomDomain
        {
            #region Properties

            public Account objAccount { get; set; }
            public List<Domain> AccountDomains { get; set; }

            #endregion

            #region Conscturcors

            public DNSAndCustomDomain()
            {
            }

            public DNSAndCustomDomain(int idAccount, GMGColorContext context)
            {
                this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();

                this.AccountDomains = new List<Domain>();
                Domain objPrimaryDNS = new Domain()
                {
                    DomainName = this.objAccount.Domain,
                    IsAvilable = true,
                    IsPrimary = !this.objAccount.IsCustomDomainActive,
                    IsOriginalDomain = true
                };
                this.AccountDomains.Add(objPrimaryDNS);

                if (!String.IsNullOrEmpty(this.objAccount.CustomDomain))
                {
                    var objCustomDNS = new Domain()
                    {
                        DomainName = this.objAccount.CustomDomain,
                        IsAvilable = GMGColorCommon.PingDomain(objAccount.CustomDomain, idAccount), //GMGColorCommon.ClaimDomain(this.objAccount.CustomDomain),
                        IsPrimary = this.objAccount.IsCustomDomainActive,
                        IsOriginalDomain = false
                    };
                    this.AccountDomains.Add(objCustomDNS);
                }
            }

            #endregion

            #region Sub Classes

            public class Domain
            {
                [RegularExpression(@"^[a-zA-Z0-9\-\.]*$", ErrorMessageResourceName = "reqInvalidDomain", ErrorMessageResourceType = typeof(Resources))]
                public string DomainName { get; set; }
                public bool IsAvilable { get; set; }
                public bool IsPrimary { get; set; }
                public bool IsOriginalDomain { get; set; }
            }

            #endregion
        }

        [Serializable]
        public class Plans
        {
            #region Properties

            public int Account { get; set; }
            [RegularExpression(@"^\d{1,2}$|^\d{1,2}\.\d{1,2}$", ErrorMessageResourceName = "gppDiscountInvalid", ErrorMessageResourceType = typeof(Resources))]
            public decimal? GPPDiscount { get; set; }
            public AccountBillingPlans objAccountBillingPrice { get; set; }
            public List<string> AttachedBillingPlans { get; set; }
            public string AlreadyAttachedBillingPlans { get; set; }
            public List<int> BillingPlans { get; set; }
            public bool IsEditAccountPlansFromReport { get; set; }

            #endregion

            #region Constructors

            public Plans()
            {
            }

            public Plans(int idAccount, GMGColorContext context)
            {
                Account objAccount = (from a in context.Accounts
                              where a.ID == idAccount
                              select a).FirstOrDefault();

                List<int> lstCurrentlyAttBillingPlans = objAccount.AttachedAccountBillingPlans.Select(m => m.BillingPlan).ToList();

                this.Account = idAccount;
                this.GPPDiscount = objAccount.GPPDiscount;
                this.objAccountBillingPrice = GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(objAccount, true, context);
                this.AttachedBillingPlans = lstCurrentlyAttBillingPlans.Select(o => o.ToString()).ToList();

                var lstCurrentAttachedBillingPlans = (from a in context.Accounts
                                    join acs in context.AccountStatus on a.Status equals acs.ID
                                    join asubs in context.AccountSubscriptionPlans on a.ID equals asubs.Account
                                    join bp in context.BillingPlans on asubs.SelectedBillingPlan equals bp.ID
                                    where a.Parent == idAccount && acs.Key != "D" && lstCurrentlyAttBillingPlans.Contains(bp.ID)
                                   select bp.ID).Union(from a in context.Accounts
                                                       join acs in context.AccountStatus on a.Status equals acs.ID
                                                       join asubs in context.SubscriberPlanInfoes on a.ID equals asubs.Account
                                                       join bp in context.BillingPlans on asubs.SelectedBillingPlan equals bp.ID
                                                       where a.Parent == idAccount && acs.Key != "D" && lstCurrentlyAttBillingPlans.Contains(bp.ID)
                                                       select bp.ID).ToList();

                this.AlreadyAttachedBillingPlans = string.Join(",", lstCurrentAttachedBillingPlans.Select(n => n.ToString(CultureInfo.InvariantCulture)).ToArray());
            }

            #endregion
        }

        [Serializable]
        public class BillingInfo
        {
            #region Properties

            public AccountBillingPlans objAccountBillingPlans { get; set; }
            public Account objAccount { get; set; }
            public Account LoggedAccount { get; set; }
            public bool NeedSubscriptionPlan { get; set; }
            public SubscriptionPlanPerModule PlanPerModule { get; set; }
            public SubscriberPlanPerModule SubscriberPlans { get; set; }
            public bool IsEditAccountPlansFromReport { get; set; }

            #endregion

            #region Constructors

            public BillingInfo()
            {
                PlanPerModule = new SubscriptionPlanPerModule();
                SubscriberPlans = new SubscriberPlanPerModule();
            }

            public BillingInfo(int idAccount, GMGColorContext context)
            {
                this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();

                this.objAccountBillingPlans = GMGColorDAL.Account.GetAccountBillingPlanPriceInfo(objAccount, true, context);
                this.NeedSubscriptionPlan = objAccount.NeedSubscriptionPlan;

                PlanPerModule = new SubscriptionPlanPerModule() {AccountId = idAccount};
                SubscriberPlans = new SubscriberPlanPerModule() {AccountId = idAccount};
            }

            #endregion
        }

        [Serializable]
        public class AddEditUser : Common
        {
            #region Properties

            public UserInfo objEditUser { get; set; }
            public UserInfo objNewUser { get; set; }

            #endregion

            #region Constructors

            public AddEditUser()
            {
            }

            public AddEditUser(Account objLoggedAccount, GMGColorContext context)
                : base(objLoggedAccount, context)
            {
            }

            #endregion
        }

        [Serializable]
        public class Users : Common
        {
            #region Properties

            public string SearchText { get; set; }
            public bool ShowAllUsers { get; set; }
            public GMGColorDAL.Account objAccount { get; set; }
            public List<GMGColorDAL.UserRolesView> ListUsers { get; set; }

            #endregion

            #region Constructors

            public Users()
            {
            }

            public Users(int idAccount, Account objLoggedAccount, GMGColorContext context) : base(objLoggedAccount, context)
            {
                if (objLoggedAccount != null && objLoggedAccount.ID == idAccount)
                {
                    this.objAccount = objLoggedAccount;
                }
                else
                {
                    this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();
                }
                

                this.ListUsers =
                    (from urv in context.UserRolesViews
                     where urv.Account == idAccount && urv.UserStatus != "D"
                     orderby urv.GivenName, urv.FamilyName
                     select urv).ToList();
            }

            #endregion

            #region Methods

            public static List<Role> GetRoles(int accountId, GMGColorContext context)
            {
                bool needSubscriptionPlan = (from a in context.Accounts where a.ID == accountId select a.NeedSubscriptionPlan).FirstOrDefault();
                    
                List<Role> roles = (from atr in context.AccountTypeRoles
                            join at in context.AccountTypes on atr.AccountType equals at.ID
                            join a in context.Accounts on atr.AccountType equals a.AccountType
                            where a.ID == accountId
                            select atr.Role1).ToList();
                return needSubscriptionPlan
                            ? roles
                            : roles.Where(o =>
                                            Role.GetRole(o) != Role.RoleName.AccountModerator &&
                                            Role.GetRole(o) != Role.RoleName.AccountContributor &&
                                            Role.GetRole(o) != Role.RoleName.AccountViewer).ToList();
            }

            #endregion
        }

        [Serializable]
        public class SuspendAccount
        {
            #region Properties

            public Account objAccount { get; set; }

            [Required(ErrorMessageResourceName = "reqSuspendReason", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
            [StringLength(256, ErrorMessageResourceName = "errTooManyCharactors", ErrorMessageResourceType = typeof(GMGColor.Resources.Resources))]
            public string Reason { get; set; }

            #endregion

            #region Consctructors

            public SuspendAccount()
            {
            }

            public SuspendAccount(int idAccount, GMGColorContext context)
            {
                this.objAccount = (from a in context.Accounts where a.ID == idAccount select a).FirstOrDefault();
            }

            #endregion
        }
    }
}
