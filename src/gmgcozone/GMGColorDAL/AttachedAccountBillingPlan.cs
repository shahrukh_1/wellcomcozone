//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttachedAccountBillingPlan
    {
        public AttachedAccountBillingPlan()
        {
            this.ChangedBillingPlans = new HashSet<ChangedBillingPlan>();
        }
    
        public int ID { get; set; }
        public int Account { get; set; }
        public int BillingPlan { get; set; }
        public Nullable<decimal> NewPrice { get; set; }
        public bool IsAppliedCurrentRate { get; set; }
        public Nullable<decimal> NewProofPrice { get; set; }
        public bool IsModifiedProofPrice { get; set; }
    
        public virtual Account Account1 { get; set; }
        public virtual BillingPlan BillingPlan1 { get; set; }
        public virtual ICollection<ChangedBillingPlan> ChangedBillingPlans { get; set; }
    }
}
