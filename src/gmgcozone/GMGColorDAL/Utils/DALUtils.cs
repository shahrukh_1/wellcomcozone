﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using GMGColor.Resources;
using GMGColorDAL.Common;
using System.Linq.Expressions;

namespace GMGColorDAL
{
    public static class DALUtils
    {
        public const string DatabaseContext = "DbContextEF6";

        public static List<T> SearchObjects<T>(Expression<Func<T, bool>> func, GMGColorContext context, string[] includes = null) where T : BusinessObjectBase
        {
            try
            {
                IQueryable<T> entityQuery = context.Set<T>();
                if (includes != null && includes.Any())
                {
                    foreach (string include in includes)
                    {
                        entityQuery.Include(include);
                    }
                }
                return entityQuery.Where(func).ToList();
            }
            finally
            {
            }
        }

        public static T GetObject<T>(int entityId, GMGColorContext context, string[] includes = null) where T : BusinessObjectBase
        {
            try
            {
                IQueryable<T> entityQuery = context.Set<T>();
                if (includes != null && includes.Any())
                {
                    foreach (var include in includes)
                    {
                        entityQuery = entityQuery.Include(include);
                    }
                }
                return entityQuery.SingleOrDefault(e => e.ID == entityId);
            }
            finally
            {
            }
        }

        public static void Delete<T>(GMGColorContext context, T item) where T : BusinessObjectBase
        {
            try
            {
                DbSet<T> entityQuery = context.Set<T>();
                entityQuery.Remove(item as T);
            }
            finally
            {
            }
        }

        public static bool IsFromCurrentAccount<T>(int entityId, int accountId, GMGColorContext context) where T : BusinessObjectBase
        {
            try
            {
                return IsFromCurrentAccount<T>(new int[] { entityId }, accountId, context);
            }
            finally
            {
            }
        }

        public static bool IsFromCurrentAccountTree<T>(int entityId, int accountId, GMGColorContext context) where T : BusinessObjectBase
        {
            try
            {
                return IsFromCurrentAccountTree<T>(new int[] { entityId }, accountId, context);
            }
            finally
            {
            }
        }

        public static bool IsFromCurrentAccount<T>(int? entityId, int accountId, GMGColorContext context) where T : BusinessObjectBase
        {
            try
            {
                return IsFromCurrentAccount<T>(new int[] { entityId.GetValueOrDefault() }, accountId, context);
            }
            finally
            {
            }
        }

        public static bool IsFromCurrentAccount<T>(IEnumerable<int?> entityIdList, int accountId, GMGColorContext context)
            where T : BusinessObjectBase
        {
            try
            {
                return IsFromCurrentAccount<T>(entityIdList.Select(o => o.GetValueOrDefault()).ToArray(), accountId, context);
            }
            finally
            {
            }
        }

        public static bool IsFromCurrentAccount<T>(IEnumerable<int> entityIdList, int accountId, GMGColorContext context)
            where T : BusinessObjectBase
        {
            bool result = false;
            try
            {
                if (typeof(T) == typeof(User))
                {
                    result =
                        ((from u in context.Users
                          where entityIdList.Contains(u.ID) && u.Account == accountId
                          select u.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(UserGroup))
                {
                    result =
                        ((from ug in context.UserGroups
                         where entityIdList.Contains(ug.ID) && ug.Account == accountId
                         select ug.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(ExternalCollaborator))
                {
                    result =
                        ((from ec in context.ExternalCollaborators
                         where entityIdList.Contains(ec.ID) && ec.Account == accountId
                         select ec.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(DeliverExternalCollaborator))
                {
                    result =
                        ((from ec in context.DeliverExternalCollaborators
                         where entityIdList.Contains(ec.ID) && ec.Account == accountId
                         select ec.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(ColorProofInstance))
                {
                    result = ((from cpi in context.ColorProofInstances
                              join u in context.Users on cpi.Owner equals u.ID
                              where entityIdList.Contains(cpi.ID) && u.Account == accountId
                              select cpi.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(Account))
                {
                    result = ((from a in context.Accounts
                              where entityIdList.Contains(a.ID) && a.Parent == accountId
                              select a).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(ColorProofCoZoneWorkflow))
                {
                    result = ((from cpczw in context.ColorProofCoZoneWorkflows
                              join cpw in context.ColorProofWorkflows on cpczw.ColorProofWorkflow equals cpw.ID
                              join cpi in context.ColorProofInstances on cpw.ColorProofInstance equals cpi.ID
                              join u in context.Users on cpi.Owner equals u.ID
                              where entityIdList.Contains(cpczw.ID) && u.Account == accountId
                              select cpczw.ID
                             ).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(Company))
                {
                    result = ((from c in context.Companies
                              where entityIdList.Contains(c.ID) && c.Account == accountId
                              select c).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(Approval))
                {
                    result =
                        ((from a in context.Approvals
                         join j in context.Jobs on a.Job equals j.ID
                         where entityIdList.Contains(a.ID) && j.Account == accountId
                         select a.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(DeliverJob))
                {
                    result =
                        ((from d in context.DeliverJobs
                         join j in context.Jobs on d.Job equals j.ID
                         where entityIdList.Contains(d.ID) && j.Account == accountId
                         select d.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(Job))
                {
                    result = ((from j in context.Jobs
                              where entityIdList.Contains(j.ID) && j.Account == accountId
                              select j.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(Folder))
                {
                    result =
                        ((from f in context.Folders
                         where entityIdList.Contains(f.ID) && f.Account == accountId
                         select f.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(UploadEnginePreset))
                {
                    result =
                        ((from pr in context.UploadEnginePresets
                         where entityIdList.Contains(pr.ID) && pr.Account == accountId
                         select pr.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(UploadEngineProfile))
                {
                    result =
                        ((from pr in context.UploadEngineProfiles
                         where entityIdList.Contains(pr.ID) && pr.Account == accountId
                         select pr.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(UploadEngineCustomXMLTemplate))
                {
                    result =
                        ((from xmlt in context.UploadEngineCustomXMLTemplates
                         where entityIdList.Contains(xmlt.ID) && xmlt.Account == accountId
                         select xmlt.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof (T) == typeof (NotificationSchedule))
                {
                    result =
                        ((from ns in context.NotificationSchedules
                        join anp in context.AccountNotificationPresets on ns.NotificationPreset equals anp.ID
                        where entityIdList.Contains(ns.ID) && anp.Account == accountId
                        select ns.ID).Count()) == entityIdList.Count() ? true : false;
                }
				 else if (typeof(T) == typeof(AccountNotificationPreset))
                {
                    result =
                       ((from anp in context.AccountNotificationPresets
                        where entityIdList.Contains(anp.ID) && anp.Account == accountId
                        select anp.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(BrandingPreset))
                {
                    result =
                       ((from bp in context.BrandingPresets
                        where entityIdList.Contains(bp.ID) && bp.Account == accountId
                        select bp.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(ApprovalWorkflow))
                {
                    result =
                       ((from aw in context.ApprovalWorkflows
                        where entityIdList.Contains(aw.ID) && aw.Account == accountId
                        select aw.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(ApprovalPhase))
                {
                    result =
                       ((from ap in context.ApprovalPhases
                        join aw in context.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                        where entityIdList.Contains(ap.ID) && aw.Account == accountId
                        select ap.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(SimulationProfile))
                {
                    result = ((from smp in context.SimulationProfiles
                              where entityIdList.Contains(smp.ID) && smp.AccountId == accountId
                              select smp).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(PaperTint))
                {
                    result =
                        ((from ppt in context.PaperTints
                         where entityIdList.Contains(ppt.ID) && ppt.AccountId == accountId
                         select ppt).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(CheckList))
                {
                    result =
                       ((from aw in context.CheckList
                        where entityIdList.Contains(aw.ID) && aw.Account == accountId
                        select aw.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(CheckListItem))
                {
                    result =
                       ((from ap in context.CheckListItem
                        join aw in context.CheckList on ap.CheckList equals aw.ID
                        where entityIdList.Contains(ap.ID) && aw.Account == accountId
                        select ap.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if(typeof(T) == typeof(PredefinedTagword))
                {
                    result =
                        ((from pt in context.PredefinedTagwords
                          where entityIdList.Contains(pt.ID) && pt.Account == accountId
                          select pt.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else if (typeof(T) == typeof(FileTransfer))
                {
                    result =
                       ((from f in context.FileTransfers
                         where entityIdList.Contains(f.ID) && f.AccountID == accountId
                         select f.ID).Count()) == entityIdList.Count() ? true : false;
                }
                else
                {
                    throw new NotImplementedException("Method IsFromCurrentAccount was not implemeted for type " +
                                                      typeof(T).ToString());
                }
               
                return result;
            }
            finally
            {
            }
        }

        public static bool IsFromCurrentAccountTree<T>(IEnumerable<int> entityIdList, int accountId, GMGColorContext context)
           where T : BusinessObjectBase
        {
            bool result = false;
            try
            {
                if (typeof(T) == typeof(Account))
                {
                    List<int> list = new List<int>();
                    var childAccountTreeList = GetAccountChildrenTree(list, accountId, context);
                    result = childAccountTreeList.Contains(entityIdList.FirstOrDefault());
                }
                else
                {
                    throw new NotImplementedException("Method IsFromCurrentAccountTree was not implemeted for type " +
                                                      typeof(T).ToString());
                }
                return result;
            }
            finally
            {
            }
        }

        public static List<int> GetAccountChildrenTree(List<int> accountChildren, int loggedAccountId, GMGColorContext context)
        {
            var children = (from a in context.Accounts
                          where a.Parent == loggedAccountId
                          select a.ID).ToList();
            if (children.Count == 0) return accountChildren;

            accountChildren.AddRange(children);
            foreach (var id in children)
            {
                GetAccountChildrenTree(accountChildren, id, context);
            }
            return accountChildren;
        }

        public static string GetDbEntityValidationException(DbEntityValidationException e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var eve in e.EntityValidationErrors)
            {
                sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                eve.Entry.Entity.GetType().Name,
                                                eve.Entry.State));
                foreach (var ve in eve.ValidationErrors)
                {
                    sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                ve.PropertyName,
                                                ve.ErrorMessage));
                }
            }
            return sb.ToString();
        }

        public static void LogDbEntityValidationException(DbEntityValidationException e)
        {
            string errorMessage = GetDbEntityValidationException(e);
            GMGColorLogging.log.Error(errorMessage, e);
            throw e;
        }

        public static bool ContainsWord(this string obj, string word)
        {
            return (obj.Split(new string[] { " ", "-" }, StringSplitOptions.RemoveEmptyEntries)).Any(o => o.Contains(word));
        }

        /// <summary>
        /// Returns the event group name based on event type key
        /// </summary>
        /// <param name="grouppingEventTypeKey"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetEventGroupName(int grouppingEventTypeID, GMGColorContext context)
        {
            string eventGroupName;

            switch (EventGroup.GetEventGroup(grouppingEventTypeID, context))
                {
                    case EventGroup.GroupName.Comments:
                        eventGroupName = Resources.lblNotificationEventGroup_Comments;
                        break;
                    case EventGroup.GroupName.Approvals:
                        eventGroupName = Resources.lblNotificationEventGroup_Approvals;
                        break;
                    case EventGroup.GroupName.Users:
                        eventGroupName = Resources.lblNotificationEventGroup_Users;
                        break;
                    case EventGroup.GroupName.Groups:
                        eventGroupName = Resources.lblNotificationEventGroup_Groups;
                        break;
                    case EventGroup.GroupName.Account:
                        eventGroupName = Resources.lblAccount;
                        break;                   
                    case EventGroup.GroupName.Deliver:
                        eventGroupName = Resources.lblDeliver;
                        break;
                    case EventGroup.GroupName.InvitedUser:
                        eventGroupName = Resources.lblInvitedUser;
                        break;
                    case EventGroup.GroupName.HostAdmin:
                        eventGroupName = Resources.lblHostAdmin;
                        break;                    
                    default:
                        eventGroupName = string.Empty;
                        break;
                }

            return eventGroupName;
        }

        public static string GetEventGroupName(string grouppingEventTypeKey)
        {
            string eventGroupName;

            switch (EventGroup.GetEventGroup(grouppingEventTypeKey))
            {
                case EventGroup.GroupName.Comments:
                    eventGroupName = Resources.lblNotificationEventGroup_Comments;
                    break;
                case EventGroup.GroupName.Approvals:
                    eventGroupName = Resources.lblNotificationEventGroup_Approvals;
                    break;
                case EventGroup.GroupName.Users:
                    eventGroupName = Resources.lblNotificationEventGroup_Users;
                    break;
                case EventGroup.GroupName.Groups:
                    eventGroupName = Resources.lblNotificationEventGroup_Groups;
                    break;
                case EventGroup.GroupName.Account:
                    eventGroupName = Resources.lblAccount;
                    break;              
                case EventGroup.GroupName.Deliver:
                    eventGroupName = Resources.lblDeliver;
                    break;
                case EventGroup.GroupName.InvitedUser:
                    eventGroupName = Resources.lblInvitedUser;
                    break;
                case EventGroup.GroupName.HostAdmin:
                    eventGroupName = Resources.lblHostAdmin;
                    break;              
                default:
                    eventGroupName = string.Empty;
                    break;
            }

            return eventGroupName;
        }
         
        /// <summary>
        /// Returns event type name bsaed on event type key
        /// </summary>
        /// <param name="eventTypeKey">Event type key</param>
        /// <returns></returns>
        public static string GetEventTypeName(string eventTypeKey)
        {
            string eventTypeName;

            switch (EventType.GetTypeEnum(eventTypeKey))
            {
                case EventType.TypeEnum.When_a_comment_is_made:
                    eventTypeName = Resources.lblNotification_WhenACommentIsMade;
                    break;
                case EventType.TypeEnum.Replies_to_my_comments:
                    eventTypeName = Resources.lblNotification_RepliesToMyComments;
                    break;
                case EventType.TypeEnum.New_comments_made:
                    eventTypeName = Resources.lblNotification_NewCommentsMade;
                    break;
                case EventType.TypeEnum.New_approvals_added:
                    eventTypeName = Resources.lblNotification_NewApprovalsAdded;
                    break;
                case EventType.TypeEnum.Approvals_was_downloaded:
                    eventTypeName = Resources.lblNotification_ApprovalsWasDownloaded;
                    break;
                case EventType.TypeEnum.Approvals_was_updated:
                    eventTypeName = Resources.lblNotification_Approvals_was_updated;
                    break;
                case EventType.TypeEnum.Approvals_status_changed:
                    eventTypeName = Resources.lblNotifitation_StatusChanged;
                    break;
                case EventType.TypeEnum.ApprovalStatusChangedByPdm:
                    eventTypeName = Resources.lblNotification_ApprovalStatusChangedByPdm;
                    break;
                case EventType.TypeEnum.Workstation_Registered:
                    eventTypeName = Resources.lblNotification_WorkstationRegistered;
                    break;
                case EventType.TypeEnum.Approvals_was_deleted:
                    eventTypeName = Resources.lblNotification_ApprovalsWasDeleted;
                    break;
                case EventType.TypeEnum.New_version_was_created:
                    eventTypeName = Resources.lblNotification_NewVersionWasCreated;
                    break;
                case EventType.TypeEnum.Approval_was_shared:
                    eventTypeName = Resources.lblNotification_ApprovalWasShared;
                    break;
                case EventType.TypeEnum.User_was_added:
                    eventTypeName = Resources.lblNotification_UserWasAdded;
                    break;
                case EventType.TypeEnum.Group_was_created:
                    eventTypeName = Resources.lblNotification_GroupWasCreated;
                    break;
                case EventType.TypeEnum.Plan_was_changed:
                    eventTypeName = Resources.lblNotification_PlanWasChanged;
                    break;
                case EventType.TypeEnum.Account_was_deleted:
                    eventTypeName = Resources.lblNotification_AccountWasDeleted;
                    break;
                case EventType.TypeEnum.Account_was_added:
                    eventTypeName = Resources.lblNotification_AccountWasAdded;
                    break;               
                case EventType.TypeEnum.Deliver_File_Submitted:
                    eventTypeName = Resources.lblNotification_FileSubmitted;
                    break;
                case EventType.TypeEnum.Deliver_File_Completed:
                    eventTypeName = Resources.lblNotification_FileCompleted;
                    break;
                case EventType.TypeEnum.Deliver_File_Errored:
                    eventTypeName = Resources.lblNotification_FileError;
                    break;
                case EventType.TypeEnum.Deliver_File_Timeout:
                    eventTypeName = Resources.lblNotification_FileTimeout;
                    break;
                case EventType.TypeEnum.Access_Revoked:
                    eventTypeName = Resources.lblNotification_InvitedUserAccessRevoked;
                    break;
                case EventType.TypeEnum.Invited_User_Deleted:
                    eventTypeName = Resources.lblNotification_InvitedUserDeleted;
                    break;
                case EventType.TypeEnum.Invited_User_File_Submitted:
                    eventTypeName = Resources.lblNotification_FileSubmitted;
                    break;
                case EventType.TypeEnum.Share_Request_Accepted:
                    eventTypeName = Resources.lblNotification_ShareRequestAccepted;
                    break;
                case EventType.TypeEnum.Shared_Workflow_Deleted:
                    eventTypeName = Resources.lblNotification_SharedWorkflowDeleted;
                    break;
                case EventType.TypeEnum.Host_Admin_User_Deleted:
                    eventTypeName = Resources.lblNotification_HostAdminUserDeleted;
                    break;
                case EventType.TypeEnum.Host_Admin_User_File_Submitted:
                    eventTypeName = Resources.lblNotification_FileSubmitted;
                    break;               
                case EventType.TypeEnum.ApprovalChangesComplete:
                    eventTypeName = Resources.lblNotification_ApprovalChangesComplete;
                    break;
                case EventType.TypeEnum.ApprovalWasRejected:
                    eventTypeName = Resources.lblNotification_ApprovalWasRejected;
                    break;
                case EventType.TypeEnum.ApprovalOverdue:
                    eventTypeName = Resources.lblNotification_ApprovalOverdue;
                    break;
                case EventType.TypeEnum.PhaseWasDeactivated:
                    eventTypeName = Resources.lblNotification_PhaseWasDeactivated;
                    break;
                case EventType.TypeEnum.PhaseWasActivated:
                    eventTypeName = Resources.lblNotification_PhaseWasActivated;
                    break;
                case EventType.TypeEnum.WorkflowPhaseRerun:
                    eventTypeName = Resources.lblNotification_WorkflowPhaseRerun;
                    break;
                case EventType.TypeEnum.ApprovalJobComplete:
                    eventTypeName = Resources.lblNotification_All_Decisions_Completed;
                    break;
                case EventType.TypeEnum.NewProjectFolderAdded:
                    eventTypeName = Resources.lblNotification_New_ProjectFolder;
                    break;
                case EventType.TypeEnum.ProjectFolderWasShared:
                    eventTypeName = Resources.lblNotification_ProjectFolder_was_shared;
                    break;
                case EventType.TypeEnum.ProjectFolderWasUpdated:
                    eventTypeName = Resources.lblNotification_ProjectFolder_was_updated;
                    break;

                default:
                    eventTypeName = string.Empty;
                    break;
            }
            return eventTypeName;
        }

        public static string GetEventTypeDescription(string eventTypeKey)
        {
            string eventTypeDescription;

            switch (EventType.GetTypeEnum(eventTypeKey))
            {
                case EventType.TypeEnum.When_a_comment_is_made:
                    eventTypeDescription = Resources.lblDescription_WhenACommentIsMade;
                    break;
                case EventType.TypeEnum.Replies_to_my_comments:
                    eventTypeDescription = Resources.lblDescription_RepliesToMyComments;
                    break;
                case EventType.TypeEnum.New_comments_made:
                    eventTypeDescription = Resources.lblDescription_NewCommentsMade;
                    break;
                case EventType.TypeEnum.New_approvals_added:
                    eventTypeDescription = Resources.lblDescription_NewApprovalsAdded;
                    break;
                case EventType.TypeEnum.Approvals_was_downloaded:
                    eventTypeDescription = Resources.lblDescription_ApprovalsWasDownloaded;
                    break;
                case EventType.TypeEnum.Approvals_was_updated:
                    eventTypeDescription = Resources.lblDescription_Approvals_was_updated;
                    break;
                case EventType.TypeEnum.Approvals_status_changed:
                    eventTypeDescription = Resources.lblDescription_StatusChanged;
                    break;
                case EventType.TypeEnum.ApprovalStatusChangedByPdm:
                    eventTypeDescription = Resources.lblDescription_ApprovalStatusChangedByPdm;
                    break;
                case EventType.TypeEnum.Workstation_Registered:
                    eventTypeDescription = Resources.lblDescription_WorkstationRegistered;
                    break;
                case EventType.TypeEnum.Approvals_was_deleted:
                    eventTypeDescription = Resources.lblDescription_ApprovalsWasDeleted;
                    break;
                case EventType.TypeEnum.New_version_was_created:
                    eventTypeDescription = Resources.lblDescription_NewVersionWasCreated;
                    break;
                case EventType.TypeEnum.Approval_was_shared:
                    eventTypeDescription = Resources.lblDescription_ApprovalWasShared;
                    break;
                case EventType.TypeEnum.ApprovalChangesComplete:
                    eventTypeDescription = Resources.lblDescription_ApprovalChangesComplete;
                    break;
                case EventType.TypeEnum.ApprovalWasRejected:
                    eventTypeDescription = Resources.lblDescription_ApprovalWasRejected;
                    break;
                case EventType.TypeEnum.ApprovalOverdue:
                    eventTypeDescription = Resources.lblNotification_ApprovalOverdue;
                    break;
                case EventType.TypeEnum.PhaseWasDeactivated:
                    eventTypeDescription = Resources.lblDescription_PhaseWasDeactivated;
                    break;
                case EventType.TypeEnum.PhaseWasActivated:
                    eventTypeDescription = Resources.lblDescription_PhaseWasActivated;
                    break;
                case EventType.TypeEnum.WorkflowPhaseRerun:
                    eventTypeDescription = Resources.lblDescription_WorkflowPhaseRerun;
                    break;
                case EventType.TypeEnum.ApprovalJobComplete:
                    eventTypeDescription = Resources.lblDescription_All_Decisions_Completed;
                    break;

                default:
                    eventTypeDescription = string.Empty;
                    break;
            }
            return eventTypeDescription;
        }
    }
}
