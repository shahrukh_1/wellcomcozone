﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GMGColorDAL
{
    public class DatabaseMaintenance
    {
        private static readonly TimeSpan RecreateIndexesTimeout = TimeSpan.FromHours(168);

        private Timer _recreateIndexesTimer;

        public void Start()
        {
            _recreateIndexesTimer = new Timer(RecreateIndexesTimerCallback, null, TimeSpan.Zero, RecreateIndexesTimeout);
        }

        #region Recreate Indexes Work

        private void RecreateIndexesTimerCallback(object state)
        {
            _recreateIndexesTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        #endregion
    }
}
