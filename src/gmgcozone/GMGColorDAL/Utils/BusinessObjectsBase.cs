﻿using System;
using System.Data.Entity;
using System.Web;

namespace GMGColorDAL
{
    [Serializable]
    public class BusinessObjectBase
    {
        public int ID { get; set; }
    }
}
