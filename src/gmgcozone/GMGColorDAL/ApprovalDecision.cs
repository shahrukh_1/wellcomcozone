//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GMGColorDAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApprovalDecision
    {
        public ApprovalDecision()
        {
            this.ApprovalCollaboratorDecisions = new HashSet<ApprovalCollaboratorDecision>();
            this.ApprovalCustomDecisions = new HashSet<ApprovalCustomDecision>();
            this.InactiveApprovalStatuses = new HashSet<InactiveApprovalStatus>();
            this.ApprovalJobPhases = new HashSet<ApprovalJobPhase>();
            this.ApprovalPhases = new HashSet<ApprovalPhase>();
        }
    
        public int ID { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public Nullable<int> Priority { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<ApprovalCollaboratorDecision> ApprovalCollaboratorDecisions { get; set; }
        public virtual ICollection<ApprovalCustomDecision> ApprovalCustomDecisions { get; set; }
        public virtual ICollection<InactiveApprovalStatus> InactiveApprovalStatuses { get; set; }
        public virtual ICollection<ApprovalJobPhase> ApprovalJobPhases { get; set; }
        public virtual ICollection<ApprovalPhase> ApprovalPhases { get; set; }
    }
}
