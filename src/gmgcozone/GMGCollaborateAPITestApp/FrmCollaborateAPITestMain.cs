﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Reflection;
using System.Collections;
using GMG.CollaborateAPI.BL.Models.FolderModels;
using GMGCollaborateAPITestApp.Models;
using System.Xml;

namespace GMGCollaborateAPITestApp
{
    enum DeadlineType
    {
    
    }

    public partial class FrmCollaborateAPITestMain : Form
    {
        #region Members

        private string _username;
        private int _phaseNumber = 1;
        private BindingList<UserModel> _users = new BindingList<UserModel>();
        private BindingList<GroupName> _groups = new BindingList<GroupName>();
        private BindingList<UserModel> _annListusers = new BindingList<UserModel>();
        private BindingList<GroupName> _annListgroups = new BindingList<GroupName>();
		private BindingList<Collaborator> _collaborators = new BindingList<Collaborator>(); 
        private BindingList<Collaborator> _updateCollaborators = new BindingList<Collaborator>();
        private BindingList<UserGroup> _userGroups = new BindingList<UserGroup>();
        private BindingList<ModulePermission> _permissions = new BindingList<ModulePermission>();
        private BindingList<WorkflowCollaborator> _collaboratorsToAdd = new BindingList<WorkflowCollaborator>();
        private List<TabContent> _phaseList = new List<TabContent>();
        System.Xml.XmlDocument _xmlDoc = new System.Xml.XmlDocument();

        private bool _isApprovalDecisionChanged;
        private bool _isJobStatusChanged;

        private const string _FIXEDDATELINE = "Fix date";
        private const string _NODEADLINE = "No deadline";
        private const string _AUTOMATICDEADLINE = "Automatic phase deadline";
        #region Logs

        public string _logTxt = String.Empty;

        #endregion

        #endregion

        public void LogResponse(ref string logger, object response, string message, bool addToLog = true, string tabs = "")
        {
            String responseLog = tabs + message + Environment.NewLine;
            if(addToLog)
            {
                responseLog += "Response:" + Environment.NewLine;
            }
            PropertyInfo[] props = response.GetType().GetProperties();            
            foreach (PropertyInfo prop in props)
            {
                if (prop.PropertyType == typeof(System.Runtime.Serialization.ExtensionDataObject))
                    continue;

                if ((typeof(string) != prop.PropertyType) && typeof(IEnumerable).IsAssignableFrom(prop.PropertyType))
                {
                    var propery = prop.GetValue(response,null);
                    responseLog += tabs + "\t" + prop.Name;
                    if (propery is IEnumerable)
                    {
                        foreach (var listitem in propery as IEnumerable)
                        {
                            string objAsStr = Environment.NewLine;
                            LogResponse(ref objAsStr, listitem, listitem.GetType().Name, false, tabs + "\t\t");
                            responseLog += objAsStr;
                        }
                    }
                    
                    responseLog += Environment.NewLine;
                    continue;
                }

                object value = response.GetType().GetProperty(prop.Name).GetValue(response, null);
                if (value != null)
                {
                    responseLog += tabs + "\t" + prop.Name + ": " + value.ToString() + Environment.NewLine;
                }
            }
            responseLog = responseLog.TrimEnd(new char[] { '\r', '\n' });

            if (!addToLog)
            {
                logger += responseLog;
            }
            else
            {
                UpdateLog(ref logger, responseLog);
            }
        }
 
        private void UpdateLog(ref string log, string message, bool newEntry = true, bool logLineBreak = true)
        {
            if (newEntry)
            {
                if (!String.IsNullOrEmpty(log))
                    log += Environment.NewLine;
                log += DateTime.Now.ToString("H:mm:ss") + ": " + message;
            }

            if (logLineBreak)
            {
                log += Environment.NewLine + "*********";
            }

            txtLog.Text = log;
            txtLog.SelectionStart = txtLog.Text.Length;
            txtLog.ScrollToCaret();
        }

        public FrmCollaborateAPITestMain()
        {
            InitializeComponent();
            if (System.Diagnostics.Debugger.IsAttached)
            {
                txtUserName.Text = "client";
                txtAccountURL.Text = "client.gmgcozonelocal.com";
            }
            InitControls();
        }

        private void InitControls()
        {
            PopulateServicesURL();

            //AnnotationReport
            dataGridViewUsers.DataSource = _users;
            dataGridViewUsers.AllowUserToAddRows = true;
            dataGridViewGroups.DataSource = _groups;
            dataGridViewGroups.AllowUserToAddRows = true;
            cbxDisplayOpt.SelectedIndex = 0;
            cbxFilterBy.SelectedIndex = 0;
            cmbSortBy.SelectedIndex = 0;
            txtCmtsPerPage.Text = "10";

            //AllVersionsAnnotationReport
            dataGridViewAllVersionsReportUsers.DataSource = _users;
            dataGridViewAllVersionsReportUsers.AllowUserToAddRows = true;
            dataGridViewAllVersionsReportGroups.DataSource = _groups;
            dataGridViewAllVersionsReportGroups.AllowUserToAddRows = true;
            cbxDisplayOptJobAnnotationReport.SelectedIndex = 0;
            cbxFilterByJobAnnotationReport.SelectedIndex = 0;
            cbxSortByJobAnnotationReport.SelectedIndex = 0;
            txtCommentsPerPageJobAnnotationReport.Text = "10";

            //Create Job
            cbxAllowToDownload.Checked = true;
            dataGridViewCollaborators.DataSource = _collaborators;
            dataGridViewCollaborators.Columns["toBeRemoved"].Visible = false;
            dataGridViewCollaborators.AllowUserToAddRows = true;
            btnCreateVersion.Enabled = false;
            chbAllowUsersToBeAdded.Enabled = false;
           
            //Annotation List
            dataGridViewAnnotationListUsers.DataSource = _annListusers;
            dataGridViewAnnotationListUsers.AllowUserToAddRows = true;
            dataGridViewAnnotationListGroups.DataSource = _annListgroups;
            dataGridViewAnnotationListGroups.AllowUserToAddRows = true;
            cbxFilterByAnnList.SelectedIndex = 0;
            cbxSortByAnnList.SelectedIndex = 0;

            //UpdateJob
            cbxJobStatUpdJob.SelectedIndex = 0;

            //UpdateApproval
            dataGridViewUpdateCollaborators.DataSource = _updateCollaborators;
            dataGridViewUpdateCollaborators.Columns["workflowPhaseGUID"].Visible = false;
            dataGridViewUpdateCollaborators.AllowUserToAddRows = true;
            cbxLoggedUserDecision.SelectedIndex = 0;

            //Create user
            dataGridViewPermissions.DataSource = _permissions;
            dataGridViewPermissions.AllowUserToAddRows = true;
            dataGridViewUserGroups.DataSource = _userGroups;
            dataGridViewUserGroups.AllowUserToAddRows = true;

            //Create Workflow
            TabContent firstPhase = new TabContent();
            firstPhase.Dock = DockStyle.Fill;
            _phaseList.Add(firstPhase);
            tabPhasesControl.SelectedTab.Controls.Add(firstPhase);

            //Add Collaborators to Phase
            dataGridViewAddCollabToPhase.DataSource = _collaboratorsToAdd;
            dataGridViewAddCollabToPhase.AllowUserToAddRows = true;
        }

        private void PopulateServicesURL()
        {
            cmbServicesURL.Items.Add("http://app.gmgcozonelocal.com/api/v1/");
            cmbServicesURL.Items.Add("https://app.staging.wellcomapproval.com/api/v1/");
            cmbServicesURL.Items.Add("https://app.wellcomapproval.com/api/v1/");
            cmbServicesURL.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                string startsessionURL = cmbServicesURL.Text + "startsession";

                var collaborateApiClient = new CollaborateAPIClient(startsessionURL, string.Empty);

                var result = collaborateApiClient.StartSessionRequest(txtUserName.Text, txtAccountURL.Text);

                if (result != null)
                {
                    string message = "Start session for user " + _username + " succeeded";
                    LogResponse(ref _logTxt, result, message);

                    if (System.Diagnostics.Debugger.IsAttached)
                    {
                        txtSessionKeyForRestoreApp.Text =
                        txtSessionKeyForCreateJob.Text =
                        txtSessionKeyForListJobs.Text =
                        txtSessionKeyDeleteApp.Text =
                        txtSessionKeyForAnnReport.Text =
                        txtStatusJobSessionKey.Text =
                        txtDeleteSessionKey.Text = 
                        txtDisplayJobSessionKey.Text =
                        txtSessionKeyAnnotationList.Text =
                        txtUpdateApprovalSessionKey.Text =
                        txtSessionKeyForGetApp.Text =
                        txtProofStudioSessionKey.Text =
                        txtSessionKeyCreateFolder.Text =
                        txtSessionKeyGetAllFolders.Text = 
                        txtListApprovalWorkflowsSessionKey.Text =
                        txtSessionKeyForCheckUser.Text =
                        txtSessionKeyCreateUser.Text =
                        txtSessionKeyCreateExternalUser.Text =
                        txtGetWorkflowDetailsSessionKey.Text = 
                        txtApprovalDecisionSessionKey.Text =
                        txtSessionKeGetFolderByName.Text =
                        txtSessionKeyForConfirm.Text = 
                        txtRerunWorkflowSessionKey.Text =
                        txtCreateWorflowSessionKey.Text = 
						txtAddCollabToPhaseSessionKey.Text =
                        txtCompareVersionsSessionKey.Text = 
                        txtSessionKeyJobAnnotationReport.Text = result.sessionKey;
                    }
                }
                else
                {
                    UpdateLog(ref _logTxt, "Startsession for user " + _username + " failed");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on start sess", MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
            }
        }

        private void btnConfirmSession_Click(object sender, EventArgs e)
        {
            try
            {
                string confirmsessionURL = cmbServicesURL.Text + "confirmidentity";

                var collaborateApiClient = new CollaborateAPIClient(confirmsessionURL, txtSessionKeyForConfirm.Text, string.Empty);
                collaborateApiClient.ConfirmSessionRequest(txtConfirmSesPass.Text);

                var result = collaborateApiClient.ConfirmSessionRequest(txtConfirmSesPass.Text);

                if (result != null)
                {
                    string message = "Confirm session for user " + _username + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Confirm session for user " + _username + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on confirm sess", MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
            }
        }

        private void tabControlCollaborateAPI_SelectedIndexChanged(object sender, EventArgs e)
        {
            int currentIndex = tabControlCollaborateAPI.SelectedIndex;
            pictureBox1.Visible = false;
        }

        private void btnDeleteJob_Click(object sender, EventArgs e)
        {
            try
            {
                string deleteJobURL = cmbServicesURL.Text + "job/" + txtDeleteJobKey.Text;

                var collaborateApiClient = new CollaborateAPIClient(deleteJobURL, txtDeleteSessionKey.Text, txtDeleteJobUsername.Text.Trim());
                var result = collaborateApiClient.DeleteJobRequest();

                if (result != null)
                {
                    string message = "Delete job " + txtDeleteJobKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Delete job " + txtDeleteJobKey.Text + " failed");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on delete job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnDisplayJob_Click(object sender, EventArgs e)
        {
            try
            {
                string getJobURL = cmbServicesURL.Text + "job/" + txtDisplayJobKey.Text;

                var collaborateApiClient = new CollaborateAPIClient(getJobURL, txtDisplayJobSessionKey.Text, txtDisplayJobUsername.Text.Trim());

                var result = collaborateApiClient.GetJobRequest();

                if (result != null)
                {
                    string message = "Display job " + txtDisplayJobKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Display job " + txtDisplayJobKey.Text + " failed");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving job details", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnStatusJob_Click(object sender, EventArgs e)
        {
            try
            {
                string getJobStatusURL = cmbServicesURL.Text + "job/" + txtStatusJobKey.Text + "/status";
                var collaborateApiClient = new CollaborateAPIClient(getJobStatusURL, txtStatusJobSessionKey.Text, txtStatusJobUsername.Text.Trim());

                var result = collaborateApiClient.GetJobStatusRequest();

                if (result != null)
                {
                    string message = "Get job status " + txtStatusJobKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get job status" + txtStatusJobKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving job status", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnApprovalStatus_Click(object sender, EventArgs e)
        {
            try
            {
                string getJobStatusURL = cmbServicesURL.Text + "approval/" + txtStatusJobKey.Text + "/status";

                var collaborateApiClient = new CollaborateAPIClient(getJobStatusURL, txtStatusJobSessionKey.Text, txtStatusJobUsername.Text.Trim());
                var result = collaborateApiClient.GetApprovalStatusRequest();

                if (result != null)
                {
                    string message = "Get approval status " + txtStatusJobKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get approval status" + txtStatusJobKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving approval status", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnFileOpen_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = openFileDialogForCreateJob.ShowDialog(this);
            if (dialogResult == DialogResult.OK)
            {
                txtFileName.Text = Path.GetFileName(openFileDialogForCreateJob.FileName);
                txtJobName.Text = Path.GetFileNameWithoutExtension(txtFileName.Text);
            }
            else
            {
                txtFileName.Text = string.Empty;
                txtJobName.Text = string.Empty;
            }
        }

        private void btnCreateJob_Click(object sender, EventArgs e)
        {
            try
            {
                string createJobMetadataURL = cmbServicesURL.Text + "job";

                var colalborateApiClient = new CollaborateAPIClient(createJobMetadataURL, txtCreateJobUsername.Text.Trim());

                var createMetadataResult = colalborateApiClient.CreateJobMetadataRequest(PopulateApprovalRequest(), txtJobName.Text, txtWorkflowGuid.Text.Trim(), txtChecklistGuid.Text.Trim(), chbAllowUsersToBeAdded.Checked);

                CreateJobResponse result = null;
                if (createMetadataResult != null)
                {
                    result = UploadFile(createMetadataResult.resourceID);
                }

                if (result != null)
                {
                    string message = "Create job " + txtSessionKeyForCreateJob.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Create job " + txtSessionKeyForCreateJob.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occur: " + ex.Message, "Error on create job", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private CreateJobResponse UploadFile(string resourceID)
        {
            var uploadFileUrl = cmbServicesURL.Text + "upload-file";

            var collaborateApiClient = new CollaborateAPIClient(uploadFileUrl, txtSessionKeyForCreateJob.Text.Trim(), txtCreateJobUsername.Text.Trim());

            return collaborateApiClient.UploadFileRequest(resourceID, openFileDialogForCreateJob.FileName);
        }

        private CreateApprovalRequest PopulateApprovalRequest()
        {
            string fileType;
           
            fileType = Path.GetExtension(openFileDialogForCreateJob.FileName);
                         
            var settings = new Settings()
            {
                allowUsersToDownloadOriginalFile = cbxAllowToDownload.Checked,
                lockProofWhenAllDecisionsAreMade = cbxLockWhenAllDecisonAreMade.Checked,
                lockProofWhenFirstDecisionIsMade = cbxLockWhenFirstDecisonMade.Checked,
                onlyOneDecisionRequired = cbxOneDecionRequired.Checked
            };         

      
            var collaborators = _collaborators.Select(collaborator => new Collaborator()
                {
                    username = collaborator.username,
                    email = collaborator.email,
                    approvalRole = collaborator.approvalRole,
                    isExternal = collaborator.isExternal,
                    pdm = collaborator.pdm,
                    workflowPhaseGUID = collaborator.workflowPhaseGUID,
                    lastname = collaborator.lastname,
                    firstname = collaborator.firstname
                }).ToList();
             
            var approval = new CreateApprovalRequest
                {
                    deadline = deadline.Value.ToString("o"),
                    fileName = txtFileName.Text,
                    fileType = fileType,
                    folder = txtFolder.Text.Trim(),
                    collaborators = collaborators,
                    sessionKey = txtSessionKeyForCreateJob.Text.Trim(),
                    settings = settings,    
                    username = txtCreateJobUsername.Text.Trim()
                };
         
            approval.phases = PopulatePhases(_collaborators);
  
            return approval;
        }

        private void btnListJobs_Click(object sender, EventArgs e)
        {
            try
            {
                string listJobsURL = cmbServicesURL.Text + "job";
                var collaborateApiClient = new CollaborateAPIClient(listJobsURL, txtSessionKeyForListJobs.Text, txtListJobsUsername.Text.Trim());

                var result = collaborateApiClient.ListJobsRequest();

                if (result != null)
                {
                    string message = "List jobs " + txtSessionKeyForListJobs.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "List jobs " + txtSessionKeyForListJobs.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on delete job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnGetReport_Click(object sender, EventArgs e)
        {
            try
            {
                string getReportURL = cmbServicesURL.Text + "approval/" + txtJobKeyForAnnReport.Text + "/report";

                var queryParams = GetReportFilters();
                var collaborateApiClient = new CollaborateAPIClient(getReportURL, txtGetAnnoReportUsername.Text.Trim());

                var result = collaborateApiClient.GetAnnotationReportRequest(queryParams);

                if (result != null)
                {
                    string message = "Get annotation Report " + txtJobKeyForAnnReport.Text + " succeeded";
                    UpdateLog(ref _logTxt, message);
                                     
                    Image image = Image.FromStream(result);
                    
                    pictureBox1.Image = image;
                    button1.Enabled = true;
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get annotation Report " + txtJobKeyForAnnReport.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on delete job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private NameValueCollection GetReportFilters()
        {
            var queryParams = new NameValueCollection
            {
                {"sessionKey", txtSessionKeyForAnnReport.Text},
                {"nrOfCommentsPerPage", txtCmtsPerPage.Text},
                {"sortBy", cmbSortBy.Text},
                {"displayOptions", cbxDisplayOpt.Text},
                {"filterBy", cbxFilterBy.Text},
                {"includeSummaryPage", checkSumaryPage.Checked.ToString()},
                {"username", txtGetAnnoReportUsername.Text.Trim()},
                {"hasAccess", "true"}

            };

            for (int i = 0; i < _users.Count; i++)
            {
                queryParams.Add(String.Format("users[{0}].username", i), _users[i].username);
                queryParams.Add(String.Format("users[{0}].isExternal", i), _users[i].isExternal.ToString());
            }

            for (int i = 0; i < _groups.Count; i++)
            {
                queryParams.Add(String.Format("groups[{0}]", i), _groups[i].Name);
            }

            if (!string.IsNullOrEmpty(txtPhases.Text))
            {
                var phases = txtPhases.Text.Split(',').ToList();
                for (int i = 0; i < phases.Count; i++)
                {
                    queryParams.Add(String.Format("phases[{0}]", i), phases[i].Trim());
                }
            }

            return queryParams;
        }

        private NameValueCollection GetJobAnnotationReportFilters()
        {
            var queryParams = new NameValueCollection
            {
                {"sessionKey", txtSessionKeyJobAnnotationReport.Text},
                {"nrOfCommentsPerPage", txtCommentsPerPageJobAnnotationReport.Text},
                {"sortBy", cbxSortByJobAnnotationReport.Text},
                {"displayOptions", cbxDisplayOptJobAnnotationReport.Text},
                {"filterBy", cbxFilterByJobAnnotationReport.Text},
                {"includeSummaryPage", ckbSumaryPageJobAnnotationReport.Checked.ToString()},
                {"username", txtUserNameJobAnnotationReport.Text.Trim()},
                {"hasAccess", "true"}
            };

            for (int i = 0; i < _users.Count; i++)
            {
                queryParams.Add(String.Format("users[{0}].username", i), _users[i].username);
                queryParams.Add(String.Format("users[{0}].isExternal", i), _users[i].isExternal.ToString());
            }

            for (int i = 0; i < _groups.Count; i++)
            {
                queryParams.Add(String.Format("groups[{0}]", i), _groups[i].Name);
            }

            return queryParams;
        }

        private void btnGetAnnotationList_Click(object sender, EventArgs e)
        {
            try
            {
                string getAnnListURL = cmbServicesURL.Text + "approval/" + txtApprovalKeyAnnotationList.Text + "/annotationlist";

                NameValueCollection queryParams = new NameValueCollection();
                queryParams.Add("sessionKey", txtSessionKeyAnnotationList.Text);
                queryParams.Add("sortBy", cbxSortByAnnList.Text);
                queryParams.Add("filterBy", cbxFilterByAnnList.Text);
                queryParams.Add("username", txtGetAnnoListUsername.Text.Trim());
            
                for (int i = 0; i < _annListusers.Count; i++)
                {
                    queryParams.Add(String.Format("users[{0}].username", i), _annListusers[i].username);
                    queryParams.Add(String.Format("users[{0}].isExternal", i), _annListusers[i].isExternal.ToString());
                }
                for (int i = 0; i < _annListgroups.Count; i++)
                {
                    queryParams.Add(String.Format("groups[{0}]", i), _annListgroups[i].Name);
                }

                var collaborateApiClient = new CollaborateAPIClient(getAnnListURL, txtGetAnnoListUsername.Text.Trim());
                var result = collaborateApiClient.GetAnnotationListRequest(queryParams);

                if (result != null)
                {
                    string message = "Get annotation List " + txtApprovalKeyAnnotationList.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get annotation List " + txtApprovalKeyAnnotationList.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on get annotation list", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnDeleteApproval_Click(object sender, EventArgs e)
        {
            try
            {
                string deleteApprovalURL = cmbServicesURL.Text + "approval/" + txtDeleteAppKey.Text;

                var collaborateApiClient = new CollaborateAPIClient(deleteApprovalURL, txtSessionKeyDeleteApp.Text, txtDeleteApprovalUsername.Text.Trim());

                var result = collaborateApiClient.DeleteApprovalRequest();

                if (result != null)
                {
                    string message = "Delete approval " + txtSessionKeyForCreateJob.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Delete job " + txtSessionKeyForCreateJob.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on delete job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnRestoreApproval_Click(object sender, EventArgs e)
        {
            try
            {
                string restoreApprovalURL = cmbServicesURL.Text + "approval/" + txtApprovalRestoreKey.Text + "/restore";

                var collaborateApiClient = new CollaborateAPIClient(restoreApprovalURL, txtSessionKeyForRestoreApp.Text, txtRestoreAppUsername.Text.Trim());

                var result = collaborateApiClient.RestoreApprovalRequest();

                if (result != null)
                {
                    string message = "Restore approval " + txtApprovalRestoreKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Restore job " + txtApprovalRestoreKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on restore job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnUpdateJob_Click(object sender, EventArgs e)
        {
            try
            {
                string updateJob = cmbServicesURL.Text + "job/" + jobKeyForUpdJob.Text;

                var body = new UpdateJobRequest();
                body.jobTitle = txtJobTitleUpdJob.Text;

                if (cbxJobStatUpdJob.Text != "None")
                {
                    switch (cbxJobStatUpdJob.Text)
                    {
                        case "In Progress":
                            body.jobStatus = "INP";
                            break;
                        case "Changes Required":
                            body.jobStatus = "CRQ";
                            break;
                        case "Changes Complete":
                            body.jobStatus = "CCM";
                            break;
                        case "Completed":
                            body.jobStatus = "CM";
                            break;
                        case "Archived":
                            body.jobStatus = "ARC";
                            break;
                    }
                }

                var collaborateApiClient = new CollaborateAPIClient(updateJob, txtSessionKeyForUpdJob.Text, txtUpdateJobUsername.Text.Trim());
                var result = collaborateApiClient.UpdateJobRequest(body);

                if (result != null)
                {
                    string message = "Update job " + jobKeyForUpdJob.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);

                    if (cbxTestPushJobStatus.Checked)
                    {
                        var getStatusUrl = cmbServicesURL.Text + "getstatus";
                        collaborateApiClient = new CollaborateAPIClient(getStatusUrl,
                            txtApprovalDecisionSessionKey.Text, txtGetWorkflowDetailsUsername.Text.Trim());
                        var statusResponse = collaborateApiClient.GetStatus();
                        if (statusResponse != null)
                        {
                            LogResponse(ref _logTxt, statusResponse, "Get status succeeded");
                        }
                    }
                }
                else
                {
                    UpdateLog(ref _logTxt, "Update job " + jobKeyForUpdJob.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Update job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnGetApp_Click(object sender, EventArgs e)
        {
            try
            {
                string getApproval = cmbServicesURL.Text + "approval/" + txtGetAppForAppGuid.Text;

                var collaborateApiClient = new CollaborateAPIClient(getApproval, txtSessionKeyForGetApp.Text, txtGetApprovalUsername.Text.Trim());

                var result = collaborateApiClient.GetApprovalRequest();

                if (result != null)
                {
                    string message = "Get approval " + txtGetAppForAppGuid.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);

                    byte[] binaryData = Convert.FromBase64String(result.thumbnail);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(binaryData))
                    {
                        image = Image.FromStream(ms);
                    }

                    pictBxGetApp.Image = image;
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get job " + txtGetAppForAppGuid.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Get job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnGetAppAccess_Click(object sender, EventArgs e)
        {
            try
            {
                string getApproval = cmbServicesURL.Text + "approval/" + txtGetAppForAppGuid.Text + "/access";

                var collaborateApiClient = new CollaborateAPIClient(getApproval, txtSessionKeyForGetApp.Text, txtGetApprovalUsername.Text.Trim());

                var result = collaborateApiClient.GetApprovalWithAccessRequest();

                if (result != null)
                {
                    string message = "Get approval with access " + txtGetAppForAppGuid.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);

                    byte[] binaryData = Convert.FromBase64String(result.thumbnail);

                    Image image;
                    using (MemoryStream ms = new MemoryStream(binaryData))
                    {
                        image = Image.FromStream(ms);
                    }

                    pictBxGetApp.Image = image;
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get approval with access " + txtGetAppForAppGuid.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Get job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            txtLog.Clear();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = string.Format("Export-{0}.txt", DateTime.UtcNow.ToString("dd-MM-yyyy-hh-mm-ss"));
            sfd.Filter = "Text File | *.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(sfd.FileName))
                {
                    sw.Write(txtLog.Text);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;
        }

        private void btnCreateVersion_Click(object sender, EventArgs e)
        {
            try
            {
                string createVersionURL = cmbServicesURL.Text + "job/" + txtJobKey.Text + "/approval";

                var collaborateApi = new CollaborateAPIClient(createVersionURL, txtCreateJobUsername.Text.Trim());

                var request = PopulateApprovalRequest();
                request.guid = txtJobKey.Text;

                var createMetadataResult = collaborateApi.CreateJobMetadataRequest(request, null, null,null, false);

                var result = UploadFile(createMetadataResult.resourceID);
                
                if (result != null)
                {
                    string message = "Create approval version " + txtDeleteJobKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Create approval version " + txtDeleteJobKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occur: " + ex.Message, "Error on create job", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void txtJobKey_TextChanged(object sender, EventArgs e)
        {
            TextBox box = (TextBox)sender;
            btnCreateVersion.Enabled = !string.IsNullOrEmpty(box.Text);
            btnCreateJob.Enabled = string.IsNullOrEmpty(box.Text);

            if (!string.IsNullOrEmpty(box.Text))
            {
                txtWorkflowGuid.Enabled = false;
                txtWorkflowGuid.Text = "";
                chbAllowUsersToBeAdded.Checked = false;
                chbAllowUsersToBeAdded.Enabled = false;
            }
            else
            {
                txtWorkflowGuid.Enabled = true;
                chbAllowUsersToBeAdded.Enabled = true;
            }
        }

        private void txtJobKey_KeyUp(object sender, KeyEventArgs e)
        {
            btnCreateVersion.Enabled = !string.IsNullOrEmpty(txtJobKey.Text);
            btnCreateJob.Enabled = string.IsNullOrEmpty(txtJobKey.Text);
        }

        private void btnUpdateApproval_Click(object sender, EventArgs e)
        {
            try
            {
                string updateJob = cmbServicesURL.Text + "approval/" + txtUpdateApprovalKey.Text;
            
                var body = new UpdateApprovalRequest
                {

                    username = txtUpdateApprovalUsername.Text.Trim(),

                    guid = txtUpdateApprovalKey.Text.Trim(),
                    sessionKey = txtUpdateApprovalSessionKey.Text.Trim(),
                    deadline = updateApprovalDeadline.Value.ToString("o"),
                    collaborators = _updateCollaborators.Select(collaborator => new Collaborator()
                    {
                        username = collaborator.username,
                        email = collaborator.email,
                        approvalRole = collaborator.approvalRole,
                        isExternal = collaborator.isExternal,
                        pdm = collaborator.pdm,
                        firstname = collaborator.firstname,
                        lastname = collaborator.lastname,
                        toBeRemoved = collaborator.toBeRemoved
                    }).ToList()
                };


                var collaborateApiClient = new CollaborateAPIClient(updateJob, txtUpdateApprovalUsername.Text.Trim());

                var result = collaborateApiClient.UpdateApprovalRequest(body);

                if (result != null)
                {
                    string message = "Update approval " + txtUpdateApprovalKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Update approval " + txtUpdateApprovalKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Update job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }
        private void btnUpdateApprovalPhaseDeadline_Click(object sender, EventArgs e)
        {
            try
            {
                string updateApprvalPhaseDeadline = cmbServicesURL.Text + "approvalPhaseDeadline/" + txtUpdateApprovalPhaseDeadlineKey.Text;

                var body = new UpdateApprovalPhaseDeadlineRequest
                {
                    username = txtUpdateApprovalPhaseDeadlineUsername.Text.Trim(),
                    guid = txtUpdateApprovalPhaseDeadlineKey.Text.Trim(),
                    approvalPhaseGuid = txtUpdateAPD_ApprovalPhaseKey.Text.Trim(),
                    sessionKey = txtUpdateApprovalPhaseDeadlineSessionKey.Text.Trim(),
                    deadline = updateApprovalPhaseDeadline.Value.ToString("o"),
                };


                var collaborateApiClient = new CollaborateAPIClient(updateApprvalPhaseDeadline, txtUpdateApprovalPhaseDeadlineUsername.Text.Trim());

                var result = collaborateApiClient.UpdateApprovalPhaseDeadlineRequest(body);

                if (result != null)
                {
                    string message = "Update approval phase deadline " + txtUpdateApprovalPhaseDeadlineKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Update approval Phase Deadline" + txtUpdateApprovalKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Update Phase Deadline", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnGetProofStudioURL_Click(object sender, EventArgs e)
        {
            try
            {
                string getProofStudioURL = cmbServicesURL.Text + "approval/" + txtProofStudioApprovalKey.Text + "/proofstudio";

                var collaborateApiClient = new CollaborateAPIClient(getProofStudioURL, txtProofStudioSessionKey.Text, txtGetProofStudioURLUsername.Text.Trim());

                var result = collaborateApiClient.GetProofStudioURLRequest();

                if (result != null)
                {
                    string message = "Get ProofStudio URL for " + txtProofStudioApprovalKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get ProofStudio URL for " + txtProofStudioApprovalKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Get job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnCreateNewFolder_Click(object sender, EventArgs e)
        {
            try
            {
                string createFolderURL = cmbServicesURL.Text + "folder";

                var requestBody = new CreateFolderRequest()
                {
                    sessionKey = txtSessionKeyCreateFolder.Text.Trim(),
                    name = txtFolderName.Text.Trim(),
                    parentGuid = txtParentFolderGuid.Text.Trim(),
                    username = txtCreateFolderUsername.Text.Trim()
                };

                var collaborateApi = new CollaborateAPIClient(createFolderURL, txtSessionKeyCreateFolder.Text, txtCreateFolderUsername.Text.Trim());

                var result = collaborateApi.CreateFolderRequest(requestBody);

                if (result != null)
                {
                    string message = "Create folder succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Create folder failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occur: " + ex.Message, "Error on create folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetAllFolders_Click(object sender, EventArgs e)
        {
            try
            {
                string getAllFoldersURL = cmbServicesURL.Text + "folders";
                var collaborateApiClient = new CollaborateAPIClient(getAllFoldersURL, txtSessionKeyGetAllFolders.Text, txtGetAllFoldersUsername.Text.Trim());

                var result = collaborateApiClient.GetAllFolders();

                if (result != null)
                {
                    string message = "Get all folders succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get all folders failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving all folders", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnListApprovalWorkflows_Click(object sender, EventArgs e)
        {
            try
            {
                string getApprovalWorkflowsURL = cmbServicesURL.Text + "workflow";
                var collaborateApiClient = new CollaborateAPIClient(getApprovalWorkflowsURL, txtListApprovalWorkflowsSessionKey.Text, txtListApprovalWorkflowUsername.Text.Trim());

                var result = collaborateApiClient.GetAllApprovalWorkflows();

                if (result != null)
                {
                    string message = "Get all approval workflows succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get all approval workflows failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving all folders", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void cbxLoggedUserDecision_SelectedIndexChanged(object sender, EventArgs e)
        {
            _isApprovalDecisionChanged = cbxLoggedUserDecision.SelectedIndex != 0;
        }

        private void cbxJobStatUpdJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            _isJobStatusChanged = cbxJobStatUpdJob.SelectedIndex != 0;
        }

        private void txtUpdateApprovalKey_TextChanged(object sender, EventArgs e)
        {
            cbxLoggedUserDecision.SelectedIndex = 0;
        }
        private void txtUpdateApprovalPhaseDeadlineKey_TextChanged(object sender, EventArgs e)
        {
            cbxLoggedUserDecision.SelectedIndex = 0;
        }
        private void txtUpdateAPD_ApprovalPhaseKey_TextChanged(object sender, EventArgs e)
        {
            cbxLoggedUserDecision.SelectedIndex = 0;
        }
        

        private void jobKeyForUpdJob_TextChanged(object sender, EventArgs e)
        {
            cbxJobStatUpdJob.SelectedIndex = 0;
        }

        private void btnGetWorkflowDetails_Click(object sender, EventArgs e)
        {
            try
            {
                string getApprovalWorkflowURL = cmbServicesURL.Text + "workflow/" + txtApprovalWorkflowKey.Text;

                var collaborateApiClient = new CollaborateAPIClient(getApprovalWorkflowURL, txtGetWorkflowDetailsSessionKey.Text, txtGetWorkflowDetailsUsername.Text.Trim());

                var result = collaborateApiClient.GetApprovalWorkflowDetails();

                if (result != null)
                {
                    string message = "Display approval workflow details " + txtApprovalWorkflowKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Display approval workflow details " + txtApprovalWorkflowKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving approval workflow details", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnCreateExternaluser_Click(object sender, EventArgs e)
        {
            try
            {
                string createExternalUserURL = cmbServicesURL.Text + "external";

                var collaborateApiClient = new CollaborateAPIClient(createExternalUserURL, txtSessionKeyCreateExternalUser.Text, string.Empty);

                var userReq = new CreateExternalUserRequest
                {
                    sessionKey = txtSessionKeyCreateExternalUser.Text,
                    Email = txtExternalEmail.Text,
                    FirstName = txtExternalUserFirstName.Text,
                    LastName = txtExternalUserLastName.Text
                };

                var result = collaborateApiClient.CreateExternalUserRequest(userReq);

                if (result != null)
                {
                    LogResponse(ref _logTxt, result, "Create user succeeded");
                }
                else
                {
                    UpdateLog(ref _logTxt, "Create user failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Create user", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnCreateuser_Click(object sender, EventArgs e)
        {
            try
            {
                string createUserURL = cmbServicesURL.Text + "user";

                var collaborateApiClient = new CollaborateAPIClient(createUserURL, txtSessionKeyCreateUser.Text, string.Empty);

                var userReq = new CreateUserRequest
                {
                    sessionKey = txtSessionKeyCreateUser.Text,
                    Email = txtEmail.Text,
                    FirstName = txtUserFirstName.Text,
                    LastName = txtUserLastName.Text,
                    Password = txtUserPassword.Text,
                    IsSSOUser = chkIsSSOUser.Checked,
                    PrivateAnnotations = chkPrivateAnnotations.Checked,
                    Groups = new List<UserGroup>()
                };

                foreach (var group in _userGroups)
                {
                    userReq.Groups.Add(new UserGroup
                    {
                        isprimary = group.isprimary,
                        name = group.name
                    });
                }

                userReq.Permissions = new List<ModulePermission>();
                foreach (var perm in _permissions)
                {
                    userReq.Permissions.Add(new ModulePermission
                    {
                        name = perm.name,
                        role = perm.role
                    });

                }

                var result = collaborateApiClient.CreateUserRequest(userReq);

                if (result != null)
                {
                    LogResponse(ref _logTxt, result, "Create user succeeded");
                }
                else
                {
                    UpdateLog(ref _logTxt, "Create user failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Create user", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void btnUpdateApprovalDecision_Click(object sender, EventArgs e)
        {
            try {
                string updateDecisionApprovalURL = cmbServicesURL.Text + "approval/" + txtApprovalDecisionApprovalKey.Text + "/decision";

                var requestBody = new UpdateApprovalDecisionRequest()
                {
                    sessionKey = txtApprovalDecisionSessionKey.Text.Trim(),
                    guid = txtApprovalDecisionApprovalKey.Text.Trim(),
                    email = txtApprovalDecisionExternalEmail.Text.Trim(),
                    username = txtApprovalDecisionUsername.Text.Trim()
                };

                if (cbxLoggedUserDecision.Text != "None")
                {
                    switch (cbxLoggedUserDecision.Text)
                    {
                        case "Pending":
                            requestBody.decision = "PDG";
                            break;
                        case "Changes Required":
                            requestBody.decision = "CHR";
                            break;
                        case "Approved with Changes":
                            requestBody.decision = "AWC";
                            break;
                        case "Approved":
                            requestBody.decision = "APD";
                            break;
                        case "Not Applicable":
                            requestBody.decision = "NAP";
                            break;
                    }
                }

                var collaborateApi = new CollaborateAPIClient(updateDecisionApprovalURL, txtApprovalDecisionSessionKey.Text, txtApprovalDecisionUsername.Text.Trim());

                var result = collaborateApi.UpdateApprovalDecision(requestBody);

                if (result != null)
                {
                    string message = "Update approval decision succeeded";
                    LogResponse(ref _logTxt, result, message);

                    if (cbxTestPushApprovalStatus.Checked)
                    {
                        var getStatusUrl = cmbServicesURL.Text + "getstatus";
                        collaborateApi = new CollaborateAPIClient(getStatusUrl, txtApprovalDecisionSessionKey.Text, txtGetWorkflowDetailsUsername.Text.Trim());
                        var statusResponse = collaborateApi.GetStatus();
                        if (statusResponse != null)
                        {
                            LogResponse(ref _logTxt, statusResponse, "Get status succeeded");
                        } 
                    }
                }
                else
                {
                    UpdateLog(ref _logTxt, "Update approval decision failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occur: " + ex.Message, "Error on create folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCheckUser_Click(object sender, EventArgs e)
        {
            try
            {
                string checkUserExistsUrl = cmbServicesURL.Text + "user/exists";

                var collaborateApiClient = new CollaborateAPIClient(checkUserExistsUrl, txtSessionKeyForCheckUser.Text, string.Empty);

                var userReq = new UserExistsRequest
                {
                    email = txtEmailForCheckUser.Text
                };

                var result = collaborateApiClient.CheckIfUserExistsRequest(userReq);

                if (result != null)
                {
                    LogResponse(ref _logTxt, result, "Check if user exists succeeded");
                }
                else
                {
                    UpdateLog(ref _logTxt, "Check user failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Check user", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }

        private void txtWorkflowGuid_TextChanged(object sender, EventArgs e)
        {
            TextBox control = (TextBox) sender;
            chbAllowUsersToBeAdded.Enabled = !string.IsNullOrEmpty(control.Text);
        }

        private List<Phase> PopulatePhases(BindingList<Collaborator> _collaborators)
        {
            List<Phase> phases = new List<Phase>();

            foreach (var collaborator in _collaborators.GroupBy(c => c.workflowPhaseGUID).Select(c => c.First()))
            {
                Phase phase = new Phase()
                {
                    phaseGUID = collaborator.workflowPhaseGUID,
                    collaborators = _collaborators.Where(c => c.workflowPhaseGUID == collaborator.workflowPhaseGUID).ToList()
                };

                phases.Add(phase);
            }

            return phases;
        }

        private void btnGetAnnotationReportURL_Click(object sender, EventArgs e)
        {
            try
            {
                string getReportURL = cmbServicesURL.Text + "approval/" + txtJobKeyForAnnReport.Text + "/reporturl";

                var queryParams = GetReportFilters();
                var collaborateApiClient = new CollaborateAPIClient(getReportURL, txtGetAnnoReportUsername.Text.Trim());

                var result = collaborateApiClient.GetAnnotationReportUrlRequest(queryParams);

                if (result != null)
                {
                    string message = "Get annotation Report URL " + txtJobKeyForAnnReport.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get annotation Report URL " + txtJobKeyForAnnReport.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on creating annotation report URL", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetJobAnnotationReportURL_Click(object sender, EventArgs e)
        {
            try
            {
                string getReportURL = cmbServicesURL.Text + "job/" + txtJobKeyJobAnnotationReport.Text + "/reporturl";

                var queryParams = GetJobAnnotationReportFilters();
                var collaborateApiClient = new CollaborateAPIClient(getReportURL, txtUserNameJobAnnotationReport.Text.Trim());

                var result = collaborateApiClient.GetAnnotationReportUrlRequest(queryParams);

                if (result != null)
                {
                    string message = "Get job annotation Report URL " + txtJobKeyForAnnReport.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get job annotation Report URL " + txtJobKeyForAnnReport.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on creating annotation report URL", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGetFolderByName_Click(object sender, EventArgs e)
        {
            try
            {
                string getFolderByNameURL = cmbServicesURL.Text + "folder";

                var collaborateApiClient = new CollaborateAPIClient(getFolderByNameURL, txtSessionKeGetFolderByName.Text, txtGetFolderByNameUsername.Text);
                var result = collaborateApiClient.GetFolderByName(txtFolderByName.Text);

                if (result != null)
                {
                    string message = "Get folder for " + txtFolderByName.Text + " name succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get folder for " + txtFolderByName.Text + " name failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving folder by name", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRerunWorkflow_Click(object sender, EventArgs e)
        {
            try
            {
                var requestBody = new RerunFromThisPhaseRequest()
                {
                    sessionKey = txtRerunWorkflowSessionKey.Text,
                    guid = txtRerunWorkflowApprovalGUID.Text,
                    phaseGuid = txtRerunWorkflowPhaseGUID.Text,
                    username = txtRerunWorkflowUserName.Text
                };
                string rerunApprovalWorkflowUrl = cmbServicesURL.Text + "approval/" + txtRerunWorkflowApprovalGUID.Text + "/rerunworkflow"; 

                var collaborateApiClient = new CollaborateAPIClient(rerunApprovalWorkflowUrl, txtRerunWorkflowSessionKey.Text, txtRerunWorkflowUserName.Text);
                var result = collaborateApiClient.RerunApprovalWorklowFromPhase(requestBody);
                
                if (result != null)
                {
                    string message = "Rerun Approval Workflow with GUID: " + txtRerunWorkflowApprovalGUID.Text + " to phase: " + txtRerunWorkflowPhaseGUID.Text + " rerun succeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Rerun Approval Workflow with GUID: " + txtRerunWorkflowApprovalGUID.Text + " to phase: " + txtRerunWorkflowPhaseGUID.Text + " rerun failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on reseting approval workflow" , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmCollaborateAPITestMain_Load(object sender, EventArgs e)
        {

        }

        private void btnAddNewPhase_Click(object sender, EventArgs e)
        {
            AddNewPhaseTab();
        }

        private void AddNewPhaseTab()
        {
            //Create the new page and tab content instances
            TabPage newTabPage = new TabPage();
            TabContent newTabContent = new TabContent();

            //Update phase name 
            _phaseNumber++;
            newTabPage.Text = "Phase_" + _phaseNumber;

            //Add the content to the new page and set it as active
            newTabPage.Controls.Add(newTabContent);
            tabPhasesControl.TabPages.Add(newTabPage);
            tabPhasesControl.SelectedTab = newTabPage;
            ActiveControl = newTabPage;

            //Add the new phase tab to the phase list
            _phaseList.Add(newTabContent);
        }

        private void btnSaveWorkflow_Click(object sender, EventArgs e)
        {
            try
            {
                string createApprovalWorkflowURL = cmbServicesURL.Text + "workflow";

                var requestBody = new CreateApprovalWorkflowRequest()
                {
                    sessionKey = txtCreateWorflowSessionKey.Text,
                    username = txtCreateWorkflowUserName.Text, 
                    workflowName = txtWorkflowName.Text,
                    rerunWorkflow = chbRerunWorkflow.Checked
                };
                PopulateWorflowPhasesList(requestBody.worflowPhases);

                var collaborateApiClient = new CollaborateAPIClient(createApprovalWorkflowURL, txtCreateWorflowSessionKey.Text, txtCreateWorkflowUserName.Text);
                var result = (CreateApprovalWorkflowResponse)collaborateApiClient.CreateApprovalWorkflow(requestBody);

                if (result != null)
                {
                    string message = "Approval Workflow was created with " + result.workflowGuid + " Guid";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Approval workflow creation failed ");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs" + ex.Message, "Error creating a new approval", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAddCollabToPhaseSave_Click(object sender, EventArgs e)
        {
            try
            {
                string addApprovalPhaseCollaboratorsURL = cmbServicesURL.Text  + "approval/" + txtAddCollabToPhaseAppGuid.Text + "/addphasecollaborators";

                var requestBody = new AddCollaboratorsToApprovalPhaseRequest()
                {
                    sessionKey = txtAddCollabToPhaseSessionKey.Text,
                    username = txtAddCollabToPhaseUserName.Text,
                    guid = txtAddCollabToPhaseAppGuid.Text,
                    phaseGuid = txtAddCollabToPhasePhaseGuid.Text,
                    phaseCollaborators = new List<PhaseCollaboratorsRequest>()
                };

                PopulateApprovalPhaseCollaboratorsListToAdd(requestBody.phaseCollaborators);

                var collaborateApiClient = new CollaborateAPIClient(addApprovalPhaseCollaboratorsURL, txtAddCollabToPhaseSessionKey.Text, txtAddCollabToPhaseUserName.Text);
                var result = (AddCollaboratorsToApprovalPhaseResponse)collaborateApiClient.AddCollaboratorsToApprovalPhase(requestBody);

                if (result != null && !string.IsNullOrEmpty(result.guid))
                {
                    string message = "Collaborators successfully added to approval phase";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Collaborators failed to be to approval phase");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs" + ex.Message, "Error adding collaborator/s", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PopulateApprovalPhaseCollaboratorsListToAdd(List<PhaseCollaboratorsRequest> collaboratorsToAdd)
        {
            foreach (var collaborator in _collaboratorsToAdd)
            {
                if (!string.IsNullOrEmpty(collaborator.username) || !string.IsNullOrEmpty(collaborator.email))
                {
                    collaboratorsToAdd.Add(new PhaseCollaboratorsRequest()
                    {
                        firstName = collaborator.firstname,
                        lastName = collaborator.lastname,
                        email = collaborator.email,
                        userName = collaborator.username,
                        userRole = collaborator.approvalRole,
                        isExternal = collaborator.isExternal
                    });
                }
            }
        }

        private void PopulateWorflowPhasesList(List<PhaseRequest> workflowPhases)
        {
            foreach (var phase in _phaseList)
            {
                var phaseToAdd = new PhaseRequest();

                phaseToAdd.phaseName = phase.Controls.Find("phaseName", true)[0].Text;
                phaseToAdd.showAnnotationsToUsersOfOtherPhases = ((CheckBox)phase.Controls.Find("chkShowAnnotationOfThisPhaseToUsersOfOtherPhases", true)[0]).Checked;
                phaseToAdd.showAnnotationsOfOtherPhasesToExternalUsers = ((CheckBox)phase.Controls.Find("chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases", true)[0]).Checked;

                var selectedDeadLineType = phase.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                if (selectedDeadLineType.Text == _FIXEDDATELINE)
                {
                    var deadLineDate = ((DateTimePicker)phase.Controls.Find("workflowDeadLineDate", true)[0]).Value.ToString("o");
                    phaseToAdd.deadLineDate = deadLineDate.Substring(0, deadLineDate.IndexOf("T"));
                    phaseToAdd.deadLineTime = ((DateTimePicker)phase.Controls.Find("workflowDeadLineTime", true)[0]).Value.Hour.ToString() +
                                                ":" +
                                              ((DateTimePicker)phase.Controls.Find("workflowDeadLineTime", true)[0]).Value.Minute.ToString();
                    phaseToAdd.deadLineMeridian = ((ComboBox)phase.Controls.Find("workflowDeadLineMeridian", true)[0]).SelectedItem.ToString();
                }
                else if(selectedDeadLineType.Text == _AUTOMATICDEADLINE)
                {
                    phaseToAdd.automaticDeadLineValue = (int?)((NumericUpDown)phase.Controls.Find("automaticDeadlineDaysHoursNumber", true)[0]).Value;
                    phaseToAdd.automaticDeadLineTrigger = ((ComboBox)phase.Controls.Find("automaticDeadlineDaysHoursDropDown", true)[0]).SelectedIndex;
                    phaseToAdd.automaticDeadLineUnit = ((ComboBox)phase.Controls.Find("automaticDeadLineType", true)[0]).SelectedIndex;
                }

                PopulateWorflowPhaseCollaboratorsList(phaseToAdd.phaseCollaborators, phase);

                phaseToAdd.selectedDecisionType = GetDecisionTypeKey(phase);
                phaseToAdd.decisionMaker = phase.Controls.Find("pdm", true)[0].Text;
                phaseToAdd.approvalTrigger = GetApprovalTriggerKey(phase);
                phaseToAdd.approvalShouldBeViewedByAllCollaborators = ((CheckBox)phase.Controls.Find("chkApprovalShouldBeViewedByAllCollaborators", true)[0]).Checked;

                workflowPhases.Add(phaseToAdd);
            }
 
        }

        private string GetDecisionTypeKey(TabContent phaseTabContent)
        {
            string key = "";
            if (phaseTabContent.Controls.Find("decisionType", true)[0].Text != "None")
            {
                switch (phaseTabContent.Controls.Find("decisionType", true)[0].Text)
                {
                    case "Primary Decision Maker":
                        key = "PDM";
                        break;
                    case "Approval Group":
                        key = "AG";
                        break;
                }
            }
            return key;
        }

        private string GetApprovalTriggerKey(TabContent phaseTabContent)
        {
            string key = "";

            if (phaseTabContent.Controls.Find("approvalTrigger", true)[0].Text != "None")
            {
                switch (phaseTabContent.Controls.Find("approvalTrigger", true)[0].Text)
                {
                    case "Approved":
                        key = "APD";
                        break;
                    case "Approved With Changes":
                        key = "AWC";
                        break;
                }
            }
            return key;
        }

        private void PopulateWorflowPhaseCollaboratorsList(List<PhaseCollaboratorsRequest> workflowPhaseCollaborators, TabContent phase)
        {
            var phaseCollaborators = phase._phaseCollaborators.Select(collab => new PhaseCollaboratorsRequest()
            {
                email = collab.email,
                firstName = collab.firstname,
                lastName = collab.lastname,
                userRole = collab.approvalRole,
                isExternal = collab.isExternal,
                userName = collab.username
            }).ToList();
            workflowPhaseCollaborators.AddRange(phaseCollaborators);
        }

        private void btnLoadWorkflowFromFile_Click(object sender, EventArgs e)
        {
            if (openFileDialogForLoadingWorkflowDetails.ShowDialog() == DialogResult.OK)
            {
                _xmlDoc.Load(openFileDialogForLoadingWorkflowDetails.FileName);

                //Populate workflow details
                txtWorkflowName.Text = _xmlDoc.GetElementsByTagName("WorkflowName")[0].InnerText;
                txtCreateWorkflowUserName.Text = _xmlDoc.GetElementsByTagName("UserName")[0].InnerText;

                //Populate phase details
                tabPhasesControl.TabPages.Clear();
                _phaseList.Select(c => c._phaseCollaborators).ToList().Clear();
                _phaseList.Clear();
                _phaseNumber = 0;

                var phaseList = _xmlDoc.GetElementsByTagName("Phase");

                foreach (System.Xml.XmlNode phase in phaseList)
                {
                    AddNewPhaseTab();

                    _phaseList[_phaseNumber - 1].Controls.Find("phaseName", true)[0].Text = phase.SelectNodes("PhaseName")[0].InnerText;
                    ((CheckBox)_phaseList[_phaseNumber - 1].Controls.Find("chkShowAnnotationOfThisPhaseToUsersOfOtherPhases", true)[0]).Checked = Convert.ToBoolean(phase.SelectNodes("ShowAnnotationOfThisPhaseToUsersOfOtherPhases")[0].InnerText);
                    ((CheckBox)_phaseList[_phaseNumber - 1].Controls.Find("chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases", true)[0]).Checked = Convert.ToBoolean(phase.SelectNodes("ShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases")[0].InnerText);

                    switch (phase.SelectNodes(".//DeadlineType")[0].InnerText)
                    {
                        case "No Deadline":
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineFixedDate", true)[0]).Checked = false;
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineNoDeadline", true)[0]).Checked = true;
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineAutomatic", true)[0]).Checked = false;
                            break;
                        case "Fixed Date":

                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineFixedDate", true)[0]).Checked = true;
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineNoDeadline", true)[0]).Checked = false;
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineAutomatic", true)[0]).Checked = false;

                            ((DateTimePicker)_phaseList[_phaseNumber - 1].Controls.Find("workflowDeadLineDate", true)[0]).Checked = true;
                            ((DateTimePicker)_phaseList[_phaseNumber - 1].Controls.Find("workflowDeadLineDate", true)[0]).Value = DateTime.Parse(phase.SelectNodes(".//DeadlineDate")[0].InnerText);
                            
                            _phaseList[_phaseNumber - 1].Controls.Find("workflowDeadLineTime", true)[0].Text = phase.SelectNodes(".//DeadlineTime")[0].InnerText;
                            _phaseList[_phaseNumber - 1].Controls.Find("workflowDeadLineMeridian", true)[0].Text = phase.SelectNodes(".//DeadlineMeridian")[0].InnerText;
                            
                            break;
                        case "Automatic Phase Deadline":
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineFixedDate", true)[0]).Checked = false;
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineNoDeadline", true)[0]).Checked = false;
                            ((RadioButton)_phaseList[_phaseNumber - 1].Controls.Find("rdoDeadLineAutomatic", true)[0]).Checked = true;

                            ((NumericUpDown)_phaseList[_phaseNumber - 1].Controls.Find("automaticDeadlineDaysHoursNumber", true)[0]).Value = Convert.ToDecimal(phase.SelectNodes(".//DeadLineDaysHoursValue")[0].InnerText);
                            var deadLineDaysHoursIndex = ((ComboBox)_phaseList[_phaseNumber - 1].Controls.Find("automaticDeadlineDaysHoursDropDown", true)[0]).FindString(phase.SelectNodes(".//DeadLineDaysHoursMode")[0].InnerText);
                            ((ComboBox)_phaseList[_phaseNumber - 1].Controls.Find("automaticDeadlineDaysHoursDropDown", true)[0]).SelectedIndex = deadLineDaysHoursIndex + 1;

                            var deadLineTypeIndex = ((ComboBox)_phaseList[_phaseNumber - 1].Controls.Find("automaticDeadLineType", true)[0]).FindString(phase.SelectNodes(".//DeadLineDaysHoursType")[0].InnerText);
                            ((ComboBox)_phaseList[_phaseNumber - 1].Controls.Find("automaticDeadLineType", true)[0]).SelectedIndex = deadLineTypeIndex + 1;
                            break;
                    }

                    foreach (var collab in GetPhaseCollaboratorsFromXml(phase.SelectNodes(".//Collaborator")))
                    {
                        _phaseList[_phaseNumber - 1]._phaseCollaborators.Add(collab);
                    }

                    _phaseList[_phaseNumber - 1].Controls.Find("decisionType", true)[0].Text = phase.SelectSingleNode("DecisionTypeKey").InnerText == "PDM" ? "Primary Decision Maker" : "Approval Group";
                    _phaseList[_phaseNumber - 1].Controls.Find("pdm", true)[0].Text = phase.SelectSingleNode("DecisionMaker").InnerText;
                    _phaseList[_phaseNumber - 1].Controls.Find("approvalTrigger", true)[0].Text = phase.SelectSingleNode("ApprovalTriggerKey").InnerText == "APD" ? "Approved" : "Approved With Changes";
                    ((CheckBox)_phaseList[_phaseNumber - 1].Controls.Find("chkApprovalShouldBeViewedByAllCollaborators", true)[0]).Checked = Convert.ToBoolean(phase.SelectNodes("//ApprovalShouldBeViewedByAllCollaborators")[0].InnerText);
                }
            }
        }

        private void btnCompareVersionsSend_Click(object sender, EventArgs e)
        {
            try
            {
                string compareVersionsURL = cmbServicesURL.Text + "compareVersions";

                var collaborateApiClient = new CollaborateAPIClient(compareVersionsURL, txtCompareVersionsSessionKey.Text, txtCompareVersionsUsername.Text.Trim());
             
                var queryParams = new NameValueCollection
                {
                    {"sessionKey", txtCompareVersionsSessionKey.Text},               
                    {"username", txtCompareVersionsUsername.Text.Trim()},
                    {"versions[0]", txtCompareVersionsV1.Text.Trim()},
                    {"versions[1]", txtCompareVersionsV2.Text.Trim()}
                };

                var result = collaborateApiClient.GetCompareVersionsURL(queryParams);

                if (result != null)
                {
                    string message = "Get ProofStudio URL for " + txtProofStudioApprovalKey.Text + " succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get ProofStudio URL for " + txtProofStudioApprovalKey.Text + " failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on Get job", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }
        private void btnAddCollabToPhaseLoadFromFile_Click(object sender, EventArgs e)
        {
            if (openFileDialogForLoadingNewPhaseCollabs.ShowDialog() == DialogResult.OK)
            {
                _xmlDoc.Load(openFileDialogForLoadingNewPhaseCollabs.FileName);

                //Approval and Phase Details
                tabAddCollaboratorToPhase.Controls.Find("txtAddCollabToPhaseUserName", true)[0].Text = _xmlDoc.GetElementsByTagName("Owner")[0].InnerText;
                tabAddCollaboratorToPhase.Controls.Find("txtAddCollabToPhaseAppGuid", true)[0].Text = _xmlDoc.GetElementsByTagName("ApprovalGuid")[0].InnerText;
                tabAddCollaboratorToPhase.Controls.Find("txtAddCollabToPhasePhaseGuid", true)[0].Text = _xmlDoc.GetElementsByTagName("PhaseGuid")[0].InnerText;

                foreach (var collab in GetPhaseCollaboratorsFromXml(_xmlDoc.SelectNodes(".//Collaborator")))
                {
                    _collaboratorsToAdd.Add(collab);
                }
            }
        }

        private List<WorkflowCollaborator> GetPhaseCollaboratorsFromXml(XmlNodeList xmlCollaborators)
        {
            var collaboratorList = new List<WorkflowCollaborator>();
            foreach (XmlNode collaborator in xmlCollaborators)
            {
                collaboratorList.Add(new WorkflowCollaborator {
                    firstname = collaborator.SelectNodes("FirstName")[0].InnerText,
                    lastname = collaborator.SelectNodes("LastName")[0].InnerText,
                    username = collaborator.SelectNodes("UserName")[0].InnerText,
                    email = collaborator.SelectNodes("Email")[0].InnerText,
                    approvalRole = collaborator.SelectNodes("UserRoleKey")[0].InnerText,
                    isExternal = Convert.ToBoolean(collaborator.SelectNodes("IsExternal")[0].InnerText)
                });
            }
            return collaboratorList;
        }

        private void btnGetChecklist_Click(object sender, EventArgs e)
        {
            try
            {
                string getChecklistURL = cmbServicesURL.Text + "checklist";

                var collaborateApiClient = new CollaborateAPIClient(getChecklistURL, txtGetChecklistSessionKey.Text, txtGetChecklistUsername.Text.Trim());

                var result = collaborateApiClient.GetChecklistDetails();

                if (result != null)
                {
                    string message = "Get all checklist succeeded";
                    LogResponse(ref _logTxt, result, message);
                }
                else
                {
                    UpdateLog(ref _logTxt, "Get all checklist failed");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurs: " + ex.Message, "Error on retrieving checklist details", MessageBoxButtons.OK,
                              MessageBoxIcon.Error);
            }
        }
    }
}
