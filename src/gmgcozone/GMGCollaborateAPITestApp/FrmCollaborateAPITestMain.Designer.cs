﻿namespace GMGCollaborateAPITestApp
{
    partial class FrmCollaborateAPITestMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCollaborateAPITestMain));
            this.tabControlCollaborateAPI = new System.Windows.Forms.TabControl();
            this.tabPageStartSession = new System.Windows.Forms.TabPage();
            this.btnStart = new System.Windows.Forms.Button();
            this.txtAccountURL = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.lblAccountURL = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.tabConfirmSes = new System.Windows.Forms.TabPage();
            this.txtConfirmSesPass = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnConfirmSession = new System.Windows.Forms.Button();
            this.txtSessionKeyForConfirm = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabListJobs = new System.Windows.Forms.TabPage();
            this.txtListJobsUsername = new System.Windows.Forms.TextBox();
            this.lblListJobsUsername = new System.Windows.Forms.Label();
            this.btnListJobs = new System.Windows.Forms.Button();
            this.txtSessionKeyForListJobs = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tabDisplayJob = new System.Windows.Forms.TabPage();
            this.txtDisplayJobUsername = new System.Windows.Forms.TextBox();
            this.lblDisplayJobUsername = new System.Windows.Forms.Label();
            this.btnDisplayJob = new System.Windows.Forms.Button();
            this.txtDisplayJobKey = new System.Windows.Forms.TextBox();
            this.txtDisplayJobSessionKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPageCreateJob = new System.Windows.Forms.TabPage();
            this.txtCreateJobUsername = new System.Windows.Forms.TextBox();
            this.lblCreateJobUsername = new System.Windows.Forms.Label();
            this.lblApprovalWorkflowSetting = new System.Windows.Forms.Label();
            this.chbAllowUsersToBeAdded = new System.Windows.Forms.CheckBox();
            this.txtWorkflowGuid = new System.Windows.Forms.TextBox();
            this.lblWorkflowGuid = new System.Windows.Forms.Label();
            this.txtJobKey = new System.Windows.Forms.TextBox();
            this.lblJobKey = new System.Windows.Forms.Label();
            this.btnCreateVersion = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.deadline = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewCollaborators = new System.Windows.Forms.DataGridView();
            this.label25 = new System.Windows.Forms.Label();
            this.cbxAllowToDownload = new System.Windows.Forms.CheckBox();
            this.cbxOneDecionRequired = new System.Windows.Forms.CheckBox();
            this.cbxLockWhenAllDecisonAreMade = new System.Windows.Forms.CheckBox();
            this.cbxLockWhenFirstDecisonMade = new System.Windows.Forms.CheckBox();
            this.btnCreateJob = new System.Windows.Forms.Button();
            this.btnFileOpen = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtJobName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSessionKeyForCreateJob = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tabStatusJob = new System.Windows.Forms.TabPage();
            this.txtStatusJobUsername = new System.Windows.Forms.TextBox();
            this.lblStatusJobUsername = new System.Windows.Forms.Label();
            this.btnApprovalStatus = new System.Windows.Forms.Button();
            this.btnStatusJob = new System.Windows.Forms.Button();
            this.txtStatusJobKey = new System.Windows.Forms.TextBox();
            this.txtStatusJobSessionKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPageGetAnnReport = new System.Windows.Forms.TabPage();
            this.lblPhases = new System.Windows.Forms.Label();
            this.txtPhases = new System.Windows.Forms.TextBox();
            this.btnGetAnnotationReportURL = new System.Windows.Forms.Button();
            this.txtGetAnnoReportUsername = new System.Windows.Forms.TextBox();
            this.lblGetAnnoReportUsername = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridViewGroups = new System.Windows.Forms.DataGridView();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.dataGridViewUsers = new System.Windows.Forms.DataGridView();
            this.checkSumaryPage = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtCmtsPerPage = new System.Windows.Forms.TextBox();
            this.cbxFilterBy = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cbxDisplayOpt = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbSortBy = new System.Windows.Forms.ComboBox();
            this.btnGetReport = new System.Windows.Forms.Button();
            this.txtJobKeyForAnnReport = new System.Windows.Forms.TextBox();
            this.txtSessionKeyForAnnReport = new System.Windows.Forms.TextBox();
            this.lblJobKeyForRetrieveJob = new System.Windows.Forms.Label();
            this.lblSessionKeyForRetrieveJob = new System.Windows.Forms.Label();
            this.tabPageGetAnnotationList = new System.Windows.Forms.TabPage();
            this.txtGetAnnoListUsername = new System.Windows.Forms.TextBox();
            this.lblGetAnnoListUsername = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.btnGetAnnotationList = new System.Windows.Forms.Button();
            this.Groups = new System.Windows.Forms.Label();
            this.dataGridViewAnnotationListGroups = new System.Windows.Forms.DataGridView();
            this.label30 = new System.Windows.Forms.Label();
            this.dataGridViewAnnotationListUsers = new System.Windows.Forms.DataGridView();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cbxFilterByAnnList = new System.Windows.Forms.ComboBox();
            this.cbxSortByAnnList = new System.Windows.Forms.ComboBox();
            this.txtApprovalKeyAnnotationList = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSessionKeyAnnotationList = new System.Windows.Forms.TextBox();
            this.tabPageGetAllVersionsAnnotReport = new System.Windows.Forms.TabPage();
            this.label54 = new System.Windows.Forms.Label();
            this.ckbSumaryPageJobAnnotationReport = new System.Windows.Forms.CheckBox();
            this.btnGetAllVersionsReportURL = new System.Windows.Forms.Button();
            this.txtUserNameJobAnnotationReport = new System.Windows.Forms.TextBox();
            this.dataGridViewAllVersionsReportGroups = new System.Windows.Forms.DataGridView();
            this.dataGridViewAllVersionsReportUsers = new System.Windows.Forms.DataGridView();
            this.label48 = new System.Windows.Forms.Label();
            this.txtCommentsPerPageJobAnnotationReport = new System.Windows.Forms.TextBox();
            this.cbxFilterByJobAnnotationReport = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.cbxDisplayOptJobAnnotationReport = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.cbxSortByJobAnnotationReport = new System.Windows.Forms.ComboBox();
            this.txtJobKeyJobAnnotationReport = new System.Windows.Forms.TextBox();
            this.txtSessionKeyJobAnnotationReport = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.tabPageUpdateJob = new System.Windows.Forms.TabPage();
            this.cbxTestPushJobStatus = new System.Windows.Forms.CheckBox();
            this.txtUpdateJobUsername = new System.Windows.Forms.TextBox();
            this.lblUpdateJobUsername = new System.Windows.Forms.Label();
            this.btnUpdateJob = new System.Windows.Forms.Button();
            this.lblJobStatusUpdateJob = new System.Windows.Forms.Label();
            this.cbxJobStatUpdJob = new System.Windows.Forms.ComboBox();
            this.txtJobTitleUpdJob = new System.Windows.Forms.TextBox();
            this.lblJobTitleUpdJob = new System.Windows.Forms.Label();
            this.jobKeyForUpdJob = new System.Windows.Forms.TextBox();
            this.txtSessionKeyForUpdJob = new System.Windows.Forms.TextBox();
            this.lblJobKeyUpdateJob = new System.Windows.Forms.Label();
            this.lblSessKeyUpdJob = new System.Windows.Forms.Label();
            this.tabPageDeleteApproval = new System.Windows.Forms.TabPage();
            this.txtDeleteApprovalUsername = new System.Windows.Forms.TextBox();
            this.lblDeleteApprovalUsername = new System.Windows.Forms.Label();
            this.btnDeleteApproval = new System.Windows.Forms.Button();
            this.txtDeleteAppKey = new System.Windows.Forms.TextBox();
            this.txtSessionKeyDeleteApp = new System.Windows.Forms.TextBox();
            this.lblTransferJobJobKey = new System.Windows.Forms.Label();
            this.lblTransferJobSessionKey = new System.Windows.Forms.Label();
            this.tabPageDeleteJob = new System.Windows.Forms.TabPage();
            this.txtDeleteJobUsername = new System.Windows.Forms.TextBox();
            this.lblDeleteJobUsername = new System.Windows.Forms.Label();
            this.btnDeleteJob = new System.Windows.Forms.Button();
            this.txtDeleteJobKey = new System.Windows.Forms.TextBox();
            this.txtDeleteSessionKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabRestoreApproval = new System.Windows.Forms.TabPage();
            this.txtRestoreAppUsername = new System.Windows.Forms.TextBox();
            this.lblRestoreAppUsername = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtApprovalRestoreKey = new System.Windows.Forms.TextBox();
            this.btnRestoreApproval = new System.Windows.Forms.Button();
            this.txtSessionKeyForRestoreApp = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPageGetApproval = new System.Windows.Forms.TabPage();
            this.txtGetApprovalUsername = new System.Windows.Forms.TextBox();
            this.lblGetApprovalUsername = new System.Windows.Forms.Label();
            this.btnGetAppAccess = new System.Windows.Forms.Button();
            this.pictBxGetApp = new System.Windows.Forms.PictureBox();
            this.btnGetApp = new System.Windows.Forms.Button();
            this.txtGetAppForAppGuid = new System.Windows.Forms.TextBox();
            this.txtSessionKeyForGetApp = new System.Windows.Forms.TextBox();
            this.labGetAppGuid = new System.Windows.Forms.Label();
            this.lblGetAppSK = new System.Windows.Forms.Label();
            this.tabUpdateApproval = new System.Windows.Forms.TabPage();
            this.txtUpdateApprovalUsername = new System.Windows.Forms.TextBox();
            this.lblUpdateApprovalUsername = new System.Windows.Forms.Label();
            this.txtUpdateApprovalKey = new System.Windows.Forms.TextBox();
            this.lblApprovalKey = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.updateApprovalDeadline = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewUpdateCollaborators = new System.Windows.Forms.DataGridView();
            this.btnUpdateApproval = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.txtUpdateApprovalSessionKey = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tabUpdateApprovalPhaseDeadline = new System.Windows.Forms.TabPage();
            this.txtUpdateApprovalPhaseDeadlineUsername = new System.Windows.Forms.TextBox();
            this.lblUpdateApprovalPhaseDeadlineUsername = new System.Windows.Forms.Label();
            this.txtUpdateApprovalPhaseDeadlineKey = new System.Windows.Forms.TextBox();
            this.txtUpdateAPD_ApprovalPhaseKey = new System.Windows.Forms.TextBox();
            this.lblApprovalPhaseDeadlineKey = new System.Windows.Forms.Label();
            this.lblUpdateAPD_ApprovalPhaseKey = new System.Windows.Forms.Label();
            this.updateApprovalPhaseDeadline = new System.Windows.Forms.DateTimePicker();
            this.btnUpdateApprovalPhaseDeadline = new System.Windows.Forms.Button();
            this.lblApprovalPhaseDeadline_Deadline = new System.Windows.Forms.Label();
            this.txtUpdateApprovalPhaseDeadlineSessionKey = new System.Windows.Forms.TextBox();
            this.lblUpdateApprovalPhaseDeadlineSessionKey = new System.Windows.Forms.Label();
            this.tabProofStudioURL = new System.Windows.Forms.TabPage();
            this.txtGetProofStudioURLUsername = new System.Windows.Forms.TextBox();
            this.lblGetProofStudioURLUsername = new System.Windows.Forms.Label();
            this.btnGetProofStudioURL = new System.Windows.Forms.Button();
            this.txtProofStudioApprovalKey = new System.Windows.Forms.TextBox();
            this.txtProofStudioSessionKey = new System.Windows.Forms.TextBox();
            this.lblProofStudioApprovalKey = new System.Windows.Forms.Label();
            this.lblProofStudioSessionKey = new System.Windows.Forms.Label();
            this.tabCreateFolder = new System.Windows.Forms.TabPage();
            this.txtCreateFolderUsername = new System.Windows.Forms.TextBox();
            this.lblCreateFolderUsername = new System.Windows.Forms.Label();
            this.txtSessionKeyCreateFolder = new System.Windows.Forms.TextBox();
            this.lblSessionKeyCreateFolder = new System.Windows.Forms.Label();
            this.btnCreateNewFolder = new System.Windows.Forms.Button();
            this.lblParentFolderGuid = new System.Windows.Forms.Label();
            this.lblFolderName = new System.Windows.Forms.Label();
            this.txtParentFolderGuid = new System.Windows.Forms.TextBox();
            this.txtFolderName = new System.Windows.Forms.TextBox();
            this.tabGetAllFolders = new System.Windows.Forms.TabPage();
            this.txtGetAllFoldersUsername = new System.Windows.Forms.TextBox();
            this.lblGetAllFoldersUsername = new System.Windows.Forms.Label();
            this.btnGetAllFolders = new System.Windows.Forms.Button();
            this.txtSessionKeyGetAllFolders = new System.Windows.Forms.TextBox();
            this.lblSessionKeyGetAllFolders = new System.Windows.Forms.Label();
            this.tabCreateUser = new System.Windows.Forms.TabPage();
            this.chkIsSSOUser = new System.Windows.Forms.CheckBox();
            this.chkPrivateAnnotations = new System.Windows.Forms.CheckBox();
            this.btnCreateuser = new System.Windows.Forms.Button();
            this.dataGridViewUserGroups = new System.Windows.Forms.DataGridView();
            this.lblgroups = new System.Windows.Forms.Label();
            this.dataGridViewPermissions = new System.Windows.Forms.DataGridView();
            this.lblUserPermissions = new System.Windows.Forms.Label();
            this.txtUserPassword = new System.Windows.Forms.TextBox();
            this.lblPasswordforUser = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmailForCreateUser = new System.Windows.Forms.Label();
            this.txtUserLastName = new System.Windows.Forms.TextBox();
            this.lblLastNameforCreateUser = new System.Windows.Forms.Label();
            this.txtUserFirstName = new System.Windows.Forms.TextBox();
            this.lblFirstNameforCreateUser = new System.Windows.Forms.Label();
            this.txtSessionKeyCreateUser = new System.Windows.Forms.TextBox();
            this.lblSessionKeyforCreaqteUser = new System.Windows.Forms.Label();
            this.tabCreateExternalUser = new System.Windows.Forms.TabPage();
            this.btnCreateExternaluser = new System.Windows.Forms.Button();
            this.txtExternalEmail = new System.Windows.Forms.TextBox();
            this.lblEmailForCreateExternalUser = new System.Windows.Forms.Label();
            this.txtExternalUserLastName = new System.Windows.Forms.TextBox();
            this.lblLastNameforCreateExternalUser = new System.Windows.Forms.Label();
            this.txtExternalUserFirstName = new System.Windows.Forms.TextBox();
            this.lblFirstNameforCreateExternalUser = new System.Windows.Forms.Label();
            this.txtSessionKeyCreateExternalUser = new System.Windows.Forms.TextBox();
            this.lblSessionKeyforCreaqteExternalUser = new System.Windows.Forms.Label();
            this.tabListApprovalWorkflows = new System.Windows.Forms.TabPage();
            this.txtListApprovalWorkflowUsername = new System.Windows.Forms.TextBox();
            this.lblListApprovalWorkflowUsername = new System.Windows.Forms.Label();
            this.btnListApprovalWorkflows = new System.Windows.Forms.Button();
            this.txtListApprovalWorkflowsSessionKey = new System.Windows.Forms.TextBox();
            this.lblListWorkflowsSessionKey = new System.Windows.Forms.Label();
            this.tabCheckUser = new System.Windows.Forms.TabPage();
            this.btnCheckUser = new System.Windows.Forms.Button();
            this.txtEmailForCheckUser = new System.Windows.Forms.TextBox();
            this.txtSessionKeyForCheckUser = new System.Windows.Forms.TextBox();
            this.lblCheckUserEmail = new System.Windows.Forms.Label();
            this.lblSessionKeyCheckUser = new System.Windows.Forms.Label();
            this.tabGetWorkflowDetails = new System.Windows.Forms.TabPage();
            this.txtGetWorkflowDetailsUsername = new System.Windows.Forms.TextBox();
            this.lblGetWorkflowDetailsUsername = new System.Windows.Forms.Label();
            this.btnGetWorkflowDetails = new System.Windows.Forms.Button();
            this.txtApprovalWorkflowKey = new System.Windows.Forms.TextBox();
            this.txtGetWorkflowDetailsSessionKey = new System.Windows.Forms.TextBox();
            this.lblApprovalWorkflowKey = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.tabUpdateApprovalDecision = new System.Windows.Forms.TabPage();
            this.cbxTestPushApprovalStatus = new System.Windows.Forms.CheckBox();
            this.cbxLoggedUserDecision = new System.Windows.Forms.ComboBox();
            this.lblDecision = new System.Windows.Forms.Label();
            this.lblApprovalDecisionApprovalKey = new System.Windows.Forms.Label();
            this.txtApprovalDecisionApprovalKey = new System.Windows.Forms.TextBox();
            this.btnUpdateApprovalDecision = new System.Windows.Forms.Button();
            this.txtApprovalDecisionUsername = new System.Windows.Forms.TextBox();
            this.txtApprovalDecisionExternalEmail = new System.Windows.Forms.TextBox();
            this.txtApprovalDecisionSessionKey = new System.Windows.Forms.TextBox();
            this.lblApprovalDecisionUsername = new System.Windows.Forms.Label();
            this.lblApprovalDecisionExternalEmail = new System.Windows.Forms.Label();
            this.lblApprovalDecisionSessionKey = new System.Windows.Forms.Label();
            this.tabGetFolderByName = new System.Windows.Forms.TabPage();
            this.txtGetFolderByNameUsername = new System.Windows.Forms.TextBox();
            this.lblUsernameForFolderName = new System.Windows.Forms.Label();
            this.btnGetFolderByName = new System.Windows.Forms.Button();
            this.txtFolderByName = new System.Windows.Forms.TextBox();
            this.txtSessionKeGetFolderByName = new System.Windows.Forms.TextBox();
            this.lblFoldersName = new System.Windows.Forms.Label();
            this.lblSessionKeyGetFolderByName = new System.Windows.Forms.Label();
            this.tabRerunWorkflow = new System.Windows.Forms.TabPage();
            this.label39 = new System.Windows.Forms.Label();
            this.txtRerunWorkflowSessionKey = new System.Windows.Forms.TextBox();
            this.btnRerunWorkflow = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtRerunWorkflowUserName = new System.Windows.Forms.TextBox();
            this.txtRerunWorkflowPhaseGUID = new System.Windows.Forms.TextBox();
            this.txtRerunWorkflowApprovalGUID = new System.Windows.Forms.TextBox();
            this.tabCreateWorkflow = new System.Windows.Forms.TabPage();
            this.chbRerunWorkflow = new System.Windows.Forms.CheckBox();
            this.btnLoadWorkflowFromFile = new System.Windows.Forms.Button();
            this.txtCreateWorkflowUserName = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tabPhasesControl = new System.Windows.Forms.TabControl();
            this.tabPhases = new System.Windows.Forms.TabPage();
            this.btnAddNewPhase = new System.Windows.Forms.Button();
            this.txtWorkflowName = new System.Windows.Forms.TextBox();
            this.btnSaveWorkflow = new System.Windows.Forms.Button();
            this.txtCreateWorflowSessionKey = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.tabCompareVersions = new System.Windows.Forms.TabPage();
            this.txtCompareVersionsUsername = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.btnCompareVersionsSend = new System.Windows.Forms.Button();
            this.txtCompareVersionsV2 = new System.Windows.Forms.TextBox();
            this.txtCompareVersionsV1 = new System.Windows.Forms.TextBox();
            this.txtCompareVersionsSessionKey = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tabAddCollaboratorToPhase = new System.Windows.Forms.TabPage();
            this.btnAddCollabToPhaseSave = new System.Windows.Forms.Button();
            this.btnAddCollabToPhaseLoadFromFile = new System.Windows.Forms.Button();
            this.lblAddCollabToPhaseCollabs = new System.Windows.Forms.Label();
            this.dataGridViewAddCollabToPhase = new System.Windows.Forms.DataGridView();
            this.txtAddCollabToPhasePhaseGuid = new System.Windows.Forms.TextBox();
            this.txtAddCollabToPhaseAppGuid = new System.Windows.Forms.TextBox();
            this.lblAddCollabToPhasePhaseGuid = new System.Windows.Forms.Label();
            this.lblAddCollabToPhaseAppGuid = new System.Windows.Forms.Label();
            this.txtAddCollabToPhaseUserName = new System.Windows.Forms.TextBox();
            this.lblAddCollabToPhaseUserName = new System.Windows.Forms.Label();
            this.txtAddCollabToPhaseSessionKey = new System.Windows.Forms.TextBox();
            this.lblAddCollabToPhaseSessionKey = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpLog = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.pnlLogCommands = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.cmbServicesURL = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.openFileDialogForCreateJob = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogForLoadingWorkflowDetails = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialogForLoadingNewPhaseCollabs = new System.Windows.Forms.OpenFileDialog();
            this.tabChecklist = new System.Windows.Forms.TabPage();
            this.txtGetChecklistUsername = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.btnGetChecklist = new System.Windows.Forms.Button();
            this.txtGetChecklistSessionKey = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txtChecklistGuid = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.tabControlCollaborateAPI.SuspendLayout();
            this.tabPageStartSession.SuspendLayout();
            this.tabConfirmSes.SuspendLayout();
            this.tabListJobs.SuspendLayout();
            this.tabDisplayJob.SuspendLayout();
            this.tabPageCreateJob.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCollaborators)).BeginInit();
            this.tabStatusJob.SuspendLayout();
            this.tabPageGetAnnReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).BeginInit();
            this.tabPageGetAnnotationList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAnnotationListGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAnnotationListUsers)).BeginInit();
            this.tabPageGetAllVersionsAnnotReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllVersionsReportGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllVersionsReportUsers)).BeginInit();
            this.tabPageUpdateJob.SuspendLayout();
            this.tabPageDeleteApproval.SuspendLayout();
            this.tabPageDeleteJob.SuspendLayout();
            this.tabRestoreApproval.SuspendLayout();
            this.tabPageGetApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictBxGetApp)).BeginInit();
            this.tabUpdateApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUpdateCollaborators)).BeginInit();
            this.tabUpdateApprovalPhaseDeadline.SuspendLayout();
            this.tabProofStudioURL.SuspendLayout();
            this.tabCreateFolder.SuspendLayout();
            this.tabGetAllFolders.SuspendLayout();
            this.tabCreateUser.SuspendLayout();
            this.tabCreateExternalUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUserGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPermissions)).BeginInit();
            this.tabListApprovalWorkflows.SuspendLayout();
            this.tabCheckUser.SuspendLayout();
            this.tabGetWorkflowDetails.SuspendLayout();
            this.tabUpdateApprovalDecision.SuspendLayout();
            this.tabGetFolderByName.SuspendLayout();
            this.tabRerunWorkflow.SuspendLayout();
            this.tabCreateWorkflow.SuspendLayout();
            this.tabPhasesControl.SuspendLayout();
            this.tabCompareVersions.SuspendLayout();
            this.tabAddCollaboratorToPhase.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAddCollabToPhase)).BeginInit();
            this.panelMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlLogCommands.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.tabChecklist.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlCollaborateAPI
            // 
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageStartSession);
            this.tabControlCollaborateAPI.Controls.Add(this.tabConfirmSes);
            this.tabControlCollaborateAPI.Controls.Add(this.tabListJobs);
            this.tabControlCollaborateAPI.Controls.Add(this.tabDisplayJob);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageCreateJob);
            this.tabControlCollaborateAPI.Controls.Add(this.tabStatusJob);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageGetAnnReport);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageGetAnnotationList);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageGetAllVersionsAnnotReport);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageUpdateJob);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageDeleteApproval);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageDeleteJob);
            this.tabControlCollaborateAPI.Controls.Add(this.tabRestoreApproval);
            this.tabControlCollaborateAPI.Controls.Add(this.tabPageGetApproval);
            this.tabControlCollaborateAPI.Controls.Add(this.tabUpdateApproval);
            this.tabControlCollaborateAPI.Controls.Add(this.tabUpdateApprovalPhaseDeadline);
            this.tabControlCollaborateAPI.Controls.Add(this.tabProofStudioURL);
            this.tabControlCollaborateAPI.Controls.Add(this.tabCreateFolder);
            this.tabControlCollaborateAPI.Controls.Add(this.tabGetAllFolders);
            this.tabControlCollaborateAPI.Controls.Add(this.tabCreateUser);
            this.tabControlCollaborateAPI.Controls.Add(this.tabCreateExternalUser);
            this.tabControlCollaborateAPI.Controls.Add(this.tabListApprovalWorkflows);
            this.tabControlCollaborateAPI.Controls.Add(this.tabCheckUser);
            this.tabControlCollaborateAPI.Controls.Add(this.tabGetWorkflowDetails);
            this.tabControlCollaborateAPI.Controls.Add(this.tabUpdateApprovalDecision);
            this.tabControlCollaborateAPI.Controls.Add(this.tabGetFolderByName);
            this.tabControlCollaborateAPI.Controls.Add(this.tabRerunWorkflow);
            this.tabControlCollaborateAPI.Controls.Add(this.tabCreateWorkflow);
            this.tabControlCollaborateAPI.Controls.Add(this.tabCompareVersions);
            this.tabControlCollaborateAPI.Controls.Add(this.tabAddCollaboratorToPhase);
            this.tabControlCollaborateAPI.Controls.Add(this.tabChecklist);
            this.tabControlCollaborateAPI.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControlCollaborateAPI.Location = new System.Drawing.Point(0, 0);
            this.tabControlCollaborateAPI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControlCollaborateAPI.Multiline = true;
            this.tabControlCollaborateAPI.Name = "tabControlCollaborateAPI";
            this.tabControlCollaborateAPI.SelectedIndex = 0;
            this.tabControlCollaborateAPI.Size = new System.Drawing.Size(587, 979);
            this.tabControlCollaborateAPI.TabIndex = 0;
            this.tabControlCollaborateAPI.SelectedIndexChanged += new System.EventHandler(this.tabControlCollaborateAPI_SelectedIndexChanged);
            // 
            // tabPageStartSession
            // 
            this.tabPageStartSession.Controls.Add(this.btnStart);
            this.tabPageStartSession.Controls.Add(this.txtAccountURL);
            this.tabPageStartSession.Controls.Add(this.txtUserName);
            this.tabPageStartSession.Controls.Add(this.lblAccountURL);
            this.tabPageStartSession.Controls.Add(this.lblUserName);
            this.tabPageStartSession.Location = new System.Drawing.Point(4, 130);
            this.tabPageStartSession.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageStartSession.Name = "tabPageStartSession";
            this.tabPageStartSession.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageStartSession.Size = new System.Drawing.Size(579, 845);
            this.tabPageStartSession.TabIndex = 0;
            this.tabPageStartSession.Text = "StartSes";
            this.tabPageStartSession.UseVisualStyleBackColor = true;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(395, 100);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(100, 28);
            this.btnStart.TabIndex = 8;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // txtAccountURL
            // 
            this.txtAccountURL.Location = new System.Drawing.Point(192, 57);
            this.txtAccountURL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAccountURL.Name = "txtAccountURL";
            this.txtAccountURL.Size = new System.Drawing.Size(305, 22);
            this.txtAccountURL.TabIndex = 7;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(192, 7);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(305, 22);
            this.txtUserName.TabIndex = 5;
            // 
            // lblAccountURL
            // 
            this.lblAccountURL.AutoSize = true;
            this.lblAccountURL.Location = new System.Drawing.Point(15, 57);
            this.lblAccountURL.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAccountURL.Name = "lblAccountURL";
            this.lblAccountURL.Size = new System.Drawing.Size(91, 17);
            this.lblAccountURL.TabIndex = 3;
            this.lblAccountURL.Text = "Account URL";
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(15, 11);
            this.lblUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(75, 17);
            this.lblUserName.TabIndex = 1;
            this.lblUserName.Text = "UserName";
            // 
            // tabConfirmSes
            // 
            this.tabConfirmSes.Controls.Add(this.txtConfirmSesPass);
            this.tabConfirmSes.Controls.Add(this.label19);
            this.tabConfirmSes.Controls.Add(this.btnConfirmSession);
            this.tabConfirmSes.Controls.Add(this.txtSessionKeyForConfirm);
            this.tabConfirmSes.Controls.Add(this.label17);
            this.tabConfirmSes.Location = new System.Drawing.Point(4, 25);
            this.tabConfirmSes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabConfirmSes.Name = "tabConfirmSes";
            this.tabConfirmSes.Size = new System.Drawing.Size(579, 950);
            this.tabConfirmSes.TabIndex = 9;
            this.tabConfirmSes.Text = "ConfirmSes";
            this.tabConfirmSes.UseVisualStyleBackColor = true;
            // 
            // txtConfirmSesPass
            // 
            this.txtConfirmSesPass.Location = new System.Drawing.Point(192, 54);
            this.txtConfirmSesPass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtConfirmSesPass.Name = "txtConfirmSesPass";
            this.txtConfirmSesPass.PasswordChar = '*';
            this.txtConfirmSesPass.Size = new System.Drawing.Size(305, 22);
            this.txtConfirmSesPass.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 54);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 17);
            this.label19.TabIndex = 17;
            this.label19.Text = "Password";
            // 
            // btnConfirmSession
            // 
            this.btnConfirmSession.Location = new System.Drawing.Point(399, 98);
            this.btnConfirmSession.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnConfirmSession.Name = "btnConfirmSession";
            this.btnConfirmSession.Size = new System.Drawing.Size(100, 28);
            this.btnConfirmSession.TabIndex = 16;
            this.btnConfirmSession.Text = "Confirm";
            this.btnConfirmSession.UseVisualStyleBackColor = true;
            this.btnConfirmSession.Click += new System.EventHandler(this.btnConfirmSession_Click);
            // 
            // txtSessionKeyForConfirm
            // 
            this.txtSessionKeyForConfirm.Location = new System.Drawing.Point(192, 7);
            this.txtSessionKeyForConfirm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForConfirm.Name = "txtSessionKeyForConfirm";
            this.txtSessionKeyForConfirm.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyForConfirm.TabIndex = 14;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 11);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 17);
            this.label17.TabIndex = 12;
            this.label17.Text = "SessionKey";
            // 
            // tabListJobs
            // 
            this.tabListJobs.Controls.Add(this.txtListJobsUsername);
            this.tabListJobs.Controls.Add(this.lblListJobsUsername);
            this.tabListJobs.Controls.Add(this.btnListJobs);
            this.tabListJobs.Controls.Add(this.txtSessionKeyForListJobs);
            this.tabListJobs.Controls.Add(this.label15);
            this.tabListJobs.Location = new System.Drawing.Point(4, 25);
            this.tabListJobs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabListJobs.Name = "tabListJobs";
            this.tabListJobs.Size = new System.Drawing.Size(579, 950);
            this.tabListJobs.TabIndex = 8;
            this.tabListJobs.Text = "ListJobs";
            this.tabListJobs.UseVisualStyleBackColor = true;
            // 
            // txtListJobsUsername
            // 
            this.txtListJobsUsername.Location = new System.Drawing.Point(192, 46);
            this.txtListJobsUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtListJobsUsername.Name = "txtListJobsUsername";
            this.txtListJobsUsername.Size = new System.Drawing.Size(300, 22);
            this.txtListJobsUsername.TabIndex = 48;
            // 
            // lblListJobsUsername
            // 
            this.lblListJobsUsername.AutoSize = true;
            this.lblListJobsUsername.Location = new System.Drawing.Point(15, 49);
            this.lblListJobsUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblListJobsUsername.Name = "lblListJobsUsername";
            this.lblListJobsUsername.Size = new System.Drawing.Size(73, 17);
            this.lblListJobsUsername.TabIndex = 47;
            this.lblListJobsUsername.Text = "Username";
            // 
            // btnListJobs
            // 
            this.btnListJobs.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnListJobs.Location = new System.Drawing.Point(393, 81);
            this.btnListJobs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnListJobs.Name = "btnListJobs";
            this.btnListJobs.Size = new System.Drawing.Size(100, 28);
            this.btnListJobs.TabIndex = 12;
            this.btnListJobs.Text = "List Jobs";
            this.btnListJobs.UseVisualStyleBackColor = true;
            this.btnListJobs.Click += new System.EventHandler(this.btnListJobs_Click);
            // 
            // txtSessionKeyForListJobs
            // 
            this.txtSessionKeyForListJobs.Location = new System.Drawing.Point(192, 7);
            this.txtSessionKeyForListJobs.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForListJobs.Name = "txtSessionKeyForListJobs";
            this.txtSessionKeyForListJobs.Size = new System.Drawing.Size(300, 22);
            this.txtSessionKeyForListJobs.TabIndex = 11;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(15, 11);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 17);
            this.label15.TabIndex = 10;
            this.label15.Text = "SessionKey";
            // 
            // tabDisplayJob
            // 
            this.tabDisplayJob.Controls.Add(this.txtDisplayJobUsername);
            this.tabDisplayJob.Controls.Add(this.lblDisplayJobUsername);
            this.tabDisplayJob.Controls.Add(this.btnDisplayJob);
            this.tabDisplayJob.Controls.Add(this.txtDisplayJobKey);
            this.tabDisplayJob.Controls.Add(this.txtDisplayJobSessionKey);
            this.tabDisplayJob.Controls.Add(this.label1);
            this.tabDisplayJob.Controls.Add(this.label4);
            this.tabDisplayJob.Location = new System.Drawing.Point(4, 25);
            this.tabDisplayJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabDisplayJob.Name = "tabDisplayJob";
            this.tabDisplayJob.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabDisplayJob.Size = new System.Drawing.Size(579, 950);
            this.tabDisplayJob.TabIndex = 4;
            this.tabDisplayJob.Text = "DisplayJob";
            this.tabDisplayJob.UseVisualStyleBackColor = true;
            // 
            // txtDisplayJobUsername
            // 
            this.txtDisplayJobUsername.Location = new System.Drawing.Point(192, 80);
            this.txtDisplayJobUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDisplayJobUsername.Name = "txtDisplayJobUsername";
            this.txtDisplayJobUsername.Size = new System.Drawing.Size(305, 22);
            this.txtDisplayJobUsername.TabIndex = 46;
            // 
            // lblDisplayJobUsername
            // 
            this.lblDisplayJobUsername.AutoSize = true;
            this.lblDisplayJobUsername.Location = new System.Drawing.Point(15, 80);
            this.lblDisplayJobUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDisplayJobUsername.Name = "lblDisplayJobUsername";
            this.lblDisplayJobUsername.Size = new System.Drawing.Size(73, 17);
            this.lblDisplayJobUsername.TabIndex = 45;
            this.lblDisplayJobUsername.Text = "Username";
            // 
            // btnDisplayJob
            // 
            this.btnDisplayJob.Location = new System.Drawing.Point(393, 130);
            this.btnDisplayJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDisplayJob.Name = "btnDisplayJob";
            this.btnDisplayJob.Size = new System.Drawing.Size(100, 28);
            this.btnDisplayJob.TabIndex = 11;
            this.btnDisplayJob.Text = "DisplayJob";
            this.btnDisplayJob.UseVisualStyleBackColor = true;
            this.btnDisplayJob.Click += new System.EventHandler(this.btnDisplayJob_Click);
            // 
            // txtDisplayJobKey
            // 
            this.txtDisplayJobKey.Location = new System.Drawing.Point(192, 44);
            this.txtDisplayJobKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDisplayJobKey.Name = "txtDisplayJobKey";
            this.txtDisplayJobKey.Size = new System.Drawing.Size(305, 22);
            this.txtDisplayJobKey.TabIndex = 10;
            // 
            // txtDisplayJobSessionKey
            // 
            this.txtDisplayJobSessionKey.Location = new System.Drawing.Point(192, 7);
            this.txtDisplayJobSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDisplayJobSessionKey.Name = "txtDisplayJobSessionKey";
            this.txtDisplayJobSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtDisplayJobSessionKey.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "JobKey";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 11);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "SessionKey";
            // 
            // tabPageCreateJob
            // 
            this.tabPageCreateJob.Controls.Add(this.txtChecklistGuid);
            this.tabPageCreateJob.Controls.Add(this.label56);
            this.tabPageCreateJob.Controls.Add(this.txtCreateJobUsername);
            this.tabPageCreateJob.Controls.Add(this.lblCreateJobUsername);
            this.tabPageCreateJob.Controls.Add(this.lblApprovalWorkflowSetting);
            this.tabPageCreateJob.Controls.Add(this.chbAllowUsersToBeAdded);
            this.tabPageCreateJob.Controls.Add(this.txtWorkflowGuid);
            this.tabPageCreateJob.Controls.Add(this.lblWorkflowGuid);
            this.tabPageCreateJob.Controls.Add(this.txtJobKey);
            this.tabPageCreateJob.Controls.Add(this.lblJobKey);
            this.tabPageCreateJob.Controls.Add(this.btnCreateVersion);
            this.tabPageCreateJob.Controls.Add(this.label32);
            this.tabPageCreateJob.Controls.Add(this.deadline);
            this.tabPageCreateJob.Controls.Add(this.dataGridViewCollaborators);
            this.tabPageCreateJob.Controls.Add(this.label25);
            this.tabPageCreateJob.Controls.Add(this.cbxAllowToDownload);
            this.tabPageCreateJob.Controls.Add(this.cbxOneDecionRequired);
            this.tabPageCreateJob.Controls.Add(this.cbxLockWhenAllDecisonAreMade);
            this.tabPageCreateJob.Controls.Add(this.cbxLockWhenFirstDecisonMade);
            this.tabPageCreateJob.Controls.Add(this.btnCreateJob);
            this.tabPageCreateJob.Controls.Add(this.btnFileOpen);
            this.tabPageCreateJob.Controls.Add(this.label13);
            this.tabPageCreateJob.Controls.Add(this.txtFolder);
            this.tabPageCreateJob.Controls.Add(this.label12);
            this.tabPageCreateJob.Controls.Add(this.label11);
            this.tabPageCreateJob.Controls.Add(this.txtFileName);
            this.tabPageCreateJob.Controls.Add(this.label10);
            this.tabPageCreateJob.Controls.Add(this.txtJobName);
            this.tabPageCreateJob.Controls.Add(this.label9);
            this.tabPageCreateJob.Controls.Add(this.txtSessionKeyForCreateJob);
            this.tabPageCreateJob.Controls.Add(this.label8);
            this.tabPageCreateJob.Location = new System.Drawing.Point(4, 130);
            this.tabPageCreateJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageCreateJob.Name = "tabPageCreateJob";
            this.tabPageCreateJob.Size = new System.Drawing.Size(579, 845);
            this.tabPageCreateJob.TabIndex = 7;
            this.tabPageCreateJob.Text = "CreateJob";
            this.tabPageCreateJob.UseVisualStyleBackColor = true;
            // 
            // txtCreateJobUsername
            // 
            this.txtCreateJobUsername.Location = new System.Drawing.Point(191, 304);
            this.txtCreateJobUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCreateJobUsername.Name = "txtCreateJobUsername";
            this.txtCreateJobUsername.Size = new System.Drawing.Size(305, 22);
            this.txtCreateJobUsername.TabIndex = 50;
            // 
            // lblCreateJobUsername
            // 
            this.lblCreateJobUsername.AutoSize = true;
            this.lblCreateJobUsername.Location = new System.Drawing.Point(13, 308);
            this.lblCreateJobUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCreateJobUsername.Name = "lblCreateJobUsername";
            this.lblCreateJobUsername.Size = new System.Drawing.Size(73, 17);
            this.lblCreateJobUsername.TabIndex = 49;
            this.lblCreateJobUsername.Text = "Username";
            // 
            // lblApprovalWorkflowSetting
            // 
            this.lblApprovalWorkflowSetting.AutoSize = true;
            this.lblApprovalWorkflowSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblApprovalWorkflowSetting.Location = new System.Drawing.Point(11, 356);
            this.lblApprovalWorkflowSetting.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalWorkflowSetting.Name = "lblApprovalWorkflowSetting";
            this.lblApprovalWorkflowSetting.Size = new System.Drawing.Size(129, 17);
            this.lblApprovalWorkflowSetting.TabIndex = 47;
            this.lblApprovalWorkflowSetting.Text = "Workflow Setting";
            // 
            // chbAllowUsersToBeAdded
            // 
            this.chbAllowUsersToBeAdded.Location = new System.Drawing.Point(191, 344);
            this.chbAllowUsersToBeAdded.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbAllowUsersToBeAdded.Name = "chbAllowUsersToBeAdded";
            this.chbAllowUsersToBeAdded.Size = new System.Drawing.Size(305, 39);
            this.chbAllowUsersToBeAdded.TabIndex = 46;
            this.chbAllowUsersToBeAdded.Text = "Allow users to be added to the phase once the file is uploaded";
            this.chbAllowUsersToBeAdded.UseVisualStyleBackColor = true;
            // 
            // txtWorkflowGuid
            // 
            this.txtWorkflowGuid.Location = new System.Drawing.Point(191, 234);
            this.txtWorkflowGuid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtWorkflowGuid.Name = "txtWorkflowGuid";
            this.txtWorkflowGuid.Size = new System.Drawing.Size(305, 22);
            this.txtWorkflowGuid.TabIndex = 44;
            this.txtWorkflowGuid.TextChanged += new System.EventHandler(this.txtWorkflowGuid_TextChanged);
            // 
            // lblWorkflowGuid
            // 
            this.lblWorkflowGuid.Location = new System.Drawing.Point(15, 238);
            this.lblWorkflowGuid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWorkflowGuid.Name = "lblWorkflowGuid";
            this.lblWorkflowGuid.Size = new System.Drawing.Size(153, 22);
            this.lblWorkflowGuid.TabIndex = 45;
            this.lblWorkflowGuid.Text = "Workflow";
            // 
            // txtJobKey
            // 
            this.txtJobKey.Location = new System.Drawing.Point(192, 39);
            this.txtJobKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJobKey.Name = "txtJobKey";
            this.txtJobKey.Size = new System.Drawing.Size(305, 22);
            this.txtJobKey.TabIndex = 43;
            this.txtJobKey.TextChanged += new System.EventHandler(this.txtJobKey_TextChanged);
            this.txtJobKey.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtJobKey_KeyUp);
            // 
            // lblJobKey
            // 
            this.lblJobKey.AutoSize = true;
            this.lblJobKey.Location = new System.Drawing.Point(15, 43);
            this.lblJobKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJobKey.Name = "lblJobKey";
            this.lblJobKey.Size = new System.Drawing.Size(59, 17);
            this.lblJobKey.TabIndex = 42;
            this.lblJobKey.Text = "Job Key";
            // 
            // btnCreateVersion
            // 
            this.btnCreateVersion.Location = new System.Drawing.Point(224, 741);
            this.btnCreateVersion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCreateVersion.Name = "btnCreateVersion";
            this.btnCreateVersion.Size = new System.Drawing.Size(144, 28);
            this.btnCreateVersion.TabIndex = 41;
            this.btnCreateVersion.Text = "Create Version";
            this.btnCreateVersion.UseVisualStyleBackColor = true;
            this.btnCreateVersion.Click += new System.EventHandler(this.btnCreateVersion_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(17, 497);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(105, 17);
            this.label32.TabIndex = 40;
            this.label32.Text = "Collaborators";
            // 
            // deadline
            // 
            this.deadline.Location = new System.Drawing.Point(191, 170);
            this.deadline.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.deadline.Name = "deadline";
            this.deadline.Size = new System.Drawing.Size(305, 22);
            this.deadline.TabIndex = 39;
            // 
            // dataGridViewCollaborators
            // 
            this.dataGridViewCollaborators.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCollaborators.Location = new System.Drawing.Point(17, 523);
            this.dataGridViewCollaborators.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewCollaborators.Name = "dataGridViewCollaborators";
            this.dataGridViewCollaborators.Size = new System.Drawing.Size(473, 210);
            this.dataGridViewCollaborators.TabIndex = 37;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(11, 391);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 17);
            this.label25.TabIndex = 17;
            this.label25.Text = "Settings";
            // 
            // cbxAllowToDownload
            // 
            this.cbxAllowToDownload.AutoSize = true;
            this.cbxAllowToDownload.Location = new System.Drawing.Point(191, 476);
            this.cbxAllowToDownload.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxAllowToDownload.Name = "cbxAllowToDownload";
            this.cbxAllowToDownload.Size = new System.Drawing.Size(277, 21);
            this.cbxAllowToDownload.TabIndex = 16;
            this.cbxAllowToDownload.Text = "Allow users to download the original file";
            this.cbxAllowToDownload.UseVisualStyleBackColor = true;
            // 
            // cbxOneDecionRequired
            // 
            this.cbxOneDecionRequired.AutoSize = true;
            this.cbxOneDecionRequired.Location = new System.Drawing.Point(191, 448);
            this.cbxOneDecionRequired.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxOneDecionRequired.Name = "cbxOneDecionRequired";
            this.cbxOneDecionRequired.Size = new System.Drawing.Size(200, 21);
            this.cbxOneDecionRequired.TabIndex = 15;
            this.cbxOneDecionRequired.Text = "Only one decision required";
            this.cbxOneDecionRequired.UseVisualStyleBackColor = true;
            // 
            // cbxLockWhenAllDecisonAreMade
            // 
            this.cbxLockWhenAllDecisonAreMade.AutoSize = true;
            this.cbxLockWhenAllDecisonAreMade.Location = new System.Drawing.Point(191, 420);
            this.cbxLockWhenAllDecisonAreMade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxLockWhenAllDecisonAreMade.Name = "cbxLockWhenAllDecisonAreMade";
            this.cbxLockWhenAllDecisonAreMade.Size = new System.Drawing.Size(303, 21);
            this.cbxLockWhenAllDecisonAreMade.TabIndex = 14;
            this.cbxLockWhenAllDecisonAreMade.Text = "Lock the proof when all decisions are made";
            this.cbxLockWhenAllDecisonAreMade.UseVisualStyleBackColor = true;
            // 
            // cbxLockWhenFirstDecisonMade
            // 
            this.cbxLockWhenFirstDecisonMade.AutoSize = true;
            this.cbxLockWhenFirstDecisonMade.Location = new System.Drawing.Point(191, 391);
            this.cbxLockWhenFirstDecisonMade.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxLockWhenFirstDecisonMade.Name = "cbxLockWhenFirstDecisonMade";
            this.cbxLockWhenFirstDecisonMade.Size = new System.Drawing.Size(294, 21);
            this.cbxLockWhenFirstDecisonMade.TabIndex = 13;
            this.cbxLockWhenFirstDecisonMade.Text = "Lock the proof when first decision is made";
            this.cbxLockWhenFirstDecisonMade.UseVisualStyleBackColor = true;
            // 
            // btnCreateJob
            // 
            this.btnCreateJob.Location = new System.Drawing.Point(389, 741);
            this.btnCreateJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCreateJob.Name = "btnCreateJob";
            this.btnCreateJob.Size = new System.Drawing.Size(100, 28);
            this.btnCreateJob.TabIndex = 8;
            this.btnCreateJob.Text = "Create Job";
            this.btnCreateJob.UseVisualStyleBackColor = true;
            this.btnCreateJob.Click += new System.EventHandler(this.btnCreateJob_Click);
            // 
            // btnFileOpen
            // 
            this.btnFileOpen.Location = new System.Drawing.Point(191, 74);
            this.btnFileOpen.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnFileOpen.Name = "btnFileOpen";
            this.btnFileOpen.Size = new System.Drawing.Size(100, 25);
            this.btnFileOpen.TabIndex = 2;
            this.btnFileOpen.Text = "Open File";
            this.btnFileOpen.UseVisualStyleBackColor = true;
            this.btnFileOpen.Click += new System.EventHandler(this.btnFileOpen_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 74);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "File";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(191, 202);
            this.txtFolder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(305, 22);
            this.txtFolder.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(13, 206);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(153, 22);
            this.label12.TabIndex = 8;
            this.label12.Text = "Folder";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(13, 174);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(153, 21);
            this.label11.TabIndex = 6;
            this.label11.Text = "Deadline";
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(192, 138);
            this.txtFileName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(305, 22);
            this.txtFileName.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 142);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "File Name";
            // 
            // txtJobName
            // 
            this.txtJobName.Location = new System.Drawing.Point(192, 106);
            this.txtJobName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJobName.Name = "txtJobName";
            this.txtJobName.Size = new System.Drawing.Size(305, 22);
            this.txtJobName.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 110);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 17);
            this.label9.TabIndex = 2;
            this.label9.Text = "Job Name";
            // 
            // txtSessionKeyForCreateJob
            // 
            this.txtSessionKeyForCreateJob.Location = new System.Drawing.Point(192, 7);
            this.txtSessionKeyForCreateJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForCreateJob.Name = "txtSessionKeyForCreateJob";
            this.txtSessionKeyForCreateJob.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyForCreateJob.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 11);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "SessionKey";
            // 
            // tabStatusJob
            // 
            this.tabStatusJob.Controls.Add(this.txtStatusJobUsername);
            this.tabStatusJob.Controls.Add(this.lblStatusJobUsername);
            this.tabStatusJob.Controls.Add(this.btnApprovalStatus);
            this.tabStatusJob.Controls.Add(this.btnStatusJob);
            this.tabStatusJob.Controls.Add(this.txtStatusJobKey);
            this.tabStatusJob.Controls.Add(this.txtStatusJobSessionKey);
            this.tabStatusJob.Controls.Add(this.label5);
            this.tabStatusJob.Controls.Add(this.label6);
            this.tabStatusJob.Location = new System.Drawing.Point(4, 25);
            this.tabStatusJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabStatusJob.Name = "tabStatusJob";
            this.tabStatusJob.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabStatusJob.Size = new System.Drawing.Size(579, 950);
            this.tabStatusJob.TabIndex = 5;
            this.tabStatusJob.Text = "StatusJob";
            this.tabStatusJob.UseVisualStyleBackColor = true;
            // 
            // txtStatusJobUsername
            // 
            this.txtStatusJobUsername.Location = new System.Drawing.Point(192, 71);
            this.txtStatusJobUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtStatusJobUsername.Name = "txtStatusJobUsername";
            this.txtStatusJobUsername.Size = new System.Drawing.Size(305, 22);
            this.txtStatusJobUsername.TabIndex = 48;
            // 
            // lblStatusJobUsername
            // 
            this.lblStatusJobUsername.AutoSize = true;
            this.lblStatusJobUsername.Location = new System.Drawing.Point(15, 75);
            this.lblStatusJobUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusJobUsername.Name = "lblStatusJobUsername";
            this.lblStatusJobUsername.Size = new System.Drawing.Size(73, 17);
            this.lblStatusJobUsername.TabIndex = 47;
            this.lblStatusJobUsername.Text = "Username";
            // 
            // btnApprovalStatus
            // 
            this.btnApprovalStatus.Location = new System.Drawing.Point(263, 121);
            this.btnApprovalStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnApprovalStatus.Name = "btnApprovalStatus";
            this.btnApprovalStatus.Size = new System.Drawing.Size(100, 28);
            this.btnApprovalStatus.TabIndex = 17;
            this.btnApprovalStatus.Text = "StatusApproval";
            this.btnApprovalStatus.UseVisualStyleBackColor = true;
            this.btnApprovalStatus.Click += new System.EventHandler(this.btnApprovalStatus_Click);
            // 
            // btnStatusJob
            // 
            this.btnStatusJob.Location = new System.Drawing.Point(399, 121);
            this.btnStatusJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnStatusJob.Name = "btnStatusJob";
            this.btnStatusJob.Size = new System.Drawing.Size(100, 28);
            this.btnStatusJob.TabIndex = 16;
            this.btnStatusJob.Text = "StatusJob";
            this.btnStatusJob.UseVisualStyleBackColor = true;
            this.btnStatusJob.Click += new System.EventHandler(this.btnStatusJob_Click);
            // 
            // txtStatusJobKey
            // 
            this.txtStatusJobKey.Location = new System.Drawing.Point(192, 39);
            this.txtStatusJobKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtStatusJobKey.Name = "txtStatusJobKey";
            this.txtStatusJobKey.Size = new System.Drawing.Size(305, 22);
            this.txtStatusJobKey.TabIndex = 15;
            // 
            // txtStatusJobSessionKey
            // 
            this.txtStatusJobSessionKey.Location = new System.Drawing.Point(192, 7);
            this.txtStatusJobSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtStatusJobSessionKey.Name = "txtStatusJobSessionKey";
            this.txtStatusJobSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtStatusJobSessionKey.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 43);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "JobKey";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 11);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "SessionKey";
            // 
            // tabPageGetAnnReport
            // 
            this.tabPageGetAnnReport.Controls.Add(this.lblPhases);
            this.tabPageGetAnnReport.Controls.Add(this.txtPhases);
            this.tabPageGetAnnReport.Controls.Add(this.btnGetAnnotationReportURL);
            this.tabPageGetAnnReport.Controls.Add(this.txtGetAnnoReportUsername);
            this.tabPageGetAnnReport.Controls.Add(this.lblGetAnnoReportUsername);
            this.tabPageGetAnnReport.Controls.Add(this.button1);
            this.tabPageGetAnnReport.Controls.Add(this.dataGridViewGroups);
            this.tabPageGetAnnReport.Controls.Add(this.label24);
            this.tabPageGetAnnReport.Controls.Add(this.label23);
            this.tabPageGetAnnReport.Controls.Add(this.dataGridViewUsers);
            this.tabPageGetAnnReport.Controls.Add(this.checkSumaryPage);
            this.tabPageGetAnnReport.Controls.Add(this.label22);
            this.tabPageGetAnnReport.Controls.Add(this.txtCmtsPerPage);
            this.tabPageGetAnnReport.Controls.Add(this.cbxFilterBy);
            this.tabPageGetAnnReport.Controls.Add(this.label21);
            this.tabPageGetAnnReport.Controls.Add(this.label20);
            this.tabPageGetAnnReport.Controls.Add(this.cbxDisplayOpt);
            this.tabPageGetAnnReport.Controls.Add(this.label16);
            this.tabPageGetAnnReport.Controls.Add(this.cmbSortBy);
            this.tabPageGetAnnReport.Controls.Add(this.btnGetReport);
            this.tabPageGetAnnReport.Controls.Add(this.txtJobKeyForAnnReport);
            this.tabPageGetAnnReport.Controls.Add(this.txtSessionKeyForAnnReport);
            this.tabPageGetAnnReport.Controls.Add(this.lblJobKeyForRetrieveJob);
            this.tabPageGetAnnReport.Controls.Add(this.lblSessionKeyForRetrieveJob);
            this.tabPageGetAnnReport.Location = new System.Drawing.Point(4, 25);
            this.tabPageGetAnnReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetAnnReport.Name = "tabPageGetAnnReport";
            this.tabPageGetAnnReport.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetAnnReport.Size = new System.Drawing.Size(579, 950);
            this.tabPageGetAnnReport.TabIndex = 1;
            this.tabPageGetAnnReport.Text = "GetAnnotReport";
            this.tabPageGetAnnReport.UseVisualStyleBackColor = true;
            // 
            // lblPhases
            // 
            this.lblPhases.AutoSize = true;
            this.lblPhases.Location = new System.Drawing.Point(15, 257);
            this.lblPhases.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPhases.Name = "lblPhases";
            this.lblPhases.Size = new System.Drawing.Size(65, 17);
            this.lblPhases.TabIndex = 41;
            this.lblPhases.Text = "Phase(s)";
            // 
            // txtPhases
            // 
            this.txtPhases.Location = new System.Drawing.Point(192, 249);
            this.txtPhases.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtPhases.Name = "txtPhases";
            this.txtPhases.Size = new System.Drawing.Size(285, 22);
            this.txtPhases.TabIndex = 40;
            // 
            // btnGetAnnotationReportURL
            // 
            this.btnGetAnnotationReportURL.Location = new System.Drawing.Point(87, 667);
            this.btnGetAnnotationReportURL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetAnnotationReportURL.Name = "btnGetAnnotationReportURL";
            this.btnGetAnnotationReportURL.Size = new System.Drawing.Size(131, 28);
            this.btnGetAnnotationReportURL.TabIndex = 39;
            this.btnGetAnnotationReportURL.Text = "Get URL";
            this.btnGetAnnotationReportURL.UseVisualStyleBackColor = true;
            this.btnGetAnnotationReportURL.Click += new System.EventHandler(this.btnGetAnnotationReportURL_Click);
            // 
            // txtGetAnnoReportUsername
            // 
            this.txtGetAnnoReportUsername.Location = new System.Drawing.Point(192, 217);
            this.txtGetAnnoReportUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetAnnoReportUsername.Name = "txtGetAnnoReportUsername";
            this.txtGetAnnoReportUsername.Size = new System.Drawing.Size(285, 22);
            this.txtGetAnnoReportUsername.TabIndex = 38;
            // 
            // lblGetAnnoReportUsername
            // 
            this.lblGetAnnoReportUsername.AutoSize = true;
            this.lblGetAnnoReportUsername.Location = new System.Drawing.Point(13, 225);
            this.lblGetAnnoReportUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetAnnoReportUsername.Name = "lblGetAnnoReportUsername";
            this.lblGetAnnoReportUsername.Size = new System.Drawing.Size(73, 17);
            this.lblGetAnnoReportUsername.TabIndex = 37;
            this.lblGetAnnoReportUsername.Text = "Username";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(227, 667);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 28);
            this.button1.TabIndex = 36;
            this.button1.Text = "ShowImage";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridViewGroups
            // 
            this.dataGridViewGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGroups.Location = new System.Drawing.Point(159, 490);
            this.dataGridViewGroups.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewGroups.Name = "dataGridViewGroups";
            this.dataGridViewGroups.Size = new System.Drawing.Size(320, 112);
            this.dataGridViewGroups.TabIndex = 35;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(19, 490);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 17);
            this.label24.TabIndex = 34;
            this.label24.Text = "Groups";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(19, 337);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 17);
            this.label23.TabIndex = 33;
            this.label23.Text = "Users";
            // 
            // dataGridViewUsers
            // 
            this.dataGridViewUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUsers.Location = new System.Drawing.Point(159, 330);
            this.dataGridViewUsers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewUsers.Name = "dataGridViewUsers";
            this.dataGridViewUsers.Size = new System.Drawing.Size(320, 145);
            this.dataGridViewUsers.TabIndex = 32;
            // 
            // checkSumaryPage
            // 
            this.checkSumaryPage.AutoSize = true;
            this.checkSumaryPage.Location = new System.Drawing.Point(192, 281);
            this.checkSumaryPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkSumaryPage.Name = "checkSumaryPage";
            this.checkSumaryPage.Size = new System.Drawing.Size(167, 21);
            this.checkSumaryPage.TabIndex = 31;
            this.checkSumaryPage.Text = "IncludeSummaryPage";
            this.checkSumaryPage.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 188);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(129, 17);
            this.label22.TabIndex = 29;
            this.label22.Text = "CommentsPerPage";
            // 
            // txtCmtsPerPage
            // 
            this.txtCmtsPerPage.Location = new System.Drawing.Point(192, 185);
            this.txtCmtsPerPage.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCmtsPerPage.Name = "txtCmtsPerPage";
            this.txtCmtsPerPage.Size = new System.Drawing.Size(285, 22);
            this.txtCmtsPerPage.TabIndex = 28;
            // 
            // cbxFilterBy
            // 
            this.cbxFilterBy.FormattingEnabled = true;
            this.cbxFilterBy.Items.AddRange(new object[] {
            "users",
            "groups"});
            this.cbxFilterBy.Location = new System.Drawing.Point(192, 148);
            this.cbxFilterBy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxFilterBy.Name = "cbxFilterBy";
            this.cbxFilterBy.Size = new System.Drawing.Size(285, 24);
            this.cbxFilterBy.TabIndex = 27;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 151);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 17);
            this.label21.TabIndex = 26;
            this.label21.Text = "FilterBy";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 118);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(77, 17);
            this.label20.TabIndex = 25;
            this.label20.Text = "DisplayOpt";
            // 
            // cbxDisplayOpt
            // 
            this.cbxDisplayOpt.FormattingEnabled = true;
            this.cbxDisplayOpt.Items.AddRange(new object[] {
            "comments",
            "pins",
            "commentsandpins"});
            this.cbxDisplayOpt.Location = new System.Drawing.Point(192, 114);
            this.cbxDisplayOpt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxDisplayOpt.Name = "cbxDisplayOpt";
            this.cbxDisplayOpt.Size = new System.Drawing.Size(285, 24);
            this.cbxDisplayOpt.TabIndex = 24;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 85);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 17);
            this.label16.TabIndex = 23;
            this.label16.Text = "SortBy";
            // 
            // cmbSortBy
            // 
            this.cmbSortBy.FormattingEnabled = true;
            this.cmbSortBy.Items.AddRange(new object[] {
            "date",
            "user",
            "group"});
            this.cmbSortBy.Location = new System.Drawing.Point(192, 81);
            this.cmbSortBy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbSortBy.Name = "cmbSortBy";
            this.cmbSortBy.Size = new System.Drawing.Size(285, 24);
            this.cmbSortBy.TabIndex = 22;
            // 
            // btnGetReport
            // 
            this.btnGetReport.Location = new System.Drawing.Point(351, 667);
            this.btnGetReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetReport.Name = "btnGetReport";
            this.btnGetReport.Size = new System.Drawing.Size(128, 28);
            this.btnGetReport.TabIndex = 21;
            this.btnGetReport.Text = "Get Report";
            this.btnGetReport.UseVisualStyleBackColor = true;
            this.btnGetReport.Click += new System.EventHandler(this.btnGetReport_Click);
            // 
            // txtJobKeyForAnnReport
            // 
            this.txtJobKeyForAnnReport.Location = new System.Drawing.Point(192, 44);
            this.txtJobKeyForAnnReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJobKeyForAnnReport.Name = "txtJobKeyForAnnReport";
            this.txtJobKeyForAnnReport.Size = new System.Drawing.Size(285, 22);
            this.txtJobKeyForAnnReport.TabIndex = 20;
            // 
            // txtSessionKeyForAnnReport
            // 
            this.txtSessionKeyForAnnReport.Location = new System.Drawing.Point(192, 7);
            this.txtSessionKeyForAnnReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForAnnReport.Name = "txtSessionKeyForAnnReport";
            this.txtSessionKeyForAnnReport.Size = new System.Drawing.Size(285, 22);
            this.txtSessionKeyForAnnReport.TabIndex = 19;
            // 
            // lblJobKeyForRetrieveJob
            // 
            this.lblJobKeyForRetrieveJob.AutoSize = true;
            this.lblJobKeyForRetrieveJob.Location = new System.Drawing.Point(15, 48);
            this.lblJobKeyForRetrieveJob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJobKeyForRetrieveJob.Name = "lblJobKeyForRetrieveJob";
            this.lblJobKeyForRetrieveJob.Size = new System.Drawing.Size(88, 17);
            this.lblJobKeyForRetrieveJob.TabIndex = 18;
            this.lblJobKeyForRetrieveJob.Text = "ApprovalKey";
            // 
            // lblSessionKeyForRetrieveJob
            // 
            this.lblSessionKeyForRetrieveJob.AutoSize = true;
            this.lblSessionKeyForRetrieveJob.Location = new System.Drawing.Point(15, 11);
            this.lblSessionKeyForRetrieveJob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyForRetrieveJob.Name = "lblSessionKeyForRetrieveJob";
            this.lblSessionKeyForRetrieveJob.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyForRetrieveJob.TabIndex = 17;
            this.lblSessionKeyForRetrieveJob.Text = "SessionKey";
            // 
            // tabPageGetAnnotationList
            // 
            this.tabPageGetAnnotationList.Controls.Add(this.txtGetAnnoListUsername);
            this.tabPageGetAnnotationList.Controls.Add(this.lblGetAnnoListUsername);
            this.tabPageGetAnnotationList.Controls.Add(this.label27);
            this.tabPageGetAnnotationList.Controls.Add(this.btnGetAnnotationList);
            this.tabPageGetAnnotationList.Controls.Add(this.Groups);
            this.tabPageGetAnnotationList.Controls.Add(this.dataGridViewAnnotationListGroups);
            this.tabPageGetAnnotationList.Controls.Add(this.label30);
            this.tabPageGetAnnotationList.Controls.Add(this.dataGridViewAnnotationListUsers);
            this.tabPageGetAnnotationList.Controls.Add(this.label29);
            this.tabPageGetAnnotationList.Controls.Add(this.label28);
            this.tabPageGetAnnotationList.Controls.Add(this.cbxFilterByAnnList);
            this.tabPageGetAnnotationList.Controls.Add(this.cbxSortByAnnList);
            this.tabPageGetAnnotationList.Controls.Add(this.txtApprovalKeyAnnotationList);
            this.tabPageGetAnnotationList.Controls.Add(this.label26);
            this.tabPageGetAnnotationList.Controls.Add(this.txtSessionKeyAnnotationList);
            this.tabPageGetAnnotationList.Location = new System.Drawing.Point(4, 46);
            this.tabPageGetAnnotationList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetAnnotationList.Name = "tabPageGetAnnotationList";
            this.tabPageGetAnnotationList.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetAnnotationList.Size = new System.Drawing.Size(579, 929);
            this.tabPageGetAnnotationList.TabIndex = 10;
            this.tabPageGetAnnotationList.Text = "GetAnnotList";
            this.tabPageGetAnnotationList.UseVisualStyleBackColor = true;
            // 
            // txtGetAnnoListUsername
            // 
            this.txtGetAnnoListUsername.Location = new System.Drawing.Point(188, 124);
            this.txtGetAnnoListUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetAnnoListUsername.Name = "txtGetAnnoListUsername";
            this.txtGetAnnoListUsername.Size = new System.Drawing.Size(296, 22);
            this.txtGetAnnoListUsername.TabIndex = 24;
            // 
            // lblGetAnnoListUsername
            // 
            this.lblGetAnnoListUsername.AutoSize = true;
            this.lblGetAnnoListUsername.Location = new System.Drawing.Point(27, 128);
            this.lblGetAnnoListUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetAnnoListUsername.Name = "lblGetAnnoListUsername";
            this.lblGetAnnoListUsername.Size = new System.Drawing.Size(73, 17);
            this.lblGetAnnoListUsername.TabIndex = 23;
            this.lblGetAnnoListUsername.Text = "Username";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(27, 62);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 17);
            this.label27.TabIndex = 13;
            this.label27.Text = "ApprovalGuid";
            // 
            // btnGetAnnotationList
            // 
            this.btnGetAnnotationList.Location = new System.Drawing.Point(364, 654);
            this.btnGetAnnotationList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetAnnotationList.Name = "btnGetAnnotationList";
            this.btnGetAnnotationList.Size = new System.Drawing.Size(131, 28);
            this.btnGetAnnotationList.TabIndex = 12;
            this.btnGetAnnotationList.Text = "Get List";
            this.btnGetAnnotationList.UseVisualStyleBackColor = true;
            this.btnGetAnnotationList.Click += new System.EventHandler(this.btnGetAnnotationList_Click);
            // 
            // Groups
            // 
            this.Groups.AutoSize = true;
            this.Groups.Location = new System.Drawing.Point(27, 433);
            this.Groups.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Groups.Name = "Groups";
            this.Groups.Size = new System.Drawing.Size(55, 17);
            this.Groups.TabIndex = 11;
            this.Groups.Text = "Groups";
            // 
            // dataGridViewAnnotationListGroups
            // 
            this.dataGridViewAnnotationListGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAnnotationListGroups.Location = new System.Drawing.Point(117, 433);
            this.dataGridViewAnnotationListGroups.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewAnnotationListGroups.Name = "dataGridViewAnnotationListGroups";
            this.dataGridViewAnnotationListGroups.Size = new System.Drawing.Size(377, 185);
            this.dataGridViewAnnotationListGroups.TabIndex = 10;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(27, 225);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 17);
            this.label30.TabIndex = 9;
            this.label30.Text = "Users";
            // 
            // dataGridViewAnnotationListUsers
            // 
            this.dataGridViewAnnotationListUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAnnotationListUsers.Location = new System.Drawing.Point(117, 225);
            this.dataGridViewAnnotationListUsers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewAnnotationListUsers.Name = "dataGridViewAnnotationListUsers";
            this.dataGridViewAnnotationListUsers.Size = new System.Drawing.Size(377, 185);
            this.dataGridViewAnnotationListUsers.TabIndex = 8;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(27, 160);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(55, 17);
            this.label29.TabIndex = 7;
            this.label29.Text = "FilterBy";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(27, 95);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(50, 17);
            this.label28.TabIndex = 6;
            this.label28.Text = "SortBy";
            // 
            // cbxFilterByAnnList
            // 
            this.cbxFilterByAnnList.FormattingEnabled = true;
            this.cbxFilterByAnnList.Items.AddRange(new object[] {
            "users",
            "groups"});
            this.cbxFilterByAnnList.Location = new System.Drawing.Point(188, 156);
            this.cbxFilterByAnnList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxFilterByAnnList.Name = "cbxFilterByAnnList";
            this.cbxFilterByAnnList.Size = new System.Drawing.Size(296, 24);
            this.cbxFilterByAnnList.TabIndex = 5;
            // 
            // cbxSortByAnnList
            // 
            this.cbxSortByAnnList.FormattingEnabled = true;
            this.cbxSortByAnnList.Items.AddRange(new object[] {
            "date",
            "user",
            "group"});
            this.cbxSortByAnnList.Location = new System.Drawing.Point(188, 91);
            this.cbxSortByAnnList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxSortByAnnList.Name = "cbxSortByAnnList";
            this.cbxSortByAnnList.Size = new System.Drawing.Size(296, 24);
            this.cbxSortByAnnList.TabIndex = 4;
            // 
            // txtApprovalKeyAnnotationList
            // 
            this.txtApprovalKeyAnnotationList.Location = new System.Drawing.Point(188, 58);
            this.txtApprovalKeyAnnotationList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalKeyAnnotationList.Name = "txtApprovalKeyAnnotationList";
            this.txtApprovalKeyAnnotationList.Size = new System.Drawing.Size(296, 22);
            this.txtApprovalKeyAnnotationList.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(27, 30);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(82, 17);
            this.label26.TabIndex = 1;
            this.label26.Text = "SessionKey";
            // 
            // txtSessionKeyAnnotationList
            // 
            this.txtSessionKeyAnnotationList.Location = new System.Drawing.Point(188, 26);
            this.txtSessionKeyAnnotationList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyAnnotationList.Name = "txtSessionKeyAnnotationList";
            this.txtSessionKeyAnnotationList.Size = new System.Drawing.Size(296, 22);
            this.txtSessionKeyAnnotationList.TabIndex = 0;
            // 
            // tabPageGetAllVersionsAnnotReport
            // 
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label54);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.ckbSumaryPageJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.btnGetAllVersionsReportURL);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.txtUserNameJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.dataGridViewAllVersionsReportGroups);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.dataGridViewAllVersionsReportUsers);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label48);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.txtCommentsPerPageJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.cbxFilterByJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label49);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label50);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.cbxDisplayOptJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label51);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.cbxSortByJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.txtJobKeyJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.txtSessionKeyJobAnnotationReport);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label52);
            this.tabPageGetAllVersionsAnnotReport.Controls.Add(this.label53);
            this.tabPageGetAllVersionsAnnotReport.Location = new System.Drawing.Point(4, 46);
            this.tabPageGetAllVersionsAnnotReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetAllVersionsAnnotReport.Name = "tabPageGetAllVersionsAnnotReport";
            this.tabPageGetAllVersionsAnnotReport.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetAllVersionsAnnotReport.Size = new System.Drawing.Size(579, 929);
            this.tabPageGetAllVersionsAnnotReport.TabIndex = 24;
            this.tabPageGetAllVersionsAnnotReport.Text = "GetAllVersionsAnnotReport";
            this.tabPageGetAllVersionsAnnotReport.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(19, 214);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(73, 17);
            this.label54.TabIndex = 41;
            this.label54.Text = "Username";
            // 
            // ckbSumaryPageJobAnnotationReport
            // 
            this.ckbSumaryPageJobAnnotationReport.AutoSize = true;
            this.ckbSumaryPageJobAnnotationReport.Location = new System.Drawing.Point(159, 242);
            this.ckbSumaryPageJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ckbSumaryPageJobAnnotationReport.Name = "ckbSumaryPageJobAnnotationReport";
            this.ckbSumaryPageJobAnnotationReport.Size = new System.Drawing.Size(167, 21);
            this.ckbSumaryPageJobAnnotationReport.TabIndex = 40;
            this.ckbSumaryPageJobAnnotationReport.Text = "IncludeSummaryPage";
            this.ckbSumaryPageJobAnnotationReport.UseVisualStyleBackColor = true;
            // 
            // btnGetAllVersionsReportURL
            // 
            this.btnGetAllVersionsReportURL.Location = new System.Drawing.Point(23, 580);
            this.btnGetAllVersionsReportURL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetAllVersionsReportURL.Name = "btnGetAllVersionsReportURL";
            this.btnGetAllVersionsReportURL.Size = new System.Drawing.Size(131, 28);
            this.btnGetAllVersionsReportURL.TabIndex = 39;
            this.btnGetAllVersionsReportURL.Text = "Get URL";
            this.btnGetAllVersionsReportURL.UseVisualStyleBackColor = true;
            this.btnGetAllVersionsReportURL.Click += new System.EventHandler(this.btnGetJobAnnotationReportURL_Click);
            // 
            // txtUserNameJobAnnotationReport
            // 
            this.txtUserNameJobAnnotationReport.Location = new System.Drawing.Point(159, 210);
            this.txtUserNameJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserNameJobAnnotationReport.Name = "txtUserNameJobAnnotationReport";
            this.txtUserNameJobAnnotationReport.Size = new System.Drawing.Size(385, 22);
            this.txtUserNameJobAnnotationReport.TabIndex = 38;
            // 
            // dataGridViewAllVersionsReportGroups
            // 
            this.dataGridViewAllVersionsReportGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAllVersionsReportGroups.Location = new System.Drawing.Point(159, 423);
            this.dataGridViewAllVersionsReportGroups.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewAllVersionsReportGroups.Name = "dataGridViewAllVersionsReportGroups";
            this.dataGridViewAllVersionsReportGroups.Size = new System.Drawing.Size(387, 112);
            this.dataGridViewAllVersionsReportGroups.TabIndex = 35;
            // 
            // dataGridViewAllVersionsReportUsers
            // 
            this.dataGridViewAllVersionsReportUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAllVersionsReportUsers.Location = new System.Drawing.Point(159, 271);
            this.dataGridViewAllVersionsReportUsers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewAllVersionsReportUsers.Name = "dataGridViewAllVersionsReportUsers";
            this.dataGridViewAllVersionsReportUsers.Size = new System.Drawing.Size(387, 145);
            this.dataGridViewAllVersionsReportUsers.TabIndex = 32;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(19, 177);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(129, 17);
            this.label48.TabIndex = 29;
            this.label48.Text = "CommentsPerPage";
            // 
            // txtCommentsPerPageJobAnnotationReport
            // 
            this.txtCommentsPerPageJobAnnotationReport.Location = new System.Drawing.Point(159, 177);
            this.txtCommentsPerPageJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCommentsPerPageJobAnnotationReport.Name = "txtCommentsPerPageJobAnnotationReport";
            this.txtCommentsPerPageJobAnnotationReport.Size = new System.Drawing.Size(385, 22);
            this.txtCommentsPerPageJobAnnotationReport.TabIndex = 28;
            // 
            // cbxFilterByJobAnnotationReport
            // 
            this.cbxFilterByJobAnnotationReport.FormattingEnabled = true;
            this.cbxFilterByJobAnnotationReport.Items.AddRange(new object[] {
            "users",
            "groups"});
            this.cbxFilterByJobAnnotationReport.Location = new System.Drawing.Point(159, 143);
            this.cbxFilterByJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxFilterByJobAnnotationReport.Name = "cbxFilterByJobAnnotationReport";
            this.cbxFilterByJobAnnotationReport.Size = new System.Drawing.Size(385, 24);
            this.cbxFilterByJobAnnotationReport.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(19, 143);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(55, 17);
            this.label49.TabIndex = 26;
            this.label49.Text = "FilterBy";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(19, 108);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(77, 17);
            this.label50.TabIndex = 25;
            this.label50.Text = "DisplayOpt";
            // 
            // cbxDisplayOptJobAnnotationReport
            // 
            this.cbxDisplayOptJobAnnotationReport.FormattingEnabled = true;
            this.cbxDisplayOptJobAnnotationReport.Items.AddRange(new object[] {
            "comments",
            "pins",
            "commentsandpins"});
            this.cbxDisplayOptJobAnnotationReport.Location = new System.Drawing.Point(159, 108);
            this.cbxDisplayOptJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxDisplayOptJobAnnotationReport.Name = "cbxDisplayOptJobAnnotationReport";
            this.cbxDisplayOptJobAnnotationReport.Size = new System.Drawing.Size(385, 24);
            this.cbxDisplayOptJobAnnotationReport.TabIndex = 24;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(19, 74);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(50, 17);
            this.label51.TabIndex = 23;
            this.label51.Text = "SortBy";
            // 
            // cbxSortByJobAnnotationReport
            // 
            this.cbxSortByJobAnnotationReport.FormattingEnabled = true;
            this.cbxSortByJobAnnotationReport.Items.AddRange(new object[] {
            "date",
            "user",
            "group"});
            this.cbxSortByJobAnnotationReport.Location = new System.Drawing.Point(159, 74);
            this.cbxSortByJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxSortByJobAnnotationReport.Name = "cbxSortByJobAnnotationReport";
            this.cbxSortByJobAnnotationReport.Size = new System.Drawing.Size(385, 24);
            this.cbxSortByJobAnnotationReport.TabIndex = 22;
            // 
            // txtJobKeyJobAnnotationReport
            // 
            this.txtJobKeyJobAnnotationReport.Location = new System.Drawing.Point(159, 41);
            this.txtJobKeyJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJobKeyJobAnnotationReport.Name = "txtJobKeyJobAnnotationReport";
            this.txtJobKeyJobAnnotationReport.Size = new System.Drawing.Size(385, 22);
            this.txtJobKeyJobAnnotationReport.TabIndex = 20;
            // 
            // txtSessionKeyJobAnnotationReport
            // 
            this.txtSessionKeyJobAnnotationReport.Location = new System.Drawing.Point(159, 7);
            this.txtSessionKeyJobAnnotationReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyJobAnnotationReport.Name = "txtSessionKeyJobAnnotationReport";
            this.txtSessionKeyJobAnnotationReport.Size = new System.Drawing.Size(385, 22);
            this.txtSessionKeyJobAnnotationReport.TabIndex = 19;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(19, 41);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(55, 17);
            this.label52.TabIndex = 18;
            this.label52.Text = "JobKey";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(19, 11);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(82, 17);
            this.label53.TabIndex = 17;
            this.label53.Text = "SessionKey";
            // 
            // tabPageUpdateJob
            // 
            this.tabPageUpdateJob.Controls.Add(this.cbxTestPushJobStatus);
            this.tabPageUpdateJob.Controls.Add(this.txtUpdateJobUsername);
            this.tabPageUpdateJob.Controls.Add(this.lblUpdateJobUsername);
            this.tabPageUpdateJob.Controls.Add(this.btnUpdateJob);
            this.tabPageUpdateJob.Controls.Add(this.lblJobStatusUpdateJob);
            this.tabPageUpdateJob.Controls.Add(this.cbxJobStatUpdJob);
            this.tabPageUpdateJob.Controls.Add(this.txtJobTitleUpdJob);
            this.tabPageUpdateJob.Controls.Add(this.lblJobTitleUpdJob);
            this.tabPageUpdateJob.Controls.Add(this.jobKeyForUpdJob);
            this.tabPageUpdateJob.Controls.Add(this.txtSessionKeyForUpdJob);
            this.tabPageUpdateJob.Controls.Add(this.lblJobKeyUpdateJob);
            this.tabPageUpdateJob.Controls.Add(this.lblSessKeyUpdJob);
            this.tabPageUpdateJob.Location = new System.Drawing.Point(4, 46);
            this.tabPageUpdateJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageUpdateJob.Name = "tabPageUpdateJob";
            this.tabPageUpdateJob.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageUpdateJob.Size = new System.Drawing.Size(579, 929);
            this.tabPageUpdateJob.TabIndex = 11;
            this.tabPageUpdateJob.Text = "Update Job";
            this.tabPageUpdateJob.UseVisualStyleBackColor = true;
            // 
            // cbxTestPushJobStatus
            // 
            this.cbxTestPushJobStatus.AutoSize = true;
            this.cbxTestPushJobStatus.Location = new System.Drawing.Point(197, 187);
            this.cbxTestPushJobStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxTestPushJobStatus.Name = "cbxTestPushJobStatus";
            this.cbxTestPushJobStatus.Size = new System.Drawing.Size(257, 38);
            this.cbxTestPushJobStatus.TabIndex = 23;
            this.cbxTestPushJobStatus.Text = "Simulate \"push status\"\r\n(option is used only fo test purpose)";
            this.cbxTestPushJobStatus.UseVisualStyleBackColor = true;
            // 
            // txtUpdateJobUsername
            // 
            this.txtUpdateJobUsername.Location = new System.Drawing.Point(197, 146);
            this.txtUpdateJobUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateJobUsername.Name = "txtUpdateJobUsername";
            this.txtUpdateJobUsername.Size = new System.Drawing.Size(296, 22);
            this.txtUpdateJobUsername.TabIndex = 22;
            // 
            // lblUpdateJobUsername
            // 
            this.lblUpdateJobUsername.AutoSize = true;
            this.lblUpdateJobUsername.Location = new System.Drawing.Point(19, 155);
            this.lblUpdateJobUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpdateJobUsername.Name = "lblUpdateJobUsername";
            this.lblUpdateJobUsername.Size = new System.Drawing.Size(73, 17);
            this.lblUpdateJobUsername.TabIndex = 21;
            this.lblUpdateJobUsername.Text = "Username";
            // 
            // btnUpdateJob
            // 
            this.btnUpdateJob.Location = new System.Drawing.Point(365, 228);
            this.btnUpdateJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdateJob.Name = "btnUpdateJob";
            this.btnUpdateJob.Size = new System.Drawing.Size(129, 28);
            this.btnUpdateJob.TabIndex = 8;
            this.btnUpdateJob.Text = "Update Job";
            this.btnUpdateJob.UseVisualStyleBackColor = true;
            this.btnUpdateJob.Click += new System.EventHandler(this.btnUpdateJob_Click);
            // 
            // lblJobStatusUpdateJob
            // 
            this.lblJobStatusUpdateJob.AutoSize = true;
            this.lblJobStatusUpdateJob.Location = new System.Drawing.Point(20, 123);
            this.lblJobStatusUpdateJob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJobStatusUpdateJob.Name = "lblJobStatusUpdateJob";
            this.lblJobStatusUpdateJob.Size = new System.Drawing.Size(75, 17);
            this.lblJobStatusUpdateJob.TabIndex = 7;
            this.lblJobStatusUpdateJob.Text = "Job Status";
            // 
            // cbxJobStatUpdJob
            // 
            this.cbxJobStatUpdJob.FormattingEnabled = true;
            this.cbxJobStatUpdJob.Items.AddRange(new object[] {
            "None",
            "In Progress",
            "Changes Required",
            "Changes Complete",
            "Completed",
            "Archived"});
            this.cbxJobStatUpdJob.Location = new System.Drawing.Point(197, 113);
            this.cbxJobStatUpdJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxJobStatUpdJob.Name = "cbxJobStatUpdJob";
            this.cbxJobStatUpdJob.Size = new System.Drawing.Size(296, 24);
            this.cbxJobStatUpdJob.TabIndex = 6;
            this.cbxJobStatUpdJob.SelectedIndexChanged += new System.EventHandler(this.cbxJobStatUpdJob_SelectedIndexChanged);
            // 
            // txtJobTitleUpdJob
            // 
            this.txtJobTitleUpdJob.Location = new System.Drawing.Point(197, 81);
            this.txtJobTitleUpdJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtJobTitleUpdJob.Name = "txtJobTitleUpdJob";
            this.txtJobTitleUpdJob.Size = new System.Drawing.Size(296, 22);
            this.txtJobTitleUpdJob.TabIndex = 5;
            // 
            // lblJobTitleUpdJob
            // 
            this.lblJobTitleUpdJob.AutoSize = true;
            this.lblJobTitleUpdJob.Location = new System.Drawing.Point(20, 90);
            this.lblJobTitleUpdJob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJobTitleUpdJob.Name = "lblJobTitleUpdJob";
            this.lblJobTitleUpdJob.Size = new System.Drawing.Size(62, 17);
            this.lblJobTitleUpdJob.TabIndex = 4;
            this.lblJobTitleUpdJob.Text = "Job Title";
            // 
            // jobKeyForUpdJob
            // 
            this.jobKeyForUpdJob.Location = new System.Drawing.Point(197, 49);
            this.jobKeyForUpdJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.jobKeyForUpdJob.Name = "jobKeyForUpdJob";
            this.jobKeyForUpdJob.Size = new System.Drawing.Size(296, 22);
            this.jobKeyForUpdJob.TabIndex = 3;
            this.jobKeyForUpdJob.TextChanged += new System.EventHandler(this.jobKeyForUpdJob_TextChanged);
            // 
            // txtSessionKeyForUpdJob
            // 
            this.txtSessionKeyForUpdJob.Location = new System.Drawing.Point(197, 17);
            this.txtSessionKeyForUpdJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForUpdJob.Name = "txtSessionKeyForUpdJob";
            this.txtSessionKeyForUpdJob.Size = new System.Drawing.Size(296, 22);
            this.txtSessionKeyForUpdJob.TabIndex = 2;
            // 
            // lblJobKeyUpdateJob
            // 
            this.lblJobKeyUpdateJob.AutoSize = true;
            this.lblJobKeyUpdateJob.Location = new System.Drawing.Point(20, 53);
            this.lblJobKeyUpdateJob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblJobKeyUpdateJob.Name = "lblJobKeyUpdateJob";
            this.lblJobKeyUpdateJob.Size = new System.Drawing.Size(55, 17);
            this.lblJobKeyUpdateJob.TabIndex = 1;
            this.lblJobKeyUpdateJob.Text = "JobKey";
            // 
            // lblSessKeyUpdJob
            // 
            this.lblSessKeyUpdJob.AutoSize = true;
            this.lblSessKeyUpdJob.Location = new System.Drawing.Point(19, 21);
            this.lblSessKeyUpdJob.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessKeyUpdJob.Name = "lblSessKeyUpdJob";
            this.lblSessKeyUpdJob.Size = new System.Drawing.Size(82, 17);
            this.lblSessKeyUpdJob.TabIndex = 0;
            this.lblSessKeyUpdJob.Text = "SessionKey";
            // 
            // tabPageDeleteApproval
            // 
            this.tabPageDeleteApproval.Controls.Add(this.txtDeleteApprovalUsername);
            this.tabPageDeleteApproval.Controls.Add(this.lblDeleteApprovalUsername);
            this.tabPageDeleteApproval.Controls.Add(this.btnDeleteApproval);
            this.tabPageDeleteApproval.Controls.Add(this.txtDeleteAppKey);
            this.tabPageDeleteApproval.Controls.Add(this.txtSessionKeyDeleteApp);
            this.tabPageDeleteApproval.Controls.Add(this.lblTransferJobJobKey);
            this.tabPageDeleteApproval.Controls.Add(this.lblTransferJobSessionKey);
            this.tabPageDeleteApproval.Location = new System.Drawing.Point(4, 46);
            this.tabPageDeleteApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDeleteApproval.Name = "tabPageDeleteApproval";
            this.tabPageDeleteApproval.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDeleteApproval.Size = new System.Drawing.Size(579, 929);
            this.tabPageDeleteApproval.TabIndex = 2;
            this.tabPageDeleteApproval.Text = "DeleteAp.";
            this.tabPageDeleteApproval.UseVisualStyleBackColor = true;
            // 
            // txtDeleteApprovalUsername
            // 
            this.txtDeleteApprovalUsername.Location = new System.Drawing.Point(192, 76);
            this.txtDeleteApprovalUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDeleteApprovalUsername.Name = "txtDeleteApprovalUsername";
            this.txtDeleteApprovalUsername.Size = new System.Drawing.Size(305, 22);
            this.txtDeleteApprovalUsername.TabIndex = 20;
            // 
            // lblDeleteApprovalUsername
            // 
            this.lblDeleteApprovalUsername.AutoSize = true;
            this.lblDeleteApprovalUsername.Location = new System.Drawing.Point(15, 85);
            this.lblDeleteApprovalUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeleteApprovalUsername.Name = "lblDeleteApprovalUsername";
            this.lblDeleteApprovalUsername.Size = new System.Drawing.Size(73, 17);
            this.lblDeleteApprovalUsername.TabIndex = 19;
            this.lblDeleteApprovalUsername.Text = "Username";
            // 
            // btnDeleteApproval
            // 
            this.btnDeleteApproval.Location = new System.Drawing.Point(395, 124);
            this.btnDeleteApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteApproval.Name = "btnDeleteApproval";
            this.btnDeleteApproval.Size = new System.Drawing.Size(100, 28);
            this.btnDeleteApproval.TabIndex = 18;
            this.btnDeleteApproval.Text = "Delete";
            this.btnDeleteApproval.UseVisualStyleBackColor = true;
            this.btnDeleteApproval.Click += new System.EventHandler(this.btnDeleteApproval_Click);
            // 
            // txtDeleteAppKey
            // 
            this.txtDeleteAppKey.Location = new System.Drawing.Point(192, 44);
            this.txtDeleteAppKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDeleteAppKey.Name = "txtDeleteAppKey";
            this.txtDeleteAppKey.Size = new System.Drawing.Size(305, 22);
            this.txtDeleteAppKey.TabIndex = 9;
            // 
            // txtSessionKeyDeleteApp
            // 
            this.txtSessionKeyDeleteApp.Location = new System.Drawing.Point(192, 7);
            this.txtSessionKeyDeleteApp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyDeleteApp.Name = "txtSessionKeyDeleteApp";
            this.txtSessionKeyDeleteApp.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyDeleteApp.TabIndex = 8;
            // 
            // lblTransferJobJobKey
            // 
            this.lblTransferJobJobKey.AutoSize = true;
            this.lblTransferJobJobKey.Location = new System.Drawing.Point(15, 48);
            this.lblTransferJobJobKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTransferJobJobKey.Name = "lblTransferJobJobKey";
            this.lblTransferJobJobKey.Size = new System.Drawing.Size(55, 17);
            this.lblTransferJobJobKey.TabIndex = 7;
            this.lblTransferJobJobKey.Text = "JobKey";
            // 
            // lblTransferJobSessionKey
            // 
            this.lblTransferJobSessionKey.AutoSize = true;
            this.lblTransferJobSessionKey.Location = new System.Drawing.Point(15, 11);
            this.lblTransferJobSessionKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTransferJobSessionKey.Name = "lblTransferJobSessionKey";
            this.lblTransferJobSessionKey.Size = new System.Drawing.Size(82, 17);
            this.lblTransferJobSessionKey.TabIndex = 6;
            this.lblTransferJobSessionKey.Text = "SessionKey";
            // 
            // tabPageDeleteJob
            // 
            this.tabPageDeleteJob.Controls.Add(this.txtDeleteJobUsername);
            this.tabPageDeleteJob.Controls.Add(this.lblDeleteJobUsername);
            this.tabPageDeleteJob.Controls.Add(this.btnDeleteJob);
            this.tabPageDeleteJob.Controls.Add(this.txtDeleteJobKey);
            this.tabPageDeleteJob.Controls.Add(this.txtDeleteSessionKey);
            this.tabPageDeleteJob.Controls.Add(this.label3);
            this.tabPageDeleteJob.Controls.Add(this.label2);
            this.tabPageDeleteJob.Location = new System.Drawing.Point(4, 46);
            this.tabPageDeleteJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDeleteJob.Name = "tabPageDeleteJob";
            this.tabPageDeleteJob.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageDeleteJob.Size = new System.Drawing.Size(579, 929);
            this.tabPageDeleteJob.TabIndex = 3;
            this.tabPageDeleteJob.Text = "DeleteJob ";
            this.tabPageDeleteJob.UseVisualStyleBackColor = true;
            // 
            // txtDeleteJobUsername
            // 
            this.txtDeleteJobUsername.Location = new System.Drawing.Point(192, 80);
            this.txtDeleteJobUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDeleteJobUsername.Name = "txtDeleteJobUsername";
            this.txtDeleteJobUsername.Size = new System.Drawing.Size(301, 22);
            this.txtDeleteJobUsername.TabIndex = 10;
            // 
            // lblDeleteJobUsername
            // 
            this.lblDeleteJobUsername.AutoSize = true;
            this.lblDeleteJobUsername.Location = new System.Drawing.Point(12, 80);
            this.lblDeleteJobUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeleteJobUsername.Name = "lblDeleteJobUsername";
            this.lblDeleteJobUsername.Size = new System.Drawing.Size(73, 17);
            this.lblDeleteJobUsername.TabIndex = 9;
            this.lblDeleteJobUsername.Text = "Username";
            // 
            // btnDeleteJob
            // 
            this.btnDeleteJob.Location = new System.Drawing.Point(395, 130);
            this.btnDeleteJob.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDeleteJob.Name = "btnDeleteJob";
            this.btnDeleteJob.Size = new System.Drawing.Size(100, 28);
            this.btnDeleteJob.TabIndex = 6;
            this.btnDeleteJob.Text = "DeleteJob";
            this.btnDeleteJob.UseVisualStyleBackColor = true;
            this.btnDeleteJob.Click += new System.EventHandler(this.btnDeleteJob_Click);
            // 
            // txtDeleteJobKey
            // 
            this.txtDeleteJobKey.Location = new System.Drawing.Point(192, 44);
            this.txtDeleteJobKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDeleteJobKey.Name = "txtDeleteJobKey";
            this.txtDeleteJobKey.Size = new System.Drawing.Size(301, 22);
            this.txtDeleteJobKey.TabIndex = 5;
            // 
            // txtDeleteSessionKey
            // 
            this.txtDeleteSessionKey.Location = new System.Drawing.Point(192, 7);
            this.txtDeleteSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtDeleteSessionKey.Name = "txtDeleteSessionKey";
            this.txtDeleteSessionKey.Size = new System.Drawing.Size(301, 22);
            this.txtDeleteSessionKey.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "JobKey";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "SessionKey";
            // 
            // tabRestoreApproval
            // 
            this.tabRestoreApproval.Controls.Add(this.txtRestoreAppUsername);
            this.tabRestoreApproval.Controls.Add(this.lblRestoreAppUsername);
            this.tabRestoreApproval.Controls.Add(this.label31);
            this.tabRestoreApproval.Controls.Add(this.txtApprovalRestoreKey);
            this.tabRestoreApproval.Controls.Add(this.btnRestoreApproval);
            this.tabRestoreApproval.Controls.Add(this.txtSessionKeyForRestoreApp);
            this.tabRestoreApproval.Controls.Add(this.label7);
            this.tabRestoreApproval.Location = new System.Drawing.Point(4, 46);
            this.tabRestoreApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabRestoreApproval.Name = "tabRestoreApproval";
            this.tabRestoreApproval.Size = new System.Drawing.Size(579, 929);
            this.tabRestoreApproval.TabIndex = 6;
            this.tabRestoreApproval.Text = "RestoreAp.";
            this.tabRestoreApproval.UseVisualStyleBackColor = true;
            // 
            // txtRestoreAppUsername
            // 
            this.txtRestoreAppUsername.Location = new System.Drawing.Point(192, 71);
            this.txtRestoreAppUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRestoreAppUsername.Name = "txtRestoreAppUsername";
            this.txtRestoreAppUsername.Size = new System.Drawing.Size(305, 22);
            this.txtRestoreAppUsername.TabIndex = 13;
            // 
            // lblRestoreAppUsername
            // 
            this.lblRestoreAppUsername.AutoSize = true;
            this.lblRestoreAppUsername.Location = new System.Drawing.Point(15, 75);
            this.lblRestoreAppUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRestoreAppUsername.Name = "lblRestoreAppUsername";
            this.lblRestoreAppUsername.Size = new System.Drawing.Size(73, 17);
            this.lblRestoreAppUsername.TabIndex = 12;
            this.lblRestoreAppUsername.Text = "Username";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(15, 43);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(98, 17);
            this.label31.TabIndex = 11;
            this.label31.Text = "Approval Guid";
            // 
            // txtApprovalRestoreKey
            // 
            this.txtApprovalRestoreKey.Location = new System.Drawing.Point(192, 39);
            this.txtApprovalRestoreKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalRestoreKey.Name = "txtApprovalRestoreKey";
            this.txtApprovalRestoreKey.Size = new System.Drawing.Size(305, 22);
            this.txtApprovalRestoreKey.TabIndex = 10;
            // 
            // btnRestoreApproval
            // 
            this.btnRestoreApproval.Location = new System.Drawing.Point(399, 103);
            this.btnRestoreApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRestoreApproval.Name = "btnRestoreApproval";
            this.btnRestoreApproval.Size = new System.Drawing.Size(100, 28);
            this.btnRestoreApproval.TabIndex = 9;
            this.btnRestoreApproval.Text = "Restore";
            this.btnRestoreApproval.UseVisualStyleBackColor = true;
            this.btnRestoreApproval.Click += new System.EventHandler(this.btnRestoreApproval_Click);
            // 
            // txtSessionKeyForRestoreApp
            // 
            this.txtSessionKeyForRestoreApp.Location = new System.Drawing.Point(192, 7);
            this.txtSessionKeyForRestoreApp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForRestoreApp.Name = "txtSessionKeyForRestoreApp";
            this.txtSessionKeyForRestoreApp.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyForRestoreApp.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "SessionKey";
            // 
            // tabPageGetApproval
            // 
            this.tabPageGetApproval.Controls.Add(this.txtGetApprovalUsername);
            this.tabPageGetApproval.Controls.Add(this.lblGetApprovalUsername);
            this.tabPageGetApproval.Controls.Add(this.btnGetAppAccess);
            this.tabPageGetApproval.Controls.Add(this.pictBxGetApp);
            this.tabPageGetApproval.Controls.Add(this.btnGetApp);
            this.tabPageGetApproval.Controls.Add(this.txtGetAppForAppGuid);
            this.tabPageGetApproval.Controls.Add(this.txtSessionKeyForGetApp);
            this.tabPageGetApproval.Controls.Add(this.labGetAppGuid);
            this.tabPageGetApproval.Controls.Add(this.lblGetAppSK);
            this.tabPageGetApproval.Location = new System.Drawing.Point(4, 67);
            this.tabPageGetApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetApproval.Name = "tabPageGetApproval";
            this.tabPageGetApproval.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageGetApproval.Size = new System.Drawing.Size(579, 908);
            this.tabPageGetApproval.TabIndex = 12;
            this.tabPageGetApproval.Text = "GetApproval";
            this.tabPageGetApproval.UseVisualStyleBackColor = true;
            // 
            // txtGetApprovalUsername
            // 
            this.txtGetApprovalUsername.Location = new System.Drawing.Point(203, 89);
            this.txtGetApprovalUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetApprovalUsername.Name = "txtGetApprovalUsername";
            this.txtGetApprovalUsername.Size = new System.Drawing.Size(272, 22);
            this.txtGetApprovalUsername.TabIndex = 8;
            // 
            // lblGetApprovalUsername
            // 
            this.lblGetApprovalUsername.AutoSize = true;
            this.lblGetApprovalUsername.Location = new System.Drawing.Point(23, 92);
            this.lblGetApprovalUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetApprovalUsername.Name = "lblGetApprovalUsername";
            this.lblGetApprovalUsername.Size = new System.Drawing.Size(73, 17);
            this.lblGetApprovalUsername.TabIndex = 7;
            this.lblGetApprovalUsername.Text = "Username";
            // 
            // btnGetAppAccess
            // 
            this.btnGetAppAccess.Location = new System.Drawing.Point(203, 154);
            this.btnGetAppAccess.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetAppAccess.Name = "btnGetAppAccess";
            this.btnGetAppAccess.Size = new System.Drawing.Size(100, 28);
            this.btnGetAppAccess.TabIndex = 6;
            this.btnGetAppAccess.Text = "GetWithAcc";
            this.btnGetAppAccess.UseVisualStyleBackColor = true;
            this.btnGetAppAccess.Click += new System.EventHandler(this.btnGetAppAccess_Click);
            // 
            // pictBxGetApp
            // 
            this.pictBxGetApp.Location = new System.Drawing.Point(132, 265);
            this.pictBxGetApp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictBxGetApp.Name = "pictBxGetApp";
            this.pictBxGetApp.Size = new System.Drawing.Size(252, 277);
            this.pictBxGetApp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictBxGetApp.TabIndex = 5;
            this.pictBxGetApp.TabStop = false;
            // 
            // btnGetApp
            // 
            this.btnGetApp.Location = new System.Drawing.Point(376, 154);
            this.btnGetApp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetApp.Name = "btnGetApp";
            this.btnGetApp.Size = new System.Drawing.Size(100, 28);
            this.btnGetApp.TabIndex = 4;
            this.btnGetApp.Text = "Get";
            this.btnGetApp.UseVisualStyleBackColor = true;
            this.btnGetApp.Click += new System.EventHandler(this.btnGetApp_Click);
            // 
            // txtGetAppForAppGuid
            // 
            this.txtGetAppForAppGuid.Location = new System.Drawing.Point(203, 57);
            this.txtGetAppForAppGuid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetAppForAppGuid.Name = "txtGetAppForAppGuid";
            this.txtGetAppForAppGuid.Size = new System.Drawing.Size(272, 22);
            this.txtGetAppForAppGuid.TabIndex = 3;
            // 
            // txtSessionKeyForGetApp
            // 
            this.txtSessionKeyForGetApp.Location = new System.Drawing.Point(203, 25);
            this.txtSessionKeyForGetApp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForGetApp.Name = "txtSessionKeyForGetApp";
            this.txtSessionKeyForGetApp.Size = new System.Drawing.Size(272, 22);
            this.txtSessionKeyForGetApp.TabIndex = 2;
            // 
            // labGetAppGuid
            // 
            this.labGetAppGuid.AutoSize = true;
            this.labGetAppGuid.Location = new System.Drawing.Point(23, 60);
            this.labGetAppGuid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labGetAppGuid.Name = "labGetAppGuid";
            this.labGetAppGuid.Size = new System.Drawing.Size(98, 17);
            this.labGetAppGuid.TabIndex = 1;
            this.labGetAppGuid.Text = "Approval Guid";
            // 
            // lblGetAppSK
            // 
            this.lblGetAppSK.AutoSize = true;
            this.lblGetAppSK.Location = new System.Drawing.Point(23, 28);
            this.lblGetAppSK.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetAppSK.Name = "lblGetAppSK";
            this.lblGetAppSK.Size = new System.Drawing.Size(82, 17);
            this.lblGetAppSK.TabIndex = 0;
            this.lblGetAppSK.Text = "SessionKey";
            // 
            // tabUpdateApproval
            // 
            this.tabUpdateApproval.Controls.Add(this.txtUpdateApprovalUsername);
            this.tabUpdateApproval.Controls.Add(this.lblUpdateApprovalUsername);
            this.tabUpdateApproval.Controls.Add(this.txtUpdateApprovalKey);
            this.tabUpdateApproval.Controls.Add(this.lblApprovalKey);
            this.tabUpdateApproval.Controls.Add(this.label34);
            this.tabUpdateApproval.Controls.Add(this.updateApprovalDeadline);
            this.tabUpdateApproval.Controls.Add(this.dataGridViewUpdateCollaborators);
            this.tabUpdateApproval.Controls.Add(this.btnUpdateApproval);
            this.tabUpdateApproval.Controls.Add(this.label38);
            this.tabUpdateApproval.Controls.Add(this.txtUpdateApprovalSessionKey);
            this.tabUpdateApproval.Controls.Add(this.label41);
            this.tabUpdateApproval.Location = new System.Drawing.Point(4, 67);
            this.tabUpdateApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUpdateApproval.Name = "tabUpdateApproval";
            this.tabUpdateApproval.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUpdateApproval.Size = new System.Drawing.Size(579, 908);
            this.tabUpdateApproval.TabIndex = 13;
            this.tabUpdateApproval.Text = "UpdateApproval";
            this.tabUpdateApproval.UseVisualStyleBackColor = true;
            // 
            // txtUpdateApprovalUsername
            // 
            this.txtUpdateApprovalUsername.Location = new System.Drawing.Point(184, 132);
            this.txtUpdateApprovalUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateApprovalUsername.Name = "txtUpdateApprovalUsername";
            this.txtUpdateApprovalUsername.Size = new System.Drawing.Size(305, 22);
            this.txtUpdateApprovalUsername.TabIndex = 68;
            // 
            // lblUpdateApprovalUsername
            // 
            this.lblUpdateApprovalUsername.AutoSize = true;
            this.lblUpdateApprovalUsername.Location = new System.Drawing.Point(11, 135);
            this.lblUpdateApprovalUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpdateApprovalUsername.Name = "lblUpdateApprovalUsername";
            this.lblUpdateApprovalUsername.Size = new System.Drawing.Size(73, 17);
            this.lblUpdateApprovalUsername.TabIndex = 67;
            this.lblUpdateApprovalUsername.Text = "Username";
            // 
            // txtUpdateApprovalKey
            // 
            this.txtUpdateApprovalKey.Location = new System.Drawing.Point(185, 47);
            this.txtUpdateApprovalKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateApprovalKey.Name = "txtUpdateApprovalKey";
            this.txtUpdateApprovalKey.Size = new System.Drawing.Size(304, 22);
            this.txtUpdateApprovalKey.TabIndex = 66;
            this.txtUpdateApprovalKey.TextChanged += new System.EventHandler(this.txtUpdateApprovalKey_TextChanged);
            // 
            // lblApprovalKey
            // 
            this.lblApprovalKey.AutoSize = true;
            this.lblApprovalKey.Location = new System.Drawing.Point(11, 50);
            this.lblApprovalKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalKey.Name = "lblApprovalKey";
            this.lblApprovalKey.Size = new System.Drawing.Size(90, 17);
            this.lblApprovalKey.TabIndex = 65;
            this.lblApprovalKey.Text = "Approval key";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 192);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(92, 17);
            this.label34.TabIndex = 63;
            this.label34.Text = "Collaborators";
            // 
            // updateApprovalDeadline
            // 
            this.updateApprovalDeadline.Location = new System.Drawing.Point(184, 89);
            this.updateApprovalDeadline.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updateApprovalDeadline.Name = "updateApprovalDeadline";
            this.updateApprovalDeadline.Size = new System.Drawing.Size(305, 22);
            this.updateApprovalDeadline.TabIndex = 62;
            // 
            // dataGridViewUpdateCollaborators
            // 
            this.dataGridViewUpdateCollaborators.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUpdateCollaborators.Location = new System.Drawing.Point(15, 226);
            this.dataGridViewUpdateCollaborators.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewUpdateCollaborators.Name = "dataGridViewUpdateCollaborators";
            this.dataGridViewUpdateCollaborators.Size = new System.Drawing.Size(480, 255);
            this.dataGridViewUpdateCollaborators.TabIndex = 61;
            // 
            // btnUpdateApproval
            // 
            this.btnUpdateApproval.Location = new System.Drawing.Point(343, 673);
            this.btnUpdateApproval.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdateApproval.Name = "btnUpdateApproval";
            this.btnUpdateApproval.Size = new System.Drawing.Size(149, 28);
            this.btnUpdateApproval.TabIndex = 53;
            this.btnUpdateApproval.TabStop = false;
            this.btnUpdateApproval.Text = "Update Approval";
            this.btnUpdateApproval.UseVisualStyleBackColor = true;
            this.btnUpdateApproval.Click += new System.EventHandler(this.btnUpdateApproval_Click);
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(11, 89);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(153, 21);
            this.label38.TabIndex = 52;
            this.label38.Text = "Deadline";
            // 
            // txtUpdateApprovalSessionKey
            // 
            this.txtUpdateApprovalSessionKey.Location = new System.Drawing.Point(185, 7);
            this.txtUpdateApprovalSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateApprovalSessionKey.Name = "txtUpdateApprovalSessionKey";
            this.txtUpdateApprovalSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtUpdateApprovalSessionKey.TabIndex = 45;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(8, 11);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 17);
            this.label41.TabIndex = 44;
            this.label41.Text = "SessionKey";
            // 
            // tabUpdateApprovalPhaseDeadline
            // 
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.txtUpdateApprovalPhaseDeadlineUsername);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.lblUpdateApprovalPhaseDeadlineUsername);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.txtUpdateApprovalPhaseDeadlineKey);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.txtUpdateAPD_ApprovalPhaseKey);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.lblApprovalPhaseDeadlineKey);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.lblUpdateAPD_ApprovalPhaseKey);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.updateApprovalPhaseDeadline);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.btnUpdateApprovalPhaseDeadline);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.lblApprovalPhaseDeadline_Deadline);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.txtUpdateApprovalPhaseDeadlineSessionKey);
            this.tabUpdateApprovalPhaseDeadline.Controls.Add(this.lblUpdateApprovalPhaseDeadlineSessionKey);
            this.tabUpdateApprovalPhaseDeadline.Location = new System.Drawing.Point(4, 67);
            this.tabUpdateApprovalPhaseDeadline.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUpdateApprovalPhaseDeadline.Name = "tabUpdateApprovalPhaseDeadline";
            this.tabUpdateApprovalPhaseDeadline.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUpdateApprovalPhaseDeadline.Size = new System.Drawing.Size(579, 908);
            this.tabUpdateApprovalPhaseDeadline.TabIndex = 68;
            this.tabUpdateApprovalPhaseDeadline.Text = "UpdateApprovalPhaseDeadline";
            this.tabUpdateApprovalPhaseDeadline.UseVisualStyleBackColor = true;
            // 
            // txtUpdateApprovalPhaseDeadlineUsername
            // 
            this.txtUpdateApprovalPhaseDeadlineUsername.Location = new System.Drawing.Point(184, 132);
            this.txtUpdateApprovalPhaseDeadlineUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateApprovalPhaseDeadlineUsername.Name = "txtUpdateApprovalPhaseDeadlineUsername";
            this.txtUpdateApprovalPhaseDeadlineUsername.Size = new System.Drawing.Size(305, 22);
            this.txtUpdateApprovalPhaseDeadlineUsername.TabIndex = 68;
            // 
            // lblUpdateApprovalPhaseDeadlineUsername
            // 
            this.lblUpdateApprovalPhaseDeadlineUsername.AutoSize = true;
            this.lblUpdateApprovalPhaseDeadlineUsername.Location = new System.Drawing.Point(11, 135);
            this.lblUpdateApprovalPhaseDeadlineUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpdateApprovalPhaseDeadlineUsername.Name = "lblUpdateApprovalPhaseDeadlineUsername";
            this.lblUpdateApprovalPhaseDeadlineUsername.Size = new System.Drawing.Size(73, 17);
            this.lblUpdateApprovalPhaseDeadlineUsername.TabIndex = 67;
            this.lblUpdateApprovalPhaseDeadlineUsername.Text = "Username";
            // 
            // txtUpdateApprovalPhaseDeadlineKey
            // 
            this.txtUpdateApprovalPhaseDeadlineKey.Location = new System.Drawing.Point(185, 47);
            this.txtUpdateApprovalPhaseDeadlineKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateApprovalPhaseDeadlineKey.Name = "txtUpdateApprovalPhaseDeadlineKey";
            this.txtUpdateApprovalPhaseDeadlineKey.Size = new System.Drawing.Size(304, 22);
            this.txtUpdateApprovalPhaseDeadlineKey.TabIndex = 66;
            this.txtUpdateApprovalPhaseDeadlineKey.TextChanged += new System.EventHandler(this.txtUpdateApprovalPhaseDeadlineKey_TextChanged);
            // 
            // txtUpdateAPD_ApprovalPhaseKey
            // 
            this.txtUpdateAPD_ApprovalPhaseKey.Location = new System.Drawing.Point(185, 194);
            this.txtUpdateAPD_ApprovalPhaseKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateAPD_ApprovalPhaseKey.Name = "txtUpdateAPD_ApprovalPhaseKey";
            this.txtUpdateAPD_ApprovalPhaseKey.Size = new System.Drawing.Size(304, 22);
            this.txtUpdateAPD_ApprovalPhaseKey.TabIndex = 66;
            this.txtUpdateAPD_ApprovalPhaseKey.TextChanged += new System.EventHandler(this.txtUpdateAPD_ApprovalPhaseKey_TextChanged);
            // 
            // lblApprovalPhaseDeadlineKey
            // 
            this.lblApprovalPhaseDeadlineKey.AutoSize = true;
            this.lblApprovalPhaseDeadlineKey.Location = new System.Drawing.Point(11, 50);
            this.lblApprovalPhaseDeadlineKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalPhaseDeadlineKey.Name = "lblApprovalPhaseDeadlineKey";
            this.lblApprovalPhaseDeadlineKey.Size = new System.Drawing.Size(90, 17);
            this.lblApprovalPhaseDeadlineKey.TabIndex = 65;
            this.lblApprovalPhaseDeadlineKey.Text = "Approval key";
            // 
            // lblUpdateAPD_ApprovalPhaseKey
            // 
            this.lblUpdateAPD_ApprovalPhaseKey.AutoSize = true;
            this.lblUpdateAPD_ApprovalPhaseKey.Location = new System.Drawing.Point(11, 194);
            this.lblUpdateAPD_ApprovalPhaseKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpdateAPD_ApprovalPhaseKey.Name = "lblUpdateAPD_ApprovalPhaseKey";
            this.lblUpdateAPD_ApprovalPhaseKey.Size = new System.Drawing.Size(134, 17);
            this.lblUpdateAPD_ApprovalPhaseKey.TabIndex = 65;
            this.lblUpdateAPD_ApprovalPhaseKey.Text = "Approval Phase key";
            // 
            // updateApprovalPhaseDeadline
            // 
            this.updateApprovalPhaseDeadline.Location = new System.Drawing.Point(184, 89);
            this.updateApprovalPhaseDeadline.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.updateApprovalPhaseDeadline.Name = "updateApprovalPhaseDeadline";
            this.updateApprovalPhaseDeadline.Size = new System.Drawing.Size(305, 22);
            this.updateApprovalPhaseDeadline.TabIndex = 62;
            // 
            // btnUpdateApprovalPhaseDeadline
            // 
            this.btnUpdateApprovalPhaseDeadline.Location = new System.Drawing.Point(267, 246);
            this.btnUpdateApprovalPhaseDeadline.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdateApprovalPhaseDeadline.Name = "btnUpdateApprovalPhaseDeadline";
            this.btnUpdateApprovalPhaseDeadline.Size = new System.Drawing.Size(240, 49);
            this.btnUpdateApprovalPhaseDeadline.TabIndex = 53;
            this.btnUpdateApprovalPhaseDeadline.TabStop = false;
            this.btnUpdateApprovalPhaseDeadline.Text = "Update Approval Phase Deadline";
            this.btnUpdateApprovalPhaseDeadline.UseVisualStyleBackColor = true;
            this.btnUpdateApprovalPhaseDeadline.Click += new System.EventHandler(this.btnUpdateApprovalPhaseDeadline_Click);
            // 
            // lblApprovalPhaseDeadline_Deadline
            // 
            this.lblApprovalPhaseDeadline_Deadline.Location = new System.Drawing.Point(11, 89);
            this.lblApprovalPhaseDeadline_Deadline.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalPhaseDeadline_Deadline.Name = "lblApprovalPhaseDeadline_Deadline";
            this.lblApprovalPhaseDeadline_Deadline.Size = new System.Drawing.Size(153, 21);
            this.lblApprovalPhaseDeadline_Deadline.TabIndex = 52;
            this.lblApprovalPhaseDeadline_Deadline.Text = "Deadline";
            // 
            // txtUpdateApprovalPhaseDeadlineSessionKey
            // 
            this.txtUpdateApprovalPhaseDeadlineSessionKey.Location = new System.Drawing.Point(185, 7);
            this.txtUpdateApprovalPhaseDeadlineSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUpdateApprovalPhaseDeadlineSessionKey.Name = "txtUpdateApprovalPhaseDeadlineSessionKey";
            this.txtUpdateApprovalPhaseDeadlineSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtUpdateApprovalPhaseDeadlineSessionKey.TabIndex = 45;
            // 
            // lblUpdateApprovalPhaseDeadlineSessionKey
            // 
            this.lblUpdateApprovalPhaseDeadlineSessionKey.AutoSize = true;
            this.lblUpdateApprovalPhaseDeadlineSessionKey.Location = new System.Drawing.Point(8, 11);
            this.lblUpdateApprovalPhaseDeadlineSessionKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUpdateApprovalPhaseDeadlineSessionKey.Name = "lblUpdateApprovalPhaseDeadlineSessionKey";
            this.lblUpdateApprovalPhaseDeadlineSessionKey.Size = new System.Drawing.Size(82, 17);
            this.lblUpdateApprovalPhaseDeadlineSessionKey.TabIndex = 44;
            this.lblUpdateApprovalPhaseDeadlineSessionKey.Text = "SessionKey";
            // 
            // tabProofStudioURL
            // 
            this.tabProofStudioURL.Controls.Add(this.txtGetProofStudioURLUsername);
            this.tabProofStudioURL.Controls.Add(this.lblGetProofStudioURLUsername);
            this.tabProofStudioURL.Controls.Add(this.btnGetProofStudioURL);
            this.tabProofStudioURL.Controls.Add(this.txtProofStudioApprovalKey);
            this.tabProofStudioURL.Controls.Add(this.txtProofStudioSessionKey);
            this.tabProofStudioURL.Controls.Add(this.lblProofStudioApprovalKey);
            this.tabProofStudioURL.Controls.Add(this.lblProofStudioSessionKey);
            this.tabProofStudioURL.Location = new System.Drawing.Point(4, 67);
            this.tabProofStudioURL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabProofStudioURL.Name = "tabProofStudioURL";
            this.tabProofStudioURL.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabProofStudioURL.Size = new System.Drawing.Size(579, 908);
            this.tabProofStudioURL.TabIndex = 14;
            this.tabProofStudioURL.Text = "GetProofStudioURL";
            this.tabProofStudioURL.UseVisualStyleBackColor = true;
            // 
            // txtGetProofStudioURLUsername
            // 
            this.txtGetProofStudioURLUsername.Location = new System.Drawing.Point(193, 106);
            this.txtGetProofStudioURLUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetProofStudioURLUsername.Name = "txtGetProofStudioURLUsername";
            this.txtGetProofStudioURLUsername.Size = new System.Drawing.Size(300, 22);
            this.txtGetProofStudioURLUsername.TabIndex = 70;
            // 
            // lblGetProofStudioURLUsername
            // 
            this.lblGetProofStudioURLUsername.AutoSize = true;
            this.lblGetProofStudioURLUsername.Location = new System.Drawing.Point(40, 110);
            this.lblGetProofStudioURLUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetProofStudioURLUsername.Name = "lblGetProofStudioURLUsername";
            this.lblGetProofStudioURLUsername.Size = new System.Drawing.Size(73, 17);
            this.lblGetProofStudioURLUsername.TabIndex = 69;
            this.lblGetProofStudioURLUsername.Text = "Username";
            // 
            // btnGetProofStudioURL
            // 
            this.btnGetProofStudioURL.Location = new System.Drawing.Point(379, 148);
            this.btnGetProofStudioURL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetProofStudioURL.Name = "btnGetProofStudioURL";
            this.btnGetProofStudioURL.Size = new System.Drawing.Size(116, 28);
            this.btnGetProofStudioURL.TabIndex = 8;
            this.btnGetProofStudioURL.Text = "Get URL";
            this.btnGetProofStudioURL.UseVisualStyleBackColor = true;
            this.btnGetProofStudioURL.Click += new System.EventHandler(this.btnGetProofStudioURL_Click);
            // 
            // txtProofStudioApprovalKey
            // 
            this.txtProofStudioApprovalKey.Location = new System.Drawing.Point(193, 70);
            this.txtProofStudioApprovalKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtProofStudioApprovalKey.Name = "txtProofStudioApprovalKey";
            this.txtProofStudioApprovalKey.Size = new System.Drawing.Size(300, 22);
            this.txtProofStudioApprovalKey.TabIndex = 7;
            // 
            // txtProofStudioSessionKey
            // 
            this.txtProofStudioSessionKey.Location = new System.Drawing.Point(193, 30);
            this.txtProofStudioSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtProofStudioSessionKey.Name = "txtProofStudioSessionKey";
            this.txtProofStudioSessionKey.Size = new System.Drawing.Size(300, 22);
            this.txtProofStudioSessionKey.TabIndex = 6;
            // 
            // lblProofStudioApprovalKey
            // 
            this.lblProofStudioApprovalKey.AutoSize = true;
            this.lblProofStudioApprovalKey.Location = new System.Drawing.Point(40, 74);
            this.lblProofStudioApprovalKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProofStudioApprovalKey.Name = "lblProofStudioApprovalKey";
            this.lblProofStudioApprovalKey.Size = new System.Drawing.Size(98, 17);
            this.lblProofStudioApprovalKey.TabIndex = 5;
            this.lblProofStudioApprovalKey.Text = "Approval Guid";
            // 
            // lblProofStudioSessionKey
            // 
            this.lblProofStudioSessionKey.AutoSize = true;
            this.lblProofStudioSessionKey.Location = new System.Drawing.Point(40, 33);
            this.lblProofStudioSessionKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProofStudioSessionKey.Name = "lblProofStudioSessionKey";
            this.lblProofStudioSessionKey.Size = new System.Drawing.Size(82, 17);
            this.lblProofStudioSessionKey.TabIndex = 4;
            this.lblProofStudioSessionKey.Text = "SessionKey";
            // 
            // tabCreateFolder
            // 
            this.tabCreateFolder.Controls.Add(this.txtCreateFolderUsername);
            this.tabCreateFolder.Controls.Add(this.lblCreateFolderUsername);
            this.tabCreateFolder.Controls.Add(this.txtSessionKeyCreateFolder);
            this.tabCreateFolder.Controls.Add(this.lblSessionKeyCreateFolder);
            this.tabCreateFolder.Controls.Add(this.btnCreateNewFolder);
            this.tabCreateFolder.Controls.Add(this.lblParentFolderGuid);
            this.tabCreateFolder.Controls.Add(this.lblFolderName);
            this.tabCreateFolder.Controls.Add(this.txtParentFolderGuid);
            this.tabCreateFolder.Controls.Add(this.txtFolderName);
            this.tabCreateFolder.Location = new System.Drawing.Point(4, 88);
            this.tabCreateFolder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCreateFolder.Name = "tabCreateFolder";
            this.tabCreateFolder.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCreateFolder.Size = new System.Drawing.Size(579, 887);
            this.tabCreateFolder.TabIndex = 15;
            this.tabCreateFolder.Text = "Create Folder";
            this.tabCreateFolder.UseVisualStyleBackColor = true;
            // 
            // txtCreateFolderUsername
            // 
            this.txtCreateFolderUsername.Location = new System.Drawing.Point(188, 129);
            this.txtCreateFolderUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCreateFolderUsername.Name = "txtCreateFolderUsername";
            this.txtCreateFolderUsername.Size = new System.Drawing.Size(305, 22);
            this.txtCreateFolderUsername.TabIndex = 42;
            // 
            // lblCreateFolderUsername
            // 
            this.lblCreateFolderUsername.AutoSize = true;
            this.lblCreateFolderUsername.Location = new System.Drawing.Point(11, 133);
            this.lblCreateFolderUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCreateFolderUsername.Name = "lblCreateFolderUsername";
            this.lblCreateFolderUsername.Size = new System.Drawing.Size(73, 17);
            this.lblCreateFolderUsername.TabIndex = 41;
            this.lblCreateFolderUsername.Text = "Username";
            // 
            // txtSessionKeyCreateFolder
            // 
            this.txtSessionKeyCreateFolder.Location = new System.Drawing.Point(188, 25);
            this.txtSessionKeyCreateFolder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyCreateFolder.Name = "txtSessionKeyCreateFolder";
            this.txtSessionKeyCreateFolder.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyCreateFolder.TabIndex = 6;
            // 
            // lblSessionKeyCreateFolder
            // 
            this.lblSessionKeyCreateFolder.AutoSize = true;
            this.lblSessionKeyCreateFolder.Location = new System.Drawing.Point(11, 28);
            this.lblSessionKeyCreateFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyCreateFolder.Name = "lblSessionKeyCreateFolder";
            this.lblSessionKeyCreateFolder.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyCreateFolder.TabIndex = 5;
            this.lblSessionKeyCreateFolder.Text = "SessionKey";
            // 
            // btnCreateNewFolder
            // 
            this.btnCreateNewFolder.Location = new System.Drawing.Point(337, 181);
            this.btnCreateNewFolder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCreateNewFolder.Name = "btnCreateNewFolder";
            this.btnCreateNewFolder.Size = new System.Drawing.Size(156, 28);
            this.btnCreateNewFolder.TabIndex = 4;
            this.btnCreateNewFolder.Text = "Create Folder";
            this.btnCreateNewFolder.UseVisualStyleBackColor = true;
            this.btnCreateNewFolder.Click += new System.EventHandler(this.btnCreateNewFolder_Click);
            // 
            // lblParentFolderGuid
            // 
            this.lblParentFolderGuid.AutoSize = true;
            this.lblParentFolderGuid.Location = new System.Drawing.Point(11, 95);
            this.lblParentFolderGuid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParentFolderGuid.Name = "lblParentFolderGuid";
            this.lblParentFolderGuid.Size = new System.Drawing.Size(84, 17);
            this.lblParentFolderGuid.TabIndex = 3;
            this.lblParentFolderGuid.Text = "Parent Guid";
            // 
            // lblFolderName
            // 
            this.lblFolderName.AutoSize = true;
            this.lblFolderName.Location = new System.Drawing.Point(11, 59);
            this.lblFolderName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFolderName.Name = "lblFolderName";
            this.lblFolderName.Size = new System.Drawing.Size(45, 17);
            this.lblFolderName.TabIndex = 2;
            this.lblFolderName.Text = "Name";
            // 
            // txtParentFolderGuid
            // 
            this.txtParentFolderGuid.Location = new System.Drawing.Point(188, 95);
            this.txtParentFolderGuid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtParentFolderGuid.Name = "txtParentFolderGuid";
            this.txtParentFolderGuid.Size = new System.Drawing.Size(305, 22);
            this.txtParentFolderGuid.TabIndex = 1;
            // 
            // txtFolderName
            // 
            this.txtFolderName.Location = new System.Drawing.Point(188, 59);
            this.txtFolderName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFolderName.Name = "txtFolderName";
            this.txtFolderName.Size = new System.Drawing.Size(305, 22);
            this.txtFolderName.TabIndex = 0;
            // 
            // tabGetAllFolders
            // 
            this.tabGetAllFolders.Controls.Add(this.txtGetAllFoldersUsername);
            this.tabGetAllFolders.Controls.Add(this.lblGetAllFoldersUsername);
            this.tabGetAllFolders.Controls.Add(this.btnGetAllFolders);
            this.tabGetAllFolders.Controls.Add(this.txtSessionKeyGetAllFolders);
            this.tabGetAllFolders.Controls.Add(this.lblSessionKeyGetAllFolders);
            this.tabGetAllFolders.Location = new System.Drawing.Point(4, 88);
            this.tabGetAllFolders.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabGetAllFolders.Name = "tabGetAllFolders";
            this.tabGetAllFolders.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabGetAllFolders.Size = new System.Drawing.Size(579, 887);
            this.tabGetAllFolders.TabIndex = 16;
            this.tabGetAllFolders.Text = "Get All Folders";
            this.tabGetAllFolders.UseVisualStyleBackColor = true;
            // 
            // txtGetAllFoldersUsername
            // 
            this.txtGetAllFoldersUsername.Location = new System.Drawing.Point(188, 52);
            this.txtGetAllFoldersUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetAllFoldersUsername.Name = "txtGetAllFoldersUsername";
            this.txtGetAllFoldersUsername.Size = new System.Drawing.Size(305, 22);
            this.txtGetAllFoldersUsername.TabIndex = 44;
            // 
            // lblGetAllFoldersUsername
            // 
            this.lblGetAllFoldersUsername.AutoSize = true;
            this.lblGetAllFoldersUsername.Location = new System.Drawing.Point(11, 55);
            this.lblGetAllFoldersUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetAllFoldersUsername.Name = "lblGetAllFoldersUsername";
            this.lblGetAllFoldersUsername.Size = new System.Drawing.Size(73, 17);
            this.lblGetAllFoldersUsername.TabIndex = 43;
            this.lblGetAllFoldersUsername.Text = "Username";
            // 
            // btnGetAllFolders
            // 
            this.btnGetAllFolders.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnGetAllFolders.Location = new System.Drawing.Point(395, 101);
            this.btnGetAllFolders.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetAllFolders.Name = "btnGetAllFolders";
            this.btnGetAllFolders.Size = new System.Drawing.Size(100, 28);
            this.btnGetAllFolders.TabIndex = 15;
            this.btnGetAllFolders.Text = "List Folders";
            this.btnGetAllFolders.UseVisualStyleBackColor = true;
            this.btnGetAllFolders.Click += new System.EventHandler(this.btnGetAllFolders_Click);
            // 
            // txtSessionKeyGetAllFolders
            // 
            this.txtSessionKeyGetAllFolders.Location = new System.Drawing.Point(188, 20);
            this.txtSessionKeyGetAllFolders.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyGetAllFolders.Name = "txtSessionKeyGetAllFolders";
            this.txtSessionKeyGetAllFolders.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyGetAllFolders.TabIndex = 14;
            // 
            // lblSessionKeyGetAllFolders
            // 
            this.lblSessionKeyGetAllFolders.AutoSize = true;
            this.lblSessionKeyGetAllFolders.Location = new System.Drawing.Point(11, 23);
            this.lblSessionKeyGetAllFolders.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyGetAllFolders.Name = "lblSessionKeyGetAllFolders";
            this.lblSessionKeyGetAllFolders.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyGetAllFolders.TabIndex = 13;
            this.lblSessionKeyGetAllFolders.Text = "SessionKey";
            // 
            // tabCreateExternalUser
            // 
            this.tabCreateExternalUser.Controls.Add(this.btnCreateExternaluser);
            this.tabCreateExternalUser.Controls.Add(this.txtExternalEmail);
            this.tabCreateExternalUser.Controls.Add(this.lblEmailForCreateExternalUser);
            this.tabCreateExternalUser.Controls.Add(this.txtExternalUserLastName);
            this.tabCreateExternalUser.Controls.Add(this.lblLastNameforCreateExternalUser);
            this.tabCreateExternalUser.Controls.Add(this.txtExternalUserFirstName);
            this.tabCreateExternalUser.Controls.Add(this.lblFirstNameforCreateExternalUser);
            this.tabCreateExternalUser.Controls.Add(this.txtSessionKeyCreateExternalUser);
            this.tabCreateExternalUser.Controls.Add(this.lblSessionKeyforCreaqteExternalUser);
            this.tabCreateExternalUser.Location = new System.Drawing.Point(4, 130);
            this.tabCreateExternalUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCreateExternalUser.Name = "tabCreateExternalUser";
            this.tabCreateExternalUser.Size = new System.Drawing.Size(579, 845);
            this.tabCreateExternalUser.TabIndex = 15;
            this.tabCreateExternalUser.Text = "CreateExternalUser";
            this.tabCreateExternalUser.UseVisualStyleBackColor = true;
            // 
            // btnCreateExternaluser
            // 
            this.btnCreateExternaluser.Location = new System.Drawing.Point(376, 615);
            this.btnCreateExternaluser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCreateExternaluser.Name = "btnCreateExternaluser";
            this.btnCreateExternaluser.Size = new System.Drawing.Size(100, 28);
            this.btnCreateExternaluser.TabIndex = 60;
            this.btnCreateExternaluser.Text = "Create User";
            this.btnCreateExternaluser.UseVisualStyleBackColor = true;
            this.btnCreateExternaluser.Click += new System.EventHandler(this.btnCreateExternaluser_Click);
            // 
            // txtExternalEmail
            // 
            this.txtExternalEmail.Location = new System.Drawing.Point(188, 42);
            this.txtExternalEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtExternalEmail.Name = "txtExternalEmail";
            this.txtExternalEmail.Size = new System.Drawing.Size(305, 22);
            this.txtExternalEmail.TabIndex = 53;
            // 
            // lblEmailForCreateExternaUser
            // 
            this.lblEmailForCreateExternalUser.AutoSize = true;
            this.lblEmailForCreateExternalUser.Location = new System.Drawing.Point(11, 46);
            this.lblEmailForCreateExternalUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailForCreateExternalUser.Name = "lblEmailForCreateExternalUser";
            this.lblEmailForCreateExternalUser.Size = new System.Drawing.Size(42, 17);
            this.lblEmailForCreateExternalUser.TabIndex = 52;
            this.lblEmailForCreateExternalUser.Text = "Email";
            // 
            // txtExternalUserLastName
            // 
            this.txtExternalUserLastName.Location = new System.Drawing.Point(188, 108);
            this.txtExternalUserLastName.Margin = new System.Windows.Forms.Padding(4);
            this.txtExternalUserLastName.Name = "txtExternalUserLastName";
            this.txtExternalUserLastName.Size = new System.Drawing.Size(305, 22);
            this.txtExternalUserLastName.TabIndex = 50;
            // 
            // lblLastNameforCreateExternalUser
            // 
            this.lblLastNameforCreateExternalUser.AutoSize = true;
            this.lblLastNameforCreateExternalUser.Location = new System.Drawing.Point(11, 106);
            this.lblLastNameforCreateExternalUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLastNameforCreateExternalUser.Name = "lblLastNameforCreateExternalUser";
            this.lblLastNameforCreateExternalUser.Size = new System.Drawing.Size(76, 17);
            this.lblLastNameforCreateExternalUser.TabIndex = 49;
            this.lblLastNameforCreateExternalUser.Text = "Last Name";
            // 
            // txtExternalUserFirstName
            // 
            this.txtExternalUserFirstName.Location = new System.Drawing.Point(188, 76);
            this.txtExternalUserFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.txtExternalUserFirstName.Name = "txtExternalUserFirstName";
            this.txtExternalUserFirstName.Size = new System.Drawing.Size(305, 22);
            this.txtExternalUserFirstName.TabIndex = 48;
            // 
            // lblFirstNameforCreateExternalUser
            // 
            this.lblFirstNameforCreateExternalUser.AutoSize = true;
            this.lblFirstNameforCreateExternalUser.Location = new System.Drawing.Point(11, 76);
            this.lblFirstNameforCreateExternalUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFirstNameforCreateExternalUser.Name = "lblFirstNameforCreateExternalUser";
            this.lblFirstNameforCreateExternalUser.Size = new System.Drawing.Size(76, 17);
            this.lblFirstNameforCreateExternalUser.TabIndex = 46;
            this.lblFirstNameforCreateExternalUser.Text = "First Name";
            // 
            // txtSessionKeyCreateExternalUser
            // 
            this.txtSessionKeyCreateExternalUser.Location = new System.Drawing.Point(188, 10);
            this.txtSessionKeyCreateExternalUser.Margin = new System.Windows.Forms.Padding(4);
            this.txtSessionKeyCreateExternalUser.Name = "txtSessionKeyCreateExternalUser";
            this.txtSessionKeyCreateExternalUser.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyCreateExternalUser.TabIndex = 45;
            // 
            // lblSessionKeyforCreaqteExternalUser
            // 
            this.lblSessionKeyforCreaqteExternalUser.AutoSize = true;
            this.lblSessionKeyforCreaqteExternalUser.Location = new System.Drawing.Point(11, 14);
            this.lblSessionKeyforCreaqteExternalUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyforCreaqteExternalUser.Name = "lblSessionKeyforCreaqteExternalUser";
            this.lblSessionKeyforCreaqteExternalUser.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyforCreaqteExternalUser.TabIndex = 44;
            this.lblSessionKeyforCreaqteExternalUser.Text = "SessionKey";
            // 
            // tabCreateUser
            // 
            this.tabCreateUser.Controls.Add(this.chkIsSSOUser);
            this.tabCreateUser.Controls.Add(this.chkPrivateAnnotations);
            this.tabCreateUser.Controls.Add(this.btnCreateuser);
            this.tabCreateUser.Controls.Add(this.dataGridViewUserGroups);
            this.tabCreateUser.Controls.Add(this.lblgroups);
            this.tabCreateUser.Controls.Add(this.dataGridViewPermissions);
            this.tabCreateUser.Controls.Add(this.lblUserPermissions);
            this.tabCreateUser.Controls.Add(this.txtUserPassword);
            this.tabCreateUser.Controls.Add(this.lblPasswordforUser);
            this.tabCreateUser.Controls.Add(this.txtEmail);
            this.tabCreateUser.Controls.Add(this.lblEmailForCreateUser);
            this.tabCreateUser.Controls.Add(this.txtUserLastName);
            this.tabCreateUser.Controls.Add(this.lblLastNameforCreateUser);
            this.tabCreateUser.Controls.Add(this.txtUserFirstName);
            this.tabCreateUser.Controls.Add(this.lblFirstNameforCreateUser);
            this.tabCreateUser.Controls.Add(this.txtSessionKeyCreateUser);
            this.tabCreateUser.Controls.Add(this.lblSessionKeyforCreaqteUser);
            this.tabCreateUser.Location = new System.Drawing.Point(4, 88);
            this.tabCreateUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCreateUser.Name = "tabCreateUser";
            this.tabCreateUser.Size = new System.Drawing.Size(579, 887);
            this.tabCreateUser.TabIndex = 15;
            this.tabCreateUser.Text = "CreateUser";
            this.tabCreateUser.UseVisualStyleBackColor = true;
            // 
            // chkIsSSOUser
            // 
            this.chkIsSSOUser.AutoSize = true;
            this.chkIsSSOUser.Location = new System.Drawing.Point(189, 178);
            this.chkIsSSOUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkIsSSOUser.Name = "chkIsSSOUser";
            this.chkIsSSOUser.Size = new System.Drawing.Size(107, 21);
            this.chkIsSSOUser.TabIndex = 61;
            this.chkIsSSOUser.Text = "Is SSO User";
            this.chkIsSSOUser.UseVisualStyleBackColor = true;
            // 
            // chkPrivateAnnotations
            // 
            this.chkPrivateAnnotations.AutoSize = true;
            this.chkPrivateAnnotations.Location = new System.Drawing.Point(320, 178);
            this.chkPrivateAnnotations.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPrivateAnnotations.Name = "chkPrivateAnnotations";
            this.chkPrivateAnnotations.Size = new System.Drawing.Size(153, 21);
            this.chkPrivateAnnotations.TabIndex = 62;
            this.chkPrivateAnnotations.Text = "Private Annotations";
            this.chkPrivateAnnotations.UseVisualStyleBackColor = true;
            // 
            // btnCreateuser
            // 
            this.btnCreateuser.Location = new System.Drawing.Point(376, 615);
            this.btnCreateuser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCreateuser.Name = "btnCreateuser";
            this.btnCreateuser.Size = new System.Drawing.Size(100, 28);
            this.btnCreateuser.TabIndex = 60;
            this.btnCreateuser.Text = "Create User";
            this.btnCreateuser.UseVisualStyleBackColor = true;
            this.btnCreateuser.Click += new System.EventHandler(this.btnCreateuser_Click);
            // 
            // dataGridViewUserGroups
            // 
            this.dataGridViewUserGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUserGroups.Location = new System.Drawing.Point(15, 459);
            this.dataGridViewUserGroups.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewUserGroups.Name = "dataGridViewUserGroups";
            this.dataGridViewUserGroups.Size = new System.Drawing.Size(320, 185);
            this.dataGridViewUserGroups.TabIndex = 59;
            // 
            // lblgroups
            // 
            this.lblgroups.AutoSize = true;
            this.lblgroups.Location = new System.Drawing.Point(19, 439);
            this.lblgroups.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblgroups.Name = "lblgroups";
            this.lblgroups.Size = new System.Drawing.Size(55, 17);
            this.lblgroups.TabIndex = 58;
            this.lblgroups.Text = "Groups";
            // 
            // dataGridViewPermissions
            // 
            this.dataGridViewPermissions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPermissions.Location = new System.Drawing.Point(19, 228);
            this.dataGridViewPermissions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewPermissions.Name = "dataGridViewPermissions";
            this.dataGridViewPermissions.Size = new System.Drawing.Size(380, 185);
            this.dataGridViewPermissions.TabIndex = 57;
            // 
            // lblUserPermissions
            // 
            this.lblUserPermissions.AutoSize = true;
            this.lblUserPermissions.Location = new System.Drawing.Point(15, 208);
            this.lblUserPermissions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserPermissions.Name = "lblUserPermissions";
            this.lblUserPermissions.Size = new System.Drawing.Size(84, 17);
            this.lblUserPermissions.TabIndex = 56;
            this.lblUserPermissions.Text = "Permissions";
            // 
            // txtUserPassword
            // 
            this.txtUserPassword.Location = new System.Drawing.Point(188, 140);
            this.txtUserPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserPassword.Name = "txtUserPassword";
            this.txtUserPassword.PasswordChar = '*';
            this.txtUserPassword.Size = new System.Drawing.Size(305, 22);
            this.txtUserPassword.TabIndex = 55;
            // 
            // lblPasswordforUser
            // 
            this.lblPasswordforUser.AutoSize = true;
            this.lblPasswordforUser.Location = new System.Drawing.Point(11, 146);
            this.lblPasswordforUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPasswordforUser.Name = "lblPasswordforUser";
            this.lblPasswordforUser.Size = new System.Drawing.Size(69, 17);
            this.lblPasswordforUser.TabIndex = 54;
            this.lblPasswordforUser.Text = "Password";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(188, 42);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(305, 22);
            this.txtEmail.TabIndex = 53;
            // 
            // lblEmailForCreateUser
            // 
            this.lblEmailForCreateUser.AutoSize = true;
            this.lblEmailForCreateUser.Location = new System.Drawing.Point(11, 46);
            this.lblEmailForCreateUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailForCreateUser.Name = "lblEmailForCreateUser";
            this.lblEmailForCreateUser.Size = new System.Drawing.Size(42, 17);
            this.lblEmailForCreateUser.TabIndex = 52;
            this.lblEmailForCreateUser.Text = "Email";
            // 
            // txtUserLastName
            // 
            this.txtUserLastName.Location = new System.Drawing.Point(188, 108);
            this.txtUserLastName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserLastName.Name = "txtUserLastName";
            this.txtUserLastName.Size = new System.Drawing.Size(305, 22);
            this.txtUserLastName.TabIndex = 50;
            // 
            // lblLastNameforCreateUser
            // 
            this.lblLastNameforCreateUser.AutoSize = true;
            this.lblLastNameforCreateUser.Location = new System.Drawing.Point(11, 106);
            this.lblLastNameforCreateUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLastNameforCreateUser.Name = "lblLastNameforCreateUser";
            this.lblLastNameforCreateUser.Size = new System.Drawing.Size(76, 17);
            this.lblLastNameforCreateUser.TabIndex = 49;
            this.lblLastNameforCreateUser.Text = "Last Name";
            // 
            // txtUserFirstName
            // 
            this.txtUserFirstName.Location = new System.Drawing.Point(188, 76);
            this.txtUserFirstName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUserFirstName.Name = "txtUserFirstName";
            this.txtUserFirstName.Size = new System.Drawing.Size(305, 22);
            this.txtUserFirstName.TabIndex = 48;
            // 
            // lblFirstNameforCreateUser
            // 
            this.lblFirstNameforCreateUser.AutoSize = true;
            this.lblFirstNameforCreateUser.Location = new System.Drawing.Point(11, 76);
            this.lblFirstNameforCreateUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFirstNameforCreateUser.Name = "lblFirstNameforCreateUser";
            this.lblFirstNameforCreateUser.Size = new System.Drawing.Size(76, 17);
            this.lblFirstNameforCreateUser.TabIndex = 46;
            this.lblFirstNameforCreateUser.Text = "First Name";
            // 
            // txtSessionKeyCreateUser
            // 
            this.txtSessionKeyCreateUser.Location = new System.Drawing.Point(188, 10);
            this.txtSessionKeyCreateUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyCreateUser.Name = "txtSessionKeyCreateUser";
            this.txtSessionKeyCreateUser.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyCreateUser.TabIndex = 45;
            // 
            // lblSessionKeyforCreaqteUser
            // 
            this.lblSessionKeyforCreaqteUser.AutoSize = true;
            this.lblSessionKeyforCreaqteUser.Location = new System.Drawing.Point(11, 14);
            this.lblSessionKeyforCreaqteUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyforCreaqteUser.Name = "lblSessionKeyforCreaqteUser";
            this.lblSessionKeyforCreaqteUser.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyforCreaqteUser.TabIndex = 44;
            this.lblSessionKeyforCreaqteUser.Text = "SessionKey";
            // 
            // tabListApprovalWorkflows
            // 
            this.tabListApprovalWorkflows.Controls.Add(this.txtListApprovalWorkflowUsername);
            this.tabListApprovalWorkflows.Controls.Add(this.lblListApprovalWorkflowUsername);
            this.tabListApprovalWorkflows.Controls.Add(this.btnListApprovalWorkflows);
            this.tabListApprovalWorkflows.Controls.Add(this.txtListApprovalWorkflowsSessionKey);
            this.tabListApprovalWorkflows.Controls.Add(this.lblListWorkflowsSessionKey);
            this.tabListApprovalWorkflows.Location = new System.Drawing.Point(4, 130);
            this.tabListApprovalWorkflows.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabListApprovalWorkflows.Name = "tabListApprovalWorkflows";
            this.tabListApprovalWorkflows.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabListApprovalWorkflows.Size = new System.Drawing.Size(579, 845);
            this.tabListApprovalWorkflows.TabIndex = 17;
            this.tabListApprovalWorkflows.Text = "List Aproval Workflows";
            this.tabListApprovalWorkflows.UseVisualStyleBackColor = true;
            // 
            // txtListApprovalWorkflowUsername
            // 
            this.txtListApprovalWorkflowUsername.Location = new System.Drawing.Point(188, 55);
            this.txtListApprovalWorkflowUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtListApprovalWorkflowUsername.Name = "txtListApprovalWorkflowUsername";
            this.txtListApprovalWorkflowUsername.Size = new System.Drawing.Size(305, 22);
            this.txtListApprovalWorkflowUsername.TabIndex = 46;
            // 
            // lblListApprovalWorkflowUsername
            // 
            this.lblListApprovalWorkflowUsername.AutoSize = true;
            this.lblListApprovalWorkflowUsername.Location = new System.Drawing.Point(11, 59);
            this.lblListApprovalWorkflowUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblListApprovalWorkflowUsername.Name = "lblListApprovalWorkflowUsername";
            this.lblListApprovalWorkflowUsername.Size = new System.Drawing.Size(73, 17);
            this.lblListApprovalWorkflowUsername.TabIndex = 45;
            this.lblListApprovalWorkflowUsername.Text = "Username";
            // 
            // btnListApprovalWorkflows
            // 
            this.btnListApprovalWorkflows.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnListApprovalWorkflows.Location = new System.Drawing.Point(316, 91);
            this.btnListApprovalWorkflows.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnListApprovalWorkflows.Name = "btnListApprovalWorkflows";
            this.btnListApprovalWorkflows.Size = new System.Drawing.Size(179, 28);
            this.btnListApprovalWorkflows.TabIndex = 15;
            this.btnListApprovalWorkflows.Text = "List Approval Workflows";
            this.btnListApprovalWorkflows.UseVisualStyleBackColor = true;
            this.btnListApprovalWorkflows.Click += new System.EventHandler(this.btnListApprovalWorkflows_Click);
            // 
            // txtListApprovalWorkflowsSessionKey
            // 
            this.txtListApprovalWorkflowsSessionKey.Location = new System.Drawing.Point(188, 23);
            this.txtListApprovalWorkflowsSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtListApprovalWorkflowsSessionKey.Name = "txtListApprovalWorkflowsSessionKey";
            this.txtListApprovalWorkflowsSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtListApprovalWorkflowsSessionKey.TabIndex = 14;
            // 
            // lblListWorkflowsSessionKey
            // 
            this.lblListWorkflowsSessionKey.AutoSize = true;
            this.lblListWorkflowsSessionKey.Location = new System.Drawing.Point(11, 27);
            this.lblListWorkflowsSessionKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblListWorkflowsSessionKey.Name = "lblListWorkflowsSessionKey";
            this.lblListWorkflowsSessionKey.Size = new System.Drawing.Size(82, 17);
            this.lblListWorkflowsSessionKey.TabIndex = 13;
            this.lblListWorkflowsSessionKey.Text = "SessionKey";
            // 
            // tabCheckUser
            // 
            this.tabCheckUser.Controls.Add(this.btnCheckUser);
            this.tabCheckUser.Controls.Add(this.txtEmailForCheckUser);
            this.tabCheckUser.Controls.Add(this.txtSessionKeyForCheckUser);
            this.tabCheckUser.Controls.Add(this.lblCheckUserEmail);
            this.tabCheckUser.Controls.Add(this.lblSessionKeyCheckUser);
            this.tabCheckUser.Location = new System.Drawing.Point(4, 88);
            this.tabCheckUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCheckUser.Name = "tabCheckUser";
            this.tabCheckUser.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCheckUser.Size = new System.Drawing.Size(579, 887);
            this.tabCheckUser.TabIndex = 18;
            this.tabCheckUser.Text = "CheckUser";
            this.tabCheckUser.UseVisualStyleBackColor = true;
            // 
            // btnCheckUser
            // 
            this.btnCheckUser.Location = new System.Drawing.Point(391, 108);
            this.btnCheckUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCheckUser.Name = "btnCheckUser";
            this.btnCheckUser.Size = new System.Drawing.Size(100, 28);
            this.btnCheckUser.TabIndex = 13;
            this.btnCheckUser.Text = "Check";
            this.btnCheckUser.UseVisualStyleBackColor = true;
            this.btnCheckUser.Click += new System.EventHandler(this.btnCheckUser_Click);
            // 
            // txtEmailForCheckUser
            // 
            this.txtEmailForCheckUser.Location = new System.Drawing.Point(188, 65);
            this.txtEmailForCheckUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtEmailForCheckUser.Name = "txtEmailForCheckUser";
            this.txtEmailForCheckUser.Size = new System.Drawing.Size(305, 22);
            this.txtEmailForCheckUser.TabIndex = 12;
            // 
            // txtSessionKeyForCheckUser
            // 
            this.txtSessionKeyForCheckUser.Location = new System.Drawing.Point(188, 16);
            this.txtSessionKeyForCheckUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeyForCheckUser.Name = "txtSessionKeyForCheckUser";
            this.txtSessionKeyForCheckUser.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeyForCheckUser.TabIndex = 11;
            // 
            // lblCheckUserEmail
            // 
            this.lblCheckUserEmail.AutoSize = true;
            this.lblCheckUserEmail.Location = new System.Drawing.Point(11, 65);
            this.lblCheckUserEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCheckUserEmail.Name = "lblCheckUserEmail";
            this.lblCheckUserEmail.Size = new System.Drawing.Size(42, 17);
            this.lblCheckUserEmail.TabIndex = 10;
            this.lblCheckUserEmail.Text = "Email";
            // 
            // lblSessionKeyCheckUser
            // 
            this.lblSessionKeyCheckUser.AutoSize = true;
            this.lblSessionKeyCheckUser.Location = new System.Drawing.Point(11, 20);
            this.lblSessionKeyCheckUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyCheckUser.Name = "lblSessionKeyCheckUser";
            this.lblSessionKeyCheckUser.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyCheckUser.TabIndex = 9;
            this.lblSessionKeyCheckUser.Text = "SessionKey";
            // 
            // tabGetWorkflowDetails
            // 
            this.tabGetWorkflowDetails.Controls.Add(this.txtGetWorkflowDetailsUsername);
            this.tabGetWorkflowDetails.Controls.Add(this.lblGetWorkflowDetailsUsername);
            this.tabGetWorkflowDetails.Controls.Add(this.btnGetWorkflowDetails);
            this.tabGetWorkflowDetails.Controls.Add(this.txtApprovalWorkflowKey);
            this.tabGetWorkflowDetails.Controls.Add(this.txtGetWorkflowDetailsSessionKey);
            this.tabGetWorkflowDetails.Controls.Add(this.lblApprovalWorkflowKey);
            this.tabGetWorkflowDetails.Controls.Add(this.label35);
            this.tabGetWorkflowDetails.Location = new System.Drawing.Point(4, 109);
            this.tabGetWorkflowDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabGetWorkflowDetails.Name = "tabGetWorkflowDetails";
            this.tabGetWorkflowDetails.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabGetWorkflowDetails.Size = new System.Drawing.Size(579, 866);
            this.tabGetWorkflowDetails.TabIndex = 19;
            this.tabGetWorkflowDetails.Text = "Get Workflow Details";
            this.tabGetWorkflowDetails.UseVisualStyleBackColor = true;
            // 
            // txtGetWorkflowDetailsUsername
            // 
            this.txtGetWorkflowDetailsUsername.Location = new System.Drawing.Point(188, 98);
            this.txtGetWorkflowDetailsUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetWorkflowDetailsUsername.Name = "txtGetWorkflowDetailsUsername";
            this.txtGetWorkflowDetailsUsername.Size = new System.Drawing.Size(305, 22);
            this.txtGetWorkflowDetailsUsername.TabIndex = 40;
            // 
            // lblGetWorkflowDetailsUsername
            // 
            this.lblGetWorkflowDetailsUsername.AutoSize = true;
            this.lblGetWorkflowDetailsUsername.Location = new System.Drawing.Point(9, 101);
            this.lblGetWorkflowDetailsUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGetWorkflowDetailsUsername.Name = "lblGetWorkflowDetailsUsername";
            this.lblGetWorkflowDetailsUsername.Size = new System.Drawing.Size(73, 17);
            this.lblGetWorkflowDetailsUsername.TabIndex = 39;
            this.lblGetWorkflowDetailsUsername.Text = "Username";
            // 
            // btnGetWorkflowDetails
            // 
            this.btnGetWorkflowDetails.Location = new System.Drawing.Point(355, 130);
            this.btnGetWorkflowDetails.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetWorkflowDetails.Name = "btnGetWorkflowDetails";
            this.btnGetWorkflowDetails.Size = new System.Drawing.Size(140, 28);
            this.btnGetWorkflowDetails.TabIndex = 16;
            this.btnGetWorkflowDetails.Text = "Display Workflow";
            this.btnGetWorkflowDetails.UseVisualStyleBackColor = true;
            this.btnGetWorkflowDetails.Click += new System.EventHandler(this.btnGetWorkflowDetails_Click);
            // 
            // txtApprovalWorkflowKey
            // 
            this.txtApprovalWorkflowKey.Location = new System.Drawing.Point(188, 60);
            this.txtApprovalWorkflowKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalWorkflowKey.Name = "txtApprovalWorkflowKey";
            this.txtApprovalWorkflowKey.Size = new System.Drawing.Size(305, 22);
            this.txtApprovalWorkflowKey.TabIndex = 15;
            // 
            // txtGetWorkflowDetailsSessionKey
            // 
            this.txtGetWorkflowDetailsSessionKey.Location = new System.Drawing.Point(188, 23);
            this.txtGetWorkflowDetailsSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetWorkflowDetailsSessionKey.Name = "txtGetWorkflowDetailsSessionKey";
            this.txtGetWorkflowDetailsSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtGetWorkflowDetailsSessionKey.TabIndex = 14;
            // 
            // lblApprovalWorkflowKey
            // 
            this.lblApprovalWorkflowKey.AutoSize = true;
            this.lblApprovalWorkflowKey.Location = new System.Drawing.Point(11, 64);
            this.lblApprovalWorkflowKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalWorkflowKey.Name = "lblApprovalWorkflowKey";
            this.lblApprovalWorkflowKey.Size = new System.Drawing.Size(93, 17);
            this.lblApprovalWorkflowKey.TabIndex = 13;
            this.lblApprovalWorkflowKey.Text = "Workflow Key";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(11, 27);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(82, 17);
            this.label35.TabIndex = 12;
            this.label35.Text = "SessionKey";
            // 
            // tabUpdateApprovalDecision
            // 
            this.tabUpdateApprovalDecision.Controls.Add(this.cbxTestPushApprovalStatus);
            this.tabUpdateApprovalDecision.Controls.Add(this.cbxLoggedUserDecision);
            this.tabUpdateApprovalDecision.Controls.Add(this.lblDecision);
            this.tabUpdateApprovalDecision.Controls.Add(this.lblApprovalDecisionApprovalKey);
            this.tabUpdateApprovalDecision.Controls.Add(this.txtApprovalDecisionApprovalKey);
            this.tabUpdateApprovalDecision.Controls.Add(this.btnUpdateApprovalDecision);
            this.tabUpdateApprovalDecision.Controls.Add(this.txtApprovalDecisionUsername);
            this.tabUpdateApprovalDecision.Controls.Add(this.txtApprovalDecisionExternalEmail);
            this.tabUpdateApprovalDecision.Controls.Add(this.txtApprovalDecisionSessionKey);
            this.tabUpdateApprovalDecision.Controls.Add(this.lblApprovalDecisionUsername);
            this.tabUpdateApprovalDecision.Controls.Add(this.lblApprovalDecisionExternalEmail);
            this.tabUpdateApprovalDecision.Controls.Add(this.lblApprovalDecisionSessionKey);
            this.tabUpdateApprovalDecision.Location = new System.Drawing.Point(4, 109);
            this.tabUpdateApprovalDecision.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUpdateApprovalDecision.Name = "tabUpdateApprovalDecision";
            this.tabUpdateApprovalDecision.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabUpdateApprovalDecision.Size = new System.Drawing.Size(579, 866);
            this.tabUpdateApprovalDecision.TabIndex = 20;
            this.tabUpdateApprovalDecision.Text = "Update Approval Decision";
            this.tabUpdateApprovalDecision.UseVisualStyleBackColor = true;
            // 
            // cbxTestPushApprovalStatus
            // 
            this.cbxTestPushApprovalStatus.AutoSize = true;
            this.cbxTestPushApprovalStatus.Location = new System.Drawing.Point(180, 183);
            this.cbxTestPushApprovalStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxTestPushApprovalStatus.Name = "cbxTestPushApprovalStatus";
            this.cbxTestPushApprovalStatus.Size = new System.Drawing.Size(257, 38);
            this.cbxTestPushApprovalStatus.TabIndex = 67;
            this.cbxTestPushApprovalStatus.Text = "Simulate \"push status\"\r\n(option is used only fo test purpose)";
            this.cbxTestPushApprovalStatus.ThreeState = true;
            this.cbxTestPushApprovalStatus.UseVisualStyleBackColor = true;
            // 
            // cbxLoggedUserDecision
            // 
            this.cbxLoggedUserDecision.FormattingEnabled = true;
            this.cbxLoggedUserDecision.Items.AddRange(new object[] {
            "None",
            "Pending",
            "Changes Required",
            "Approved with Changes",
            "Approved",
            "Not Applicable"});
            this.cbxLoggedUserDecision.Location = new System.Drawing.Point(180, 86);
            this.cbxLoggedUserDecision.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxLoggedUserDecision.Name = "cbxLoggedUserDecision";
            this.cbxLoggedUserDecision.Size = new System.Drawing.Size(296, 24);
            this.cbxLoggedUserDecision.TabIndex = 66;
            // 
            // lblDecision
            // 
            this.lblDecision.Location = new System.Drawing.Point(19, 90);
            this.lblDecision.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDecision.Name = "lblDecision";
            this.lblDecision.Size = new System.Drawing.Size(153, 22);
            this.lblDecision.TabIndex = 65;
            this.lblDecision.Text = "Decision";
            // 
            // lblApprovalDecisionApprovalKey
            // 
            this.lblApprovalDecisionApprovalKey.AutoSize = true;
            this.lblApprovalDecisionApprovalKey.Location = new System.Drawing.Point(19, 58);
            this.lblApprovalDecisionApprovalKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalDecisionApprovalKey.Name = "lblApprovalDecisionApprovalKey";
            this.lblApprovalDecisionApprovalKey.Size = new System.Drawing.Size(92, 17);
            this.lblApprovalDecisionApprovalKey.TabIndex = 8;
            this.lblApprovalDecisionApprovalKey.Text = "Approval Key";
            // 
            // txtApprovalDecisionApprovalKey
            // 
            this.txtApprovalDecisionApprovalKey.Location = new System.Drawing.Point(180, 54);
            this.txtApprovalDecisionApprovalKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalDecisionApprovalKey.Name = "txtApprovalDecisionApprovalKey";
            this.txtApprovalDecisionApprovalKey.Size = new System.Drawing.Size(296, 22);
            this.txtApprovalDecisionApprovalKey.TabIndex = 7;
            // 
            // btnUpdateApprovalDecision
            // 
            this.btnUpdateApprovalDecision.Location = new System.Drawing.Point(280, 234);
            this.btnUpdateApprovalDecision.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnUpdateApprovalDecision.Name = "btnUpdateApprovalDecision";
            this.btnUpdateApprovalDecision.Size = new System.Drawing.Size(197, 28);
            this.btnUpdateApprovalDecision.TabIndex = 6;
            this.btnUpdateApprovalDecision.Text = "Update Approval Decision";
            this.btnUpdateApprovalDecision.UseVisualStyleBackColor = true;
            this.btnUpdateApprovalDecision.Click += new System.EventHandler(this.btnUpdateApprovalDecision_Click);
            // 
            // txtApprovalDecisionUsername
            // 
            this.txtApprovalDecisionUsername.Location = new System.Drawing.Point(180, 119);
            this.txtApprovalDecisionUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalDecisionUsername.Name = "txtApprovalDecisionUsername";
            this.txtApprovalDecisionUsername.Size = new System.Drawing.Size(296, 22);
            this.txtApprovalDecisionUsername.TabIndex = 5;
            // 
            // txtApprovalDecisionExternalEmail
            // 
            this.txtApprovalDecisionExternalEmail.Location = new System.Drawing.Point(180, 151);
            this.txtApprovalDecisionExternalEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalDecisionExternalEmail.Name = "txtApprovalDecisionExternalEmail";
            this.txtApprovalDecisionExternalEmail.Size = new System.Drawing.Size(296, 22);
            this.txtApprovalDecisionExternalEmail.TabIndex = 4;
            // 
            // txtApprovalDecisionSessionKey
            // 
            this.txtApprovalDecisionSessionKey.Location = new System.Drawing.Point(180, 22);
            this.txtApprovalDecisionSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtApprovalDecisionSessionKey.Name = "txtApprovalDecisionSessionKey";
            this.txtApprovalDecisionSessionKey.Size = new System.Drawing.Size(296, 22);
            this.txtApprovalDecisionSessionKey.TabIndex = 3;
            // 
            // lblApprovalDecisionUsername
            // 
            this.lblApprovalDecisionUsername.AutoSize = true;
            this.lblApprovalDecisionUsername.Location = new System.Drawing.Point(19, 123);
            this.lblApprovalDecisionUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalDecisionUsername.Name = "lblApprovalDecisionUsername";
            this.lblApprovalDecisionUsername.Size = new System.Drawing.Size(73, 17);
            this.lblApprovalDecisionUsername.TabIndex = 2;
            this.lblApprovalDecisionUsername.Text = "Username";
            // 
            // lblApprovalDecisionExternalEmail
            // 
            this.lblApprovalDecisionExternalEmail.AutoSize = true;
            this.lblApprovalDecisionExternalEmail.Location = new System.Drawing.Point(19, 160);
            this.lblApprovalDecisionExternalEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalDecisionExternalEmail.Name = "lblApprovalDecisionExternalEmail";
            this.lblApprovalDecisionExternalEmail.Size = new System.Drawing.Size(131, 17);
            this.lblApprovalDecisionExternalEmail.TabIndex = 1;
            this.lblApprovalDecisionExternalEmail.Text = "External User Email";
            // 
            // lblApprovalDecisionSessionKey
            // 
            this.lblApprovalDecisionSessionKey.AutoSize = true;
            this.lblApprovalDecisionSessionKey.Location = new System.Drawing.Point(19, 22);
            this.lblApprovalDecisionSessionKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblApprovalDecisionSessionKey.Name = "lblApprovalDecisionSessionKey";
            this.lblApprovalDecisionSessionKey.Size = new System.Drawing.Size(86, 17);
            this.lblApprovalDecisionSessionKey.TabIndex = 0;
            this.lblApprovalDecisionSessionKey.Text = "Session Key";
            // 
            // tabGetFolderByName
            // 
            this.tabGetFolderByName.Controls.Add(this.txtGetFolderByNameUsername);
            this.tabGetFolderByName.Controls.Add(this.lblUsernameForFolderName);
            this.tabGetFolderByName.Controls.Add(this.btnGetFolderByName);
            this.tabGetFolderByName.Controls.Add(this.txtFolderByName);
            this.tabGetFolderByName.Controls.Add(this.txtSessionKeGetFolderByName);
            this.tabGetFolderByName.Controls.Add(this.lblFoldersName);
            this.tabGetFolderByName.Controls.Add(this.lblSessionKeyGetFolderByName);
            this.tabGetFolderByName.Location = new System.Drawing.Point(4, 109);
            this.tabGetFolderByName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabGetFolderByName.Name = "tabGetFolderByName";
            this.tabGetFolderByName.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabGetFolderByName.Size = new System.Drawing.Size(579, 866);
            this.tabGetFolderByName.TabIndex = 21;
            this.tabGetFolderByName.Text = "Get Folder By Name";
            this.tabGetFolderByName.UseVisualStyleBackColor = true;
            // 
            // txtGetFolderByNameUsername
            // 
            this.txtGetFolderByNameUsername.Location = new System.Drawing.Point(188, 80);
            this.txtGetFolderByNameUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtGetFolderByNameUsername.Name = "txtGetFolderByNameUsername";
            this.txtGetFolderByNameUsername.Size = new System.Drawing.Size(305, 22);
            this.txtGetFolderByNameUsername.TabIndex = 53;
            // 
            // lblUsernameForFolderName
            // 
            this.lblUsernameForFolderName.AutoSize = true;
            this.lblUsernameForFolderName.Location = new System.Drawing.Point(11, 80);
            this.lblUsernameForFolderName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUsernameForFolderName.Name = "lblUsernameForFolderName";
            this.lblUsernameForFolderName.Size = new System.Drawing.Size(73, 17);
            this.lblUsernameForFolderName.TabIndex = 52;
            this.lblUsernameForFolderName.Text = "Username";
            // 
            // btnGetFolderByName
            // 
            this.btnGetFolderByName.Location = new System.Drawing.Point(389, 130);
            this.btnGetFolderByName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGetFolderByName.Name = "btnGetFolderByName";
            this.btnGetFolderByName.Size = new System.Drawing.Size(100, 28);
            this.btnGetFolderByName.TabIndex = 51;
            this.btnGetFolderByName.Text = "Get Folder";
            this.btnGetFolderByName.UseVisualStyleBackColor = true;
            this.btnGetFolderByName.Click += new System.EventHandler(this.btnGetFolderByName_Click);
            // 
            // txtFolderByName
            // 
            this.txtFolderByName.Location = new System.Drawing.Point(188, 44);
            this.txtFolderByName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFolderByName.Name = "txtFolderByName";
            this.txtFolderByName.Size = new System.Drawing.Size(305, 22);
            this.txtFolderByName.TabIndex = 50;
            // 
            // txtSessionKeGetFolderByName
            // 
            this.txtSessionKeGetFolderByName.Location = new System.Drawing.Point(188, 7);
            this.txtSessionKeGetFolderByName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSessionKeGetFolderByName.Name = "txtSessionKeGetFolderByName";
            this.txtSessionKeGetFolderByName.Size = new System.Drawing.Size(305, 22);
            this.txtSessionKeGetFolderByName.TabIndex = 49;
            // 
            // lblFoldersName
            // 
            this.lblFoldersName.AutoSize = true;
            this.lblFoldersName.Location = new System.Drawing.Point(11, 48);
            this.lblFoldersName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFoldersName.Name = "lblFoldersName";
            this.lblFoldersName.Size = new System.Drawing.Size(89, 17);
            this.lblFoldersName.TabIndex = 48;
            this.lblFoldersName.Text = "Folder Name";
            // 
            // lblSessionKeyGetFolderByName
            // 
            this.lblSessionKeyGetFolderByName.AutoSize = true;
            this.lblSessionKeyGetFolderByName.Location = new System.Drawing.Point(11, 11);
            this.lblSessionKeyGetFolderByName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSessionKeyGetFolderByName.Name = "lblSessionKeyGetFolderByName";
            this.lblSessionKeyGetFolderByName.Size = new System.Drawing.Size(82, 17);
            this.lblSessionKeyGetFolderByName.TabIndex = 47;
            this.lblSessionKeyGetFolderByName.Text = "SessionKey";
            // 
            // tabRerunWorkflow
            // 
            this.tabRerunWorkflow.Controls.Add(this.label39);
            this.tabRerunWorkflow.Controls.Add(this.txtRerunWorkflowSessionKey);
            this.tabRerunWorkflow.Controls.Add(this.btnRerunWorkflow);
            this.tabRerunWorkflow.Controls.Add(this.label37);
            this.tabRerunWorkflow.Controls.Add(this.label36);
            this.tabRerunWorkflow.Controls.Add(this.label33);
            this.tabRerunWorkflow.Controls.Add(this.txtRerunWorkflowUserName);
            this.tabRerunWorkflow.Controls.Add(this.txtRerunWorkflowPhaseGUID);
            this.tabRerunWorkflow.Controls.Add(this.txtRerunWorkflowApprovalGUID);
            this.tabRerunWorkflow.Location = new System.Drawing.Point(4, 109);
            this.tabRerunWorkflow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabRerunWorkflow.Name = "tabRerunWorkflow";
            this.tabRerunWorkflow.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabRerunWorkflow.Size = new System.Drawing.Size(579, 866);
            this.tabRerunWorkflow.TabIndex = 22;
            this.tabRerunWorkflow.Text = "RerunWorkflow";
            this.tabRerunWorkflow.UseVisualStyleBackColor = true;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(15, 16);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(86, 17);
            this.label39.TabIndex = 8;
            this.label39.Text = "Session Key";
            // 
            // txtRerunWorkflowSessionKey
            // 
            this.txtRerunWorkflowSessionKey.Location = new System.Drawing.Point(179, 12);
            this.txtRerunWorkflowSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRerunWorkflowSessionKey.Name = "txtRerunWorkflowSessionKey";
            this.txtRerunWorkflowSessionKey.Size = new System.Drawing.Size(315, 22);
            this.txtRerunWorkflowSessionKey.TabIndex = 7;
            // 
            // btnRerunWorkflow
            // 
            this.btnRerunWorkflow.Location = new System.Drawing.Point(371, 161);
            this.btnRerunWorkflow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRerunWorkflow.Name = "btnRerunWorkflow";
            this.btnRerunWorkflow.Size = new System.Drawing.Size(124, 28);
            this.btnRerunWorkflow.TabIndex = 6;
            this.btnRerunWorkflow.Text = "Rerun Workflow";
            this.btnRerunWorkflow.UseVisualStyleBackColor = true;
            this.btnRerunWorkflow.Click += new System.EventHandler(this.btnRerunWorkflow_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(11, 80);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(86, 17);
            this.label37.TabIndex = 5;
            this.label37.Text = "Phase GUID";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(11, 112);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(75, 17);
            this.label36.TabIndex = 4;
            this.label36.Text = "UserName";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(11, 48);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 17);
            this.label33.TabIndex = 3;
            this.label33.Text = "Approval GUID";
            // 
            // txtRerunWorkflowUserName
            // 
            this.txtRerunWorkflowUserName.Location = new System.Drawing.Point(179, 108);
            this.txtRerunWorkflowUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRerunWorkflowUserName.Name = "txtRerunWorkflowUserName";
            this.txtRerunWorkflowUserName.Size = new System.Drawing.Size(315, 22);
            this.txtRerunWorkflowUserName.TabIndex = 2;
            // 
            // txtRerunWorkflowPhaseGUID
            // 
            this.txtRerunWorkflowPhaseGUID.Location = new System.Drawing.Point(179, 76);
            this.txtRerunWorkflowPhaseGUID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRerunWorkflowPhaseGUID.Name = "txtRerunWorkflowPhaseGUID";
            this.txtRerunWorkflowPhaseGUID.Size = new System.Drawing.Size(315, 22);
            this.txtRerunWorkflowPhaseGUID.TabIndex = 1;
            // 
            // txtRerunWorkflowApprovalGUID
            // 
            this.txtRerunWorkflowApprovalGUID.Location = new System.Drawing.Point(179, 44);
            this.txtRerunWorkflowApprovalGUID.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtRerunWorkflowApprovalGUID.Name = "txtRerunWorkflowApprovalGUID";
            this.txtRerunWorkflowApprovalGUID.Size = new System.Drawing.Size(315, 22);
            this.txtRerunWorkflowApprovalGUID.TabIndex = 0;
            // 
            // tabCreateWorkflow
            // 
            this.tabCreateWorkflow.Controls.Add(this.chbRerunWorkflow);
            this.tabCreateWorkflow.Controls.Add(this.btnLoadWorkflowFromFile);
            this.tabCreateWorkflow.Controls.Add(this.txtCreateWorkflowUserName);
            this.tabCreateWorkflow.Controls.Add(this.label43);
            this.tabCreateWorkflow.Controls.Add(this.tabPhasesControl);
            this.tabCreateWorkflow.Controls.Add(this.btnAddNewPhase);
            this.tabCreateWorkflow.Controls.Add(this.txtWorkflowName);
            this.tabCreateWorkflow.Controls.Add(this.btnSaveWorkflow);
            this.tabCreateWorkflow.Controls.Add(this.txtCreateWorflowSessionKey);
            this.tabCreateWorkflow.Controls.Add(this.label42);
            this.tabCreateWorkflow.Controls.Add(this.label40);
            this.tabCreateWorkflow.Location = new System.Drawing.Point(4, 130);
            this.tabCreateWorkflow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCreateWorkflow.Name = "tabCreateWorkflow";
            this.tabCreateWorkflow.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCreateWorkflow.Size = new System.Drawing.Size(579, 845);
            this.tabCreateWorkflow.TabIndex = 23;
            this.tabCreateWorkflow.Text = "Create Workflow";
            this.tabCreateWorkflow.UseVisualStyleBackColor = true;
            // 
            // chbRerunWorkflow
            // 
            this.chbRerunWorkflow.AutoSize = true;
            this.chbRerunWorkflow.Location = new System.Drawing.Point(129, 132);
            this.chbRerunWorkflow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbRerunWorkflow.Name = "chbRerunWorkflow";
            this.chbRerunWorkflow.Size = new System.Drawing.Size(130, 21);
            this.chbRerunWorkflow.TabIndex = 62;
            this.chbRerunWorkflow.Text = "Rerun Worklfow";
            this.chbRerunWorkflow.UseVisualStyleBackColor = true;
            // 
            // btnLoadWorkflowFromFile
            // 
            this.btnLoadWorkflowFromFile.Location = new System.Drawing.Point(156, 802);
            this.btnLoadWorkflowFromFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLoadWorkflowFromFile.Name = "btnLoadWorkflowFromFile";
            this.btnLoadWorkflowFromFile.Size = new System.Drawing.Size(191, 28);
            this.btnLoadWorkflowFromFile.TabIndex = 55;
            this.btnLoadWorkflowFromFile.Text = "Load Workflow From File";
            this.btnLoadWorkflowFromFile.UseVisualStyleBackColor = true;
            this.btnLoadWorkflowFromFile.Click += new System.EventHandler(this.btnLoadWorkflowFromFile_Click);
            // 
            // txtCreateWorkflowUserName
            // 
            this.txtCreateWorkflowUserName.Location = new System.Drawing.Point(131, 91);
            this.txtCreateWorkflowUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCreateWorkflowUserName.Name = "txtCreateWorkflowUserName";
            this.txtCreateWorkflowUserName.Size = new System.Drawing.Size(291, 22);
            this.txtCreateWorkflowUserName.TabIndex = 54;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(48, 95);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(73, 17);
            this.label43.TabIndex = 0;
            this.label43.Text = "Username";
            // 
            // tabPhasesControl
            // 
            this.tabPhasesControl.Controls.Add(this.tabPhases);
            this.tabPhasesControl.Location = new System.Drawing.Point(4, 160);
            this.tabPhasesControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPhasesControl.Name = "tabPhasesControl";
            this.tabPhasesControl.SelectedIndex = 0;
            this.tabPhasesControl.Size = new System.Drawing.Size(564, 633);
            this.tabPhasesControl.TabIndex = 4;
            // 
            // tabPhases
            // 
            this.tabPhases.Location = new System.Drawing.Point(4, 25);
            this.tabPhases.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPhases.Name = "tabPhases";
            this.tabPhases.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPhases.Size = new System.Drawing.Size(556, 604);
            this.tabPhases.TabIndex = 0;
            this.tabPhases.Text = "Phase_1";
            this.tabPhases.UseVisualStyleBackColor = true;
            // 
            // btnAddNewPhase
            // 
            this.btnAddNewPhase.Location = new System.Drawing.Point(355, 802);
            this.btnAddNewPhase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddNewPhase.Name = "btnAddNewPhase";
            this.btnAddNewPhase.Size = new System.Drawing.Size(100, 28);
            this.btnAddNewPhase.TabIndex = 53;
            this.btnAddNewPhase.Text = "New Phase";
            this.btnAddNewPhase.UseVisualStyleBackColor = true;
            this.btnAddNewPhase.Click += new System.EventHandler(this.btnAddNewPhase_Click);
            // 
            // txtWorkflowName
            // 
            this.txtWorkflowName.Location = new System.Drawing.Point(131, 55);
            this.txtWorkflowName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtWorkflowName.Name = "txtWorkflowName";
            this.txtWorkflowName.Size = new System.Drawing.Size(291, 22);
            this.txtWorkflowName.TabIndex = 3;
            // 
            // btnSaveWorkflow
            // 
            this.btnSaveWorkflow.Location = new System.Drawing.Point(463, 802);
            this.btnSaveWorkflow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSaveWorkflow.Name = "btnSaveWorkflow";
            this.btnSaveWorkflow.Size = new System.Drawing.Size(100, 28);
            this.btnSaveWorkflow.TabIndex = 52;
            this.btnSaveWorkflow.Text = "Save";
            this.btnSaveWorkflow.UseVisualStyleBackColor = true;
            this.btnSaveWorkflow.Click += new System.EventHandler(this.btnSaveWorkflow_Click);
            // 
            // txtCreateWorflowSessionKey
            // 
            this.txtCreateWorflowSessionKey.Location = new System.Drawing.Point(129, 21);
            this.txtCreateWorflowSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCreateWorflowSessionKey.Name = "txtCreateWorflowSessionKey";
            this.txtCreateWorflowSessionKey.Size = new System.Drawing.Size(291, 22);
            this.txtCreateWorflowSessionKey.TabIndex = 2;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(35, 25);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(86, 17);
            this.label42.TabIndex = 1;
            this.label42.Text = "Session Key";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(11, 59);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 17);
            this.label40.TabIndex = 0;
            this.label40.Text = "Workflow Name";
            // 
            // tabCompareVersions
            // 
            this.tabCompareVersions.Controls.Add(this.txtCompareVersionsUsername);
            this.tabCompareVersions.Controls.Add(this.label47);
            this.tabCompareVersions.Controls.Add(this.btnCompareVersionsSend);
            this.tabCompareVersions.Controls.Add(this.txtCompareVersionsV2);
            this.tabCompareVersions.Controls.Add(this.txtCompareVersionsV1);
            this.tabCompareVersions.Controls.Add(this.txtCompareVersionsSessionKey);
            this.tabCompareVersions.Controls.Add(this.label46);
            this.tabCompareVersions.Controls.Add(this.label45);
            this.tabCompareVersions.Controls.Add(this.label44);
            this.tabCompareVersions.Location = new System.Drawing.Point(4, 130);
            this.tabCompareVersions.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCompareVersions.Name = "tabCompareVersions";
            this.tabCompareVersions.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabCompareVersions.Size = new System.Drawing.Size(579, 845);
            this.tabCompareVersions.TabIndex = 24;
            this.tabCompareVersions.Text = "CompareVersions";
            this.tabCompareVersions.UseVisualStyleBackColor = true;
            // 
            // txtCompareVersionsUsername
            // 
            this.txtCompareVersionsUsername.Location = new System.Drawing.Point(192, 46);
            this.txtCompareVersionsUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCompareVersionsUsername.Name = "txtCompareVersionsUsername";
            this.txtCompareVersionsUsername.Size = new System.Drawing.Size(305, 22);
            this.txtCompareVersionsUsername.TabIndex = 19;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(15, 49);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(73, 17);
            this.label47.TabIndex = 18;
            this.label47.Text = "Username";
            // 
            // btnCompareVersionsSend
            // 
            this.btnCompareVersionsSend.Location = new System.Drawing.Point(399, 151);
            this.btnCompareVersionsSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCompareVersionsSend.Name = "btnCompareVersionsSend";
            this.btnCompareVersionsSend.Size = new System.Drawing.Size(100, 28);
            this.btnCompareVersionsSend.TabIndex = 17;
            this.btnCompareVersionsSend.Text = "Send";
            this.btnCompareVersionsSend.UseVisualStyleBackColor = true;
            this.btnCompareVersionsSend.Click += new System.EventHandler(this.btnCompareVersionsSend_Click);
            // 
            // txtCompareVersionsV2
            // 
            this.txtCompareVersionsV2.Location = new System.Drawing.Point(192, 108);
            this.txtCompareVersionsV2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCompareVersionsV2.Name = "txtCompareVersionsV2";
            this.txtCompareVersionsV2.Size = new System.Drawing.Size(305, 22);
            this.txtCompareVersionsV2.TabIndex = 16;
            // 
            // txtCompareVersionsV1
            // 
            this.txtCompareVersionsV1.Location = new System.Drawing.Point(192, 76);
            this.txtCompareVersionsV1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCompareVersionsV1.Name = "txtCompareVersionsV1";
            this.txtCompareVersionsV1.Size = new System.Drawing.Size(305, 22);
            this.txtCompareVersionsV1.TabIndex = 16;
            // 
            // txtCompareVersionsSessionKey
            // 
            this.txtCompareVersionsSessionKey.Location = new System.Drawing.Point(192, 14);
            this.txtCompareVersionsSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCompareVersionsSessionKey.Name = "txtCompareVersionsSessionKey";
            this.txtCompareVersionsSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtCompareVersionsSessionKey.TabIndex = 16;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(15, 112);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(64, 17);
            this.label46.TabIndex = 15;
            this.label46.Text = "Version2";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(15, 80);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(64, 17);
            this.label45.TabIndex = 15;
            this.label45.Text = "Version1";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(15, 17);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(82, 17);
            this.label44.TabIndex = 15;
            this.label44.Text = "SessionKey";
            // 
            // tabAddCollaboratorToPhase
            // 
            this.tabAddCollaboratorToPhase.Controls.Add(this.btnAddCollabToPhaseSave);
            this.tabAddCollaboratorToPhase.Controls.Add(this.btnAddCollabToPhaseLoadFromFile);
            this.tabAddCollaboratorToPhase.Controls.Add(this.lblAddCollabToPhaseCollabs);
            this.tabAddCollaboratorToPhase.Controls.Add(this.dataGridViewAddCollabToPhase);
            this.tabAddCollaboratorToPhase.Controls.Add(this.txtAddCollabToPhasePhaseGuid);
            this.tabAddCollaboratorToPhase.Controls.Add(this.txtAddCollabToPhaseAppGuid);
            this.tabAddCollaboratorToPhase.Controls.Add(this.lblAddCollabToPhasePhaseGuid);
            this.tabAddCollaboratorToPhase.Controls.Add(this.lblAddCollabToPhaseAppGuid);
            this.tabAddCollaboratorToPhase.Controls.Add(this.txtAddCollabToPhaseUserName);
            this.tabAddCollaboratorToPhase.Controls.Add(this.lblAddCollabToPhaseUserName);
            this.tabAddCollaboratorToPhase.Controls.Add(this.txtAddCollabToPhaseSessionKey);
            this.tabAddCollaboratorToPhase.Controls.Add(this.lblAddCollabToPhaseSessionKey);
            this.tabAddCollaboratorToPhase.Location = new System.Drawing.Point(4, 130);
            this.tabAddCollaboratorToPhase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabAddCollaboratorToPhase.Name = "tabAddCollaboratorToPhase";
            this.tabAddCollaboratorToPhase.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabAddCollaboratorToPhase.Size = new System.Drawing.Size(579, 845);
            this.tabAddCollaboratorToPhase.TabIndex = 24;
            this.tabAddCollaboratorToPhase.Text = "Add Collaborator To Phase";
            this.tabAddCollaboratorToPhase.UseVisualStyleBackColor = true;
            // 
            // btnAddCollabToPhaseSave
            // 
            this.btnAddCollabToPhaseSave.Location = new System.Drawing.Point(468, 379);
            this.btnAddCollabToPhaseSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddCollabToPhaseSave.Name = "btnAddCollabToPhaseSave";
            this.btnAddCollabToPhaseSave.Size = new System.Drawing.Size(100, 28);
            this.btnAddCollabToPhaseSave.TabIndex = 46;
            this.btnAddCollabToPhaseSave.Text = "Save";
            this.btnAddCollabToPhaseSave.UseVisualStyleBackColor = true;
            this.btnAddCollabToPhaseSave.Click += new System.EventHandler(this.btnAddCollabToPhaseSave_Click);
            // 
            // btnAddCollabToPhaseLoadFromFile
            // 
            this.btnAddCollabToPhaseLoadFromFile.Location = new System.Drawing.Point(261, 379);
            this.btnAddCollabToPhaseLoadFromFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddCollabToPhaseLoadFromFile.Name = "btnAddCollabToPhaseLoadFromFile";
            this.btnAddCollabToPhaseLoadFromFile.Size = new System.Drawing.Size(199, 28);
            this.btnAddCollabToPhaseLoadFromFile.TabIndex = 45;
            this.btnAddCollabToPhaseLoadFromFile.Text = "Load Collaborators From File";
            this.btnAddCollabToPhaseLoadFromFile.UseVisualStyleBackColor = true;
            this.btnAddCollabToPhaseLoadFromFile.Click += new System.EventHandler(this.btnAddCollabToPhaseLoadFromFile_Click);
            // 
            // lblAddCollabToPhaseCollabs
            // 
            this.lblAddCollabToPhaseCollabs.AutoSize = true;
            this.lblAddCollabToPhaseCollabs.Location = new System.Drawing.Point(31, 167);
            this.lblAddCollabToPhaseCollabs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddCollabToPhaseCollabs.Name = "lblAddCollabToPhaseCollabs";
            this.lblAddCollabToPhaseCollabs.Size = new System.Drawing.Size(92, 17);
            this.lblAddCollabToPhaseCollabs.TabIndex = 44;
            this.lblAddCollabToPhaseCollabs.Text = "Collaborators";
            // 
            // dataGridViewAddCollabToPhase
            // 
            this.dataGridViewAddCollabToPhase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAddCollabToPhase.Location = new System.Drawing.Point(8, 187);
            this.dataGridViewAddCollabToPhase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dataGridViewAddCollabToPhase.Name = "dataGridViewAddCollabToPhase";
            this.dataGridViewAddCollabToPhase.Size = new System.Drawing.Size(560, 185);
            this.dataGridViewAddCollabToPhase.TabIndex = 43;
            // 
            // txtAddCollabToPhasePhaseGuid
            // 
            this.txtAddCollabToPhasePhaseGuid.Location = new System.Drawing.Point(129, 117);
            this.txtAddCollabToPhasePhaseGuid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAddCollabToPhasePhaseGuid.Name = "txtAddCollabToPhasePhaseGuid";
            this.txtAddCollabToPhasePhaseGuid.Size = new System.Drawing.Size(291, 22);
            this.txtAddCollabToPhasePhaseGuid.TabIndex = 42;
            // 
            // txtAddCollabToPhaseAppGuid
            // 
            this.txtAddCollabToPhaseAppGuid.Location = new System.Drawing.Point(129, 85);
            this.txtAddCollabToPhaseAppGuid.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAddCollabToPhaseAppGuid.Name = "txtAddCollabToPhaseAppGuid";
            this.txtAddCollabToPhaseAppGuid.Size = new System.Drawing.Size(291, 22);
            this.txtAddCollabToPhaseAppGuid.TabIndex = 41;
            // 
            // lblAddCollabToPhasePhaseGuid
            // 
            this.lblAddCollabToPhasePhaseGuid.AccessibleRole = System.Windows.Forms.AccessibleRole.Caret;
            this.lblAddCollabToPhasePhaseGuid.AutoSize = true;
            this.lblAddCollabToPhasePhaseGuid.Location = new System.Drawing.Point(39, 121);
            this.lblAddCollabToPhasePhaseGuid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddCollabToPhasePhaseGuid.Name = "lblAddCollabToPhasePhaseGuid";
            this.lblAddCollabToPhasePhaseGuid.Size = new System.Drawing.Size(82, 17);
            this.lblAddCollabToPhasePhaseGuid.TabIndex = 40;
            this.lblAddCollabToPhasePhaseGuid.Text = "Phase Guid";
            // 
            // lblAddCollabToPhaseAppGuid
            // 
            this.lblAddCollabToPhaseAppGuid.AutoSize = true;
            this.lblAddCollabToPhaseAppGuid.Location = new System.Drawing.Point(23, 89);
            this.lblAddCollabToPhaseAppGuid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddCollabToPhaseAppGuid.Name = "lblAddCollabToPhaseAppGuid";
            this.lblAddCollabToPhaseAppGuid.Size = new System.Drawing.Size(98, 17);
            this.lblAddCollabToPhaseAppGuid.TabIndex = 39;
            this.lblAddCollabToPhaseAppGuid.Text = "Approval Guid";
            // 
            // txtAddCollabToPhaseUserName
            // 
            this.txtAddCollabToPhaseUserName.Location = new System.Drawing.Point(129, 53);
            this.txtAddCollabToPhaseUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAddCollabToPhaseUserName.Name = "txtAddCollabToPhaseUserName";
            this.txtAddCollabToPhaseUserName.Size = new System.Drawing.Size(291, 22);
            this.txtAddCollabToPhaseUserName.TabIndex = 38;
            // 
            // lblAddCollabToPhaseUserName
            // 
            this.lblAddCollabToPhaseUserName.AutoSize = true;
            this.lblAddCollabToPhaseUserName.Location = new System.Drawing.Point(48, 57);
            this.lblAddCollabToPhaseUserName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddCollabToPhaseUserName.Name = "lblAddCollabToPhaseUserName";
            this.lblAddCollabToPhaseUserName.Size = new System.Drawing.Size(73, 17);
            this.lblAddCollabToPhaseUserName.TabIndex = 37;
            this.lblAddCollabToPhaseUserName.Text = "Username";
            // 
            // txtAddCollabToPhaseSessionKey
            // 
            this.txtAddCollabToPhaseSessionKey.Location = new System.Drawing.Point(129, 21);
            this.txtAddCollabToPhaseSessionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtAddCollabToPhaseSessionKey.Name = "txtAddCollabToPhaseSessionKey";
            this.txtAddCollabToPhaseSessionKey.Size = new System.Drawing.Size(291, 22);
            this.txtAddCollabToPhaseSessionKey.TabIndex = 5;
            // 
            // lblAddCollabToPhaseSessionKey
            // 
            this.lblAddCollabToPhaseSessionKey.AutoSize = true;
            this.lblAddCollabToPhaseSessionKey.Location = new System.Drawing.Point(35, 25);
            this.lblAddCollabToPhaseSessionKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddCollabToPhaseSessionKey.Name = "lblAddCollabToPhaseSessionKey";
            this.lblAddCollabToPhaseSessionKey.Size = new System.Drawing.Size(86, 17);
            this.lblAddCollabToPhaseSessionKey.TabIndex = 4;
            this.lblAddCollabToPhaseSessionKey.Text = "Session Key";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1211, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 28);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.panel1);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1323, 979);
            this.panelMain.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grpLog);
            this.panel1.Controls.Add(this.tabControlCollaborateAPI);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1323, 979);
            this.panel1.TabIndex = 2;
            // 
            // grpLog
            // 
            this.grpLog.Controls.Add(this.pictureBox1);
            this.grpLog.Controls.Add(this.txtLog);
            this.grpLog.Controls.Add(this.pnlLogCommands);
            this.grpLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpLog.Location = new System.Drawing.Point(587, 0);
            this.grpLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpLog.Name = "grpLog";
            this.grpLog.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpLog.Size = new System.Drawing.Size(736, 979);
            this.grpLog.TabIndex = 3;
            this.grpLog.TabStop = false;
            this.grpLog.Text = "Events log";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(9, 1);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(720, 745);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 36;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // txtLog
            // 
            this.txtLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLog.Location = new System.Drawing.Point(4, 19);
            this.txtLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLog.Size = new System.Drawing.Size(728, 909);
            this.txtLog.TabIndex = 1;
            this.txtLog.TabStop = false;
            this.txtLog.Text = "Events log ...";
            // 
            // pnlLogCommands
            // 
            this.pnlLogCommands.Controls.Add(this.btnExport);
            this.pnlLogCommands.Controls.Add(this.btnClearLog);
            this.pnlLogCommands.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlLogCommands.Location = new System.Drawing.Point(4, 928);
            this.pnlLogCommands.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlLogCommands.Name = "pnlLogCommands";
            this.pnlLogCommands.Size = new System.Drawing.Size(728, 47);
            this.pnlLogCommands.TabIndex = 2;
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.Location = new System.Drawing.Point(620, 12);
            this.btnExport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(100, 28);
            this.btnExport.TabIndex = 1;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnClearLog
            // 
            this.btnClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearLog.Location = new System.Drawing.Point(493, 12);
            this.btnClearLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(100, 28);
            this.btnClearLog.TabIndex = 0;
            this.btnClearLog.Text = "Clear";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // panelBottom
            // 
            this.panelBottom.Controls.Add(this.cmbServicesURL);
            this.panelBottom.Controls.Add(this.label18);
            this.panelBottom.Controls.Add(this.btnClose);
            this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBottom.Location = new System.Drawing.Point(0, 979);
            this.panelBottom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panelBottom.Name = "panelBottom";
            this.panelBottom.Size = new System.Drawing.Size(1323, 50);
            this.panelBottom.TabIndex = 3;
            // 
            // cmbServicesURL
            // 
            this.cmbServicesURL.FormattingEnabled = true;
            this.cmbServicesURL.Location = new System.Drawing.Point(123, 14);
            this.cmbServicesURL.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbServicesURL.Name = "cmbServicesURL";
            this.cmbServicesURL.Size = new System.Drawing.Size(265, 24);
            this.cmbServicesURL.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 17);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(91, 17);
            this.label18.TabIndex = 2;
            this.label18.Text = "Service URL:";
            // 
            // openFileDialogForLoadingWorkflowDetails
            // 
            this.openFileDialogForLoadingWorkflowDetails.FileName = "openFileDialog1";
            // 
            // tabChecklist
            // 
            this.tabChecklist.Controls.Add(this.txtGetChecklistUsername);
            this.tabChecklist.Controls.Add(this.label55);
            this.tabChecklist.Controls.Add(this.btnGetChecklist);
            this.tabChecklist.Controls.Add(this.txtGetChecklistSessionKey);
            this.tabChecklist.Controls.Add(this.label57);
            this.tabChecklist.Location = new System.Drawing.Point(4, 130);
            this.tabChecklist.Name = "tabChecklist";
            this.tabChecklist.Size = new System.Drawing.Size(579, 845);
            this.tabChecklist.TabIndex = 69;
            this.tabChecklist.Text = "Get Checklist";
            this.tabChecklist.UseVisualStyleBackColor = true;
            // 
            // txtGetChecklistUsername
            // 
            this.txtGetChecklistUsername.Location = new System.Drawing.Point(188, 53);
            this.txtGetChecklistUsername.Margin = new System.Windows.Forms.Padding(4);
            this.txtGetChecklistUsername.Name = "txtGetChecklistUsername";
            this.txtGetChecklistUsername.Size = new System.Drawing.Size(305, 22);
            this.txtGetChecklistUsername.TabIndex = 47;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(9, 56);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(73, 17);
            this.label55.TabIndex = 46;
            this.label55.Text = "Username";
            // 
            // btnGetChecklist
            // 
            this.btnGetChecklist.Location = new System.Drawing.Point(355, 85);
            this.btnGetChecklist.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetChecklist.Name = "btnGetChecklist";
            this.btnGetChecklist.Size = new System.Drawing.Size(140, 28);
            this.btnGetChecklist.TabIndex = 45;
            this.btnGetChecklist.Text = "Display Checklist";
            this.btnGetChecklist.UseVisualStyleBackColor = true;
            this.btnGetChecklist.Click += new System.EventHandler(this.btnGetChecklist_Click);
            // 
            // txtGetChecklistSessionKey
            // 
            this.txtGetChecklistSessionKey.Location = new System.Drawing.Point(188, 17);
            this.txtGetChecklistSessionKey.Margin = new System.Windows.Forms.Padding(4);
            this.txtGetChecklistSessionKey.Name = "txtGetChecklistSessionKey";
            this.txtGetChecklistSessionKey.Size = new System.Drawing.Size(305, 22);
            this.txtGetChecklistSessionKey.TabIndex = 43;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(11, 21);
            this.label57.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(82, 17);
            this.label57.TabIndex = 41;
            this.label57.Text = "SessionKey";
            // 
            // txtChecklistGuid
            // 
            this.txtChecklistGuid.Location = new System.Drawing.Point(191, 271);
            this.txtChecklistGuid.Margin = new System.Windows.Forms.Padding(4);
            this.txtChecklistGuid.Name = "txtChecklistGuid";
            this.txtChecklistGuid.Size = new System.Drawing.Size(305, 22);
            this.txtChecklistGuid.TabIndex = 51;
            // 
            // label56
            // 
            this.label56.Location = new System.Drawing.Point(15, 275);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(153, 22);
            this.label56.TabIndex = 52;
            this.label56.Text = "Checklist";
            // 
            // FrmCollaborateAPITestMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1323, 1029);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelBottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmCollaborateAPITestMain";
            this.Text = "GMG CoZone Collaborate API Test";
            this.Load += new System.EventHandler(this.FrmCollaborateAPITestMain_Load);
            this.tabControlCollaborateAPI.ResumeLayout(false);
            this.tabPageStartSession.ResumeLayout(false);
            this.tabPageStartSession.PerformLayout();
            this.tabConfirmSes.ResumeLayout(false);
            this.tabConfirmSes.PerformLayout();
            this.tabListJobs.ResumeLayout(false);
            this.tabListJobs.PerformLayout();
            this.tabDisplayJob.ResumeLayout(false);
            this.tabDisplayJob.PerformLayout();
            this.tabPageCreateJob.ResumeLayout(false);
            this.tabPageCreateJob.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCollaborators)).EndInit();
            this.tabStatusJob.ResumeLayout(false);
            this.tabStatusJob.PerformLayout();
            this.tabPageGetAnnReport.ResumeLayout(false);
            this.tabPageGetAnnReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUsers)).EndInit();
            this.tabPageGetAnnotationList.ResumeLayout(false);
            this.tabPageGetAnnotationList.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAnnotationListGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAnnotationListUsers)).EndInit();
            this.tabPageGetAllVersionsAnnotReport.ResumeLayout(false);
            this.tabPageGetAllVersionsAnnotReport.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllVersionsReportGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAllVersionsReportUsers)).EndInit();
            this.tabPageUpdateJob.ResumeLayout(false);
            this.tabPageUpdateJob.PerformLayout();
            this.tabPageDeleteApproval.ResumeLayout(false);
            this.tabPageDeleteApproval.PerformLayout();
            this.tabPageDeleteJob.ResumeLayout(false);
            this.tabPageDeleteJob.PerformLayout();
            this.tabRestoreApproval.ResumeLayout(false);
            this.tabRestoreApproval.PerformLayout();
            this.tabPageGetApproval.ResumeLayout(false);
            this.tabPageGetApproval.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictBxGetApp)).EndInit();
            this.tabUpdateApproval.ResumeLayout(false);
            this.tabUpdateApproval.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUpdateCollaborators)).EndInit();
            this.tabUpdateApprovalPhaseDeadline.ResumeLayout(false);
            this.tabUpdateApprovalPhaseDeadline.PerformLayout();
            this.tabProofStudioURL.ResumeLayout(false);
            this.tabProofStudioURL.PerformLayout();
            this.tabCreateFolder.ResumeLayout(false);
            this.tabCreateFolder.PerformLayout();
            this.tabGetAllFolders.ResumeLayout(false);
            this.tabGetAllFolders.PerformLayout();
            this.tabCreateUser.ResumeLayout(false);
            this.tabCreateUser.PerformLayout();
            this.tabCreateExternalUser.ResumeLayout(false);
            this.tabCreateExternalUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUserGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPermissions)).EndInit();
            this.tabListApprovalWorkflows.ResumeLayout(false);
            this.tabListApprovalWorkflows.PerformLayout();
            this.tabCheckUser.ResumeLayout(false);
            this.tabCheckUser.PerformLayout();
            this.tabGetWorkflowDetails.ResumeLayout(false);
            this.tabGetWorkflowDetails.PerformLayout();
            this.tabUpdateApprovalDecision.ResumeLayout(false);
            this.tabUpdateApprovalDecision.PerformLayout();
            this.tabGetFolderByName.ResumeLayout(false);
            this.tabGetFolderByName.PerformLayout();
            this.tabRerunWorkflow.ResumeLayout(false);
            this.tabRerunWorkflow.PerformLayout();
            this.tabCreateWorkflow.ResumeLayout(false);
            this.tabCreateWorkflow.PerformLayout();
            this.tabPhasesControl.ResumeLayout(false);
            this.tabCompareVersions.ResumeLayout(false);
            this.tabCompareVersions.PerformLayout();
            this.tabAddCollaboratorToPhase.ResumeLayout(false);
            this.tabAddCollaboratorToPhase.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAddCollabToPhase)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.grpLog.ResumeLayout(false);
            this.grpLog.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlLogCommands.ResumeLayout(false);
            this.panelBottom.ResumeLayout(false);
            this.panelBottom.PerformLayout();
            this.tabChecklist.ResumeLayout(false);
            this.tabChecklist.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlCollaborateAPI;
        private System.Windows.Forms.TabPage tabPageStartSession;
        private System.Windows.Forms.TabPage tabPageGetAnnReport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.TabPage tabPageDeleteApproval;
        private System.Windows.Forms.TextBox txtAccountURL;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblAccountURL;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabPage tabPageDeleteJob;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDeleteJobKey;
        private System.Windows.Forms.TextBox txtDeleteSessionKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDeleteJob;
        private System.Windows.Forms.TabPage tabDisplayJob;
        private System.Windows.Forms.Button btnDisplayJob;
        private System.Windows.Forms.TextBox txtDisplayJobKey;
        private System.Windows.Forms.TextBox txtDisplayJobSessionKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabStatusJob;
        private System.Windows.Forms.Button btnStatusJob;
        private System.Windows.Forms.TextBox txtStatusJobKey;
        private System.Windows.Forms.TextBox txtStatusJobSessionKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabRestoreApproval;
        private System.Windows.Forms.TextBox txtSessionKeyForRestoreApp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnRestoreApproval;
        private System.Windows.Forms.TabPage tabPageCreateJob;
        private System.Windows.Forms.TextBox txtConversionProfileKey;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPreflightProfileKey;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtJobName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSessionKeyForCreateJob;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnFileOpen;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.OpenFileDialog openFileDialogForCreateJob;
        private System.Windows.Forms.TextBox txtCallBackUrl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnCreateJob;
        private System.Windows.Forms.TabPage tabListJobs;
        private System.Windows.Forms.Button btnListJobs;
        private System.Windows.Forms.TextBox txtSessionKeyForListJobs;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabConfirmSes;
        private System.Windows.Forms.Button btnConfirmSession;
        private System.Windows.Forms.TextBox txtSessionKeyForConfirm;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtDeleteAppKey;
        private System.Windows.Forms.TextBox txtSessionKeyDeleteApp;
        private System.Windows.Forms.Label lblTransferJobJobKey;
        private System.Windows.Forms.Label lblTransferJobSessionKey;
        private System.Windows.Forms.Button btnDeleteApproval;
        private System.Windows.Forms.Panel pnlLogCommands;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnClearLog;
        private System.Windows.Forms.Button btnGetReport;
        private System.Windows.Forms.TextBox txtJobKeyForAnnReport;
        private System.Windows.Forms.TextBox txtSessionKeyForAnnReport;
        private System.Windows.Forms.Label lblJobKeyForRetrieveJob;
        private System.Windows.Forms.Label lblSessionKeyForRetrieveJob;
        private System.Windows.Forms.GroupBox grpLog;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox cmbServicesURL;
        private System.Windows.Forms.TextBox txtConfirmSesPass;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbSortBy;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbxDisplayOpt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCmtsPerPage;
        private System.Windows.Forms.ComboBox cbxFilterBy;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox checkSumaryPage;
        private System.Windows.Forms.DataGridView dataGridViewUsers;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridView dataGridViewGroups;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox cbxAllowToDownload;
        private System.Windows.Forms.CheckBox cbxOneDecionRequired;
        private System.Windows.Forms.CheckBox cbxLockWhenAllDecisonAreMade;
        private System.Windows.Forms.CheckBox cbxLockWhenFirstDecisonMade;
        private System.Windows.Forms.DataGridView dataGridViewCollaborators;
        private System.Windows.Forms.DateTimePicker deadline;
        private System.Windows.Forms.TextBox txtApprovalRestoreKey;
        private System.Windows.Forms.TabPage tabPageGetAnnotationList;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtSessionKeyAnnotationList;
        private System.Windows.Forms.TextBox txtApprovalKeyAnnotationList;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbxFilterByAnnList;
        private System.Windows.Forms.ComboBox cbxSortByAnnList;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridView dataGridViewAnnotationListUsers;
        private System.Windows.Forms.DataGridView dataGridViewAnnotationListGroups;
        private System.Windows.Forms.Label Groups;
        private System.Windows.Forms.Button btnGetAnnotationList;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPageUpdateJob;
        private System.Windows.Forms.Label lblSessKeyUpdJob;
        private System.Windows.Forms.Label lblJobKeyUpdateJob;
        private System.Windows.Forms.Button btnUpdateJob;
        private System.Windows.Forms.Label lblJobStatusUpdateJob;
        private System.Windows.Forms.ComboBox cbxJobStatUpdJob;
        private System.Windows.Forms.TextBox txtJobTitleUpdJob;
        private System.Windows.Forms.Label lblJobTitleUpdJob;
        private System.Windows.Forms.TextBox jobKeyForUpdJob;
        private System.Windows.Forms.Button btnCreateVersion;
        private System.Windows.Forms.TextBox txtJobKey;
        private System.Windows.Forms.Label lblJobKey;
        private System.Windows.Forms.TextBox txtSessionKeyForUpdJob;
        private System.Windows.Forms.TabPage tabPageGetApproval;
        private System.Windows.Forms.Label lblGetAppSK;
        private System.Windows.Forms.Label labGetAppGuid;
        private System.Windows.Forms.TextBox txtGetAppForAppGuid;
        private System.Windows.Forms.TextBox txtSessionKeyForGetApp;
        private System.Windows.Forms.Button btnGetApp;
        private System.Windows.Forms.PictureBox pictBxGetApp;
        private System.Windows.Forms.Button btnGetAppAccess;
        private System.Windows.Forms.TabPage tabUpdateApproval;
        private System.Windows.Forms.TabPage tabUpdateApprovalPhaseDeadline;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DateTimePicker updateApprovalDeadline;
        private System.Windows.Forms.DateTimePicker updateApprovalPhaseDeadline;
        private System.Windows.Forms.DataGridView dataGridViewUpdateCollaborators;
        private System.Windows.Forms.Button btnUpdateApproval;
        private System.Windows.Forms.Button btnUpdateApprovalPhaseDeadline;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblApprovalPhaseDeadline_Deadline;
        private System.Windows.Forms.TextBox txtUpdateApprovalSessionKey;
        private System.Windows.Forms.TextBox txtUpdateApprovalPhaseDeadlineSessionKey;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label lblUpdateApprovalPhaseDeadlineSessionKey;
        private System.Windows.Forms.TextBox txtUpdateApprovalKey;
        private System.Windows.Forms.TextBox txtUpdateApprovalPhaseDeadlineKey;
        private System.Windows.Forms.TextBox txtUpdateAPD_ApprovalPhaseKey;
        private System.Windows.Forms.Label lblApprovalKey;
        private System.Windows.Forms.Label lblApprovalPhaseDeadlineKey;
        private System.Windows.Forms.Label lblUpdateAPD_ApprovalPhaseKey;
        private System.Windows.Forms.Button btnApprovalStatus;
        private System.Windows.Forms.TabPage tabProofStudioURL;
        private System.Windows.Forms.TextBox txtProofStudioApprovalKey;
        private System.Windows.Forms.TextBox txtProofStudioSessionKey;
        private System.Windows.Forms.Label lblProofStudioApprovalKey;
        private System.Windows.Forms.Label lblProofStudioSessionKey;
        private System.Windows.Forms.Button btnGetProofStudioURL;
        private System.Windows.Forms.TabPage tabCreateFolder;
        private System.Windows.Forms.Label lblFolderName;
        private System.Windows.Forms.TextBox txtParentFolderGuid;
        private System.Windows.Forms.TextBox txtFolderName;
        private System.Windows.Forms.Label lblParentFolderGuid;
        private System.Windows.Forms.Button btnCreateNewFolder;
        private System.Windows.Forms.TextBox txtSessionKeyCreateFolder;
        private System.Windows.Forms.Label lblSessionKeyCreateFolder;
        private System.Windows.Forms.TabPage tabGetAllFolders;
        private System.Windows.Forms.Button btnGetAllFolders;
        private System.Windows.Forms.TextBox txtSessionKeyGetAllFolders;
        private System.Windows.Forms.Label lblSessionKeyGetAllFolders;
        private System.Windows.Forms.TabPage tabCreateUser;
        private System.Windows.Forms.TextBox txtUserPassword;
        private System.Windows.Forms.Label lblPasswordforUser;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmailForCreateUser;
        private System.Windows.Forms.TextBox txtUserLastName;
        private System.Windows.Forms.Label lblLastNameforCreateUser;
        private System.Windows.Forms.TextBox txtUserFirstName;
        private System.Windows.Forms.Label lblFirstNameforCreateUser;
        private System.Windows.Forms.TextBox txtSessionKeyCreateUser;
        private System.Windows.Forms.Label lblSessionKeyforCreaqteUser;
        private System.Windows.Forms.Label lblUserPermissions;
        private System.Windows.Forms.DataGridView dataGridViewPermissions;
        private System.Windows.Forms.Label lblgroups;
        private System.Windows.Forms.DataGridView dataGridViewUserGroups;
        private System.Windows.Forms.Button btnCreateuser;
        private System.Windows.Forms.TabPage tabCreateExternalUser;
        private System.Windows.Forms.TextBox txtExternalEmail;
        private System.Windows.Forms.Label lblEmailForCreateExternalUser;
        private System.Windows.Forms.TextBox txtExternalUserLastName;
        private System.Windows.Forms.Label lblLastNameforCreateExternalUser;
        private System.Windows.Forms.TextBox txtExternalUserFirstName;
        private System.Windows.Forms.Label lblFirstNameforCreateExternalUser;
        private System.Windows.Forms.TextBox txtSessionKeyCreateExternalUser;
        private System.Windows.Forms.Label lblSessionKeyforCreaqteExternalUser;
        private System.Windows.Forms.Button btnCreateExternaluser;
        private System.Windows.Forms.TextBox txtWorkflowGuid;
        private System.Windows.Forms.Label lblWorkflowGuid;
        private System.Windows.Forms.TabPage tabListApprovalWorkflows;
        private System.Windows.Forms.Button btnListApprovalWorkflows;
        private System.Windows.Forms.TextBox txtListApprovalWorkflowsSessionKey;
        private System.Windows.Forms.Label lblListWorkflowsSessionKey;
        private System.Windows.Forms.TabPage tabCheckUser;
        private System.Windows.Forms.Button btnCheckUser;
        private System.Windows.Forms.TextBox txtEmailForCheckUser;
        private System.Windows.Forms.TextBox txtSessionKeyForCheckUser;
        private System.Windows.Forms.Label lblCheckUserEmail;
        private System.Windows.Forms.Label lblSessionKeyCheckUser;
        private System.Windows.Forms.TabPage tabGetWorkflowDetails;
        private System.Windows.Forms.Button btnGetWorkflowDetails;
        private System.Windows.Forms.TextBox txtApprovalWorkflowKey;
        private System.Windows.Forms.TextBox txtGetWorkflowDetailsSessionKey;
        private System.Windows.Forms.Label lblApprovalWorkflowKey;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lblApprovalWorkflowSetting;
        private System.Windows.Forms.CheckBox chbAllowUsersToBeAdded;
        private System.Windows.Forms.TabPage tabUpdateApprovalDecision;
        private System.Windows.Forms.Label lblApprovalDecisionUsername;
        private System.Windows.Forms.Label lblApprovalDecisionExternalEmail;
        private System.Windows.Forms.Label lblApprovalDecisionSessionKey;
        private System.Windows.Forms.TextBox txtApprovalDecisionUsername;
        private System.Windows.Forms.TextBox txtApprovalDecisionExternalEmail;
        private System.Windows.Forms.TextBox txtApprovalDecisionSessionKey;
        private System.Windows.Forms.Button btnUpdateApprovalDecision;
        private System.Windows.Forms.Label lblApprovalDecisionApprovalKey;
        private System.Windows.Forms.TextBox txtApprovalDecisionApprovalKey;
        private System.Windows.Forms.ComboBox cbxLoggedUserDecision;
        private System.Windows.Forms.Label lblDecision;

        private System.Windows.Forms.TextBox txtDeleteApprovalUsername;
        private System.Windows.Forms.Label lblDeleteApprovalUsername;
        private System.Windows.Forms.TextBox txtDeleteJobUsername;
        private System.Windows.Forms.Label lblDeleteJobUsername;
        private System.Windows.Forms.TextBox txtGetApprovalUsername;
        private System.Windows.Forms.Label lblGetApprovalUsername;
        private System.Windows.Forms.TextBox txtUpdateJobUsername;
        private System.Windows.Forms.Label lblUpdateJobUsername;
        private System.Windows.Forms.TextBox txtGetAnnoListUsername;
        private System.Windows.Forms.Label lblGetAnnoListUsername;
        private System.Windows.Forms.TextBox txtGetAnnoReportUsername;
        private System.Windows.Forms.Label lblGetAnnoReportUsername;
        private System.Windows.Forms.TextBox txtGetWorkflowDetailsUsername;
        private System.Windows.Forms.Label lblGetWorkflowDetailsUsername;
        private System.Windows.Forms.TextBox txtDisplayJobUsername;
        private System.Windows.Forms.Label lblDisplayJobUsername;
        private System.Windows.Forms.TextBox txtCreateFolderUsername;
        private System.Windows.Forms.Label lblCreateFolderUsername;
        private System.Windows.Forms.TextBox txtGetAllFoldersUsername;
        private System.Windows.Forms.Label lblGetAllFoldersUsername;
        private System.Windows.Forms.TextBox txtListJobsUsername;
        private System.Windows.Forms.Label lblListJobsUsername;
        private System.Windows.Forms.TextBox txtStatusJobUsername;
        private System.Windows.Forms.Label lblStatusJobUsername;
        private System.Windows.Forms.TextBox txtCreateJobUsername;
        private System.Windows.Forms.Label lblCreateJobUsername;
        private System.Windows.Forms.TextBox txtListApprovalWorkflowUsername;
        private System.Windows.Forms.Label lblListApprovalWorkflowUsername;
        private System.Windows.Forms.TextBox txtRestoreAppUsername;
        private System.Windows.Forms.Label lblRestoreAppUsername;
        private System.Windows.Forms.TextBox txtUpdateApprovalUsername;
        private System.Windows.Forms.TextBox txtUpdateApprovalPhaseDeadlineUsername;
        private System.Windows.Forms.Label lblUpdateApprovalUsername;
        private System.Windows.Forms.Label lblUpdateApprovalPhaseDeadlineUsername;
        private System.Windows.Forms.TextBox txtGetProofStudioURLUsername;
        private System.Windows.Forms.Label lblGetProofStudioURLUsername;
        private System.Windows.Forms.CheckBox cbxTestPushJobStatus;
        private System.Windows.Forms.CheckBox cbxTestPushApprovalStatus;
        private System.Windows.Forms.Button btnGetAnnotationReportURL;
        private System.Windows.Forms.Label lblPhases;
        private System.Windows.Forms.TextBox txtPhases;
        private System.Windows.Forms.TabPage tabGetFolderByName;
        private System.Windows.Forms.TextBox txtGetFolderByNameUsername;
        private System.Windows.Forms.Label lblUsernameForFolderName;
        private System.Windows.Forms.Button btnGetFolderByName;
        private System.Windows.Forms.TextBox txtFolderByName;
        private System.Windows.Forms.TextBox txtSessionKeGetFolderByName;
        private System.Windows.Forms.Label lblFoldersName;
        private System.Windows.Forms.Label lblSessionKeyGetFolderByName;
        private System.Windows.Forms.TabPage tabRerunWorkflow;
        private System.Windows.Forms.Button btnRerunWorkflow;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtRerunWorkflowUserName;
        private System.Windows.Forms.TextBox txtRerunWorkflowPhaseGUID;
        private System.Windows.Forms.TextBox txtRerunWorkflowApprovalGUID;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtRerunWorkflowSessionKey;
        private System.Windows.Forms.TabPage tabCreateWorkflow;
        private System.Windows.Forms.TabControl tabPhasesControl;
        private System.Windows.Forms.TabPage tabPhases;
        private System.Windows.Forms.TextBox txtWorkflowName;
        private System.Windows.Forms.TextBox txtCreateWorflowSessionKey;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btnSaveWorkflow;
        private System.Windows.Forms.Button btnAddNewPhase;
        private System.Windows.Forms.TextBox txtCreateWorkflowUserName;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnLoadWorkflowFromFile;
        private System.Windows.Forms.OpenFileDialog openFileDialogForLoadingWorkflowDetails;
        private System.Windows.Forms.TabPage tabCompareVersions;
        private System.Windows.Forms.TextBox txtCompareVersionsV2;
        private System.Windows.Forms.TextBox txtCompareVersionsV1;
        private System.Windows.Forms.TextBox txtCompareVersionsSessionKey;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnCompareVersionsSend;
        private System.Windows.Forms.TextBox txtCompareVersionsUsername;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage tabPageGetAllVersionsAnnotReport;
        private System.Windows.Forms.Button btnGetAllVersionsReportURL;
        private System.Windows.Forms.TextBox txtUserNameJobAnnotationReport;
        private System.Windows.Forms.DataGridView dataGridViewAllVersionsReportGroups;
        private System.Windows.Forms.DataGridView dataGridViewAllVersionsReportUsers;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtCommentsPerPageJobAnnotationReport;
        private System.Windows.Forms.ComboBox cbxFilterByJobAnnotationReport;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox cbxDisplayOptJobAnnotationReport;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox cbxSortByJobAnnotationReport;
        private System.Windows.Forms.TextBox txtJobKeyJobAnnotationReport;
        private System.Windows.Forms.TextBox txtSessionKeyJobAnnotationReport;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.CheckBox ckbSumaryPageJobAnnotationReport;
        private System.Windows.Forms.TabPage tabAddCollaboratorToPhase;
        private System.Windows.Forms.Label lblAddCollabToPhaseCollabs;
        private System.Windows.Forms.DataGridView dataGridViewAddCollabToPhase;
        private System.Windows.Forms.TextBox txtAddCollabToPhasePhaseGuid;
        private System.Windows.Forms.TextBox txtAddCollabToPhaseAppGuid;
        private System.Windows.Forms.Label lblAddCollabToPhasePhaseGuid;
        private System.Windows.Forms.Label lblAddCollabToPhaseAppGuid;
        private System.Windows.Forms.TextBox txtAddCollabToPhaseUserName;
        private System.Windows.Forms.Label lblAddCollabToPhaseUserName;
        private System.Windows.Forms.TextBox txtAddCollabToPhaseSessionKey;
        private System.Windows.Forms.Label lblAddCollabToPhaseSessionKey;
        private System.Windows.Forms.Button btnAddCollabToPhaseSave;
        private System.Windows.Forms.Button btnAddCollabToPhaseLoadFromFile;
        private System.Windows.Forms.OpenFileDialog openFileDialogForLoadingNewPhaseCollabs;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.CheckBox chkIsSSOUser;
        private System.Windows.Forms.CheckBox chkPrivateAnnotations;
        private System.Windows.Forms.CheckBox chbRerunWorkflow;
        private System.Windows.Forms.TabPage tabChecklist;
        private System.Windows.Forms.TextBox txtGetChecklistUsername;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button btnGetChecklist;
        private System.Windows.Forms.TextBox txtGetChecklistSessionKey;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtChecklistGuid;
        private System.Windows.Forms.Label label56;
    }
}

