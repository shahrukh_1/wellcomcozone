﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using GMG.CollaborateAPI.BL.Models.AnnotationModels;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Model;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;
using GMG.CollaborateAPI.BL.Models.FolderModels;
using GMGCollaborateAPITestApp.Models;
using System.Windows.Forms;

namespace GMGCollaborateAPITestApp
{
    public class CollaborateAPIClient
    {
        #region Fields

        private readonly string _resourceURL;

        private readonly string _sessionKey;

        private readonly string _username;

        #endregion

        #region Constructors

        public CollaborateAPIClient(string resourceUrl, string username)
        {
            _resourceURL = resourceUrl;
            _username = username;
        }

        public CollaborateAPIClient(string resourceUrl, string sessionKey, string username)
            : this(resourceUrl, username)
        {
            _sessionKey = sessionKey;
        }

        #endregion

        #region Public Methods

        public GetJobActivityResponse GetJobStatusRequest()
        {
            var queryParams = new NameValueCollection { { "sessionKey", _sessionKey } };
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetJobActivityResponse), queryParams) as GetJobActivityResponse;
        }

        public CreateJobMetadataResponse CreateJobMetadataRequest(CreateApprovalRequest approvalModel, string jobName, string workflowGuid, string checklistGuid, bool allowUsesrToBeAddedOnPhase)
        {
            if (jobName != null)
            {
                var request = new CreateJobRequest {jobName = jobName,  workflow = workflowGuid, checklist = checklistGuid, approval = approvalModel, allowUsersToBeAddedOnPhase = allowUsesrToBeAddedOnPhase };
                return SendRequest(_resourceURL, "POST", typeof(CreateJobMetadataResponse), null, request) as CreateJobMetadataResponse;
            }

            return SendRequest(_resourceURL, "POST", typeof(CreateJobMetadataResponse), null, approvalModel) as CreateJobMetadataResponse;
        }

        public CreateJobResponse UploadFileRequest(string resourceID, string fileName)
        {
            var queryParams = new NameValueCollection();
            queryParams.Add("sessionKey", _sessionKey);
            queryParams.Add("resourceID", resourceID);
            queryParams.Add("username", _username);
            byte[] data;

            byte[] fileContent = File.ReadAllBytes(fileName);

            using (var client = new WebClient())
            {
                client.QueryString = queryParams;
                // invoke the REST method
                data = client.UploadData(
                        _resourceURL,
                        "POST",
                        fileContent);
            }

            // put the downloaded data in a memory stream
            var ms = new MemoryStream();
            ms = new MemoryStream(data);

            // deserialize from json
            var ser = new DataContractJsonSerializer(typeof(CreateJobResponse));

            return ser.ReadObject(ms) as CreateJobResponse;
        }

        public ConfirmIdentityResponse ConfirmSessionRequest(string password)
        {
            string encodedHash, hashedAccessKey;

            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.GetEncoding(1252).GetBytes(password));
                encodedHash = Encoding.GetEncoding(1252).GetString(hash);
            }

            string accessKey = encodedHash + _sessionKey;

            using (var hasher = new SHA256Managed())
            {
                hashedAccessKey = Convert.ToBase64String(hasher.ComputeHash(Encoding.UTF8.GetBytes(accessKey)));
            }

            var queryParams = new NameValueCollection
            {
                {"sessionKey", _sessionKey},
                {"accessKey", Uri.EscapeDataString(hashedAccessKey)}
            };

            return SendRequest(_resourceURL, "GET", typeof(ConfirmIdentityResponse), queryParams) as ConfirmIdentityResponse;
        }

        public StartSessionResponse StartSessionRequest(string userName, string accountUrl)
        {
            var queryParams = new NameValueCollection {{"username", userName}};

            if (!String.IsNullOrEmpty(accountUrl))
            {
                queryParams.Add("accountURL", accountUrl);
            }

            return SendRequest(_resourceURL, "GET", typeof(StartSessionResponse), queryParams) as StartSessionResponse;
        }

        public DeleteJobResponse DeleteJobRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "DELETE", typeof(DeleteJobResponse), queryParams) as DeleteJobResponse;
        }

        public GetJobResponse GetJobRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetJobResponse), queryParams) as GetJobResponse;
        }

        public GetApprovalActivityResponse GetApprovalStatusRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetApprovalActivityResponse), queryParams) as GetApprovalActivityResponse;
        }

        public ListJobsResponse ListJobsRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(ListJobsResponse), queryParams) as ListJobsResponse;
        }

        public ListApprovalWorkflowsResponse GetAllApprovalWorkflows()
        {
            var queryParams = new NameValueCollection { { "sessionKey", _sessionKey } };
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(ListApprovalWorkflowsResponse), queryParams) as ListApprovalWorkflowsResponse;
        }

        public GetAllFoldersResponse GetAllFolders()
        {
            var queryParams = new NameValueCollection { { "sessionKey", _sessionKey } };
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetAllFoldersResponse), queryParams) as GetAllFoldersResponse;
        }

        public MemoryStream GetAnnotationReportRequest(NameValueCollection queryParams)
        {
            return SendRequest(_resourceURL, "GET", typeof(AnnotationReportResponse), queryParams) as MemoryStream;
        }

        public AnnotationReportUrlResponse GetAnnotationReportUrlRequest(NameValueCollection queryParams)
        {
            return SendRequest(_resourceURL, "GET", typeof(AnnotationReportUrlResponse), queryParams) as AnnotationReportUrlResponse;
        }

        public AnnotationListResponse GetAnnotationListRequest(NameValueCollection queryParams)
        {
            return SendRequest(_resourceURL, "GET", typeof(AnnotationListResponse), queryParams) as AnnotationListResponse;
        }

        public DeleteJobResponse DeleteApprovalRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "DELETE", typeof(DeleteJobResponse), queryParams) as DeleteJobResponse;
        }

        public RestoreApprovalResponse RestoreApprovalRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "PUT", typeof(RestoreApprovalResponse), queryParams) as RestoreApprovalResponse;
        }

        public UpdateJobResponse UpdateJobRequest(UpdateJobRequest jobReqModel)
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "PUT", typeof(UpdateJobResponse), queryParams, jobReqModel) as UpdateJobResponse;
        }

        public GetApprovalResponse GetApprovalRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetApprovalResponse), queryParams) as GetApprovalResponse;
        }

        public GetApprovalWithAccessResponse GetApprovalWithAccessRequest()
        {
            var queryParams = new NameValueCollection();
            queryParams.Add("sessionKey", _sessionKey);
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetApprovalWithAccessResponse), queryParams) as GetApprovalWithAccessResponse;
        }

        public UpdateApprovalResponse UpdateApprovalRequest(UpdateApprovalRequest approvalReqModel)
        {
            return SendRequest(_resourceURL, "PUT", typeof(UpdateApprovalResponse), null, approvalReqModel) as UpdateApprovalResponse;
        }

        public UpdateApprovalPhaseDeadlineResponse UpdateApprovalPhaseDeadlineRequest(UpdateApprovalPhaseDeadlineRequest approvalPhaseDeadlineReqModel)
        {
            return SendRequest(_resourceURL, "PUT", typeof(UpdateApprovalPhaseDeadlineResponse), null, approvalPhaseDeadlineReqModel) as UpdateApprovalPhaseDeadlineResponse;
        }

        public GetProofStudioURLResponse GetProofStudioURLRequest()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}};
            queryParams.Add("username", _username);

            return SendRequest(_resourceURL, "GET", typeof(GetProofStudioURLResponse), queryParams) as GetProofStudioURLResponse;
        }

        public FolderResponse CreateFolderRequest(CreateFolderRequest request)
        {
            return SendRequest(_resourceURL, "POST", typeof(FolderResponse), null, request) as FolderResponse;
        }
		
        public CreateUserResponse CreateExternalUserRequest(CreateExternalUserRequest req)
        {
            return SendRequest(_resourceURL, "POST", typeof(CreateUserResponse), null, req) as CreateUserResponse;
        }


		public CreateUserResponse CreateUserRequest(CreateUserRequest req)
        {
            return SendRequest(_resourceURL, "POST", typeof(CreateUserResponse), null, req) as CreateUserResponse;
        }

        public CreateUserResponse CheckIfUserExistsRequest(UserExistsRequest req)
        {
            var queryParams = new NameValueCollection { { "sessionKey", _sessionKey }, { "email", req.email } };

            return SendRequest(_resourceURL, "GET", typeof(CreateUserResponse), queryParams) as CreateUserResponse;
        }

        public ApprovalWorkflowDetailsResponse GetApprovalWorkflowDetails()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}, {"username", _username}};

            return SendRequest(_resourceURL, "GET", typeof(ApprovalWorkflowDetailsResponse), queryParams) as ApprovalWorkflowDetailsResponse;
        }

        public UpdateApprovalDecisionResponse UpdateApprovalDecision(UpdateApprovalDecisionRequest requestBody)
        {
            return SendRequest(_resourceURL, "PUT", typeof(UpdateApprovalDecisionResponse), null, requestBody) as UpdateApprovalDecisionResponse;
        }

        public GetProofStudioURLResponse GetCompareVersionsURL(NameValueCollection queryParams)
        {
            return SendRequest(_resourceURL, "GET", typeof(GetProofStudioURLResponse), queryParams) as GetProofStudioURLResponse;
        }
        
        public object GetStatus()
        {
            var queryParams = new NameValueCollection {{"sessionKey", _sessionKey}, {"username", _username}};

            return SendRequest(_resourceURL, "GET", typeof(JobStatusDetails), queryParams) as JobStatusDetails;
        }

        public GetFolderByNameResponse GetFolderByName(string fodlerName)
        {
            var queryParams = new NameValueCollection
            {
                {"sessionKey", _sessionKey},
                {"username", _username},
                {"name", fodlerName}
            };

            return SendRequest(_resourceURL, "GET", typeof(GetFolderByNameResponse), queryParams) as GetFolderByNameResponse;
        }
        
        public object RerunApprovalWorklowFromPhase(RerunFromThisPhaseRequest requestBody)
        {
            return SendRequest(_resourceURL, "PUT", typeof(RerunFromThisPhaseResponse), null, requestBody) as RerunFromThisPhaseResponse;
        }

        public object CreateApprovalWorkflow(CreateApprovalWorkflowRequest requestBody)
        {
            return SendRequest(_resourceURL, "POST", typeof(CreateApprovalWorkflowResponse), null, requestBody) as CreateApprovalWorkflowResponse;
        }

        public object AddCollaboratorsToApprovalPhase(AddCollaboratorsToApprovalPhaseRequest requestBody)
        {
            return SendRequest(_resourceURL, "PUT", typeof(AddCollaboratorsToApprovalPhaseResponse), null, requestBody) as AddCollaboratorsToApprovalPhaseResponse;
        }

        public ChecklistDetailsResponse GetChecklistDetails()
        {
            var queryParams = new NameValueCollection { { "sessionKey", _sessionKey }, { "username", _username } };

            return SendRequest(_resourceURL, "GET", typeof(ChecklistDetailsResponse), queryParams) as ChecklistDetailsResponse;
        }

        #endregion

        #region Private Methods

        object SendRequest(string methodURL, string httpMethod, Type responseType, NameValueCollection queryParams = null, object body = null)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers["Content-type"] = "application/json";

                    if (queryParams != null)
                    {
                        client.QueryString = queryParams;
                    }

                    byte[] data;
                    if (httpMethod.Equals("GET"))
                    {
                        data = client.DownloadData(methodURL);
                    }
                    else
                    {
                        var serializer = new JavaScriptSerializer();
                        serializer.MaxJsonLength = 104857600;
                        string serializedBody = String.Empty;
                        if (body != null)
                        {
                            serializedBody = serializer.Serialize(body);
                        }

                        // invoke the REST method
                        data = client.UploadData(
                               methodURL,
                               httpMethod,
                               Encoding.UTF8.GetBytes(serializedBody));
                    }

                    // put the downloaded data in a memory stream
                    var ms = new MemoryStream(data);

                    if (responseType != typeof(AnnotationReportResponse))
                    {
                        // deserialize from json
                        var ser = new DataContractJsonSerializer(responseType,
                            new List<Type>() { typeof(Collaborator), typeof(GetApprovalResponse) });

                        return ser.ReadObject(ms);
                    }
                    return ms;
                }
            }
            catch (WebException webEx)
            {
                using (var stream = webEx.Response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string text = reader.ReadToEnd();

                        List<RuleViolation> violations = (new JavaScriptSerializer()).Deserialize<List<RuleViolation>>(text);
                        var response = new ResponseErrors() { errors = violations };

                        if (System.Windows.Forms.Application.OpenForms["FrmCollaborateAPITestMain"] != null)
                        {
                            (System.Windows.Forms.Application.OpenForms["FrmCollaborateAPITestMain"] as FrmCollaborateAPITestMain).LogResponse(
                                ref (System.Windows.Forms.Application.OpenForms["FrmCollaborateAPITestMain"] as FrmCollaborateAPITestMain)._logTxt, 
                                response, "Request Failed with status " + ((HttpWebResponse)webEx.Response).StatusCode);
                        }
                    }
                }
            }
            return null;
        }       

        #endregion
    }
}
