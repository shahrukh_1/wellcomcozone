﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GMGCollaborateAPITestApp.Models;

namespace GMGCollaborateAPITestApp
{
    public partial class TabContent : UserControl
    {

        public BindingList<WorkflowCollaborator> _phaseCollaborators = new BindingList<WorkflowCollaborator>();

        public TabContent()
        {
            InitializeComponent();
            
            //Init the datagrid source
            dataGridAccess.DataSource = _phaseCollaborators;

            //Init deadline fields
            workflowDeadLineTime.Format = DateTimePickerFormat.Custom;
            
            workflowDeadLineTime.CustomFormat = "hh:mm";
            workflowDeadLineTime.ShowUpDown = true;
        }

        private void TabContent_Load(object sender, EventArgs e)
        {

        }

        private void rdoDeadLineFixedDate_CheckedChanged(object sender, EventArgs e)
        {
            workflowDeadLineDate.Enabled = true;
            workflowDeadLineTime.Enabled = true;
            workflowDeadLineMeridian.Enabled = true;
            automaticDeadlineDaysHoursDropDown.Enabled = false;
            automaticDeadlineDaysHoursNumber.Enabled = false;
            automaticDeadLineType.Enabled = false;
        }

        private void rdoDeadLineNoDeadline_CheckedChanged(object sender, EventArgs e)
        {
            automaticDeadlineDaysHoursDropDown.Enabled = false;
            automaticDeadlineDaysHoursNumber.Enabled = false;
            workflowDeadLineDate.Enabled = false;
            workflowDeadLineTime.Enabled = false;
            workflowDeadLineMeridian.Enabled = false;
            automaticDeadLineType.Enabled = false;
        }

        private void rdoDeadLineAutomatic_CheckedChanged(object sender, EventArgs e)
        {
            automaticDeadLineType.Enabled = true;
            automaticDeadlineDaysHoursDropDown.Enabled = true;
            automaticDeadlineDaysHoursNumber.Enabled = true;
            workflowDeadLineDate.Enabled = false;
            workflowDeadLineTime.Enabled = false;
            workflowDeadLineMeridian.Enabled = false;
        }

        private void decisionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (decisionType.SelectedItem == "Approval Group")
            {
                pdm.Clear();
                pdm.Enabled = false;
                chkApprovalShouldBeViewedByAllCollaborators.Enabled = false;
            }
            else
            {
                pdm.Enabled = true;
                chkApprovalShouldBeViewedByAllCollaborators.Enabled = true;
            }
        }
    }
}
