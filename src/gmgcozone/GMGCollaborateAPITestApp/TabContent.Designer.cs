﻿namespace GMGCollaborateAPITestApp
{
    partial class TabContent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pdm = new System.Windows.Forms.TextBox();
            this.approvalTrigger = new System.Windows.Forms.ComboBox();
            this.decisionType = new System.Windows.Forms.ComboBox();
            this.chkApprovalShouldBeViewedByAllCollaborators = new System.Windows.Forms.CheckBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.dataGridAccess = new System.Windows.Forms.DataGridView();
            this.automaticDeadlineDaysHoursNumber = new System.Windows.Forms.NumericUpDown();
            this.automaticDeadlineDaysHoursDropDown = new System.Windows.Forms.ComboBox();
            this.automaticDeadLineType = new System.Windows.Forms.ComboBox();
            this.workflowDeadLineDate = new System.Windows.Forms.DateTimePicker();
            this.rdoDeadLineAutomatic = new System.Windows.Forms.RadioButton();
            this.rdoDeadLineFixedDate = new System.Windows.Forms.RadioButton();
            this.rdoDeadLineNoDeadline = new System.Windows.Forms.RadioButton();
            this.label44 = new System.Windows.Forms.Label();
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases = new System.Windows.Forms.CheckBox();
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases = new System.Windows.Forms.CheckBox();
            this.phaseName = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.workflowDeadLineTime = new System.Windows.Forms.DateTimePicker();
            this.workflowDeadLineMeridian = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.automaticDeadlineDaysHoursNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // pdm
            // 
            this.pdm.Location = new System.Drawing.Point(95, 433);
            this.pdm.Name = "pdm";
            this.pdm.Size = new System.Drawing.Size(199, 20);
            this.pdm.TabIndex = 75;
            // 
            // approvalTrigger
            // 
            this.approvalTrigger.FormattingEnabled = true;
            this.approvalTrigger.Items.AddRange(new object[] {
            "Approved",
            "Approved With Changes"});
            this.approvalTrigger.Location = new System.Drawing.Point(95, 462);
            this.approvalTrigger.Name = "approvalTrigger";
            this.approvalTrigger.Size = new System.Drawing.Size(199, 21);
            this.approvalTrigger.TabIndex = 74;
            // 
            // decisionType
            // 
            this.decisionType.FormattingEnabled = true;
            this.decisionType.Items.AddRange(new object[] {
            "Primary Decision Maker",
            "Approval Group"});
            this.decisionType.Location = new System.Drawing.Point(95, 406);
            this.decisionType.Name = "decisionType";
            this.decisionType.Size = new System.Drawing.Size(199, 21);
            this.decisionType.TabIndex = 73;
            this.decisionType.SelectedIndexChanged += new System.EventHandler(this.decisionType_SelectedIndexChanged);
            // 
            // chkApprovalShouldBeViewedByAllCollaborators
            // 
            this.chkApprovalShouldBeViewedByAllCollaborators.AutoSize = true;
            this.chkApprovalShouldBeViewedByAllCollaborators.Enabled = false;
            this.chkApprovalShouldBeViewedByAllCollaborators.Location = new System.Drawing.Point(94, 489);
            this.chkApprovalShouldBeViewedByAllCollaborators.Name = "chkApprovalShouldBeViewedByAllCollaborators";
            this.chkApprovalShouldBeViewedByAllCollaborators.Size = new System.Drawing.Size(244, 17);
            this.chkApprovalShouldBeViewedByAllCollaborators.TabIndex = 72;
            this.chkApprovalShouldBeViewedByAllCollaborators.Text = "Approval should be viewed by all collaborators";
            this.chkApprovalShouldBeViewedByAllCollaborators.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 465);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(85, 13);
            this.label48.TabIndex = 71;
            this.label48.Text = "Approval Trigger";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(16, 436);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(81, 13);
            this.label47.TabIndex = 70;
            this.label47.Text = "Decision Maker";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(22, 409);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(75, 13);
            this.label46.TabIndex = 69;
            this.label46.Text = "Decision Type";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(46, 230);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(42, 13);
            this.label45.TabIndex = 68;
            this.label45.Text = "Access";
            // 
            // dataGridAccess
            // 
            this.dataGridAccess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAccess.Location = new System.Drawing.Point(11, 246);
            this.dataGridAccess.Name = "dataGridAccess";
            this.dataGridAccess.Size = new System.Drawing.Size(402, 150);
            this.dataGridAccess.TabIndex = 59;
            // 
            // automaticDeadlineDaysHoursNumber
            // 
            this.automaticDeadlineDaysHoursNumber.Enabled = false;
            this.automaticDeadlineDaysHoursNumber.Location = new System.Drawing.Point(94, 202);
            this.automaticDeadlineDaysHoursNumber.Name = "automaticDeadlineDaysHoursNumber";
            this.automaticDeadlineDaysHoursNumber.Size = new System.Drawing.Size(31, 20);
            this.automaticDeadlineDaysHoursNumber.TabIndex = 67;
            // 
            // automaticDeadlineDaysHoursDropDown
            // 
            this.automaticDeadlineDaysHoursDropDown.Enabled = false;
            this.automaticDeadlineDaysHoursDropDown.FormattingEnabled = true;
            this.automaticDeadlineDaysHoursDropDown.Items.AddRange(new object[] {
            "Hours",
            "Days"});
            this.automaticDeadlineDaysHoursDropDown.Location = new System.Drawing.Point(131, 202);
            this.automaticDeadlineDaysHoursDropDown.Name = "automaticDeadlineDaysHoursDropDown";
            this.automaticDeadlineDaysHoursDropDown.Size = new System.Drawing.Size(60, 21);
            this.automaticDeadlineDaysHoursDropDown.TabIndex = 66;
            // 
            // automaticDeadLineType
            // 
            this.automaticDeadLineType.Enabled = false;
            this.automaticDeadLineType.FormattingEnabled = true;
            this.automaticDeadLineType.Items.AddRange(new object[] {
            "After Upload",
            "Last Phase Completed",
            "Approval Deadline"});
            this.automaticDeadLineType.Location = new System.Drawing.Point(197, 202);
            this.automaticDeadLineType.Name = "automaticDeadLineType";
            this.automaticDeadLineType.Size = new System.Drawing.Size(97, 21);
            this.automaticDeadLineType.TabIndex = 65;
            // 
            // workflowDeadLineDate
            // 
            this.workflowDeadLineDate.Enabled = false;
            this.workflowDeadLineDate.Location = new System.Drawing.Point(94, 153);
            this.workflowDeadLineDate.Name = "workflowDeadLineDate";
            this.workflowDeadLineDate.Size = new System.Drawing.Size(97, 20);
            this.workflowDeadLineDate.TabIndex = 61;
            // 
            // rdoDeadLineAutomatic
            // 
            this.rdoDeadLineAutomatic.AutoSize = true;
            this.rdoDeadLineAutomatic.Location = new System.Drawing.Point(94, 179);
            this.rdoDeadLineAutomatic.Name = "rdoDeadLineAutomatic";
            this.rdoDeadLineAutomatic.Size = new System.Drawing.Size(147, 17);
            this.rdoDeadLineAutomatic.TabIndex = 64;
            this.rdoDeadLineAutomatic.Text = "Automatic phase deadline";
            this.rdoDeadLineAutomatic.UseVisualStyleBackColor = true;
            this.rdoDeadLineAutomatic.CheckedChanged += new System.EventHandler(this.rdoDeadLineAutomatic_CheckedChanged);
            // 
            // rdoDeadLineFixedDate
            // 
            this.rdoDeadLineFixedDate.AutoSize = true;
            this.rdoDeadLineFixedDate.Location = new System.Drawing.Point(94, 130);
            this.rdoDeadLineFixedDate.Name = "rdoDeadLineFixedDate";
            this.rdoDeadLineFixedDate.Size = new System.Drawing.Size(62, 17);
            this.rdoDeadLineFixedDate.TabIndex = 63;
            this.rdoDeadLineFixedDate.Text = "Fix date";
            this.rdoDeadLineFixedDate.UseVisualStyleBackColor = true;
            this.rdoDeadLineFixedDate.CheckedChanged += new System.EventHandler(this.rdoDeadLineFixedDate_CheckedChanged);
            // 
            // rdoDeadLineNoDeadline
            // 
            this.rdoDeadLineNoDeadline.AutoSize = true;
            this.rdoDeadLineNoDeadline.Checked = true;
            this.rdoDeadLineNoDeadline.Location = new System.Drawing.Point(94, 105);
            this.rdoDeadLineNoDeadline.Name = "rdoDeadLineNoDeadline";
            this.rdoDeadLineNoDeadline.Size = new System.Drawing.Size(84, 17);
            this.rdoDeadLineNoDeadline.TabIndex = 62;
            this.rdoDeadLineNoDeadline.TabStop = true;
            this.rdoDeadLineNoDeadline.Text = "No Deadline";
            this.rdoDeadLineNoDeadline.UseVisualStyleBackColor = true;
            this.rdoDeadLineNoDeadline.CheckedChanged += new System.EventHandler(this.rdoDeadLineNoDeadline_CheckedChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(39, 106);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(49, 13);
            this.label44.TabIndex = 60;
            this.label44.Text = "Deadline";
            // 
            // chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases
            // 
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.AutoSize = true;
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.Location = new System.Drawing.Point(94, 70);
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.Name = "chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases";
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.Size = new System.Drawing.Size(330, 17);
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.TabIndex = 58;
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.Text = "Show annotations of this phase to external users of other phases";
            this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases.UseVisualStyleBackColor = true;
            // 
            // chkShowAnnotationOfThisPhaseToUsersOfOtherPhases
            // 
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.AutoSize = true;
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.Location = new System.Drawing.Point(94, 47);
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.Name = "chkShowAnnotationOfThisPhaseToUsersOfOtherPhases";
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.Size = new System.Drawing.Size(290, 17);
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.TabIndex = 57;
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.Text = "Show annotations of this phase to users of other phases";
            this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases.UseVisualStyleBackColor = true;
            // 
            // phaseName
            // 
            this.phaseName.Location = new System.Drawing.Point(95, 16);
            this.phaseName.Name = "phaseName";
            this.phaseName.Size = new System.Drawing.Size(219, 20);
            this.phaseName.TabIndex = 56;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(53, 19);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(35, 13);
            this.label43.TabIndex = 55;
            this.label43.Text = "Name";
            // 
            // workflowDeadLineTime
            // 
            this.workflowDeadLineTime.Enabled = false;
            this.workflowDeadLineTime.Location = new System.Drawing.Point(197, 153);
            this.workflowDeadLineTime.Name = "workflowDeadLineTime";
            this.workflowDeadLineTime.Size = new System.Drawing.Size(97, 20);
            this.workflowDeadLineTime.TabIndex = 76;
            // 
            // workflowDeadLineMeridian
            // 
            this.workflowDeadLineMeridian.Enabled = false;
            this.workflowDeadLineMeridian.FormattingEnabled = true;
            this.workflowDeadLineMeridian.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.workflowDeadLineMeridian.Location = new System.Drawing.Point(300, 153);
            this.workflowDeadLineMeridian.Name = "workflowDeadLineMeridian";
            this.workflowDeadLineMeridian.Size = new System.Drawing.Size(56, 21);
            this.workflowDeadLineMeridian.TabIndex = 77;
            // 
            // TabContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.workflowDeadLineMeridian);
            this.Controls.Add(this.workflowDeadLineTime);
            this.Controls.Add(this.pdm);
            this.Controls.Add(this.approvalTrigger);
            this.Controls.Add(this.decisionType);
            this.Controls.Add(this.chkApprovalShouldBeViewedByAllCollaborators);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.dataGridAccess);
            this.Controls.Add(this.automaticDeadlineDaysHoursNumber);
            this.Controls.Add(this.automaticDeadlineDaysHoursDropDown);
            this.Controls.Add(this.automaticDeadLineType);
            this.Controls.Add(this.workflowDeadLineDate);
            this.Controls.Add(this.rdoDeadLineAutomatic);
            this.Controls.Add(this.rdoDeadLineFixedDate);
            this.Controls.Add(this.rdoDeadLineNoDeadline);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases);
            this.Controls.Add(this.chkShowAnnotationOfThisPhaseToUsersOfOtherPhases);
            this.Controls.Add(this.phaseName);
            this.Controls.Add(this.label43);
            this.Name = "TabContent";
            this.Size = new System.Drawing.Size(429, 518);
            this.Load += new System.EventHandler(this.TabContent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.automaticDeadlineDaysHoursNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox pdm;
        private System.Windows.Forms.ComboBox approvalTrigger;
        private System.Windows.Forms.ComboBox decisionType;
        private System.Windows.Forms.CheckBox chkApprovalShouldBeViewedByAllCollaborators;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DataGridView dataGridAccess;
        private System.Windows.Forms.NumericUpDown automaticDeadlineDaysHoursNumber;
        private System.Windows.Forms.ComboBox automaticDeadlineDaysHoursDropDown;
        private System.Windows.Forms.ComboBox automaticDeadLineType;
        private System.Windows.Forms.DateTimePicker workflowDeadLineDate;
        private System.Windows.Forms.RadioButton rdoDeadLineAutomatic;
        private System.Windows.Forms.RadioButton rdoDeadLineFixedDate;
        private System.Windows.Forms.RadioButton rdoDeadLineNoDeadline;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.CheckBox chkShowAnnotationsOfThisPhaseToExternalUsersOfOtherPhases;
        private System.Windows.Forms.CheckBox chkShowAnnotationOfThisPhaseToUsersOfOtherPhases;
        private System.Windows.Forms.TextBox phaseName;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.DateTimePicker workflowDeadLineTime;
        private System.Windows.Forms.ComboBox workflowDeadLineMeridian;

    }
}
