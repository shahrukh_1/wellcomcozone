﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGCollaborateAPITestApp.Models
{
    public class UpdateApprovalRequest : CollaborateAPIIdentityModel
    {
        #region Properties
        
        public string deadline { get; set; }
        public List<Collaborator> collaborators { get; set; }

        #endregion

        #region Constructors

        public UpdateApprovalRequest()
        {
            collaborators = new List<Collaborator>();
        }

        #endregion
    }
}
