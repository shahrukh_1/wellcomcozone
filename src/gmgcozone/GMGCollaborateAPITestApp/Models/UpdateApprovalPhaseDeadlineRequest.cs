﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GMGCollaborateAPITestApp.Models
{
    public class UpdateApprovalPhaseDeadlineRequest : CollaborateAPIIdentityModel
    {
        #region Properties
        public string deadline { get; set; }
        public string approvalPhaseGuid { get; set; }
        #endregion

    }
}
