﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class GetApprovalWithAccessResponse : GetApprovalResponse
    {
        #region Properties

        public List<Collaborator> collaborators { get; set; }
 
        #endregion
    }
}
