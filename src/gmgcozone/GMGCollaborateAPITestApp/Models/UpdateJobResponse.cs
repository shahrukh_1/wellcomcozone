﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class UpdateJobResponse
    {
        #region Properties

        public string jobGuid { get; set; }

        #endregion
    }
}
