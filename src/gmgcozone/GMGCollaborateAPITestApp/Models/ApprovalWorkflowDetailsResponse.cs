﻿using System.Collections.Generic;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class ApprovalWorkflowDetailsResponse
    {
        #region Properties

        public List<ApprovalWorkflowPhase> phases { get; set; }

        #endregion
    }
}
