﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class ResponseErrors
    {
        public List<RuleViolation> errors { get; set; } 
    }
}
