﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    [DataContract]
    public class CreateApprovalResponse
    {
        [DataMember]
        public string guid { get; set; }

        [DataMember]
        public int version { get; set; }
    }
}
