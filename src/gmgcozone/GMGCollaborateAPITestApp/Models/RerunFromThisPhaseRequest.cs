﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class RerunFromThisPhaseRequest : CollaborateAPIIdentityModel
    {
        public string phaseGuid { get; set; }
    }
}
