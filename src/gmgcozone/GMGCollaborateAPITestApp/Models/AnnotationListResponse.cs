﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class AnnotationListResponse
    {
        #region Properties

        public string guid { get; set; }
        public List<AnnotationObjectModel> annotations { get; set; } 

        #endregion

        #region Constructors

        public AnnotationListResponse()
        {
            annotations = new List<AnnotationObjectModel>();
        }

        #endregion
    }

    public class AnnotationObjectModel
    {
        #region Properties

        public int pageNumber { get; set; }
        public int annotationNumber { get; set; }
        public string annotationDate { get; set; }
        public string user { get; set; }
        public string comment { get; set; }

        #endregion
    }
}
