﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GMGCollaborateAPITestApp.Models
{
    public class CreateUserRequest : CollaborateAPIBaseModel
    {
        [DataMember(Name = "firstname")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastname")]
        public string LastName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        public List<ModulePermission> Permissions { get; set; }
        public List<UserGroup> Groups { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "isssouser")]
        public bool IsSSOUser { get; set; }

        [DataMember(Name = "privateannotations")]
        public bool PrivateAnnotations { get; set; }
    }
}
