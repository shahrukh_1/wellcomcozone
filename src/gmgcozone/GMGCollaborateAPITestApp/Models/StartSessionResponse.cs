﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class StartSessionResponse
    {
        #region Properties

        public string sessionKey { get; set; }
        public string accountName { get; set; }
        public string accountKey { get; set; }
        public string userKey { get; set; }

        #endregion
    }
}
