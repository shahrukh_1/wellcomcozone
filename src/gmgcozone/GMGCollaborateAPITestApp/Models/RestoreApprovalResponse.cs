﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class RestoreApprovalResponse
    {
        #region Properties

        public string guid { get; set; }

        #endregion
    }
}
