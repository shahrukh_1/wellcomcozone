﻿

using System;

namespace GMGCollaborateAPITestApp.Models
{
    [Serializable]
    public class CreateApprovalRequest : ApprovalBaseModel
    {
        #region Properties
     
        public string fileType { get; set; }
     
        public string fileContent { get; set; }

        public Settings settings { get; set; }

        #endregion

        #region Constructors

         public CreateApprovalRequest()
         {
             settings = new Settings();
         }

        #endregion
    }
}
