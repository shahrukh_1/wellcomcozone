﻿using System.Collections.Generic;

namespace GMGCollaborateAPITestApp.Models
{
    public class UpdateApprovalDecisionResponse
    {
        #region Properties

        public string guid { get; set; }

        public List<string> warningMessages { get; set; } 

        #endregion

        #region Constructors

        public UpdateApprovalDecisionResponse()
        {
            warningMessages =  new List<string>();
        }
        #endregion
    }
}
