﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class GetJobResponse : JobDetails
    {
        #region Properties

        public List<ApprovalDetails> approvals { get; set; }

        #endregion


        #region Constructor

        public GetJobResponse()
        {
            approvals = new List<ApprovalDetails>();
        }

        #endregion
    }
}
