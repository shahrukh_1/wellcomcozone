﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class ChecklistDetailsResponse
    {
        #region Properties

        public List<Checklist> Checklist { get; set; }

        #endregion
    }
}
