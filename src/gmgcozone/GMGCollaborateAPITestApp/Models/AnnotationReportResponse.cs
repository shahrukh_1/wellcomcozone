﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class AnnotationReportResponse
    {
        #region Properties

        public string guid { get; set; }

        public string imageReport { get; set; }

        #endregion
    }
}
