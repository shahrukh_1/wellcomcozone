﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class GetProofStudioURLResponse
    {
        #region Properties

        public string proofstudioURL { get; set; }

        #endregion
    }
}
