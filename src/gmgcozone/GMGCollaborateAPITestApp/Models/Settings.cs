﻿using System;

namespace GMGCollaborateAPITestApp.Models
{
    [Serializable]
    public class Settings
    {
        #region Private properties

        //private bool _allowDownload = true;

        #endregion

        #region Public properties

        public bool lockProofWhenFirstDecisionIsMade { get; set; }
        public bool lockProofWhenAllDecisionsAreMade { get; set; }
        public bool onlyOneDecisionRequired { get; set; }
        public bool allowUsersToDownloadOriginalFile { get; set; }
        

        #endregion
    }
}
