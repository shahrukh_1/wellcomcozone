﻿namespace GMGCollaborateAPITestApp.Models
{
    public class FolderResponse : BaseFolderResponse
    {
        #region Properties

        public string name { get; set; }

        #endregion
    }
}
