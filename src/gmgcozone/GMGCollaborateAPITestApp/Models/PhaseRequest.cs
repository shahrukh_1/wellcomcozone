﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class PhaseRequest
    {
        public string phaseName { get; set; }
        public bool showAnnotationsToUsersOfOtherPhases { get; set; }
        public bool showAnnotationsOfOtherPhasesToExternalUsers { get; set; }

        public string deadLineDate { get; set; }
        public string deadLineTime { get; set; }
        public string deadLineMeridian { get; set; }
        public int? automaticDeadLineUnit { get; set; }
        public int? automaticDeadLineTrigger { get; set; }
        public int? automaticDeadLineValue { get; set; }

        public List<PhaseCollaboratorsRequest> phaseCollaborators { get; set; }

        public string selectedDecisionType { get; set; }
        public string decisionMaker { get; set; }
        public string approvalTrigger { get; set; }
        public bool approvalShouldBeViewedByAllCollaborators { get; set; }

        public PhaseRequest()
        {
            phaseCollaborators = new List<PhaseCollaboratorsRequest>();
        }
    }
}
