﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class GetApprovalResponse
    {
        #region Properties

        public string guid { get; set; }
        public int version { get; set; }
        public string filename { get; set; }
        public string thumbnail { get; set; }
        public string creatorName { get; set; }
        public string uploadDate { get; set; }
        public string deadline { get; set; }
        public string fileSize { get; set; }
        public int pages { get; set; }
        public int annotationsNo { get; set; }
        public bool isLocked { get; set; }
        public bool hasProcessingErrors { get; set; }

        #endregion
    }
}
