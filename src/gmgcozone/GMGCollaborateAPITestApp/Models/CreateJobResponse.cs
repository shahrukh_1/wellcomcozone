﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GMGCollaborateAPITestApp.Models
{
    public class CreateJobResponse
    {
        #region Properties

        public string guid { get; set; }
        public CreateApprovalResponse approval { get; set; }
        public List<string> warningMessages { get; set; } 

        #endregion


        #region Constructors

        public CreateJobResponse()
        {
            warningMessages =  new List<string>();
        }

        #endregion
    }
}
