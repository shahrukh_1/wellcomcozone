﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class CreateApprovalWorkflowRequest : CollaborateAPIBaseModel
    {
        public string workflowName { get; set; }
        public bool rerunWorkflow { get; set; }
        public List<PhaseRequest> worflowPhases { get; set; }

        public CreateApprovalWorkflowRequest()
        {
            worflowPhases = new List<PhaseRequest>();
        }
    }
}
