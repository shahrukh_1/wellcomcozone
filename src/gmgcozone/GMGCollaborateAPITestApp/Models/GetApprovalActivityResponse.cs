﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class GetApprovalActivityResponse
    {
        #region Properties

        public string guid { get; set; }
        public int version { get; set; }
        public string fileName { get; set; }       
        public List<CollaboratorActivity> collaborators { get; set; }
        public List<PhaseActivity> phasesActivity { get; set; }

        #endregion

        #region Constructors

        public GetApprovalActivityResponse()
        {
            phasesActivity = new List<PhaseActivity>();
            collaborators = new List<CollaboratorActivity>();
        }

        #endregion
    }


    public class PhaseActivity
    {
        #region Properties

        public string name { get; set; }
        public string guid { get; set; }
        public string status { get; set; }
        public string deadline { get; set; }

        public List<CollaboratorActivity> collaborators { get; set; }

        #endregion

        #region Constructors

        public PhaseActivity()
        {
            collaborators = new List<CollaboratorActivity>();
        }

        #endregion
    }
}
