﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class Checklist
    {
        #region Properties

        public string guid { get; set; }

        public string name { get; set; }

        #endregion
    }
}
