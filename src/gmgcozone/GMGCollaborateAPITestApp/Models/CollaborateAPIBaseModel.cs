﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace GMGCollaborateAPITestApp.Models
{
    [Serializable]
    public class CollaborateAPIBaseModel
    {
        #region Properties
     
        public string sessionKey { get; set; }

        public string username { get; set; }

        #endregion
    }

    [Serializable]
    public class CollaborateAPIIdentityModel : CollaborateAPIBaseModel
    {
        #region Properties

        public string guid { get; set; }

        #endregion
    }
}