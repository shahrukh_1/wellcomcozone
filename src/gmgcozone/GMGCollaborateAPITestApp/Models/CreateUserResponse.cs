﻿using System.Runtime.Serialization;

namespace GMGCollaborateAPITestApp.Models
{
    public class CreateUserResponse
    {
        [DataMember(Name = "guid")]
        public string Guid { get; set; }
    }
}
