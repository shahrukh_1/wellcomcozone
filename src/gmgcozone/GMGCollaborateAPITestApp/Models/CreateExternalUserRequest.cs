﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GMGCollaborateAPITestApp.Models
{
    public class CreateExternalUserRequest : CollaborateAPIBaseModel
    {
        [DataMember(Name = "firstname")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastname")]
        public string LastName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
}