﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GMGCollaborateAPITestApp.Models
{
    [Serializable]
    public class ApprovalBaseModel : CollaborateAPIIdentityModel
    {
        #region Properties

        public string fileName { get; set; }
        public string deadline { get; set; }
        public string folder { get; set; }
        public List<Collaborator> collaborators { get; set; }
        public List<Phase> phases { get; set; }

        #endregion

        #region Constructors

        public ApprovalBaseModel()
        {
            collaborators = new List<Collaborator>();
            phases = new List<Phase>();
        }

        #endregion
    }

    [DataContract]
    public class Phase
    {
        #region Properties

        [DataMember]
        public string phaseGUID;

        [DataMember]
        public List<Collaborator> collaborators { get; set; }

        #endregion

        #region Constructors

        public Phase()
        {
            collaborators = new List<Collaborator>();
        }

        #endregion
    }
}
