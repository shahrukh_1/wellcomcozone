﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class ApprovalDetails
    {
        #region Properties

        public string guid { get; set; }
        public int version { get; set; }
        public string fileName { get; set; }
        public string thumbnail { get; set; }
        public Guid creatorGuid { get; set; }
        public DateTime uploadDate { get; set; }
        public DateTime? deadline { get; set; }
        public string fileSize { get; set; }
        public int pages { get; set; }
        public int annotationsNo { get; set; }
        public bool locked { get; set; }
        public string errorMessage { get; set; }

        #endregion
    }
}
