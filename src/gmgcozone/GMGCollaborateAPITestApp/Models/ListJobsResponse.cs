﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class ListJobsResponse
    {
        #region Properties

        public int jobsCount { get; set; }

        public List<JobDetails> jobs { get; set; } 

        #endregion

        #region Constructors

        public ListJobsResponse()
        {
            jobs =  new List<JobDetails>();    
        }

        #endregion
    }
}
