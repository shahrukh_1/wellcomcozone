﻿using GMGCollaborateAPITestApp.Models;

namespace GMG.CollaborateAPI.BL.Models.FolderModels
{
    public class CreateFolderRequest: CollaborateAPIBaseModel
    {
        #region Parameters

        public string parentGuid { get; set; }

        public string name { get; set; }

        #endregion
    }
}
