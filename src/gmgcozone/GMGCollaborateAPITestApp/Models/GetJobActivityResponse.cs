﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class GetJobActivityResponse
    {
        #region

        public string guid { get; set; }
        public string jobStatus { get; set; }
        public List<GetApprovalActivityResponse> approvals { get; set; }
 
        #endregion
    }
}
