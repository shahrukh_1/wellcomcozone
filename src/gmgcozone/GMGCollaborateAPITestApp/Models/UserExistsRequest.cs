﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class UserExistsRequest : CollaborateAPIBaseModel
    {
        #region Properties

        public string email { get; set; }

        #endregion
    }
}
