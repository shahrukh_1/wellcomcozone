﻿using System.Collections.Generic;

namespace GMGCollaborateAPITestApp.Models
{
    public class AddCollaboratorsToApprovalPhaseRequest : CollaborateAPIIdentityModel
    {
        public string phaseGuid { get; set; }
        public List<PhaseCollaboratorsRequest> phaseCollaborators { get; set; }
    }
}
