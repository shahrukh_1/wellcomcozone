﻿using System.Collections.Generic;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Model
{
    public class ListApprovalWorkflowsResponse
    {
        #region Properties

        public List<ApprovalWorkflow> workflows { get; set; }

        #endregion
    }
}
