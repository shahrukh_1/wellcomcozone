﻿using System;
using System.Runtime.Serialization;


namespace GMGCollaborateAPITestApp.Models
{
    [DataContract]
    public class Collaborator
    {
        #region Properties
        [DataMember]
        public string workflowPhaseGUID { get; set; }
         [DataMember]
        public string lastname { get; set; }
         [DataMember]
        public string firstname { get; set; }
         [DataMember]
        public string username { get; set; }
         [DataMember]
        public string email { get; set; }
         [DataMember]
        public string approvalRole { get; set; }
         [DataMember]
        public bool pdm { get; set; }
         [DataMember]
        public bool isExternal { get; set; }
         [DataMember]
         public bool toBeRemoved { get; set; }

        #endregion
    }

    [DataContract]
    public class WorkflowCollaborator
    {
        #region Properties
        [DataMember]
        public string lastname { get; set; }
        [DataMember]
        public string firstname { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string approvalRole { get; set; }
        [DataMember]
        public bool isExternal { get; set; }

        #endregion
    }

    public class CollaboratorActivity
    {
        #region Properties

        public string username { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string role { get; set; }       
        public string sent { get; set; }
        public string opened { get; set; }
        public int annotations { get; set; }
        public int replies { get; set; }
        public string decision { get; set; }
        public bool isExternal { get; set; }
        public bool isPDM { get; set; }

        #endregion
    }
}
