﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class JobStatusDetails
    {
        #region Properties

        public string jobGuid { get; set; }
        public string jobStatus { get; set; }
        public string jobStatusKey { get; set; }
        public string approvalGuid { get; set; }
        public string approvalStatus { get; set; }
        public string approvalStatusKey { get; set; }
        public string workflowName { get; set; }
        public string phaseName { get; set; }
        public string phaseGuid { get; set; }
        public string workflowGuid { get; set; }
        public string username { get; set; }
        public string email { get; set; }

        #endregion
    }
}
