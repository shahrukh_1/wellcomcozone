﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class UpdateJobRequest
    {
        #region Properties

        public string jobStatus { get; set; }
        public string jobTitle { get; set; }

        #endregion
    }
}
