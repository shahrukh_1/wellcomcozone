﻿namespace GMGCollaborateAPITestApp.Models
{
    public class UpdateApprovalDecisionRequest : CollaborateAPIIdentityModel
    {
        #region Properties
        
        public string decision { get; set; }

        public string email { get; set; }

        #endregion
    }
}
