﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class PhaseCollaboratorsRequest
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string userRole { get; set; }
        public bool isExternal { get; set; }
        public string userName { get; set; }
    }
}
