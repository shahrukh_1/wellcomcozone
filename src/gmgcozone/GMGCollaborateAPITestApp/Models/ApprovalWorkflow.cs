﻿namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Model
{
    public class ApprovalWorkflow
    {
        #region Properties

        public string guid { get; set; }

        public string name { get; set; }

        #endregion
    }
}
