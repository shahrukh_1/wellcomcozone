﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class CreateApprovalWorkflowResponse
    {
        public string workflowGuid { get; set; }
    }
}
