﻿namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class ApprovalWorkflowPhase
    {
        #region Properties

        public string id { get; set; }

        public string name { get; set; }
        public string fixedDeadlineDatetime { get; set; }

        public int? automaticDeadlineValue { get; set; }

        public string automaticDeadlineUnit { get; set; }

        public string automaticDeadlineType { get; set; }

        #endregion
    }
}
