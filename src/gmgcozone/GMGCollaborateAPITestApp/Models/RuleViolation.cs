﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMGCollaborateAPITestApp.Models
{
    public class RuleViolation
    {
        public string ErrorMessage { get; set; }
        public string PropertyName { get; set; }

        public RuleViolation(string errorMessage, string propertyName)
        {
            ErrorMessage = errorMessage;
            PropertyName = propertyName;
        }

        public RuleViolation()
        {
            
        }
    }
}
