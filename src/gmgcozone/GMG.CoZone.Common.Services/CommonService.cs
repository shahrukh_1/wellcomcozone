﻿using GMG.CoZone.Common.Interfaces;
using GMGColorDAL;
using System.Linq;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.Common.Services
{
    public class CommonService : ICommonService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CommonService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Account GetAccountById(int accountId)
        {
            return _unitOfWork.AccountRepository.GetByID(accountId);
        }

        public User GetUserByEmail(string email, int accountId)
        {
            return _unitOfWork.UserRepository.Get(filter: us => us.EmailAddress == email && 
                                                          us.UserStatu.Key == "A" && 
                                                          us.Account1.ID == accountId).FirstOrDefault();
        }
    }
}
