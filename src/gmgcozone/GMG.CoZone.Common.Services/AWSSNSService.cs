﻿using System;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.AWS.Interfaces;
using GMG.CoZone.Common.DomainModels;
using Newtonsoft.Json;
using System.Web;
using System.IO;

namespace GMG.CoZone.Common.Services
{
    public class AWSSNSService : IAWSSNSService
    {
        private IAWSSimpleNotificationService _snsClient;
        private IGMGConfiguration _gmgConfiguration;

        public AWSSNSService(IAWSSimpleNotificationService snsClient, IGMGConfiguration gmgConfiguration)
        {
            _snsClient = snsClient;
            _gmgConfiguration = gmgConfiguration;
        }

        public void PublishStatus(string messageObj)
        {
            try
            {
                var topicArn = _snsClient.GetTopicArn(_gmgConfiguration.AWSSNSMediaDefineResponseTopic);
                if (_snsClient.CheckTopicExists(topicArn))
                {
                    _snsClient.WriteMessage(topicArn, messageObj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MediaDefineApprovalProgressStatusModel ProcessPost(HttpContext context)
        {
            try
            {
                var contentBoby = string.Empty;

                using (var reader = new StreamReader(context.Request.InputStream))
                {
                    contentBoby = reader.ReadToEnd();
                }
                var approvalStatus = _snsClient.ProcessMessage(context, contentBoby);

                if (!string.IsNullOrEmpty(approvalStatus))
                {
                    return JsonConvert.DeserializeObject<MediaDefineApprovalProgressStatusModel>(approvalStatus);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateSubscription(string domain)
        {
            try
            {
                var topicArn = _snsClient.GetTopicArn(_gmgConfiguration.AWSSNSMediaDefineResponseTopic);
                _snsClient.CreateSubscriber(topicArn,
                                            _gmgConfiguration.ServerProtocol,
                                            domain);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
