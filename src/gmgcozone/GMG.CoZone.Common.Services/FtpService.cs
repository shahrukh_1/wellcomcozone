﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Repositories.Interfaces;
using GMGColorDAL;
using GMGColorDAL.Common;

namespace GMG.CoZone.Common.Services
{
    public class FtpService: IFtpService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;

        public FtpService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public List<int> AddFtpPresetJobDownloads(List<FtpPresetJobDownloadModel> ftpPresetJobDownloadModel)
        {
           var ftpPresetJobDownloads = _mapper.Map<IList<FtpPresetJobDownloadModel>, IList<FTPPresetJobDownload>>(ftpPresetJobDownloadModel);

            _unitOfWork.FtpPresetJobDownloadRepository.AddRange(ftpPresetJobDownloads);
            _unitOfWork.Save();

            return ftpPresetJobDownloads.Select(s => s.ID).ToList();
        }

        public void AddMessageToQueue(List<int> tempObjIdLists, string groupKey, AppModule fromAppModule)
        {
            Dictionary<string, string> dict = groupKey != null ? GetDictionaryForFtpQueue(groupKey, fromAppModule, FTPAction.UploadAction)
                                                           : GetDictionaryForFtpQueue(tempObjIdLists.First(), fromAppModule, FTPAction.UploadAction);

            GMGColorCommon.CreateFileProcessMessage(dict, GMGColorCommon.MessageType.FTPJob);
        }

        public Dictionary<string, string> GetDictionaryForFtpQueue(string groupKey, AppModule applicationModule, FTPAction ftpAction)
        {
            var dict = new CustomDictionary<string, string>
            {
                {Constants.FTPGroupKey, groupKey},
                {Constants.JobMessageType, applicationModule.ToString()},
                {Constants.FTPAction, ftpAction.ToString()}
            };

            return dict;
        }

        public Dictionary<string, string> GetDictionaryForFtpQueue(int tempObjId, AppModule applicationModule, FTPAction ftpAction)
        {
            var dict = new CustomDictionary<string, string>
            {
                {Constants.FTPPresetJobID, tempObjId.ToString()},
                {Constants.JobMessageType, applicationModule.ToString()},
                {Constants.FTPAction, ftpAction.ToString()}
            };

            return dict;
        }
    }
}
