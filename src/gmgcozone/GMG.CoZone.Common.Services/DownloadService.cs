﻿using System.Collections.Generic;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Repositories.Interfaces;
using GMGColorDAL;

namespace GMG.CoZone.Common.Services
{
    public class DownloadService : IDownloadService
    {
        private readonly IUnitOfWork _unitOfWork;
        private static Dictionary<AppModule, IModuleDownlaodService> _strategies = new Dictionary<AppModule, IModuleDownlaodService>();

        public DownloadService(IUnitOfWork unitOfWork, IDeliverDownloadService deliverDownloadService, ICollaborateDownloadService collaborateDownloadService,IFileTransferDownloadService fileTransferDownloadService)
        {
            _unitOfWork = unitOfWork;

            if (_strategies.Count == 0)
            {
                _strategies.Add(AppModule.Collaborate, collaborateDownloadService);
                _strategies.Add(AppModule.Deliver, deliverDownloadService);
                _strategies.Add(AppModule.FileTransfer, fileTransferDownloadService);
                _strategies.Add(AppModule.ProjectFolders, collaborateDownloadService);
            }
        }

        public Dictionary<int, string> GetDownloadPaths(FileDownloadModel model, int loggedUserId, Role.RoleName loggedAccountRole = Role.RoleName.AccountAdministrator)
        {
            if(model.ApplicationModule==GMG.CoZone.Common.AppModule.FileTransfer) return _strategies[model.ApplicationModule].GetDownloadPaths(model, loggedUserId);
            if (model.SelectedPresetId > 0)
            {
                _strategies[model.ApplicationModule].SetFtpDownloadPaths(model, loggedUserId);
                return null;
            }

           return _strategies[model.ApplicationModule].GetDownloadPaths(model, loggedUserId);
        }
    }
}
