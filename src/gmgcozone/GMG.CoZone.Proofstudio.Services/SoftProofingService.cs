﻿using GMG.CoZone.Proofstudio.Interfaces;
using GMG.CoZone.Repositories.Interfaces;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;
using System;

namespace GMG.CoZone.Proofstudio.Services
{
    public class SoftProofingService : ISoftProofingService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SoftProofingService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ViewingCondition GetViewingConditionByJobId(int jobId)
        {
            return _unitOfWork.SoftProofingSessionParamRepository.GetViewingConditionByJobId(jobId);
        }

        public SoftProofingSessionParamsModel GetJobSoftProofingParams(int approvalId, int jobId)
        {
            var versionDetails = _unitOfWork.ApprovalRepository.GetDetailedVersion(approvalId);

            if (versionDetails.IsRGBColorSpace)
            {
                return _unitOfWork.SoftProofingSessionParamRepository.GetDefaultRGBProofingSessionParameters(approvalId);
            }

            var viewingConditions = _unitOfWork.SoftProofingSessionParamRepository.GetJobViewingCondition(jobId);

            if (viewingConditions != null && viewingConditions.SimulationProfile.HasValue)
            {
                return _unitOfWork.SoftProofingSessionParamRepository.GetExistingProofingSessionParameters(approvalId, viewingConditions);
            }

            return null;
        }

        public void AddNewSoftProofinSession(SoftProofingSessionParam sessionParams)
        {
            _unitOfWork.SoftProofingSessionParamRepository.Add(sessionParams);
            _unitOfWork.Save();
        }
    }
}
