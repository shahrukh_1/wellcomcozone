﻿using GMG.CoZone.Proofstudio.Interfaces;
using GMG.CoZone.Proofstudio.DomainModels.RepositoryOutput;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;
using GMGColorDAL.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using GMG.CoZone.Proofstudio.DomainModels.Responses;
using GMG.CoZone.Repositories.Interfaces;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Common.Interfaces;

namespace GMG.CoZone.Proofstudio.Services
{
    // TODO: analyse how to eliminate the update parts from this class in : GetCollaboratorsAndAttachColors and GetSoftProofingParamsDefaultOrCreate
    public class GetProofstudioApproval : IGetProofstudioApproval
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IColorHelper _colorHelper;
        private const string ShowAllFilesToAdminSetting = "ShowAllFilesToAdmins";
        private const string DateFormat = "yyyy-MM-dd HH:mm:ss"; //2014-04-30 13:09:40

        public GetProofstudioApproval(IUnitOfWork unitOfWork, IColorHelper colorHelper)
        {
            _unitOfWork = unitOfWork;
        }
		
		private string DateTime2String(DateTime dt, string timezone)
        {
            return GMGColorFormatData.GetUserTimeFromUTC(dt, timezone).ToString(DateFormat);
        }

        public ApprovalsBasicResponseModel GetApprovalsBasic(string approvalsIds)
        {
            var approvals = approvalsIds.Split(',').Select(int.Parse).ToList<int>();
            var basics = _unitOfWork.ApprovalRepository.GetApprovalsBasics(approvals);

            var approvalsBasicsResponse = new ApprovalsBasicResponseModel();
            approvalsBasicsResponse.approval_basic = basics;

            return approvalsBasicsResponse;
        }

        private List<ApprovalVersionModel> GetInitialVersions(ApprovalBasicModel approval, string userKey, bool isExternalUser, int accountId, bool userIsAdmin)
        {
            var versionsList = new List<ApprovalVersionModel>();

            List<InternalUseVersionModel> versions;

            // the mandatory data load for all versions
            if (isExternalUser)
            {
                versions = _unitOfWork.ApprovalRepository.GetExternalUserVersions(approval.JobId, approval.ApprovalId, approval.CurrentPhaseId, userKey);
            }
            else
            {
                bool isAdministrator = userIsAdmin &&
                    _unitOfWork.AccountSettingsRepository.IsShowAllFilesToAdminsChecked(accountId, ShowAllFilesToAdminSetting);

                versions = _unitOfWork.ApprovalRepository.GetInternalUserVersions(approval.JobId, approval.ApprovalId, approval.CurrentPhaseId, userKey, isAdministrator);
            }
            
            versionsList.AddRange(versions.Select(version => new ApprovalVersionModel()
            {
                ID = version.ID,
                Title = version.Title.Contains(".") ? version.Title.Substring(0, version.Title.LastIndexOf("."))  : version.Title,
                VersionNumber = version.VersionNumber,
                VersionLabel = ConstructVersionLabel(version.VersionLabel, version.VersionNumber),
                PageVersionLabel = version.Title.Contains(".") ?  ConstructPageVersionLabel(version.Title.Substring(0, version.Title.LastIndexOf(".")), version.VersionNumber)  : version.Title,
                VersionThumbnail = version.IsVideo ? GMGColorDAL.Approval.GetVideoImagePath(version.Guid, version.AccountRegion, version.AccountDomain) : GMGColorDAL.Approval.GetThumbnailImagePath(version.AccountDomain, version.AccountRegion, version.Guid, "1"),
            }));


            // load all data for the version that shows first time in proofstudio 
            versionsList.Add(GetVersion(approval.ApprovalId, userKey, isExternalUser));

            return versionsList;
        }

        private ApprovalVersionModel GetVersion(int versionId, string userKey, bool isExternalUser)
        {
            int? parent = _unitOfWork.ApprovalRepository.GetVersionParent(versionId);

            if (parent == 0)
                parent = null;

            var approvalVersion = new ApprovalVersionModel();
            var version = _unitOfWork.ApprovalRepository.GetDetailedVersion(versionId);

            if (version != null)
            {
                var decision = isExternalUser ? _unitOfWork.ApprovalCollaboratorDecisionRepository.GetExternalUserApprovalDecision(version.ID, userKey) :
                                                    _unitOfWork.ApprovalCollaboratorDecisionRepository.GetInternalUserApprovalDecision(version.ID, userKey);

                if(version.FileName.LastIndexOf(".") > 0)
                {
                    version.FileName = version.FileName.Substring(0, version.FileName.LastIndexOf("."));
                }

                ApprovalVersionModel v = new ApprovalVersionModel()
                {
                    ID = version.ID,
                    Creator = version.Creator,
                    ErrorMessage = version.ErrorMessage,
                    Guid = version.Guid,
                    IsDeleted = version.IsDeleted,
                    Parent = parent,
                    Title = version.FileName,
                    VersionNumber = version.VersionNumber,
                    IsLocked = version.IsLocked || (version.CurrentPhase != null && !_unitOfWork.ApprovalRepository.IsLastVersion(versionId)),
                    AllowDownloadOriginal = version.AllowDownloadOriginal,
                    Decision = decision,
                    ApprovalType = version.ApprovalTypeName, //version.ID == 224880 ? "Zip" : version.ApprovalTypeName,
                    VersionThumbnail = version.isVideo
                                                        ? GMGColorDAL.Approval.GetVideoImagePath(version.Guid, version.AccountRegion,
                                                                                                    version.AccountDomain)
                                                        : GMGColorDAL.Approval.GetThumbnailImagePath(version.AccountDomain,
                                                                                                        version.AccountRegion, version.Guid,
                                                                                                        version.FirstPageNumber.ToString()),
                    PagesCount = version.TotalPagesCount,
                    JobStatus = version.JobStatus,
                    IsRGBColorSpace = version.IsRGBColorSpace,
                    CurrentPhase = version.CurrentPhase,
                    CurrentPhaseName = version.PhaseName,
                    IsCompleted = version.IsPhaseCompleted,
                    VersionLabel = ConstructVersionLabel(version.VersionLabel, version.VersionNumber),
                    PageVersionLabel = ConstructPageVersionLabel(version.FileName, version.VersionNumber),
                    CurrentPhaseShouldBeViewedByAll = version.shouldBeViewedByAll,
                    RestrictedZoom = version.RestrictedZoom
                };

                var movieApproval = version.ApprovalType == (int)DomainModels.ServiceOutput.ApprovalType.Video;

                if (movieApproval)
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        string result1 = Path.ChangeExtension(version.FileName, ".mp4");
                        v.MovieFilePath =
                            GMGColorCommon.GetDataFolderHttpPath(version.AccountRegion, version.AccountDomain) +
                            "approvals/" + version.Guid + "/" + GMGColorConfiguration.CONVERTED + "/" + result1;
                    }
                    else
                    {
                        v.MovieFilePath = GMGColorDAL.Approval.GetOriginalFilePath(version.AccountDomain, version.AccountRegion,
                                                                            version.Guid, version.FileName);
                    }
                }
                approvalVersion = v;
            }
            return approvalVersion;

        }


        private List<ApprovalCollaboratorModel> GetCollaboratorsAndAttachColors(List<InternalUseCollaboratorModel> collaborators, bool isExternalUser, string timezone)
        {
            List<string> avoidedColors = new List<string>();

            var list = new List<ApprovalCollaboratorModel>();

            if (isExternalUser)
            {
                collaborators = AttachColorToExternalUserCollaborators(collaborators);
            }

            foreach (var collaborator in collaborators)
            {
                var coll = new ApprovalCollaboratorModel
                {
                    ID = collaborator.Collaborator,
                    CollaboratorRole = collaborator.CollaboratorRole,
                    FamilyName = collaborator.FamilyName,
                    GivenName = collaborator.GivenName,
                    AssignedDate = DateTime2String(collaborator.AssignedDate, timezone),
                    CollaboratorAvatar = GMGColorDAL.User.GetImagePath(collaborator.AccountRegion, collaborator.AccountDomain,
                                                            collaborator.UserPhotoPath, collaborator.UserGuid, false,
                                                            collaborator.IsExternal),
                    UserColor = collaborator.UserColor,
                    IsAcessRevoked = collaborator.IsAcessRevoked,
                    SubstituteUserName = collaborator.SubstituteUserName,
                    UserStatus = collaborator.UserStatus
                };

                list.Add(coll);
            }

            return list.GroupBy(i => i.ID).Select(i => i.First()).ToList();
        }

        private List<InternalUseCollaboratorModel> AttachColorToExternalUserCollaborators(List<InternalUseCollaboratorModel> collaborators)
        {
            var avoidedColors = new List<string>();

            foreach (var collaborator in collaborators)
            {
                // In case the user doesn't have a color assigned then generate a random and save it to database                
                if (String.IsNullOrEmpty(collaborator.UserColor))
                {
                    string userColor = GenerateCollaboratorProofStudioColor(collaborator.AccountID, avoidedColors);
                    _unitOfWork.ApprovalCollaboratorRepository.AttachColorToExternalCollaborator(collaborator.ID, userColor);

                    avoidedColors.Add(collaborator.UserColor);
                }
            }
            if (avoidedColors.Any())
            {
                _unitOfWork.Save();
            }

            return collaborators;
        }

        private Dictionary<int, bool> GetVersionIsLast(IEnumerable<int> versions)
        {
            Dictionary<int, bool> versionIsLastVersion = new Dictionary<int, bool>();
            foreach (var version in versions)
            {
                versionIsLastVersion.Add(version, _unitOfWork.ApprovalRepository.IsLastVersion(version));
            }

            return versionIsLastVersion;
        }

        public List<ApprovalCollaboratorLinkModel> GetCollaboratorLinks(List<InternalUseCollaboratorLinkModel> colaboratorLinks, string timezone, int firstVerion)
        {

            Dictionary<int, bool> versionIsLastVersion = GetVersionIsLast(colaboratorLinks.Select(cl => cl.ApprovalId).Distinct());

            List<ApprovalCollaboratorLinkModel> list =
                colaboratorLinks.Select(o => new ApprovalCollaboratorLinkModel
                {
                    collaborator_id = o.ID,
                    ExpireDate = DateTime2String(o.ExpireDate, timezone),
                    IsExpired = o.IsExpired,
                    approval_role = o.approval_role,
                    approval_version = o.approval_version,
                    UserGroup = o.PrimaryGroup > 0 ? (int?)o.PrimaryGroup : null,
                    Decision = o.Decision.HasValue ? (int?)o.Decision.Value : null,
                    CanAnnotate = ((o.CurrentPhase == null && (!o.IsLocked || o.ID == o.Owner)) || (o.CurrentPhase != null && versionIsLastVersion[o.ApprovalId] && 
                                                                                (!o.IsLocked || (o.IsLocked && o.ID == o.JobOwner)))) && GMG.CoZone.Common.Permissions.CanAnnotate(o.Permission),
                    CanApprove = ((o.CurrentPhase == null && (!o.IsLocked || o.ID == o.Owner)) || (o.CurrentPhase != null && versionIsLastVersion[o.ApprovalId] && 
                                                                                (!o.IsLocked || (o.IsLocked && o.ID == o.JobOwner)))) && GMG.CoZone.Common.Permissions.CanApprove(o.Permission)
                }).ToList();

            for (int i = 0; i < list.Count(); i++)
            {
                list[i].ID = (firstVerion * 10000) + i + 1;
            }
            return list;
        }
        
        [ObsoleteAttribute("This method is obsolete. TODO: refactor to use GetCollaboratorLinks instead.", false)]
        public List<ApprovalCollaboratorLinkModel> GetCollaboratorLinksForUpdateApprovalDecision(int approvalId, bool isExternalUser, string timezone,
                                                                            bool userCanViewAllPhasesAnnotations, List<int> visiblePhases)
        {
            var jobId = _unitOfWork.ApprovalRepository.GetJobId(approvalId);

            var colaboratorsLinks = isExternalUser ? _unitOfWork.ApprovalCollaboratorRepository.GetExternalUserCollaboratorLinksForUpdateApprovalDecision(jobId, userCanViewAllPhasesAnnotations, visiblePhases)
                                : _unitOfWork.ApprovalCollaboratorRepository.GetInternalUserCollaboratorLinksForUpdateApprovalDecision(jobId, userCanViewAllPhasesAnnotations, visiblePhases);

            List<ApprovalCollaboratorLinkModel> list =
                colaboratorsLinks.Select(o => new ApprovalCollaboratorLinkModel
                {
                    collaborator_id = o.ID,
                    ExpireDate = DateTime2String(o.ExpireDate, timezone),
                    IsExpired = o.IsExpired,
                    approval_role = o.approval_role,
                    approval_version = o.approval_version,
                    UserGroup = o.PrimaryGroup > 0 ? (int?)o.PrimaryGroup : null,
                    Decision = o.Decision.HasValue ? (int?)o.Decision.Value : null
                }).ToList();
            for (int i = 0; i < list.Count(); i++)
            {
                list[i].ID = (approvalId * 10000) + i + 1;
            }
            return list;
        }

        private List<SoftProofingSessionParamsModel> GetSoftProofingParamsDefaultOrCreate(int approval, int job, bool isRGB)
        {
            var softProofingParam = GetSoftProofingParamDefaultOrCreate(approval, job, isRGB);
            if (softProofingParam == null)
                return new List<SoftProofingSessionParamsModel>();

            return new List<SoftProofingSessionParamsModel> { softProofingParam };
        }

        private SoftProofingSessionParamsModel GetSoftProofingParamDefaultOrCreate(int approval, int job, bool isRGB)
        {
            if (isRGB) //for rgb documents just return def session
                return _unitOfWork.SoftProofingSessionParamRepository.GetDefaultRGBProofingSessionParameters(approval);

            // isCMYK            
            var jobViewingConditions = _unitOfWork.SoftProofingSessionParamRepository.GetJobViewingCondition(job);

            //check if job viewing cond are set in job details
            if (jobViewingConditions != null && jobViewingConditions.SimulationProfile.HasValue)
            {
                var jobExistingSession = _unitOfWork.SoftProofingSessionParamRepository.GetExistingProofingSessionParameters(approval, jobViewingConditions);
                if (jobExistingSession != null)
                    return jobExistingSession;

                var defaulttSessionParam = _unitOfWork.SoftProofingSessionParamRepository.GetDefaultSessionParamsByApprovalId(approval);

                SoftProofingSessionParamsModel jobSpspModel = _unitOfWork.SoftProofingSessionParamRepository.CreateNewSoftproofingParameters(approval, jobViewingConditions, defaulttSessionParam == null ? false : defaulttSessionParam.HighResolution);
                _unitOfWork.Save();

                return jobSpspModel;
            }

            int? paperTintId = jobViewingConditions != null ? jobViewingConditions.PaperTint : null;

            if (!paperTintId.HasValue)
                return _unitOfWork.SoftProofingSessionParamRepository.GetDefaultCMYKProofingSessionParameters(approval);


            var defaultConditions = _unitOfWork.SoftProofingSessionParamRepository.GetApprovalViewingCondition(approval);

            //check if already rendered
            var existingSession = _unitOfWork.SoftProofingSessionParamRepository.GetExistingProofingSessionParameters(approval, paperTintId, defaultConditions);

            if (existingSession != null)
                return existingSession;

            SoftProofingSessionParamsModel spspModel = _unitOfWork.SoftProofingSessionParamRepository.CreateNewSoftproofingParameters(approval, paperTintId, defaultConditions);
            spspModel.HighResolution = false;
            _unitOfWork.Save();

            return spspModel;
        }

        private string ConstructVersionLabel(string versionLabel, int versionNumber)
        {
            return versionLabel != string.Empty && versionLabel != null
                ? versionLabel
                : "v" + versionNumber;
        }

        private string ConstructPageVersionLabel(string title, int versionNumber)
        {
            return "<span class='versionstyle'>v" + versionNumber + "</span>" + "<span class='versiontitlestyle' title='" + title + "'>" + title + "</span>";
        }

        public string GenerateCollaboratorProofStudioColor(int accountID, List<string> avoidColors)
        {
            // Retrieve static internal user color and account existing external users color
            List<string> predefinedColors = _unitOfWork.ApprovalCollaboratorRepository.GetPredefinedUserColors(accountID, avoidColors);

            // Transform existing colors to grayscale
            List<Color> colors = predefinedColors.Select(ColorTranslator.FromHtml).ToList();
            List<float> hueVals = colors.Select(i => i.GetHue()).GroupBy(v => v).Select(v => v.First()).OrderBy(v => v).ToList();
            // Get next available color in grayscale and convert it to RGB
            double nextAvailableColor = _colorHelper.GetNextAvailableColorInGrayscale(hueVals);
            Color externalUSerColor = _colorHelper.ColorFromHSV(nextAvailableColor, 0.75, 0.75);

            return _colorHelper.GetColorAsHex(externalUSerColor);
        }

        public ApprovalUserGroupsResponseModel GetApprovalUserGroups(ApprovalBasicModel approvalBasic, string userKey, bool isExternalUser)
        {
            ApprovalUserGroupsResponseModel approvalResponse = new ApprovalUserGroupsResponseModel();

            approvalResponse.user_groups = _unitOfWork.ApprovalCollaboratorRepository.GetJobPrimayGroups(approvalBasic.JobId, userKey, isExternalUser);
            approvalResponse.user_group_links = _unitOfWork.ApprovalCollaboratorRepository.GetJobPrimaryGroupDecisions(userKey, approvalBasic.JobId);

            return approvalResponse;
        }

        public ApprovalInitialVersionsResponseModel GetApprovalInitialVersions(ApprovalBasicModel approvalBasic, string userKey, bool isExternalUser)
        {
            var approvalResponse = new ApprovalInitialVersionsResponseModel();  

            bool userIsAdmin = _unitOfWork.UserRepository.UserIsAdministrator(userKey);
            bool userIsManager = _unitOfWork.UserRepository.UserIsManager(userKey);           

            ApprovalInternalUseModel approval = null;
            if (isExternalUser)
            {
                approval = _unitOfWork.ApprovalRepository.GetApprovalForExternalUser(approvalBasic.ApprovalId, userKey);
            }
            else
            {
                var loggedUserId = _unitOfWork.UserRepository.GetUserIdByGuid(userKey);
                approval = _unitOfWork.ApprovalRepository.GetApprovalForInternalUser(approvalBasic.ApprovalId, loggedUserId, userIsAdmin, userIsManager);
            }  

            if (approval == null)
                throw new ErrorModel(ErrorApiCodesEnum.Forbidden, "No approval for approval id");
            
            approvalResponse.approvals.Add(new ApprovalModel
            {
                ID = approval.ID,
                ErrorMessage = approval.ErrorMessage,
                Title = approval.Title,
                ApprovalType = approval.ApprovalTypeName,// approval.ID == 224880 ?"Zip" : approval.ApprovalTypeName,
                Creator = approval.Creator,
            });

            approvalResponse.versions = GetInitialVersions(approvalBasic, userKey, isExternalUser, approval.Account, userIsAdmin);
            approvalResponse.approvals[0].Versions = approvalResponse.versions.Select(v => v.ID).ToArray();

            var isRGB = approvalResponse.versions.FirstOrDefault(t => t.ID == approval.ID).IsRGBColorSpace;
            approvalResponse.softProofingParams = GetSoftProofingParamsDefaultOrCreate(approval.ID, approval.Job, isRGB);

            return approvalResponse;
        }

        public ApprovalCollaboratorsResponseModel GetApprovalCollaborators(ApprovalBasicModel approval, string userKey, bool isExternalUser)
        {
            ApprovalCollaboratorsResponseModel approvalResponse = new ApprovalCollaboratorsResponseModel();            

            string timezone = _unitOfWork.ApprovalRepository.GetApprovalTimezone(approval.ApprovalId);

            var userCanViewAllPhasesAnnotations = approval.CurrentPhaseId != null &&
                        (isExternalUser ? _unitOfWork.UserRepository.ExternalUserCanViewAllPhasesAnnotations(userKey) :
                                            _unitOfWork.UserRepository.InternalUserCanViewAllPhasesAnnotations(userKey));

            var visiblePhases = isExternalUser ? _unitOfWork.ApprovalJobPhaseRepository.GetExternalUserVisiblePhases(approval.ApprovalId, approval.CurrentPhaseId) :
                                                    _unitOfWork.ApprovalJobPhaseRepository.GetInternalUserVisiblePhases(approval.ApprovalId, approval.CurrentPhaseId);

            var maxVersion = new[] { approval.ApprovalId };

            var internalPartialCollaborators = _unitOfWork.ApprovalCollaboratorRepository.GetInternalUserPartialCollaborators(maxVersion.ToList(), userCanViewAllPhasesAnnotations, visiblePhases);
            var externalPartialCollaborators = _unitOfWork.ApprovalCollaboratorRepository.GetExternalUserPartialCollaborators(maxVersion.ToList(), userCanViewAllPhasesAnnotations, visiblePhases);


            approvalResponse.internal_collaborators = GetCollaboratorsAndAttachColors(
                _unitOfWork.ApprovalCollaboratorRepository.GetInternalUserCollaborators(internalPartialCollaborators), false, timezone);

            approvalResponse.internal_collaborators_links = GetCollaboratorLinks(
                _unitOfWork.ApprovalCollaboratorRepository.GetInternalUserCollaboratorLinks(internalPartialCollaborators), timezone, maxVersion.First());

            approvalResponse.external_collaborators = GetCollaboratorsAndAttachColors(
                _unitOfWork.ApprovalCollaboratorRepository.GetExternalUserCollaborators(externalPartialCollaborators), false, timezone);

            approvalResponse.external_collaborators_links = GetCollaboratorLinks(
                _unitOfWork.ApprovalCollaboratorRepository.GetExternalUserCollaboratorLinks(externalPartialCollaborators), timezone, maxVersion.First());

            return approvalResponse;
        }

        public bool CheckIfApprovalIsNotDeleted(int approvalId)
        {
            return _unitOfWork.ApprovalRepository.CheckIfApprovalIsNotDeleted(approvalId);
        }

        public bool IsFromCurrentAccount(List<int> IdList, string userGuid, string typeOf)
        {
            return _unitOfWork.UserRepository.IsFromCurrentAccount(IdList, userGuid, typeOf);
        }

    }
}
