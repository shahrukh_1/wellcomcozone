﻿using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMG.CoZone.Proofstudio.Interfaces;
using GMGColor.Resources;
using System;
using System.Linq;
using System.Collections.Generic;
using GMG.CoZone.Proofstudio.DomainModels.Requests;
using GMG.CoZone.Proofstudio.DomainModels.Responses;
using GMG.CoZone.Repositories.Interfaces;
using GMGColorDAL;

namespace GMG.CoZone.Proofstudio.Services
{
    public class ProofstudioApiService : IProofstudioApiService
    {
        IGetProofstudioApproval _getApproval;
        IUnitOfWork _unitOfWork;

        public ProofstudioApiService(IGetProofstudioApproval getApproval, IUnitOfWork unitOfWork)
        {
            _getApproval = getApproval;
            _unitOfWork = unitOfWork;
        }

        public void ValidateRequest(BaseGetEntityRequestModel model)
        {
            if (string.IsNullOrEmpty(model.key) || string.IsNullOrEmpty(model.user_key))
            {
                throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblApiMissingRequestParameters);
            }
            if (model.key.ToLower() != GMGColorDAL.GMGColorConfiguration.AppConfiguration.RestApiSecurityToken.ToLower())
            {
                throw new ErrorModel(ErrorApiCodesEnum.Forbidden, Resources.lblApiOperationNotAllowed);
            }
        }

        public void ValidateRequest(ApprovalBasicRequestModel model)
        {
            ValidateRequest((BaseGetEntityRequestModel)model);

            if (string.IsNullOrEmpty(model.approvalsIds))
            {
                throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblApiMissingRequestParameters);
            }
        }

        public void ValidateRequest(ApprovalGranularRequestModel model)
        {
            ValidateRequest((BaseGetEntityRequestModel)model);

            if (string.IsNullOrEmpty(model.approvalBasic))
            {
                throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblApiMissingRequestParameters);
            }

        }

        public void IsFromCurrentAccount(List<int> IdList, string userGuid, string typeOf)
        {
            if (!_getApproval.IsFromCurrentAccount(IdList, userGuid, typeOf))
            {
                throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblApiMissingRequestParameters);
            }
        }

        public ApprovalsBasicResponseModel GetApprovalsBasic(ApprovalBasicRequestModel approvalsBasicRequest)
        {
            try
            {
                return _getApproval.GetApprovalsBasic(approvalsBasicRequest.approvalsIds);
            }
            catch(Exception ex)
            {
                GMGColorDAL.GMGColorLogging.log.Error(ex.Message, ex);
                throw new ErrorModel(ErrorApiCodesEnum.InternalServerError, "error in GetApprovalsBasic");
            }
        }

        public ApprovalInitialVersionsResponseModel GetApprovalInitialVersions(List<ApprovalBasicModel> approvals, string userKey, bool isExternalUser)
        {
            var response = new ApprovalInitialVersionsResponseModel();
            int noOfVersions = 0;

            foreach (var approval in approvals)
            {
                bool approvalExists = _getApproval.CheckIfApprovalIsNotDeleted(approval.ApprovalId);
                if (approvalExists)
                {
                    ApprovalInitialVersionsResponseModel approvalItem = _getApproval.GetApprovalInitialVersions(approval, userKey, isExternalUser);

                    response.approvals.AddRange(approvalItem.approvals);

                    foreach (var version in approvalItem.versions)
                    {
                        // version parent is not loaded all the times and it is required in ProofStudion
                        version.Parent = approvalItem.approvals[0].ID;
                    }

                    response.versions.AddRange(approvalItem.versions);
                    response.softProofingParams.AddRange(approvalItem.softProofingParams.Where(x => response.softProofingParams.All(y => y.ID != x.ID)));
                }
                else
                {
                    throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblNotification_ApprovalsWasDeleted);
                }
            }
            // set the order in which the versions are seen in dropdown list (the versions are displayed in ascending order)
            for (int i = 0; i < approvals.Count; i++)
            {
                var versions =
                    response.versions.Where(v => v.Parent == response.approvals[i].ID)
                        .OrderByDescending(v => v.VersionNumber)
                        .ToList();
                for (int j = 0; j < versions.Count(); j++)
                {
                    versions[j].OrderInDropDownMenu = noOfVersions++;
                }
            }
            return response;
        }

        public ApprovalUserGroupsResponseModel GetApprovalUserGroups(List<ApprovalBasicModel> approvals, string userKey, bool isExternalUser)
        {
            var response = new ApprovalUserGroupsResponseModel();
            foreach (var approval in approvals)
            {
                bool approvalExists = _getApproval.CheckIfApprovalIsNotDeleted(approval.ApprovalId);
                if (approvalExists)
                {
                    ApprovalUserGroupsResponseModel approvalItem = _getApproval.GetApprovalUserGroups(approval, userKey, isExternalUser);

                    response.user_groups.AddRange(approvalItem.user_groups.Where(x => response.user_groups.All(y => y.ID != x.ID)));
                    response.user_group_links.AddRange(approvalItem.user_group_links.Where(x => response.user_group_links.All(y => y.ID != x.ID)));
                }
                else
                {
                    throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblNotification_ApprovalsWasDeleted);
                }
            }

            if (response.user_groups.Count == 0)
            {
                throw new ErrorModel(ErrorApiCodesEnum.NoContent, "no user groups available");
            }

            return response;
        }

        public ApprovalCollaboratorsResponseModel GetApprovalCollaborators(List<ApprovalBasicModel> approvals, string userKey, bool isExternalUser)
        {
            var response = new ApprovalCollaboratorsResponseModel();
            foreach (var approval in approvals)
            {
                bool approvalExists = _getApproval.CheckIfApprovalIsNotDeleted(approval.ApprovalId);
                if (approvalExists)
                {
                    ApprovalCollaboratorsResponseModel approvalItem = _getApproval.GetApprovalCollaborators(approval, userKey, isExternalUser);

                    response.external_collaborators.AddRange(approvalItem.external_collaborators.Where(x => response.external_collaborators.All(y => y.ID != x.ID)));
                    response.external_collaborators_links.AddRange(approvalItem.external_collaborators_links.Where(x => response.external_collaborators_links.All(y => y.ID != x.ID)));
                    response.internal_collaborators.AddRange(approvalItem.internal_collaborators.Where(x => response.internal_collaborators.All(y => y.ID != x.ID)));
                    response.internal_collaborators_links.AddRange(approvalItem.internal_collaborators_links.Where(x => response.internal_collaborators_links.All(y => y.ID != x.ID)));

                }
                else
                {
                    throw new ErrorModel(ErrorApiCodesEnum.BadRequest, Resources.lblNotification_ApprovalsWasDeleted);
                }

            }
            return response;
        }

        public void ResetApprovalPagesProgress(int approvalId)
        {
            _unitOfWork.ApprovalPageRepository.ResetApprovalPagesProgress(approvalId);
        }

        public int GetApprovalIdByProofStudioSession(string sessionId)
        {
            return _unitOfWork.SoftProofingSessionParamRepository.GetApprovalIdByProofStudioSession(sessionId);
        }

        public void ResetDefaultForNonHighResSessionParams(int approvalId)
        {
            _unitOfWork.SoftProofingSessionParamRepository.ResetDefaultForNonHighResSessionParams(approvalId);
        }
        public SoftProofingSessionParam GetDefaultSessionParamsByApprovalId(int approvalId)
        {
            return _unitOfWork.SoftProofingSessionParamRepository.GetDefaultSessionParamsByApprovalId(approvalId);
        }

        public int GetUserByUserKey(string userKey)
        {
            return _unitOfWork.UserRepository.GetUserIdByGuid(userKey);
        }

        public bool CheckHighResProcessingIsInProgressForApproval(int approvalId)
        {
            var highResInProgressSessionGuid = _unitOfWork.SoftProofingSessionParamRepository.GetApprovalHighResSoftProofingSessionInProgress(approvalId);
            if (string.IsNullOrEmpty(highResInProgressSessionGuid))
            {
                return false;
            }
            return true;
        }
    }
}
