﻿using GMG.CoZone.Settings.Interfaces;
using GMG.CoZone.Common.Interfaces;
using System.Messaging;
using System.IO;
using Newtonsoft.Json;
using GMG.CoZone.Common.DomainModels;
using GMGColor.AWS;
using System.Threading;
using System;

namespace GMG.CoZone.Settings.Services
{
    public class SmtpQueueReader : ISmtpQueueParser
    {
        private readonly IQueueMessageManager _queueMessageManager;
        private readonly IGMGConfiguration _gmgConfiguration;

        public SmtpQueueReader(IQueueMessageManager queueMessageManager, IGMGConfiguration gmgConfiguration)
        {
            _queueMessageManager = queueMessageManager;
            _gmgConfiguration = gmgConfiguration;
        }

        public string ParseConnectionReplyFromAWSQueue(string queueName, string messageGuid)
        {
            var connectionError = string.Empty;
            var isFound = false;
            var start = DateTime.UtcNow;

            while (!isFound && DateTime.UtcNow.Subtract(start).Minutes < 2)
            {
                var queueMessages = _queueMessageManager.ReadMessagesFromAWSQueue(queueName);

                foreach (var message in queueMessages)
                {
                    var messageBody = JsonConvert.DeserializeObject<CustomSmtpEmailSenderReply>(message.Body);

                    if (messageGuid == messageBody.MessageGuid)
                    {
                        isFound = true;
                        connectionError = messageBody.Error;

                        AWSSimpleQueueService.DeleteMessage(queueName, message, _gmgConfiguration.AWSAccessKey, _gmgConfiguration.AWSSecretKey, _gmgConfiguration.AWSRegion);
                        break;
                    }
                }
                Thread.Sleep(2000);
            }
            return connectionError;
        }

        public string ParseConnectionReplyFromLocalQueue(string queueName, string messageGuid)
        {
            var connectionError = string.Empty;
            var isFound = false;

            var messageQueue = new MessageQueue(queueName);
            messageQueue.Formatter = new BinaryMessageFormatter();

            while (!isFound)
            {
                var msgEnumerator = messageQueue.GetMessageEnumerator2();

                while (msgEnumerator.MoveNext())
                {
                    var mess = msgEnumerator.Current;
                    var reader = new StreamReader(mess.BodyStream);
                    var msgBody = reader.ReadToEnd();
                    msgBody = msgBody.Substring(msgBody.IndexOf("{"), msgBody.LastIndexOf("}") + 1 - msgBody.IndexOf("{"));
                    var msgObj = JsonConvert.DeserializeObject<CustomSmtpEmailSenderReply>(msgBody);

                    if (messageGuid == msgObj.MessageGuid)
                    {
                        msgEnumerator.RemoveCurrent();
                        connectionError = msgObj.Error;
                        isFound = true;
                        break;
                    }
                }
            }
            return connectionError;
        }
    }
}
