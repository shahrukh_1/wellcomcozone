﻿using System;
using GMG.CoZone.Settings.Interfaces;
using GMG.CoZone.Common.Module.Enums;
using GMG.CoZone.Common.Module.ExtensionMethods;
using GMGColorDAL;
using GMGColorBusinessLogic;
using GMG.CoZone.Settings.Interfaces.Settings;
using GMG.CoZone.Common.Interfaces;
using Newtonsoft.Json;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Settings.DomainModels;
using System.Linq;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.Settings.Services.Settings
{
    public class SmtpSettings : ISmtpSettings
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGMGConfiguration _gmgConfiguration;
        private readonly IQueueMessageManager _queueMessageManager;
        private readonly ISmtpQueueParser _smtpQueueReader;
        private readonly string _settingsKey;

        public SmtpSettings(IUnitOfWork unitOfWork,
                            IGMGConfiguration gmgConfiguration, 
                            IQueueMessageManager queueMessageManager, 
                            ISmtpQueueParser smtpQueueReader)
        {
            _unitOfWork = unitOfWork;
            _settingsKey = Enum.GetName(typeof(SettingsKeyEnum), SettingsKeyEnum.CustomSmtpServer);
            _gmgConfiguration = gmgConfiguration;
            _queueMessageManager = queueMessageManager;
            _smtpQueueReader = smtpQueueReader;
        }

        public void AddUpdate(SmtpSettingsModel model, int loggedAccount, string loggedAccountGuid, string valueTypeKey)
        {
            var newSettings = new SmtpSettingsModel();

            if (!model.IsPasswordEnabled)
            {
                var accountSmpt = this.Get(loggedAccount);

                newSettings = accountSmpt ?? new SmtpSettingsModel();
                newSettings.ServerName = model.ServerName;
                newSettings.Port = model.Port;
                newSettings.EnableSsl = model.EnableSsl;
                newSettings.EmailAddress = model.EmailAddress;
            }
            else
            {
                newSettings = model;
                newSettings.Password = Common.Utils.Encrypt(model.Password, loggedAccountGuid);
                newSettings.ConfirmPassword = newSettings.Password;
            }

            newSettings.IsEnabled = true;
            newSettings.IsPasswordEnabled = model.IsPasswordEnabled;
            var settingsString = newSettings.AsDictionaryString();

            BuildSettingObject(settingsString, loggedAccount, _settingsKey, Enum.GetName(typeof(AccountSettingsBL.AccountSettingValueType), AccountSettingsBL.AccountSettingValueType.STRG));
        }

        public void Disable(int loggedAccount)
        {
            var existingSetting = _unitOfWork.AccountSettingsRepository.Get(filter: acs => acs.Name == _settingsKey && acs.Account == loggedAccount).FirstOrDefault();
            _unitOfWork.AccountSettingsRepository.Delete(existingSetting);
            _unitOfWork.Save();
        }

        public SmtpSettingsModel Get(int loggedAccount)
        {
            var dbSettings = _unitOfWork.AccountSettingsRepository.Get(filter: acs => acs.Name == _settingsKey && acs.Account == loggedAccount).FirstOrDefault();
            var model = new SmtpSettingsModel();

            if (dbSettings != null && dbSettings.Value != null)
            {
                model = dbSettings.Value.ToObject<SmtpSettingsModel>();
            }
            return model;
        }

        public string TestConnection(CustomSmtpTestConnectionModel messageModel)
        {
            var connectionError = string.Empty;
            try
            {
                _queueMessageManager.WriteMessageToQueue(_gmgConfiguration.CustomSmtpWriteQueueName.ToString(), JsonConvert.SerializeObject(messageModel));
                if (_gmgConfiguration.IsEnabledAmazonSQS)
                {
                    connectionError = _smtpQueueReader.ParseConnectionReplyFromAWSQueue(_gmgConfiguration.CustomSmtpReadQueueName.ToString(), messageModel.MessageGuid);
                }
                else
                {
                    connectionError = _smtpQueueReader.ParseConnectionReplyFromLocalQueue(_gmgConfiguration.CustomSmtpReadQueueName.ToString(), messageModel.MessageGuid);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return connectionError;
        }

        private AccountSetting BuildSettingObject(string settingsString, int loggedAccount, string _settingsKey, string valueKey)
        {
            var dbSettings = _unitOfWork.AccountSettingsRepository.Get(filter: acs => acs.Name == _settingsKey && acs.Account == loggedAccount).FirstOrDefault();

            if (dbSettings == null)
            {
                _unitOfWork.AccountSettingsRepository.Add(new AccountSetting
                {
                    Account = loggedAccount,
                    Name = _settingsKey,
                    ValueDataType = _unitOfWork.AccountSettingsValueTypeRepository.Get(vtp => vtp.Key == valueKey).Select(vtp => vtp.ID).FirstOrDefault(),
                    Description = _settingsKey,
                    Value = settingsString,
                    CreatedDate = DateTime.UtcNow,
                    ModifiedData = DateTime.UtcNow,
                    Creator = loggedAccount,
                    Modifier = loggedAccount,
                });
            }
            else
            {
                dbSettings.Value = settingsString;
                dbSettings.Modifier = loggedAccount;
                dbSettings.ModifiedData = DateTime.UtcNow;
                _unitOfWork.AccountSettingsRepository.Update(dbSettings);
            }
            _unitOfWork.Save();
            return dbSettings;
        }

        public string GetPassword(int loggedAccount)
        {
            var settings = _unitOfWork.AccountSettingsRepository.Get(acs => acs.Name == _settingsKey && acs.Account == loggedAccount).FirstOrDefault().Value;
            if (settings != null)
            {
                return settings.ToObject<SmtpSettingsModel>().Password;
            }
            return string.Empty;
        }
    }
}
