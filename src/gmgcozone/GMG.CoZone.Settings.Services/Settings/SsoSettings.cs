﻿using System;
using GMG.CoZone.Common.Module.Enums;
using GMG.CoZone.Common.Module.ExtensionMethods;
using GMG.CoZone.Settings.Interfaces.Settings;
using GMG.CoZone.Settings.DomainModels;
using GMGColorDAL.Common;
using GMGColorDAL;
using GMGColorBusinessLogic.Common;
using GMG.CoZone.SSO.DomainModels;
using System.Linq;
using GMG.CoZone.Repositories.Interfaces;

namespace GMG.CoZone.Settings.Services.Settings
{
    public class SsoSettings : ISsoSettings
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly string _settingsKey = Enum.GetName(typeof(SettingsKeyEnum), SettingsKeyEnum.SingleSignOn);

        public SsoSettings(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddUpdate(SsoModel settings, AccountSettingsDataModel accountSettingsData)
        {
            SaveSsoSettings(settings, accountSettingsData);

            if (settings.ActiveConfiguration == SsoOptionsEnum.Saml && !string.IsNullOrEmpty(settings.Saml.CertificateName))
            {
                SaveCertificateFile(settings.Saml, accountSettingsData);
            }
        }

        private void SaveSsoSettings(SsoModel settings, AccountSettingsDataModel accountSettingsData)
        {
            var dbSetings = _unitOfWork.AccountSettingsRepository.Get(filter: vtp => vtp.Name == _settingsKey && vtp.Account  == accountSettingsData.AccountId).FirstOrDefault();

            if (settings.ActiveConfiguration == SsoOptionsEnum.Saml)
            {
                settings.Saml.CertificateName = settings.Saml.CertificateName.Replace("|", string.Empty);
            }
            AddUpdateSsoSettings(settings, accountSettingsData, dbSetings);
        }

        private void AddUpdateSsoSettings(SsoModel settings, AccountSettingsDataModel accountSettingsData, AccountSetting dbSetings)
        {
            if (dbSetings == null)
            {
                AddSsoSettings(settings, accountSettingsData);
            }
            else
            {
                UpdateSsoSettings(settings, accountSettingsData, dbSetings);
            }
        }

        private void AddSsoSettings(SsoModel settings, AccountSettingsDataModel accountSettingsData)
        {
            _unitOfWork.AccountSettingsRepository.Add(new AccountSetting
            {
                Account = accountSettingsData.AccountId,
                Name = _settingsKey,
                ValueDataType = _unitOfWork.AccountSettingsValueTypeRepository.Get(filter: vtp => vtp.Key == accountSettingsData.SettingValueType).Select(vtp => vtp.ID).FirstOrDefault(),
                Description = _settingsKey,
                Creator = accountSettingsData.UserId,
                Modifier = accountSettingsData.UserId,
                ModifiedData = DateTime.UtcNow,
                CreatedDate = DateTime.UtcNow,
                Value = settings.ActiveConfiguration == SsoOptionsEnum.Saml ? settings.Saml.AsDictionaryString() : settings.Auth0.AsDictionaryString(),
            });

            _unitOfWork.Save();
        }


        private void UpdateSsoSettings(SsoModel settings, AccountSettingsDataModel accountSettingsData, AccountSetting dbSetings)
        {
            dbSetings.ModifiedData = DateTime.UtcNow;
            dbSetings.Modifier = accountSettingsData.UserId;

            if (settings.ActiveConfiguration == SsoOptionsEnum.Saml  && string.IsNullOrEmpty(settings.Saml.CertificateName))
            {
                var ssoSettings = dbSetings.Value.ToObject<SsoSamlSettingsModel>();
                ssoSettings.EntityId = settings.Saml.EntityId;
                ssoSettings.IdpIssuerUrl = settings.Saml.IdpIssuerUrl;
            }
            else
            {
                dbSetings.Value = settings.ActiveConfiguration == SsoOptionsEnum.Saml ? settings.Saml.AsDictionaryString() : settings.Auth0.AsDictionaryString();
            }
            _unitOfWork.AccountSettingsRepository.Update(dbSetings);
            _unitOfWork.Save();
        }

        private void SaveCertificateFile(SsoSamlSettingsModel settings, AccountSettingsDataModel accountSettingsData)
        {
            var uploadFileName = settings.CertificateName.Replace("|", string.Empty);
            var relativeSourceFilePath = accountSettingsData.AccountTempPath + uploadFileName;
            var relativeDestinationFolderPath = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath,
                                                                  accountSettingsData.AccountGuid,
                                                                  GMGColorConfiguration.AppConfiguration.CertificateFolderRelativePath);

            if (!GMGColorIO.FileExists(relativeDestinationFolderPath + uploadFileName, accountSettingsData.AccountRegion))
            {
                GMGColorIO.DeleteFolderIfExists(relativeDestinationFolderPath, accountSettingsData.AccountRegion);
                if (GMGColorIO.FolderExists(relativeDestinationFolderPath, accountSettingsData.AccountRegion, true) && GMGColorIO.FileExists(relativeSourceFilePath, accountSettingsData.AccountRegion))
                {
                    GMGColorIO.MoveFile(relativeSourceFilePath, relativeDestinationFolderPath + uploadFileName, accountSettingsData.AccountRegion);
                }
            }
        }

        public void Disable(int loggedAccount, string region, string accoundGuid)
        { 
            var relativePathToCertificateFolder = Utils.PathCombine(GMGColorConfiguration.AppConfiguration.AccountsFolderRelativePath,
                                                       accoundGuid,
                                                       GMGColorConfiguration.AppConfiguration.CertificateFolderRelativePath);
            GMGColorIO.DeleteFolderIfExists(relativePathToCertificateFolder, region);
            _unitOfWork.AccountSettingsRepository.Delete(_unitOfWork.AccountSettingsRepository.Get(filter: vtp => vtp.Name == _settingsKey && vtp.Account == loggedAccount).FirstOrDefault());
            _unitOfWork.Save();
        }

        public SsoModel Get(int loggedAccount)
        {
            var dbSettings = _unitOfWork.AccountSettingsRepository.Get(filter: vtp => vtp.Name == _settingsKey && vtp.Account == loggedAccount).FirstOrDefault().Value;
            var model = new SsoModel();
            if (dbSettings != null && dbSettings.IndexOf(Enum.GetName(typeof(SsoOptionsEnum), SsoOptionsEnum.Auth0)) != -1)
            {
                model.Auth0 = dbSettings.ToObject<SsoAuth0SettingsModel>();
                model.ActiveConfiguration = SsoOptionsEnum.Auth0;
            }
            else
            {
                model.Saml = dbSettings.ToObject<SsoSamlSettingsModel>();
                model.ActiveConfiguration = SsoOptionsEnum.Saml;
            }
            return model;
        }
    }
}
