﻿using GMG.CoZone.Settings.Interfaces;
using Microsoft.Practices.Unity;
using GMG.CoZone.Settings.Interfaces.Settings;

namespace GMG.CoZone.Settings.Services
{
    /// <summary>
    /// Class that implements the ISettingsService
    /// </summary>
    public class SettingsService : ISettingsService
    {
        [Dependency]
        public ISsoSettings SsoSettings { get; set; }

        [Dependency]
        public ISmtpSettings SmtpSettings { get; set; }
    }
}
