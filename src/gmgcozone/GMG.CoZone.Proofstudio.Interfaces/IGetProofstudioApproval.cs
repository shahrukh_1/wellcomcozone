﻿using GMG.CoZone.Proofstudio.DomainModels.Requests;
using GMG.CoZone.Proofstudio.DomainModels.Responses;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using System;

namespace GMG.CoZone.Proofstudio.Interfaces
{
    public interface IGetProofstudioApproval
    {
        ApprovalsBasicResponseModel GetApprovalsBasic(string approvalsIds);
        ApprovalInitialVersionsResponseModel GetApprovalInitialVersions(ApprovalBasicModel approval, string userKey, bool isExternalUser);
        ApprovalUserGroupsResponseModel GetApprovalUserGroups(ApprovalBasicModel approval, string userKey, bool isExternalUser);        
        ApprovalCollaboratorsResponseModel GetApprovalCollaborators(ApprovalBasicModel approval, string userKey, bool isExternalUser);
        bool CheckIfApprovalIsNotDeleted(int approvalId);
        bool IsFromCurrentAccount(System.Collections.Generic.List<int> IdList, string userGuid, string typeOf);
    }
}
