﻿using GMG.CoZone.Proofstudio.DomainModels.Requests;
using GMG.CoZone.Proofstudio.DomainModels.Responses;
using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;
using System.Collections.Generic;

namespace GMG.CoZone.Proofstudio.Interfaces
{
    public interface IProofstudioApiService
    {
        void ValidateRequest(BaseGetEntityRequestModel model);
        void ValidateRequest(ApprovalBasicRequestModel model);
        void ValidateRequest(ApprovalGranularRequestModel model);
        void IsFromCurrentAccount(List<int> IdList, string userGuid, string typeOf);

        ApprovalsBasicResponseModel GetApprovalsBasic(ApprovalBasicRequestModel approvalsBasicRequest);
        ApprovalInitialVersionsResponseModel GetApprovalInitialVersions(List<ApprovalBasicModel> model, string userKey, bool isExternalUser);
        ApprovalUserGroupsResponseModel GetApprovalUserGroups(List<ApprovalBasicModel> model, string userKey, bool isExternalUser);
        ApprovalCollaboratorsResponseModel GetApprovalCollaborators(List<ApprovalBasicModel> model, string userKey, bool isExternalUser);

        void ResetApprovalPagesProgress(int approvalId);
        int GetApprovalIdByProofStudioSession(string sessionId);
        void ResetDefaultForNonHighResSessionParams(int approvalId);
        SoftProofingSessionParam GetDefaultSessionParamsByApprovalId(int approvalId);

        int GetUserByUserKey(string userKey);

        bool CheckHighResProcessingIsInProgressForApproval(int approvalId);
    }
}