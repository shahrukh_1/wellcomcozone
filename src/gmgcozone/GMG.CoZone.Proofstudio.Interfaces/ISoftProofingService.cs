﻿using GMG.CoZone.Proofstudio.DomainModels.ServiceOutput;
using GMGColorDAL;

namespace GMG.CoZone.Proofstudio.Interfaces
{
    public interface ISoftProofingService
    {
        ViewingCondition GetViewingConditionByJobId(int approvalId);
        SoftProofingSessionParamsModel GetJobSoftProofingParams(int approvalId, int jobId);
        void AddNewSoftProofinSession(SoftProofingSessionParam sessionParams);
    }
}
