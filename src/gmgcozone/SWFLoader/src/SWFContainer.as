package {

import flash.display.Sprite;
import flash.events.Event;
import flash.events.ProgressEvent;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.system.Security;
import flash.utils.ByteArray;

    public class SWFContainer extends Sprite
    {
        private var loadedFile:ByteArray;
        private var url:String;

        public function SWFContainer()
        {
            Security.allowDomain("*");
            Security.allowInsecureDomain("*");
        }

        private function loadSwfApplication():void
        {
            var loader:URLLoader = new URLLoader();
            loader.dataFormat = URLLoaderDataFormat.BINARY;
            loader.addEventListener(ProgressEvent.PROGRESS, onLoaderProgress);
            loader.addEventListener(Event.COMPLETE, onSWFLoaded);
            loader.load(new URLRequest(url));
        }

        private function onLoaderProgress(event:ProgressEvent):void
        {
            dispatchEvent(event);
        }

        private function onSWFLoaded(e:Event):void
        {
            var loader:URLLoader = URLLoader(e.target);
            loader.removeEventListener(Event.COMPLETE, onSWFLoaded);
            loadedFile = loader.data;
            dispatchEvent(new Event('loadFileComplete'));
        }

        public function loadFile(url:String):void
        {
            this.url = url;
            loadSwfApplication();
        }

        public function getLoadedFile():ByteArray
        {
            return loadedFile;
        }
    }
}
