﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class JobDetails
    {
        #region Properties

        public string jobGuid { get; set; }
        public string jobTitle { get; set; }
        public string jobStatus { get; set; }
        public string folder { get; set; }

        #endregion
    }
}
