﻿using GMGColor.Areas.CollaborateAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.JobModels
{
    public class ProofStudioCompareVersionsRequest : CollaborateAPIBaseModel
    {
        public List<string> versions { get; set; }
    }
}
