﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Validators;

namespace GMG.CollaborateAPI.BL.Models
{
    public class CreateJobMetadataResponse
    {
        #region Properties

        public string resourceID { get; set; }

        #endregion
    }

    public class CreateJobResponse
    {
        #region Properties

        public string guid { get; set; }
        public CreateApprovalResponse approval { get; set; }
        public List<string> warningMessages { get; set; } 

        #endregion


        #region Constructors

        public CreateJobResponse()
        {
            warningMessages =  new List<string>();
        }

        #endregion
    }
}
