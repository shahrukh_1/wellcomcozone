﻿using System;

namespace GMG.CollaborateAPI.BL.Models
{
    [Serializable]
    public class CreateJobRequest
    {
        #region Properties

        public string jobName { get; set; }

        public string workflow { get; set; }

        public string checklist { get; set; }

        public bool allowUsersToBeAddedOnPhase { get; set; }

        public CreateApprovalRequest approval { get; set; }

        #endregion
    }
}
