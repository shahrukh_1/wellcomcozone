﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UpdateJobRequest : CollaborateAPIIdentityModel
    {
        #region Properties

        public string jobStatus { get; set; }
        public string jobTitle { get; set; }

        #endregion
    }
}
