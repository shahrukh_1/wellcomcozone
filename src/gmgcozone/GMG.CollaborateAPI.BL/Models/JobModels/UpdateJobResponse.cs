﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.JobModels
{
    public class UpdateJobResponse
    {
        #region Properties

        public string jobGuid { get; set; }

        #endregion
    }
}
