﻿using System.ComponentModel.DataAnnotations;
using GMG.CollaborateAPI.BL.Models;
using System.Collections.Generic;
using System;

namespace GMGColor.Areas.CollaborateAPI.Models
{
    [Serializable]
    public class CollaborateAPIBaseModel : ICollaborateAPIModel
    {
        #region Properties

        [Required]
        public string sessionKey { get; set; }

        public string username { get; set; }

        #endregion
    }

    [Serializable]
    public class CollaborateAPIIdentityModel : CollaborateAPIBaseModel
    {
        #region Properties

        public string guid { get; set; }

        #endregion
    }
}