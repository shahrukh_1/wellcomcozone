﻿namespace GMG.CollaborateAPI.BL.Models.AnnotationModels
{
    public class AnnotationReportUrlResponse
    {
        public string url { get; set; }
    }
}
