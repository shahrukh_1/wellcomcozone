﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class AnnotationBaseModel : CollaborateAPIIdentityModel
    {
        #region Properties

        public string sortBy { get; set; }
        public string filterBy { get; set; }
        public List<UserCollaborator> users { get; set; }
        public List<string> groups { get; set; }
        public List<string> phases { get; set; }
        public bool hasAccess { get; set; }

        #endregion
    }

    public class UserCollaborator
    {
        #region Properties

        public string username { get; set; }
        public bool isExternal { get; set; }

        #endregion
    }
}
