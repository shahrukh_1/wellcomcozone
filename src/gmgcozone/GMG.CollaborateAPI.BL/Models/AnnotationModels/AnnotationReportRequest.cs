﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using System.ComponentModel;

namespace GMG.CollaborateAPI.BL.Models
{
    public class AnnotationReportRequest : AnnotationBaseModel
    {
        #region Properties

        public int nrOfCommentsPerPage { get; set; }
        public string displayOptions { get; set; }
        public bool? includeSummaryPage { get; set; }

        [DefaultValue(true)]
        public bool includeAllVersions { get; set; }

        #endregion
    }
}
