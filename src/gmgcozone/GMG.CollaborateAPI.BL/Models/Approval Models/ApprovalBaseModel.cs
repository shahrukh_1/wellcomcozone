﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    [Serializable]
    public class ApprovalBaseModel : CollaborateAPIIdentityModel
    {
        #region Properties

        [Required]
        public string fileName { get; set; }
        public string deadline { get; set; }
        public string folder { get; set; }
        public List<Collaborator> collaborators { get; set; }
        public List<Phase> phases { get; set; }

        #endregion

        #region Constructors

        public ApprovalBaseModel()
        {
            collaborators = new List<Collaborator>();
            phases = new List<Phase>();
        }

        #endregion

        public override string ToString()
        {
            return fileName + ", Deadline: " + deadline + ", Collaborators: " + string.Join(",", collaborators.Select(c => c.ToString()).ToArray()) +
                ", Phases: " + string.Join(",", phases.Select(c => c.ToString()).ToArray());
        }
    }

    [Serializable]
    public class Phase
    {
        #region Properties

        public string phaseGUID { get; set; }

        public List<Collaborator> collaborators { get; set; }

        #endregion

        #region Constructor

        public Phase()
        {
            collaborators = new List<Collaborator>();
        }

        public override string ToString()
        {
            return phaseGUID + ", Phase Collaborators: " + string.Join(",", collaborators.Select(c => c.ToString()).ToArray());
        }

        #endregion
    }
}
