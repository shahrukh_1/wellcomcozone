﻿namespace GMG.CollaborateAPI.BL.Models.Approval_Models
{
    public class RestoreApprovalResponse
    {
        #region Properties

        public string guid { get; set; }

        #endregion
    }
}
