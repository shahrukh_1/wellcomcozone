﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    [Serializable]
    public class CreateApprovalRequest : ApprovalBaseModel
    {
        #region Properties
        
        [Required]
        public string fileType { get; set; }

        public Settings settings { get; set; }

        #endregion

        #region Constructors

         public CreateApprovalRequest()
         {
             settings = new Settings();
         }

        #endregion
    }
}
