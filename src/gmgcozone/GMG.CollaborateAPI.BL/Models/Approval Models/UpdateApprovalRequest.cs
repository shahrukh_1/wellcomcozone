﻿using System.Collections.Generic;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UpdateApprovalRequest : CollaborateAPIIdentityModel
    {
        #region Properties
        
        public string deadline { get; set; }
        public List<Collaborator> collaborators { get; set; }

        #endregion

        #region Constructors

        public UpdateApprovalRequest()
        {
            collaborators = new List<Collaborator>();
        }

        #endregion
    }
}
