﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UpdateApprovalResponse
    {
        #region Properties

        public string guid { get; set; }

        public List<string> warningMessages { get; set; } 

        #endregion

        #region Constructors

        public UpdateApprovalResponse()
        {
            warningMessages =  new List<string>();
        }
        #endregion
    }
}
