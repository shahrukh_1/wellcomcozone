﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class DeleteApprovalResponse
    {
        #region Properties

        public string guid { get; set; }

        #endregion
    }
}
