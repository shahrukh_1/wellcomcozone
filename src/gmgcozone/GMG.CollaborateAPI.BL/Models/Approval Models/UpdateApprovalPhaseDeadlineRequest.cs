﻿using System.Collections.Generic;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UpdateApprovalPhaseDeadlineRequest : CollaborateAPIIdentityModel
    {
        #region Properties

        public string deadline { get; set; }
        public string approvalPhaseGuid { get; set; }
       
        #endregion
    }
}
