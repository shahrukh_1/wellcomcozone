﻿using System.ComponentModel.DataAnnotations;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models.Approval_Models
{
    public class UpdateApprovalDecisionRequest : CollaborateAPIIdentityModel
    {
        #region Properties

        [Required]
        public string decision { get; set; }

        public string email { get; set; }

        #endregion
    }
}
