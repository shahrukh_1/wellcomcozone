﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class CreateApprovalResponse
    {
        public string guid { get; set; }

        public int version { get; set; }
    }
}
