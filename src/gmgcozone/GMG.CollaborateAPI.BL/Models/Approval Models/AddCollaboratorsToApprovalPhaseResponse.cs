﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.Approval_Models
{
    public class AddCollaboratorsToApprovalPhaseResponse
    {
        public string guid { get; set; }
    }
}
