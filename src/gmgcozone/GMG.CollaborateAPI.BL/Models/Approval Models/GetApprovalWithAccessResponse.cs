﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class GetApprovalWithAccessResponse : GetApprovalResponse
    {
        #region Properties

        public List<Collaborator> collaborators { get; set; }

        public GetApprovalWithAccessResponse(GetApprovalResponse respParent)
        {
            guid = respParent.guid;
            version = respParent.version;
            filename = respParent.filename;
            thumbnail = respParent.thumbnail;
            creatorName = respParent.creatorName;
            uploadDate = respParent.uploadDate;
            deadline = respParent.deadline;
            fileSize = respParent.fileSize;
            pages = respParent.pages;
            annotationsNo = respParent.annotationsNo;
            isLocked = respParent.isLocked;
            hasProcessingErrors = respParent.hasProcessingErrors;
        }
 
        #endregion
    }
}
