﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.Approval_Models
{
    public class UpdateApprovalDecisionResponse
    {
        #region Properties

        public string guid { get; set; }

        public List<string> warningMessages { get; set; } 

        #endregion

        #region Constructors

        public UpdateApprovalDecisionResponse()
        {
            warningMessages =  new List<string>();
        }
        #endregion
    }
}
