﻿using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;
using GMGColor.Areas.CollaborateAPI.Models;
using System.Collections.Generic;

namespace GMG.CollaborateAPI.BL.Models.Approval_Models
{
    public class AddCollaboratorsToApprovalPhaseRequest : CollaborateAPIIdentityModel
    {
        public string phaseGuid { get; set; }
        public List<PhaseCollaboratorsRequest> phaseCollaborators { get; set; }
    }
}
