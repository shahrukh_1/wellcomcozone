﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UploadApprovalFileRequest : CollaborateAPIBaseModel
    {
        #region Properties

        public string resourceID { get; set; }

        #endregion
    }
}
