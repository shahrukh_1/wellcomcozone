﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.Approval_Models
{
    public class UpdateApprovalPhaseDeadlineResponse
    {
        #region Properties

        public string guid { get; set; }

        public List<string> warningMessages { get; set; }

        #endregion

        #region Constructors

        public UpdateApprovalPhaseDeadlineResponse()
        {
            warningMessages = new List<string>();
        }
        #endregion
    }
}
