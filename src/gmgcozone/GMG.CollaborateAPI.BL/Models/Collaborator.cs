﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    [Serializable]
    public class Collaborator
    {
        #region Properties
        
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string username { get; set; }
        
        //[RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email { get; set; }
        public string approvalRole { get; set; }
        public bool pdm { get; set; }
        public bool isExternal { get; set; }
        public bool toBeRemoved { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(firstname))
            {
                sb.AppendFormat("FirstName: {0}, ", firstname);
            }

            if (!string.IsNullOrEmpty(lastname))
            {
                sb.AppendFormat("LastName: {0}, ", lastname);
            }

            if (!string.IsNullOrEmpty(username))
            {
                sb.AppendFormat("UserName: {0}, ", username);
            }

            if (!string.IsNullOrEmpty(email))
            {
                sb.AppendFormat("Email: {0}, ", email);
            }

            if (!string.IsNullOrEmpty(approvalRole))
            {
                sb.AppendFormat("ApprovalRole: {0}, ", approvalRole);
            }

            sb.AppendFormat("PDM: {0}, ", pdm.ToString());

            sb.AppendFormat("IsExternal: {0}, ", isExternal.ToString());

            sb.AppendFormat("ToBeRemoved: {0}, ", toBeRemoved.ToString());

            sb.Remove(sb.Length - 2, 2);

            return sb.ToString();
        }

        #endregion
    }

    [Serializable]
    public class CollaboratorActivity
    {
        #region Properties

        public string username { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string role { get; set; }
        public string sent { get; set; }
        public string opened { get; set; }
        public int annotations { get; set; }
        public int replies { get; set; }
        public string decision { get; set; }
        public bool isExternal { get; set; }
        public bool isPDM { get; set; }

        #endregion
    }
}
