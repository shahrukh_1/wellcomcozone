﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class ConfirmIdentityRequest : CollaborateAPIBaseModel
    {
        #region Properties

        [Required]
        public string accessKey { get; set; }

        public bool ascii { get; set; }

        #endregion
    }
}
