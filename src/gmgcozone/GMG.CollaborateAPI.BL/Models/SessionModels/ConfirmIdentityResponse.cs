﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class ConfirmIdentityResponse
    {
        #region Properties

        public bool identityConfirmed { get; set; }

        #endregion
    }
}
