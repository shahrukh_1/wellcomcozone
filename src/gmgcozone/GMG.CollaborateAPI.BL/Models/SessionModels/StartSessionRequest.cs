﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace GMG.CollaborateAPI.BL.Models
{
    public class StartSessionRequest : ICollaborateAPIModel
    {
        #region Properties

        [Required]
        [StringLength(128)]
        public string username { get; set; }

        [StringLength(255)]
        public string accountURL { get; set; }

        #endregion
    }
}
