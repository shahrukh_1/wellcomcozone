﻿using System.Runtime.Serialization;

namespace GMG.CollaborateAPI.BL.Models.UserModels
{
    public class CreateUserResponse
    {
        [DataMember(Name = "guid")]
        public string Guid { get; set; }
    }
}
