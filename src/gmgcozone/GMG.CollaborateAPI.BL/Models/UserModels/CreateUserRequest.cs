﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using GMG.CoZone.Common;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models.UserModels
{
    public class CreateUserRequest : CollaborateAPIBaseModel
    {
        [DataMember(Name = "firstname")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastname")]
        public string LastName { get; set; }

        [Required]
        [DataMember(Name = "email")]
        [RegularExpression(Constants.EmailRegex, ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        public List<ModulePermission> Permissions { get; set; }
        public List<UserGroup> Groups { get; set; }

        [DataMember(Name = "password")]
        public string Password { get; set; }

        [DataMember(Name = "isssouser")]
        public bool IsSSOUser { get; set; }

        [DataMember(Name = "privateannotations")]
        public bool PrivateAnnotations { get; set; }
    }
}
