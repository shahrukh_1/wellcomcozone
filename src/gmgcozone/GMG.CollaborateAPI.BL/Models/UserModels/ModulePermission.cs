﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class ModulePermission
    {
        #region Properties

        public string name { get; set; }
        public string role { get; set; }

        #endregion
    }
}
