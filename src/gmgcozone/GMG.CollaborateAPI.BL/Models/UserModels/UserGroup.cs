﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UserGroup
    {
        #region Properties

        public string name { get; set; }
        public bool isprimary { get; set; }

        #endregion
    }
}
