﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models
{
    public class UserExistsRequest : CollaborateAPIBaseModel
    {
        #region Properties

        public string email { get; set; }

        #endregion
    }
}
