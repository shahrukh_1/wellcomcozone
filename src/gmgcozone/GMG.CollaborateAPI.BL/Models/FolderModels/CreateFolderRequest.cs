﻿using System.ComponentModel.DataAnnotations;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models.FolderModels
{
    public class CreateFolderRequest: CollaborateAPIBaseModel
    {
        #region Parameters

        public string parentGuid { get; set; }

        [Required]
        public string name { get; set; }

        #endregion
    }
}
