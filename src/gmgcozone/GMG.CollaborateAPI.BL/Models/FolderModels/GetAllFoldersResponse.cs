﻿using System.Collections.Generic;

namespace GMG.CollaborateAPI.BL.Models.FolderModels
{
    public class GetAllFoldersResponse
    {
        #region Properties

        public List<FolderResponse> folders { get; set; }

        #endregion
    }
}
