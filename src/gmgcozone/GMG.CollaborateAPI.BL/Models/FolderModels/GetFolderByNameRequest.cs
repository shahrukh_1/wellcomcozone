﻿using System.ComponentModel.DataAnnotations;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models.FolderModels
{
    public class GetFolderByNameRequest : CollaborateAPIBaseModel
    {
        [Required]
        public string name { get; set; }
    }
}
