﻿namespace GMG.CollaborateAPI.BL.Models.FolderModels
{
    public class FolderResponse : BaseFolderResponse
    {
        #region Properties

        public string name { get; set; }

        #endregion
    }
}
