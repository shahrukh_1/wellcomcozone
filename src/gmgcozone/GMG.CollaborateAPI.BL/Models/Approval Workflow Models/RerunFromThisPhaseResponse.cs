﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class RerunFromThisPhaseResponse
    {
        public string guid { get; set; }
    }
}
