﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class RerunFromThisPhaseRequest : CollaborateAPIIdentityModel
    {
        public string phaseGuid { get; set; }
    }
}
