﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class PhaseCollaboratorsRequest
    {
        public string firstName { get; set; }
        public string userName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string userRole { get; set; }
        public bool isExternal { get; set; }
        public bool toBeRemoved { get; set; }
    }
}
