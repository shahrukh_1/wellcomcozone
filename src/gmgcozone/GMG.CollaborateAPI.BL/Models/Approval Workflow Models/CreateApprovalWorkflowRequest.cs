﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class CreateApprovalWorkflowRequest : CollaborateAPIIdentityModel
    {
        public string workflowName { get; set; }
        public bool rerunWorkflow { get; set; }
        public List<PhaseRequest> worflowPhases { get; set; }
        public CreateApprovalWorkflowRequest()
        {
            worflowPhases = new List<PhaseRequest>();
        }
    }
}
