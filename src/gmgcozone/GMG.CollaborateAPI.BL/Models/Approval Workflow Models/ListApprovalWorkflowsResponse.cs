﻿using System.Collections.Generic;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Model;

namespace GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models
{
    public class ListApprovalWorkflowsResponse
    {
        #region Properties

        public List<ApprovalWorkflow> workflows { get; set; }

        #endregion
    }
}
