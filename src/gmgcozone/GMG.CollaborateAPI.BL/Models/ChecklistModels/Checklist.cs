﻿namespace GMG.CollaborateAPI.BL.Models.ChecklistModels
{
    public class Checklist
    {
        #region Properties

        public string guid { get; set; }

        public string name { get; set; }

        #endregion
    }
}
