﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CollaborateAPI.BL.Models.ChecklistModels
{
    public class ListChecklistResponse
    {
        #region Properties

        public List<Checklist> Checklist { get; set; }

        #endregion
    }
}
