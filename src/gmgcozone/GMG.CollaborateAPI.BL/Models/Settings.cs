﻿using System;

namespace GMG.CollaborateAPI.BL.Models
{
    public class Settings
    {
        #region Private properties

        private bool _allowDownload = true;

        #endregion


        #region Public properties

        public bool lockProofWhenFirstDecisionIsMade { get; set; }
        public bool lockProofWhenAllDecisionsAreMade { get; set; }
        public bool onlyOneDecisionRequired { get; set; }
        public bool allowUsersToDownloadOriginalFile
        {
            get { return _allowDownload; }
            set { _allowDownload = value; }
        }

        #endregion
    }
}
