﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL
{
    public enum ApprovalTrigger
    {
        AWC = 1,
        APD,
    }

    public enum DecisionType
    { 
        PDM = 1,
        AG
    }

    public enum UserRole
    {
        RDO = 1,
        RVW,
        ANR,
        RET
    }
}
