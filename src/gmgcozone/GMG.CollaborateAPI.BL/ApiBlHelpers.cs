﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColorBusinessLogic;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;

namespace GMG.CollaborateAPI.BL
{
    /// <summary>
    /// Static Helper class for common API BL methods
    /// </summary>
    public static class ApiBlHelpers
    {

        /// <summary>
        /// Get user object based on the provided username
        /// </summary>
        /// <param name="username"></param>
        /// <param name="sessionKey"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static GMGColorDAL.User GetUserDetails(string username, string sessionKey, DbContextBL dbContext)
        {
            return (from u in dbContext.Users
                    join us in dbContext.UserStatus on u.Status equals us.ID
                    let accountId = (from cas in dbContext.CollaborateAPISessions
                                     join usr in dbContext.Users on cas.User equals usr.ID
                                     where cas.SessionKey == sessionKey
                                     select usr.Account).FirstOrDefault()
                    where u.Account == accountId && u.Username == username && us.Key == "A"
                    select u).FirstOrDefault();
        }

        /// <summary>
        /// Checks if there are no duplicat phase names
        /// </summary>
        /// <param name="phaseList"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static bool CheckWorkflowPhaseNamesAreUnique(List<PhaseRequest>phaseList, DbContextBL dbContext)
        {
            var phaseNamesList = new List<string>();

            foreach (var phase in phaseList)
            {
                phaseNamesList.Add(phase.phaseName);
            }

            if (phaseNamesList.Count != phaseNamesList.Distinct().Count())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check if the provided phases exists and return that phase's id
        /// </summary>
        /// <param name="approvalGuid"></param>
        /// <param name="phaseGuid"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static int CheckApprovalWorkflowHasPhase(string approvalGuid, string phaseGuid, DbContextBL dbContext)
        {
            var jobWorkflow = (from a in dbContext.Approvals
                               join j in dbContext.Jobs on a.Job equals j.ID
                               join ajw in dbContext.ApprovalJobWorkflows on j.ID equals ajw.Job
                               where a.Guid == approvalGuid
                               select ajw.ID).FirstOrDefault();

            var requestedPhaseID = (from ajp in dbContext.ApprovalJobPhases
                                    join app in dbContext.ApprovalPhases on ajp.PhaseTemplateID equals app.ID
                                    where app.Guid == phaseGuid && ajp.ApprovalJobWorkflow == jobWorkflow
                                    select ajp.ID).FirstOrDefault();
            return requestedPhaseID;
        }


        /// <summary>
        /// Check if there are any duplicate collaborators
        /// </summary>
        /// <param name="collaboratorList"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        public static bool CheckDuplicateCollaboratorsExists(List<PhaseCollaboratorsRequest> collaboratorList, DbContextBL dbContext)
        {
            if (collaboratorList.Where(c => !string.IsNullOrEmpty(c.userName))
                                .GroupBy(c => c.userName.ToLower())
                                .SelectMany(grp => grp.Skip(1))
                                .Count() >= 1 ||
                collaboratorList.Where(c => !string.IsNullOrEmpty(c.email) && c.isExternal)
                                  .GroupBy(c => c.email)
                                  .SelectMany(grp => grp.Skip(1))
                                  .Count() >= 1)
            {
                return true;
            }
            return false;
        }
    }
}
