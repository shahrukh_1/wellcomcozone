﻿using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.JobModels;
using GMG.CollaborateAPI.BL.Validators;
using GMG.CollaborateAPI.BL.Validators.JobValidators;
using GMG.WebHelpers.Job;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorDAL.CustomModels.ApprovalWorkflows;
using Newtonsoft.Json;
using System.Data.Entity.Validation;
using GMGColorBusinessLogic.Common;
using System.Threading.Tasks;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class JobRepository : IRepository
    {
        #region Fields

        private DbContextBL _dbContext;

        #endregion

        #region Constructors

        public JobRepository(DbContextBL dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region Public Methods

        public object CreateJob(UploadApprovalFileRequest model, MemoryStream fileStream, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CreateApprovalRequestValidator(model, fileStream.Length);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Created;
                return CreateJob(model, fileStream);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Create Job Metadata entity and generate resource ID
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object CreateJobMetadata(CreateJobRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CreateApprovalMetadataRequestValidator(model.approval, model.workflow, model.checklist);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return CreateJobMetadata(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Create Approval Version Metadata entity and generate resource ID
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object CreateApprovalVersionMetadata(CreateApprovalRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CreateApprovalMetadataRequestValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                var jobModel = new CreateJobRequest { approval = model };
                return CreateJobMetadata(jobModel);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Deletes the specified job
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object DeleteJob(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new DeleteJobValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return DeleteJob(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get job details and its versions
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetJob(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetJobValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetJob(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Gets all jobs that logged user owns
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object ListJobs(CollaborateAPIBaseModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new ListJobsValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return ListJobs(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Update the current job
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object UpdateJob(UpdateJobRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            var validator = new UpdateJobValidator(model);
            returnStatusCode = validator.Validate(_dbContext);
            errors = string.Empty;

            if (validator.IsValid)
            {
                return UpdateJob(model, out errors);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get job status and versions activity
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetJobActivity(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetJobValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetJobActivity(model);
            }

            return validator.GetRuleViolations();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates job (if guid is not specified) and approval
        /// </summary>
        /// <param name="model"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        private CreateJobResponse CreateJob(UploadApprovalFileRequest model, MemoryStream fileStream)
        {
                //Retrieve metadata from DB
                var metadataBO = _dbContext.CollaborateAPIJobMetadatas.FirstOrDefault(t => t.ResourceID == model.resourceID.Trim());
                //Recreate job model from json
                CreateJobRequest jobMetadata = JsonConvert.DeserializeObject<CreateJobRequest>(metadataBO.Metadata);

                var response = new CreateJobResponse();
                var fileGuid = Guid.NewGuid().ToString();

                var loggedUserDetails = (from u in _dbContext.Users
                                         join us in _dbContext.UserStatus on u.Status equals us.ID
                                         let account = (from cas in _dbContext.CollaborateAPISessions
                                                        join usr in _dbContext.Users on cas.User equals usr.ID
                                                        join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                                        where cas.SessionKey == model.sessionKey
                                                        select ac).FirstOrDefault()
                                         where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                                         select new
                                         {
                                             User = u,
                                             Account = account
                                         }).FirstOrDefault();

                if (loggedUserDetails != null)
                {
                    var approvalRoles = (from ar in _dbContext.ApprovalCollaboratorRoles select ar).ToList();
                    var collaborateBillingPlan = loggedUserDetails.Account.CollaborateBillingPlan(_dbContext);

                    string codePoint = "\uFF1A";

                    if (!string.IsNullOrEmpty(jobMetadata.jobName))
                    {
                        jobMetadata.jobName = jobMetadata.jobName.Replace(":", codePoint);
                    }

                    jobMetadata.approval.fileName = jobMetadata.approval.fileName.Replace(":", codePoint);

                    var fileName = jobMetadata.approval.fileName;
                    var extension = Path.GetExtension(jobMetadata.approval.fileName);
                    if (!Path.HasExtension(jobMetadata.approval.fileName) || extension != jobMetadata.approval.fileType)
                    {
                        fileName = String.Format("{0}{1}", jobMetadata.approval.fileName, jobMetadata.approval.fileType);
                    }

                    var files = new List<ApprovalJob>
                            {
                                new ApprovalJob()
                                    {
                                        FileName = fileName,
                                        FileTitle = jobMetadata.jobName,
                                        FileSize = decimal.Parse(fileStream.Length.ToString()),
                                        FileGuid = fileGuid
                                    }
                            };
                    var folderId = (from f in _dbContext.Folders where f.Guid == jobMetadata.approval.folder select f.ID).FirstOrDefault();
                    var jobID = !string.IsNullOrEmpty(jobMetadata.approval.guid) ? (from j in _dbContext.Jobs where j.Guid == jobMetadata.approval.guid select j.ID).FirstOrDefault() : 0;

                    var dateTime = DateTime.Parse(jobMetadata.approval.deadline, null, System.Globalization.DateTimeStyles.RoundtripKind);

                    var newApproval = new NewApprovalModel()
                    {
                        Creator = loggedUserDetails.User.ID,
                        Job = jobID,
                        Owner = loggedUserDetails.User.ID,
                        Folder = folderId,
                        IsUploadApproval = true,
                        KeepOriginalFileName = string.IsNullOrEmpty(jobMetadata.jobName),
                        IsDueDateSelected = dateTime != default(DateTime),
                        ApprovalDeadlineDate = GMGColorFormatData.GetFormattedDate(dateTime, loggedUserDetails.Account.DateFormat1.Pattern),
                        DeadlineTime = loggedUserDetails.Account.TimeFormat == 1 ? dateTime.ToString("hh:mm") : dateTime.ToString("HH:mm"),
                        DeadlineTimeMeridiem = dateTime.ToString("tt", CultureInfo.InvariantCulture),
                        OnlyOneDecisionRequired = jobMetadata.approval.settings.onlyOneDecisionRequired,
                        LockProofWhenAllDecisionsMade = jobMetadata.approval.settings.lockProofWhenAllDecisionsAreMade,
                        LockProofWhenFirstDecisionsMade = jobMetadata.approval.settings.lockProofWhenFirstDecisionIsMade,
                        AllowUsersToDownload = jobMetadata.approval.settings.allowUsersToDownloadOriginalFile,
                        SelectedUsersAndGroups = GetInternalCollaborators(jobMetadata.approval.collaborators, approvalRoles, loggedUserDetails.User.ID, loggedUserDetails.Account.ID, ref response),
                        PrimaryDecisionMaker = GetPrimaryDecisionMaker(jobMetadata.approval.collaborators, loggedUserDetails.Account.ID)
                    };


                    //Remove metadata from db
                    _dbContext.CollaborateAPIJobMetadatas.Remove(metadataBO);

                    var approvalWorkflowCollaborators = new ApprovalWorkflowPhasesInfo();

                    if (jobMetadata.checklist != null)
                    {
                        newApproval.ChecklistId = GetSpecifiedChecklistId(jobMetadata.checklist);
                    }
                    // Fill workflow based on specified collaborators/phases information
                    var collaborators = GetSpecifiedWorkflowInformation(jobID, loggedUserDetails.Account.ID, jobMetadata, approvalRoles, ref newApproval, ref approvalWorkflowCollaborators, ref response, loggedUserDetails.User.ID);

                    GetExternalCollaborators(collaborators, approvalRoles, loggedUserDetails.Account.ID, ref newApproval, ref response);

                    NewApprovalResponse newApprovalResponse;
                    ApprovalBL.AddNewApproval(collaborateBillingPlan, loggedUserDetails.Account, loggedUserDetails.User, newApproval, files, null, approvalWorkflowCollaborators, _dbContext, out newApprovalResponse, true);

                    CopyFileToBucket(fileGuid, newApprovalResponse.ApprovalID, loggedUserDetails.Account.Region, fileStream, fileName, _dbContext);

                    response.guid = newApprovalResponse.JobGuid;
                    response.approval = new CreateApprovalResponse
                    {
                        guid = fileGuid,
                        version = newApprovalResponse.Version
                    };
                }
                else
                {
                    response.warningMessages.Add(String.Format(ValidationErrorMessages.UserNotFound));
                }

                return response;
           
        }

        private List<Collaborator> GetSpecifiedWorkflowInformation(int jobID, int loggedAccountId, CreateJobRequest jobMetadata, List<ApprovalCollaboratorRole> approvalRoles, ref NewApprovalModel newApproval, ref ApprovalWorkflowPhasesInfo approvalWorkflowCollaborators, ref CreateJobResponse response, int loggedUser)
        {
                var workflowId = 0;
                ApprovalWorkflowPhasesInfo approvalWorkflowPhasesInfo = new ApprovalWorkflowPhasesInfo();
                List<Collaborator> collaboratorsList = jobMetadata.approval.collaborators;

                if (!string.IsNullOrEmpty(jobMetadata.workflow))
                {
                    ApprovalWorkflow approvalWorkflow = new ApprovalWorkflow();
                    approvalWorkflow = _dbContext.ApprovalWorkflows.Where(w => w.Guid == jobMetadata.workflow).FirstOrDefault();
                    workflowId = approvalWorkflow.ID;

                    if (jobMetadata.approval.phases.Count() == 0)
                    {
                        approvalWorkflowCollaborators = ApprovalBL.GetApprovalWorkflowCollaborators(workflowId, _dbContext);
                    }
                    else
                    {
                        if (approvalWorkflow.ApprovalPhases.Count() != jobMetadata.approval.phases.Count())
                        {
                            response.warningMessages.Add(String.Format(ValidationErrorMessages.WorkflowPhasesInformationNotComplete));
                        }

                        List<ApprovalPhase> workflowPhases = _dbContext.ApprovalPhases.Where(ap => ap.ApprovalWorkflow == workflowId).ToList();
                        // Loop through template workflow phases to reflect their own phasing order
                        foreach (ApprovalPhase approvalPhase in workflowPhases)
                        {
                            Phase phase = new Phase();
                            phase = jobMetadata.approval.phases.FirstOrDefault(p => p.phaseGUID == approvalPhase.Guid);

                            ApprovalWorkflowPhase approvalWorkflowPhase = new ApprovalWorkflowPhase();
                            approvalWorkflowPhase.Phase = approvalPhase.ID;

                            ApprovalPhaseCollaboratorInfo approvalPhaseCollaboratorInfo = SetApprovalPhaseCollaboratorInfo(approvalRoles, response, phase, approvalWorkflowPhase, loggedAccountId, ref collaboratorsList, loggedUser);
                            approvalWorkflowPhase.PhaseCollaborators = approvalPhaseCollaboratorInfo;

                            approvalWorkflowPhasesInfo.ApprovalWorkflowPhases.Add(approvalWorkflowPhase);
                        }

                        approvalWorkflowCollaborators = approvalWorkflowPhasesInfo;
                    }

                    newApproval.AllowOtherUsersToBeAdded = jobMetadata.allowUsersToBeAddedOnPhase;
                }
                else
                {
                    ApprovalJobWorkflow approvalJobWorkflow = _dbContext.ApprovalJobWorkflows.Where(aw => aw.Job == jobID && aw.Account == loggedAccountId).FirstOrDefault();

                    if (approvalJobWorkflow != null)
                    {
                        var lastApprovalInJob = (from a in _dbContext.Approvals
                                                 join j in _dbContext.Jobs on a.Job equals j.ID
                                                 let collaborators = (from ac in _dbContext.ApprovalCollaborators
                                                                      where a.ID == ac.Approval
                                                                      select ac.Collaborator).ToList()
                                                 where
                                                     (collaborators.Contains(loggedUser) || j.JobOwner == loggedUser) &&
                                                     j.ID == jobID &&
                                                     j.JobStatu.Key != "ARC" &&
                                                     a.IsDeleted == false
                                                 orderby a.Version descending
                                                 select new
                                                 {
                                                     a.ID,
                                                     a.AllowOtherUsersToBeAdded,
                                                     a.CurrentPhase
                                                 }).FirstOrDefault();

                        newApproval.AllowOtherUsersToBeAdded = lastApprovalInJob.AllowOtherUsersToBeAdded;

                        if (lastApprovalInJob != null)
                        {
                            var currentJobPhasePosition = approvalJobWorkflow.RerunWorkflow ? 1 : ApprovalBL.GetCurrentJobPhasePosition(lastApprovalInJob.ID, _dbContext);

                            // Get current phase template in order to use it's GUID to match the retrieved phases
                            int currentPhaseID = lastApprovalInJob.CurrentPhase.Value;
                            var currentPhaseTemplateID = approvalJobWorkflow.ApprovalJobPhases.FirstOrDefault(ajp => ajp.ID == currentPhaseID).PhaseTemplateID;
                            ApprovalPhase currentApprovalPhase = _dbContext.ApprovalPhases.FirstOrDefault(ap => ap.ID == currentPhaseTemplateID);
                            workflowId = currentApprovalPhase.ApprovalWorkflow;

                            // Get collaborators for the current and pending phases
                            approvalWorkflowPhasesInfo = ApprovalBL.GetVersionExistingPhasesCollaborators(lastApprovalInJob.ID, approvalJobWorkflow.ID, currentJobPhasePosition, _dbContext);
                            List<ApprovalWorkflowPhase> approvalWorkflowPhases = approvalWorkflowPhasesInfo.ApprovalWorkflowPhases;

                            if (lastApprovalInJob.AllowOtherUsersToBeAdded)
                            {
                                List<ApprovalPhase> workflowPhases = _dbContext.ApprovalPhases.Where(ap => ap.ApprovalWorkflow == currentApprovalPhase.ApprovalWorkflow).ToList();
                                workflowPhases.RemoveAll(wp => wp.Position < currentJobPhasePosition);

                                int i = 0;

                                // Only phases after curent phase will be kept
                                foreach (ApprovalPhase approvalPhase in workflowPhases)
                                {
                                    Phase phase = new Phase();
                                    phase = jobMetadata.approval.phases.FirstOrDefault(p => p.phaseGUID == approvalPhase.Guid);

                                    if (phase != null)
                                    {
                                        ApprovalWorkflowPhase approvalWorkflowPhase = new ApprovalWorkflowPhase();
                                        approvalWorkflowPhase.Phase = approvalWorkflowPhases[i].Phase;

                                        ApprovalPhaseCollaboratorInfo approvalPhaseCollaboratorInfo = SetApprovalPhaseCollaboratorInfo(approvalRoles, response, phase, approvalWorkflowPhase, loggedAccountId, ref collaboratorsList, loggedUser);

                                        approvalWorkflowPhase.PhaseCollaborators = approvalPhaseCollaboratorInfo;

                                        // At update, PDM should be the one that was selected on the create
                                        approvalWorkflowPhase.PDMId = approvalWorkflowPhases[i].PDMId;
                                        approvalWorkflowPhase.IsExternal = approvalWorkflowPhases[i].IsExternal;

                                        approvalWorkflowPhasesInfo.ApprovalWorkflowPhases[i] = approvalWorkflowPhase;
                                    }
                                    else
                                    {
                                        approvalWorkflowPhasesInfo.ApprovalWorkflowPhases[i] = (approvalWorkflowPhases[i]);
                                    }

                                    i++;
                                }

                                approvalWorkflowCollaborators = approvalWorkflowPhasesInfo;
                            }
                            else
                            {
                                approvalWorkflowCollaborators = approvalWorkflowPhasesInfo;
                            }
                        }
                    }
                }

                newApproval.ApprovalWorkflowId = workflowId;
                return collaboratorsList;
        }

        private int GetSpecifiedChecklistId(string checklistGuid)
        {
            return _dbContext.CheckList.Where(ap => ap.Guid == checklistGuid).Select(x => x.ID).FirstOrDefault();
        }

        private List<Collaborator> GetSpecifiedWorkflowInformation(int jobId, int loggedAccountId, string workflowTemplateId, List<Phase> phases, List<ApprovalCollaboratorRole> approvalRoles,
            bool allowUsersToBeAddedOnPhase, ref NewApprovalModel newApproval, ref ApprovalWorkflowPhasesInfo approvalWorkflowCollaborators, ref CreateJobResponse response, List<Collaborator> collaboratorsList, int loggedUser)
        {
            var workflowId = 0;
            ApprovalWorkflowPhasesInfo approvalWorkflowPhasesInfo = new ApprovalWorkflowPhasesInfo();

            if (!string.IsNullOrEmpty(workflowTemplateId))
            {
                #region Create

                ApprovalWorkflow approvalWorkflow = new ApprovalWorkflow();
                approvalWorkflow = _dbContext.ApprovalWorkflows.Where(w => w.Guid == workflowTemplateId).FirstOrDefault();
                workflowId = approvalWorkflow.ID;

                if (phases.Count() == 0)
                {
                    approvalWorkflowCollaborators = ApprovalBL.GetApprovalWorkflowCollaborators(workflowId, _dbContext);
                }
                else
                {
                    if (approvalWorkflow.ApprovalPhases.Count() != phases.Count())
                    {
                        response.warningMessages.Add(String.Format(ValidationErrorMessages.WorkflowPhasesInformationNotComplete));
                    }

                    List<ApprovalPhase> workflowPhases = _dbContext.ApprovalPhases.Where(ap => ap.ApprovalWorkflow == workflowId).ToList();
                    // Loop through template workflow phases to reflect their own phasing order
                    foreach (ApprovalPhase approvalPhase in workflowPhases)
                    {
                        Phase phase = new Phase();
                        phase = phases.FirstOrDefault(p => p.phaseGUID == approvalPhase.Guid);

                        ApprovalWorkflowPhase approvalWorkflowPhase = new ApprovalWorkflowPhase();
                        approvalWorkflowPhase.Phase = approvalPhase.ID;

                        ApprovalPhaseCollaboratorInfo approvalPhaseCollaboratorInfo = SetApprovalPhaseCollaboratorInfo(approvalRoles, response, phase, approvalWorkflowPhase, loggedAccountId, ref collaboratorsList, loggedUser);
                        approvalWorkflowPhase.PhaseCollaborators = approvalPhaseCollaboratorInfo;

                        approvalWorkflowPhasesInfo.ApprovalWorkflowPhases.Add(approvalWorkflowPhase);
                    }

                    approvalWorkflowCollaborators = approvalWorkflowPhasesInfo;
                }

                newApproval.AllowOtherUsersToBeAdded = allowUsersToBeAddedOnPhase; 

                #endregion
            }
            else
            {
                #region Update

                ApprovalJobWorkflow approvalJobWorkflow = _dbContext.ApprovalJobWorkflows.Where(aw => aw.Job == jobId && aw.Account == loggedAccountId).FirstOrDefault();

                if (approvalJobWorkflow != null)
                {
                    Approval lastApprovalInJob = _dbContext.Approvals.Where(a => a.Job == jobId).OrderByDescending(a => a.Version).FirstOrDefault();
                    newApproval.AllowOtherUsersToBeAdded = lastApprovalInJob.AllowOtherUsersToBeAdded;

                    if (lastApprovalInJob != null)
                    {
                        var currentJobPhasePosition = ApprovalBL.GetCurrentJobPhasePosition(lastApprovalInJob.ID, _dbContext);

                        // Get current phase template in order to use it's GUID to match the retrieved phases
                        int currentPhaseID = lastApprovalInJob.CurrentPhase.Value;
                        var currentPhaseTemplateID = approvalJobWorkflow.ApprovalJobPhases.FirstOrDefault(ajp => ajp.ID == currentPhaseID).PhaseTemplateID;
                        ApprovalPhase currentApprovalPhase = _dbContext.ApprovalPhases.FirstOrDefault(ap => ap.ID == currentPhaseTemplateID);
                        workflowId = currentApprovalPhase.ApprovalWorkflow;

                        // Get collaborators for the current and pending phases
                        approvalWorkflowPhasesInfo = ApprovalBL.GetVersionExistingPhasesCollaborators(lastApprovalInJob.ID, approvalJobWorkflow.ID, currentJobPhasePosition, _dbContext);
                        List<ApprovalWorkflowPhase> approvalWorkflowPhases = approvalWorkflowPhasesInfo.ApprovalWorkflowPhases;

                        if (lastApprovalInJob.AllowOtherUsersToBeAdded)
                        {
                            List<ApprovalPhase> workflowPhases = _dbContext.ApprovalPhases.Where(ap => ap.ApprovalWorkflow == currentApprovalPhase.ApprovalWorkflow).ToList();
                            workflowPhases.RemoveAll(wp => wp.Position < currentJobPhasePosition);

                            int i = 0;

                            // Only phases after curent phase will be kept
                            foreach (ApprovalPhase approvalPhase in workflowPhases)
                            {
                                Phase phase = new Phase();
                                phase = phases.FirstOrDefault(p => p.phaseGUID == approvalPhase.Guid);

                                if (phase != null)
                                {
                                    #region Modified input phases

                                    ApprovalWorkflowPhase approvalWorkflowPhase = new ApprovalWorkflowPhase();
                                    approvalWorkflowPhase.Phase = approvalWorkflowPhases[i].Phase;

                                    ApprovalPhaseCollaboratorInfo approvalPhaseCollaboratorInfo = SetApprovalPhaseCollaboratorInfo(approvalRoles, response, phase, approvalWorkflowPhase, loggedAccountId, ref collaboratorsList, loggedUser);

                                    approvalWorkflowPhase.PhaseCollaborators = approvalPhaseCollaboratorInfo;

                                    // At update, PDM should be the one that was selected on the create
                                    approvalWorkflowPhase.PDMId = approvalWorkflowPhases[i].PDMId;
                                    approvalWorkflowPhase.IsExternal = approvalWorkflowPhases[i].IsExternal;

                                    approvalWorkflowPhasesInfo.ApprovalWorkflowPhases[i] = approvalWorkflowPhase;

                                    #endregion
                                }
                                else
                                {
                                    #region Copy existing workflow information

                                    approvalWorkflowPhasesInfo.ApprovalWorkflowPhases[i] = (approvalWorkflowPhases[i]);

                                    #endregion
                                }

                                i++;
                            }

                            approvalWorkflowCollaborators = approvalWorkflowPhasesInfo;
                        }
                        else
                        {
                            approvalWorkflowCollaborators = approvalWorkflowPhasesInfo;
                        }
                    }
                }

                #endregion
            }

            newApproval.ApprovalWorkflowId = workflowId;
            return collaboratorsList;
        }

        private ApprovalPhaseCollaboratorInfo SetApprovalPhaseCollaboratorInfo(List<ApprovalCollaboratorRole> approvalRoles, CreateJobResponse response, Phase phase, ApprovalWorkflowPhase approvalWorkflowPhase, int loggedAccountId, ref List<Collaborator> collaboratorsList, int loggedUser)
        {
            var approvalPhaseCollaboratorInfo = new ApprovalPhaseCollaboratorInfo
            {
                CollaboratorsWithRole = new List<string>(),
                ExternalCollaboratorsWithRole = new List<string>(),
                Groups = new List<int>()
            };

            var isExternal = false;
            int? pdmId = null;

            foreach (Collaborator collaborator in phase.collaborators)
            {
                var appRole =
                    approvalRoles.Where(r =>
                        r.Key.ToUpper() == collaborator.approvalRole)
                        .Select(r => r.ID)
                        .FirstOrDefault();

                if (!collaborator.isExternal)
                {
                    var user = (from u in _dbContext.Users
                                join us in _dbContext.UserStatus on u.Status equals us.ID
                                where u.Username == collaborator.username && u.Account == loggedAccountId && us.Key == "A"
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        if (collaborator.pdm)
                        {
                            pdmId = user.ID;
                        }

                        approvalPhaseCollaboratorInfo.CollaboratorsWithRole.Add(user.ID + "|" + appRole);

                        if (collaborator.pdm)
                        {
                            approvalWorkflowPhase.PDMId = user.ID;
                            approvalWorkflowPhase.IsExternal = false;
                        }
                    }
                    else
                    {
                        response.warningMessages.Add(String.Format(WarningMessages.InvalidUsernameInternalUser,
                                                                    collaborator.username));
                    }
                }

                if (collaborator.isExternal)
                {
                    var externalCollaboratorId = (from ec in _dbContext.ExternalCollaborators
                                        where ec.EmailAddress.Trim().ToLower() == collaborator.email.Trim().ToLower() && ec.IsDeleted == false && ec.Account == loggedAccountId
                                        select ec.ID).FirstOrDefault();

                    if (externalCollaboratorId == 0)
                    {                   
                       externalCollaboratorId = SaveExternalCollaborator(collaborator, appRole, loggedAccountId, loggedUser);
                       collaboratorsList.Where(c => c.approvalRole.Trim().ToLower() == collaborator.approvalRole.Trim().ToLower() && c.email.Trim().ToLower() == collaborator.email.Trim().ToLower() && c.isExternal);
                    }

                    if (collaborator.pdm)
                    {
                        pdmId = externalCollaboratorId;
                        isExternal = true;
                    }

                    approvalPhaseCollaboratorInfo.ExternalCollaboratorsWithRole.Add(externalCollaboratorId + "|" + appRole);

                    if (collaborator.pdm)
                    {
                        approvalWorkflowPhase.PDMId = externalCollaboratorId;
                        approvalWorkflowPhase.IsExternal = true;
                    }
                }
            }

            approvalWorkflowPhase.PDMId = pdmId;
            approvalWorkflowPhase.IsExternal = isExternal;

            return approvalPhaseCollaboratorInfo;
        }

        private int SaveExternalCollaborator(Collaborator externalCollaborator, int approvalRole,int loggedAccountId, int loggedUser)
        {            
            approvalRole = approvalRole > 0 ? approvalRole : (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;

            var firstName = string.Empty;
            var lastName = string.Empty;
            if (!string.IsNullOrEmpty(externalCollaborator.firstname) && !string.IsNullOrEmpty(externalCollaborator.lastname))
            {
                firstName = externalCollaborator.firstname;
                lastName = externalCollaborator.lastname;
            }
            else
            {
               firstName = externalCollaborator.email.ToLower();
               lastName =  ".";
            }
            
            var userLanguage = _dbContext.Accounts.Where(a => a.ID == loggedAccountId).Select(a => a.Locale).FirstOrDefault();

            var avoidedColors = new List<string>();
            var extCollaborator = new ExternalCollaborator()
            {
                GivenName = firstName,
                FamilyName = lastName,
                Locale = userLanguage,
                EmailAddress = externalCollaborator.email.Trim().ToLower(),
                Guid = Guid.NewGuid().ToString(),
                Creator = loggedUser,
                Account = loggedAccountId,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow,
                ProofStudioColor = ExternalCollaboratorBL.GenerateCollaboratorProofStudioColor(loggedAccountId, new List<string>(), _dbContext),
                Modifier = loggedUser
            };
                     
            _dbContext.ExternalCollaborators.Add(extCollaborator);
            _dbContext.SaveChanges();

            return extCollaborator.ID;
        }

        private CreateJobMetadataResponse CreateJobMetadata(CreateJobRequest model)
        {
            var serializedRequest = JsonConvert.SerializeObject(model);

            var newJobMetadata = new CollaborateAPIJobMetadata
            {
                Metadata = serializedRequest,
                CreatedDate = DateTime.UtcNow,
                ResourceID = Guid.NewGuid().ToString()
            };

            _dbContext.CollaborateAPIJobMetadatas.Add(newJobMetadata);
            _dbContext.SaveChanges();

            return new CreateJobMetadataResponse()
            {
                resourceID = newJobMetadata.ResourceID
            };
        }

        /// <summary>
        /// Retruns internal/external primary decision maker, if any
        /// </summary>
        /// <param name="collaborators">All collaboratos from reqest</param>
        /// <param name="accounId">Logged account id</param>
        /// <returns></returns>
        private string GetPrimaryDecisionMaker(List<Collaborator> collaborators, int accounId)
        {
            var pdmUser = string.Empty;
            var pdm = collaborators.FirstOrDefault(c => c.pdm && c.approvalRole == "ANR");
            if (pdm != null)
            {
                if (pdm.isExternal)
                {
                    var externalUser = (from ex in _dbContext.ExternalCollaborators
                                        where ex.EmailAddress.ToLower() == pdm.email.ToLower() && ex.Account == accounId && ex.IsDeleted == false
                                        select ex.EmailAddress.ToLower()).FirstOrDefault();

                    // In case of newly added external user
                    if (externalUser == null && !string.IsNullOrEmpty(pdm.firstname) && !string.IsNullOrEmpty(pdm.lastname))
                    {
                        pdmUser = pdm.email;
                    }

                    if (externalUser != null)
                        pdmUser = externalUser;
                }
                else
                {
                    pdmUser = (from u in _dbContext.Users
                               join us in _dbContext.UserStatus on u.Status equals us.ID
                               where
                                    u.Username.ToLower() == pdm.username.ToLower() &&
                                    u.Account == accounId && us.Key == "A"
                               select u.ID).FirstOrDefault().ToString();
                }
            }
            return pdmUser;
        }

        /// <summary>
        /// Get all internal collaborators for the current approval
        /// </summary>
        /// <param name="collaborators">Job collaborators</param>
        /// <param name="approvalRoles">All approval roles</param>
        /// <param name="loggedUserId">The id of the logged user</param>
        /// <param name="loggedAccountId">The id of the logged account</param>
        /// <param name="response">The response model</param>
        /// <returns></returns>
        private List<PermissionsModel.UserOrUserGroup> GetInternalCollaborators(List<Collaborator> collaborators, List<ApprovalCollaboratorRole> approvalRoles, int loggedUserId, int loggedAccountId, ref CreateJobResponse response)
        {
            var users = new List<PermissionsModel.UserOrUserGroup>();

            var internalCollaborators = collaborators.Where(c => !c.isExternal).ToList();
            foreach (var internalUser in internalCollaborators)
            {
                var user = (from u in _dbContext.Users
                            join us in _dbContext.UserStatus on u.Status equals us.ID
                            where u.Username == internalUser.username && u.Account == loggedAccountId && us.Key == "A"
                            select u).FirstOrDefault();

                if (user != null)
                {
                    var approvalRole = 0;
                    if (!string.IsNullOrEmpty(internalUser.approvalRole))
                    {
                        if (
                            approvalRoles.Any(
                                ar => ar.Key.ToLower().Trim() == internalUser.approvalRole.ToLower().Trim()))
                        {
                            approvalRole =
                                approvalRoles.Where(
                                    r => r.Key.ToLower().Trim() == internalUser.approvalRole.ToLower().Trim())
                                                .Select(r => r.ID)
                                                .FirstOrDefault();
                        }
                        else
                        {
                            approvalRole = ApprovalCollaboratorRole.GetCollaborateDefaultRoleId(user.ID, _dbContext);
                            var defaultRole =
                                approvalRoles.Where(r => r.ID == approvalRole).Select(r => r.Name).FirstOrDefault();

                            response.warningMessages.Add(String.Format(WarningMessages.TypoInApprovalRoleName,
                                                                        internalUser.username, defaultRole));
                        }
                    }
                    else
                    {
                        approvalRole = ApprovalCollaboratorRole.GetCollaborateDefaultRoleId(user.ID, _dbContext);
                    }

                    var userPermissions = new PermissionsModel.UserOrUserGroup()
                        {
                            ID = user.ID,
                            IsChecked = true,
                            IsExternal = false,
                            ApprovalRole = approvalRole

                        };
                    users.Add(userPermissions);
                }
                else
                {
                    response.warningMessages.Add(String.Format(WarningMessages.UserDoesNotExist,
                                                                internalUser.username));
                }
            }

            //if no collaborators were added for this approval, then add the logged user (the owner of the job) as collaborator
            if (collaborators.Count == 0 || users.Count == 0 || users.All(u => u.ID != loggedUserId))
            {
                var appRole =
                approvalRoles.Where(
                    r =>
                    r.Key.ToUpper() == "ANR")
                                .Select(r => r.ID)
                                .FirstOrDefault();

                var userPermissions = new PermissionsModel.UserOrUserGroup()
                {
                    ID = loggedUserId,
                    IsChecked = true,
                    IsExternal = false,
                    ApprovalRole = appRole
                };
                users.Add(userPermissions);
            }

            return users;
        }

        /// <summary>
        /// Return all external users in a formated string
        /// </summary>
        /// <param name="collaborators">Job collaborators</param>
        /// <param name="approvalRoles">All approval roles</param>
        /// <param name="loggedAccountId">The id of the logged account</param>
        /// <param name="response">The response model</param>
        /// <returns></returns>
        private void GetExternalCollaborators(List<Collaborator> collaborators, List<ApprovalCollaboratorRole> approvalRoles, int loggedAccountId, ref NewApprovalModel newApproval, ref CreateJobResponse response)
        {
                List<string> externalUsers = new List<string>();
                List<string> newExternalUsers = new List<string>();

                var externalCollaborators = collaborators.Where(c => c.isExternal);
                foreach (var externalCollaborator in externalCollaborators)
                {
                    var user = (from ec in _dbContext.ExternalCollaborators
                                where ec.EmailAddress.Trim().ToLower() == externalCollaborator.email.Trim().ToLower() && ec.IsDeleted == false && ec.Account == loggedAccountId
                                select ec).FirstOrDefault();

                    if (user != null)
                    {
                        var approvalRole = 0;
                        if (!string.IsNullOrEmpty(externalCollaborator.approvalRole))
                        {
                            if (approvalRoles.Any(ar => ar.Key.ToLower().Trim() == externalCollaborator.approvalRole.ToLower().Trim()))
                            {
                                approvalRole = approvalRoles.Where(r => r.Key.ToLower().Trim() == externalCollaborator.approvalRole.ToLower().Trim())
                                                             .Select(r => r.ID)
                                                             .FirstOrDefault();
                            }
                            else
                            {
                                approvalRole = (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;
                                var defaultRole = approvalRoles.Where(r => r.ID == approvalRole)
                                                               .Select(r => r.Name)
                                                               .FirstOrDefault();

                                response.warningMessages.Add(String.Format(WarningMessages.TypoInApprovalRoleName, externalCollaborator.email, defaultRole));
                            }
                        }
                        approvalRole = approvalRole > 0 ? approvalRole : (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;
                        externalUsers.Add(String.Format("{0}|{1}", user.ID, approvalRole));

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(externalCollaborator.email))
                        {
                            var approvalRole = approvalRoles.Where(r => r.Key.ToLower().Trim() == externalCollaborator.approvalRole.ToLower().Trim())
                                                                                    .Select(r => r.ID)
                                                                                    .FirstOrDefault();
                            approvalRole = approvalRole > 0 ? approvalRole : (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;

                            var firstName = string.Empty;
                            var lastName = string.Empty;
                            if (!string.IsNullOrEmpty(externalCollaborator.firstname) && !string.IsNullOrEmpty(externalCollaborator.lastname))
                            {
                                firstName = externalCollaborator.firstname;
                                lastName = externalCollaborator.lastname;
                            }
                            else
                            {
                                firstName = externalCollaborator.email.ToLower();
                                lastName = ".";
                            }

                            var userLanguage = _dbContext.Accounts.Where(a => a.ID == loggedAccountId).Select(a => a.Locale).FirstOrDefault();
                            var fullname = externalCollaborator.firstname + " " + externalCollaborator.lastname;
                            newExternalUsers.Add(String.Format("{0}|{1}|{2}|{3}", externalCollaborator.email, approvalRole, fullname, userLanguage));
                        }
                        else
                        {
                            response.warningMessages.Add(String.Format(WarningMessages.UserDoesNotExist, externalCollaborator.email));
                        }
                    }
                }

                newApproval.ExternalUsers = string.Join(",", externalUsers.ToArray());
                newApproval.ExternalEmails = string.Join(",", newExternalUsers.ToArray());
        }

        /// <summary>
        /// Copy the file to S3 bucket and send a message to queue for the current job
        /// </summary>
        /// <param name="fileGuid">File guid</param>
        /// <param name="approvalID">Approval id</param>
        /// <param name="userId">Logged user id</param>
        /// <param name="stream">File content</param>
        /// <param name="jobFileName">File name</param>
        /// <param name="_dbContext">Database context</param>
        private void CopyFileToBucket(string fileGuid, int approvalID, string accountRegion, Stream stream, string jobFileName, DbContextBL context)
        {
                string relativeFilePath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + fileGuid + "/";
                if (!GMGColorIO.FolderExists(relativeFilePath, accountRegion, true))
                {
                    throw new Exception("Could not create folder structure for " + relativeFilePath);
                }

                // calling file scanner to check the uploaded file stream for virus and malicious mime type
                GMG.CoZone.ScanFile.ScanFile fileScanner = new GMG.CoZone.ScanFile.ScanFile();
                if (!jobFileName.Contains(".zip"))
                {
                    bool IsCleanFile = fileScanner.ProcessFileScanner(stream, jobFileName);
                    if (!IsCleanFile)
                        throw new Exception("Issue in file scanner");
                }
                string destinationFilePath = relativeFilePath + jobFileName;

                if (jobFileName.Contains(".zip"))
                {
                    stream.Position = 0;
                    List<AssetFile> approvalFiles = new List<AssetFile>();
                    MemoryStream destination = new MemoryStream();
                    stream.CopyTo(destination);
                    approvalFiles.AddRange(GMGColorBusinessLogic.Common.Utils.UnzipStreamRootFolder(destination, jobFileName.Replace(".zip", "")));
                    approvalFiles.AddRange(GMGColorBusinessLogic.Common.Utils.UnzipStreamSubFolders(stream, jobFileName.Replace(".zip", "")));

                    var listoftasks = new List<Task>();
                    foreach (var file in approvalFiles)
                    {
                        listoftasks.Add(Task.Factory.StartNew(() =>
                       GMGColorIO.UploadZipFile(file.Stream, relativeFilePath + file.Name, accountRegion, false)));
                    }
                    Task.WhenAll(listoftasks).Wait();
                    System.Threading.Thread.Sleep(2500);
                    GetHtmlFileApprovalPage(destinationFilePath, accountRegion, approvalID, context);
                }
                else if (!GMGColorIO.MoveStream(stream, destinationFilePath, accountRegion))
                {
                    throw new Exception("File could not be uploaded to " + destinationFilePath);
                }


                #region Send message to queue for the current job

                int mediaServiceId = (from app in _dbContext.Approvals
                                      join j in _dbContext.Jobs on app.Job equals j.ID
                                      join acc in _dbContext.Accounts on j.Account equals acc.ID
                                      join asp in _dbContext.AccountSubscriptionPlans on acc.ID equals asp.Account
                                      join bp in _dbContext.BillingPlans on asp.SelectedBillingPlan equals bp.ID
                                      join bpt in _dbContext.BillingPlanTypes on bp.Type equals bpt.ID
                                      join ms in _dbContext.MediaServices on bpt.MediaService equals ms.ID
                                      select ms.ID).FirstOrDefault();
                if (mediaServiceId == 0)
                {
                    mediaServiceId = (from app in _dbContext.Approvals
                                      join j in _dbContext.Jobs on app.Job equals j.ID
                                      join acc in _dbContext.Accounts on j.Account equals acc.ID
                                      join spi in _dbContext.SubscriberPlanInfoes on acc.ID equals spi.Account
                                      join bp in _dbContext.BillingPlans on spi.SelectedBillingPlan equals bp.ID
                                      join bpt in _dbContext.BillingPlanTypes on bp.Type equals bpt.ID
                                      join ms in _dbContext.MediaServices on bpt.MediaService equals ms.ID
                                      select ms.ID).FirstOrDefault();
                }

                var approvalType = (Approval.ApprovalTypeEnum)((from at in _dbContext.ApprovalTypes
                                                                join a in _dbContext.Approvals on at.ID equals a.Type
                                                                where
                                                                    a.ID == approvalID
                                                                select at.Key).FirstOrDefault());

                var dictJobParameters = new Dictionary<string, string>();
                dictJobParameters.Add(GMG.CoZone.Common.Constants.ApprovalMsgApprovalType, Convert.ToString(approvalType));
                dictJobParameters.Add(GMG.CoZone.Common.Constants.ApprovalMsgApprovalID, Convert.ToString(approvalID));
                dictJobParameters.Add(GMG.CoZone.Common.Constants.JobMessageType, GMGColorCommon.JobType.ApprovalJob.ToString());

                foreach (KeyValuePair<string, string> item in FileProcessorBL.ConstructPdfToolboxParametersForSQSMessage(mediaServiceId, _dbContext))
                {
                    dictJobParameters.Add(item.Key, item.Value);
                }

                GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.ApprovalJob);
            #endregion
        }

        private bool GetHtmlFileApprovalPage(string destinationFilePath, string accountRegion, int approvalID, DbContextBL context)
        {
            List<string> lsthtmlfiles = new List<string>();
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                lsthtmlfiles = GMGColorIO.GetObjects(GMGColorConfiguration.AppConfiguration.HtmlDataFolderName(accountRegion),
                                       GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                       destinationFilePath.Replace(".zip", ""), accountRegion);
            }
            else
            {
                lsthtmlfiles = System.IO.Directory.GetFiles(GMGColorConfiguration.AppConfiguration.PathToDataFolder(accountRegion) + destinationFilePath.Replace(".zip", ""),
                    "*.html", SearchOption.AllDirectories).Where(x => !x.Contains("__MACOSX")).Select(x => x).ToList();
            }
            List<GMGColorDAL.ApprovalPage> approvalHtmlPageList = new List<GMGColorDAL.ApprovalPage>();
            int cnt = 1;
            foreach (var obj in lsthtmlfiles)
            {
                var lstPageDimensions = ManualExtractHtmlDimensions(obj);

                GMGColorDAL.ApprovalPage objApprovalPage = new GMGColorDAL.ApprovalPage();
                objApprovalPage.Number = cnt++;
                objApprovalPage.Approval = approvalID;
                objApprovalPage.DPI = 0;
                objApprovalPage.PageSmallThumbnailHeight = 0;
                objApprovalPage.PageSmallThumbnailWidth = 0;
                objApprovalPage.PageLargeThumbnailHeight = 0;
                objApprovalPage.PageLargeThumbnailWidth = 0;
                objApprovalPage.Progress = 100;
                objApprovalPage.HTMLFilePath = obj.Substring(obj.LastIndexOf("approvals"), (obj.Length - (obj.LastIndexOf("approvals"))));
                objApprovalPage.OriginalImageWidth = lstPageDimensions[0];
                objApprovalPage.OriginalImageHeight = lstPageDimensions[1];

                approvalHtmlPageList.Add(objApprovalPage);
            }
            context.ApprovalPages.AddRange(approvalHtmlPageList);
            context.SaveChanges();
            return true;
        }

        private DeleteJobResponse DeleteJob(CollaborateAPIIdentityModel model)
        {
            //get user rights info
            var ownerInfo = UserBL.GetJobOwnerInfo(_dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == model.sessionKey).User, _dbContext);

            var loggedUser = (from u in _dbContext.Users
                              join us in _dbContext.UserStatus on u.Status equals us.ID
                              let account = (from cas in _dbContext.CollaborateAPISessions
                                             join usr in _dbContext.Users on cas.User equals usr.ID
                                             join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                             where cas.SessionKey == model.sessionKey
                                             select ac).FirstOrDefault()
                              where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                              select u).FirstOrDefault();

            //get last version that is not deleted for current job
            var versions = (from ap in _dbContext.Approvals
                            join j in _dbContext.Jobs on ap.Job equals j.ID
                            join js in _dbContext.JobStatus on j.Status equals js.ID
                            where j.Guid == model.guid && ap.IsDeleted == false
                            select new
                            {
                                ap.ID,
                                isOwner = ap.Owner == loggedUser.ID,
                                version = ap.Version,
                                jobStatus = js.Key,
                                owner = ap.Owner,
                                ap.IsError
                            }).ToList();

            foreach (var vers in versions)
            {
                if (vers.isOwner || ownerInfo.CanViewAllFiles)
                {
                    if (JobStatu.GetJobStatus(vers.jobStatus) == JobStatu.Status.Archived || vers.IsError)
                    {
                        //delete from database
                        Approval.MarkApprovalForPermanentDeletion(vers.ID, _dbContext);
                    }
                    else
                    {
                        var currObj = _dbContext.Approvals.FirstOrDefault(o => o.ID == vers.ID);
                        currObj.IsDeleted = true;
                        currObj.ModifiedDate = DateTime.UtcNow;

                        ApprovalBL.ApprovalUserRecycleBinHistory(currObj, vers.owner, true, _dbContext);
                    }
                }
            }

            //commit changes to db
            _dbContext.SaveChanges();

            return new DeleteJobResponse { guid = model.guid };
        }

        /// <summary>
        /// Returns the job details and all its versions
        /// </summary>
        /// <param name="model">The request model</param>
        /// <returns></returns>
        private object GetJob(CollaborateAPIIdentityModel model)
        {
            var loggedUser = (from u in _dbContext.Users
                              join us in _dbContext.UserStatus on u.Status equals us.ID
                              let account = (from cas in _dbContext.CollaborateAPISessions
                                             join usr in _dbContext.Users on cas.User equals usr.ID
                                             join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                             where cas.SessionKey == model.sessionKey
                                             select ac).FirstOrDefault()
                              where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                              select new
                              {
                                  u.ID,
                                  u.Guid,
                                  LoggedAccount = account
                              }).FirstOrDefault();

            var jobInfo = (from j in _dbContext.Jobs
                           join js in _dbContext.JobStatus on j.Status equals js.ID

                           let jobFolder = (from f in _dbContext.Folders
                                            let approval = (from ap in _dbContext.Approvals
                                                            join job in _dbContext.Jobs on ap.Job equals job.ID
                                                            where job.Guid == model.guid
                                                            select ap).FirstOrDefault()
                                            where f.Approvals.Contains(approval)
                                            select f.Guid
                                            ).FirstOrDefault()

                           let jobVersions = (from a in _dbContext.Approvals
                                              join apt in _dbContext.ApprovalTypes on a.Type equals apt.ID
                                              join jj in _dbContext.Jobs on a.Job equals jj.ID

                                              let isCollaborator = (from acl in _dbContext.ApprovalCollaborators
                                                                    where a.ID == acl.Approval && acl.Collaborator == loggedUser.ID
                                                                    select acl.ID).Any()

                                              let pagesCount = (from ap in _dbContext.ApprovalPages
                                                                where ap.Approval == a.ID
                                                                select ap.ID).Count()
                                              let comments = (from an in _dbContext.ApprovalAnnotations
                                                              join page in _dbContext.ApprovalPages on an.Page equals page.ID
                                                              where
                                                                  page.Approval == a.ID && an.Parent == null &&
                                                                  a.CurrentPhase == an.Phase
                                                              select an.ID).Count()
                                              where jj.Guid == model.guid && a.IsDeleted == false && a.DeletePermanently == false && (isCollaborator || loggedUser.ID == j.JobOwner)

                                              select new
                                                  {
                                                      id = a.ID,
                                                      guid = a.Guid,
                                                      version = a.Version,
                                                      fileName = a.FileName,
                                                      creatorGuid = loggedUser.Guid,
                                                      uploadDate = a.CreatedDate,
                                                      deadline = a.Deadline,
                                                      pages = pagesCount,
                                                      annotationsNo = comments,
                                                      locked = a.IsLocked,
                                                      fileSize = SqlFunctions.StringConvert(a.Size),
                                                      fileType = apt.Key
                                                  }).Distinct().ToList()
                           where j.Guid == model.guid
                           select new
                               {
                                   jobGuid = model.guid,
                                   jobName = j.Title,
                                   jobStatus = js.Name,
                                   folder = jobFolder,
                                   approvals = jobVersions
                               }).FirstOrDefault();

            var approvals = jobInfo.approvals.Select(approval => new ApprovalDetails()
                {
                    fileSize = GMGColorFormatData.GetFileSizeString(Convert.ToDecimal(approval.fileSize)),
                    thumbnail = GetBase64Image(approval.guid, approval.fileType, approval.fileName, loggedUser.LoggedAccount),
                    guid = approval.guid,
                    version = approval.version,
                    fileName = approval.fileName,
                    creatorGuid = Guid.Parse(approval.creatorGuid),
                    uploadDate = approval.uploadDate,
                    deadline = approval.deadline,
                    pages = approval.pages,
                    annotationsNo = approval.annotationsNo,
                    locked = approval.locked
                }).ToList();

            return new GetJobResponse()
                {
                    jobTitle = jobInfo.jobName,
                    jobGuid = jobInfo.jobGuid,
                    jobStatus = jobInfo.jobStatus,
                    folder = jobInfo.folder,
                    approvals = approvals
                };
        }

        private string GetBase64Image(string approvalGuid, int fileType, string fileName, Account loggedAccount)
        {
            bool isVideoFile = fileType == (int)Approval.ApprovalTypeEnum.Movie;
            string thumbnailUrl = isVideoFile
                ? Approval.GetVideoImagePath(approvalGuid, loggedAccount.Region, loggedAccount.Domain)
                : Approval.GetApprovalImage(loggedAccount.Domain, loggedAccount.Region, approvalGuid,
                    fileName, Approval.ImageType.Thumbnail);
            string thumbnailBase64;

            using (var client = new WebClient())
            {
                byte[] thumbnailData = client.DownloadData(thumbnailUrl);
                thumbnailBase64 = Convert.ToBase64String(thumbnailData);
            }
            return thumbnailBase64;
        }

        /// <summary>
        /// Returns a list with all jobs for the logged user
        /// </summary>
        /// <param name="model">The request model</param>
        /// <returns></returns>
        private object ListJobs(CollaborateAPIBaseModel model)
        {
            var loggedUserId = (from u in _dbContext.Users
                                join us in _dbContext.UserStatus on u.Status equals us.ID
                                let accountId = (from cas in _dbContext.CollaborateAPISessions
                                                 join usr in _dbContext.Users on cas.User equals usr.ID
                                                 where cas.SessionKey == model.sessionKey
                                                 select usr.Account).FirstOrDefault()
                                where u.Account == accountId && u.Username == model.username && us.Key == "A"
                                select u.ID).FirstOrDefault();

            var jobs = (from j in _dbContext.Jobs
                        join js in _dbContext.JobStatus on j.Status equals js.ID
                        join a in _dbContext.Approvals on j.ID equals a.Job

                        let isCollaborator = (from acl in _dbContext.ApprovalCollaborators
                                              where a.ID == acl.Approval && acl.Collaborator == loggedUserId
                                              select acl.ID).Any()

                        let jobFolder = (from f in _dbContext.Folders
                                         let approval = (from ap in _dbContext.Approvals
                                                         join job in _dbContext.Jobs on ap.Job equals job.ID
                                                         where job.Guid == j.Guid
                                                         select ap).FirstOrDefault()
                                         where f.Approvals.Contains(approval)
                                         select f.Guid
                                           ).FirstOrDefault()
                        where (isCollaborator || loggedUserId == j.JobOwner) && a.IsDeleted == false && a.DeletePermanently == false
                        select new
                            {
                                jobGuid = j.Guid,
                                jobName = j.Title,
                                jobStatus = js.Name,
                                folder = jobFolder
                            }).Distinct().ToList();

            var response = new ListJobsResponse() { jobsCount = jobs.Count };

            foreach (var job in jobs)
            {
                response.jobs.Add(new JobDetails()
                    {
                        jobGuid = job.jobGuid,
                        jobTitle = job.jobName,
                        jobStatus = job.jobStatus,
                        folder = job.folder
                    });
            }

            return response;
        }

        /// <summary>
        /// Updates the current job
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private UpdateJobResponse UpdateJob(UpdateJobRequest model, out string errors)
        {
            var job = _dbContext.Jobs.FirstOrDefault(t => t.Guid == model.guid);

            if (!String.IsNullOrEmpty(model.jobTitle))
            {
                job.Title = model.jobTitle;
            }

            JobStatu.Status newStatus = JobStatu.Status.ChangesRequired;
            bool validStatus = true;
            errors = string.Empty;

            if (!String.IsNullOrEmpty(model.jobStatus))
            {
                switch (model.jobStatus.ToUpper())
                {
                    case "CRQ":
                    case "INP":
                        newStatus = JobStatu.Status.ChangesRequired;
                        break;
                    case "CCM":
                        newStatus = JobStatu.Status.ChangesComplete;
                        break;
                    case "COM":
                        newStatus = JobStatu.Status.Completed;
                        break;
                    case "ARC":
                        newStatus = JobStatu.Status.Archived;
                        break;
                    default:
                        validStatus = false;
                        break;
                }
                if (validStatus)
                {
                    job.Status = JobStatu.GetJobStatusId(newStatus, _dbContext);
                }
            }

            _dbContext.SaveChanges();

            if (validStatus)
            {
                try
                {
                    var jobStatus = (from js in _dbContext.JobStatus where js.ID == job.Status select js).FirstOrDefault();
                    if (jobStatus != null)
                    {
                        var loggedUser = (from u in _dbContext.Users
                                          join us in _dbContext.UserStatus on u.Status equals us.ID
                                          let accountId = (from cas in _dbContext.CollaborateAPISessions
                                                           join usr in _dbContext.Users on cas.User equals usr.ID
                                                           where cas.SessionKey == model.sessionKey
                                                           select usr.Account).FirstOrDefault()
                                          where u.Account == accountId && u.Username == model.username && us.Key == "A"
                                          select new UserDetails()
                                          {
                                              Username = u.Username,
                                              Email = u.EmailAddress
                                          }).FirstOrDefault();

                        var jobDetails = new JobStatusDetails()
                        {
                            jobStatusKey = jobStatus.Key,
                            jobGuid = job.Guid,
                            jobStatus = jobStatus.Name
                        };

                        ApprovalBL.PushStatus(loggedUser, jobDetails, job.Account, _dbContext);
                    }
                }
                catch (Exception ex)
                {
                    errors =
                        String.Format(
                            "Push approval status failed in UpdateApprovalVersion, with error:{0}",
                            ex);
                }
            }

            return new UpdateJobResponse { jobGuid = model.guid };
        }

        /// <summary>
        /// Get job status and versions activity
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        private GetJobActivityResponse GetJobActivity(CollaborateAPIIdentityModel model)
        {
            var appId = (from ap in _dbContext.Approvals
                         join j in _dbContext.Jobs on ap.Job equals j.ID
                         join js in _dbContext.JobStatus on j.Status equals js.ID
                         where j.Guid == model.guid
                         select new
                        {
                            ap.ID,
                            Status = js.Name
                        }).ToList();

            ApprovalRepository approvalRepository = new ApprovalRepository(_dbContext);

            GetJobActivityResponse response = new GetJobActivityResponse()
            {
                approvals =
                    approvalRepository.GetApprovalActivityModel(appId.Select(t => t.ID).ToList(), model),
                guid = model.guid,
                jobStatus = appId.FirstOrDefault().Status
            };

            return response;
        }

        public List<int> ManualExtractHtmlDimensions(string txtUrlPath)
        {
            List<int> htmldimensions = new List<int>();
            try
            {
                string content = "";
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    content = client.DownloadString(txtUrlPath);
                }
                string htmlwidth = "", htmlheight = "";
                var lstwidth = System.Text.RegularExpressions.Regex.Matches(content, @"width:(.*?)px");
                var lstheight = System.Text.RegularExpressions.Regex.Matches(content, @"height:(.*?)px");

                if (lstwidth.Count == 0)
                {
                    var getwidth = System.Text.RegularExpressions.Regex.Matches(content, @"content=""width=(.*?),");
                    if (getwidth.Count > 0)
                    {
                        if (getwidth[0].ToString().Contains("%"))
                        {
                            htmlwidth = getwidth[1].ToString().Substring(15, (getwidth[1].ToString().LastIndexOf(',') - 15));
                        }
                        else
                        {
                            htmlwidth = getwidth[0].ToString().Substring(15, (getwidth[0].ToString().LastIndexOf(',') - 15));
                        }
                    }
                }
                else
                {
                    if (lstwidth[0].ToString().Contains("%"))
                    {
                        htmlwidth = lstwidth[1].ToString().Substring(6, (lstwidth[1].ToString().LastIndexOf("px") - 6));
                    }
                    else
                    {
                        htmlwidth = lstwidth[0].ToString().Substring(6, (lstwidth[0].ToString().LastIndexOf("px") - 6));
                    }
                }

                if (lstheight.Count == 0)
                {
                    var getheight = System.Text.RegularExpressions.Regex.Matches(content, @",height=(.*?)""");

                    if (getheight.Count > 0)
                    {
                        htmlheight = getheight[0].ToString().Substring(8, (getheight[0].ToString().LastIndexOf('"') - 8));
                    }
                }
                else
                {
                    htmlheight = lstheight[0].ToString().Substring(7, (lstheight[0].ToString().LastIndexOf("px") - 7));
                }

                int widthnumeric, heightnumeric;
                if (int.TryParse(htmlwidth, out widthnumeric) && int.TryParse(htmlheight, out heightnumeric))
                {
                    if (widthnumeric == 0 || heightnumeric == 0)
                    {
                        htmldimensions.Add(250);
                        htmldimensions.Add(250);
                    }
                    else
                    {
                        htmldimensions.Add(widthnumeric);
                        htmldimensions.Add(heightnumeric);
                    }
                }
                else
                {
                    htmldimensions.Add(250);
                    htmldimensions.Add(250);
                }

                return htmldimensions;
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, 0, "ExtractHtmlDimensions failed at {0}, Stacktrace:{1}", ex.Message, ex.StackTrace);
            }
            htmldimensions.Add(250);
            htmldimensions.Add(250);
            return htmldimensions;
        }


        #endregion

    }
}
