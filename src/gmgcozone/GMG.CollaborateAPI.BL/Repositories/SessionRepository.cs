﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Validators;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.Data.SqlClient;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class SessionRepository : IRepository
    {
        #region Fields

        private DbContextBL _dbContext;

        #endregion

        #region Constructors

        public SessionRepository(DbContextBL dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Creates new session based on the current user and saves it on the database
        /// </summary>
        /// <param name="session"></param>
        /// <param name="dbContext"></param>
        StartSessionResponse CreateSession(StartSessionRequest session)
        {
            var userInfo = (from u in _dbContext.Users
                            join us in _dbContext.UserStatus on u.Status equals us.ID
                            join ac in _dbContext.Accounts on u.Account equals ac.ID
                            where u.Username.ToLower().Trim() == session.username.ToLower().Trim() && us.Key == "A" &&
                              (String.IsNullOrEmpty(session.accountURL) ||
                              (ac.IsCustomDomainActive
                                  ? session.accountURL.ToLower() == ac.CustomDomain.ToLower()
                                  : session.accountURL.ToLower() == ac.Domain.ToLower()))
                            select new
                            {
                                u.ID,
                                userKey = u.Guid,
                                accountName = ac.Name,
                                accountKey = ac.Guid
                            }).FirstOrDefault();

            if (userInfo != null)
            {
                var newSession = new CollaborateAPISession
                {
                    User = userInfo.ID,
                    TimeStamp = DateTime.UtcNow,
                    SessionKey = Guid.NewGuid().ToString()
                };
                _dbContext.CollaborateAPISessions.Add(newSession);
                _dbContext.SaveChanges();

                return new StartSessionResponse
                {
                    sessionKey = newSession.SessionKey,
                    accountKey = userInfo.accountKey,
                    accountName = userInfo.accountName,
                    userKey = userInfo.userKey
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// var session = _dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == sessionKey);
        /// session.IsConfirmed = true;
        /// session.TimeStamp = DateTime.UtcNow;
        /// _dbContext.SaveChanges();
        /// </summary>
        /// <param name="sessionKey"></param>
        /// <returns></returns>
        ConfirmIdentityResponse ConfirmSession(string sessionKey)
        {

            string query = "UPDATE CollaborateAPISession SET IsConfirmed = 1, TimeStamp = '" + DateTime.UtcNow.ToString() + "' WHERE SessionKey = @sessionKey ";

            ///Performance improvement. Do not remove the top commented code
            _dbContext.Database.ExecuteSqlCommand(query,
                new SqlParameter("sessionKey", sessionKey));
            return new ConfirmIdentityResponse { identityConfirmed = true };
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initial Startup Session Request
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object ProcessStartupRequest(StartSessionRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new StartSessionValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                StartSessionResponse response = CreateSession(model);

                if (response == null)
                {
                    returnStatusCode = HttpStatusCodeEnum.BadRequest;
                }

                return response;
            }
            else
            {
                return validator.GetRuleViolations();
            }
        }

        /// <summary>
        /// Initial Startup Session Request
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object ProcessConfirmationRequest(ConfirmIdentityRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new ConfirmSessionValidator(model);
            returnStatusCode = validator.ValidateLogin(_dbContext);

            if (validator.IsValid)
            {
                return ConfirmSession(model.sessionKey.Trim());
            }
            else
            {                              
                return validator.GetRuleViolations();
            }
        }

        #endregion
    }
}
