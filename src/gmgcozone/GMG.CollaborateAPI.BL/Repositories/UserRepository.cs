﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.UserModels;
using GMG.CollaborateAPI.BL.Validators;
using GMG.CollaborateAPI.BL.Validators.UserValidators;
using GMG.CoZone.Common;
using GMGColorBusinessLogic;
using Newtonsoft.Json;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class UserRepository : IRepository
    {
         #region Fields

        private readonly DbContextBL _dbContext;
        
        #endregion

        #region Constructors

        public UserRepository(DbContextBL dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creates a new user in the system, and returns the GUID in case in succeeds
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object CreateUser(CreateUserRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CreateUserValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Created;
                return CreateUserResponse(model);
            }
           
            return validator.GetRuleViolations();
        }

        public object CreateExternalUser(CreateExternalUserRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CreateExternalUserValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Created;
                return CreateExternalUserResponse(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Checkes whether the specified email belongs to an user for the current account
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object CheckIfUserExists(UserExistsRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CheckIfUserExistsValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return UserExistsResponse(model, _dbContext);
            }

            return validator.GetRuleViolations();
        }

        #endregion

        #region Private Methods

        private CreateUserResponse CreateExternalUserResponse(CreateExternalUserRequest model)
        {
            var userInfo = (from cas in _dbContext.CollaborateAPISessions
                            join u in _dbContext.Users on cas.User equals u.ID
                            join us in _dbContext.UserStatus on u.Status equals us.ID
                            join ac in _dbContext.Accounts on u.Account equals ac.ID
                            where cas.SessionKey == model.sessionKey && us.Key == "A"
                            select new
                            {
                                u.Account,
                                u.ID,
                                ac.Locale
                            }).FirstOrDefault();

            GMGColorDAL.GMGColorLogging.log.Info($"CreateExternalUser request from Collaborate REST API. Creator: {userInfo.ID}, Account: {userInfo.Account}, Body content: {JsonConvert.SerializeObject(model)} ");
            var userLanguage = _dbContext.Accounts.Where(a => a.ID == userInfo.Account).Select(a => a.Locale).FirstOrDefault();
            var objUser = new GMGColorDAL.ExternalCollaborator
            {
                GivenName = model.FirstName,
                FamilyName = model.LastName,
                Locale = userLanguage,
                EmailAddress = model.Email.Trim().ToLower(),
                Guid = Guid.NewGuid().ToString(),
                Creator = userInfo.ID,
                Account = userInfo.Account,
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow,
                ProofStudioColor = ExternalCollaboratorBL.GenerateCollaboratorProofStudioColor(userInfo.Account, new List<string>(), _dbContext),
                Modifier = userInfo.ID,
            };
            _dbContext.ExternalCollaborators.Add(objUser);
            _dbContext.SaveChanges();

            GMGColorDAL.GMGColorLogging.log.Info($"CreateExternalUser response from Collaborate REST API succeeded. Created ExternalUser ID: {objUser.ID} ");

            return new CreateUserResponse { Guid = objUser.Guid };
        }

        private CreateUserResponse CreateUserResponse(CreateUserRequest model)
        {
            var userInfo = (from cas in _dbContext.CollaborateAPISessions
                           join u in _dbContext.Users on cas.User equals u.ID
                            join us in _dbContext.UserStatus on u.Status equals us.ID
                           join ac in _dbContext.Accounts on u.Account equals ac.ID
                           where cas.SessionKey == model.sessionKey && us.Key == "A"
                           select new
                           {
                               u.Account,
                               u.ID,
                               ac.Locale
                           }).FirstOrDefault();

            GMGColorDAL.GMGColorLogging.log.Info($"CreateUser request from Collaborate REST API. Creator: {userInfo.ID}, Account: {userInfo.Account}, Body content: {JsonConvert.SerializeObject(model)} ");

            var objUser = new GMGColorDAL.User
            {
                Username = model.Email,
                Password = GMGColorDAL.User.GetNewEncryptedPassword(model.Password, _dbContext),
                EmailAddress = model.Email,
                IsSsoUser = model.IsSSOUser,
                Account = userInfo.Account,
                Status = GMGColorDAL.UserStatu.GetUserStatus(GMGColorDAL.UserStatu.Status.Active, _dbContext).ID,
                Preset = GMGColorDAL.Preset.GetPreset(GMGColorDAL.Preset.PresetType.Low, _dbContext).ID,
                Creator = userInfo.ID,
                CreatedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
                Modifier = userInfo.ID,
                ModifiedDate = Convert.ToDateTime(DateTime.UtcNow.ToString("g")),
                GivenName = model.FirstName,
                FamilyName = model.LastName,
                OfficeTelephoneNumber = string.Empty,
                NotificationFrequency =
                    GMGColorDAL.NotificationFrequency.GetNotificationFrequency(
                        GMGColorDAL.NotificationFrequency.Frequency.Never, _dbContext).ID,
                Guid = Guid.NewGuid().ToString(),
                ProofStudioColor = UserBL.GetAvailableUserColor(userInfo.Account, _dbContext).ID,
                Locale = userInfo.Locale,
                PrivateAnnotations = model.PrivateAnnotations
            };

            _dbContext.Users.Add(objUser);

            //Check Administration Admin 
            bool IsValidAdminRole = false;
            var CollaborateRole = model.Permissions.Where(o => o.name.ToLower() == "collaborate").Select(o => o.role).FirstOrDefault();
            var AdminRole = model.Permissions.Where(o => o.name.ToLower() == "admin").Select(o => o.role).FirstOrDefault();

            if( (CollaborateRole  != null && AdminRole != null) ? (CollaborateRole.ToUpper() == "ADM"  && AdminRole.ToUpper() == "ADM") : false)
            {
                IsValidAdminRole = true;
            }

            //check permissions, for non set modules permissions with NON role must exist
            foreach (var module in Enum.GetValues(typeof (AppModule)))
            {
                int appModuleKey;

                var currentModule = module.ToString().ToLower();
                switch (currentModule)
                {
                    case "collaborate":
                        appModuleKey = (int) AppModule.Collaborate;
                        break;
                    case "deliver":
                        appModuleKey = (int) AppModule.Deliver;
                        break;
                    case "admin":
                        appModuleKey = (int) AppModule.Admin;
                        break;
                    case "filetransfer":
                        appModuleKey = (int)AppModule.FileTransfer;
                        break;
                    default:
                        continue;
                }

                //get role for current module or set to NON if wasn t specified
                var currentPerm = model.Permissions.FirstOrDefault(t => t.name.ToLower().Trim() == currentModule);
                var roleForModule = currentPerm?.role.ToUpper().Trim() ?? "NON";

                var role = (from r in _dbContext.Roles
                            join apm in _dbContext.AppModules on r.AppModule equals apm.ID
                            where apm.Key == appModuleKey && r.Key.ToLower() == roleForModule
                            select r.ID).FirstOrDefault();

                if( !IsValidAdminRole && appModuleKey == (int)AppModule.Admin && roleForModule != "NON")
                {
                    role = (int) RoleID.Admin_NoAccess;
                }

                if (role > 0)
                {
                    var objUserRole = new GMGColorDAL.UserRole { Role = role, User1 = objUser };
                    objUser.UserRoles.Add(objUserRole);
                }
                else
                {
                    role = (from r in _dbContext.Roles
                                join apm in _dbContext.AppModules on r.AppModule equals apm.ID
                                where apm.Key == appModuleKey && r.Key == "NON"
                                select r.ID).FirstOrDefault();

                    var objUserRole = new GMGColorDAL.UserRole { Role = role, User1 = objUser };
                    objUser.UserRoles.Add(objUserRole);
                }
                
            }

            if (model.Groups != null)
            {
                var groups = GetGroupInfoByName(model.Groups, userInfo.Account, _dbContext);         
                
                AddUserToGroup(groups, objUser, model.Groups, _dbContext);
             
                AddUserToFolderCollaborators(objUser.ID, groups, _dbContext);              
            }

            _dbContext.SaveChanges();

            GMGColorDAL.GMGColorLogging.log.Info($"CreateUser response from Collaborate REST API succeeded. Created User ID: {objUser.ID} ");

            return new CreateUserResponse { Guid = objUser.Guid };
        }

        private static Dictionary<int, string> GetGroupInfoByName(List<UserGroup> userGroups, int account, DbContextBL dbContext)
        {
            var groupNames = userGroups.Select(g => g.name.ToLower().Trim()).ToList();
            return (from ug in dbContext.UserGroups
                    where ug.Account == account && groupNames.Contains(ug.Name.ToLower())
                          select new
                          {
                              ug.ID,
                              ug.Name
                          }).ToDictionary(t => t.ID, t => t.Name);
        }

        private static void AddUserToGroup(Dictionary<int, string> groups, GMGColorDAL.User user, List<UserGroup> userGroups, DbContextBL dbContext)
        {
            foreach (var group in groups)
            {
                var ugu = new GMGColorDAL.UserGroupUser { User1 = user, UserGroup = group.Key };

                var isPrimary = userGroups.Where(g => g.name == group.Value).Select(g => g.isprimary).FirstOrDefault();

                //Set user Primary Group
                if (isPrimary)
                {
                    ugu.IsPrimary = true;
                }

                dbContext.UserGroupUsers.Add(ugu);
            }
        }

        private static void AddUserToFolderCollaborators(int userId, Dictionary<int, string> groups, DbContextBL dbContext)
        {
            var groupsIds = groups.Select(g => g.Key).ToList();
            var folders = dbContext.FolderCollaboratorGroups.Where(fcg => groupsIds.Contains(fcg.CollaboratorGroup)).Select(f => f.Folder).Distinct().ToList();

            foreach (var folderId in folders)
            {
                var userHasAlreadyAccessToThisFolder = dbContext.FolderCollaborators.Any(fc => fc.Collaborator == userId && fc.Folder == folderId);
                if (!userHasAlreadyAccessToThisFolder)
                {
                    var objCollaborator = new GMGColorDAL.FolderCollaborator
                    {
                        Folder = folderId,
                        Collaborator = userId,
                        AssignedDate = DateTime.UtcNow
                    };
                    dbContext.FolderCollaborators.Add(objCollaborator);
                }                
            }
        }

        private static CreateUserResponse UserExistsResponse(UserExistsRequest model, DbContextBL dbContext)
        {
            var account = (from cas in dbContext.CollaborateAPISessions
                           join u in dbContext.Users on cas.User equals u.ID
                           where cas.SessionKey == model.sessionKey
                           select u.Account).FirstOrDefault();

            var userGuid = (from u in dbContext.Users
                            join us in dbContext.UserStatus on u.Status equals us.ID
                            where u.Account == account && us.Key == "A" &&
                            u.EmailAddress.ToLower() == model.email.ToLower().Trim()
                            select u.Guid).FirstOrDefault();

            return new CreateUserResponse {Guid = userGuid};
        }

        #endregion
    }
}
