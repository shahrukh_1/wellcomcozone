﻿using GMG.CollaborateAPI.BL.Models.ChecklistModels;
using GMGColorBusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GMG.CollaborateAPI.BL.Validators.ChecklistValidators;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class ChecklistRepository : IRepository
    {
        #region Fields

        private readonly DbContextBL _dbContext;

        #endregion

        #region Constructors 

        public ChecklistRepository(DbContextBL context)
        {
            _dbContext = context;
        }

        #endregion

        public object GetCheckListDetails(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new ChecklistValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Ok;
                return GetCheckListDetail(model);
            }
            return validator.GetRuleViolations();
        }

        private object GetCheckListDetail(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model)
        {
            var checklistInfo = (from aw in _dbContext.CheckList
                             join u in _dbContext.Users on aw.Account equals u.Account
                             join si in _dbContext.CollaborateAPISessions on u.ID equals si.User
                             where si.SessionKey == model.sessionKey
                             select new GMG.CollaborateAPI.BL.Models.ChecklistModels.Checklist()
                             {
                                 name = aw.Name,
                                 guid = aw.Guid
                             }).ToList();


            return new ListChecklistResponse() { Checklist = checklistInfo };
        }
    }
}
