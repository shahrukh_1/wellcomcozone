﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CollaborateAPI.BL.Models.FolderModels;
using GMG.CollaborateAPI.BL.Validators.FolderValidators;
using GMGColorBusinessLogic;
using GMGColorDAL.CustomModels;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class FolderRepository : IRepository
    {
        #region Fields

        private DbContextBL _dbContext;

        #endregion

        #region Constructors

        public FolderRepository(DbContextBL dbContext)
        {
            _dbContext = dbContext;
        }

        #endregion

        #region Public Methods

        public object CreateFolder(CreateFolderRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new CreateFolderValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Created;
                return CreateNewFolder(model);
            }

            return validator.GetRuleViolations();
        }

        public object GetAllFolders(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetAllFoldersValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Created;
                return GetAllFolders(model);
            }

            return validator.GetRuleViolations();
        }

        public object GetFolderByName(GetFolderByNameRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetFolderByNameValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Created;
                return GetFolderByName(model);
            }

            return validator.GetRuleViolations();
        }

        #endregion

        #region Private

        private object GetFolderByName(GetFolderByNameRequest model)
        {
            return (from f in _dbContext.Folders
                    join fc in _dbContext.FolderCollaborators on f.ID equals fc.Folder
                    join u in _dbContext.Users on fc.Collaborator equals u.ID
                    join us in _dbContext.UserStatus on u.Status equals us.ID

                    let account = (from cas in _dbContext.CollaborateAPISessions
                                   join usr in _dbContext.Users on cas.User equals usr.ID
                                   join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                   where cas.SessionKey == model.sessionKey
                                   select ac).FirstOrDefault()
                    where u.Account == account.ID && u.Username == model.username && us.Key == "A" && f.Name.ToLower() == model.name.Trim().ToLower()
                    select new GetFolderByNameResponse()
                    {
                        guid = f.Guid
                    }).FirstOrDefault();
        }

        private BaseFolderResponse CreateNewFolder(CreateFolderRequest model)
        {
            var loggedUser = (from u in _dbContext.Users
                              join us in _dbContext.UserStatus on u.Status equals us.ID
                              let account = (from cas in _dbContext.CollaborateAPISessions
                                             join usr in _dbContext.Users on cas.User equals usr.ID
                                             join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                             where cas.SessionKey == model.sessionKey
                                             select ac).FirstOrDefault()
                              where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                              select u).FirstOrDefault();

            var parentFolder = 0;
            if (!string.IsNullOrEmpty(model.parentGuid))
            {
                parentFolder = (from f in _dbContext.Folders
                                where f.Guid == model.parentGuid
                                select f.ID).FirstOrDefault();
            }

            if (loggedUser != null)
            {
                var objModel = new FoldersModel()
                {
                    Account = loggedUser.Account,
                    Creator = loggedUser.ID,
                    Name = model.name,
                    Parent = parentFolder
                };

                FolderBL.FolderCRD(_dbContext, ref objModel);

                return new BaseFolderResponse() { guid = objModel.Guid };
            }

            return null;
        }

        private GetAllFoldersResponse GetAllFolders(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model)
        {
            var folders = (from f in _dbContext.Folders
                           join fc in _dbContext.FolderCollaborators on f.ID equals fc.Folder
                           join u in _dbContext.Users on fc.Collaborator equals u.ID
                           join us in _dbContext.UserStatus on u.Status equals us.ID

                           let account = (from cas in _dbContext.CollaborateAPISessions
                                          join usr in _dbContext.Users on cas.User equals usr.ID
                                          join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                          where cas.SessionKey == model.sessionKey
                                          select ac).FirstOrDefault()
                           where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                           select new FolderResponse()
                          {
                              guid = f.Guid,
                              name = f.Name
                          }).ToList();

            return new GetAllFoldersResponse() { folders = folders };
        }

        #endregion


    }
}
