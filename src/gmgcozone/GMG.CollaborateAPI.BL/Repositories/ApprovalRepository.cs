﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading;
using System.Transactions;
using BrowshotScreenshotService;
using GMG.CollaborateAPI.BL.Models;
using GMG.CollaborateAPI.BL.Models.AnnotationModels;
using GMG.CollaborateAPI.BL.Models.Approval_Models;
using GMG.CollaborateAPI.BL.Validators;
using GMG.CollaborateAPI.BL.Validators.Approval_Validators;
using GMG.CoZone.Common;
using GMG.WebHelpers.Job;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMG.CollaborateAPI.BL.Validators.JobValidators;
using GMG.CollaborateAPI.BL.Validators.AnnotationValidators;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;
using GMGColorNotificationService;
using GMG.CollaborateAPI.BL.Models.JobModels;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class ApprovalRepository : IRepository
    {
        #region Fields

        private readonly DbContextBL _dbContext;

        #endregion

        #region Constructors

        public ApprovalRepository(DbContextBL context)
        {
            _dbContext = context;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Generates the report thumbnail using Browshot Screenshot Service
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private Stream GenerateReport(AnnotationReportRequest model, string annotationAction)
        {
            string url = GetAnnotationReportScreenshotURL(model, annotationAction);
            int id = BrowshotService.ScreenshotCreate(url, GMGColorConfiguration.AppConfiguration.BrowshotInstanceID, 1, GMGColorConfiguration.AppConfiguration.BrowshotAccessKey);

            if (id == 0)
            {
                throw new Exception(String.Format("Could not create annotation report screenshot for sessionKey {0}", model.sessionKey));
            }

            var status = BrowshotService.BrowshotStatus.Pending;
            DateTime startTime = DateTime.UtcNow;

            while (status == BrowshotService.BrowshotStatus.Pending && (DateTime.UtcNow - startTime).TotalSeconds < GMGColorConfiguration.AppConfiguration.WebPageTimeout)
            {
                status = BrowshotService.GetScreenshotStatus(id, GMGColorConfiguration.AppConfiguration.BrowshotAccessKey);
                Thread.Sleep(500);
            }

            if (status == BrowshotService.BrowshotStatus.Success)
            {
                return BrowshotService.GetThumbnailStream(id, GMGColorConfiguration.AppConfiguration.BrowshotAccessKey);
            }
            else
            {
                throw new Exception(String.Format("Annotation report snapshot resulted in error state for sessionKey {0}", model.sessionKey));
            }
        }

        /// <summary>
        /// Gets the url that will be sent to Browshot Service to make the screenshot
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string GetAnnotationReportScreenshotURL(AnnotationReportRequest model, string annotationAction)
        {
            var approvalInfo = (from ap in _dbContext.Approvals
                                join j in _dbContext.Jobs on ap.Job equals j.ID
                                join u in _dbContext.Users on ap.Creator equals u.ID
                                join ac in _dbContext.Accounts on u.Account equals ac.ID
                                where ap.Guid == model.guid.Trim()
                                select new ApprovalInfo()
                                {
                                    ID = ap.ID,
                                    Domain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                    Account = ac.ID,
                                    CurrentPhase = ap.CurrentPhase,
                                    JobOwner = j.JobOwner
                                }).FirstOrDefault();

            return GetReportScreenshotUrl(model, annotationAction, approvalInfo);
        }

        /// <summary>
        /// Gets the url that will be sent to Browshot Service to make the screenshot
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string GetJobAnnotationReportScreenshotURL(AnnotationReportRequest model, string annotationAction)
        {
            var approvalInfo = (from ap in _dbContext.Approvals
                                join j in _dbContext.Jobs on ap.Job equals j.ID
                                join u in _dbContext.Users on ap.Creator equals u.ID
                                join ac in _dbContext.Accounts on u.Account equals ac.ID
                                where j.Guid == model.guid.Trim()
                                select new ApprovalInfo()
                                {
                                    ID = ap.ID,
                                    JobId = j.ID,
                                    Domain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                    Account = ac.ID,
                                    CurrentPhase = ap.CurrentPhase,
                                    JobOwner = j.JobOwner
                                }).FirstOrDefault();

            model.includeAllVersions = true;

            return GetReportScreenshotUrl(model, annotationAction, approvalInfo);
        }

        private string GetReportScreenshotUrl(AnnotationReportRequest model, string annotationAction, ApprovalInfo approvalInfo)
        {
            string accountUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + approvalInfo.Domain + annotationAction;

            var annotationReportUri = new UriBuilder(accountUrl);

            Hashtable queryParams = GetAnnotationReportQueryParams(model, approvalInfo);

            StringBuilder query = new StringBuilder();
            foreach (DictionaryEntry pair in queryParams)
            {
                query.Append(pair.Key);
                query.Append("=");
                query.Append(pair.Value);
                query.Append("&");
            }

            annotationReportUri.Query = Uri.EscapeUriString(query.ToString());

            return annotationReportUri.ToString();
        }

        /// <summary>
        /// Creates the query string for the annotation report request url
        /// </summary>
        /// <param name="model"></param>
        /// <param name="appId"></param>
        /// <param name="accId"></param>
        /// <param name="dbContext"></param>
        /// <returns></returns>
        private Hashtable GetAnnotationReportQueryParams(AnnotationReportRequest model, ApprovalInfo approval)
        {
            var queryParams = new Hashtable();
            PopulateApprovalOrJob(model, approval, queryParams);
            queryParams.Add("AnnotationsPerPage", model.nrOfCommentsPerPage);
            queryParams.Add("ShowSummaryPage", model.includeSummaryPage.Value);

            //determine report type
            int reportType;
            switch (model.displayOptions)
            {
                case Validators.Constants.PinsLabel:
                    reportType = (int)AnnotationReportType.Pins;
                    break;
                case Validators.Constants.CommentsAndPinsLabel:
                    reportType = (int)AnnotationReportType.CommentsAndPins;
                    break;
                default:
                    reportType = (int)AnnotationReportType.Comments;
                    break;
            }
            queryParams.Add("ReportTypeID", reportType);

            //determine filter type type
            int filterBy = (int)AnnotationReportFiltering.User;
            if (model.filterBy.Equals(Validators.Constants.GroupsLabel))
            {
                filterBy = (int)AnnotationReportFiltering.Group;
            }
            queryParams.Add("ReportFilteringID", filterBy);

            //determine groupping type
            int sortBy;
            switch (model.sortBy)
            {
                case Validators.Constants.UserLabel:
                    sortBy = (int)AnnotationReportGrouping.User;
                    break;
                case Validators.Constants.GroupLabel:
                    sortBy = (int)AnnotationReportGrouping.Group;
                    break;
                default:
                    sortBy = (int)AnnotationReportGrouping.Date;
                    break;
            }
            queryParams.Add("ReportGroupingID", sortBy);

            //check filter option and filter users or gr
            if (model.filterBy.Equals(Validators.Constants.UsersLabel))
            {
                var validUsers = UserBL.GetUsersByApprovalID(new[] { approval.ID }, _dbContext, true);
                //if any user specified select only those
                if (model.users != null && model.users.Any())
                {
                    validUsers.RemoveAll(t => !model.users.Select(o => o.username.ToLower()).Contains(t.Name.ToLower()));
                }
                for (int i = 0; i < validUsers.Count; i++)
                {
                    queryParams.Add(String.Format("AnnotationsReportUsers[{0}].ID", i), validUsers[i].ID);
                    queryParams.Add(String.Format("AnnotationsReportUsers[{0}].IsSelected", i), true);
                    queryParams.Add(String.Format("AnnotationsReportUsers[{0}].IsExternal", i), validUsers[i].IsExternal);
                }
            }
            else
            {
                var accountGroups = (from ug in _dbContext.UserGroups
                                     where ug.Account == approval.Account
                                     select ug).ToList();

                if (model.groups != null && model.groups.Any())
                {
                    accountGroups.RemoveAll(t => !model.groups.Contains(t.Name));
                }

                for (int i = 0; i < accountGroups.Count; i++)
                {
                    queryParams.Add(String.Format("AnnotationsReportGroups[{0}].ID", i), accountGroups[i].ID);
                }
            }

            if (approval.CurrentPhase != null)
            {
                List<int> phases = new List<int>();

                var loggedUserId = (from u in _dbContext.Users
                                    join us in _dbContext.UserStatus on u.Status equals us.ID
                                    let accountId = (from cas in _dbContext.CollaborateAPISessions
                                                     join usr in _dbContext.Users on cas.User equals usr.ID
                                                     where cas.SessionKey == model.sessionKey
                                                     select usr.Account).FirstOrDefault()
                                    where u.Account == accountId && u.Username == model.username && us.Key == "A"
                                    select u.ID).FirstOrDefault();

                if (!model.includeAllVersions)
                {
                    if (model.phases != null && model.phases.Count > 0)
                    {
                        phases = (from ajpa in _dbContext.ApprovalJobPhaseApprovals
                                  join ajp in _dbContext.ApprovalJobPhases on ajpa.Phase equals ajp.ID
                                  join aph in _dbContext.ApprovalPhases on ajp.PhaseTemplateID equals aph.ID

                                  let userIsCollaborator = (from acl in _dbContext.ApprovalCollaborators
                                                            where acl.Approval == approval.ID && acl.Collaborator == loggedUserId && acl.Phase == ajp.ID
                                                            select acl.ID).Any()

                                  let hasAnnotations = (from annot in _dbContext.ApprovalAnnotations
                                                        join app in _dbContext.ApprovalPages on annot.Page equals app.ID
                                                        where app.Approval == approval.ID && annot.Parent == null && annot.Phase == ajp.ID
                                                        select annot.ID).Any()

                                  where ajpa.Approval == approval.ID && model.phases.Contains(aph.Guid) && hasAnnotations && (userIsCollaborator || loggedUserId == approval.JobOwner)
                                  select ajp.ID).ToList();
                    }
                    else
                    {
                        phases = (from ajp in _dbContext.ApprovalJobPhases
                                  join ajpa in _dbContext.ApprovalJobPhaseApprovals on ajp.ID equals ajpa.Phase

                                  let userIsCollaborator = (from acl in _dbContext.ApprovalCollaborators
                                                            where acl.Approval == approval.ID && acl.Collaborator == loggedUserId && acl.Phase == ajp.ID
                                                            select acl.ID).Any()

                                  let hasAnnotation = (from annot in _dbContext.ApprovalAnnotations
                                                       join app in _dbContext.ApprovalPages on annot.Page equals app.ID
                                                       where app.Approval == approval.ID && annot.Parent == null && annot.Phase == ajp.ID
                                                       select annot.ID).Any()
                                  where ajpa.Approval == approval.ID && hasAnnotation && (userIsCollaborator || loggedUserId == approval.JobOwner)
                                  select ajp.ID).ToList();

                    }
                }
                else
                {
                    phases = ApprovalBL.GetJobAllAvailablePhasesWithAnnotations(approval.ID, loggedUserId, _dbContext)
                        .Select(s => s.ID).Distinct().ToList();

                }

                for (int i = 0; i < phases.Count; i++)
                {
                    queryParams.Add(String.Format("AnnotationsReportPhases[{0}].ID", i), phases[i]);
                    queryParams.Add(String.Format("AnnotationsReportPhases[{0}].IsSelected", i), true);
                }
            }

            return queryParams;
        }

        private static void PopulateApprovalOrJob(AnnotationReportRequest model, ApprovalInfo approval, Hashtable queryParams)
        {
            if (approval.JobId > 0)
            {
                queryParams.Add("SelectedJob", approval.JobId);
            }
            else
            {
                queryParams.Add("SelectedApprovals", approval.ID);
            }  
        }

        private AnnotationListResponse GenerateAnnotationList(AnnotationListRequest model)
        {
            var approvalInfo = (from ap in _dbContext.Approvals
                                join u in _dbContext.Users on ap.Creator equals u.ID
                                join ac in _dbContext.Accounts on u.Account equals ac.ID
                                join df in _dbContext.DateFormats on ac.DateFormat equals df.ID
                                where ap.Guid == model.guid.Trim()
                                select new
                                {
                                    ap.ID,
                                    Account = ac.ID,
                                    ac.TimeZone,
                                    DatePattern = df.Pattern
                                }).FirstOrDefault();

            //determine groupping type
            AnnotationReportGrouping sortBy;
            switch (model.sortBy)
            {
                case Validators.Constants.UserLabel:
                    sortBy = AnnotationReportGrouping.User;
                    break;
                case Validators.Constants.GroupLabel:
                    sortBy = AnnotationReportGrouping.Group;
                    break;
                default:
                    sortBy = AnnotationReportGrouping.Date;
                    break;
            }

            var selectedUsers = new List<AnnotationsReportUser>();

            //check filter option and filter users or gr
            if (model.filterBy.Equals(Validators.Constants.UsersLabel))
            {
                selectedUsers = UserBL.GetUsersByApprovalID(new[] { approvalInfo.ID }, _dbContext, true);
                //if any user specified select only those
                if (model.users != null && model.users.Any())
                {
                    selectedUsers.RemoveAll(t => !model.users.Select(o => o.username.ToLower()).Contains(t.Name.ToLower()));
                }
            }
            else
            {
                var accountGroups = (from ug in _dbContext.UserGroups
                                     where ug.Account == approvalInfo.Account
                                     select ug).ToList();

                if (model.groups != null && model.groups.Any())
                {
                    accountGroups.RemoveAll(t => !model.groups.Contains(t.Name));
                }

                selectedUsers = UserBL.GetUsersByGroupsId(accountGroups.Select(t => t.ID), _dbContext);
            }

            //Reuse function used in web to create annotation list
            List<ApprovalReportCSVModel> approvalAnnotationList =
                AnnotationBL.GetAnnotationReportCSVModel(new List<int> { approvalInfo.ID }, null,sortBy,
                    selectedUsers, approvalInfo.TimeZone, approvalInfo.DatePattern, _dbContext);

            //Create response
            AnnotationListResponse response = new AnnotationListResponse
            {
                guid = model.guid
            };

            foreach (var approvalAnnotations in approvalAnnotationList)
            {
                foreach (var annotationItem in approvalAnnotations.Annotations)
                {

                    AnnotationObjectModel annotationModel = new AnnotationObjectModel
                    {
                        pageNumber = annotationItem.PageNumber,
                        annotationDate = annotationItem.AnnotatedDate,
                        annotationNumber = annotationItem.AnnotationNumber,
                        comment = annotationItem.Comment,
                        user = annotationItem.CreatorName
                    };
                    response.annotations.Add(annotationModel);
                }
            }

            return response;
        }

        /// <summary>
        /// Update approval info for the given guid
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private object UpdateApprovalVersion(UpdateApprovalRequest model, out string errors)
        {
            errors = string.Empty;
            var response = new UpdateApprovalResponse() { guid = model.guid };

            var approval = (from a in _dbContext.Approvals where a.Guid == model.guid select a).FirstOrDefault();

            if (approval != null)
            {
                var loggedUser = (from u in _dbContext.Users
                                  join us in _dbContext.UserStatus on u.Status equals us.ID
                                  let account = (from cas in _dbContext.CollaborateAPISessions
                                                 join usr in _dbContext.Users on cas.User equals usr.ID
                                                 join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                                 where cas.SessionKey == model.sessionKey
                                                 select ac).FirstOrDefault()
                                  where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                                  select u).FirstOrDefault();

                var loggedUserCollaborateRole = Role.GetRole(loggedUser.CollaborateRoleKey(_dbContext));
                using (var ts = new TransactionScope())
                {
                    #region Update the logged user approval decision and approval deadline

                    approval.Modifier = loggedUser.ID;
                    approval.ModifiedDate = DateTime.UtcNow;

                    var dateTime = DateTime.Parse(model.deadline, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    if (dateTime != default(DateTime))
                    {
                        approval.Deadline = GetFormatedDeadline(dateTime, loggedUser.Account);
                    }

                    if ((approval.CurrentPhase != null && approval.AllowOtherUsersToBeAdded) || approval.CurrentPhase == null)
                    {
                        //change the approval primary decision maker, if any
                        if (model.collaborators.Count > 0)
                        {
                            var pdm = model.collaborators.FirstOrDefault(u => u.pdm);

                            if (pdm != null && (loggedUserCollaborateRole == Role.RoleName.AccountAdministrator || approval.Owner == loggedUser.ID))
                            {
                                if (pdm.isExternal)
                                {
                                    var externalUser = (from ex in _dbContext.ExternalCollaborators
                                                        where
                                                            ex.EmailAddress.ToLower() == pdm.email.ToLower() &&
                                                            ex.Account == loggedUser.Account && ex.IsDeleted == false
                                                        select ex.ID).FirstOrDefault();
                                    if (externalUser > 0)
                                    {
                                        approval.ExternalPrimaryDecisionMaker = externalUser;
                                        approval.PrimaryDecisionMaker = null;
                                    }
                                }
                                else
                                {
                                    var internalUser = (from u in _dbContext.Users
                                                        join us in _dbContext.UserStatus on u.Status equals us.ID
                                                        where
                                                            u.Username.ToLower() == pdm.username.ToLower() &&
                                                            u.Account == loggedUser.Account && us.Key == "A"
                                                        select u.ID).FirstOrDefault();
                                    if (internalUser > 0)
                                    {
                                        approval.ExternalPrimaryDecisionMaker = null;
                                        approval.PrimaryDecisionMaker = internalUser;
                                    }
                                }
                            }
                        }
                    }

                    _dbContext.SaveChanges();

                    #endregion

                    #region change the access rights for approval collaborators

                    if ((approval.CurrentPhase != null && approval.AllowOtherUsersToBeAdded) ||
                        approval.CurrentPhase == null)
                    {
                        if (model.collaborators.Count > 0)
                        {
                            List<ApprovalCollaborator> approvalCollaborators = GetApprovalUsersList(model.collaborators, approval.ID, loggedUser.Account, ref response, loggedUser, _dbContext, approval.CurrentPhase);

                            PermissionsBL.CollaborateAPIGrantDenyPermissions(approvalCollaborators, approval, _dbContext, loggedUser, loggedUserCollaborateRole);
                        }
                    }

                    #endregion

                    ts.Complete();
                }
            }

            return response;
        }

        /// <summary>
        /// Update approval phase deadline info for the given approvalJobPhase ID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private object UpdateApprovalPhaseDeadline(UpdateApprovalPhaseDeadlineRequest model, out string errors)
        {
            errors = string.Empty;
            var response = new UpdateApprovalPhaseDeadlineResponse() { guid = model.guid };

                var loggedUser = (from u in _dbContext.Users
                                  join us in _dbContext.UserStatus on u.Status equals us.ID
                                  let account = (from cas in _dbContext.CollaborateAPISessions
                                                 join usr in _dbContext.Users on cas.User equals usr.ID
                                                 join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                                 where cas.SessionKey == model.sessionKey
                                                 select ac).FirstOrDefault()
                                  where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                                  select new { u.ID, u.Account }).FirstOrDefault();

                var approvalJobPhase = (from j in _dbContext.Jobs
                                        join a in _dbContext.Approvals on j.ID equals a.Job
                                        join ajw in _dbContext.ApprovalJobWorkflows on j.ID equals ajw.Job
                                        join ajp in _dbContext.ApprovalJobPhases on ajw.ID equals ajp.ApprovalJobWorkflow
                                        join ap in _dbContext.ApprovalPhases on ajp.PhaseTemplateID equals ap.ID
                                        where a.Guid == model.guid && ap.Guid == model.approvalPhaseGuid
                                        select ajp).FirstOrDefault();
                                             
                if (loggedUser != null && approvalJobPhase != null)
                {
                    var dateTime = DateTime.Parse(model.deadline, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    if (dateTime != default(DateTime))
                    {
                        approvalJobPhase.Deadline = GetFormatedDeadline(dateTime, loggedUser.Account);
                        approvalJobPhase.ModifiedBy = loggedUser.ID;
                        approvalJobPhase.ModifiedDate = DateTime.UtcNow;
                        int ItsLastPhaseID = (from  ajp in _dbContext.ApprovalJobPhases
                                            where ajp.ApprovalJobWorkflow == approvalJobPhase.ApprovalJobWorkflow
                                            select ajp.ID).Max();
                        var approvalDeadline = (from a in _dbContext.Approvals where a.Guid == model.guid select a).FirstOrDefault();
                        bool IsApprovalDeadlineUpdate = (ItsLastPhaseID == approvalJobPhase.ID ) && (approvalDeadline.Deadline.HasValue ? approvalDeadline.Deadline.Value < dateTime : false);
                        if (IsApprovalDeadlineUpdate)
                        {
                            approvalDeadline.Deadline = dateTime;
                        }
                        _dbContext.SaveChanges();
                    }
                }
            
            return response;
        }

        private List<ApprovalCollaborator> GetApprovalUsersList(List<Collaborator> collaborators, int approvalID, int accountID, ref UpdateApprovalResponse response, GMGColorDAL.User loggedUser, DbContextBL _dbContext, int? currentPhase = null)
        {
            List<ApprovalCollaborator> newCollaborators = new List<ApprovalCollaborator>();

            var approvalRoles = (from ar in _dbContext.ApprovalCollaboratorRoles select ar).ToList();

            AddUpdateExternalCollaborators(collaborators, approvalID, approvalRoles, accountID, ref response, loggedUser, _dbContext, currentPhase);
            ValidateInternalCollaborators(collaborators, approvalRoles, accountID, ref response);

            // Get existing approval users 
            List<ApprovalCollaborator> existingApprovalCollaborators = _dbContext.ApprovalCollaborators.Where(ap => ap.Approval == approvalID && ap.Phase == currentPhase).ToList();
            existingApprovalCollaborators.RemoveAll(ec => collaborators.Where(s => s.toBeRemoved).Select(c => c.username).Contains(ec.User.Username));

            // Remove specified users
            if (collaborators != null && collaborators.Any(c => c.toBeRemoved))
            {
                //remove internal collaborators
                var toBeRemovedUsers = collaborators.Where(c => c.toBeRemoved && c.username != null).Select(c => c.username.ToLower()).ToList();
                var internalCollaborators = (from u in _dbContext.Users
                                             join us in _dbContext.UserStatus on u.Status equals us.ID
                                             where toBeRemovedUsers.Contains(u.Username) && u.Account == loggedUser.Account && us.Key == "A"
                                             select u.ID).ToList();

                _dbContext.ApprovalCollaborators.RemoveRange(_dbContext.ApprovalCollaborators.Where(c => toBeRemovedUsers.Contains(c.User.Username) && c.Approval == approvalID && c.Phase == currentPhase));

                //remove external collaborators
                var externalsToDelete = collaborators.Where(c => c.isExternal && !string.IsNullOrEmpty(c.email) && c.toBeRemoved).Select(ec => ec.email.ToLower()).ToList();
                var externalCollaborators = _dbContext.ExternalCollaborators.Where(ec => externalsToDelete.Contains(ec.EmailAddress) && ec.IsDeleted == false && ec.Account == loggedUser.Account).Select(ec => ec.ID).ToList();
                _dbContext.SharedApprovals.RemoveRange(_dbContext.SharedApprovals.Where(sa => externalCollaborators.Contains(sa.ExternalCollaborator) && sa.Approval == approvalID && sa.Phase == currentPhase));

                //Delete internal/external collaborators decision for current approval
                _dbContext.ApprovalCollaboratorDecisions.RemoveRange(_dbContext.ApprovalCollaboratorDecisions.Where(acd => (externalCollaborators.Contains(acd.ExternalCollaborator.Value) || internalCollaborators.Contains(acd.Collaborator.Value)) && acd.Approval == approvalID && acd.Phase == currentPhase));
            }

            foreach (Collaborator collaborator in collaborators)
            {
                ApprovalCollaborator approvalCollaborator = new ApprovalCollaborator();

                if (!collaborator.isExternal)
                {
                    var user = (from u in _dbContext.Users
                                join us in _dbContext.UserStatus on u.Status equals us.ID
                                where
                                    u.Username.ToLower() == collaborator.username.ToLower() &&
                                    u.Account == loggedUser.Account && us.Key == "A"
                                select u).FirstOrDefault();

                    if (user != null)
                    {
                        approvalCollaborator = new ApprovalCollaborator()
                        {
                            Approval = approvalID,
                            Collaborator = user.ID,
                            AssignedDate = DateTime.UtcNow,
                            ApprovalCollaboratorRole = approvalRoles.FirstOrDefault(ar => ar.Key == collaborator.approvalRole).ID,
                            Phase = currentPhase
                        };

                        newCollaborators.Add(approvalCollaborator);
                    }
                }
            }

            newCollaborators.AddRange(existingApprovalCollaborators);

            return newCollaborators;
        }

        /// <summary>
        /// Returns formated deadline
        /// </summary>
        /// <param name="deadline">The deadline to be formated</param>
        /// <param name="accountId">Logged account id</param>
        /// <returns></returns>
        private DateTime? GetFormatedDeadline(DateTime deadline, int accountId)
        {
            //get logged account info
            var loggedAccount = (from ac in _dbContext.Accounts where ac.ID == accountId select ac).FirstOrDefault();
            var deadlineDate = GMGColorFormatData.GetFormattedDate(deadline, loggedAccount.DateFormat1.Pattern);
            var deadlineTime = loggedAccount.TimeFormat == 1 ? deadline.ToString("hh:mm") : deadline.ToString("HH:mm");
            var deadlineTimeMeridiem = deadline.Date.ToString("tt", CultureInfo.InvariantCulture);

            DateTime formatedDeadline = ApprovalBL.GetFormatedApprovalDeadline(deadlineDate, deadlineTime, deadlineTimeMeridiem, loggedAccount.DateFormat1.Pattern, loggedAccount.TimeFormat);
            return GMGColorFormatData.SetUserTimeToUTC(formatedDeadline, loggedAccount.TimeZone);
        }

        /// <summary>
        /// Deletes the specified approval and creates response object
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private DeleteApprovalResponse DeleteApproval(CollaborateAPIIdentityModel model)
        {
            //get approval
            var approval = (from ap in _dbContext.Approvals
                            join j in _dbContext.Jobs on ap.Job equals j.ID
                            join js in _dbContext.JobStatus on j.Status equals js.ID
                            where ap.Guid == model.guid && ap.IsDeleted == false
                            select new
                            {
                                ap.ID,
                                version = ap.Version,
                                jobStatus = js.Key,
                                owner = ap.Owner,
                                ap.IsError
                            }).FirstOrDefault();

            if (JobStatu.GetJobStatus(approval.jobStatus) == JobStatu.Status.Archived || approval.IsError)
            {
                //delete from database
                Approval.MarkApprovalForPermanentDeletion(approval.ID, _dbContext);
            }
            else
            {
                var currObj = _dbContext.Approvals.FirstOrDefault(o => o.ID == approval.ID);
                currObj.IsDeleted = true;
                currObj.ModifiedDate = DateTime.UtcNow;

                ApprovalBL.ApprovalUserRecycleBinHistory(currObj, approval.owner, true, _dbContext);
            }

            //commit changes to db
            _dbContext.SaveChanges();

            return new DeleteApprovalResponse() { guid = model.guid };
        }

        /// <summary>
        /// Restores the specified approval and creates response object
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private RestoreApprovalResponse RestoreApproval(CollaborateAPIIdentityModel model)
        {

            var approvalInfo = (from ap in _dbContext.Approvals
                                where ap.Guid == model.guid
                                select new
                                {
                                    ap.ID,
                                    ap.Owner
                                }).FirstOrDefault();

            ApprovalBL.RestoreApproval(approvalInfo.ID, approvalInfo.Owner, _dbContext);

            //commit changes to db
            _dbContext.SaveChanges();

            return new RestoreApprovalResponse() { guid = model.guid };
        }

        /// <summary>
        /// Creates the response for get approval request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private GetApprovalResponse GetApproval(CollaborateAPIIdentityModel model)
        {
            var approvalInfo = (from ap in _dbContext.Approvals
                                join apt in _dbContext.ApprovalTypes on ap.Type equals apt.ID
                                join u in _dbContext.Users on ap.Creator equals u.ID
                                join ac in _dbContext.Accounts on u.Account equals ac.ID
                                join dt in _dbContext.DateFormats on ac.DateFormat equals dt.ID
                                where ap.Guid == model.guid
                                select new
                                {
                                    ap.Version,
                                    ap.FileName,
                                    CreatorName = u.GivenName + " " + u.FamilyName,
                                    ap.Size,
                                    NrOfPages = (from app in _dbContext.ApprovalPages
                                                 where app.Approval == ap.ID
                                                 select app.ID).Count(),
                                    ap.IsLocked,
                                    hasProcessingErrors = ap.IsError,
                                    AnnotationsNr = (from an in _dbContext.ApprovalAnnotations
                                                     join app in _dbContext.ApprovalPages on an.Page equals app.ID
                                                     where app.Approval == ap.ID && an.Parent == null && an.Phase == ap.CurrentPhase
                                                     select an.ID).Count(),
                                    Domain = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                    ac.Region,
                                    ap.CreatedDate,
                                    ap.Deadline,
                                    Type = apt.Key,
                                    DatePattern = dt.Pattern
                                }).FirstOrDefault();

            bool isVideoFile = approvalInfo.Type == (int)Approval.ApprovalTypeEnum.Movie;
            string thumbnailUrl = isVideoFile
                ? Approval.GetVideoImagePath(model.guid, approvalInfo.Region, approvalInfo.Domain)
                : Approval.GetApprovalImage(approvalInfo.Domain, approvalInfo.Region, model.guid,
                    approvalInfo.FileName, Approval.ImageType.Thumbnail);

            string thumbnailBase64 = String.Empty;
            using (WebClient client = new WebClient())
            {
                byte[] thumbnailData = client.DownloadData(thumbnailUrl);
                thumbnailBase64 = Convert.ToBase64String(thumbnailData);
            }

            return new GetApprovalResponse
            {
                guid = model.guid,
                version = approvalInfo.Version,
                filename = approvalInfo.FileName,
                creatorName = approvalInfo.CreatorName,
                uploadDate = GMGColorFormatData.GetFormattedDate(approvalInfo.CreatedDate, approvalInfo.DatePattern),
                deadline = approvalInfo.Deadline.HasValue ? GMGColorFormatData.GetFormattedDate(approvalInfo.Deadline.Value, approvalInfo.DatePattern) : "-",
                fileSize = GMGColorFormatData.GetFileSizeString(approvalInfo.Size),
                pages = approvalInfo.NrOfPages,
                isLocked = approvalInfo.IsLocked,
                hasProcessingErrors = approvalInfo.hasProcessingErrors,
                annotationsNo = approvalInfo.AnnotationsNr,
                thumbnail = thumbnailBase64
            };
        }

        /// <summary>
        /// Creates the response for get approval request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private GetApprovalWithAccessResponse GetApprovalWithAccess(CollaborateAPIIdentityModel model)
        {
            GetApprovalWithAccessResponse response = new GetApprovalWithAccessResponse(GetApproval(model))
            {
                collaborators = GetCollaborators(model.guid)
            };

            return response;
        }

        /// <summary>
        /// Creates the response for get approval request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private GetApprovalActivityResponse GetApprovalActivity(CollaborateAPIIdentityModel model)
        {
            var appId = (from ap in _dbContext.Approvals
                         where ap.Guid == model.guid
                         select ap.ID).ToList();

            GetApprovalActivityResponse response = GetApprovalActivityModel(appId, model).FirstOrDefault();

            return response;
        }

        /// <summary>
        /// Gets Collaborators for specified approval guid
        /// </summary>
        /// <param name="appGuid"></param>
        /// <returns></returns>
        private List<Collaborator> GetCollaborators(string appGuid)
        {
            return (from apc in _dbContext.ApprovalCollaborators
                    join u in _dbContext.Users on apc.Collaborator equals u.ID
                    join ap in _dbContext.Approvals on apc.Approval equals ap.ID
                    join apcr in _dbContext.ApprovalCollaboratorRoles on apc.ApprovalCollaboratorRole equals apcr.ID
                    where ap.Guid == appGuid && apc.Phase == ap.CurrentPhase
                    select new Collaborator
                    {
                        username = u.Username,
                        email = u.EmailAddress,
                        approvalRole = apcr.Name,
                        pdm = ap.PrimaryDecisionMaker == u.ID,
                        isExternal = false
                    }
                ).Union(from apc in _dbContext.SharedApprovals
                        join u in _dbContext.ExternalCollaborators on apc.ExternalCollaborator equals u.ID
                        join ap in _dbContext.Approvals on apc.Approval equals ap.ID
                        join apcr in _dbContext.ApprovalCollaboratorRoles on apc.ApprovalCollaboratorRole equals apcr.ID
                        where ap.Guid == appGuid && apc.Phase == ap.CurrentPhase
                        select new Collaborator
                        {
                            username = "",
                            email = u.EmailAddress,
                            approvalRole = apcr.Name,
                            pdm = ap.PrimaryDecisionMaker == u.ID,
                            isExternal = true
                        }).ToList();
        }

        /// <summary>
        /// Validates internal users
        /// </summary>
        /// <param name="collaborators">List of collaborators</param>
        /// <param name="approvalRoles">Approval roles defined in the system</param>
        /// <param name="response">A list of warnings</param>
        /// <returns></returns>
        private void AddUpdateExternalCollaborators(List<Collaborator> collaborators, int approvalId, List<ApprovalCollaboratorRole> approvalRoles, int accountId, ref UpdateApprovalResponse response, GMGColorDAL.User loggedUser, DbContextBL dbContext, int? currentPhase = null)
        {
            var externalCollaborators = collaborators.Where(u => u.isExternal && !string.IsNullOrEmpty(u.email) && !string.IsNullOrEmpty(u.approvalRole) && !u.toBeRemoved).ToList();
            if (externalCollaborators.Count > 0)
            {
                var externalCollaboratorsEmails = externalCollaborators.Select(ec => ec.email.ToLower()).ToList();
                var invalidApprovalRolesUsers = string.Empty;

                var accountExternalUsers = (from ec in dbContext.ExternalCollaborators
                                            where ec.Account == accountId && externalCollaboratorsEmails.Contains(ec.EmailAddress) && ec.IsDeleted == false
                                            select new
                                            {
                                                ec.ID,
                                                ec.EmailAddress
                                            }).ToList();

                externalCollaborators = externalCollaborators.Where(c => accountExternalUsers.Select(u => u.EmailAddress.ToLower().Trim()).Contains(c.email.ToLower())).ToList();

                var newExternalCollaborators = collaborators.Where(u => u.isExternal && !string.IsNullOrEmpty(u.email) && !accountExternalUsers.Select(acu => acu.EmailAddress.ToLower()).ToList().Contains(u.email.ToLower())).ToList();

                var permissions = new PermissionsUsersAndRolesModel();
                foreach (var newExternalCollaborator in newExternalCollaborators)
                {
                    if (!string.IsNullOrEmpty(newExternalCollaborator.firstname) && !string.IsNullOrEmpty(newExternalCollaborator.lastname))
                    {
                        var approvalRole = approvalRoles.Where(r => r.Key.ToLower().Trim() == newExternalCollaborator.approvalRole.ToLower().Trim())
                                                         .Select(r => r.ID)
                                                         .FirstOrDefault();

                        approvalRole = approvalRole > 0 ? approvalRole : (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;

                        var userLocale = dbContext.Accounts.Where(a => a.ID == accountId).Select(a => a.Locale).FirstOrDefault();
                        var fullname = newExternalCollaborator.firstname + " " + newExternalCollaborator.lastname;

                        permissions.NewExternalUsers.Add(new NewExternalUserPermissions
                        {
                            Phase = currentPhase,
                            Role = approvalRole,
                            Email = newExternalCollaborator.email,
                            FullName = fullname,
                            Locale = userLocale
                        });
                    }
                    else
                    {
                        response.warningMessages.Add(String.Format(WarningMessages.UserDoesNotExist, newExternalCollaborator.email));
                    }
                }

                foreach (var externalCollaborator in externalCollaborators)
                {

                    var approvalRole = approvalRoles.Where(r => r.Key.ToLower().Trim() == externalCollaborator.approvalRole.ToLower().Trim())
                                                     .Select(r => r.ID)
                                                     .FirstOrDefault();

                    approvalRole = approvalRole > 0 ? approvalRole : (int)ApprovalCollaboratorRole.ApprovalRoleName.ReadOnly;

                    permissions.ExternalUsers.Add(new ExternalUserPermissions()
                    {
                        Phase = currentPhase,
                        Role = approvalRole,
                        ID = accountExternalUsers.Where(ae => ae.EmailAddress.ToLower() == externalCollaborator.email.ToLower()).Select(ex => ex.ID).FirstOrDefault()
                    });
                }

                PermissionsBL.ShareApprovalFromApi(permissions, approvalId, dbContext, loggedUser, accountId, currentPhase);

                foreach (var externalCollaborator in externalCollaborators)
                {
                    var approvalRole = approvalRoles.Where(ar => ar.Key == externalCollaborator.approvalRole)
                                         .Select(r => r.ID)
                                         .FirstOrDefault();

                    if (approvalRole == 0)
                    {
                        invalidApprovalRolesUsers += externalCollaborator.approvalRole + ",";
                    }
                }

                if (!string.IsNullOrEmpty(invalidApprovalRolesUsers))
                {
                    var message = String.Format(WarningMessages.TypeInApprovaleRoleNameExternalUser, invalidApprovalRolesUsers.TrimEnd(','));
                    response.warningMessages.Add(message);
                }
            }
        }

        /// <summary>
        /// Validates internal users
        /// </summary>
        /// <param name="collaborators">List of collaborators</param>
        /// <param name="approvalId">The id of the approval for which the update is made</param>
        /// <param name="approvalRoles">Approval roles defined in the system</param>
        /// <param name="response">A list of warnings</param>
        /// <returns></returns>
        private void ValidateInternalCollaborators(List<Collaborator> collaborators, List<ApprovalCollaboratorRole> approvalRoles, int accountId, ref UpdateApprovalResponse response)
        {
            var internalCollaborators = collaborators.Where(u => !u.isExternal && !string.IsNullOrEmpty(u.username) && !string.IsNullOrEmpty(u.approvalRole)).ToList();
            var internalCollaboratorsUsernames = internalCollaborators.Select(u => u.username.ToLower());
            var invalidUsersApprovalRoles = string.Empty;

            //get all internal users from account
            var accountInternalUsers = (from u in _dbContext.Users
                                        join us in _dbContext.UserStatus on u.Status equals us.ID

                                        where u.Account == accountId && us.Key == "A" && internalCollaboratorsUsernames.Contains(u.Username)
                                        select new
                                        {
                                            u.ID,
                                            u.Username
                                        }).ToList();

            //get existing collaborators, specified in request, that have valid usernames
            internalCollaborators = internalCollaborators.Where(c => accountInternalUsers.Select(u => u.Username.ToLower().Trim())
                                        .ToList()
                                        .Contains(c.username.ToLower())).ToList();

            //get list of invalid usernames from request (users that are from another account or those that are not in the system)
            var wrongUsername = string.Join(",", collaborators.Where(u => !u.isExternal && !string.IsNullOrEmpty(u.username) &&
                                                                            !accountInternalUsers.Select(acu => acu.Username.ToLower()).Contains(u.username.ToLower()))
                                                                                     .ToList());

            foreach (var internalCollaborator in internalCollaborators)
            {
                var approvalRole = approvalRoles.Where(ar => ar.Key == internalCollaborator.approvalRole)
                                     .Select(r => r.ID)
                                     .FirstOrDefault();

                //check if specified role is valid, otherwise set user default role
                if (approvalRole == 0)
                {
                    invalidUsersApprovalRoles += internalCollaborator.approvalRole + ",";
                }
            }

            if (!string.IsNullOrEmpty(invalidUsersApprovalRoles))
            {
                var message = String.Format(WarningMessages.TypeInApprovaleRoleNameInternalUser, invalidUsersApprovalRoles.TrimEnd(','));
                response.warningMessages.Add(message);
            }

            if (!string.IsNullOrEmpty(wrongUsername))
            {
                var message = String.Format(WarningMessages.InvalidUsernameInternalUser, wrongUsername.TrimEnd(','));
                response.warningMessages.Add(message);
            }
        }

        /// <summary>
        /// Returns the URL for teh given approval that points to ProofStudio
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private object GetProofStudioURL(CollaborateAPIIdentityModel model)
        {
            var userInfo = (from u in _dbContext.Users
                            join us in _dbContext.UserStatus on u.Status equals us.ID
                            let account = (from cas in _dbContext.CollaborateAPISessions
                                           join usr in _dbContext.Users on cas.User equals usr.ID
                                           join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                           where cas.SessionKey == model.sessionKey && cas.IsConfirmed
                                           select ac).FirstOrDefault()
                            where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                            select new
                            {
                                UserGuid = u.Guid,
                                AccountDomain = account.IsCustomDomainActive ? account.CustomDomain : account.Domain
                            }).FirstOrDefault();

            string approvalHtmlViewUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +
                                         userInfo.AccountDomain +
                                         "/Studio/HtmlViewerExternal?apg="
                                         + model.guid +
                                         "&sessionKey=" + userInfo.UserGuid;

            return new GetProofStudioURLResponse { proofstudioURL = approvalHtmlViewUrl };
        }

        private object GetCompareVersionsProofStudioURL(ProofStudioCompareVersionsRequest model)
        {
            var userInfo = (from u in _dbContext.Users
                            join us in _dbContext.UserStatus on u.Status equals us.ID
                            let account = (from cas in _dbContext.CollaborateAPISessions
                                           join usr in _dbContext.Users on cas.User equals usr.ID
                                           join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                           where cas.SessionKey == model.sessionKey && cas.IsConfirmed
                                           select ac).FirstOrDefault()
                            where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                            select new
                            {
                                UserGuid = u.Guid,
                                AccountDomain = account.IsCustomDomainActive ? account.CustomDomain : account.Domain
                            }).FirstOrDefault();

            string approvalHtmlViewUrl = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" +
                                         userInfo.AccountDomain +
                                         "/Studio/HtmlViewerCompareVersions?v1Guid="
                                         + model.versions[0] +
                                         "&v2Guid=" +
                                         model.versions[1] +
                                         "&sessionKey=" + userInfo.UserGuid;

            return new GetProofStudioURLResponse { proofstudioURL = approvalHtmlViewUrl };
        }

        private UpdateApprovalDecisionResponse UpdateApprovalDecision(UpdateApprovalDecisionRequest model, out string errors)
        {
            errors = string.Empty;
            var response = new UpdateApprovalDecisionResponse() { guid = model.guid };
            try
            {


                var approval = (from a in _dbContext.Approvals where a.Guid == model.guid select a).FirstOrDefault();

                if (approval != null)
                {
                    var accountId = (from cas in _dbContext.CollaborateAPISessions
                                     join u in _dbContext.Users on cas.User equals u.ID
                                     where cas.SessionKey == model.sessionKey
                                     select u.Account).FirstOrDefault();

                    if (accountId > 0)
                    {
                        var loggedUser = GetLoggedUser(model.username, model.email, accountId, _dbContext);

                        if (loggedUser != null && loggedUser.Id > 0)
                        {
                            string currentDecision = GetCollaboratorApprovalDecision(model.username, approval.ID, loggedUser.Id, approval.CurrentPhase, _dbContext);

                            if (currentDecision != model.decision)
                            {
                                var decisionDetails = (from ad in _dbContext.ApprovalDecisions
                                                       where ad.Key.ToLower() == model.decision.ToLower().Trim()
                                                       select ad).FirstOrDefault();

                                if (decisionDetails != null)
                                {
                                    var decision = !string.IsNullOrEmpty(model.username) ? approval.ApprovalCollaboratorDecisions.FirstOrDefault(acd => acd.Collaborator == loggedUser.Id && acd.Phase == approval.CurrentPhase)
                                                                                        : approval.ApprovalCollaboratorDecisions.FirstOrDefault(acd => acd.ExternalCollaborator == loggedUser.Id && acd.Phase == approval.CurrentPhase);

                                    var approvalsoad = approval.ApprovalCollaborators.FirstOrDefault(acd => acd.Collaborator == loggedUser.Id && acd.Phase == approval.CurrentPhase);

                                    if (decision != null)
                                    {
                                        decision.Decision = decisionDetails.ID;
                                        approvalsoad.SOADState = 3;
                                        _dbContext.SaveChanges();

                                        ApprovalWorkflowBL.CheckApprovalShouldMoveToNextPhase(new List<int> { approval.ID }, null, null, decision.ID);

                                        var approvalDetails = new JobStatusDetails()
                                        {
                                            approvalGuid = approval.Guid,
                                            approvalStatus = decisionDetails.Name,
                                            approvalStatusKey = decisionDetails.Key
                                        };

                                        ApprovalBL.PushStatus(loggedUser, approvalDetails, accountId, _dbContext, approval.CurrentPhase);
                                    }
                                }
                                else
                                {
                                    response.warningMessages.Add(WarningMessages.WrongDecisionKey);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errors = String.Format("Push approval status failed in UpdateApprovalVersion, with error:{0}", ex);
            }
            return response;
        }

        private string GetCollaboratorApprovalDecision(string username, int approvalId, int collaboratorId, int? currentPhase, DbContextBL _dbContext)
        {
            var collaboratorDecision = !string.IsNullOrEmpty(username) ? (from acl in _dbContext.ApprovalCollaboratorRoles
                                                                          join ac in _dbContext.ApprovalCollaborators on acl.ID equals ac.ApprovalCollaboratorRole
                                                                          join acd in _dbContext.ApprovalCollaboratorDecisions
                                                                          on new { a = ac.Approval, cl = (int?)ac.Collaborator } equals new { a = acd.Approval, cl = (int?)acd.Collaborator.Value }
                                                                          where ac.Approval == approvalId && ac.Collaborator == collaboratorId && acl.Key == "ANR" && acd.Phase == currentPhase
                                                                          select acd.Decision).FirstOrDefault()
                                                                         : (from acl in _dbContext.ApprovalCollaboratorRoles
                                                                            join sa in _dbContext.SharedApprovals on acl.ID equals sa.ApprovalCollaboratorRole
                                                                            join acd in _dbContext.ApprovalCollaboratorDecisions
                                                                            on new { a = sa.Approval, cl = (int?)sa.ExternalCollaborator } equals new { a = acd.Approval, cl = (int?)acd.Collaborator.Value }
                                                                            where sa.Approval == approvalId && sa.ExternalCollaborator == collaboratorId && acl.Key == "ANR" && acd.Phase == currentPhase
                                                                            select acd.Decision).FirstOrDefault();
            return (collaboratorDecision != null)
                ? (from ad in _dbContext.ApprovalDecisions
                   where ad.ID == collaboratorDecision
                   select ad.Key).FirstOrDefault()
                : string.Empty;
        }

        private UserDetails GetLoggedUser(string username, string email, int account, DbContextBL _dbContext)
        {
            return !string.IsNullOrEmpty(username) ? (from u in _dbContext.Users
                                                      join us in _dbContext.UserStatus on u.Status equals us.ID
                                                      where u.Account == account && u.Username == username.ToLower() && us.Key == "A"
                                                      select new UserDetails
                                                      {
                                                          Id = u.ID,
                                                          Username = u.Username,
                                                          Email = u.EmailAddress
                                                      }).FirstOrDefault()
                                                     : (from u in _dbContext.ExternalCollaborators
                                                        where u.Account == account && u.EmailAddress == email.ToLower() && u.IsDeleted == false
                                                        select new UserDetails
                                                        {
                                                            Id = u.ID,
                                                            Username = string.Empty,
                                                            Email = u.EmailAddress
                                                        }).FirstOrDefault();
        }

        private void PopulateNonPhasedApproval(int appId, GMGColorDAL.User loggedUser,Account account, int loggedUserLocale, ref GetApprovalActivityResponse approval)
        {
            var versionModel = ApprovalBL.PopulateVersionDetailsModel(appId, loggedUser, account, loggedUserLocale, _dbContext, true);
            var currentApproval = new GetApprovalActivityResponse
            {
                guid = versionModel.objApprovalVersion.Guid,
                version = versionModel.Version,
                fileName = versionModel.objApprovalVersion.FileName
            };

            approval.collaborators.AddRange(GetCollaboratorsActivities(versionModel.ColloaboratorActivities,
                                                                        versionModel.ExternalColloaboratorActivities));
        }

        private List<CollaboratorActivity> GetCollaboratorsActivities(List<ApprovalDetailsCollaboratorActivity> internalColloaboratorActivities, List<ApprovalDetailsCollaboratorActivity> externalColloaboratorActivities)
        {
            var collaborators = new List<CollaboratorActivity>();
            foreach (var collab in internalColloaboratorActivities)
            {
                CollaboratorActivity colAct = new CollaboratorActivity
                {
                    annotations = collab.Annotations,
                    replies = collab.Replies,
                    decision = collab.DecisionMade,
                    isExternal = false,
                    isPDM = collab.IsPrimaryDecisionMaker,
                    name = collab.Name,
                    email = collab.EmailAddress,
                    role = collab.Role,
                    sent = collab.Sent.ToString("o"),
                    opened = collab.OpenedDate.ToString("o"),
                    username = collab.UserName
                };
                collaborators.Add(colAct);
            }

            foreach (var collab in externalColloaboratorActivities)
            {
                CollaboratorActivity colAct = new CollaboratorActivity
                {
                    annotations = collab.Annotations,
                    replies = collab.Replies,
                    decision = collab.DecisionMade,
                    isExternal = true,
                    isPDM = collab.IsPrimaryDecisionMaker,
                    name = collab.Name,
                    email = collab.EmailAddress,
                    role = collab.Role,
                    sent = collab.Sent.ToString("o"),
                    opened = collab.OpenedDate.ToString("o"),
                    username = collab.UserName
                };
                collaborators.Add(colAct);
            }
            return collaborators;
        }

        /// <summary>
        /// Add internal or external collaborators to in progress or not yet stared phase
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private object AddCollaboratorsToApprovalPhase(AddCollaboratorsToApprovalPhaseRequest model)
        {
            var userTobeAdded = model.phaseCollaborators.Where(c => c.toBeRemoved == false).Select(c => c).ToList();
            if (userTobeAdded.Count() == 0)
            {
                return new AddCollaboratorsToApprovalPhaseResponse() { guid = model.guid };
            }
                var approvalId = _dbContext.Approvals.Where(ap => ap.Guid == model.guid).FirstOrDefault().ID;
            var phaseId = ApiBlHelpers.CheckApprovalWorkflowHasPhase(model.guid, model.phaseGuid, _dbContext);
            var newExtCollaborators = new List<NewExternalUserPermissions>();
            var ownerDetails = ApiBlHelpers.GetUserDetails(model.username, model.sessionKey, _dbContext);

            foreach (var collaborator in model.phaseCollaborators)
            {
                if (collaborator.toBeRemoved == false)
                { 

                    var collabRoleId = _dbContext.ApprovalCollaboratorRoles.Where(r => r.Key == collaborator.userRole).FirstOrDefault().ID;

                if (!collaborator.isExternal)
                {
                    BuildInternalCollaboratorDbData(approvalId, phaseId, collaborator, collabRoleId, ownerDetails.Account);
                }
                else
                {
                    if (_dbContext.ExternalCollaborators.Any(exc => exc.EmailAddress == collaborator.email))
                    {
                        BuildExternalCollaboratorDbData(approvalId, phaseId, collaborator, collabRoleId);
                    }
                    else
                    {
                        newExtCollaborators.Add(CreateNewExternalCollaborator(collaborator, phaseId, collabRoleId, ownerDetails.Account1.Locale));
                    }
                }
            }
            }

            PermissionsBL.AddApiExternalCollaboratorsAndDecisions(newExtCollaborators, approvalId, ownerDetails.ID, ownerDetails.Account1.ID, _dbContext);

            _dbContext.SaveChanges();          

            CreateNotificationItems(model, approvalId, phaseId);
            return new AddCollaboratorsToApprovalPhaseResponse() { guid = model.guid };
        }

        private object RemoveCollaboratorsToApprovalPhase(AddCollaboratorsToApprovalPhaseRequest model)
        {
            
                var approvalId = _dbContext.Approvals.Where(ap => ap.Guid == model.guid).FirstOrDefault().ID;
                var phaseId = ApiBlHelpers.CheckApprovalWorkflowHasPhase(model.guid, model.phaseGuid, _dbContext);
                var newExtCollaborators = new List<NewExternalUserPermissions>();
                var ownerDetails = ApiBlHelpers.GetUserDetails(model.username, model.sessionKey, _dbContext);

                //remove internal collaborators
                var toBeRemovedUsers = model.phaseCollaborators.Where(c => c.toBeRemoved && c.userName != null).Select(c => c.userName.ToLower()).ToList();
                var internalCollaborators = (from u in _dbContext.Users
                                             join us in _dbContext.UserStatus on u.Status equals us.ID
                                             where toBeRemovedUsers.Contains(u.Username) && u.Account == ownerDetails.Account1.ID && us.Key == "A"
                                             select u.ID).ToList();

                _dbContext.ApprovalCollaborators.RemoveRange(_dbContext.ApprovalCollaborators.Where(c => toBeRemovedUsers.Contains(c.User.Username) && c.Approval == approvalId && c.Phase == phaseId));

                //remove external collaborators
                var externalsToDelete = model.phaseCollaborators.Where(c => c.isExternal && !string.IsNullOrEmpty(c.email) && c.toBeRemoved).Select(ec => ec.email.ToLower()).ToList();
                var externalCollaborators = _dbContext.ExternalCollaborators.Where(ec => externalsToDelete.Contains(ec.EmailAddress) && ec.IsDeleted == false && ec.Account == ownerDetails.Account1.ID).Select(ec => ec.ID).ToList();
                _dbContext.SharedApprovals.RemoveRange(_dbContext.SharedApprovals.Where(sa => externalCollaborators.Contains(sa.ExternalCollaborator) && sa.Approval == approvalId && sa.Phase == phaseId));

                //Delete internal/external collaborators decision for current approval
                _dbContext.ApprovalCollaboratorDecisions.RemoveRange(_dbContext.ApprovalCollaboratorDecisions.Where(acd => (externalCollaborators.Contains(acd.ExternalCollaborator.Value) || internalCollaborators.Contains(acd.Collaborator.Value)) && acd.Approval == approvalId && acd.Phase == phaseId));

                _dbContext.SaveChanges();

            return new AddCollaboratorsToApprovalPhaseResponse() { guid = model.guid };
        }


        /// <summary>
        /// If external collaborator does not exist in the database create it
        /// </summary>
        /// <param name="newExtCollab">Object supplied by the API</param>
        /// <param name="phaseId"></param>
        /// <param name="collabRoleId"></param>
        /// <param name="locale"></param>
        /// <returns></returns>
        private NewExternalUserPermissions CreateNewExternalCollaborator(PhaseCollaboratorsRequest newExtCollab, int phaseId, int collabRoleId, int locale)
        {
            return new NewExternalUserPermissions()
            {
                Email = newExtCollab.email,
                FullName = (string.IsNullOrEmpty(newExtCollab.firstName) || string.IsNullOrEmpty(newExtCollab.lastName)) 
                                ? 
                                newExtCollab.email + " " + "." : newExtCollab.firstName + " " + newExtCollab.lastName,
                Phase = phaseId,
                Role = collabRoleId,
                Locale = locale
            };
        }

        /// <summary>
        /// Create notification item if users are added to the current phase
        /// </summary>
        /// <param name="model"></param>
        /// <param name="approvalId"></param>
        /// <param name="phaseId"></param>
        private void CreateNotificationItems(AddCollaboratorsToApprovalPhaseRequest model, int approvalId, int phaseId)
        {
            if (_dbContext.Approvals.Where(ap => ap.ID == approvalId).FirstOrDefault().CurrentPhase == phaseId)
            {
                var eventCreator = ApiBlHelpers.GetUserDetails(model.username, model.sessionKey, _dbContext);
                foreach (var collaborator in model.phaseCollaborators)
                {
                    var approvalWasShared = new ApprovalWasShared
                    {
                        EventCreator = eventCreator.ID,
                        ApprovalsIds = new[] { approvalId }
                    };

                    if (collaborator.isExternal)
                    {
                        approvalWasShared.ExternalRecipient = _dbContext.ExternalCollaborators.Where(exc => exc.EmailAddress == collaborator.email).FirstOrDefault().ID;
                    }
                    else
                    {
                        approvalWasShared.InternalRecipient = _dbContext.Users.Where(c => c.Username == collaborator.userName).FirstOrDefault().ID;
                    }

                    NotificationServiceBL.CreateNotification(approvalWasShared, eventCreator, _dbContext);
                }
            }
        }

        /// <summary>
        /// Create the Approval Collaborator and Approval Collaborator Decision objects for internal collaborators and add them to the context
        /// </summary>
        /// <param name="approvalId"></param>
        /// <param name="phaseId"></param>
        /// <param name="collaborator"></param>
        /// <param name="collabRoleId"></param>
        private void BuildInternalCollaboratorDbData(int approvalId, int phaseId, PhaseCollaboratorsRequest collaborator, int collabRoleId, int accountID)
        {
			var internalUserId = _dbContext.Users.Where(c => c.Username == collaborator.userName && c.Account == accountID).FirstOrDefault().ID;

			var appCollabToAdd = new ApprovalCollaborator()
            {
                Approval = approvalId,
                Phase = phaseId,
                AssignedDate = DateTime.UtcNow,
                ApprovalCollaboratorRole = collabRoleId,
                Collaborator = internalUserId
			};
            _dbContext.ApprovalCollaborators.Add(appCollabToAdd);

            AddCollaboratorDecision(approvalId, phaseId, internalUserId, collaborator.userRole, collaborator.isExternal);
        }

        /// <summary>
        /// Create the Approval Collaborator and Approval Collaborator Decision objects for external collaborators and add them to the context
        /// </summary>
        /// <param name="approvalId"></param>
        /// <param name="phaseId"></param>
        /// <param name="collaborator"></param>
        /// <param name="collabRoleId"></param>
        private void BuildExternalCollaboratorDbData(int approvalId, int phaseId, PhaseCollaboratorsRequest collaborator, int collabRoleId)
        {
			var externalUserId = _dbContext.ExternalCollaborators.Where(exc => exc.EmailAddress == collaborator.email).FirstOrDefault().ID;

			var appExtCollabToAdd = new SharedApproval()
            {
                ApprovalCollaboratorRole = collabRoleId,
                IsSharedURL = true,
                IsSharedDownloadURL = true,
                IsExpired = false,
                SharedDate = DateTime.UtcNow,
                ExpireDate = DateTime.UtcNow.AddDays(7),
                Phase = phaseId,
                Approval = approvalId,
                ExternalCollaborator = externalUserId
			};

            _dbContext.SharedApprovals.Add(appExtCollabToAdd);

            AddCollaboratorDecision(approvalId, phaseId, externalUserId, collaborator.userRole, collaborator.isExternal);
        }


        /// <summary>
        /// Create the Approval Collaborator object
        /// </summary>
        /// <param name="approvalId"></param>
        /// <param name="phaseId"></param>
        /// <param name="userName"></param>
        /// <param name="userRole"></param>
        /// <param name="isExternal"></param>
        private void AddCollaboratorDecision(int approvalId, int phaseId, int userId, string userRole, bool isExternal)
        {
            if (userRole == Enum.GetName(typeof(UserRole), 3))
            {
                var appCollabDecisionToAdd = new ApprovalCollaboratorDecision()
                {
                    Approval = approvalId,
                    Phase = phaseId,
                    AssignedDate = DateTime.UtcNow,
                    Collaborator = isExternal ? null : (int?) userId,
					ExternalCollaborator = isExternal ? (int?) userId : null
				};
                _dbContext.ApprovalCollaboratorDecisions.Add(appCollabDecisionToAdd);
            }
        }

        #endregion

        #region Public Methods

        public object UpdateApprovalDecision(UpdateApprovalDecisionRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            errors = string.Empty;
            var validator = new ApprovalDecisionValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return UpdateApprovalDecision(model, out errors);
            }
            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Gets the annotation report snapshot as a bytearray, or returns the error list in case of error
        /// </summary>
        /// <param name="model"></param>
        /// <param name="annotationAction"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetAnnotationReport(AnnotationReportRequest model, string annotationAction, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new AnnotationReportValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GenerateReport(model, annotationAction);
            }
            else
            {
                return validator.GetRuleViolations();
            }
        }

        public object GetAnnotationReportUrl(AnnotationReportRequest model, string annotationAction, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new AnnotationReportValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetAnnotationReportUrl(model, annotationAction); 
            }
            return validator.GetRuleViolations();
        }

        public object GetJobAnnotationReportUrl(AnnotationReportRequest model, string annotationAction, out HttpStatusCodeEnum returnStatusCode)
        {
            var jobGuid = (from j in _dbContext.Jobs
                           join ap in _dbContext.Approvals on j.ID equals ap.Job
                           where ap.Guid == model.guid
                           select j.Guid).FirstOrDefault();

            model.guid = jobGuid;
            var validator = new JobAnnotationReportValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetJobAnnotationReportUrl(model, annotationAction);
            }
            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Gets the annotation list model for the specified approval
        /// </summary>
        /// <param name="model"></param>
        /// <param name="reStatusCode"></param>
        /// <returns></returns>
        public object GetAnnotationList(AnnotationListRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new AnnotationListValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GenerateAnnotationList(model);
            }
            else
            {
                return validator.GetRuleViolations();
            }
        }

        /// <summary>
        /// Update the approval info like: deadline, name and collaborator's access
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object UpdateApprovalVersion(UpdateApprovalRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            var validator = new UpdateApprovalRequestValidator(model);
            returnStatusCode = validator.Validate(_dbContext);
            errors = string.Empty;

            if (validator.IsValid)
            {
                return UpdateApprovalVersion(model, out errors);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Update the approval phase deadline info like: deadline
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object UpdateApprovalPhaseDeadline(UpdateApprovalPhaseDeadlineRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            var validator = new UpdateApprovalPhaseDeadlineRequestValidator(model);
            returnStatusCode = validator.Validate(_dbContext);
            errors = string.Empty;

            if (validator.IsValid)
            {
                return UpdateApprovalPhaseDeadline(model, out errors);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Deleted the approval with the specified guid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object DeleteApproval(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new DeleteApprovalValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return DeleteApproval(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Restore the specified approval
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object RestoreApproval(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new RestoreApprovalValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return RestoreApproval(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get approval with the specified guid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetApproval(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetApprovalValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetApproval(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get the approval URL, for logged user, that points to ProofStudio
        /// </summary>
        /// <param name="model">Request model</param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetProofStudioURL(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetApprovalValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetProofStudioURL(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get the approval URL, for logged user, that points to ProofStudio
        /// </summary>
        /// <param name="model">Request model</param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetCompareVersionsProofStudioURL(ProofStudioCompareVersionsRequest model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetCompareVersionsValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetCompareVersionsProofStudioURL(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get approval with access for the specified guid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetApprovalWithAccess(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetApprovalValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetApprovalWithAccess(model);
            }

            return validator.GetRuleViolations();
        }

        /// <summary>
        /// Get approval with access for the specified guid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <returns></returns>
        public object GetApprovalActivity(CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new GetApprovalValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                return GetApprovalActivity(model);
            }

            return validator.GetRuleViolations();
        }

        public List<GetApprovalActivityResponse> GetApprovalActivityModel(List<int> appIds, CollaborateAPIIdentityModel model)
        {
            List<GetApprovalActivityResponse> approvalActivityModel = new List<GetApprovalActivityResponse>();

            var firstId = appIds.FirstOrDefault();

            var loggedUser = (from u in _dbContext.Users
                              join us in _dbContext.UserStatus on u.Status equals us.ID
                              let account = (from cas in _dbContext.CollaborateAPISessions
                                             join usr in _dbContext.Users on cas.User equals usr.ID
                                             join ac in _dbContext.Accounts on usr.Account equals ac.ID
                                             where cas.SessionKey == model.sessionKey
                                             select ac).FirstOrDefault()
                              where u.Account == account.ID && u.Username == model.username && us.Key == "A"
                              select new
                              {
                                  User = u,
                                  Account = account
                              }).FirstOrDefault();

            var loggedUserCollaborateRole = Role.GetRole(loggedUser.User.CollaborateRoleKey(_dbContext));

            foreach (var appId in appIds)
            {
                var approvalDetails = (from a in _dbContext.Approvals
                                        where a.ID == appId
                                        select new 
                                        {
                                            guid = a.Guid,
                                            version = a.Version,
                                            fileName = a.FileName,
                                            a.CurrentPhase
                                        }).FirstOrDefault();

                if (approvalDetails != null)
                {
                    var approval = new GetApprovalActivityResponse()
                    {
                        guid = approvalDetails.guid,
                        version = approvalDetails.version,
                        fileName = approvalDetails.fileName
                    };

                   var versionModel = new ApprovalDetailsModel.VersionDetailsModel();
                   if (approvalDetails.CurrentPhase == null)
                   {
                       PopulateNonPhasedApproval(appId, loggedUser.User, loggedUser.Account, loggedUser.User.Locale, ref approval);                      
                   }
                   else
                   {
                       PopulatePhasedApproval(appId, loggedUser.Account, loggedUser.User.ID, loggedUser.User.Locale, loggedUserCollaborateRole, loggedUser.Account.DateFormat1.Pattern, ref approval);                     
                   }
                   approvalActivityModel.Add(approval);
                }                                
            }

            return approvalActivityModel;
        }

        private AnnotationReportUrlResponse GetAnnotationReportUrl(AnnotationReportRequest model, string annotationAction)
        {
            var response = new AnnotationReportUrlResponse
            {
                url = GetAnnotationReportScreenshotURL(model, annotationAction)
            };

            return response;
        }

        private AnnotationReportUrlResponse GetJobAnnotationReportUrl(AnnotationReportRequest model, string annotationAction)
        {
            var response = new AnnotationReportUrlResponse
            {
                url = GetJobAnnotationReportScreenshotURL(model, annotationAction)
            };

            return response;
        }

        private void PopulatePhasedApproval(int appId, Account account, int loggedUserId, int loggedUserLocale, Role.RoleName loggedUserCollaborateRole, string loggedAccountDatePattern, ref GetApprovalActivityResponse approval)
        {
            var versionModel = ApprovalBL.GetApprovalCurrentVersion(appId, account, loggedUserId, loggedUserLocale, loggedUserCollaborateRole, _dbContext);

            foreach (var phase in versionModel.Phases)
            {
                var approvalPhase = new PhaseActivity()
                {
                    name = phase.Name,
                    guid = phase.Guid,
                    status = phase.Status,
                    deadline = phase.Deadline.HasValue ? GMGColorFormatData.GetFormattedDate(phase.Deadline.Value, loggedAccountDatePattern) : "-"
                };

                approvalPhase.collaborators.AddRange(GetCollaboratorsActivities(phase.PhaseVersionDetails.ColloaboratorActivities, 
                                                                                phase.PhaseVersionDetails.ExternalColloaboratorActivities));

                approval.phasesActivity.Add(approvalPhase);
            }
        }
		    
		 /// <summary>
        /// Add Collaborators to an In Progress or Not Yet Started Phase for an approval
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnStatusCode"></param>
        /// <param name="errors"></param>
        /// <returns></returns>
        public object AddCollaboratorsToApprovalPhaseRequest(AddCollaboratorsToApprovalPhaseRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            errors = string.Empty;
            var validator = new AddCollaboratorsToApprovalPhaseValidator(model);

            returnStatusCode = validator.Validate(_dbContext);
            if (validator.IsValid)
            {
                 var ResponseResult = AddCollaboratorsToApprovalPhase(model);

                var userTobeRemoved = model.phaseCollaborators.Where(c => c.toBeRemoved == true).Select(c => c).ToList();
                if (userTobeRemoved.Count() > 0)
                {
                    ResponseResult = RemoveCollaboratorsToApprovalPhase(model);
                }
                 return ResponseResult;
            }
            return validator.GetRuleViolations();
        }


        #endregion
    }
}
