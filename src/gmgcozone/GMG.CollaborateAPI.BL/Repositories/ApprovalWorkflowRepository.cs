﻿using System;
using System.Linq;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Model;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;
using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidattors;
using GMGColorBusinessLogic;
using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators;
using GMGColorDAL;
using System.Collections.Generic;
using GMGColorDAL.Common;

namespace GMG.CollaborateAPI.BL.Repositories
{
    public class ApprovalWorkflowRepository : IRepository
    {
        #region Fields

        private readonly DbContextBL _dbContext;

        #endregion

        #region Constructors 

        public ApprovalWorkflowRepository(DbContextBL context)
        {
            _dbContext = context;
        }

        #endregion

        #region Public Methods

        public object GetAllWorkflows(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new ApprovalWorkflowValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Ok;
                return GetAllWorkflows(model);
            }

            return validator.GetRuleViolations();
        }

        public object GetApprovalWorkflowDetails(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model, out HttpStatusCodeEnum returnStatusCode)
        {
            var validator = new ApprovalWorkflowValidator(model);
            returnStatusCode = validator.Validate(_dbContext);

            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Ok;
                return GetApprovalWorkflowDetails(model);
            }

            return validator.GetRuleViolations();
        }

        public object RerunFromThisPhase(GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models.RerunFromThisPhaseRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            var validator = new RerunFromThisPhaseValidator(model);
            errors = string.Empty;

            returnStatusCode = validator.Validate(_dbContext);
            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Ok;
                return RerunFromThisPhase(model.guid, model.phaseGuid, UserBL.GetUserByUserName(model.username, _dbContext), _dbContext, out errors);
            }
            return validator.GetRuleViolations();
        }

        public object CreateApprovalWorkflow(CreateApprovalWorkflowRequest model, out HttpStatusCodeEnum returnStatusCode, out string errors)
        {
            var validator = new CreateApprovalWorkflowValidator(model);
            errors = string.Empty;

            returnStatusCode = validator.Validate(_dbContext);
            if (validator.IsValid)
            {
                returnStatusCode = HttpStatusCodeEnum.Ok;
                return CreateApprovalWorkflow(model, out errors);
            }

            return validator.GetRuleViolations();
        }

        #endregion

        #region Private Methods

        private CreateApprovalWorkflowResponse CreateApprovalWorkflow(CreateApprovalWorkflowRequest model, out string errors)
        {
            errors = string.Empty;
            var workflowId = 0;
            var approvalWorkflow = new GMGColorDAL.CustomModels.ApprovalWorkflows.ApprovalWorkflowModel() { Name = model.workflowName, RerunWorkflow = model.rerunWorkflow };
            var account = ApiBlHelpers.GetUserDetails(model.username, model.sessionKey, _dbContext).Account1;

            User loggedUser = ApiBlHelpers.GetUserDetails(model.username, model.sessionKey, _dbContext);

            try
            {
                workflowId = ApprovalWorkflowBL.AddOrUpdateWorkflow(approvalWorkflow, loggedUser.Account, loggedUser.ID, _dbContext);
                if (workflowId != 0)
                {
                    var position = 1;
                    foreach (var phase in model.worflowPhases)
                    {
                        CreateApprovalWorkflowPhase(phase, workflowId, position, account, loggedUser.ID, out errors);
                        position++;
                    }
                }
            }
            catch (Exception ex)
            {
                errors = String.Format("Create approval workflow failed , with error {0} ", ex.Message);
            }

            return new CreateApprovalWorkflowResponse()
            {
                workflowGuid = GetApprovalWorkflowGuid(workflowId, account.ID),
            };
        }

        private void CreateApprovalWorkflowPhase(PhaseRequest phase, int workflowId, int phasePosition, Account account, int loggedUserId, out string errors)
        {
            errors = string.Empty;

            try
            {
                phase.deadLineDate = GMGColorFormatData.GetFormattedDate(phase.deadLineDate, account, _dbContext);
                var phaseModelToSave = new ApprovalPhase()
                {
                    ApprovalWorkflow = workflowId,
                    Name = phase.phaseName,
                    Position = phasePosition,
                    ShowAnnotationsToUsersOfOtherPhases = phase.showAnnotationsToUsersOfOtherPhases,
                    ShowAnnotationsOfOtherPhasesToExternalUsers = phase.showAnnotationsOfOtherPhasesToExternalUsers,
                    Deadline = GetDeadlineFormatedDate(phase, account.DateFormat1.Pattern, account.TimeFormat, account.TimeZone),
                    DecisionType = _dbContext.DecisionTypes.Where(d => d.Key == phase.selectedDecisionType).FirstOrDefault().ID,
                    Guid = Guid.NewGuid().ToString(),
                    ApprovalShouldBeViewedByAll = phase.approvalShouldBeViewedByAllCollaborators,
                    ApprovalTrigger = _dbContext.ApprovalDecisions.Where(d=>d.Key == phase.approvalTrigger).FirstOrDefault().ID
                };

                if (phase.automaticDeadLineUnit != null)
                {
                    phaseModelToSave.DeadlineUnit = _dbContext.PhaseDeadlineUnits.Where(u => u.Key == phase.automaticDeadLineUnit).FirstOrDefault().ID;
                    phaseModelToSave.DeadlineUnitNumber = phase.automaticDeadLineValue;
                    phaseModelToSave.DeadlineTrigger = _dbContext.PhaseDeadlineTriggers.Where(t => t.Key == phase.automaticDeadLineTrigger).FirstOrDefault().ID;
                }

                SetPhasePrimaryDecisionMaker(phase, phaseModelToSave);

                phaseModelToSave.CreatedBy = phaseModelToSave.ModifiedBy = loggedUserId;
                phaseModelToSave.CreatedDate = phaseModelToSave.ModifiedDate = DateTime.UtcNow;
                _dbContext.ApprovalPhases.Add(phaseModelToSave);
                _dbContext.SaveChanges();

                SaveApprovalCollaborators(phase.phaseCollaborators, phaseModelToSave.ID, account.ID, loggedUserId);
            }
            catch (Exception ex)
            {
                errors = String.Format("Create approval workflow phase, with error {0} ", ex.Message);
            }
        }

        private void SaveApprovalCollaborators(List<PhaseCollaboratorsRequest> phaseCollaborators, int phaseId, int accountID, int loggedUserId)
        {
            if (phaseCollaborators != null && phaseCollaborators.Count > 0)
            {
                foreach (var collaborator in phaseCollaborators)
                {
                    if (!collaborator.isExternal)
                    {
                        var internalCollabToSave = new ApprovalPhaseCollaborator()
                        {
                            Phase = phaseId,
                            Collaborator = _dbContext.Users.Where(u => u.Username == collaborator.userName).FirstOrDefault().ID,
                            ApprovalPhaseCollaboratorRole = (int)Enum.Parse(typeof(UserRole), collaborator.userRole),
                            CreatedBy = loggedUserId,
                            ModifiedBy = loggedUserId,
                            CreatedDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        _dbContext.ApprovalPhaseCollaborators.Add(internalCollabToSave);
                    }
                    else
                    {
                        var externalCollabToSave = new SharedApprovalPhase()
                        {
                            Phase = phaseId,
                            ExternalCollaborator = _dbContext.ExternalCollaborators.Where(u => u.EmailAddress == collaborator.email && u.Account == accountID).FirstOrDefault().ID,
                            ApprovalPhaseCollaboratorRole = (int)Enum.Parse(typeof(UserRole), collaborator.userRole),
                            CreatedBy = loggedUserId,
                            ModifiedBy = loggedUserId,
                            CreatedDate = DateTime.UtcNow,
                            ModifiedDate = DateTime.UtcNow
                        };
                        _dbContext.SharedApprovalPhases.Add(externalCollabToSave);
                    }
                }
            }

            _dbContext.SaveChanges();
        }

        private DateTime? GetDeadlineFormatedDate(PhaseRequest phase, string datePattern, int timeFormat, string timeZone)
        {
            if (!string.IsNullOrEmpty(phase.deadLineDate))
            {
                var deadLine = ApprovalBL.GetFormatedApprovalDeadline(phase.deadLineDate, phase.deadLineTime, phase.deadLineMeridian, datePattern, timeFormat);
                return GMGColorDAL.Common.GMGColorFormatData.SetUserTimeToUTC(deadLine, timeZone);
            }
            return null;
        }

        private void SetPhasePrimaryDecisionMaker(PhaseRequest phase, ApprovalPhase phaseModelToSave)
        {
            if (phase.phaseCollaborators != null && phase.phaseCollaborators.Count > 0 && phase.selectedDecisionType == DecisionType.PDM.ToString())
            {
                var pdmCollab = phase.phaseCollaborators.Where(c => c.userName == phase.decisionMaker).FirstOrDefault();
                var pdmId = _dbContext.Users.Where(u => u.Username == phase.decisionMaker).FirstOrDefault().ID;
                if (!pdmCollab.isExternal)
                {
                    phaseModelToSave.PrimaryDecisionMaker = pdmId;
                }
                else
                {
                    phaseModelToSave.ExternalPrimaryDecisionMaker = pdmId;
                }
            }
        }

        private string GetApprovalWorkflowGuid(int workflowId, int accountId)
        {
            return _dbContext.ApprovalWorkflows.Where(w => w.ID == workflowId && w.Account == accountId).FirstOrDefault().Guid;
        }

    	private RerunFromThisPhaseResponse RerunFromThisPhase(string approvalGuid, string phaseGuid, GMGColorDAL.User loggedUser, DbContextBL context, out string errors)
        {
            errors = string.Empty;
            var requestDetails = (from ap in context.Approvals
                                  join j in context.Jobs on ap.Job equals j.ID
                                  join ajw in context.ApprovalJobWorkflows on j.ID equals ajw.Job
                                  join apjp in context.ApprovalJobPhases on ajw.ID equals apjp.ApprovalJobWorkflow
                                  join app in context.ApprovalPhases on apjp.PhaseTemplateID equals app.ID
                                  where ap.Guid == approvalGuid && app.Guid == phaseGuid
                                  select new 
                                  { 
                                      PhaseId = apjp.ID,
                                      ApprovalId = ap.ID
                                  }).FirstOrDefault();

            var response = new RerunFromThisPhaseResponse();
            if (requestDetails != null)
            {
                errors = ApprovalWorkflowBL.RerunFromThisPhase(requestDetails.ApprovalId, requestDetails.PhaseId, loggedUser, context);
                response.guid = approvalGuid;
            }

            return response;
        }

        private object GetAllWorkflows(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIBaseModel model)
        {
            var workflows = (from aw in _dbContext.ApprovalWorkflows
                            join u in _dbContext.Users on aw.Account equals u.Account
                            join si in _dbContext.CollaborateAPISessions on u.ID equals si.User
                            where si.SessionKey == model.sessionKey
                            select new GMG.CollaborateAPI.BL.Models.Approval_Workflow_Model.ApprovalWorkflow()
                            {
                                name = aw.Name,
                                guid = aw.Guid
                            }).ToList();

            return new ListApprovalWorkflowsResponse(){ workflows = workflows};
        }

        private ApprovalWorkflowDetailsResponse GetApprovalWorkflowDetails(GMGColor.Areas.CollaborateAPI.Models.CollaborateAPIIdentityModel model)
        {
            var phases = (from ap in _dbContext.ApprovalPhases
                          join aw in _dbContext.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID

                          let triggerKey = (from pdt in _dbContext.PhaseDeadlineTriggers where ap.DeadlineTrigger == pdt.ID select pdt.Key).FirstOrDefault()
                          let unitKey = (from pdu in _dbContext.PhaseDeadlineUnits where ap.DeadlineUnit == pdu.ID select pdu.Key).FirstOrDefault()

                          where aw.Guid == model.guid
                          select new
                          {
                              id = ap.Guid,
                              name = ap.Name,
                              DeadlineUnit = unitKey,
                              DeadlineTrigger = triggerKey,
                              ap.DeadlineUnitNumber,
                              ap.Deadline
                          }).ToList();

            List<ApprovalWorkflowPhase> lstApprovalWorkflowPhase = new List<ApprovalWorkflowPhase>();
            if (phases != null && phases.Count > 0)
            {
                foreach(var obj in phases)
                {
                    ApprovalWorkflowPhase approvalwrkflowPhase = new ApprovalWorkflowPhase();

                    approvalwrkflowPhase.id = obj.id;
                    approvalwrkflowPhase.name = obj.name;
                    approvalwrkflowPhase.automaticDeadlineValue = obj.DeadlineUnitNumber;

                    if (obj.Deadline != null)
                    {
                        approvalwrkflowPhase.fixedDeadlineDatetime = obj.Deadline.ToString();
                    }
                    else if (obj.DeadlineTrigger != null && obj.DeadlineUnitNumber != null && obj.DeadlineUnit != null)
                    {

                        if (obj.DeadlineTrigger == 0)
                            approvalwrkflowPhase.automaticDeadlineType = "After Upload";
                        else if (obj.DeadlineTrigger == 1)
                            approvalwrkflowPhase.automaticDeadlineType = "Last Phase Completed";
                        else if (obj.DeadlineTrigger == 2)
                            approvalwrkflowPhase.automaticDeadlineType = "Approval Deadline";

                        if (obj.DeadlineUnit == 0)
                            approvalwrkflowPhase.automaticDeadlineUnit = "Hours";
                        else if (obj.DeadlineUnit == 1)
                            approvalwrkflowPhase.automaticDeadlineUnit = "Days";
                    }

                    lstApprovalWorkflowPhase.Add(approvalwrkflowPhase);
                }

            }
            return new ApprovalWorkflowDetailsResponse() { phases = lstApprovalWorkflowPhase };
        }

        #endregion
    }
}
