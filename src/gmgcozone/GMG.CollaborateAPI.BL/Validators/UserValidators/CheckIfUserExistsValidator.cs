﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class CheckIfUserExistsValidator : SessionValidatorBase
    {
        #region Properties

        private UserExistsRequest _model;

        #endregion

        #region Constructor

        public CheckIfUserExistsValidator(UserExistsRequest model)
            : base(model)
        {
            _model = model;
        }

        #endregion

        #region Validation

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);

            if (IsValid)
            {
                var account = (from cas in dbContext.CollaborateAPISessions
                               join u in dbContext.Users on cas.User equals u.ID
                               where cas.SessionKey == _model.sessionKey
                               select u.Account).FirstOrDefault();

                bool emailExists = (from u in dbContext.Users
                                    join us in dbContext.UserStatus on u.Status equals us.ID
                                    where u.Account == account &&
                                    u.EmailAddress.ToLower() == _model.email.ToLower().Trim() &&
                                    us.Key != "D"
                                    select u.ID).Any();

                if (!emailExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.EmailNotFound, "email"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
            }

            return returnCode;
        }

        #endregion
    }
}
