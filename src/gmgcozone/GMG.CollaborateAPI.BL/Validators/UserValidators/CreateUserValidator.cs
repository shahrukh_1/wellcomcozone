﻿using System;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using GMG.CollaborateAPI.BL.Models.UserModels;
using GMGColorBusinessLogic;
using GMGColorDAL;
using AppModule = GMG.CoZone.Common.AppModule;
using common = GMG.CoZone.Common;
using System.Text.RegularExpressions;

namespace GMG.CollaborateAPI.BL.Validators.UserValidators
{
    public class CreateUserValidator : SessionValidatorBase
    {
        private new readonly CreateUserRequest _model;

        public CreateUserValidator(CreateUserRequest model)
            : base(model)
        {
            _model = model;
        }

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);

            if (IsValid)
            {
                if (!ValidateRequiredFieldsOrUpdateUser(dbContext))
                {
                    return HttpStatusCodeEnum.Forbidden;
                }

                var account = GetAccountBySession(dbContext, _model.sessionKey);

                if (_model.IsSSOUser)
                {
                    var accSsoSettings = GetSSOAccountSettingsForUser(dbContext, account);
                    if (accSsoSettings == null)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NotSSOAccount, "isssouser"));
                        return HttpStatusCodeEnum.Forbidden;
                    }
                }

                var userNameExists = CheckIfUserNameExists(dbContext, account, _model.Email.ToLower().Trim());
                if (userNameExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNameInUse, "email"));
                    return HttpStatusCodeEnum.Forbidden;
                }

                var emailAlreadyExits = CheckIfEmailAlreadyExits(dbContext, account, _model.Email.ToLower().Trim());
                if (emailAlreadyExits)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.EmailInUse, "email"));
                    return HttpStatusCodeEnum.Forbidden;
                }

                // at least one permission has to be provided
                if (_model.Permissions == null || !_model.Permissions.Any())
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.PermissionsRequired, "permissions"));
                    return HttpStatusCodeEnum.BadRequest;
                }

                // check if the provided permissions are for the available modules
                var modules = GetAllModules();
                if (!_model.Permissions.Select(t => t.name.ToLower()).Intersect(modules).Any())
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.PermissionsRequired, "permissions"));
                    return HttpStatusCodeEnum.BadRequest;
                }

                // check the if max users aready reached
                var collaboratePlan = GetCollaboratePlan(dbContext, account);
                if (collaboratePlan != null && PlansBL.IsNewCollaborateBillingPlan(collaboratePlan))
                {
                    if (PlansBL.GetActiveUsersNr(account, AppModule.Collaborate, dbContext) > collaboratePlan.MaxUsers)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UsersLimitReached, ""));
                        return HttpStatusCodeEnum.Forbidden;
                    }
                }
            }

            return returnCode;
        }

        private bool ValidateRequiredFieldsOrUpdateUser(DbContextBL dbContext)
        {
            if (!_model.IsSSOUser)
            {
                // First name and last name are mandatory in case the user is not SSO enable user
                if (string.IsNullOrEmpty(_model.FirstName))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.FirstNameRequired, "firstname"));
                    return false;
                }
                if (string.IsNullOrEmpty(_model.LastName))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.LastNameRequired, "lastname"));
                    return false;
                }
                if (string.IsNullOrEmpty(_model.Password) || !Regex.IsMatch(_model.Password, common.Constants.PasswordValidDataPattern))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.PasswordRequired, "password"));
                    return false;
                }
            }
            else
            {
                // set first name and last name for the SSO enabled users (in case they are not filled already)
                if (string.IsNullOrEmpty(_model.FirstName))
                {
                    _model.FirstName = _model.Email;
                }
                if (string.IsNullOrEmpty(_model.LastName))
                {
                    _model.LastName = ".";
                }
                if (string.IsNullOrEmpty(_model.Password))
                {
                    _model.Password = User.GetNewEncryptedRandomPassword(dbContext);
                }
            }
            return true;
        }

        private static string[] GetAllModules()
        {
            var modules = new[]
            {
                AppModule.Collaborate.ToString().ToLower(),
                AppModule.Deliver.ToString().ToLower(),               
                AppModule.Admin.ToString().ToLower()               
            };
            return modules;
        }

        private static bool CheckIfEmailAlreadyExits(DbContextBL dbContext, int account, string email)
        {
            return (from u in dbContext.Users
                        join us in dbContext.UserStatus on u.Status equals us.ID
                        where u.Account == account && us.Key != "D" &&
                            u.EmailAddress.ToLower() == email
                        select u.ID).Any();
        }

        private static AccountSetting GetSSOAccountSettingsForUser(DbContextBL dbContext, int account)
        {
            var accountSettingsKey = Enum.GetName(typeof(AccountSettingsBL.AccountSettingsKeyEnum), AccountSettingsBL.AccountSettingsKeyEnum.SingleSignOn);
            return dbContext.AccountSettings.FirstOrDefault(acs => acs.Account == account && acs.Name == accountSettingsKey);
        }

        private static bool CheckIfUserNameExists(DbContextBL dbContext, int account, string email)
        {
            return (from u in dbContext.Users
                        join us in dbContext.UserStatus on u.Status equals us.ID
                        where u.Username.ToLower() == email && us.Key != "D" && u.Account == account
                        select u.ID).Any();
        }

        private static int GetAccountBySession(DbContextBL dbContext, string sessionKey)
        {
            return (from cas in dbContext.CollaborateAPISessions
                        join u in dbContext.Users on cas.User equals u.ID
                        where cas.SessionKey == sessionKey
                        select u.Account).FirstOrDefault();
        }

        private static BillingPlan GetCollaboratePlan(DbContextBL dbContext, int account)
        {
            return (from asp in dbContext.AccountSubscriptionPlans
                        join a in dbContext.Accounts on asp.Account equals a.ID
                        join bp in dbContext.BillingPlans on asp.BillingPlan.ID equals bp.ID
                        join bpt in dbContext.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                        join am in dbContext.AppModules on bpt.AppModule equals am.ID
                        where a.ID == account && am.Key == (int)AppModule.Collaborate
                        select bp).FirstOrDefault()
                        ??
                        (from spi in dbContext.SubscriberPlanInfoes
                        join a in dbContext.Accounts on spi.Account equals a.ID
                        join bp in dbContext.BillingPlans on spi.BillingPlan.ID equals bp.ID
                        join bpt in dbContext.BillingPlanTypes on bp.BillingPlanType.ID equals bpt.ID
                        join am in dbContext.AppModules on bpt.AppModule equals am.ID
                        where a.ID == account && am.Key == (int)AppModule.Collaborate
                        select bp).FirstOrDefault();
        }
    }
}
