﻿using System;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using GMG.CollaborateAPI.BL.Models.UserModels;
using GMGColorBusinessLogic;
using GMGColorDAL;
using AppModule = GMG.CoZone.Common.AppModule;
using common = GMG.CoZone.Common;
using System.Text.RegularExpressions;

namespace GMG.CollaborateAPI.BL.Validators.UserValidators
{
    public class CreateExternalUserValidator : SessionValidatorBase
    {
        private new readonly CreateExternalUserRequest _model;

        public CreateExternalUserValidator(CreateExternalUserRequest model)
            : base(model)
        {
            _model = model;
        }

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);

            if (IsValid)
            {
                if (!ValidateRequiredFieldsOrUpdateUser(dbContext))
                {
                    return HttpStatusCodeEnum.Forbidden;
                }

                var account = GetAccountBySession(dbContext, _model.sessionKey);

                var userNameExists = CheckIfUserNameExists(dbContext, account, _model.Email.ToLower().Trim());
                if (userNameExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNameInUse, "email"));
                    return HttpStatusCodeEnum.Forbidden;
                }
            }

            return returnCode;
        }

        private bool ValidateRequiredFieldsOrUpdateUser(DbContextBL dbContext)
        {

                // set first name and last name for the SSO enabled users (in case they are not filled already)
                if (string.IsNullOrEmpty(_model.FirstName))
                {
                    _model.FirstName = _model.Email;
                }
                if (string.IsNullOrEmpty(_model.LastName))
                {
                    _model.LastName = ".";
                }
            return true;
        }

        private static bool CheckIfUserNameExists(DbContextBL dbContext, int account, string email)
        {
            return (from ex in dbContext.ExternalCollaborators
                    where ex.EmailAddress.ToLower() == email && !ex.IsDeleted && ex.Account == account
                    select ex.ID).Any();
        }

        private static int GetAccountBySession(DbContextBL dbContext, string sessionKey)
        {
            return (from cas in dbContext.CollaborateAPISessions
                    join u in dbContext.Users on cas.User equals u.ID
                    where cas.SessionKey == sessionKey
                    select u.Account).FirstOrDefault();
        }

    }
}
