﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMGColorDAL.CustomModels;

namespace GMG.CollaborateAPI.BL.Validators.Approval_Validators
{
    public class GetApprovalValidator : SessionValidatorBase
    {
         #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public GetApprovalValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                Guid parsedGuid = new Guid();

                if (string.IsNullOrEmpty(_model.guid) ||
                    // When the URL is not properly formated and approval GUID is missing or not a GUID
                    !Guid.TryParse(_model.guid, out parsedGuid))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalGuidMissing, "guid"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
                else
                {

                    var approvalExists = dbContext.Approvals.Any(t => t.Guid == _model.guid);

                    if (!approvalExists)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                    else
                    {
                        //get user rights info
                        var loggedUser = (from u in dbContext.Users
                                          join us in dbContext.UserStatus on u.Status equals us.ID
                                          let accountId = (from cas in dbContext.CollaborateAPISessions
                                                           join usr in dbContext.Users on cas.User equals usr.ID
                                                           where cas.SessionKey == _model.sessionKey
                                                           select usr.Account).FirstOrDefault()
                                          where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                          select new
                                          {
                                              u.ID,
                                              Account = accountId
                                          }).FirstOrDefault();

                        if (loggedUser != null)
                        {
                            var collaborateSettings = new AccountSettings.CollaborateGlobalSettings(loggedUser.Account, 2, false, dbContext);

                            if (!string.IsNullOrEmpty(_model.guid))
                            {
                                //check if user has access rights to this job
                                var approval = (from ap in dbContext.Approvals
                                                join j in dbContext.Jobs on ap.Job equals j.ID
                                                let isCollaborator = (from acl in dbContext.ApprovalCollaborators
                                                                      where ap.ID == acl.Approval && acl.Collaborator == loggedUser.ID && acl.Phase == ap.CurrentPhase
                                                                      select acl.ID).Any()

                                                where ap.Guid == _model.guid && (isCollaborator || loggedUser.ID == j.JobOwner)
                                                select new
                                                {
                                                    ap.IsDeleted
                                                }).FirstOrDefault();
                                //user must be owner on the last version or admin with access rights
                                if (approval == null && !collaborateSettings.ShowAllFilesToAdmins)
                                {
                                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNoAccess, "guid"));
                                    returnCode = HttpStatusCodeEnum.Forbidden;
                                }
                                else if (approval != null && approval.IsDeleted)
                                {
                                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalIsDeleted, "guid"));
                                    returnCode = HttpStatusCodeEnum.Forbidden;
                                }
                            }
                        }
                        else
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                            returnCode = HttpStatusCodeEnum.NotFound;
                        }
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
