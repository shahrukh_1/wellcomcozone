﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using AppModule = GMG.CoZone.Common.AppModule;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class CreateApprovalRequestValidator : SessionValidatorBase
    {  
        #region Properties

        private UploadApprovalFileRequest _model;

        private readonly decimal _uploadedFileLength;

        #endregion

        #region Constructor

        public CreateApprovalRequestValidator(UploadApprovalFileRequest model, decimal fileLength) :
            base(model)
        {
            _model = model;
            _uploadedFileLength = fileLength;
        }

        #endregion


        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {              
                if(!dbContext.CollaborateAPIJobMetadatas.Any(t => t.ResourceID == _model.resourceID.Trim()))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.MetadataNotFound, "resourceID"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }

                var loggedUser = (from u in dbContext.Users
                                  join us in dbContext.UserStatus on u.Status equals us.ID
                                  let account = (from cas in dbContext.CollaborateAPISessions
                                                 join usr in dbContext.Users on cas.User equals usr.ID
                                                 join ac in dbContext.Accounts on usr.Account equals ac.ID
                                                 where cas.SessionKey == _model.sessionKey
                                                 select ac).FirstOrDefault()
                                  where u.Account == account.ID && u.Username == _model.username && us.Key == "A"
                                  select u).FirstOrDefault();
                if (loggedUser != null)
                {
                    var loggedAccount = (from a in dbContext.Accounts where a.ID == loggedUser.Account select a).FirstOrDefault();

                    //Check for account subscription
                    var warningMessage = PlansBL.CheckIfJobSubmissionAllowed(loggedAccount, loggedUser, AppModule.Collaborate, 1, dbContext, loggedAccount.GetBillingPlanByAppModule(AppModule.Collaborate, dbContext), _uploadedFileLength, true);
                    if (!string.IsNullOrEmpty(warningMessage))
                    {
                        _ruleViolations.Add(new RuleViolation(warningMessage, ""));
                        returnCode = HttpStatusCodeEnum.Forbidden;
                    }
                }
                else
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
               
            }
            
            return returnCode;
        }

        #endregion
    }
}
