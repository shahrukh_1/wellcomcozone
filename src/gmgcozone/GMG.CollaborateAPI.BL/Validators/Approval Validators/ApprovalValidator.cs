﻿using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using System.Linq;

namespace GMG.CollaborateAPI.BL.Validators.Approval_Validators
{
    public class ApprovalValidator : CollaborateDashboardAccessValidator
    {

        #region Proprieties
        private new CollaborateAPIIdentityModel _model;
        #endregion

        #region Constructor
        public ApprovalValidator(CollaborateAPIIdentityModel model):
            base(model)
        {
            _model = model;
        }
        #endregion

        #region Validate
        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode =  base.Validate(dbContext);

            if (IsValid)
            {
                var approvalExists = dbContext.Approvals.Any(ap => ap.Guid == _model.guid);
                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
            }

            return returnCode;
        }
        #endregion
    }
}
