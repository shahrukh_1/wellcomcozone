﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class DeleteApprovalValidator : SessionValidatorBase
    {
        #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public DeleteApprovalValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var approvalExists = dbContext.Approvals.Any(t => t.Guid == _model.guid);

                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    //get user rights info
                    var ownerInfo = UserBL.GetJobOwnerInfo(dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == _model.sessionKey).User, dbContext);

                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                      let account = (from cas in dbContext.CollaborateAPISessions
                                                     join usr in dbContext.Users on cas.User equals usr.ID
                                                     join ac in dbContext.Accounts on usr.Account equals ac.ID
                                                     where cas.SessionKey == _model.sessionKey
                                                     select ac).FirstOrDefault()
                                      where u.Account == account.ID && u.Username == _model.username && us.Key == "A"
                                      select u).FirstOrDefault();

                    //get last version that is not deleted for current job
                    var approval = (from ap in dbContext.Approvals
                                    where ap.Guid == _model.guid && ap.IsDeleted == false && ap.Owner == loggedUser.ID
                                    select ap).FirstOrDefault();

                    //user must be owner on the last version or admin with access rights
                    if (approval == null && !ownerInfo.CanViewAllFiles)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserIsNoVersOwner, "guid"));
                        returnCode = HttpStatusCodeEnum.Forbidden;
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
