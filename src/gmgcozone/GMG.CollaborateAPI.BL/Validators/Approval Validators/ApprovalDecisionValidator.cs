﻿using System.Linq;
using GMG.CollaborateAPI.BL.Models.Approval_Models;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace GMG.CollaborateAPI.BL.Validators.Approval_Validators
{
    public class ApprovalDecisionValidator : SessionValidatorBase
    {
        #region Properties

        private UpdateApprovalDecisionRequest _model;

        #endregion

        #region Constructor

        public ApprovalDecisionValidator(UpdateApprovalDecisionRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var approvalExists = dbContext.Approvals.Any(t => t.Guid == _model.guid);

                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    if (!string.IsNullOrEmpty(_model.username))
                    {
                        var loggedUser = (from u in dbContext.Users
                                          join us in dbContext.UserStatus on u.Status equals us.ID
                                          let accountId = (from cas in dbContext.CollaborateAPISessions
                                                           join usr in dbContext.Users on cas.User equals usr.ID
                                                           where cas.SessionKey == _model.sessionKey
                                                           select usr.Account).FirstOrDefault()
                                          where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                          select u).FirstOrDefault();

                        if (loggedUser != null)
                        {
                            //get last version that is not deleted for current job
                            var collaborator = (from ap in dbContext.Approvals
                                                join acl in dbContext.ApprovalCollaborators on ap.ID equals acl.Approval
                                                where
                                                    ap.Guid == _model.guid && ap.IsDeleted == false &&
                                                    (acl.Collaborator == loggedUser.ID || ap.Creator == loggedUser.ID) && acl.Phase == ap.CurrentPhase
                                                select new
                                                {
                                                    acl.ApprovalCollaboratorRole
                                                }).FirstOrDefault();

                            if (collaborator != null)
                            {
                                var loggedUserCollaborateRole = Role.GetRole(collaborator.ApprovalCollaboratorRole, dbContext);

                                //Check the user role on collaborate dashboard
                                if (loggedUserCollaborateRole == Role.RoleName.AccountViewer ||
                                    loggedUserCollaborateRole == Role.RoleName.AccountRetoucher ||
                                    loggedUserCollaborateRole == Role.RoleName.AccountContributor)
                                {
                                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UpdateActionNotAllowed, ""));
                                    returnCode = HttpStatusCodeEnum.Forbidden;
                                }
                            }
                            else
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNoAccess, "guid"));
                                _ruleViolations.Add(new RuleViolation(string.Format("For session {0}, userId {1},  username {2} and guid {3}",
                                   _model.sessionKey, loggedUser.ID, _model.username, _model.guid), "App Params"));
                                returnCode = HttpStatusCodeEnum.Forbidden;
                            }
                        }
                        else
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                            returnCode = HttpStatusCodeEnum.NotFound;
                        }
                    }
                    else if (!string.IsNullOrEmpty(_model.email))
                    {
                        var loggedUser = (from u in dbContext.ExternalCollaborators
                                          let accountId = (from cas in dbContext.CollaborateAPISessions
                                                           join us in dbContext.Users on cas.User equals us.ID
                                                           where cas.SessionKey == _model.sessionKey
                                                           select us.Account).FirstOrDefault()
                                          where u.Account == accountId && u.EmailAddress == _model.email.ToLower() && u.IsDeleted == false
                                          select u).FirstOrDefault();

                        if (loggedUser != null)
                        {
                            //get last version that is not deleted for current job
                            var collaborator = (from ap in dbContext.Approvals
                                                join sa in dbContext.SharedApprovals on ap.ID equals sa.Approval
                                                where
                                                    ap.Guid == _model.guid && ap.IsDeleted == false &&
                                                    sa.ExternalCollaborator == loggedUser.ID
                                                select ap.ID).Any();

                            if (!collaborator)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNoAccess, "guid"));
                                returnCode = HttpStatusCodeEnum.Forbidden;
                            }
                        }
                        else
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ExternalUserNotFound, "email"));
                            returnCode = HttpStatusCodeEnum.NotFound;
                        }
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
