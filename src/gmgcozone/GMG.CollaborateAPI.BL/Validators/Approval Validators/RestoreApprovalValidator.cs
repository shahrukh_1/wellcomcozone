﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators.Approval_Validators
{
    public class RestoreApprovalValidator : SessionValidatorBase
    {
        #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public RestoreApprovalValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var approvalExists = dbContext.Approvals.Any(t => t.Guid == _model.guid && t.IsDeleted);

                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.DeleteApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    //get user rights info
                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                      let accountId = (from cas in dbContext.CollaborateAPISessions
                                                       join usr in dbContext.Users on cas.User equals usr.ID
                                                       where cas.SessionKey == _model.sessionKey
                                                       select usr.Account).FirstOrDefault()
                                      where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                      select new
                                      {
                                          u.ID,
                                          Account = accountId
                                      }).FirstOrDefault();

                    if (loggedUser != null)
                    {
                        //get last version that is not deleted for current job
                        var isApprovalOwner = (from ap in dbContext.Approvals
                                                join j in dbContext.Jobs on ap.Job equals j.ID
                                                where ap.Guid == _model.guid && (loggedUser.ID == ap.Owner || loggedUser.ID == j.JobOwner)
                                                select ap.ID).Any();
                   
                        //user must be owner on the last version or admin with access rights
                        if (!isApprovalOwner)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserIsNoVersOwner, "guid"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
