﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;
using AppModule = GMG.CoZone.Common.AppModule;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class CreateApprovalMetadataRequestValidator : SessionValidatorBase
    {
        #region Properties

        private CreateApprovalRequest _model;
        private string _workflowGuid;
        private string _checklistGuid;


        #endregion

        #region Constructor

        public CreateApprovalMetadataRequestValidator(CreateApprovalRequest model, string workflowGuid = null, string checklistGuid = null) :
            base(model)
        {
            _model = model;
            _workflowGuid = workflowGuid;
            _checklistGuid = checklistGuid;

        }

        #endregion


        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var loggedUser = (from u in dbContext.Users
                                  join us in dbContext.UserStatus on u.Status equals us.ID
                                  let accountId = (from cas in dbContext.CollaborateAPISessions
                                                   join usr in dbContext.Users on cas.User equals usr.ID
                                                   where cas.SessionKey == _model.sessionKey
                                                   select usr.Account).FirstOrDefault()
                                  where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                  select u).FirstOrDefault();

                if (loggedUser != null)
                {
                    var loggedUserCollaborateRole = Role.GetRole(loggedUser.CollaborateRoleKey(dbContext));

                    if (!string.IsNullOrEmpty(_model.folder))
                    {
                        //check if user has access to the specified folder
                        var hasAcess = (from f in dbContext.Folders
                                        join fc in dbContext.FolderCollaborators on f.ID equals fc.Folder
                                        where f.Guid == _model.folder && fc.Collaborator == loggedUser.ID
                                        select f.ID).Any();
                        if (!hasAcess)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.FolderNotFound, "folder"));
                            returnCode = HttpStatusCodeEnum.NotFound;
                        }
                    }

                    //Check the user role on collaborate dashboard
                    if (loggedUserCollaborateRole == Role.RoleName.AccountViewer ||
                        loggedUserCollaborateRole == Role.RoleName.AccountRetoucher ||
                        loggedUserCollaborateRole == Role.RoleName.AccountContributor)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.CreateActionNotAllowed, ""));
                        returnCode = HttpStatusCodeEnum.Forbidden;
                    }

                    //Check if file type is valid based on account upload settings
                    var fileTypeAllowed = Account.GetAccountApprovalFileTypesAllowed(loggedUser.ID, dbContext);
                    if (!_model.fileType.StartsWith("."))
                    {
                        _model.fileType = String.Format(".{0}", _model.fileType);
                    }

                    if (fileTypeAllowed == null || !ApprovalBL.IsValidApprovalFileType(fileTypeAllowed, _model.fileType))
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.FileTypeIsNotAllowed, "fileType"));
                        returnCode = HttpStatusCodeEnum.BadRequest;
                    }

                    ValidateFileName(ref returnCode);
                   
                    var dateTime = DateTime.Parse(_model.deadline, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    if (dateTime.Date < DateTime.Now.Date || (dateTime.Date == DateTime.Now.Date && TimeSpan.Compare(dateTime.TimeOfDay, DateTime.Now.TimeOfDay) != 1))
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.DeadlineMustBeHigher, "deadline"));
                        returnCode = HttpStatusCodeEnum.BadRequest;
                    }
                    
                    //validate given job guid if not empty
                    Job job = ValidateSpecifiedJobGuid(_model.guid, loggedUser.Account, loggedUserCollaborateRole, loggedUser.ID, dbContext, ref returnCode);

                    //validation related with non-workflow job
                    if (_workflowGuid == null && (_model.guid == null || (job != null && job.JobOwner == null)))
                    {
                        ValidateJobCollaborators(_model, dbContext, ref returnCode);
                    }
                    else //validation related with jobs within a workflow
                    {
                        ValidateWorkflowPhases(_model, job, dbContext, loggedUser.Account, _workflowGuid, ref returnCode);
                    }
                    ValidateSpecifiedChecklistGuid(_checklistGuid, dbContext, ref returnCode);
                }
                else
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
            }

            return returnCode;
        }

        private void ValidateFileName(ref HttpStatusCodeEnum returnCode)
        {
            if (_model.fileName.IndexOfAny(new char[] {'\\', '/', '*', '?', '<', '>', '|', '"', ':' }) != -1)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.IncorrectFileName, "filename"));
                returnCode = HttpStatusCodeEnum.BadRequest;
            }
        }

        private Job ValidateSpecifiedJobGuid(string jobGuid, int loggedAccount, Role.RoleName loggedUserCollaborateRole, int loggedUserId, DbContextBL dbContext, ref HttpStatusCodeEnum returnCode)
        {
            Job job = null;
            if (jobGuid != null)
            {
                Guid guid;
                bool isValid = Guid.TryParse(jobGuid, out guid);

                //Validate the Guid format
                if (isValid)
                {
                    jobGuid = guid.ToString();
                    job = (from j in dbContext.Jobs
                           join js in dbContext.JobStatus on j.Status equals js.ID
                           where j.Guid == jobGuid && js.Key != "ARC"
                           select j).FirstOrDefault();

                    if (job == null)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.JobGuidDoesNotExist,
                            "guid"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                    else
                    {
                        //Check if the user has permissions to create an approval or to access the specified job
                        var hasAccessToTheJob = (from ac in dbContext.ApprovalCollaborators
                                                 join a in dbContext.Approvals on ac.Approval equals a.ID
                                                 join j in dbContext.Jobs on a.Job equals j.ID
                                                 where
                                                 j.Guid == _model.guid && loggedAccount == j.Account &&
                                                 ac.Collaborator == loggedUserId
                                                 select ac.Collaborator).Any();

                        var collaborateSettings =
                            new AccountSettings.CollaborateGlobalSettings(loggedAccount, 2, false,
                                dbContext);

                        if (!hasAccessToTheJob && loggedUserCollaborateRole != Role.RoleName.AccountAdministrator && !collaborateSettings.ShowAllFilesToAdmins)
                        {
                            _ruleViolations.Add(
                                new RuleViolation(
                                    ValidationErrorMessages.UserHaveNoPermissionsForTheSpecifiedJob, "guid"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }
                    }
                }
                else
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UnrecognizedGuidFormat, "guid"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
            }
            return job;
        }

        private void ValidateWorkflowPhases(CreateApprovalRequest model, Job job, DbContextBL dbContext, int loggedAccount, string workflowGuid, ref HttpStatusCodeEnum returnCode)
        {
            Approval maxVersion = null;
            if (job != null && job.ID > 0)
            {
                maxVersion = dbContext.Approvals.Where(a => a.Job == job.ID)
                        .OrderByDescending(a => a.Version)
                        .FirstOrDefault();
            }

            // If the job does allow other users to be added
            if ((maxVersion != null && maxVersion.AllowOtherUsersToBeAdded) ||
                maxVersion == null)
            {
                // If no user is present
                if (model.phases == null || model.phases.Count == 0)
                {
                    bool hasCollaborators = CheckIfAtLeastOnePhaseHasCollaborators(workflowGuid, dbContext);
                    // If it is for create new approval
                    if (maxVersion == null && !hasCollaborators)
                    {
                        _ruleViolations.Add(
                            new RuleViolation(ValidationErrorMessages.AtLeastOneApproverAndReviewerUser, "users"));
                        returnCode = HttpStatusCodeEnum.BadRequest;
                    }
                }

                if (model.phases != null && model.phases.Count > 0)
                {
                    // In order to get workflow template 
                    string firstPhaseGUID = model.phases[0].phaseGUID;
                    ApprovalPhase approvalPhase =
                        dbContext.ApprovalPhases.FirstOrDefault(ap => ap.Guid == firstPhaseGUID);

                    if (approvalPhase != null)
                    {
                        ApprovalWorkflow approvalWorkflow =
                            dbContext.ApprovalWorkflows.FirstOrDefault(
                                aw => aw.ID == approvalPhase.ApprovalWorkflow);
                        List<ApprovalPhase> approvalPhases = approvalWorkflow.ApprovalPhases.ToList();

                        // In case of udapte, take into consideration remaining phases only
                        int currentPhasePosition = 0;

                        if (maxVersion != null)
                        {
                            currentPhasePosition = ApprovalBL.GetCurrentJobPhasePosition(maxVersion.ID,
                                dbContext);
                            approvalPhases.RemoveAll(ap => ap.Position < currentPhasePosition);
                        }

                        // If workflow is selected, check if all rows have phaseGUID filled
                        if (approvalWorkflow != null &&
                            model.phases.Any(p => string.IsNullOrEmpty(p.phaseGUID)))
                        {
                            _ruleViolations.Add(
                                new RuleViolation(ValidationErrorMessages.WorkflowPhasesInformationNotComplete,
                                    "workflow phases"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }

                        // If an approval template is selected and retrieved phases are less than template phases. 
                        if (approvalWorkflow != null && approvalPhases.Count() > model.phases.Count() &&
                            maxVersion == null) // Ignore at update
                        {
                            _ruleViolations.Add(
                                new RuleViolation(ValidationErrorMessages.WorkflowPhasesInformationNotComplete,
                                    "workflow phases"));
                            returnCode = HttpStatusCodeEnum.BadRequest;

                            // Specify missing phases
                            List<string> existingWorkflowPhases = approvalPhases.Select(ap => ap.Guid).ToList();
                            List<string> inputWorkflowPhases = model.phases.Select(ap => ap.phaseGUID).ToList();
                            List<string> missingWorkflowPhases = new List<string>();

                            missingWorkflowPhases = existingWorkflowPhases.Except(inputWorkflowPhases).ToList();

                            if (missingWorkflowPhases.Any())
                            {
                                _ruleViolations.Add(
                                    new RuleViolation(
                                        ValidationErrorMessages.WorkflowPhasesInformationNotCompleteSpecific +
                                        string.Join(",", missingWorkflowPhases.ToArray()), "workflow phases"));

                                returnCode = HttpStatusCodeEnum.BadRequest;
                            }
                        }
                    }
                    else
                    {
                        // When only one decision is required, only one PDM is required
                        if (model.settings.onlyOneDecisionRequired &&
                            model.collaborators.Where(c => c.pdm).Count() != 1)
                        {
                            _ruleViolations.Add(
                                new RuleViolation(ValidationErrorMessages.OnlyOneDecisionIsRequiredPDM,
                                    "collaborators"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }
                    }
                }

                foreach (Phase phase in model.phases)
                {
                    ApprovalPhase approvalPhaseTemplate = dbContext.ApprovalPhases.FirstOrDefault(ap => ap.Guid == phase.phaseGUID);

                    if (phase != null && phase.collaborators != null)
                    {
                        // Do not allow duplicated users per phase (internal users)
                        if (phase.collaborators.Where(c => !string.IsNullOrEmpty(c.username))
                                .GroupBy(c => c.username)
                                .SelectMany(grp => grp.Skip(1))
                                .Count() >= 1)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoDuplicatedUsers,
                                "collaborators"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }

                        // Do not allow duplicated users per phase (external users)
                        if (phase.collaborators.Where(c => !string.IsNullOrEmpty(c.email) && c.isExternal).GroupBy(c => c.email).SelectMany(grp => grp.Skip(1)).Count() >= 1)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoDuplicatedUsers,
                                "collaborators"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }

                        // If a phase has all collaborators different roles than ANR. Jobs without workflow are excluded. 
                        if (phase.collaborators.All(c => c.approvalRole != "ANR") && phase.phaseGUID != null)
                        {
                            _ruleViolations.Add(
                                new RuleViolation(ValidationErrorMessages.AtLeastOneApproverAndReviewerUser,
                                    "collaborators - pdm"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }

                        if (approvalPhaseTemplate != null && approvalPhaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.PrimaryDecisionMaker)
                        {
                            if (phase.collaborators.All(c => c.pdm == false))
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.PDMIsNotPresent,
                                    "collaborators - pdm"));
                                returnCode = HttpStatusCodeEnum.BadRequest;
                            }
                            else
                            {
                                if (phase.collaborators.Where(c => c.pdm == true).Count() > 1)
                                {
                                    _ruleViolations.Add(new RuleViolation(
                                        ValidationErrorMessages.OnlyOnePDMPerPhase, "collaborators - pdm"));
                                    returnCode = HttpStatusCodeEnum.BadRequest;
                                }
                                else
                                {
                                    if (phase.collaborators.Where(c => c.approvalRole == "ANR" && c.pdm == true)
                                            .Count() != 1)
                                    {
                                        _ruleViolations.Add(
                                            new RuleViolation(ValidationErrorMessages.PDMUserShouldHaveANRRole,
                                                "collaborators - pdm role"));
                                        returnCode = HttpStatusCodeEnum.BadRequest;
                                    }

                                    var externalPdm = phase.collaborators.Where(c => c.isExternal && c.approvalRole == "ANR" && c.pdm == true).ToList();
                                    if (((string.IsNullOrEmpty(model.guid) && approvalPhaseTemplate.Position > 1) || (maxVersion != null && maxVersion.AllowOtherUsersToBeAdded && maxVersion.CurrentPhase != approvalPhaseTemplate.ID)) && externalPdm != null && externalPdm.Count() == 1)
                                    {
                                        var email = externalPdm[0].email.Trim().ToLower();
                                        var externalUserExists = (from u in dbContext.ExternalCollaborators where u.EmailAddress == email && u.Account == loggedAccount && u.IsDeleted == false select u).Any();
                                        if (!externalUserExists)
                                        {
                                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NewExternalPDMMustBeAddedOnFirstPhase, "New external collaborators - pdm role"));
                                            returnCode = HttpStatusCodeEnum.BadRequest;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (approvalPhaseTemplate != null &&
                        approvalPhaseTemplate.DecisionType == (int)ApprovalPhase.ApprovalDecisionType.ApprovalGroup)
                    {
                        if (!phase.collaborators.All(c => c.pdm == false))
                        {
                            _ruleViolations.Add(new RuleViolation(
                                ValidationErrorMessages.NoPDMForApprovalGroup, "collaborators - pdm"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }
                    }
                }
            }
            else
            {
                foreach (Phase phase in model.phases)
                {
                    ApprovalPhase approvalPhase = dbContext.ApprovalPhases.FirstOrDefault(ap => ap.Guid == phase.phaseGUID.Trim());

                    if (phase != null && phase.collaborators != null)
                    {
                        // If a phase has all collaborators different roles than ANR. Jobs without workflow are excluded. 
                        if (phase.collaborators.All(c => c.approvalRole != "ANR") && phase.phaseGUID != null)
                        {
                            _ruleViolations.Add(
                                new RuleViolation(ValidationErrorMessages.AtLeastOneApproverAndReviewerUser,
                                    "collaborators - pdm"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }

                        var externalPdm = phase.collaborators.Where(c => c.isExternal && c.approvalRole == "ANR" && c.pdm == true).ToList();
                        if (string.IsNullOrEmpty(model.guid) && approvalPhase.Position > 1 && externalPdm != null && externalPdm.Count() == 1)
                        {
                            var email = externalPdm[0].email.Trim().ToLower();
                            var externalUserExists = (from u in dbContext.ExternalCollaborators where u.EmailAddress == email && u.Account == loggedAccount && u.IsDeleted == false select u).Any();
                            if (!externalUserExists)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NewExternalPDMMustBeAddedOnFirstPhase, "New external collaborators - pdm role"));
                                returnCode = HttpStatusCodeEnum.BadRequest;
                            }
                        }
                    }
                }
            }
        }

        private bool CheckIfAtLeastOnePhaseHasCollaborators(string workflowGuid, DbContextBL dbContext)
        {
            var internalCollaborators = (from apc in dbContext.ApprovalPhaseCollaborators
                                         join ap in dbContext.ApprovalPhases on apc.Phase equals ap.ID
                                         join aw in dbContext.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                                         where aw.Guid == workflowGuid
                                         select apc.ID).Any();

            var externalCollaborators = (from sap in dbContext.SharedApprovalPhases
                                         join ap in dbContext.ApprovalPhases on sap.Phase equals ap.ID
                                         join aw in dbContext.ApprovalWorkflows on ap.ApprovalWorkflow equals aw.ID
                                         where aw.Guid == workflowGuid
                                         select sap.ID).Any();

            return internalCollaborators || externalCollaborators;
        }

        private void ValidateJobCollaborators(CreateApprovalRequest model, DbContextBL dbContext, ref HttpStatusCodeEnum returnCode)
        {
            if (model.collaborators != null && model.collaborators.Count > 0)
            {
                if (model.collaborators.Any(c => c.pdm && c.approvalRole != "ANR"))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ThePDMShouldHaveANRRole,
                        "collaborators"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }

                if (model.collaborators.Any(c => c.isExternal && string.IsNullOrEmpty(c.email)))
                {
                    _ruleViolations.Add(
                        new RuleViolation(ValidationErrorMessages.EmailIsMandatoryForExternalUsers,
                            "collaborators"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }

                if (model.collaborators.Any(c => !c.isExternal && string.IsNullOrEmpty(c.username)))
                {
                    _ruleViolations.Add(
                        new RuleViolation(ValidationErrorMessages.UsernameIsMandatoryForInternalUsers,
                            "collaborators"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
                // When only one decision is required, only one PDM is required
                if (model.settings.onlyOneDecisionRequired &&
                    model.collaborators.Where(c => c.pdm).Count() != 1)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.OnlyOneDecisionIsRequiredPDM,
                        "collaborators"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
            }          
        }

        private void ValidateSpecifiedChecklistGuid(string checklistGuid, DbContextBL dbContext, ref HttpStatusCodeEnum returnCode)
        {
            CheckList checkList = null;
            returnCode = HttpStatusCodeEnum.Ok;
            if (checklistGuid != null)
            {
                checkList = (from j in dbContext.CheckList
                             where j.Guid == checklistGuid
                             select j).FirstOrDefault();

                if (checkList == null)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ChecklistNotFound,
                        "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
            }
        }

        #endregion
    }
}
