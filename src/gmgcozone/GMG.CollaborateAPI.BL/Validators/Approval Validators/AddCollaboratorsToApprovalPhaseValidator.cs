﻿using GMG.CollaborateAPI.BL.Models.Approval_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using GMGColorBusinessLogic;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;

namespace GMG.CollaborateAPI.BL.Validators.Approval_Validators
{
    public class AddCollaboratorsToApprovalPhaseValidator : ApprovalValidator
    {
        #region Propreties
        private new AddCollaboratorsToApprovalPhaseRequest _model;
        private HttpStatusCodeEnum _returnCode = HttpStatusCodeEnum.Ok;
        private int _requestePhaseId = 0;
        #endregion

        #region Constructor
        public AddCollaboratorsToApprovalPhaseValidator(AddCollaboratorsToApprovalPhaseRequest model)
            :base(model)
        {
            _model = model;
        }
        #endregion

        #region Validate
        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            _returnCode = base.Validate(dbContext);

            if (IsValid)
            {
                var usersAreAllowedToBeAdded = dbContext.Approvals.Where(ap => ap.Guid == _model.guid).FirstOrDefault().AllowOtherUsersToBeAdded;

                if (!usersAreAllowedToBeAdded)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UsersAreNotAllowedToBeAdded, ""));
                    return HttpStatusCodeEnum.BadRequest;
                }

                var userDetails = ApiBlHelpers.GetUserDetails(_model.username, _model.sessionKey, dbContext);

                //var submitterIsOwner = userDetails.ID  
                //              ==  
                //              dbContext.Approvals.Where(ap => ap.Guid == _model.guid).FirstOrDefault().Owner 
                //              ? 
                //              true : false;

                //if (!submitterIsOwner)
                //{
                //    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserIsNotOwner, "userName"));
                //    return HttpStatusCodeEnum.BadRequest;
                //}

                ValidateRequestDetails(dbContext);
                ValidateRequestCollaborators(dbContext, userDetails.Account);
            }
            return _returnCode;
        }

        private void ValidateRequestCollaborators(DbContextBL dbContext, int loggedAccount)
        {
            if (ValidateRequestCollaboratorsExist(_model.phaseCollaborators, dbContext, loggedAccount, out _returnCode))
            {
                var AddingUsers = _model.phaseCollaborators.Where(c => c.toBeRemoved == false).Select(c => c).ToList();
                var RemovingUsers = _model.phaseCollaborators.Where(c => c.toBeRemoved == true).Select(c => c).ToList();
                if (AddingUsers != null && AddingUsers.Count > 0)
                {
                    ValidateThereAreNoDuplicateCollaborators(AddingUsers, dbContext);
                    ValidateCollaborateRoles(AddingUsers, dbContext);
                }
                if (RemovingUsers != null && RemovingUsers.Count > 0)
                {
                    ValidateUserIsCollaborators(RemovingUsers, dbContext);
                }
            }
        }

        private void ValidateCollaborateRoles(List<PhaseCollaboratorsRequest> collaborators, DbContextBL dbContext)
        {
            foreach (var collaborator in collaborators)
            {
                if (!Enum.IsDefined(typeof(UserRole), collaborator.userRole.ToUpper()))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.InvalidUserRole, "collaborators"));
                    _returnCode = HttpStatusCodeEnum.BadRequest;
                }
            }
        }

        private void ValidateThereAreNoDuplicateCollaborators(List<PhaseCollaboratorsRequest> newCollaborators, DbContextBL dbContext)
        {
            var phaseCollaborators = new List<PhaseCollaboratorsRequest>();

            phaseCollaborators.AddRange((from ap in dbContext.Approvals
                                           join apc in dbContext.ApprovalCollaborators on ap.ID equals apc.Approval
                                           join apcr in dbContext.ApprovalCollaboratorRoles on apc.ApprovalCollaboratorRole equals apcr.ID
                                           join us in dbContext.Users on apc.Collaborator equals us.ID
                                           where ap.Guid == _model.guid && apc.Phase == _requestePhaseId
                                           select new PhaseCollaboratorsRequest()
                                           {
                                               email = us.EmailAddress,
                                               firstName = us.FamilyName,
                                               lastName = us.GivenName,
                                               userName = us.Username,
                                               userRole = apcr.Key,
                                               isExternal = false,
                                           }).ToList());

            phaseCollaborators.AddRange((from sh in dbContext.SharedApprovals
                                           join ap in dbContext.Approvals on sh.Approval equals ap.ID
                                           join exc in dbContext.ExternalCollaborators on sh.ExternalCollaborator equals exc.ID
                                           join acr in dbContext.ApprovalCollaboratorRoles on sh.ApprovalCollaboratorRole equals acr.ID
                                           where ap.Guid == _model.guid && sh.Phase == _requestePhaseId
                                           select new PhaseCollaboratorsRequest()
                                           {
                                               email = exc.EmailAddress,
                                               firstName = exc.FamilyName,
                                               lastName = exc.GivenName,
                                               userRole = acr.Key,
                                               isExternal = true
                                           }).ToList());

            phaseCollaborators.AddRange(newCollaborators);

            if (ApiBlHelpers.CheckDuplicateCollaboratorsExists(phaseCollaborators, dbContext))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoDuplicatedUsers, "collaborators"));
                _returnCode = HttpStatusCodeEnum.BadRequest;
            }
        }

        private void ValidateUserIsCollaborators(List<PhaseCollaboratorsRequest> newCollaborators, DbContextBL dbContext)
        {
            var phaseCollaborators = new List<PhaseCollaboratorsRequest>();
            if (newCollaborators.Count == 0)
            {
                _returnCode = HttpStatusCodeEnum.Ok;
            }
            else
            {
                phaseCollaborators.AddRange((from ap in dbContext.Approvals
                                             join apc in dbContext.ApprovalCollaborators on ap.ID equals apc.Approval
                                             join apcr in dbContext.ApprovalCollaboratorRoles on apc.ApprovalCollaboratorRole equals apcr.ID
                                             join us in dbContext.Users on apc.Collaborator equals us.ID
                                             where ap.Guid == _model.guid && apc.Phase == _requestePhaseId
                                             select new PhaseCollaboratorsRequest()
                                             {
                                                 email = us.EmailAddress,
                                                 firstName = us.FamilyName,
                                                 lastName = us.GivenName,
                                                 userName = us.Username,
                                                 userRole = apcr.Key,
                                                 isExternal = false,
                                             }).ToList());

                phaseCollaborators.AddRange((from sh in dbContext.SharedApprovals
                                             join ap in dbContext.Approvals on sh.Approval equals ap.ID
                                             join exc in dbContext.ExternalCollaborators on sh.ExternalCollaborator equals exc.ID
                                             join acr in dbContext.ApprovalCollaboratorRoles on sh.ApprovalCollaboratorRole equals acr.ID
                                             where ap.Guid == _model.guid && sh.Phase == _requestePhaseId
                                             select new PhaseCollaboratorsRequest()
                                             {
                                                 email = exc.EmailAddress,
                                                 firstName = exc.FamilyName,
                                                 lastName = exc.GivenName,
                                                 userRole = acr.Key,
                                                 isExternal = true
                                             }).ToList());

                phaseCollaborators.AddRange(newCollaborators);

                if (!ApiBlHelpers.CheckDuplicateCollaboratorsExists(phaseCollaborators, dbContext))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserIsNotaCollaborator, "collaborators"));
                    _returnCode = HttpStatusCodeEnum.BadRequest;
                }
            }
        }

        private bool ValidateRequestCollaboratorsExist(List<PhaseCollaboratorsRequest> collaborators, DbContextBL dbContext, int loggedAccount, out HttpStatusCodeEnum _returnCode)
        {
            var validUser = true;
            _returnCode = HttpStatusCodeEnum.Ok;

            foreach (var collaborator in _model.phaseCollaborators)
            {
                if (!collaborator.isExternal)
                {
                    validUser = dbContext.Users.Any(c => c.Username == collaborator.userName && c.Account == loggedAccount);
                }

                if (!validUser)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.CollaboratorDoesNotExists, "collaborators"));
                    _returnCode = HttpStatusCodeEnum.BadRequest;
                    return false;
                }
            }
            return validUser;
        }

        private void ValidateRequestDetails(DbContextBL dbContext)
        {
            _requestePhaseId = ApiBlHelpers.CheckApprovalWorkflowHasPhase(_model.guid, _model.phaseGuid, dbContext);

            if (_requestePhaseId == 0)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.PhaseNotFound, "phaseGuid"));
                _returnCode = HttpStatusCodeEnum.NotFound;
            }
            else
            {
                var isPhaseComplete  = ApprovalBL.IsPhaseCompleted(_requestePhaseId, dbContext.Approvals.Where(ap=>ap.Guid == _model.guid).FirstOrDefault().ID, dbContext);
                if (isPhaseComplete)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.CannotAddCollaboratorsToCompletedPhase, ""));
                    _returnCode = HttpStatusCodeEnum.BadRequest;
                }
            }

        }
        #endregion
    }
}
