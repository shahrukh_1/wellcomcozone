﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMGColorDAL.CustomModels;
using GMG.CollaborateAPI.BL.Models.JobModels;

namespace GMG.CollaborateAPI.BL.Validators.Approval_Validators
{
    public class GetCompareVersionsValidator : SessionValidatorBase
    {
        #region Properties

        private ProofStudioCompareVersionsRequest _model;

        #endregion

        #region Constructor

        public GetCompareVersionsValidator(ProofStudioCompareVersionsRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);

            if (IsValid) 
            {
                bool isValid = true;   

                foreach (string guid in _model.versions) 
                {
                    if (!dbContext.Approvals.Any(t => t.Guid == guid)) 
                    {
                        isValid = false;
                        break;
                    }
                }

                if (!isValid)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "versions"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    //get user rights info
                    int? accountId = (from cas in dbContext.CollaborateAPISessions
                                        join usr in dbContext.Users on cas.User equals usr.ID
                                        where cas.SessionKey == _model.sessionKey
                                        select usr.Account).FirstOrDefault();

                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                      where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                      select new
                                      {
                                          u.ID,
                                          Account = accountId
                                      }).FirstOrDefault();

                    if (loggedUser != null)
                    {
                        var collaborateSettings = new AccountSettings.CollaborateGlobalSettings((int)loggedUser.Account, 2, false, dbContext);

                        foreach (string guid in _model.versions)
                        {
                            //check if user has access rights to this job
                            var approval = (from ap in dbContext.Approvals
                                            join j in dbContext.Jobs on ap.Job equals j.ID
                                            let isCollaborator = (from acl in dbContext.ApprovalCollaborators
                                                                  where ap.ID == acl.Approval && acl.Collaborator == loggedUser.ID && acl.Phase == ap.CurrentPhase
                                                                  select acl.ID).Any()

                                            where ap.Guid == guid && (isCollaborator || loggedUser.ID == j.JobOwner)
                                            select new
                                            {
                                                ap.IsDeleted
                                            }).FirstOrDefault();
                            //user must be owner on the last version or admin with access rights
                            if (approval == null && !collaborateSettings.ShowAllFilesToAdmins)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNoAccess, "versions"));
                                returnCode = HttpStatusCodeEnum.Forbidden;
                            }
                            else if (approval != null && approval.IsDeleted)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalIsDeleted, "versions"));
                                returnCode = HttpStatusCodeEnum.Forbidden;
                            }
                        }
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }

            return returnCode;
        }

        #endregion Validate
    }
}
