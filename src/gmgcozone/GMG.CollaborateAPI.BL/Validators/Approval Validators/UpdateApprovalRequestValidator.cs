﻿using System.Linq;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class UpdateApprovalRequestValidator : SessionValidatorBase
    {
        #region Properties

        private UpdateApprovalRequest _model;

        #endregion

        #region Constructor

        public UpdateApprovalRequestValidator(UpdateApprovalRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var approvalExists = dbContext.Approvals.Any(t => t.Guid == _model.guid);

                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                     var loggedUserId = (from u in dbContext.Users
                                         join us in dbContext.UserStatus on u.Status equals us.ID
                                        let accountId = (from cas in dbContext.CollaborateAPISessions
                                                         join usr in dbContext.Users on cas.User equals usr.ID
                                                         where cas.SessionKey == _model.sessionKey
                                                         select usr.Account).FirstOrDefault()
                                        where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                        select u.ID).FirstOrDefault();

                    if (loggedUserId > 0)
                    {
                        var userId = loggedUserId;
                        //get user rights info
                        var ownerInfo = UserBL.GetJobOwnerInfo(userId, dbContext);

                        //get last version that is not deleted for current job
                        var isCollaborator = (from ap in dbContext.Approvals
                                              join acl in dbContext.ApprovalCollaborators on ap.ID equals acl.Approval
                                              where
                                                  ap.Guid == _model.guid && ap.IsDeleted == false && acl.Phase == ap.CurrentPhase && 
                                                  (acl.Collaborator == userId || ap.Owner == userId)
                                              select ap.ID).Any();

                        var loggedUserCollaborateRole = Role.GetRole(ownerInfo.Role);
                        //Check the user role on collaborate dashboard
                        if ((!isCollaborator || loggedUserCollaborateRole == Role.RoleName.AccountViewer ||
                            loggedUserCollaborateRole == Role.RoleName.AccountRetoucher ||
                            loggedUserCollaborateRole == Role.RoleName.AccountContributor) &&
                            loggedUserCollaborateRole != Role.RoleName.AccountAdministrator)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UpdateActionNotAllowed, ""));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }

                        var dateTime = DateTime.Parse(_model.deadline, null, System.Globalization.DateTimeStyles.RoundtripKind);
                        if (dateTime.Date < DateTime.Now.Date || (dateTime.Date == DateTime.Now.Date && TimeSpan.Compare(dateTime.TimeOfDay, DateTime.Now.TimeOfDay) != 1))
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.DeadlineMustBeHigher, "deadline"));
                            returnCode = HttpStatusCodeEnum.BadRequest;
                        }
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }
            return returnCode;
        }
        #endregion
    }

}
