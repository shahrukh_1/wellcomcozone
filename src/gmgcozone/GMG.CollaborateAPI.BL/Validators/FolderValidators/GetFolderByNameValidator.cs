﻿using System.Linq;
using GMG.CollaborateAPI.BL.Models.FolderModels;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators.FolderValidators
{
    public class GetFolderByNameValidator : SessionValidatorBase
    {
        #region Properties

        private GetFolderByNameRequest _model;

        #endregion

        #region Constructor

        public GetFolderByNameValidator(GetFolderByNameRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validation

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);

            if (IsValid)
            {
                 //get user rights info
                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                        let accountId = (from cas in dbContext.CollaborateAPISessions
                                                         join usr in dbContext.Users on cas.User equals usr.ID
                                                         where cas.SessionKey == _model.sessionKey
                                                         select usr.Account).FirstOrDefault()
                                        where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                        select new 
                                        {
                                            u.ID,
                                            Account = accountId
                                        }).FirstOrDefault();

                if (loggedUser != null)
                {
                    var folderExists = (from f in dbContext.Folders
                                        join fc in dbContext.FolderCollaborators on f.ID equals fc.Folder
                                        where f.Account == loggedUser.Account && fc.Collaborator == loggedUser.ID && f.Name.ToLower() == _model.name.Trim().ToLower()
                                        select f.ID).Any();
                    if (!folderExists)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.FolderNameNotFound, "name"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
                else
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
            }

            return returnCode;
        }
        #endregion
    }
}
