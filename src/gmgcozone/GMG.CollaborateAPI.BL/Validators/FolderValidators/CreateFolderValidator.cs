﻿using System.Linq;
using GMG.CollaborateAPI.BL.Models.FolderModels;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace GMG.CollaborateAPI.BL.Validators.FolderValidators
{
    public class CreateFolderValidator : SessionValidatorBase
    {
        private const int FolderMaxCharacters = 64;

        #region Properties

        private CreateFolderRequest _model;

        #endregion

        #region Constructor

        public CreateFolderValidator(CreateFolderRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var loggedUser = (from u in dbContext.Users
                                  join us in dbContext.UserStatus on u.Status equals us.ID
                                  let accountId = (from cas in dbContext.CollaborateAPISessions
                                                   join usr in dbContext.Users on cas.User equals usr.ID
                                                   where cas.SessionKey == _model.sessionKey
                                                   select usr.Account).FirstOrDefault()
                                  where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                  select u).FirstOrDefault();

                if (loggedUser != null)
                {
                    if (_model?.name.Length > FolderMaxCharacters)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.FolderNameExceeds64Chars, "name"));
                        returnCode = HttpStatusCodeEnum.BadRequest;
                    }
                    else
                    {
                        // Find the ID of the parent folder if the current user is collaborator)
                        var folder = (from f in dbContext.Folders
                                      join fc in dbContext.FolderCollaborators on f.ID equals fc.Folder
                                      join u in dbContext.Users on fc.Collaborator equals u.ID
                                      where u.Account == loggedUser.Account && u.Username == _model.username && f.Guid == _model.parentGuid
                                      select f.ID).FirstOrDefault();

                        if (folder > 0)
                        {
                            var loggedUserCollaborateRole = Role.GetRole(loggedUser.CollaborateRoleKey(dbContext));

                            //Check the user role on collaborate dashboard
                            if (loggedUserCollaborateRole != Role.RoleName.AccountAdministrator &&
                                loggedUserCollaborateRole != Role.RoleName.AccountManager &&
                                loggedUserCollaborateRole != Role.RoleName.AccountContributor &&
                                loggedUserCollaborateRole != Role.RoleName.AccountModerator)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.CreateFolderActionNotAllowed, ""));
                                returnCode = HttpStatusCodeEnum.Forbidden;
                            }
                        }
                    }
                }
                else
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
            }

            return returnCode;
        }

        #endregion
    }
}
