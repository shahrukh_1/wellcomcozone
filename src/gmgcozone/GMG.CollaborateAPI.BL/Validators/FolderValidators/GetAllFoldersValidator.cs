﻿using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Validators.FolderValidators
{
    public class GetAllFoldersValidator : SessionValidatorBase
    {
        #region Constructor

        public GetAllFoldersValidator(CollaborateAPIBaseModel model) :
            base(model)
        {
        }

        #endregion
    }
}
