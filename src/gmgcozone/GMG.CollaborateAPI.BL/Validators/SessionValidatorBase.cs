﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public abstract class SessionValidatorBase : ValidatorBase
    {
        #region Fields

        protected CollaborateAPIBaseModel _model;

        #endregion

        #region Constructors

        protected SessionValidatorBase(CollaborateAPIBaseModel model)
        {
            _model = model;
        }

        #endregion

        #region Methods

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = HttpStatusCodeEnum.Ok;
            var session = dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == _model.sessionKey.Trim());
            if (session == null)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyInvalid, "sessionKey"));
                returnCode = HttpStatusCodeEnum.NotFound;
            }
            else if (!session.IsConfirmed)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyNotConfirmed, "sessionKey"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }
            else
            {
                //check for TTL expiration
                bool isValid = (DateTime.UtcNow - session.TimeStamp).TotalMinutes < Convert.ToInt32(ConfigurationManager.AppSettings["CollaborateAPISessionTimeoutMinutes"]);
                if (isValid)
                {
                    session.TimeStamp = DateTime.UtcNow;
                }
                else
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyExpired, "sessionKey"));
                    dbContext.CollaborateAPISessions.Remove(session);
                    returnCode = HttpStatusCodeEnum.Unauthorized;
                }
                dbContext.SaveChanges();
            }

            return returnCode;
        }

        #endregion
    }
}
