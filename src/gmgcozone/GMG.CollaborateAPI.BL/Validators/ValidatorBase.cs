﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.Schema;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public abstract class ValidatorBase
    {
        #region Fields

        protected List<RuleViolation>  _ruleViolations = new List<RuleViolation>();

        #endregion

        #region Methods

        public bool IsValid
        {
            get { return _ruleViolations.Count == 0; }
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            return _ruleViolations;
        }

        #endregion

        #region Abstract

        public abstract HttpStatusCodeEnum Validate(DbContextBL dbContext);

        #endregion
    }
}
