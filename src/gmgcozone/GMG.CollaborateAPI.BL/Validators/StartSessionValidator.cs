﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using AppModule = GMG.CoZone.Common.AppModule;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class StartSessionValidator : ValidatorBase
    {
        #region Properties

        private readonly StartSessionRequest _model;

        #endregion

        #region Constructor

        public StartSessionValidator(StartSessionRequest model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = HttpStatusCodeEnum.Ok;

            var usersAndAccDomains = (from ac in dbContext.Accounts
                                        join u in dbContext.Users on ac.ID equals u.Account
                                        join us in dbContext.UserStatus on u.Status equals us.ID
                                        where us.Key == "A" && u.Username.ToLower() == _model.username.ToLower().Trim()
                                        select new
                                        {
                                            u.Username,
                                            AccountURL = ac.IsCustomDomainActive ? ac.CustomDomain : ac.Domain,
                                            CollaborateRole = (from r in dbContext.Roles
                                                               join ur in dbContext.UserRoles on r.ID equals ur.Role
                                                                join apm in dbContext.AppModules on r.AppModule equals apm.ID
                                                                where ur.User == u.ID && apm.Key == (int)AppModule.Collaborate
                                                               select r.Key).FirstOrDefault(),
                                            HasCollaborateSubsciptionPlan = (from acs in dbContext.AccountSubscriptionPlans
                                                join bp in dbContext.BillingPlans on acs.SelectedBillingPlan equals bp.ID
                                                join bpt in dbContext.BillingPlanTypes on bp.Type equals bpt.ID
                                                join apm in dbContext.AppModules on bpt.AppModule equals apm.ID
                                                where acs.Account == ac.ID && apm.Key == (int)AppModule.Collaborate
                                                select acs.ID).Any() || (
                                                    from acs in dbContext.SubscriberPlanInfoes
                                                    join bp in dbContext.BillingPlans on acs.SelectedBillingPlan equals bp.ID
                                                    join bpt in dbContext.BillingPlanTypes on bp.Type equals bpt.ID
                                                    join apm in dbContext.AppModules on bpt.AppModule equals apm.ID
                                                    where acs.Account == ac.ID && apm.Key == (int)AppModule.Collaborate
                                                    select acs.ID
                                                    ).Any()
                                        }).ToList();


            if (usersAndAccDomains.Count == 0)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                returnCode = HttpStatusCodeEnum.NotFound;
            }
            else if (usersAndAccDomains.Count > 1)
            {
                if (String.IsNullOrEmpty(_model.accountURL))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.DuplicateUser, "accountURL"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
                else if (usersAndAccDomains.All(t => t.AccountURL.ToLower() != _model.accountURL.ToLower().Trim()))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.AccountMismatch, "accountURL"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
                else
                {
                    var userInfo = usersAndAccDomains.FirstOrDefault(t => t.AccountURL.ToLower() == _model.accountURL.ToLower().Trim());
                    
                    returnCode = ValidateCollaboratorSubscriptionAndRole(userInfo.HasCollaborateSubsciptionPlan, 
                        userInfo.CollaborateRole);
                }
            }
            else
            {
                var userInfo = usersAndAccDomains.FirstOrDefault();

                returnCode = ValidateCollaboratorSubscriptionAndRole(userInfo.HasCollaborateSubsciptionPlan, 
                    userInfo.CollaborateRole);
            }

            return returnCode;
        }

        private HttpStatusCodeEnum ValidateCollaboratorSubscriptionAndRole(bool hasCollaborateSubsciptionPlan, string collaborateRole)
        {
            HttpStatusCodeEnum returnCode = HttpStatusCodeEnum.Ok;

            if (!hasCollaborateSubsciptionPlan)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoSubscriptionPlan, "accountURL"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }
            else if (String.IsNullOrEmpty(collaborateRole) ||
                     Role.GetRole(collaborateRole) == Role.RoleName.AccountNone)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoRoleForCol, "username"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }

            return returnCode;
        }

        #endregion
    }
}
