﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class WarningMessage
    {
        #region Properties

        public List<string> WarningMessages { get; set; }

        #endregion
    }
}
