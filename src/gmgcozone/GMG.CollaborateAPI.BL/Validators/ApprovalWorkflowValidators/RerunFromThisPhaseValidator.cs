﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidattors;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;

namespace GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators
{
    public class RerunFromThisPhaseValidator : SessionValidatorBase
    {
        private RerunFromThisPhaseRequest _model;

        public RerunFromThisPhaseValidator(RerunFromThisPhaseRequest model) :
            base(model)
        {
            _model = model;
        }

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var approvalExists = dbContext.Approvals.Any(ap => ap.Guid == _model.guid);
                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    returnCode = ValidatedWorklowPhaseDetails(dbContext, returnCode);
                }
            }
            return returnCode;
        }

        /// <summary>
        /// Validate the provided details
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="returnCode"></param>
        /// <returns></returns>
        private HttpStatusCodeEnum ValidatedWorklowPhaseDetails(DbContextBL dbContext, HttpStatusCodeEnum returnCode)
        {
            return ValidateRequestPhaseIdExists(dbContext, returnCode);
        }

        /// <summary>
        /// Check if the provided phase id is valid for the approval
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="returnCode"></param>
        /// <returns></returns>
        private HttpStatusCodeEnum ValidateRequestPhaseIdExists(DbContextBL dbContext, HttpStatusCodeEnum returnCode)
        {
            var jobWorkflow = (from a in dbContext.Approvals
                               join j in dbContext.Jobs on a.Job equals j.ID
                               join ajw in dbContext.ApprovalJobWorkflows on j.ID equals ajw.Job
                               where a.Guid == _model.guid
                               select ajw.ID).FirstOrDefault();

            var requestedPhaseID = (from ajp in dbContext.ApprovalJobPhases
                                    join app in dbContext.ApprovalPhases on ajp.PhaseTemplateID equals app.ID
                                    where app.Guid == _model.phaseGuid && ajp.ApprovalJobWorkflow == jobWorkflow
                                    select ajp.ID).FirstOrDefault();

            if (requestedPhaseID == 0)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.InvalidPhaseID, "PhaseGuid"));
                return HttpStatusCodeEnum.NotFound;
            }
            return ValidateRequestUser(dbContext, returnCode, requestedPhaseID);
        }

        /// <summary>
        /// Check if the provided is the approval owner since only he/she can rerun workflow
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="returnCode"></param>
        /// <returns></returns>
        private HttpStatusCodeEnum ValidateRequestUser(DbContextBL dbContext, HttpStatusCodeEnum returnCode, int requestPhaseId)
        {           
            var userDetails = (from u in dbContext.Users
                                join us in dbContext.UserStatus on u.Status equals us.ID
                                let accountId = (from cas in dbContext.CollaborateAPISessions
                                                join usr in dbContext.Users on cas.User equals usr.ID
                                                where cas.SessionKey == _model.sessionKey
                                                select usr.Account).FirstOrDefault()
                                where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                select u).FirstOrDefault();

            var approvalOwner = (from ap in dbContext.Approvals 
                                 where ap.Guid == _model.guid
                                 select ap.Owner).FirstOrDefault();
            if (userDetails.ID != approvalOwner)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.OnlyOwnerisAllowedToRestWorkflow, "UserName"));
                return HttpStatusCodeEnum.Forbidden;
            }
            return ValidatePhaseResetCanOccur(dbContext, returnCode, requestPhaseId);
        }

        /// <summary>
        /// Check if the provided phase si smaller that the current phase
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="returnCode"></param>
        /// <returns></returns>
        private HttpStatusCodeEnum ValidatePhaseResetCanOccur(DbContextBL dbContext, HttpStatusCodeEnum returnCode, int requestPhaseId)
        {
            var canResetToPhase = (from ap in dbContext.Approvals
                                   join ajpa in dbContext.ApprovalJobPhaseApprovals on ap.ID equals ajpa.Approval
                                   where ap.Guid == _model.guid
                                   select ajpa.Phase).ToList().Contains(requestPhaseId);
            if (!canResetToPhase)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ProvidedPhaseIsGraterThanTheCurrentPhase, "PhaseGuid"));
                return HttpStatusCodeEnum.Forbidden;
            }
            return returnCode = HttpStatusCodeEnum.Ok;
        }
    }
}
