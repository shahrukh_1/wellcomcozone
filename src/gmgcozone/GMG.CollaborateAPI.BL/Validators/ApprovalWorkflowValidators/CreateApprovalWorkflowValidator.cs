﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidattors;
using GMGColorBusinessLogic;
using GMG.CollaborateAPI.BL.Models.Approval_Workflow_Models;

namespace GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators
{
    public class CreateApprovalWorkflowValidator : CollaborateDashboardAccessValidator
    {
        #region Fields
        
        private CreateApprovalWorkflowRequest _model;
        private HttpStatusCodeEnum _returnCode;

        #endregion

        #region Constructor
        
        public CreateApprovalWorkflowValidator(CreateApprovalWorkflowRequest model)
            :base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL context)
        {
            _returnCode = base.Validate(context);

            if (IsValid)
            {
                var accountId = ApiBlHelpers.GetUserDetails(_model.username, _model.sessionKey, context).Account; //the account id of the user creating the workflow
                _returnCode = ValidateWorkflowDetails(accountId, context);
            }

            return _returnCode;
        }

        private HttpStatusCodeEnum ValidateWorkflowDetails(int accountId, DbContextBL context)
        {
            // Check if workflow name is empty
            if (!string.IsNullOrEmpty(_model.workflowName))
            {
                //Check the Workflow name is unique at account level
                if (ApprovalWorkflowBL.CheckAdHocWorkflowNameIsUnique(_model.workflowName, accountId, context))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowNameIsNotUnique, "workflowname"));
                    return HttpStatusCodeEnum.BadRequest;
                }
            }
            else
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowNameIsMandatory, "workflowname"));
                return HttpStatusCodeEnum.BadRequest;
            }
            return ValidateWorkflowPhases(context);
        }

        private HttpStatusCodeEnum ValidateWorkflowPhases(DbContextBL context)
        {
            if (!ApiBlHelpers.CheckWorkflowPhaseNamesAreUnique(_model.worflowPhases, context))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowHasPhasesWithDuplicateNames, "worflowphasenames"));
                return HttpStatusCodeEnum.BadRequest;
            }

            foreach (var phase in _model.worflowPhases)
            {
                _returnCode = ValidatWorkflowPhaseDetails(phase, context);
                if (_returnCode != HttpStatusCodeEnum.Ok)
                {
                    return _returnCode;
                }
            }
            return _returnCode;
        }

        private HttpStatusCodeEnum ValidatWorkflowPhaseDetails(PhaseRequest phase, DbContextBL context)
        {
            if (!string.IsNullOrEmpty(phase.phaseName))
            {
                _returnCode = ValidateWorkflowPhaseApprovalTrigger(phase);

                if (_returnCode == HttpStatusCodeEnum.Ok)
                {
                    _returnCode = ValidateWorkflowPhaseDecisionType(phase);
                }

                if (_returnCode == HttpStatusCodeEnum.Ok)
                {
                    _returnCode = ValidatePhaseHasNoDuplicateCollaborators(phase.phaseCollaborators, context);
                }
            }
            else
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.PhaseNameIsMandatory, "worflowphasenames"));
                return HttpStatusCodeEnum.BadRequest;
            }
            return _returnCode;
        }

        private HttpStatusCodeEnum ValidateWorkflowPhaseDecisionType(PhaseRequest phase)
        {
            if (!Enum.IsDefined(typeof(DecisionType), phase.selectedDecisionType))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowPhasesDecisionKeyIsIncorrect, "phasedecisiontype"));
                _returnCode = HttpStatusCodeEnum.BadRequest;
            }
            else
            {
                if (phase.selectedDecisionType == Enum.GetName(typeof(DecisionType), 1))
                {
                    _returnCode = ValidateWorkflowPhaseDecisionMaker(phase.phaseCollaborators, phase.decisionMaker);
                }
                else
                {
                    _returnCode = ValidateWorkflowGroupHasAtLeastOnePdm(phase.phaseCollaborators);
                }
            }
            return _returnCode;
        }

        private HttpStatusCodeEnum ValidateWorkflowGroupHasAtLeastOnePdm(List<PhaseCollaboratorsRequest> phaseCollaborators)
        {
            if (!phaseCollaborators.Any(c => c.userRole == Enum.GetName(typeof(UserRole), 3)))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowPhaseGroupShouldHaveAtLeastOneApprovareAndReviewer, "phaseCollaborators"));
                _returnCode  = HttpStatusCodeEnum.BadRequest;
            }
            return _returnCode;
        }

        private HttpStatusCodeEnum ValidateWorkflowPhaseApprovalTrigger(PhaseRequest phase)
        {
            if (!Enum.IsDefined(typeof(ApprovalTrigger), phase.approvalTrigger))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowPhaseApprovalTriggerKeyIsInvalid, "phaseapprovaltrigger"));
                return HttpStatusCodeEnum.BadRequest;
            }
            return HttpStatusCodeEnum.Ok;
        }

        private HttpStatusCodeEnum ValidateWorkflowPhaseDecisionMaker(List<PhaseCollaboratorsRequest> phaseCollaborators, string phaseDecisionMaker)
        {
            if (phaseCollaborators != null && phaseCollaborators.Count > 0)
	        {
                if (!phaseCollaborators.Any(c => c.userName == phaseDecisionMaker))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowPhaseDecisionMakerIsNotAValidCollaborator, "phasecollaborator"));
                    return HttpStatusCodeEnum.BadRequest;
                }
                if (phaseCollaborators.Where(c => c.userName == phaseDecisionMaker).FirstOrDefault().userRole != Enum.GetName(typeof(UserRole), 3))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.WorkflowPhasePDMIsNotApprovalAndReviewer, "phaseCollaborators"));
                    return HttpStatusCodeEnum.BadRequest;
                }
	        }
            return HttpStatusCodeEnum.Ok;
        }

        private HttpStatusCodeEnum ValidatePhaseHasNoDuplicateCollaborators(List<PhaseCollaboratorsRequest> phaseCollaborators, DbContextBL context)
        {
            if (ApiBlHelpers.CheckDuplicateCollaboratorsExists(phaseCollaborators, context))
            { 
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoDuplicatedUsers, "phaseCollaborators"));
                _returnCode = HttpStatusCodeEnum.BadRequest;
            }
            return _returnCode;
        }

        #endregion
    }
}
