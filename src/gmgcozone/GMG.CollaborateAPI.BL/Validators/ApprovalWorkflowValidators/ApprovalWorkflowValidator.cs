﻿using System.Linq;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators;

namespace GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidattors
{
    public class ApprovalWorkflowValidator : CollaborateDashboardAccessValidator
    {
        #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public ApprovalWorkflowValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                if (!string.IsNullOrEmpty(_model.guid))
                {
                    var approvalExists = dbContext.ApprovalWorkflows.Any(ap => ap.Guid == _model.guid);

                    if (!approvalExists)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, ""));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }
            return returnCode;
        }

        #endregion
    }
}
