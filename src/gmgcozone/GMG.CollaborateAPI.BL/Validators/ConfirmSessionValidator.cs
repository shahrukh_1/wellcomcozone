﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class ConfirmSessionValidator : ValidatorBase
    {
        #region Properties

        private ConfirmIdentityRequest _model;

        #endregion

        #region Constructor

        public ConfirmSessionValidator(ConfirmIdentityRequest model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            var returnCode = HttpStatusCodeEnum.Ok;

            var session = dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == _model.sessionKey.Trim());
            if (session == null)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyInvalid, "sessionKey"));
                returnCode = HttpStatusCodeEnum.NotFound;
            }
            else if ((DateTime.UtcNow - session.TimeStamp).TotalMinutes > Convert.ToInt32(ConfigurationManager.AppSettings["CollaborateAPISessionTimeoutMinutes"]))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyExpired, "sessionKey"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }
            else
            {
                var userPass = (from colses in dbContext.CollaborateAPISessions
                                join u in dbContext.Users on colses.User equals u.ID
                                join us in dbContext.UserStatus on u.Status equals us.ID
                                join ac in dbContext.Accounts on u.Account equals ac.ID
                                where colses.SessionKey == _model.sessionKey.Trim() && us.Key == "A"
                                select u.Password).FirstOrDefault();

                var accessKey = userPass +
                                session.SessionKey;

                var accessKeyHash = GMGColorBusinessLogic.Common.Utils.GetSHA256Base64(accessKey, _model.ascii);
                if (!accessKeyHash.Equals(_model.accessKey))
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.AccessKeyInvalid, "accessKey"));
                    returnCode = HttpStatusCodeEnum.Forbidden;
                }
            }

            return returnCode;
        }

        #endregion

        #region ValidateLogin 

        public  HttpStatusCodeEnum ValidateLogin(DbContextBL dbContext)
        {
            var returnCode = HttpStatusCodeEnum.Ok;

            var session = dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == _model.sessionKey.Trim());
            var UserLoginFailedAttempts = session !=null ? GMGColorDAL.User.GetUserLoginAttempts(session.User, dbContext): null;
            bool IsTemporarilyLocked = UserLoginFailedAttempts == null ? false : 
                (UserLoginFailedAttempts.Attempts == 5 ? UserLoginFailedAttempts.DateLastLoginFailed > DateTime.UtcNow.AddMinutes(-15) : false);
            bool IsPermanentlyLocked = UserLoginFailedAttempts == null ? false : UserLoginFailedAttempts.IsUserLoginLocked;

            if (session == null)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyInvalid, "sessionKey"));
                returnCode = HttpStatusCodeEnum.NotFound;
            }
            else if ((DateTime.UtcNow - session.TimeStamp).TotalMinutes > Convert.ToInt32(ConfigurationManager.AppSettings["CollaborateAPISessionTimeoutMinutes"]))
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.SessionKeyExpired, "sessionKey"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }
            else if(IsTemporarilyLocked)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserAccountTemporarilyLocked, "accessKey"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }
            else if(IsPermanentlyLocked)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserAccountPermanentlyLocked, "accessKey"));
                returnCode = HttpStatusCodeEnum.Forbidden;
            }
            else
            {
                var userPass = (from colses in dbContext.CollaborateAPISessions
                                join u in dbContext.Users on colses.User equals u.ID
                                join us in dbContext.UserStatus on u.Status equals us.ID
                                join ac in dbContext.Accounts on u.Account equals ac.ID
                                where colses.SessionKey == _model.sessionKey.Trim() && us.Key == "A"
                                select u.Password).FirstOrDefault();

                var accessKey = userPass +
                                session.SessionKey;
                
                var accessKeyHash = GMGColorBusinessLogic.Common.Utils.GetSHA256Base64(accessKey, _model.ascii);
                if (!accessKeyHash.Equals(_model.accessKey) && !IsTemporarilyLocked && !IsPermanentlyLocked)
                {
                    if (UserLoginFailedAttempts == null)
                    {
                        var Loginfailed = new GMGColorDAL.UserLoginFailedDetail
                        {
                            User = session.User,
                            IpAddress = "",
                            Attempts = 1,
                            DateLastLoginFailed = DateTime.UtcNow,
                            IsUserLoginLocked = false
                        };
                        dbContext.UserLoginFailedDetails.Add(Loginfailed);
                    }
                    else
                    {
                        UserLoginFailedAttempts.IpAddress = "";
                        UserLoginFailedAttempts.DateLastLoginFailed = DateTime.UtcNow;
                        UserLoginFailedAttempts.Attempts += 1;
                        UserLoginFailedAttempts.IsUserLoginLocked = UserLoginFailedAttempts.Attempts >= 10;
                    }
                    dbContext.SaveChanges();
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.AccessKeyInvalid, "accessKey"));
                    returnCode = HttpStatusCodeEnum.Forbidden;
                }
                else 
                {
                    if (UserLoginFailedAttempts != null)
                    {
                        dbContext.UserLoginFailedDetails.Remove(UserLoginFailedAttempts);
                        dbContext.SaveChanges();
                    }
                }
            }

            return returnCode;
        }



        #endregion
    }
}
