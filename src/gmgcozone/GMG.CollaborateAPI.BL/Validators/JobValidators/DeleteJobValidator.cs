﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using GMG.CoZone.Common;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class DeleteJobValidator : SessionValidatorBase
    {
        #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public DeleteJobValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var jobExists = dbContext.Jobs.Any(t => t.Guid == _model.guid);

                if (!jobExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.JobNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    //get user rights info
                    var ownerInfo = UserBL.GetJobOwnerInfo(dbContext.CollaborateAPISessions.FirstOrDefault(t => t.SessionKey == _model.sessionKey).User, dbContext);

                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                      let accountId = (from cas in dbContext.CollaborateAPISessions
                                                       join usr in dbContext.Users on cas.User equals usr.ID
                                                       where cas.SessionKey == _model.sessionKey
                                                       select usr.Account).FirstOrDefault()
                                      where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                      select u).FirstOrDefault();

                    if (loggedUser != null)
                    {
                        //get last version that is not deleted for current job
                        var lastVers = (from ap in dbContext.Approvals
                                        join j in dbContext.Jobs on ap.Job equals j.ID
                                        join js in dbContext.JobStatus on j.Status equals js.ID

                                        let maxVersion = (from v in dbContext.Approvals
                                                          where v.Job == ap.Job && v.IsDeleted == false
                                                          select v.Version).Max()
                                        where j.Guid == _model.guid && ap.IsDeleted == false && ap.Version == maxVersion
                                        select new
                                        {
                                            isOwner = ap.Owner == loggedUser.ID,
                                            version = maxVersion
                                        }).FirstOrDefault();

                        if (lastVers == null)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoVersionAvailable, "guid"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }
                        else
                        {
                            //user must be owner on the last version or admin with access rights
                            if (!lastVers.isOwner && !ownerInfo.CanViewAllFiles)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserIsNoOwner, "guid"));
                                returnCode = HttpStatusCodeEnum.Forbidden;
                            }
                        }
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
