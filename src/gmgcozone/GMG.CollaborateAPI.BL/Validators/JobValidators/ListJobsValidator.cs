﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;

namespace GMG.CollaborateAPI.BL.Validators.JobValidators
{
    public class ListJobsValidator : SessionValidatorBase
    {
        #region Constructor

        public ListJobsValidator(CollaborateAPIBaseModel model) :
            base(model)
        {
        }

        #endregion
    }
}
