﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators.JobValidators
{
    public class GetJobValidator : SessionValidatorBase
    {
        #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public GetJobValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                var jobExists = dbContext.Jobs.Any(t => t.Guid == _model.guid);

                if (!jobExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.JobNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    var loggedUserId = (from u in dbContext.Users
                                        join us in dbContext.UserStatus on u.Status equals us.ID
                                        let accountId = (from cas in dbContext.CollaborateAPISessions
                                                         join usr in dbContext.Users on cas.User equals usr.ID
                                                         where cas.SessionKey == _model.sessionKey
                                                         select usr.Account).FirstOrDefault()
                                        where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                        select u.ID).FirstOrDefault();
                    if (loggedUserId > 0)
                    {
                        //check if user has access rights to this job
                        var approvalId = (from ap in dbContext.Approvals
                                          join j in dbContext.Jobs on ap.Job equals j.ID
                                          let isCollaborator = (from acl in dbContext.ApprovalCollaborators
                                                                where ap.ID == acl.Approval && acl.Collaborator == loggedUserId
                                                                select acl.ID).Any()

                                          where j.Guid == _model.guid && ap.IsDeleted == false && (isCollaborator || loggedUserId == j.JobOwner)
                                          select ap.ID).FirstOrDefault();


                        //check if user is collaborator on this job
                        if (approvalId == 0)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.JobNoAccess, "guid"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
