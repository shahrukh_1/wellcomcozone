﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.CustomModels;

namespace GMG.CollaborateAPI.BL.Validators.JobValidators
{
    public class UpdateJobValidator : SessionValidatorBase
    {
         #region Properties

        private UpdateJobRequest _model;

        #endregion

        #region Constructor

        public UpdateJobValidator(UpdateJobRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (!IsValid) return returnCode;

            var jobExists = dbContext.Jobs.Any(t => t.Guid == _model.guid);

            if (!jobExists)
            {
                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.JobNotFound, "guid"));
                returnCode = HttpStatusCodeEnum.NotFound;
            }
            else
            {
                //validate incomig status string
                JobStatu.Status newStatus = JobStatu.Status.ChangesRequired;
                bool validStatus = true;

                if (!String.IsNullOrEmpty(_model.jobStatus))
                {
                    switch (_model.jobStatus.ToUpper())
                    {
                        case "CRQ":
                        case "INP":
                            newStatus = JobStatu.Status.ChangesRequired;
                            break;
                        case "CCM":
                            newStatus = JobStatu.Status.ChangesComplete;
                            break;
                        case "CM":
                            newStatus = JobStatu.Status.Completed;
                            break;
                        case "ARC":
                            newStatus = JobStatu.Status.Archived;
                            break;
                        default:
                            validStatus = false;
                            break;
                    }
                }
                else
                {
                    validStatus = false;
                }

                //check if any parameter is valid to be updated
                if (String.IsNullOrEmpty(_model.jobTitle) && !validStatus)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UpdateJobBadReq, "guid"));
                    returnCode = HttpStatusCodeEnum.BadRequest;
                }
                else
                {
                    //get user rights info
                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                      let accountId = (from cas in dbContext.CollaborateAPISessions
                                                       join usr in dbContext.Users on cas.User equals usr.ID
                                                       where cas.SessionKey == _model.sessionKey
                                                       select usr.Account).FirstOrDefault()
                                      where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                      select new
                                      {
                                          u.ID,
                                          Account = accountId
                                      }).FirstOrDefault();

                    if (loggedUser != null)
                    {
                        //check if user has access rights to this job
                        var job = (from j in dbContext.Jobs
                                   join js in dbContext.JobStatus on j.Status equals js.ID

                                   let isCollaborator = (from ap in dbContext.Approvals
                                                         join acl in dbContext.ApprovalCollaborators on ap.ID equals acl.Approval
                                                         where acl.Collaborator == loggedUser.ID && ap.IsDeleted == false
                                                         select acl.ID).Any()

                                   let isRetoucherWfEn = (from acs in dbContext.AccountSettings
                                                          where acs.Account == loggedUser.Account && acs.Name == "EnableRetoucherWorkflow"
                                                          select acs.Value).FirstOrDefault()

                                   where j.Guid == _model.guid && (isCollaborator || loggedUser.ID == j.JobOwner)
                                   select new
                                   {
                                       js.Key,
                                       isCollaborator,
                                       isRetoucherWfEn
                                   }).FirstOrDefault();

                        if (!job.isCollaborator)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserHaveNoPermissionsForTheSpecifiedJob, "jobStatus"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }
                        else if (validStatus)
                        {
                            bool isRetoucherWf;
                            bool.TryParse(job.isRetoucherWfEn, out isRetoucherWf);

                            JobStatu.Status currentStatus = JobStatu.GetJobStatus(job.Key);

                            if (newStatus == JobStatu.Status.Archived && currentStatus != JobStatu.Status.Completed)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UpdateJobArcStatusInvalid, "jobStatus"));
                                returnCode = HttpStatusCodeEnum.BadRequest;
                            }
                            else if (!isRetoucherWf && newStatus == JobStatu.Status.ChangesComplete)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UpdateJobCCMStatusInvalid, "jobStatus"));
                                returnCode = HttpStatusCodeEnum.BadRequest;
                            }
                            else if (currentStatus == JobStatu.Status.Archived)
                            {
                                _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ArchiveStatusCntBeChanged, "jobStatus"));
                                returnCode = HttpStatusCodeEnum.BadRequest;
                            }
                        } 
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }

            return returnCode;
        }

        #endregion
    }
}
