﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL.CustomModels;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class AnnotationReportValidator : AnnotationRequestBaseValidator
    {
       #region Properties

        private AnnotationReportRequest _model;

        #endregion

        #region Constructor

        public AnnotationReportValidator(AnnotationReportRequest model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                //Validate optional fields and set default values
                if (_model.nrOfCommentsPerPage == 0)
                {
                    _model.nrOfCommentsPerPage = 10;
                }

                if (String.IsNullOrEmpty(_model.displayOptions))
                {
                    _model.displayOptions = Constants.GroupsLabel;
                }

                if (!_model.includeSummaryPage.HasValue)
                {
                    _model.includeSummaryPage = true;
                }
            }

            return returnCode;
        }

        #endregion
    }
}
