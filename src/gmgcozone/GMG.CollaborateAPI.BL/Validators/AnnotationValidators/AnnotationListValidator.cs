﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class AnnotationListValidator : AnnotationRequestBaseValidator
    {
        #region Constructor

        public AnnotationListValidator(AnnotationListRequest model) :
            base(model)
        {
        }

        #endregion
    }
}
