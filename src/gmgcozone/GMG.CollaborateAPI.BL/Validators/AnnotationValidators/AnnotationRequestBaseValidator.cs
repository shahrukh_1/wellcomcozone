﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMG.CollaborateAPI.BL.Models;
using GMGColorBusinessLogic;
using GMGColorDAL;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class AnnotationRequestBaseValidator : SessionValidatorBase
    {
        #region Properties

        private AnnotationBaseModel _model;

        #endregion

        #region Constructor

        public AnnotationRequestBaseValidator(AnnotationBaseModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {

                var approvalExists =
                    dbContext.Approvals.Any(
                        t => t.Guid == _model.guid && t.IsDeleted == false && t.DeletePermanently == false);

                if (!approvalExists)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNotFound, "guid"));
                    returnCode = HttpStatusCodeEnum.NotFound;
                }
                else
                {
                    //get user rights info
                    var loggedUser = (from u in dbContext.Users
                                      join us in dbContext.UserStatus on u.Status equals us.ID
                                      let accountId =  (from cas in dbContext.CollaborateAPISessions
                                                       join usr in dbContext.Users on cas.User equals usr.ID
                                                       where cas.SessionKey == _model.sessionKey
                                                       select usr.Account).FirstOrDefault()
                                      where u.Account == accountId && u.Username == _model.username && us.Key == "A"
                                      select new
                                      {
                                          u.ID,
                                          Account = accountId
                                      }).FirstOrDefault();

                    if (loggedUser != null)
                    {
                        ApprovalInfo approval = CheckIfUserHasAccessToThisApproval(_model.guid, loggedUser.ID,_model.hasAccess, dbContext);
                                       
                        if (approval == null)
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNoAccess, "guid"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        } 
                        else if (!HasAnyAnnotations(approval, loggedUser.ID, _model.hasAccess, dbContext))
                        {
                            _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ApprovalNoAnnotations, "guid"));
                            returnCode = HttpStatusCodeEnum.Forbidden;
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(_model.sortBy))
                            {
                                _model.sortBy = Constants.DateLabel;
                            }

                            if (String.IsNullOrEmpty(_model.filterBy))
                            {
                                _model.filterBy = Constants.UsersLabel;
                            }

                            //validate incoming user or groups and remove invalid selections
                            if (_model.filterBy.Equals(Constants.UsersLabel))
                            {
                                if (_model.users != null && _model.users.Any())
                                {
                                    //remove empty entries
                                    _model.users.RemoveAll(t => t.username == null);

                                    //validate internal users
                                    List<UserCollaborator> validUsers = _model.users.Where(t => !t.isExternal).Where(userCollaborator => dbContext.Users.Any(t => t.Username.ToLower() == userCollaborator.username.Trim().ToLower() && t.Account == loggedUser.Account)).ToList();

                                    //validate external users
                                    validUsers.AddRange(_model.users.Where(t => t.isExternal).Where(userCollaborator => dbContext.ExternalCollaborators.Any(t => t.EmailAddress.ToLower() == userCollaborator.username.Trim().ToLower() && t.Account == loggedUser.Account)));
                                    
                                    //replace with validated list
                                    _model.users = validUsers;
                                }
                            }
                            else if (_model.filterBy.Equals(Constants.GroupsLabel))
                            {
                                if (_model.groups != null && _model.groups.Any())
                                {
                                    //validate user groups
                                    List<string> validGroups = _model.groups.Where(userGroup => dbContext.UserGroups.Any(t => t.Name.ToLower() == userGroup.Trim().ToLower() && t.Account == loggedUser.Account)).ToList();
                                    
                                    //replace with validated list
                                    _model.groups = validGroups;
                                }
                            }
                        }
                    }
                    else
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.UserNotFound, "username"));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }

            return returnCode;
        }

        private ApprovalInfo CheckIfUserHasAccessToThisApproval(string approvalGuid, int loggedUserId,bool hasAccess, DbContextBL dbContext)
        {
            return (from ap in dbContext.Approvals
                    join j in dbContext.Jobs on ap.Job equals j.ID

                    let isCollaborator = hasAccess ? true : (from acl in dbContext.ApprovalCollaborators
                                                             where ap.ID == acl.Approval && acl.Collaborator == loggedUserId
                                                             select acl.ID).Any()
                    where ap.Guid == approvalGuid && (isCollaborator || loggedUserId == j.JobOwner)
                    select new ApprovalInfo()
                    {
                        ID = ap.ID,
                        CurrentPhase = ap.CurrentPhase,
                        JobOwner = j.JobOwner
                    }).FirstOrDefault();
        }

        private bool HasAnyAnnotations(ApprovalInfo approval, int loggedUserId,bool hasAccess, DbContextBL dbContext)
        {
            if (approval.CurrentPhase == null)
            {
                return (from app in dbContext.ApprovalPages
                                     join annot in dbContext.ApprovalAnnotations on app.ID equals annot.Page
                                     where app.Approval == approval.ID && annot.Parent == null
                                     select annot.ID).Any();
            }
            return (_model.phases != null && _model.phases.Count > 0)
                                    ? (from ajpa in dbContext.ApprovalJobPhaseApprovals
                                    join apphase in dbContext.ApprovalJobPhases on ajpa.Phase equals apphase.ID
                                    join aph in dbContext.ApprovalPhases on apphase.PhaseTemplateID equals aph.ID

                                    let userIsCollaborator = hasAccess ? true : (from acl in dbContext.ApprovalCollaborators
                                                                where acl.Approval == approval.ID && acl.Collaborator == loggedUserId && acl.Phase == apphase.ID
                                                                select acl.ID).Any()

                                    let hasAnnotations = (from annot in dbContext.ApprovalAnnotations
                                                            join app in dbContext.ApprovalPages on annot.Page equals app.ID
                                                            where app.Approval == approval.ID && annot.Parent == null && annot.Phase == apphase.ID
                                                            select annot.ID).Any()

                                    where ajpa.Approval == approval.ID && _model.phases.Contains(aph.Guid) && hasAnnotations && (userIsCollaborator || loggedUserId == approval.JobOwner)
                                    select apphase.ID).Any()
                                    : (from app in dbContext.ApprovalPages
                                    join annot in dbContext.ApprovalAnnotations on app.ID equals annot.Page

                                    let phasesWhereUserIsCollaborator = (from ajp in dbContext.ApprovalJobPhases
                                                                            join acl in dbContext.ApprovalCollaborators on ajp.ID equals acl.Phase
                                                                            where acl.Approval == approval.ID && acl.Collaborator == loggedUserId
                                                                            select ajp.ID).ToList()

                                    where app.Approval == approval.ID && annot.Parent == null && (phasesWhereUserIsCollaborator.Contains(annot.Phase.Value) || (loggedUserId == approval.JobOwner || hasAccess))
                                    select annot.ID).Any();
            
        }

        #endregion
    }
}
