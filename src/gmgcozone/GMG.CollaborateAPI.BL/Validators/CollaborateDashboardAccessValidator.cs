﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorDAL;

namespace GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators
{
    public class CollaborateDashboardAccessValidator : SessionValidatorBase
    {
        #region Fields

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructors
        
        public CollaborateDashboardAccessValidator(CollaborateAPIIdentityModel model)
            :base(model)
        {
            _model = model;
        }
        
        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(GMGColorBusinessLogic.DbContextBL dbContext)
        {
            var returnCode = base.Validate(dbContext);
            
            if (IsValid)
            {
                var loggedUser = ApiBlHelpers.GetUserDetails(_model.username, _model.sessionKey, dbContext);

                var loggedUserCollaborateRole = Role.GetRole(loggedUser.CollaborateRoleKey(dbContext));

                //Check the user role on collaborate dashboard
                if (loggedUserCollaborateRole != Role.RoleName.AccountAdministrator && loggedUserCollaborateRole != Role.RoleName.AccountManager && loggedUserCollaborateRole != Role.RoleName.AccountModerator)
                {
                    _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.NoRoleForCol, ""));
                    returnCode = HttpStatusCodeEnum.Forbidden;
                }
            }
            return returnCode;
        }

        #endregion
    }
}
