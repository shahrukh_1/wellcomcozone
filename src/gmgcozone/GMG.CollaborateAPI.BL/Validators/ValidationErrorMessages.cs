﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GMG.CollaborateAPI.BL.Validators
{
    public class ValidationErrorMessages
    {
        public const string SessionKeyInvalid = "SessionKey not found";
        public const string UserNotFound = "Active user with this username was not found in the system";
        public const string ExternalUserNotFound = "Active user with this email address was not found in the system";
        public const string DuplicateUser = "Two or more users were found with this username. Please provide accountURL";
        public const string AccountMismatch = "Mismatch between user account and the provided account url";
        public const string SessionKeyNotConfirmed = "SessionKey was not confirmed";
        public const string SessionKeyExpired = "SessionKey Expired";
        public const string AccessKeyInvalid = "The provided AccessKey is invalid";
        public const string GenericError = "An internal server error occured. Please try again later and if the problem persist please contact the service provider";
        public const string ApprovalGuidMissing = "Approval GUID is missing or is not a properly formatted GUID.";
        public const string ApprovalNotFound = "An approval with specified guid was not found";
        public const string ApprovalPhaseNotFound = "An approval phase with specified guid was not found";
        public const string DeleteApprovalNotFound = "A deleted approval with specified guid was not found";
        public const string ApprovalNoAccess = "The user has no access to the specified approval";
		public const string FileTypeIsNotAllowed = "The file type is not allowed";
        public const string CreateActionNotAllowed = "Create job action is not allowed because of the role on collaborate";
        public const string UpdateActionNotAllowed = "Update approval action is not allowed because of the role on collaborate";
        public const string JobGuidDoesNotExist = "The specified job guid does not exist";
        public const string UnrecognizedGuidFormat = "Unrecognized Guid format";
        public const string UserHaveNoPermissionsForTheSpecifiedJob = "You don't have permissions for the specified job";
        public const string ApprovalNoAnnotations = "The specified approval has no annotations made";
        public const string JobNotFound = "A job with the specified guid was not found";
        public const string JobNotOwner = "User is not owner on the last version of the job";
        public const string NoVersionAvailable = "There is no version available for deletion for the current job";
        public const string ApprovalCantBeDeleted = "The specified approval can't be deleted";
        public const string UserIsNoOwner = "User does not have delete rights for the last approval version";
        public const string UserIsNoVersOwner = "User does not have delete rights for the specified approval";
        public const string UpdateJobBadReq = "There is no valid parameter to update";
        public const string UpdateJobArcStatusInvalid = "Current job is not in completed status so it can t be archived";
        public const string UpdateJobCCMStatusInvalid = "This status is not available outside retoucher workflow which is inactive on your account";
        public const string ArchiveStatusCntBeChanged = "Jobs with archived status can t have their status changed";
        public const string ApprovalIsDeleted = "The specified approval is currently marked as deleted";
        public const string JobNoAccess = "The user has no access to the specified job";
        public const string NoSubscriptionPlan = "The user account has no subscription plan on Collaborate module";
        public const string NoRoleForCol = "The user hasn't role access on Collaborate module";
        public const string FolderNotFound = "A folder with specified guid was not found";
        public const string FolderNameNotFound = "A folder with specified name was not found";
        public const string FolderNameExceeds64Chars = "The folder name exceeds 64 characters";
        public const string MetadataNotFound = "Metadata for the specified resource ID was not found";
        public const string EmailInUse = "Email Address is already in use for the current account";
        public const string UserNameInUse = "Email Address is already in use as username in the system";
        public const string FirstNameRequired = "First Name is required";
        public const string LastNameRequired = "Last Name is required";
        public const string PasswordRequired = "Password must contain: between 8 and 20 characters, At least 1 uppercase letter  and At least 1 lowercase letter, At least 1 number, At least 1 special character";
        public const string NotSSOAccount = "You don't have a SSO account";
        public const string UsersLimitReached = "Users limit on subsciption plan has been reached";
        public const string PermissionsRequired = "At least one valid permission is required";
		public const string CreateFolderActionNotAllowed = "Create folder action is not allowed because of the role on collaborate";
        public const string EmailNotFound = "The specified email was not found in the current account";
		public const string GetAllApprovalWorkflowsActionNotAllowed = "Get approval workflows action is not allowed for this account role";
        public const string WorkflowNotFound = "A workflow with the specified guid was not found";
        public const string PDMIsNotPresent = "The mentioned phase requires a PDM selected";
        public const string AtLeastOneApproverAndReviewerUser = "At least one user with Approver and Reviewer role should be selected";
        public const string OnlyOnePDMPerPhase = "Only one PDM is allowed per phase";
        public const string PDMUserShouldHaveANRRole = "The selected PDM must have Approver and Reviewer role";
        public const string WorkflowPhasesInformationNotComplete = "Please fill in all phases coresponding to the mentioned workflow";
        public const string WorkflowPhasesInformationNotCompleteSpecific = "Please fill in the corresponding user with Approver and Reviewer role for the missing workflow phases: ";
        public const string NoPDMForApprovalGroup = "The specified workflow phase, with decision type set to approval group, does not need a PDM selected";
        public const string NoDuplicatedUsers = "Duplicate collaborators are not allowed"; 
        public const string UserIsNotaCollaborator = "User is not a collaborator, Can not remove user";
        public const string OnlyOneDecisionIsRequiredPDM = "When OnlyOneDecisionIsRequired is set, only one PDM is mandatory";
        public const string ThePDMShouldHaveANRRole = "Any collaborator set as Primary Decision Maker should have Approver and Reviewer role set also";
        public const string EmailIsMandatoryForExternalUsers = "The email address is mandatory for external users";
        public const string UsernameIsMandatoryForInternalUsers = "The user name is mandatory for internal users";
        public const string NewExternalPDMMustBeAddedOnFirstPhase = "The new external user can be added as PDM only on first phase or on current phase of the approval";
        public const string DeadlineMustBeHigher = "The deadline must be higher than current date and time";
        public const string InvalidPhaseID = "The provided phase id is not part of the workflow on which the approval is on";
        public const string OnlyOwnerisAllowedToRestWorkflow = "Only the approval owner is allowed to reset the workflow";
        public const string ProvidedPhaseIsGraterThanTheCurrentPhase = "The provided phase si grater than the current phase";
        public const string WorkflowNameIsNotUnique = "The provided workflow name is not unique at the account level";
        public const string WorkflowHasPhasesWithDuplicateNames = "Workflow has multiple phases with the same name";
        public const string WorkflowPhasesDecisionKeyIsIncorrect = "Workflow phase decision is invalid";
        public const string WorkflowPhaseApprovalTriggerKeyIsInvalid = "Workflow phase approval trigger is invalid";
        public const string WorkflowPhaseDecisionMakerIsNotAValidCollaborator = "Workflow phase decision Maker is not a valid collaborator";
        public const string WorkflowPhasePDMIsNotApprovalAndReviewer = "Workflow phase PDM is not Approver and Reviewer";
        public const string WorkflowPhaseGroupShouldHaveAtLeastOneApprovareAndReviewer = "Workflow phase with Approval Group should have at least on collaborated with Approver and Reviewer role";
        public const string WorkflowNameIsMandatory = "Workflow name is mandatory";
        public const string PhaseNameIsMandatory = "Phase name is mandatory";
        public const string PhaseNotFound = "Phase not found";
        public const string CannotAddCollaboratorsToCompletedPhase = "Cannot add collaborators to completed phase";
        public const string UsersAreNotAllowedToBeAdded = "Users are not allowed to be added";
        public const string UserIsNotOwner = "User is not approval owner";
        public const string CollaboratorDoesNotExists = "Collaborator does not exist in the system";
        public const string InvalidUserRole = "Invalid user role provided";
        public const string IncorrectFileName = "A file name can't contain any of the following characters: '.', '\\', '/', '*', '?', '<', '>', '|', '\"' ";
        public const string UserAccountTemporarilyLocked = "Your account has been temporarily locked, Please try after 15 minutes";
        public const string UserAccountPermanentlyLocked = "Your account has been permanently locked, Please reset your password";
        public const string ChecklistNotFound = "An checklist with specified guid was not found";

    }

    public class Constants
    {
        public const string UsersLabel = "users";
        public const string GroupsLabel = "groups";
        public const string UserLabel = "user";
        public const string GroupLabel = "group";
        public const string CommentsLabel = "comments";
        public const string CommentsAndPinsLabel = "commentsandpins";
        public const string PinsLabel = "pins";
        public const string DateLabel = "date";
    }

    public class WarningMessages
    {
        public const string UserDoesNotExist = "The user {0} does not exist. Please note: the user is not added as collaborator";
        public const string TypoInApprovalRoleName = "There is typo in approval role key for the '{0}' user. Therefore, the '{1}' (default) role was applied";
        public const string TypeInApprovaleRoleNameExternalUser = "There is typo in approval role key for the '{0}' external user(s). Please note: for those users the access was not changed.";
        public const string TypeInApprovaleRoleNameInternalUser = "There is typo in approval role key for the '{0}' user(s). Please note: for those users the access was not changed.";
        public const string InvalidEmailExternalUser = "The following '{0}' external user(s) does not exist.";
        public const string InvalidUsernameInternalUser = "The following '{0}' user(s) does not exist.";
        public const string WrongDecisionKey = "Wrong decision key. Therefore, the decision was not updated.";
    }
}
