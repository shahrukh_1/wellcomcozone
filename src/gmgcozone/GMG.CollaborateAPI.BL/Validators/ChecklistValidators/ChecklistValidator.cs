﻿using GMG.CollaborateAPI.BL.Validators.ApprovalWorkflowValidators;
using GMGColor.Areas.CollaborateAPI.Models;
using GMGColorBusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMG.CollaborateAPI.BL.Validators.ChecklistValidators
{
    public class ChecklistValidator: CollaborateDashboardAccessValidator
    {
        #region Properties

        private CollaborateAPIIdentityModel _model;

        #endregion

        #region Constructor

        public ChecklistValidator(CollaborateAPIIdentityModel model) :
            base(model)
        {
            _model = model;
        }

        #endregion

        #region Validate

        public override HttpStatusCodeEnum Validate(DbContextBL dbContext)
        {
            HttpStatusCodeEnum returnCode = base.Validate(dbContext);
            if (IsValid)
            {
                if (!string.IsNullOrEmpty(_model.guid))
                {
                    var checklistExists = dbContext.CheckList.Any(ap => ap.Guid == _model.guid);

                    if (!checklistExists)
                    {
                        _ruleViolations.Add(new RuleViolation(ValidationErrorMessages.ChecklistNotFound, ""));
                        returnCode = HttpStatusCodeEnum.NotFound;
                    }
                }
            }
            return returnCode;
        }

        #endregion
    }
}
