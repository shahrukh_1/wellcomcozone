﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using nClam;
using System.IO;
using System.Xml.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace GMG.CoZone.ScanFile
{
    public class ScanFile
    {

        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static int FindMimeFromData(
    IntPtr pBC,
    [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
    [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
    int cbSize,
    [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
    int dwMimeFlags,
    out IntPtr ppwzMimeOut,
    int dwReserverd
);

        public bool ProcessFileScanner(Stream fileStream, string fileName)
        {
            try
            {
                MaliciousFileCheck(fileStream, fileName); // This will be impemented in next phase
                VirusFileCheck(fileStream);
                return true;
            }
            catch (IndexOutOfRangeException Ex)
            {
                //return "Invalid file. File extension and signature does not match in Validation";
                return false;

            }
            catch (Exception Ex)
            {
                //return Ex.Message.ToString();
                return false;
            }
        }

        private bool MaliciousFileCheck(Stream fileStream, string fileName)
        {
            string extension;
            extension = Path.GetExtension(fileName).ToLower().Replace(".", "");

            List<string> whiteListFileExtensions = new List<string>();
            if (GeneralMimeTypesAllowed(extension))
            {
                //We need to check the file which we recived is present in our WhiteList or not.

                fileStream.Seek(0, SeekOrigin.Begin);
                byte[] buffer = ReadFully(fileStream);

                string originalMimType = MimeTypeFrom(buffer);
                List<string> MimeTypes = new List<string>();

                //Validate the file signature with file extension.
                var ValidExt = string.Empty;
                if (originalMimType != String.Empty)
                {
                    if (originalMimType.Contains("/"))
                    {
                        string[] mimeTypeList = originalMimType.Split('/');
                        foreach (string mime in mimeTypeList)
                        {
                            if (extension.ToUpper() == "JPG" && (mime.ToUpper() == "JPEG" || mime.ToUpper() == "PJPEG"))
                            {
                                ValidExt = mime.ToUpper();
                                break;
                            }
                            else
                            {
                                if (mime.ToUpper().Contains(extension.ToUpper()))
                                {
                                    ValidExt = mime.ToUpper();
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (originalMimType.Contains(extension.ToUpper()))
                        {
                            ValidExt = extension.ToUpper();
                        }
                        else
                            ValidExt = originalMimType.ToUpper();
                    }
                }
                if (!GeneralMimeTypesAllowed(ValidExt.ToLower()))
                {
                    throw new Exception("Invalid file. File extension and signature does not match with " + ValidExt);
                }
                return true;
            }
            else
            {
                if (VideoMimeTypesAllowed(extension)){
                    return true;
                }
                
            }
            return false;
        }

        private bool VirusFileCheck(Stream fileStream)
        {
            try
            {
                System.Threading.Tasks.Task.Run(async () =>
                {
                    var clam = new ClamClient("localhost", 3310);
                    clam.MaxStreamSize = 2097152000;
                    var scanResult = await clam.SendAndScanFileAsync(fileStream);  //any file you would like!
                    switch (scanResult.Result)
                    {
                        case ClamScanResults.Clean:
                            break;
                        case ClamScanResults.VirusDetected:
                            throw new Exception("Virus Detected");
                        case ClamScanResults.Error:
                            throw new Exception(scanResult.RawResult);
                        default:
                            throw new Exception("Unable to scan the file");
                    }
                }).Wait();

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string MimeTypeFrom(byte[] dataBytes)
        {
            if (dataBytes == null)
                throw new ArgumentNullException("dataBytes");
            string mimeRet = String.Empty;
            IntPtr suggestPtr = IntPtr.Zero, filePtr = IntPtr.Zero, outPtr = IntPtr.Zero;
            int ret = FindMimeFromData(IntPtr.Zero, null, dataBytes, dataBytes.Length, null, 0, out outPtr, 0);
            if (ret == 0 && outPtr != IntPtr.Zero)
            {
                mimeRet = Marshal.PtrToStringUni(outPtr);
                Marshal.FreeCoTaskMem(outPtr); //msdn docs wrongly states that operator 'delete' must be used. Do not remove FreeCoTaskMem
                return mimeRet;
            }
            return mimeRet;
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        private bool GeneralMimeTypesAllowed(string extension)
        {
            List<string> extensionsallowed = new List<string> {
                "pdf","ps","jpg","jpeg","pjpeg","png","x-png","tiff","tif","eps","pdf","psd","swf","zip"
            };
            return extensionsallowed.Contains(extension);
        }

        private bool VideoMimeTypesAllowed(string extension)
        {
            List<string> extensionsallowed = new List<string> {
                "mp4","m4v","flv","f4v","mov","mpg","mpg2","3gp","wmv","avi","asf","fm","ogv",
                "rm","qt","mjpeg","vob","xvid","3vix"
            };
            return extensionsallowed.Contains(extension);
        }


    }
}
