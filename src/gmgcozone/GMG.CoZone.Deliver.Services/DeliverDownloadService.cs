﻿using System;
using System.Collections.Generic;
using System.Linq;
using GMG.CoZone.Common.DomainModels;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Deliver.DomainModels;
using GMG.CoZone.Repositories.Interfaces;
using GMGColorDAL;
using static GMGColorDAL.Role;
using AppModule = GMG.CoZone.Common.AppModule;

namespace GMG.CoZone.Deliver.Services
{
    public class DeliverDownloadService: IDeliverDownloadService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFtpService _ftpService;

        public DeliverDownloadService(IUnitOfWork unitOfWork, IFtpService ftpService)
        {
            _unitOfWork = unitOfWork;
            _ftpService = ftpService;
        }

        public void SetFtpDownloadPaths(FileDownloadModel model, int loggedUserId)
        {
            string groupKey = null;
            if (model.FileIds.Count() > 1)
            {
                groupKey = Guid.NewGuid().ToString();
            }

            var ftpPresetJobDownloadModels = model.FileIds.Select(job => new FtpPresetJobDownloadModel {Preset = model.SelectedPresetId, GroupKey = groupKey, DeliverJob = job}).ToList();
            var tempObjIdLists = _ftpService.AddFtpPresetJobDownloads(ftpPresetJobDownloadModels);

            _ftpService.AddMessageToQueue(tempObjIdLists, groupKey, AppModule.Deliver);
        }

        public Dictionary<int, string> GetDownloadPaths(FileDownloadModel model, int loggedUserId)
        {
            // var relativePaths = new List<string>();
            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            try
            {
                var deliverFileModels = _unitOfWork.DeliverJobRepository.GetDeliverJobsByIds(model.FileIds);

                //TODO this line must be uncommented after send notification functionality will be refatored. The return type of this method will be only List<string>.
                //relativePaths = GetDeliverJobPaths(deliverFileModels);

                //TODO to be removed after send notification functionality will be refatored
                foreach (var deliverFileModel in deliverFileModels)
                {
                    dictionary.Add(deliverFileModel.Id, "deliver/" + deliverFileModel.Guid + "/" + deliverFileModel.FileName);
                }
            }
            catch (Exception ex)
            {
                GMGColorLogging.log.ErrorFormat(ex, model.AccountId, "Logged User: {0}, Error in Base Control -> DeliverDownloadService -> GetDownloadPaths Method. Exception : {1}", loggedUserId, ex.Message);
            }
            
            return dictionary;
        }

        private List<string> GetDeliverJobPaths(List<DeliverFileModel> deliverFileModels)
        {
            return deliverFileModels.Select(deliverJob => "deliver/" + deliverJob.Guid + "/" + deliverJob.FileName).ToList();
        }
    }
}
