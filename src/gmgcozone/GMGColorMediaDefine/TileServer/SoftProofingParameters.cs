﻿using System;
using System.Linq;
using System.Xml.Linq;
using GMG.CoZone.Common;
using GMGColorDAL;

namespace GMGColorMediaDefine.TileServer
{
    public class SoftProofingParameters
    {
        #region Properties

        public bool PaperTint { get; set; }
        public int ApprovalPageId { get; set; }
        public string ActiveChannels { get; set; }
        public string OutputIntentProfile { get; set; }
        public string SoftProofingSessionGuid { get; set; }
        public bool IsDefaultProfile { get; set; }
        public int CurrentZoomLevel { get; set; }
        public string CurrentViewTiles { get; set; }
        public int? PaperTintId { get; set; }
        public int? SimulationProfileId { get; set; }
        public int? EmbeddedProfileId { get; set; }
        public int? CustomProfileId { get; set; }
        public bool IsSessionHighRes { get; set; }

        #endregion

        #region Public Methods

        public void LoadFromXML(string xmlDoc)
        {
            XDocument doc = XDocument.Parse(xmlDoc);

            ApprovalPageId = Convert.ToInt32(doc.Descendants(Constants.PAGEID).FirstOrDefault().Value);

            SoftProofingSessionGuid = doc.Descendants(Constants.SESSIONGUID).FirstOrDefault().Value;
        }

        public void FillSessionParamaters(SoftProofingSessionParam sessionParamBO)
        {
            PaperTint = sessionParamBO.SimulatePaperTint;
            ActiveChannels = sessionParamBO.ActiveChannels;
            CurrentZoomLevel = sessionParamBO.ZoomLevel;
            CurrentViewTiles = sessionParamBO.ViewTiles;
            IsSessionHighRes = sessionParamBO.HighResolution;

            if (sessionParamBO.ViewingCondition1 != null)
            {
                SimulationProfileId = sessionParamBO.ViewingCondition1.SimulationProfile;
                EmbeddedProfileId = sessionParamBO.ViewingCondition1.EmbeddedProfile;
                CustomProfileId = sessionParamBO.ViewingCondition1.CustomProfile;
                PaperTintId = sessionParamBO.ViewingCondition1.PaperTint;
            }
        }

        #endregion
    }
}
