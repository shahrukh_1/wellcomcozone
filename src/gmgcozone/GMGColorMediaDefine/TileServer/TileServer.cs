﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.SQS.Model;
using GMG.CoZone.Common;
using GMGColor.AWS;
using GMGColor.MediaProcessor;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorMediaDefine.Common;
using Utils = GMGColorMediaDefine.Common.Utils;

namespace GMGColorMediaDefine.TileServer
{
    public class TileServer
    {
        #region Private Fields

        private Task _checkTileServerQueue;
        private Task _updateVisibilityTimeout;
        private Task _checkCacheExpiration;
        private CancellationTokenSource _checkTileServerQueueCTS = new CancellationTokenSource();
        private CancellationTokenSource _updateVisibilityTimeoutCTS = new CancellationTokenSource();
        private CancellationTokenSource _checkCacheExpirationCTS = new CancellationTokenSource();
        private volatile int _tileServerWorkerThreads;
        private int _timeLeftTileServerQueue = 0;
        private int _timePeriodTileServerQueue = 0;
        private static string _monitorDesaturationProfile;

        #endregion

        #region Properties

        private List<Utils.SQSMessage> ProcessingSQSMessages { get; set; }

        public static string MonitorDesaturationProfile
        {
            get
            {
                return _monitorDesaturationProfile;
            }
        }

        #endregion

        #region Public Methods

        public void StartTileServer()
        {
            ProcessingSQSMessages = new List<Utils.SQSMessage>();

            SoftProofRenderer.InitRenderer();

            if (!Utils.GetDefaultProfileAbsolutePath(GMGColorConfiguration.AppConfiguration.MonitorDesaturationProfile, out _monitorDesaturationProfile))
            {
                Utils.LogEvent("MonitorDesaturationProfile profile could not be found at specified path", EventLogEntryType.Error);
                throw new Exception("MonitorDesaturationProfile profile not found!");
            }

            _checkTileServerQueue = new Task(CheckTileServerQueue);
            _checkTileServerQueue.Start();
            Utils.LogEvent("GMGCoZone TileServer CheckTileServerQueue event was started.", EventLogEntryType.Information);

            if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
            {
                _updateVisibilityTimeout = new Task(UpdateVisibilityTimeOut);
                _updateVisibilityTimeout.Start();
                Utils.LogEvent("GMGCoZone TileServer UpdateVisibilityTimeOut event was started.",
                    EventLogEntryType.Information);

                //in case multiple instance of the service are running on the same machine, start this thread only on the first service to avoid concurrency file access issues
                if (ConfigurationManager.AppSettings.Get("ServiceName") == MediaProcessor.DEFSERVICENAME)
                {
                    _checkCacheExpiration = new Task(CheckCacheExpiration);
                    _checkCacheExpiration.Start();
                    Utils.LogEvent("GMGCoZone TileServer CheckCacheExpiration event was started.",
                        EventLogEntryType.Information);
                }
            }
        }

        public void StopTileServer()
        {
            _checkTileServerQueueCTS.Cancel();
            _updateVisibilityTimeoutCTS.Cancel();
            TileServerProcessor.ReleaseOutputProfileBuffer();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Check Tile Server Queue For Approval Pages to be processed.
        /// </summary>
        private void CheckTileServerQueue()
        {
            //cache outputProfileBuffer
            TileServerProcessor.LoadOutputProfileBuffer();

            while (!_checkTileServerQueueCTS.IsCancellationRequested)
            {
                bool queueIsEmpty = false;
                string messageQueueName = GMGColorConfiguration.AppConfiguration.TileServerQueueName;

                try
                {
                    string itemGuid = Guid.NewGuid().ToString().ToUpper();

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                    {
                        #region SQS
                        try
                        {
                            int readCount = (GMGColorConfiguration.AppConfiguration.TileServerNrOfThreads - _tileServerWorkerThreads);
                            if (readCount > 0)
                            {
                                ReceiveMessageResponse oResponse = AWSSimpleQueueService.CheckQueueForMessages(messageQueueName, readCount, TileServerQueueVisibilityTimePeriod, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
                                if (oResponse.Messages.Count > 0)
                                {
                                    foreach (Amazon.SQS.Model.Message processMsg in oResponse.Messages)
                                    {
                                        if (ProcessingSQSMessages.SingleOrDefault(o => o.ReceiptHandle == processMsg.ReceiptHandle) == null)
                                        {
                                            //check if the same page for same soft proof condition is already in processing
                                            SoftProofingParameters softProofParams = new SoftProofingParameters();
                                            softProofParams.LoadFromXML(processMsg.Body);
                                            if (!IsPageAlreadyProcessing(softProofParams))
                                            {
                                                WorkerThreadData wThread = new WorkerThreadData(true, itemGuid, processMsg, string.Empty, messageQueueName);

                                                Task processApprovalPageTask = new Task(() => ExecuteWokerThread(wThread, softProofParams));
                                                processApprovalPageTask.Start();

                                                _tileServerWorkerThreads++;

                                                Utils.LogEvent(String.Format("GMGCoZone TileServer: Process Approval Page {0} SoftProofSessionGuid {1} Thread Started.",
                                                    softProofParams.ApprovalPageId, softProofParams.SoftProofingSessionGuid),
                                                    EventLogEntryType.SuccessAudit);

                                                // Sleep thread for a seconds
                                                Thread.Sleep(200);
                                            }
                                            else
                                            {
                                                Utils.LogEvent(String.Format("GMGCoZone TileServer: Process Approval Page {0} SoftProofSessionGuid {1} ignored because already processing.",
                                                    softProofParams.ApprovalPageId, softProofParams.SoftProofingSessionGuid),
                                                    EventLogEntryType.SuccessAudit);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    queueIsEmpty = true;
                                }
                            }
                            else
                            {
                                queueIsEmpty = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessingSQSMessages.RemoveAll(o => o.Guid == itemGuid);
                            Utils.LogEvent(String.Format("GMGCoZone Tile Server: Exception thrown by CheckTileServerQueue. Queue Name: {0}, ex-{1}", messageQueueName, ex.Message), EventLogEntryType.Error);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Local

                        string message;
                        try
                        {
                            int readCount = (GMGColorConfiguration.AppConfiguration.TileServerNrOfThreads - _tileServerWorkerThreads);
                            if (readCount > 0)
                            {
                                MessageQueue mq = new MessageQueue(messageQueueName);
                                System.Messaging.Message msg = mq.Receive(new TimeSpan(0, 0, 3));
                                msg.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
                                message = msg.Body.ToString();

                                //check if the same page for same soft proof condition is already in processing
                                SoftProofingParameters softProofParams = new SoftProofingParameters();
                                softProofParams.LoadFromXML(message);
                                if (!IsPageAlreadyProcessing(softProofParams))
                                {
                                    WorkerThreadData wThread = new WorkerThreadData(false, itemGuid, null, message,
                                        string.Empty);

                                    Task processApprovalPageTask = new Task(() => ExecuteWokerThread(wThread, softProofParams));
                                    processApprovalPageTask.Start();

                                    _tileServerWorkerThreads++;

                                    Utils.LogEvent(String.Format("GMGCoZone TileServer: Process Approval Page {0} SoftProofSessionGuid {1} Thread Started.",
                                        softProofParams.ApprovalPageId, softProofParams.SoftProofingSessionGuid),
                                        EventLogEntryType.SuccessAudit);
                                }
                            }

                            // Sleep thread for a seconds
                            Thread.Sleep(1000);
                        }
                        catch (MessageQueueException ex)
                        {
                            if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                queueIsEmpty = true;
                            }
                            else
                            {
                                Utils.LogEvent(String.Format("GMGCoZone Tile Server: Exception thrown by CheckTileServerQueue. Queue Name: {0}, ex-{1}", messageQueueName, ex.Message), EventLogEntryType.Error);
                            }
                        }
                        #endregion
                    }

                    // Sleep thread for 5 seconds
                    if (queueIsEmpty)
                    {
                        Thread.Sleep(1000 * 5); // Perform main service function here... Simulate some lengthy operations
                    }
                }
                catch (Exception ex)
                {
                    Utils.LogEvent(String.Format("GMGCoZone Tile Server: Exception thrown by CheckTileServerQueue. Queue Name: {0}, ex-{1}", messageQueueName, ex.Message), EventLogEntryType.Error);
                }
            }
        }

        private int TileServerQueueVisibilityTimePeriod
        {
            get
            {
                if (_timePeriodTileServerQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorConfiguration.AppConfiguration.TileServerQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSRegion);

                        _timePeriodTileServerQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        Utils.LogEvent("Media Processor unable to get the SQS TileServerQueueName attributes. Exception: " + ex.Message, EventLogEntryType.Error);
                    }

                    if (_timePeriodTileServerQueue < 240)
                    {
                        _timePeriodTileServerQueue = 240; // 240 Seconds
                    }
                    _timeLeftTileServerQueue = 120;
                }

                return _timePeriodTileServerQueue;
            }
        }

        private int TileServerQueueVisibilityTimeLeft
        {
            get
            {
                if (_timeLeftTileServerQueue == 0)
                {
                    return TileServerQueueVisibilityTimePeriod;
                }

                return _timeLeftTileServerQueue;
            }
        }

        /// <summary>
        /// Update visibility timeout for jobs that are still in processing
        /// </summary>
        private void UpdateVisibilityTimeOut()
        {
            while (!_updateVisibilityTimeoutCTS.IsCancellationRequested)
            {
                try
                {
                    Utils.UpdateVisibilityTimeOut(ProcessingSQSMessages, TileServerQueueVisibilityTimeLeft, TileServerQueueVisibilityTimePeriod, GMGColorConfiguration.AppConfiguration.TileServerQueueName);

                    // Sleep thread for 5 seconds
                    Thread.Sleep(1000 * 5); // Perform main service function here... Simulate some lengthy operations.
                }
                catch (Exception ex)
                {
                    Utils.LogEvent("GMGCoZone Tile Server: Exception thrown by UpdateVisibilityTimeOut. ex-" + ex.Message, EventLogEntryType.Error);
                }
            }
        }

        private bool IsPageAlreadyProcessing(SoftProofingParameters softProofingParams)
        {
            try
            {
                if (
                    ProcessingSQSMessages.Any(
                        t =>
                            t.ApprovalPage == softProofingParams.ApprovalPageId &&
                            t.SoftProofingGuid == softProofingParams.SoftProofingSessionGuid))
                {
                    return true;
                }

            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone TileServer: Exception thrown by IsPageAlreadyProcessing, while processing measseage. Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
            }

            return false;
        }

        /// <summary>
        /// Execure current approval page message on dedicated thread
        /// </summary>
        /// <param name="wThread">Current Thread Data</param>
        private void ExecuteWokerThread(WorkerThreadData wThread, SoftProofingParameters softProofParams)
        {   
            try
            {
                TileServerProcessor tileServerProcessor = new TileServerProcessor();
                bool success = false;
                string errorMessage = String.Empty;

                if (wThread.IsEnabledAmazonSQS)
                {
                    // Add message to the list to extend the time-out
                    ProcessingSQSMessages.Add(new Utils.SQSMessage { Guid = wThread.ItemGuid, ReceiptHandle = wThread.ProcessMessage.ReceiptHandle,
                                                                        NextTimeOut = DateTime.Now.AddSeconds(TileServerQueueVisibilityTimePeriod),
                                                                        ApprovalPage = softProofParams.ApprovalPageId, SoftProofingGuid = softProofParams.SoftProofingSessionGuid });

                    success = tileServerProcessor.ProcessApprovalPage(softProofParams, out errorMessage);

                    // Delete the message from queue
                    try
                    {
                        AWSSimpleQueueService.DeleteMessage(wThread.MessageQueueName, wThread.ProcessMessage, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
                    }
                    catch (Exception ex)
                    {
                        Utils.LogEvent("Exception in ExecuteWorkerThread TileServer : DeleteMessage from Amazon SQS, Exception:" + ex.Message, EventLogEntryType.Error);
                    }
                }
                else
                {
                    // Add message to the list to extend the time-out
                    ProcessingSQSMessages.Add(new Utils.SQSMessage { Guid = wThread.ItemGuid, ApprovalPage =  softProofParams.ApprovalPageId, SoftProofingGuid = softProofParams.SoftProofingSessionGuid});
                    success = tileServerProcessor.ProcessApprovalPage(softProofParams, out errorMessage);
                }

                // Log processing failiure to database
                if (!success)
                {
                    SoftProofingSessionErrorBL.LogSoftProofingSessionError(softProofParams.ApprovalPageId, softProofParams.SoftProofingSessionGuid, errorMessage);
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone TileServer: Exception thrown by ExecuteWorkerThread, while processing measseage. Exception: {0}", ex.Message), EventLogEntryType.Error, ex);
            }
            finally
            {
                ProcessingSQSMessages.RemoveAll(o => o.Guid == wThread.ItemGuid);
                _tileServerWorkerThreads--;
            }
        }

        /// <summary>
        /// Cleanup Cache folder remove folders that haven t been accessed from a certain period of time
        /// </summary>
        private void CheckCacheExpiration()
        {
            string cacheFolder =
                Path.Combine(
                    GMGColorConfiguration.AppConfiguration.PathToProcessingFolder +
                    GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath, ApprovalJobFolder.CACHE_FOLDER);
            
            if (!Directory.Exists(cacheFolder))
            {
                Directory.CreateDirectory(cacheFolder);
            }
            while (!_checkCacheExpirationCTS.IsCancellationRequested)
            {
                try
                {
                    //iterate through approval folder
                    foreach (var approvalFolder in Directory.GetDirectories(cacheFolder))
                    {
                        //iterate through page folder
                        foreach (var pageFolder in Directory.GetDirectories(approvalFolder))
                        {
                            if ((DateTime.Now - Directory.GetCreationTime(pageFolder)).TotalHours > GMGColorConfiguration.AppConfiguration.BandsCacheExpirationHours)
                            {
                                //delete pageFolder
                                Directory.Delete(pageFolder, true);
                            }
                        }
                        if (!Directory.GetDirectories(approvalFolder).Any())
                        {
                            // if all page folder have been deleted also delete approval folder
                            Directory.Delete(approvalFolder, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Utils.LogEvent(String.Format("GMGCoZone Tile Server: Exception thrown by CheckCacheExpiration.  ex-{0} stacktrace-{1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
                }
                Thread.Sleep(TimeSpan.FromHours(1));
            }
        }

        #endregion
    }
}
