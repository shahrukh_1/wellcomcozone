﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using GMG.CoZone.Common;
using GMGColor.AWS;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorDAL.CustomModels;
using GMGColorMediaDefine.Common;

namespace GMGColorMediaDefine.TileServer
{
    public class TileServerProcessor
    {
        #region Nested Classes

        private class OutputProfileBuffer : IDisposable
        {
            #region Private Fields

            private GCHandle _outputProfileHandle;

            #endregion

            #region Properties

            public uint BufferLenght { get; private set; }

            public IntPtr OutputProfileData { get; private set; }

            #endregion

            #region Public Methods

            /// <summary>
            /// Load profile from file in memory into IntPtr for marshalling
            /// </summary>
            public void LoadBuffer()
            {
                //create buffer from output profile
                string outputProfilePath;
                GMGColorMediaDefine.Common.Utils.GetDefaultProfileAbsolutePath(GMGColorConfiguration.AppConfiguration.OutputProfile, out outputProfilePath);
                byte[] outputProfileBuffer = File.ReadAllBytes(outputProfilePath);
                _outputProfileHandle = GCHandle.Alloc(outputProfileBuffer, GCHandleType.Pinned);
                OutputProfileData = _outputProfileHandle.AddrOfPinnedObject();
                BufferLenght = (uint) outputProfileBuffer.Length;
            }

            /// <summary>
            /// Release handle
            /// </summary>
            public void Dispose()
            {
                if (_outputProfileHandle.IsAllocated)
                {
                    _outputProfileHandle.Free();
                }
            }

            #endregion
        }

        #endregion

        #region Private Fields

        private static object _cleanupObjectsLock = new object();
        private static List<CleanupObject> _cleanupObjectsQueue = new List<CleanupObject>();
        private static OutputProfileBuffer _outputProfileBuffer;

        #endregion

        #region Public Methods

        /// <summary>
        /// Process Approval page with current softProofingParams
        /// </summary>
        /// <param name="softProofingParams"></param>
        /// <returns></returns>
        [HandleProcessCorruptedStateExceptions]
        public bool ProcessApprovalPage(SoftProofingParameters softProofingParams, out string errorMessage)
        {
            string tempOutputFolder = String.Empty;
            errorMessage = "NoError";
            try
            {
                ApprovalPageProcessingDetails approvalDetails = null;
                ApprovalJobFolder approvalJobfolder = null;
                var timer = new Stopwatch();

                bool isRGBDocument;
                using (DbContextBL context = new DbContextBL())
                {
                    //get parameters for current session from database
                    SoftProofingSessionParam sessionParamsBO =
                        ApprovalBL.GetSessionParamsBySessionGuid(softProofingParams.SoftProofingSessionGuid, context);
                    if (sessionParamsBO == null)
                    {
                        throw new Exception(
                            String.Format("SoftProofing Session with Session Guid {0} not found in database !",
                                softProofingParams.SoftProofingSessionGuid));
                    }

                    softProofingParams.FillSessionParamaters(sessionParamsBO);

                    approvalDetails = ApprovalBL.GetPageProcessingDetails(softProofingParams.ApprovalPageId,
                                                                          softProofingParams.EmbeddedProfileId,
                                                                          softProofingParams.CustomProfileId, 
                                                                          softProofingParams.SimulationProfileId,
                                                                          softProofingParams.PaperTintId,
                                                                          context);
                    isRGBDocument = ApprovalBL.IsRGBDocument(approvalDetails.PageChannels, context);



                    approvalJobfolder = new ApprovalJobFolder(approvalDetails.JobGuid,
                        approvalDetails.FileName,
                        GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket
                            ? GMGColorConfiguration.AppConfiguration.PathToProcessingFolder
                            : GMGColorConfiguration.AppConfiguration.FileSystemPathToDataFolder,
                        GMGColorConfiguration.AppConfiguration.PathToDataFolder(approvalDetails.Region));

                    string softproofingSessionPageRelativePath =
                        Path.Combine(approvalJobfolder.ApprovalFolderRelativePath,
                            softProofingParams.SoftProofingSessionGuid, approvalDetails.PageNumber.ToString());

                    //check if this softproofing session for this page has already been processed
                    if (GMGColorIO.FolderExists(softproofingSessionPageRelativePath, approvalDetails.Region))
                    {
                        GMGColorMediaDefine.Common.Utils.LogEvent(
                            String.Format(
                                "TileServerProcessor:ApprovalPage {0} with sessionId {1} is already processed, avoid processing again!",
                                softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid),
                            EventLogEntryType.Warning);
                        return true;
                    }

                    // set the temp folder for the S3 setup
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        tempOutputFolder = GMGColorConfiguration.AppConfiguration.PathToProcessingFolder +
                                           Guid.NewGuid() +
                                           @"\";
                    }
                    else
                    {
                        tempOutputFolder = approvalJobfolder.ApprovalProcessingFolderPath +
                                           softProofingParams.SoftProofingSessionGuid + @"\" +
                                           approvalDetails.PageNumber;
                    }

                    Directory.CreateDirectory(tempOutputFolder);
                   
                    if (isRGBDocument)
                    {
                        timer.Restart();
                        CreateTilesFromRGB(approvalJobfolder, approvalDetails, tempOutputFolder);
                        timer.Stop();
                        GMGColorMediaDefine.Common.Utils.LogEvent(
                            String.Format(
                                "TileServerProcessor:CreateTilesFromRGB method for page {0} with sessionId {1} took {2} ms",
                                softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid,
                                timer.ElapsedMilliseconds), EventLogEntryType.Information);
                    }
                    else
                    {
                        ProcessCMYKPage(approvalJobfolder, approvalDetails, softProofingParams, tempOutputFolder, context);
                    }
                }

                //Upload to s3
                if (softProofingParams.IsSessionHighRes || !File.Exists(approvalJobfolder.ApprovalProcessingFolderPath + "\\UHR.txt"))
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        UploadTilesToS3(approvalJobfolder, approvalDetails, softProofingParams, tempOutputFolder, timer);
                    }
                    else
                    {
                        //create dummy file for proofstudio to know when all the files are uploaded and ready to be used
                        string partialCompletedMarkerPath = Path.Combine(tempOutputFolder,
                            ApprovalJobFolder.PARTIAL_COMPLETED_MARKER);
                        File.WriteAllText(partialCompletedMarkerPath, DateTime.UtcNow.ToString());
                        string completedMarkerPath = Path.Combine(tempOutputFolder, ApprovalJobFolder.COMPLETED_MARKER);
                        File.WriteAllText(completedMarkerPath, DateTime.UtcNow.ToString());
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                AddObjectTocleanup(new CleanupObject(tempOutputFolder));
                errorMessage = String.Format("Exception in TileServerProcessor:ProcessApprovalPage {0} with sessionGuid {1} :  Exception:{2}; Stack Trace {3}", softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid, ex.Message, ex.StackTrace);
                GMGColorMediaDefine.Common.Utils.LogEvent(errorMessage, EventLogEntryType.Error, ex);
            }

            return false;
        }

        /// <summary>
        /// Cache OutputProfile Buffer on Service Startup
        /// </summary>
        public static void LoadOutputProfileBuffer()
        {
            if (_outputProfileBuffer == null)
            {
                _outputProfileBuffer = new OutputProfileBuffer();
                _outputProfileBuffer.LoadBuffer();
            }
        }

        /// <summary>
        /// Relese output profile buffer on service stop
        /// </summary>
        public static void ReleaseOutputProfileBuffer()
        {
            if (_outputProfileBuffer != null)
            {
                _outputProfileBuffer.Dispose();
            }
        }

        /// <summary>
        /// Remove the temporary objects (must be called on a dedicated thread)
        /// </summary>
        public static void ProcessTileServerCleanupQueue()
        {
            lock (_cleanupObjectsLock)
            {
                foreach (CleanupObject obj in _cleanupObjectsQueue)
                {
                    var uhrFile = obj.Path + "\\UHR.txt";

                    if ((DateTime.UtcNow - obj.AddedTime).TotalMinutes > 1 && !File.Exists(uhrFile))
                    {
                        if (obj.IsFile)
                        {
                            try
                            {
                                if (obj.NrOfRetries <= 5 && File.Exists(obj.Path))
                                    File.Delete(obj.Path);
                                obj.IsRemoved = true;
                            }
                            catch (System.Exception ex)
                            {
                                GMGColorMediaDefine.Common.Utils.LogEvent(
                                    string.Format(
                                        "TileServerProcessor: An error occured while deleting the file {0}, Exception: {1}, StackTrace: {2}, ActiveConfiguration: {3}",
                                        obj.Path, ex.Message, ex.StackTrace.ToString(),
                                        GMGColorConfiguration.AppConfiguration.ActiveConfiguration),
                                    EventLogEntryType.Error);
                                obj.NrOfRetries++;
                            }
                        }
                        else
                        {
                            try
                            {
                                if (obj.NrOfRetries <= 5)
                                {
                                    var dir = new DirectoryInfo(obj.Path);
                                    if (dir.Exists)
                                        dir.Delete(true);
                                }
                                obj.IsRemoved = true;
                            }
                            catch (System.Exception ex)
                            {
                                GMGColorMediaDefine.Common.Utils.LogEvent(
                                    string.Format(
                                        "TileServerProcessor: An error occured while deleting the directory {0}, Exception: {1}, StackTrace: {2}, ActiveConfiguration: {3}",
                                        obj.Path, ex.Message, ex.StackTrace.ToString(),
                                        GMGColorConfiguration.AppConfiguration.ActiveConfiguration),
                                    EventLogEntryType.Error);
                                obj.NrOfRetries++;
                            }
                        }
                    }
                }

                _cleanupObjectsQueue.RemoveAll(item => item.IsRemoved);
            }
        }

        #endregion

        #region Private Methods

        // writes a marker that files were uploaded to amazon ws
        private void WriteMarkerToAWSSimpleStorage(string tempOutputFolder, string markerName, ApprovalJobFolder approvalJobfolder, ApprovalPageProcessingDetails approvalDetails, SoftProofingParameters softProofingParams)
        {
            string markerPath = Path.Combine(tempOutputFolder, markerName);
            File.WriteAllText(markerPath, DateTime.UtcNow.ToString());

            GMGColor.AWS.AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(approvalDetails.Region),
                                                            markerPath,
                                                            approvalJobfolder.GetPageSoftProofingSessionRelativeFolderPath((uint)approvalDetails.PageNumber, softProofingParams.SoftProofingSessionGuid) + markerName,
                                                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                            GMGColorConfiguration.AppConfiguration.AWSSecretKey);
        }

        /// <summary>
        /// Creates the transform for the given job page
        /// </summary>
        /// <param name="approvalJobFolder"></param>
        /// <returns></returns>
        private IntPtr CreateTransform(ApprovalJobFolder approvalJobFolder, ApprovalPageProcessingDetails pageDetails, SoftProofingParameters spParams, string tempOutputFolder, DbContextBL context)
        {
            string inputProfilePath = String.Empty;
            string outputProfilePath = String.Empty;

            bool disposeInputProfile = false;

            if (spParams.CustomProfileId != null)
            {
                //custom profile
                inputProfilePath = Common.Utils.GetCustomProfilePath(pageDetails.CustomProfileDetails, tempOutputFolder, pageDetails.Region);
                disposeInputProfile = true;
            }
            else if (spParams.EmbeddedProfileId != null)
            {
                //embedded profile
                inputProfilePath = GetEmbeddedProfilePath(approvalJobFolder, pageDetails, tempOutputFolder);
                disposeInputProfile = true;
            }
            else if (spParams.SimulationProfileId != null)
            {
                //simulation profile
                if (pageDetails.SimulationProfileDetails.IsGlobal)
                {
                    string remoteProfileRepoURI = GMGColorConfiguration.RemoteProfileRepoURI;
                    Common.Utils.GetProfileAbsolutePath(pageDetails.SimulationProfileDetails.Name, remoteProfileRepoURI, out inputProfilePath);
                }
                else
                {
                    inputProfilePath = Common.Utils.GetSimulationProfilePath(pageDetails.SimulationProfileDetails, tempOutputFolder, pageDetails.Region);
                    disposeInputProfile = true;
                }
            }

            Common.Utils.GetDefaultProfileAbsolutePath(GMGColorConfiguration.AppConfiguration.OutputProfile, out outputProfilePath);

            if (!File.Exists(inputProfilePath))
            {
                throw new Exception("Input intentn profile " + inputProfilePath + " not found");
            }
            if (!File.Exists(outputProfilePath))
            {
                throw new Exception("Output intentn profile " + outputProfilePath + " not found");
            }

            Stopwatch timer = new Stopwatch();
            
            //check if there are extra plates
            if (pageDetails.PageChannels.Any(t => t.IsSpotChannel))
            {
                timer.Start();

                string mergedOutputProfilePath = Path.Combine(GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? tempOutputFolder : Directory.GetParent(tempOutputFolder).FullName, ApprovalJobFolder.CUSTOM_ICC_PROFILE);

                //check if the merged profile was already created for a previous page
                if (
                    GMGColorIO.FileExists(
                        approvalJobFolder.GetCustomICCRelativeFilePath(spParams.SoftProofingSessionGuid),
                        pageDetails.Region))
                {
                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        var client = new WebClient();
                        client.DownloadFile(
                            approvalJobFolder.DataStorageFolderPath +
                            HttpUtility.UrlEncode(
                                approvalJobFolder.GetCustomICCRelativeFilePath(spParams.SoftProofingSessionGuid)),
                            mergedOutputProfilePath);
                        inputProfilePath = mergedOutputProfilePath;
                    }
                    else
                    {
                        inputProfilePath = mergedOutputProfilePath;
                    }
                }
                else
                {
                    foreach (
                        var extraPlate in
                            pageDetails.PageChannels.Where(t => t.IsSpotChannel).OrderBy(t => t.ChannelIndex))
                    {
                        //get lab color measurements from db for current spot channel
                        List<LabColorMeasurement> extraPlateLabColorMeasurements =
                            (from labclr in context.LabColorMeasurements
                                where labclr.ApprovalSeparationPlate == extraPlate.ID
                                select labclr).ToList();

                        double[] labValues = new double[extraPlateLabColorMeasurements.Count*4];
                        uint labValueIndex = 0;
                        foreach (var labMeasurement in extraPlateLabColorMeasurements.OrderBy(t => t.Step))
                        {
                            labValues[labValueIndex++] = labMeasurement.Step;
                            labValues[labValueIndex++] = labMeasurement.L;
                            labValues[labValueIndex++] = labMeasurement.a;
                            labValues[labValueIndex++] = labMeasurement.b;
                        }

                        string tempProfilePath = Path.Combine(tempOutputFolder, extraPlate.ChannelIndex + "_1ch.icc");

                        //create merged profile with current spot channel
                        SoftProofRenderer.CreateMergedProfile(extraPlate.Name, labValues,
                            (uint) extraPlateLabColorMeasurements.Count, inputProfilePath,
                            mergedOutputProfilePath, tempProfilePath);
                        inputProfilePath = mergedOutputProfilePath;

                        //delete one channel temp profiles
                        if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                        {
                            File.Delete(tempProfilePath);
                        }
                    }

                    timer.Stop();
                    Common.Utils.LogEvent(
                        String.Format(
                            "TileServerProcessor:Create CustomICCProfile for pageNumber {0} for approval {1} took {2} ms",
                            pageDetails.PageNumber, pageDetails.JobGuid, timer.ElapsedMilliseconds),
                        EventLogEntryType.Information);

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        GMGColorIO.FolderExists(
                            approvalJobFolder.GetSoftProofingSessionRelativeFolderPath(spParams.SoftProofingSessionGuid),
                            pageDetails.Region, true);
                        using (var stream = new FileStream(inputProfilePath, FileMode.Open))
                        {
                            GMGColorIO.UploadFile(stream,
                                approvalJobFolder.GetCustomICCRelativeFilePath(spParams.SoftProofingSessionGuid),
                                pageDetails.Region);
                        }
                    }
                }  
            }

            if (pageDetails.PaperTintWhite != null)
            {
                string adjustedWithPaperTint = Path.Combine(tempOutputFolder, ApprovalJobFolder.PAPERTINT_ADJUSTED_NAME);
                int err = SoftProofRenderer.AdjustProfilePaperWhite(
                                                        inputProfilePath,
                                                        pageDetails.PaperTintWhite.L,
                                                        pageDetails.PaperTintWhite.a,
                                                        pageDetails.PaperTintWhite.b,
                                                        adjustedWithPaperTint);

                if (err != 0)
                {
                    Common.Utils.LogEvent(String.Format("TileServerProcessor: AdjustProfilePaperWhite for approval {0} , pageNr {1}, errorCode {2}", pageDetails.JobGuid, pageDetails.PageNumber, err), EventLogEntryType.Error);
                }
                else
                {
                    inputProfilePath = adjustedWithPaperTint;
                }
            }

             timer.Restart();
            IntPtr transform =  SoftProofRenderer.CreateTransform(inputProfilePath, TileServer.MonitorDesaturationProfile, outputProfilePath, spParams.PaperTint, (uint)pageDetails.PageChannels.Count);
            timer.Stop();
            Common.Utils.LogEvent(String.Format("TileServerProcessor:Create LCMS Transform handler for pageNumber {0} for approval {1} took {2} ms",
                                                                        pageDetails.PageNumber, pageDetails.JobGuid, timer.ElapsedMilliseconds), EventLogEntryType.Information);
            //if doc has embedded profile that was downloaded to local instance delete it after processing
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                if (disposeInputProfile)
                {
                    File.Delete(inputProfilePath);
                }
            }

            return transform;
        }

        /// <summary>
        /// Download bands, apply lcms transformation, create and tag tiles with output profile for all zoom levels
        /// </summary>
        /// <param name="transformHandler">LCMS transform Handler</param>
        /// <param name="approvalJobFolder">Approval Folder Structure</param>
        /// <param name="approvalPageDetails">Details about current processing page</param>
        /// <param name="pageOutputFolderPath">Output Folder</param>
        /// <param name="activeChannels">Current active channels</param>
        private void CreateTilesFromCMYK(IntPtr transformHandler, ApprovalJobFolder approvalJobFolder, ApprovalPageProcessingDetails approvalPageDetails, string pageOutputFolderPath, string activeChannels)
        {
            //get relative path to the Bands folder
            string bandsRelativePath = Path.Combine(approvalJobFolder.GetPageBandsFolder((uint)approvalPageDetails.PageNumber), ApprovalJobFolder.BAND_FORMAT);
            uint currentBandIndex = 0;
            int channelCount = approvalPageDetails.PageChannels.Count;

            //Setup Rendering objects
            SoftProofRenderer softProofRenderer = new SoftProofRenderer(null, null, false);
            RenderEngineTileSettings tileSettings = new RenderEngineTileSettings(256, 256, 20, (uint)GMGColorConfiguration.AppConfiguration.DefaultImageSize, approvalPageDetails.Resolution, 100, Approval.ProcessedThumbnailDimension);
            softProofRenderer.InitTileSettings(tileSettings, (uint)approvalPageDetails.PageNumber - 1, pageOutputFolderPath, (uint)approvalPageDetails.PageWidth, (uint)approvalPageDetails.PageHeight);

            //create mask for inactive channels
            ulong channelsMask;
            GetChannelMask(activeChannels, approvalPageDetails.PageChannels, out channelsMask);

            string currentBandRelativePath = String.Format(bandsRelativePath, currentBandIndex);

            Stopwatch timer = new Stopwatch();
            while (GMGColorIO.FileExists(currentBandRelativePath, approvalPageDetails.Region))
            {
                timer.Restart();
                string cacheErrorMessage = null;
                byte[] bandRawByteArray = GMGColorIO.GetByteArrayOfAFile(currentBandRelativePath, approvalJobFolder.GetPageBandCachePath((uint)approvalPageDetails.PageNumber, currentBandIndex), approvalPageDetails.Region, ref cacheErrorMessage);
                timer.Stop();
                Common.Utils.LogEvent(String.Format("TileServerProcessor:Get memory stream for pageNumber {0} for approval {1} for band {2} took {3} ms",
                                                                        approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, currentBandIndex, timer.ElapsedMilliseconds), EventLogEntryType.Information);

                if (!String.IsNullOrEmpty(cacheErrorMessage))
                {
                    Common.Utils.LogEvent(String.Format("TileServerProcessor:Get band from cache failed for page {0} for approval {1} for band {2} errMsg: {3}",
                                                                        approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, currentBandIndex, cacheErrorMessage), EventLogEntryType.Error);
                }

                timer.Restart();
                softProofRenderer.ApplyTransformAndCreateFirstLevelTiles(transformHandler, bandRawByteArray,
                    (uint) channelCount, channelsMask, _outputProfileBuffer.OutputProfileData,
                    _outputProfileBuffer.BufferLenght);

                Common.Utils.LogEvent(String.Format("TileServerProcessor:ApplyTransformAndCreateFirstLevelTiles for pageNumber {0} for approval {1} for band {2} took {3} ms",
                                                                       approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, currentBandIndex, timer.ElapsedMilliseconds), EventLogEntryType.Information);

                //move to the next band
                currentBandRelativePath = String.Format(bandsRelativePath, ++currentBandIndex);
            }

            try
            {
                //update creation time for page folder in cache to exclude it from thread that cleans up unused folders after a period of time
                Directory.SetCreationTime(approvalJobFolder.GetPageCacheFolder((uint)approvalPageDetails.PageNumber), DateTime.Now);
            }
            catch (Exception ex)
            {
                Common.Utils.LogEvent(String.Format("TileServerProcessor: Update cache folder creation time failed for approval {0} , pageNr {1}, ex:{2}", approvalPageDetails.JobGuid, approvalPageDetails.PageNumber, ex.Message), EventLogEntryType.Warning);
            }

            timer.Restart();
            //create tiles for all remaining zoom levels
            softProofRenderer.DownSampleTiles(_outputProfileBuffer.OutputProfileData, _outputProfileBuffer.BufferLenght);
            timer.Stop();
            Common.Utils.LogEvent(String.Format("TileServerProcessor:DownSampleTiles for pageNumber {0} for approval {1} took {2} ms",
                                                                            approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, timer.ElapsedMilliseconds), EventLogEntryType.Information);

            //release memory
            tileSettings.Dispose();
            softProofRenderer.Dispose();
        }

        /// <summary>
        /// Download bands, create and tag tiles with output profile for all zoom levels for documents with rgb color space
        /// </summary>
        /// <param name="transformHandler">LCMS transform Handler</param>
        /// <param name="approvalJobFolder">Approval Folder Structure</param>
        /// <param name="approvalPageDetails">Details about current processing page</param>
        /// <param name="pageOutputFolderPath">Output Folder</param>
        /// <param name="activeChannels">Current active channels</param>
        private void CreateTilesFromRGB(ApprovalJobFolder approvalJobFolder, ApprovalPageProcessingDetails approvalPageDetails, string pageOutputFolderPath)
        {
            //get relative path to the Bands folder
            string bandsRelativePath = Path.Combine(approvalJobFolder.GetPageBandsFolder((uint)approvalPageDetails.PageNumber), "Band{0}.bin");
            uint currentBandIndex = 0;

            //Setup Rendering objects
            SoftProofRenderer softProofRenderer = new SoftProofRenderer(null, null, false);
            RenderEngineTileSettings tileSettings = new RenderEngineTileSettings(256, 256, 20, (uint)GMGColorConfiguration.AppConfiguration.DefaultImageSize, approvalPageDetails.Resolution, 100, Approval.ProcessedThumbnailDimension);
            softProofRenderer.InitTileSettings(tileSettings, (uint)approvalPageDetails.PageNumber - 1, pageOutputFolderPath, (uint)approvalPageDetails.PageWidth, (uint)approvalPageDetails.PageHeight);

            string currentBandRelativePath = String.Format(bandsRelativePath, currentBandIndex);

            //variables needed to tag default or embbeded profile in tiles
            IntPtr taggedTilesProfile = _outputProfileBuffer.OutputProfileData;
            uint taggedProfileSize = _outputProfileBuffer.BufferLenght;
            GCHandle embeddedProfileHandler = new GCHandle();
            bool useEmbeddedProfile = false;
            //chech if page or doc has embedded profile and it it has use it to embedd it in the tiles
            if (!String.IsNullOrEmpty(approvalPageDetails.DocumentEmbeddedProfile))
            {
                string embeddedProfileRelativePath =  approvalJobFolder.EmbededDocumentProfileRelativePath(approvalPageDetails.DocumentEmbeddedProfile);

                byte[] profileByteArray = GMGColorIO.GetByteArrayOfAFile(embeddedProfileRelativePath, approvalPageDetails.Region);

                //create pointer for raw data band
                embeddedProfileHandler = GCHandle.Alloc(profileByteArray, GCHandleType.Pinned);
                taggedTilesProfile = embeddedProfileHandler.AddrOfPinnedObject();
                taggedProfileSize = (uint)profileByteArray.Length;
                
                useEmbeddedProfile = true;
            }

            Stopwatch timer = new Stopwatch();
            while (GMGColorIO.FileExists(currentBandRelativePath, approvalPageDetails.Region))
            {
                timer.Restart();
                byte[] bandRawByteArray = GMGColorIO.GetByteArrayOfAFile(currentBandRelativePath, approvalPageDetails.Region);
                timer.Stop();
                Common.Utils.LogEvent(
                    String.Format(
                        "TileServerProcessor:Get memory stream for pageNumber {0} for approval {1} for band {2} took {3} ms",
                        approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, currentBandIndex,
                        timer.ElapsedMilliseconds), EventLogEntryType.Information);

                //create pointer for raw data band
                GCHandle rawDataHandler = GCHandle.Alloc(bandRawByteArray, GCHandleType.Pinned);
                IntPtr bandRawData = rawDataHandler.AddrOfPinnedObject();
                int rawbandSize = bandRawByteArray.Length;

                timer.Restart();
                //create tiles for first zoom level
                softProofRenderer.CreateFirstLevelTiles(bandRawData, (uint) rawbandSize, taggedTilesProfile,
                    taggedProfileSize);

                timer.Stop();
                Common.Utils.LogEvent(
                    String.Format(
                        "TileServerProcessor:Create First Level Tiles for pageNumber {0} for approval {1} for band {2} took {3} ms",
                        approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, currentBandIndex,
                        timer.ElapsedMilliseconds), EventLogEntryType.Information);
                rawDataHandler.Free();

                //move to the next band
                currentBandRelativePath = String.Format(bandsRelativePath, ++currentBandIndex);
            }

            timer.Restart();
            //create tiles for all remaining zoom levels
            softProofRenderer.DownSampleTiles(taggedTilesProfile, taggedProfileSize);
            timer.Stop();
            Common.Utils.LogEvent(String.Format("TileServerProcessor:DownSampleTiles for pageNumber {0} for approval {1} took {2} ms",
                                                approvalPageDetails.PageNumber, approvalPageDetails.JobGuid, timer.ElapsedMilliseconds), EventLogEntryType.Information);

            if (useEmbeddedProfile)
            {
                embeddedProfileHandler.Free();
            }
            //release memory
            tileSettings.Dispose();
            softProofRenderer.Dispose();
        }

        /// <summary>
        /// Adds a new object for being removed
        /// </summary>
        private void AddObjectTocleanup(CleanupObject cleanupObject)
        {
            lock (_cleanupObjectsLock)
            {
                _cleanupObjectsQueue.Add(cleanupObject);
            }
        }

        /// <summary>
        /// Get Channels mask to mask inactive channels
        /// </summary>
        /// <param name="activeChannels">Active channels</param>
        /// <param name="documentChannels">All channels from DB</param>
        /// <param name="channelsMask">Output Mask containing only active channels</param>
        private void GetChannelMask(string activeChannels, List<ApprovalSeparationPlate> documentChannels, out ulong channelsMask)
        {
            channelsMask = UInt64.MaxValue;
            if (activeChannels != null)
            {
                //if there is at least one inactive channel
                int[] activeChannelsIndexes = activeChannels.Split(',').Select(int.Parse).ToArray();
                foreach (var channel in documentChannels)
                {
                    if (!activeChannelsIndexes.Contains(channel.ChannelIndex.GetValueOrDefault())) // if channel is not selected 
                    {
                        channelsMask &= ~((ulong)1 << channel.ChannelIndex.GetValueOrDefault()); // remove it from the mask
                    }
                }
            }
        }

        /// <summary>
        /// Gets the path for the page/document embedded profile
        /// </summary>
        /// <param name="approvalJobFolder"></param>
        /// <param name="pageDetails"></param>
        /// <param name="tempOutputFolder"></param>
        /// <param name="documentHasEmbeddedProfile"></param>
        /// <returns></returns>
        private string GetEmbeddedProfilePath(ApprovalJobFolder approvalJobFolder, ApprovalPageProcessingDetails pageDetails, string tempOutputFolder)
        {
            string embeddedProfilePath = String.Empty;

            // check if the document has an embeded profile
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                if (!String.IsNullOrEmpty(pageDetails.DocumentEmbeddedProfile))
                {
                    embeddedProfilePath = Path.Combine(tempOutputFolder, pageDetails.DocumentEmbeddedProfile);
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(approvalJobFolder.DataStorageFolderPath + HttpUtility.UrlEncode(approvalJobFolder.EmbededDocumentProfileRemoteRelativePath(pageDetails.DocumentEmbeddedProfile)), embeddedProfilePath);
                    }
                }
            }
            else
            {
                embeddedProfilePath = approvalJobFolder.EmbededDocumentProfileProocessPath(pageDetails.DocumentEmbeddedProfile);
            }

            return embeddedProfilePath;
        }

        /// <summary>
        /// Process Current Approval Page using CMYK Version of the Engine which includes LCMS
        /// </summary>
        /// <param name="approvalJobfolder"></param>
        /// <param name="approvalDetails"></param>
        /// <param name="softProofingParams"></param>
        /// <param name="tempOutputFolder"></param>
        private bool ProcessCMYKPage(ApprovalJobFolder approvalJobfolder, ApprovalPageProcessingDetails approvalDetails, SoftProofingParameters softProofingParams, string tempOutputFolder, DbContextBL context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            IntPtr transformHandler = CreateTransform(approvalJobfolder, approvalDetails, softProofingParams, tempOutputFolder, context);
            timer.Stop();
            GMGColorMediaDefine.Common.Utils.LogEvent(String.Format("TileServerProcessor:CreateTransform method for page {0} with sessionId {1} took {2} ms", softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid, timer.ElapsedMilliseconds), EventLogEntryType.Information);

            if (transformHandler != IntPtr.Zero)
            {
                timer.Restart();
                CreateTilesFromCMYK(transformHandler, approvalJobfolder, approvalDetails, tempOutputFolder, softProofingParams.ActiveChannels);
                timer.Stop();
                GMGColorMediaDefine.Common.Utils.LogEvent(String.Format("TileServerProcessor:CreateTiles method for page {0} with sessionId {1} took {2} ms", softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid, timer.ElapsedMilliseconds), EventLogEntryType.Information);
                SoftProofRenderer.DeleteTransform(transformHandler);

                return true;
            }
            else
            {
                throw new Exception(String.Format("TileServerProcessor:CreateTransform method failed, null pointer returned for page {0}", softProofingParams.ApprovalPageId));
            }
        }

        /// <summary>
        /// Upload Tiles to S3 for AWS Environment
        /// </summary>
        /// <param name="approvalJobfolder"></param>
        /// <param name="approvalDetails"></param>
        /// <param name="softProofingParams"></param>
        /// <param name="tempOutputFolder"></param>
        /// <param name="timer"></param>
        private void UploadTilesToS3(ApprovalJobFolder approvalJobfolder, ApprovalPageProcessingDetails approvalDetails,
            SoftProofingParameters softProofingParams, string tempOutputFolder, Stopwatch timer)
        {
            try
            {
                timer.Restart();
                /*
                        * load tiles in the following order:
                        * 1) 0-0-0, z-v-v + one for each previous zoom levels to make sure it does the rendering
                        * 2) z-x-x
                        * 3) y-y-y
                        * if z-v-v exists write partial completed marker after 1)
                        * if z-v-v does not exist write partial completed marker after 2)
                        */
                List<string> fileNameContainsRegExp = new List<string>() { @"0-0-0" };
                List<string> visibleTiles = new List<string>();

                if (!string.IsNullOrWhiteSpace(softProofingParams.CurrentViewTiles) &&
                    // CurrentViewTiles exists and it is in the correct format
                    (Regex.Match(softProofingParams.CurrentViewTiles, @"\d+-\d+,\d+-\d+").Success))
                {
                    try
                    {
                        // softProofingParams.CurrentViewTiles is of the form x-x,y-y
                        // x-x is the left top tile and y-y is the bottom-down tile
                        // from softProofingParams.CurrentViewTiles the matrix of tiles is extracted
                        string[] tiles = softProofingParams.CurrentViewTiles.Split(',');
                        int[] start = new int[2];
                        int[] end = new int[2];

                        string[] tilesStart = tiles[0].Split('-');
                        start[0] = Int32.Parse(tilesStart[0]);
                        start[1] = Int32.Parse(tilesStart[1]);
                        string[] tilesEnd = tiles[1].Split('-');
                        end[0] = Int32.Parse(tilesEnd[0]);
                        end[1] = Int32.Parse(tilesEnd[1]);

                        for (int i = start[0]; i <= end[0]; i++)
                        {
                            for (int j = start[1]; j <= end[1]; j++)
                            {
                                visibleTiles.Add("" + i + "-" + j);
                            }
                        }
                        if (visibleTiles.Any()) // adds one tile per lower levels to make sure it does the rendering
                        {
                            int zLevel = softProofingParams.CurrentZoomLevel - 1;
                            int i = 4;
                            while (zLevel > 0)
                            {
                                int x = (start[0] + end[0]) / i;
                                int y = (start[1] + end[1]) / i;
                                fileNameContainsRegExp.Add(@"" + zLevel + @"-" + x + @"-" + y);
                                zLevel -= 1;
                                i *= 4;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                List<string> uploadedFiles = new List<string>();
                var uploadAggregateExceptions = new ConcurrentQueue<Exception>();

                if (visibleTiles.Any()) //if z-v-v exists write partial completed marker after 1)
                {
                    foreach (var visibleTile in visibleTiles)
                    {
                        fileNameContainsRegExp.Add(@"" + softProofingParams.CurrentZoomLevel + @"-" + visibleTile);
                    }
                    uploadedFiles.AddRange(
                        AWSSimpleStorageService.UploadSpecificFilesInFolderByFileInParallel(
                            GMGColorConfiguration.AppConfiguration.DataFolderName(approvalDetails.Region),
                            tempOutputFolder,
                            approvalJobfolder.GetPageSoftProofingSessionRelativeFolderPath(
                                (uint)approvalDetails.PageNumber, softProofingParams.SoftProofingSessionGuid),
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                            fileNameContainsRegExp,
                            uploadedFiles,
                            uploadAggregateExceptions));

                    WriteMarkerToAWSSimpleStorage(tempOutputFolder, ApprovalJobFolder.PARTIAL_COMPLETED_MARKER,
                        approvalJobfolder, approvalDetails, softProofingParams);
                    timer.Stop();
                    GMGColorMediaDefine.Common.Utils.LogEvent(
                        String.Format(
                            "TileServerProcessor:Upload visible tiles to S3 method for page {0} with sessionId {1} took {2} ms",
                            softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid,
                            timer.ElapsedMilliseconds), EventLogEntryType.Information);
                    timer.Restart();

                    fileNameContainsRegExp.Clear();
                    fileNameContainsRegExp.Add(@"" + softProofingParams.CurrentZoomLevel + @"-\d+-\d+");

                    uploadedFiles.AddRange(
                        AWSSimpleStorageService.UploadSpecificFilesInFolderByFileInParallel(
                            GMGColorConfiguration.AppConfiguration.DataFolderName(approvalDetails.Region),
                            tempOutputFolder,
                            approvalJobfolder.GetPageSoftProofingSessionRelativeFolderPath(
                                (uint)approvalDetails.PageNumber, softProofingParams.SoftProofingSessionGuid),
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                            fileNameContainsRegExp,
                            uploadedFiles,
                            uploadAggregateExceptions));
                }
                else // if z-v-v does not exist write partial completed marker after 2)
                {
                    fileNameContainsRegExp.Add(@"" + softProofingParams.CurrentZoomLevel + @"-\d+-\d+");

                    uploadedFiles.AddRange(
                        AWSSimpleStorageService.UploadSpecificFilesInFolderByFileInParallel(
                            GMGColorConfiguration.AppConfiguration.DataFolderName(approvalDetails.Region),
                            tempOutputFolder,
                            approvalJobfolder.GetPageSoftProofingSessionRelativeFolderPath(
                                (uint)approvalDetails.PageNumber, softProofingParams.SoftProofingSessionGuid),
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                            fileNameContainsRegExp,
                            uploadedFiles,
                            uploadAggregateExceptions));
                    WriteMarkerToAWSSimpleStorage(tempOutputFolder, ApprovalJobFolder.PARTIAL_COMPLETED_MARKER,
                        approvalJobfolder, approvalDetails, softProofingParams);
                    timer.Stop();
                    GMGColorMediaDefine.Common.Utils.LogEvent(
                        String.Format(
                            "TileServerProcessor:Upload partial tiles to S3 method for page {0} with sessionId {1} took {2} ms",
                            softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid,
                            timer.ElapsedMilliseconds), EventLogEntryType.Information);
                    timer.Restart();

                }

                fileNameContainsRegExp.Clear();
                fileNameContainsRegExp.Add(@"\d+-\d+-\d+");

                uploadedFiles.AddRange(
                    AWSSimpleStorageService.UploadSpecificFilesInFolderByFileInParallel(
                        GMGColorConfiguration.AppConfiguration.DataFolderName(approvalDetails.Region),
                        tempOutputFolder,
                        approvalJobfolder.GetPageSoftProofingSessionRelativeFolderPath(
                            (uint)approvalDetails.PageNumber, softProofingParams.SoftProofingSessionGuid),
                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                        fileNameContainsRegExp,
                        uploadedFiles,
                        uploadAggregateExceptions));

                //create dummy file for proofstudio to know when all the files are uploaded and ready to be used
                WriteMarkerToAWSSimpleStorage(tempOutputFolder, ApprovalJobFolder.COMPLETED_MARKER,
                    approvalJobfolder, approvalDetails, softProofingParams);

                AddObjectTocleanup(new CleanupObject(tempOutputFolder));

                timer.Stop();
                GMGColorMediaDefine.Common.Utils.LogEvent(
                    String.Format(
                        "TileServerProcessor:Upload rest of files to S3 method for page {0} with sessionId {1} took {2} ms",
                        softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid,
                        timer.ElapsedMilliseconds), EventLogEntryType.Information);
                //if any exceptions encountered in parralel upload task
                if (uploadAggregateExceptions.Count > 0)
                {
                    throw new AggregateException(uploadAggregateExceptions);
                }
            }
            catch (AggregateException agEx)
            {
                foreach (var ex in agEx.InnerExceptions)
                {
                    string errorMessage = String.Format("Exception in TileServerProcessor:ProcessApprovalPage {0} with sessionGuid {1} :  Exception:{2}; Stack Trace {3}", softProofingParams.ApprovalPageId, softProofingParams.SoftProofingSessionGuid, ex.Message, ex.StackTrace);
                    GMGColorMediaDefine.Common.Utils.LogEvent(errorMessage, EventLogEntryType.Error, ex);
                }
            }
        }

        #endregion
    }
}
