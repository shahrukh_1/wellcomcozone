﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amazon.SQS.Model;
using GMG.CoZone.Common;
using GMGColor.AWS;
using GMGColor.MediaProcessor;
using GMGColorBusinessLogic;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorMediaDefine.Common;
using Utils = GMGColorMediaDefine.Common.Utils;

namespace GMGColorMediaDefine.MediaDefine
{
    public class MediaDefine
    {
        #region Private Field
        public MessageQueue mq;

        private Task _checkMediaDefineQueue;
        private Task _updateVisibilityTimeout;
        private CancellationTokenSource _checkMediaDefineQueueCTS = new CancellationTokenSource();
        private CancellationTokenSource _updateVisibilityTimeoutCTS = new CancellationTokenSource();

        private volatile int defineWokerThreadsCount = 0;
        private int timeLeftApprovalJobQueue = 0;
        private int timePeriodApprovalJobQueue = 0;

        private string videoTranscoderPipelineID = null;
        private string videoTranscoderPresetID = null;
        private static bool isWebPageProcessing;
        private static int defSimulationProfile = 0;


        #endregion

        #region Properties

        private List<Utils.SQSMessage> ProcessingSQSMessages { get; set; }

        private int ApprovalJobQueueVisibilityTimeLeft
        {
            get
            {
                if (timeLeftApprovalJobQueue == 0)
                {
                    return ApprovalJobQueueVisibilityTimePeriod;
                }

                return timeLeftApprovalJobQueue;
            }
        }

        private int ApprovalJobQueueVisibilityTimePeriod
        {
            get
            {
                if (timePeriodApprovalJobQueue == 0)
                {
                    try
                    {
                        var result = AWSSimpleQueueService.GetQueueAttribute(GMGColorConfiguration.AppConfiguration.ApprovalJobQueueName,
                                                                             AWSSimpleQueueService.SQSAttribute.VisibilityTimeout,
                                                                             GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                             GMGColorConfiguration.AppConfiguration.AWSRegion);

                        timePeriodApprovalJobQueue = result.VisibilityTimeout;
                    }
                    catch (Exception ex)
                    {
                        Utils.LogEvent("Media Processor unable to get the SQS ApprovalQueueName attributes. Exception: " + ex.Message, EventLogEntryType.Error);
                    }

                    if (timePeriodApprovalJobQueue < 240)
                    {
                        timePeriodApprovalJobQueue = 240; // 240 Seconds
                    }
                    timeLeftApprovalJobQueue = this.GetTimeLeft(timePeriodApprovalJobQueue);
                }

                return timePeriodApprovalJobQueue;
            }
        }

        public static int DefaultSimulationProfileId
        {
            get { return defSimulationProfile; }
        }

        #endregion

        #region Public Methods

        public void StartMediaDefine()
        {
            ProcessingSQSMessages = new List<Utils.SQSMessage>();

            // Log a service start message to the Application log.
            Utils.LogEvent("GMGCoZone Media Define Start.", EventLogEntryType.Information);

            if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
            {
                VideoTranscoderSetup();

                _updateVisibilityTimeout = new Task(UpdateVisibilityTimeOut);
                _updateVisibilityTimeout.Start();
                Utils.LogEvent("GMGCoZone TileServer UpdateVisibilityTimeOut event was started.", EventLogEntryType.Information);
            }

            _checkMediaDefineQueue = new Task(CheckDefineQueue);
            _checkMediaDefineQueue.Start();
            Utils.LogEvent("GMGCoZone MediaDefine CheckMediaDefineQueue event was started.", EventLogEntryType.Information);

            Utils.LogEvent(String.Format("GMGCoZone Media Service Configuration. ApprovalJobQueueName: {0}",
                                                   GMGColorConfiguration.AppConfiguration.ApprovalJobQueueName), EventLogEntryType.Information);

        }

        public void StopMediaDefine()
        {
            _checkMediaDefineQueueCTS.Cancel();
            _updateVisibilityTimeoutCTS.Cancel();
        }

        #endregion

        #region Invoke Methods

        private void CheckDefineQueue()
        {
            try
            {
                try
                {
                    using (var context = new DbContextBL())
                    {
                        defSimulationProfile = (from simPr in context.SimulationProfiles
                            where simPr.AccountId == null && simPr.FileName == GMGColorConfiguration.AppConfiguration.InputProfile
                            select simPr.ID).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    Utils.LogEvent(
                                    String.Format(
                                        "GMGCoZone Media Define Service: Exception thrown by CheckApprovalJobQueue. Failed to get defSimProfId, ex-{0}",
                                         ex.Message), EventLogEntryType.Error);
                }

                while (!_checkMediaDefineQueueCTS.IsCancellationRequested)
                {
                    bool queueIsEmpty = false;
                    string messageQueueName = GMGColorConfiguration.AppConfiguration.ApprovalJobQueueName;

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledAmazonSQS)
                    {
                        #region SQS

                        string itemGuid = Guid.NewGuid().ToString().ToUpper();
                        try
                        {
                            int readCount = (GMGColorConfiguration.AppConfiguration.MediaDefineNrOfThreads - defineWokerThreadsCount);
                            if (readCount > 0)
                            {
                                ReceiveMessageResponse oResponse = AWSSimpleQueueService.CheckQueueForMessages(messageQueueName, readCount, this.ApprovalJobQueueVisibilityTimePeriod, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
                                if (oResponse.Messages.Count > 0)
                                {
                                    foreach (Amazon.SQS.Model.Message processMsg in oResponse.Messages)
                                    {
                                        if (this.ProcessingSQSMessages.SingleOrDefault(o => o.ReceiptHandle == processMsg.ReceiptHandle) == null)
                                        {
                                            //determine msg job type
                                            CustomDictionary<string, string> msgDict = ProcessFile.GetMessageParameters(processMsg.Body);

                                            string approvalType = msgDict[Constants.ApprovalMsgApprovalType].Trim();
                                            if (approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.Image.ToString() || approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.Movie.ToString())
                                            {
                                                WorkerThreadData wThread = new WorkerThreadData(true, itemGuid, processMsg, string.Empty, messageQueueName);
                                                wThread.IsVideo = approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.Movie.ToString() ? true : false;

                                                Task approvalJobTask = new Task(() => ExecuteWokerThread(wThread));
                                                approvalJobTask.Start();

                                                defineWokerThreadsCount++;

                                                Utils.LogEvent("GMGCoZone Media Define Service: Thread Started.", EventLogEntryType.SuccessAudit);

                                            }
                                            else if (approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.WebPage.ToString() && !isWebPageProcessing)
                                            {
                                                Utils.LogEvent("WebPage message read from sqs queue", EventLogEntryType.Information);

                                                isWebPageProcessing = true;
                                                WorkerThreadData wThread = new WorkerThreadData(true, itemGuid, processMsg, string.Empty, messageQueueName);

                                                Task approvalJobTask = new Task(() => ExecuteWebPageThread(wThread));
                                                approvalJobTask.Start();
                                            }
                                            
                                            // Sleep thread for a seconds
                                            Thread.Sleep(200);
                                        }
                                    }
                                }
                                else
                                {
                                    queueIsEmpty = true;
                                }
                            }
                            else
                            {
                                queueIsEmpty = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessingSQSMessages.RemoveAll(o => o.Guid == itemGuid);
                            Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by CheckApprovalJobQueue. Queue Name: {0}, ex-{1}", messageQueueName, ex.Message), EventLogEntryType.Error);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Local

                        string message;
                        try
                        {
                            int readCount = (GMGColorConfiguration.AppConfiguration.MediaDefineNrOfThreads -
                                             defineWokerThreadsCount);
                            if (readCount > 0)
                            {
                                mq = new MessageQueue(messageQueueName);
                                System.Messaging.Message msg = mq.Receive(new TimeSpan(0, 0, 3));
                                msg.Formatter = new XmlMessageFormatter(new String[] {"System.String,mscorlib"});
                                message = msg.Body.ToString();

                                //determine msg job type
                                CustomDictionary<string, string> msgDict = ProcessFile.GetMessageParameters(message);

                                string approvalType = msgDict["ApprovalType"].Trim();

                                if (approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.Image.ToString() ||
                                    approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.Movie.ToString())
                                {
                                    WorkerThreadData wThread = new WorkerThreadData(false, string.Empty, null,
                                        message,
                                        string.Empty);

                                    Task approvalJobTask = new Task(() => ExecuteWokerThread(wThread));
                                    approvalJobTask.Start();

                                    defineWokerThreadsCount++;

                                    Utils.LogEvent("GMGCoZone Media Define Service: Thread Started.",
                                        EventLogEntryType.SuccessAudit);
                                }
                                else if (approvalType == GMGColorDAL.Approval.ApprovalTypeEnum.WebPage.ToString())
                                {
                                    WorkerThreadData wThread = new WorkerThreadData(false, string.Empty, null,
                                        message,
                                        string.Empty);

                                    Task approvalJobTask = new Task(() => ExecuteWebPageThread(wThread));
                                    approvalJobTask.Start();
                                }
                                
                            }

                            // Sleep thread for a seconds
                            Thread.Sleep(1000);
                        }
                        catch (MessageQueueException ex)
                        {
                            if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                queueIsEmpty = true;
                            }
                            else
                            {
                                Utils.LogEvent(
                                    String.Format(
                                        "GMGCoZone Media Define Service: Exception thrown by CheckApprovalJobQueue. Queue Name: {0}, ex-{1}",
                                        messageQueueName, ex.Message), EventLogEntryType.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.LogEvent(
                                    String.Format(
                                        "GMGCoZone Media Define Service: Exception thrown by CheckApprovalJobQueue. Queue Name: {0}, ex-{1}",
                                        messageQueueName, ex.Message), EventLogEntryType.Error);
                        }
                        #endregion
                    }

                    // Sleep thread for 10 seconds
                    if (queueIsEmpty)
                    {
                        Thread.Sleep(1000 * 10); // Perform main service function here... Simulate some lengthy operations
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by ExecuteThread:PTB_LibInit in CheckDefineQueue, Callas PDF Software. Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
            }
        }

        private void UpdateVisibilityTimeOut()
        {
            try
            {
                while (!_updateVisibilityTimeoutCTS.IsCancellationRequested)
                {
                    try
                    {
                        Utils.UpdateVisibilityTimeOut(ProcessingSQSMessages, ApprovalJobQueueVisibilityTimeLeft, ApprovalJobQueueVisibilityTimePeriod, GMGColorConfiguration.AppConfiguration.ApprovalJobQueueName);

                        // Sleep thread for 5 seconds
                        Thread.Sleep(1000 * 5); // Perform main service function here... Simulate some lengthy operations.
                    }
                    catch (Exception ex)
                    {
                        Utils.LogEvent("GMGCoZone Media Define Service: Exception thrown by UpdateVisibilityTimeOut. ex-" + ex.Message, EventLogEntryType.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent("GMGCoZone Media Define Service: Exception thrown by UpdateVisibilityTimeOut. ex-" + ex.Message, EventLogEntryType.Error);
            }
        }

        #endregion

        #region Methods

        private void ExecuteWokerThread(WorkerThreadData wThread)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    ProcessFile objProcessFile = new ProcessFile(context);
                    try
                    {
                        if (wThread.IsEnabledAmazonSQS)
                        {
                            // Add message to the list to extend the time-out
                            ProcessingSQSMessages.Add(new Utils.SQSMessage() { Guid = wThread.ItemGuid, ReceiptHandle = wThread.ProcessMessage.ReceiptHandle, NextTimeOut = DateTime.Now.AddSeconds(this.ApprovalJobQueueVisibilityTimePeriod) });

                            bool processWithSuccess = true;
                            // Put the message into process
                            if (wThread.IsVideo)
                            {
                                objProcessFile.TranscodeVideoFile(wThread.ProcessMessage.Body, videoTranscoderPipelineID, videoTranscoderPresetID);
                            }
                            else
                            {
                                processWithSuccess = objProcessFile.ProcessGivenFile(wThread.ProcessMessage.Body);
                            }

                            if (processWithSuccess)
                            {
                                // Delete the message from queue
                                try
                                {
                                    AWSSimpleQueueService.DeleteMessage(wThread.MessageQueueName, wThread.ProcessMessage, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
                                }
                                catch (Exception ex)
                                {
                                    Utils.LogEvent("Exception in ExecuteWorkerThread : DeleteMessage from Amazon SQS, Exception:" + ex.Message, EventLogEntryType.Error);
                                }
                            }
                        }
                        else
                        {
                            objProcessFile.ProcessGivenFile(wThread.MessageBody);
                        }
                        //check if approval has profile output preset and needs to be uploaded to FTP;
                    }
                    catch (Exception ex)
                    {
                        Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by ExecuteThread:ProcessGivenFile, while processing measseage. Exception: {0}", ex.Message), EventLogEntryType.Error);
                        throw ex;
                    }
                    finally
                    {
                        if (wThread.IsEnabledAmazonSQS)
                        {
                            // Remove the message from list
                            this.ProcessingSQSMessages.RemoveAll(o => o.Guid == wThread.ItemGuid);
                        }
                        defineWokerThreadsCount--;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by ExecuteThread:PTB_LibInit, Callas PDF Software. Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
            }
        }

        private void ExecuteWebPageThread(WorkerThreadData wThread)
        {
            try
            {
                using (DbContextBL context = new DbContextBL())
                {
                    ProcessFile objProcessFile = new ProcessFile(context);
                    try
                    {
                        string messageBody = wThread.IsEnabledAmazonSQS ? wThread.ProcessMessage.Body : wThread.MessageBody;

                        string capturingImagePath = GMGColorConfiguration.AppConfiguration.PathToProcessingFolder + Guid.NewGuid().ToString() + ".png";
                        string Url = ProcessFile.GetMessageParameters(messageBody)["Url"].ToString().Trim();
                        string approvalID = ProcessFile.GetMessageParameters(messageBody)["ApprovalID"].ToString().Trim();
                        string webCapturedelay = ProcessFile.GetMessageParameters(messageBody)["WebCaptureDelay"].ToString().Trim();
                        int delay;
                        int.TryParse(webCapturedelay, out delay);

                        int approval;
                        int.TryParse(approvalID, out approval);

                        int nrOfTries = 0;

                        while (!objProcessFile.ConvertHtmlToImage(Url, capturingImagePath, delay, approval) && nrOfTries < GMGColorConfiguration.AppConfiguration.WebPageNrOfTries)
                        {
                            nrOfTries++;
                            Utils.LogEvent(String.Format("Processing webpage with id {0} failed for the {1} time", approval, nrOfTries), EventLogEntryType.Warning);
                        }

                        if (nrOfTries < GMGColorConfiguration.AppConfiguration.WebPageNrOfTries)
                        {
                            Utils.LogEvent(String.Format("Started processing webpage with id {0}", approval), EventLogEntryType.Information);
                            objProcessFile.ProcessGivenFile(messageBody, capturingImagePath);
                        }
                        else
                        {
                            objProcessFile.RecordApprovalError(approval, "Process WebPage Screenshot failed max nr of tries reached");
                        }

                        if (wThread.IsEnabledAmazonSQS)
                        {
                            // Delete the message from queue
                            try
                            {
                                AWSSimpleQueueService.DeleteMessage(wThread.MessageQueueName, wThread.ProcessMessage, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion);
                            }
                            catch
                            { } //TODO improve error handling here, plus handle the situation when DeleteMEssage fails.
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by ExecuteWebPageThread.Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by ExecuteWebPageThread:PTB_LibInit, Callas PDF Software. Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
            }
            finally
            {
                isWebPageProcessing = false;
            }
        }

        private int GetTimeLeft(int timeOut)
        {
            if (timeOut < 240)
            {
                return (timeOut / 4);
            }
            else
            {
                return 120;
            }
        }

        private void VideoTranscoderSetup()
        {
            try
            {
                string httpDomain = GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + GMGColorConfiguration.AppConfiguration.DomainUrl + "/AppService/SNSVideoTranscodingNotificationEvent";
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Transcoding jobs Notifications http endpoind: {0}", httpDomain), EventLogEntryType.Information);

                string topicARN = AWSSimpleNotificationService.CreateHttpSubscriber(GMGColorConfiguration.AppConfiguration.AWSAccessKey
                                                                                    , GMGColorConfiguration.AppConfiguration.AWSSecretKey
                                                                                    , GMGColorConfiguration.AppConfiguration.VideoTranscoderAWSRegion
                                                                                    , GMGColorConfiguration.AppConfiguration.AWSTranscodedVideoTopicName
                                                                                    , httpDomain
                                                                                    , GMGColorConfiguration.AppConfiguration.ServerProtocol);

                if (topicARN != null)
                {
                    Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Create Topic and Subscribe , Topic ARN :{0}", topicARN), EventLogEntryType.Information);
                }
                else
                {
                    Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Create Topic and Subscribe failed"), EventLogEntryType.Error);
                }

                videoTranscoderPipelineID = AWSElasticTranscoderService.CreatePipeline(GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                         GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                         GMGColorConfiguration.AppConfiguration.VideoTranscoderAWSRegion,
                                                                         topicARN,
                                                                         GMGColorConfiguration.AppConfiguration.AWSETSRoleARN,
                                                                         GMGColorConfiguration.AppConfiguration.VideoPipelineName,
                                                                         GMGColorConfiguration.AppConfiguration.DataFolderName(GMGColorConfiguration.AppConfiguration.AWSRegion));

                if (videoTranscoderPipelineID != null)
                {
                    Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Create Pipeline , Pipeline ID :{0}", videoTranscoderPipelineID), EventLogEntryType.Information);
                }
                else
                {
                    Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Create Pipeline failed"), EventLogEntryType.Error);
                }

                videoTranscoderPresetID = AWSElasticTranscoderService.CreateCustomPreset(GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                                            GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                                            GMGColorConfiguration.AppConfiguration.VideoTranscoderAWSRegion,
                                                                                            GMGColorConfiguration.AppConfiguration.VideoTranscoderPresetName
                                                                                            );

                if (videoTranscoderPresetID != null)
                {
                    Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Create Custom Preset , Preset ID :{0}", videoTranscoderPresetID), EventLogEntryType.Information);
                }
                else
                {
                    Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Create Custom Preset failed"), EventLogEntryType.Error);
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Video Transcoder Setup. Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
            }
        }

        #endregion
    }
}
