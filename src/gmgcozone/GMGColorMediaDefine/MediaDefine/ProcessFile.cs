﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Xml;
using System.Xml.XPath;
using GMG.CoZone.Common;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColor.AWS;
using GMGColor.MediaProcessor;
using GMGColorDAL;
using GMGColorDAL.Common;
using GMGColorMediaDefine.Common;
using TET_dotnet;
using System.Globalization;
using BrowshotScreenshotService;
using System.Runtime.ExceptionServices;
using System.Drawing.Drawing2D;
using GMGColorBusinessLogic;
using Microsoft.Practices.Unity;
using System.Net.Http;
using GMG.CoZone.Common.Module.Enums;
using GMGColorMediaDefine.Interfaces;
using GMG.CoZone.Common.Util;
using GMG.CoZone.Common.Interfaces;
using System.Threading.Tasks;

namespace GMGColorMediaDefine.MediaDefine
{
    public class ProcessFile
    {
        private DbContextBL context;
        private IUnityContainer _unityContainer = UnityConfig.GetContainerInstance();

        private static List<CleanupObject> _cleanupObjectsQueue = new List<CleanupObject>();
        private static object _cleanupObjectsLock = new object();

        private static object _stopProcessForHighResQueueLock = new object();

        #region Constructors
        public ProcessFile(DbContextBL context)
        {
            this.context = context;
        }
        #endregion

        #region Constants

        private static int ApprovalPageDPIFactor = 18;

        #endregion

        #region Methods

        #region Approval

        [HandleProcessCorruptedStateExceptions]
        public bool ProcessGivenFile(string message, string capturingImagePath = "")
        {
            try
            {
                LogEvent("GMGCoZone Media Define Service: ProcessGivenFile message data: " + message,
                                        EventLogEntryType.Information);
                CustomDictionary<string, string> dicPara = GetMessageParameters(message);

                GMGColorCommon.JobType jobType = GMGColorCommon.JobType.ApprovalJob;
                try
                {
                    jobType = (GMGColorCommon.JobType)Enum.Parse(typeof(GMGColorCommon.JobType),
                                                                  dicPara[Constants.JobMessageType] ??
                                                                  string.Empty);
                }
                catch (Exception ex)
                {
                    LogEvent(Constants.JobMessageType + " attribute is not present on message" + "Exception:" + ex,
                                         EventLogEntryType.Warning);
                    return true;
                }

                switch (jobType)
                {
                    case GMGColorCommon.JobType.ApprovalJob:
                        {
                            int ApprovalID = int.Parse(dicPara["ApprovalID"].ToString().Trim());
                            string quality = dicPara.ContainsKey("Compression") ? dicPara["Compression"].ToString().Trim() : "100";

                            float resolution = 300;
                            float.TryParse(dicPara["Resolution"].ToString().Trim(), out resolution);

                            GMGColorDAL.Approval objApproval = DALUtils.GetObject<GMGColorDAL.Approval>(ApprovalID, context);

                            if (objApproval == null)
                            {
                                LogEvent(String.Format("GMGCoZone Media Define Service: Aproval with id {0} not found in database, message is deleted from queue.", ApprovalID), EventLogEntryType.Error);
                                return true;
                            }

                            if (objApproval.StartProcessingDate == null || (DateTime.UtcNow - objApproval.StartProcessingDate.Value).TotalHours > GMGColorConfiguration.AppConfiguration.ApprovalsProcessingTimeoutInHrs)
                            {
                                if(objApproval.StartProcessingDate != null && objApproval.ApprovalPages.Count > 0 && objApproval.ApprovalPages.Min(t => t.Progress) == 100)
                                {
                                    // if the job was processed at an early time and was successfully completed then it should only be deleted from the queue
                                    return true;
                                }
                                else
                                {
                                    //-------> Temp to check exception behaviour
                                    bool retryStartProcessingDate = false;
                                    for (int i = 0; i < 3; i++)
                                    {
                                        if (!retryStartProcessingDate)
                                        {
                                            try
                                            {
                                                objApproval.StartProcessingDate = DateTime.UtcNow;
                                                context.SaveChanges();
                                                retryStartProcessingDate = true;
                                                break;
                                            }
                                            catch (Exception ex)
                                            {
                                                RecordApprovalError(objApproval.ID, ex.StackTrace.ToString());
                                                Thread.Sleep(1000);
                                                LogEvent(string.Format("ApprovalID:" + objApproval.ID + " An error occured in the StartProcessingDate, Exception: {0}, StackTrace: {1}, InnerExecption: {2}", ex.Message, ex.StackTrace.ToString(), ex.InnerException), EventLogEntryType.Error);
                                            }
                                        }
                                    }
                                }
                            }
                            else 
                            {
                                if (objApproval.ApprovalPages.Count > 0 && objApproval.ApprovalPages.Min(t => t.Progress) == 100)
                                {
                                    // if the job was processed at an early time and was successfully completed then it should only be deleted from the queue
                                    return true;
                                }

                                //The job is being processed return and not delete the message from SQS, the actual processing thread should take care of that
                                return false;
                            }

                            string domain = objApproval.Job1.Account1.IsCustomDomainActive ? objApproval.Job1.Account1.CustomDomain : objApproval.Job1.Account1.Domain;
                            string region = objApproval.Job1.Account1.Region;

                            ApprovalJobFolder approvalJobfolder = new ApprovalJobFolder(objApproval.Guid,
                                                                                        objApproval.FileName,                                                                                        
                                                                                        GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ? 
                                                                                                            GMGColorConfiguration.AppConfiguration.PathToProcessingFolder:
                                                                                                            GMGColorConfiguration.AppConfiguration.FileSystemPathToDataFolder,
                                                                                        GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket ?
                                                                                            GMGColorConfiguration.AppConfiguration.PathToDataFolder(region) :
                                                                                            GMGColorConfiguration.AppConfiguration.ServerProtocol + "://" + domain + "/" + GMGColorConfiguration.AppConfiguration.DataFolderName(region));


                            LogEvent("GMGCoZone Media Define Service: In ProcessGivenFile approvalJobfolder data for approval: " + ApprovalID  + " path is " + approvalJobfolder.ApprovalFileRelativePath,
                        EventLogEntryType.Information);

                            try
                            {
                                string ApprovalType = dicPara["ApprovalType"].ToString().Trim();

                                // Video File
                                string fileExtenssion = Path.GetExtension(approvalJobfolder.ApprovalFileRelativePath);
                                if (GMGColorCommon.IsVideoFile(fileExtenssion))
                                {
                                    if (!GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                                    {
                                        TranscodeVideoFileToGIF(ApprovalID, region, approvalJobfolder);
                                        GMGColorDAL.ApprovalPage objApprovalPage = new GMGColorDAL.ApprovalPage();
                                        objApprovalPage.Number = 1;
                                        objApprovalPage.Approval = ApprovalID;
                                        objApprovalPage.DPI = 0;
                                        objApprovalPage.PageSmallThumbnailHeight = 0;
                                        objApprovalPage.PageSmallThumbnailWidth = 0;
                                        objApprovalPage.PageLargeThumbnailHeight = 0;
                                        objApprovalPage.PageLargeThumbnailWidth = 0;
                                        objApprovalPage.Progress = 100;

                                        context.ApprovalPages.Add(objApprovalPage);
                                        context.SaveChanges();
                                        PublishStatus(new ApprovalPageProgressModel
                                        {
                                            ApprovalId = objApproval.ID,
                                            PageNumber = objApprovalPage.Number,
                                            Domain = objApproval.OwnerUser.Account1.Domain,
                                            ApprovalType = (ApprovalTypeEnum)objApproval.ApprovalType.Key,
                                            Progress = 100
                                        });
                                    }
                                }
                                else
                                {
                                    if (ApprovalType == GMGColorDAL.Approval.ApprovalTypeEnum.WebPage.ToString())
                                    {
                                        FileInfo sizeInfo = new FileInfo(capturingImagePath);
                                        long size = sizeInfo.Length;
                                        objApproval.Size = size;
                                        context.SaveChanges();

                                        LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessGivenFile Update Aproval size for approval:{0} and size is {1}", ApprovalID, size), EventLogEntryType.Information);
                                        LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessGivenFile FolderExists path for approval:{0} is {1} and capturingImagePath is {2}", ApprovalID, approvalJobfolder.ApprovalFolderRelativePath, capturingImagePath), EventLogEntryType.Information);

                                        // Create folder with Guid in case it does not exist
                                        GMGColorIO.FolderExists(approvalJobfolder.ApprovalFolderRelativePath, region, true);
                                        if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                                        {
                                            GMGColor.AWS.AWSSimpleStorageService.UploadFile(GMGColorConfiguration.AppConfiguration.DataFolderName(region), capturingImagePath, approvalJobfolder.ApprovalFileRemoteRelativePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                                            File.Delete(capturingImagePath);
                                        }
                                        else
                                        {
                                            File.Move(capturingImagePath, GMGColorConfiguration.AppConfiguration.PathToDataFolder(region) + approvalJobfolder.ApprovalFileRelativePath);
                                        }
                                    }

                                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                                    {
                                        DeleteAndCreateLocalFolder(approvalJobfolder.ApprovalProcessingFolderPath, Convert.ToBoolean(dicPara[Constants.IsJobHighRes]));

                                        using (WebClient client = new WebClient())
                                        {
                                            LogEvent(String.Format("GMGCoZone Media Define Service: DownloadFile: {0} # {1} # {2}", approvalJobfolder.DataStorageFolderPath, HttpUtility.UrlEncode(approvalJobfolder.ApprovalFileRemoteRelativePath), approvalJobfolder.ApprovalProcessingFilePath), EventLogEntryType.Information);
                                            if (!File.Exists(approvalJobfolder.ApprovalProcessingFilePath))
                                            {
                                                _unityContainer.Resolve<IRetryMechanism>().Run(
                                                    () => client.DownloadFile(approvalJobfolder.DataStorageFolderPath + HttpUtility.UrlEncode(approvalJobfolder.ApprovalFileRemoteRelativePath), approvalJobfolder.ApprovalProcessingFilePath),
                                                    TimeSpan.FromSeconds(2));
                                            }
                                        }
                                        // TODO - check if file exists and return if not
                                    }

                                    LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessGivenFile before ProcessApprovalFile {0}.", ApprovalID), EventLogEntryType.Information);
                                    ProcessApprovalFile(objApproval, approvalJobfolder, region, quality, Convert.ToBoolean(dicPara[Constants.IsJobHighRes]), dicPara[Constants.HighResSessiobGuid], resolution);

                                    LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessGivenFile after ProcessApprovalFile {0}.", ApprovalID), EventLogEntryType.Information);

                                    try
                                    {
                                        //Check if Approval was uploaded with upload engine and the profile it is attached has an output preset
                                        if (!String.IsNullOrEmpty(objApproval.FTPSourceFolder))
                                        {
                                            ApprovalBL.UploadApprovalToFTPIfNeeded(objApproval, context);
                                        }
                                    }
                                    catch (ExceptionBL ex)
                                    {
                                        string innerException = ex.InnerException != null ? ex.InnerException.StackTrace.ToString() : String.Empty;
                                        LogEvent(string.Format("GMGCoZone Media Define Service: Exception thrown by ProcessFile ProcessGivenFile method when trying to upload approval to FTP. Exception: {0}, StackTrace: {1}, InnerException: {2}, ActiveConfiguration: {3}", ex.Message, ex.StackTrace.ToString(), innerException, GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                RecordApprovalError(objApproval.ID, ex.StackTrace.ToString());
                                string innerException = ex.InnerException != null ? ex.InnerException.StackTrace.ToString() : String.Empty;
                                LogEvent(string.Format("GMGCoZone Media Define Service: Exception thrown by ProcessFile ProcessGivenFile method. Exception: {0}, StackTrace: {1}, InnerException: {2}, ActiveConfiguration: {3}, ApprovalID: {4}", ex.Message, ex.StackTrace.ToString(), innerException, GMGColorConfiguration.AppConfiguration.ActiveConfiguration, ApprovalID), EventLogEntryType.Error);
                                RecordApprovalError(ApprovalID, ex.ToString());
                            }
                        }
                        break;
                    case GMGColorCommon.JobType.DeliverJob:
                        {
                            int deliverId = 0;
                            Int32.TryParse((dicPara[Constants.DeliverQueueJobId] ?? string.Empty).Trim(), out deliverId);

                            GMGColorDAL.DeliverJob objDeliverJob = DALUtils.GetObject<GMGColorDAL.DeliverJob>(deliverId, context);

                            if (objDeliverJob == null)
                            {
                                LogEvent(String.Format("GMGCoZone Media Define Service: Deliver with id {0} not found in database, message is deleted from queue.", deliverId), EventLogEntryType.Error);
                                return true;
                            }

                            string deliverFolderRelativePath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + objDeliverJob.Guid + "/";
                            string deliverFileRelativePath = deliverFolderRelativePath + objDeliverJob.FileName;
                            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(objDeliverJob.Job1.Account1.Region);

                            string processingFilePath = string.Empty;

                            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                            {
                                string processingFolderPath =
                                GMGColorConfiguration.AppConfiguration.PathToProcessingFolder + objDeliverJob.Guid;
                                DeleteAndCreateLocalFolder(processingFolderPath);

                                processingFilePath = processingFolderPath + "/" + objDeliverJob.FileName;
                                using (WebClient client = new WebClient())
                                {
                                    _unityContainer.Resolve<IRetryMechanism>().Run(
                                        () => client.DownloadFile(pathtoDataFolder + HttpUtility.UrlEncode(deliverFileRelativePath), processingFilePath), 
                                        TimeSpan.FromSeconds(2));
                                }
                            }
                            else
                            {
                                processingFilePath = pathtoDataFolder + deliverFileRelativePath;
                            }
                            LogEvent(String.Format("GMGCoZone Media Define Service: Processing Aproval {0}.", deliverId), EventLogEntryType.SuccessAudit);
                            GenerateDeliverPreview(objDeliverJob, processingFilePath);
                        }
						break;
                }
            }
            catch (Exception ex)
            {
                string innerException = ex.InnerException != null ? ex.InnerException.StackTrace.ToString() : String.Empty;
                LogEvent(string.Format("GMGCoZone Media Define Service: Exception thrown by ProcessFile ProcessGivenFile method. Exception: {0}, StackTrace: {1}, InnerException: {2}, ActiveConfiguration: {3}", ex.Message, ex.StackTrace.ToString(), innerException, GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                return false;
            }
            return true;
        }

        public void TranscodeVideoFile(string message, string pipelineID, string presetID)
        {
            try
            {
                LogEvent(String.Format("GMGCoZone Media Define Service: Started Transcoding Video File"), EventLogEntryType.Information);

                CustomDictionary<string, string> dicPara = GetMessageParameters(message);

                int ApprovalID = int.Parse(dicPara["ApprovalID"].Trim());

                GMGColorDAL.Approval objApproval = ApprovalBL.GetApprovalById(ApprovalID, context);

                if (objApproval.ApprovalPages.Count > 0)
                {
                    ApprovalPage objApprovalPage = objApproval.ApprovalPages.SingleOrDefault();
                    objApprovalPage.Progress = 10;
                }
                else 
                {
                    var objApprovalPage = new ApprovalPage
                    {
                        Number = 1, 
                        Approval = ApprovalID,
                        DPI = 0,
                        PageSmallThumbnailHeight = 0,
                        PageSmallThumbnailWidth = 0,
                        PageLargeThumbnailHeight = 0,
                        PageLargeThumbnailWidth = 0,
                        Progress = 10
                    };
                    objApproval.ApprovalPages.Add(objApprovalPage);

                    context.ApprovalPages.Add(objApprovalPage);
                    context.SaveChanges();
                }

                var approvalVideoProgress = new ApprovalPageProgressModel()
                {
                    ApprovalId = objApproval.ID,
                    PageNumber = 1,
                    Domain = objApproval.OwnerUser.Account1.Domain,
                    ApprovalType = (ApprovalTypeEnum)objApproval.ApprovalType.Key,
                    Progress = 10
                };
                PublishStatus(approvalVideoProgress);

                string relativeFolderPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + objApproval.Guid + "/";
                string destinationFilePath = relativeFolderPath + objApproval.FileName;

                string videoRelativePath = relativeFolderPath + GMGColorConfiguration.CONVERTED + "/" + Path.ChangeExtension(objApproval.FileName, ".mp4");
                string thumbnailRelativePath = relativeFolderPath + GMGColorConfiguration.THUMBNAIL + "/";

                //Add delay as the file let upload to s3
                if (!GMGColorIO.FileExists(videoRelativePath, GMGColorConfiguration.AppConfiguration.AWSRegion))
                {
                    LogEvent(String.Format("GMGCoZone TranscodeVideoFile videoRelativePath fileExist not exist {0}", videoRelativePath), EventLogEntryType.Warning);
                    Thread.Sleep(2500);
                }
                TranscodeVideoFileToGIF(ApprovalID, GMGColorConfiguration.AppConfiguration.AWSRegion, null);
                string jobID = GMGColor.AWS.AWSElasticTranscoderService.TranscodeVideo(GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey,
                                                                                       GMGColorConfiguration.AppConfiguration.AWSRegion, destinationFilePath, videoRelativePath, thumbnailRelativePath,
                                                                                       pipelineID, presetID);

                if (jobID != null)
                {
                    LogEvent(String.Format("GMGCoZone Media Define Service: TranscodeVideo job , Job ID :{0}", jobID), EventLogEntryType.Information);
                }
                else
                {
                    LogEvent(String.Format("GMGCoZone Media Define Service:Create TranscodeVideo job failed"), EventLogEntryType.Error);
                }

                objApproval.TranscodingJobId = jobID;
                context.SaveChanges();

                //Check if Approval was uploaded with upload engine and the profile it is attached has an output preset
                if (!String.IsNullOrEmpty(objApproval.FTPSourceFolder))
                {
                    ApprovalBL.UploadApprovalToFTPIfNeeded(objApproval, context);
                }

            }
            catch (Exception ex)
            {
                LogEvent( "ProcessFile.TranscodeVideoFile. Transcode video method failed " + ex.Message, EventLogEntryType.Error);
            }
        }

        public void TranscodeVideoFileToGIF(int ApprovalID, string region, ApprovalJobFolder approvalJobfolder)
        {
            try
            {

                LogEvent(String.Format("GMGCoZone Media Define Service: Started TranscodeVideoFileToGIF Video File"), EventLogEntryType.Information);
                GMGColorDAL.Approval objApproval = ApprovalBL.GetApprovalById(ApprovalID, context);

                LogEvent(String.Format("TranscodeVideoFileToGIF after PublishStatus"), EventLogEntryType.Information);

                string relativeFolderPath = "", videoRelativePath = "", thumbnailRelativePath = "", awsDataFolderPath = "", ffmpegPath = "";
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    Thread.Sleep(1000);
                    awsDataFolderPath = GMGColorCommon.GetDataFolderHttpPath(region, objApproval.OwnerUser.Account1.Domain) + "approvals" + "/" + objApproval.Guid;
                    videoRelativePath = awsDataFolderPath + "/" + objApproval.FileName;
                    thumbnailRelativePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content\\VideoThumbnailFiles");
                    LogEvent(String.Format("TranscodeVideoFileToGIF dirPath: " + thumbnailRelativePath), EventLogEntryType.Information);

                    if (!Directory.Exists(thumbnailRelativePath))
                    {
                        Directory.CreateDirectory(thumbnailRelativePath);
                    }
                    thumbnailRelativePath += "/" + GMGColorConfiguration.VIDEOTHUMBNAIL;
                    ffmpegPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Tools\\ffmpeg\\ffmpeg.exe");
                    LogEvent(String.Format("TranscodeVideoFileToGIF after dirPath: " + thumbnailRelativePath), EventLogEntryType.Information);
                }
                else
                {
                    relativeFolderPath = GMGColorConfiguration.AppConfiguration.ApprovalFolderRelativePath + "/" + objApproval.Guid + "/";
                    videoRelativePath = approvalJobfolder.ApprovalProcessingFilePath;
                    thumbnailRelativePath = approvalJobfolder.ApprovalProcessingFolderPath + "/Thumbnail";
                    Directory.CreateDirectory(thumbnailRelativePath);
                    thumbnailRelativePath += "/" + GMGColorConfiguration.VIDEOTHUMBNAIL;
                    ffmpegPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", "").Replace("\\Debug", ""), "Tools\\ffmpeg\\ffmpeg.exe");
                }

                LogEvent(String.Format("TranscodeVideoFileToGIF relativeFolderPath file path: " + relativeFolderPath), EventLogEntryType.Information);
                LogEvent(String.Format("TranscodeVideoFileToGIF videoRelativePath file path: " + videoRelativePath), EventLogEntryType.Information);

                string ffmpgArgument = "-ss 1 -t 2.5 -i \"" + videoRelativePath + "\" -filter_complex \"[0:v] fps=12,scale=250:-1,split [a][b];[a] palettegen [p];[b][p] paletteuse\" \"" + thumbnailRelativePath + "\"";

                LogEvent(String.Format("TranscodeVideoFileToGIF ffmpgArgument path: " + ffmpgArgument), EventLogEntryType.Information);

                using (var process = new Process())
                {
                    var startInfo = process.StartInfo;
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                    // Add ffmpeg.exe file in the applicataion folder
                    startInfo.FileName = ffmpegPath;

                    //ffmpeg arguments callings to grab the JPEG file from video file
                    startInfo.Arguments = ffmpgArgument;
                    startInfo.UseShellExecute = false;
                    process.Start();
                    process.WaitForExit();
                    process.Close();
                }

                LogEvent(String.Format("TranscodeVideoFileToGIF after ffmpeg gif extraction"), EventLogEntryType.Information);

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    if (File.Exists(thumbnailRelativePath))
                    {
                        bool success = PushGIFToS3Bucket(thumbnailRelativePath, region, objApproval.Guid, GMGColorConfiguration.VIDEOTHUMBNAIL, context);
                        if (success)
                        {
                            DeleteTempGIFfile(thumbnailRelativePath);
                        }
                    }
                    else
                    {
                        LogEvent(String.Format("TranscodeVideoFileToGIF PushGIFToS3Bucket file not exist {0}", thumbnailRelativePath), EventLogEntryType.Warning);
                    }
                }

                LogEvent(String.Format("TranscodeVideoFileToGIF after pushing GIF to s3"), EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                LogEvent(string.Format("TranscodeVideoFileToGIF method failed, Exception: {0}, StackTrace: {1}, InnerExecption: {2}", ex.Message, ex.StackTrace.ToString(), ex.InnerException), EventLogEntryType.Error);
            }
        }

        public static bool PushGIFToS3Bucket(string LocaleFilePath, string accountRegion, string approvalGuid, string fileName, DbContextBL context)
        {
            bool success = false;
            string outputPath = "approvals" + "/" + approvalGuid + "/" + "Thumbnail/";
            bool folderExists = GMGColorIO.FolderExists(outputPath, accountRegion, true);
            using (FileStream stream = new FileStream(LocaleFilePath, FileMode.Open, FileAccess.Read))
            {
                success = GMGColorIO.UploadFile(stream, outputPath + fileName, accountRegion, false);
            }
            return success;
        }

        public static void DeleteTempGIFfile(string outputFilePath)
        {
            if (File.Exists(outputFilePath))
            {
                File.Delete(outputFilePath);
            }
        }

        public void RecordApprovalError(int approvalID, string errorMessage)
        {
            GMGColorDAL.Approval objApproval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalID, context);

            objApproval.IsError = true;
            objApproval.ErrorMessage = errorMessage.Substring(0, Math.Min(errorMessage.Length, 512));

            context.SaveChanges();
        }
        
        #endregion

        #region Deliver 
        [HandleProcessCorruptedStateExceptions]
        public void GenerateDeliverPreview(GMGColorDAL.DeliverJob deliverJob, string processingFilePath)
        {
            //TODO Remove creating "1" folder
            string deliverFolderRelativePath = GMGColorConfiguration.AppConfiguration.DeliverFolderRelativePath + "/" + deliverJob.Guid + "/";
            string deliverProcessingFolderPath = Path.GetDirectoryName(processingFilePath) + "\\" + "1" + "\\";
            Directory.CreateDirectory(deliverProcessingFolderPath);

            RenderEngineInstance renderInstance = null;
            SoftProofRenderer softProofRenderer = null;
            try
            {
                if (String.IsNullOrEmpty(MediaProcessor.InputProfile))
                {
                    LogEvent("Input profile could not be found at specified path", EventLogEntryType.Error);
                    throw new Exception("RGB Profile not found!");
                }

                renderInstance = new RenderEngineInstance(true, 0);

                softProofRenderer = new SoftProofRenderer(renderInstance, processingFilePath);

                softProofRenderer.Setup(MediaProcessor.InputProfile, MediaProcessor.OutputProfie, GMGColorConfiguration.AppConfiguration.MaxNrOfChannels, true);

                if (!File.Exists(deliverProcessingFolderPath + @"\Thumb-" + GMGColorDAL.Approval.ProcessedThumbnailDimension + "px.jpg"))
                {
                    softProofRenderer.RenderPage(deliverProcessingFolderPath + @"\Thumb-" + GMGColorDAL.Approval.ProcessedThumbnailDimension + "px.jpg", "jpg", GMGColorDAL.Approval.ProcessedThumbnailDimension, 0);
                }

                //s3Upload
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    string deliverJobFolderPath = Path.GetDirectoryName(processingFilePath) + "\\";
                    // Delete original file from processing folder. (No need to upload again)
                    File.Delete(processingFilePath);

                    bool uploadingCompleted =
                        GMGColor.AWS.AWSSimpleStorageService.UploadFolderByFile(
                            GMGColorConfiguration.AppConfiguration.DataFolderName(deliverJob.Job1.Account1.Region),
                            deliverJobFolderPath,
                            deliverFolderRelativePath,
                            GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                            GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    if (uploadingCompleted)
                    {
                        //delete deliver folder
                        try
                        {
                            var dir = new DirectoryInfo(deliverProcessingFolderPath);
                            dir.Delete(true);
                        }
                        catch (Exception ex)
                        {
                            LogEvent(string.Format("An error occured in the ProcessDeliverFile() method while While deleting deliver folder Exception is: {0}", ex.Message), EventLogEntryType.Error);
                        }
                    }
                }
            }
            catch(AccessViolationException ex)
            {
                LogEvent(string.Format("An access violation exception occured in the GenerateDeliverPreview, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                LogEvent( "An error occurred in the GenerateDeliverPreview : While adding deliver failed. " + ex.Message, EventLogEntryType.Error);
                
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket && Directory.Exists(deliverProcessingFolderPath))
                {
                    Directory.Delete(deliverProcessingFolderPath, true);
                }
                throw;
            }
            finally
            {
                try
                {
                    softProofRenderer.Dispose();
                    renderInstance.Dispose();
                }
                catch (Exception ex)
                {
                    LogEvent(string.Format("DeliverGuid:" + deliverJob.Guid + " An error occured in the GenerateDeliverPreview() method while While disposing AOI objects in finally statement. Exception: {0} StackTrace {1}", ex.Message, ex.StackTrace.ToString()), EventLogEntryType.Error);
                }
            }
        }

        #endregion

        #region Approval Job
        [HandleProcessCorruptedStateExceptions]
        private void ProcessApprovalFile(Approval objApproval, ApprovalJobFolder approvalJobFolder, string region, string Compression, bool isJobHighRes, string sessionGuid, float resolution = 300)
        {
            short quality;

            if (!short.TryParse(Compression, out quality))
            {
                quality = 100;
            }

            RenderEngineInstance renderInstance = null;
            SoftProofRenderer softProofRenderer = null;
            RenderEngineTileSettings settings = null;
            try
            {
                if (isJobHighRes)
                {
                    var folderPath = approvalJobFolder.ApprovalProcessingFolderPath;

                    //Create file for locking 
                    using (File.Create(approvalJobFolder.ApprovalProcessingFolderPath + "\\UHR.txt"));
                }

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} after isJobHighRes", objApproval.ID), EventLogEntryType.Information);

                var gmgConfig = _unityContainer.Resolve<IGMGConfiguration>();
                var cachePath = approvalJobFolder.ApprovalCacheProcessingFolderPath;
                Task deletionTask = PrepareJobForHighResReprocesing(objApproval, isJobHighRes, gmgConfig, cachePath);

                if (deletionTask != null)
                {
                    deletionTask.Start();
                }

                #region Creating Tetml

                string inputFileExtension = "";
                // Writing XML
                try
                {
                    inputFileExtension = Path.GetExtension(approvalJobFolder.ApprovalProcessingFilePath).ToLower();
                    if (inputFileExtension == ".pdf")
                    {
                        this.CreateTetml(approvalJobFolder.ApprovalProcessingFilePath, approvalJobFolder.TextDescriptorFilePath);

                    }
                    else
                    {
                        this.CopyDefaultTetml(approvalJobFolder.TextDescriptorFilePath);
                    }
                }
                catch (System.Exception ex)
                {
                    LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalFile: Creating Tetml, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                    throw ex;
                }

                #endregion

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} after deletionTask", objApproval.ID), EventLogEntryType.Information);
                string inputProfile = MediaProcessor.InputProfile;

                //check if current approval needs to be rendered using a custom profile
                bool useCustomProfile = false;
                bool isCstmProfileRGB = false;
                int cstmProfileId = 0;
                ApprovalEmbeddedProfile documentEmbeddedProfile = null;

                //check if custom profile was used for rendering
                if (objApproval.ApprovalCustomICCProfiles.Any() || useCustomProfile)
                {
                    inputProfile = Common.Utils.GetCustomProfilePath(objApproval, approvalJobFolder.ApprovalProcessingFolderPath, region, out isCstmProfileRGB, out cstmProfileId, context);
                    useCustomProfile = true;
                }

                int[] docFilesTypes = { 30, 31, 32, 33 };
                if (docFilesTypes.Contains(objApproval.FileType)) {
                    LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile objApproval.FileType {0}", objApproval.FileType), EventLogEntryType.Information);
                    Thread.Sleep(500);
                    if (!IsProfileExist(approvalJobFolder.TextDescriptorFilePath, region) && !useCustomProfile)
                    {
                        inputProfile = Common.Utils.GetCustomNonProfileFile(objApproval, approvalJobFolder.ApprovalProcessingFolderPath, region, out isCstmProfileRGB, out cstmProfileId, context);
                        useCustomProfile = true;
                    }
                }
                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} after ApprovalCustomICCProfiles with inputProfile {1}", objApproval.ID, inputProfile), EventLogEntryType.Information);

                renderInstance = new RenderEngineInstance(true, 0);
                softProofRenderer = new SoftProofRenderer(renderInstance, approvalJobFolder.ApprovalProcessingFilePath);
                softProofRenderer.Setup(inputProfile, MediaProcessor.OutputProfie, GMGColorConfiguration.AppConfiguration.MaxNrOfChannels, useCustomProfile);

                if (!useCustomProfile)
                {
                    //-------> Temp to check exception behaviour
                    bool retryEmbeddedProfile = false;
                    for (int i = 0; i < 3; i++)
                    {
                        if (!retryEmbeddedProfile)
                        {
                            try
                            {
                                //check for embedded profile
                                documentEmbeddedProfile = SaveEmbeddedProfileIfFound(objApproval.ID, approvalJobFolder, softProofRenderer, region);
                                retryEmbeddedProfile = true;
                                break;
                            }
                            catch (Exception ex)
                            {
                                Thread.Sleep(1000);
                                LogEvent(string.Format("ApprovalID:" + objApproval.ID + " An error occured in the SaveEmbeddedProfileIfFound, Exception: {0}, StackTrace: {1}, InnerExecption: {2}", ex.Message, ex.StackTrace.ToString(), ex.InnerException), EventLogEntryType.Error);
                            }
                        }
                    }
                }

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} after useCustomProfile", objApproval.ID), EventLogEntryType.Information);

                //create SoftProofing Session
                var sPSessionGuid = sessionGuid;
                if (string.IsNullOrEmpty(sPSessionGuid))
                {
                    //-------> Temp to check exception behaviour
                    bool retrySoftProofing = false;
                    for (int i = 0; i < 3; i++)
                    {
                        if (!retrySoftProofing)
                        {
                            try
                            {
                                sPSessionGuid = CreateSoftProofingSession(objApproval, documentEmbeddedProfile, useCustomProfile, isCstmProfileRGB, inputProfile, cstmProfileId);
                                retrySoftProofing = true;
                                break;
                            }
                            catch (Exception ex)
                            {
                                Thread.Sleep(1000);
                                LogEvent(string.Format("ApprovalID:" + objApproval.ID + " An error occured in the SaveEmbeddedProfileIfFound, Exception: {0}, StackTrace: {1}, InnerExecption: {2}", ex.Message, ex.StackTrace.ToString(), ex.InnerException), EventLogEntryType.Error);
                            }
                        }
                    }
                }

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} after sPSessionGuid", objApproval.ID), EventLogEntryType.Information);


                List<GMGColorDAL.ApprovalPage> pagesBO = new List<GMGColorDAL.ApprovalPage>();
                uint numberOfPages = softProofRenderer.GetPagesCount();
                bool retryApprovalPage = false;
                for (int i = 0; i < 3; i++) {
                    if (!retryApprovalPage)
                    {
                        try
                        {
                            //Create pages in Database
                            for (uint pageNumber = 0; pageNumber < numberOfPages; ++pageNumber)
                            {
                                int pageNr = (int)(pageNumber + 1);
                                GMGColorDAL.ApprovalPage objApprovalPage = (from ap in context.ApprovalPages
                                                                            where ap.Approval == objApproval.ID && ap.Number == pageNr
                                                                            select ap).Take(1).FirstOrDefault();

                                if (objApprovalPage == null)
                                {
                                    objApprovalPage = new GMGColorDAL.ApprovalPage();
                                    objApprovalPage.Number = (int)pageNumber + 1;
                                    objApprovalPage.Approval = objApproval.ID;
                                    objApprovalPage.PageSmallThumbnailHeight = 0;
                                    objApprovalPage.PageSmallThumbnailWidth = 0;
                                    objApprovalPage.PageLargeThumbnailHeight = 0;
                                    objApprovalPage.PageLargeThumbnailWidth = 0;
                                    context.ApprovalPages.Add(objApprovalPage);
                                }
                                else
                                {
                                    objApprovalPage.Progress = 5;
                                }
                                pagesBO.Add(objApprovalPage);
                            }

                            context.SaveChanges();
                            retryApprovalPage = true;
                            break;
                        }
                        catch (System.Exception ex)
                        {
                            RecordApprovalError(objApproval.ID, ex.StackTrace.ToString());
                            Thread.Sleep(1000 * (i + 1));
                            LogEvent(string.Format("ApprovalID:" + objApproval.ID + " An error occured in the InsertApprovalPage: Inserting to ApprovalPage table, {0}, ActiveConfiguration: {1}", GetInsertApprovalPageExceptionTree(ex), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                        }
                    }
                }

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} is ApprovalPage created {1}", objApproval.ID, retryApprovalPage), EventLogEntryType.Information);
                
                // determine the dpi for image files (jpeg, tiff, etc) and set it as output dpi for render engine in case of files with low dpi
                // this is done because files with low dpi (< 300) are resampled to 300 dpi (which is not necessary) and results in a very large image
                FileType.FileExtention fileType;
                Enum.TryParse(objApproval.FileType1.Extention.ToUpper(), out fileType);
                if (fileType != FileType.FileExtention.PDF && fileType != FileType.FileExtention.EPS && fileType != FileType.FileExtention.PPTX && fileType != FileType.FileExtention.PPT && fileType != FileType.FileExtention.DOC && fileType != FileType.FileExtention.DOCX)
                {
                    resolution = GetImageResolution(approvalJobFolder.ApprovalProcessingFilePath, resolution);
                }

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalFile for Approval {0} after GetImageResolution", objApproval.ID), EventLogEntryType.Information);

                settings = new RenderEngineTileSettings(256, 256, 20, isJobHighRes ? (uint)gmgConfig.HighResImageSize : (uint)gmgConfig.DefaultImageSize, resolution, quality, 1200);

                for (uint pageNumber = 0; pageNumber < numberOfPages; ++pageNumber)
                {
                    if (!isJobHighRes)
                    {
                        if (File.Exists(approvalJobFolder.ApprovalProcessingFolderPath + "UHR.txt"))
                        {
                            return;
                        }
                    }
                    ProcessApprovalPage(objApproval, approvalJobFolder, softProofRenderer, pageNumber, pagesBO, sPSessionGuid, region, settings, deletionTask, isJobHighRes);
                }

                ApprovalBL.SetHighResSessionInProgress(context, objApproval.ID, false);

                try
                {
                    #region Delete Temp Files

                    if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                    {
                        AddObjectTocleanup(new CleanupObject(approvalJobFolder.ApprovalProcessingFolderPath));
                    }
                    else
                    {
                        AddObjectTocleanup(new CleanupObject(approvalJobFolder.TextDescriptorFilePath, true));
                    }

                    #endregion
                }
                catch (Exception ex)
                {
                    LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalFile: Delete temop files, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                }

            }
            catch (Exception ex)
            {
                RecordApprovalError(objApproval.ID, ex.StackTrace.ToString());

                LogEvent("ApprovalGuid:" + objApproval.Guid + " ProcessFile.ProcessApprovalFile : While adding approval file failed. " + ex.StackTrace.ToString(), EventLogEntryType.Error);
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket && Directory.Exists(approvalJobFolder.ApprovalProcessingFolderPath))
                {
                    AddObjectTocleanup(new CleanupObject(approvalJobFolder.ApprovalProcessingFolderPath));
                }
                throw;
            }
            finally
            {
                // Release memory
                try
                {
                    // encapsulate in a try catch statement to avoid ending up with the job in error state after the file processing is completed and one of the 
                    // dispose calls fails in AOI.
                    // softProofRenderer.Dispose randomly throws "Attempted to read or write protected memory" (this must be fixed on AOI side)

                    if (settings != null)
                    {
                        settings.Dispose();
                    }

                    if (softProofRenderer != null)
                    {
                        softProofRenderer.Dispose();
                    }

                    if (renderInstance != null)
                    {
                        renderInstance.Dispose();
                    }
                }
                catch(Exception ex)
                {
                    LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalPage() method while While disposing AOI objects in finally statement. Exception: {0}", ex.StackTrace.ToString()), EventLogEntryType.Error);
                }

            }
        }

        private Task PrepareJobForHighResReprocesing(Approval objApproval, bool isJobHighRes, IGMGConfiguration gmgConfig, string cachePath)
        {
            var fileManager = _unityContainer.Resolve<IFileManager>();
            var folderContent = fileManager.GetFoldersContent(objApproval.Guid, objApproval.User1.Account1.Region);
            var pageNo = objApproval.ApprovalPages.Count;

            if (isJobHighRes)
            {
                var profile = folderContent.Where(p => p.Contains("icc")).FirstOrDefault();
                if (!string.IsNullOrEmpty(profile))
                {
                    folderContent.Remove(profile);
                    fileManager.DeleteFile(profile, objApproval.User1.Account1.Region);
                }

                var descriptorXml = folderContent.Where(p => p.Contains("TextDescriptor.xml")).FirstOrDefault();
                if (!string.IsNullOrEmpty(descriptorXml))
                {
                    fileManager.DeleteFile(descriptorXml, objApproval.User1.Account1.Region);
                    folderContent.Remove(descriptorXml);
                }

                folderContent.Remove(folderContent.Find(item => item.Contains(objApproval.FileName)));

                var rawDataFolderList = new List<string>();

                for (int i = 1; i <= pageNo; i++)
                {
                    if(folderContent.Any(item => item.Contains(objApproval.Guid + "\\" + i)))
                    {
                        var pageFolderContent = fileManager.GetFoldersContent(objApproval.Guid + "\\" + i, objApproval.User1.Account1.Region);
                        var rawDataWithoutThumbnails = pageFolderContent.Where(item => item.Contains(objApproval.Guid + "\\" + i + "\\Thumb-"));
                        rawDataFolderList.AddRange(pageFolderContent.Except(rawDataWithoutThumbnails));
                    }
                }
                
                if (gmgConfig.IsEnabledS3Bucket && Directory.Exists(cachePath))
                {
                    Directory.Delete(cachePath, true);
                }

                var deletionTask = new Task(() =>
                {
                    fileManager.DeleteFolderContent(rawDataFolderList, objApproval.User1.Account1.Region);
                });

                return deletionTask;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Checks if the doccument has embedded profile and saves it to the specified path
        /// </summary>
        /// <param name="approvalId"></param>
        /// <param name="approvalJobFolder"></param>
        /// <param name="softProofRenderer"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        private ApprovalEmbeddedProfile SaveEmbeddedProfileIfFound(int approvalId, ApprovalJobFolder approvalJobFolder, SoftProofRenderer softProofRenderer, string region)
        {
            ApprovalEmbeddedProfile documentEmbeddedProfile = null;

            var docProfileName = new StringBuilder(255);

            if (softProofRenderer.SaveDocumentEmbeddedProfile(approvalJobFolder.ApprovalProcessingFolderPath,
                docProfileName))
            {
                string embeddedProfileName = docProfileName.ToString();
                //in case document has embedded profile upload it to S3
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    AWSSimpleStorageService.UploadFile(
                        GMGColorConfiguration.AppConfiguration.DataFolderName(region),
                        approvalJobFolder.EmbededDocumentProfileProocessPath(embeddedProfileName),
                        approvalJobFolder.EmbededDocumentProfileRelativePath(embeddedProfileName),
                        GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                        GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                }
                documentEmbeddedProfile = new ApprovalEmbeddedProfile
                {
                    Approval = approvalId,
                    Name = embeddedProfileName,
                    IsRGB = softProofRenderer.IsDocumentRGBColorSpace()
                };
                if (!documentEmbeddedProfile.IsRGB)
                {
                    SpotColorCieLab profilePaperWhite;
                    //get profile paper white lab
                    softProofRenderer.GetProfilePaperWhiteCIELab(out profilePaperWhite);
                    documentEmbeddedProfile.L = profilePaperWhite.Lab[0];
                    documentEmbeddedProfile.a = profilePaperWhite.Lab[1];
                    documentEmbeddedProfile.b = profilePaperWhite.Lab[2];

                    //check if the embedded profile is found through the existing list of profiles and copy print substrate
                    var existingSimProfilePrintSubstrate =
                        context.SimulationProfiles.Where(
                            t => t.FileName.ToLower() == embeddedProfileName.ToLower())
                            .Select(t => t.MediaCategory)
                            .FirstOrDefault();
                    //if found 
                    if (existingSimProfilePrintSubstrate > 0)
                    {
                        documentEmbeddedProfile.PrintSubstrate = existingSimProfilePrintSubstrate;
                    }
                }
                context.ApprovalEmbeddedProfiles.Add(documentEmbeddedProfile);
            }

            return documentEmbeddedProfile;
        }

        /// <summary>
        /// Create Default SoftProofing Session
        /// </summary>
        /// <param name="objApproval"></param>
        /// <param name="documentEmbeddedProfile"></param>
        /// <param name="useCustomProfile"></param>
        /// <param name="isCstmProfileRGB"></param>
        /// <param name="inputProfile"></param>
        /// <param name="cstmProfileId"></param>
        /// <returns></returns>
        private string CreateSoftProofingSession(Approval objApproval, ApprovalEmbeddedProfile documentEmbeddedProfile, bool useCustomProfile, bool isCstmProfileRGB, string inputProfile, int cstmProfileId)
        {
            string processingSessionGuid;

            //create default softproofing session where approval job details conditions are not taken into account
            ViewingCondition defVwCnd = null;
            bool alreadyExistingSession = false;
            if (documentEmbeddedProfile != null)
            {
                defVwCnd = new ViewingCondition {ApprovalEmbeddedProfile = documentEmbeddedProfile};
            }
            else if (cstmProfileId > 0)
            {
                defVwCnd = new ViewingCondition {CustomProfile = cstmProfileId};
            }
            else
            {
                defVwCnd = SoftProofingBL.GetJobViewingConditionBySimProfile(MediaDefine.DefaultSimulationProfileId,
                    context);
                if (defVwCnd == null)
                {
                    defVwCnd = new ViewingCondition {SimulationProfile = MediaDefine.DefaultSimulationProfileId};
                }
                else
                {
                    alreadyExistingSession = true;
                }
                
            }

            if (!alreadyExistingSession)
            {
                context.ViewingConditions.Add(defVwCnd);
            }

            var defaultSoftProofingSession = new SoftProofingSessionParam
            {
                Approval = objApproval.ID,
                OutputRGBProfileName =
                    useCustomProfile && isCstmProfileRGB
                        ? Path.GetFileName(inputProfile)
                        : (documentEmbeddedProfile != null && documentEmbeddedProfile.IsRGB 
                                                                ? documentEmbeddedProfile.Name 
                                                                : GMGColorConfiguration.AppConfiguration.OutputProfile),
                SessionGuid = Guid.NewGuid().ToString(),
                SimulatePaperTint = true,
                LastAccessedDate = DateTime.UtcNow,
                DefaultSession = true,
                ViewingCondition1 = defVwCnd
            };

            context.SoftProofingSessionParams.Add(defaultSoftProofingSession);

            processingSessionGuid = defaultSoftProofingSession.SessionGuid;

            //check viewing conditions
            var existingViewingCond = objApproval.Version > 1 ? SoftProofingBL.GetJobViewingCondition(objApproval.Job, context) : null;
            ViewingCondition jobVwCnd = null;
            //check if job has viewing conditions set
            if (existingViewingCond != null && (existingViewingCond.SimulationProfile.HasValue || existingViewingCond.PaperTint.HasValue))
            {
                //check if different vrom default viewing cond
                if (existingViewingCond.SimulationProfile != defVwCnd.SimulationProfile ||
                    existingViewingCond.PaperTint != defVwCnd.PaperTint)
                {
                    jobVwCnd = new ViewingCondition();

                    if (existingViewingCond.SimulationProfile == null)
                    {
                        jobVwCnd.CustomProfile = defVwCnd.CustomProfile;
                        jobVwCnd.ApprovalEmbeddedProfile = defVwCnd.ApprovalEmbeddedProfile;
                        jobVwCnd.SimulationProfile = defVwCnd.SimulationProfile;
                        jobVwCnd.PaperTint = existingViewingCond.PaperTint;
                    }
                    else
                    {
                        jobVwCnd.SimulationProfile = existingViewingCond.SimulationProfile;
                        jobVwCnd.PaperTint = existingViewingCond.PaperTint;
                    }

                    context.ViewingConditions.Add(jobVwCnd);

                    //create default soft proofing session
                    var sfSession = new SoftProofingSessionParam
                    {
                        Approval = objApproval.ID,
                        OutputRGBProfileName =
                            useCustomProfile && isCstmProfileRGB
                                ? Path.GetFileName(inputProfile)
                                : (documentEmbeddedProfile != null && documentEmbeddedProfile.IsRGB
                                                                ? documentEmbeddedProfile.Name
                                                                : GMGColorConfiguration.AppConfiguration.OutputProfile),
                        SessionGuid = Guid.NewGuid().ToString(),
                        SimulatePaperTint = true,
                        LastAccessedDate = DateTime.UtcNow,
                        ViewingCondition1 = jobVwCnd
                    };

                    context.SoftProofingSessionParams.Add(sfSession);

                    processingSessionGuid = sfSession.SessionGuid;
                }
            }

            return processingSessionGuid;
        }

        /// <summary>
        /// Process approval page
        /// </summary>
        /// <param name="objApproval"></param>
        /// <param name="approvalJobFolder"></param>
        /// <param name="softProofRenderer"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pagesBO"></param>
        /// <param name="progressValue"></param>
        /// <param name="sPSessionGuid"></param>
        /// <param name="region"></param>
        /// <param name="settings"></param>
        private void ProcessApprovalPage(Approval objApproval, ApprovalJobFolder approvalJobFolder, SoftProofRenderer softProofRenderer, uint pageNumber, IEnumerable<ApprovalPage> pagesBO, string sPSessionGuid, string region, RenderEngineTileSettings settings, Task deletionTask, bool isJobHighRes)
        {
            var approvalPageProgressObj = new ApprovalPageProgressModel()
            {
                ApprovalId = objApproval.ID,
                PageNumber = (int)pageNumber + 1,
                Domain = objApproval.OwnerUser.Account1.Domain,
                ApprovalType = (ApprovalTypeEnum)objApproval.ApprovalType.Key,
                Progress = 5
            };

            string pageFolderPath = approvalJobFolder.GetPageProcessingFolderPath(pageNumber + 1);
            Directory.CreateDirectory(pageFolderPath);
            ApprovalPage objApprovalPage = pagesBO.SingleOrDefault(t => t.Number == (pageNumber + 1));

            LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalPage for Approval {0} for folder path {1} and pageID {2}", objApproval.ID, pageFolderPath, objApprovalPage.ID), EventLogEntryType.Information);


            //Get page dimenssions and save them to db
            float width, height;
            softProofRenderer.GetPageDimenssions(pageNumber, out width, out height);

            approvalPageProgressObj.Progress = 40;
            PublishStatus(approvalPageProgressObj);

            int outputDPI = 0;
            //-------> Temp to check exception behaviour
            bool IsRenderRawData = false;
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    if (!IsRenderRawData)
                    {
                        // Generate tiles and full size preview
                        softProofRenderer.RenderRawData(settings, pageFolderPath, pageNumber, out outputDPI);
                        IsRenderRawData = true;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalPage: while RenderRawData, Exception: {0}, StackTrace: {1}, InnerException: {2}", ex.Message, ex.StackTrace.ToString(), ex.InnerException), EventLogEntryType.Error);
                }
            }
            objApprovalPage.DPI = outputDPI;
            objApprovalPage.OutputRenderHeight = (int)(height / ApprovalPageDPIFactor * outputDPI);
            objApprovalPage.OutputRenderWidth = (int)(width / ApprovalPageDPIFactor * outputDPI);

            FileType.FileExtention fileType;
            Enum.TryParse(objApproval.FileType1.Extention.ToUpper(), out fileType);

            switch (fileType)
            {
                case FileType.FileExtention.PDF:
                case FileType.FileExtention.EPS:
                case FileType.FileExtention.PSD:
                case FileType.FileExtention.DOC:
                case FileType.FileExtention.DOCX:
                case FileType.FileExtention.PPT:
                case FileType.FileExtention.PPTX:
                    {
                        objApprovalPage.OriginalImageHeight = (int)(height / ApprovalPageDPIFactor * 300);
                        objApprovalPage.OriginalImageWidth = (int)(width / ApprovalPageDPIFactor * 300);
                        break;
                    }
                default:
                    {
                        int imgWidth = 0, imgHeight = 0;
                        if (!softProofRenderer.GetImageSize(out imgWidth, out imgHeight))
                        {
                            // in case the renderer fails to read the image size read it directly from the file
                            Size imgSize = GetImageSize(approvalJobFolder.ApprovalProcessingFilePath);
                            imgHeight = imgSize.Height;
                            imgWidth = imgSize.Width;
                        }
                        objApprovalPage.OriginalImageHeight = imgHeight;
                        objApprovalPage.OriginalImageWidth = imgWidth;
                        break;
                    }
            }

            LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalPage for Approval {0} after objApprovalPage", objApproval.ID), EventLogEntryType.Information);


            ResizedImage smallThumbImage = new ResizedImage();
            ResizedImage processedThumbImage = new ResizedImage();

            #region Generate Thumbnails

            Image generatedPreview = Image.FromFile(approvalJobFolder.GetPageFullSizePreviewProcessingPath(pageNumber + 1));
            if (!File.Exists(approvalJobFolder.GetPageSmallPreviewProcessingPath(pageNumber + 1, GMGColorDAL.Approval.SmallThumbnailDimension)))
            {
                Size smallThumbnailSize = new Size();
                if (generatedPreview.Size.Width > generatedPreview.Size.Height)
                {
                    smallThumbnailSize.Width = GMGColorDAL.Approval.SmallThumbnailDimension_220px;
                    smallThumbnailSize.Height = (int)Math.Ceiling((float)smallThumbnailSize.Width * generatedPreview.Size.Height / generatedPreview.Size.Width);
                }
                else
                {
                    smallThumbnailSize.Height = GMGColorDAL.Approval.SmallThumbnailDimension_220px; ;
                    smallThumbnailSize.Width = (int)Math.Ceiling((float)smallThumbnailSize.Height * generatedPreview.Size.Width / generatedPreview.Size.Height);
                }

                Image thumbnail = ResizeImage(generatedPreview, smallThumbnailSize);
                smallThumbImage.Width = smallThumbnailSize.Width;
                smallThumbImage.Height = smallThumbnailSize.Height;

                thumbnail.Save(approvalJobFolder.GetPageSmallPreviewProcessingPath(pageNumber + 1, GMGColorDAL.Approval.SmallThumbnailDimension), System.Drawing.Imaging.ImageFormat.Jpeg);
                thumbnail.Dispose();
            }

            if (!File.Exists(approvalJobFolder.GetPageSmallPreviewProcessingPath(pageNumber + 1, GMGColorDAL.Approval.ProcessedThumbnailDimension)))
            {
                Size processedThumbnailSize = new Size();
                if (generatedPreview.Size.Width > generatedPreview.Size.Height)
                {
                    processedThumbnailSize.Width = GMGColorDAL.Approval.ProcessedThumbnailDimension;
                    processedThumbnailSize.Height = (int)Math.Ceiling((float)processedThumbnailSize.Width * generatedPreview.Size.Height / generatedPreview.Size.Width);
                }
                else
                {
                    processedThumbnailSize.Height = GMGColorDAL.Approval.ProcessedThumbnailDimension;
                    processedThumbnailSize.Width = (int)Math.Ceiling((float)processedThumbnailSize.Height * generatedPreview.Size.Width / generatedPreview.Size.Height);
                }

                Image thumbnail = ResizeImage(generatedPreview, processedThumbnailSize);
                processedThumbImage.Width = processedThumbnailSize.Width;
                processedThumbImage.Height = processedThumbnailSize.Height;

                thumbnail.Save(approvalJobFolder.GetPageSmallPreviewProcessingPath(pageNumber + 1, GMGColorDAL.Approval.ProcessedThumbnailDimension), System.Drawing.Imaging.ImageFormat.Jpeg);
                thumbnail.Dispose();
            }
            generatedPreview.Dispose();

            LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalPage for Approval {0} after Generate Thumbnails", objApproval.ID), EventLogEntryType.Information);


            #endregion

            approvalPageProgressObj.Progress = 75;
            PublishStatus(approvalPageProgressObj);

            LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalPage for Approval {0} after PublishStatus profress is 75", objApproval.ID), EventLogEntryType.Information);


            //save Channels Into DB
            for (int i = 0; i < softProofRenderer.GetDocumentChannelCount(); i++)
            {
                GMGColorDAL.ApprovalSeparationPlate plate = (from apsp in context.ApprovalSeparationPlates
                                                             where apsp.Page == objApprovalPage.ID && apsp.ChannelIndex == i
                                                             select apsp).FirstOrDefault();

                if (plate == null)
                {
                    plate = new GMGColorDAL.ApprovalSeparationPlate();
                    plate.Name = softProofRenderer.GetDocumentChannelName((uint)i);
                    plate.ChannelIndex = i;
                    objApprovalPage.ApprovalSeparationPlates.Add(plate);
                    context.ApprovalSeparationPlates.Add(plate);
                }
            }

            //Spot colors are only for cmyk profiles
            uint spotColorsCount = softProofRenderer.GetDocumentSpotColorsCount();

            //in case document has extra plates retrieve lab color and save to database
            if (spotColorsCount > 0)
            {
                for (uint i = 0; i < spotColorsCount; ++i)
                {
                    SpotColorCieLab spotColorLab;
                    softProofRenderer.GetSpotColorCIELab(i, out spotColorLab);

                    ApprovalSeparationPlate currentPlate = objApprovalPage.ApprovalSeparationPlates.FirstOrDefault(t => t.Name == spotColorLab.ChannelName);

                    LabColorMeasurement labColorMeasurement = null;
                    if (currentPlate != null)
                    {
                        labColorMeasurement = context.LabColorMeasurements.Where(l => l.ApprovalSeparationPlate == currentPlate.ID).FirstOrDefault();
                    }

                    if (labColorMeasurement != null)
                    {
                        continue;
                    }

                    currentPlate.IsSpotChannel = true;
                    currentPlate.RGBHex = new SoapHexBinary(spotColorLab.Rgb).ToString();

                    var extraPlatelabColor = new LabColorMeasurement
                    {
                        ApprovalSeparationPlate1 = currentPlate,
                        L = spotColorLab.Lab[0],
                        a = spotColorLab.Lab[1],
                        b = spotColorLab.Lab[2],
                        Step = 100
                    };

                    context.LabColorMeasurements.Add(extraPlatelabColor);

                    var extraPlatePaperWhite = new LabColorMeasurement
                    {
                        ApprovalSeparationPlate1 = currentPlate,
                        L = 100,
                        a = 0,
                        b = 0,
                        Step = 0
                    };

                    context.LabColorMeasurements.Add(extraPlatePaperWhite);
                }
            }

            try
            {
                #region Create Tetml Per Page

                CreateTextDescriptorsForPage(approvalJobFolder.TextDescriptorFilePath, ((int)pageNumber + 1));
                TextHighlightingBL.GenerateNewTextHighlightJSONFile(objApproval.ID, objApprovalPage.ID);

                #endregion
            }
            catch (Exception ex)
            {
                PublishStatus(approvalPageProgressObj, true);
                LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalPage: Create Tetml for pages, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                throw ex;
            }

            try
            {
                #region  Uploading

                // TEMPORARY FIX for reporting converted.jpg as still been in use when trying to upload it . wait 2 minutes
                FileInfo previewFile = new FileInfo(approvalJobFolder.GetPageFullSizePreviewProcessingPath(pageNumber + 1));
                if (previewFile.Exists)
                {
                    Stopwatch s1 = new Stopwatch();
                    s1.Start();
                    while (IsFileLocked(previewFile) && s1.ElapsedMilliseconds < 2 * 60 * 1000)
                    {
                        Thread.Sleep(100);
                    }
                }

                if (deletionTask != null && deletionTask.Status != TaskStatus.RanToCompletion)
                {
                    deletionTask.Wait();
                }

                approvalPageProgressObj.Progress = 90;
                PublishStatus(approvalPageProgressObj);

                if (!isJobHighRes && File.Exists(approvalJobFolder.ApprovalProcessingFolderPath + "UHR.txt"))
                {
                    return;
                }

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    AWSSimpleStorageService.UploadFolderByFileInParallel(GMGColorConfiguration.AppConfiguration.DataFolderName(region),
                                                                approvalJobFolder.GetPageProcessingFolderPath(pageNumber + 1),
                                                                approvalJobFolder.GetPageFolderReleativePath(pageNumber + 1),
                                                                GMGColorConfiguration.AppConfiguration.AWSAccessKey,
                                                                GMGColorConfiguration.AppConfiguration.AWSSecretKey
                                                                );
                }

                #endregion
            }
            catch (Exception ex)
            {
                PublishStatus(approvalPageProgressObj, true);
                LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalPage: Uploading files, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                throw ex;
            }

            try
            {
                #region Updating Database

                objApprovalPage.PageSmallThumbnailHeight = int.Parse(smallThumbImage.Height.ToString());
                objApprovalPage.PageSmallThumbnailWidth = int.Parse(smallThumbImage.Width.ToString());
                objApprovalPage.PageLargeThumbnailHeight = int.Parse(processedThumbImage.Height.ToString());
                objApprovalPage.PageLargeThumbnailWidth = int.Parse(processedThumbImage.Width.ToString());
                objApprovalPage.Progress = 100;
                // set to completed

                context.SaveChanges();

                approvalPageProgressObj.Progress = 100;
                PublishStatus(approvalPageProgressObj);

                LogEvent(String.Format("GMGCoZone Media Define Service: In ProcessApprovalPage for Approval {0} after PublishStatus profress is 100", objApproval.ID), EventLogEntryType.Information);

                #endregion
            }
            catch (Exception ex)
            {
                PublishStatus(approvalPageProgressObj, true);
                LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalPage: Updating database, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
                throw ex;
            }

            #region Move Raw Bands to cache

            try
            {
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    var cacheDirectory = Directory.GetParent(approvalJobFolder.GetPageBandCacheFolder(pageNumber + 1)).FullName;
                    if (Directory.Exists(cacheDirectory))
                    {
                        Directory.Delete(cacheDirectory, true);
                    }
                    Directory.CreateDirectory(cacheDirectory);
                    Directory.Move(approvalJobFolder.GetPageBandsFolderProcessingPath(pageNumber + 1),
                        approvalJobFolder.GetPageBandCacheFolder(pageNumber + 1));
                }
            }
            catch (Exception ex)
            {
                PublishStatus(approvalPageProgressObj, true);
                LogEvent(string.Format("ApprovalGuid:" + objApproval.Guid + " An error occured in the ProcessApprovalPage: Move Raw Bands to cache, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
            }

            #endregion

            PostMessageToTileServerQueue(sPSessionGuid, objApproval.Guid, objApprovalPage.ID, approvalPageProgressObj);
        }

        private void PostMessageToTileServerQueue(string sessionGuid, string approvalGuid, int approvalPageId, ApprovalPageProgressModel progress)
        {
            try
            {
                Dictionary<string, string> dictJobParameters = new Dictionary<string, string>();
                dictJobParameters.Add(Constants.PAGEID, approvalPageId.ToString());
                dictJobParameters.Add(Constants.SESSIONGUID, sessionGuid);
                GMGColorCommon.CreateFileProcessMessage(dictJobParameters, GMGColorCommon.MessageType.TileServerJob);
            }
            catch (Exception ex)
            {
                if (progress != null)
                {
                    PublishStatus(progress, true);
                }
                LogEvent(string.Format("ApprovalGuid:" + approvalGuid + " An error occured in the ProcessApprovalFile: Post queue message for tile server, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
            }
        }

        private void PublishStatus(ApprovalPageProgressModel approvalPageProgress, bool isError = false)
        {
            if (approvalPageProgress.PageNumber == 1)
            {
                var unityContainer = UnityConfig.GetContainerInstance();
                unityContainer.Resolve<IApprovalStatusMessageSender>().SendMessage(new ApprovalStatusMessageModel {
                    ApprovalId = approvalPageProgress.ApprovalId,
                    Progress = approvalPageProgress.Progress,
                    IsError = isError,
                    Domain = approvalPageProgress.Domain,
                    Context = context,
                    ApprovalType = approvalPageProgress.ApprovalType
                });
            }
        }

        #endregion     

        #region File Processing

        private void CreateTetml(string pdfFileName, string xmlFileName)
        {
            GMGColorMediaDefine.Common.Utils.LogEvent("ProcessFile.CreateTetml - pdfFileName: " + pdfFileName + "; xmlFileName: " + xmlFileName, EventLogEntryType.Information);

            /* Global option list. */
            string globaloptlist = "searchpath={{../data} {../../data}}";

            /* Document specific option list. */
            string basedocoptlist = " engines={noimage}";

            /* Page-specific option list. */
            /* Remove the tetml= option if you don't need font and geometry
               information */
            //string pageoptlist = "granularity=word tetml={glyphdetails={all}} skipengines={image} contentanalysis={shadowdetect=true}";

            string pageoptlist = "granularity=word tetml={glyphdetails={all}}";


            /* set this to true to generate TETML output in memory */
            bool inmemory = false;

            TET tet = null;

            try
            {
                String docoptlist;

                tet = new TET();
                string licFilePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\licensekeys.txt";
                tet.set_option("licensefile={" + licFilePath + "}");
                tet.set_option(globaloptlist);

                if (inmemory)
                {
                    /*
                     * This program fetches the TETML data encoded in UTF-8.
                     * Subsequently the data is converted to a VisualBasic String,
                     * which is encoded in UTF-16.
                     * While it is not strictly necessary in case of this program, it
                     * is more clean to instruct TET to put 'encoding="UTF-16"' into
                     * the XML header.
                     */
                    docoptlist = "tetml={encodingname=UTF-16} " + basedocoptlist;
                }
                else
                {
                    //docoptlist = "tetml={filename={" + xmlFileName + "} elements={nodocxmp}}"
                    //    + basedocoptlist;

                    docoptlist = "tetml={filename={" + xmlFileName + "} elements={annotations=false docinfo=true bookmarks=false destinations=false fields=false javascripts=false metadata=false options=true}}"
                           + basedocoptlist;
                }

                int doc = tet.open_document(pdfFileName, docoptlist);

                if (doc == -1)
                {
                    LogEvent("ProcessFile.CreateTetml - " + "Error " + tet.get_errnum() + " in " + tet.get_apiname() + "(): " + tet.get_errmsg(), EventLogEntryType.Error);
                    CopyDefaultTetml(xmlFileName);
                    return;
                }

                int n_pages = (int)tet.pcos_get_number(doc, "length:pages");

                /* Loop over pages in the document */
                for (int pageno = 1; pageno <= n_pages; ++pageno)
                {
                    tet.process_page(doc, pageno, pageoptlist);
                }

                /* This could be combined with the last page-related call. */
                tet.process_page(doc, 0, "tetml={trailer}");

                if (inmemory)
                {
                    /* Get the XML document as a byte array. */
                    byte[] tetml = tet.get_tetml(doc, "");

                    if (tetml == null)
                    {
                        LogEvent("ProcessFile.CreateTetml - Error : tetml: couldn't retrieve XML data", EventLogEntryType.Error);
                        CopyDefaultTetml(xmlFileName);
                        return;
                    }

                    /* Process the in-memory XML document to print out some
                     * information that is extracted with the sax_handler class.
                     */
                    XmlDocument xmldoc = new XmlDocument();
                    UTF8Encoding utf8_enc = new UTF8Encoding();
                    String stetml = utf8_enc.GetString(tetml);
                    xmldoc.LoadXml(stetml);

                    XmlNodeList nodeList;
                    XmlElement root = xmldoc.DocumentElement;

                    /* Create an XmlNamespaceManager for resolving namespaces. */
                    XmlNamespaceManager nsmgr =
                              new XmlNamespaceManager(xmldoc.NameTable);
                    nsmgr.AddNamespace("tet",
                              "http://www.pdflib.com/XML/TET3/TET-3.0");

                    nodeList = root.SelectNodes("//tet:Font", nsmgr);
                    IEnumerator ienum = nodeList.GetEnumerator();
                    while (ienum.MoveNext())
                    {
                        XmlNode font = (XmlNode)ienum.Current;
                        XmlAttributeCollection attrColl = font.Attributes;

                        XmlAttribute name_attr =
                              (XmlAttribute)attrColl.GetNamedItem("name");
                        XmlAttribute type_attr =
                              (XmlAttribute)attrColl.GetNamedItem("type");
                        Console.WriteLine("Font " + name_attr.Value + " "
                                      + type_attr.Value);
                    }
                    nodeList = root.SelectNodes("//tet:Word", nsmgr);
                }

                tet.close_document(doc);
            }
            catch (TETException e)
            {
                LogEvent("ProcessFile.CreateTetml " + "Error " + e.get_errnum() + " in " + e.get_apiname() + "(): " + e.get_errmsg(), EventLogEntryType.Error);
                CopyDefaultTetml(xmlFileName);
            }
            catch (Exception ex)
            {
                LogEvent(string.Format("ProcessFile CreateTetml. Exception: {0}, StackTrace: {1}", ex.Message, ex.StackTrace.ToString()), EventLogEntryType.Error);
                CopyDefaultTetml(xmlFileName);
            }
            finally
            {
                if (tet != null)
                {
                    tet.Dispose();
                }
            }
        }

        private void CopyDefaultTetml(string location)
        {
            if (!File.Exists(location))
            {
                string defaultTetPath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\TextDescriptor.xml";
                File.Copy(defaultTetPath, location);
            }
        }

        private void CreateTextDescriptorsForPage(string textDescriptorPath, int pageNumber)
        {
            try
            {
                string pageDescriptorPath = Path.GetDirectoryName(textDescriptorPath);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(textDescriptorPath);

                LogEvent("ProcessFile.CreateTextDescriptorsForPage - pageDescriptorPath: pageDescriptorPath", EventLogEntryType.Information);

                if (xmlDoc.ChildNodes[2].HasChildNodes)
                {
                    XmlNode resourcesNode = GetResourcesNode(xmlDoc);

                    StreamReader strm = File.OpenText(textDescriptorPath);
                    XPathDocument doc = new XPathDocument(strm);
                    XPathNavigator nav = doc.CreateNavigator();

                    nav.MoveToRoot();
                    nav.MoveToFirstChild();
                    nav.MoveToNext(); // Tet
                    nav.MoveToFirstChild();
                    nav.MoveToNext(); // Document
                    nav.MoveToFirstChild();
                    nav.MoveToNext();
                    nav.MoveToNext(); // Pages                

                    XPathNodeIterator xi = nav.SelectChildren("Page", nav.NamespaceURI);

                    if(xi.Count == 0)
                    {
                        nav.MoveToNext();
                        xi = nav.SelectChildren("Page", nav.NamespaceURI);
                        if(xi.Count == 0)
                        {
                            XPathNavigator navigator = doc.CreateNavigator();
                            XPathExpression query = navigator.Compile("/x:TET/x:Document/x:Pages");
                            XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
                            manager.AddNamespace("x", "http://www.pdflib.com/XML/TET5/TET-5.0");
                            query.SetContext(manager);
                            xi = navigator.Select(query);
                        }
                    }

                    if (xi.Count > 0)
                    {
                        nav.MoveToFirstChild(); // first page
                        bool found = false;
                        do
                        {
                            try
                            {
                                if (found)
                                {
                                    break;
                                }
                                int count = 1;
                                int savingPage = 1;

                                foreach (XPathNavigator val in xi)
                                {
                                    if (savingPage == pageNumber)
                                    {
                                        // Generating Page(i) TextDescriptors
                                        //GMGColorIO.WriteProgressFile(String.Format(Resources.Resources.lblProgressTextGeneratingTextDescriptors + " {0}%", ((count * 100) / numberOfPages).ToString()), approvalFolderRelativePath + "Progress.txt");
                                        count++;

                                        if (xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2] == null)
                                        {
                                            xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[1].RemoveAll();
                                        }
                                        else
                                        {
                                            xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2].RemoveAll();
                                        }

                                        XmlDocument xmlSavingDoc = xmlDoc;

                                        XmlTextReader xmlReader = new XmlTextReader(new StringReader(val.OuterXml));
                                        XmlDocument temp = new XmlDocument();
                                        XmlNode x = temp.ReadNode(xmlReader);

                                        XmlNode importPageNode = xmlSavingDoc.ImportNode(x, true);
                                        XmlNode importRecourcesNode = xmlSavingDoc.ImportNode(resourcesNode, true);

                                        if (xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2] == null)
                                        {
                                            xmlSavingDoc.ChildNodes[2].ChildNodes[1].ChildNodes[1].AppendChild(importPageNode);
                                            xmlSavingDoc.ChildNodes[2].ChildNodes[1].ChildNodes[1].AppendChild(importRecourcesNode);
                                        }
                                        else
                                        {
                                            xmlSavingDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2].AppendChild(importPageNode);
                                            xmlSavingDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2].AppendChild(importRecourcesNode);
                                        }

                                        xmlSavingDoc.Save(pageDescriptorPath + @"\" + pageNumber + @"\TextDescriptor.xml");

                                        GMGColorMediaDefine.Common.Utils.LogEvent("ProcessFile.CreateTextDescriptorsForPage - pageDescriptorPath +  pageNumber: " + pageDescriptorPath + @"\" + pageNumber + @"\TextDescriptor.xml", EventLogEntryType.Information);

                                        found = true;
                                        break;
                                    }
                                    savingPage++;
                                }
                            }
                            catch (Exception ex)
                            {
                                CopyDefaultTetml(pageDescriptorPath + @"\" + pageNumber + @"\TextDescriptor.xml");
                                LogEvent(string.Format("ProcessFile SetTextDescriptorForPages. Exception: {0}, StackTrace: {1}", ex.Message, ex.StackTrace.ToString()), EventLogEntryType.Error);
                            }
                        }
                        while (nav.MoveToNext("Page", nav.NamespaceURI));
                    }
                }
                else
                {
                     CopyDefaultTetml(pageDescriptorPath + @"\" + pageNumber + @"\TextDescriptor.xml");
                }
            }
            catch (Exception ex)
            {
                LogEvent(string.Format("ProcessFile SetTextDescriptorForPages. Exception: {0}, StackTrace: {1}", ex.Message, ex.StackTrace.ToString()), EventLogEntryType.Error);
            }
        }

        private static XmlNode GetResourcesNode(XmlDocument xmlDoc)
        {
            XmlNode resourcesNode;

            resourcesNode = xmlDoc.GetElementsByTagName("Resources")[0];
            
            if (resourcesNode == null)
            {
                if (xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2] == null)
                {
                    // CZ-2646
                    resourcesNode = xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[1].LastChild;
                }
                else
                {
                    resourcesNode = xmlDoc.ChildNodes[2].ChildNodes[1].ChildNodes[2].LastChild;
                }
            }

            return resourcesNode;
        }

        #endregion

        #region Common

        /// <summary>
        /// Resize the given image to the specified dimensions
        /// </summary>
        /// <param name="srcImage"></param>
        /// <param name="size"></param>
        /// <param name="preserveAspectRatio"></param>
        /// <returns></returns>
        public static Image ResizeImage(Image srcImage, Size size, bool preserveAspectRatio = true)
        {        
            int newWidth;
            int newHeight;
            if (preserveAspectRatio)
            {
                int originalWidth = srcImage.Width;
                int originalHeight = srcImage.Height;
                float percentWidth = (float)size.Width / (float)originalWidth;
                float percentHeight = (float)size.Height / (float)originalHeight;
                float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }
            Image newImage = new Bitmap(newWidth, newHeight);
            using (Graphics graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(srcImage, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        /// <summary>
        /// Adds a new object for being removed
        /// </summary>
        private void AddObjectTocleanup(CleanupObject cleanupObject)
        {
            lock (_cleanupObjectsLock)
            {
                _cleanupObjectsQueue.Add(cleanupObject);
            }
        }

        /// <summary>
        /// Remove the temporary objects (must be called on a dedicated thread)
        /// </summary>
        public static void ProcessMediaDefineCleanupQueue()
        {
            lock (_cleanupObjectsLock)
            {
                foreach (CleanupObject obj in _cleanupObjectsQueue)
                {
                    var uhrFile = obj.Path + "\\UHR.txt";
                    if ((DateTime.UtcNow - obj.AddedTime).TotalMinutes > 1 && !File.Exists(uhrFile))
                    {
                        if (obj.IsFile)
                        {
                            try
                            {
                                if (obj.NrOfRetries <= 5 && File.Exists(obj.Path))
                                    File.Delete(obj.Path);
                                obj.IsRemoved = true;
                            }
                            catch (System.Exception ex)
                            {
                                LogEvent(
                                    string.Format(
                                        "MediaDefine: An error occured while deleting the file {0}, Exception: {1}, StackTrace: {2}, ActiveConfiguration: {3}",
                                        obj.Path, ex.Message, ex.StackTrace.ToString(),
                                        GMGColorConfiguration.AppConfiguration.ActiveConfiguration),
                                    EventLogEntryType.Error);
                                obj.NrOfRetries++;
                            }
                        }
                        else
                        {
                            try
                            {
                                if (obj.NrOfRetries <= 5)
                                {
                                    var dir = new DirectoryInfo(obj.Path);
                                    if (dir.Exists)
                                        dir.Delete(true);
                                }
                                obj.IsRemoved = true;
                            }
                            catch (System.Exception ex)
                            {
                                LogEvent(
                                    string.Format(
                                        "MediaDefine: An error occured while deleting the directory {0}, Exception: {1}, StackTrace: {2}, ActiveConfiguration: {3}",
                                        obj.Path, ex.Message, ex.StackTrace.ToString(),
                                        GMGColorConfiguration.AppConfiguration.ActiveConfiguration),
                                    EventLogEntryType.Error);
                                obj.NrOfRetries++;
                            }
                        }
                    }
                }

                _cleanupObjectsQueue.RemoveAll(item => item.IsRemoved == true);
            }
        }

        private void UpdateApprovalPageProgress(GMGColorDAL.ApprovalPage objApprovalPage, int progress)
        {
            try
            {
                objApprovalPage.Progress = progress;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                LogEvent(string.Format("An error occured while updating database, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Warning);
            }
        }

        public static void LogEvent(string message, EventLogEntryType entryType)
        {
            try
            {
                Severity severity = Severity.Debug;

                switch (entryType)
                {
                    case EventLogEntryType.Information:
                    case EventLogEntryType.SuccessAudit:
                        severity = Severity.Info;
                        break;
                    case EventLogEntryType.Warning:
                        severity = Severity.Warning;
                        break;
                    case EventLogEntryType.Error:
                        severity = Severity.Error;
                        break;
                    case EventLogEntryType.FailureAudit:
                        severity = Severity.Fatal;
                        break;
                }

                ServiceLog.Log(new LoggingNotification(message, severity));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetInsertApprovalPageExceptionTree(Exception ex)
        {
            return "Message: " + ex.Message.ToString() + Environment.NewLine + "StackTrace: " + ex.StackTrace?.ToString() +
                (ex.InnerException != null ? Environment.NewLine + GetInsertApprovalPageExceptionTree(ex.InnerException) : "");

        }

        public static void DeleteAndCreateLocalFolder(string folderPath, bool isJobHighRes = false)
        {
            try
            {
                if (Directory.Exists(folderPath))
                {
                    if (!isJobHighRes)
                    {
                        Directory.Delete(folderPath, true);
                        Directory.CreateDirectory(folderPath);
                    }
                }
                else
                {
                    Directory.CreateDirectory(folderPath);
                }
            }
            catch (Exception ex)
            {
                LogEvent(String.Format("Could Not Delete folder: {0} Exception {1}", folderPath, ex.Message), EventLogEntryType.Warning);
            }
        }

        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static CustomDictionary<string, string> GetMessageParameters(string message)
        {
            CustomDictionary<string, string> dicPara = new CustomDictionary<string, string>();

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(message);

            if (xmlDoc.HasChildNodes)
            {
                foreach (XmlNode node in xmlDoc.LastChild.ChildNodes)
                {
                    dicPara.Add(node.Name, node.InnerText);
                }
            }
            return dicPara;
        }

        #endregion

        public bool ConvertHtmlToImage(string url, string destinationFilePath, int delay, int approvalId)
        {
            try
            {
                int id = BrowshotService.ScreenshotCreate(url, GMGColorConfiguration.AppConfiguration.BrowshotInstanceID, delay, GMGColorConfiguration.AppConfiguration.BrowshotAccessKey);

                if (id == 0)
                {
                    return false;
                }
                else
                {
                    GMGColorDAL.Approval approval = DALUtils.GetObject<GMGColorDAL.Approval>(approvalId, context);
                    approval.TranscodingJobId = id.ToString();
                    context.SaveChanges();
                }

                int status = (int)BrowshotService.BrowshotStatus.Pending;
                DateTime startTime = DateTime.UtcNow;

                while (status == (int)BrowshotService.BrowshotStatus.Pending && ( DateTime.UtcNow - startTime).TotalSeconds < GMGColorConfiguration.AppConfiguration.WebPageTimeout)
                {
                    status = BrowshotService.ScreenshotInfo(id, destinationFilePath, GMGColorConfiguration.AppConfiguration.BrowshotAccessKey);
                    Thread.Sleep(1000);
                }
                    
                if (status == (int)BrowshotService.BrowshotStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogEvent(String.Format("ConvertHtmlToimage method failed Exception: {0} StackTrace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
                return false;
            }
        }

        /// <summary>
        /// Retrieves the size on an image without loding it into memory
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private Size GetImageSize(string path)
        {
            using(FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using(Image img = Image.FromStream(stream, false, false))
                {
                    return new Size((int)img.PhysicalDimension.Width, (int)img.PhysicalDimension.Height);
                }
            }
        }

        private float GetImageResolution(string path, float defaultResolution)
        {
            try
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (Image img = Image.FromStream(stream, false, false))
                    {
                        if (img.VerticalResolution <= 0)
                        {
                            return Math.Min(defaultResolution, 72);
                        }
                        return Math.Min(defaultResolution, (int)img.VerticalResolution);
                    }

                }
            }
            catch (Exception)
            {
                return 72;
            }
        }

        private bool IsProfileExist(string filePath, string accountRegion)
        {
            bool fileExists = false;
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                fileExists = AWSSimpleStorageService.FileExistsInBucket(GMGColorConfiguration.AppConfiguration.DataFolderName(accountRegion), filePath, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
            }
            else
            {
                var fi = new FileInfo(filePath);
                fileExists = fi.Exists;
            }
            if (!fileExists)
            {
                LogEvent(String.Format("GMGCoZone Media Define Service: In IsProfileExist filePath is not exist {0}", filePath), EventLogEntryType.Information);
                Thread.Sleep(2000);
            }
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);
            XmlNode idNode = doc.SelectSingleNode("/*[local-name()='TET']/*[local-name()='Document']/*[local-name()='OutputIntents']");
            if (idNode == null) return false;
            return true;
        }
                
        #endregion
    }
}
