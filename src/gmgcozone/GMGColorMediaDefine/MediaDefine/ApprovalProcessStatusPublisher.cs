﻿using GMGColorMediaDefine.Interfaces;
using GMG.CoZone.Common.AWS.Interfaces;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Models;
using System;
using Newtonsoft.Json;
using GMG.CoZone.Common.Models.Enums;

namespace GMGColorMediaDefine.MediaDefine
{
    public class ApprovalProcessStatusPublisher : IApprovalProcessStatusPublisher
    {
        private IAWSSimpleNotificationService _snsClient;
        private IGMGConfiguration _gmgConfiguration;

        public ApprovalProcessStatusPublisher(IAWSSimpleNotificationService snsClient, IGMGConfiguration gmgConfiguration)
        {
            _snsClient = snsClient;
            _gmgConfiguration = gmgConfiguration;
        }

        public void PublishStatus(int approvalId, ApprovalProgressStatusEnum status)
        {
            try
            {
                var topicArn = _snsClient.GetTopicArn(_gmgConfiguration.AWSSNSMediaDefineResponseTopic);
                if (_snsClient.CheckTopicExists(topicArn))
                {
                    var objStatusMessage = new MediaDefineApprovalProgressStatusModel
                    {
                        ApprovalId = approvalId,
                        Status = status
                    };
                    _snsClient.WriteMessage(topicArn, JsonConvert.SerializeObject(objStatusMessage));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
