﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;

namespace GMGColor.MediaProcessor
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();

            //this.serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            //this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
        }

        private void serviceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            //this.serviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            //this.serviceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;

            var serviceController = new ServiceController(serviceInstaller.ServiceName);
            serviceController.Start();
        }
    }
}
