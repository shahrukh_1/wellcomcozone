﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using GMGColorDAL;
using GMGColorMediaDefine.Common;
using GMGColorMediaDefine.MediaDefine;
using GMGColorMediaDefine.TileServer;
using GMGColor.AWS;

namespace GMGColor.MediaProcessor
{
    public partial class MediaProcessor : ServiceBase
    {
        #region Fields

        private static RenderEngineInstance aoiInstance;

        private static string inputProfile = null;
        private static string outputProfile = null;
        private static string grayProfile = null;

        private CancellationTokenSource _cleanupObjectsTaskCancellationToken = new CancellationTokenSource();
        private Task _cleanupObjectsTask = null;

        private TileServer _tileServer;
        private MediaDefine _mediaDefine;

        public const string DEFSERVICENAME = "GMGCoZone Media Define";

        #endregion

        #region Properties

        public static RenderEngineInstance AOIInstance 
        {
            get
            {
                return aoiInstance;
            }
        }

        public static string InputProfile
        {
            get
            {
                return inputProfile;
            }
        }

        public static string GrayProfile
        {
            get
            {
                return grayProfile;
            }
        }

        public static string OutputProfie
        {
            get
            {
                return outputProfile;
            }
        }

        #endregion

        #region Constructors

        public MediaProcessor()
        {
            InitializeComponent();
        }

        #endregion

        #region Overridden Events
        protected override void OnStart(string[] args)
        {
            StartService();
        }
        
        static void SetRecoveryOptions()
        {
            int exitCode;
            using (var process = new Process())
            {
                var startInfo = process.StartInfo;
                startInfo.FileName = "sc";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                // tell Windows that the service should restart if it fails
                startInfo.Arguments = string.Format("failure \"{0}\" reset= 0 actions= restart/60000", ConfigurationManager.AppSettings.Get("ServiceName"));

                process.Start();
                process.WaitForExit();

                exitCode = process.ExitCode;

                process.Close();
            }

            if (exitCode != 0)
                throw new InvalidOperationException();
        }

        protected override void OnStop()
        {
            StopService();   
        }

        #endregion

        #region Constants

        public const string LibrariesRepositoryPath = @"C:\Users\Public\";
        public const string AOIInstanceID = "CoZone";
        public const string LibrariesFolderName = "Libraries";

        #endregion

        #region Methods

        public void StartService()
        {
            try
            {

#if !DEBUG
                // set recovery mode to autorestart
                SetRecoveryOptions();
#endif
                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
                {
                    AWSClient.Init(new AWSSettings() { Region = GMGColorConfiguration.AppConfiguration.AWSRegion });
                }

                _cleanupObjectsTask = Task.Factory.StartNew(() => CleanupTemporaryObjects(_cleanupObjectsTaskCancellationToken.Token));

                // Log a service start message to the Application log.
                Utils.LogEvent("GMGCoZone Media Processor Service in OnStart.", EventLogEntryType.Information);

                aoiInstance = RenderEngineInstance.Startup(AOIInstanceID, "E7B365A1", "AOI_CoZone");

                if (!Utils.GetDefaultProfileAbsolutePath(GMGColorConfiguration.AppConfiguration.OutputProfile, out outputProfile))
                {
                    Utils.LogEvent("Output profile could not be found at specified path", EventLogEntryType.Error);
                    throw new Exception("Output profile not found!");
                }
                if (!Utils.GetDefaultProfileAbsolutePath(GMGColorConfiguration.AppConfiguration.InputProfile, out inputProfile))
                {
                    Utils.LogEvent("Input profile could not be found at specified path", EventLogEntryType.Error);
                    throw new Exception("Input profile not found!");
                }
                if (!Utils.GetDefaultProfileAbsolutePath(GMGColorConfiguration.AppConfiguration.GrayProfile, out grayProfile))
                {
                    Utils.LogEvent("Grey profile could not be found at specified path", EventLogEntryType.Error);
                    throw new Exception("Gray profile not found!");
                }

                //start Media Define
                _mediaDefine = new MediaDefine();
                _mediaDefine.StartMediaDefine();

                //start tileServer
                _tileServer = new TileServer();
                _tileServer.StartTileServer();

                //copy pantone libraries
                Task copyPantoneLibrariesTask = new Task(CopyPantoneLibraries);
                copyPantoneLibrariesTask.Start();

            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service OnStart throw an exception, Message: {0} Stack Trace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
                throw ex;
            }
        }

        public void StopService()
        {
            // Log a service stop message to the Application log.            
            Utils.LogEvent("GMGCoZone Media Define Service in OnStop.", EventLogEntryType.Information);

            _cleanupObjectsTaskCancellationToken.Cancel();

            try
            {
                RenderEngineInstance.Terminate();
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by ExecuteThread:AOI_Instance.Terminate. Exception: {0}", ex.Message), EventLogEntryType.Error);
                throw ex;
            }

            //stop mediaDefine
            if (_mediaDefine != null)
            {
                _mediaDefine.StopMediaDefine();
            }
            //stop tileServer 
            if (_tileServer != null)
            {
                _tileServer.StopTileServer();
            }
        }

        /// <summary>
        /// Remove the temporary objects (must be called on a dedicated thread)
        /// </summary>
        public static void CleanupTemporaryObjects(CancellationToken cancellationToken)
        {
            int sleepTime = 2000;
            try
            {
                while (true)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                    ProcessFile.ProcessMediaDefineCleanupQueue();
                    TileServerProcessor.ProcessTileServerCleanupQueue();
                    Thread.Sleep(sleepTime);
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(string.Format("An error occured while cleaning up the temporary object, Exception: {0}, StackTrace: {1}, ActiveConfiguration: {2}", ex.Message, ex.StackTrace.ToString(), GMGColorConfiguration.AppConfiguration.ActiveConfiguration), EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// Copy Pantone Libraries to expected folder by AOI
        /// </summary>
        private void CopyPantoneLibraries()
        {
            try
            {
                string pantoneLibraryPath = Path.Combine(LibrariesRepositoryPath, AOIInstanceID, LibrariesFolderName);

                if (!Directory.Exists(pantoneLibraryPath))
                {
                    Directory.CreateDirectory(pantoneLibraryPath);
                }

                string workingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                DirectoryInfo dir = new DirectoryInfo(Path.Combine(workingDirectory, LibrariesFolderName));

                // Get the files in the directory and copy them to the new location.
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string temppath = Path.Combine(pantoneLibraryPath, file.Name);
                    file.CopyTo(temppath, true);
                }
            }
            catch (Exception ex)
            {
                Utils.LogEvent(String.Format("GMGCoZone MediaProcessor CopyPantoneLibraries throw an exception, Message: {0} Stack Trace: {1}", ex.Message, ex.StackTrace), EventLogEntryType.Error);
            }
        }

        #endregion
    }
}
