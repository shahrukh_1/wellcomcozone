﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

using Zencoder;

using GMGColor.AWS;
using GMGColor.BusinessObjects;
using GMGColor.BusinessObjects.Common;
using GMGColor.MediaProcessor.Common;
using GMGColor.MediaProcessor.Interfaces;
using GMGColor.Resources;

namespace GMGColor.MediaProcessor
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class GMGColorZencoder : IGMGColorZencoder
    {
        private ZencoderData objZencoderData = null;
        public static SortedDictionary<int, JobProgress> jobProgressInfo = new SortedDictionary<int, JobProgress>();

        public string FTPPath { get; set; }
        public bool UseAmazonBucket { get; set; }

        #region Consctuctors

        public GMGColorZencoder()
        {

        }

        #endregion

        #region Methods

        /// <summary>        
        /// this will encode video and uplode encoded video in a default zencoder location and return the output url of the encoded video        
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="url"></param>
        /// <returns></returns>        
        public List<string> CreateVideoJob(string apiKey, string url)
        {
            List<string> lstOutputURLs = null;
            var zen = new Zencoder.Zencoder(apiKey);

            Zencoder.Output outputMP4 = new Output();
            outputMP4.H264Level = Zencoder.H264Level.FivePointOne;
            outputMP4.H264Profile = Zencoder.H264Profile.High;
            outputMP4.H264ReferenceFrames = 5;
            outputMP4.Tuning = Zencoder.Tuning.FastDecode;

            Zencoder.Output outputOGG = new Output();
            outputOGG.Tuning = Zencoder.Tuning.FastDecode;
            outputOGG.VideoCodec = Zencoder.VideoCodec.Theora; //webm

            Zencoder.Output outputThumb = new Output();
            Thumbnails thumbs = new Thumbnails();
            thumbs.Public = false;
            thumbs.StartAtFirstFrame = false;
            thumbs.Number = 2;
            thumbs.Format = ThumbnailFormat.Png;
            thumbs.Label = "Thumb";
            thumbs.Prefix = "MyThumb";
            thumbs.Interval = 5;
            //thumbs.BaseUrl = outputBaseUrl;
            outputThumb.Thumbnails = new Thumbnails[] { thumbs };

            Zencoder.CreateJobResponse response = zen.CreateJob(url, new Zencoder.Output[] { outputMP4, outputOGG, outputThumb });
            if (response.Success)
            {
                Zencoder.JobDetailsResponse detailsResponse = zen.JobDetails(response.Id);
                Zencoder.JobProgressResponse progressResponse = zen.JobProgress(response.Outputs.First().Id);

                if (progressResponse.Success)
                {
                    while (progressResponse.State != OutputState.Finished)
                    {
                        progressResponse = zen.JobProgress(response.Outputs.First().Id);
                    }
                }

                if (detailsResponse.Job.OutputMediaFiles.Count() > 0)
                {
                    lstOutputURLs = new List<string>();

                    for (int i = 0; i < detailsResponse.Job.OutputMediaFiles.Count(); i++)
                    {
                        lstOutputURLs.Add(((Zencoder.MediaFile)(detailsResponse.Job.OutputMediaFiles[i])).Url);
                    }
                }
            }

            return lstOutputURLs;
        }

        public double GetJobProgress(string apiKey, int jobID)
        {
            var zen = new Zencoder.Zencoder(apiKey);
            Zencoder.JobProgressResponse progressResponse = zen.JobProgress(jobID);
            return progressResponse.Progress;
        }

        public bool DeleteJob(string apiKey, int jobID)
        {
            var zen = new Zencoder.Zencoder(apiKey);
            Zencoder.DeleteJobResponse deleteResponse = zen.DeleteJob(jobID);

            return deleteResponse.Success;
        }

        public bool ReSubmitJob(string apiKey, int jobID)
        {
            var zen = new Zencoder.Zencoder(apiKey);
            Zencoder.ResubmitJobResponse resubmitResponse = zen.ResubmitJob(jobID);

            return resubmitResponse.Success;
        }

        public bool CancelJob(string apiKey, int jobID)
        {
            var zen = new Zencoder.Zencoder(apiKey);
            Zencoder.CancelJobResponse cancelResponse = zen.CancelJob(jobID);

            return cancelResponse.Success;
        }

        public static Dictionary<string, double> GetImageWidthHeight(string imgPath)
        {
            Dictionary<string, double> imgPara = new Dictionary<string, double>();
            System.Drawing.Image objImg = System.Drawing.Image.FromFile(imgPath);

            imgPara.Add("Width", objImg.Width);
            imgPara.Add("Height", objImg.Height);

            return imgPara;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key">Zencoder key</param>
        /// <param name="approvalID">Created Approval ID</param>
        /// <param name="approvalFolderPath"></param>
        /// <param name="sourceFilePath">location of the video or audio file</param>
        /// <param name="outputBaseUrl">path to write zencoder output</param>
        /// <param name="jobType">'Audio' or 'Video'</param>
        public void SetJobPara(string key, int approvalID, string approvalFolderPath, string sourceFilePath, string outputBaseUrl, string jobType)
        {
            objZencoderData = new ZencoderData();

            this.UseAmazonBucket = GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket;
            objZencoderData.Key = key;
            objZencoderData.ApprovalID = approvalID;
            objZencoderData.ApprovalFolderPath = approvalFolderPath;
            objZencoderData.SourceFile = sourceFilePath;
            objZencoderData.OutputBaseUrl = outputBaseUrl;
            objZencoderData.JobType = jobType;
        }

        public void StartVideoJob()
        {
            Thread oThread = new Thread(new ThreadStart(CreateVideoJob));
            oThread.Start();
        }

        public void StartAudioJob()
        {
            Thread oThread = new Thread(new ThreadStart(CreateAudioJob));
            oThread.Start();
        }

        private void CreateVideoJob()
        {
            var zen = new Zencoder.Zencoder(objZencoderData.Key);
            string fileName = Path.GetFileNameWithoutExtension(objZencoderData.SourceFile);

            S3Access objS3Access = new S3Access();
            objS3Access.Grantee = "http://acs.amazonaws.com/groups/global/AllUsers";
            objS3Access.Permissions = new S3Permission[] { S3Permission.Read };

            // Thumbnail 128px
            Thumbnails thumb128 = new Thumbnails();
            thumb128.Public = false;
            thumb128.StartAtFirstFrame = false;
            thumb128.Number = 1;
            thumb128.Format = ThumbnailFormat.Jpg;
            thumb128.Label = "Thumb-128px";
            thumb128.Prefix = "Thumb-128px";
            thumb128.Width = 128;
            thumb128.Height = 128;
            //thumbs.Interval = 5;
            thumb128.BaseUrl = objZencoderData.OutputBaseUrl + "1/";
            thumb128.WithAccessControl(objS3Access);

            // Thumbnail 360px
            Thumbnails thumb360 = new Thumbnails();
            thumb360.Public = false;
            thumb360.StartAtFirstFrame = false;
            thumb360.Number = 1;
            thumb360.Format = ThumbnailFormat.Jpg;
            thumb360.Label = "Thumb-360px";
            thumb360.Prefix = "Thumb-360px";
            thumb360.Width = 360;
            thumb360.Height = 360;
            //thumbs.Interval = 5;
            thumb360.BaseUrl = objZencoderData.OutputBaseUrl + "1/";
            thumb360.WithAccessControl(objS3Access);

            // mp4 output
            //Zencoder.Output outputMP4 = new Output();
            //outputMP4.H264Level = Zencoder.H264Level.FivePointOne;
            //outputMP4.H264Profile = Zencoder.H264Profile.High;
            //outputMP4.H264ReferenceFrames = 5;
            //outputMP4.Tuning = Zencoder.Tuning.FastDecode;
            //outputMP4.BaseUrl = objZencoderData.OutputBaseUrl;
            //outputMP4.Label = "ActivityMP4";
            //outputMP4.FileName = "EncordedActivity.mp4";
            //outputMP4.Thumbnails = new Thumbnails[] { thumb128, thumb360 };
            //outputMP4.AccessControl = new S3Access[] { objS3Access };

            //f4v Output
            Zencoder.Output outputF4V = new Output();
            outputF4V.Format = Zencoder.MediaFileFormat.FFourV;
            outputF4V.VideoCodec = Zencoder.VideoCodec.H264;
            outputF4V.AudioCodec = Zencoder.AudioCodec.Aac;
            outputF4V.Quality = 4;
            outputF4V.Tuning = Zencoder.Tuning.FastDecode;
            outputF4V.BaseUrl = objZencoderData.OutputBaseUrl;
            outputF4V.Label = fileName;
            outputF4V.FileName = fileName + ".f4v";
            outputF4V.Thumbnails = new Thumbnails[] { thumb128, thumb360 };
            outputF4V.AccessControl = new S3Access[] { objS3Access };

            //WebM output
            //Zencoder.Output outputWebm = new Output();
            //outputWebm.Format = Zencoder.MediaFileFormat.WEBM;
            //outputWebm.Tuning = Zencoder.Tuning.FastDecode;
            //outputWebm.VideoCodec = Zencoder.VideoCodec.VP8; //webm
            //outputWebm.BaseUrl = objZencoderData.OutputBaseUrl;
            //outputWebm.Label = fileName;
            //outputWebm.FileName = fileName + ".webm";
            //outputWebm.AccessControl = new S3Access[] { objS3Access };

            try
            {
                //IImageProcess objImageProcess = new ImageProcess();
                Zencoder.CreateJobResponse response = zen.CreateJob(objZencoderData.SourceFile, new Zencoder.Output[] { /*outputMP4, outputWebm,*/ outputF4V });

                if (response.Success)
                {
                    Zencoder.JobDetailsResponse detailsResponse = zen.JobDetails(response.Id);
                    Zencoder.JobProgressResponse progressResponse = zen.JobProgress(response.Outputs.First().Id);

                    if (progressResponse.Success)
                    {
                        JobProgress progress = new JobProgress();
                        progress.JobType = "Video";
                        jobProgressInfo.Add(objZencoderData.ApprovalID, progress);

                        while (progressResponse.State != OutputState.Finished)
                        {
                            progressResponse = zen.JobProgress(response.Outputs.First().Id);

                            progress = jobProgressInfo[objZencoderData.ApprovalID];
                            progress.ApprovalId = objZencoderData.ApprovalID;
                            progress.JobOutputId = response.Outputs.First().Id;
                            progress.JobCurrentProgress = ((progressResponse.State != OutputState.Finished) && (progressResponse.Progress == 100)) ? progressResponse.Progress - 1 : progressResponse.Progress;
                            progress.EventText = progressResponse.CurrentEvent.ToString();

                            if (this.UseAmazonBucket)
                            {
                                AWSSimpleStorageService.UploadFileWithContent(GMGColorConfiguration.AppConfiguration.DataFolderName, objZencoderData.OutputBaseUrl.Replace(GMGColorConfiguration.AppConfiguration.PathToDataFolder, string.Empty) + "Progress.txt", progress.EventText + ":" + progress.JobCurrentProgress, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                            }
                            else
                            {
                                GMGColorIO.WriteProgressFile(progress.EventText + ":" + progress.JobCurrentProgress, objZencoderData.ApprovalFolderPath + @"\Progress.txt");
                                //jobProgressInfo[objZencoderData.ApprovalID] = progress;
                            }
                        }

                        progress = jobProgressInfo[objZencoderData.ApprovalID];
                        progress.ApprovalId = objZencoderData.ApprovalID;
                        progress.JobOutputId = response.Outputs.First().Id;
                        progress.JobCurrentProgress = 100;
                        progress.EventText = "Finished";

                        if (this.UseAmazonBucket)
                        {
                            AWSSimpleStorageService.UploadFileWithContent(GMGColorConfiguration.AppConfiguration.DataFolderName, objZencoderData.OutputBaseUrl.Replace(GMGColorConfiguration.AppConfiguration.PathToDataFolder, string.Empty) + "Progress.txt", progress.EventText + ":" + progress.JobCurrentProgress, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                        }
                        else
                        {
                            GMGColorIO.WriteProgressFile(progress.EventText + ":" + progress.JobCurrentProgress, objZencoderData.ApprovalFolderPath + @"\Progress.txt");
                            //jobProgressInfo[objZencoderData.ApprovalID] = progress;
                        }
                        progress = null;
                    }
                    else if (this.UseAmazonBucket)
                    {
                        AWSSimpleStorageService.UploadFileWithContent(GMGColorConfiguration.AppConfiguration.DataFolderName, objZencoderData.OutputBaseUrl.Replace(GMGColorConfiguration.AppConfiguration.PathToDataFolder, string.Empty) + "Progress.txt", "ERROR" + ":0", GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                    }

                    //Write Output to approvals folder
                    if (!this.UseAmazonBucket)
                    {
                        if (detailsResponse.Job.OutputMediaFiles.Count() > 0)
                        {
                            string[] filePaths;
                            FileInfo f;

                            filePaths = Directory.GetFiles(objZencoderData.OutputBaseUrl, "*.webm");
                            f = new FileInfo(filePaths[0]);
                            f.MoveTo(objZencoderData.ApprovalFolderPath + "\\" + fileName + ".webm");

                            filePaths = Directory.GetFiles(objZencoderData.OutputBaseUrl, "*.f4v");
                            f = new FileInfo(filePaths[0]);
                            f.MoveTo(objZencoderData.ApprovalFolderPath + "\\" + fileName + ".f4v");

                            filePaths = Directory.GetFiles(objZencoderData.OutputBaseUrl, "*.jpg");
                            foreach (string thumbPath in filePaths)
                            {
                                f = new FileInfo(thumbPath);
                                f.MoveTo(objZencoderData.ApprovalFolderPath + "\\1\\" + Path.GetFileName(thumbPath));
                            }

                            if (Directory.Exists(objZencoderData.OutputBaseUrl))
                            {
                                try { Directory.Delete(objZencoderData.OutputBaseUrl, true); }
                                catch { }
                            }
                        }
                    }
                }
                else if (this.UseAmazonBucket)
                {
                    GMGColorIO.WriteProgressFile(Resources.Resources.lblError, objZencoderData.OutputBaseUrl + "Progress.txt");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateAudioJob()
        {
            var zen = new Zencoder.Zencoder(objZencoderData.Key);

            // ogg output
            Zencoder.Output outputOGG = new Output();
            outputOGG.Tuning = Zencoder.Tuning.FastDecode;
            outputOGG.AudioCodec = Zencoder.AudioCodec.Vorbis; //ogg
            outputOGG.BaseUrl = objZencoderData.OutputBaseUrl;
            outputOGG.Label = "ActivityOGG";
            outputOGG.FileName = "EncordedActivity.ogg";

            // ogg output
            Zencoder.Output outputMp3 = new Output();
            outputMp3.Tuning = Zencoder.Tuning.FastDecode;
            outputMp3.AudioCodec = Zencoder.AudioCodec.Mp3; //mp3
            outputMp3.BaseUrl = objZencoderData.OutputBaseUrl;
            outputMp3.Label = "ActivityMp3";
            outputMp3.FileName = "EncordedActivity.mp3";
            try
            {
                Zencoder.CreateJobResponse response = zen.CreateJob(objZencoderData.SourceFile, new Zencoder.Output[] { outputOGG, outputMp3 });
                if (response.Success)
                {
                    Zencoder.JobDetailsResponse detailsResponse = zen.JobDetails(response.Id);
                    Zencoder.JobProgressResponse progressResponse = zen.JobProgress(response.Outputs.First().Id);

                    if (progressResponse.Success)
                    {
                        JobProgress progress = new JobProgress();
                        progress.JobType = "Audio";
                        jobProgressInfo.Add(objZencoderData.ApprovalID, progress);

                        while (progressResponse.State != OutputState.Finished)
                        {
                            progressResponse = zen.JobProgress(response.Outputs.First().Id);

                            progress = jobProgressInfo[objZencoderData.ApprovalID];
                            progress.ApprovalId = objZencoderData.ApprovalID;
                            progress.JobOutputId = response.Outputs.First().Id;
                            progress.JobCurrentProgress = ((progressResponse.State != OutputState.Finished) && (progressResponse.Progress == 100)) ? progressResponse.Progress - 1 : progressResponse.Progress;
                            progress.EventText = progressResponse.CurrentEvent.ToString();

                            if (this.UseAmazonBucket)
                            {
                                AWSSimpleStorageService.UploadFileWithContent(GMGColorConfiguration.AppConfiguration.DataFolderName, objZencoderData.OutputBaseUrl.Replace(GMGColorConfiguration.AppConfiguration.PathToDataFolder, string.Empty) + "progress.text", progress.EventText + ":" + progress.JobCurrentProgress, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                            }
                            else
                            {
                                GMGColorIO.WriteProgressFile(progress.EventText + ":" + progress.JobCurrentProgress, objZencoderData.OutputBaseUrl + @"\Progress.txt");
                                //jobProgressInfo[objZencoderData.ApprovalID] = progress;
                            }
                        }

                        progress = jobProgressInfo[objZencoderData.ApprovalID];
                        progress.ApprovalId = objZencoderData.ApprovalID;
                        progress.JobOutputId = response.Outputs.First().Id;
                        progress.JobCurrentProgress = 100;
                        progress.EventText = "Finished";

                        if (this.UseAmazonBucket)
                        {
                            AWSSimpleStorageService.UploadFileWithContent(GMGColorConfiguration.AppConfiguration.DataFolderName, objZencoderData.OutputBaseUrl.Replace(GMGColorConfiguration.AppConfiguration.PathToDataFolder, string.Empty) + "progress.text", progress.EventText + ":" + progress.JobCurrentProgress, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey);
                        }
                        else
                        {
                            GMGColorIO.WriteProgressFile(progress.EventText + ":" + progress.JobCurrentProgress, objZencoderData.OutputBaseUrl + @"\Progress.txt");
                            //jobProgressInfo[objZencoderData.ApprovalID] = progress;
                        }
                        progress = null;
                    }

                    if (!this.UseAmazonBucket)
                    {
                        if (detailsResponse.Job.OutputMediaFiles.Count() > 0)
                        {
                            string[] filePaths = Directory.GetFiles(objZencoderData.FTPPath, "*.ogg");
                            FileInfo f = new FileInfo(filePaths[0]);
                            if (filePaths[0].Contains("sample"))
                            {
                                f = new FileInfo(filePaths[1]);
                                f.MoveTo(objZencoderData.SourcePath + "EncordedActivity.ogg");
                            }
                            else
                            {
                                f.MoveTo(objZencoderData.SourcePath + "EncordedActivity.ogg");
                            }

                            filePaths = Directory.GetFiles(objZencoderData.FTPPath, "*.mp3");
                            f = new FileInfo(filePaths[0]);
                            f.MoveTo(objZencoderData.SourcePath + "EncordedActivity.mp3");

                            for (int i = 0; i < filePaths.Count<string>(); i++)
                            {
                                if (File.Exists(filePaths[i]))
                                    File.Delete(filePaths[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SortedDictionary<int, JobProgress> GetJobProgress()
        {
            return jobProgressInfo;
        }
        #endregion
    }
}
