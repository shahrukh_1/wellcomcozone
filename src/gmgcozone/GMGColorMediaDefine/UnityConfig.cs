﻿using GMG.CoZone.Common.AWS;
using GMG.CoZone.Common.AWS.Interfaces;
using GMG.CoZone.Common.Interfaces;
using GMG.CoZone.Common.Module;
using GMG.CoZone.Common.Module.IO;
using GMG.CoZone.Common.Services;
using GMG.CoZone.Common.Util;
using GMGColorMediaDefine.Common;
using GMGColorMediaDefine.Interfaces;
using Microsoft.Practices.Unity;

namespace GMGColorMediaDefine
{
    public class UnityConfig
    {
        private static IUnityContainer _container;

        public static IUnityContainer GetContainerInstance()
        {
            _container = new UnityContainer();
            RegisterComponents();
            return _container;
        }

        private static void RegisterComponents()
        {
            _container.RegisterType<IGMGConfiguration, GMGConfiguration>();
            _container.RegisterType<IQueueMessageManager, QueueMessageManager>();
            _container.RegisterType<IAWSSNSService, AWSSNSService>();
            _container.RegisterType<IApprovalStatusMessageSender, ApprovalStatusMessageSender>();
            _container.RegisterType<IAWSSimpleNotificationService, AWSSimpleNotificationService>();
            _container.RegisterType<IRetryMechanism, RetryMechanism>();
            _container.RegisterType<IFileManager, FileManager>();
            _container.RegisterType<IAWSSimpleStorageService, AWSSimpleStorageService>();
        }
    }
}