﻿using System.Diagnostics;
using System.ServiceProcess;
using System;
using System.Threading;

namespace GMGColor.MediaProcessor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            if (!Debugger.IsAttached)
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new MediaProcessor()
                };
                ServiceBase.Run(ServicesToRun);                
            }
            else
            {
                Console.WriteLine("Server started. Press any key to exit.");
                MediaProcessor processor = new MediaProcessor();
                processor.StartService();

                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }

                processor.Stop();
            }

        }
    }
}
