﻿using GMGColorMediaDefine.Common;

namespace GMGColorMediaDefine.Interfaces
{
    public interface IApprovalStatusMessageSender
    {
        void SendMessage(ApprovalStatusMessageModel messageData);
    }
}
