﻿using GMG.CoZone.Common.Module.Enums;

namespace GMGColorMediaDefine.Common
{
    class ApprovalPageProgressModel
    {
        public int ApprovalId { get; set; }
        public int PageNumber { get; set; }
        public int Progress { get; set; }
        public string Domain { get; set; }
        public ApprovalTypeEnum ApprovalType { get; set; }
    }
}
