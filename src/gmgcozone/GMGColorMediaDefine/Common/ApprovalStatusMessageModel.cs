﻿using GMG.CoZone.Common.Module.Enums;
using GMGColorBusinessLogic;

namespace GMGColorMediaDefine.Common
{
    public class ApprovalStatusMessageModel
    {
        public int ApprovalId { get; set; }
        public int Progress { get; set; }
        public string Domain { get; set; }
        public DbContextBL Context { get; set; }
        public bool IsError { get; set; }
        public ApprovalTypeEnum ApprovalType { get; set; }
    }
}
