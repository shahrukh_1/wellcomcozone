﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using GMG.CoZone.Component;
using GMG.CoZone.Component.Logging;
using GMGColor.AWS;
using GMGColorBusinessLogic;
using GMGColorDAL;
using System.IO;
using System.Reflection;
using System.Net;
using System.Web;
using GMG.CoZone.Common;
using GMGColorDAL.CustomModels;

namespace GMGColorMediaDefine.Common
{
    public class Utils
    {
        public class SQSMessage
        {
            public string Guid { get; set; }
            public string ReceiptHandle { get; set; }
            public DateTime NextTimeOut { get; set; }
            public string SoftProofingGuid { get; set; }
            public int ApprovalPage { get; set; }
        }

        #region Public Methods

        public static void UpdateVisibilityTimeOut(List<SQSMessage> ProcessingSQSMessages, int visibilityTimeLeft, int visibilityTimePeriod, string messageQueueName )
        {
            if (ProcessingSQSMessages.Count > 0)
            {
                string itemGuid = string.Empty;
                try
                {
                    foreach (SQSMessage msgSQS in ProcessingSQSMessages.GetRange(0, ProcessingSQSMessages.Count))
                    {
                        if (!String.IsNullOrEmpty(messageQueueName) && ((msgSQS.NextTimeOut - DateTime.Now).TotalSeconds < visibilityTimeLeft))
                        {
                            itemGuid = msgSQS.Guid;

                            // Extend message hidden time-out
                            if (AWSSimpleQueueService.ChangeVisibilityTimeOut(messageQueueName, msgSQS.ReceiptHandle, visibilityTimePeriod, GMGColorConfiguration.AppConfiguration.AWSAccessKey, GMGColorConfiguration.AppConfiguration.AWSSecretKey, GMGColorConfiguration.AppConfiguration.AWSRegion))
                            {
                                DateTime nextTimeOut = DateTime.Now.AddSeconds(visibilityTimePeriod);
                                ProcessingSQSMessages.RemoveAll(o => o.Guid == itemGuid);

                                ProcessingSQSMessages.Add(new Utils.SQSMessage() { Guid = msgSQS.Guid, ReceiptHandle = msgSQS.ReceiptHandle, NextTimeOut = nextTimeOut });

                                LogEvent(String.Format("GMGCoZone Media Define Service Update Message Visibility Time-out for queue {0}. ID: {1}, NowTime: {2},  NextTimeOut: {3}", messageQueueName, msgSQS.Guid, DateTime.Now, nextTimeOut), EventLogEntryType.SuccessAudit);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (!String.IsNullOrEmpty(itemGuid))
                    {
                        LogEvent(String.Format("GMGCoZone Media Define Service: Exception thrown by UpdateVisibilityTimeOut for queue {0}.", messageQueueName), EventLogEntryType.Error, ex);
                        ProcessingSQSMessages.RemoveAll(o => o.Guid == itemGuid);
                    }
                }
            }
        }

        public static void LogEvent(string message, EventLogEntryType entryType, Exception exception = null)
        {
            try
            {
                Severity severity = Severity.Debug;

                switch (entryType)
                {
                    case EventLogEntryType.Information:
                    case EventLogEntryType.SuccessAudit:
                        severity = Severity.Info;
                        break;
                    case EventLogEntryType.Warning:
                        severity = Severity.Warning;
                        break;
                    case EventLogEntryType.Error:
                        severity = Severity.Error;
                        break;
                    case EventLogEntryType.FailureAudit:
                        severity = Severity.Fatal;
                        break;
                }

                if (exception != null)
                {
                    ServiceLog.Log(message, exception);
                }
                else
                {
                    ServiceLog.Log(new LoggingNotification(message, severity));
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        /// <summary>
        /// Retrieves the default profile absolute path
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="profileAbsPath"></param>
        /// <returns></returns>
        public static bool GetDefaultProfileAbsolutePath(string profile, out string profileAbsPath)
        {
            string workingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            profileAbsPath = Path.Combine(workingDirectory, profile);
            if (File.Exists(profileAbsPath))
                return true;
            return false;
        }

        /// <summary> 
        /// Retrieves the absolute path of the profile file 
        /// </summary> 
        /// <param name="profileName">profile relative path to the application root(including name)</param> 
        /// <returns></returns> 
        public static bool GetProfileAbsolutePath(string profileFileName, string remoteRepositoryPath, out string profileAbsPath)
        {
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                string workingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                workingDirectory = Path.Combine(workingDirectory, GMGColorConfiguration.AppConfiguration.SoftProofingProfilesRepo);
                if (!Directory.Exists(workingDirectory))
                {
                    Directory.CreateDirectory(workingDirectory);
                }
                profileAbsPath = Path.Combine(workingDirectory, profileFileName);

                if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket && !File.Exists(profileAbsPath))
                {
                    using (WebClient client = new WebClient())
                    {
                        client.DownloadFile(remoteRepositoryPath + HttpUtility.UrlEncode(profileFileName), profileAbsPath);
                    }
                }
            }
            else
            {
                profileAbsPath = Path.Combine(remoteRepositoryPath, profileFileName);
            }

            if (File.Exists(profileAbsPath))
                return true;
            return false;
        }

        /// <summary>
        /// Gets custom profile path
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="approvalJobFolder"></param>
        /// <param name="region"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetCustomProfilePath(ApprovalPageProcessingDetails.CustomICCProfileDetails customProfile, string jobFolderPath,
            string region)
        {
            string profileFolderRelativePath;
            //check if it is global profile
            if (customProfile.IsGlobal)
            {
                profileFolderRelativePath = SettingsBL.GetGlobalCustomProfileRelativeVirtualPath();
            }
            else
            {
                profileFolderRelativePath = SettingsBL.GetCustomProfileRelativeVirtualPath(customProfile.AccGuid, customProfile.Guid);
            }

            string profileFileRelativePath = profileFolderRelativePath + customProfile.Name;
            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(region);

            string processingFilePath = string.Empty;

            string processingFolderPath = jobFolderPath + customProfile.Guid;
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                Directory.CreateDirectory(processingFolderPath);

                processingFilePath = processingFolderPath + "/" + customProfile.Name;
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(
                        pathtoDataFolder + HttpUtility.UrlEncode(profileFileRelativePath),
                        processingFilePath);
                }
            }
            else
            {
                processingFilePath = pathtoDataFolder + profileFileRelativePath;
            }

            return processingFilePath;
        }

        /// <summary>
        /// Gets custom profile path
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="approvalJobFolder"></param>
        /// <param name="region"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetSimulationProfilePath(ApprovalPageProcessingDetails.SimulationICCProfileDetails simulationProfile, string jobFolderPath,
            string region)
        {
            //check if it is global profile

            string profileFolderRelativePath = SoftProofingBL.GetSimulationProfileRelativeVirtualPath(simulationProfile.AccGuid, simulationProfile.Guid);

            string profileFileRelativePath = profileFolderRelativePath + simulationProfile.Name;
            string pathtoDataFolder = GMGColorConfiguration.AppConfiguration.PathToDataFolder(region);

            string processingFilePath = string.Empty;

            string processingFolderPath = jobFolderPath + simulationProfile.Guid;
            if (GMGColorConfiguration.AppConfiguration.IsEnabledS3Bucket)
            {
                Directory.CreateDirectory(processingFolderPath);

                processingFilePath = processingFolderPath + "/" + simulationProfile.Name;
                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(
                        pathtoDataFolder + HttpUtility.UrlEncode(profileFileRelativePath),
                        processingFilePath);
                }
            }
            else
            {
                processingFilePath = pathtoDataFolder + profileFileRelativePath;
            }

            return processingFilePath;
        }

        /// <summary>
        /// Gets custom profile path, and also downlaod it in case of amazon
        /// </summary>
        /// <param name="objApproval"></param>
        /// <param name="approvalJobFolder"></param>
        /// <param name="region"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static string GetCustomProfilePath(Approval objApproval, string jobFolderPath,
            string region, out bool isRGB, out int cstmProfId, DbContextBL context)
        {
            ApprovalCustomICCProfile cstmProfile = objApproval.ApprovalCustomICCProfiles.FirstOrDefault();

            var profileInfo = (from acsp in context.AccountCustomICCProfiles
                                where acsp.ID == cstmProfile.CustomICCProfile
                                select new
                                {
                                    acsp.ID,
                                    acsp.Account,
                                    acsp.Name,
                                    acsp.Guid,
                                    acsp.IsRGB
                                }).FirstOrDefault();

            isRGB = profileInfo.IsRGB.GetValueOrDefault();

            ApprovalPageProcessingDetails.CustomICCProfileDetails profileDet = new ApprovalPageProcessingDetails.
                CustomICCProfileDetails()
            {
                IsGlobal = profileInfo.Account == null,
                Name = profileInfo.Name,
                Guid = profileInfo.Guid
            };

            if (!profileDet.IsGlobal)
            {
                profileDet.AccGuid = (from ac in context.Accounts
                                  join u in context.Users on ac.ID equals u.Account
                                  join ap in context.Approvals on u.ID equals ap.Owner
                                  where ap.ID == objApproval.ID
                                  select ac.Guid).FirstOrDefault();
            }

            cstmProfId = profileInfo.ID;

            return GetCustomProfilePath(profileDet, jobFolderPath, region);
        }


        public static string GetCustomNonProfileFile(Approval objApproval, string jobFolderPath,
            string region, out bool isRGB, out int cstmProfId, DbContextBL context)
        {
            var profileInfo = (from acsp in context.AccountCustomICCProfiles
                               where acsp.Name == "AdobeRGB1998.icc" && acsp.Account == null
                               select new
                               {
                                   acsp.ID,
                                   acsp.Account,
                                   acsp.Name,
                                   acsp.Guid,
                                   acsp.IsRGB
                               }).FirstOrDefault();

            isRGB = profileInfo.IsRGB.GetValueOrDefault();

            ApprovalPageProcessingDetails.CustomICCProfileDetails profileDet = new ApprovalPageProcessingDetails.
                CustomICCProfileDetails()
            {
                IsGlobal = profileInfo.Account == null,
                Name = profileInfo.Name,
                Guid = profileInfo.Guid
            };

            if (!profileDet.IsGlobal)
            {
                profileDet.AccGuid = (from ac in context.Accounts
                                      join u in context.Users on ac.ID equals u.Account
                                      join ap in context.Approvals on u.ID equals ap.Owner
                                      where ap.ID == objApproval.ID
                                      select ac.Guid).FirstOrDefault();
            }

            cstmProfId = profileInfo.ID;

            return GetCustomProfilePath(profileDet, jobFolderPath, region);
        }


    }

    internal class WorkerThreadData
    {
        private static object syncLock = new object();

        public bool IsVideo { get; set; }
        public bool IsEnabledAmazonSQS { get; set; }
        public string ItemGuid { get; set; }
        public string MessageBody { get; set; }
        public string MessageQueueName { get; set; }

        public Amazon.SQS.Model.Message ProcessMessage { get; set; }

        public WorkerThreadData(bool isEnabelSQS, string itemGuid, Amazon.SQS.Model.Message sqsMessage, string messageBody, string messageQueueName)
        {
            lock (syncLock)
            {
                this.IsEnabledAmazonSQS = isEnabelSQS;
                this.ItemGuid = itemGuid;
                this.ProcessMessage = sqsMessage;
                this.MessageBody = messageBody;
                this.MessageQueueName = messageQueueName;
            }
        }
    }

    /// <summary>
    /// Internal class used for cleaning up temporary files and folders
    /// </summary>
    internal class CleanupObject
    {
        #region Properties

        public string Path { get; private set; }
        public bool IsFile { get; private set; }
        public bool IsRemoved { get; set; }
        public DateTime AddedTime { get; private set; }
        public int NrOfRetries { get; set; }
        #endregion

        #region Constructors
        public CleanupObject(string path, bool isFile = false)
        {
            Path = path;
            IsFile = isFile;
            IsRemoved = false;
            AddedTime = DateTime.UtcNow;
            NrOfRetries = 0;
        }
        #endregion
    }
}
